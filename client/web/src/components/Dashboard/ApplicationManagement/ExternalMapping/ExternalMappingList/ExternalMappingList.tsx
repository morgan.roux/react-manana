import React, { FC, useState, useEffect } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { Button } from '@material-ui/core';
import useStyles from './styles';
import { MANY_CRYPTO_MD5_cryptoMd5s } from '../../../../../graphql/CryptoMd5/types/MANY_CRYPTO_MD5';
import ExternalMappingTable from './ExternalMappingTable';
import { Action } from '../ExternalMapping';

export interface ExternalMappingListProps {
  buttonAction?: (param: any) => void;
  md5Application: MANY_CRYPTO_MD5_cryptoMd5s;
  added: boolean;
  deleted: boolean;
}

export interface MappingData {
  id: string;
  userName: string;
  email: string;
  idExternaluser: string;
  idClient: string;
  password: string;
  userID: string;
}

const ExternalMappingList: FC<RouteComponentProps & ExternalMappingListProps> = ({
  md5Application,
  buttonAction,
  added,
  deleted,
}) => {
  const classes = useStyles({});
  const [mappingData, setMappingData] = useState<MappingData[]>([]);

  useEffect(() => {
    console.log('useEffect change 2');
    const mappingDataTemp: MappingData[] = [];
    md5Application &&
      md5Application.externalUserMappings &&
      md5Application.externalUserMappings.map(mapping => {
        mapping &&
          mappingDataTemp.push({
            id: mapping.id,
            email: (mapping.user && mapping.user.email) || '',
            userName: (mapping.user && mapping.user.userName) || '',
            idClient: mapping.idClient || '',
            idExternaluser: mapping.idExternaluser || '',
            password: mapping.password || '',
            userID: (mapping.user && mapping.user.id) || '',
          });
      });
    setMappingData(mappingDataTemp);
  }, [md5Application, added, deleted]);

  return (
    <>
      <div className={classes.marginTop}>
        <div>
          <h3 className={classes.titleApps}>
            {' '}
            {md5Application.SsoApplication && md5Application.SsoApplication.nom}{' '}
          </h3>
          <Button
            variant="contained"
            color="primary"
            className={classes.buttonAdd}
            onClick={() =>
              buttonAction &&
              buttonAction({
                action: Action.add,
                md5Id: md5Application.id,
                externalMappings: md5Application.externalUserMappings,
                ssoApplicationName:
                  (md5Application.SsoApplication && md5Application.SsoApplication.nom) || '',
              })
            }
          >
            Ajouter un mapping
          </Button>
        </div>
        <div className={classes.root}>
          <ExternalMappingTable
            mappings={mappingData}
            buttonAction={buttonAction}
            md5Application={md5Application}
            ssoApplicationName={
              (md5Application.SsoApplication && md5Application.SsoApplication.nom) || ''
            }
          />
        </div>
      </div>
    </>
  );
};
export default withRouter(ExternalMappingList);
