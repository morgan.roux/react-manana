import React, { FC, useState, useEffect } from 'react';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { ExternalMappingInput } from '../../../../../../types/graphql-global-types';
import { DataInterface, DialogAction } from '../AddExternalMapping';
import useForm from '../CustomHooks/useForm';
import CustomSelect from '../../../../../Common/CustomSelect';
import CustomButton from '../../../../../Common/CustomButton';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import { USERS_GROUPEMENT_usersGroupement } from '../../../../../../graphql/User/types/USERS_GROUPEMENT';
import { MANY_CRYPTO_MD5_cryptoMd5s } from '../../../../../../graphql/CryptoMd5/types/MANY_CRYPTO_MD5';

interface FormProps {
  defaultData?: ExternalMappingInput;
  isAdd: boolean;
  userList: USERS_GROUPEMENT_usersGroupement[];
  isNew: boolean;
  appToShow?: MANY_CRYPTO_MD5_cryptoMd5s[];
  action(data: DataInterface): void;
}

interface AppToShow {
  md5Id: string;
  nom: string;
}

const styles = () =>
  createStyles({
    textareaStyle: {
      marginBottom: 25,
      overflowY: 'auto',
    },
    marginBottom: {
      marginBottom: 25,
    },
    marginRight: {
      marginRight: 10,
    },
    titleForm: {
      color: '#1D1D1D',
      marginTop: 0,
    },
    flex: {
      display: 'flex',
      flexDirection: 'row',
    },
    w100: {
      width: '100%',
    },
  });

const Form: FC<RouteComponentProps<any, any, any> & WithStyles & FormProps> = ({
  classes,
  defaultData,
  action,
  isAdd,
  userList,
  appToShow,
  isNew,
}) => {
  const { values, handleChange, handleSubmit } = useForm(action, isAdd, isNew, defaultData);
  const { userID, idClient, idExternaluser, password, md5Id } = values;
  const [appToShowState, setAppToShowState] = useState<AppToShow[]>([]);

  const canSubmit = (): boolean => {
    if (isNew) {
      return md5Id && userID && idClient && idExternaluser && password ? true : false;
    }
    return userID && idClient && idExternaluser && password ? true : false;
  };

  useEffect(() => {
    const appTemp: AppToShow[] = [];
    appToShow &&
      appToShow.map(app => {
        appTemp.push({
          md5Id: app.id,
          nom: (app.SsoApplication && app.SsoApplication.nom) || '',
        });
      });
    setAppToShowState(appTemp);
    console.log('appTemp ', appTemp);
  }, [appToShow]);

  return (
    <>
      <form
        onSubmit={() =>
          action({
            dialogAction: isAdd ? DialogAction.addMapping : DialogAction.updateMapping,
          })
        }
      >
        {appToShow && isNew && (
          <div className={classes.marginBottom}>
            <CustomSelect
              label="Applications"
              list={appToShowState}
              index="nom"
              listId="md5Id"
              name="md5Id"
              value={md5Id}
              onChange={handleChange}
              disabled={false}
            />
          </div>
        )}
        <div className={classes.marginBottom}>
          <CustomSelect
            label="Utilisateur"
            list={userList}
            index="userName"
            listId="id"
            name="userID"
            value={userID}
            onChange={handleChange}
            disabled={!isAdd}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="idClient"
            label="ID client"
            onChange={handleChange}
            value={idClient}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="idExternaluser"
            label="ID externe"
            onChange={handleChange}
            value={idExternaluser}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="password"
            label="Mot de passe"
            onChange={handleChange}
            value={password}
            required={true}
          />
        </div>
        <div className={`${classes.marginBottom} ${classes.flex}`}>
          <div className={classes.w100}>
            <CustomButton
              size="large"
              color="primary"
              fullWidth={true}
              disabled={!canSubmit()}
              onClick={handleSubmit}
            >
              ENREGISTRER
            </CustomButton>
          </div>
        </div>
      </form>
    </>
  );
};

export default withStyles(styles, { withTheme: true })(withRouter(Form));
