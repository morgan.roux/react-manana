import { useState, ChangeEvent } from 'react';
import { ExternalMappingInput } from '../../../../../../types/graphql-global-types';
import { DataInterface, DialogAction } from '../AddExternalMapping';

const useForm = (
  callback: (data: DataInterface) => void,
  isAdd: boolean,
  isNew: boolean,
  defaultState?: ExternalMappingInput,
) => {
  const initialState: ExternalMappingInput = defaultState || {
    md5Id : '',
    userID : '',
    idClient: '',
    idExternaluser: '',
    password: ''
  };

  const [values, setValues] = useState<ExternalMappingInput>(initialState);

  const handleSubmit = (e: any) => {
    if (e) e.preventDefault();
    let action: DialogAction;
    if (isAdd && isNew) {
      console.log("isadd isnew");
      action = DialogAction.addNewMapping
    }
    else if(isAdd && !isNew){
      console.log("isAdd !isnew");
      action = DialogAction.addMapping
    }
    else{
      console.log("else");
      action = DialogAction.updateMapping
    }
    callback({
      dialogAction: action, 
      data: values
    });
  };

  const handleChange = (e: ChangeEvent<any>) => {
    const { name, value, checked, type } = e.target;
    e.persist();
    const valueResult = (type && type === 'checkbox') ? checked : 
      (type === 'number' ? parseInt(value) : value);
    setValues(prevState => ({ ...prevState,  [name]: valueResult }));
  };

  return {
    handleChange,
    handleSubmit,
    values,
  };
};

export default useForm;
