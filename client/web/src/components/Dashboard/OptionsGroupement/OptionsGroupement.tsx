import React, { FC } from 'react';
import Parameters from '../../Common/Parameters';

const OptionsGroupement: FC = () => {
  return <Parameters withSearchInput={true} baseUrl="/db/options-groupement" where="inDashboard" />;
};

export default OptionsGroupement;
