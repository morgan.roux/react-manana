import React, { FC, useState } from 'react';
import { Box, IconButton, Typography } from '@material-ui/core';
import { Backdrop, CardTheme, CardThemeList, ErrorPage } from '@app/ui-kit';
import { Add, Cached, Edit } from '@material-ui/icons';
import NoItemContentImage from '../../Common/NoItemContentImage';
import { getPharmacie, getUser } from '../../../services/LocalStorage';
import { useStyles } from './style';
import { TITULAIRE_PHARMACIE } from '../../../Constant/roles';
import useFicheTypesWithItemScoringOrdre from '../../hooks/demarche-qualite/useFicheTypesWithItemScoringOrdre';
import useUpdateItemScoringForAllUsers from '../../hooks/tools/useUpdateItemScoringForAllUsers';
import useSaveOneItemScoringPersonnalisation from '../../hooks/tools/useSaveOneItemScoringPersonnalisation';
import ItemScoringForm from './ItemScoringForm/ItemScoringForm';
import { GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDRE_dQFicheTypes_nodes } from '../../../federation/demarche-qualite/fiche-type/types/GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDRE';

const DashboardItemScoring: FC<{}> = ({}) => {
  const classes = useStyles({});

  const user = getUser();
  const pharmacie = getPharmacie();
  const isTitulairePharmacie = user?.role?.code === TITULAIRE_PHARMACIE;
  const { loading, error, data, refetch } = useFicheTypesWithItemScoringOrdre(pharmacie.id);
  const [updateItemScoring, updatingItemScoring] = useUpdateItemScoringForAllUsers();
  const [savePersonnalisation, savingPersonnalisation] = useSaveOneItemScoringPersonnalisation();

  const [openEditModal, setOpenEditModal] = useState<boolean>(false);
  const [ficheTypeToEdit, setFicheTypeToEdit] = useState<
    GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDRE_dQFicheTypes_nodes
  >();
  const [ordre, setOrdre] = useState<number>();

  const handleUpdateItemScoring = () => {
    updateItemScoring();
  };

  const handleSavePersonnalisation = () => {
    if (ordre && ficheTypeToEdit?.id) {
      savePersonnalisation(
        {
          ordre,
          idTypeAssocie: ficheTypeToEdit?.id,
          type: 'DQ_FICHE_TYPE',
        },
        ficheTypeToEdit?.itemScoringIdPersonnalisation as any,
      ).then(() => {
        refetch();
        setOpenEditModal(false);
      });
    }
  };

  if (loading) return <Backdrop value="Récuperation de données..." open={true} />;

  if (error) {
    return <ErrorPage />;
  }

  return (
    <>
      <ItemScoringForm
        open={openEditModal}
        setOpen={setOpenEditModal}
        libelle={ficheTypeToEdit?.libelle || ''}
        ordre={ordre}
        setOrdre={setOrdre}
        saving={savingPersonnalisation.loading || false}
        onSubmit={handleSavePersonnalisation}
      />
      {data?.dQFicheTypes.nodes.length || 0 > 0 ? (
        <Box>
          <div className={classes.headerContent}>
            <div>Types des fiches d'incident</div>
            <Typography>Types des fiches d'incident</Typography>
            <IconButton
              onClick={handleUpdateItemScoring}
              disabled={updatingItemScoring.loading}
              className={classes.iconReload}
            >
              <Cached color={updatingItemScoring.loading ? 'disabled' : 'primary'} />
            </IconButton>
          </div>
          <CardThemeList>
            {data?.dQFicheTypes.nodes
              .sort((a, b) => (a?.itemScoringOrdre || 0) - (b?.itemScoringOrdre || 0))
              .map((item: any, index: number) => {
                return (
                  <CardTheme
                    applyMinHeight={false}
                    key={item.id}
                    title={item.libelle}
                    littleComment={`Score : ${
                      item.itemScoringOrdre ? `${item.itemScoringOrdre}` : '-'
                    }`}
                    setCurrentId={console.log}
                    onClick={console.log}
                    moreOptions={[
                      {
                        menuItemLabel: {
                          title: `Modifier le score`,
                          icon: <Edit />,
                        },
                        onClick: event => {
                          event.preventDefault();
                          event.stopPropagation();
                          setFicheTypeToEdit(item);
                          setOpenEditModal(true);
                          setOrdre(item.itemScoringOrdre || undefined);
                        },
                      },
                    ]}
                  />
                );
              })}
          </CardThemeList>
        </Box>
      ) : (
        <NoItemContentImage title="Aucun type des fiches d'incident" subtitle="" />
      )}
    </>
  );
};

export default DashboardItemScoring;
