import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    headerContent: {
      padding: 10,
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      fontWeight: 'bold',
      background: theme.palette.primary.main,
      opacity: 0.8,
      color: '#fff',
      alignItems: 'center',
    },
    iconReload: {
      '& svg': {
        fill: '#fff',
      },
    },
  }),
);

export default useStyles;
