import { useMutation, useQuery, useApolloClient } from '@apollo/react-hooks';
import React, { FC, useState } from 'react';
import { getPharmacie, getUser } from './../../../services/LocalStorage';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import { displaySnackBar } from '../../../utils/snackBarUtils';

import { FrigoPage } from '@app/ui-kit';
import { ME_me } from '../../../graphql/Authentication/types/ME';
import {
  useValueParameterAsBoolean,
  useValueParameterAsString,
} from '../../../utils/getValueParameter';
import {
  CREATE_FRIGO,
  UPDATE_FRIGO,
  DELETE_ONE_FRIGO,
} from '../../../federation/tools/releve-temperature/frigo/mutation';
import {
  GET_ROW_FRIGO,
  GET_FRIGOS,
} from '../../../federation/tools/releve-temperature/frigo/query';
import {
  GET_FRIGOS as GET_FRIGOS_TYPE,
  GET_FRIGOSVariables,
} from '../../../federation/tools/releve-temperature/frigo/types/GET_FRIGOS';
import {
  GET_ROW_FRIGO as GET_ROW_FRIGO_TYPE,
  GET_ROW_FRIGOVariables,
} from '../../../federation/tools/releve-temperature/frigo/types/GET_ROW_FRIGO';

import {
  UPDATE_FRIGO as UPDATE_FRIGO_TYPE,
  UPDATE_FRIGOVariables,
} from '../../../federation/tools/releve-temperature/frigo/types/UPDATE_FRIGO';

import {
  CREATE_FRIGO as CREATE_FRIGO_TYPE,
  CREATE_FRIGOVariables,
} from '../../../federation/tools/releve-temperature/frigo/types/CREATE_FRIGO';

import {
  DELETE_ONE_FRIGO as DELETE_ONE_FRIGO_TYPE,
  DELETE_ONE_FRIGOVariables,
} from '../../../federation/tools/releve-temperature/frigo/types/DELETE_ONE_FRIGO';

import {
  GET_TRAITEMENT_AUTOMATIQUES_TYPES as GET_TRAITEMENT_AUTOMATIQUES_TYPES_TYPE,
  GET_TRAITEMENT_AUTOMATIQUES_TYPESVariables,
} from '../../../federation/auto/traitement-automatique/types/GET_TRAITEMENT_AUTOMATIQUES_TYPES';
import { GET_TRAITEMENT_AUTOMATIQUES_TYPES } from '../../../federation/auto/traitement-automatique/query';
import {
  CREATE_MANY_TRAITEMENTS_AUTOMATIQUES as CREATE_MANY_TRAITEMENTS_AUTOMATIQUESTYPE,
  CREATE_MANY_TRAITEMENTS_AUTOMATIQUESVariables,
} from '../../../federation/auto/traitement-automatique/types/CREATE_MANY_TRAITEMENTS_AUTOMATIQUES';
import {
  CREATE_MANY_TRAITEMENTS_AUTOMATIQUES,
  DELETE_MANY_TRAITEMENTS_AUTOMATIQUES,
} from '../../../federation/auto/traitement-automatique/mutation';
import {
  DELETE_MANY_TRAITEMENTS_AUTOMATIQUES as DELETE_MANY_TRAITEMENTS_AUTOMATIQUES_TYPE,
  DELETE_MANY_TRAITEMENTS_AUTOMATIQUESVariables,
} from '../../../federation/auto/traitement-automatique/types/DELETE_MANY_TRAITEMENTS_AUTOMATIQUES';
import CommonFieldsForm from '../../Common/CommonFieldsForm';

const ParametreFrigo: FC<{}> = () => {
  const [state, setState] = useState<any>({});
  const [assignedUsers, setAssingedUsers] = useState<any[]>([]);
  const [frigoToEdit, setFrigoToEdit] = React.useState<any>(undefined);
  const [frigoToSave, setFrigoTosave] = React.useState<any>(undefined);
  const [saved, setSaved] = useState<boolean>(false);
  const [saving, setSaving] = useState<boolean>(false);

  const enableMatriceResponsabilite = useValueParameterAsBoolean('0501');
  const currentUser: ME_me = getUser();
  const pharmacie: any = getPharmacie();

  const defaultTicketMust = [
    {
      term: { isRemoved: false },
    },
    {
      bool: {
        should:
          enableMatriceResponsabilite && pharmacie
            ? [
                {
                  term: { 'pharmacie.id': pharmacie.id },
                },
              ]
            : [
                {
                  term: { 'userCreation.id': currentUser.id },
                },
              ],
        minimum_should_match: 1,
      },
    },
  ];

  const client = useApolloClient();
  const filterFrigo = {
    filter: {
      idPharmacie: {
        eq: getPharmacie().id,
      },
    },
  };

  const loadingFrigos = useQuery<GET_FRIGOS_TYPE, GET_FRIGOSVariables>(GET_FRIGOS, {
    variables: filterFrigo,
    client: FEDERATION_CLIENT,
  });

  const loadFrigoAggregate = useQuery<GET_ROW_FRIGO_TYPE, GET_ROW_FRIGOVariables>(GET_ROW_FRIGO, {
    variables: {
      filter: {
        idPharmacie: {
          eq: getPharmacie().id,
        },
      },
    },
    client: FEDERATION_CLIENT,
  });

  const loadTraitementTypes = useQuery<
    GET_TRAITEMENT_AUTOMATIQUES_TYPES_TYPE,
    GET_TRAITEMENT_AUTOMATIQUES_TYPESVariables
  >(GET_TRAITEMENT_AUTOMATIQUES_TYPES, {
    client: FEDERATION_CLIENT,
  });

  const [updateFrigo, updatingFrigo] = useMutation<UPDATE_FRIGO_TYPE, UPDATE_FRIGOVariables>(
    UPDATE_FRIGO,
    {
      onCompleted: () => {
        displaySnackBar(client, {
          message: 'Le Frigo a été modifié avec succès !',
          type: 'SUCCESS',
          isOpen: true,
        });
        setState([]);

        setSaved(true);
        setSaving(false);
      },

      onError: () => {
        displaySnackBar(client, {
          message: 'Des erreurs se sont survenues pendant la modification du Frigo',
          type: 'ERROR',
          isOpen: true,
        });
        setSaving(false);
      },
      client: FEDERATION_CLIENT,
    },
  );

  const [createFrigo, creatingFrigo] = useMutation<CREATE_FRIGO_TYPE, CREATE_FRIGOVariables>(
    CREATE_FRIGO,
    {
      onCompleted: () => {
        displaySnackBar(client, {
          message: 'Le Frigo a été créé avec succès !',
          type: 'SUCCESS',
          isOpen: true,
        });
        setState([]);
        setSaved(true);
        setSaving(false);
      },
      update: (cache, result) => {
        if (result.data?.createOneRTFrigo) {
          const previousList = cache.readQuery<GET_FRIGOS_TYPE, GET_FRIGOSVariables>({
            query: GET_FRIGOS,
            variables: filterFrigo,
          })?.rTFrigos;

          cache.writeQuery<GET_FRIGOS_TYPE, GET_FRIGOSVariables>({
            query: GET_FRIGOS,
            variables: filterFrigo,
            data: {
              rTFrigos: {
                ...(previousList || {}),
                nodes: [...(previousList?.nodes || []), result.data.createOneRTFrigo],
              } as any,
            },
          });
        }
      },

      onError: () => {
        displaySnackBar(client, {
          message: 'Des erreurs se sont survenues pendant la création du Frigo',
          type: 'ERROR',
          isOpen: true,
        });
        setSaving(false);
      },
      client: FEDERATION_CLIENT,
    },
  );

  const [deleteFrigo, deletingFrigo] = useMutation<
    DELETE_ONE_FRIGO_TYPE,
    DELETE_ONE_FRIGOVariables
  >(DELETE_ONE_FRIGO, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le Frigo a été supprimé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
    },
    update: (cache, result) => {
      if (result.data?.deleteOneRTFrigo) {
        const previousList = cache.readQuery<GET_FRIGOS_TYPE, GET_FRIGOSVariables>({
          query: GET_FRIGOS,
          variables: filterFrigo,
        })?.rTFrigos;

        cache.writeQuery<GET_FRIGOS_TYPE, GET_FRIGOSVariables>({
          query: GET_FRIGOS,
          variables: filterFrigo,
          data: {
            rTFrigos: {
              ...(previousList || {}),
              nodes:
                previousList?.nodes?.filter(
                  frigo => frigo.id !== result.data?.deleteOneRTFrigo.id,
                ) || [],
            } as any,
          },
        });
      }
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression du Frigo',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const handleChangeTache = value => {
    handleChange({ target: { name: 'projet', value } });
  };

  const [createManyTraitement, creatingManyTraitement] = useMutation<
    CREATE_MANY_TRAITEMENTS_AUTOMATIQUESTYPE,
    CREATE_MANY_TRAITEMENTS_AUTOMATIQUESVariables
  >(CREATE_MANY_TRAITEMENTS_AUTOMATIQUES, {
    onCompleted: result => {
      displaySnackBar(client, {
        message: 'Le traitement automatique a été créé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      if (frigoToSave.id) {
        const idTraitements = frigoToSave.traitements
          .filter(traitement => traitement.id)
          .map(traitement => traitement.id);
        updateFrigo({
          variables: {
            id: frigoToSave.id,
            input: {
              ...frigoToSave,
              id: undefined,
              temporisation: frigoToSave.temporisation || 30,
              traitements: undefined,
              idTraitements: [
                ...idTraitements,
                ...result.createManyTATraitements.map(traitement => traitement.id),
              ],
            },
          },
        });
      } else {
        createFrigo({
          variables: {
            input: {
              ...frigoToSave,
              temporisation: frigoToSave.temporisation || 30,
              traitements: undefined,
              idTraitements: result.createManyTATraitements.map(traitement => traitement.id),
            },
          },
        });
      }
      setState([]);
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la création du traitement automatique',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [deleteManyTraitement, deletingManyTraitement] = useMutation<
    DELETE_MANY_TRAITEMENTS_AUTOMATIQUES_TYPE,
    DELETE_MANY_TRAITEMENTS_AUTOMATIQUESVariables
  >(DELETE_MANY_TRAITEMENTS_AUTOMATIQUES, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le traitement automatique a été supprimé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression du traitement automatique',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const handleUserDestinationChanges = (selected: any) => {
    setAssingedUsers(selected);
  };

  const handleChange = event => {
    if (event.target) {
      const { value, name } = event.target;
      setState(prevState => ({ ...prevState, [name]: value }));
    }
  };

  const handleRequestEdit = (frigo: any) => {
    setFrigoToEdit(frigo);
  };

  const handleRequestDelete = (frigo: any) => {
    deleteFrigo({
      variables: {
        input: { id: frigo.id },
      },
    });
    deleteManyTraitement({
      variables: {
        ids: frigo.traitements.map(traitement => traitement.id),
        //   input: [...traitementsToDelete.map(traitement => traitement.id )]
      },
    });
  };

  const handleRequestSaveTraitement = (traitement: any): any => {
    const projet = state.projet;
    console.log(
      '---------------------------Traitement frigo-----------------------------: ',
      traitement,
    );
    return {
      ...traitement,
      idUrgence: state.idUrgence,
      idFonction: projet?.idFonction,
      idTache: projet?.idTache,
      idImportance: state.idImportance,
      idUserParticipants: assignedUsers.map(({ id }) => id),
    };
  };

  const handleRequestSave = (frigo: any) => {
    setSaved(false);
    setSaving(true);
    if (
      frigo.typeReleve === 'MANUEL' &&
      ((frigo?.traitements && frigo?.traitements?.length > 0) ||
        (frigoToEdit?.traitements && frigoToEdit?.traitements?.length > 0))
    ) {
      // console.log('frigoTraitement', frigo.traitements);
      // console.log('frigoto Edit Traitement', frigoToEdit?.traitements);
      const traitementsToAdd = frigo.traitements.filter(
        traitement =>
          !frigoToEdit?.traitements.some(traitementEdit => traitementEdit.id === traitement.id),
      );
      const traitementsToDelete = frigoToEdit?.traitements.filter(
        traitementEdit =>
          !frigo.traitements.some(traitement => traitementEdit.id === traitement.id),
      );

      setFrigoTosave(frigo);

      if (traitementsToAdd && traitementsToAdd.length !== 0) {
        createManyTraitement({
          variables: {
            input: [...traitementsToAdd.map(traitement => ({ ...traitement, id: undefined }))],
          },
        });
      }

      if (traitementsToDelete && traitementsToDelete.length !== 0) {
        const idTraitements = frigo.traitements
          .filter(traitement => traitement.id)
          .map(traitement => traitement.id);
        // console.log('idTraitements', idTraitements);
        deleteManyTraitement({
          variables: {
            ids: traitementsToDelete.map(traitement => traitement.id),
          },
        }).then(() =>
          updateFrigo({
            variables: {
              id: frigo.id,
              input: {
                ...frigo,
                id: undefined,
                temporisation: frigo.temporisation || 30,
                traitements: undefined,
                idTraitements,
              },
            },
          }),
        );
      }
    } else {
      if (frigo.id) {
        updateFrigo({
          variables: {
            id: frigo.id,
            input: {
              nom: frigo.nom,
              temperatureHaute: frigo.temperatureHaute,
              temperatureBasse: frigo.temperatureBasse,
              temporisation: frigo.temporisation || 30,
              typeReleve: frigo.typeReleve,
              capteurs: frigo.capteurs,
            },
          },
        });
      } else {
        createFrigo({
          variables: {
            input: {
              nom: frigo.nom,
              temperatureHaute: frigo.temperatureHaute,
              temperatureBasse: frigo.temperatureBasse,
              temporisation: frigo.temporisation || 30,
              typeReleve: frigo.typeReleve,
              capteurs: frigo.capteurs,
            },
          },
        });
      }
    }
  };

  return (
    <FrigoPage
      saved={saved}
      setSaved={setSaved}
      saving={saving}
      onRequestSaveTraitement={handleRequestSaveTraitement}
      loading={loadingFrigos.loading}
      error={loadingFrigos.error}
      data={(loadingFrigos.data?.rTFrigos.nodes || []) as any}
      rowsTotal={
        (loadFrigoAggregate.data?.rTFrigoAggregate.length || 0) > 0
          ? loadFrigoAggregate.data?.rTFrigoAggregate[0].count?.id || 0
          : 0
      }
      onRequestEdit={handleRequestEdit}
      onRequestSave={handleRequestSave}
      onRequestDelete={handleRequestDelete}
      idUrgence={state.idUrgence || ''}
      idImportance={state.idImportance || ''}
      collaborateurs={assignedUsers}
      idTache={state.idProject || ''}
      commonFieldsComponent={
        <CommonFieldsForm
          selectedUsers={assignedUsers}
          urgence={state.idUrgence}
          urgenceProps={{
            useCode: false,
          }}
          usersModalProps={{
            withNotAssigned: false,
            searchPlaceholder: 'Rechercher...',
            withAssignTeam: false,
            singleSelect: false,
          }}
          projet={state.projet}
          importance={state.idImportance ? state.idImportance : undefined}
          onChangeUsersSelection={handleUserDestinationChanges}
          onChangeUrgence={urgence =>
            handleChange({ target: { name: 'idUrgence', value: urgence.id } })
          }
          onChangeImportance={importance =>
            handleChange({ target: { name: 'idImportance', value: importance.id } })
          }
          onChangeProjet={projet => {
            handleChangeTache(projet);
          }}
        />
      }
      frigoToEdit={frigoToEdit}
      traitementTypes={{
        loading: loadTraitementTypes.loading,
        error: loadTraitementTypes.error,
        data: loadTraitementTypes.data?.tATraitementTypes.nodes,
      }}
    />
  );
};

export default ParametreFrigo;
