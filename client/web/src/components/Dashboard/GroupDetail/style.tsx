import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const styles = makeStyles((theme: Theme) =>
  createStyles({
    contentDetailsGroupeDamis: {
      maxWidth: 1300,
      margin: '0 auto',
      padding: theme.spacing(3, 2),
      '& .MuiTabs-flexContainer': {
        justifyContent: 'center',
        maxWidth: 340,
        margin: '2px auto',
        boxShadow: '0px 3px 6px #00000029',
      },
    },
    contentTabs: {
      padding: '24px 0 0',
      '& > div:nth-child(2)': {
        padding: theme.spacing(3, 0),
      },
    },
  }),
);

export default styles;
