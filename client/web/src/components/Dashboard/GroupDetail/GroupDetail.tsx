import React, { FC, useEffect, useState } from 'react';
import { Box, Tab, Tabs } from '@material-ui/core';
import { Add, ArrowBack, Close } from '@material-ui/icons';
import { responsePathAsArray } from 'graphql';

import { useHistory, useParams } from 'react-router';
import CustomButton from '../../Common/CustomButton';
import CustomContent from '../../Common/newCustomContent';
import CustomContentSearchInput from '../../Common/newCustomContent/SearchInput/SearchInput';
import SubToolbar from '../../Common/newCustomContent/SubToolbar';
import style from './style';
import { useGroupDetailColumns } from './useGroupDetailColumns';
import uniq from 'lodash';
import { useLeaveGroup } from './leaveGroup';
import Backdrop from '../../Common/Backdrop';
import { IDENTIFIER_PREFIX } from '../Groupement/Groupement';
import InviteFriend from '../../Main/Content/Friends/InviterFriends/InviterFriend';
import ConfirmDeleteDialog from '../../Common/ConfirmDialog/ConfirmDeleteDialog';
import { useQuery } from '@apollo/react-hooks';
import { GROUP_AMIS, GROUP_AMISVariables } from '../../../graphql/GroupDetail/types/GROUP_AMIS';
import { DO_GROUP_AMIS } from '../../../graphql/GroupDetail/query';

interface GroupAmisDetailProps {
  listResult: any;
}

const GroupDetail: FC<GroupAmisDetailProps> = ({ listResult }) => {
  const classes = style();
  const history = useHistory();
  const [tabsIndex, setTabsIndex] = React.useState(0);
  const { idgroup }: any = useParams();
  const [openModal, setopenModal] = useState(false);
  const [selected, setSelected] = useState<any[]>([]);

  const { data } = useQuery<GROUP_AMIS, GROUP_AMISVariables>(DO_GROUP_AMIS, {
    variables: { id: idgroup },
  });

  useEffect(() => {
    if (tabsIndex === 0) {
      history.push(`/db/groupes-amis/${idgroup}`);
    }
    if (tabsIndex === 1) {
      history.push(`/db/groupes-amis/${idgroup}/membre`);
    }
  }, []);

  const [open, setopen] = useState<boolean>(false);
  const { doLeaveGroup, loading } = useLeaveGroup();

  const { columnsPartage, columnsMembre } = useGroupDetailColumns();

  const onClickBack = () => {
    history.goBack();
  };

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setTabsIndex(newValue);
    if (newValue === 0) {
      history.push(`/db/groupes-amis/${idgroup}`);
    }
    if (newValue === 1) {
      history.push(`/db/groupes-amis/${idgroup}/membre`);
    }
  };
  const idSelected = selected && selected.map(select => select.id);
  const onClickLeave = () => {
    doLeaveGroup({ variables: { id: idgroup, idsPharmacies: idSelected } }).then(() => {
      setSelected([]);
      listResult.refetch();
      setopen(!open);
    });
  };
  const handleOpenModal = () => {
    setopenModal(!openModal);
  };
  const handleOpenDialogLeave = () => {
    setopen(!open);
  };

  return (
    <Box width="100%">
      <Backdrop open={loading} />
      <InviteFriend openModal={openModal} handleClose={handleOpenModal} listResult={listResult} />
      <ConfirmDeleteDialog
        open={open}
        setOpen={setopen}
        title={'Quitter Groupe'}
        onClickConfirm={onClickLeave}
        content={'Voulez-vous vraiment quitter le groupe'}
      />
      <Box>
        <SubToolbar
          dark={false}
          titleBack={''}
          title={data?.groupeAmis?.nom ?? ''}
          withBackBtn={true}
          onClickBack={onClickBack}
          backBtnIcon={<ArrowBack />}
        >
          <CustomButton
            color="default"
            startIcon={<Close />}
            disabled={selected.length > 0 ? false : true}
            onClick={handleOpenDialogLeave}
          >
            {'QUITTER GROUPE'}
          </CustomButton>
          <CustomButton
            color="secondary"
            startIcon={<Add />}
            disabled={false}
            onClick={handleOpenModal}
          >
            {'INVITER DES AMIS'}
          </CustomButton>
        </SubToolbar>
      </Box>
      <Box className={classes.contentDetailsGroupeDamis}>
        <Tabs value={tabsIndex} onChange={handleChange}>
          <Tab label="Articles partagés" key={'0'} />
          <Tab label="Liste des membres" key={'1'} />
        </Tabs>

        {tabsIndex === 0 && (
          <Box className={classes.contentTabs}>
            <CustomContentSearchInput searchPlaceholder={'Rechercher ...'} fullWidth={false} />
            <CustomContent showResetFilters={false} {...{ listResult, columns: columnsPartage }} />
          </Box>
        )}
        {tabsIndex === 1 && (
          <Box className={classes.contentTabs}>
            <CustomContentSearchInput searchPlaceholder={'Rechercher...'} fullWidth={false} />
            <CustomContent
              selected={selected}
              setSelected={setSelected}
              isSelectable={true}
              showResetFilters={false}
              {...{ listResult, columns: columnsMembre }}
            />
          </Box>
        )}
      </Box>
    </Box>
  );
};

export default GroupDetail;
