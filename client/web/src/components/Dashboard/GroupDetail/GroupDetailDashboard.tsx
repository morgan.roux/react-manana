import React from 'react';
import { useHistory, useParams } from 'react-router';
import { DO_GROUP_AMIS_DETAIL, DO_PRODUIT_CANAL_PARTAGE } from '../../../graphql/GroupDetail/query';
import WithSearch from '../../Common/newWithSearch/withSearch';
import GroupDetail from './GroupDetail';

const GroupDetailDashboard = () => {
  const { idgroup }: any = useParams();
  const history = useHistory();
  /* const mustMembre = [{ terms: { 'groupeAmis.id': idgroup } }, { term: { isRemoved: false } }]; */

  const isOnGroupe = history.location.pathname.includes('/membre');

  const paramMember = isOnGroupe
    ? [{ term: { 'groupeAmis.id': idgroup } }]
    : [{ term: { 'partage.typePartage.groupeAmisCibles.groupeAmis.id': idgroup } }];
  const must = [...paramMember, { term: { isRemoved: false } }];

  return (
    <WithSearch
      type={isOnGroupe ? 'groupeamisdetail' : 'produitcanal'}
      WrappedComponent={GroupDetail}
      searchQuery={isOnGroupe ? DO_GROUP_AMIS_DETAIL : DO_PRODUIT_CANAL_PARTAGE}
      optionalMust={must}
    />
  );
};

export default GroupDetailDashboard;
