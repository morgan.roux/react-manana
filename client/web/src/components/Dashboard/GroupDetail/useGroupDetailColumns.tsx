import moment from 'moment';
import VisibilityIcon from '@material-ui/icons/Visibility';
import React, { Fragment } from 'react';
import { Box, IconButton } from '@material-ui/core';

export const useGroupDetailColumns = () => {
  const columnsPartage = [
    {
      label: 'Photo',
      name: 'publicUrl',
      renderer: (row: any) => {
        return row?.produit?.produitPhoto?.fichier?.publicUrl || '-';
      },
    },
    {
      label: 'Titre',
      name: 'Titre',
      renderer: (row: any) => {
        return row?.produit?.libelle || '-';
      },
    },
    {
      label: 'Pharmacies',
      name: 'pharmacieRemisePanachees.nom',
    },
    {
      label: 'Personnes',
      name: 'fullName',
      renderer: (row: any) => {
        return row?.pharmacieRemisePanachees?.titulaires?.fullName || '-';
      },
    },
    {
      label: 'Fonctions',
      name: 'Fonctions',
      renderer: (row: any) => {
        return row?.pharmacieRemisePanachees?.users?.role?.nom || '-';
      },
    },
    {
      label: 'Recommandation',
      name: 'Recommandation',
      renderer: (row: any) => {
        return row?.partage?.typePartage === 'RECOMMANDATION' ? 'OUI' : 'NON';
      },
    },
    {
      label: '',
      name: '',
      renderer: (row: any) => {
        return (
          <IconButton>
            <VisibilityIcon />
          </IconButton>
        );
      },
    },
  ];

  const columnsMembre = [
    {
      label: 'Date',
      name: 'dateCreation',
      renderer: (row: any) => {
        return row.dateCreation ? moment(row.dateCreation).format('DD/MM/YYYY') : '-';
      },
    },
    {
      label: 'CIP',
      name: 'CIP',
      renderer: (row: any) => {
        return row?.pharmacie?.cip ?? '-';
      },
    },
    {
      label: 'Pharmacie',
      name: 'Pharmacie',
      renderer: (row: any) => {
        return row?.pharmacie?.nom ?? '-';
      },
    },
    {
      label: 'Titulaire',
      name: 'Titulaire',
      renderer: (row: any) => {
        return row?.pharmacie?.titulaires[0].fullName ?? '-';
      },
    },
    {
      label: 'Code postale',
      name: 'Code postale',
      renderer: (row: any) => {
        return row?.pharmacie?.cp ?? '-';
      },
    },
    {
      label: 'Ville',
      name: 'Ville',
      renderer: (row: any) => {
        return row?.pharmacie?.ville ?? '-';
      },
    },
    {
      label: 'Nom du département',
      name: 'Nom du département',
      renderer: (row: any) => {
        return row?.pharmacie?.departement?.nom ?? '-';
      },
    },
    {
      label: '',
      name: '',
      renderer: (row: any) => {
        return (
          <IconButton>
            <VisibilityIcon />
          </IconButton>
        );
      },
    },
  ];

  return {
    columnsPartage,
    columnsMembre,
  };
};
