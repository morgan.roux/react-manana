import { useQuery } from '@apollo/react-hooks';
import { GET_CHECKEDS_PHARMACIE } from '../../../../graphql/Pharmacie/local';

export const useCheckedsPharmacie = () => {
  const checkedsPharmacie = useQuery(GET_CHECKEDS_PHARMACIE);
  const checkeds =
    checkedsPharmacie && checkedsPharmacie.data && checkedsPharmacie.data.checkedsPharmacie;
  return checkeds;
};
