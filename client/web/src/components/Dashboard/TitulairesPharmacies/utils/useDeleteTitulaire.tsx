import { useContext } from 'react';
import { ContentStateInterface, ContentContext } from '../../../../AppContext';
import { useCheckedsTitulaire } from './useCheckedsTitulaire';
import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { DO_DELETE_TITULAIRES } from '../../../../graphql/Titulaire/mutation';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import {
  deleteSoftTitulaires,
  deleteSoftTitulairesVariables,
} from '../../../../graphql/Titulaire/types/deleteSoftTitulaires';
import { differenceBy } from 'lodash';

export const useDeleteTitulaire = () => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const checkeds = useCheckedsTitulaire();

  const client = useApolloClient();

  const [deletePersonnels] = useMutation<deleteSoftTitulaires, deleteSoftTitulairesVariables>(
    DO_DELETE_TITULAIRES,
    {
      update: (cache, { data }) => {
        if (data && data.deleteSoftTitulaires) {
          if (variables && operationName) {
            const req: any = cache.readQuery({
              query: operationName,
              variables: variables,
            });
            if (req && req.search && req.search.data) {
              const source = req.search.data;
              const result = data.deleteSoftTitulaires;
              const dif = differenceBy(source, result, 'id');
              cache.writeQuery({
                query: operationName,
                data: {
                  search: {
                    ...req.search,
                    ...{
                      data: dif,
                    },
                  },
                },
                variables: variables,
              });
              client.writeData({
                data: { checkedsTitulairePharmacie: differenceBy(checkeds, result, 'id') },
              });
            }
          }
        }
      },
      onError: errors => {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
        });
      },
    },
  );

  return deletePersonnels;
};
