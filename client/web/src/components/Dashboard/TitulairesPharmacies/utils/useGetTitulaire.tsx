import { useLazyQuery, useApolloClient, QueryLazyOptions } from '@apollo/react-hooks';
import { TITULAIRE, TITULAIREVariables } from '../../../../graphql/Titulaire/types/TITULAIRE';
import { GET_TITULAIRE } from '../../../../graphql/Titulaire/query';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import { createUpdateTitulaireVariables } from '../../../../graphql/Titulaire/types/createUpdateTitulaire';

export const useGetTitulaire = (
  setValues: React.Dispatch<React.SetStateAction<createUpdateTitulaireVariables>>,
): [(options: QueryLazyOptions<TITULAIREVariables> | undefined) => void] => {
  const client = useApolloClient();

  const [getTitulaire] = useLazyQuery<TITULAIRE, TITULAIREVariables>(GET_TITULAIRE, {
    onCompleted: data => {
      if (data && data.titulaire) {
        const titulaire = data.titulaire;
        const idPharmacies =
          (titulaire &&
            titulaire.pharmacies &&
            titulaire.pharmacies.map(pharmacie => (pharmacie && pharmacie.id) || '')) ||
          [];
        const nom = (titulaire && titulaire.nom) || '';
        const civilite = (titulaire && titulaire.civilite) || '';
        const prenom = (titulaire && titulaire.prenom) || '';
        const id = (titulaire && titulaire.id) || '';
        const contact = titulaire &&
          titulaire.contact && {
            adresse1: titulaire.contact.adresse1,
            adresse2: titulaire.contact.adresse2,
            cp: titulaire.contact.cp,
            faxPerso: titulaire.contact.faxPerso,
            faxProf: titulaire.contact.adresse1,
            mailPerso: titulaire.contact.mailPerso,
            mailProf: titulaire.contact.mailProf,
            pays: titulaire.contact.pays,
            sitePerso: titulaire.contact.sitePerso,
            siteProf: titulaire.contact.siteProf,
            telMobPerso: titulaire.contact.telMobPerso,
            telMobProf: titulaire.contact.telMobProf,
            telPerso: titulaire.contact.telPerso,
            telProf: titulaire.contact.telProf,
            ville: titulaire.contact.ville,
          };

        setValues({
          idPharmacies,
          nom,
          civilite,
          prenom,
          id,
          contact,
        });

        client.writeData({
          data: { checkedsPharmacie: idPharmacies.map(id => ({ id, __typename: 'Pharmacie' })) },
        });
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        });
      });
    },
  });

  return [getTitulaire];
};
