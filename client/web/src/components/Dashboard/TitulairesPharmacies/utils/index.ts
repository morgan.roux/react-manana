import { useTitulaireColumns } from './useTitulaireColumns';
import { useCheckedsTitulaire } from './useCheckedsTitulaire';
import useTitulaireForm from './useTitulaireForm';
import { useCheckedsPharmacie } from './useCheckedsPharmacie';
import { useDeleteTitulaire } from './useDeleteTitulaire';

export {
  useTitulaireColumns,
  useCheckedsTitulaire,
  useTitulaireForm,
  useCheckedsPharmacie,
  useDeleteTitulaire,
};
