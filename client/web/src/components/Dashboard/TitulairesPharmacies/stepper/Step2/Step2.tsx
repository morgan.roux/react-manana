import * as React from 'react';
import FormContainer from '../../../../Common/FormContainer';
import InputRow from '../../../../Common/InputRow';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import { createUpdateTitulaireVariables } from '../../../../../graphql/Titulaire/types/createUpdateTitulaire';
import { ContactInput } from '../../../../../types/graphql-global-types';

interface IStep2Props {
  handleChange: (e: React.ChangeEvent<any>) => void;
  values: createUpdateTitulaireVariables;
}

const Step2: React.FC<IStep2Props> = props => {
  const { values, handleChange } = props;

  const { contact } = values;

  const contactComponent = (
    <>
      <InputRow title="Bureau">
        <CustomFormTextField
          name="telProf"
          value={contact && contact.telProf}
          onChange={handleChange}
          placeholder="Son num bureau"
        />
      </InputRow>
      <InputRow title="Domicile">
        <CustomFormTextField
          name="telPerso"
          value={contact && contact.telPerso}
          onChange={handleChange}
          placeholder="Son num domicile"
        />
      </InputRow>
      <InputRow title="Fax">
        <CustomFormTextField
          name="faxProf"
          value={contact && contact.faxProf}
          onChange={handleChange}
          placeholder="Son num fax"
        />
      </InputRow>
      <InputRow title="Mobile">
        <CustomFormTextField
          name="telMobProf"
          value={contact && contact.telMobProf}
          onChange={handleChange}
          placeholder="Son num mobile"
        />
      </InputRow>
    </>
  );

  const siteComponent = (
    <>
      <InputRow title="Mail">
        <CustomFormTextField
          name="mailProf"
          value={contact && contact.mailProf}
          onChange={handleChange}
          placeholder="Son mail"
        />
      </InputRow>
      <InputRow title="Page web">
        <CustomFormTextField
          name="siteProf"
          value={contact && contact.siteProf}
          onChange={handleChange}
          placeholder="Son page web"
        />
      </InputRow>
    </>
  );

  const coordonneeComponent = (
    <>
      <InputRow title="Adresse 1">
        <CustomFormTextField
          name="adresse1"
          value={contact && contact.adresse1}
          onChange={handleChange}
          placeholder="Son adresse 1"
        />
      </InputRow>
      <InputRow title="Adresse 2">
        <CustomFormTextField
          name="adresse2"
          value={contact && contact.adresse2}
          onChange={handleChange}
          placeholder="Son adresse 2"
        />
      </InputRow>
      <InputRow title="Ville">
        <CustomFormTextField
          name="ville"
          value={contact && contact.ville}
          onChange={handleChange}
          placeholder="Sa ville"
        />
      </InputRow>
      <InputRow title="Code postal">
        <CustomFormTextField
          name="cp"
          value={contact && contact.cp}
          onChange={handleChange}
          placeholder="Son Code postal"
        />
      </InputRow>
    </>
  );

  return (
    <div>
      <FormContainer title="Internet">{siteComponent}</FormContainer>
      <FormContainer title="Numéros de téléphone">{contactComponent}</FormContainer>
      <FormContainer title="Coordonnées">{coordonneeComponent}</FormContainer>
    </div>
  );
};

export default Step2;
