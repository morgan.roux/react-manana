import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { Avatar, Button, Link } from '@material-ui/core';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import image from '../../../../assets/img/img_avatar.png';
import { PHARMACIE_URL } from '../../../../Constant/url';
import { GET_TITULAIRE } from '../../../../graphql/Titulaire/query';
import { TITULAIRE, TITULAIREVariables } from '../../../../graphql/Titulaire/types/TITULAIRE';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import { Loader } from '../../Content/Loader';
import { StyledBadge, useStyles } from './styles';

interface DetailsTitulaireProps {
  id?: string;
  hideFooterActions?: boolean;
}

const DetailsTitulaire: FC<DetailsTitulaireProps & RouteComponentProps> = ({
  match: { params },
  history,
  id,
  hideFooterActions,
}) => {
  const classes = useStyles({});
  const { idTitulaire } = params as any;
  const titreTableau: string[] = [
    'Date de naissance',
    'Adresse 1',
    'Adresse 2',
    'Code postal',
    'Ville',
    'Téléphone bureau',
    'Téléphone domicile',
    'Téléphone professionnel',
    'Fax',
    'Email professionnel',
  ];

  const client = useApolloClient();

  const { loading, error, data } = useQuery<TITULAIRE, TITULAIREVariables>(GET_TITULAIRE, {
    variables: {
      id: id || idTitulaire,
    },
    fetchPolicy: 'cache-and-network',
  });

  const handleClick = () => {
    history.goBack();
  };

  if (loading) return <Loader />;

  if (error) {
    const snackBarData: SnackVariableInterface = {
      type: 'ERROR',
      message: "Des erreurs se sont survenues pendant l'affichage de la fiche du titulaire",
      isOpen: true,
    };
    displaySnackBar(client, snackBarData);
    history.push('/');
  }

  return (
    <div className={classes.container}>
      {data && data.titulaire ? (
        <div
          style={{
            alignItems: 'center',
            background: 'white',
            width: '25%',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <div>
            <StyledBadge
              overlap="circle"
              anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
              variant="dot"
            >
              <Avatar alt="Image" src={image} className={classes.large} />
            </StyledBadge>
          </div>
          <h2>
            {`${data.titulaire.civilite} ${data.titulaire.prenom} ${data.titulaire.nom}` ||
              'Titulaire inconnu'}
          </h2>
          <div className={classes.tableau}>
            <div>
              {titreTableau.map(titre => (
                <h5
                  key={titre}
                  style={{
                    color: '#878787',
                    opacity: 1,
                    letterSpacing: 0,
                    font: 'Regular 16px/33px Montserrat',
                  }}
                >
                  {titre}
                </h5>
              ))}
            </div>
            <div>
              <h5 className={classes.h5}>Non renseignée</h5>
              <h5 className={classes.h5}>
                {data.titulaire.contact ? data.titulaire.contact.adresse1 : 'Non renseignée'}
              </h5>
              <h5 className={classes.h5}>
                {data.titulaire.contact ? data.titulaire.contact.adresse2 : 'Non renseignée'}
              </h5>
              <h5 className={classes.h5}>
                {data.titulaire.contact ? data.titulaire.contact.cp : 'Non renseigné'}
              </h5>
              <h5 className={classes.h5}>
                {data.titulaire.contact ? data.titulaire.contact.ville : 'Non renseignée'}
              </h5>
              <h5 className={classes.h5}>
                {data.titulaire.contact ? data.titulaire.contact.telProf : 'Non renseigné'}
              </h5>
              <h5 className={classes.h5}>
                {data.titulaire.contact ? data.titulaire.contact.telMobProf : 'Non renseigné'}
              </h5>
              <h5 className={classes.h5}>
                {data.titulaire.contact ? data.titulaire.contact.telPerso : 'Non renseigné'}
              </h5>
              <h5 className={classes.h5}>
                {data.titulaire.contact ? data.titulaire.contact.faxProf : 'Non renseigné'}
              </h5>
              <h5 className={classes.h5}>
                {data.titulaire.contact ? data.titulaire.contact.mailProf : 'Non renseigné'}
              </h5>
            </div>
          </div>
          <p style={{ textAlign: 'justify' }}>
            {data.titulaire.pharmacies && data.titulaire.pharmacies.length !== 0
              ? data.titulaire.pharmacies.map((pharmacie: any) =>
                  pharmacie ? (
                    <Link
                      component="button"
                      variant="body2"
                      onClick={() => history.push(`/db/${PHARMACIE_URL}/fiche/${pharmacie.id}`)}
                      style={{ color: '#e4315e', textAlign: 'left' }}
                    >
                      {pharmacie && `${pharmacie.nom}`}
                    </Link>
                  ) : (
                    '-'
                  ),
                )
              : 'Aucune pharmacie'}
          </p>
          {!hideFooterActions && (
            <p style={{ textAlign: 'justify' }}>
              <Button variant="contained" onClick={handleClick}>
                Retour
              </Button>
            </p>
          )}
        </div>
      ) : null}
    </div>
  );
};

export default withRouter(DetailsTitulaire);
