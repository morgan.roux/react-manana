import React, { FC, useState, useEffect } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Column } from '../Content/Interface';
import { getGroupement, getUser } from '../../../services/LocalStorage';
import { ResetUserPasswordModal } from '../ResetUserPasswordModal';
import { useGetColumns } from '../columns';
import CustomContent from '../../Common/newCustomContent';
import { useQuery } from '@apollo/react-hooks';
import ICurrentPharmacieInterface from '../../../Interface/CurrentPharmacieInterface';
import { GET_CURRENT_PHARMACIE } from '../../../graphql/Pharmacie/local';
import { SEARCH_FIELDS_OPTIONS } from './constants';
import { ME_me } from '../../../graphql/Authentication/types/ME';
import { SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT } from '../../../Constant/roles';
import HistoriquePersonnel from '../HistoriquePersonnel';
import { TitulaireDashboard } from '../../Common/newWithSearch/ComponentInitializer';

const TitulairesPharmacies: FC<RouteComponentProps> = ({ history, location: { pathname } }) => {
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [openHistory, setOpenHistory] = useState<boolean>(false);
  const [userId, setUserId] = useState<string>('');
  const [userLogin, setUserLogin] = useState<string>('');
  const [userEmail, setUserEmail] = useState<string>('');
  const currentUser: ME_me = getUser();

  const groupement = getGroupement();
  const id = groupement.id ? groupement.id : null;

  const handleModal = (open: boolean, userId: string, email: string, login:string) => {
    setOpenModal(open);
    setUserId(userId);
    setUserEmail(email);
    setUserLogin(login)
  };

  const handleClickHistory = (open: boolean, userId: string) => {
    setOpenHistory(open);
    setUserId(userId);
  };

  const columns: Column[] = useGetColumns(
    'TITULAIRE_PHARMACIE',
    history,
    pathname,
    handleModal,
    handleClickHistory,
  );

  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const [currentPharmacie, setcurrentPharmacie] = useState<string>('');

  let filterBy: any[] = [{ term: { idGroupement: id } }];

  if (
    currentUser &&
    currentUser.role &&
    currentUser.role.code !== SUPER_ADMINISTRATEUR &&
    currentUser.role.code !== ADMINISTRATEUR_GROUPEMENT
  ) {
    filterBy = filterBy.concat([{ term: { 'pharmacie.id': currentPharmacie } }]);
  }

  useEffect(() => {
    if (
      myPharmacie &&
      myPharmacie.data &&
      myPharmacie.data.pharmacie &&
      myPharmacie.data.pharmacie.id
    ) {
      setcurrentPharmacie(myPharmacie.data.pharmacie.id);
    }
  }, [myPharmacie]);

  return (
    <>
      <TitulaireDashboard {...{ columns }} />
      <div>
        <ResetUserPasswordModal
          open={openModal}
          setOpen={setOpenModal}
          userId={userId}
          email={userEmail}
          login={userLogin}
        />
      </div>
      <HistoriquePersonnel open={openHistory} setOpen={setOpenHistory} userId={userId} />
    </>
  );
};

export default withRouter(TitulairesPharmacies);
