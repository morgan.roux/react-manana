import { ThemeProvider } from '@material-ui/core';
import createTheme from './theme';
import { useQuery } from '@apollo/react-hooks';
import { ME } from '../../../graphql/Authentication/types/ME';
import { GET_ME } from '../../../graphql/Authentication/query';
import { availableThemes } from '../../../Theme';
import EspaceDocumentaire from './EspaceDocumentaire/EspaceDocumentaire';
import React, { FC } from 'react';
import { IFolder } from './EspaceDocumentaire/CreateFolder/CreateFolder';
import { DocumentCategorie } from '../../Main/Content/EspaceDocumentaire/EspaceDocumentaire';

interface EspaceDocumentaireProviderProps{
  documents : {
    loading : boolean
    error : Error
    data : DocumentCategorie []
  }
  categories : {
    loading : boolean
    error : Error
    data : any []
  }
  onRequestApplyReplacement : ( statut : string , document : DocumentCategorie , commentaire : string ) => void
  onRequestCreateNewFolder : ( folder : IFolder ) => void
  onRequestGetCategories : () => void
}

const EspaceDocumentaireProvider: FC<EspaceDocumentaireProviderProps> = ({
  categories,
  documents,
  onRequestApplyReplacement,
  onRequestCreateNewFolder,
  onRequestGetCategories
}) => {
  const me = useQuery<ME>(GET_ME);
  const themeName = me && me.data && me.data.me && me.data.me.theme;
  const theme = createTheme(themeName && availableThemes[themeName]);

  return (
    <ThemeProvider theme={theme}>
      <EspaceDocumentaire
        onRequestApplyReplacement={onRequestApplyReplacement}
        documents={documents}
        categories={categories}
        onRequestGetCategories={onRequestGetCategories}
        onRequestCreateNewFolder={onRequestCreateNewFolder}
      />
    </ThemeProvider>
  );
};

export default EspaceDocumentaireProvider;
