import { Box, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import DocumentAction from '../DocumentAction';
import styles from './style';
import classnames from 'classnames';
import { DocumentCategorie } from '../../../../Main/Content/EspaceDocumentaire/EspaceDocumentaire';
import moment from 'moment';

interface InfoDocumentProps {
  document: DocumentCategorie;
  withStatut?: boolean;
  statut?: 'approuver' | 'rejeter';
  setStatut?: (statut: 'approuver' | 'rejeter') => void;
  commentaire: string;
  setCommentaire: (commentaire: string) => void;
}

const InfoDocument: FC<InfoDocumentProps & { flex?: boolean }> = ({
  commentaire,
  setCommentaire,
  flex,
  document,
  statut,
  withStatut,
  setStatut,
}) => {
  const classes = styles();

  return (
    <Box className={classnames(classes.root, { [classes.flex]: flex })}>
      <Box className={classes.detail}>
        <Box>
          <Typography variant="subtitle2">Emplacement</Typography>
          <Typography variant="caption">
            {document.categorie?.libelle} &gt; {document.sousCategorie?.libelle}
          </Typography>
        </Box>
        <Box>
          <Typography variant="subtitle2">Nomenclature</Typography>
          <Typography variant="caption">{document?.nomenclature}</Typography>
        </Box>
        <Box>
          <Typography variant="subtitle2">Date de parution</Typography>
          <Typography variant="caption">
            {moment(new Date(document?.dateHeureParution as any)).format('L')}
          </Typography>
        </Box>
        <Box>
          <Typography variant="subtitle2">Date de validité</Typography>
          <Typography variant="caption">
            Début : {`${moment(new Date(document?.dateHeureDebutValidite as any)).format('L')}`}
          </Typography>
          <Typography variant="caption">
            {` `}Fin : {`${moment(new Date(document?.dateHeureFinValidite as any)).format('L')}`}
          </Typography>
        </Box>
        <Box>
          <Typography variant="subtitle2">Numéro de version</Typography>
          <Typography variant="caption">{document?.numeroVersion}</Typography>
        </Box>
        <Box>
          <Typography variant="subtitle2">Rédacteur</Typography>
          <Typography variant="caption">{document?.redacteur?.fullName}</Typography>
        </Box>
        <Box>
          <Typography variant="subtitle2">Vérificateur</Typography>
          <Typography variant="caption">{document?.verificateur?.fullName}</Typography>
        </Box>
      </Box>
      <Box className={classes.detail}>
        <Box>
          <Typography variant="subtitle2">Description</Typography>
          <Typography variant="caption">{document?.description}</Typography>
        </Box>
        <Box>
          <Typography variant="subtitle2">Mot clés</Typography>
          <Typography variant="caption">
            {` ${document?.motCle1 ? document.motCle1 + ' , ' : ''}
                ${document?.motCle2 ? document.motCle2 + ' , ' : ''}
                ${document?.motCle3 ? document.motCle3 : ''}
               `}
          </Typography>
        </Box>
        <DocumentAction
          withStatut={withStatut}
          commentaire={commentaire}
          setCommentaire={setCommentaire}
          statut={statut}
          setStatut={setStatut}
        />
      </Box>
    </Box>
  );
};

export default InfoDocument;
