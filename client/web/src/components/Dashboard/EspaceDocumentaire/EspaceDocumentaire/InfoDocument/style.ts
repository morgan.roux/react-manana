import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
  root:{
     flexGrow:1
  },
  detail: {
    padding: theme.spacing(2),
    minWidth: '40%',
    '@media(min-width: 1280px)': {
      maxWidth: '50%',
    },
    '&>div': {
      marginBottom: theme.spacing(2),
    },
  },
  flex: {
    display: 'flex',
    alignItems: 'flex-start',
  },
}));
