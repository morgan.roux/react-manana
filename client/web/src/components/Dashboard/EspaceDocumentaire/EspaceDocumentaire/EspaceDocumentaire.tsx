import {
  AppBar,
  Box,
  Button,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField,
  Toolbar,
  Typography,
} from '@material-ui/core';
import { Add, Create, Refresh, Search, Tune } from '@material-ui/icons';
import React, { FC, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import style from './style';
import ModalDocument from './ModalDocument';
import CreateFolder from './CreateFolder';
import ValidateDocument from './ValidateDocument';
import { IFolder } from './CreateFolder/CreateFolder';
import { Column, ErrorPage, Loader, Table as Tableau } from '@app/ui-kit';
import { DocumentCategorie } from '../../../Main/Content/EspaceDocumentaire/EspaceDocumentaire';
import moment from 'moment';

interface EspaceDocumentaireProps {
  categories: {
    loading: boolean;
    error: Error;
    data: any[];
  };
  documents: {
    loading: boolean;
    error: Error;
    data: DocumentCategorie[];
  };
  onRequestApplyReplacement: (
    statut: string,
    document: DocumentCategorie,
    commentaire: string,
  ) => void;
  onRequestGetCategories: () => void;
  onRequestCreateNewFolder: (folderInfo: IFolder) => void;
}

const EspaceDocumentaire: FC<EspaceDocumentaireProps & RouteComponentProps> = ({
  categories,
  documents,
  onRequestApplyReplacement,
  onRequestGetCategories,
  onRequestCreateNewFolder,
}) => {
  const classes = style();
  const [open, setOpen] = useState<boolean>(false);
  const [currentDocument, setCurrentDocument] = useState<DocumentCategorie | undefined>(undefined);

  const handleCloseCreateFolder = () => setOpen(false);

  const [openValidate, setOpenValidate] = useState<boolean>(false);
  const [type, setType] = useState<'ADD' | 'REPLACE'>('ADD');

  const handleValidate = (row: DocumentCategorie): void => {
    setCurrentDocument(row);
    if (row.documentARemplacer) setType('REPLACE');
    setOpenValidate(true);
  };

  const handleCreateFolder = (): void => {
    setOpen(true);
    onRequestGetCategories();
  };

  const handleApplyRemplacement = (
    statut: string,
    document: DocumentCategorie,
    commentaire: string,
  ) => {
    onRequestApplyReplacement(statut, document, commentaire);
    setOpenValidate(false);
  };

  const handleCloseValidate = () => setOpenValidate(false);

  const columns: Column[] = [
    {
      name: '',
      label: 'Auteur',
      renderer: (row: DocumentCategorie) => {
        return row.redacteur?.fullName || '-';
      },
    },
    {
      name: 'dateHeureChargement',
      label: 'Date de chargement',
      renderer: (row: DocumentCategorie) => {
        return row?.createdAt ? moment(new Date(row.createdAt)).format('L') : '-';
      },
    },
    {
      name: '',
      label: 'Description',
      renderer: (row: DocumentCategorie) => {
        return row.description ? <div dangerouslySetInnerHTML={{__html: row.description}} /> : '-';
      },
    },

    {
      name: 'categorie',
      label: 'Espace documentaire',
      renderer: (row: DocumentCategorie) => {
        return row?.categorie?.libelle ? row.categorie.libelle : '-';
      },
    },
    {
      name: 'sousCategorie',
      label: 'Sous-espace documentaire',
      renderer: (row: DocumentCategorie) => {
        return row?.sousCategorie?.libelle ? row.sousCategorie.libelle : '-';
      },
    },
    {
      name: 'typeDemande',
      label: 'Type de demande',
      renderer: (row: DocumentCategorie) => {
        return row.idDocumentARemplacer ? 'Remplacement' : 'Ajout';
      },
    },

    {
      name: '',
      label: 'Commentaire',
      renderer: (row: DocumentCategorie) => {
        return row?.dernierChangementStatut?.commentaire || '-';
      },
    },
    {
      name: 'statut',
      label: 'Statut',
      renderer: (row: DocumentCategorie) => {
        return row?.dernierChangementStatut?.status
          ? row.dernierChangementStatut.status === 'APPROUVE'
            ? 'Approuvé'
            : 'Refusé'
          : "En attente d'approbation";
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: DocumentCategorie) => {
        return (
          <IconButton
            disabled={!!row.dernierChangementStatut}
            size="small"
            onClick={() => handleValidate(row)}
          >
            <Create fontSize="small" />
          </IconButton>
        );
      },
    },
  ];

  return (
    <>
      <Box className={classes.container}>
        <AppBar position="sticky" className={classes.appBar} elevation={0}>
          <Toolbar>
            <Typography variant="h2">Liste des documents à valider</Typography>
            <Box display="flex" alignItems="center" ml="auto">
              {/*<Button
                startIcon={<Add />}
                variant="outlined"
                style={{ marginRight: 16 }}
                onClick={handleCreateFolder}
              >
                Créer nouveau dossier
              </Button>
              <Button startIcon={<Add htmlColor="#FFF" />} variant="contained" color="secondary">
                Ajouter un document
              </Button>*/}
            </Box>
          </Toolbar>
        </AppBar>
        {documents.loading ? (
          <Loader />
        ) : !documents.error && documents.data ? (
          <Box className={classes.content}>
            {/*<Box className={classes.search}>
              <TextField
                variant="outlined"
                InputProps={{
                  endAdornment: <Search />,
                }}
                placeholder="Rechercher un document"
                fullWidth
              />
            </Box>

            <Box className={classes.filter}>
              <Typography variant="subtitle2">
                {`${'0'}/${documents.data.length} séléctionné(s) (${documents.data.length})`}
              </Typography>
              <Button startIcon={<Tune />}>Filtres</Button>
              <Button startIcon={<Refresh />}>Réactualiser</Button>
            </Box>
        */}
            <Box className={classes.table}>
              <Tableau search={false} columns={columns} data={documents.data as any} />
            </Box>
          </Box>
        ) : (
          <ErrorPage />
        )}
      </Box>

      <ModalDocument
        label="Nouveau dossier"
        open={open}
        onClose={handleCloseCreateFolder}
        handleClose={handleCloseCreateFolder}
        maxWidth="sm"
        fullWidth
      >
        <CreateFolder onRequestCreateNewFolder={onRequestCreateNewFolder} categories={categories} />
      </ModalDocument>

      <ModalDocument
        label="Documents à valider"
        open={openValidate}
        onClose={handleCloseValidate}
        handleClose={handleCloseValidate}
        fullScreen
      >
        <ValidateDocument
          document={currentDocument as any}
          type={type}
          onRequestApplyReplacement={handleApplyRemplacement}
        />
      </ModalDocument>
    </>
  );
};

export default withRouter(EspaceDocumentaire);
