import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
  root: {
    height: 'calc(100% - 62px)',
    overflowY: 'hidden',
  },
  validate: {
    display: 'flex',
    height: '100%',
    overflowY: 'auto',
  },
  replace: {
    flexDirection: 'column',
    overflowY: 'auto',
    '&>div': {
      height: '50%',
      overflowY: 'hidden',
    },
    '& iframe': {
      flex: '0 1 60%',
    },
  },
  view: {
    flex: '0 0 60%',
    minWidth: '30%',
    margin: theme.spacing(2),
  },
  actionBtn: {
    position: 'absolute',
    top: theme.spacing(1.5),
    right: theme.spacing(2),
  },
  divider: {
    position: 'absolute',
    top: 5,
    left: '50%',
    padding: theme.spacing(0, 2),
    fontWeight: 600,
    transform: 'translate(-50%)',
    backgroundColor: theme.palette.common.white,
  },
}));
