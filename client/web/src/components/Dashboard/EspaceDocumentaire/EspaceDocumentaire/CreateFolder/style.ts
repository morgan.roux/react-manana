import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
  create: {
    width: '100%',
    minHeight: 400,
    '& [class*=MuiFormControl-root]': {
      marginBottom: theme.spacing(4),
    },
    '@media(min-width: 1366px)': {
      maxWidth: '80%',
      margin: 'auto',
    },
  },
  actions: {
    padding: theme.spacing(2),
    '& button': {
      margin: 'auto',
      minWidth: '30%',
    },
    boxShadow: '0 -2px 6px rgba(0,0,0,.16)',
  },
}));
