import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useCommonStyles = makeStyles((theme: Theme) =>
  createStyles({
    formRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      marginTop: 50,
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
    },
    inputTitle: {
      fontWeight: 'bold',
      fontSize: 18,
      color: theme.palette.secondary.main,
      marginBottom: 15,
    },
    formContainer: {
      width: 815,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'center',
      marginBottom: 25,
    },
    inputsContainer: {
      border: '1px solid #DCDCDC',
      borderRadius: 12,
      marginBottom: 25,
      width: '100%',
      '& > div:not(:nth-last-child(1))': {
        borderBottom: '1px solid #DCDCDC',
      },
    },
    formRow: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      flexDirection: 'row',
      padding: '15px 25px',
      '& > div': {
        maxWidth: 300,
        marginBottom: 0,
        '& input, & .MuiSelect-root': {
          fontWeight: '600',
          fontSize: 16,
          letterSpacing: 0.28,
        },
      },
    },
    formRowTitle: {
      fontSize: 14,
      fontWeight: 'normal',
    },
    inputLabel: {
      '& > span': {
        color: 'red',
      },
    },
  }),
);

export default useCommonStyles;
