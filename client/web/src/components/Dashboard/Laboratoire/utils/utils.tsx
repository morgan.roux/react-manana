import { QueryLazyOptions, useApolloClient, useQuery } from '@apollo/react-hooks';
import React, { useContext, useState } from 'react';
import { ContentContext, ContentStateInterface } from '../../../../AppContext';
import { LABORATOIRE_URL, PHARMACIE_URL } from '../../../../Constant/url';
import {
  PERSONNELVariables,
  PERSONNEL_personnel,
} from '../../../../graphql/Personnel/types/PERSONNEL';
import { GET_CHECKEDS_PHARMACIE } from '../../../../graphql/Pharmacie/local';
import { Column } from '../../../Common/newCustomContent/interfaces';
import TableActionColumn from '../../TableActionColumn';

/**
 *
 *  Create personnel hooks
 *
 */
export const useCreatePharmacie = (mutationVariables: any): [(variables: any) => any, boolean] => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const client = useApolloClient();

  const [mutationSuccess, setMutationSuccess] = useState<boolean>(false);

  // const [createUpdatePersonnel, { loading: mutationLoading }] = useMutation<
  //   CREATE_PERSONNEL,
  //   CREATE_PERSONNELVariables
  // >(DO_CREATE_PERSONNEL, {
  //   update: (cache, { data }) => {
  //     console.log(mutationVariables);
  //     if (
  //       data &&
  //       data.createUpdatePersonnel &&
  //       mutationVariables &&
  //       mutationVariables.variables &&
  //       !mutationVariables.variables.id
  //     ) {
  //       if (variables && operationName) {
  //         const req: any = cache.readQuery({
  //           query: operationName,
  //           variables: variables,
  //         });
  //         if (req && req.search && req.search.data) {
  //           cache.writeQuery({
  //             query: operationName,
  //             data: {
  //               search: {
  //                 ...req.search,
  //                 ...{
  //                   data: [...req.search.data, data.createUpdatePersonnel],
  //                 },
  //               },
  //             },

  //             variables: variables,
  //           });
  //         }
  //       }
  //     }
  //   },
  //   onCompleted: data => {
  //     if (data && data.createUpdatePersonnel) {
  //       setMutationSuccess(true);
  //     }
  //   },
  //   onError: errors => {
  //     errors.graphQLErrors.map(err => {
  //       displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
  //     });
  //     displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
  //   },
  // });

  const createUpdatePharmacie = () => {};

  return [createUpdatePharmacie, mutationSuccess];
};

/**
 *
 *  Delete personnel hooks
 *
 */

export const useDeletePharmacie = () => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);
  const checkeds = useCheckedPharmacie();

  const client = useApolloClient();

  // const [deletePharmacies, { loading, data: deleteData }] = useMutation<
  //   DELETE_PERSONNELS,
  //   DELETE_PERSONNELSVariables
  // >(DO_DELETE_PERSONNELS, {
  //   update: (cache, { data }) => {
  //     if (data && data.deletePersonnels) {
  //       if (variables && operationName) {
  //         const req: any = cache.readQuery({
  //           query: operationName,
  //           variables: variables,
  //         });
  //         if (req && req.search && req.search.data) {
  //           const source = req.search.data;
  //           const result = data.deletePersonnels;
  //           const dif = differenceBy(source, result, 'id');
  //           cache.writeQuery({
  //             query: operationName,
  //             data: {
  //               search: {
  //                 ...req.search,
  //                 ...{
  //                   data: dif,
  //                 },
  //               },
  //             },
  //             variables: variables,
  //           });
  //           client.writeData({
  //             data: { checkedsPersonnelGroupement: differenceBy(checkeds, result, 'id') },
  //           });
  //         }
  //       }
  //     }
  //   },
  //   onError: errors => {
  //     errors.graphQLErrors.map(err => {
  //       displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
  //     });
  //   },
  // });

  const deletePharmacies = () => {};
  return deletePharmacies;
};

/**
 *
 *  list checked personnel groupement hooks
 *
 */

export const useCheckedPharmacie = () => {
  const checkedsPharmacie = useQuery(GET_CHECKEDS_PHARMACIE);
  return checkedsPharmacie && checkedsPharmacie.data && checkedsPharmacie.data.checkedsPharmacie;
};

/**
 *
 *  subtoolbar button action hooks
 *
 */

export const useButtonHeadAction = (push: any) => {
  const goToAddPharmacie = () => push(`/db/${PHARMACIE_URL}/create`);
  const goBack = () => push(`/db/${PHARMACIE_URL}`);

  return [goToAddPharmacie, goBack];
};

/**
 *
 *  get personnel hooks
 *
 */

export const useGetPharmacie = (): [
  (options?: QueryLazyOptions<PERSONNELVariables> | undefined) => void,
  PERSONNEL_personnel | null | undefined,
] => {
  const client = useApolloClient();

  // const [getPersonnel, { data, loading: personnelLoading }] = useLazyQuery<
  //   PERSONNEL,
  //   PERSONNELVariables
  // >(GET_PERSONNEL, {
  //   onError: errors => {
  //     errors.graphQLErrors.map(err => {
  //       displaySnackBar(client, {
  //         type: 'ERROR',
  //         message: err.message,
  //         isOpen: true,
  //       });
  //     });
  //   },
  // });

  // const personnel = data && data.personnel;

  const getPharmacie = () => {};
  const pharmacie = null;
  return [getPharmacie, pharmacie];
};

export const usePharmacieColumns = (setOpenDeleteDialog: any): [Column[], any, () => void] => {
  const deletePharmacies = useDeletePharmacie();
  const [deleteRow, setDeleteRow] = useState<any>();
  const checkedsPharmacie = useCheckedPharmacie();
  const client = useApolloClient();

  const onClickDelete = (row: any) => {
    setDeleteRow(row);
  };

  const onClickConfirmDelete = () => {
    setOpenDeleteDialog(false);
    if (checkedsPharmacie.length > 0) {
      const ids: string[] = checkedsPharmacie.map(i => i.id);
      // deletePharmacies({ variables: { ids } });
    }

    if (deleteRow && deleteRow.id) {
      // deletePharmacies({ variables: { ids: [deleteRow.id] } });
    }
  };

  const columns = [
    {
      name: 'cip',
      label: 'CIP',
      renderer: (value: any) => {
        return value.cip ? value.cip : '-';
      },
    },
    {
      name: 'nom',
      label: 'Nom',
      renderer: (value: any) => {
        return value.nom ? value.nom : '-';
      },
    },
    {
      name: 'titulaire.fullName',
      label: 'Titulaire(s)',
      renderer: (value: any) => {
        // const handleClick = (titulaireId: string) => () => {
        //     if (historyGlobal && !viewInModal) {
        //         historyGlobal.push(`/db/titulaires-pharmacies/fiche/${titulaireId}`);
        //     }
        //     if (viewInModal) handleOpenDetail(titulaireId, 'TITULAIRE');
        // };
        // return (
        //     value.titulaires &&
        //     value.titulaires.map((titulaire: any, key: number) =>
        //         titulaire ? (
        //             <>
        //                 <Link
        //                     component="button"
        //                     variant="body2"
        //                     onClick={handleClick(titulaire.id)}
        //                     style={{ color: theme.palette.secondary.main, textAlign: 'left' }}
        //                 >
        //                     {titulaire && `${titulaire.fullName}`}
        //                 </Link>
        //                 <br />
        //             </>
        //         ) : (
        //                 '-'
        //             ),
        //     )
        // );
      },
    },
    {
      name: 'adresse1',
      label: 'Adresse',
      renderer: (value: any) => {
        return value.adresse1 ? value.adresse1 : '-';
      },
    },
    {
      name: 'cp',
      label: 'Code postal',
      renderer: (value: any) => {
        return value.cp ? value.cp : '-';
      },
    },
    {
      name: 'departement.nom',
      label: 'Département',
      renderer: (value: any) => {
        return value.departement && value.departement.nom ? value.departement.nom : '-';
      },
    },
    {
      name: 'ville',
      label: 'Ville',
      renderer: (value: any) => {
        return value.ville ? value.ville : '-';
      },
    },
    {
      name: 'tele1',
      label: 'Téléphone',
      renderer: (value: any) => {
        return value.tele1 ? value.tele1 : '-';
      },
    },
    {
      name: 'numIvrylab',
      label: 'Code plateforme',
      renderer: (value: any) => {
        return value.numIvrylab ? value.numIvrylab : '-';
      },
    },
    {
      name: 'contrat',
      label: 'Contrat',
      renderer: (value: any) => {
        return value.contrat ? value.contrat : '-';
      },
    },
    {
      name: 'sortie',
      label: 'Sortie',
      renderer: (value: any) => {
        return value.sortie ? (value.sortie === 1 ? 'Oui' : 'Non') : '-';
      },
    },
    {
      name: 'actived',
      label: 'Statut',
      renderer: (value: any) => {
        return value && value.actived !== undefined && value.actived !== null
          ? value.actived
            ? 'Activée'
            : 'Inactive'
          : '-';
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <TableActionColumn
            row={value}
            baseUrl={LABORATOIRE_URL}
            setOpenDeleteDialog={setOpenDeleteDialog}
            handleClickDelete={onClickDelete}
          />
        );
      },
    },
  ];

  return [columns, deleteRow, onClickConfirmDelete];
};
