import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import {
  AppBar,
  Box,
  CssBaseline,
  Hidden,
  Paper,
  Tab,
  Tabs,
  TextField,
  Typography,
} from '@material-ui/core';
import { InsertComment, LibraryBooks, ShowChart } from '@material-ui/icons';
import React, { ChangeEvent, createRef, FC, Fragment, useEffect, useRef, useState } from 'react';
import ReactQuill from 'react-quill';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { CommentListNew } from '../../../../../Comment/CommentList';
import { Comment } from '../../../../../Comment';
import Backdrop from '../../../../../Common/Backdrop';
import { CustomDatePicker } from '../../../../../Common/CustomDateTimePicker';
import { CustomModal } from '../../../../../Common/CustomModal';
import CustomSelect from '../../../../../Common/CustomSelect';
import { isMobile } from '../../../../../../utils/Helpers';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import useStyles from './styles';
import { StatutPartenaire, TypePartenaire } from '../../../../../../types/graphql-global-types';
import { DO_CREATE_UPDATE_LABORATOIRE_PARTENAIRE } from '../../../../../../graphql/LaboratoirePartenaire';
import {
  CREATE_UPDATE_LABORATOIRE_PARTENAIRE,
  CREATE_UPDATE_LABORATOIRE_PARTENAIREVariables,
} from '../../../../../../graphql/LaboratoirePartenaire/types/CREATE_UPDATE_LABORATOIRE_PARTENAIRE';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import moment from 'moment';

interface PartenariatlProps {
  idLaboratoire: string | null;
  open: boolean;
  setOpen: (value: boolean) => void;
  partenaire: any;
  refetchLabo?: any;
}

interface LaboratoirePartenaireForm {
  id: string;
  idLaboratoire: string;
  debutPartenaire: any;
  finPartenaire: any;
  typePartenaire: any;
  statutPartenaire: any;
}

const DefinePartenariat: FC<PartenariatlProps & RouteComponentProps> = props => {
  const classes = useStyles({});
  const client = useApolloClient();

  const initialState: LaboratoirePartenaireForm = {
    id: '',
    idLaboratoire: '',
    debutPartenaire: null,
    finPartenaire: null,
    typePartenaire: '',
    statutPartenaire: '',
  };
  const [values, setValues] = useState<LaboratoirePartenaireForm>(initialState);

  // data to fetch from back
  const { idLaboratoire, partenaire, open, setOpen, refetchLabo } = props;

  const typePartenaires = [
    { value: TypePartenaire.NEGOCIATION_EN_COURS, label: 'NEGOCIATION_EN_COURS' },
    { value: TypePartenaire.NON_PARTENAIRE, label: 'NON_PARTENAIRE' },
    { value: TypePartenaire.NOUVEAU_REFERENCEMENT, label: 'NOUVEAU_REFERENCEMENT' },
    { value: TypePartenaire.PARTENARIAT_ACTIF, label: 'PARTENARIAT_ACTIF' },
  ];

  const statutPartenaires = [
    { value: StatutPartenaire.ACTIVER, label: 'ACTIVER' },
    { value: StatutPartenaire.BLOQUER, label: 'BLOQUER' },
  ];

  const FORM_INPUTS = [
    {
      title: 'Informations',
      inputs: [
        {
          label: 'Date début de partenariat',
          type: 'date',
          name: 'debutPartenaire',
          value: values.debutPartenaire,
          inputType: undefined,
          required: true,
        },
        {
          label: 'Date fin de partenariat',
          type: 'date',
          name: 'finPartenaire',
          value: values.finPartenaire,
          inputType: undefined,
          required: true,
        },
        {
          label: 'Type',
          placeholder: 'Type de partenariat',
          type: 'select',
          listId: 'value',
          index: 'label',
          name: 'typePartenaire',
          value: values.typePartenaire,
          inputType: undefined,
          selectOptions: typePartenaires,
        },
        {
          label: 'Statut',
          placeholder: 'Statut de partenariat',
          type: 'select',
          listId: 'value',
          index: 'label',
          name: 'statutPartenaire',
          value: values.statutPartenaire,
          inputType: undefined,
          selectOptions: statutPartenaires,
        },
      ],
    },
  ];

  React.useEffect(() => {
    if (partenaire)
      setValues(prevState => ({
        ...prevState,
        id: partenaire.id,
        debutPartenaire: partenaire.debutPartenaire,
        finPartenaire: partenaire.finPartenaire,
        typePartenaire: partenaire.typePartenaire,
        statutPartenaire: partenaire.statutPartenaire,
      }));
  }, [partenaire]);

  React.useEffect(() => {
    if (idLaboratoire) setValues(prevState => ({ ...prevState, idLaboratoire }));
  }, [idLaboratoire]);

  const handleChangeInput = (e: ChangeEvent<any>) => {
    const { value, name } = e.target;
    setValues(prevState => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleChangeDate = (name: string) => (date: any) => {
    setValues(prevState => ({ ...prevState, [name]: date }));
  };

  const [createUpdateLaboratoirePartenaire, { loading: ceateLoading }] = useMutation<
    CREATE_UPDATE_LABORATOIRE_PARTENAIRE,
    CREATE_UPDATE_LABORATOIRE_PARTENAIREVariables
  >(DO_CREATE_UPDATE_LABORATOIRE_PARTENAIRE, {
    onCompleted: data => {
      if (data && data.createUpdateLaboratoirePartenaire) {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: values.id
            ? 'Modification effectuée avec succès'
            : 'Création effectuée avec succès',
        });
        setValues(initialState);
        setOpen(false);
        console.log('refresh', refetchLabo);

        if (refetchLabo) refetchLabo();
      }
    },
    onError: error => {
      console.log('error :>> ', error);
      displaySnackBar(client, {
        isOpen: true,
        type: 'ERROR',
        message: error.message,
      });
    },
  });

  const onClickConfirm = () => {
    createUpdateLaboratoirePartenaire({
      variables: {
        input: {
          ...values,
          debutPartenaire: moment(values.debutPartenaire) as any,
          finPartenaire: moment(values.finPartenaire) as any,
        },
      },
    });
  };

  console.log('values', values);

  return (
    <CustomModal
      onClick={event => event.stopPropagation()}
      open={open}
      setOpen={setOpen}
      title={'Definir partenariat'}
      withBtnsActions={true}
      withCancelButton={isMobile() ? false : true}
      closeIcon={true}
      headerWithBgColor={true}
      fullWidth={true}
      actionButton={'Enregistrer'}
      onClickConfirm={onClickConfirm}
    >
      {ceateLoading && <Backdrop />}
      <div className={classes.pharmacieFormRoot}>
        <div className={classes.formContainer}>
          {FORM_INPUTS.map((item: any, index: number) => {
            return (
              <Fragment key={`pharmacie_form_item_${index}`}>
                <Typography className={classes.inputTitle}>{item.title}</Typography>
                <div className={classes.inputsContainer}>
                  {item.inputs.map((input: any, inputIndex: number) => {
                    return (
                      <div className={classes.formRow} key={`pharmacie_form_input_${inputIndex}`}>
                        <Typography className={classes.inputLabel}>
                          {input.label} {input.required && <span>*</span>}
                        </Typography>
                        {input.type === 'select' ? (
                          <CustomSelect
                            label=""
                            list={input.selectOptions || []}
                            listId={input.listId || 'id'}
                            index={input.index || 'value'}
                            name={input.name}
                            value={input.value}
                            onChange={handleChangeInput}
                            shrink={false}
                            placeholder={input.placeholder}
                            withPlaceholder={true}
                          />
                        ) : input.type === 'date' ? (
                          <CustomDatePicker
                            placeholder={input.placeholder}
                            onChange={handleChangeDate(input.name)}
                            name={input.name}
                            value={input.value}
                            disabled={
                              input.name === 'finPartenaire' && !values.debutPartenaire
                                ? true
                                : false
                            }
                            minDate={
                              input.name === 'finPartenaire' ? values.debutPartenaire : '1999-1-1'
                            }
                            disablePast={false}
                          />
                        ) : (
                          <CustomFormTextField
                            name={input.name}
                            value={input.value}
                            onChange={handleChangeInput}
                            placeholder={input.placeholder}
                            type={input.inputType}
                          />
                        )}
                      </div>
                    );
                  })}
                </div>
              </Fragment>
            );
          })}
        </div>
      </div>
    </CustomModal>
  );
};

export default withRouter(DefinePartenariat);
