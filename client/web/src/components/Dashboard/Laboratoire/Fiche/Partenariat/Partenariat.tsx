import {
  List,
  ListItem,
  Divider,
  ListItemIcon,
  ListItemText,
  Box,
  Typography,
} from '@material-ui/core';
import moment from 'moment';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { StatutPartenaire, TypePartenaire } from '../../../../../types/graphql-global-types';
import CustomButton from '../../../../Common/CustomButton';
import DefinePartenariat from './Modal/DefinePartenariat';
import useStyles from './styles';

interface PartenariatProps {
  idLaboratoire: string | null;
  partenaire: any;
  refetchLabo?: any;
  open: boolean;
  setOpen: (value: boolean) => void;
}

const Partenariat: FC<PartenariatProps & RouteComponentProps> = ({
  partenaire,
  idLaboratoire,
  refetchLabo,
  open,
  setOpen,
}) => {
  const classes = useStyles({});

  const titreTableau: string[] = ['Date debut', 'Date Fin', 'Type de partenariat', 'statut'];

  const typePartenaire =
    partenaire && partenaire.typePartenaire === TypePartenaire.NEGOCIATION_EN_COURS
      ? 'Négociation en cours'
      : partenaire && partenaire.typePartenaire === TypePartenaire.NON_PARTENAIRE
      ? 'Non partenaire'
      : partenaire && partenaire.typePartenaire === TypePartenaire.NOUVEAU_REFERENCEMENT
      ? 'Nouveau Référencement'
      : 'Partenariat actif';

  const statutPartenariat =
    partenaire && partenaire.statutPartenaire === StatutPartenaire.ACTIVER ? 'Activé' : 'Bloqué';

  const handleClick = () => {
    setOpen(true);
  };

  return (
    <>
      <Box className={classes.tableContainer}>
        <Box width="100%" display="flex" flexDirection="row" justifyContent="space-between">
          <Typography className={classes.title}>Information partenariat</Typography>

          <CustomButton color="secondary" onClick={handleClick}>
            {partenaire && partenaire.id ? 'Modifier le' : 'Définir'} partenariat
          </CustomButton>
        </Box>
        {partenaire && (
          <List className={classes.list}>
            <Box>
              <ListItem className={classes.listItem}>
                <ListItemIcon>Date début</ListItemIcon>
                <ListItemText
                  primary={
                    (partenaire &&
                      partenaire.debutPartenaire &&
                      moment(partenaire.debutPartenaire).format('L')) ||
                    '-'
                  }
                />
              </ListItem>
              <Divider />
            </Box>
            <Box>
              <ListItem className={classes.listItem}>
                <ListItemIcon>Date Fin</ListItemIcon>
                <ListItemText
                  primary={
                    (partenaire &&
                      partenaire.finPartenaire &&
                      moment(partenaire.finPartenaire).format('L')) ||
                    '-'
                  }
                />
              </ListItem>
              <Divider />
            </Box>
            <Box>
              <ListItem className={classes.listItem}>
                <ListItemIcon>Type de partenariat</ListItemIcon>
                <ListItemText primary={typePartenaire || '-'} />
              </ListItem>
              <Divider />
            </Box>
            <Box>
              <ListItem className={classes.listItem}>
                <ListItemIcon>statut</ListItemIcon>
                <ListItemText primary={statutPartenariat || '-'} />
              </ListItem>
              <Divider />
            </Box>
          </List>
        )}
      </Box>
      <DefinePartenariat
        idLaboratoire={idLaboratoire}
        partenaire={partenaire}
        open={open}
        setOpen={setOpen}
        refetchLabo={refetchLabo}
      />
    </>
  );
};
export default withRouter(Partenariat);
