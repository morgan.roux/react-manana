import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { Paper, Tab, Tabs, Box } from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons';
import React, { FC,useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

import { LABORATOIRE_URL} from '../../../../Constant/url';
import { GET_LABORATOIRE } from '../../../../graphql/Laboratoire/query';
import {
  LABORATOIRE,
  LABORATOIREVariables,
} from '../../../../graphql/Laboratoire/types/LABORATOIRE';

import { displaySnackBar } from '../../../../utils/snackBarUtils';

import SubToolbar from '../../../Common/newCustomContent/SubToolbar';
import Comments from './Comment';
import Contact from './Contact';
import Partenariat from './Partenariat';
import ProduitCanals from './Produit';
import Reclamations from './Reclamation';
import Ressource from './Ressource';

import useStyles from './styles';

interface FichePrestataireProps {
  match: { params: { laboratoireId: string | undefined } };
}

const Fiche: FC<FichePrestataireProps & RouteComponentProps> = ({
  location: { pathname },
  match: {
    params: { laboratoireId },
  },
  history: { push },
}) => {
  /* const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext); */

  const classes = useStyles({});
  const client = useApolloClient();
  const [tab, setTab] = useState<string>('PARTENARIAT');
  const [open, setOpen] = useState<boolean>(false);
  const [openFormContact, setOpenFormContact] = useState<boolean>(false);
  const [openDetailContact, setOpenDetailContact] = useState<boolean>(false);

  const { data: laboData, loading, refetch } = useQuery<LABORATOIRE, LABORATOIREVariables>(
    GET_LABORATOIRE,
    {
      variables: { id: laboratoireId || '' },
      fetchPolicy: 'cache-and-network',
      onError: error => {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );

  const laboratoire = (laboData && laboData.laboratoire) || null;
  const partenaire =
    (laboData && laboData.laboratoire && laboData.laboratoire.laboratoirePartenaire) || null;

  console.log('logdata', laboData);

  let currentTab = 0;
  switch (tab) {
    case 'PARTENARIAT':
      currentTab = 0;
      break;
    case 'CONTACT':
      currentTab = 1;
      break;
    case 'RESSOURCE':
      currentTab = 2;
      break;
    case 'PRODUIT':
      currentTab = 3;
      break;
    case 'RECLAMATION':
      currentTab = 4;
      break;
    case 'COMMENT':
      currentTab = 5;
      break;
    default:
      currentTab = 0;
      break;
  }

  const handleTabClick = (event: React.ChangeEvent<{}>, newValue: number) => {
    switch (newValue) {
      case 0:
        setTab('PARTENARIAT');
        break;
      case 1:
        setTab('CONTACT');
        break;
      case 2:
        setTab('RESSOURCE');
        break;
      case 3:
        setTab('PRODUIT');
        break;
      case 4:
        setTab('RECLAMATION');
        break;
      case 5:
        setTab('COMMENT');
        break;
      default:
        setTab('PARTENARIAT');
        break;
    }
  };

  return (
    <Box className={classes.root}>
      <SubToolbar
        title={`Fiche du laboratoire ${laboratoire && laboratoire.nomLabo}`}
        dark={true}
        withBackBtn={true}
        backBtnIcon={<ArrowBack />}
        onClickBack={() => {
          push(`/db/${LABORATOIRE_URL}`);
        }}
      >
        {/* <CustomButton color="secondary">{'Modifier'}</CustomButton> */}
      </SubToolbar>

      <Box className={classes.globalTabsContainer}>
        <Paper elevation={3}>
          <Tabs
            value={currentTab}
            indicatorColor="secondary"
            textColor="primary"
            onChange={handleTabClick}
          >
            <Tab label="Partenariat" />
            <Tab label="Contacts" />
            <Tab label="Ressources" />
            <Tab label="Produits" />
            <Tab label="Reclamations" />
            <Tab label="Commentaires" />
          </Tabs>
        </Paper>
      </Box>
      <Box className={classes.contentContainer}>
        <Box>
          {currentTab === 1 ? (
            <Contact
              laboratoire={laboratoire}
              open={openFormContact}
              setOpen={setOpenFormContact}
              openDetail={openDetailContact}
              setOpenDetail={setOpenDetailContact}
            />
          ) : currentTab === 2 ? (
            <Ressource idLaboratoire={laboratoire && laboratoire.id} />
          ) : currentTab === 3 ? (
            <ProduitCanals idLaboratoire={laboratoire && laboratoire.id} />
          ) : currentTab === 4 ? (
            <Reclamations idLaboratoire={laboratoire && laboratoire.id} />
          ) : currentTab === 5 ? (
            <Comments idLaboratoire={laboratoire && laboratoire.id} />
          ) : (
            <Partenariat
              idLaboratoire={laboratoire && laboratoire.id}
              partenaire={partenaire}
              refetchLabo={refetch}
              open={open}
              setOpen={setOpen}
            />
          )}
        </Box>
      </Box>
    </Box>
  );
};
export default withRouter(Fiche);
