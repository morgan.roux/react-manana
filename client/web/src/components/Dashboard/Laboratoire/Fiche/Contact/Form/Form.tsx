import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import { Typography } from '@material-ui/core';
import { truncate } from 'fs';
import { head, last, split } from 'lodash';
import React, { ChangeEvent, FC, Fragment, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { ContentContext, ContentStateInterface } from '../../../../../../AppContext';
import mutationSuccessImg from '../../../../../../assets/img/mutation-success.png';
import { PARTENAIRE_LABORATOIRE_URL } from '../../../../../../Constant/url';
import { DO_GET_LABO_PARTENAIRE } from '../../../../../../graphql/LaboratoirePartenaire/query';
import {
  GET_LABO_PARTENAIRE,
  GET_LABO_PARTENAIREVariables,
} from '../../../../../../graphql/LaboratoirePartenaire/types/GET_LABO_PARTENAIRE';
import {
  DO_CREATE_UPDATE_LABO_REPRESENTANT,
  DO_GET_LABORATOIRE_REPRESENTANT,
} from '../../../../../../graphql/LaboratoireRepresentant';
import {
  CREATE_UPDATE_LABO_REPRESENTANT,
  CREATE_UPDATE_LABO_REPRESENTANTVariables,
} from '../../../../../../graphql/LaboratoireRepresentant/types/CREATE_UPDATE_LABO_REPRESENTANT';
/* import {
  DO_CREATE_UPDATE_LABO_PARTENAIRE_REPRESENTANT,
  DO_GET_LABORATOIRE_PARTENAIRE_REPRESENTANT,
} from '../../../../../../graphql/LaboratoirePartenaireRepresentant';
import {
  CREATE_UPDATE_LABO_PARTENAIRE_REPRESENTANT,
  CREATE_UPDATE_LABO_PARTENAIRE_REPRESENTANTVariables,
} from '../../../../../../graphql/LaboratoirePartenaireRepresentant/types/CREATE_UPDATE_LABO_PARTENAIRE_REPRESENTANT';
import {
  GET_LABORATOIRE_PARTENAIRE_REPRESENTANT,
  GET_LABORATOIRE_PARTENAIRE_REPRESENTANTVariables,
} from '../../../../../../graphql/LaboratoirePartenaireRepresentant/types/GET_LABORATOIRE_PARTENAIRE_REPRESENTANT'; */
import { GET_PARTENAIRE_REPRESENTANT } from '../../../../../../graphql/PartenaireRepresentant/query';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import SnackVariableInterface from '../../../../../../Interface/SnackVariableInterface';
import { getGroupement } from '../../../../../../services/LocalStorage';
import { uploadToS3 } from '../../../../../../services/S3';
import { FichierInput, TypeFichier } from '../../../../../../types/graphql-global-types';
import { formatFilenameWithoutDate } from '../../../../../../utils/filenameFormater';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import { isEmailValid } from '../../../../../../utils/Validator';
import Backdrop from '../../../../../Common/Backdrop';
import CustomButton from '../../../../../Common/CustomButton';
import { CustomModal } from '../../../../../Common/CustomModal';
import NoItemContentImage from '../../../../../Common/NoItemContentImage';
import Stepper from '../../../../../Common/Stepper';
import { Step } from '../../../../../Common/Stepper/Stepper';
import { FormStepOne, FormStepTwo } from './Steps';
import useStyles from './styles';
interface FormProps {
  open: boolean;
  setOpen: (value: any) => void;
  idLaboratoire: string | null;
  representant: any;
  setRepresentant: (value: any) => void;
}

const Form: FC<FormProps & RouteComponentProps> = ({
  open,
  setOpen,
  representant,
  idLaboratoire,
  setRepresentant,
}) => {
  const classes = useStyles({});
  const initialState = {
    id: '',
    photo: null,
    civilite: '',
    nom: '',
    prenom: '',
    sexe: '',
    fonction: '',
    idLaboratoire: idLaboratoire || '',
    afficherComme: '',
    contact: {},
  };

  const groupement = getGroupement();
  const [loading, setLoading] = useState<boolean>(false);
  const [nextBtnDisabled, setNextBtnDisabled] = useState<boolean>(false);
  const [mutationSucess, setMutationSuccess] = useState<boolean>(false);
  const [values, setValues] = useState<any>(initialState);
  const [photo, setPhoto] = useState<any>(null);
  const client = useApolloClient();

  const isOnCreate = representant ? false : true;

  console.log('contact', representant);
  React.useEffect(() => {
    if (representant) {
      const contact = representant.contact;
      const { ['__typename']: typename, ...contactRest } = contact;
      if (representant && representant.photo) {
        setPhoto(representant.photo);
      }
      setValues({
        id: representant.id || '',
        civilite: representant.civilite || '',
        nom: representant.nom,
        prenom: representant.prenom || '',
        sexe: representant.sexe || '',
        fonction: representant.fonction || '',
        idLaboratoire: (representant.laboratoire && representant.laboratoire.id) || '',
        afficherComme: representant.afficherComme || '',
        contact: contactRest,
      });
    }
  }, [representant]);

  const otherHandleChange = (name: string, value: any, checked: boolean) => {
    const nameArray = split(name, '.');
    const nameKey: any = head(nameArray);
    const exactName: any = last(nameArray);
    if (name.includes('checkbox')) {
      if (nameKey === exactName && exactName !== 'cap') {
        setValues(prevState => ({
          ...prevState,
          [exactName]: checked,
        }));
      } else {
        setValues(prevState => ({
          ...prevState,
          [nameKey]: { ...prevState[nameKey], [exactName]: checked },
        }));
      }
    } else {
      setValues(prevState => ({
        ...prevState,
        [nameKey]: { ...prevState[nameKey], [exactName]: value },
      }));
    }
  };

  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const [createUpdateRepresentant, { loading: loadingCreateUpdate }] = useMutation<
    CREATE_UPDATE_LABO_REPRESENTANT,
    CREATE_UPDATE_LABO_REPRESENTANTVariables
  >(DO_CREATE_UPDATE_LABO_REPRESENTANT, {
    update: (cache, { data }) => {
      if (data && data.createUpdateLaboratoireRepresentant && values) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables: variables,
          });
          if (req && req.search && req.search.data && isOnCreate) {
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    total: req.search.total + 1,
                    data: [...req.search.data, data.createUpdateLaboratoireRepresentant],
                  },
                },
              },
              variables: variables,
            });
          }
        }
      }
    },
    onCompleted: data => {
      if (data && data.createUpdateLaboratoireRepresentant) {
        setLoading(false);
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Contact ${isOnCreate ? 'crée' : 'modifiée'} avec succès`,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
        setMutationSuccess(true);
        setRepresentant(null);
        setOpen(false);
      }
    },
    onError: errors => {
      setLoading(false);
      let errorMessage: string = 'Erreur du serveur';
      errors.graphQLErrors.map(error => {
        if (error.extensions) {
          switch (error.extensions.code) {
            case 'MAIL_NOT_SENT':
              errorMessage = "L'email n'est pas envoyé à l'utilisatuer";
              break;
            case 'EMAIL_ALREADY_EXIST':
              errorMessage = 'Cet email est déjà utilisé par un autre utilisateur';
              break;
          }
        }
      });
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errorMessage,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const submit = () => {
    setLoading(true);
    if (photo && photo.size) {
      // Presigned file
      const filePathsTab: string[] = [];
      filePathsTab.push(
        `users/photos/${groupement && groupement.id}/${formatFilenameWithoutDate(photo)}`,
      );
      doCreatePutPresignedUrl({ variables: { filePaths: filePathsTab } });
      return;
    }

    let userPhoto: FichierInput | null = null;
    if (photo && photo.id) {
      userPhoto = {
        chemin: photo.chemin,
        nomOriginal: photo.nomOriginal,
        type: TypeFichier.AVATAR,
        idAvatar: photo && photo.idAvatar,
      };
    }
    createUpdateRepresentant({
      variables: {
        input: {
          ...values,
          contact: { ...values.contact },
          photo: userPhoto,
        },
      },
    });
  };

  const finalStepBtn = {
    buttonLabel: isOnCreate ? 'Ajouter' : 'Modifier',
    action: submit,
  };

  const [doCreatePutPresignedUrl, { loading: presignedLoading }] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (
        data &&
        data.createPutPresignedUrls &&
        data.createPutPresignedUrls.length > 0 &&
        photo &&
        photo.size
      ) {
        const presignedPhoto = data.createPutPresignedUrls[0];
        if (presignedPhoto && presignedPhoto.filePath && presignedPhoto.presignedUrl) {
          setLoading(true);
          await uploadToS3(photo, presignedPhoto.presignedUrl)
            .then(result => {
              if (result && result.status === 200) {
                createUpdateRepresentant({
                  variables: {
                    input: {
                      ...values,
                      contact: { ...values.contact },
                      photo: {
                        chemin: presignedPhoto.filePath,
                        nomOriginal: photo.name,
                        type: TypeFichier.PHOTO,
                      },
                    },
                  },
                });
              }
            })
            .catch(error => {
              console.error(error);
              setLoading(false);
              displaySnackBar(client, {
                type: 'ERROR',
                message:
                  "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
                isOpen: true,
              });
            });
        }
      }
    },
    onError: errors => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      });
    },
  });

  const handleChange = (e: ChangeEvent<any>) => {
    const { name, value, checked, type } = e.target;
    if (name.includes('.')) {
      otherHandleChange(name, value, checked);
      return;
    }
    if (e && e.target) {
      const { name, value } = e.target;
      setValues(prevState => ({ ...prevState, [name]: value }));
    }
  };

  const title = `${isOnCreate ? 'Ajout' : 'Modification'} de contact`;

  useEffect(() => {
    const { civilite, nom, sexe, contact } = values;

    if (nom === '' || sexe === '' || civilite === '') {
      setNextBtnDisabled(true);
    } else if (
      contact &&
      contact.mailProf &&
      contact.mailProf.length > 0 &&
      !isEmailValid(contact.mailProf)
    ) {
      displaySnackBar(client, {
        isOpen: true,
        type: 'ERROR',
        message: 'Adresse de messagerie invalide',
      });
      setNextBtnDisabled(true);
    } else {
      setNextBtnDisabled(false);
    }
  }, [values]);

  const steps: Step[] = [
    {
      title: 'Information',
      content: (
        <FormStepOne
          photo={photo}
          setPhoto={setPhoto}
          values={values}
          handleChange={handleChange}
        />
      ),
    },
    {
      title: 'Contact',
      content: <FormStepTwo values={values} handleChange={handleChange} />,
    },
  ];

  return (
    <CustomModal
      onClick={event => event.stopPropagation()}
      open={open}
      setOpen={setOpen}
      withBtnsActions={false}
      closeIcon={true}
      headerWithBgColor={true}
      fullWidth={true}
      actionButton={'Enregistrer'}
    >
      <Fragment>
        {loading && <Backdrop />}
        <Stepper
          title={title}
          steps={steps}
          disableNextBtn={nextBtnDisabled}
          backToHome={() => setOpen(false)}
          finalStep={finalStepBtn}
          finished={false}
        />
      </Fragment>
    </CustomModal>
  );
};
export default withRouter(Form);
