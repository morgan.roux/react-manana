import { useApolloClient } from '@apollo/react-hooks';
import { Box } from '@material-ui/core';
import { Delete, Edit, Visibility } from '@material-ui/icons';
import React, { FC, Fragment, useContext, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { ContentContext, ContentStateInterface } from '../../../../../../AppContext';
import { PARTENAIRE_LABORATOIRE_URL } from '../../../../../../Constant/url';
import { DO_SEARCH_LABO_REPRESENTANT } from '../../../../../../graphql/LaboratoireRepresentant';
/* import { DO_SEARCH_LABO_PARTENAIRE_REPRESENTANT } from '../../../../../../graphql/LaboratoireRepresentant'; */
import {} from '../../../../../../graphql/PartenaireRepresentant/mutation';
import { GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT } from '../../../../../../graphql/PartenaireRepresentant/query';
import { ConfirmDeleteDialog } from '../../../../../Common/ConfirmDialog';
import CustomAvatar from '../../../../../Common/CustomAvatar';
import CustomButton from '../../../../../Common/CustomButton';
import CustomContent from '../../../../../Common/newCustomContent';
import SearchInput from '../../../../../Common/newCustomContent/SearchInput';
import WithSearch from '../../../../../Common/newWithSearch/withSearch';
import { Column } from '../../../../Content/Interface';
import TableActionColumn, {
  AttributionDepartementIcon,
  TableActionColumnMenu,
} from '../../../../TableActionColumn';
import { useDeletePartenaireRepresentant } from './deleteContact';
import useStyles from './styles';

interface ContactListProps {
  laboratoire: any;
  setContact: (value: any) => void;
  setOpen: (value: boolean) => void;
  setOpenDetail: (value: boolean) => void;
}

const ContactList: FC<ContactListProps & RouteComponentProps> = ({
  location: { pathname },
  history: { push },
  laboratoire,
  setOpen,
  setContact,
  setOpenDetail,
}) => {
  const classes = useStyles({});

  const [openAffectationDialog, setOpenAffectationDialog] = useState<boolean>(false);

  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [representantToAffect, setRepresentantToAffect] = useState<any>({});
  const [deleteRow, setDeleteRow] = useState<any>(null);
  const [selected, setSelected] = useState<any[]>([]);

  const deleteRepresentant = useDeletePartenaireRepresentant(setSelected);

  const onClickDelete = (_event: any, row: any) => {
    setOpenDeleteDialog(true);
    setDeleteRow(row);
  };

  const onClickEdit = (_event: any, row: any) => {
    setOpen(true);
    setContact(row);
  };

  const onClickDetail = (_event: any, row: any) => {
    setOpenDetail(true);
    setContact(row);
  };

  const onClickConfirmDelete = () => {
    if (deleteRow) {
      const { id } = deleteRow;

      deleteRepresentant({
        variables: { ids: [id] },
      });
      setDeleteRow(null);
    } else if (selected && selected.length > 0) {
      deleteRepresentant({
        variables: { ids: selected.map(item => item.id) },
      });
    }
    setOpenDeleteDialog(false);
  };

  const onClickAffectation = (event: any, action: any) => {
    event.preventDefault();
    event.stopPropagation();
    setOpenAffectationDialog(true);
    setRepresentantToAffect({ ...action });
  };

  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const client = useApolloClient();

  const menuItems: TableActionColumnMenu[] = [
    {
      label: 'Modifier',
      icon: <Edit />,
      onClick: onClickEdit,
      disabled: false,
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: onClickDelete,
      disabled: false,
    },
    {
      label: 'Voir détails',
      icon: <Visibility />,
      onClick: onClickDetail,
      disabled: false,
    },
  ];

  const columns: Column[] = [
    {
      label: 'Photo',
      name: '',
      renderer: (value: any) => {
        return (
          <CustomAvatar
            name={value && value.fullName}
            url={value && value.photo && value.photo.publicUrl}
          />
        );
      },
    },
    { label: 'Fonction', name: 'fonction' },
    { label: 'Prénom', name: 'prenom' },
    { label: 'Nom', name: 'nom' },
    {
      label: 'Téléphone',
      name: 'contact.telProf',
      renderer: (value: any) => {
        return (value.contact && value.contact.telProf) || '-';
      },
    },
    {
      label: 'Mail',
      name: 'contact.mailProf',
      renderer: (value: any) => {
        return (value.contact && value.contact.mailProf) || '-';
      },
    },
    {
      label: 'Adresse',
      name: 'contact.adresse1',
      renderer: (value: any) => {
        return (value.contact && value.contact.adresse1) || '-';
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <Fragment>
            <TableActionColumn
              row={value}
              baseUrl=""
              showResendEmail={false}
              customMenuItems={menuItems}
            />
          </Fragment>
        );
      },
    },
  ];

  const tableProps = {
    isSelectable: false,
    paginationCentered: true,
    columns,
    selected,
    setSelected,
  };

  const DeleteDialogContent = () => {
    if (deleteRow) {
      const name = `${deleteRow.prenom || ''} ${deleteRow.nom || ''}`;
      return (
        <span>
          Êtes-vous sur de vouloir supprimer
          <span style={{ fontWeight: 'bold' }}> {name} </span>?
        </span>
      );
    }
    return <span>Êtes-vous sur de vouloir supprimer ces contacts ?</span>;
  };

  return (
    <Box width="100%">
      <Box className={classes.searchInputBox}>
        <SearchInput searchPlaceholder="Rechercher un contact..." />
        <Box>
          {selected && selected.length > 0 && (
            <CustomButton onClick={() => setOpenDeleteDialog(true)}>
              SUPPRIMER LA SELECTION
            </CustomButton>
          )}
          <CustomButton color="secondary" className={classes.btnAdd} onClick={() => setOpen(true)}>
            Ajout de contact
          </CustomButton>
        </Box>
      </Box>
      <WithSearch
        type="laboratoirerepresentant"
        props={tableProps}
        WrappedComponent={CustomContent}
        searchQuery={DO_SEARCH_LABO_REPRESENTANT}
        optionalMust={[
          { term: { isRemoved: false } },
          { term: { 'laboratoire.id': laboratoire && laboratoire.id } },
        ]}
      />
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
    </Box>
  );
};
export default withRouter(ContactList);
