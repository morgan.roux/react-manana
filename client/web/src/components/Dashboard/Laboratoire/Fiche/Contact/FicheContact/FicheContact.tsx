import { ArrowBack, Edit } from '@material-ui/icons';
import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import CustomAvatar from '../../../../../Common/CustomAvatar';
import CustomButton from '../../../../../Common/CustomButton';
import useStyles from './styles';
import classnames from 'classnames';
import { useApolloClient, useQuery } from '@apollo/react-hooks';
/* import Sectioncontainer from '../../../SectionContainer'; */
import { Box, IconButton, Link, Typography } from '@material-ui/core';
import { GET_PARTENAIRE_REPRESENTANT } from '../../../../../../graphql/PartenaireRepresentant/query';
import {
  PARTENAIRE_REPRESENTANT,
  PARTENAIRE_REPRESENTANTVariables,
} from '../../../../../../graphql/PartenaireRepresentant/types/PARTENAIRE_REPRESENTANT';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import useCommonStyles from '../../../../commonStyles';
import { PARTENAIRE_LABORATOIRE_URL } from '../../../../../../Constant/url';
import { DO_GET_LABO_PARTENAIRE } from '../../../../../../graphql/LaboratoirePartenaire/query';
import {
  GET_LABO_PARTENAIRE,
  GET_LABO_PARTENAIREVariables,
} from '../../../../../../graphql/LaboratoirePartenaire/types/GET_LABO_PARTENAIRE';
import {
  GET_LABORATOIRE_REPRESENTANT,
  GET_LABORATOIRE_REPRESENTANTVariables,
} from '../../../../../../graphql/LaboratoireRepresentant/types/GET_LABORATOIRE_REPRESENTANT';
import { DO_GET_LABORATOIRE_REPRESENTANT } from '../../../../../../graphql/LaboratoireRepresentant';
import SectionContainer from '../SectionContainer';
import { CustomModal } from '../../../../../Common/CustomModal';

interface FicheContactProps {
  representant: any;
  open: boolean;
  setOpen: (value: any) => void;
}

const FicheContact: React.FC<FicheContactProps & RouteComponentProps> = ({
  representant,
  open,
  setOpen,
}) => {
  const classes = useStyles({});
  const handleClick = () => {};
  const client = useApolloClient();
  const commonStyles = useCommonStyles();

  const data = representant;

  const sections = [
    {
      title: 'Informations Personnelles',
      content: [
        {
          subtitle: 'Civilité',
          item: data ? data.civilite : '-',
        },

        {
          subtitle: 'Prénom',
          item: data ? data.prenom : '-',
        },
        {
          subtitle: 'Nom',
          item: data ? data.nom : '-',
        },
        {
          subtitle: 'Genre',
          item: data ? data.sexe : '-',
        },
      ],
    },
    {
      title: 'Coordonnées',
      content: [
        {
          subtitle: 'Adresse',
          item: data && data.contact ? data.contact.adresse1 : '-',
        },
        {
          subtitle: 'Code postal',
          item: data && data.contact ? data.contact.cp : '-',
        },
        {
          subtitle: 'Ville',
          item: data && data.contact ? data.contact.ville : '-',
        },
      ],
    },
    {
      title: 'Internet',
      content: [
        {
          subtitle: 'Adresse de messagerie',
          item: data && data.contact ? data.contact.mailPerso : '-',
        },
        {
          subtitle: 'Afficher comme',
          item: data && data.contact ? data.contact.mailPerso : '-',
        },
        {
          subtitle: 'Page web',
          item: data && data.contact ? data.contact.siteProf : '-',
        },
        { subtitle: 'Adresse de messagerie instantanée', item: '' },
      ],
    },
    {
      title: 'Numéros de téléphone',
      content: [
        {
          subtitle: 'Bureau',
          item: data && data.contact ? data.contact.telProf : '-',
        },
        {
          subtitle: 'Domicile',
          item: data && data.contact ? data.contact.telPerso : '-',
        },
        {
          subtitle: 'Télécopie (bureau)',
          item: data && data.contact ? data.contact.telProf : '-',
        },
        {
          subtitle: 'Téléphone mobile',
          item: data && data.contact ? data.contact.telPerso : '-',
        },
      ],
    },
    {
      title: 'Réseaux sociaux',
      content: [
        {
          subtitle: 'LinkedIn',
          item: (
            <Link href={'https://' + (data?.contact?.urlLinkedinProf ?? '-') || ''} target="_blank">
              linkedin.com
            </Link>
          ),
        },
        {
          subtitle: 'Facebook',
          item: (
            <Link href={'https://' + (data?.contact?.urlFacebookProf ?? '-') || ''} target="_blank">
              facebook.com
            </Link>
          ),
        },
        {
          subtitle: 'WhatSapp',
          item: data && data.contact ? data.contact.whatsAppMobProf : '-',
        },
        {
          subtitle: 'Messenger',
          item: (
            <Link
              href={'https://' + (data?.contact?.urlMessenger ?? '-') || ''}
              target="_blank"
              color="primary"
            >
              messenger.com
            </Link>
          ),
        },
        {
          subtitle: 'Youtube',
          item: (
            <Link href={'https://' + (data?.contact?.urlYoutube ?? '-') || ''} target="_blank">
              youtube.com
            </Link>
          ),
        },
      ],
    },
  ];

  const ImageSection = (
    <Box className={classes.contentSection}>
      <Box className={classes.nameContent}>
        <Box className={classes.sectionImage}>
          <CustomAvatar name={data?.nom ?? 'Nom'} url={data?.photo?.publicUrl ?? 'url'} />
          <Typography className={classes.titleName}>
            {data?.nom ?? '-'} {data?.prenom ?? '-'}
            <Typography className={classes.contentLabel}>
              <Typography className={classes.labelTitle}>Fonction :</Typography>

              <Typography className={classes.labelResult}>{data ? data.fonction : '-'}</Typography>
            </Typography>
          </Typography>
        </Box>
      </Box>
    </Box>
  );
  return (
    <CustomModal
      onClick={event => event.stopPropagation()}
      open={open}
      setOpen={setOpen}
      withBtnsActions={false}
      closeIcon={true}
      headerWithBgColor={true}
      fullWidth={true}
      actionButton={'Enregistrer'}
      withCancelButton={true}
    >
      <Box className={classes.ficheContactContainer}>
        <Box
          width="100%"
          display="flex"
          flexDirection="column"
          alignItems="center"
          justifyContent="center"
        >
          <Typography className={classes.title}>Fiche détaillé de contact</Typography>
          <Box display="flex" alignItems="center" justifyContent="center" flexDirection="column">
            <Typography>{data && data.contact ? data.contact.adresse1 : '-'}</Typography>
          </Box>
        </Box>
        <Box width="100%" padding="25px">
          <Box className={classnames(classes.contentSectionNote)}>
            {ImageSection}
            <Box className={classnames(classes.sectionImg)}>
              <Typography className={classes.inputTitle}>Note</Typography>
              <Box> Note</Box>{' '}
            </Box>
          </Box>
          <Box className={classes.sections}>
            {sections.map(section => (
              <SectionContainer
                title={section.title}
                children={
                  <Box>
                    {section.content.map(content => (
                      <Box style={{ marginTop: 10 }}>
                        <Typography className={classes.titleSub}>{content.subtitle}</Typography>
                        <Typography className={classes.subtitle}>
                          {content.item === '' ? '-' : content.item}
                        </Typography>
                      </Box>
                    ))}
                  </Box>
                }
              />
            ))}
          </Box>
        </Box>
      </Box>
    </CustomModal>
  );
};
export default withRouter(FicheContact);
