import { Paper, Typography, Box } from '@material-ui/core';
import React, { FC, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import ContactList from './ContactList';
import FicheContact from './FicheContact';
import Form from './Form';
import useStyles from './styles';

interface ContactProps {
  laboratoire: any;
  open: boolean;
  setOpen: (value: boolean) => void;
  openDetail: boolean;
  setOpenDetail: (value: boolean) => void;
}

const Contact: FC<ContactProps & RouteComponentProps> = ({
  location: { pathname },
  history: { push },
  laboratoire,
  open,
  setOpen,
  openDetail,
  setOpenDetail,
}) => {
  const classes = useStyles({});
  const [contact, setContact] = useState<any>(null);

  const handleDetail = (open: boolean) => {
    if (!open) setContact(null);
    setOpenDetail(open);
  };

  return (
    <Box className={classes.tableContainer}>
      <Box width="100%">
        <Typography className={classes.title}>Liste des contacts</Typography>
        <ContactList
          laboratoire={laboratoire}
          setContact={setContact}
          setOpen={setOpen}
          setOpenDetail={setOpenDetail}
        />
      </Box>
      <Form
        open={open}
        setOpen={setOpen}
        idLaboratoire={laboratoire && laboratoire.id}
        representant={contact}
        setRepresentant={setContact}
      />
      <FicheContact open={openDetail} setOpen={handleDetail} representant={contact} />
    </Box>
  );
};
export default withRouter(Contact);
