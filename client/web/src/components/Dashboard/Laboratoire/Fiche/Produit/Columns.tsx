import React, { FC } from 'react';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import BlockTwoToneIcon from '@material-ui/icons/BlockTwoTone';
import RemiseCell from '../../../../Main/Content/Produits/ProduitListView/RemiseCell';

export const getColumns = () => {
  return [
    {
      name: 'produit.produitCode.code',
      label: 'Code',
      renderer: (value: any) => {
        return (
          (value && value.produit && value.produit.produitCode && value.produit.produitCode.code) ||
          '-'
        );
      },
    },
    {
      name: 'produit.libelle',
      label: 'Libellé',
      renderer: (value: any) => {
        return (value && value.produit && value.produit.libelle) || '-';
      },
    },
    {
      name: 'stv',
      label: 'STV',
      sortable: true,
    },
    {
      name: 'unitePetitCond',
      label: 'Carton',
      renderer: (value: any) => {
        return (value && value.unitePetitCond) || '-';
      },
      centered: true,
    },
    {
      name: '',
      label: 'Stock plateforme',
      renderer: (value: any) => {
        if (value.qteStock > 0) {
          return <CheckCircleIcon />;
        } else {
          return <BlockTwoToneIcon color="secondary" />;
        }
      },
      centered: true,
    },
    {
      name: '',
      label: 'Remises',
      renderer: (value: any) => {
        return (
          <RemiseCell currentCanalArticle={value ? value : null} type="remise" isView={true} />
        );
      },
      centered: true,
    },
    {
      name: '',
      label: 'Tarif HT',
      renderer: (value: any) => {
        return value.prixPhv ? value.prixPhv + '€' : '-';
      },
      centered: true,
    },
    {
      name: '',
      label: 'Prix net',
      renderer: (value: any) => {
        return <RemiseCell currentCanalArticle={value ? value : null} type="prixNet" />;
      },
      centered: true,
    },
  ];
};
