import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { Box, Typography } from '@material-ui/core';
import React, { ChangeEvent, FC, Fragment, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { GET_SEARCH_CUSTOM_CONTENT_LABORATOIRE_RESSOURCE } from '../../../../../graphql/LaboratoireRessource/query';
import { SEARCH } from '../../../../../graphql/search/query';
import CustomContent from '../../../../Common/newCustomContent';
import SearchInput from '../../../../Common/newCustomContent/SearchInput';
import WithSearch from '../../../../Common/newWithSearch/withSearch';
import { getColumns } from './Columns';
import { Column } from '../../../Content/Interface';
import useStyles from './styles';

interface ProduitCanalsProps {
  idLaboratoire: string | null;
}

const ProduitCanals: FC<ProduitCanalsProps & RouteComponentProps> = ({
  location: { pathname },
  history: { push },
  idLaboratoire,
}) => {
  const classes = useStyles({});
  const columns: Column[] = getColumns();

  const tableProps = {
    isSelectable: false,
    paginationCentered: true,
    columns,
  };

  return (
    <Box className={classes.tableContainer}>
      <Box width="100%">
        <Typography className={classes.title}>Liste des produits</Typography>
        <Box width="100%">
          <Box className={classes.searchInputBox}>
            <SearchInput searchPlaceholder="Rechercher un produit..." />
          </Box>
          <WithSearch
            type="produitcanal"
            props={tableProps}
            WrappedComponent={CustomContent}
            searchQuery={SEARCH}
            optionalMust={[
              { term: { isRemoved: false } },
              { term: { 'produit.produitTechReg.laboExploitant.id': idLaboratoire } },
            ]}
          />
        </Box>
      </Box>
    </Box>
  );
};
export default withRouter(ProduitCanals);
