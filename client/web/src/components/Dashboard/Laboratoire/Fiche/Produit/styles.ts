import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    searchInputBox: {
      padding: '20px 0px 0px 20px',
      display: 'flex',
      justifyContent: 'space-between',
    },
    btnAdd: {
      marginLeft: 8,
      marginRight: 16,
    },
    tableContainer: {
      width: '100%',
      maxWidth: '1300px',
      margin: '8px auto',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize: 18,
      fontFamily: 'Montserrat',
      letterSpacing: 0,
      color: theme.palette.secondary.main,
      opacity: 1,
    },
  }),
);

export default useStyles;
