import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import { Typography } from '@material-ui/core';
import React, { FC, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { LABORATOIRE_URL } from '../../../../Constant/url';
import { GET_LABORATOIRE } from '../../../../graphql/Laboratoire/query';
import {
  LABORATOIRE,
  LABORATOIREVariables,
} from '../../../../graphql/Laboratoire/types/LABORATOIRE';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { getGroupement } from '../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop';
import { LaboratoireForm } from './Form';
import useStyles from './styles';

const CreateUpdate: FC<RouteComponentProps> = ({
  history,
  location: { pathname },
  match: { params },
}) => {
  const classes = useStyles({});
  const { laboratoireId } = params as any;
  const client = useApolloClient();

  const isOnCreate = pathname.includes(`/db/${LABORATOIRE_URL}/create`);
  const isOnEdit = pathname.includes(`/db/${LABORATOIRE_URL}/edit`);

  const { data: laboData, loading: laboLoading } = useQuery<LABORATOIRE, LABORATOIREVariables>(
    GET_LABORATOIRE,
    {
      variables: { id: laboratoireId || '' },
      onError: error => {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );
  const laboratoire = (laboData && laboData.laboratoire) || null;

  return (
    <div className={classes.root}>
      <Typography variant="h6" gutterBottom={true} className={classes.title}>
        {isOnEdit ? 'Modifier un' : 'Créer un nouveau'} laboratoire
      </Typography>
      <LaboratoireForm laboratoire={laboratoire} />
    </div>
  );
};

export default withRouter(CreateUpdate);
