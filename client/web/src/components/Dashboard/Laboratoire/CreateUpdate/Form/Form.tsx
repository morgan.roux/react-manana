import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import classnames from 'classnames';
import React, { FC, useEffect, useState } from 'react';
import { values } from 'react-intl/locale-data/af';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { LABORATOIRE_URL } from '../../../../../Constant/url';
import { CIVILITE_LIST } from '../../../../../Constant/user';
import { DO_CREATE_UPDATE_LABORATOIRE } from '../../../../../graphql/Laboratoire/mutation';
import {
  CREATE_UPDATE_LABORATOIRE,
  CREATE_UPDATE_LABORATOIREVariables,
} from '../../../../../graphql/Laboratoire/types/CREATE_UPDATE_LABORATOIRE';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { isEmailValid } from '../../../../../utils/Validator';
import Backdrop from '../../../../Common/Backdrop';
import CustomButton from '../../../../Common/CustomButton';
import { CustomCheckbox } from '../../../../Common/CustomCheckbox';
import CustomSelect from '../../../../Common/CustomSelect';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import useStyles from './styles';
import useForm, { LaboratoireInterface } from './useForm';

interface laboratoireInterface {
  laboratoire: any;
  isEdit?: boolean;
  goBack?: () => void;
}

const Form: FC<RouteComponentProps & laboratoireInterface> = ({
  laboratoire,
  history: { push },
  location: { pathname },
  isEdit,
  goBack,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();

  const isOnEdit = isEdit || pathname.includes(`/db/${LABORATOIRE_URL}/edit`);

  const defaultFormData = {
    id: (laboratoire && laboratoire.id) || '',
    nomLabo: (laboratoire && laboratoire.nomLabo) || '',
    adresse: laboratoire && laboratoire.laboSuite ? laboratoire.laboSuite.adresse : '',
    telephone: laboratoire && laboratoire.laboSuite ? laboratoire.laboSuite.telephone : '',
    ville: laboratoire && laboratoire.laboSuite ? laboratoire.laboSuite.ville : '',
    codePostal: laboratoire && laboratoire.laboSuite ? laboratoire.laboSuite.codePostal : '',
    webSiteUrl: laboratoire && laboratoire.laboSuite ? laboratoire.laboSuite.webSiteUrl : '',
  };

  const {
    values: { id, nomLabo, adresse, telephone, ville, codePostal, webSiteUrl },
    setValues,
    handleChange,
  } = useForm(defaultFormData);

  const [createUpdateLaboratoire, { loading }] = useMutation<
    CREATE_UPDATE_LABORATOIRE,
    CREATE_UPDATE_LABORATOIREVariables
  >(DO_CREATE_UPDATE_LABORATOIRE, {
    onCompleted: data => {
      if (data && data.createUpdateLaboratoire) {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: `${isOnEdit ? 'Modification' : 'Création'} avec succès`,
        });
        goToList();
      }
    },
  });

  React.useEffect(() => {
    if (laboratoire) {
      setValues(defaultFormData);
    }
  }, [laboratoire]);

  const goToList = () => {
    if (goBack) {
      goBack();
    } else {
      push(`/db/${LABORATOIRE_URL}`);
    }
  };

  const handleSubmit = () => {
    createUpdateLaboratoire({
      variables: {
        id: id || '',
        nomLabo,
        suite: {
          adresse,
          telephone,
          ville,
          codePostal,
          webSiteUrl,
        },
      },
    });
  };

  return (
    <div className={classes.form}>
      {loading && <Backdrop />}
      <div className={classes.marginBottom}>
        <div className={classnames(classes.marginBottom, classes.flex)}>
          <CustomFormTextField
            name="nomLabo"
            label="Nom"
            required={true}
            value={nomLabo}
            onChange={handleChange}
          />
        </div>
        <div className={classnames(classes.marginBottom, classes.flex)}>
          <CustomFormTextField
            name="telephone"
            label="Telephone"
            value={telephone}
            onChange={handleChange}
          />
        </div>{' '}
        <div className={classnames(classes.marginBottom, classes.flex)}>
          <CustomFormTextField
            name="adresse"
            label="Adresse"
            value={adresse}
            onChange={handleChange}
          />
        </div>
        <div className={classnames(classes.marginBottom, classes.flex)}>
          <div className={classnames(classes.marginRight, classes.w100)}>
            <CustomFormTextField name="ville" label="Ville" value={ville} onChange={handleChange} />
          </div>
          <div className={classes.w100}>
            <CustomFormTextField
              name="codePostal"
              label="Code postal"
              value={codePostal}
              onChange={handleChange}
            />
          </div>
        </div>
        <div className={classnames(classes.marginBottom, classes.flex)}>
          <CustomFormTextField
            name="webSiteUrl"
            label="Site url"
            value={webSiteUrl}
            onChange={handleChange}
          />
        </div>
        <div className={classnames(classes.marginBottom, classes.flex)}>
          <div className={classnames(classes.marginRight, classes.w100)}>
            <CustomButton size="large" fullWidth={true} onClick={goToList}>
              RETOUR
            </CustomButton>
          </div>
          <div className={classes.w100}>
            <CustomButton
              size="large"
              color="secondary"
              fullWidth={true}
              onClick={handleSubmit}
              disabled={!nomLabo}
            >
              {false ? 'Chargement...' : 'ENREGISTRER'}
            </CustomButton>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(Form);
