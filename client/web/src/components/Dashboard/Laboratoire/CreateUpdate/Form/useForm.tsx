import { ChangeEvent, MouseEvent, useState } from 'react';

export interface LaboratoireInterface {
  id?: string;
  nomLabo: string;
  adresse: string;
  telephone: string;
  ville: string;
  codePostal: string;
  webSiteUrl: string;
}

const useForm = (defaultState?: LaboratoireInterface | null) => {
  const initialLaboratoire = {
    id: '',
    nomLabo: '',
    adresse: '',
    telephone: '',
    ville: '',
    codePostal: '',
    webSiteUrl: '',
    __typename: 'Laboratoire',
  };

  const initialState: LaboratoireInterface | null = defaultState || initialLaboratoire;

  const [values, setValues] = useState<LaboratoireInterface>(initialState);

  const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement | any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      setValues(prevState => ({ ...prevState, [name]: value }));
    }
  };

  return {
    handleChange,
    values,
    setValues,
  };
};

export default useForm;
