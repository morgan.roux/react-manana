import React, { FC } from 'react';
import useStyles from './styles';
import { Avatar, Button } from '@material-ui/core';
import image from '../../../../assets/img/2.png';
import {
  FICHE_LABORATOIRE,
  FICHE_LABORATOIREVariables,
} from '../../../../graphql/Laboratoire/types/FICHE_LABORATOIRE';
import { useQuery, useApolloClient } from '@apollo/react-hooks';
import { GET_FICHE_LABORATOIRE } from '../../../../graphql/Laboratoire/query';
import { Loader } from '../../Content/Loader';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../utils/snackBarUtils';

interface FicheLaboratoireProps {
  match: {
    params: { idLaboratoire: string };
  };
}

const FicheLaboratoire: FC<FicheLaboratoireProps & RouteComponentProps> = ({ match, history }) => {
  const classes = useStyles();
  const titreTableau: string[] = ['Identifiant', 'Code', 'Code groupe', 'Nom'];
  const laboratoire = useApolloClient();
  const { loading, error, data } = useQuery<FICHE_LABORATOIRE, FICHE_LABORATOIREVariables>(
    GET_FICHE_LABORATOIRE,
    {
      variables: {
        id: match && match.params && match.params.idLaboratoire,
      },
      fetchPolicy: 'cache-and-network',
    },
  );

  if (loading) return <Loader />;

  if (error) {
    const snackBarData: SnackVariableInterface = {
      type: 'ERROR',
      message: "Des erreurs se sont survenues pendant l'affichage de la fiche laboratoire choisie",
      isOpen: true,
    };
    displaySnackBar(laboratoire, snackBarData);
    history.push('/');
  }

  const handleClick = () => {
    history.goBack();
  };

  return (
    <div className={classes.container}>
      {data && data.laboratoire ? (
        <div
          style={{
            alignItems: 'center',
            background: 'white',
            width: '25%',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <div>
            <Avatar alt="Image" src={image} className={classes.large} />
          </div>
          <h2>{data.laboratoire.nomLabo || 'Laboratoire inconnue'}</h2>
          <div className={classes.tableau}>
            <div>
              {titreTableau.map(titre => (
                <h5
                  key={titre}
                  style={{
                    color: '#878787',
                    opacity: 1,
                    letterSpacing: 0,
                    font: 'Regular 16px/33px Montserrat',
                  }}
                >
                  {titre}
                </h5>
              ))}
            </div>
            <div>
              <h5 className={classes.h5}>{data.laboratoire.id || 'Non renseigné'}</h5>
              <h5 className={classes.h5}>{'Non renseigné'}</h5>
              <h5 className={classes.h5}>{'Non renseigné'}</h5>
              <h5 className={classes.h5}>{data.laboratoire.nomLabo || 'Non renseigné'}</h5>
            </div>
          </div>
          <div>
            <Button variant="contained" onClick={handleClick}>
              Retour
            </Button>
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default withRouter(FicheLaboratoire);
