import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import { ModeleOperationMarketingPage } from '@app/ui-kit';
import React, { FC, useState } from 'react';
import { ChromePicker } from 'react-color';
import { RouteComponentProps, withRouter } from 'react-router';
import noContentActionImageSrc from '../../../assets/img/image_default.png';
import {
  CREATE_MODELE_OPERATION_MARKETING,
  DELETE_MODELE_OPERATION_MARKETING,
  DO_CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS,
  DO_PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION,
  UPDATE_MODELE_OPERATION_MARKETING,
} from '../../../federation/partenaire-service/ModeleOperationMarketing/mutation';
import { GET_MODELE_OPERATION_MARKETINGS } from '../../../federation/partenaire-service/ModeleOperationMarketing/query';
import {
  CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS,
  CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUSVariables,
} from '../../../federation/partenaire-service/ModeleOperationMarketing/types/CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS';
import {
  CREATE_MODELE_OPERATION_MARKETING as CREATE_MODELE_OPERATION_MARKETING_TYPE,
  CREATE_MODELE_OPERATION_MARKETINGVariables,
} from '../../../federation/partenaire-service/ModeleOperationMarketing/types/CREATE_MODELE_OPERATION_MARKETING';
import {
  DELETE_MODELE_OPERATION_MARKETING as DELETE_MODELE_OPERATION_MARKETING_TYPE,
  DELETE_MODELE_OPERATION_MARKETINGVariables,
} from '../../../federation/partenaire-service/ModeleOperationMarketing/types/DELETE_MODELE_OPERATION_MARKETING';
import {
  GET_MODELE_OPERATION_MARKETINGS as GET_MODELE_OPERATION_MARKETINGS_ALL,
  GET_MODELE_OPERATION_MARKETINGSVariables,
} from '../../../federation/partenaire-service/ModeleOperationMarketing/types/GET_MODELE_OPERATION_MARKETINGS';
import {
  PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION,
  PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTIONVariables,
} from '../../../federation/partenaire-service/ModeleOperationMarketing/types/PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION';
import {
  UPDATE_MODELE_OPERATION_MARKETING as UPDATE_MODELE_OPERATION_MARKETING_TYPE,
  UPDATE_MODELE_OPERATION_MARKETINGVariables,
} from '../../../federation/partenaire-service/ModeleOperationMarketing/types/UPDATE_MODELE_OPERATION_MARKETING';
import {
  CREATE_PLAN_MARKETING_TYPE,
  DELETE_ONE_PLAN_MARKETING_TYPE,
  DO_CHANGE_PLAN_MARKETIN_TYPE_ACTIVATION_STATUS,
  DO_PERSONNALISE_ONE_PLAN_MARKETIN_TYPE,
  UPDATE_ONE_PLAN_MARKETIN_TYPE,
} from '../../../federation/partenaire-service/planMarketing/mutation';
import { GET_LIST_PLAN_MARKETING_TYPES } from '../../../federation/partenaire-service/planMarketing/query';
import {
  CHANGE_PLAN_MARKETIN_TYPE_ACTIVATION_STATUS,
  CHANGE_PLAN_MARKETIN_TYPE_ACTIVATION_STATUSVariables,
} from '../../../federation/partenaire-service/planMarketing/types/CHANGE_PLAN_MARKETIN_TYPE_ACTIVATION_STATUS';
import {
  CREATE_PLAN_MARKETING_TYPE as CREATE_PLAN_MARKETING_TYPES,
  CREATE_PLAN_MARKETING_TYPEVariables,
} from '../../../federation/partenaire-service/planMarketing/types/CREATE_PLAN_MARKETING_TYPE';
import {
  DELETE_ONE_PLAN_MARKETING_TYPE as DELETE_ONE_PLAN_MARKETING_TYPES,
  DELETE_ONE_PLAN_MARKETING_TYPEVariables,
} from '../../../federation/partenaire-service/planMarketing/types/DELETE_ONE_PLAN_MARKETING_TYPE';
// import {commonFieldComponent} from './../../Common/CommonFieldsForm';
import {
  GET_LIST_PLAN_MARKETING_TYPES as GET_LIST_PLAN_MARKETING_TYPES_TYPE,
  GET_LIST_PLAN_MARKETING_TYPESVariables,
} from '../../../federation/partenaire-service/planMarketing/types/GET_LIST_PLAN_MARKETING_TYPES';
import {
  PERSONNALISE_ONE_PLAN_MARKETIN_TYPE,
  PERSONNALISE_ONE_PLAN_MARKETIN_TYPEVariables,
} from '../../../federation/partenaire-service/planMarketing/types/PERSONNALISE_ONE_PLAN_MARKETIN_TYPE';
import {
  UPDATE_ONE_PLAN_MARKETIN_TYPE as UPDATE_ONE_PLAN_MARKETIN_TYPES,
  UPDATE_ONE_PLAN_MARKETIN_TYPEVariables,
} from '../../../federation/partenaire-service/planMarketing/types/UPDATE_ONE_PLAN_MARKETIN_TYPE';
import { AppAuthorization } from '../../../services/authorization';
import { getGroupement, getPharmacie, getUser } from '../../../services/LocalStorage';
import {
  PlanMarketingTypeActionSortFields,
  SortDirection,
} from '../../../types/federation-global-types';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import CommonFieldsForm from '../../Common/CommonFieldsForm';
import { FEDERATION_CLIENT } from '../DemarcheQualite/apolloClientFederation';

interface ModeleOperationMarketingProps {
  match: {
    params: {
      idModele: string | undefined;
    };
  };
}
const ModeleOperationMarketing: FC<ModeleOperationMarketingProps & RouteComponentProps> = ({
  match,
  history: { push },
}) => {
  const user = getUser();
  const auth = new AppAuthorization(user);
  const [commonFields, setCommonFields] = useState<any>({
    userParticipants: [],
    tache: null,
    importance: null,
  });
  const [couleur, setCouleur] = useState<any>();
  const [morePlanMarketingType, setMorePlanMarketingType] = useState<number>(5);
  const [moreTask, setMoreTask] = useState<number>(5);
  const client = useApolloClient();
  const pharmacie = getPharmacie();
  const groupement = getGroupement();
  const [idPlanMarketingType, setIdPlanMarketingType] = useState<string | undefined>(
    match.params.idModele,
  );

  // To-Do
  const [loadOperationMarketingActions, loadingOperationMarketingActions] = useLazyQuery<
    GET_MODELE_OPERATION_MARKETINGS_ALL,
    GET_MODELE_OPERATION_MARKETINGSVariables
  >(GET_MODELE_OPERATION_MARKETINGS, { client: FEDERATION_CLIENT });

  const [createOperationMarketingAction, creatingOperationMarketingAction] = useMutation<
    CREATE_MODELE_OPERATION_MARKETING_TYPE,
    CREATE_MODELE_OPERATION_MARKETINGVariables
  >(CREATE_MODELE_OPERATION_MARKETING, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La To-Do a été créee avec success',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingOperationMarketingActions.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs sont survenues pendant la création de la To-Do',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [updateOperationMarketingAction, updatingOperationMarketingAction] = useMutation<
    UPDATE_MODELE_OPERATION_MARKETING_TYPE,
    UPDATE_MODELE_OPERATION_MARKETINGVariables
  >(UPDATE_MODELE_OPERATION_MARKETING, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La To-Do a été modifiéé avec success',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingOperationMarketingActions.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs sont survenues pendant la modification de la to-Do',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [deleteOperationmarketingAction, deletingOperationMarketingAction] = useMutation<
    DELETE_MODELE_OPERATION_MARKETING_TYPE,
    DELETE_MODELE_OPERATION_MARKETINGVariables
  >(DELETE_MODELE_OPERATION_MARKETING, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La To-Do a été supprimé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingOperationMarketingActions.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression de la To-Do',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  // Modele
  const planMarketingTypes = useQuery<
    GET_LIST_PLAN_MARKETING_TYPES_TYPE,
    GET_LIST_PLAN_MARKETING_TYPESVariables
  >(GET_LIST_PLAN_MARKETING_TYPES, {
    fetchPolicy: 'cache-and-network',
    variables: {
      idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
      filter: { idGroupement: { eq: groupement.id } },
      paging: {
        offset: 0,
        limit: 100,
      },
    },
    client: FEDERATION_CLIENT,
    onCompleted: data => {
      if (data.pRTPlanMarketingTypes) {
        idPlanMarketingType &&
          loadOperationMarketingActions({
            variables: {
              idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
              filter: {
                and: [
                  {
                    idPharmacie: {
                      eq: pharmacie.id,
                    },
                  },
                  {
                    idPlanMarketingType: {
                      eq: idPlanMarketingType,
                    },
                  },
                ],
              },
              sorting: [
                {
                  field: PlanMarketingTypeActionSortFields.jourLancement,
                  direction: SortDirection.ASC,
                },
              ],
            },
          });
      }
    },
  });

  const [createModele, creatingModele] = useMutation<
    CREATE_PLAN_MARKETING_TYPES,
    CREATE_PLAN_MARKETING_TYPEVariables
  >(CREATE_PLAN_MARKETING_TYPE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La modèle a été créee avec success',
        type: 'SUCCESS',
        isOpen: true,
      });
      planMarketingTypes.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs sont survenues pendant la création de la modèle',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [updateModele, updatingModele] = useMutation<
    UPDATE_ONE_PLAN_MARKETIN_TYPES,
    UPDATE_ONE_PLAN_MARKETIN_TYPEVariables
  >(UPDATE_ONE_PLAN_MARKETIN_TYPE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La modèle a été modifiéé avec success',
        type: 'SUCCESS',
        isOpen: true,
      });
      planMarketingTypes.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs sont survenues pendant la modification de la modèle',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [personnaliseModele, personnalisingModele] = useMutation<
    PERSONNALISE_ONE_PLAN_MARKETIN_TYPE,
    PERSONNALISE_ONE_PLAN_MARKETIN_TYPEVariables
  >(DO_PERSONNALISE_ONE_PLAN_MARKETIN_TYPE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le modèle a été personnalisé avec succès',
        type: 'SUCCESS',
        isOpen: true,
      });
      planMarketingTypes.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs sont survenues pendant la personnalisation du modèle',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [personnaliseTask, personnalisingTask] = useMutation<
    PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION,
    PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTIONVariables
  >(DO_PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: "L'action a été personnalisée avec succès",
        type: 'SUCCESS',
        isOpen: true,
      });
      planMarketingTypes.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs sont survenues pendant la personnalisation de l'action",
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [
    changePlanMarketingTypeActivationStatus,
    changingPlanMarketingTypeActivationStatus,
  ] = useMutation<
    CHANGE_PLAN_MARKETIN_TYPE_ACTIVATION_STATUS,
    CHANGE_PLAN_MARKETIN_TYPE_ACTIVATION_STATUSVariables
  >(DO_CHANGE_PLAN_MARKETIN_TYPE_ACTIVATION_STATUS, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le modèle a été activé avec succès',
        type: 'SUCCESS',
        isOpen: true,
      });
      planMarketingTypes.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs sont survenues pendant l'activation du modèle",
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [
    changePlanMarketingTypeActionActivationStatus,
    changingPlanMarketingTypeActionActivationStatus,
  ] = useMutation<
    CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS,
    CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUSVariables
  >(DO_CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le modèle a été activé avec succès',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingOperationMarketingActions.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs sont survenues pendant l'activation du modèle",
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [deletePlanMarketingType, deletingPlanMarketingType] = useMutation<
    DELETE_ONE_PLAN_MARKETING_TYPES,
    DELETE_ONE_PLAN_MARKETING_TYPEVariables
  >(DELETE_ONE_PLAN_MARKETING_TYPE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La modèle a été supprimé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      planMarketingTypes.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression de la modèle',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const handeleRequestTaskSave = (data: any) => {
    if (!data.id) {
      createOperationMarketingAction({
        variables: {
          input: {
            jourLancement: data.jourLancement,
            idParticipants: data.idParticipants,
            description: data.description,
            idPlanMarketingType: idPlanMarketingType || '',
            idFonction: commonFields.tache?.idFonction,
            idTache: commonFields.tache?.idTache,
            idImportance: data.idImportance,
            pendant: data.pendant,
          },
        },
      });
      return;
    }
    if (data.id) {
      updateOperationMarketingAction({
        variables: {
          id: data.id,
          input: {
            jourLancement: data.jourLancement,
            idParticipants: data.idParticipants,
            description: data.description,
            idPlanMarketingType: idPlanMarketingType || '',
            idFonction: data.idfonction,
            idTache: data.idTache,
            idImportance: data.idImportance,
            pendant: data.pendant,
          },
        },
      });
    }
  };

  const handeleRequestTaskPersonnalisation = (task: any) => {
    const { jourLancement, pendant, description } = task;
    personnaliseTask({
      variables: {
        idPharmacie: pharmacie.id,
        idPlanMarketingTypeAction: task.id,
        input: {
          description,
          jourLancement,
          pendant,
          idPlanMarketingType: idPlanMarketingType || '',
          idFonction: commonFields.tache?.idFonction,
          idTache: commonFields.tache?.idTache,
          idImportance: task.idImportance,
          idParticipants: task.idParticipants,
        },
      },
    });
  };

  const handleRequestSaveModele = (data: any) => {
    if (!data.id) {
      createModele({
        variables: {
          input: {
            pRTPlanMarketingType: {
              idGroupement: groupement.id,
              libelle: data.libelle,
              code: data.code,
              couleur, //: data.couleur,
            },
          },
        },
      });
      return;
    }
    if (data.id) {
      updateModele({
        variables: {
          input: {
            id: data.id,
            update: {
              idGroupement: groupement.id,
              libelle: data.libelle,
              code: data.code,
              couleur, //: data.couleur,
            },
          },
        },
      });
    }
  };

  const handleRequestPlanMarketingTypePersonnalisation = (planMarketingType: any) => {
    const { libelle, code } = planMarketingType;
    personnaliseModele({
      variables: {
        idPharmacie: pharmacie.id,
        idPlanMarketingType: planMarketingType.id,
        input: { libelle, code, couleur, idGroupement: groupement.id },
      },
    });
  };

  const handleRequestDeletePlanMarketingType = (planMarketingType: any) => {
    deletePlanMarketingType({
      variables: { id: planMarketingType.id },
    });
  };

  const handleRequestDeleteTask = (data: any) => {
    deleteOperationmarketingAction({
      variables: {
        id: data.id,
      },
    });
  };

  const handleOpenTaskForm = (task: any) => {
    setCommonFields({
      handlePlanMarketingTypeClick: task.participants || [],
      importance: task.importance,
      tache: { id: task.idTask, idFonction: task.idFonction, idTask: task.idTask },
    });
  };

  const handleFetchMoreType = (count: number) => {
    setMorePlanMarketingType(count);
  };

  const handleRequestChangeTaskStatus = (task: any) => {};

  const handleRequestChangeTaskActivationStatus = (task: any) => {
    changePlanMarketingTypeActionActivationStatus({
      variables: {
        active: !task.active,
        idPlanMarketingTypeAction: task.id,
        idPharmacie: pharmacie.id,
      },
    });
  };

  const handleRequestChangePlanMarketingTypeActivationStatus = (planMarketingType: any) => {
    changePlanMarketingTypeActivationStatus({
      variables: {
        active: !planMarketingType.active,
        idPlanMarketingType: planMarketingType.id,
        idPharmacie: pharmacie.id,
      },
    });
  };

  const handlePlanMarketingTypeClick = (planMarketingType: any) => {
    window.history.pushState(null, '', `/#/db/modele-operation-marketing/${planMarketingType.id}`);
    setIdPlanMarketingType(planMarketingType.id);

    loadOperationMarketingActions({
      variables: {
        idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
        filter: {
          and: [
            {
              idGroupement: {
                eq: groupement.id,
              },
            },
            {
              idPlanMarketingType: {
                eq: planMarketingType.id,
              },
            },
          ],
        },
        sorting: [
          {
            field: PlanMarketingTypeActionSortFields.jourLancement,
            direction: SortDirection.ASC,
          },
        ],
      },
    });
  };

  return (
    <>
      <ModeleOperationMarketingPage
        isAdmin={auth.isSupAdminOrIsGpmAdmin}
        idPlanMarketingType={idPlanMarketingType}
        noContentActionImageSrc={noContentActionImageSrc}
        onRequestTaskSave={handeleRequestTaskSave}
        tasks={{
          data: loadingOperationMarketingActions.data?.planMarketingTypeActions.nodes as any,
          loading: loadingOperationMarketingActions.loading,
        }}
        savingTask={
          creatingOperationMarketingAction.loading ||
          updatingOperationMarketingAction.loading ||
          personnalisingTask.loading
        }
        savingPlanMarketingType={
          creatingModele.loading || updatingModele.loading || personnalisingTask.loading
        }
        commonFieldsIds={{
          idImportance: commonFields.importance?.id,
          idTache: commonFields.tache?.idTache,
          idFonction: commonFields.tache?.idFonction,
          idParticipants: commonFields.userParticipants?.map(user => user.id) as any,
        }}
        commonFieldComponent={
          <CommonFieldsForm
            selectedUsers={commonFields.userParticipants}
            urgence={null}
            hideUserInput={true} // Pas de selection de collaborateurs
            urgenceProps={{
              useCode: false,
            }}
            usersModalProps={{
              withNotAssigned: false,
              searchPlaceholder: 'Rechercher...',
              withAssignTeam: false,
              singleSelect: false,
              title: 'Choix de collaborateurs',
            }}
            selectUsersFieldLabel="Collaborateurs"
            projet={commonFields.tache}
            onChangeUsersSelection={users =>
              setCommonFields(prevState => ({ ...prevState, userParticipants: users }))
            }
            /*onChangeProjet={projet =>
              setCommonFields(prevState => ({ ...prevState, tache: projet }))
            }*/
            importance={commonFields.importance}
            onChangeImportance={importance =>
              setCommonFields(prevState => ({ ...prevState, importance }))
            }
          />
        }
        planMarketingTypes={{
          data: planMarketingTypes.data?.pRTPlanMarketingTypes.nodes as any,
          loading: planMarketingTypes.loading,
        }}
        couleur={couleur}
        setCouleur={setCouleur}
        colorPicker={
          <fieldset style={{ display: 'flex', padding: 16, justifyContent: 'center' }}>
            <legend>
              <span style={{ fontSize: '0.75rem' }}>Couleur</span>
            </legend>
            <ChromePicker
              //className={classes.colorPicker}
              color={couleur}
              onChangeComplete={color => setCouleur(color?.hex || '')}
            />
          </fieldset>
        }
        onOpenTaskForm={handleOpenTaskForm}
        onPlanMarketingTypeClick={handlePlanMarketingTypeClick}
        onRequestPlanMarketingTypeSave={handleRequestSaveModele}
        onRequestDeletePlanMarketingType={handleRequestDeletePlanMarketingType}
        onRequestDeleteTask={handleRequestDeleteTask}
        onFetchMoreType={handleFetchMoreType}
        onRequestPlanMarketingTypePersonnalisation={handleRequestPlanMarketingTypePersonnalisation}
        onRequestChangePlanMarketingTypeActivationStatus={
          handleRequestChangePlanMarketingTypeActivationStatus
        }
        onRequestChangeTaskStatus={handleRequestChangeTaskStatus}
        onRequestChangeTaskActivationStatus={handleRequestChangeTaskActivationStatus}
        onRequestTaskPersonnalisation={handeleRequestTaskPersonnalisation}
      />
    </>
  );
};

export default withRouter(ModeleOperationMarketing);
