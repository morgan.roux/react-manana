import { useContext, Dispatch, SetStateAction } from 'react';
import { ContentStateInterface, ContentContext } from '../../../../AppContext';
import { useApolloClient, useMutation } from '@apollo/react-hooks';
import {
  DELETE_SOFT_PHARMACIES,
  DELETE_SOFT_PHARMACIESVariables,
} from '../../../../graphql/Pharmacie/types/DELETE_SOFT_PHARMACIES';
import { DO_DELETE_SOFT_PHARMACIES } from '../../../../graphql/Pharmacie';
import { differenceBy } from 'lodash';
import { displaySnackBar } from '../../../../utils/snackBarUtils';

/**
 *  Delete pharmacie hooks
 */
const useDeletePharmacie = (
  selected: any[],
  setSelected: Dispatch<SetStateAction<any[]>>,
  deleteRow: any,
  setDeleteRow: Dispatch<SetStateAction<any>>,
) => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  // const { checkedsPharmacie } = useCheckedPharmacie();
  const selectedIds = (selected.length > 0 && selected.map(i => i.id)) || [];
  const client = useApolloClient();

  const [deletePharmacies, { data, loading }] = useMutation<
    DELETE_SOFT_PHARMACIES,
    DELETE_SOFT_PHARMACIESVariables
  >(DO_DELETE_SOFT_PHARMACIES, {
    variables: { ids: selectedIds },
    update: (cache, { data }) => {
      if (data && data.deleteSoftPharmacies) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables,
          });
          if (req && req.search && req.search.data) {
            const source = req.search.data;
            const result = data.deleteSoftPharmacies;
            const dif = differenceBy(source, result, 'id');
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: dif,
                  },
                },
              },
              variables,
            });
            client.writeData({
              data: { checkedsPharmacie: differenceBy(selected, result, 'id') },
            });
          }
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  return { deletePharmacies, data, loading };
};

export default useDeletePharmacie;
