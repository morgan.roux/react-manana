import {
  CREATE_UPDATE_PHARMACIEVariables,
  CREATE_UPDATE_PHARMACIE,
} from '../../../../graphql/Pharmacie/types/CREATE_UPDATE_PHARMACIE';
import { useContext, useState } from 'react';
import { ContentStateInterface, ContentContext } from '../../../../AppContext';
import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { DO_CREATE_UPDATE_PHARMACIE } from '../../../../graphql/Pharmacie';
import { displaySnackBar } from '../../../../utils/snackBarUtils';

/**
 *  Create pharmacie hooks
 */
const useCreateUpdatePharmacie = (values: CREATE_UPDATE_PHARMACIEVariables) => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const client = useApolloClient();
  const [mutationSuccess, setMutationSuccess] = useState<boolean>(false);

  const { informatique: info, nbEmploye, nbAssistant, nbAutreTravailleur, concept: c } = values;

  const nbreComptoir = Number((info && info.nbreComptoir) || 0);
  const nbreBackOffice = Number((info && info.nbreBackOffice) || 0);
  const nbreBureau = Number((info && info.nbreBureau) || 0);
  const nbrePoste = Number((info && info.nbrePoste) || 0);

  const surfaceTotale = Number((c && c.surfaceTotale) || 0);
  const surfaceVente = Number((c && c.surfaceTotale) || 0);
  const SurfaceVitrine = Number((c && c.surfaceTotale) || 0);
  const volumeLeaflet = Number((c && c.surfaceTotale) || 0);
  const nbreLineaire = Number((c && c.surfaceTotale) || 0);
  const nbreVitrine = Number((c && c.surfaceTotale) || 0);

  const [createUpdatePharmacie, { loading: mutationLoading }] = useMutation<
    CREATE_UPDATE_PHARMACIE,
    CREATE_UPDATE_PHARMACIEVariables
  >(DO_CREATE_UPDATE_PHARMACIE, {
    variables: {
      ...values,
      sortie: values.sortie ? 1 : 0,
      sortieFuture: values.sortieFuture ? 1 : 0,
      nbEmploye: Number(nbEmploye),
      nbAssistant: Number(nbAssistant),
      nbAutreTravailleur: Number(nbAutreTravailleur),
      // contact: { ...values.contact },
      entreeSortie: {
        ...values.entreeSortie,
        sortie: values.entreeSortie && values.entreeSortie.sortie ? 1 : 0,
        sortieFuture: values.entreeSortie && values.entreeSortie.sortieFuture ? 1 : 0,
      },
      // satisfaction: { ...values.satisfaction },
      // segmentation: { ...values.segmentation },
      // compta: { ...values.compta },
      informatique: {
        ...values.informatique,
        nbreComptoir,
        nbreBackOffice,
        nbreBureau,
        nbrePoste,
      },
      // achat: { ...values.achat },
      // cap: { ...values.cap },
      concept: {
        ...values.concept,
        surfaceTotale,
        surfaceVente,
        SurfaceVitrine,
        volumeLeaflet,
        nbreLineaire,
        nbreVitrine,
      },
      // chiffreAffaire: { ...values.chiffreAffaire },
    },
    update: (cache, { data }) => {
      if (data && data.createUpdatePharmacie && values && !values.id) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables,
          });
          if (req && req.search && req.search.data) {
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    total: req.search.total + 1,
                    data: [...req.search.data, data.createUpdatePharmacie],
                  },
                },
              },
              variables,
            });
          }
        }
      }
    },
    onCompleted: data => {
      if (data && data.createUpdatePharmacie) {

        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: `${values.id ? 'Modification' : 'Création'} réussie`,
         
        });
        setMutationSuccess(true);
        client.writeData({ data: { checkedsPharmacie: null } });
      }
    },
    onError: errors => {
      if (errors.graphQLErrors) {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
        });
      }
    },
  });

  return { createUpdatePharmacie, mutationSuccess, mutationLoading };
};

export default useCreateUpdatePharmacie;
