import { PHARMACIE_URL } from '../../../../Constant/url';

/**
 *  Subtoolbar button action hooks
 */
export const useButtonHeadAction = (push: any) => {
  const goToAddPharmacie = () => push(`/db/${PHARMACIE_URL}/create`);
  const goBack = () => push(`/db/${PHARMACIE_URL}`);
  return { goToAddPharmacie, goBack };
};
