import { useQuery } from '@apollo/react-hooks';
import { GET_CHECKEDS_PHARMACIE } from '../../../../graphql/Pharmacie/local';

/**
 *  List checked pharmacie hooks
 */
const useCheckedPharmacie = () => {
  const { data } = useQuery(GET_CHECKEDS_PHARMACIE);
  const checkedsPharmacie = data && data.data && data.data.checkedsPharmacie;
  return { checkedsPharmacie };
};

export default useCheckedPharmacie;
