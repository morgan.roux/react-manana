import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    informatiqueRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      padding: '25px 0px',
      '@media (max-width: 900px)': {
        flexWrap: 'wrap',
      },
      '& > div': {
        width: '50%',
        '@media (max-width: 900px)': {
          width: '90%',
          marginBottom: 24,
        },
      },
      '& > div:not(:nth-last-child(1)) ': {
        marginRight: 15,
        '@media (max-width: 900px)': {
          marginRight: 0,
        },
      },
      '& .MuiTableHead-root th': {
        minWidth: 'auto !important',
        padding: '5px 16px !important',
      },
      '& .MuiInputBase-root.Mui-disabled': {
        background: '#F2F2F2',
      },
      '& .MuiInputBase-root': {
        padding: 0,
        height: 44,
      },

      '& textarea[disabled]': {
        background: '#F2F2F2',
      },
    },
    inputsContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      '& > div': {
        marginBottom: 16,
        height: 44,
        '@media (max-width: 992px)': {
          height: 'auto',
        },
      },
      '& > div:nth-child(1)': {
        marginRight: 15,
        '@media (max-width: 992px)': {
          marginRight: 0,
        },
      },
      '& .MuiInputBase-root.Mui-disabled': {
        background: '#F2F2F2',
      },
      '@media (max-width: 992px)': {
        flexWrap: 'wrap',
      },
    },
    employeesInfosInformatique: {
      fontSize: 12,
      display: 'flex',
      alignItems: 'center',
    },
    nbPosteContainer: {
      display: 'flex',
      flexDirection: 'column',
      '& > p': {
        marginBottom: 15,
        marginTop: 16,
      },
    },
  }),
);

export default useStyles;
