import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../FichePharmacie';
import InfoLogiciel from './InfoLogiciel';
import HistoriqueTransmissions from './HistoriqueTransmissions';
import useStyles from './styles';
import FichePharmacieCard from '../FichePharmacieCard';

const Informatique: FC<FichePharmacieChildProps> = ({
  pharmacie,
  handleChange,
  handleChangeDate,
  values,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  isEdit,
}) => {
  const classes = useStyles({});

  const cards = [
    {
      title: 'Information logiciel',
      children: (
        <InfoLogiciel
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      title: 'Historique des Transmissions des Statistiques',
      children: (
        <HistoriqueTransmissions
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
  ];

  return (
    <div className={classes.informatiqueRoot}>
      {cards.map((card, index) => (
        <FichePharmacieCard key={`fiche_pharma_entree_sortie_1_${index}`} title={card.title}>
          {card.children}
        </FichePharmacieCard>
      ))}
    </div>
  );
};

export default Informatique;
