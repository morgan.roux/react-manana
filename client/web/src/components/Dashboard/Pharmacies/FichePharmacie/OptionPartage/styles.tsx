import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      padding: 20,
      justifyContent:'center'
    },
    button: {
      margin: theme.spacing(1),
    },
    titre: {
      textAlign: 'left',
      fontFamily: 'Roboto',
      fontSize: '18',
      fontWeight: 'normal',
      color: '#212121',
    },
    rowBox: {
      borderBottom: 'solid 1px #eeeeee',
      padding: 16,
    },
    textName: {
      textAlign: 'left',
      fontFamily: 'Roboto',
      fontSize: '18',
      fontWeight: 'normal',
      color: '#424242',
    },
    textRadio: {
      textAlign: 'left',
      fontFamily: 'Roboto',
      fontSize: '18',
      fontWeight: 'normal',
      color: '#27272F',
    },
  }),
);

export default useStyles;
