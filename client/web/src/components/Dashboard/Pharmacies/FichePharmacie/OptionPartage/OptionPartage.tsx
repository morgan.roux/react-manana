import React, { FC, useState } from 'react';
import { Box, FormControlLabel, FormLabel, Radio, RadioGroup, Typography } from '@material-ui/core';
import useStyles from './styles';
import { CustomButton } from '@app/ui-kit';
import { useQuery, useLazyQuery } from '@apollo/react-hooks';
import { 
  GET_OPTION_PARTAGES as GET_OPTION_PARTAGES_TYPE,  
  GET_OPTION_PARTAGESVariables
} from '../../../../../federation/ged/option-partage/types/GET_OPTION_PARTAGES';
import { GET_OPTION_PARTAGES } from '../../../../../federation/ged/option-partage/query';
import { FEDERATION_CLIENT } from '../../../DemarcheQualite/apolloClientFederation';
import { PARAMETER, PARAMETERVariables } from '../../../../../graphql/Parametre/types/PARAMETER';
import { GET_PARAMETER } from '../../../../../graphql/Parametre/query';

interface MatriceTachesProps {
  pharmacie: any;
}

const MatriceTaches: FC<MatriceTachesProps> = ({ pharmacie }) => {
  const classes = useStyles();

  const loadOptionPartages = useQuery<
  GET_OPTION_PARTAGES_TYPE,
  GET_OPTION_PARTAGESVariables
>(GET_OPTION_PARTAGES, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
    variables: {
      filter: {
        idPharmacie: { eq: pharmacie.id } 
      }
    },
  });

  const [loadParametre, loadingParametre] = useLazyQuery<
    PARAMETER,
    PARAMETERVariables
  > (GET_PARAMETER, {
    fetchPolicy: 'cache-and-network',
  });

  loadOptionPartages.data?.optionPartages.nodes.map((option) => loadParametre({
    variables : {
      id: option.idParametre,
    }
  }));

  const partagePharmacies = [
    {
      name: 'GED',
      code: 'MON_EQUIPE',
    },
    {
      name: 'Cahier de liaison',
      code: 'TOUT_LE_MONDE',
    }
  ];

  // const getOptionPartage = (idParametre: string) => {
  //   const result = partagePharmacies.filter((option) => option.idParametre === idParametre);
  //   return result ? result[0] : undefined;
  // }

  const partageList = [
    {
      id: '',
      code: 'MON_EQUIPE',
      libelle: 'Mon équipe',
    },
    {
      id: '',
      code: 'TOUT_LE_MONDE',
      libelle: 'Tout le monde',
    }
  ];

  const [partages, setPartages] = useState<any>(partagePharmacies.reduce(((prevPartages, currentPartages) => {
      return {...prevPartages, [currentPartages.name]: currentPartages.code}
  }), {}));

  const handlePartageChange = (name: string, value: string) => {
    setPartages(prev => ({...prev, [name]: value}))
  }

  const handleSubmit = () => {
    console.log('partage code', partages);
  }

  return (
    <Box className={classes.root}>
      <form 
        onSubmit={(event) => {
          event.stopPropagation();
          event.preventDefault();
          handleSubmit();
        }}
      >
        <Box py={1}>
            <Typography className={classes.titre}>Partager avec</Typography>
        </Box>

        <Box py={2}>
          {partagePharmacies.map(partagePharmacie => (
            <Box 
              key={`partage${partagePharmacie.code}`} 
              display="flex" 
              flexDirection="row" 
              alignItems="center"
              className={classes.rowBox}
            >
              <Box flexGrow={1} mr={4}>
              <FormLabel component="legend" className={classes.textName}>{partagePharmacie.name}</FormLabel>
              </Box>
              <RadioGroup row aria-label="gender" name="gender1" value={partages[partagePharmacie.name]} 
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                  handlePartageChange(partagePharmacie.name, (event.target as HTMLInputElement).value);
              }}
              >
                {partageList.map(item => (
                  <FormControlLabel
                    key={`partage${partagePharmacie.code}${item.code}`}
                    value={item.code} control={<Radio />} 
                    label={item.libelle}
                    className={classes.textRadio}
                  />
                ))}
              </RadioGroup>
            </Box>
            ))}
        </Box>
        <Box display="flex" justifyContent="flex-end">
          <CustomButton type="submit" color="secondary">Enregistrer</CustomButton>
        </Box>
      </form>
    </Box>
  );
};

export default MatriceTaches;
