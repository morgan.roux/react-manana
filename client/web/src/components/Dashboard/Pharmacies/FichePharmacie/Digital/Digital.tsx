import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { debounce, head, isEqual, lowerCase, split } from 'lodash';
import React, {
  ChangeEvent,
  Dispatch,
  FC,
  SetStateAction,
  useCallback,
  useMemo,
  useRef,
  useState,
} from 'react';
import { GET_MINIM_PRESTATAIRE_PHARMACIES } from '../../../../../graphql/PrestatairePharmacie';
import { MINIM_PRESTATAIRE_PHARMACIES } from '../../../../../graphql/PrestatairePharmacie/types/MINIM_PRESTATAIRE_PHARMACIES';
import { GET_SERVICE_PHARMACIES } from '../../../../../graphql/ServicePharmacie';
import { SERVICE_PHARMACIES } from '../../../../../graphql/ServicePharmacie/types/SERVICE_PHARMACIES';
import { PharmacieDigitaleInput } from '../../../../../types/graphql-global-types';
import { normalizeString } from '../../../../../utils/Helpers';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { isUrlValid } from '../../../../../utils/Validator';
import Backdrop from '../../../../Common/Backdrop';
import Inputs from '../../../../Common/Inputs/Inputs';
import { PharmacieFormInterface } from '../../PharmacieForm/usePharmacieForm';
import { FichePharmacieChildProps } from '../FichePharmacie';
import FichePharmacieCard from '../FichePharmacieCard';
import useStyles from './styles';
// import { useCommonStyles } from '../styles';

export interface DigitalProps {
  setValues: Dispatch<SetStateAction<PharmacieFormInterface>>;
}

const Digital: FC<FichePharmacieChildProps & DigitalProps> = ({
  // pharmacie,
  // handleChange,
  // handleChangeDate,
  values,
  setValues,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  setDisabledSaveBtn,
  disabledSaveBtn,
  isEdit,
}) => {
  const classes = useStyles({});
  // const commonClasses = useCommonStyles();
  const client = useApolloClient();

  const selectOptionsPartenaire = [
    { id: 'true', value: 'OUI' },
    { id: 'false', value: 'NON' },
  ];

  /**
   * Get prestataires pharmacies
   */
  const { data: prestataireData, loading: prestataireLoading } = useQuery<
    MINIM_PRESTATAIRE_PHARMACIES
  >(GET_MINIM_PRESTATAIRE_PHARMACIES, {
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const prestataireList = (prestataireData && prestataireData.prestatairePharmacies) || [];

  /**
   * Get automate list
   */
  const { data: servicePharmaData, loading: servicePharmaLoading, error } = useQuery<
    SERVICE_PHARMACIES
  >(GET_SERVICE_PHARMACIES, {
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const [inputValues, setInputValues] = useState<any>(null);
  const [inputList, setInputList] = useState<any>(null);

  /**
   * Set inputs default values
   */
  useMemo(() => {
    const servicePharmaList = servicePharmaData && servicePharmaData.servicePharmacies;
    if (servicePharmaList && servicePharmaList.length > 0) {
      const inputsInitValue = servicePharmaList.map(s => {
        if (s && s.nom) {
          const formattedName = normalizeString(lowerCase(s.nom));
          const digitale =
            values.digitales && values.digitales.find(d => d && d.idServicePharmacie === s.id);
          return {
            idDigital: (digitale && digitale.id) || '',
            key: formattedName,
            id: (digitale && digitale.idServicePharmacie) || s.id,
            [`${formattedName}_checkbox`]: digitale && digitale.idServicePharmacie ? true : false,
            [`${formattedName}_DateInstallation`]: (digitale && digitale.dateInstallation) || null,
            [`${formattedName}_Prestataire`]: (digitale && digitale.idPrestataire) || '',
            [`${formattedName}_Partenaire`]:
              digitale && digitale.flagService !== undefined ? `${digitale.flagService}` : '',
            [`${formattedName}_Url`]: (digitale && digitale.url) || '',
          };
        }
      });

      setInputValues(inputsInitValue);
    }
  }, [servicePharmaData]);

  /**
   * Set Input List
   */
  useMemo(() => {
    const servicePharmaList = servicePharmaData && servicePharmaData.servicePharmacies;
    if (
      servicePharmaList &&
      servicePharmaList.length > 0 &&
      inputValues &&
      inputValues.length > 0
    ) {
      const newInputsList = servicePharmaList.map(s => {
        if (s && s.nom) {
          const formattedName = normalizeString(lowerCase(s.nom));
          const initVal = inputValues.find(i => i && i.key === formattedName);
          const checkboxValue = (initVal && initVal[`${formattedName}_checkbox`]) || false;

          const prestataireValue = (initVal && initVal[`${formattedName}_Prestataire`]) || '';

          return [
            {
              label: s.nom,
              name: `${formattedName}_checkbox`,
              value: checkboxValue,
              type: 'checkbox',
              disabled: isEdit,
            },
            {
              label: 'Date installation',
              name: `${formattedName}_DateInstallation`,
              value: (initVal && initVal[`${formattedName}_DateInstallation`]) || null,
              type: 'date',
              disabled: !checkboxValue || isEdit,
            },
            {
              label: 'Prestataire',
              name: `${formattedName}_Prestataire`,
              value: prestataireValue,
              type: 'select',
              selectOptions: prestataireList,
              selectOptionValueKey: 'nom',
              disabled: !checkboxValue || isEdit,
            },
            {
              label: 'Partenaire',
              name: `${formattedName}_Partenaire`,
              value: (initVal && initVal[`${formattedName}_Partenaire`]) || '',
              type: 'select',
              selectOptions: selectOptionsPartenaire,
              disabled: !checkboxValue || isEdit,
            },
            {
              label: 'URL',
              name: `${formattedName}_Url`,
              value: (initVal && initVal[`${formattedName}_Url`]) || '',
              type: 'url',
              disabled: !checkboxValue || isEdit,
            },
          ];
        }
      });
      setInputList(newInputsList);
    }
  }, [servicePharmaData, inputValues, isEdit]);

  // Check URL Validation
  const debouncedInput = useRef(
    debounce((text: string) => {
      if (text.length > 0) {
        if (!isUrlValid(text)) {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: 'URL invalide' });
          if (!disabledSaveBtn) {
            if (setDisabledSaveBtn) setDisabledSaveBtn(true);
          }
        } else {
          if (disabledSaveBtn === true) {
            if (setDisabledSaveBtn) setDisabledSaveBtn(false);
          }
        }
      }
    }, 2000),
  );

  /**
   * Set values
   */
  useMemo(() => {
    if (inputValues && inputValues.length > 0) {
      const newDigitales: any[] = inputValues.map(i => {
        const key = i.key;
        if (i[`${key}_checkbox`]) {
          const item: PharmacieDigitaleInput = {
            id: i.idDigital,
            idServicePharmacie: i.id,
            idPrestataire: i[`${key}_Prestataire`],
            flagService: Boolean(i[`${key}_Partenaire`]),
            dateInstallation: i[`${key}_DateInstallation`],
            url: i[`${key}_Url`],
          };

          // Check URL Validation
          debouncedInput.current(i[`${key}_Url`]);

          return item;
        }
      });

      const newState = {
        ...values,
        digitales: newDigitales ? newDigitales.filter(i => i !== undefined && i !== null) : [],
      };
      if (!isEqual(values, newState)) {
        setValues(newState);
      }
    }
  }, [inputValues]);

  const handleChangeDigitaleInput = useCallback((e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value, checked } = e.target;
      const nameKey = head(split(name, '_'));
      setInputValues(prevState => {
        if (prevState) {
          const newState = prevState.map(i => {
            if (i.key === nameKey) {
              i[name] = name.includes('checkbox') ? checked : value;
            }
            return i;
          });
          return newState;
        }
      });
    }
  }, []);

  const handleChangeDateDigitale = useCallback(
    (name: string) => (date: any) => {
      const nameKey = head(split(name, '_'));
      setInputValues(prevState => {
        if (prevState) {
          const newState = prevState.map(i => {
            if (i.key === nameKey) {
              i[name] = date;
            }
            return i;
          });
          return newState;
        }
      });
    },
    [],
  );

  if (error) return <div>Error</div>;

  return (
    <div className={classes.digitalRoot}>
      {(servicePharmaLoading || prestataireLoading) && <Backdrop />}
      <FichePharmacieCard title="">
        {inputList &&
          inputList.length > 0 &&
          inputList.map((inputs, index) => {
            return (
              <div className={classes.inputsContainer} key={`allInputs_input_${index}`}>
                <Inputs
                  inputs={inputs as any}
                  handleChange={handleChangeDigitaleInput}
                  handleChangeDate={handleChangeDateDigitale}
                  handleChangeAutoComplete={handleChangeAutoComplete}
                  handleChangeInputAutoComplete={handleChangeInputAutoComplete}
                />
              </div>
            );
          })}
      </FichePharmacieCard>
    </div>
  );
};

export default Digital;
