import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    digitalRoot: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
      padding: '25px 0px',
      minHeight: 200,
      '& > div': {
        width: '100%',
      },
      '& .MuiInputBase-root.Mui-disabled': {
        background: '#F2F2F2',
      },
      '& textarea[disabled]': {
        background: '#F2F2F2',
      },
    },
    inputsContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      '&:not(:nth-last-child(1))': {
        marginBottom: 23,
      },
      '& > div': {
        marginBottom: 0,
      },
      '& > div:not(:nth-last-child(1))': {
        marginRight: 25,
        maxWidth: 245,
        '@media (max-width: 1024px)': {
          marginRight: 0,
          maxWidth: '100%',
          marginBottom: 16,
        },
      },

      '& > div:nth-child(1)': {
        '& > label': {
          minWidth: 170,
          marginRight: 0,
        },
      },
      '@media (max-width: 1024px)': {
        flexWrap: 'wrap',
      },
    },
  }),
);

export default useStyles;
