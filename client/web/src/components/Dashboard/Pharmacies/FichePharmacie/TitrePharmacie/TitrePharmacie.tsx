import React, { FC, MouseEvent } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import usesStyles from './styles';
import { Grid, Typography, Link } from '@material-ui/core';
import { PHARMACIE_pharmacie } from '../../../../../graphql/Pharmacie/types/PHARMACIE';
import { TITULAIRE_PHARMACIE_URL } from '../../../../../Constant/url';
import classnames from 'classnames';
import { startCase, lowerCase } from 'lodash';

interface TitrePharmacieProps {
  currentPharmacie: PHARMACIE_pharmacie;
}

const TitrePharmacie: FC<TitrePharmacieProps & RouteComponentProps> = ({
  currentPharmacie,
  history: { push },
}) => {
  const classes = usesStyles();
  const title = (currentPharmacie && currentPharmacie.nom) || 'Pharmacie inconnue';
  const cp = currentPharmacie && currentPharmacie.cp;
  const titulaires = currentPharmacie && currentPharmacie.titulaires;

  const goToTitulaire = (id: string) => (e: MouseEvent<any>) => {
    e.preventDefault();
    push(`/db/${TITULAIRE_PHARMACIE_URL}/fiche/${id}`);
  };

  return (
    <div>
      <Grid className={classes.container}>
        <Typography className={classes.title}>
          {title} {cp ? `- ${cp}` : ''}
        </Typography>
        {titulaires && titulaires.length !== 0 ? (
          <div className={classnames(classes.conteneur, classes.titulaires)}>
            <div>Les Titulaires&nbsp;:&nbsp;</div>
            {titulaires.map(
              (titulaire, index) =>
                titulaire && (
                  <div className={classes.conteneur} key={`titre_pharmacie_titulaire_${index}`}>
                    <Link
                      color="secondary"
                      onClick={goToTitulaire(titulaire.id)}
                      style={{ cursor: 'pointer' }}
                    >
                      {startCase(lowerCase(titulaire.civilite || '')) +
                        '. ' +
                        titulaire.prenom +
                        ' ' +
                        titulaire.nom}
                    </Link>
                    {index !== titulaires.length - 1 ? <span>&nbsp;-&nbsp;</span> : ' '}
                  </div>
                ),
            )}
          </div>
        ) : (
          'Aucun titulaire à afficher'
        )}
      </Grid>
    </div>
  );
};

export default withRouter(TitrePharmacie);
