import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../FichePharmacie';
import AchatCard from './AchatCard';
import useStyles from './styles';
import FichePharmacieCard from '../FichePharmacieCard';
import { InputInterface } from '../../../../Common/Inputs/Inputs';
import { useQuery, useApolloClient } from '@apollo/react-hooks';
import { MINIM_COMMANDE_CANALS } from '../../../../../graphql/CommandeCanal/types/MINIM_COMMANDE_CANALS';
import { GET_MINIM_COMMANDE_CANALS } from '../../../../../graphql/CommandeCanal';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import Backdrop from '../../../../Common/Backdrop';

const Achat: FC<FichePharmacieChildProps> = ({
  pharmacie,
  handleChange,
  handleChangeDate,
  values,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  isEdit,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();

  const { cap: c, achat: a } = values;

  const contractCAP = (c && c.cap) || false;
  const dateDebutCap = (c && c.dateDebut) || null;
  const dateFinCap = (c && c.dateFin) || null;
  const identifiantCap = (c && c.identifiantCap) || '';
  const commentaireCAP = (c && c.commentaire) || '';

  const idCanal = (a && a.idCanal) || '';
  const dateDebutPlateforme = (a && a.dateDebut) || null;
  const dateFinPlateforme = (a && a.dateFin) || null;
  const identifiantAchatCanal = (a && a.identifiantAchatCanal) || '';
  const commentairePlateforme = (a && a.commentaire) || '';

  /**
   * Get canal list
   */
  const { data: canalData, loading: canalLoading } = useQuery<MINIM_COMMANDE_CANALS>(
    GET_MINIM_COMMANDE_CANALS,
    {
      onError: errors => {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );

  const canalList = (canalData && canalData.commandeCanals) || [];

  const inputsCardOne: InputInterface[] = [
    {
      label: 'Contrat CAP',
      name: 'cap.checkbox.cap',
      value: contractCAP,
      type: 'checkbox',
      disabled: isEdit,
    },
    {
      label: 'Date de début CAP',
      name: 'cap.dateDebut',
      value: dateDebutCap,
      type: 'date',
      disabled: isEdit,
    },
    {
      label: 'Date de fin CAP',
      name: 'cap.dateFin',
      value: dateFinCap,
      type: 'date',
      disabled: isEdit,
    },
    {
      label: 'Identifiant CAP',
      name: 'cap.identifiantCap',
      value: identifiantCap,
      type: 'text',
      disabled: isEdit,
    },
    {
      label: 'Commentaire',
      name: 'cap.commentaire',
      value: commentaireCAP,
      type: 'textArea',
      disabled: isEdit,
    },
  ];

  const inputsCardTwo: InputInterface[] = [
    {
      label: 'Canal Plateforme',
      name: 'achat.idCanal',
      value: idCanal,
      type: 'autocomplete',
      selectOptions: canalList,
      selectOptionIdKey: 'id',
      selectOptionValueKey: 'libelle',
      autocompleteValue: canalList.find(i => i && i.id === idCanal),
      disabled: isEdit,
    },
    {
      label: 'Date de début Plateforme',
      name: 'achat.dateDebut',
      value: dateDebutPlateforme,
      type: 'date',
      disabled: isEdit,
    },
    {
      label: 'Date de fin Plateforme',
      name: 'achat.dateFin',
      value: dateFinPlateforme,
      type: 'date',
      disabled: isEdit,
    },
    {
      label: 'Identifiant Plateforme',
      name: 'achat.identifiantAchatCanal',
      value: identifiantAchatCanal,
      type: 'text',
      disabled: isEdit,
    },
    {
      label: 'Commentaire',
      name: 'achat.commentaire',
      value: commentairePlateforme,
      type: 'textArea',
      disabled: isEdit,
    },
  ];

  const cards = [
    {
      title: '',
      children: (
        <AchatCard
          pharmacie={pharmacie}
          inputs={inputsCardOne}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      title: '',
      children: (
        <AchatCard
          pharmacie={pharmacie}
          inputs={inputsCardTwo}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
  ];

  return (
    <div className={classes.achatRoot}>
      {canalLoading && <Backdrop />}
      {cards.map((card, index) => (
        <FichePharmacieCard key={`fiche_pharma_achat_1_${index}`} title={card.title}>
          {card.children}
        </FichePharmacieCard>
      ))}
    </div>
  );
};

export default Achat;
