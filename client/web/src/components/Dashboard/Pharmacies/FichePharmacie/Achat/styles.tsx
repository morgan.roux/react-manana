import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    achatRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      padding: '25px 0px',
      '& > div:not(:nth-last-child(1))': {
        marginBottom: 25,
      },
    },
    inputsContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      '@media (max-width: 992px)': {
        flexWrap: 'wrap',
        justifyContent: 'start',
      },
      '& > div': {
        marginBottom: 24,
      },
      '& > div:nth-child(1)': {
        minWidth: 150,
        '& span.MuiFormControlLabel-label': {
          minWidth: 'fit-content',
        },
      },
      '& > div:nth-child(2), & > div:nth-child(3)': {
        maxWidth: 270,
      },
      '& > div:nth-last-child(1)': {
        width: '100%',
      },
      '& > div:not(:nth-last-child(1))': {
        marginRight: 30,
      },
      '& .MuiInputBase-root.Mui-disabled': {
        background: '#F2F2F2',
      },
      '& textarea[disabled]': {
        background: '#F2F2F2',
      },
    },
  }),
);

export default useStyles;
