import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
import useStyles from '../styles';
import { useCommonStyles } from '../../styles';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import Inputs from '../../../../../Common/Inputs';
import { InputInterface } from '../../../../../Common/Inputs/Inputs';
import classnames from 'classnames';

const MainInfo: FC<FichePharmacieChildProps> = ({
  handleChange,
  handleChangeDate,
  values,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  isEdit,
  // pharmacie,
}) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const { compta: c } = values;

  const siret = (c && c.siret) || '';
  const codeERP = (c && c.codeERP) || '';
  const ape = (c && c.ape) || '';
  const tvaIntra = (c && c.tvaIntra) || '';
  const rcs = (c && c.rcs) || '';
  const contactCompta = (c && c.contactCompta) || '';

  const chequeGrp = (c && c.chequeGrp) || false;
  const valeurChequeGrp = (c && c.valeurChequeGrp) || '';
  const commentaireChequeGrp = (c && c.commentaireChequeGrp) || '';

  const droitAccesGrp = (c && c.droitAccesGrp) || false;
  const valeurChequeAccesGrp = (c && c.valeurChequeAccesGrp) || '';
  const commentaireChequeAccesGrp = (c && c.commentaireChequeAccesGrp) || '';

  const lineOneInputs: InputInterface[] = [
    { label: 'N° SIRET', name: 'compta.siret', value: siret, type: 'text', disabled: isEdit },
    { label: 'N° ERP', name: 'compta.codeERP', value: codeERP, type: 'text', disabled: isEdit },
    { label: 'Code APE', name: 'compta.ape', value: ape, type: 'text', disabled: isEdit },
    {
      label: 'TVA INTRA',
      name: 'compta.tvaIntra',
      value: tvaIntra,
      type: 'text',
      disabled: isEdit,
    },
    { label: 'RCS', name: 'compta.rcs', value: rcs, type: 'text', disabled: isEdit },
    {
      label: 'Recruteur',
      name: 'compta.contactCompta',
      value: contactCompta,
      type: 'text',
      disabled: isEdit,
    },
  ];

  const lineTwoInputs: InputInterface[] = [
    {
      label: 'Le Groupement',
      name: 'compta.checkbox.chequeGrp',
      value: chequeGrp,
      type: 'checkbox',
      disabled: isEdit,
    },
    {
      label: 'Valeur du chèque',
      name: 'compta.valeurChequeGrp',
      value: valeurChequeGrp,
      type: 'text',
      inputType: 'number',
      disabled: isEdit,
    },
    {
      label: 'Commentaire',
      name: 'compta.commentaireChequeGrp',
      value: commentaireChequeGrp,
      type: 'text',
      disabled: isEdit,
    },
  ];

  const lineThreeInputs: InputInterface[] = [
    {
      label: "Droit d'entrée Groupement",
      name: 'compta.checkbox.droitAccesGrp',
      value: droitAccesGrp,
      type: 'checkbox',
      disabled: isEdit,
    },
    {
      label: 'Valeur du chèque',
      name: 'compta.valeurChequeAccesGrp',
      value: valeurChequeAccesGrp,
      type: 'text',
      inputType: 'number',
      disabled: isEdit,
    },
    {
      label: 'Commentaire',
      name: 'compta.commentaireChequeAccesGrp',
      value: commentaireChequeAccesGrp,
      type: 'text',
      disabled: isEdit,
    },
  ];

  return (
    <div className={commonClasses.cardContent}>
      <div className={classes.inputsContainer}>
        {lineOneInputs.map((input, index) => (
          <CustomFormTextField
            key={`fiche_pharma_compta_lineOneInputs_${index}`}
            label={input.label}
            name={input.name}
            value={input.value}
            type={input.inputType || 'text'}
            onChange={handleChange}
            disabled={input.disabled}
          />
        ))}
      </div>
      <div className={classnames(classes.inputsContainer, classes.inputsContainerSecond)}>
        <Inputs
          inputs={lineTwoInputs}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
        />
      </div>
      <div className={classnames(classes.inputsContainer, classes.inputsContainerSecond)}>
        <Inputs
          inputs={lineThreeInputs}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
        />
      </div>
    </div>
  );
};

export default MainInfo;
