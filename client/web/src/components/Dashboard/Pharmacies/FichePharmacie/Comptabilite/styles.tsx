import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    comptabiliteRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      padding: '20px 0px',
      '& > div:nth-child(1)': {
        marginBottom: 25,
        width: '100%',
      },
      '& > div:not(:nth-child(1))': {
        // width: '50%',
      },
      '& > div:nth-child(2)': {
        marginRight: 15,
      },
      '& .MuiInputBase-root.Mui-disabled': {
        background: '#F2F2F2',
      },
      '& textarea[disabled]': {
        background: '#F2F2F2',
      },
    },
    cardsContainers: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      '& .MuiInputBase-root.Mui-disabled': {
        background: '#F2F2F2',
      },
      '& > div:nth-child(1)': {
        marginRight: 15,
        '@media (max-width: 600px)': {
          marginRight: 0,
          marginBottom: 32,
        },
      },
      '& > div': {
        width: '50%',
        '@media (max-width: 600px)': {
          width: '100%',
        },
      },
      '@media (max-width: 600px)': {
        flexWrap: 'wrap',
      },
    },
    infoBancaireTitle: {
      fontSize: '0.875rem',
      opacity: 0.3,
      fontWeight: 500,
    },
    inputsContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginBottom: 20,
      '& > div:not(:nth-last-child(1))': {
        marginRight: 25,
        '@media (max-width: 992px)': {
          marginRight: 0,
        },
      },
      '& > div': {
        marginBottom: 0,
        maxWidth: 282,
        '@media (max-width: 992px)': {
          maxWidth: '45%',
          marginBottom: 16,
        },
      },
      '@media (max-width: 992px)': {
        flexWrap: 'wrap',
      },
    },
    inputsContainerInfoJuridique: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginBottom: 20,
      '& > div:not(:nth-last-child(1))': {
        marginRight: 25,
        '@media (max-width: 1199px)': {
          marginRight: 0,
        },
      },
      '& > div': {
        marginBottom: 0,
        maxWidth: 282,
        '@media (max-width: 1199px)': {
          marginBottom: 16,
          maxWidth: '70%',
        },
        '@media (max-width: 992px)': {
          maxWidth: '100%',
        },
      },
      '@media (max-width: 1199px)': {
        flexWrap: 'wrap',
      },
    },
    inputsContainerInfoBancaire: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginBottom: 20,
      '& > div:not(:nth-last-child(1))': {
        marginRight: 25,
        '@media (max-width: 1199px)': {
          marginRight: 0,
        },
      },
      '& > div': {
        marginBottom: 0,
        maxWidth: 282,
        '@media (max-width: 1199px)': {
          marginBottom: 16,
          maxWidth: '70%',
        },
        '@media (max-width: 992px)': {
          maxWidth: '100%',
        },
      },
      '@media (max-width: 1199px)': {
        flexWrap: 'wrap',
      },
    },
    infoContainerBancaire: {
      marginBottom: 24,
    },
    inputsContainerSecond: {
      '& > div:not(:nth-last-child(1))': {
        minWidth: 282,
        maxWidth: 282,
        '@media (max-width: 992px)': {
          maxWidth: '100%',
          minWidth: '100%',
        },
      },
      '& > div': {
        maxWidth: 'inherit',
      },
    },
    grossisteContainer: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'flex-start',
      '& > p': {
        marginBottom: 14,
      },
    },
    marginTitle: {
      marginTop: 14,
    },
    infoBancaireInputs: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      '& > div': {
        marginBottom: 0,
      },
      '& > div:not(:nth-last-child(1))': {
        marginRight: 15,
        '@media (max-width: 992px)': {
          marginRight: 0,
        },
      },
      '@media (max-width: 1485px)': {
        flexWrap: 'wrap',
      },
    },
    grossisteInputsContainer: {
      '& > div': {
        width: 85,
        '@media (max-width: 1485px)': {
          width: '22%',
          marginBottom: 16,
        },
        '@media (max-width: 1324px)': {
          width: '28%',
        },
        '@media (max-width: 992px)': {
          width: '100%',
        },
      },
      '& > div:nth-child(3)': {
        width: 143,
        '@media (max-width: 992px)': {
          width: '100%',
        },
      },
      '& > div:nth-last-child(1)': {
        width: 415,
        '@media (max-width: 992px)': {
          width: '100%',
        },
      },
    },
    ibanInputsContainer: {
      '& > div': {
        maxWidth: 85,
        '@media (max-width: 1485px)': {
          maxWidth: '26%',
          marginBottom: 16,
          '@media (max-width: 675px)': {
            maxWidth: '45%',
          },
        },
      },
    },
    ibanAndSwifContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      '& > div:nth-last-child(1)': {
        marginLeft: 15,
        '@media (max-width: 1080px)': {
          marginLeft: 0,
        },
      },
      '@media (max-width: 1080px)': {
        flexWrap: 'wrap',
      },
    },
    swiftInput: {
      minWidth: 160,
      '& > div': { marginBottom: 0 },
      '@media (max-width: 1024px)': {
        minWidth: '100%',
      },
    },
    modifStatusContainer: {
      '& > label': {
        minWidth: 125,
      },
    },
  }),
);

export default useStyles;
