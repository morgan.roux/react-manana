import React, { FC, useState, ChangeEvent, useEffect, createRef } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
import useStyles from '../styles';
import { useCommonStyles } from '../../styles';
import { Typography } from '@material-ui/core';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import classnames from 'classnames';
import Inputs, { InputInterface } from '../../../../../Common/Inputs/Inputs';
import { ComptabiliteProps } from '../Comptabilite';
import { formatIBAN } from '../../../../../../utils/Helpers';
import { split, last } from 'lodash';

export interface SeparatedIban {
  part0: string;
  part1: string;
  part2: string;
  part3: string;
  part4: string;
  part5: string;
  part6: string;
}

const InfoBancaire: FC<FichePharmacieChildProps & ComptabiliteProps> = ({
  handleChange,
  handleChangeDate,
  values,
  setValues,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  isEdit,
  // pharmacie,
}) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const { compta: c } = values;

  const siret = (c && c.siret) || '';
  const codeERP = (c && c.codeERP) || '';
  const dateMajRib = (c && c.dateMajRib) || null;

  const nomBanque = (c && c.nomBanque) || '';
  const banqueGuichet = (c && c.banqueGuichet) || '';
  const banqueCompte = (c && c.banqueCompte) || '';
  const banqueRib = (c && c.banqueRib) || '';

  const domiciliation = (c && c.banqueCle) || '';

  const iban = (c && c.iban) || '';

  const ibanArray = formatIBAN(iban).split(' ');

  const defaultSeparatedIban: SeparatedIban = {
    part0: ibanArray[0] || '',
    part1: ibanArray[1] || '',
    part2: ibanArray[2] || '',
    part3: ibanArray[3] || '',
    part4: ibanArray[4] || '',
    part5: ibanArray[5] || '',
    part6: ibanArray[6] || '',
  };

  const [separatedIban, setSeparatedIban] = useState(defaultSeparatedIban);
  const { part0, part1, part2, part3, part4, part5, part6 } = separatedIban;

  const swift = (c && c.swift) || '';

  const lineOneInputs: InputInterface[] = [
    { label: 'N° SIRET', name: 'compta.siret', value: siret, type: 'text', disabled: isEdit },
    { label: 'N° ERP', name: 'compta.codeERP', value: codeERP, type: 'text', disabled: isEdit },
    { label: 'Date', name: 'compta.dateMajRib', value: dateMajRib, type: 'date', disabled: isEdit },
  ];

  const grossisteInputs: InputInterface[] = [
    { label: 'Banque', name: 'compta.nomBanque', value: nomBanque, type: 'text', disabled: isEdit },
    {
      label: 'Guichet',
      name: 'compta.banqueGuichet',
      value: banqueGuichet,
      type: 'text',
      disabled: isEdit,
    },
    {
      label: 'N° de compte',
      name: 'compta.banqueCompte',
      value: banqueCompte,
      type: 'text',
      inputType: 'number',
      disabled: isEdit,
    },
    {
      label: 'Clé RIB',
      name: 'compta.banqueRib',
      value: banqueRib,
      type: 'text',
      disabled: isEdit,
    },
    {
      label: 'Domiciliation',
      name: 'compta.banqueCle',
      value: domiciliation,
      type: 'text',
      disabled: isEdit,
    },
  ];

  const ibanInputs: any[] = [
    { id: 0, label: '', name: 'part0', value: part0, type: 'text', maxLength: 4, disabled: isEdit },
    { id: 1, label: '', name: 'part1', value: part1, type: 'text', maxLength: 4, disabled: isEdit },
    { id: 2, label: '', name: 'part2', value: part2, type: 'text', maxLength: 4, disabled: isEdit },
    { id: 3, label: '', name: 'part3', value: part3, type: 'text', maxLength: 4, disabled: isEdit },
    { id: 4, label: '', name: 'part4', value: part4, type: 'text', maxLength: 4, disabled: isEdit },
    { id: 5, label: '', name: 'part5', value: part5, type: 'text', maxLength: 4, disabled: isEdit },
    { id: 6, label: '', name: 'part6', value: part6, type: 'text', maxLength: 3, disabled: isEdit },
  ];

  // Create input refs
  const ibanInputsRefs = ibanInputs.reduce((acc, value) => {
    acc[value.id] = createRef<HTMLInputElement>();
    return acc;
  }, [] as Array<React.RefObject<HTMLInputElement>>);

  // TODO : Dernière modification

  const handleChangeIban = (input: any) => (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      if (value.length <= input.maxLength) {
        setSeparatedIban(prevState => ({ ...prevState, [name]: value }));
      }

      // Focus next input
      if (value.length === input.maxLength) {
        const refIndex = Number(last(split(name, '')));
        const nextInput = ibanInputsRefs[refIndex + 1];
        if (nextInput && nextInput.current) {
          nextInput.current.focus();
        }
      }
    }
  };

  // Set final iban
  useEffect(() => {
    const finalIban = `${part0}${part1}${part2}${part3}${part4}${part5}${part6}`;
    setValues(prevState => ({ ...prevState, compta: { ...prevState.compta, iban: finalIban } }));
  }, [separatedIban]);

  return (
    <div className={commonClasses.cardContent}>
      <div className={classes.infoContainerBancaire}>
        <Typography className={classes.infoBancaireTitle}>
          Dernière modification faite par Nadia MAHIOU le 23/01/2014.
        </Typography>
      </div>
      <div className={classes.inputsContainerInfoBancaire}>
        <Inputs
          inputs={lineOneInputs}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
        />
      </div>
      <div className={classes.grossisteContainer}>
        <Typography className={commonClasses.employeesInfos}>Grossiste</Typography>
        <div className={classnames(classes.infoBancaireInputs, classes.grossisteInputsContainer)}>
          <Inputs
            inputs={grossisteInputs}
            handleChange={handleChange}
            handleChangeDate={handleChangeDate}
            handleChangeAutoComplete={handleChangeAutoComplete}
            handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          />
        </div>
      </div>
      <div className={classes.ibanAndSwifContainer}>
        <div className={classes.grossisteContainer}>
          <Typography className={classnames(commonClasses.employeesInfos, classes.marginTitle)}>
            IBAN
          </Typography>
          <div className={classnames(classes.infoBancaireInputs, classes.ibanInputsContainer)}>
            {ibanInputs.map((input, index) => (
              <CustomFormTextField
                key={`input_CustomFormTextField_iban${index}`}
                label={input.label}
                name={input.name}
                value={input.value}
                disabled={input.disabled}
                type={input.inputType || 'text'}
                onChange={handleChangeIban(input)}
                inputRef={ibanInputsRefs[index]}
                InputProps={{ inputProps: { maxLength: input.maxLength } }}
              />
            ))}
          </div>
        </div>
        <div className={classnames(classes.grossisteContainer, classes.swiftInput)}>
          <Typography className={classnames(commonClasses.employeesInfos, classes.marginTitle)}>
            SWIFT
          </Typography>
          <CustomFormTextField label="" name="compta.swift" value={swift} onChange={handleChange} />
        </div>
      </div>
    </div>
  );
};

export default InfoBancaire;
