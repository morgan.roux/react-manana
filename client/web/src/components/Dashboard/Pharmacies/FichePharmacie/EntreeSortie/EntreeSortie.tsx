import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../FichePharmacie';
import Feedback from './Feedback';
import SortieFuture from './SortieFuture';
import useStyles from './styles';
import FichePharmacieCard from '../FichePharmacieCard';
import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { CONCURRENTS_MINIM } from '../../../../../graphql/Concurrent/types/CONCURRENTS_MINIM';
import { GET_CONCURRENTS_MINIM } from '../../../../../graphql/Concurrent';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import Backdrop from '../../../../Common/Backdrop';
import {
  MOTIFS_BY_TYPE,
  MOTIFS_BY_TYPEVariables,
} from '../../../../../graphql/Motif/types/MOTIFS_BY_TYPE';
import { GET_MOTIFS_BY_TYPE } from '../../../../../graphql/Motif';
import { TypeMotif } from '../../../../../types/graphql-global-types';

export interface EntreeSortieChildProps {
  motifSortieList: any[];
  concurrentList: any[];
}

const EntreeSortie: FC<FichePharmacieChildProps> = ({
  pharmacie,
  handleChange,
  handleChangeDate,
  values,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  isEdit,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();

  /**
   * Get Concurrent list
   */
  const { data: concurrentData, loading: concurrentLoading } = useQuery<CONCURRENTS_MINIM>(
    GET_CONCURRENTS_MINIM,
    {
      onError: errors => {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );

  /**
   * Get motif sortie list
   */
  const { data: sMotifData, loading: sMotifLoading } = useQuery<
    MOTIFS_BY_TYPE,
    MOTIFS_BY_TYPEVariables
  >(GET_MOTIFS_BY_TYPE, {
    variables: { type: TypeMotif.S },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const concurrentList = (concurrentData && concurrentData.concurrents) || [];
  const motifSortieList = (sMotifData && sMotifData.motifs) || [];

  const cards = [
    {
      title: 'Feedback',
      children: (
        <Feedback
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          motifSortieList={motifSortieList}
          concurrentList={concurrentList}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      title: 'Sortie Future',
      children: (
        <SortieFuture
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          motifSortieList={motifSortieList}
          concurrentList={concurrentList}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
  ];

  return (
    <div className={classes.entreeSortieRoot}>
      {(concurrentLoading || sMotifLoading) && <Backdrop />}
      {cards.map((card, index) => (
        <FichePharmacieCard key={`fiche_pharma_entree_sortie_1_${index}`} title={card.title}>
          {card.children}
        </FichePharmacieCard>
      ))}
    </div>
  );
};

export default EntreeSortie;
