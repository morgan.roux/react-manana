import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
// import useStyles from '../styles';
import { Typography } from '@material-ui/core';
import classnames from 'classnames';
import { useCommonStyles } from '../../styles';
import { noValueText } from '../../../../../../Constant/text';

const InformationsRegion: FC<FichePharmacieChildProps> = ({
  pharmacie,
  // handleChange,
  // handleChangeDate,
  // values,
  // handleChangeAutoComplete,
  // handleChangeInputAutoComplete,
}) => {
  // const classes = useStyles({});
  const commonClasses = useCommonStyles();
  const p = pharmacie;

  const departement = (p && p.departement && p.departement.nom) || noValueText;
  const region =
    (p && p.departement && p.departement.region && p.departement.region.nom) || noValueText;
  // TODO : grandeRegion
  const grandeRegion = noValueText;
  const pdr =
    (p &&
      p.presidentRegion &&
      p.presidentRegion.titulaire &&
      p.presidentRegion.titulaire.fullName) ||
    noValueText;
  const respCommercial = noValueText;
  const commercial = noValueText;
  const formateur = noValueText;
  const merch = noValueText;
  const sav = noValueText;

  const infosRegion = [
    { label: 'Département', value: departement },
    { label: 'Région', value: region },
    { label: 'Grande Région', value: grandeRegion },
    { label: 'Président de région', value: pdr },
    { label: 'Resp. Commercial', value: respCommercial },
    { label: 'Commercial', value: commercial },
    { label: 'Formateur', value: formateur },
    { label: 'Merch', value: merch },
    { label: 'SAV', value: sav },
  ];

  return (
    <div className={commonClasses.cardContent}>
      {infosRegion.map((item, index) => (
        <div
          className={classnames(commonClasses.infoContainer, commonClasses.mb12)}
          key={`infosRegion_${index}`}
        >
          <Typography className={commonClasses.infoTitle}>{item.label}</Typography>
          <Typography className={commonClasses.infoValue}>{item.value}</Typography>
        </div>
      ))}
    </div>
  );
};

export default InformationsRegion;
