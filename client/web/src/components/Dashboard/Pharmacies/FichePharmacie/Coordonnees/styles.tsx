import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const usesStyles = makeStyles((theme: Theme) =>
  createStyles({
    coordonneesRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      paddingBottom: 23,
    },
    coordonneeCardsContainer: {
      display: 'flex',
      flexDirection: 'row',
      // flexWrap: 'wrap',
      justifyContent: 'space-between',
      '& > div': {
        marginTop: 23,
      },
      '& > div:not(:nth-last-child(1))': {
        marginRight: 24,
        '@media (max-width: 1500px)': {
          marginRight: 12,
        },
        '@media (max-width: 1300px)': {
          marginRight: 24,
        },
      },
      '& .MuiInputBase-root.Mui-disabled': {
        background: '#F2F2F2',
      },
      '& textarea[disabled]': {
        background: '#F2F2F2',
      },
      '@media (max-width: 1300px)': {
        justifyContent: 'start',
        flexWrap: 'wrap',
      },
    },
    coordonneeCardsContainerFirst: {
      '& > div:not(:nth-last-child(1))': {
        width: '20%',
        '@media (max-width: 1500px)': {
          marginRight: 12,
        },
        '@media (max-width: 1300px)': {
          marginRight: 24,
          width: '30%',
          minWidth: 258,
        },
        '@media (max-width: 756px)': {
          width: '100%',
          marginRight: 0,
        },
      },
      '& > div:nth-last-child(-n+1)': {
        width: '40%',
        '@media (max-width: 1500px)': {
          marginRight: 12,
        },
        '@media (max-width: 1300px)': {
          marginRight: 24,
          minWidth: 510,
        },
        '@media (max-width: 900px)': {
          marginRight: 24,
          minWidth: 474,
        },
        '@media (max-width: 756px)': {
          width: '100%',
          marginRight: 0,
        },
        '@media (max-width: 599px)': {
          minWidth: '100%',
        },
      },
      '& span': {
        marginBottom: 20,
      },
      '& span.MuiIconButton-label': {
        marginBottom: 0,
      },
    },
    coordonneeCardsContainerSecond: {
      '& > div:not(:nth-last-child(1))': {
        marginRight: 24,
        width: '30%',
        '@media (max-width: 1500px)': {
          width: '28%',
          marginRight: 12,
        },
        '@media (max-width: 1300px)': {
          width: '31%',
          marginRight: 24,
          minWidth: 328,
        },
        '@media (max-width: 756px)': {
          width: '100%',
          marginRight: 0,
        },
      },
      '& span': {
        marginBottom: 12,
      },
      '& span.MuiIconButton-label': {
        marginBottom: 0,
      },
      '& > div:nth-last-child(-n+3)': {
        width: '23%',
        '@media (max-width: 1500px)': {
          width: '24%',
        },
        '@media (max-width: 1300px)': {
          width: '31%',
        },
        '@media (max-width: 1200px)': {
          width: '45%',
        },
        '@media (max-width: 756px)': {
          width: '100%',
        },
      },
    },
    addressCardContent: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      '& > div:nth-child(1)': {
        marginRight: 15,
        '@media (max-width: 599px)': {
          marginRight: 0,
        },
      },
      '@media (max-width: 599px)': {
        flexWrap: 'wrap',
      },
    },
    mapAdressContainer: {
      overflow: 'hidden',
      maxWidth: 330,
      width: '100%',
      '@media (max-width: 599px)': {
        height: 250,
        marginTop: 16,
        maxWidth: '100%',
      },
    },
    gridFormContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      '& > div:nth-child(1)': {
        marginRight: 23,
      },
      '@media (max-width: 1500px)': {
        flexDirection: 'column',
      },
    },
    twoVerticalInputContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      '& > p': {
        marginBottom: 5,
      },
      '& > div': {
        margin: 0,
      },
      '& > div:not(:nth-last-child(1))': {
        marginBottom: 10,
      },
    },
    horaireRow: {
      display: 'flex',
      flexDirection: 'row',
      marginBottom: 16,
      '& > p, & > hr': {
        marginRight: 17,
      },
      '& > p:nth-last-child(1)': {
        marginRight: 0,
      },
      '& .MuiDivider-vertical': {
        height: 19,
      },
      '&:nth-last-child(1)': {
        '& > hr': {
          display: 'none',
        },
      },
    },
    horaireDay: {
      fontSize: 12,
      fontWeight: 'normal',
      letterSpacing: 0.47,
      color: theme.palette.secondary.main,
    },
    horaireValue: {
      fontSize: 14,
      fontWeight: 'normal',
      letterSpacing: 0.28,
      fontFamily: 'Montserrat',
    },
  }),
);

export default usesStyles;
