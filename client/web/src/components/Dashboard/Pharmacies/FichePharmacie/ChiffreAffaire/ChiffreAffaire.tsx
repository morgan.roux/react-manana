import React, { FC, useCallback, useMemo, Dispatch, SetStateAction } from 'react';
import { FichePharmacieChildProps } from '../FichePharmacie';
import useStyles from './styles';
import classnames from 'classnames';
import FichePharmacieCard from '../FichePharmacieCard';
import { InputInterface } from '../../../../Common/Inputs/Inputs';
import CustomSelect from '../../../../Common/CustomSelect';
import { useCommonStyles } from '../styles';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import { Typography } from '@material-ui/core';
import { years } from '../../../../../utils/Helpers';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { useApolloClient } from '@apollo/react-hooks';
import { PharmacieFormInterface } from '../../PharmacieForm/usePharmacieForm';

export interface ChiffreAffaireProps {
  setValues: Dispatch<SetStateAction<PharmacieFormInterface>>;
}

const ChiffreAffaire: FC<FichePharmacieChildProps & ChiffreAffaireProps> = ({
  handleChange,
  values,
  disabledSaveBtn,
  setDisabledSaveBtn,
  setValues,
  isEdit,
  // pharmacie,
  // handleChangeDate,
  // handleChangeAutoComplete,
  // handleChangeInputAutoComplete,
}) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();
  const client = useApolloClient();
  const { chiffreAffaire: c } = values;

  const exercice = (c && c.exercice) || '';

  const startYear = new Date().getFullYear() - 5;
  const yearsList = years(startYear);
  const exerciceList = yearsList.map(y => ({ id: y, value: y }));

  const caTTC = (c && c.caTTC) || '';
  const caHt = (c && c.caHt) || '';

  const caTVA1 = (c && c.caTVA1) || '';
  const caTVA2 = (c && c.caTVA2) || '';
  const caTVA3 = (c && c.caTVA3) || '';
  const caTVA4 = (c && c.caTVA4) || '';

  const disabledCAInputs = (): boolean => {
    if (!exercice) return true;
    return false;
  };

  const inputsOne: InputInterface[] = [
    {
      label: 'CA TTC',
      name: 'chiffreAffaire.caTTC',
      value: caTTC,
      type: 'text',
      inputType: 'number',
      disabled: disabledCAInputs(),
    },
    {
      label: 'CA HT',
      name: 'chiffreAffaire.caHt',
      value: caHt,
      type: 'text',
      inputType: 'number',
      disabled: disabledCAInputs(),
    },
  ];

  const inputsTwo: InputInterface[] = [
    {
      label: 'TVA 2.10% (en €)',
      name: 'chiffreAffaire.caTVA1',
      value: caTVA1,
      type: 'text',
      inputType: 'number',
      disabled: disabledCAInputs(),
    },
    {
      label: 'TVA 5.5% (en €)',
      name: 'chiffreAffaire.caTVA2',
      value: caTVA2,
      type: 'text',
      inputType: 'number',
      disabled: disabledCAInputs(),
    },
    {
      label: 'TVA 10.00% (en €)',
      name: 'chiffreAffaire.caTVA3',
      value: caTVA3,
      type: 'text',
      inputType: 'number',
      disabled: disabledCAInputs(),
    },
    {
      label: 'TVA 20.00% (en €)',
      name: 'chiffreAffaire.caTVA4',
      value: caTVA4,
      type: 'text',
      inputType: 'number',
      disabled: disabledCAInputs(),
    },
  ];

  /**
   * Check caHt value
   */
  useMemo(() => {
    const cumulCaTVA = Number(caTVA1) + Number(caTVA2) + Number(caTVA3) + Number(caTVA4);
    if (cumulCaTVA !== Number(caHt)) {
      if (!disabledSaveBtn) {
        if (setDisabledSaveBtn) setDisabledSaveBtn(true);
        displaySnackBar(client, {
          isOpen: true,
          type: 'ERROR',
          message: 'CA HT doit être égal à la TVA cumulée',
        });
      }

      // Set CA HT
      if (caTVA1 && caTVA2 && caTVA3 && caTVA4) {
        setValues(prevState => ({
          ...prevState,
          chiffreAffaire: { ...prevState.chiffreAffaire, caHt: cumulCaTVA },
        }));
      }
    } else {
      if (disabledSaveBtn) {
        if (setDisabledSaveBtn) setDisabledSaveBtn(false);
      }
    }
  }, [caHt, caTVA1, caTVA2, caTVA3, caTVA4]);

  const inputsWithLeftLabel = useCallback((inputs: InputInterface[]) => {
    return (
      <div className={commonClasses.inputsWithLeftLabelContainer}>
        {inputs.map((input, index) => (
          <div key={`InputsWithLeftLabel_inputs_${index}`} className={classes.formRowContainer}>
            <div className={commonClasses.infoContainer}>
              <Typography className={commonClasses.employeesInfos}>{input.label}</Typography>
            </div>
            <CustomFormTextField
              label=""
              name={input.name}
              value={input.value}
              type={input.inputType}
              onChange={handleChange}
              disabled={input.disabled}
              inputProps={{ min: '0' }}
            />
          </div>
        ))}
      </div>
    );
  }, []);

  return (
    <div className={classes.chiffreAffaireRoot}>
      <FichePharmacieCard title="">
        <div className={classes.inputsContainer}>
          <CustomSelect
            listId="id"
            index="value"
            list={exerciceList.reverse()}
            name="chiffreAffaire.exercice"
            label="Exercice"
            value={exercice}
            onChange={handleChange}
            disabled={isEdit}
          />
          {inputsWithLeftLabel(inputsOne)}
          <div className={commonClasses.infoContainer}>
            <Typography
              className={classnames(classes.margeVentillation, commonClasses.employeesInfos)}
            >
              Ventillation CA par taux de TVA
            </Typography>
          </div>
          {inputsWithLeftLabel(inputsTwo)}
        </div>
      </FichePharmacieCard>
    </div>
  );
};

export default ChiffreAffaire;
