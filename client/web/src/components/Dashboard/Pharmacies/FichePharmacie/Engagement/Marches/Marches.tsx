import React, { FC, useState } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
// import useStyles from '../styles';
import { useCommonStyles } from '../../styles';
import CustomTable, {
  CustomTableColumn,
} from '../../../../../Main/Content/Pilotage/Dashboard/Table/CustomTable';
import { IconButton } from '@material-ui/core';
import { Visibility } from '@material-ui/icons';

const Marches: FC<FichePharmacieChildProps> = ({}) => {
  // const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(5);
  const [take, setTake] = useState<number>(5);
  const [skip, setSkip] = useState<number>(0);

  const data: any[] = [
    {
      nom: 'Marché NUTRI 2018',
      palier: '300 à 599',
      remiseSurFacture: '5,00',
      remiseSupplémentaire: '5,00',
      dateEngagement: '05/03/2020',
      datePriseEnCharge: '05/03/2020',
    },
    {
      nom: 'Marché NUTRI 2018',
      palier: '300 à 599',
      remiseSurFacture: '5,00',
      remiseSupplémentaire: '5,00',
      dateEngagement: '05/03/2020',
      datePriseEnCharge: '05/03/2020',
    },
  ];

  const columns: CustomTableColumn[] = [
    {
      label: 'Nom',
      key: 'nom',
      disablePadding: false,
      numeric: false,
    },
    {
      label: 'Palier',
      key: 'palier',
      disablePadding: false,
      numeric: true,
    },
    {
      label: 'Remise sur Facture (%)',
      key: 'remiseSurFacture',
      disablePadding: false,
      numeric: true,
    },
    {
      label: 'Remise supplémentaire (%)',
      key: 'remiseSupplémentaire',
      disablePadding: false,
      numeric: true,
    },
    {
      label: 'Date Engagement',
      key: 'dateEngagement',
      disablePadding: false,
      numeric: true,
    },
    {
      label: 'Date Prise en charge',
      key: 'datePriseEnCharge',
      disablePadding: false,
      numeric: true,
    },
    {
      label: '',
      key: '',
      sortable: false,
      disablePadding: false,
      numeric: true,
      renderer: (row: any) => {
        return (
          <IconButton>
            <Visibility />
          </IconButton>
        );
      },
    },
  ];

  return (
    <div className={commonClasses.cardContent}>
      <CustomTable
        columns={columns}
        showToolbar={false}
        selectable={false}
        data={data}
        rowCount={data.length}
        total={data.length}
        page={page}
        rowsPerPage={rowsPerPage}
        take={take}
        skip={skip}
        setTake={setTake}
        setSkip={setSkip}
        setPage={setPage}
        setRowsPerPage={setRowsPerPage}
      />
    </div>
  );
};

export default Marches;
