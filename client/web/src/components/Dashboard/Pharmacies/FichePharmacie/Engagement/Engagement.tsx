import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../FichePharmacie';
import Marches from './Marches';
import Engagements from './Engagements';
import useStyles from './styles';
import FichePharmacieCard from '../FichePharmacieCard';
import { v4 as uuidv4 } from 'uuid';
import Commentaire from './Commentaire';

const Engagement: FC<FichePharmacieChildProps> = ({
  pharmacie,
  handleChange,
  handleChangeDate,
  values,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  isEdit,
}) => {
  const classes = useStyles({});

  const cards = [
    {
      title: 'Marchés',
      children: (
        <Marches
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      title: 'Engagements',
      children: (
        <Engagements
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
  ];

  return (
    <div className={classes.engagementRoot}>
      <div className={classes.tablesContainer}>
        {cards.map(card => (
          <FichePharmacieCard key={uuidv4()} title={card.title}>
            {card.children}
          </FichePharmacieCard>
        ))}
      </div>
      <FichePharmacieCard title="">
        <Commentaire
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      </FichePharmacieCard>
    </div>
  );
};

export default Engagement;
