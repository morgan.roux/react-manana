import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
// import useStyles from '../styles';
import { useCommonStyles } from '../../styles';
import Inputs from '../../../../../Common/Inputs';
import classnames from 'classnames';
import { InputInterface } from '../../../../../Common/Inputs/Inputs';

const Facade: FC<FichePharmacieChildProps> = ({
  // pharmacie,
  handleChange,
  handleChangeDate,
  values,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  isEdit,
}) => {
  // const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const { concept: c } = values;

  const avecFacade = (c && c.avecFacade) || '';
  const idEnseigniste = (c && c.idEnseigniste) || '';
  const dateInstallFacade = (c && c.dateInstallFacade) || null;
  const conformiteFacade = (c && c.conformiteFacade) || false;

  // TODO Modele facade
  const modelFacade = '';

  const inputs: InputInterface[] = [
    {
      name: 'concept.checkbox.avecFacade',
      label: 'Façade',
      value: avecFacade,
      type: 'checkbox',
      disabled: isEdit,
    },
    {
      name: 'concept.idEnseigniste',
      label: 'Enseigniste',
      value: idEnseigniste,
      type: 'text',
      disabled: isEdit,
    },
    {
      name: 'concept.dateInstallFacade',
      label: 'Date installation',
      value: dateInstallFacade,
      type: 'date',
      disabled: isEdit,
    },
    {
      name: 'concept.checkbox.conformiteFacade',
      label: 'Conformité façade',
      value: conformiteFacade,
      type: 'checkbox',
      disabled: isEdit,
    },
    {
      name: 'concept.modelFacade',
      label: 'Modèle façade',
      value: modelFacade,
      type: 'text',
      disabled: isEdit,
    },
  ];

  return (
    <div className={classnames(commonClasses.cardContent, commonClasses.contentInputTextfield)}>
      <Inputs
        inputs={inputs}
        handleChange={handleChange}
        handleChangeDate={handleChangeDate}
        handleChangeAutoComplete={handleChangeAutoComplete}
        handleChangeInputAutoComplete={handleChangeInputAutoComplete}
      />
    </div>
  );
};

export default Facade;
