import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
// import useStyles from '../styles';
import { useCommonStyles } from '../../styles';
import classnames from 'classnames';
import Inputs, { InputInterface } from '../../../../../Common/Inputs/Inputs';
import { Typography } from '@material-ui/core';

const Pharmacie: FC<FichePharmacieChildProps> = ({
  pharmacie,
  handleChange,
  handleChangeDate,
  values,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  isEdit,
}) => {
  // const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const { concept: c } = values;

  const surfaceTotale = (c && c.surfaceTotale) || 0;
  const surfaceVente = (c && c.surfaceVente) || 0;
  const SurfaceVitrine = (c && c.SurfaceVitrine) || 0;
  const volumeLeaflet = (c && c.volumeLeaflet) || 0;
  const nbreLineaire = (c && c.nbreLineaire) || 0;
  const nbreVitrine = (c && c.nbreVitrine) || 0;

  const otcLibAcces = (c && c.otcLibAcces) || false;
  const commentaire = (c && c.commentaire) || '';

  const infos = [
    {
      name: 'concept.surfaceTotale',
      label: 'Surface totale',
      value: surfaceTotale,
      disabled: isEdit,
    },
    {
      name: 'concept.surfaceVente',
      label: 'Surface de vente',
      value: surfaceVente,
      disabled: isEdit,
    },
    {
      name: 'concept.SurfaceVitrine',
      label: 'Surface vitrine',
      value: SurfaceVitrine,
      disabled: isEdit,
    },
    {
      name: 'concept.volumeLeaflet',
      label: 'Volume Leaflet',
      value: volumeLeaflet,
      disabled: isEdit,
    },
    {
      name: 'concept.nbreLineaire',
      label: 'Nombre de linéaire',
      value: nbreLineaire,
      disabled: isEdit,
    },
    {
      name: 'concept.nbreVitrine',
      label: 'Nombre de vitrine',
      value: nbreVitrine,
      disabled: isEdit,
    },
  ];

  const inputs: InputInterface[] = [
    {
      name: 'concept.checkbox.otcLibAcces',
      label: 'OTC en libre accès',
      value: otcLibAcces,
      type: 'checkbox',
      disabled: isEdit,
    },
    {
      name: 'concept.commentaire',
      label: 'Commentaire',
      value: commentaire,
      type: 'textArea',
      textAreaRow: 4,
      disabled: isEdit,
    },
  ];

  return (
    <div className={classnames(commonClasses.cardContent)}>
      <div
        className={classnames(
          commonClasses.infoContainer,
          commonClasses.espacePharmacieInfoContainer,
        )}
      >
        {infos.map((item, index) => (
          <Typography key={`infos_inputs_${index}`} className={commonClasses.employeesInfos}>
            {item.label}
            <input
              type="number"
              name={item.name}
              value={item.value}
              onChange={handleChange}
              style={{ width: item.value === 0 ? 55 : item.value.toString().length * 14 }}
              disabled={isEdit}
            />
          </Typography>
        ))}
      </div>

      <div className={commonClasses.contentInputTextfield}>
        <Inputs
          inputs={inputs}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
        />
      </div>
    </div>
  );
};

export default Pharmacie;
