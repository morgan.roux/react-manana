import React, { FC, ChangeEvent, Fragment } from 'react';
import useStyles from './styles';
import { PharmacieFormInterface } from './usePharmacieForm';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Typography } from '@material-ui/core';
import CustomSelect from '../../../Common/CustomSelect';
import { CustomFormTextField } from '../../../Common/CustomTextField';
import { CustomDatePicker } from '../../../Common/CustomDateTimePicker';
import { useQuery, useApolloClient } from '@apollo/react-hooks';
import {
  MOTIFS_BY_TYPE,
  MOTIFS_BY_TYPEVariables,
} from '../../../../graphql/Motif/types/MOTIFS_BY_TYPE';
import { GET_MOTIFS_BY_TYPE } from '../../../../graphql/Motif';
import { TypeMotif } from '../../../../types/graphql-global-types';
import Backdrop from '../../../Common/Backdrop';
import { CONTRATS } from '../../../../graphql/Contrat/types/CONTRATS';
import { GET_CONTRATS } from '../../../../graphql/Contrat';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import { LOGICIELS_WITH_MINIM_INFO } from '../../../../graphql/Logiciel/types/LOGICIELS_WITH_MINIM_INFO';
import { GET_LOGICIELS_WITH_MINIM_INFO } from '../../../../graphql/Logiciel';

export interface InputInterface {
  name: string;
  value: any;
  autocompleteValue?: any;
  label?: string;
  type?: 'select' | 'date' | 'text' | 'checkbox';
  placeholder?: string;
  inputType?: string;
  selectOptions?: any[];
  selectOptionIdKey?: string;
  selectOptionValueKey?: string;
  required?: boolean;
}

export interface FormInputInterface {
  title: string;
  inputs: InputInterface[];
}

export interface PharmacieFormProps {
  values: PharmacieFormInterface;
  isOnCreate: boolean;
  isOnEdit: boolean;
  handleChangeInput: (e: ChangeEvent<any>) => void;
  handleChangeDate: (name: string) => (date: any) => void;
}

const PharmacieForm: FC<PharmacieFormProps & RouteComponentProps> = ({
  values,
  handleChangeInput,
  handleChangeDate,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();

  const { entreeSortie, cip, nom, adresse1, cp, ville, informatique } = values;

  /**
   * Get enter motif list
   */
  const { data: eMotifData, loading: eMotifLoading } = useQuery<
    MOTIFS_BY_TYPE,
    MOTIFS_BY_TYPEVariables
  >(GET_MOTIFS_BY_TYPE, {
    variables: { type: TypeMotif.E },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  /**
   * Get contrat list
   */
  const { data: contratData, loading: contratLoading } = useQuery<CONTRATS>(GET_CONTRATS, {
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  /**
   * Get logiciel list
   */
  const { data: logicielData, loading: logicielLoading } = useQuery<LOGICIELS_WITH_MINIM_INFO>(
    GET_LOGICIELS_WITH_MINIM_INFO,
    {
      onError: errors => {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );
  const logicielList = (logicielData && logicielData.logiciels) || [];

  const FORM_INPUTS: FormInputInterface[] = [
    {
      title: 'Informations personnelles',
      inputs: [
        {
          label: "Motif d'entrée",
          placeholder: 'Choisissez le motif',
          type: 'select',
          name: 'entreeSortie.idMotifEntree',
          value: (entreeSortie && entreeSortie.idMotifEntree) || '',
          inputType: undefined,
          selectOptions: (eMotifData && eMotifData.motifs) || [],
          selectOptionValueKey: 'libelle',
        },
        {
          label: 'CIP',
          placeholder: 'Code CIP de la pharmacie',
          type: 'text',
          name: 'cip',
          value: cip,
          inputType: 'number',
          required: true,
        },
        {
          label: 'Nom Pharmacie',
          placeholder: 'Nom Pharmacie',
          type: 'text',
          name: 'nom',
          value: nom,
          inputType: 'text',
          required: true,
        },
        {
          label: "Date d'entrée",
          placeholder: '00/00/0000',
          type: 'date',
          name: 'entreeSortie.dateEntree',
          value: (entreeSortie && entreeSortie.dateEntree) || null,
          inputType: undefined,
        },
        {
          label: 'Contrat',
          placeholder: 'Type de contrat',
          type: 'select',
          name: 'entreeSortie.idContrat',
          value: (entreeSortie && entreeSortie.idContrat) || '',
          inputType: undefined,
          selectOptions: (contratData && contratData.contrats) || [],
          selectOptionValueKey: 'nom',
        },
        // TODO : lgo
        {
          label: 'LGO',
          placeholder: 'Votre LGO',
          type: 'select',
          name: 'informatique.idLogiciel',
          value: (informatique && informatique.idLogiciel) || '',
          inputType: undefined,
          selectOptions: logicielList,
          selectOptionIdKey: 'id',
          selectOptionValueKey: 'nom',
        },
      ],
    },
    {
      title: 'Coordonnées',
      inputs: [
        {
          label: 'Adresse',
          placeholder: 'Adresse de la pharmacie',
          type: 'text',
          name: 'adresse1',
          value: adresse1,
          inputType: 'text',
        },
        {
          label: 'Code Postal',
          placeholder: 'Code postal de la pharmacie',
          type: 'text',
          name: 'cp',
          value: cp,
          inputType: 'number',
        },
        {
          label: 'Villle',
          placeholder: 'Ville de la pharmacie',
          type: 'text',
          name: 'ville',
          value: ville,
          inputType: 'text',
        },
      ],
    },
  ];

  return (
    <div className={classes.pharmacieFormRoot}>
      {(eMotifLoading || contratLoading) && <Backdrop />}
      <div className={classes.formContainer}>
        {FORM_INPUTS.map((item: FormInputInterface, index: number) => {
          return (
            <Fragment key={`pharmacie_form_item_${index}`}>
              <Typography className={classes.inputTitle}>{item.title}</Typography>
              <div className={classes.inputsContainer}>
                {item.inputs.map((input: InputInterface, inputIndex: number) => {
                  return (
                    <div className={classes.formRow} key={`pharmacie_form_input_${inputIndex}`}>
                      <Typography className={classes.inputLabel}>
                        {input.label} {input.required && <span>*</span>}
                      </Typography>
                      {input.type === 'select' ? (
                        <CustomSelect
                          label=""
                          list={input.selectOptions || []}
                          listId={input.selectOptionIdKey || 'id'}
                          index={input.selectOptionValueKey || 'value'}
                          name={input.name}
                          value={input.value}
                          onChange={handleChangeInput}
                          shrink={false}
                          placeholder={input.placeholder}
                          withPlaceholder={true}
                        />
                      ) : input.type === 'date' ? (
                        <CustomDatePicker
                          placeholder={input.placeholder}
                          onChange={handleChangeDate(input.name)}
                          name={input.name}
                          value={input.value}
                        />
                      ) : (
                        <CustomFormTextField
                          name={input.name}
                          value={input.value}
                          onChange={handleChangeInput}
                          placeholder={input.placeholder}
                          type={input.inputType}
                        />
                      )}
                    </div>
                  );
                })}
              </div>
            </Fragment>
          );
        })}
      </div>
    </div>
  );
};

export default withRouter(PharmacieForm);
