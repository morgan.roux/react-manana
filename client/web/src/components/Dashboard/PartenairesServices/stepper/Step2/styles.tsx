import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    radioGroup: {
      width: 800,
      display: 'flex',
      justifyContent: 'space-around',
    },
  }),
);

export default useStyles;
