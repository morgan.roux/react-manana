import moment from 'moment';
import React, { useState } from 'react';
import { PARTENAIRE_SERVICE_URL } from '../../../../Constant/url';
import { ConfirmDeleteDialog } from '../../../Common/ConfirmDialog';
import { Column } from '../../Content/Interface';
import TableActionColumn from '../../TableActionColumn';
import { useCheckedsPartenaireService } from './useCheckedsPartenaireService';
import { useDeletePartenaireService } from './useDeletePartenaireService';

export const usePartenaireServiceColumns = (
  history: any,
  viewInModal?: boolean,
  handleOpenDetail?: any,
) => {
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [selected, setSelected] = useState<any[]>([]);
  const [deleteRow, setDeleteRow] = useState<any>();
  const deletePartenaireService = useDeletePartenaireService();
  const checkedPartenaireService = useCheckedsPartenaireService();
  const handleClickDelete = (row?: any) => {
    if (row) {
      setDeleteRow(row);
    }
    setOpenDeleteDialog(true);
  };

  const onClickConfirmDelete = () => {
    if (deleteRow) {
      const { id } = deleteRow;
      deletePartenaireService({ variables: { ids: [id] } });
      setDeleteRow(null);
    } else if (checkedPartenaireService) {
      const ids = checkedPartenaireService.map(partenaire => partenaire.id);
      deletePartenaireService({ variables: { ids } });
    }
    setOpenDeleteDialog(false);
  };

  const DeleteDialogContent = () => {
    if (deleteRow) {
      return (
        <span>
          Êtes-vous sur de vouloir supprimer
          <span style={{ fontWeight: 'bold' }}>{deleteRow.nom || ''}</span>?
        </span>
      );
    }
    return <span>Êtes-vous sur de vouloir supprimer ce(s) partenaire(s) ?</span>;
  };

  const confirmDeleteDialog = (
    <ConfirmDeleteDialog
      open={openDeleteDialog}
      setOpen={setOpenDeleteDialog}
      content={<DeleteDialogContent />}
      onClickConfirm={onClickConfirmDelete}
    />
  );

  const columns: Column[] = [
    {
      name: 'nom',
      label: 'Prestataire de service',
      renderer: (value: any) => {
        return value.nom ? value.nom : '-';
      },
    },
    {
      name: 'adresse1',
      label: 'Adresse',
      renderer: (value: any) => {
        return value.partenaireServiceSuite && value.partenaireServiceSuite.adresse1
          ? value.partenaireServiceSuite.adresse1
          : '-';
      },
    },
    {
      name: 'codePostal',
      label: 'CP',
      renderer: (value: any) => {
        return value.partenaireServiceSuite && value.partenaireServiceSuite.codePostal
          ? value.partenaireServiceSuite.codePostal
          : '-';
      },
    },
    {
      name: 'ville',
      label: 'Ville',
      renderer: (value: any) => {
        return value.partenaireServiceSuite && value.partenaireServiceSuite.ville
          ? value.partenaireServiceSuite.ville
          : '-';
      },
    },
    {
      name: 'partenaireServiceSuite.telephone',
      label: 'Téléphone',
      renderer: (value: any) => {
        return value.partenaireServiceSuite && value.partenaireServiceSuite.telephone
          ? value.partenaireServiceSuite.telephone
          : '-';
      },
    },
    {
      name: 'service',
      label: 'Services',
      //   renderer: (value: any) => {
      //     return value.numIvrylab ? value.numIvrylab : '-';
      //   },
    },
    {
      name: 'dateDebutPartenaire',
      label: 'Date début de Partenariat',
      renderer: (value: any) => {
        return value.partenaireServicePartenaire &&
          value.partenaireServicePartenaire.dateDebutPartenaire
          ? moment(value.partenaireServicePartenaire.dateDebutPartenaire).format('DD/MM/YYYY')
          : '-';
      },
    },
    {
      name: 'dateFinPartenaire',
      label: 'Date fin de Partenariat',
      renderer: (value: any) => {
        return value.partenaireServicePartenaire &&
          value.partenaireServicePartenaire.dateFinPartenaire
          ? moment(value.partenaireServicePartenaire.dateFinPartenaire).format('DD/MM/YYYY')
          : '-';
      },
    },
    {
      name: 'typePartenaire',
      label: 'Type de Partenariat',
      renderer: (value: any) => {
        return value.partenaireServicePartenaire && value.partenaireServicePartenaire.typePartenaire
          ? value.partenaireServicePartenaire.typePartenaire
          : '-';
      },
    },
    {
      name: 'statutPartenaire',
      label: 'Statut de Partenariat',
      renderer: (value: any) => {
        if (
          value.partenaireServicePartenaire &&
          value.partenaireServicePartenaire.statutPartenaire === 'ACTIVER'
        ) {
          return 'Activé';
        } else if (
          value.partenaireServicePartenaire &&
          value.partenaireServicePartenaire.statutPartenaire === 'BLOQUER'
        ) {
          return 'Bloqué';
        } else {
          return '-';
        }
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <TableActionColumn
            row={value}
            setOpenDeleteDialog={setOpenDeleteDialog}
            baseUrl={PARTENAIRE_SERVICE_URL}
            handleClickDelete={handleClickDelete}
          />
        );
      },
    },
  ];

  return {
    columns,
    confirmDeleteDialog,
    handleClickDelete,
    selected,
    setSelected,
    setOpenDeleteDialog,
  };
};
