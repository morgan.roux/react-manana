import React from 'react';
import {
  useQuery,
  useMutation,
  useApolloClient,
  useLazyQuery,
  QueryLazyOptions,
} from '@apollo/react-hooks';
import {
  PARTENAIRE_SERVICE,
  PARTENAIRE_SERVICEVariables,
} from '../../../../graphql/PartenairesServices/types/PARTENAIRE_SERVICE';
import { GET_PARTENAIRE_SERVICE } from '../../../../graphql/PartenairesServices';

export const useGetPartenaireService = () => {
  const [getPartenaireService, { data, loading }] = useLazyQuery<
    PARTENAIRE_SERVICE,
    PARTENAIRE_SERVICEVariables
  >(GET_PARTENAIRE_SERVICE, {
    onError: errors => {
      errors.graphQLErrors.map(err => {});
    },
  });
  const partenaireService = data && data.partenaire;
  return [getPartenaireService, partenaireService];
};
