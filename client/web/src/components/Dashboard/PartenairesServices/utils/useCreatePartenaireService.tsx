import { useMutation, useApolloClient } from '@apollo/react-hooks';
import {
  CREATE_UPDATE_PARTENAIRE_SERVICE,
  CREATE_UPDATE_PARTENAIRE_SERVICEVariables,
} from '../../../../graphql/PartenairesServices/types/CREATE_UPDATE_PARTENAIRE_SERVICE';
import { DO_CREATE_UPDATE_PARTENAIRE_SERVICE } from '../../../../graphql/PartenairesServices';
import { useState, useContext } from 'react';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import { ContentStateInterface, ContentContext } from '../../../../AppContext';
import { PARTENAIRE_SERVICE_URL } from '../../../../Constant/url';
import { RouteComponentProps } from 'react-router';

export const useCreateUpdatePartenaireService = (
  values: CREATE_UPDATE_PARTENAIRE_SERVICEVariables,
  operationName: any,
  variables: any,
  isOnEdit: boolean,
): [() => void, boolean, boolean] => {
  const client = useApolloClient();

  const [mutationSuccess, setMutationSuccess] = useState<boolean>(false);

  const [createUpdatePartenaireService, { loading }] = useMutation<
    CREATE_UPDATE_PARTENAIRE_SERVICE,
    CREATE_UPDATE_PARTENAIRE_SERVICEVariables
  >(DO_CREATE_UPDATE_PARTENAIRE_SERVICE, {
    variables: { ...values },
    update: (cache, { data }) => {
      if (data && data.createUpdatePartenaireService && values) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables: variables,
          });
          if (req && req.search && req.search.data) {
            if (isOnEdit) {
              cache.writeQuery({
                query: operationName,
                data: {
                  search: {
                    ...req.search,
                    ...{
                      total: req.search.total,
                      data:
                        req.search.data &&
                        req.search.data.map(partenaire => {
                          if (
                            partenaire &&
                            partenaire.id &&
                            data.createUpdatePartenaireService &&
                            data.createUpdatePartenaireService.id === partenaire.id
                          ) {
                            return data.createUpdatePartenaireService;
                          } else {
                            return partenaire;
                          }
                        }),
                    },
                  },
                },
                variables: variables,
              });
            } else {
              cache.writeQuery({
                query: operationName,
                data: {
                  search: {
                    ...req.search,
                    ...{
                      total: req.search.total + 1,
                      data: [...req.search.data, data.createUpdatePartenaireService],
                    },
                  },
                },
                variables: variables,
              });
            }
          }
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
      displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
    },
    onCompleted: data => {
      if (data && data.createUpdatePartenaireService) {
        setMutationSuccess(true);
      }
    },
  });

  return [createUpdatePartenaireService, mutationSuccess, loading];
};
