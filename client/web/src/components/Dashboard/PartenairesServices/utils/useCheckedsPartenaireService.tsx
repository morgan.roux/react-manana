import { useQuery } from '@apollo/react-hooks';
import { GET_CHECKEDS_PARTENAIRE_SERVICE } from '../../../../graphql/PartenairesServices/local';

export const useCheckedsPartenaireService = () => {
  const checkedsPartenaireService = useQuery(GET_CHECKEDS_PARTENAIRE_SERVICE);
  const checkeds =
    checkedsPartenaireService &&
    checkedsPartenaireService.data &&
    checkedsPartenaireService.data.checkedsPartenaireService;
  return checkeds;
};
