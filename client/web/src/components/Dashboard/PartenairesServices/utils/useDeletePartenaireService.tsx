import { useContext } from 'react';
import { ContentStateInterface, ContentContext } from '../../../../AppContext';
import { useCheckedsPartenaireService } from './useCheckedsPartenaireService';
import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { DO_DELETE_PARTENAIRE_SERVICES } from '../../../../graphql/PartenairesServices/mutation';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import {
  DELETE_SOFT_PARTENAIRE_SERVICE,
  DELETE_SOFT_PARTENAIRE_SERVICEVariables,
} from '../../../../graphql/PartenairesServices/types/DELETE_SOFT_PARTENAIRE_SERVICE';
import { differenceBy } from 'lodash';

export const useDeletePartenaireService = () => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const checkeds = useCheckedsPartenaireService();

  const client = useApolloClient();

  const [deletePartenaireService] = useMutation<
    DELETE_SOFT_PARTENAIRE_SERVICE,
    DELETE_SOFT_PARTENAIRE_SERVICEVariables
  >(DO_DELETE_PARTENAIRE_SERVICES, {
    update: (cache, { data }) => {
      if (data && data.softDeletePartenaireServices) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables: variables,
          });
          if (req && req.search && req.search.data) {
            const source = req.search.data;
            const result = data.softDeletePartenaireServices;
            const dif = differenceBy(source, result, 'id');
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: dif,
                  },
                },
              },
              variables: variables,
            });
            client.writeData({
              data: { useCheckedsPartenaireService: differenceBy(checkeds, result, 'id') },
            });
          }
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  return deletePartenaireService;
};
