import { Box, FormControlLabel, Radio, RadioGroup, Typography } from '@material-ui/core';
import { last } from 'lodash';
import { any } from 'prop-types';
import React, {
  Dispatch,
  FC,
  Fragment,
  SetStateAction,
  useCallback,
  useEffect,
  useState,
} from 'react';
import { AWS_HOST } from '../../../../../../../utils/s3urls';
import ChoiceAvatarForm from '../../../../../../Common/ChoiceAvatarForm';
import { CustomModal } from '../../../../../../Common/CustomModal';
import { CustomReactEasyCrop } from '../../../../../../Common/CustomReactEasyCrop';
import CustomSelect from '../../../../../../Common/CustomSelect';
import { CustomFormTextField } from '../../../../../../Common/CustomTextField';
import Dropzone from '../../../../../../Common/Dropzone';
import {
  FormInputInterface,
  InputInterface,
} from '../../../../../GroupeClient/GroupeClientForm/GroupeClientForm';
import useStyles from '../styles';
import useCommonStyles from '../../../../../commonStyles';
import { address } from 'faker';
import { CIVILITE_LIST, SEXE_LIST } from '../../../../../../../Constant/user';

interface FormStepTwoProps {
  values: any;
  handleChange: any;
}

const FormStepTwo: FC<FormStepTwoProps> = ({ values, handleChange }) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const { afficherComme, contact } = values;

  const FORM_INPUTS: FormInputInterface[] = [
    {
      title: 'Internet',
      inputs: [
        {
          label: 'Adresse de messagerie',
          placeholder: 'Adresse de messagerie de la personne',
          type: 'text',
          name: 'contact.mailProf',
          value: contact && contact.mailProf, // TODO
        },
        {
          label: 'Afficher comme',
          placeholder: 'Afficher comme',
          type: 'text',
          name: 'afficherComme',
          value: afficherComme, // TODO
        },
        {
          label: 'Page web',
          placeholder: 'Page web de la personne',
          type: 'text',
          name: 'contact.siteProf',
          value: contact && contact.siteProf, // TODO
        },
        {
          label: 'Adresse de messagerie instantanée',
          placeholder: 'Adresse de messagerie instantanée de la personne',
          type: 'text',
          name: 'contact.mailPerso',
          value: contact && contact.mailPerso, // TODO
        },
      ],
    },
    {
      title: 'Numéros de téléphone',
      inputs: [
        {
          label: 'Bureau',
          placeholder: 'Son bureau',
          type: 'text',
          name: 'contact.telProf',
          value: contact && contact.telProf, // TODO
        },
        {
          label: 'Domicile',
          placeholder: 'Son domicile',
          type: 'text',
          name: 'contact.telPerso',
          value: contact && contact.telPerso, // TODO
        },
        {
          label: 'code postal',
          placeholder: 'Son code postal',
          type: 'text',
          name: 'contact.cp',
          value: contact && contact.cp, // TODO
        },
      ],
    },
    {
      title: 'Réseaux sociaux',
      inputs: [
        {
          label: 'LinkedIn',
          placeholder: 'Son LinkedIn',
          type: 'text',
          name: 'contact.urlLinkedinProf',
          value: contact && contact.urlLinkedinProf, // TODO
        },
        {
          label: 'Facebook',
          placeholder: 'Son Facebook',
          type: 'text',
          name: 'contact.urlFacebookProf',
          value: contact && contact.urlFacebookProf, // TODO
        },
        {
          label: 'WhatSapp',
          placeholder: 'Son WhatSapp',
          type: 'text',
          name: 'contact.whatsAppMobProf',
          value: contact && contact.whatsAppMobProf, // TODO
        },
        {
          label: 'Messenger',
          placeholder: 'Son Messenger',
          type: 'text',
          name: 'contact.urlMessenger',
          value: contact && contact.urlMessenger, // TODO
        },
        {
          label: 'Youtube',
          placeholder: 'Son Youtube',
          type: 'text',
          name: 'contact.urlYoutube',
          value: contact && contact.urlYoutube, // TODO
        },
      ],
    },
  ];

  return (
    <Box maxWidth="816px" width="100%">
      <div className={commonClasses.formContainer}>
        {FORM_INPUTS.map((item: FormInputInterface, index: number) => {
          return (
            <Fragment key={`partenaire_contact_form_item_${index}`}>
              <Typography className={commonClasses.inputTitle}>{item.title}</Typography>
              <div className={commonClasses.inputsContainer}>
                {item.inputs.map((input: InputInterface, inputIndex: number) => {
                  return (
                    <div
                      className={commonClasses.formRow}
                      key={`partenaire_contact_form_input_${inputIndex}`}
                    >
                      <Typography className={commonClasses.inputLabel}>
                        {input.label} {input.required && <span>*</span>}
                      </Typography>
                      {input.type === 'select' ? (
                        <CustomSelect
                          label=""
                          list={input.selectOptions || []}
                          listId="id"
                          index="value" // TODO
                          name={input.name}
                          value={input.value}
                          onChange={handleChange}
                          shrink={false}
                          placeholder={input.placeholder}
                          withPlaceholder={true}
                        />
                      ) : (
                        <CustomFormTextField
                          name={input.name}
                          value={input.value}
                          onChange={handleChange}
                          placeholder={input.placeholder}
                          type={input.inputType}
                        />
                      )}
                    </div>
                  );
                })}
              </div>
            </Fragment>
          );
        })}
      </div>
    </Box>
  );
};

export default FormStepTwo;
