import { Paper, Tab, Tabs } from '@material-ui/core';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { PARTENAIRE_partenaire } from '../../../../../graphql/Partenaire/types/PARTENAIRE';
import AffectationList from './AffectationList';
import ContactList from './ContactList';
import Form from './Form';
import useStyles from './styles';
interface ContactProps {
  partenaire: PARTENAIRE_partenaire | null | undefined;
}

const Contact: FC<ContactProps & RouteComponentProps> = ({
  location: { pathname },
  history: { push },
  partenaire,
}) => {
  const classes = useStyles({});
  const isOnCreate: boolean = pathname.includes('/create') || pathname.includes('/edit');
  const isOnList: boolean = pathname.includes('/list');
  const currentTab = isOnList ? 0 : 1;

  const handleTabClick = (event: React.ChangeEvent<{}>, newValue: number) => {
    push(
      newValue === 0
        ? pathname.replace('/affectation', '/list')
        : pathname.replace('/list', '/affectation'),
    );
  };

  console.log('partenaire contact form :>> ', partenaire);

  if (isOnCreate) {
    return <Form />;
  }

  return (
    <div>
      <Paper elevation={3}>
        <Tabs
          value={currentTab}
          indicatorColor="secondary"
          textColor="primary"
          onChange={handleTabClick}
        >
          <Tab label="Liste des contacts" />
          <Tab label="Affectation territoriale" />
        </Tabs>
        {currentTab === 0 ? <ContactList /> : <AffectationList />}
      </Paper>
    </div>
  );
};
export default withRouter(Contact);
