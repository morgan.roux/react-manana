import { Box } from '@material-ui/core';
import { Delete, Edit, Visibility } from '@material-ui/icons';
import React, { FC, Fragment, useContext, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { PARTENAIRE_SERVICE_URL } from '../../../../../../Constant/url';
import { GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT } from '../../../../../../graphql/PartenaireRepresentant/query';
import { ConfirmDeleteDialog } from '../../../../../Common/ConfirmDialog';
import CustomAvatar from '../../../../../Common/CustomAvatar';
import CustomButton from '../../../../../Common/CustomButton';
import CustomContent from '../../../../../Common/newCustomContent';
import SearchInput from '../../../../../Common/newCustomContent/SearchInput';
import WithSearch from '../../../../../Common/newWithSearch/withSearch';
import { Column } from '../../../../Content/Interface';
import {} from '../../../../../../graphql/PartenaireRepresentant/mutation';
import TableActionColumn, {
  AttributionDepartementIcon,
  TableActionColumnMenu,
} from '../../../../TableActionColumn';
import useStyles from './styles';
import { useDeletePartenaireRepresentant } from './deleteContact';
import AffectationDialog from '../../../../../Common/AffectationDialog';
import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { ContentStateInterface, ContentContext } from '../../../../../../AppContext';
import { DO_AFFECT_PARTENAIRE_REPRESENTANT } from '../../../../../../graphql/PartenaireRepresentantAffectation/mutation';
import {
  AFFECT_PARTENAIRE_REPRESENTANT,
  AFFECT_PARTENAIRE_REPRESENTANTVariables,
} from '../../../../../../graphql/PartenaireRepresentantAffectation/types/AFFECT_PARTENAIRE_REPRESENTANT';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
interface ContactListProps {
  match: { params: { idPartenaire: string | undefined; id: string | undefined } };
}

const ContactList: FC<ContactListProps & RouteComponentProps> = ({
  location: { pathname },
  history: { push },
  match: {
    params: { idPartenaire, id },
  },
}) => {
  const classes = useStyles({});
  const isOnList: boolean = pathname.includes('/list');

  const [openAffectationDialog, setOpenAffectationDialog] = useState<boolean>(false);

  // const isOnCreate: boolean = pathname.includes('/create');
  // const currentTab = isOnList ? 0 : 1;

  const CREATE_URL = `/db/${PARTENAIRE_SERVICE_URL}/fiche/${idPartenaire}/contact/create`;

  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [representantToAffect, setRepresentantToAffect] = useState<any>({});
  const [deleteRow, setDeleteRow] = useState<any>(null);
  const [selected, setSelected] = useState<any[]>([]);
  const deletePartenaire = useDeletePartenaireRepresentant(setSelected);

  const goToCreate = () => {
    push(CREATE_URL);
  };

  const goToEdit = (_event: any, row: any) => {
    if (row && row.id) {
      push(`/db/${PARTENAIRE_SERVICE_URL}/fiche/${idPartenaire}/contact/edit/${row.id}`);
    }
  };

  const goToDetails = (_event: any, row: any) => {
    if (row && row.id) {
      push(`/db/${PARTENAIRE_SERVICE_URL}/fiche/${idPartenaire}/contact/details/${row.id}`);
    }
  };

  const onClickDelete = (_event: any, row: any) => {
    setOpenDeleteDialog(true);
    setDeleteRow(row);
  };

  const onClickConfirmDelete = () => {
    if (deleteRow) {
      const { id } = deleteRow;

      deletePartenaire({
        variables: { ids: [id] },
      });
      setDeleteRow(null);
    } else if (selected && selected.length > 0) {
      deletePartenaire({
        variables: { ids: selected.map(item => item.id) },
      });
    }
    setOpenDeleteDialog(false);
  };

  const onClickAffectation = (event: any, action: any) => {
    event.preventDefault();
    event.stopPropagation();
    setOpenAffectationDialog(true);
    setRepresentantToAffect({ ...action });
  };

  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const client = useApolloClient();
  const [affectPartenaireRepresentant, { loading, data: affectData }] = useMutation<
    AFFECT_PARTENAIRE_REPRESENTANT,
    AFFECT_PARTENAIRE_REPRESENTANTVariables
  >(DO_AFFECT_PARTENAIRE_REPRESENTANT, {
    update: (cache, { data }) => {
      if (data && data.createUpdatePartenaireRepresentantAffectation) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables: variables,
          });
          if (req && req.search && req.search.data) {
            // const source = req.search.data;
            const result = data.createUpdatePartenaireRepresentantAffectation;
            // const dif = differenceBy(source, result, 'id');
            // cache.writeQuery({
            //   query: operationName,
            //   data: {
            //     search: {
            //       ...req.search,
            //       ...{
            //         data: dif,
            //       },
            //     },
            //   },
            //   variables: variables,
            // });
            // client.writeData({
            //   data: { checkedsPersonnelGroupement: differenceBy(checkeds, result, 'id') },
            // });
            displaySnackBar(client, {
              type: 'SUCCESS',
              message: 'Utilisateur affecté avec succès',
              isOpen: true,
            });
          }
        }
      }
    },
    onError: errors => {
      console.log('======eror =========+> ', errors);
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  const affectationDialog = (
    <AffectationDialog
      open={openAffectationDialog}
      setOpen={setOpenAffectationDialog}
      personnelToAffect={representantToAffect}
      setPersonnelToAffect={setRepresentantToAffect}
      affectPersonnel={affectPartenaireRepresentant}
    />
  );

  const menuItems: TableActionColumnMenu[] = [
    { label: 'Modifier', icon: <Edit />, onClick: goToEdit, disabled: false },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: onClickDelete,
      disabled: false,
    },
    {
      label: 'Voir détails',
      icon: <Visibility />,
      onClick: goToDetails,
      disabled: false,
    },
    {
      label: 'Attribution de département',
      icon: (
        <div className={classes.dptIconBackground}>
          <AttributionDepartementIcon color="inherit" />
        </div>
      ),
      onClick: (e, row) => {
        onClickAffectation(e, {
          action: { type: 'DEPARTEMENT', label: 'Attribution de département' },
          partenaireRepresentant: { ...row },
        });
      },
      disabled: false,
    },
  ];

  const columns: Column[] = [
    {
      label: 'Photo',
      name: '',
      renderer: (value: any) => {
        return (
          <CustomAvatar
            name={value && value.fullName}
            url={value && value.photo && value.photo.publicUrl}
          />
        );
      },
    },
    { label: 'Fonction', name: 'fonction' },
    { label: 'Prénom', name: 'prenom' },
    { label: 'Nom', name: 'nom' },
    {
      label: 'Téléphone',
      name: 'contact.telProf',
      renderer: (value: any) => {
        return (value.contact && value.contact.telProf) || '-';
      },
    },
    {
      label: 'Mail',
      name: 'contact.mailProf',
      renderer: (value: any) => {
        return (value.contact && value.contact.mailProf) || '-';
      },
    },
    {
      label: 'Adresse',
      name: 'contact.adresse1',
      renderer: (value: any) => {
        return (value.contact && value.contact.adresse1) || '-';
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <Fragment>
            <TableActionColumn
              row={value}
              baseUrl=""
              showResendEmail={false}
              customMenuItems={menuItems}
            />
          </Fragment>
        );
      },
    },
  ];

  const tableProps = {
    isSelectable: true,
    paginationCentered: true,
    columns,
    selected,
    setSelected,
  };

  const DeleteDialogContent = () => {
    if (deleteRow) {
      const name = `${deleteRow.prenom || ''} ${deleteRow.nom || ''}`;
      return (
        <span>
          Êtes-vous sur de vouloir supprimer
          <span style={{ fontWeight: 'bold' }}> {name} </span>?
        </span>
      );
    }
    return <span>Êtes-vous sur de vouloir supprimer ces contacts ?</span>;
  };

  return (
    <Box width="100%">
      <div className={classes.searchInputBox}>
        <SearchInput searchPlaceholder="Rechercher un contact..." />
        <div>
          {selected && selected.length > 0 && (
            <CustomButton onClick={() => setOpenDeleteDialog(true)}>
              SUPPRIMER LA SELECTION
            </CustomButton>
          )}
          <CustomButton color="secondary" className={classes.btnAdd} onClick={goToCreate}>
            Ajout de contact
          </CustomButton>
        </div>
      </div>
      <WithSearch
        type="partenairerepresentant"
        props={tableProps}
        WrappedComponent={CustomContent}
        searchQuery={GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT}
        optionalMust={[{ term: { isRemoved: false } }, { term: { 'partenaire.id': idPartenaire } }]}
      />
      {affectationDialog}
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
    </Box>
  );
};
export default withRouter(ContactList);
