import { useContext } from 'react';
import { ContentStateInterface, ContentContext } from '../../../../../../AppContext';
import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import { DO_DELETE_PARTENAIRE_REPRESENTANT } from '../../../../../../graphql/PartenaireRepresentant/mutation';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import {
  DELETE_PARTENAIRE_REPRESENTANT,
  DELETE_PARTENAIRE_REPRESENTANTVariables,
} from '../../../../../../graphql/PartenaireRepresentant/types/DELETE_PARTENAIRE_REPRESENTANT';
import { differenceBy } from 'lodash';
import { GET_CHECKED_PARTENAIRE_REPRESENTANT } from '../../../../../../graphql/PartenaireRepresentant/local';

const useCheckedPartenaireRepresentant = () => {
  const checkedsPartenaireRepresentant = useQuery(GET_CHECKED_PARTENAIRE_REPRESENTANT);
  return (
    checkedsPartenaireRepresentant &&
    checkedsPartenaireRepresentant.data &&
    checkedsPartenaireRepresentant.data.checkedsPartenaireRepresentant
  );
};

export const useDeletePartenaireRepresentant = (setSelected: any) => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const checkeds = useCheckedPartenaireRepresentant();

  const client = useApolloClient();

  const [deletePersonnels] = useMutation<
    DELETE_PARTENAIRE_REPRESENTANT,
    DELETE_PARTENAIRE_REPRESENTANTVariables
  >(DO_DELETE_PARTENAIRE_REPRESENTANT, {
    update: (cache, { data }) => {
      if (data && data.softDeletePartenaireRepresentants) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables: variables,
          });
          if (req && req.search && req.search.data) {
            const source = req.search.data;
            const result = data.softDeletePartenaireRepresentants;
            const dif = differenceBy(source, result, 'id');
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: dif,
                  },
                },
              },
              variables: variables,
            });
            setSelected([]);
          }
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
    onCompleted: data => {
      if (data && data.softDeletePartenaireRepresentants) {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'Contact supprimé(s) avec succès',
          isOpen: true,
        });
      }
    },
  });

  return deletePersonnels;
};
