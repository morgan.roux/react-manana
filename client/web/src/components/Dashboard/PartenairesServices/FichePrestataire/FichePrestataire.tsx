import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import { Paper, Tab, Tabs, Typography } from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons';
import React, { ChangeEvent, FC, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { PARTENAIRE_SERVICE_URL } from '../../../../Constant/url';
import { GET_PARTENAIRE } from '../../../../graphql/Partenaire/query';
import { PARTENAIRE, PARTENAIREVariables } from '../../../../graphql/Partenaire/types/PARTENAIRE';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop';
import CustomButton from '../../../Common/CustomButton';
import SubToolbar from '../../../Common/newCustomContent/SubToolbar';
import Contact from './Contact';
import Ressource from './Ressource';
import RessourceForm from './Ressource/Form/Form';
import useStyles from './styles';
import { useCreateUpdateRessource } from './Ressource/useCreateRessource';
import { ContentContext, ContentStateInterface } from '../../../../AppContext';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { uploadToS3 } from '../../../../services/S3';
import { TypeFichier } from '../../../../types/graphql-global-types';
import { formatFilenameWithoutDate } from '../../../../utils/filenameFormater';
import { getGroupement } from '../../../../services/LocalStorage';
import FicheContact from './Contact/FicheContact';

interface FichePrestataireProps {
  match: { params: { idPartenaire: string | undefined; id: string | undefined } };
}

const FichePrestataire: FC<FichePrestataireProps & RouteComponentProps> = ({
  location: { pathname },
  match: {
    params: { idPartenaire, id },
  },
  history: { push },
}) => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);
  const classes = useStyles({});
  const client = useApolloClient();

  const isOnContactCreate: boolean = pathname.includes('/contact/create');
  const isOnContactList: boolean = pathname.includes('/contact/list');
  const isOnRessourceCreate: boolean =
    pathname.includes('/ressource/create') || pathname.includes('/ressource/edit');
  const isOnRessourceEdit: boolean = pathname.includes('/ressource/edit');
  const isOnRessourceList: boolean = pathname.includes('/ressource/list');
  const isOnContactDetails: boolean = pathname.includes('/contact/details/');
  const isOnEdit: boolean = id ? true : false;
  const isOnContact: boolean = pathname.includes('/contact');
  const isOnRessource: boolean = pathname.includes('/ressource');
  const currentTab = isOnContact ? 0 : 1;
  const [loading, setLoading] = useState<boolean>(false);
  const [operationNameState, setOperationName] = useState<any>();
  const [variablesState, setVariables] = useState<any>();
  const isOnContactDetail: boolean = pathname.includes('/contact/details');
  useEffect(() => {
    if (operationName && !operationNameState) {
      setOperationName(operationName);
      setVariables(variables);
    }
  }, [operationName]);

  const RessourceInitialState = {
    code: '',
    libelle: '',
    dateChargement: new Date(),
    typeRessource: '',
    idPartenaireService: '',
    idItem: '',
    url: '',
  };

  const { data, loading: loadingPartenaire } = useQuery<PARTENAIRE, PARTENAIREVariables>(
    GET_PARTENAIRE,
    {
      variables: { id: idPartenaire as string },
      onError: error => {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );

  const [values, setValues] = useState<any>(RessourceInitialState);
  const [createUpdateRessource, mutationSuccess] = useCreateUpdateRessource(
    { input: values },
    operationNameState,
    variablesState,
    push,
    setLoading,
    `/db/${PARTENAIRE_SERVICE_URL}/fiche/${idPartenaire}/ressource/list`,
    isOnEdit,
  );

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      setValues(prevState => ({ ...prevState, [name]: value }));
    }
  };

  const handleChangeAutocomplete = (value: any) => {
    setValues(prevState => ({ ...prevState, item: value }));
  };

  const groupement = getGroupement();

  const handleChangeDate = (name: string) => (date: any) => {
    setValues(prevState => ({ ...prevState, [name]: date }));
  };
  const partenaire = data && data.partenaire;

  const onClickBack = () =>
    isOnRessourceCreate
      ? push(
          `/db/${PARTENAIRE_SERVICE_URL}/fiche/${idPartenaire}/${
            isOnContact ? 'contact' : 'ressource'
          }/list`,
        )
      : push(`/db/${PARTENAIRE_SERVICE_URL}`);

  const onClickEdit = () => {
    push(`/db/${PARTENAIRE_SERVICE_URL}/edit/${idPartenaire}`);
  };

  const handleTabClick = (event: React.ChangeEvent<{}>, newValue: number) => {
    push(
      newValue === 0
        ? pathname.replace('/ressource', '/contact')
        : pathname.replace('/contact', '/ressource').replace('/affectation', '/list'),
    );
  };

  const [doCreatePutPresignedUrl, { loading: presignedLoading }] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (
        data &&
        data.createPutPresignedUrls &&
        data.createPutPresignedUrls.length > 0 &&
        values &&
        values.photo &&
        values.photo.size
      ) {
        const presignedPhoto = data.createPutPresignedUrls[0];
        if (presignedPhoto && presignedPhoto.filePath && presignedPhoto.presignedUrl) {
          await uploadToS3(values.photo, presignedPhoto.presignedUrl)
            .then(result => {
              if (result && result.status === 200) {
                createUpdateRessource({
                  variables: {
                    input: {
                      id,
                      idPartenaireService: idPartenaire,
                      typeRessource: values.typeRessource,
                      idItem: values && values.item && values.item.id,
                      code: values && values.code,
                      libelle: values && values.libelle,
                      dateChargement: values && values.dateChargement,
                      url: values && values.url,
                      fichier: {
                        chemin: presignedPhoto.filePath,
                        nomOriginal: values.photo.name,
                        type: TypeFichier.PHOTO,
                      },
                    },
                  },
                });
              }
            })
            .catch(error => {
              console.error(error);
              displaySnackBar(client, {
                type: 'ERROR',
                message:
                  "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
                isOpen: true,
              });
            });
        }
      }
    },
    onError: errors => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      });
    },
  });

  const handleCreateRessource = () => {
    setLoading(true);
    if (values && values.photo && values.photo.size) {
      // Presigned file
      const filePathsTab: string[] = [];
      filePathsTab.push(
        `users/photos/${groupement && groupement.id}/${formatFilenameWithoutDate(values.photo)}`,
      );
      doCreatePutPresignedUrl({ variables: { filePaths: filePathsTab } });
      return;
    }
    createUpdateRessource({
      variables: {
        input: {
          id,
          idPartenaireService: idPartenaire,
          typeRessource: values.typeRessource,
          idItem: values && values.item && values.item.id,
          code: values && values.code,
          libelle: values && values.libelle,
          dateChargement: values && values.dateChargement,
          url: values && values.url,
        },
      },
    });
  };
  console.log('Items', values.item);
  return (
    <div className={classes.root}>
      {loading && <Backdrop />}
      {!isOnContactDetail && (
        <SubToolbar
          title={
            isOnRessourceCreate
              ? isOnRessourceEdit
                ? 'Modification de ressource digital'
                : 'Ajout de ressource digital'
              : 'Fiche détaillés de prestataire de service'
          }
          dark={true}
          withBackBtn={true}
          onClickBack={onClickBack}
          backBtnIcon={<ArrowBack />}
        >
          <CustomButton
            onClick={isOnRessourceCreate ? handleCreateRessource : onClickEdit}
            color="secondary"
            disabled={values.item === undefined}
          >
            {isOnRessourceCreate ? (isOnRessourceEdit ? 'Modifier' : 'Ajouter') : 'Modifier'}
          </CustomButton>
        </SubToolbar>
      )}
      {!isOnContactDetail && (
        <div className={classes.partenaireContainer}>
          <Typography>{partenaire && partenaire.nom}</Typography>
          <Typography>
            {partenaire &&
              partenaire.partenaireServiceSuite &&
              (partenaire.partenaireServiceSuite.adresse1 ||
                partenaire.partenaireServiceSuite.adresse2)}
          </Typography>
        </div>
      )}
      {isOnRessourceCreate ? (
        <RessourceForm
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          handleChangeAutocomplete={handleChangeAutocomplete}
          setValues={setValues}
          values={values}
        />
      ) : isOnContactDetails ? (
        <FicheContact />
      ) : (
        <>
          {isOnRessourceCreate ? (
            <RessourceForm
              handleChange={handleChange}
              handleChangeDate={handleChangeDate}
              handleChangeAutocomplete={handleChangeAutocomplete}
              setValues={setValues}
              values={values}
            />
          ) : (
            <>
              <div className={classes.globalTabsContainer}>
                <Paper elevation={3}>
                  <Tabs
                    value={currentTab}
                    indicatorColor="secondary"
                    textColor="primary"
                    onChange={handleTabClick}
                  >
                    <Tab label="Contact" />
                    <Tab label="Ressource" />
                  </Tabs>
                </Paper>
              </div>
              <div className={classes.contentContainer}>
                <Paper elevation={3}>
                  {isOnContact ? <Contact partenaire={partenaire} /> : <Ressource />}
                </Paper>
              </div>{' '}
            </>
          )}
        </>
      )}
    </div>
  );
};
export default withRouter(FichePrestataire);
