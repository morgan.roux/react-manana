import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { Box } from '@material-ui/core';
import { Delete, Edit, Visibility } from '@material-ui/icons';
import { differenceBy } from 'lodash';
import moment from 'moment';
import React, { ChangeEvent, FC, Fragment, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { ContentContext, ContentStateInterface } from '../../../../../AppContext';
import { PARTENAIRE_SERVICE_URL } from '../../../../../Constant/url';
import { GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT } from '../../../../../graphql/PartenaireRepresentant/query';
import { DO_DELETE_RESSOURCES } from '../../../../../graphql/Ressource/mutation';
import { GET_SEARCH_CUSTOM_CONTENT_RESSOURCE } from '../../../../../graphql/Ressource/query';
import {
  DELETE_RESSOURCES,
  DELETE_RESSOURCESVariables,
} from '../../../../../graphql/Ressource/types/DELETE_RESSOURCES';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { ConfirmDeleteDialog } from '../../../../Common/ConfirmDialog';
import CustomAvatar from '../../../../Common/CustomAvatar';
import CustomButton from '../../../../Common/CustomButton';
import CustomContent from '../../../../Common/newCustomContent';
import SearchInput from '../../../../Common/newCustomContent/SearchInput';
import WithSearch from '../../../../Common/newWithSearch/withSearch';
import { Column } from '../../../Content/Interface';
import TableActionColumn, { TableActionColumnMenu } from '../../../TableActionColumn';
import useStyles from './styles';

interface RessourceListProps {
  match: { params: { idPartenaire: string | undefined; id: string | undefined } };
}

const RessourceList: FC<RessourceListProps & RouteComponentProps> = ({
  location: { pathname },
  history: { push },
  match: {
    params: { idPartenaire, id },
  },
}) => {
  const classes = useStyles({});
  const CREATE_URL = `/db/${PARTENAIRE_SERVICE_URL}/fiche/${idPartenaire}/ressource/create`;
  const isOnList: boolean = pathname.includes('/list');
  const isOnCreate: boolean = pathname === CREATE_URL;
  const client = useApolloClient();
  // const isOnCreate: boolean = pathname.includes('/create');
  // const currentTab = isOnList ? 0 : 1;

  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [deleteRow, setDeleteRow] = useState<any>(null);
  const [selected, setSelected] = useState<any[]>([]);

  const goToCreate = () => {
    push(CREATE_URL);
  };

  const goToEdit = (_event: any, row: any) => {
    if (row && row.id) {
      push(`/db/${PARTENAIRE_SERVICE_URL}/fiche/${idPartenaire}/ressource/edit/${row.id}`);
    }
  };

  const goToDetails = (_event: any, row: any) => {
    if (row && row.id) {
      push(`/db/${PARTENAIRE_SERVICE_URL}/fiche/${idPartenaire}/contact/details/${row.id}`);
    }
  };

  const onClickDelete = (_event: any, row: any) => {
    setOpenDeleteDialog(true);
    setDeleteRow(row);
  };

  const [itemToDelete, setItemToDelete] = useState<any>();

  const onClickConfirmDelete = () => {
    setOpenDeleteDialog(false);
    if (deleteRow) {
      deleteRessource({ variables: { ids: [deleteRow && deleteRow.id] } });
    } else if (selected && selected.length > 0) {
      deleteRessource({
        variables: { ids: selected.map(item => item.id) },
      });
    }
  };

  const menuItems: TableActionColumnMenu[] = [
    { label: 'Modifier', icon: <Edit />, onClick: goToEdit, disabled: false },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: onClickDelete,
      disabled: false,
    },
    {
      label: 'Voir détails',
      icon: <Visibility />,
      onClick: goToDetails,
      disabled: false,
    },
  ];

  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const [deleteRessource] = useMutation<DELETE_RESSOURCES, DELETE_RESSOURCESVariables>(
    DO_DELETE_RESSOURCES,
    {
      update: (cache, { data }) => {
        if (data && data.softDeleteRessources) {
          if (variables && operationName) {
            const req: any = cache.readQuery({
              query: operationName,
              variables: variables,
            });
            if (req && req.search && req.search.data) {
              const source = req.search.data;
              const result = data.softDeleteRessources;
              const dif = differenceBy(source, result, 'id');
              cache.writeQuery({
                query: operationName,
                data: {
                  search: {
                    ...req.search,
                    ...{
                      data: dif,
                    },
                  },
                },
                variables: variables,
              });
              // client.writeData({
              //   data: { useCheckedsPartenaireService: differenceBy(checkeds, result, 'id') },
              // });
            }
          }
        }
      },
      onError: errors => {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
        });
      },
      onCompleted: data => {
        if (data && data.softDeleteRessources) {
          displaySnackBar(client, {
            type: 'SUCCESS',
            message: 'Ressource(s) supprimée(s) avec succès',
            isOpen: true,
          });
        }
      },
    },
  );

  const handleClickDelete = (row: any) => {
    setItemToDelete(row);
  };

  const columns: Column[] = [
    { label: 'Type de ressource', name: 'typeRessource' },
    {
      label: 'Item',
      name: 'item.name',
      renderer: (value: any) => {
        return (value && value.item && value.item.name) || '-';
      },
    },
    { label: 'Code', name: 'code' },
    {
      label: 'Libellé',
      name: 'libelle',
    },
    {
      label: 'Date chargement',
      name: 'dateChargement',
      renderer: (value: any) => {
        return value && value.dateChargement && moment(value.dateChargement).format('DD-MM-YYYY');
      },
    },
    {
      label: 'Fichier ou URL',
      name: 'url',
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <Fragment>
            <TableActionColumn
              row={value}
              baseUrl=""
              showResendEmail={false}
              customMenuItems={menuItems}
              handleClickDelete={handleClickDelete}
            />
          </Fragment>
        );
      },
    },
  ];

  const tableProps = {
    isSelectable: true,
    paginationCentered: true,
    columns,
    selected,
    setSelected,
  };

  const DeleteDialogContent = () => {
    if (deleteRow) {
      const name = `${deleteRow.prenom || ''} ${deleteRow.nom || ''}`;
      return (
        <span>
          Êtes-vous sur de vouloir supprimer
          <span style={{ fontWeight: 'bold' }}> {name} </span>?
        </span>
      );
    }
    return <span>Êtes-vous sur de vouloir supprimer ces ressources ?</span>;
  };

  return (
    <>
      <Box width="100%">
        {isOnList && (
          <>
            <div className={classes.searchInputBox}>
              <SearchInput searchPlaceholder="Rechercher une ressource digitale..." />
              <div>
                {selected && selected.length > 0 && (
                  <CustomButton onClick={() => setOpenDeleteDialog(true)}>
                    SUPPRIMER LA SELECTION
                  </CustomButton>
                )}
                <CustomButton color="secondary" className={classes.btnAdd} onClick={goToCreate}>
                  Ajout de ressource digital
                </CustomButton>
              </div>
            </div>
            <WithSearch
              type="ressource"
              props={tableProps}
              WrappedComponent={CustomContent}
              searchQuery={GET_SEARCH_CUSTOM_CONTENT_RESSOURCE}
              optionalMust={[{ term: { isRemoved: false } }]}
            />
            <ConfirmDeleteDialog
              open={openDeleteDialog}
              setOpen={setOpenDeleteDialog}
              content={<DeleteDialogContent />}
              onClickConfirm={onClickConfirmDelete}
            />
          </>
        )}
      </Box>
    </>
  );
};
export default withRouter(RessourceList);
