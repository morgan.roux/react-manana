import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    searchInputBox: {
      padding: '20px 0px 0px 20px',
      display: 'flex',
      justifyContent: 'space-between',
    },
    btnAdd: {
      marginLeft: 8,
      marginRight: 16,
    },
  }),
);

export default useStyles;
