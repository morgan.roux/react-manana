import { RouteComponentProps, withRouter } from 'react-router';
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import useStyles from './styles';
import { Backdrop, FormControlLabel, RadioGroup, Typography, Radio } from '@material-ui/core/';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import InputRow from '../../../../../Common/InputRow';
import CustomSelect from '../../../../../Common/CustomSelect';
import { Label } from '@material-ui/icons';
import { CustomDatePicker } from '../../../../../Common/CustomDateTimePicker';
import FormContainer from '../../../../../Common/FormContainer';
import Dropzone from '../../../../../Common/Dropzone';
import { useQuery } from '@apollo/react-hooks';
import { GET_ALL_ITEMS } from '../../../../../../graphql/Item';
import CustomAutocomplete from '../../../../../Common/CustomAutocomplete';
import { GET_RESSOURCE } from '../../../../../../graphql/Ressource/query';
import { RESSOURCE, RESSOURCEVariables } from '../../../../../../graphql/Ressource/types/RESSOURCE';
interface FormProps {
  handleChange: (e: ChangeEvent<any>) => any;
  handleChangeDate: (date: string) => any;
  handleChangeAutocomplete: (value: any) => void;
  values: any;
  setValues: (values: any) => void;
  match: { params: { id: string | undefined } };
}

const RessourceForm: FC<FormProps & RouteComponentProps> = ({
  location: { pathname },
  handleChange,
  handleChangeDate,
  handleChangeAutocomplete,
  values,
  setValues,
  match: {
    params: { id },
  },
}) => {
  const classes = useStyles({});
  const [documentType, setDocumentType] = useState<'FILE' | 'URL'>('FILE');
  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);
  const handlChangeRadioGroup = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDocumentType((event.target as HTMLInputElement).value as any);
    setSelectedFiles([]);
  };

  const ressourceResult = useQuery<RESSOURCE, RESSOURCEVariables>(GET_RESSOURCE, {
    variables: { id: id || '' },
    skip: !id ? true : false,
    onCompleted: data => {
      if (data && data.ressource) {
        setValues(data.ressource);
        if (data.ressource.url) {
          setDocumentType('URL');
        }
      }
    },
  });

  //list fictive
  const resourceTypelist = [
    { label: 'Ressource type A', value: 'A' },
    { label: 'Ressource type B', value: 'B' },
  ];

  const itemsQuery = useQuery(GET_ALL_ITEMS);

  useEffect(() => {
    if (selectedFiles && selectedFiles.length > 0) {
      setValues(prevState => ({ ...prevState, photo: selectedFiles[0] }));
    }
  }, [selectedFiles]);

  console.log('selectedFiles : ', selectedFiles);

  const items = itemsQuery && itemsQuery.data && itemsQuery.data.items;

  const [operationNameState, setOperationName] = useState();
  const typeServiceList = [
    { label: 'service type A', value: 'A' },
    { label: 'service type B', value: 'B' },
  ];
  const {
    code,
    libelle,
    item,
    dateChargement,
    typeRessource,
    idPartenaireService,
    idItem,
    url,
  } = values;
  return (
    <div className={classes.pharmacieFormRoot}>
      <div className={classes.formContainer}>
        <FormContainer title=" ">
          <InputRow title="Type">
            <CustomSelect
              list={resourceTypelist}
              listId="value"
              index="label"
              name="typeRessource"
              value={typeRessource}
              onChange={handleChange}
            />
          </InputRow>
          <InputRow title="Item">
            <CustomAutocomplete
              id="itemId"
              options={items || []}
              optionLabelKey="name"
              value={item}
              onAutocompleteChange={handleChangeAutocomplete}
              label="Item de la ressource"
              required={true}
            />
          </InputRow>
          <InputRow title="Code">
            <CustomFormTextField
              name="code"
              value={code}
              onChange={handleChange}
              placeholder="Code de la ressource"
              type="text"
            />
          </InputRow>
          <InputRow title="Libellé">
            <CustomFormTextField
              name="libelle"
              value={libelle}
              onChange={handleChange}
              placeholder="Libellé de la ressource"
            />
          </InputRow>
          <InputRow title="Date de chargement">
            <CustomDatePicker
              placeholder=""
              name="dateChargement"
              value={dateChargement}
              onChange={handleChangeDate('dateChargement')}
            />
          </InputRow>
        </FormContainer>
        <RadioGroup
          row={true}
          aria-label="documentType"
          name="documentType"
          value={documentType}
          onChange={handlChangeRadioGroup}
          className={classes.radioGroup}
        >
          <FormControlLabel
            value="FILE"
            control={<Radio color="primary" />}
            label="Fichier de la ressource digital"
          />
          <Typography>ou</Typography>
          <FormControlLabel
            value="URL"
            control={<Radio color="primary" />}
            label="URL de la ressource digital"
          />
        </RadioGroup>
        {documentType === 'FILE' ? (
          <Dropzone
            multiple={false}
            setSelectedFiles={setSelectedFiles}
            selectedFiles={selectedFiles}
          />
        ) : (
          <CustomFormTextField
            name="url"
            value={url}
            onChange={handleChange}
            type="text"
            fullWidth={true}
          />
        )}
      </div>
    </div>
  );
};
export default withRouter(RessourceForm);
