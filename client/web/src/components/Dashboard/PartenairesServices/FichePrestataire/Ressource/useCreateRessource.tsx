import { useMutation, useApolloClient } from '@apollo/react-hooks';
import { DO_CREATE_UPDATE_RESSOURCE } from '../../../../../graphql/Ressource/mutation';
import {
  CREATE_UPDATE_RESSOURCE,
  CREATE_UPDATE_RESSOURCEVariables,
} from '../../../../../graphql/Ressource/types/CREATE_UPDATE_RESSOURCE';
import { useState, useContext } from 'react';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { ContentStateInterface, ContentContext } from '../../../../../AppContext';

export const useCreateUpdateRessource = (
  values: CREATE_UPDATE_RESSOURCEVariables,
  operationName: any,
  variables: any,
  push: any,
  setLoading: any,
  pushUrl: string,
  isOnEdit: boolean,
): [any, boolean, boolean] => {
  const client = useApolloClient();

  const [mutationSuccess, setMutationSuccess] = useState<boolean>(false);
  const [createUpdateRessource, { loading }] = useMutation<
    CREATE_UPDATE_RESSOURCE,
    CREATE_UPDATE_RESSOURCEVariables
  >(DO_CREATE_UPDATE_RESSOURCE, {
    update: (cache, { data }) => {
      if (data && data.createUpdateRessource && values) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables: variables,
          });
          if (req && req.search && req.search.data && !isOnEdit) {
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    total: req.search.total + 1,
                    data: [...req.search.data, data.createUpdateRessource],
                  },
                },
              },
              variables: variables,
            });
          }
        }
      }
    },
    onError: errors => {
      setLoading(false);
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
      displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
    },
    onCompleted: data => {
      setLoading(false);
      if (data && data.createUpdateRessource) {
        push(pushUrl);
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: `Ressource ${isOnEdit ? 'modifiée' : 'crée'} avec succès`,
          isOpen: true,
        });
      }
    },
  });

  return [createUpdateRessource, mutationSuccess, loading];
};
