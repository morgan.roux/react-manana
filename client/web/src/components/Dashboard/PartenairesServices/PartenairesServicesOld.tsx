import React, { FC, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Column } from '../Content/Interface';
import { AddButton } from './../Content/AddButton';
import { getGroupement } from '../../../services/LocalStorage';
import { useGetColumns } from '../columns';
import { ResetUserPasswordModal } from '../ResetUserPasswordModal';
import CustomContent from '../../Common/CustomContent';
import { PARTENAIRE_SERVICE_URL } from '../../../Constant/url';

const PartenairesServices: FC<RouteComponentProps> = ({ history, location: { pathname } }) => {
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [userId, setUserId] = useState<string>('');
  const [userLogin, setUserLogin] = useState<string>('');
  const [userEmail, setUserEmail] = useState<string>('');

  const groupement = getGroupement();
  const id = groupement.id ? groupement.id : null;

  const handleModal = (open: boolean, userId: string, email: string, login: string) => {
    setOpenModal(open);
    setUserId(userId);
    setUserEmail(email);
    setUserLogin(login);
  };

  const columns: Column[] = useGetColumns('PARTENAIRE_SERVICE', history, pathname, handleModal);

  return (
    <>
      <CustomContent
        buttons={
          <AddButton
            onClick={() => history.push(`/db/${PARTENAIRE_SERVICE_URL}/new`)}
            label="Nouveau partenaire"
          />
        }
        type="partenaire"
        columns={columns}
        sortBy={[{ nom: { order: 'desc' } }]}
        filterBy={[{ term: { idGroupement: id } }]}
        searchPlaceholder="Rechercher un service"
        enableGlobalSearch={true}
        inSelectContainer={true}
      />

      <div>
        <ResetUserPasswordModal
          open={openModal}
          setOpen={setOpenModal}
          userId={userId}
          email={userEmail}
          login={userLogin}
        />
      </div>
    </>
  );
};

export default withRouter(PartenairesServices);
