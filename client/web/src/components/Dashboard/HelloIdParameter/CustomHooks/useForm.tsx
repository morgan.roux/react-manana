import { useState, ChangeEvent } from 'react';
import IHelloidSso from '../../../../Interface/HelloidSsoInterface';


const useForm = (
  callback: (data: IHelloidSso) => void,
  defaultState?: IHelloidSso,
) => {
  const initialState: IHelloidSso = defaultState || {
    id: "",
    helloIdUrl : "",
    idGroupement: "",
    helloIDConsumerUrl: "",
    apiKey : "",
    apiPass : "",
    x509Certificate : "",
  };

  const [values, setValues] = useState<IHelloidSso>(initialState);

  const handleSubmit = (e: any) => {
    if (e) e.preventDefault();
    callback(values);
  };

  const handleChange = (e: ChangeEvent<any>) => {
    const { name, value } = e.target;
    e.persist();
    setValues(prevState => ({ ...prevState, [name]: value }));
  };

  return {
    handleChange,
    handleSubmit,
    values,
  };
};

export default useForm;
