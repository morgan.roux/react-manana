import React, { FC } from 'react';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { CustomFormTextField } from '../../../Common/CustomTextField';
import CustomButton from '../../../Common/CustomButton';
import IHelloidSso from '../../../../Interface/HelloidSsoInterface';
import { useForm } from '../CustomHooks';
import CustomTextarea from '../../../Common/CustomTextarea';

interface FormProps {
  defaultData?: IHelloidSso;
  action(data: IHelloidSso): void;
}

const styles = () =>
  createStyles({
    marginBottom: {
      marginBottom: 25,
    },
    marginRight: {
      marginRight: 10,
    },
    titleForm: {
      color: '#1D1D1D',
      marginTop: 0,
    },
    flex: {
      display: 'flex',
      flexDirection: 'row',
    },
    w100: {
      width: '100%',
    },
  });

const Form: FC<RouteComponentProps<any, any, any> & WithStyles & FormProps> = ({
  classes,
  defaultData,
  action,
}) => {
  const { values, handleChange, handleSubmit } = useForm(action, defaultData);
  const { helloIdUrl, helloIDConsumerUrl, apiKey, apiPass, x509Certificate } = values;

  const canSubmit = (): boolean => {
    return helloIdUrl && helloIDConsumerUrl && apiKey && apiPass && x509Certificate ? true : false;
  };

  return (
    <>
      <h3 className={classes.titleForm}>HELLOID</h3>
      <form onSubmit={handleSubmit}>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="helloIdUrl"
            label="Lien Helloid"
            onChange={handleChange}
            value={helloIdUrl}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="helloIDConsumerUrl"
            label="Consumer Url"
            onChange={handleChange}
            value={helloIDConsumerUrl}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomTextarea
            name="x509Certificate"
            label="Certificat X509"
            onChangeTextarea={handleChange}
            value={x509Certificate}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="apiKey"
            label="Utilisateur de l'API"
            onChange={handleChange}
            value={apiKey}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="apiPass"
            label="Mot de passe"
            onChange={handleChange}
            value={apiPass}
            required={true}
          />
        </div>
        <div className={`${classes.marginBottom} ${classes.flex}`}>
          <div className={classes.w100}>
            <CustomButton
              size="large"
              color="primary"
              fullWidth={true}
              disabled={!canSubmit()}
              onClick={handleSubmit}
            >
              ENREGISTRER
            </CustomButton>
          </div>
        </div>
      </form>
    </>
  );
};

export default withStyles(styles, { withTheme: true })(withRouter(Form));
