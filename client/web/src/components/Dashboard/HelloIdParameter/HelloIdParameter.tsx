import React, { FC, useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { Grid, Box } from '@material-ui/core';
import useStyles from './styles';
import Form from './HelloidForm';
import { getGroupement } from '../../../services/LocalStorage';
import IHelloidSso from '../../../Interface/HelloidSsoInterface';
import { useQuery, useMutation, useApolloClient } from '@apollo/react-hooks';
import { GET_HELLOID_GROUPEMENT } from '../../../graphql/HelloIdSso/query';
import { DO_CREATE_HELLOID_SSO, DO_UPDATE_HELLOID_SSO } from '../../../graphql/HelloIdSso/mutation';
import { HELLOIDSSO, HELLOIDSSOVariables } from '../../../graphql/HelloIdSso/types/HELLOIDSSO';
import {
  CREATE_HELLOID_SSO,
  CREATE_HELLOID_SSOVariables,
} from '../../../graphql/HelloIdSso/types/CREATE_HELLOID_SSO';
import { Loader } from '../Content/Loader';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import SnackVariableInterface from '../../../Interface/SnackVariableInterface';
import {
  UPDATE_HELLOID_SSOVariables,
  UPDATE_HELLOID_SSO,
} from '../../../graphql/HelloIdSso/types/UPDATE_HELLOID_SSO';

const HelloIdParameter: FC = () => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [myHelloid, setmyHelloid] = useState<IHelloidSso>();
  const [update, setUpdate] = useState(true);

  const groupement = getGroupement();

  const { data, loading } = useQuery<HELLOIDSSO, HELLOIDSSOVariables>(GET_HELLOID_GROUPEMENT, {
    fetchPolicy: 'cache-and-network',
    variables: {
      idgroupement: (groupement && groupement.id) || '',
    },
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur lors de la récupération du détail d'HelloID`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const [createHelloidSso] = useMutation<CREATE_HELLOID_SSO, CREATE_HELLOID_SSOVariables>(
    DO_CREATE_HELLOID_SSO,
    {
      onCompleted: async data => {
        if (data && data.createHelloIdSso) {
          const snackBarData: SnackVariableInterface = {
            type: 'SUCCESS',
            message: `Création HelloidSso avec succès`,
            isOpen: true,
          };

          displaySnackBar(client, snackBarData);

          setmyHelloid({
            id: data.createHelloIdSso.id,
            apiKey: data.createHelloIdSso.apiKey ? data.createHelloIdSso.apiKey : '',
            apiPass: data.createHelloIdSso.apiPass ? data.createHelloIdSso.apiPass : '',
            helloIDConsumerUrl: data.createHelloIdSso.helloIDConsumerUrl
              ? data.createHelloIdSso.helloIDConsumerUrl
              : '',
            helloIdUrl: data.createHelloIdSso.helloIdUrl ? data.createHelloIdSso.helloIdUrl : '',
            idGroupement: groupement && groupement.id,
            x509Certificate: data.createHelloIdSso.x509Certificate
              ? data.createHelloIdSso.x509Certificate
              : '',
          });
          window.location.reload();
        }
      },
      onError: errors => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: `Erreur lors de la création du HelloidSso`,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      },
    },
  );

  const [updateteHelloidSso] = useMutation<UPDATE_HELLOID_SSO, UPDATE_HELLOID_SSOVariables>(
    DO_UPDATE_HELLOID_SSO,
    {
      onCompleted: data => {
        if (data && data.updateHelloIdSso) {
          const snackBarData: SnackVariableInterface = {
            type: 'SUCCESS',
            message: `Modification HelloidSso avec succès`,
            isOpen: true,
          };

          displaySnackBar(client, snackBarData);
          window.location.reload();
        }
      },
      onError: errors => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: `Erreur lors de la création du HelloidSso`,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      },
    },
  );

  const submitUpdate = (incommingDatas: IHelloidSso) => {
    if (update) updateteHelloidSso({ variables: { ...incommingDatas } });
    else {
      createHelloidSso({
        variables: { ...incommingDatas, idGroupement: groupement && groupement.id },
      });
      setUpdate(true);
    }
  };

  useEffect(() => {
    if (data && data.helloIdSsoByGroupementId) {
      setUpdate(true);
      const myHelloidSso = data.helloIdSsoByGroupementId;
      setmyHelloid({
        id: myHelloidSso.id,
        apiKey: myHelloidSso.apiKey ? myHelloidSso.apiKey : '',
        apiPass: myHelloidSso.apiPass ? myHelloidSso.apiPass : '',
        helloIDConsumerUrl: myHelloidSso.helloIDConsumerUrl ? myHelloidSso.helloIDConsumerUrl : '',
        helloIdUrl: myHelloidSso.helloIdUrl ? myHelloidSso.helloIdUrl : '',
        idGroupement: groupement && groupement.id,
        x509Certificate: myHelloidSso.x509Certificate ? myHelloidSso.x509Certificate : '',
      });
    } else {
      setUpdate(false);
    }
  }, [data]);

  if (loading) return <Loader />;

  return (
    <Box display="flex" justifyContent="center" className={classes.contentBottom}>
      {myHelloid && (
        <Grid item={true} xs={6} className={classes.spacingLeft}>
          <Form defaultData={myHelloid} action={submitUpdate} />
        </Grid>
      )}
      {!myHelloid && (
        <Grid item={true} xs={6} className={classes.spacingLeft}>
          <Form defaultData={undefined} action={submitUpdate} />
        </Grid>
      )}
    </Box>
  );
};
export default withRouter(HelloIdParameter);
