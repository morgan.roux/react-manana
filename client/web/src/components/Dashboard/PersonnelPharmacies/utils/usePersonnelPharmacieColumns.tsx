import { Link } from '@material-ui/core';
import React, { Fragment, useMemo, useState } from 'react';
import {
  PERSONNEL_PHARMACIE_URL,
  PHARMACIE_URL,
  TITULAIRE_PHARMACIE_URL,
} from '../../../../Constant/url';
import CustomAvatar from '../../../Common/CustomAvatar';
import { Column } from '../../Content/Interface';
import UserStatus from '../../Content/UserStatus';
import TableActionColumn from '../../TableActionColumn';
import useDeletePersonnelPharmacie from './useDeletePersonnelPharmacie';

const usePersonnelPharmacieColumns = (
  setOpenDeleteDialog: any,
  history: any,
  viewInModal?: boolean,
  handleOpenDetail?: any,
) => {
  const [selected, setSelected] = useState<any[]>([]);
  const [deleteRow, setDeleteRow] = useState<any>(null);
  const { pathname } = window.location;

  const { deletePpersonnels, loading: deleteLoading } = useDeletePersonnelPharmacie(
    deleteRow ? [deleteRow] : selected,
    setSelected,
    deleteRow,
    setDeleteRow,
  );

  /**
   * Set null deleteRow if selected length > 0
   */
  useMemo(() => {
    if (selected && selected.length > 0) {
      if (deleteRow !== null) setDeleteRow(null);
    }
  }, [selected]);

  const onClickDelete = (row: any) => {
    setDeleteRow(row);
  };

  const onClickConfirmDelete = () => {
    setOpenDeleteDialog(false);
    if (selected.length > 0) {
      deletePpersonnels();
    }

    if (deleteRow && deleteRow.id) {
      deletePpersonnels();
    }
  };

  const columns: Column[] = [
    {
      name: '',
      label: 'Photo',
      renderer: (value: any) => {
        return (
          <CustomAvatar
            name={value && value.fullName}
            url={
              value &&
              value.user &&
              value.user.userPhoto &&
              value.user.userPhoto.fichier &&
              value.user.userPhoto.fichier.publicUrl
            }
          />
        );
      },
    },
    { name: 'civilite', label: 'Civilité' },
    { name: 'fullName', label: 'Nom' },
    {
      name: 'user.anneeNaissance',
      label: 'Année de naissance',
      renderer: (value: any) => (value && value.user && value.user.anneeNaissance) || '-',
    },
    {
      name: 'pharmacie.nom',
      label: 'Pharmacie',
      renderer: (value: any) => {
        const goToPharmacie = () => {
          history.push(`/db/${PHARMACIE_URL}/fiche/${value.pharmacie.id}`);
        };
        return (
          (value.pharmacie && (
            <Link
              color="secondary"
              variant="body2"
              onClick={goToPharmacie}
              style={{ textAlign: 'left', cursor: 'pointer' }}
            >
              {value.pharmacie.nom}
            </Link>
          )) ||
          '-'
        );
      },
    },
    {
      name: '',
      label: 'Titulaire',
      renderer: (value: any) => {
        const goToTitulaire = (id: string) => () => {
          history.push(`/db/${TITULAIRE_PHARMACIE_URL}/fiche/${id}`);
        };
        const titulaires: any[] = (value.pharmacie && value.pharmacie.titulaires) || [];
        return titulaires.map((t: any, index: number) => {
          const isMultiple: boolean = titulaires.length > 1 && index < titulaires.length - 1;
          if (t) {
            return (
              <Fragment key={`titulaire_link_${index}`}>
                <Link
                  color="secondary"
                  component="button"
                  variant="body2"
                  onClick={goToTitulaire(t.id)}
                  style={{ textAlign: 'left', cursor: 'pointer' }}
                >
                  {t && `${t.fullName}`}
                  {isMultiple && (
                    <>
                      ,<span style={{ visibility: 'hidden' }}>&nbsp;</span>
                    </>
                  )}
                </Link>
              </Fragment>
            );
          } else {
            return '-';
          }
        });
      },
    },
    {
      name: 'estAmbassadrice',
      label: 'Rôle',
      renderer: (value: any) => {
        return value.user?.role?.nom || '-';
      },
    },
    {
      name: '',
      label: 'Statut',
      renderer: (value: any) => {
        return <UserStatus user={value.user} pathname={pathname} />;
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <TableActionColumn
            row={value}
            baseUrl={PERSONNEL_PHARMACIE_URL}
            setOpenDeleteDialog={setOpenDeleteDialog}
            handleClickDelete={onClickDelete}
          />
        );
      },
    },
  ];

  return {
    columns,
    deleteRow,
    setDeleteRow,
    onClickConfirmDelete,
    deleteLoading,
    selected,
    setSelected,
  };
};

export default usePersonnelPharmacieColumns;
