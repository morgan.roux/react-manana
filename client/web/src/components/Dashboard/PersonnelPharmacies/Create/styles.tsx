import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
    },
    title: {
      textAlign: 'center',
      letterSpacing: 0,
      color: '#1D1D1D',
      fontSize: '1.5rem',
      margin: '24px auto',
      '@media (max-width: 1024px)': {
        fontSize: '1.25rem',
        margin: '24px 20px',
      },
    },
    loaderContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: 20,
    },
  }),
);

export default useStyles;
