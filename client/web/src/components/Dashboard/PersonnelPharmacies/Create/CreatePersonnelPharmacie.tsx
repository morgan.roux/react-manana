import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { Typography } from '@material-ui/core';
import { AxiosResponse } from 'axios';
import moment from 'moment';
import React, { FC, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { ContentContext, ContentStateInterface } from '../../../../AppContext';
import { PERSONNEL_PHARMACIE_URL } from '../../../../Constant/url';
import { DO_CREATE_UPDATE_PPERSONNEL } from '../../../../graphql/Ppersonnel/mutation';
import {
  CREATE_UPDATE_PPERSONNEL,
  CREATE_UPDATE_PPERSONNELVariables,
} from '../../../../graphql/Ppersonnel/types/CREATE_UPDATE_PPERSONNEL';
import { SEARCH as SEARCH_QUERY } from '../../../../graphql/search/query';
import { SEARCH, SEARCHVariables } from '../../../../graphql/search/types/SEARCH';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { getGroupement } from '../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop';
import { PersonnelPharmacieForm } from '../Form';
import PersonnelPharmacieInterface from '../Interface/PersonnelPharmacieInterface';
import useStyles from './styles';

// Image identifier prefix (for preventing filename conflicts)
export const IDENTIFIER_PREFIX = moment().format('YYYYMMDDhmmss');
export const DIRECTORY = 'avatars';

const CreatePersonnelPharmacie: FC<RouteComponentProps> = ({ history, location: { pathname } }) => {
  const classes = useStyles({});

  const isOnCreate = pathname.includes(`/db/${PERSONNEL_PHARMACIE_URL}/create`);
  const isOnEdit = pathname.includes(`/db/${PERSONNEL_PHARMACIE_URL}/edit`);

  const {
    content: { type, page, rowsPerPage, query, sortBy, filterBy },
  } = useContext<ContentStateInterface>(ContentContext);

  const client = useApolloClient();
  const [startUpload, setStartUpload] = useState<boolean>(false);
  const [fileUploadResult, setFileUploadResult] = useState<AxiosResponse<any> | null>(null);
  const [formData, setFormData] = useState<PersonnelPharmacieInterface | null>(null);
  const [files, setFiles] = useState<File[]>([]);

  const queryVariables: SEARCHVariables = {
    type: [type],
    skip: page * rowsPerPage,
    take: rowsPerPage,
    query,
    sortBy,
    filterBy,
    idPharmacie: '',
  };

  const [createPpersonnel, { loading }] = useMutation<
    CREATE_UPDATE_PPERSONNEL,
    CREATE_UPDATE_PPERSONNELVariables
  >(DO_CREATE_UPDATE_PPERSONNEL, {
    update: (cache, { data }) => {
      if (data && data.createUpdatePpersonnel) {
        const req = cache.readQuery<SEARCH, SEARCHVariables>({
          query: SEARCH_QUERY,
          variables: queryVariables,
        });

        if (req && req.search && req.search.data) {
          cache.writeQuery({
            query: SEARCH_QUERY,
            data: {
              search: {
                ...req.search,
                ...{ data: [...req.search.data, ...[data.createUpdatePpersonnel]] },
              },
            },
            variables: queryVariables,
          });
        }
      }
    },
    onCompleted: data => {
      if (data && data.createUpdatePpersonnel) {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: `${isOnEdit ? 'Modification' : 'Création'} avec succès`,
        });
        history.push(`/db/${PERSONNEL_PHARMACIE_URL}`);
      }
    },
    onError: errors => {
      let errorMessage: string = 'Erreur du serveur';
      errors.graphQLErrors.map(error => {
        if (error.extensions) {
          switch (error.extensions.code) {
            case 'EMAIL_ALREADY_EXIST':
              errorMessage = 'Email déjà utilisé';
              break;
            default:
              break;
          }
        }
      });
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errorMessage,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const groupement = getGroupement();

  const submitCreate = (data: PersonnelPharmacieInterface) => {
    // Start the file upload
    setStartUpload(true);
    setFormData(data);
  };

  // Run the user creation only after successfull image upload or if no file is specified and input data is available
  useEffect(() => {
    if (
      files.length > 0 &&
      fileUploadResult &&
      fileUploadResult.status === 200 &&
      fileUploadResult.statusText === 'OK' &&
      formData
    ) {
      delete formData.pharmacie;
      createPpersonnel({
        variables: {
          input: {
            ...formData,
            idPharmacie: formData.idPharmacie,
            idGroupement: groupement.id,
            estAmbassadrice: formData.estAmbassadrice,
            sortie: formData.sortie ? 1 : 0,
          },
        },
      });
    } else if (files.length === 0 && formData) {
      delete formData.pharmacie;
      createPpersonnel({
        variables: {
          input: {
            ...formData,
            idPharmacie: formData.idPharmacie,
            idGroupement: groupement.id,
            estAmbassadrice: formData.estAmbassadrice,
            sortie: formData.sortie ? 1 : 0,
          },
        },
      });
    }
  }, [files, fileUploadResult, formData]);

  const loadingMsg = (): string => {
    if (isOnCreate) return 'Création en cours...';
    if (isOnEdit) return 'Modification en cours...';
    return 'Chargement en cours...';
  };

  return (
    <div className={classes.root}>
      <div className={classes.loaderContainer}>
        {(startUpload || loading) && <Backdrop value={loadingMsg()} />}
      </div>
      <Typography variant="h6" gutterBottom={true} className={classes.title}>
        {isOnEdit ? 'Modifier un' : 'Créer un nouveau'} personnel de pharmacie
      </Typography>
      <PersonnelPharmacieForm
        submit={submitCreate}
        loading={loading || startUpload}
        setStartUpload={setStartUpload}
        startUpload={startUpload}
        setFileUploadResult={setFileUploadResult}
        withImagePreview={true}
        fileIdentifierPrefix={IDENTIFIER_PREFIX}
        setFilesOut={setFiles}
        uploadDirectory={DIRECTORY}
      />
    </div>
  );
};

export default withRouter(CreatePersonnelPharmacie);
