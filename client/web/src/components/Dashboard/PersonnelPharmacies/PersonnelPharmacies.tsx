import { Add } from '@material-ui/icons';
import React, { useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import mutationSuccessImg from '../../../assets/img/mutation-success.png';
import { PERSONNEL_PHARMACIE_URL } from '../../../Constant/url';
import { ConfirmDeleteDialog } from '../../Common/ConfirmDialog';
import CustomButton from '../../Common/CustomButton';
import CustomContent from '../../Common/newCustomContent';
import SearchInput from '../../Common/newCustomContent/SearchInput';
import SubToolbar from '../../Common/newCustomContent/SubToolbar';
import NoItemContentImage from '../../Common/NoItemContentImage';
import useStyles from './styles';
import usePersonnelPharmacieColumns from './utils/usePersonnelPharmacieColumns';
import {
  useButtonHeadAction,
  useCheckedPharmacie,
  useCreatePharmacie,
  useGetPharmacie,
} from './utils/utils';

interface IPersonnelPharmacieProps {
  listResult: any;
}

const PersonnelPharmacies: React.FC<IPersonnelPharmacieProps & RouteComponentProps> = props => {
  const {
    listResult,
    history,
    history: { push },
    location: { pathname },
    match: { params },
  } = props;
  const classes = useStyles({});
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [openHistory, setOpenHistory] = useState<boolean>(false);
  const [userId, setUserId] = useState<string>('');
  const [userEmail, setUserEmail] = useState<string>('');

  const handleModal = (open: boolean, userId: string, email: string) => {
    setOpenModal(open);
    setUserId(userId);
    setUserEmail(email);
  };

  const handleClickHistory = (open: boolean, userId: string) => {
    setOpenHistory(open);
    setUserId(userId);
  };

  const {
    columns,
    deleteRow,
    onClickConfirmDelete,
    deleteLoading,
    selected,
    setSelected,
  } = usePersonnelPharmacieColumns(setOpenDeleteDialog, history, undefined, null);

  const checkedsPharmacie = useCheckedPharmacie();
  // const { handleChange, values, setValues } = usePharmacie();

  const [createUpdatePharmacie, mutationSuccess] = useCreatePharmacie({
    // variables: { ...values, idGroupement },
  });
  const [goToAddPharmacie, goBack] = useButtonHeadAction(push);
  const [getPharmacie, pharmacie] = useGetPharmacie();

  const isOnList = pathname === `/db/${PERSONNEL_PHARMACIE_URL}`;
  const isOnCreate = pathname === `/db/${PERSONNEL_PHARMACIE_URL}/create`;
  const isOnEdit: boolean = pathname.startsWith(`/db/${PERSONNEL_PHARMACIE_URL}/edit`);

  const mutationSuccessTitle = `${
    isOnCreate ? 'Ajout' : 'Modification'
  } du personnel de pharmacie réussi`;

  const mutationSuccessSubTitle = `Le nouveau personnel de pharmacie que vous venez ${
    isOnCreate ? "d'ajouter" : 'de modifier'
  } est désormais dans la liste des personnels de pharmacie du groupement.`;

  const { personnelId } = params as any;
  const EDIT_URL = `/db/${PERSONNEL_PHARMACIE_URL}/edit/${personnelId}`;

  const children = (
    <div className={classes.childrenRoot}>
      {((checkedsPharmacie && checkedsPharmacie.length > 0) || !isOnList) && (
        <CustomButton
          color="default"
          onClick={() => (!isOnList ? goBack() : setOpenDeleteDialog(true))}
        >
          {!isOnList ? 'Annuler' : 'Supprimer la sélection'}
        </CustomButton>
      )}

      <CustomButton
        color="secondary"
        startIcon={!isOnList ? null : <Add />}
        onClick={() => (!isOnList ? createUpdatePharmacie : goToAddPharmacie())}
      >
        {isOnCreate ? 'Enregistrer' : isOnEdit ? 'Modifier' : 'Ajouter un personnel de pharmacie'}
      </CustomButton>
    </div>
  );

  const listPersonnelPharmacie = (
    <CustomContent
      selected={selected}
      setSelected={setSelected}
      {...{ listResult, columns }}
      isSelectable={false}
    />
  );

  const pharmacieForm = null;

  const actionSuccess = (
    <div className={classes.mutationSuccessContainer}>
      <NoItemContentImage
        src={mutationSuccessImg}
        title={mutationSuccessTitle}
        subtitle={mutationSuccessSubTitle}
      >
        <CustomButton color="default" onClick={goBack}>
          Retour à la liste
        </CustomButton>
      </NoItemContentImage>
    </div>
  );

  const DeleteDialogContent = () => {
    if (deleteRow) {
      return (
        <span>
          Êtes-vous sur de vouloir supprimer
          <span style={{ fontWeight: 'bold' }}> {deleteRow.fullName || ''} </span>?
        </span>
      );
    }
    return <span>Êtes-vous sur de vouloir supprimer ces personnels ?</span>;
  };

  useEffect(() => {
    if (pathname === EDIT_URL && personnelId) {
      getPharmacie({ variables: { id: personnelId } });
    }
  }, [pathname, personnelId]);

  useEffect(() => {
    if (pharmacie) {
      // setValues({
      //   id: personnel.id,
      //   codeService: (personnel.service && personnel.service.code) || '',
      //   respHierarch: null,
      //   sortie: personnel.sortie as any,
      //   dateSortie: personnel.dateSortie,
      //   civilite: personnel.civilite,
      //   nom: personnel.nom,
      //   prenom: personnel.prenom,
      //   sexe: personnel.sexe,
      //   adresse1: personnel.contact ? personnel.contact.adresse1 : '',
      //   adresse2: personnel.contact ? personnel.contact.adresse2 : '',
      //   telBureau: personnel.contact ? personnel.contact.telProf : '',
      //   telDomicile: personnel.contact ? personnel.contact.telPerso : '',
      //   telMobProf: personnel.contact ? personnel.contact.telMobProf : '',
      //   telMobPerso: personnel.contact ? personnel.contact.telMobPerso : '',
      //   faxBureau: personnel.contact ? personnel.contact.faxProf : '',
      //   faxDomicile: personnel.contact ? personnel.contact.faxPerso : '',
      //   cp: personnel.contact ? personnel.contact.cp : '',
      //   ville: personnel.contact ? personnel.contact.ville : '',
      //   mailProf: personnel.contact ? personnel.contact.mailProf : '',
      //   mailPerso: personnel.contact ? personnel.contact.mailPerso : '',
      //   commentaire: personnel.commentaire,
      //   idGroupement: personnel.idGroupement,
      // });
    }
  }, [pharmacie]);

  const goToList = () => push(`/db/${PERSONNEL_PHARMACIE_URL}`);
  const goToAdd = () => push(`/db/${PERSONNEL_PHARMACIE_URL}/create`);

  return (
    <div className={classes.container}>
      <SubToolbar
        title={
          isOnCreate
            ? `Ajout nouveau personnel de pharmacie`
            : isOnEdit
            ? `Modification personnel de pharmacie`
            : 'Liste du personnel de la pharmacie'
        }
        dark={!isOnList}
        withBackBtn={false}
        onClickBack={goToList}
      >
        <CustomButton color="secondary" startIcon={<Add />} onClick={goToAdd}>
          Ajouter un personnel
        </CustomButton>
      </SubToolbar>
      <div className={classes.searchInputBox}>
        <SearchInput searchPlaceholder="Rechercher un personnel de pharmacie" />
      </div>
      {!isOnList ? pharmacieForm : listPersonnelPharmacie}
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
    </div>
  );
};

export default withRouter(PersonnelPharmacies);
