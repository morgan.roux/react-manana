import { ChangeEvent, MouseEvent, useState } from 'react';
import PersonnelPharmacieInterface from '../Interface/PersonnelPharmacieInterface';
import PharmacieInterface from '../Interface/PharmacieInterface';

const usePersonnelPharmacieForm = (
  callback: (data: PersonnelPharmacieInterface) => void,
  defaultState?: PersonnelPharmacieInterface,
) => {
  const initialPharmacie = {
    id: '',
    nom: '',
    __typename: 'Pharmacie',
  };

  const initialState: PersonnelPharmacieInterface = defaultState || {
    id: null,
    pharmacie: initialPharmacie,
    idPharmacie: '',
    nom: '',
    prenom: '',
    civilite: '',
    telBureau: '',
    telMobile: '',
    mail: '',
    estAmbassadrice: false,
    sortie: false,
    commentaire: '',
  };

  const [values, setValues] = useState<PersonnelPharmacieInterface>(initialState);
  const [selectedFiles, setSelectedFiles] = useState<File[]>();

  const handleSubmit = (e: MouseEvent<any>) => {
    if (e) {
      e.preventDefault();
      e.stopPropagation();
    }
    callback(values);
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement | any>) => {
    if (e && e.target) {
      const { name, value, checked } = e.target;
      setValues(prevState => ({ ...prevState, [name]: value }));
      if (name === 'estAmbassadrice' || name === 'sortie') {
        setValues(prevState => ({ ...prevState, [name]: checked }));
      }
    }
  };

  const handleChangeAutocomplete = (value: any) => {
    setValues(prevState => ({ ...prevState, pharmacie: value, idPharmacie: value.id }));
  };

  return {
    selectedFiles,
    setSelectedFiles,
    handleChange,
    handleChangeAutocomplete,
    handleSubmit,
    values,
    setValues,
  };
};

export default usePersonnelPharmacieForm;
