import React, { FC, useState, useEffect } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { getGroupement, getUser } from '../../../services/LocalStorage';
import { Column } from '../Content/Interface';
import { AddButton } from '../Content/AddButton';
import { useQuery } from '@apollo/react-hooks';
// import { MY_PHARMACIE } from '../../../graphql/Pharmacie/types/MY_PHARMACIE';
// import { GET_MY_PHARMACIE } from '../../../graphql/Pharmacie/query';
import { useGetColumns } from '../columns';
import { ResetUserPasswordModal } from '../ResetUserPasswordModal';
import CustomContent from '../../Common/CustomContent';
import { FieldsOptions } from '../../Common/CustomContent/interfaces';
import ICurrentPharmacieInterface from '../../../Interface/CurrentPharmacieInterface';
import { GET_CURRENT_PHARMACIE } from '../../../graphql/Pharmacie/local';
import { ME_me } from '../../../graphql/Authentication/types/ME';
import { SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT } from '../../../Constant/roles';
import HistoriquePersonnel from '../HistoriquePersonnel';
import { PERSONNEL_PHARMACIE_URL } from '../../../Constant/url';

const placeholder = 'Tapez ici';

const searchFields: FieldsOptions[] = [
  {
    name: 'nom',
    label: 'Nom Personnel',
    value: '',
    type: 'Search',
    extraNames: ['prenom'],
    placeholder,
    ordre: 1,
  },
  {
    name: 'titulaires',
    label: 'Titulaire',
    value: '',
    type: 'Search',
    placeholder,
    // extraNames: ['titulaires.nom', 'titulaires.prenom'],
    extraNames: ['titulaires.fullName'],
    ordre: 2,
  },
  {
    name: 'numIvrylab',
    label: 'Code IVRYLAB',
    value: '',
    type: 'Search',
    placeholder,
    ordre: 3,
  },
  {
    name: 'cip',
    label: 'CIP',
    value: '',
    type: 'Search',
    placeholder,
    ordre: 4,
  },
  {
    name: 'cp',
    label: 'Code postal',
    value: '',
    type: 'Search',
    placeholder,
    ordre: 5,
  },
];

const PersonnelPharmacies: FC<RouteComponentProps> = ({ history, location: { pathname } }) => {
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [openHistory, setOpenHistory] = useState<boolean>(false);
  const [userId, setUserId] = useState<string>('');
  const [userLogin, setUserLogin] = useState<string>('');
  const [userEmail, setUserEmail] = useState<string>('');

  const groupement = getGroupement();
  const id = groupement.id ? groupement.id : null;

  const currentUser: ME_me = getUser();

  // const getMyPharmacie = useQuery<MY_PHARMACIE>(GET_MY_PHARMACIE);
  // const idPharmacie =
  //   (getMyPharmacie &&
  //     getMyPharmacie.data &&
  //     getMyPharmacie.data.me &&
  //     getMyPharmacie.data.me.pharmacie &&
  //     getMyPharmacie.data.me.pharmacie.id) ||
  //   null;

  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const [currentPharmacie, setcurrentPharmacie] = useState<string>('');

  useEffect(() => {
    if (
      myPharmacie &&
      myPharmacie.data &&
      myPharmacie.data.pharmacie &&
      myPharmacie.data.pharmacie.id
    ) {
      setcurrentPharmacie(myPharmacie.data.pharmacie.id);
    }
  }, [myPharmacie]);

  // const term = idPharmacie
  //   ? [{ term: { idGroupement: id } }, { term: { idPharmacie: idPharmacie } }]
  //   : [{ term: { idGroupement: id } }];

  const handleModal = (open: boolean, userId: string, email: string,login:string) => {
    setOpenModal(open);
    setUserId(userId);
    setUserEmail(email);
    setUserLogin(login)
  };

  const handleClickHistory = (open: boolean, userId: string) => {
    setOpenHistory(open);
    setUserId(userId);
  };

  const columns: Column[] = useGetColumns(
    'PERSONNEL_PHARMACIE',
    history,
    pathname,
    handleModal,
    handleClickHistory,
  );

  const goToCreatePersonnelPharmacie = () => {
    history.push(`/db/${PERSONNEL_PHARMACIE_URL}/new`);
  };

  let filterBy: any[] = [{ term: { idGroupement: id } }];

  if (
    currentUser &&
    currentUser.role &&
    currentUser.role.code !== SUPER_ADMINISTRATEUR &&
    currentUser.role.code !== ADMINISTRATEUR_GROUPEMENT
  ) {
    filterBy = filterBy.concat([{ term: { 'pharmacie.id': currentPharmacie } }]);
  }

  return (
    <>
      <CustomContent
        buttons={<AddButton onClick={goToCreatePersonnelPharmacie} label="ajouter un personnel" />}
        searchPlaceholder="Rechercher un personnel"
        searchInputsLabel="Filtre de recherche de personnel de pharmacie"
        type="ppersonnel"
        columns={columns}
        sortBy={[{ nom: { order: 'desc' } }]}
        filterBy={filterBy}
        searchInputs={searchFields}
        enableGlobalSearch={true}
        inSelectContainer={false}
      />

      <div>
        <ResetUserPasswordModal
          open={openModal}
          setOpen={setOpenModal}
          userId={userId}
          email={userEmail}
          login={userLogin}
        />
      </div>
      <HistoriquePersonnel open={openHistory} setOpen={setOpenHistory} userId={userId} />
    </>
  );
};

export default withRouter(PersonnelPharmacies);
