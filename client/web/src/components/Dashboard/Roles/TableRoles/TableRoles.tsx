import {
  Box,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableSortLabel,
} from '@material-ui/core';
import { Favorite } from '@material-ui/icons';
import EditIcon from '@material-ui/icons/Edit';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { ROLES_SEARCH_search_data_Role } from '../../../../graphql/Role/types/ROLES_SEARCH';
import { capitalizeFirstLetter } from '../../../../utils/capitalizeFirstLetter';
import CommonPagination from '../../../Common/CommonPagination';
import { useStyles } from '../style';
import StyledTableBodyCell from './StyledTableBodyCell';
import StyledTableCell from './StyledTableCell';

interface TablRolesProps {
  roles: Array<ROLES_SEARCH_search_data_Role>;
  order: 'asc' | 'desc';
  setOrder: React.Dispatch<React.SetStateAction<'asc' | 'desc'>>;
  take: number;
  setTake(take: number): void;
  skip: number;
  setSkip(skip: number): void;
  total: number;
}

const TableRoles: FC<TablRolesProps & RouteComponentProps> = props => {
  const {
    roles,
    history: { push },
    take,
    skip,
    order,
    setTake,
    setSkip,
    setOrder,
    total,
  } = props;
  const classes = useStyles({});

  const handleSort = () => {
    setOrder(order === 'asc' ? 'desc' : 'asc');
  };

  return (
    <Box className={classes.container}>
      <TableContainer className={classes.tableContainer} component={Box}>
        <Table stickyHeader className={classes.table} size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <StyledTableCell onClick={handleSort} align="left">
                <TableSortLabel
                  direction={order}
                  style={{ color: 'white' }}
                  className={classes.order}
                >
                  Nom
                </TableSortLabel>
              </StyledTableCell>
              <StyledTableCell align="left"></StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {roles &&
              roles.map((item, index: number) => {
                const { nom, id } = item;
                const capitalizedName = nom && capitalizeFirstLetter(nom);
                return (
                  <TableRow key={index}>
                    <StyledTableBodyCell align="left">{capitalizedName}</StyledTableBodyCell>
                    <TableCell align="right">
                      <IconButton
                        color="secondary"
                        onClick={() => push(`/db/roles/appetence/${id}`)}
                      >
                        <Favorite />
                      </IconButton>
                      <IconButton
                        color="secondary"
                        onClick={() => push(`/db/roles/accessPermision/${id}`)}
                      >
                        <EditIcon />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
      <CommonPagination
        {...{ take, skip, total, setTake, setSkip }}
        className={classes.pagination}
      />
    </Box>
  );
};

export default withRouter(TableRoles);
