import React, { FC, useState } from 'react';
import { Box } from '@material-ui/core';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import SubHeader from './SubHeader';
import { useQuery, useApolloClient, useMutation } from '@apollo/react-hooks';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import {
  CREATE_UPDATE_ROLE_TRAITEMENT,
  CREATE_UPDATE_ROLE_TRAITEMENTVariables,
} from '../../../graphql/Traitement/types/CREATE_UPDATE_ROLE_TRAITEMENT';
import { DO_CREATE_UPDATE_ROLE_TRAITEMENT } from '../../../graphql/Traitement/mutation';
import { ROLEVariables, ROLE } from '../../../graphql/Role/types/ROLE';
import { GET_ROLE } from '../../../graphql/Role/query';
import { capitalizeFirstLetter } from '../../../utils/capitalizeFirstLetter';
import PermissionAccessList from '../../Common/PermissionAccessList';
import Backdrop from '../../Common/Backdrop';

const AccessPermision: FC<RouteComponentProps> = ({ match: { params }, history: { push } }) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const { code: id } = params as any;

  const [selected, setSelected] = useState<any[]>([]);
  const selectedCodes: string[] =
    (selected && selected.map((item: any) => item && item.code)) || [];

  const { data: dataRole, loading: loadingRole } = useQuery<ROLE, ROLEVariables>(GET_ROLE, {
    variables: { id },
    onCompleted: data => {
      if (data && data.role && data.role.roleTraitements) {
        const traitements = data.role.roleTraitements.map(item => {
          if (item && item.codeTraitement) {
            return { code: item.codeTraitement };
          }
        });

        if (traitements) {
          setSelected(traitements);
        }
      }
    },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        });
      });
    },
  });

  const roleName =
    (dataRole &&
      dataRole.role &&
      dataRole.role.nom &&
      (capitalizeFirstLetter(dataRole.role.nom) as string)) ||
    '';
  const codeRole = (dataRole && dataRole.role && dataRole.role.code) || '';

  const roleTraitements =
    (dataRole && dataRole.role && dataRole.role.roleTraitements && dataRole.role.roleTraitements) ||
    [];

  const [updateRoleTraitement, { loading: mutationLoading }] = useMutation<
    CREATE_UPDATE_ROLE_TRAITEMENT,
    CREATE_UPDATE_ROLE_TRAITEMENTVariables
  >(DO_CREATE_UPDATE_ROLE_TRAITEMENT, {
    variables: { codeRole, codeTraitements: selectedCodes },
    onCompleted: data => {
      if (data && data.createUpdateRoleTraitement && data.createUpdateRoleTraitement.id) {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: `Nouveau droit d'accès enregistré`,
          isOpen: true,
        });
        push('/db/roles');
      }
    },
    onError: error => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: error.message,
        isOpen: true,
      });
    },
  });

  const handleSave = () => {
    updateRoleTraitement();
  };

  return (
    <Box>
      {(loadingRole || mutationLoading) && (
        <Backdrop value={`${mutationLoading ? 'Enregistrement' : 'Chargement'} en cours...`} />
      )}
      <SubHeader {...{ handleSave, roleName }} />
      <Box className={classes.container}>
        <Box className={classes.content}>
          {roleTraitements && (
            <PermissionAccessList
              selected={selected}
              setSelected={setSelected}
              withFilterByGroupement={false}
              withAddBtn={false}
            />
          )}
        </Box>
      </Box>
    </Box>
  );
};

export default withRouter(AccessPermision);
