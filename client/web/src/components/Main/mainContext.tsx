import React, {
  FC,
  createContext,
  Dispatch,
  SetStateAction,
  useState,
  useContext,
  ReactNode,
  ReactElement,
} from 'react';

export interface ContentInterface {
  laboratoireVariables: {
    type: string[];
    must: any[];
    mustNot: any[];
    take: number;
    skip: number;
    searchTxt: string;
    typesActive: number[];
  };
}

export interface ContentStateMainInterface {
  content: ContentInterface;
  setContent: Dispatch<SetStateAction<ContentInterface>>;
}

export const defaultContent: ContentInterface = {
  laboratoireVariables: {
    type: ['laboratoire'],
    must: [],
    mustNot: [],
    take: 10,
    skip: 0,
    searchTxt: '',
    typesActive: [],
  },
};

export interface MainContextProviderProps {
  defaults?: Partial<ContentInterface>;
  children?: ReactNode;
}

export const useContent = (overrides?: Partial<ContentInterface>): ContentStateMainInterface => {
  const [content, setContent] = useState<ContentInterface>({
    ...defaultContent,
    ...overrides,
  });
  return { content, setContent };
};

const defaultContentState: ContentStateMainInterface = {
  content: defaultContent,
  setContent: (): void => {},
};

export const MainContext = createContext<ContentStateMainInterface>(defaultContentState);

export const useMainContext = (): ContentStateMainInterface => {
  return useContext(MainContext);
};

export const MainContextProvider: FC<MainContextProviderProps> = ({
  children,
  defaults,
}): ReactElement => {
  const [content, setContent] = useState<ContentInterface>({
    ...defaultContent,
    ...defaults,
  });
  return <MainContext.Provider value={{ content, setContent }}>{children}</MainContext.Provider>;
};
