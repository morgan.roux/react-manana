import React, { FC, useState, useEffect } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Box, Button, Paper, Typography, IconButton, Tooltip } from '@material-ui/core';
import { GET_COMMANDE_LIGNES } from '../../../../../graphql/Commande/query';
import { Commande_Lignes_commandeLignes } from '../../../../../graphql/Commande/types/Commande_Lignes';
import { commande } from '../../../../../graphql/Commande/types/commande';
import { useQuery } from '@apollo/react-hooks';
import ICurrentPharmacieInterface from '../../../../../Interface/CurrentPharmacieInterface';
import { GET_CURRENT_PHARMACIE } from '../../../../../graphql/Pharmacie/local';
// import Image1 from '../../../../../assets/img/panier-liste-1.png';
import ArticleImage from '../../Panier/Article/Image/Image';
import ArticleTitle from '../../Panier/Article/Title/Title';
import UserActionInfo from '../../../../Common/UserAction/UserActionInfo/UserActionInfo';
import ListRow from '../../Panier/Article/ListBox/ListRow/ListRow';
import PanierResume from '../../Panier/PanierResume/PanierResume';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import moment from 'moment';
import Backdrop from '../../../../Common/Backdrop';
import { PharmacieMinimInfo } from '../../../../../graphql/Pharmacie/types/PharmacieMinimInfo';

interface CommandeDetailProps {
  match: {
    params: { id: string };
  };
}
const CommandeDetail: FC<CommandeDetailProps & RouteComponentProps<any, any, any>> = ({
  match,
  history,
}) => {
  const classes = useStyles({});
  const commandeId = match.params.id;

  // take currentPharmacie
  const [currentPharmacie, setCurrentPharmacie] = useState<PharmacieMinimInfo | null>(null);
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

  useEffect(() => {
    if (myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie) {
      setCurrentPharmacie(myPharmacie.data.pharmacie);
    }
  }, [myPharmacie]);

  const commandeLignes = useQuery<commande>(GET_COMMANDE_LIGNES, {
    variables: {
      id: commandeId,
      idPharmacie: currentPharmacie ? currentPharmacie.id : '',
      take: 10,
      skip: 0,
    },
  });

  const goToDetails = (id: string) => {
    history.push(`/catalogue-produits/card/${id}`, { from: '/suivi-commandes' });
  };

  const goBack = () => {
    history.push(`/suivi-commandes`);
  };

  let infos = ``;
  if (
    commandeLignes &&
    commandeLignes.data &&
    commandeLignes.data.commande &&
    commandeLignes.data.commande.operation &&
    commandeLignes.data.commande.operation.libelle
  ) {
    infos = `${commandeLignes.data.commande.operation.libelle} `;
  }
  if (
    commandeLignes &&
    commandeLignes.data &&
    commandeLignes.data.commande &&
    commandeLignes.data.commande.codeReference &&
    commandeLignes.data.commande.dateCommande &&
    commandeLignes.data.commande.commandeStatut &&
    commandeLignes.data.commande.commandeStatut.libelle
  ) {
    infos += `Du ${moment(commandeLignes.data.commande.dateCommande).format(
      'DD/MM/YYYY',
    )} - Rèf Cde : ${commandeLignes.data.commande.codeReference} / ${
      commandeLignes.data.commande.commandeStatut.libelle
    }`;
  }

  return (
    <Box>
      {commandeLignes.loading && !commandeLignes.data && <Backdrop />}
      <Box className={classes.titleContainer}>
        <Tooltip title="Retour à la liste de commande">
          <IconButton onClick={goBack} className={classes.backIconButton}>
            <ArrowBackIcon />
          </IconButton>
        </Tooltip>
        <Typography variant="h6">
          Historique de commande: <i>{infos}</i>
        </Typography>
      </Box>
      <Box className={classes.contentBodyContainer} paddingRight="31px">
        <Box display="flex" flexDirection="row" flexWrap="wrap" flex="1">
          {commandeLignes &&
            commandeLignes.data &&
            commandeLignes.data.commande &&
            commandeLignes.data.commande.commandeLignes &&
            commandeLignes.data.commande.commandeLignes.map(
              (ligne: Commande_Lignes_commandeLignes | null, index: number) => (
                <Paper className={classes.panierListeItem} key={index}>
                  <ArticleImage
                    src={
                      ligne &&
                      ligne.produitCanal &&
                      ligne.produitCanal.produit &&
                      ligne.produitCanal.produit.produitPhoto &&
                      ligne.produitCanal.produit.produitPhoto.fichier &&
                      ligne.produitCanal.produit.produitPhoto.fichier.urlPresigned
                        ? ligne.produitCanal.produit.produitPhoto.fichier.urlPresigned
                        : ''
                    }
                  />
                  <ArticleTitle
                    title={
                      ligne &&
                      ligne.produitCanal &&
                      ligne.produitCanal.produit &&
                      ligne.produitCanal.produit.libelle
                        ? ligne.produitCanal.produit.libelle
                        : ''
                    }
                  />
                  <ListRow
                    label={'Code'}
                    value={
                      ligne &&
                      ligne.produitCanal &&
                      ligne.produitCanal.produit &&
                      ligne.produitCanal.produit.produitCode &&
                      ligne.produitCanal.produit.produitCode.code
                        ? ligne.produitCanal.produit.produitCode.code
                        : ''
                    }
                    pink={false}
                  />
                  <ListRow
                    label={'Laboratoire'}
                    value={
                      ligne &&
                      ligne.produitCanal &&
                      ligne.produitCanal.produit &&
                      ligne.produitCanal.produit.produitTechReg &&
                      ligne.produitCanal.produit.produitTechReg.laboExploitant &&
                      ligne.produitCanal.produit.produitTechReg.laboExploitant.nomLabo
                        ? ligne.produitCanal.produit.produitTechReg.laboExploitant.nomLabo
                        : ''
                    }
                    pink={false}
                  />
                  <ListRow label={'Carton'} value={''} pink={false} />
                  <ListRow
                    label={'Quantités'}
                    value={ligne && ligne.quantiteCdee ? ligne.quantiteCdee : ''}
                    pink={false}
                  />
                  <ListRow
                    label={'Tarif HT'}
                    value={`${ligne && ligne.prixBaseHT ? ligne.prixBaseHT : ''}€`}
                    pink={false}
                  />
                  <ListRow
                    label={'Remise'}
                    value={`${
                      ligne && ligne.remiseGamme
                        ? ligne.remiseGamme
                        : ligne && ligne.remiseLigne
                        ? ligne.remiseLigne
                        : '0'
                    }€`}
                    pink={true}
                  />
                  <ListRow
                    label={'Prix Net'}
                    value={ligne && ligne.prixNetUnitaireHT ? ligne.prixNetUnitaireHT : ''}
                    pink={false}
                  />
                  <ListRow
                    label={'Total Net'}
                    value={ligne && ligne.prixTotalHT ? ligne.prixTotalHT : ''}
                    pink={false}
                  />
                  <ListRow
                    label={'Stock plateforme'}
                    value={
                      ligne && ligne.produitCanal && ligne.produitCanal.qteStock
                        ? ligne.produitCanal.qteStock
                        : ''
                    }
                    pink={false}
                  />
                  {(ligne?.produitCanal?.produit as any)?.qteStockPharmacie ? (
                    <ListRow
                      label={'Stock Pharmacie'}
                      value={(ligne?.produitCanal?.produit as any)?.qteStockPharmacie || ''}
                      pink={false}
                    />
                  ) : (
                    <ListRow label={'NR'} value={''} pink={false} />
                  )}
                  <UserActionInfo />
                  <Box
                    display="flex"
                    flexDirection="row"
                    marginTop="16px"
                    justifyContent="space-between"
                  >
                    <Button
                      className={classes.panierListeButtonLeft}
                      onClick={() =>
                        goToDetails(ligne && ligne.produitCanal ? ligne.produitCanal.id : '')
                      }
                    >
                      voir détails
                    </Button>
                  </Box>
                </Paper>
              ),
            )}
        </Box>
        <PanierResume
          currentPanier={
            commandeLignes && commandeLignes.data && commandeLignes.data.commande
              ? commandeLignes.data.commande
              : null
          }
        />
      </Box>
    </Box>
  );
};

export default withRouter(CommandeDetail);
