import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    panierListeItem: {
      /*width: '23%',*/
      width: '342px',
      display: 'flex',
      flexDirection: 'column',
      padding: '10px 12px 14px',
      margin: '0 0 24px 40px',
      '& .MuiTypography-root': {
        fontSize: '0.875rem',
      },
      '@media (max-width: 1200px)': {
        width: '31.5%',
      },
      '@media (max-width: 768px)': {
        width: '48%',
      },
      '@media (max-width: 599px)': {
        width: 320,
      },
      '@media (max-width: 375px)': {
        width: 290,
      },
    },
    panierListeButtonLeft: {
      background: '#EFEFEF',
      textTransform: 'lowercase',
      fontFamily: 'Montserrat',
      fontSize: '0.75rem',
      marginRight: theme.spacing(1),
    },
    contentBodyContainer: {
      // paddingLeft: '31px',
      paddingTop: '31px',
      display: 'flex',
      flexDirection: 'row',
    },
    titleContainer: {
      margin: '24px 31px 0 31px',
      display: 'flex',
      alignItems: 'center',
    },
    backIconButton: {
      marginRight: '16px',
    },
  }),
);
