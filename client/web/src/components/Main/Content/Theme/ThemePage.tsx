import React, { useEffect } from 'react';
import { THEMES_LIST, CODE_THEME } from '../../../../Constant/parameter';
import { Radio, Typography } from '@material-ui/core';
import CircleCheckedFilled from '@material-ui/icons/CheckCircle';
import ThemePreview from './ThemePreview';
import useStyles from './styles';
import classnames from 'classnames';
import { useParameters, useValueParameter } from '../../../../utils/getValueParameter';
import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import ApolloClient from 'apollo-client';
import {
  UPDATE_PARAMETER_VALUES,
  UPDATE_PARAMETER_VALUESVariables,
} from '../../../../graphql/Parametre/types/UPDATE_PARAMETER_VALUES';
import { DO_UPDATE_PARAMETER_VALUES } from '../../../../graphql/Parametre/mutation';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop';
import { DO_UPDATE_USER_THEME } from '../../../../graphql/User/mutation';
import {
  UPDATE_USER_THEME,
  UPDATE_USER_THEMEVariables,
} from '../../../../graphql/User/types/UPDATE_USER_THEME';
import { ME } from '../../../../graphql/Authentication/types/ME';
import { GET_ME } from '../../../../graphql/Authentication/query';
// import CircleChecked from '@material-ui/icons/CheckCircleOutline';
// import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';

/**
 *
 * @param parametersList {Array<any>}
 * @param selectedTheme {string}
 * @param apolloClient {ApolloClient<object>}
 */
export const saveSelectedThemeToApolloCache = (
  parametersList: any[],
  selectedTheme: string,
  apolloClient: ApolloClient<object>,
) => {
  if (parametersList && parametersList.length > 0) {
    const themeParams = parametersList.find((item: any) => item && item.code === CODE_THEME);
    if (themeParams && themeParams.value && themeParams.value.value !== selectedTheme) {
      const newThemeParams = {
        ...themeParams,
        value: { ...themeParams.value, value: selectedTheme },
      };

      let newParameters = parametersList.filter((item: any) => item && item.code !== CODE_THEME);
      if (newParameters && newParameters.length > 0 && newThemeParams) {
        newParameters = [...[newThemeParams], ...newParameters];
      }

      if (newParameters && newParameters.length > 0) {
        apolloClient.writeData({
          data: { parameters: newParameters, __typename: 'parameters' },
        });
      }
    }
  }
};

const ThemePage = () => {
  const classes = useStyles({});
  const client = useApolloClient();
  const me = useQuery<ME>(GET_ME);
  const userTheme: any = me && me.data && me.data.me && me.data.me.theme;
  //const userTheme = useValueParameter(CODE_THEME);
  const [selectedTheme, setSelectedTheme] = React.useState('onyx');

  useEffect(() => {
    if (userTheme && userTheme !== selectedTheme) {
      setSelectedTheme(userTheme);
    }
  }, [userTheme]);

  // Get parameters from Cache
  const parametersList = useParameters();


  const [doUpdateParameterValues, { loading }] = useMutation<
    UPDATE_PARAMETER_VALUES,
    UPDATE_PARAMETER_VALUESVariables
  >(DO_UPDATE_PARAMETER_VALUES, {
    onCompleted: ({ updateParameterValues }) => {
      if (updateParameterValues && updateParameterValues.length > 0) {
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: 'Thème mis à jour avec succès',
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      }
    },
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const [doUpdateUserTheme, doUpdateUserThemeResult] = useMutation<
    UPDATE_USER_THEME,
    UPDATE_USER_THEMEVariables
  >(DO_UPDATE_USER_THEME);

  const saveSelectedThemeToDB = (theme:string) => {
    if (parametersList && parametersList.length > 0) {
      const themeParams = parametersList.find((item: any) => item && item.code === CODE_THEME);
      if (themeParams && themeParams.value && themeParams.value.value !== theme) {
        const newThemeParams = {
          ...themeParams,
          value: { ...themeParams.value, value: theme },
        };

        if (newThemeParams && newThemeParams.id) {
          const value: any =
            (newThemeParams.value && newThemeParams.value.value) || newThemeParams.defaultValue;
          doUpdateParameterValues({
            variables: { parameters: [{ id: newThemeParams.id, value }] },
          });
          doUpdateUserTheme({ variables: { theme: value } });
        }
      }
    }
  };

  const handleChange = (theme: string) => {
    if (theme !== userTheme) {
      setSelectedTheme(theme);
      saveSelectedThemeToApolloCache(parametersList, theme, client);
      saveSelectedThemeToDB(theme);
    }
  };



  return (
    <>
      {loading && <Backdrop />}
      <div className={classes.themePageRootContainer}>
        <div>
          <Typography variant="h6">Thème</Typography>
          <Typography variant="caption">Personnalisez Digital4win...</Typography>
        </div>
        <div className={classes.themeListContainer}>
          {THEMES_LIST.map(item => (
            <div
              key={item.value}
              className={
                selectedTheme === item.value
                  ? classnames(classes.themeItem, classes.themeItemChecked)
                  : classes.themeItem
              }
              onClick={() => handleChange(item.value)}
            >
              <Radio
                className={classes.themeItemRadio}
                checked={selectedTheme === item.value}
                onChange={() => handleChange(item.value)}
                value={item.value}
                name={item.value}
                inputProps={{ 'aria-label': `${item.value.toUpperCase()}` }}
                checkedIcon={<CircleCheckedFilled />}
              />
              <ThemePreview
                color={{
                  primary: item.color.primary,
                  secondary: item.color.secondary,
                }}
                name={item.libelle}
              />
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default ThemePage;
