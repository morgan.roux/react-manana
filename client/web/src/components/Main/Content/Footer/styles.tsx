import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    footerRoot: {
      height: 27,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      background: theme.palette.primary.main,
      color: theme.palette.common.white,
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 10,
      width: '100%',
      position: 'absolute',
      bottom: 0,
    },
    policyLink: {
      marginLeft: 10,
      cursor: 'pointer',
      '&:hover': {
        textDecoration: 'underline',
      },
    },
  }),
);
