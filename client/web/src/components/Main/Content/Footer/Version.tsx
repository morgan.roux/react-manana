import React, { FC, CSSProperties } from 'react';
import { CURRENT_VERSION } from '../../../../config';

interface VersionProps {
  style?: CSSProperties;
}

const Version: FC<VersionProps> = ({ style }) => {
  return <span style={style}>{CURRENT_VERSION}</span>;
};

export default Version;
