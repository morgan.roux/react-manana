import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      height: '100%',
      marginTop: 50,
      [theme.breakpoints.up('lg')]: {
        marginTop: 0,
      },
    },
    listContainer: {
      width: '100%',
      height: 'calc(100vh - 86px)',
      overflow: 'auto',
      border: 'none',
      padding: '0 16px',
      [theme.breakpoints.up('md')]: {
        border: `1px solid ${theme.palette.grey[400]}`,
        width: '391px',
      },
    },
    contentContainer: {
      width: '100%',
      height: '100%',
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'block',
        width: 'calc(100% - 391px)',
      },
    },
  }),
);

export default useStyles;
