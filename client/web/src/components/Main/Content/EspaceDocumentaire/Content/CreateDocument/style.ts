import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
  form: {
    padding: '16px 0',
    width: '100%',
    '& [class*=MuiFormControl-root]': {
      marginBottom: theme.spacing(2),
    },
    '& [class*=MuiChip-root]': {
      marginRight: theme.spacing(2),
      backgroundColor: '#F5F6FA',
      '& svg': {
        color: theme.palette.primary.main,
      },
    },
  },
  multiple: {
    marginBottom: theme.spacing(2),
    padding: theme.spacing(1),
    border: `1px solid ${theme.palette.text.secondary}`,
    borderRadius: 5,
    '& input': {
      marginBottom: theme.spacing(2),
    },
  },
  numeroCommande: {
    '& .MuiFormControl-root': {
      minWidth: '100%',
      marginLeft: 'auto',
    },
  },
  flex: {
    display: 'flex',
    alignItems: 'center',
    '&>div': {
      flex: '1 1 calc(50% - 16px)',
      maxWidth: 'calc(50% - 8px)',
    },
    '&>div:last-of-type': {
      marginLeft: theme.spacing(2),
    },
  },
  caption: {
    display: 'inline-block',
    marginBottom: theme.spacing(1.5),
  },
  actions: {
    padding: theme.spacing(2),
    '& button': {
      margin: 'auto',
      minWidth: '25%',
    },
    boxShadow: '0 -2px 6px rgba(0,0,0,.16)',
  },
  endValidationBox: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  field: {
    width: '33%',
  },

  switchControl: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  joindreButton: {
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 24,
    paddingRight: 24,
  },
  preavisBox: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  preavisField: {
    display: 'flex',
    alignItems: 'center',
  },
  preavisText: {
    fontSize: 16,
    fontFamily: 'Roboto',
  },
}));
