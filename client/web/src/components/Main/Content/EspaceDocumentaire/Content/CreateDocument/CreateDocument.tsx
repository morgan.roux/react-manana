import { useLazyQuery, useQuery } from '@apollo/react-hooks';
import { CustomButton, Loader, CustomEditorText, CustomModal } from '@app/ui-kit';
import {
  Box,
  TextField,
  FormControlLabel,
  Switch,
  Checkbox,
  FormControl,
  OutlinedInput,
} from '@material-ui/core';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import { GET_MES_CATEGORIES } from '../../../../../../federation/ged/categorie/query';
import { GET_MES_CATEGORIES as GET_MES_CATEGORIES_TYPE } from '../../../../../../federation/ged/categorie/types/GET_MES_CATEGORIES';
import { CustomDatePicker } from '../../../../../Common/CustomDateTimePicker';
import CustomSelect from '../../../../../Common/CustomSelect';
import Dropzone from '../../../../../Common/Dropzone';
import UserInput from '../../../../../Common/UserInput';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import OrigineInput from '../../../../../Common/OrigineInput/OrigineInput';
import styles from './style';
import { isMobile } from '../../../../../../utils/Helpers';
import ConditionCommerciale from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/ConditionCommerciale';
import { ConditionCommandeItem } from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/ConditionCommerciale/ConditionCommerciale';
import {
  GET_PRESTATION_TYPE,
  GET_PRT_REGLEMENT_MODE,
  GET_PRT_REMUNERATIONS_REGLEMENTS,
} from '../../../../../../federation/partenaire-service/remuneration/query';
import {
  GET_PRT_REGLEMENT_MODE as GET_PRT_REGLEMENT_MODE_TYPE,
  GET_PRT_REGLEMENT_MODEVariables,
} from '../../../../../../federation/partenaire-service/remuneration/types/GET_PRT_REGLEMENT_MODE';
import {
  DocumentFacture,
  HistoriqueFacturePage,
} from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/HistoriqueFacture/HistoriqueFacture';
import { PRTRemunerationReglement } from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/Remuneration/TableauRemuneration/TableChart/interface';
import { HistoriqueRemuneration } from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/Remuneration/Remuneration';
import {
  GET_PRT_REMUNERATIONS_REGLEMENT as GET_PRT_REMUNERATIONS_REGLEMENT_TYPE,
  GET_PRT_REMUNERATIONS_REGLEMENTVariables,
} from '../../../../../../federation/partenaire-service/remuneration/types/GET_PRT_REMUNERATIONS_REGLEMENT';
import { getGroupement, getPharmacie, getUser } from '../../../../../../services/LocalStorage';
import moment from 'moment';
import {
  GET_PRESTATION_TYPE as GET_PRESTATION_TYPE_TYPE,
  GET_PRESTATION_TYPEVariables,
} from '../../../../../../federation/partenaire-service/remuneration/types/GET_PRESTATION_TYPE';
import useOrigines from '../../../../../hooks/basis/useOrigines';
import {
  GET_DOCUMENTS,
  GET_TOTAUX_FACTURES,
} from '../../../../../../federation/ged/document/query';
import {
  GET_DOCUMENTS as GET_DOCUMENTS_TYPE,
  GET_DOCUMENTSVariables,
} from '../../../../../../federation/ged/document/types/GET_DOCUMENTS';
import {
  TOTAUX_FACTURES,
  TOTAUX_FACTURESVariables,
} from '../../../../../../federation/ged/document/types/TOTAUX_FACTURES';
import { PRTConditionCommercialeFilter } from '../../../../../../types/federation-global-types';
import {
  GET_CONDITIONS,
  GET_ROW_CONDITIONS,
} from '../../../../../../federation/partenaire-service/laboratoire/condition/query';
import {
  GET_CONDITIONS as GET_CONDITIONS_TYPE,
  GET_CONDITIONSVariables,
} from '../../../../../../federation/partenaire-service/laboratoire/condition/types/GET_CONDITIONS';
import {
  GET_ROW_CONDITIONS as GET_ROW_CONDITION_TYPES,
  GET_ROW_CONDITIONSVariables,
} from '../../../../../../federation/partenaire-service/laboratoire/condition/types/GET_ROW_CONDITIONS';
import { GET_TYPES_CONDITION } from '../../../../../../federation/partenaire-service/laboratoire/condition/type/query';
import {
  GET_TYPES_CONDITION as GET_TYPES_CONDITION_TYPE,
  GET_TYPES_CONDITIONVariables,
} from '../../../../../../federation/partenaire-service/laboratoire/condition/type/types/GET_TYPES_CONDITION';
import { GET_STATUS_CONDITION } from '../../../../../../federation/partenaire-service/laboratoire/condition/status/query';
import {
  GET_STATUS_CONDITION as GET_STATUS_CONDITION_TYPE,
  GET_STATUS_CONDITIONVariables,
} from '../../../../../../federation/partenaire-service/laboratoire/condition/status/types/GET_STATUS_CONDITION';
import { GET_LABORATOIRE } from '../../../../../../federation/partenaire-service/laboratoire/query';
import {
  GET_LABORATOIRE as GET_LABORATOIRE_TYPE,
  GET_LABORATOIREVariables,
} from '../../../../../../federation/partenaire-service/laboratoire/types/GET_LABORATOIRE';
import { Alert } from '@material-ui/lab';
import {
  ChangeDocumentProps,
  CreateDocumentProps,
  HandleChangeInterface,
  TotalProps,
} from './util/interface';
import FactureTvaSelect from './FactureTva/FactureTva';
import {
  GET_PLAN_MARKETINGSVariables,
  GET_PLAN_MARKETINGS as GET_PLAN_MARKETINGS_TYPE,
} from '../../../../../../federation/partenaire-service/planMarketing/types/GET_PLAN_MARKETINGS';
import {
  GET_LIST_PLAN_MARKETING_TYPES,
  GET_PLAN_MARKETINGS,
} from '../../../../../../federation/partenaire-service/planMarketing/query';
import {
  GET_LIST_PLAN_MARKETING_TYPESVariables,
  GET_LIST_PLAN_MARKETING_TYPES as GET_LIST_PLAN_MARKETING_TYPES_TYPE,
} from '../../../../../../federation/partenaire-service/planMarketing/types/GET_LIST_PLAN_MARKETING_TYPES';
import { AppAuthorization } from '../../../../../../services/authorization';

const initialTotalProps: TotalProps = {
  hT: false,
  ttc: false,
  tva: false,
};

const typeDocuments = [
  {
    id: 'AUTRES',
    code: 'AUTRES',
    libelle: 'Autres',
  },
  {
    id: 'CONTRAT',
    code: 'CONTRAT',
    libelle: 'Contrat',
  },
];

// const typeFactures = [
//   {
//     id: 'FACTURE',
//     code: 'FACTURE',
//     libelle: 'Facture',
//   },
// ];

const associationsPartenaires = [
  {
    id: 'FACTURE',
    code: 'FACTURE',
    libelle: 'Facture',
  },
  {
    id: 'BRI',
    code: 'BRI',
    libelle: 'BRI',
  },
  {
    id: 'CONDITION_COMMERCIALE',
    code: 'CONDITION_COMMERCIALE',
    libelle: 'Condition Commerciale',
  },
  {
    id: 'REPRISE_PERIMES',
    code: 'REPRISE_PERIMES',
    libelle: 'Reprise des périmés',
  },
];

const CreateDocument: FC<CreateDocumentProps> = ({
  saving,
  activeSousCategorie,
  mode,
  document,
  onRequestCreateDocument,
  open,
  setOpen,
  title,
  defaultType,
  idDefaultOrigine,
  defaultCorrespondant,
  searchVariables,
  tvas,
}) => {
  const classes = styles();
  const [motsCles, setMotsCles] = React.useState<string[] | string>('');

  const pharmacie = getPharmacie();
  const groupement = getGroupement();
  const user = getUser();
  const auth = new AppAuthorization(user);

  const { loading: categoriesLoading, error: categoriesError, data: categoriesData } = useQuery<
    GET_MES_CATEGORIES_TYPE,
    any
  >(GET_MES_CATEGORIES, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const loadingReglementMode = useQuery<
    GET_PRT_REGLEMENT_MODE_TYPE,
    GET_PRT_REGLEMENT_MODEVariables
  >(GET_PRT_REGLEMENT_MODE, { client: FEDERATION_CLIENT });

  const [loadRemunerationReglement, loadingRemunerationReglement] = useLazyQuery<
    GET_PRT_REMUNERATIONS_REGLEMENT_TYPE,
    GET_PRT_REMUNERATIONS_REGLEMENTVariables
  >(GET_PRT_REMUNERATIONS_REGLEMENTS, {
    client: FEDERATION_CLIENT,
  });

  // const loadingPrestationType = useQuery<GET_PRESTATION_TYPE_TYPE, GET_PRESTATION_TYPEVariables>(
  //   GET_PRESTATION_TYPE,
  //   { client: FEDERATION_CLIENT },
  // );

  const planMarketingTypes = useQuery<
    GET_LIST_PLAN_MARKETING_TYPES_TYPE,
    GET_LIST_PLAN_MARKETING_TYPESVariables
  >(GET_LIST_PLAN_MARKETING_TYPES, {
    client: FEDERATION_CLIENT,
    variables: {
      idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
      filter: { idGroupement: { eq: groupement.id } },
    },
  });

  const [getDocuments, gettingDocuments] = useLazyQuery<GET_DOCUMENTS_TYPE, GET_DOCUMENTSVariables>(
    GET_DOCUMENTS,
    {
      client: FEDERATION_CLIENT,
    },
  );

  const [loadTotauxFacture, loadingTotauxFacture] = useLazyQuery<
    TOTAUX_FACTURES,
    TOTAUX_FACTURESVariables
  >(GET_TOTAUX_FACTURES, {
    client: FEDERATION_CLIENT,
  });

  const [loadConditions, loadingConditions] = useLazyQuery<
    GET_CONDITIONS_TYPE,
    GET_CONDITIONSVariables
  >(GET_CONDITIONS, {
    client: FEDERATION_CLIENT,
  });

  const [loadRowConditions, loadingRowConditions] = useLazyQuery<
    GET_ROW_CONDITION_TYPES,
    GET_ROW_CONDITIONSVariables
  >(GET_ROW_CONDITIONS, {
    client: FEDERATION_CLIENT,
  });

  const loadType = useQuery<GET_TYPES_CONDITION_TYPE, GET_TYPES_CONDITIONVariables>(
    GET_TYPES_CONDITION,
    {
      client: FEDERATION_CLIENT,
    },
  );

  const loadStatus = useQuery<GET_STATUS_CONDITION_TYPE, GET_STATUS_CONDITIONVariables>(
    GET_STATUS_CONDITION,
    {
      client: FEDERATION_CLIENT,
    },
  );

  const [loadLaboratoire] = useLazyQuery<GET_LABORATOIRE_TYPE, GET_LABORATOIREVariables>(
    GET_LABORATOIRE,
    {
      onCompleted: result => {
        setDocumentToSave(prev => ({
          ...prev,
          correspondant: result.laboratoire,
        }));
      },
      client: FEDERATION_CLIENT,
    },
  );

  const [loadPlanMarketing, loadingPlanMarketing] = useLazyQuery<
    GET_PLAN_MARKETINGS_TYPE,
    GET_PLAN_MARKETINGSVariables
  >(GET_PLAN_MARKETINGS, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
  });

  // const handleChangeMotCle = (event: React.ChangeEvent<HTMLInputElement>): void => {
  //   const { value } = event.target as HTMLInputElement;
  //   const temp: string[] = [];
  //   const input: string[] = value.split(',');
  //   input.forEach((motCle: string) => temp.push(motCle));

  //   setMotsCles(temp);
  // };

  // const handleDeleteMotsCle = (index: number) => (_: React.MouseEvent) => {
  //   if (typeof motsCles === 'object') {
  //     motsCles.splice(index, 1);
  //     setMotsCles([...motsCles]);
  //   } else setMotsCles(['']);
  // };
  const origines = useOrigines();
  const idOrigineLaboratoire = (origines.data?.origines.nodes || []).find(
    ({ code }) => code === 'LABORATOIRE',
  )?.id;

  const idOrigine =
    idDefaultOrigine ||
    searchVariables?.idOrigine ||
    ('FACTURE' === searchVariables?.type ? idOrigineLaboratoire : undefined);

  const initialValueDocument: ChangeDocumentProps = {
    description: '',
    nomenclature: '',
    numeroVersion: '',
    idUserRedacteur: '',
    idUserVerificateur: '',
    isGenererCommande: false,
    dateHeureParution: null,
    dateHeureDebutValidite: null,
    dateHeureFinValidite: null,
    idSousCategorie: activeSousCategorie?.id,
    factureDate: new Date(),
    factureTotalHt: undefined,
    factureTotalTtc: undefined,
    factureTva: undefined,
    type: defaultType || searchVariables?.type || typeDocuments[0].id,
    numeroFacture: '',
    dateReglement: moment()
      .add(1, 'days')
      .toDate(),
    idReglementMode: undefined,
    associer: undefined,
    conditionIds: [],
    factureIds: [],
    remunerationIds: [],
    reprisePerimes: '',
    userRedacteur: [],
    userVerificateur: [],
    isMultipleTva: false,
    factureTvas: tvas.data.map(tva => ({
      tva,
      idTva: tva.id,
      montantHt: undefined,
      montantTtc: 0,
      montantTva: 0,
    })),
  };

  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);
  const [openUserVerificateurInput, setOpenUserVerificateurInput] = useState<boolean>(false);
  const [openUserRedacteurInput, setOpenUserRedacteurInput] = useState<boolean>(false);

  const [origine, setOrigine] = useState<any>();
  const [correspondant, setCorrespondant] = useState<any>();
  const [showAdditionnalInfosFields, setShowAdditionnalInfosFields] = useState<boolean>(false);
  const [openAssocieDialog, setOpenAssocieDialog] = useState<boolean>(false);
  const [changeCondition, setChangeCondition] = useState<any>();
  const [changeFacture, setChangeFacture] = useState<any>();
  const [isModifie, setIsModifie] = useState<TotalProps>(initialTotalProps);

  const [documentToSave, setDocumentToSave] = useState<ChangeDocumentProps>(initialValueDocument);

  useEffect(() => {
    if (open) {
      if (document && mode === 'remplacement') {
        const fic: any = {
          name: document?.fichier?.nomOriginal as any,
          text: document?.fichier?.nomOriginal as any,
        };
        const keywords: string[] = [];
        document?.motCle1 && keywords.push(document?.motCle1 as any);
        document?.motCle2 && keywords.push(document?.motCle2 as any);
        document?.motCle3 && keywords.push(document?.motCle3 as any);

        if (
          !defaultCorrespondant &&
          document.idOrigineAssocie &&
          document.idOrigine === idOrigineLaboratoire
        ) {
          loadLaboratoire({
            variables: {
              id: document.idOrigineAssocie,
              idPharmacie: getPharmacie().id,
            },
          });
        }
        setSelectedFiles([fic]);
        setMotsCles(keywords);

        const idOrigine =
          document.idOrigine ||
          idDefaultOrigine ||
          searchVariables?.idOrigine ||
          ('FACTURE' === searchVariables?.type ? idOrigineLaboratoire : undefined);
        setOrigine((origines.data?.origines.nodes || []).find(({ id }) => id === idOrigine));
        setCorrespondant(defaultCorrespondant || searchVariables?.idOrigineAssocie);
        setIsModifie(initialTotalProps);

        setDocumentToSave({
          ...document,
          isGenererCommande: document.isGenererCommande || false,
          idSousCategorie: document?.sousCategorie?.id,
          type: searchVariables?.type || document.type || 'AUTRES',
          associer: document.typeAvoirAssociations?.length
            ? document.typeAvoirAssociations[0].type
            : undefined,
          conditionIds: document.typeAvoirAssociations?.length
            ? document.typeAvoirAssociations
                .filter(avoirAssociation => avoirAssociation.type === 'CONDITION_COMMERCIALE')
                .map(avoirAssociation => avoirAssociation.correspondant)
            : [],
          factureIds: document.typeAvoirAssociations?.length
            ? document.typeAvoirAssociations
                .filter(avoirAssociation => avoirAssociation.type === 'FACTURE')
                .map(avoirAssociation => avoirAssociation.correspondant)
            : [],
          remunerationIds: document.typeAvoirAssociations?.length
            ? document.typeAvoirAssociations
                .filter(avoirAssociation => avoirAssociation.type === 'BRI')
                .map(avoirAssociation => avoirAssociation.correspondant)
            : [],
          reprisePerimes:
            document.typeAvoirAssociations?.length &&
            document.typeAvoirAssociations[0].type === 'REPRISE_PERIMES'
              ? document.typeAvoirAssociations[0].correspondant
              : '',
          userRedacteur: [],
          userVerificateur: [],
          factureTvas: tvas.data.map(tva => {
            const tvaCorrespondant = document.facturesTva?.filter(
              factureTva => factureTva.idTva === tva.id,
            );
            return {
              tva,
              idDocument: tvaCorrespondant?.length ? tvaCorrespondant[0].idDocument : undefined,
              idTva: tva.id,
              montantHt: tvaCorrespondant?.length ? tvaCorrespondant[0].montantHt : undefined,
              montantTtc: tvaCorrespondant?.length ? tvaCorrespondant[0].montantTtc : 0,
              montantTva: tvaCorrespondant?.length ? tvaCorrespondant[0].montantTva : 0,
            };
          }),
        });
      } else {
        setMotsCles([]);
        setSelectedFiles([]);

        setOrigine((origines.data?.origines.nodes || []).find(({ id }) => id === idOrigine));
        setCorrespondant(defaultCorrespondant || searchVariables?.idOrigineAssocie);
        setIsModifie(initialTotalProps);

        setDocumentToSave(initialValueDocument);
      }
    }
  }, [document, mode, searchVariables, open]);

  useEffect(() => {
    if ('FACTURE' === documentToSave.type) {
      // setOrigine(idOrigineLaboratoire);
      setOrigine(idOrigineLaboratoire);
    }
  }, [documentToSave.type]);

  useEffect(() => {
    setDocumentToSave(prev => ({ ...prev, correspondant: defaultCorrespondant }));
  }, [defaultCorrespondant]);

  const briType = planMarketingTypes.data?.pRTPlanMarketingTypes.nodes.find(
    type => type.code === 'BRI',
  );

  useEffect(() => {
    if (briType && correspondant?.id) {
      loadPlanMarketing({
        variables: {
          filter: {
            and: [
              {
                partenaireType: {
                  eq: 'LABORATOIRE',
                },
              },
              {
                idPartenaireTypeAssocie: {
                  eq: correspondant?.id,
                },
              },
              {
                idPharmacie: {
                  eq: pharmacie.id,
                },
              },
              {
                idType: {
                  eq: briType?.id,
                },
              },
            ],
          },
        },
      });
    }
  }, [correspondant, defaultCorrespondant]);

  useEffect(() => {
    const filterAnd = [
      {
        idOrigine: {
          eq: origine?.id,
        },
      },
      {
        idOrigineAssocie: {
          eq: correspondant?.id || '',
        },
      },
      {
        idPharmacie: {
          eq: pharmacie.id,
        },
      },
      {
        type: {
          eq: 'FACTURE',
        },
      },
      {
        factureTotalHt: {
          gte: 0,
        },
      },
      {
        factureTva: {
          gte: 0,
        },
      },
    ];

    if (changeFacture?.searchText) {
      filterAnd.push({
        numeroFacture: {
          iLike: `%${changeFacture?.searchText}%`,
        },
      } as any);
    }

    if (origine?.id && correspondant?.id) {
      getDocuments({
        variables: {
          filter: {
            and: filterAnd,
          },
          paging: {
            offset: changeFacture?.skip,
            limit: changeFacture?.take,
          },
        },
      });
      loadTotauxFacture({
        variables: {
          idPharmacie: pharmacie.id,
          idOrigine: origine.id,
          idOrigineAssocie: correspondant?.id,
          noAvoir: true,
          searchText: changeFacture?.searchText ? `%${changeFacture?.searchText}%` : undefined,
        },
      });
    }
  }, [origine, correspondant, changeFacture]);

  useEffect(() => {
    const keywords = ['idType', 'idCanal', 'idStatut'];
    if (correspondant?.id && changeCondition) {
      const _filters = keywords
        .map(keyword => {
          const tempObject = { or: [] };
          for (let filter of changeCondition.filters) {
            if (filter.keyword === keyword) {
              tempObject.or.push({
                [keyword]: {
                  eq: filter.id,
                },
              } as never);
            }
          }
          return tempObject.or.length > 0 ? tempObject : undefined;
        })
        .filter(e => e);

      const filterAnd: PRTConditionCommercialeFilter[] = [
        {
          partenaireType: {
            eq: 'LABORATOIRE',
          },
        },
        {
          idPartenaireTypeAssocie: {
            eq: correspondant?.id,
          },
        },
        {
          idPharmacie: {
            eq: pharmacie.id,
          },
        },
        {
          or: [
            {
              titre: {
                iLike: `%${changeCondition?.searchText}%`,
              },
            },
            {
              description: {
                iLike: `%${changeCondition?.searchText}%`,
              },
            },
          ],
        },
      ];

      if (_filters.length > 0) {
        _filters.map(filter => {
          filterAnd.push(filter as any);
        });
      }

      loadConditions({
        variables: {
          filter: {
            and: filterAnd,
          },
          paging: {
            offset: changeCondition?.skip,
            limit: changeCondition?.take,
          },
        },
      });
      loadRowConditions({
        variables: {
          filter: {
            and: filterAnd,
          },
        },
      });
    }
  }, [correspondant, changeCondition]);

  const searchDocuments = (change: any) => {
    setChangeFacture(change);
  };

  const onRequestSearchCondition = ({ skip, take, searchText, filters, sortTable }: any) => {
    setChangeCondition({ skip, take, searchText, filters });
  };

  const handleChange = ({ name, value }: HandleChangeInterface) => {
    if (name) {
      setDocumentToSave(prev => ({ ...prev, [name]: value }));
    }

    if (origine && 'associer' === name) {
      setOpenAssocieDialog(true);
    }
  };

  const handleChangeHT = (event: ChangeEvent<HTMLInputElement>): void => {
    handleChange({ name: 'factureTotalHt', value: event.target.value });
    setIsModifie(prev => ({
      ...prev,
      hT: true,
    }));
    if (documentToSave.factureTva && !isModifie.ttc)
      handleChange({
        name: 'factureTotalTtc',
        value: parseFloat(event.target.value || '0') + parseFloat(documentToSave.factureTva),
      });
    else if (documentToSave.factureTotalTtc && !isModifie.tva)
      handleChange({
        name: 'factureTva',
        value: parseFloat(documentToSave.factureTotalTtc) - parseFloat(event.target.value || '0'),
      });
  };

  const handleChangeTVA = (event: ChangeEvent<HTMLInputElement>): void => {
    handleChange({ name: 'factureTva', value: event.target.value });
    setIsModifie(prev => ({
      ...prev,
      tva: true,
    }));
    if (documentToSave.factureTotalHt && !isModifie.ttc)
      handleChange({
        name: 'factureTotalTtc',
        value: parseFloat(event.target.value || '0') + parseFloat(documentToSave.factureTotalHt),
      });
    else if (documentToSave.factureTotalTtc && !isModifie.hT)
      handleChange({
        name: 'factureTotalHt',
        value: parseFloat(documentToSave.factureTotalTtc) - parseFloat(event.target.value || '0'),
      });
  };

  const handleChangeTTC = (event: ChangeEvent<HTMLInputElement>): void => {
    handleChange({ name: 'factureTotalTtc', value: event.target.value });
    setIsModifie(prev => ({
      ...prev,
      ttc: true,
    }));
    if (documentToSave.factureTotalHt && !isModifie.tva)
      handleChange({
        name: 'factureTva',
        value: parseFloat(event.target.value || '0') - parseFloat(documentToSave.factureTotalHt),
      });
    else if (documentToSave.factureTva && !isModifie.hT)
      handleChange({
        name: 'factureTotalHt',
        value: parseFloat(event.target.value || '0') - parseFloat(documentToSave.factureTva),
      });
  };

  const selectedConditionIds = (conditions: ConditionCommandeItem[]) => {
    setDocumentToSave(prev => ({
      ...prev,
      conditionIds: (conditions || []).map(condition => condition.id || ''),
    }));
    setOpenAssocieDialog(false);
  };

  const createDocument = (): void => {
    if (selectedFiles[0]) {
      const doc: any = {
        idSousCategorie:
          'FACTURE' !== documentToSave.type ? documentToSave.idSousCategorie : undefined,
        description: documentToSave.description,
        nomenclature: documentToSave.nomenclature,
        numeroVersion: documentToSave.numeroVersion,
        idUserVerificateur: documentToSave.idUserVerificateur,
        idUserRedacteur: documentToSave.idUserRedacteur,
        dateHeureParution: documentToSave.dateHeureParution
          ? new Date(documentToSave.dateHeureParution)
          : undefined,
        dateHeureDebutValidite: documentToSave.dateHeureDebutValidite
          ? new Date(documentToSave.dateHeureDebutValidite)
          : undefined,
        dateHeureFinValidite: documentToSave.dateHeureFinValidite
          ? new Date(documentToSave.dateHeureFinValidite)
          : undefined,
        motCle1: motsCles[0],
        motCle2: motsCles[1],
        motCle3: motsCles[2],
        origine: origine,
        correspondant: correspondant,
        dateFacture: 'FACTURE' === documentToSave.type ? documentToSave.factureDate : undefined,
        hT:
          'FACTURE' === documentToSave.type && documentToSave.factureTotalHt
            ? parseFloat(documentToSave.factureTotalHt)
            : undefined,
        tva:
          'FACTURE' === documentToSave.type && documentToSave.factureTva
            ? parseFloat(documentToSave.factureTva)
            : undefined,
        ttc:
          'FACTURE' === documentToSave.type && documentToSave.factureTotalTtc
            ? parseFloat(documentToSave.factureTotalTtc)
            : undefined,
        type: documentToSave.type ?? 'AUTRES',

        numeroFacture: 'FACTURE' === documentToSave.type ? documentToSave.numeroFacture : undefined,
        factureDateReglement:
          'FACTURE' === documentToSave.type ? documentToSave.dateReglement : undefined,
        idReglementMode:
          'FACTURE' === documentToSave.type ? documentToSave.idReglementMode : undefined,
        numeroCommande:
          'FACTURE' === documentToSave.type && documentToSave.numeroCommande
            ? parseInt(documentToSave.numeroCommande)
            : undefined,
        isGenererCommande:
          'FACTURE' === documentToSave.type ? documentToSave.isGenererCommande : undefined,
        avoirType:
          'FACTURE' === documentToSave.type && documentToSave.associer
            ? documentToSave.associer
            : undefined,
        avoirCorrespondants: !(
          (parseFloat(documentToSave.factureTotalHt || '0') < 0 ||
            parseFloat(documentToSave.factureTva || '0') < 0 ||
            parseFloat(documentToSave.factureTotalTtc || '0') < 0) &&
          origine &&
          correspondant
        )
          ? []
          : 'FACTURE' === documentToSave.type && documentToSave.associer === 'CONDITION_COMMERCIALE'
          ? documentToSave.conditionIds
          : documentToSave.associer === 'FACTURE'
          ? documentToSave.factureIds
          : documentToSave.associer === 'BRI'
          ? documentToSave.remunerationIds
          : documentToSave.associer === 'REPRISE_PERIMES'
          ? [documentToSave.reprisePerimes]
          : [],
        nombreJoursPreavis:
          'CONTRAT' === documentToSave.type ? documentToSave.nombreJoursPreavis : undefined,
        isRenouvellementTacite:
          'CONTRAT' === documentToSave.type ? documentToSave.isRenouvellementTacite : undefined,
        isMultipleTva: 'FACTURE' === documentToSave.type ? documentToSave.isMultipleTva : undefined,
        factureTvas:
          'FACTURE' === documentToSave.type && documentToSave.isMultipleTva
            ? documentToSave.factureTvas?.map(factureTva => ({
                ...factureTva,
                montantHt: parseFloat(factureTva.montantHt || '0'),
                tva: undefined,
                idDocument: undefined,
              }))
            : undefined,
      };
      const dataToSave =
        mode === 'creation'
          ? {
              ...doc,
              selectedFile: selectedFiles[0],
              launchUpload: !!(selectedFiles[0] && selectedFiles[0].size),
            }
          : {
              ...doc,
              id: document?.id,
              selectedFile: selectedFiles[0],
              previousFichier: document?.fichier,
              launchUpload: !!(selectedFiles.length > 0 && selectedFiles[0].size),
            };
      onRequestCreateDocument(dataToSave);
    }
  };

  const isFormValuesValid = () => {
    return !documentToSave.type || documentToSave.type === 'AUTRES'
      ? selectedFiles.length > 0 && documentToSave.description && documentToSave.idSousCategorie
      : documentToSave.type === 'FACTURE'
      ? selectedFiles.length > 0 &&
        documentToSave.description &&
        // origine &&
        correspondant &&
        documentToSave.factureDate &&
        // dateReglement &&
        // dateReglement > dateFacture &&
        // numeroFacture &&
        documentToSave.idReglementMode
      : 'CONTRAT' === documentToSave.type
      ? selectedFiles.length > 0 &&
        documentToSave.description &&
        correspondant &&
        documentToSave.dateHeureDebutValidite
      : false;
  };

  const sousCategories =
    categoriesData?.gedMesCategories.flatMap(categorie => categorie.mesSousCategories) || [];
  const idUserParticipants =
    (
      documentToSave.idSousCategorie &&
      sousCategories?.find(({ id }) => id === documentToSave.idSousCategorie)
    )?.participants.map(({ id }) => id) || [];

  return (
    <>
      <CustomModal
        open={open}
        setOpen={setOpen}
        title={title}
        disableBackdropClick={true}
        closeIcon={true}
        withBtnsActions={true}
        headerWithBgColor={true}
        fullWidth={true}
        disabledButton={!isFormValuesValid() || saving}
        onClickConfirm={createDocument}
        maxWidth="sm"
        fullScreen={!!isMobile()}
        isDraggable
      >
        <Box className={classes.form}>
          <form>
            {mode === 'creation' && (
              <Dropzone selectedFiles={selectedFiles} setSelectedFiles={setSelectedFiles} />
            )}
            <Box pb={2}>
              <CustomEditorText
                placeholder="Description"
                value={documentToSave.description || ''}
                onChange={(value: string): void => handleChange({ name: 'description', value })}
              />
            </Box>

            {/*<Box className={classes.multiple}>
          <InputBase
            placeholder="Mots clés"
            value={motsCles}
            onChange={handleChangeMotCle}
            fullWidth
          />
          <Box>
            {typeof motsCles === 'object' && motsCles.length > 0
              ? motsCles.map((motsCle, index: number) => (
                <Chip
                  size="small"
                  label={motsCle}
                  onDelete={handleDeleteMotsCle(index)}
                  key={index}
                />
              ))
              : ''}
          </Box>
        </Box>*/}

            {'FACTURE' !== defaultType && (
              <CustomSelect
                label="Type document"
                list={typeDocuments}
                onChange={event => {
                  const { value } = event.target;
                  handleChange({ name: 'type', value });
                  handleChange({ name: 'idSousCategorie', value: undefined });
                }}
                listId="id"
                index="libelle"
                value={documentToSave.type}
                required
              />
            )}

            <OrigineInput
              onChangeOrigine={setOrigine}
              origine={origine}
              origineAssocie={correspondant}
              onChangeOrigineAssocie={setCorrespondant}
              readonly={idDefaultOrigine ? true : false}
            />

            {categoriesData && documentToSave.type === 'AUTRES' && (
              <CustomSelect
                disabled={categoriesLoading}
                error={categoriesError}
                label="Sous-espace documentaire"
                list={sousCategories}
                name="sousCategorie"
                onChange={event => {
                  const { value, name } = event.target;
                  handleChange({ name: 'idSousCategorie', value });
                }}
                listId="id"
                index="libelle"
                value={documentToSave.idSousCategorie}
                required={true}
              />
            )}

            {documentToSave.type === 'FACTURE' && (
              <>
                <Box className={classes.flex}>
                  <TextField
                    label="N° de facture"
                    variant="outlined"
                    value={documentToSave.numeroFacture}
                    onChange={(event: ChangeEvent<HTMLInputElement>): void => {
                      handleChange({ name: 'numeroFacture', value: event.target.value });
                    }}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <CustomDatePicker
                    label="Date facture"
                    value={documentToSave.factureDate}
                    onChange={(date: MaterialUiPickersDate) => {
                      handleChange({ name: 'factureDate', value: date });
                    }}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    required
                  />
                </Box>

                <Box className={classes.flex}>
                  <CustomSelect
                    label="Mode règlement"
                    list={loadingReglementMode.data?.pRTReglementModes.nodes}
                    onChange={event => {
                      const { value } = event.target;
                      handleChange({ name: 'idReglementMode', value });
                    }}
                    listId="id"
                    index="libelle"
                    value={documentToSave.idReglementMode}
                    required
                  />
                  <CustomDatePicker
                    label="Date règlement"
                    value={documentToSave.dateReglement}
                    onChange={(date: MaterialUiPickersDate) => {
                      handleChange({ name: 'dateReglement', value: date });
                    }}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                </Box>

                <Box pb={2}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={documentToSave.isMultipleTva}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                          if (document && documentToSave.isMultipleTva !== document.isMultipleTva) {
                            handleChange({
                              name: 'factureTotalHt',
                              value: document.factureTotalHt,
                            });
                            handleChange({ name: 'factureTva', value: document.factureTva });
                            handleChange({
                              name: 'factureTotalTtc',
                              value: document.factureTotalTtc,
                            });
                            handleChange({
                              name: 'factureTvas',
                              value: tvas.data.map(tva => {
                                const tvaCorrespondant = document.facturesTva?.filter(
                                  factureTva => factureTva.idTva === tva.id,
                                );
                                return {
                                  tva,
                                  idDocument: tvaCorrespondant?.length
                                    ? tvaCorrespondant[0].idDocument
                                    : undefined,
                                  idTva: tva.id,
                                  montantHt: tvaCorrespondant?.length
                                    ? tvaCorrespondant[0].montantHt
                                    : undefined,
                                  montantTtc: tvaCorrespondant?.length
                                    ? tvaCorrespondant[0].montantTtc
                                    : 0,
                                  montantTva: tvaCorrespondant?.length
                                    ? tvaCorrespondant[0].montantTva
                                    : 0,
                                };
                              }),
                            });
                          } else {
                            handleChange({
                              name: 'factureTotalHt',
                              value: 0,
                            });
                            handleChange({ name: 'factureTva', value: 0 });
                            handleChange({
                              name: 'factureTotalTtc',
                              value: 0,
                            });
                            handleChange({
                              name: 'factureTvas',
                              value: tvas.data.map(tva => ({
                                tva,
                                idTva: tva.id,
                                montantHt: undefined,
                                montantTtc: 0,
                                montantTva: 0,
                              })),
                            });
                          }
                          handleChange({
                            name: 'isMultipleTva',
                            value: event.target.checked,
                          });
                        }}
                      />
                    }
                    label="TVA multiple"
                  />
                </Box>

                {documentToSave.isMultipleTva ? (
                  <FactureTvaSelect
                    documentToSave={documentToSave}
                    setDocumentToSave={setDocumentToSave}
                    tvas={tvas}
                    isGed={!defaultCorrespondant}
                  />
                ) : (
                  <>
                    <Box className={classes.flex}>
                      <TextField
                        type="number"
                        label="Total HT"
                        variant="outlined"
                        value={documentToSave.factureTotalHt}
                        onChange={handleChangeHT}
                        InputLabelProps={{
                          shrink: true,
                        }}
                        inputProps={{
                          step: 0.5,
                        }}
                        required
                      />
                      <TextField
                        type="number"
                        label="Total TVA"
                        variant="outlined"
                        value={documentToSave.factureTva}
                        onChange={handleChangeTVA}
                        InputLabelProps={{
                          shrink: true,
                        }}
                        inputProps={{
                          step: 0.5,
                        }}
                        required
                      />
                    </Box>
                    <Box>
                      <TextField
                        type="number"
                        label="Total TTC"
                        variant="outlined"
                        value={documentToSave.factureTotalTtc}
                        onChange={handleChangeTTC}
                        fullWidth
                        InputLabelProps={{
                          shrink: true,
                        }}
                        inputProps={{
                          step: 0.5,
                        }}
                        required
                      />
                    </Box>
                  </>
                )}
                {parseFloat(documentToSave.factureTotalTtc || '0').toFixed(2) !==
                  (
                    parseFloat(documentToSave.factureTotalHt || '0') +
                    parseFloat(documentToSave.factureTva || '0')
                  ).toFixed(2) &&
                  !documentToSave.isMultipleTva &&
                  documentToSave.factureTotalHt &&
                  documentToSave.factureTva &&
                  documentToSave.factureTotalTtc && (
                    <Box pb={2}>
                      <Alert severity="warning">
                        Le total TTC est différent de Total HT + Total TVA
                      </Alert>
                    </Box>
                  )}

                {(parseFloat(documentToSave.factureTotalHt || '0') < 0 ||
                  parseFloat(documentToSave.factureTva || '0') < 0 ||
                  parseFloat(documentToSave.factureTotalTtc || '0') < 0) &&
                  origine &&
                  correspondant && (
                    <Box>
                      <CustomSelect
                        label="Associer"
                        list={associationsPartenaires}
                        onChange={event => {
                          const { value } = event.target;
                          handleChange({ name: 'associer', value });
                        }}
                        listId="id"
                        index="libelle"
                        value={documentToSave.associer}
                        required
                      />
                    </Box>
                  )}
                <div>
                  <Box className={classes.numeroCommande}>
                    <TextField
                      type="number"
                      label="N° de commande"
                      variant="outlined"
                      value={documentToSave.numeroCommande}
                      onChange={(event: ChangeEvent<HTMLInputElement>): void => {
                        handleChange({ name: 'numeroCommande', value: event.target.value });
                      }}
                      fullWidth
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </Box>
                  <Box>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={documentToSave.isGenererCommande}
                          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                            handleChange({
                              name: 'isGenererCommande',
                              value: event.target.checked,
                            });
                          }}
                        />
                      }
                      label="Générer une commande"
                    />
                  </Box>
                </div>
              </>
            )}

            {documentToSave.type !== 'CONTRAT' && (
              <FormControlLabel
                labelPlacement="start"
                className={classes.switchControl}
                value={showAdditionnalInfosFields}
                onChange={(_event, checked) => setShowAdditionnalInfosFields(checked)}
                control={<Switch />}
                label="Saisissez des informations supplémentaires"
              />
            )}

            {showAdditionnalInfosFields && documentToSave.type !== 'CONTRAT' && (
              <>
                <Box className={classes.flex}>
                  {categoriesLoading ? (
                    <Loader />
                  ) : (
                    <UserInput
                      label="Vérificateur"
                      idParticipants={idUserParticipants}
                      singleSelect={true}
                      selected={documentToSave.userVerificateur}
                      setSelected={users => {
                        if (users.length > 0) {
                          // setUserVerificateur(users);
                          handleChange({ name: 'userVerificateur', value: users });
                          handleChange({ name: 'idUserVerificateur', value: users[0].id });
                          // setIdUserVerificateur(users[0].id);
                        } else {
                          // setUserVerificateur([]);
                          // setIdUserVerificateur('');
                          handleChange({ name: 'userVerificateur', value: [] });
                          handleChange({ name: 'idUserVerificateur', value: '' });
                        }
                      }}
                      openModal={openUserVerificateurInput}
                      setOpenModal={setOpenUserVerificateurInput}
                      withAssignTeam={false}
                      withNotAssigned={false}
                    />
                  )}
                  {categoriesLoading ? (
                    <Loader />
                  ) : (
                    <UserInput
                      label="Rédacteur"
                      idParticipants={idUserParticipants}
                      singleSelect={true}
                      selected={documentToSave.userRedacteur}
                      setSelected={users => {
                        if (users.length > 0) {
                          // setUserRedacteur(users);
                          // setIdUserRedacteur(users[0].id);
                          handleChange({ name: 'userRedacteur', value: users });
                          handleChange({ name: 'idUserRedacteur', value: users[0].id });
                        } else {
                          // setUserRedacteur([]);
                          // setIdUserRedacteur('');
                          handleChange({ name: 'userRedacteur', value: [] });
                          handleChange({ name: 'idUserRedacteur', value: '' });
                        }
                      }}
                      openModal={openUserRedacteurInput}
                      setOpenModal={setOpenUserRedacteurInput}
                      withAssignTeam={false}
                      withNotAssigned={false}
                    />
                  )}
                </Box>

                <Box className={classes.flex}>
                  <TextField
                    variant="outlined"
                    label="Nomenclature"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    fullWidth
                    value={documentToSave.nomenclature}
                    onChange={(event: ChangeEvent<HTMLInputElement>): void => {
                      handleChange({ name: 'nomenclature', value: event.target.value });
                    }}
                  />
                  <TextField
                    variant="outlined"
                    label="Numéro de version"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    fullWidth
                    value={documentToSave.numeroVersion}
                    onChange={(event: ChangeEvent<HTMLInputElement>): void => {
                      handleChange({ name: 'numeroVersion', value: event.target.value });
                    }}
                  />
                </Box>
              </>
            )}

            {(showAdditionnalInfosFields || documentToSave.type === 'CONTRAT') && (
              <>
                <Box className={classes.endValidationBox}>
                  <CustomDatePicker
                    label={
                      'CONTRAT' === documentToSave.type ? 'Date signature' : 'Date de parution'
                    }
                    value={documentToSave.dateHeureParution}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    onChange={value => handleChange({ name: 'dateHeureParution', value })}
                    fullWidth
                  />
                </Box>
                <Box className={classes.flex}>
                  <CustomDatePicker
                    label={`Date début ${'CONTRAT' === documentToSave.type ? '' : 'validité'}`}
                    value={documentToSave.dateHeureDebutValidite}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    onChange={value => handleChange({ name: 'dateHeureDebutValidite', value })}
                    fullWidth
                    required={'CONTRAT' === documentToSave.type}
                  />
                  <CustomDatePicker
                    label={`Date fin ${'CONTRAT' === documentToSave.type ? '' : 'de validité'}`}
                    value={documentToSave.dateHeureFinValidite}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    onChange={value => handleChange({ name: 'dateHeureFinValidite', value })}
                    fullWidth
                  />
                </Box>
              </>
            )}

            {'CONTRAT' === documentToSave.type && (
              <>
                <Box className={classes.preavisBox}>
                  <Box className={classes.preavisText}>Nombre de jour de préavis</Box>
                  <Box className={classes.preavisField}>
                    <FormControl variant="outlined" style={{ maxWidth: 70, marginRight: 16 }}>
                      <OutlinedInput
                        type="number"
                        onChange={(event: ChangeEvent<HTMLInputElement>) => {
                          handleChange({
                            name: 'nombreJoursPreavis',
                            value: event.currentTarget.value
                              ? parseInt(event.currentTarget.value, 10)
                              : undefined,
                          });
                        }}
                        value={documentToSave.nombreJoursPreavis}
                        id="outlined-adornment-weight"
                        aria-describedby="outlined-weight-helper-text"
                        inputProps={{
                          'aria-label': 'weight',
                        }}
                        labelWidth={0}
                      />
                    </FormControl>
                    <Box className={classes.preavisText}>Jour(s)</Box>
                  </Box>
                </Box>
                <Box>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={documentToSave.isRenouvellementTacite}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                          handleChange({
                            name: 'isRenouvellementTacite',
                            value: event.target.checked,
                          });
                        }}
                      />
                    }
                    label={<Box className={classes.preavisText}>Tacite reconduction</Box>}
                  />
                </Box>
              </>
            )}
          </form>
        </Box>
      </CustomModal>
      <CustomModal
        open={openAssocieDialog}
        setOpen={setOpenAssocieDialog}
        title={
          documentToSave.associer === 'CONDITION_COMMERCIALE'
            ? 'Joindre une condition commerciale'
            : documentToSave.associer === 'FACTURE'
            ? 'Joindre une facture'
            : documentToSave.associer === 'BRI'
            ? 'Joindre BRI'
            : documentToSave.associer === 'REPRISE_PERIMES'
            ? 'Joindre une reprise périmés'
            : ''
        }
        closeIcon
        withBtnsActions={false}
        headerWithBgColor
        disableBackdropClick={true}
        fullScreen={!!isMobile()}
        isDraggable
      >
        <Box style={{ minWidth: 960 }}>
          {documentToSave.associer === 'CONDITION_COMMERCIALE' && (
            <ConditionCommerciale
              onRequestSearch={onRequestSearchCondition}
              data={
                (loadingConditions?.data?.pRTConditionCommerciales.nodes || []).map(item => ({
                  id: item.id,
                  canal: item.canal,
                  type: item.type,
                  status: item.statut,
                  libelle: item.titre,
                  fichiers: item.fichiers || null,
                  details: item.description || '',
                  periode: {
                    debut: item.dateDebut,
                    fin: item.dateFin,
                  },
                })) as any
              }
              loading={loadingConditions?.loading || false}
              total={
                (loadingRowConditions.data?.pRTConditionCommercialeAggregate.length || 0) > 0
                  ? loadingRowConditions.data?.pRTConditionCommercialeAggregate[0].count?.id || 0
                  : 0
              }
              listStatusConditionCommande={loadType.data?.pRTConditionCommercialeTypes.nodes || []}
              typesConditionCommande={loadStatus.data?.pRTConditionCommercialeStatuts.nodes || []}
              isFacture
              onRequestSelected={selectedConditionIds}
              selectedItemsIds={
                document?.typeAvoirAssociations?.length
                  ? document.typeAvoirAssociations
                      .filter(avoirAssociation => avoirAssociation.type === 'CONDITION_COMMERCIALE')
                      .map(avoirAssociation => avoirAssociation.correspondant)
                  : []
              }
            />
          )}
          {documentToSave.associer === 'FACTURE' && (
            <HistoriqueFacturePage
              loading={gettingDocuments.loading}
              error={gettingDocuments.error as any}
              data={gettingDocuments.data?.gedDocuments.nodes as any}
              rowsTotal={gettingDocuments.data?.gedDocuments.nodes.length || 0}
              onRequestSearch={searchDocuments}
              totaux={{
                totalHT: loadingTotauxFacture?.data?.getTotauxFactures?.totalHT || 0,
                totalTVA: loadingTotauxFacture?.data?.getTotauxFactures?.totalTVA || 0,
                totalTTC: loadingTotauxFacture?.data?.getTotauxFactures?.totalTTC || 0,
              }}
              isSelectable={true}
              onRequestSelected={(factures: DocumentFacture[]) => {
                handleChange({
                  name: 'factureIds',
                  value: factures.map(facture => facture.id || ''),
                });
                setOpenAssocieDialog(false);
              }}
              selectedItemsIds={
                document?.typeAvoirAssociations?.length
                  ? document.typeAvoirAssociations
                      .filter(avoirAssociation => avoirAssociation.type === 'FACTURE')
                      .map(avoirAssociation => avoirAssociation.correspondant)
                  : []
              }
            />
          )}
          {documentToSave.associer === 'BRI' && (
            <HistoriqueRemuneration
              loading={loadingPlanMarketing.loading}
              error={loadingPlanMarketing.error}
              data={
                loadingPlanMarketing.data?.pRTPlanMarketings.nodes.map(element => {
                  return {
                    ...element,
                  };
                }) as any
              }
              isSelectable
              onRequestSelected={(remunerations?: any[]) => {
                handleChange({
                  name: 'remunerationIds',
                  value: (remunerations || []).map(facture => facture.id || ''),
                });
                setOpenAssocieDialog(false);
              }}
              selectedItemsIds={
                document?.typeAvoirAssociations?.length
                  ? document.typeAvoirAssociations
                      .filter(avoirAssociation => avoirAssociation.type === 'BRI')
                      .map(avoirAssociation => avoirAssociation.correspondant)
                  : []
              }
            />
          )}
          {documentToSave.associer === 'REPRISE_PERIMES' && (
            <Box>
              <Box p={2}>
                <CustomEditorText
                  placeholder="Reprise des périmés"
                  value={documentToSave.reprisePerimes}
                  onChange={(value: string) => {
                    // setReprisePerimes(value);
                    handleChange({ name: 'remunerationIds', value });
                  }}
                />
              </Box>
              <Box display="flex" justifyContent="flex-end">
                <CustomButton
                  color="secondary"
                  onClick={(event: React.MouseEvent<HTMLButtonElement>) => {
                    event.preventDefault();
                    event.stopPropagation();
                    setOpenAssocieDialog(false);
                  }}
                  disabled={!documentToSave.reprisePerimes}
                  className={classes.joindreButton}
                >
                  Joindre
                </CustomButton>
              </Box>
            </Box>
          )}
        </Box>
      </CustomModal>
    </>
  );
};

export default CreateDocument;
