import { DocumentCategorie, FactureTva, IDocumentSearch } from '../../../EspaceDocumentaire';

export interface CreateDocumentProps {
  activeSousCategorie?: any | undefined;
  mode: 'creation' | 'remplacement';
  document?: DocumentCategorie;
  saving: boolean;
  onRequestCreateDocument: (document: DocumentCategorie) => void;
  open: boolean;
  setOpen: (value: boolean) => void;
  title: string;
  defaultType?: string;
  idDefaultOrigine?: string;
  defaultCorrespondant?: any;
  searchVariables?: IDocumentSearch;
  tvas: {
    loading: boolean;
    error: Error;
    data: any[];
  };
}

export interface TotalProps {
  hT?: boolean;
  ttc?: boolean;
  tva?: boolean;
}

export interface HandleChangeInterface {
    name: string;
    value: any;
}

export interface Tva {
  id: string;
  codeTva: number;
  tauxTva: number;
  designation: string;
}

export interface TvaLigne {
  idDocument?: string;
  idTva: string;
  montantHt?: any;
  montantTva?: number;
  montantTtc?: number;
  tva?: Tva;
}

export interface ChangeDocumentProps {
    description?: string;
    nomenclature?: string;
    numeroVersion?: string;
    dateHeureParution?: any | null;
    dateHeureDebutValidite?: any | null;
    dateHeureFinValidite?: any | null;
    idUserRedacteur?: string;
    idUserVerificateur?: string;
    userRedacteur: any[];
    userVerificateur: any[];
    idSousCategorie: any; 
    factureDate?: any;
    factureTotalHt?: any;
    factureTva?: any;
    factureTotalTtc?: any;
    type: string;
    numeroFacture?: string;
    dateReglement?: any;
    idReglementMode?: string;
    associer?: string;
    numeroCommande?: any;
    isGenererCommande: boolean;
    changeCondition?: any;
    conditionIds: string[];
    changeFacture?: any;
    factureIds: string[];
    remunerationIds: string[];
    reprisePerimes: string;
    nombreJoursPreavis?: number;
    isRenouvellementTacite?: boolean;
    isMultipleTva?: boolean;
    factureTvas?: TvaLigne[];
  }