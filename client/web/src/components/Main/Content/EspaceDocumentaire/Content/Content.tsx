import { Box, Button, IconButton, Menu, Tooltip } from '@material-ui/core';
import { Edit, GetApp, MoreHoriz, Share } from '@material-ui/icons';
import style from './style';
import React, { Dispatch, FC, SetStateAction, useEffect, useState } from 'react';
import { getGroupement, getUser } from '../../../../../services/LocalStorage';
import { useApolloClient, useQuery, useLazyQuery, useMutation } from '@apollo/react-hooks';
import {
  SMYLEYS,
  SMYLEYSVariables,
  SMYLEYS_smyleys,
} from '../../../../../graphql/Smyley/types/SMYLEYS';
import {
  USER_SMYLEYS,
  USER_SMYLEYSVariables,
} from '../../../../../graphql/Smyley/types/USER_SMYLEYS';
import {
  CREATE_GET_PRESIGNED_URL,
  CREATE_GET_PRESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_GET_PRESIGNED_URL';
import { DO_CREATE_GET_PRESIGNED_URL } from '../../../../../graphql/S3';
import { GET_SMYLEYS, GET_USER_SMYLEYS } from '../../../../../graphql/Smyley';
import { SEND_GED_DOCUMENT_BY_EMAIL } from '../../../../../federation/notification/notification-email/mutation';
import {
  SEND_GED_DOCUMENT_BY_EMAILVariables,
  SEND_GED_DOCUMENT_BY_EMAIL as SEND_GED_DOCUMENT_BY_EMAIL_TYPE,
} from '../../../../../federation/notification/notification-email/types/SEND_GED_DOCUMENT_BY_EMAIL';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
// import ReactIframe from 'react-iframe';

import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import CommentList from '../../../../Comment/CommentList';
import { Comment } from '../../../../Comment';
import { DocumentCardMenu } from '../Sidebar/DocumentCardMenu';
import { DocumentCategorie, User, Role, IDocumentSearch } from '../EspaceDocumentaire';
import NoItemContentImage from '../../../../Common/NoItemContentImage';
import UserAction from '../../../../Common/UserAction';
import { ErrorPage, Loader, CustomModal, CustomFormTextField, CustomEditorText } from '@app/ui-kit';
import Chip from '@material-ui/core/Chip';
import InputAdornment from '@material-ui/core/InputAdornment';
import Alert from '@material-ui/lab/Alert';
import AlertDonneesMedicales from './../../../../Common/CustomAlertDonneesMedicales';
import {
  useValueParameterAsBoolean,
  useValueParameterAsString,
} from '../../../../../utils/getValueParameter';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { isMobile } from '../../../../../utils/Helpers';
import {
  UPDATE_DOCUMENT_STATUS as UPDATE_DOCUMENT_STATUS_TYPE,
  UPDATE_DOCUMENT_STATUSVariables,
} from '../../../../../federation/ged/document/types/UPDATE_DOCUMENT_STATUS';
import { UPDATE_DOCUMENT_STATUS } from '../../../../../federation/ged/document/mutation';
import moment from 'moment';
import { AppAuthorization } from '../../../../../services/authorization';

import classNames from 'classnames';
import { MagnifierIcon } from '../Sidebar/DocumentCard/MagnifierIcon/MagnifierIcon';
import {
  OCR_DOCUMENTS,
  OCR_DOCUMENTSVariables,
} from '../../../../../federation/ged/document/types/OCR_DOCUMENTS';
import { GET_OCR_DOCUMENTS } from '../../../../../federation/ged/document/query';
import { InfoLine } from './InfoLine/InfoLine';
import CreateDocument from './CreateDocument';

const mimeTypes = require('mime-types');

const STATUT_LABELS = {
  EN_ATTENTE_APPROBATION: 'En attente',
  REFUSE: 'Rejeté',
  APPROUVE: 'Validé',
};
export interface IContent {
  content: {
    loading: boolean;
    error: Error;
    data: DocumentCategorie;
  };
  commentaires: {
    loading: boolean;
    error: Error;
    data: any;
  };
  documentIds?: string[];
  saving: boolean;
  saved: boolean;
  setSaved: (newValue: boolean) => void;
  activeSousCategorie: any | undefined;
  activeDocument: any | undefined;
  onRequestCreateDocument: (document: any) => void;
  onRequestAddToFavorite: (document: any) => void;
  onRequestDelete: (document: any) => void;
  onRequestRemoveFromFavorite: (document: any) => void;
  onRequestReplace: (document: any) => void;
  onRequestFetchMoreComments: (document: DocumentCategorie) => void;
  onRequestLike: (document: DocumentCategorie, smyley: SMYLEYS_smyleys | null) => void;
  onRequestDeplace: (document: DocumentCategorie) => void;
  onCompleteDownload: (document: DocumentCategorie) => void;
  handleFilterMenu(): void;
  handleSidebarMenu(): void;
  refetchComments: () => void;
  mobileView: boolean;
  tabletView: boolean;
  handleFilterMenu(): void;
  handleSidebarMenu(): void;
  refetchSmyleys: () => void;
  hidePartage?: boolean;
  hideRecommandation?: boolean;
  onRequestValider?: (id: string) => void;
  onRequestRefuser?: (id: string) => void;
  validation?: boolean;
  refetchDocument: () => void;
  onRequestDocument: (id: string) => void;
  onRequestFetchAll: (withSearch: boolean) => void;
  searchVariables?: IDocumentSearch;
  setSearchVariables?: Dispatch<SetStateAction<IDocumentSearch>>;
  noValidation?: boolean;

  espaceType?: string;
  tvas: {
    loading: boolean;
    error: Error;
    data: any[];
  };
  defaultCorrespondant?: any;
}

const Content: FC<IContent> = ({
  onRequestAddToFavorite,
  onRequestDelete,
  onRequestRemoveFromFavorite,
  onRequestReplace,
  onRequestFetchMoreComments,
  onRequestDeplace,
  refetchComments,
  refetchSmyleys,
  onCompleteDownload,
  saving,
  saved,
  setSaved,
  content,
  commentaires,
  validation,
  activeDocument,
  activeSousCategorie,
  hidePartage,
  hideRecommandation,
  onRequestValider,
  onRequestRefuser,
  documentIds,
  onRequestDocument,
  searchVariables,
  onRequestFetchAll,
  setSearchVariables,
  noValidation = false,

  espaceType,
  tvas,
  defaultCorrespondant,
}) => {
  const commentInputRef = React.useRef<HTMLInputElement>();

  const classes = style();
  const user = getUser();
  const auth = new AppAuthorization(user);
  const enableSendGedEmailGroupement = useValueParameterAsBoolean('0826');
  const enableSendGedEmailPharmacie = useValueParameterAsBoolean('0726');
  const enableValidationPrestataire = useValueParameterAsBoolean('0850');
  const idPrestataireValidateur = useValueParameterAsString('0851');
  const enableOCRisation = useValueParameterAsBoolean('0839');

  const [option, setOption] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [downloading, setDownloading] = useState<boolean>(false);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [documentIdsCurrent, setDocumentIdsCurrent] = useState<string[] | undefined>();
  const [page, setPage] = useState<number>(0);
  const [openReplaceDocument, setOpenReplaceDocument] = useState<boolean>(false);
  const [loadingPreview, setLoadingPreview] = useState<boolean>(true);

  const changementStatuts = activeDocument?.changementStatuts || [];
  const changementStatutValidationInterne = changementStatuts.find(
    ({ createdBy }) => !createdBy.prestataireService,
  );
  const changementStatutValidationPrestataireValidateur = changementStatuts.find(
    ({ createdBy }) => createdBy.prestataireService,
  );

  const isFactureValid = (document: any) => {
    return (
      document &&
      document.type === 'FACTURE' &&
      document.idOrigine &&
      document.idOrigineAssocie &&
      document.factureDate &&
      document.factureTotalHt &&
      document.factureTva
    );
  };

  useEffect(() => {
    // if the user have an acces to validate the doc
    if (validation) {
      setIsOpen(true);
    }
  }, []);

  useEffect(() => {
    setDocumentIdsCurrent(documentIds);
    if (documentIds && content.data) {
      setPage(documentIds?.indexOf(content.data.id || ''));
    }
  }, [documentIds, content.data]);

  useEffect(() => {
    if (saved) {
      setOpenReplaceDocument(false);
      // setSaved(false);
    }
  }, [saved]);

  const handlePrev = () => {
    if (documentIdsCurrent && page >= 1) {
      onRequestDocument(documentIdsCurrent[page - 1]);
      setPage(page - 1);
    }
  };
  const handleNext = () => {
    if (documentIdsCurrent && page < documentIdsCurrent.length - 1) {
      onRequestDocument(documentIdsCurrent[page + 1]);
      setPage(page + 1);
    }
  };

  const handleToggleOption = (event: React.MouseEvent) => {
    setOption(prev => !prev);
    setAnchorEl(event.currentTarget as HTMLElement);
  };

  const changeStatutDocument = (statut: 'REFUSE' | 'APPROUVE' | 'PAYE'): void => {
    if (statut === 'REFUSE' && (commentaires?.data?.total || 0) > 0) {
      updateDocumentStatut({
        variables: {
          input: {
            idDocument: activeDocument.id as any,
            commentaire: '',
            status: statut,
          },
        },
      }).then(() => {
        setSearchVariables &&
          setSearchVariables(prev => ({
            ...prev,
            type: 'FACTURE',
            accesRapide: 'EN_ATTENTE_APPROBATION',
          }));
      });
    } else if (statut === 'REFUSE' && commentaires?.data?.total === 0) {
      displaySnackBar(client, {
        message: 'Le commentaire est obligatoire en cas de refus de document.',
        type: 'ERROR',
        isOpen: true,
      });
      commentInputRef.current?.focus();
    } else if (statut === 'APPROUVE' || statut === 'PAYE') {
      if (activeDocument.type === 'FACTURE' && !isFactureValid(activeDocument)) {
        displaySnackBar(client, {
          message: `Veuillez remplir tous les champs obligatoires avant de ${
            statut === 'APPROUVE' ? 'valider' : 'payer'
          } la facture`,
          type: 'ERROR',
          isOpen: true,
        });
        handleModifierDocument();
        return;
      }

      updateDocumentStatut({
        variables: {
          input: {
            idDocument: activeDocument.id as any,
            commentaire: '',
            status: statut,
          },
        },
      }).then(() => {
        setSearchVariables &&
          setSearchVariables(prev => ({
            ...prev,
            type: 'FACTURE',
            accesRapide: 'EN_ATTENTE_APPROBATION',
          }));
      });
    }
  };

  const handleClose = () => {
    setOption(false);
    setAnchorEl(null);
  };

  const downloadDocument = (documentToDownload: DocumentCategorie): void => {
    const { publicUrl, nomOriginal } = documentToDownload.fichier || {};
    if (publicUrl && nomOriginal) {
      setDownloading(true);
      fetch(publicUrl)
        .then(response => response.blob())
        .then(blob => {
          const downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(
            new Blob([blob], { type: mimeTypes.lookup(nomOriginal) }),
          );
          downloadLink.download = nomOriginal;
          downloadLink.click();

          onCompleteDownload(documentToDownload);
          setDownloading(false);
        });
    }
  };

  const client = useApolloClient();
  const groupement = getGroupement();

  const [sendGedDocument, sendingGedDocument] = useMutation<
    SEND_GED_DOCUMENT_BY_EMAIL_TYPE,
    SEND_GED_DOCUMENT_BY_EMAILVariables
  >(SEND_GED_DOCUMENT_BY_EMAIL, {
    client: FEDERATION_CLIENT,
  });

  const [doCreateGetPresignedUrls, doCreateGetPresignedUrlsResult] = useMutation<
    CREATE_GET_PRESIGNED_URL,
    CREATE_GET_PRESIGNED_URLVariables
  >(DO_CREATE_GET_PRESIGNED_URL);

  /**Change Status  */
  const [updateDocumentStatut, updatingDocumentStatut] = useMutation<
    UPDATE_DOCUMENT_STATUS_TYPE,
    UPDATE_DOCUMENT_STATUSVariables
  >(UPDATE_DOCUMENT_STATUS, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le statut du document a été changé avec succès!',
        isOpen: true,
        type: 'SUCCESS',
      });

      onRequestFetchAll(true);
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant le changement du statut du document!',
        isOpen: true,
        type: 'ERROR',
      });
    },
    client: FEDERATION_CLIENT,
  });

  /**get validation document */

  const getSmyleys = useQuery<SMYLEYS, SMYLEYSVariables>(GET_SMYLEYS, {
    variables: {
      idGroupement: groupement && groupement.id,
    },

    onCompleted: smyleysResult => {
      if (smyleysResult.smyleys) {
        const filePaths: string[] = smyleysResult.smyleys
          .filter(item => !!item?.photo)
          .map((item: any) => item.photo);

        if (filePaths.length > 0) {
          doCreateGetPresignedUrls({ variables: { filePaths } });
        }
      }
    },

    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  const [loadUserSmyleys, getUserSmyleys] = useLazyQuery<USER_SMYLEYS, USER_SMYLEYSVariables>(
    GET_USER_SMYLEYS,
  );

  const smyleys = getSmyleys && getSmyleys.data && getSmyleys.data.smyleys;

  const presignedUrls =
    doCreateGetPresignedUrlsResult &&
    doCreateGetPresignedUrlsResult.data &&
    doCreateGetPresignedUrlsResult.data.createGetPresignedUrls;

  /**
   * Handle open modal send email
   */
  const [openModal, setOpenModal] = useState<boolean>(false);
  const handleOpenModal = () => {
    setOpenModal(!openModal);
  };

  const handleModifierDocument = () => {
    setOpenReplaceDocument(true);
    setSaved(false);
  };

  /*
   * Send email ************************************************************************************
   */
  const [emailReceivers, setEmailReceivers] = React.useState<string[]>([]);
  const [emailHtml, setEmailHtml] = React.useState<any>('');

  const [messageSubmitted, setMessageSubmitted] = React.useState<{
    error: boolean;
    success: boolean;
    message: string | null | undefined;
  }>({
    error: false,
    success: false,
    message: '',
  });

  const [loadOcrDocument] = useLazyQuery<OCR_DOCUMENTS, OCR_DOCUMENTSVariables>(GET_OCR_DOCUMENTS, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: "L'OCR de ce document a été effectué avec succès !",
        type: 'SUCCESS',
        isOpen: true,
      });
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs se sont survenues pendant l'OCR du document !",
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const handleChangeMotCle = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const { value } = event.target as HTMLInputElement;
    const temp: string[] = [];
    const input: string[] = value.split(',');
    input.forEach((motCle: string) => temp.push(motCle));

    setEmailReceivers(temp);
  };

  const handleDeleteMotsCle = (index: number) => (_: React.MouseEvent) => {
    if (typeof emailReceivers === 'object') {
      emailReceivers.splice(index, 1);
      setEmailReceivers([...emailReceivers]);
    } else setEmailReceivers(['']);
  };

  const handleSubmitSendEmail = (e: React.FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
    const emailRegex = /^([A-Za-z0-9_\-.+])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,})$/;
    let i = 0;
    emailReceivers.forEach((element: string) => {
      if (!emailRegex.test(element.trim())) {
        i++;
        setMessageSubmitted({ error: true, success: false, message: 'Email invalide' });
      }
    });
    if (i == 0) {
      const document = (documentToDownload: DocumentCategorie): { url: string; name: string } => {
        const { publicUrl, nomOriginal } = documentToDownload.fichier || {};
        if (publicUrl && nomOriginal) {
          return { url: publicUrl, name: nomOriginal };
        }
        return { url: '', name: '' };
      };

      sendGedDocument({
        variables: {
          input: {
            to: emailReceivers,
            html: emailHtml,
            attachment: {
              content: document(content.data).url,
              filename: document(content.data).name,
              type: 'application/pdf',
            },
          },
        },
      })
        .then(response => {
          let message = response.data?.sendGEDDocumentByEmail?.message;
          if (message == undefined || message == null) {
            message = 'Une erreur est survenue';
          }
          if (response.data?.sendGEDDocumentByEmail?.code == '200') {
            setMessageSubmitted({ error: false, success: true, message: message });
            setEmailReceivers([]);
          } else {
            setMessageSubmitted({ error: true, success: false, message: message });
          }
        })
        .catch(() => {
          setMessageSubmitted({ error: true, success: false, message: 'Une erreur est survenue' });
        });
    }
  };

  const handleOcR = () => {
    if (activeDocument?.id) {
      loadOcrDocument({
        variables: {
          idDocument: activeDocument?.id,
        },
      });
    }
  };

  /**Validation authorization */
  const isAuthorizedToValidateDocument = (document: any) => {
    if (document.type === 'FACTURE') {
      if (
        enableValidationPrestataire &&
        idPrestataireValidateur &&
        user?.userPartenaire?.id &&
        idPrestataireValidateur === user?.userPartenaire?.id
      ) {
        return true;
      }
      return auth.isAuthorizedToValidateGedDocument();
    }

    if (
      document.sousCategorie?.workflowValidation === 'INTERNE' ||
      document.categorie?.workflowValidation === 'INTERNE'
    ) {
      return auth.isAuthorizedToValidateGedDocument();
    }
    if (
      document.sousCategorie?.workflowValidation === 'UTILISATEUR' ||
      document.categorie?.workflowValidation === 'UTILISATEUR'
    ) {
      return true;
    }

    if (
      document.sousCategorie?.workflowValidation === 'PARTENAIRE_SERVICE' ||
      document.categorie?.workflowValidation === 'PARTENAIRE_SERVICE'
    ) {
      return (
        document.sousCategorie?.idPartenaireValidateur === user?.userPartenaire?.id ||
        document.categorie?.idPartenaireValidateur === user?.userPartenaire?.id
      );
    }
    return false;
  };

  /********************************************************************************************/

  React.useEffect(() => {
    if (activeDocument?.id) {
      setLoadingPreview(true);
      loadUserSmyleys({
        variables: {
          codeItem: 'GED_DOCUMENT',
          idSource: activeDocument.id,
        },
      });
    }
  }, [activeDocument]);

  return (
    <>
      <Box
        display="flex"
        flexDirection="column"
        flex={1}
        className={classNames(classes.root, { [classes.hidescrollBarContent]: isMobile() })}
      >
        {activeDocument ? (
          content.loading ? (
            <Loader />
          ) : !content.error && content.data ? (
            <Box
              className={classNames(classes.content, {
                [classes.hidescrollBarContent]: isMobile(),
              })}
            >
              <Box className={classes.header}>
                <h2>
                  <div dangerouslySetInnerHTML={{ __html: content.data.description || '' }} />
                </h2>
                <Box className={classes.actionHeader}>
                  {isMobile() ? (
                    <IconButton
                      style={{ marginRight: 16 }}
                      size="small"
                      onClick={() => downloadDocument(content?.data)}
                    >
                      <GetApp />
                    </IconButton>
                  ) : (
                    <Box className={classes.pagination}>
                      <IconButton size="small" onClick={handlePrev}>
                        <ChevronLeftIcon />
                      </IconButton>
                      <IconButton size="small" onClick={handleNext}>
                        <ChevronRightIcon />
                      </IconButton>
                    </Box>
                  )}
                  <IconButton size="small" onClick={handleToggleOption}>
                    <MoreHoriz />
                  </IconButton>
                </Box>
              </Box>
              <Box marginBottom="25px">
                <InfoLine
                  items={[
                    {
                      label: 'Créé par',
                      value: (activeDocument?.createdBy?.fullName || '').split(' ')[0],
                    },
                    {
                      label: 'Origine',
                      value:
                        activeDocument?.origineAssocie?.nom ||
                        activeDocument?.origineAssocie?.fullName,
                    },
                    {
                      label: 'Statut',
                      value:
                        'FACTURE' === activeDocument?.type ||
                        isAuthorizedToValidateDocument(activeDocument)
                          ? changementStatutValidationInterne
                            ? changementStatutValidationInterne.status === 'PAYE'
                              ? 'Payé'
                              : `${STATUT_LABELS[changementStatutValidationInterne.status]} par ${
                                  (
                                    changementStatutValidationInterne.createdBy?.fullName || ''
                                  ).split(' ')[0]
                                }`
                            : STATUT_LABELS.EN_ATTENTE_APPROBATION
                          : undefined,
                    },
                    {
                      label: 'Validation comptable',
                      value:
                        ('FACTURE' === activeDocument?.type ||
                          isAuthorizedToValidateDocument(activeDocument)) &&
                        enableValidationPrestataire &&
                        'APPROUVE' === changementStatutValidationInterne?.status
                          ? changementStatutValidationPrestataireValidateur
                            ? STATUT_LABELS[changementStatutValidationPrestataireValidateur.status]
                            : STATUT_LABELS.EN_ATTENTE_APPROBATION
                          : undefined,
                    },
                  ]}
                />
                <InfoLine
                  items={[
                    {
                      label: 'Facture n°',
                      value:
                        activeDocument.type === 'FACTURE'
                          ? activeDocument?.numeroFacture
                          : undefined,
                    },
                    {
                      label: 'Montant TTC',
                      value:
                        activeDocument.type === 'FACTURE' && activeDocument?.factureTotalTtc
                          ? `${(activeDocument?.factureTotalTtc).toFixed(2)} €`
                          : undefined,
                    },
                    {
                      label: 'Date signature',
                      value:
                        'CONTRAT' === activeDocument.type && activeDocument?.dateHeureParution
                          ? moment(activeDocument?.dateHeureParution).format('DD/MM/YYYY')
                          : undefined,
                    },
                    {
                      label: 'Date début',
                      value:
                        'CONTRAT' === activeDocument.type && activeDocument?.dateHeureDebutValidite
                          ? moment(activeDocument?.dateHeureDebutValidite).format('DD/MM/YYYY')
                          : undefined,
                    },
                    {
                      label: 'Date fin',
                      value:
                        'CONTRAT' === activeDocument.type && activeDocument?.dateHeureFinValidite
                          ? moment(activeDocument?.dateHeureFinValidite).format('DD/MM/YYYY')
                          : undefined,
                    },
                    {
                      label: 'Renouvellement par tacite',
                      value:
                        'CONTRAT' === activeDocument.type
                          ? activeDocument?.isRenouvellementTacite
                            ? 'Oui'
                            : 'Non'
                          : undefined,
                    },
                    {
                      label: 'Nombre de jour de préavis',
                      value:
                        'CONTRAT' === activeDocument.type && activeDocument?.nombreJoursPreavis
                          ? `${activeDocument?.nombreJoursPreavis} jour${
                              activeDocument?.nombreJoursPreavis > 1 ? 's' : ''
                            }`
                          : undefined,
                    },
                  ]}
                />
              </Box>
              {!isMobile() && (
                <Box className={classes.actionBtnContainer}>
                  <Box className={classes.actionDocBtn}>
                    <Button
                      onClick={() => downloadDocument(content?.data)}
                      variant="contained"
                      color="secondary"
                      startIcon={<GetApp />}
                      style={{ cursor: 'pointer' }}
                      disabled={downloading}
                    >
                      Télécharger
                    </Button>
                    {enableSendGedEmailGroupement && enableSendGedEmailPharmacie && (
                      <Button
                        onClick={handleOpenModal}
                        variant="contained"
                        color="secondary"
                        startIcon={<Share />}
                        style={{ cursor: 'pointer', marginLeft: 5 }}
                      >
                        Envoyer par email
                      </Button>
                    )}

                    {enableOCRisation && !activeDocument?.isOcr && (
                      <Box pl={1}>
                        <Tooltip title="OCR-iser le document">
                          <IconButton onClick={handleOcR}>
                            <MagnifierIcon />
                          </IconButton>
                        </Tooltip>
                      </Box>
                    )}
                  </Box>
                  {((user?.userPartenaire?.id &&
                    !changementStatutValidationPrestataireValidateur) ||
                    !changementStatutValidationInterne) &&
                    isAuthorizedToValidateDocument(activeDocument) &&
                    !noValidation && (
                      <Box className={classes.listActionValidation}>
                        <Box className={classes.btnValidation}>
                          <Button
                            variant="text"
                            color="secondary"
                            disabled={updatingDocumentStatut.loading}
                            onClick={() => {
                              changeStatutDocument('REFUSE');
                            }}
                          >
                            Refuser
                          </Button>
                          <Button
                            style={{ marginLeft: '5%', height: 33 }}
                            variant="outlined"
                            color="secondary"
                            disabled={updatingDocumentStatut.loading}
                            onClick={() => {
                              changeStatutDocument('APPROUVE');
                            }}
                          >
                            {user?.userPartenaire?.id ? 'Comptabiliser' : 'Valider'}
                          </Button>
                        </Box>
                      </Box>
                    )

                  /*: activeDocument?.dernierChangementStatut?.status === 'APPROUVE' ? (
                    <Box className={classes.messageValidation}>
                      <Alert severity="success" className={classes.alert}>
                        Validé le:{' '}
                        {moment(activeDocument.dernierChangementStatut.createdAt).format(
                          'DD/MM/YYYY',
                        )}
                      </Alert>
                    </Box>
                  ) : (
                    activeDocument?.dernierChangementStatut?.status === 'REFUSE' && (
                      <Box className={classes.messageValidation}>
                        <Alert severity="error" className={classes.alert}>
                          Refusé
                        </Alert>
                      </Box>
                    )
                    ) */
                  }
                  {!user?.userPartenaire?.id &&
                    changementStatutValidationPrestataireValidateur?.status === 'APPROUVE' &&
                    changementStatutValidationInterne?.status !== 'PAYE' &&
                    isAuthorizedToValidateDocument(activeDocument) &&
                    !noValidation && (
                      <Box className={classes.listActionValidation}>
                        <Box className={classes.btnValidation}>
                          <Button
                            style={{ marginLeft: '5%', height: 33 }}
                            variant="outlined"
                            color="secondary"
                            disabled={updatingDocumentStatut.loading}
                            onClick={() => {
                              changeStatutDocument('PAYE');
                            }}
                          >
                            Payer
                          </Button>
                        </Box>
                      </Box>
                    )}
                  {!user?.userPartenaire?.id && content.data.type === 'FACTURE' && !noValidation && (
                    <Box
                      className={
                        ((user?.userPartenaire?.id &&
                          !changementStatutValidationPrestataireValidateur) ||
                          !changementStatutValidationInterne ||
                          (!user?.userPartenaire?.id &&
                            changementStatutValidationPrestataireValidateur?.status ===
                              'APPROUVE' &&
                            changementStatutValidationInterne?.status !== 'PAYE')) &&
                        isAuthorizedToValidateDocument(activeDocument)
                          ? undefined
                          : classes.listActionValidation
                      }
                    >
                      <Button
                        onClick={handleModifierDocument}
                        style={{ marginLeft: 8 }}
                        variant="contained"
                        color="secondary"
                      >
                        <Edit />
                      </Button>
                    </Box>
                  )}
                </Box>
              )}

              {loadingPreview && <Loader />}

              <iframe
                className={classes.view}
                src={`https://docs.google.com/viewerng/viewer?url=${
                  content?.data?.fichier?.publicUrl
                }&embedded=true&random=${Math.random()}`}
                frameBorder="0"
                width="100%"
                onLoad={() => setLoadingPreview(false)}
              />
              {/* <ReactIframe
                id={content.data.id}
                className={classes.view}
                width="100%"
                onLoad={()=>setLoadingPreview(false)}
                url={`${content?.data?.fichier?.presignedUrl}#toolbar=0&embedded=true&navpanes=0&scrollbar=0`}
              /> */}
              {activeDocument && (
                <UserAction
                  refetchSmyleys={refetchSmyleys}
                  codeItem="GED_DOCUMENT"
                  idSource={activeDocument.id}
                  nbComment={activeDocument.nombreCommentaires}
                  nbSmyley={activeDocument.nombreReactions}
                  userSmyleys={getUserSmyleys.data?.userSmyleys as any}
                  presignedUrls={presignedUrls}
                  smyleys={smyleys}
                  hidePartage={hidePartage}
                  hideRecommandation={hideRecommandation}
                />
              )}

              {commentaires.loading ? (
                <Loader />
              ) : commentaires.error ? (
                <ErrorPage />
              ) : (
                <CommentList
                  key={`comment_list`}
                  comments={commentaires.data}
                  fetchMoreComments={() => onRequestFetchMoreComments as any}
                />
              )}

              {!commentaires.loading && (
                <Comment
                  forwardedInputRef={commentInputRef as any}
                  refetch={refetchComments}
                  codeItem="GED_DOCUMENT"
                  idSource={content.data?.id as any}
                />
              )}
            </Box>
          ) : (
            <ErrorPage />
          )
        ) : (
          <NoItemContentImage
            title="Détails de document"
            subtitle="Veuilez cliquer sur un document de la partie gauche ou bien même en ajouter un pour voir les détails dans cette partie"
          />
        )}
      </Box>
      <Menu
        open={option}
        onClose={handleClose}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <DocumentCardMenu
          isOpenDetail={isOpen}
          document={content.data}
          setOpenOptions={setOption}
          saving={saving}
          saved={saved}
          setSaved={setSaved}
          onRequestAddToFavorite={onRequestAddToFavorite}
          onRequestDelete={onRequestDelete}
          onRequestRemoveFromFavorite={onRequestRemoveFromFavorite}
          onRequestReplaceDocument={onRequestReplace}
          onRequestDeplace={onRequestDeplace}
          onRequestRefuser={onRequestRefuser}
          onRequestValider={onRequestValider}
          searchVariables={searchVariables}
          activeSousCategorie={activeSousCategorie}
          espaceType={espaceType}
          tvas={tvas}
        />
      </Menu>
      <CustomModal
        open={openModal}
        setOpen={handleOpenModal}
        title="Envoyer par email"
        withBtnsActions={false}
        closeIcon={true}
        headerWithBgColor={true}
        maxWidth="lg"
      >
        <form onSubmit={handleSubmitSendEmail}>
          <AlertDonneesMedicales style={{ marginBottom: 10 }} />
          <Box my={3}>
            <CustomFormTextField
              required
              placeholder="Email"
              value={emailReceivers}
              onChange={handleChangeMotCle}
              variant="standard"
              InputProps={{
                startAdornment: <InputAdornment position="start">A:</InputAdornment>,
              }}
            />
            <Box>
              {typeof emailReceivers === 'object' && emailReceivers.length
                ? emailReceivers.map((element: string, index: number) => (
                    <Chip
                      size="small"
                      label={element}
                      onDelete={handleDeleteMotsCle(index)}
                      key={index}
                      style={{ marginLeft: 2, marginBottom: 2 }}
                    />
                  ))
                : null}
            </Box>
            <Box>
              <CustomEditorText value={emailHtml} onChange={setEmailHtml} />
            </Box>
          </Box>
          <Box my={3}>
            {messageSubmitted.error && <Alert severity="error">{messageSubmitted.message}</Alert>}
            {messageSubmitted.success && (
              <Alert severity="success">{messageSubmitted.message}</Alert>
            )}
            <Button
              variant="contained"
              color="secondary"
              type="submit"
              style={{ marginLeft: 25 }}
              disabled={sendingGedDocument.loading}
            >
              Envoyer
            </Button>
          </Box>
        </form>
      </CustomModal>
      <CreateDocument
        open={openReplaceDocument}
        setOpen={setOpenReplaceDocument}
        title={
          'espace-facture' === espaceType
            ? 'Modification de la facture'
            : 'Modification de document'
        }
        saving={saving}
        activeSousCategorie={activeSousCategorie}
        document={content.data}
        mode="remplacement"
        onRequestCreateDocument={onRequestReplace as any}
        searchVariables={searchVariables}
        defaultType={'espace-facture' === espaceType ? 'FACTURE' : undefined}
        tvas={tvas}
        defaultCorrespondant={defaultCorrespondant}
      />
    </>
  );
};

export default Content;
