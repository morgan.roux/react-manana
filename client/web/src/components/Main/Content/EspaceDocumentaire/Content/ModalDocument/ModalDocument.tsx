import {
  Dialog,
  DialogProps,
  DialogTitle,
  IconButton,
  makeStyles,
  Typography,
} from '@material-ui/core';
import { Close } from '@material-ui/icons';
import React, { FC } from 'react';

const styles = makeStyles(theme => ({
  bgTitle: {
    position: 'relative',
    color: theme.palette.common.white,
    backgroundColor: theme.palette.primary.main,
    '& h2': {
      fontSize: 30,
    },
  },
  btnStyle: {
    position: 'absolute',
    top: theme.spacing(2),
    right: theme.spacing(2),
  },
}));

const ModalDocument: FC<DialogProps & {
  heading?: boolean;
  label?: string;
  handleClose?(): void;
}> = ({ label, children, handleClose, maxWidth, heading = true, ...props }) => {
  const classes = styles();
  return (
    <Dialog {...props} maxWidth={maxWidth ? maxWidth : 'md'} fullWidth>
      {heading && (
        <DialogTitle className={classes.bgTitle} disableTypography>
          <Typography variant="h2">{label}</Typography>
          <IconButton size="small" className={classes.btnStyle} onClick={handleClose}>
            <Close htmlColor="#FFF" />
          </IconButton>
        </DialogTitle>
      )}
      {children}
    </Dialog>
  );
};

export default ModalDocument;
