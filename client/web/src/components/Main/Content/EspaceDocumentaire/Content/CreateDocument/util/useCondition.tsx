import ConditionCommerciale, {
  ConditionCommandeItem,
  ConditionStatus,
  TypeConditionCommande,
} from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/ConditionCommerciale/ConditionCommerciale';
import React from 'react';

interface ConditionCommercialePageProps {
  data: ConditionCommandeItem[];
  error?: Error;
  loading: boolean;
  total: number;
  typesConditionCommande: TypeConditionCommande[];
  listStatusConditionCommande: ConditionStatus[];
  onRequestSearch: ({ skip, take, searchText, filters, sortTable }: any) => void;
  isFacture?: boolean;
  onRequestSelected?: (conditions: ConditionCommandeItem[]) => void;
}

export const useCondition = (props: ConditionCommercialePageProps | undefined): [JSX.Element] => {
  //   const [conditionIds, setConditionIds] = React.useState<string[]>([]);
  //   const selectedConditionIds = (condition: ConditionCommandeItem[]) => {
  //     // setConditionIds((conditin || []).map((condition) => condition.id || ''));
  //   };
  return [
    <ConditionCommerciale
      key="condition"
      data={props?.data || []}
      error={props?.error}
      total={props?.total || 0}
      loading={props?.loading || false}
      typesConditionCommande={props?.typesConditionCommande || []}
      listStatusConditionCommande={props?.listStatusConditionCommande || []}
      onRequestSearch={props?.onRequestSearch}
      onRequestSelected={props?.onRequestSelected}
      isFacture
    />,
  ];
};
