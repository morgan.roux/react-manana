import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
  GEDContainer: {
    display: 'flex',
    width: '100%',
    height: 'calc(100vh - 86px)',
    overflow: 'auto',
    overflowY: 'hidden',
  },
  container: {
    display: 'flex',
    flex: 1,
    height: '100%',
  },
});
