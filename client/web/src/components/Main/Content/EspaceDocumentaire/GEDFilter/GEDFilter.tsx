import {
  Box,
  FormControl,
  FormControlLabel,
  IconButton,
  MenuItem,
  Radio,
  RadioGroup,
  Slider,
  Typography,
} from '@material-ui/core';
import React, { FC, useState, useEffect, Dispatch, SetStateAction } from 'react';
import style from './style';
import TreeStructure from './TreeStructure';
import { CustomDatePicker, DebouncedSearchInput, ErrorPage } from '@app/ui-kit';
import { IDocumentSearch } from '../EspaceDocumentaire';
import TreeItem from '@material-ui/lab/TreeItem';
import { TreeView } from '@material-ui/lab';
import ReplayIcon from '@material-ui/icons/Replay';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { isMobile } from '../../../../../utils/Helpers';
import useOrigines from './../../../../hooks/basis/useOrigines';
import classNames from 'classnames';
import moment from 'moment';
import {
  useValueParameterAsBoolean,
  useValueParameterAsString,
} from '../../../../../utils/getValueParameter';
import { getUser } from '../../../../../services/LocalStorage';
import MobileTopBar from '../../../../Common/MobileTopBar';
interface TemporalCategorie {
  nom: string;
  dateDebut: Date;
  dateFin: Date;
}

const filterStatutsDocument = [
  {
    id: '1',
    libelle: 'Tous les documents',
    code: 'TOUT',
  },
  {
    id: '2',
    libelle: 'Documents à traiter',
    code: 'EN_ATTENTE_APPROBATION',
  },

  {
    id: '3',
    libelle: 'Documents validés',
    code: 'APPROUVES',
  },

  {
    id: '4',
    libelle: 'Documents refusés',
    code: 'REFUSES',
  },
];

export interface FilterRadioProps {
  // valueFilterBy?: any;
  // valueFilterTime?: any;
  valueFilterSatut?: any;
  // onChangeFilterBy: (value: any) => void;
  // onChangeFilterTime: (value: any) => void;
  onChangeFilterStatut: (value: any) => void;
  espaceType?: string;
}

export const FilterRadio: FC<FilterRadioProps> = ({
  // valueFilterBy,
  // valueFilterTime,
  valueFilterSatut = 'TOUT',
  // onChangeFilterBy,
  // onChangeFilterTime,
  onChangeFilterStatut,
  espaceType,
}) => {
  /* const handleChangeFilterType = (__event: React.ChangeEvent<HTMLInputElement>, value: string) => {
     onChangeFilterBy(JSON.parse(value));
   };
 
   const handleChangeFilterTime = (__event: React.ChangeEvent<HTMLInputElement>, value: string) => {
     onChangeFilterTime(JSON.parse(value));
   };
   */

  const handleChangeFilterStatut = (
    __event: React.ChangeEvent<HTMLInputElement>,
    value: string,
  ) => {
    onChangeFilterStatut(value);
  };
  return (
    <>
      <FormControl style={isMobile() ? { marginTop: 32 } : undefined} component="fieldset">
        <RadioGroup name="doc" value={valueFilterSatut} onChange={handleChangeFilterStatut}>
          {('espace-facture' === espaceType ? [] : filterStatutsDocument).map(value => (
            <FormControlLabel
              value={value.code}
              control={<Radio />}
              label={value.libelle}
              checked={value.code === valueFilterSatut}
            />
          ))}
        </RadioGroup>
      </FormControl>

      {/*<FormControl style={{ marginBottom: 24 }} component="fieldset">
        <RadioGroup name="doc" value={valueFilterBy} onChange={handleChangeFilterType}>
          {filterDocumentProprietaire.map(value => (
            <FormControlLabel
              value={JSON.stringify(value)}
              control={<Radio />}
              label={value.libelle}
            />
          ))}
        </RadioGroup>
      </FormControl>
      <hr />
      <FormControl style={{ margin: '24px 0' }} component="fieldset">
        <RadioGroup name="doc" value={valueFilterTime} onChange={handleChangeFilterTime}>
          {filterDocumentFilterBy.map(value => (
            <FormControlLabel
              value={JSON.stringify(value)}
              control={<Radio />}
              label={value.libelle}
            />
          ))}
        </RadioGroup>
      </FormControl>
      <hr />*/}
    </>
  );
};

interface GedFilterProps {
  categories: {
    loading: boolean;
    error: Error;
    data: any[];
  };
  factureLaboratoires: any[];
  facturePartenaires: any[];
  facturesRejeteesInterneCategories: { annee: number; mois: number }[];
  facturesRejeteesPrestataireServiceCategories: { annee: number; mois: number }[];
  activeSousCategorie: any | undefined;
  setActiveSousCategorie: (sousCategorie: any | undefined) => void;
  setSearchVariables: Dispatch<SetStateAction<IDocumentSearch>>;
  searchVariables: IDocumentSearch;
  onRequestRefresh: () => void;
  onGoBack: () => void;
  contratFournisseurs?: any[]; // Laboratoire ou PrestataireService

  espaceType?: string;
  setOpenDrawer?: React.Dispatch<React.SetStateAction<boolean>>;
  maxTTC?: number;
}

const GEDFilter: FC<GedFilterProps> = ({
  setActiveSousCategorie,
  setSearchVariables,
  searchVariables,
  categories,
  factureLaboratoires,
  facturePartenaires,
  onRequestRefresh,
  onGoBack,
  espaceType,
  contratFournisseurs,
  setOpenDrawer,
  maxTTC,
}) => {
  const classes = style();
  const [expanded, setExpanded] = useState<any>({
    accesRapide: true,
    pharmacie: false,
    partage: false,
    facture: true,
  });
  const user = getUser();

  const origines = useOrigines();
  const idOrigineLaboratoire = (origines.data?.origines.nodes || []).find(
    ({ code }) => code === 'LABORATOIRE',
  )?.id;
  const idOriginePrestataireService = (origines.data?.origines.nodes || []).find(
    ({ code }) => code === 'PRESTATAIRE_SERVICE',
  )?.id;
  // const idOrigineInterne = (origines.data?.origines.nodes || []).find(
  //   ({ code }) => code === 'INTERNE',
  // )?.id;
  const enableValidationPrestataire = useValueParameterAsBoolean('0850');
  const idPrestataireValidateur = useValueParameterAsString('0851');

  console.log('parametre', enableValidationPrestataire, idPrestataireValidateur);

  const [currentSousCategorie, setCurrentSousCategorie] = useState<any | undefined>();
  const [textToSearch, setTextToSearch] = useState<string>('');
  const [partagerSousCategorie, setPartagerSousCategorie] = useState<any[]>([]);
  const [pharmacieSousCategorie, setPharmacieSousCategorie] = useState<any[]>([]);
  // const [valueFilterTime, setValueFilterTime] = useState<string | undefined>();
  const [valueFilterStatut, setValueFilterStatut] = useState<string | undefined>();

  useEffect(() => {
    if (currentSousCategorie) {
      setActiveSousCategorie(currentSousCategorie);
    }
  }, [currentSousCategorie]);

  useEffect(() => {
    setValueFilterStatut(searchVariables.accesRapide || 'TOUT');
  }, [searchVariables]);

  useEffect(() => {
    const _partageSousCategorie = [];
    const _pharmacieSousCategorie = [];
    (categories.data || []).map(categorie => {
      const sousCategoriePartage = [];
      const sousCategoriePharmacie = [];
      for (let sousCategorie of categorie.mesSousCategories || []) {
        if (sousCategorie.idPartenaireValidateur) {
          sousCategoriePartage.push(sousCategorie as never);
        } else {
          sousCategoriePharmacie.push(sousCategorie as never);
        }
      }
      if (sousCategoriePartage.length > 0)
        _partageSousCategorie.push({
          ...categorie,
          mesSousCategories: sousCategoriePartage,
        } as never);

      if (sousCategoriePharmacie.length > 0)
        _pharmacieSousCategorie.push({
          ...categorie,
          mesSousCategories: sousCategoriePharmacie,
        } as never);
    });
    setPartagerSousCategorie(_partageSousCategorie);
    setPharmacieSousCategorie(_pharmacieSousCategorie);
  }, [categories.data]);

  const handleExpandAccordion = (id: string) => (_: React.ChangeEvent<{}>, expanded: boolean) => {
    setExpanded(prevState => ({ ...prevState, [id]: expanded }));
  };

  const handleSousCategorieClick = (sousCategorie: any): void => {
    setCurrentSousCategorie(sousCategorie);
    handleChangeSearchVariables({
      accesRapide: undefined,
      idSousCategorie: sousCategorie?.id as any,
      type: 'AUTRES',
    });
  };

  const quickAccess = (type: string): void => {
    handleChangeSearchVariables({
      accesRapide: type === 'TOUT' ? undefined : (type as any),
      type: 'espace-facture' === espaceType ? 'FACTURE' : 'AUTRES',
    });
  };

  const handleTextToSearchChange = (value: string): void => {
    setTextToSearch(value);
    setSearchVariables(prev => ({
      ...prev,
      idSousCategorie: undefined,
      accesRapide: undefined,
      libelleSousEspace: undefined,
      searchText: value,
    }));
    setOpenDrawer && setOpenDrawer(false);
  };

  const handleChangeSearchVariables = (newValues: any): void => {
    setSearchVariables(prev => ({
      ...prev,
      idSousCategorie: undefined,
      filterBy: 'TOUT',
      sortBy: undefined,
      type: undefined,
      validation: 'TOUT',
      idOrigine: undefined,
      idOrigineAssocie: undefined,
      dateDebut: undefined,
      dateFin: undefined,
      libelleSousEspace: undefined,
      accesRapide: undefined,
      montantMinimal: undefined,
      montantMaximal: undefined,
      searchText: undefined,
      ...newValues,
    }));
    setOpenDrawer && setOpenDrawer(false);
  };

  const handleChangeFilterMontant = (_event: any, newValue: any) => {
    if (newValue?.length === 2) {
      setSearchVariables(prev => ({
        ...prev,
        montantMinimal: newValue[0],
        montantMaximal: newValue[1],
      }));
    }
  };

  const handleChangeFilterDate = (name: string, value: any) => {
    // if (
    //   (name === 'dateDebut' &&
    //     searchVariables.dateFin &&
    //     moment(searchVariables.dateFin).isBefore(value)) ||
    //   (name === 'dateFin' &&
    //     searchVariables.dateDebut &&
    //     moment(value).isBefore(searchVariables.dateDebut))
    // ) {
    //   handleChangeSearchVariables({
    //     type: 'FACTURE',
    //     montantMinimal: searchVariables.montantMinimal,
    //     montantMaximal: searchVariables.montantMaximal,
    //   });
    // } else {
    //   handleChangeSearchVariables({
    //     type: 'FACTURE',
    //     montantMinimal: searchVariables.montantMinimal,
    //     montantMaximal: searchVariables.montantMaximal,
    //     dateDebut: name === 'dateDebut' ? moment(value).toISOString() : searchVariables.dateDebut,
    //     dateFin: name === 'dateDebut' ? searchVariables.dateFin : moment(value).toISOString(),
    //   });
    // }
    setSearchVariables(prev => ({
      ...prev,
      dateDebut: name === 'dateDebut' ? moment(value).toISOString() : searchVariables.dateDebut,
      dateFin: name === 'dateDebut' ? searchVariables.dateFin : moment(value).toISOString(),
    }));
  };

  /*  const handleChangeFilterType = (value: any) => {
      setValueFilterByDoc(value);
      setSearchVariables(prev => ({
        ...prev,
        proprietaire: value.code,
      }));
    };
  
  */

  // const handleChangeFilterTime = (value: any) => {
  //   setValueFilterTime(value);
  //   setSearchVariables(prev => ({
  //     ...prev,
  //     filterBy: value.code,
  //   }));
  // };

  const withPrestataireServiceValidateur = enableValidationPrestataire && idPrestataireValidateur;
  const isPrestataire = !!user?.userPartenaire?.id;
  return (
    <Box>
      {!categories.loading &&
        (!categories?.error && categories?.data ? (
          <>
            {!isMobile() ? (
              <>
                <Box className={classes.heading}>
                  <IconButton size="small" onClick={onGoBack}>
                    <ArrowBackIcon />
                  </IconButton>
                  <Typography variant="h2" color="secondary">
                    {espaceType === 'espace-facture' ? 'Espace facturation' : 'Espace documentaire'}
                  </Typography>
                  <IconButton size="small" onClick={onRequestRefresh}>
                    <ReplayIcon />
                  </IconButton>
                </Box>
                <Box marginBottom={3} marginTop={3}>
                  <DebouncedSearchInput
                    wait={500}
                    outlined
                    onChange={handleTextToSearchChange}
                    value={textToSearch}
                  />
                  {'espace-facture' === espaceType && (
                    <>
                      <Box className={classes.filterSubtitle}>Par période</Box>
                      <Box className={classes.flex}>
                        <CustomDatePicker
                          label="De"
                          value={searchVariables.dateDebut}
                          InputLabelProps={{
                            shrink: true,
                          }}
                          onChange={value => {
                            // handleChangeSearchVariables({
                            //   type: 'FACTURE',
                            //   montantMinimal: searchVariables.montantMinimal,
                            //   montantMaximal: searchVariables.montantMaximal,
                            //   dateDebut: moment(value).toISOString(),
                            //   dateFin: searchVariables.dateFin,
                            // });
                            handleChangeFilterDate('dateDebut', value);
                          }}
                          fullWidth
                          className={classes.dateField}
                        />
                        <CustomDatePicker
                          label="À"
                          value={searchVariables.dateFin}
                          InputLabelProps={{
                            shrink: true,
                          }}
                          onChange={value => {
                            // handleChangeSearchVariables({
                            //   type: 'FACTURE',
                            //   montantMinimal: searchVariables.montantMinimal,
                            //   montantMaximal: searchVariables.montantMaximal,
                            //   dateDebut: searchVariables.dateDebut,
                            //   dateFin: moment(value).toISOString(),
                            // });
                            handleChangeFilterDate('dateFin', value);
                          }}
                          fullWidth
                          className={classes.dateField}
                        />
                      </Box>

                      <Typography className={classes.filterSubtitle} id="range-slider">
                        Par montant
                      </Typography>
                      <Box display="flex" justifyContent="space-between">
                        <Box>
                          min :{' '}
                          <span className={classes.filterSubtitle}>
                            {(searchVariables.montantMinimal || 0).toFixed(2)}&nbsp;€
                          </span>
                        </Box>
                        <Box>
                          max :{' '}
                          <span className={classes.filterSubtitle}>
                            {(searchVariables.montantMaximal || 0).toFixed(2)}&nbsp;€
                          </span>
                        </Box>
                      </Box>
                      <Slider
                        value={[
                          searchVariables.montantMinimal || 0,
                          searchVariables.montantMaximal || 0,
                        ]}
                        onChange={handleChangeFilterMontant}
                        valueLabelDisplay="auto"
                        aria-labelledby="range-slider"
                        min={0}
                        max={maxTTC}
                      />
                      <Box className={classes.rangeBox}>
                        <Box className={classes.textRange}>0.00 €</Box>
                        <Box className={classes.textRange}>{(maxTTC || 0).toFixed(2)} €</Box>
                      </Box>
                    </>
                  )}
                  {/* <TextField
                    variant="outlined"
                    placeholder="Rechercher..."
                    InputProps={{
                      endAdornment: <Search />,
                    }}
                    type="search"
                    fullWidth={true}
                    value={textToSearch}
                    onChange={handleTextToSearchChange}
                  />*/}
                </Box>
              </>
            ) : (
              <>
                <MobileTopBar
                  title={
                    espaceType === 'espace-facture' ? 'Espace facturation' : 'Espace documentaire'
                  }
                  withBackBtn={true}
                  // handleDrawerToggle={handleDrawerToggle}
                  // optionBtn={optionBtn}
                  withTopMargin={true}
                  breakpoint="sm"
                  // backUrl={drawerBackUrl}
                  // closeUrl={drawerCloseUrl}
                  onClickBack={onGoBack}
                />
                <FilterRadio
                  //  onChangeFilterBy={handleChangeFilterType}
                  //  onChangeFilterTime={handleChangeFilterTime}
                  onChangeFilterStatut={quickAccess}
                  // valueFilterBy={valueFilterByDoc}
                  // valueFilterTime={valueFilterTime}
                  valueFilterSatut={valueFilterStatut}
                  espaceType={espaceType}
                />
              </>
            )}
            {'espace-facture' !== espaceType && (
              <TreeStructure
                summary="Accès rapide"
                expanded={expanded.accesRapide === true}
                onChange={handleExpandAccordion('accesRapide')}
                id="acces-rapide"
              >
                <MenuItem
                  style={{ marginBottom: 8 }}
                  className={classNames(
                    {
                      [classes.active]: searchVariables.accesRapide === 'FAVORIS',
                    },
                    classes.fontMenu,
                  )}
                  onClick={() => quickAccess('FAVORIS')}
                >
                  Document Favoris
                </MenuItem>
                <MenuItem
                  style={{ marginBottom: 8 }}
                  className={classNames(
                    {
                      [classes.active]: searchVariables.accesRapide === 'NOUVEAUX',
                    },
                    classes.fontMenu,
                  )}
                  onClick={() => quickAccess('NOUVEAUX')}
                >
                  Nouveaux document
                </MenuItem>
                <MenuItem
                  style={{ marginBottom: 8 }}
                  className={classNames(
                    {
                      [classes.active]: searchVariables.accesRapide === 'RECENTS',
                    },
                    classes.fontMenu,
                  )}
                  onClick={() => quickAccess('RECENTS')}
                >
                  Document récents
                </MenuItem>
                <MenuItem
                  style={{ marginBottom: 8 }}
                  className={classNames(
                    {
                      [classes.active]: searchVariables.accesRapide === 'PLUS_CONSULTES',
                    },
                    classes.fontMenu,
                  )}
                  onClick={() => quickAccess('PLUS_CONSULTES')}
                >
                  Document les plus consultés
                </MenuItem>
                <MenuItem
                  className={classNames(
                    {
                      [classes.active]: searchVariables.accesRapide === 'PLUS_TELECHARGES',
                    },
                    classes.fontMenu,
                  )}
                  onClick={() => quickAccess('PLUS_TELECHARGES')}
                >
                  Document les plus téléchargés
                </MenuItem>
              </TreeStructure>
            )}

            {'espace-facture' === espaceType && (
              <TreeStructure
                summary={isPrestataire ? 'Espace des factures partenaires' : 'Espace facturation'}
                expanded={expanded.facture === true}
                onChange={handleExpandAccordion('facture')}
                id="espace-facture"
              >
                <TreeView>
                  <MenuItem
                    className={classNames(
                      {
                        [classes.active]:
                          searchVariables.type === 'FACTURE' && !searchVariables.accesRapide,
                      },
                      classes.fontMenu,
                    )}
                    onClick={() => quickAccess('TOUT')}
                  >
                    Toutes les factures
                  </MenuItem>

                  <MenuItem
                    className={classNames(
                      {
                        [classes.active]:
                          searchVariables.type === 'FACTURE' &&
                          searchVariables.accesRapide === 'EN_ATTENTE_APPROBATION',
                      },
                      classes.fontMenu,
                    )}
                    onClick={() =>
                      handleChangeSearchVariables({
                        type: 'FACTURE',
                        accesRapide: 'EN_ATTENTE_APPROBATION',
                        libelleSousEspace: 'Nouvelle Facture',
                      })
                    }
                  >
                    Nouvelles Factures
                  </MenuItem>

                  {!isPrestataire && (
                    <MenuItem
                      className={classNames(
                        {
                          [classes.active]:
                            searchVariables.type === 'FACTURE' &&
                            searchVariables.accesRapide === 'APPROUVES' &&
                            searchVariables.validation === 'INTERNE',
                        },
                        classes.fontMenu,
                      )}
                      onClick={() =>
                        handleChangeSearchVariables({
                          accesRapide: 'APPROUVES',
                          type: 'FACTURE',
                          validation: 'INTERNE',
                          libelleSousEspace: 'Facture validée',
                        })
                      }
                    >
                      <span>Facture validée</span>
                    </MenuItem>
                  )}

                  {!isPrestataire && (
                    <MenuItem
                      className={classNames(
                        {
                          [classes.active]:
                            searchVariables.type === 'FACTURE' &&
                            searchVariables.accesRapide === 'REFUSES' &&
                            (!withPrestataireServiceValidateur ||
                              searchVariables.validation === 'INTERNE'),
                        },
                        classes.fontMenu,
                      )}
                      onClick={() =>
                        handleChangeSearchVariables({
                          validation: withPrestataireServiceValidateur ? 'INTERNE' : undefined,
                          type: 'FACTURE',
                          accesRapide: 'REFUSES',
                          libelleSousEspace: withPrestataireServiceValidateur
                            ? 'Rejetée pharmacie'
                            : 'Facture rejetée',
                        })
                      }
                    >
                      <span>
                        {withPrestataireServiceValidateur ? 'Rejetée pharmacie' : 'Facture rejetée'}
                      </span>
                    </MenuItem>
                    // <TreeItem
                    //   className={classes.treeItem}
                    //   label={
                    //     <span className={classes.fontMenu}>
                    //       <span>
                    //         {withPrestataireServiceValidateur
                    //           ? 'Rejetées pharmacie'
                    //           : 'Factures rejetées'}
                    //       </span>{' '}
                    //     </span>
                    //   }
                    //   nodeId="factures-rejetees-interne"
                    //   id="factures-rejetees-interne"
                    //   classes={{
                    //     label: classes.treeItemSelected,
                    //     iconContainer: classes.treeItemIconContainer,
                    //   }}
                    // >
                    //   {facturesRejeteesInterneCategories.map(({ mois, annee }) => {
                    //     const factureMonth = moment
                    //       .utc()
                    //       .set('year', annee)
                    //       .set('month', mois - 1);

                    //     const dateDebut = factureMonth
                    //       .clone()
                    //       .startOf('month')
                    //       .toISOString();
                    //     const dateFin = factureMonth
                    //       .clone()
                    //       .endOf('month')
                    //       .endOf('day')
                    //       .toISOString();
                    //     const nom = moment(dateDebut).format('YYYY-MM');

                    //     return (
                    //       <MenuItem
                    //         style={{ marginTop: 16 }}
                    //         className={classNames(
                    //           {
                    //             [classes.active]:
                    //               searchVariables.type === 'FACTURE' &&
                    //               searchVariables.accesRapide === 'REFUSES' &&
                    //               (!withPrestataireServiceValidateur ||
                    //                 searchVariables.validation === 'INTERNE') &&
                    //               searchVariables.dateDebut === dateDebut &&
                    //               searchVariables.dateFin === dateFin,
                    //           },
                    //           classes.fontMenu,
                    //         )}
                    //         onClick={() =>
                    //           handleChangeSearchVariables({
                    //             dateDebut,
                    //             dateFin,
                    //             validation: withPrestataireServiceValidateur
                    //               ? 'INTERNE'
                    //               : undefined,
                    //             type: 'FACTURE',
                    //             accesRapide: 'REFUSES',
                    //             libelleSousEspace: withPrestataireServiceValidateur
                    //               ? 'Rejetées pharmacie'
                    //               : 'Factures rejetées',
                    //           })
                    //         }
                    //         key={nom}
                    //       >
                    //         <span>{nom}</span>
                    //       </MenuItem>
                    //     );
                    //   })}
                    // </TreeItem>
                  )}

                  {withPrestataireServiceValidateur && (
                    <MenuItem
                      className={classNames(
                        {
                          [classes.active]:
                            searchVariables.type === 'FACTURE' &&
                            searchVariables.accesRapide === 'APPROUVES' &&
                            searchVariables.validation !== 'INTERNE',
                        },
                        classes.fontMenu,
                      )}
                      onClick={() =>
                        handleChangeSearchVariables({
                          type: 'FACTURE',
                          accesRapide: 'APPROUVES',
                          libelleSousEspace: 'Facture comptabilisée',
                        })
                      }
                    >
                      Factures comptabilisées
                    </MenuItem>
                  )}

                  {withPrestataireServiceValidateur && (
                    <MenuItem
                      className={classNames(
                        {
                          [classes.active]:
                            searchVariables.type === 'FACTURE' &&
                            searchVariables.accesRapide === 'REFUSES' &&
                            searchVariables.validation === 'PRESTATAIRE_SERVICE',
                        },
                        classes.fontMenu,
                      )}
                      onClick={() =>
                        handleChangeSearchVariables({
                          validation: 'PRESTATAIRE_SERVICE',
                          type: 'FACTURE',
                          accesRapide: 'REFUSES',
                          libelleSousEspace: isPrestataire
                            ? `Facture rejetée`
                            : `Rejetée comptable`,
                        })
                      }
                    >
                      <span>{isPrestataire ? `Facture rejetée` : `Rejetée comptable`}</span>
                    </MenuItem>
                    // <TreeItem
                    //   className={classes.treeItem}
                    //   label={
                    //     <span className={classes.fontMenu}>
                    //       <span>{isPrestataire ? `Factures rejetées` : `Rejetées comptable`}</span>
                    //     </span>
                    //   }
                    //   nodeId="factures-rejetees-comptable"
                    //   id="factures-rejetees-comptable"
                    //   classes={{
                    //     label: classes.treeItemSelected,
                    //     iconContainer: classes.treeItemIconContainer,
                    //   }}
                    // >
                    //   {facturesRejeteesPrestataireServiceCategories.map(({ mois, annee }) => {
                    //     const factureMonth = moment
                    //       .utc()
                    //       .set('year', annee)
                    //       .set('month', mois - 1);

                    //     const dateDebut = factureMonth
                    //       .clone()
                    //       .startOf('month')
                    //       .toISOString();
                    //     const dateFin = factureMonth
                    //       .clone()
                    //       .endOf('month')
                    //       .endOf('day')
                    //       .toISOString();
                    //     const nom = moment(dateDebut).format('YYYY-MM');

                    //     return (
                    //       <MenuItem
                    //         style={{ marginTop: 16 }}
                    //         className={classNames(
                    //           {
                    //             [classes.active]:
                    //               searchVariables.type === 'FACTURE' &&
                    //               searchVariables.accesRapide === 'REFUSES' &&
                    //               searchVariables.validation === 'PRESTATAIRE_SERVICE' &&
                    //               searchVariables.dateDebut === dateDebut &&
                    //               searchVariables.dateFin === dateFin,
                    //           },
                    //           classes.fontMenu,
                    //         )}
                    //         onClick={() =>
                    //           handleChangeSearchVariables({
                    //             dateDebut,
                    //             dateFin,
                    //             validation: 'PRESTATAIRE_SERVICE',
                    //             type: 'FACTURE',
                    //             accesRapide: 'REFUSES',
                    //             libelleSousEspace: isPrestataire
                    //               ? `Factures rejetées`
                    //               : `Rejetées comptable`,
                    //           })
                    //         }
                    //         key={nom}
                    //       >
                    //         <span>{nom}</span>
                    //       </MenuItem>
                    //     );
                    //   })}
                    // </TreeItem>
                  )}
                  {withPrestataireServiceValidateur && (
                    <MenuItem
                      style={{ marginBottom: 8 }}
                      className={classNames(
                        {
                          [classes.active]:
                            searchVariables.type === 'FACTURE' &&
                            searchVariables.accesRapide === 'PAYES',
                        },
                        classes.fontMenu,
                      )}
                      onClick={() =>
                        handleChangeSearchVariables({
                          type: 'FACTURE',
                          accesRapide: 'PAYES',
                          libelleSousEspace: 'Facture payée',
                        })
                      }
                    >
                      Factures payées
                    </MenuItem>
                  )}
                </TreeView>
              </TreeStructure>
            )}

            {!isPrestataire && 'espace-facture' !== espaceType && (
              <TreeStructure
                summary="Espace pharmacie"
                expanded={expanded.pharmacie === true}
                onChange={handleExpandAccordion('pharmacie')}
                id="espace-pharmacie"
              >
                <TreeView>
                  {(pharmacieSousCategorie || []).map((categorie: any, index: number) => {
                    const reducer = (accumulator, currentValue) =>
                      accumulator + currentValue.nombreDocuments;
                    const nombreDocumentCategorie = (categorie?.mesSousCategories || []).reduce(
                      reducer,
                      0,
                    );
                    return (
                      <TreeItem
                        className={classes.treeItem}
                        key={categorie?.id || index}
                        label={
                          <span className={classes.fontMenu}>
                            <span>{categorie.libelle}</span>{' '}
                            <span style={{ fontWeight: 'bold' }}>{nombreDocumentCategorie}</span>
                          </span>
                        }
                        nodeId={categorie?.id}
                        id={categorie?.id}
                        classes={{ label: classes.treeItemSelected }}
                      >
                        {(categorie?.mesSousCategories || []).map(
                          (sousCategorie: any, indice: number) => (
                            <MenuItem
                              style={{ marginTop: 16 }}
                              className={classNames(
                                {
                                  [classes.active]: currentSousCategorie?.id === sousCategorie?.id,
                                },
                                classes.fontMenu,
                              )}
                              onClick={() => handleSousCategorieClick(sousCategorie)}
                              key={sousCategorie?.id || indice}
                            >
                              <span>{sousCategorie.libelle}</span>{' '}
                              <span style={{ fontWeight: 'bold' }}>
                                {sousCategorie.nombreDocuments}
                              </span>
                            </MenuItem>
                          ),
                        )}
                      </TreeItem>
                    );
                  })}
                  <TreeItem
                    className={classes.treeItem}
                    label={
                      <span className={classes.fontMenu}>
                        <span>Contrat</span>
                      </span>
                    }
                    nodeId="espace-contrat"
                    id="espace-contrat"
                    classes={{ label: classes.treeItemSelected }}
                  >
                    {(contratFournisseurs || []).map((partenaire: any, indice: number) => (
                      <MenuItem
                        style={{ marginTop: 16 }}
                        className={classNames(
                          {
                            [classes.active]:
                              searchVariables.type === 'CONTRAT' &&
                              searchVariables.idOrigineAssocie === partenaire.id,
                          },
                          classes.fontMenu,
                        )}
                        onClick={() =>
                          handleChangeSearchVariables({
                            type: 'CONTRAT',
                            idOrigineAssocie: partenaire.id,
                            validation: 'TOUT',
                            idOrigine:
                              partenaire.dataType === 'Laboratoire'
                                ? idOrigineLaboratoire
                                : idOriginePrestataireService,
                            libelleSousEspace: 'Contrat',
                          })
                        }
                        key={partenaire?.id || indice}
                      >
                        <span>{partenaire.nom}</span>
                      </MenuItem>
                    ))}
                  </TreeItem>
                </TreeView>
              </TreeStructure>
            )}

            {'espace-facture' === espaceType && !isPrestataire && (
              <TreeStructure
                summary="Factures fournisseurs"
                expanded={expanded.facturation === true}
                onChange={handleExpandAccordion('facturation')}
                id="espace-facturation"
              >
                <TreeView>
                  <TreeItem
                    className={classes.treeItem}
                    label={
                      <span className={classes.fontMenu}>
                        <span>Laboratoire</span>
                      </span>
                    }
                    nodeId="espace-laboratoire"
                    id="espace-laboratoire"
                    classes={{ label: classes.treeItemSelected }}
                  >
                    <MenuItem
                      style={{ marginTop: 16 }}
                      className={classNames(
                        {
                          [classes.active]:
                            searchVariables.type === 'FACTURE' &&
                            searchVariables.idOrigine === idOrigineLaboratoire &&
                            !searchVariables.idOrigineAssocie,
                        },
                        classes.fontMenu,
                      )}
                      onClick={() =>
                        handleChangeSearchVariables({
                          type: 'FACTURE',
                          validation: 'TOUT',
                          idOrigine: idOrigineLaboratoire,
                          libelleSousEspace: 'Laboratoire',
                        })
                      }
                    >
                      <span>Tous les laboratoires</span>
                    </MenuItem>
                    {(factureLaboratoires || []).map((partenaire: any, indice: number) => (
                      <MenuItem
                        style={{ marginTop: 16 }}
                        className={classNames(
                          {
                            [classes.active]:
                              searchVariables.type === 'FACTURE' &&
                              searchVariables.idOrigineAssocie === partenaire.id,
                          },
                          classes.fontMenu,
                        )}
                        onClick={() =>
                          handleChangeSearchVariables({
                            type: 'FACTURE',
                            idOrigineAssocie: partenaire.id,
                            validation: 'TOUT',
                            idOrigine: idOrigineLaboratoire,
                            libelleSousEspace: 'Laboratoire',
                          })
                        }
                        key={partenaire?.id || indice}
                      >
                        <span>{partenaire.nom}</span>
                      </MenuItem>
                    ))}
                  </TreeItem>
                  <TreeItem
                    className={classes.treeItem}
                    label={
                      <span className={classes.fontMenu}>
                        <span>Partenaire de service</span>
                      </span>
                    }
                    nodeId="espace-partenaire"
                    id="espace-partenaire"
                    classes={{ label: classes.treeItemSelected }}
                  >
                    <MenuItem
                      style={{ marginTop: 16 }}
                      className={classNames(
                        {
                          [classes.active]:
                            searchVariables.type === 'FACTURE' &&
                            searchVariables.idOrigine === idOriginePrestataireService &&
                            !searchVariables.idOrigineAssocie,
                        },
                        classes.fontMenu,
                      )}
                      onClick={() =>
                        handleChangeSearchVariables({
                          type: 'FACTURE',
                          validation: 'TOUT',
                          idOrigine: idOriginePrestataireService,
                          libelleSousEspace: 'Partenaire de service',
                        })
                      }
                    >
                      <span>Tous</span>
                    </MenuItem>
                    {(facturePartenaires || []).map((partenaire: any, indice: number) => (
                      <MenuItem
                        style={{ marginTop: 16 }}
                        className={classNames(
                          {
                            [classes.active]:
                              searchVariables.type === 'FACTURE' &&
                              searchVariables.idOrigineAssocie === partenaire.id,
                          },
                          classes.fontMenu,
                        )}
                        onClick={() =>
                          handleChangeSearchVariables({
                            type: 'FACTURE',
                            idOrigineAssocie: partenaire.id,
                            validation: 'TOUT',
                            idOrigine: idOriginePrestataireService,
                            libelleSousEspace: 'Partenaire de service',
                          })
                        }
                        key={partenaire?.id || indice}
                      >
                        <span>{partenaire.nom}</span>
                      </MenuItem>
                    ))}
                  </TreeItem>
                  <MenuItem
                    style={{ marginLeft: 18 }}
                    className={classNames(
                      {
                        [classes.active]:
                          searchVariables.type === 'FACTURE' &&
                          searchVariables.libelleSousEspace === 'Autre Tier',
                      },
                      classes.fontMenu,
                    )}
                    onClick={() =>
                      handleChangeSearchVariables({
                        type: 'FACTURE',
                        validation: 'TOUT',
                        idOrigine: 'TIERS',
                        libelleSousEspace: 'Autre Tier',
                      })
                    }
                  >
                    <span>Autres tiers</span>
                  </MenuItem>
                </TreeView>
              </TreeStructure>
            )}

            {'espace-facture' !== espaceType && (
              <TreeStructure
                summary="Espace partagé"
                expanded={expanded.partage === true}
                onChange={handleExpandAccordion('partage')}
                id="espace-partage"
              >
                <TreeView>
                  {(partagerSousCategorie || []).map((categorie: any, index: number) => {
                    const reducer = (accumulator, currentValue) =>
                      accumulator + currentValue.nombreDocuments;
                    const nombreDocumentCategorie = (categorie?.mesSousCategories || []).reduce(
                      reducer,
                      0,
                    );
                    return (
                      <TreeItem
                        className={classes.treeItem}
                        key={categorie?.id || index}
                        label={
                          <span className={classes.fontMenu}>
                            <span>{categorie.libelle}</span>{' '}
                            <span style={{ fontWeight: 'bold' }}>{nombreDocumentCategorie}</span>
                          </span>
                        }
                        nodeId={categorie?.id}
                        id={categorie?.id}
                        classes={{
                          label: classes.treeItemSelected,
                          iconContainer: classes.treeItemIconContainer,
                        }}
                      >
                        {(categorie?.mesSousCategories || []).map(
                          (sousCategorie: any, indice: number) => (
                            <MenuItem
                              style={{ marginTop: 16 }}
                              className={classNames(
                                {
                                  [classes.active]: currentSousCategorie?.id === sousCategorie?.id,
                                },
                                classes.fontMenu,
                              )}
                              onClick={() => handleSousCategorieClick(sousCategorie)}
                              key={sousCategorie?.id || indice}
                            >
                              <span>{sousCategorie.libelle}</span>{' '}
                              <span style={{ fontWeight: 'bold' }}>
                                {sousCategorie.nombreDocuments}
                              </span>
                            </MenuItem>
                          ),
                        )}
                      </TreeItem>
                    );
                  })}
                </TreeView>
              </TreeStructure>
            )}
          </>
        ) : (
          <ErrorPage />
        ))}
    </Box>
  );
};

export default GEDFilter;
