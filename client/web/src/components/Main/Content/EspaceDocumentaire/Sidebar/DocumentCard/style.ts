import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
  menuList: {
    color: theme.palette.grey[700],
    '& svg': {
      marginRight: theme.spacing(1),
      color: 'inherit',
    },
  },
  cardContent: {
    padding: 0,
  },
  separateur: {
    margin: '16px 0',
    borderBottom: `2px solid ${theme.palette.grey[200]}`,
  },
  card: {
    padding: '16px 0 ',
    marginBottom: 16,
    width: '100%',
    position: 'relative',
    cursor: 'pointer',
    '&:hover': {
      background: '#F8F8F8',
    },
  },
  active: {
    background: '#F8F8F8',
  },
  libelleContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  bold: {
    fontFamily: 'Roboto',
    fontWeight: 'bolder',
    fontSize: 16,
  },
  light: {
    fontFamily: 'Roboto',
    fontWeight: 'normal',
    fontSize: 14,
  },
  padding: {
    padding: '0 16px',
  },
  details: {
    display: 'flex',
    flexWrap: 'wrap',
    '&>div': {
      flex: '0 0 50%',
    },
  },
  status: {
    display: 'flex',
    marginBottom: -8,
    justifyContent: 'space-between',
    '& [class*=MuiChip-root]': {
      justifyContent: 'flex-start',
      background: 'none',
      '& svg': {
        marginLeft: 0,
        flexWrap: 'wrap',
      },
    },
  },
}));
