import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    menuList: {
      color: theme.palette.grey[700],
      '& svg': {
        marginRight: theme.spacing(1),
        color: 'inherit',
      },
    },
    resilierBox: {
        display: "flex",
        alignItems: "center",
    },
    textResilier: {
      color: '#212121',
      fontSize: 14,
      fontWeight: 500 as any,
      paddingLeft: 24,
    },
  }),
);

export default useStyles;
