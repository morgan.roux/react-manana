import { CustomButton, ErrorPage, Loader, DebouncedSearchInput } from '@app/ui-kit';
import { Box, Fab, IconButton, Menu, Tooltip } from '@material-ui/core';
import { Add, Sort, Email, CenterFocusWeak, CloudDownload } from '@material-ui/icons';
import React, { FC, useEffect, useState, Dispatch, SetStateAction } from 'react';
import NoItemContentImage from '../../../../Common/NoItemContentImage';
import { DocumentCategorie, IDocumentSearch } from '../EspaceDocumentaire';
import DocumentCard from './DocumentCard';
import SearchMenu from './SearchMenu/SearchMenu';
import style from './style';
import FilterAlt from '../../../../../assets/icons/todo/filter_alt.svg';
import CreateDocument from '../Content/CreateDocument';
import { isMobile } from '../../../../../utils/Helpers';
import classNames from 'classnames';
import { getUser } from '../../../../../services/LocalStorage';
import { AppAuthorization } from '../../../../../services/authorization';
import InfiniteScroll from 'react-infinite-scroll-component';
import { CustomModal } from '../../../../Common/CustomModal';
import { FormDocument } from './../Content/FormDocument';
import {
  GET_OCR_DOCUMENTS,
  GET_OCR_IS_CONFIGURE,
  GET_OCR_PHARMACIE_EMAIL_DATA,
} from '../../../../../federation/ged/document/query';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { useApolloClient, useLazyQuery, useQuery } from '@apollo/react-hooks';
import { OCR_IS_CONFIGURE } from '../../../../../federation/ged/document/types/OCR_IS_CONFIGURE';
import { OCR_PHARMACIE_EMAIL_DATA } from '../../../../../federation/ged/document/types/OCR_PHARMACIE_EMAIL_DATA';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import {
  OCR_DOCUMENTS,
  OCR_DOCUMENTSVariables,
} from '../../../../../federation/ged/document/types/OCR_DOCUMENTS';
import { MagnifierIcon } from './DocumentCard/MagnifierIcon/MagnifierIcon';
import {
  useValueParameterAsBoolean,
  useValueParameterAsString,
} from '../../../../../utils/getValueParameter';

export interface SideBarProps {
  documents: {
    loading: boolean;
    error: Error;
    data: DocumentCategorie[];
    total: number;
  };
  saving: boolean;
  saved: boolean;
  setSaved: (newValue: boolean) => void;
  activeSousCategorie: any | undefined;
  searchVariables: any;
  setActiveDocument: (document: DocumentCategorie | undefined) => void;
  onRequestAddToFavorite: (document: DocumentCategorie) => void;
  onRequestRemoveFromFavorite: (document: DocumentCategorie) => void;
  onRequestReplace: (document: DocumentCategorie) => void;
  onRequestDelete: (document: DocumentCategorie) => void;
  onRequestDetails: (document: DocumentCategorie) => void;
  onRequestScroll: () => void;
  onRequestGetComments: (document: DocumentCategorie) => void;
  onRequestDeplace: (document: DocumentCategorie) => void;
  setSearchVariables: Dispatch<SetStateAction<IDocumentSearch>>;
  onRequestCreateDocument: (document: any) => void;
  document?: DocumentCategorie;
  onRequestRefresh: () => void;

  espaceType?: string;
  tvas: {
    loading: boolean;
    error: Error;
    data: any[];
  };
  defaultCorrespondant?: any;
}

const Sidebar: FC<SideBarProps> = ({
  documents,
  activeSousCategorie,
  saving,
  saved,
  searchVariables,
  setSaved,
  setActiveDocument,
  onRequestCreateDocument,
  onRequestAddToFavorite,
  onRequestRemoveFromFavorite,
  onRequestReplace,
  onRequestDelete,
  onRequestDetails,
  onRequestScroll,
  onRequestGetComments,
  onRequestDeplace,
  setSearchVariables,
  onRequestRefresh,
  document,

  espaceType,
  tvas,
  defaultCorrespondant,
}) => {
  const classes = style();
  const client = useApolloClient();
  const user = getUser();
  const auth = new AppAuthorization(user);
  const [openFilter, setOpenFilter] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [textToSearch, setTextToSearch] = useState<string>('');
  const [isASC, setIsASC] = useState<boolean>(true);
  const [openModalCreateDoc, setOpenModalCreateDoc] = useState<boolean>(false);
  const [activeCard, setActiveCard] = useState<string | undefined>();
  const [openFormDocument, setOpenFormDocument] = useState<boolean>(false);
  const [savingFormDocument, setSavingFormDocument] = useState<boolean>(false);

  const smtpUser = useValueParameterAsString('0833');
  const smtpPassword = useValueParameterAsString('0834');
  const smtpHost = useValueParameterAsString('0835');
  const smtpPort = useValueParameterAsString('0836');

  const enableOCRisation = useValueParameterAsBoolean('0839');
  const enableSmtp = smtpUser && smtpPassword && smtpHost && smtpPort;

  useEffect(() => {
    if (saved) {
      setOpenModalCreateDoc(false);
      setSaved(false);
    }
  }, [saved]);

  useEffect(() => {
    if (document) {
      setActiveCard(document.id);
    }
  }, [document]);

  const [loadPharmacieEmail, loadingPharmacie] = useLazyQuery<OCR_PHARMACIE_EMAIL_DATA>(
    GET_OCR_PHARMACIE_EMAIL_DATA,
    {
      onCompleted: () => {
        displaySnackBar(client, {
          message: 'Récupération effectuée avec succès !',
          type: 'SUCCESS',
          isOpen: true,
        });
        onRequestRefresh();
        setSearchVariables(prev => ({
          ...prev,
          idSousCategorie: undefined,
          filterBy: 'TOUT',
          sortBy: undefined,
          type: 'FACTURE',
          validation: 'TOUT',
          idOrigine: undefined,
          idOrigineAssocie: undefined,
          dateDebut: undefined,
          dateFin: undefined,
          accesRapide: 'EN_ATTENTE_APPROBATION',
        }));
      },
      onError: () => {
        displaySnackBar(client, {
          message:
            'Des erreurs se sont survenues pendant la récupération des factures dans le boîte email !',
          type: 'ERROR',
          isOpen: true,
        });
      },
      client: FEDERATION_CLIENT,
      fetchPolicy: 'cache-and-network',
    },
  );

  const [loadOcrDocument, loadingOCRDocument] = useLazyQuery<OCR_DOCUMENTS, OCR_DOCUMENTSVariables>(
    GET_OCR_DOCUMENTS,
    {
      onCompleted: () => {
        displaySnackBar(client, {
          message: 'OCR des documents effectué avec succès !',
          type: 'SUCCESS',
          isOpen: true,
        });
        onRequestRefresh();
      },
      onError: () => {
        displaySnackBar(client, {
          message: "Des erreurs se sont survenues pendant l'OCR des documents !",
          type: 'ERROR',
          isOpen: true,
        });
      },
      client: FEDERATION_CLIENT,
      fetchPolicy: 'cache-and-network',
    },
  );

  const handleCloseFilter = () => {
    setAnchorEl(null);
    setOpenFilter(false);
  };
  const handleClickFilter = (event: React.MouseEvent) => {
    setAnchorEl(event.currentTarget as HTMLElement);
    setOpenFilter(prev => !prev);
  };

  const handleClick = (document: DocumentCategorie, currentActive?: string): void => {
    setActiveDocument(document);
    onRequestDetails(document);
    onRequestGetComments(document);
    setActiveCard(currentActive);
  };

  const handleReplace = (document: DocumentCategorie): void => {
    onRequestReplace(document);
  };

  const handleSort = (): void => {
    setIsASC(!isASC);
    setSearchVariables(prev => ({
      ...prev,
      sortBy: 'description',
      sortDirection: isASC ? 'ASC' : 'DESC',
    }));
  };

  const handleAddClick = () => {
    setOpenModalCreateDoc(true);
  };

  const handleAttachFile = () => {
    setOpenFormDocument(true);
  };

  const handleScan = () => {
    loadPharmacieEmail();
  };

  const handleOcR = () => {
    loadOcrDocument();
  };

  return (
    <Box>
      {!isMobile() && (
        <Box className={classes.header}>
          <CustomButton
            disabled={!auth.isAuthorizedToAddGedDocument()}
            onClick={handleAddClick}
            color="secondary"
            startIcon={<Add />}
          >
            {'espace-facture' === espaceType ? 'NOUVELLE FACTURE' : 'NOUVEAU DOCUMENT'}
          </CustomButton>
          <Box className={classes.acitionContainer}>
            {/*
            <Box className={classes.attach}>
              <IconButton onClick={handleAttachFile}>
                <AttachFile />
              </IconButton>
            </Box>
            */}
            <Box className={classes.acitionContainer}>
              {enableSmtp && (
                <Tooltip title="Récupérer les factures dans le boîte email">
                  <IconButton onClick={handleScan} disabled={loadingPharmacie.loading}>
                    <CloudDownload />
                  </IconButton>
                </Tooltip>
              )}

              {enableOCRisation && (
                <Tooltip title="OCR-iser tous les documents">
                  <IconButton onClick={handleOcR} disabled={loadingOCRDocument.loading}>
                    <MagnifierIcon />
                  </IconButton>
                </Tooltip>
              )}

              <IconButton onClick={handleSort}>
                <Sort />
              </IconButton>
              <IconButton onClick={handleClickFilter}>
                <img src={FilterAlt} alt="iconFilterAlt" />
              </IconButton>
            </Box>
          </Box>
        </Box>
      )}
      {isMobile() && (
        <Box className={classes.searchTextContainer}>
          <DebouncedSearchInput
            wait={100}
            outlined
            onChange={setTextToSearch}
            value={textToSearch}
          />
        </Box>
      )}
      <Box
        id="infinty-scroll"
        className={classNames(
          classes.documentContainer,
          classes[isMobile() ? 'heigthMobile' : 'heigthWeb'],
        )}
      >
        <InfiniteScroll
          style={{ padding: '0 4px' }}
          dataLength={documents.total}
          next={onRequestScroll}
          hasMore={(documents?.data?.length || 0) < documents.total}
          loader={<></>}
          scrollableTarget="infinty-scroll"
        >
          {documents.loading ? (
            <Loader />
          ) : (
            <>
              {documents?.error || !documents?.data ? (
                <ErrorPage />
              ) : documents?.data?.length > 0 ? (
                documents?.data?.map((document: DocumentCategorie, index: number) => (
                  <DocumentCard
                    saving={saving}
                    saved={saved}
                    setSaved={setSaved}
                    document={document}
                    activeSousCategorie={activeSousCategorie}
                    active={document.id === activeCard}
                    key={document?.id || index}
                    info={document}
                    onRequestAddToFavorite={onRequestAddToFavorite}
                    onRequestRemoveFromFavorite={onRequestRemoveFromFavorite}
                    onRequestDelete={onRequestDelete}
                    onRequestReplaceDocument={handleReplace}
                    onRequestDeplace={onRequestDeplace}
                    onClick={() => handleClick(document, document.id)}
                    searchVariables={searchVariables}
                    espaceType={espaceType}
                    tvas={tvas}
                    defaultCorrespondant={defaultCorrespondant}
                  />
                ))
              ) : (
                <NoItemContentImage
                  title="Affichage des documents"
                  subtitle="Aucun document à afficher. Créez-en un en cliquant sur le bouton adéquat."
                />
              )}
            </>
          )}
        </InfiniteScroll>
      </Box>

      <Menu
        open={openFilter}
        anchorEl={anchorEl}
        onClose={handleCloseFilter}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <SearchMenu searchVariables={searchVariables} setSearchVariables={setSearchVariables} />
      </Menu>

      <CreateDocument
        open={openModalCreateDoc}
        setOpen={setOpenModalCreateDoc}
        title={'espace-facture' === espaceType ? 'Nouvelle facture' : "Ajout d'un document"}
        saving={saving}
        activeSousCategorie={activeSousCategorie}
        mode="creation"
        onRequestCreateDocument={onRequestCreateDocument}
        document={document}
        searchVariables={searchVariables}
        defaultType={'espace-facture' === espaceType ? 'FACTURE' : undefined}
        tvas={tvas}
        defaultCorrespondant={defaultCorrespondant}
      />
      <CustomModal
        open={openFormDocument}
        setOpen={setOpenFormDocument}
        title={'espace-facture' === espaceType ? 'Nouvelles factures' : 'Ajout des documents'}
        closeIcon
        withBtnsActions
        headerWithBgColor
        fullWidth
        maxWidth="sm"
        fullScreen={!!isMobile()}
        disabledButton={savingFormDocument}
      >
        <FormDocument />
      </CustomModal>
      {isMobile() && (
        <Fab className={classes.fab} color="secondary" variant="round" onClick={handleAddClick}>
          <Add />
        </Fab>
      )}
    </Box>
  );
};

export default Sidebar;
