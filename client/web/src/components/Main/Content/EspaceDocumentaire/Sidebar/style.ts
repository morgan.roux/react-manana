import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
  sidebarContainer: {
    maxWidth: 400,
    height: '100%',
    flexShrink: 0,
    borderRight: `1px solid ${theme.palette.divider}`,
    overflowY: 'auto',
  },
  header: {
    margin: '16px 0',
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'space-between',
  },
  acitionContainer: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  documentContainer: {
    marginTop: 16,
    overflowY: 'scroll',
    scrollbarWidth: 'none',
    '&::-webkit-scrollbar': {
      height: 0,
      width: 0,
    },
  },
  heigthMobile: {
    height: 'calc(100vh - 316px)',
  },
  heigthWeb: {
    height: 'calc(100vh - 186px)',
  },
  searchTextContainer: {
    width: '100%',
    margin: '16px 0 24px 0',
  },
  fab: {
    position: 'absolute',
    right: 32,
    bottom: 90,
  },
  attach: {
    display: 'flex',
    marginRight: 3,
  },
}));
