import React, { FC } from 'react';
import useStyles from './styles';
import { RouteComponentProps, withRouter } from 'react-router';

export const List: FC<RouteComponentProps> = ({ history }) => {
  const classes = useStyles({});

  return <div className={classes.root}></div>;
};

export default withRouter(List);
