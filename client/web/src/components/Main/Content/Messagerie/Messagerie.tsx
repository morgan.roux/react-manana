import { useApolloClient, useLazyQuery, useQuery } from '@apollo/react-hooks';
import { debounce, groupBy, isEqual, values as _values } from 'lodash';
import moment from 'moment';
import React, { FC } from 'react';
import { push } from 'react-intl/locale-data/af';
import { RouteComponentProps, withRouter } from 'react-router';
import {
  MESSAGE_DELETED_URL,
  MESSAGE_NEW_URL,
  MESSAGE_SENDED_URL,
  MESSAGE_URL,
} from '../../../../Constant/url';
import { ME_me } from '../../../../graphql/Authentication/types/ME';
import { GET_MESSAGERIES } from '../../../../graphql/Messagerie';
import {
  GET_FILTER_COUNT_MESSAGERIES,
  GET_TOTAL_MESSAGERIES,
  GET_TOTAL_MESSAGERIE_NON_LUS,
} from '../../../../graphql/Messagerie/query';
import {
  FILTER_COUNT_MESSAGERIES,
  FILTER_COUNT_MESSAGERIESVariables,
} from '../../../../graphql/Messagerie/types/FILTER_COUNT_MESSAGERIES';
import {
  MESSAGERIES,
  MESSAGERIESVariables,
  MESSAGERIES_messageries_data,
} from '../../../../graphql/Messagerie/types/MESSAGERIES';
import {
  TOTAL_MESSAGERIES,
  TOTAL_MESSAGERIESVariables,
} from '../../../../graphql/Messagerie/types/TOTAL_MESSAGERIES';
import { TOTAL_MESSAGERIE_LUS } from '../../../../graphql/Messagerie/types/TOTAL_MESSAGERIE_LUS';
import { TOTAL_MESSAGERIE_NON_LUS } from '../../../../graphql/Messagerie/types/TOTAL_MESSAGERIE_NON_LUS';
import { getUser } from '../../../../services/LocalStorage';
import { MessagerieType } from '../../../../types/graphql-global-types';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop';
import MobileTopBar from '../../../Common/MobileTopBar';
import SideBar from './SideBar';
import useMessageForm from './SideBar/FormMessage/useMessageForm';
import useStyles from './styles';

export const defaultQueryVariables: MESSAGERIESVariables = {
  isRemoved: false,
  typeMessagerie: MessagerieType.R,
  orderBy: '',
  typeFilter: 'MY_PHARMACIE',
};

export const Messagerie: FC<RouteComponentProps> = ({
  history: { push },
  location: { pathname, state },
  match: { params },
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const {
    values,
    setValues,
    handleChange,
    handleChangeAutocomplete,
    handleChangeMessageContent,
  } = useMessageForm();

  const [selectedMsg, setSelectedMsg] = React.useState<any[]>([]);

  const currentUser: ME_me = getUser();
  const currentUserId = (currentUser && currentUser.id) || '';

  const [mobileOpen, setMobileOpen] = React.useState(false);

  const { messageId } = params as any;

  const isOnMailbox = pathname === MESSAGE_URL || pathname.includes('inbox');
  const isOnSended = pathname === MESSAGE_SENDED_URL || pathname.includes('sent');
  const isOnDeleted = pathname === MESSAGE_DELETED_URL || pathname.includes('trash');
  const isOnNewMsg = pathname === MESSAGE_NEW_URL;

  const [messages, setMessages] = React.useState<MESSAGERIES_messageries_data[]>([]);
  const [message, setMessage] = React.useState<MESSAGERIES_messageries_data | null>(null);
  const [groupedMessage, setGroupedMessage] = React.useState<any[]>([]);
  const [searchTxt, setSearchTxt] = React.useState('');
  const [currentCountMessage, setCurrentCountMessage] = React.useState<number>(0);
  const [nbNonLus, setNbNonLus] = React.useState<number>(0);
  const [nbMsg, setNbMsg] = React.useState<number>(0);

  const [queryVariables, setQueryVariables] = React.useState<MESSAGERIESVariables>(
    isOnMailbox || isOnNewMsg
      ? { ...defaultQueryVariables, typeMessagerie: MessagerieType.R }
      : isOnSended
      ? { ...defaultQueryVariables, typeMessagerie: MessagerieType.E }
      : isOnDeleted
      ? { isRemoved: true, typeMessagerie: null }
      : defaultQueryVariables,
  );

  const { data: msgData, refetch: refetchreadMsg } = useQuery<
    TOTAL_MESSAGERIES,
    TOTAL_MESSAGERIESVariables
  >(GET_TOTAL_MESSAGERIES, {
    fetchPolicy: 'cache-and-network',
    variables: { isRemoved: false, typeMessagerie: MessagerieType.R },
  });

  const { data: msgNonLus, refetch: refetchUnreadMsg } = useQuery<TOTAL_MESSAGERIE_NON_LUS>(
    GET_TOTAL_MESSAGERIE_NON_LUS,
    {
      fetchPolicy: 'cache-and-network',
    },
  );

  const [getFilterMessage, { data: filterCount }] = useLazyQuery<
    FILTER_COUNT_MESSAGERIES,
    FILTER_COUNT_MESSAGERIESVariables
  >(GET_FILTER_COUNT_MESSAGERIES, {
    fetchPolicy: 'cache-and-network',
  });

  const refetchAll = () => {
    if (refetchreadMsg) refetchreadMsg();
    if (refetchUnreadMsg) refetchUnreadMsg();
    getFilterMessage({ variables: queryVariables });
  };

  React.useEffect(() => {
    getFilterMessage({
      variables: {
        typeMessagerie: queryVariables.typeMessagerie,
      },
    });
    setSelectedMsg([]);
  }, [queryVariables.typeMessagerie]);

  const [getMessageries, { data, loading }] = useLazyQuery<MESSAGERIES, MESSAGERIESVariables>(
    GET_MESSAGERIES,
    {
      fetchPolicy: 'cache-and-network',
      onError: error => {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );

  React.useEffect(() => {
    if (msgData && msgData.messageries && msgData.messageries.total) {
      setNbMsg(msgData.messageries.total);
      console.log('REFETCH');
      // push(MESSAGE_URL, { lastPathname: pathname });
      getMessageries({ variables: queryVariables });

      getFilterMessage({
        variables: {
          typeMessagerie: queryVariables.typeMessagerie,
        },
      });

      setCurrentCountMessage(msgData.messageries.total);
    }
  }, [msgData]);

  React.useEffect(() => {
    if (msgNonLus && msgNonLus.messagerieNonLus) {
      setNbNonLus(msgNonLus.messagerieNonLus.total);
    }
  }, [msgNonLus]);

  /**
   * Set default message
   */
  React.useMemo(() => {
    if (messageId && pathname.includes(messageId) && messages.length > 0) {
      const msg = messages.find(m => m.id === messageId);
      if (!message || (message && message.id !== messageId)) {
        setMessage(msg as any);
      }
    }
  }, [messages, pathname, messageId]);

  /**
   * Execute query
   */
  React.useEffect(() => {
    if (isOnNewMsg) push(MESSAGE_URL);
    else getMessageries({ variables: queryVariables });
  }, []);

  React.useEffect(() => {
    getMessageries({ variables: queryVariables });
  }, [queryVariables, isOnMailbox, isOnSended, isOnDeleted]);

  /**
   * Set messages
   */
  React.useEffect(() => {
    if (data && data.messageries && data.messageries.data) {
      setMessages(data.messageries.data as any);
    }
  }, [data]);

  /**
   * Grouped message by date
   */
  React.useEffect(() => {
    if (messages.length > 0) {
      const dateCreation = (item: any) => moment(item.dateHeureMessagerie).format('YYYY');
      let newData: any = groupBy(messages, dateCreation);

      if (newData) {
        newData = _values(newData).map(i => ({ year: dateCreation(i[0]), messages: i }));
        setGroupedMessage(newData.reverse());
      }
    } else {
      setGroupedMessage([]);
    }
  }, [messages]);

  React.useEffect(() => {
    setValues(prevState => ({ ...prevState, typeMessagerie: MessagerieType.R }));
  }, []);

  const messageries = (data && data.messageries && data.messageries.data) || [];

  const onChangeSearchTxt = (event: React.ChangeEvent<any>) => {
    if (event.target) {
      setSearchTxt(event.target.value);
      debouncedSearch.current(event.target.value, messageries as any);
    }
  };

  const debouncedSearch = React.useRef(
    debounce((text: string, messageries: MESSAGERIES_messageries_data[]) => {
      if (text) {
        const newMessages = messageries.filter(m => {
          if (m) {
            const objet = m.objet;
            const content = m.message;
            const userEmetteur = m.userEmetteur?.userName;
            return (
              objet?.toLowerCase().includes(text.toLowerCase()) ||
              content?.toLowerCase().includes(text.toLowerCase()) ||
              userEmetteur?.toLowerCase().includes(text.toLowerCase()) ||
              userEmetteur?.toLowerCase().includes(text.toLowerCase())
            );
          }
        });
        setMessages(newMessages as any);
      } else {
        if (!isEqual(messages, messageries)) {
          setMessages(messageries as any);
        }
      }
    }, 1000),
  );

  const title = 'Ma messagerie';

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  return (
    <div className={classes.root}>
      {loading && <Backdrop />}
      <MobileTopBar
        title={title}
        withBackBtn={true}
        handleDrawerToggle={handleDrawerToggle}
        // optionBtn={
        //   <ActionButton
        //     actionMenu="HEADER_MENU"
        //     setTriage={setTriage}
        //     refetchSection={refetchSection}
        //     setCacheTacheAchevE={setCacheTacheAchevE}
        //     cacheTacheAchevE={cacheTacheAchevE}
        //     statutMainHeader={statutMainHeader}
        //     refetchCountTodos={refetchCountTodos}
        //     parameters={parameters}
        //     refetchAll={refetchAll}
        //   />
        // }
      />
      <SideBar
        handleDrawerToggle={handleDrawerToggle}
        messageList={groupedMessage as any}
        values={values}
        setValues={setValues}
        onChangeInput={handleChange}
        onChangeAutocomplete={handleChangeAutocomplete}
        handleChangeMessageContent={handleChangeMessageContent}
        queryVariables={queryVariables}
        setQueryVariables={setQueryVariables}
        searchValue={searchTxt}
        setSearchValue={setSearchTxt}
        onChangeSearchValue={onChangeSearchTxt}
        message={message}
        setMessage={setMessage}
        currentUserId={currentUserId}
        selectedMsg={selectedMsg}
        setSelectedMsg={setSelectedMsg}
        nbMsgNonLus={nbNonLus}
        nbMsg={nbMsg}
        refetchUnreadMsg={refetchUnreadMsg}
        filterCounts={
          filterCount &&
          filterCount.filterMessageriesCount &&
          filterCount.filterMessageriesCount.length
            ? filterCount.filterMessageriesCount
            : []
        }
        refetchAll={refetchAll}
        mobileOpen={mobileOpen}
      />
    </div>
  );
};

export default withRouter(Messagerie);
