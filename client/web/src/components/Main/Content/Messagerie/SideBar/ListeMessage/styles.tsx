import { createStyles, makeStyles } from '@material-ui/core/styles';
import { Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: '500px',
      width: '100%',
      borderRight: '1px solid #E0E0E0',
      height: 'calc(100vh - 184px)',
      [theme.breakpoints.only('md')]: {
        marginTop: 170,
      },
    },
    margin: {
      margin: theme.spacing(1),
    },
    paper: {
      marginRight: theme.spacing(2),
    },

    appBar: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: '0 6px 0 24px',
      height: 54,
      borderBottom: '1px solid #E0E0E0',
      '& .MuiIconButton-root': {
        padding: 0,
      },
      [theme.breakpoints.only('md')]: {
        position: 'fixed',
        width: 500,
      },
    },
    bold: {
      fontWeight: 500,
    },
    expendMore: {
      '& .MuiButton-label': {
        fontSize: '0.75rem',
      },
    },

  }),
);

export default useStyles;
