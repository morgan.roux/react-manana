import React from 'react';
import {
  MessagerieAttachmentInput,
  MessagerieType,
} from '../../../../../../types/graphql-global-types';

export interface MessageInterface {
  id: string | null;
  recepteurs: any[];
  recepteursIds: string[];
  objet: string;
  message: string;
  typeMessagerie: MessagerieType | null;
  theme: any;
  themeId: string | null;
  sourceId: string | null;
  attachments: MessagerieAttachmentInput[] | null;
  selectedFiles: File[];
}

export const initialState: MessageInterface = {
  id: null,
  recepteurs: [],
  recepteursIds: [],
  objet: '',
  message: '',
  typeMessagerie: null,
  theme: null,
  themeId: null,
  sourceId: null,
  attachments: null,
  selectedFiles: [],
};

const useMessageForm = (
  defaultState?: MessageInterface,
  callback?: (data: MessageInterface) => void,
) => {
  const initValues: MessageInterface = defaultState || initialState;
  const [values, setValues] = React.useState<MessageInterface>(initValues);

  const { recepteurs, recepteursIds, theme, themeId } = values;

  const handleChange = (e: React.ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      setValues(prevState => ({ ...prevState, [name]: value }));
    }
  };

  const handleChangeAutocomplete = (name: string) => (e: React.ChangeEvent<any>, value: any) => {
    setValues(prevState => ({ ...prevState, [name]: value }));
  };
  const handleChangeMessageContent = (content: string, name: string) => {
    setValues(prevState => ({ ...prevState, [name]: content }));
    console.log(values);
  };

  // Set recepteursIds
  React.useMemo(() => {
    if (recepteurs.length > 0) {
      const ids = recepteurs.map(i => i.id);
      setValues(prevState => ({ ...prevState, recepteursIds: ids }));
    } else {
      if (recepteursIds.length !== 0) {
        setValues(prevState => ({ ...prevState, recepteursIds: [] }));
      }
    }
  }, [recepteurs]);

  // Set themeId
  React.useMemo(() => {
    if (theme) {
      setValues(prevState => ({ ...prevState, themeId: theme.id }));
    } else {
      if (themeId !== null) {
        setValues(prevState => ({ ...prevState, themeId: null }));
      }
    }
  }, [theme]);

  return {
    handleChange,
    handleChangeAutocomplete,
    handleChangeMessageContent,
    values,
    setValues,
  };
};

export default useMessageForm;
