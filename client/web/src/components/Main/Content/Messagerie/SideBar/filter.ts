import { PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories } from '../../../../../graphql/Parametre/types/PARAMETERS_GROUPES_CATEGORIES';

export const hiddenFilter = (
  isUserPharmacie: boolean,
  codeFilter: string,
  parameters: PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories[],
) => {
  let codeGroupement = '';
  // let codePharmacie = '';

  switch (codeFilter) {
    case 'MY_PHARMACIE':
      codeGroupement = '0110';
      // codePharmacie = '0104';
      break;
    case 'MY_REGION':
      codeGroupement = '0111';
      // codePharmacie = '0105';
      break;
    case 'OTHER_PHARMACIE':
      codeGroupement = '0112';
      // codePharmacie = '0106';
      break;
    case 'MY_GROUPEMENT':
      codeGroupement = '0113';
      // codePharmacie = '0107';
      break;
    case 'LABORATOIRE':
      codeGroupement = '0114';
      // codePharmacie = '0108';
      break;
    case 'PARTENAIRE_SERVICE':
      codeGroupement = '0115';
      // codePharmacie = '0109';
      break;
  }

  /*if (isUserPharmacie) {
    return parameters.reduce((results, parameter) => {
      if (
        parameter &&
        parameter.code &&
        parameter.code === codeGroupement &&
        (parameter.value && parameter.value.id
          ? parameter.value.value === 'true'
          : parameter.defaultValue === 'true')
      )
        return [...results, parameter];

      if (
        parameter &&
        parameter.code &&
        parameter.code === codePharmacie &&
        (parameter.value && parameter.value.id
          ? parameter.value.value === 'true'
          : parameter.defaultValue === 'true')
      )
        return [...results, parameter];

      return results;
    }, [] as any[]).length > 1
      ? false
      : true;
  } else {*/
  return parameters.filter(
    (parameter) =>
      parameter &&
      parameter.code &&
      parameter.code === codeGroupement &&
      (parameter.value && parameter.value.id
        ? parameter.value.value === 'true'
        : parameter.defaultValue === 'true'),
  ).length
    ? false
    : true;
  // }
};
