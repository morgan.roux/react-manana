import { createStyles, makeStyles } from '@material-ui/core/styles';
import { Theme, darken } from '@material-ui/core';
const drawerWidth = 300;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    addButton: {
      background: '#8CC63F',
      color: theme.palette.common.white,
      textTransform: 'none',
      fontSize: 16,
      '&:hover': {
        background: darken('#8CC63F', 0.1),
      },
    },
    boiteReceptButton: {
      background: theme.palette.secondary.main,
      color: theme.palette.common.white,
      textTransform: 'none',
      fontSize: 16,
    },
    root: {
      display: 'flex',
      width: '100%',
    },
    rootList: {
      width: '100%',
      maxWidth: 300,
      '& .MuiListItem-root': {
        display: 'flex',
        justifyContent: 'space-between',
        '&:hover, &.active': {
          backgroundColor: theme.palette.secondary.main,
          color: theme.palette.common.white,
          borderRadius: 6,
          '& .MuiListItemIcon-root': {
            color: theme.palette.common.white,
          },
        },
      },
      '& .MuiListItemIcon-root': {
        minWidth: 36,
      },
    },
    rootListFilter: {
      width: '100%',
      maxWidth: 300,
      '& .MuiListItem-root': {
        display: 'flex',
        justifyContent: 'space-between',
        '&.active': {
          backgroundColor: theme.palette.success.main,
          color: theme.palette.common.white,
          borderRadius: 6,

          '& .MuiListItemIcon-root': {
            color: theme.palette.common.white,
          },
        },
        '&:hover': {
          backgroundColor: theme.palette.primary.main,
          color: theme.palette.common.white,
          borderRadius: 6,

          '& .MuiListItemIcon-root': {
            color: theme.palette.common.white,
          },
        },
      },
      '& .MuiListItemIcon-root': {
        minWidth: 36,
      },
    },
    rootDrawer: {
      height: 'calc(100vh - 86px)',
      overflow: 'auto',
      border: '1px solid rgba(0,0,0,0.25)',
      '& .MuiDivider-root': {
        margin: '22px 0 0',
      },
      [theme.breakpoints.down('lg')]: {
        height: 'calc(100vh - 70px)',
        width: '100%',
        border: 0,
      },
    },
    drawer: {
      [theme.breakpoints.down('md')]: {
        width: '100vw',
      },
    },
    appBar: {
      [theme.breakpoints.up('md')]: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
      },
    },
    menuButton: {
      marginRight: theme.spacing(2),
      background: '#e34168',
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
    // necessary for content to be below app bar
    toolbar: {
      padding: theme.spacing(3),
      backgroundColor: '#F5F6FA',
      [theme.breakpoints.only('md')]: {
        marginTop: 72,
        position: 'fixed',
        width: '100%',
      },
    },
    drawerPaper: {
      width: drawerWidth,
      top: 86,
      boxShadow: 'none!important',
      [theme.breakpoints.down('sm')]: {
        width: '100%',
      },
    },
    drawerWeb: {
      width: '100vw',
      height: '100%',
      overflow: 'auto',
      borderRight: '1px solid rgba(0, 0, 0, 0.12)',
      [theme.breakpoints.up('md')]: {
        width: drawerWidth,
        flexShrink: 0,
        /* marginTop: 86, */
      },
    },
    content: {
      flexGrow: 1,
    },
    contentSearch: {
      border: '1px solid #E0E0E0',
      borderRadius: '3px',
      maxHeight: '50px',
      maxWidth: '445px',
      width: '100%',
      backgroundColor: '#ffffff',
      '& .MuiInputBase-root': {
        width: '100%',
      },
    },
    buttonContent: {
      '& .MuiButton-label': {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        '& .MuiTypography-root': {
          fontSize: '0.75rem',
          textTransform: 'lowercase',
        },
      },
      '&.MuiButton-text': {
        padding: '0 8px',
      },
    },
    textList: {
      '& .MuiListItemText-primary': {
        fontSize: '0.875rem',
      },
    },
    nombreFiltre: {
      borderRadius: 13,
      border: '1px solid #616161',
      minWidth: 23,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      padding: '2px 6px',
      fontSize: '0.75rem',
      color: '#616161',
      '&.active': {
        border: '1px solid #ffffff',
        color: theme.palette.common.white,
      },
    },
    colorFilterCircle: {
      '& .MuiSvgIcon-root': {
        height: 14,
        width: 14,
      },
    },
    mobileListeFilter: {
      display: 'flex',
      width: '100%',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: '0px 8px',
      borderBottom: '1px solid #E1E1E1',
      height: 57,
      '& .MuiInput-underline:before': {
        borderBottom: 0,
      },
      '& .MuiSelect-select': {
        display: 'flex',
        alignItems: 'center',
        '& svg': {
          marginRight: 8,
        },
      },
    },
    listeMenuItem: {
      '& svg': {
        marginRight: 8,
      },
    },
    msgNbr: {
      display: 'flex',
      padding: '0px 16px',
      '& .MuiTypography-root': {
        fontFamily: 'Roboto',
        fontSize: 12,
        color: '#9E9E9E',
        marginRight: 16,
      },
    },
    createMessageButton: {
      position: 'absolute',
      bottom: 30,
      right: 16,
      background: theme.palette.secondary.main,
      borderRadius: '50%',
      '& .MuiIconButton-root': {
        color: theme.palette.common.white,
      },
    },
    listMessageContent: {
      display: 'flex',
      [theme.breakpoints.only('md')]: {
        marginTop: 170,
      },
    },
    mobileTopBar: {
      width: '100%',
    },
    fab: {
      position: 'absolute',
      right: 32,
      bottom: 75,
      zIndex:5000
    }
  }),
);

export default useStyles;
