import { createStyles, makeStyles } from '@material-ui/core/styles';
import { Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      overflow: 'auto',
      height: 'calc(100vh - 310px)',
      '& .MuiPaper-root': {
        boxShadow: 'none!important',
      },
      '& .MuiAccordion-root': {
        borderBottom: '1px solid rgba(0, 0, 0, .125)!important',
        border: '0!important',
      },

      '& .MuiAccordionDetails-root': {
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        '& .MuiList-padding': {
          paddingTop: 0,
          paddingBottom: 0,
          borderTop: '1px solid #E0E0E0',
        },
      },
      '& .MuiAccordionSummary-root': {
        minHeight: '44px!important',
        '& .MuiAccordionSummary-content': {
          margin: '6px 0',
          justifyContent: 'center',
          '& .MuiTypography-root': {
            fontWeight: 500,
            color: '#9E9E9E',
          },
        },
      },
      [theme.breakpoints.only('md')]: {
        marginTop: 55,
        position: 'fixed',
        width: 500,
      },
    },
    contentListItem: {
      transition: 'linear 0.6s',

      '&:hover': {
        '& .hoverMessage': {
          display: 'block',
        },
      },
      '& .hoverMessage': {
        display: 'none',
      },
    },

    nomSender: {
      marginTop: 0,
      marginBottom: 2,
      '& .MuiTypography-root': {
        color: '#212121',
        fontWeight: 500,
        fontSize: '0.875rem',
      },
    },
    objet: {
      display: 'flex',
      alignItems: 'center',
      '& .MuiSvgIcon-root': {
        height: 14,
        width: 14,
        marginRight: 9,
      },
      '& .MuiTypography-root': {
        fontSize: '0.75rem',
        color: '#424242',
        fontWeight: 400,
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        '-webkit-line-clamp': 1,
        '-webkit-box-orient': 'vertical',
        whiteSpace: 'nowrap',
        maxWidth: 370,
        width: '100%',
      },
    },
    messageItem: {
      '& .MuiListItem-root ': {
        display: 'flex',
        justifyContent: 'space-between',
        padding: '12px 24px',
      },
    },
    date: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-end',
      minWidth: 42,
      marginRight: 6,
      whiteSpace: 'pre',
      '& .MuiTypography-root ': {
        fontSize: '0.75rem',
      },
    },
    read: { background: '#F5F6FA' },
    notRead: { background: '#E0E0E0' },
    noMessageTxt: {
      marginTop: 60,
      fontWeight: 600,
      color: '#00000029',
    },
    checkedMsgContainer: {
      position: 'absolute',
      left: -20,
      top: -4,
      '& svg': {
        width: 18,
        height: 18,
      },
    },

    nomRecept: {
      margin: '10px',
      '& .MuiTypography-root': {
        color: '#212121',
        fontWeight: 100,
        fontSize: '0.875rem',
      },
    },
  }),
);

export default useStyles;
