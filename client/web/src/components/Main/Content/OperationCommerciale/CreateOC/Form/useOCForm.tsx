import { useState, ChangeEvent, FormEvent } from 'react';
import OCInterface, { LaboratoireInterface } from '../Interface/OCInterface';
import { ProjectInterface } from '../../../Actualite/Interface';

const useOCForm = (defaultState?: OCInterface) => {
  const initialState: OCInterface = defaultState || {
    libelle: '',
    description: '',
    dateDebut: null,
    dateFin: null,
    accordCommercial: false,
    generateAction: false,
    selectedFiles: { presentation: [] },
    caMoyenPharmacie: 0,
    nbPharmacieCommande: 0,
    nbMoyenLigne: 0,
    isMarchePromotion: false,
    idMarche: '',
    idPromotion: '',
    marchePromotion: null,
  };

  const [values, setValues] = useState<OCInterface>(initialState);
  const [generateAction, setGenerateAction] = useState<boolean>(false);
  const numberFields = ['caMoyenPharmacie', 'nbPharmacieCommande', 'nbMoyenLigne'];
  const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement | any>) => {
    if (e && e.target) {
      const { name, value, checked } = e.target;
      setValues(prevState => ({
        ...prevState,
        ...defaultState,
        [name]: typeof value === 'string' ? value.trimLeft() : value,
      }));
      if (name === 'accordCommercial') {
        setValues(prevState => ({ ...prevState, ...defaultState, [name]: checked }));
      }
      if (numberFields.includes(name)) {
        const parsed = parseInt(value, 10);
        setValues(prevState => ({
          ...prevState,
          ...defaultState,
          [name]: isNaN(parsed) ? 0 : parsed,
        }));
      }
    }
  };

  const handleChangeAutocomplete = (inputValue: any, name: string) => {
    setValues(prevState => ({ ...prevState, ...defaultState, [name]: inputValue }));
  };

  const handleChangeProjectAutocomplete = (inputValue: any) => {
    setValues(prevState => ({ ...prevState, project: inputValue }));
    setValues(prevState => ({ ...prevState, idProject: inputValue.id }));
  };

  const handleChangeActionDescription = (value: string) => {
    setValues(prevState => ({ ...prevState, actionDescription: value }));
  };

  const handleChangeActionDueDate = date => {
    if (date) setValues(prevState => ({ ...prevState, actionDueDate: date }));
  };

  const handleTodoChangeCheckbox = () => {
    setValues(prevState => ({ ...prevState, generateAction: !prevState.generateAction }));
  };

  return {
    handleChange,
    handleChangeAutocomplete,
    handleTodoChangeCheckbox,
    handleChangeActionDescription,
    handleChangeActionDueDate,
    handleChangeProjectAutocomplete,
    values,
    generateAction,
    setValues,
  };
};

export default useOCForm;
