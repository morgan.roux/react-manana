import { AxiosResponse } from 'axios';
import { ItemInterface } from '../Interface/AllValuesInterface';
export default interface OCFormInterface {
  setNextBtnDisabled: (disabled: boolean) => void;
  setPharmaciesSelectedFiles: (files: File[]) => void;
  match: {
    params: { filter: string | undefined; type: string | undefined };
  };
  setAllValues: (values: any) => void;
  allValues: any;
}
