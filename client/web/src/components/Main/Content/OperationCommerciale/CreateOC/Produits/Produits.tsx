import React, { FC, useState, useEffect } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import useStyles from './styles';
import { Box, RadioGroup, Typography } from '@material-ui/core';
import RadioButton from '../../../../../Common/SelectionContainer/RadioButton';
import ProduitsInterface from '../Interface/ProduitsInterface';
import Dropzone from '../../../../../Common/Dropzone';
import { useQuery } from '@apollo/react-hooks';
import RemiseCell from '../../../Produits/ProduitListView/RemiseCell';
import BodyCell from '../../../Produits/ProduitListView/BodyCell';
import Validator from '../Produits/Validator';
import FullScreenModal from '../../../../../Common/FullScreenModal';
import ProductDetails from '../../../../../Main/Content/Produits/ProduitDetails/ProduitDetails';
import { CreateOperationProduit } from '../../../../../Common/newWithSearch/ComponentInitializer';
import { Column } from '../../../../../Common/newCustomContent/interfaces';
import TableQuantite from '../../../../../Common/TableQuantite/TableQuantite';
import { GET_CHECKEDS_PRODUIT } from '../../../../../../graphql/ProduitCanal/local';
import SearchInput from '../../../../../Common/newCustomContent/SearchInput';

export const columns: Column[] = [
  {
    name: 'produit.produitCode.code',
    label: 'CODE',
    renderer: (value: any) => {
      return (
        (value && value.produit && value.produit.produitCode && value.produit.produitCode.code) ||
        '-'
      );
    },
  },
  {
    name: 'produit.libelle',
    label: 'LIBELLE',
    renderer: (value: any) => {
      return (value && value.produit && value.produit.libelle) || '-';
    },
  },
  {
    name: '',
    label: 'QUANTITE MIN',
    renderer: (value: any) => {
      return <TableQuantite currentCanalArticle={value} />;
    },
  },
  {
    name: 'produit.produitTechReg.laboExploitant.nomLabo',
    label: 'LABORATOIRE',
    renderer: (value: any) => {
      return (
        (value &&
          value.produit &&
          value.produit.produitTechReg &&
          value.produit.produitTechReg.laboExploitant &&
          value.produit.produitTechReg.laboExploitant.nomLabo) ||
        '-'
      );
    },
  },
  { name: 'stv', label: 'STV' },
  {
    name: '',
    label: 'CARTON',
    renderer: (value: any) => {
      return '-';
    },
  },
  {
    name: '',
    label: 'NPFM',
  },
  {
    name: 'prixPhv',
    label: 'TARIF HT',
    renderer: (value: any) => {
      return value && value.prixPhv ? `${value.prixPhv} €` : '-';
    },
  },
  {
    name: '',
    label: 'REMISE',
    renderer: (value: any) => {
      return (
        <RemiseCell
          currentCanalArticle={value ? value : null}
          remiseOnly={true}
          useLocalPanier={true}
        />
      );
    },
  },
  {
    name: 'qteStock',
    label: 'STOCK PLATEFORME',
    renderer: (value: any) => {
      return (
        <BodyCell
          value={value && value.qteStock ? value.qteStock : 0}
          pink={false}
          plateforme={true}
        />
      );
    },
  },
];

const Produits: FC<ProduitsInterface & RouteComponentProps> = ({
  setNextBtnDisabled,
  history,
  setProduitsSelectedFiles,
  setAllValues,
  allValues,
}) => {
  const classes = useStyles({});
  const types = [
    { value: 'array', label: 'Sélection de Produits' },
    { value: 'file', label: 'Fichier de Produits' },
  ];

  const [openFullScreenModal, setOpenFullScreenModal] = useState<boolean>(false);
  const [currentId, setCurrentId] = useState<string>();

  const checkesdProduitResult = useQuery(GET_CHECKEDS_PRODUIT);

  const checkedsProduit =
    (checkesdProduitResult &&
      checkesdProduitResult.data &&
      checkesdProduitResult.data.checkedsProduit) ||
    [];

  useEffect(() => {
    if (allValues) {
      setNextBtnDisabled(Validator(allValues, activeSelection, checkedsProduit));
    }
  }, [allValues, checkedsProduit]);

  const pathName = history.location.pathname;
  const [activeSelection, setActiveSelection] = useState<string>(
    allValues && allValues.produitsType,
  );

  const handleSelection = (value: string) => {
    if (!pathName.includes('/produit') && value === 'array') {
      history.push(`${pathName}/produit`);
    }
    setActiveSelection(value);
  };

  useEffect(() => {
    const newPathName = pathName.replace('/produit', '').replace('/pharmacie', '');
    if (activeSelection !== 'array') {
      history.push(newPathName);
    } else {
      history.push(`${newPathName}/produit`);
    }
    setAllValues((prevState: any) => ({ ...prevState, ...{ produitsType: activeSelection } }));
  }, [activeSelection]);

  return (
    <Box display="flex" flexDirection="column" alignItems="center" width="100%">
      <Typography>Cible de produits</Typography>
      <Box display="flex" flexDirection="row">
        <RadioGroup row={true}>
          {types.map(
            item =>
              item && (
                <RadioButton
                  active={activeSelection === item.value ? true : false}
                  setSelection={handleSelection}
                  value={item.value}
                  label={item.label}
                  key={item.value}
                />
              ),
          )}
        </RadioGroup>
      </Box>
      {activeSelection === 'array' && (
        <div className={classes.tableContainer}>
          <SearchInput fullWidth={true} searchPlaceholder="Rechercher un produit" />
          <Box>{CreateOperationProduit}</Box>
        </div>
      )}
      {activeSelection === 'file' && (
        <Dropzone
          acceptFiles={
            'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
          }
          setSelectedFiles={setProduitsSelectedFiles}
          selectedFiles={allValues && allValues.selectedFiles && allValues.selectedFiles.produits}
          multiple={false}
        />
      )}
      <FullScreenModal
        open={openFullScreenModal}
        setOpen={setOpenFullScreenModal}
        fullWidth={true}
        content={<ProductDetails idProduct={currentId} />}
      />
    </Box>
  );
};

export default withRouter(Produits);
