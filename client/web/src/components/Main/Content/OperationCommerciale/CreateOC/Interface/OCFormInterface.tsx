import OCInterface from './OCInterface';
export default interface OCFormInterface {
  match: {
    params: {
      id: string | undefined;
    };
  };
  defaultState?: OCInterface;
  setNextBtnDisabled: (disabled: boolean) => void;
  setPresentationSelectedFiles?: (files: File[]) => void;
  setAllValues: (values: any) => void;
  allValues: any;
}
