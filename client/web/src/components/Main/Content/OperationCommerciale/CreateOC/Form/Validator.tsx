import AllValuesInterface from '../Interface/AllValuesInterface';

const Validator = (values: AllValuesInterface) => {
  switch (values.libelle) {
    case '':
    case undefined:
    case null: {
      return true;
    }
  }
  switch (values.description) {
    case '':
    case undefined:
    case null: {
      return true;
    }
  }
  switch (values.codeItem) {
    case '':
    case undefined:
    case null: {
      return true;
    }
  }
  switch (values.dateDebut) {
    case undefined:
    case null: {
      return true;
    }
  }
  switch (values.dateFin) {
    case undefined:
    case null: {
      return true;
    }
  }
  switch (values.selectedFiles.presentation.length) {
    case 0:
    case undefined:
    case null: {
      return true;
    }
  }
  switch (values.canalCommande) {
    case undefined:
    case null: {
      return true;
    }
  }
  return false;
};
export default Validator;
