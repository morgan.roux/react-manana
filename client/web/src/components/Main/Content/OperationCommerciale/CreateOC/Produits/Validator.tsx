import AllValuesInterface from '../Interface/AllValuesInterface';
const Validator = (values: AllValuesInterface, activeSelection: string, checkedsProduit: any) => {
  if (activeSelection === 'array') {
    switch (checkedsProduit) {
      case undefined: {
        return true;
      }
    }
    switch (checkedsProduit.length) {
      case 0: {
        return true;
      }
    }
    return false;
  } else if (activeSelection === 'file') {
    switch (values.selectedFiles.produits.length) {
      case 0: {
        return true;
      }
    }
    return false;
  } else {
    return false;
  }
};
export default Validator;
