import React, { FC, useEffect, useState } from 'react';
import useStyles from './styles';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import useOCForm from './useOCForm';
import OCFOrmInterface from '../Interface/OCFormInterface';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import ValidatorType from './ValidatorType';
import { Moment } from 'moment';
import { DateRangePicker } from '../../../../../Common/DateRangePicker';
import FormInterface from '../Interface/OCInterface';
import { Button, Checkbox, Fade, FormLabel, IconButton, Tooltip } from '@material-ui/core';
import CustomTextarea from '../../../../../Common/CustomTextarea';
import CustomTree from '../../../../../Common/CustomTree';
import { CustomFullScreenModal } from '../../../../../Common/CustomModal';
import WithSearch from '../../../../../Common/newWithSearch/withSearch';
import CustomContent from '../../../../../Common/newCustomContent';
import { GET_SEARCH_CUSTOM_CONTENT_MARCHE } from '../../../../../../graphql/Marche/query';
import { GET_SEARCH_CUSTOM_CONTENT_PROMOTION } from '../../../../../../graphql/Promotion/query';
import { CustomTableWithSearchColumn } from '../../../../../Common/CustomTableWithSearch/interface';
import { getColumns } from '../../../../../Dashboard/Marche/MarcheList/column';
import { CustomCheckbox } from '../../../../../Common/CustomCheckbox';
import moment from 'moment';

const TypeOperation: FC<OCFOrmInterface & RouteComponentProps> = ({
  setNextBtnDisabled,
  setAllValues,
  allValues,
}) => {
  const classes = useStyles({});
  const { values, setValues, handleChange } = useOCForm(allValues as FormInterface);
  const [openModal, setOpenModal] = useState<boolean>(false);

  const { idMarche, idPromotion, dateDebut, dateFin } = values;

  const isOnMarche =
    allValues &&
    allValues.codeItem &&
    allValues.codeItem.code &&
    allValues.codeItem.code &&
    allValues.codeItem.code === 'MARCHE';

  const searchType = isOnMarche ? 'marche' : 'promotion';
  const searchQuery = isOnMarche
    ? GET_SEARCH_CUSTOM_CONTENT_MARCHE
    : GET_SEARCH_CUSTOM_CONTENT_PROMOTION;

  const DATES_COLUMNS: any[] = getColumns('date');

  const MARCHE_COLUMNS: any[] = getColumns('marche');
  const PROMO_COLUMNS: any[] = getColumns('promotion');

  const ACTIONS_COLUMNS: any[] = [
    {
      name: '',
      label: '',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        return (
          <>
            <Tooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Modifier">
              <IconButton color="secondary">
                <Checkbox
                  value={row.type === 'marche' ? idMarche : idPromotion}
                  checked={row.type === 'marche' ? idMarche === row.id : idPromotion === row.id}
                  onChange={handleSelectType(row)}
                  edge="start"
                />
              </IconButton>
            </Tooltip>
          </>
        );
      },
    },
  ];

  const COLUMNS: CustomTableWithSearchColumn[] = isOnMarche
    ? [...ACTIONS_COLUMNS, ...MARCHE_COLUMNS, ...DATES_COLUMNS]
    : [...ACTIONS_COLUMNS, ...PROMO_COLUMNS, ...DATES_COLUMNS];

  const tableProps = {
    isSelectable: false,
    paginationCentered: true,
    columns: COLUMNS,
  };

  useEffect(() => {
    if (values) {
      setAllValues((prevState: any) => ({
        ...allValues,
        ...values,
      }));
    }
  }, [values]);

  useEffect(() => {
    if (allValues) {
      setNextBtnDisabled(ValidatorType(allValues));
    }
  }, [allValues]);

  const [checkedItem, setCheckedItem] = useState<any>(allValues && allValues.codeItem);

  useEffect(() => {
    setValues(prevState => ({
      ...prevState,
      ...{ codeItem: checkedItem },
    }));
  }, [checkedItem]);

  const handleSelectType = (row: any) => () => {
    setValues(prevState => ({
      ...prevState,
      marchePromotion: row,
      idMarche: row.type === 'marche' ? row.id : '',
      idPromotion: row.type === 'promotion' ? row.id : '',
    }));
  };
  const startDate = moment(dateDebut).format('YYYY-MM-DD');
  const endDate = moment(dateFin).format('YYYY-MM-DD');
  return (
    <WithSearch
      type={searchType}
      WrappedComponent={CustomContent}
      searchQuery={searchQuery}
      props={tableProps}
      optionalMust={[
        { term: { isRemoved: false } },
        { range: { dateDebut: { lte: startDate } } },
        { range: { dateFin: { gte: endDate } } },
      ]}
    />
  );
};

export default withRouter(TypeOperation);
