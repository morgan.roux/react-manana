import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
    contentDialog: {
      '& .MuiDialog-paper': {
        minWidth: 381,
      },
      '& .MuiDialogActions-root': {
        padding: theme.spacing(1, 2, 2),
      },
    },
    inputText: {
      '& .MuiFormLabel-root': {
        color: '#878787',
      },
    },
    switch: {
      '& .MuiSwitch-colorSecondary.Mui-checked': {
        color: theme.palette.secondary.main,
      },
      '& .MuiSwitch-colorSecondary.Mui-checked + .MuiSwitch-track': {
        backgroundColor: theme.palette.secondary.main,
      },
    },
    btnDesagree: {
      background: '#EFEFEF',
      textTransform: 'uppercase',
      fontFamily: 'Roboto',
      fontSize: '0.875rem',
      fontWeight: 'bold',
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2),
      boxShadow: '0px 2px 2px #00000040',
      borderRadius: '3px',
      color: '#4D4D4D',
    },
    btnAgree: {
      background:
        'transparent linear-gradient(231deg, #E34741 0%, #E34168 100%) 0% 0% no-repeat padding-box',
      textTransform: 'uppercase',
      fontFamily: 'Roboto',
      fontSize: '0.875rem',
      fontWeight: 'bold',
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2),
      boxShadow: '0px 2px 2px #00000040',
      borderRadius: '3px',
      color: '#FFFFFF',
    },
  }),
);

export default useStyles;
