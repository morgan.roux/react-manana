import { useApolloClient, useQuery } from '@apollo/react-hooks';
import {
  Box,
  Collapse,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Switch,
  Typography,
} from '@material-ui/core';
import { Add, ArrowBack, Close } from '@material-ui/icons';
import classnames from 'classnames';
import React, { Dispatch, FC, Fragment, SetStateAction } from 'react';
import { ME_me } from '../../../../graphql/Authentication/types/ME';
import { GET_RGPD_PARTENAIRES } from '../../../../graphql/Rgpd';
import {
  RGPD_PARTENAIRES,
  RGPD_PARTENAIRESVariables,
  RGPD_PARTENAIRES_rgpdPartenaires,
} from '../../../../graphql/Rgpd/types/RGPD_PARTENAIRES';
import { nl2br } from '../../../../services/Html';
import { getAccessToken, getGroupement, getUser } from '../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import CustomButton from '../../../Common/CustomButton';
import { LoaderSmall } from '../../../Dashboard/Content/Loader';
import { RgpdPopupChildProps } from './RgpdPopup';
import useStyles from './styles';

export interface RgpdPartenaireInterface extends RGPD_PARTENAIRES_rgpdPartenaires {
  open: boolean;
  accepted?: boolean;
}

export interface RgpdPopupPartenaireProps {
  partenaireList: RgpdPartenaireInterface[];
  setPartenaireList: Dispatch<SetStateAction<RgpdPartenaireInterface[]>>;
}

const RgpdPopupPartenaire: FC<RgpdPopupChildProps & RgpdPopupPartenaireProps> = ({
  setIsOnPartenaire,
  setIsOnAccueiPlus,
  partenaireList,
  setPartenaireList,
  display,
  handleClickSaveAndClose,
  mutationVariables,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();

  const groupement = getGroupement();
  const idGroupement = (groupement && groupement.id) || '';

  const user: ME_me = getUser();
  const token = getAccessToken();

  const [parteListAccepteds, setParteListAccepteds] = React.useState<Array<boolean | undefined>>(
    [],
  );

  const [showSaveBtn, setShowSaveBtn] = React.useState<boolean>(false);
  const [showUserInfo, setShowUserInfo] = React.useState<boolean>(false);

  const title = 'Sélectionner les partenaires';
  const description =
    "Vous pouvez définir vos préférences de consentement pour chaque partenaire listé ci-dessous individuellement. Cliquez sur le nom d'un partenaire pour obtenir plus d'informations sur ce qu'il fait, les données qu'il récolte et comment il les utilise.";

  const { data: parteData, loading: parteLoading } = useQuery<
    RGPD_PARTENAIRES,
    RGPD_PARTENAIRESVariables
  >(GET_RGPD_PARTENAIRES, {
    fetchPolicy: 'cache-and-network',
    variables: { idGroupement },
    onError: error => {
      console.log('error :>> ', error);
      error.graphQLErrors.map(err =>
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message }),
      );
    },
  });

  // Set partenaireList
  React.useEffect(() => {
    if (parteData && parteData.rgpdPartenaires && parteData.rgpdPartenaires.length > 0) {
      const partenaires: RgpdPartenaireInterface[] = parteData.rgpdPartenaires.map(i => ({
        ...i,
        open: false,
        accepted: true,
      }));
      setPartenaireList(partenaires);
    }
  }, [parteData]);

  // Set parteListAccepteds
  React.useEffect(() => {
    if (partenaireList && partenaireList.length > 0) {
      const res = partenaireList.map(i => i.accepted);
      setParteListAccepteds(res);
    }
  }, [partenaireList]);

  // Set showSaveBtn
  React.useEffect(() => {
    if (parteListAccepteds.includes(true) && parteListAccepteds.includes(false)) {
      setShowSaveBtn(true);
    } else {
      setShowSaveBtn(false);
    }
  }, [parteListAccepteds]);

  const handleClickBack = () => {
    setIsOnAccueiPlus(true);
    setIsOnPartenaire(false);
  };

  const handleToggleUserInfo = () => {
    setShowUserInfo(prev => !prev);
  };

  const handleClickRefuseAll = () => {
    setPartenaireList(prev => prev.map(i => ({ ...i, accepted: false })));
    handleClickSaveAndClose({
      input: {
        ...mutationVariables.input,
        refuse_all: true,
        accept_all: false,
        historiquesInfosPlus: [],
      },
    });
  };

  const handleClickAcceptAll = () => {
    setPartenaireList(prev => prev.map(i => ({ ...i, accepted: true })));
    handleClickSaveAndClose({
      input: {
        ...mutationVariables.input,
        refuse_all: false,
        accept_all: true,
        historiquesInfosPlus: [],
      },
    });
  };

  const handleClickSave = () => {
    console.log('mutationVariables :>> ', mutationVariables);
    handleClickSaveAndClose(mutationVariables);
  };

  const handleClickItem = (item: RgpdPartenaireInterface) => (event: React.ChangeEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    setPartenaireList(prev =>
      prev.map(i => {
        if (i.id === item.id) {
          return { ...i, open: !i.open };
        } else {
          return i;
        }
      }),
    );
  };

  const handleSwithItem = (item: RgpdPartenaireInterface) => (
    event: React.ChangeEvent<any>,
    checked: boolean,
  ) => {
    event.preventDefault();
    event.stopPropagation();
    setPartenaireList(prev =>
      prev.map(i => {
        if (i.id === item.id) {
          return { ...i, accepted: !i.accepted };
        } else {
          return i;
        }
      }),
    );
  };

  return (
    <Box width="100%" display={display} flexDirection="column">
      <div className={classes.flexRowContainer}>
        <IconButton
          size="small"
          color="inherit"
          className={classes.arrowBack}
          onClick={handleClickBack}
        >
          <ArrowBack />
        </IconButton>
        <Typography className={classnames(classes.title, classes.titleWithoutMargin)}>
          {title}
        </Typography>
      </div>
      <Typography
        className={classnames(classes.description, classes.accueilPlusDesc)}
        dangerouslySetInnerHTML={{ __html: nl2br(description) } as any}
      />
      {parteLoading ? (
        <div className={classes.loaderContainer}>
          <LoaderSmall />
        </div>
      ) : (
        <List component="line" className={classes.list}>
          {partenaireList.map((auth, index) => {
            return (
              <Fragment key={index}>
                <ListItem button={true}>
                  <ListItemIcon onClick={handleClickItem(auth)}>
                    {auth.open ? <Close /> : <Add />}
                  </ListItemIcon>
                  <ListItemText primary={auth.title} />
                  <Switch checked={auth.accepted || false} onChange={handleSwithItem(auth)} />
                </ListItem>
                <Collapse in={auth.open} timeout="auto" unmountOnExit={true}>
                  <Typography
                    className={classnames(classes.description)}
                    dangerouslySetInnerHTML={{ __html: nl2br(auth.description) } as any}
                  />
                </Collapse>
              </Fragment>
            );
          })}
        </List>
      )}
      <div className={classes.userInfoBtnContainer} onClick={handleToggleUserInfo}>
        {showUserInfo ? <Close /> : <Add />}
        <Typography>Voir les infos de l'utilisateur</Typography>
      </div>
      <Collapse
        in={showUserInfo}
        timeout="auto"
        unmountOnExit={true}
        className={classes.userInfoContainer}
      >
        <Typography>
          <span>ID utilisateur : </span> {(user && user.id) || ''}
        </Typography>
        <Typography>
          <span>Token : </span> {token || ''}
        </Typography>
      </Collapse>
      <div className={classes.btnsContainer}>
        {showSaveBtn ? (
          <CustomButton color="secondary" variant="contained" onClick={handleClickSave}>
            Enregistrer
          </CustomButton>
        ) : (
          <Fragment>
            <CustomButton color="default" variant="outlined" onClick={handleClickRefuseAll}>
              Tout Refuser
            </CustomButton>
            <CustomButton color="secondary" variant="contained" onClick={handleClickAcceptAll}>
              Tout accepter
            </CustomButton>
          </Fragment>
        )}
      </div>
    </Box>
  );
};

export default RgpdPopupPartenaire;
