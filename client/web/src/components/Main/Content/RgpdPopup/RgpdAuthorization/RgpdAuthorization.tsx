import { IconButton, Typography } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import React, { FC } from 'react';
import useStyles from './styles';
import AddIcon from '@material-ui/icons/Add';
interface RgpdAuthorizationProps {}
export const RgpdAuthorization: FC<RgpdAuthorizationProps> = () => {
  const classes = useStyles({});
  const handleClick = event => {};
  return (
    <>
      <Box className={classes.root}>
        <Box>
          <Typography className={classes.title}> Partenaires </Typography>
        </Box>
        <Box>
          <IconButton onClick={event => handleClick(event)}>
            <AddIcon />
          </IconButton>
        </Box>
      </Box>
    </>
  );
};
