import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      fontFamily: 'Roboto',
      letterSpacing: 0,
    },
    title:{
        fontSize:"1.2rem",
        marginRight:"auto",
        
    },
  }
));

export default useStyles;
