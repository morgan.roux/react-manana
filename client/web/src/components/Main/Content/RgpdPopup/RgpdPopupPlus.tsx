import { useApolloClient, useQuery } from '@apollo/react-hooks';
import {
  Box,
  Collapse,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Switch,
  Typography,
} from '@material-ui/core';
import { Add, ArrowBack, Close } from '@material-ui/icons';
import classnames from 'classnames';
import React, { Dispatch, FC, Fragment, SetStateAction } from 'react';
import { GET_RGPD_ACCUEILS_PLUSES, GET_RGPD_AUTORISATIONS } from '../../../../graphql/Rgpd';
import {
  RGPD_ACCUEILS_PLUSES,
  RGPD_ACCUEILS_PLUSESVariables,
} from '../../../../graphql/Rgpd/types/RGPD_ACCUEILS_PLUSES';
import {
  RGPD_AUTORISATIONS,
  RGPD_AUTORISATIONSVariables,
  RGPD_AUTORISATIONS_rgpdAutorisations,
} from '../../../../graphql/Rgpd/types/RGPD_AUTORISATIONS';
import { nl2br } from '../../../../services/Html';
import { getGroupement } from '../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import CustomButton from '../../../Common/CustomButton';
import { LoaderSmall } from '../../../Dashboard/Content/Loader';
import { RgpdPopupChildProps } from './RgpdPopup';
import useStyles from './styles';

export interface AuthorizationInterface extends RGPD_AUTORISATIONS_rgpdAutorisations {
  open: boolean;
  accepted?: boolean;
}

export interface RgpdPopupPlusProps {
  authorizationList: AuthorizationInterface[];
  setAuthorizationList: Dispatch<SetStateAction<AuthorizationInterface[]>>;
}

const RgpdPopupPlus: FC<RgpdPopupChildProps & RgpdPopupPlusProps> = ({
  setIsOnAccueiPlus,
  setIsOnPartenaire,
  authorizationList,
  setAuthorizationList,
  display,
  handleClickSaveAndClose,
  mutationVariables,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();

  const groupement = getGroupement();
  const idGroupement = (groupement && groupement.id) || '';

  const title = 'La sécurité de vos données est pour nous une priorité absolue';
  const description =
    'Nos partenaires et nous déposons des cookies et utilisons des informations non sensibles de votre appareil pour améliorer nos produits et afficher des publicités et contenus personnalisés. Vous pouvez accepter ou refuser ces différentes opérations. Pour en savoir plus sur les cookies, les données que nous utilisons, les traitements que nous réalisons et les partenaires avec qui nous travaillons, vous pouvez consulter notre';

  const [authListAccepteds, setAuthListAccepteds] = React.useState<Array<boolean | undefined>>([]);
  const [showSaveBtn, setShowSaveBtn] = React.useState<boolean>(false);

  const { data } = useQuery<RGPD_ACCUEILS_PLUSES, RGPD_ACCUEILS_PLUSESVariables>(
    GET_RGPD_ACCUEILS_PLUSES,
    {
      variables: { idGroupement },
      onError: error => {
        console.log('error :>> ', error);
        error.graphQLErrors.map(err =>
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message }),
        );
      },
    },
  );

  const rgpdAccueilPlus = data && data.rgpdAccueilPluses && data.rgpdAccueilPluses[0];

  const { data: authData, loading: authLoading } = useQuery<
    RGPD_AUTORISATIONS,
    RGPD_AUTORISATIONSVariables
  >(GET_RGPD_AUTORISATIONS, {
    fetchPolicy: 'cache-and-network',
    variables: { idGroupement },
    onError: error => {
      console.log('error :>> ', error);
      error.graphQLErrors.map(err =>
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message }),
      );
    },
  });

  // Set authorizationList
  React.useEffect(() => {
    if (authData && authData.rgpdAutorisations && authData.rgpdAutorisations.length > 0) {
      const authorizations: AuthorizationInterface[] = authData.rgpdAutorisations.map(i => ({
        ...i,
        open: false,
        accepted: true,
        // accepted: i.order === 1 ? true : undefined,
      }));
      setAuthorizationList(authorizations);
    }
  }, [authData]);

  // Set authListAccepteds
  React.useEffect(() => {
    if (authorizationList && authorizationList.length > 0) {
      const res = authorizationList.filter(i => i.order !== 1).map(i => i.accepted);
      setAuthListAccepteds(res);
    }
  }, [authorizationList]);

  // Set showSaveBtn
  React.useEffect(() => {
    if (authListAccepteds.includes(true) && authListAccepteds.includes(false)) {
      setShowSaveBtn(true);
    } else {
      setShowSaveBtn(false);
    }
  }, [authListAccepteds]);

  const handleClickBack = () => {
    setIsOnAccueiPlus(false);
    setIsOnPartenaire(false);
  };

  const handleClickShowPartenaire = () => {
    setIsOnPartenaire(true);
    setIsOnAccueiPlus(false);
  };

  const handleClickRefuseAll = () => {
    setAuthorizationList(prev => prev.map(i => ({ ...i, accepted: false })));
    handleClickSaveAndClose({
      input: {
        ...mutationVariables.input,
        refuse_all: true,
        accept_all: false,
        historiquesInfosPlus: [],
      },
    });
  };

  const handleClickAcceptAll = () => {
    setAuthorizationList(prev => prev.map(i => ({ ...i, accepted: true })));
    handleClickSaveAndClose({
      input: {
        ...mutationVariables.input,
        refuse_all: false,
        accept_all: true,
        historiquesInfosPlus: [],
      },
    });
  };

  const handleClickSave = () => {
    console.log('mutationVariables :>> ', mutationVariables);
    handleClickSaveAndClose(mutationVariables);
  };

  const handleClickItem = (item: AuthorizationInterface) => (event: React.ChangeEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    setAuthorizationList(prev =>
      prev.map(i => {
        if (i.id === item.id) {
          return { ...i, open: !i.open };
        } else {
          return i;
        }
      }),
    );
  };

  const handleSwithItem = (item: AuthorizationInterface) => (
    event: React.ChangeEvent<any>,
    checked: boolean,
  ) => {
    event.preventDefault();
    event.stopPropagation();
    setAuthorizationList(prev =>
      prev.map(i => {
        if (i.id === item.id) {
          return { ...i, accepted: !i.accepted };
        } else {
          return i;
        }
      }),
    );
  };

  return (
    <Box width="100%" display={display} flexDirection="column">
      <div className={classes.flexRowContainer}>
        <IconButton
          size="small"
          color="inherit"
          className={classes.arrowBack}
          onClick={handleClickBack}
        >
          <ArrowBack />
        </IconButton>
        <Typography className={classnames(classes.title, classes.titleWithoutMargin)}>
          {(rgpdAccueilPlus && rgpdAccueilPlus.title) || title}
        </Typography>
      </div>
      <Typography
        className={classnames(classes.description, classes.accueilPlusDesc)}
        dangerouslySetInnerHTML={
          {
            __html: nl2br(
              `${(rgpdAccueilPlus && rgpdAccueilPlus.description) ||
                description} <a href="/rgpd-politique-de-confident">politique de confidentialité.</a>`,
            ),
          } as any
        }
      />
      <div className={classes.row}>
        <Typography className={classnames(classes.title, classes.titleWithoutMargin)}>
          Vous autorisez
        </Typography>
      </div>
      {authLoading ? (
        <div className={classes.loaderContainer}>
          <LoaderSmall />
        </div>
      ) : (
        <List component="line" className={classes.list}>
          {authorizationList.map((auth, index) => {
            return (
              <Fragment key={index}>
                <ListItem button={true}>
                  <ListItemIcon onClick={handleClickItem(auth)}>
                    {auth.open ? <Close /> : <Add />}
                  </ListItemIcon>
                  <ListItemText primary={auth.title} />
                  {auth.order === 1 ? (
                    <span>Requis</span>
                  ) : (
                    <Switch checked={auth.accepted || false} onChange={handleSwithItem(auth)} />
                  )}
                </ListItem>
                <Collapse in={auth.open} timeout="auto" unmountOnExit={true}>
                  <Typography
                    className={classnames(classes.description)}
                    dangerouslySetInnerHTML={{ __html: nl2br(auth.description) } as any}
                  />
                </Collapse>
              </Fragment>
            );
          })}
        </List>
      )}

      <div className={classes.row}>
        <Typography className={classnames(classes.title, classes.titleWithoutMargin)}>
          Par tous nos partenaires
        </Typography>
        <CustomButton variant="text" color="secondary" onClick={handleClickShowPartenaire}>
          Voir nos partenaires
        </CustomButton>
      </div>
      <div className={classes.btnsContainer}>
        {showSaveBtn ? (
          <CustomButton color="secondary" variant="contained" onClick={handleClickSave}>
            Enregistrer
          </CustomButton>
        ) : (
          <Fragment>
            <CustomButton color="default" variant="outlined" onClick={handleClickRefuseAll}>
              Tout Refuser
            </CustomButton>
            <CustomButton color="secondary" variant="contained" onClick={handleClickAcceptAll}>
              Tout accepter
            </CustomButton>
          </Fragment>
        )}
      </div>
    </Box>
  );
};

export default RgpdPopupPlus;
