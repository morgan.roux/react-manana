import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const mb25 = 25;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      '& .MuiFormControl-root': {
        marginBottom: 0,
      },
      '& > div.MuiFormControl-root': {
        marginBottom: mb25,
      },
    },
    formRow: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      marginBottom: mb25,
    },
    mr: {
      marginRight: 30,
    },
    mb: {
      marginBottom: mb25,
    },
    colContainer: {
      width: '100%',
      display: 'flex',
      justifyContent: 'flex-start',
      flexDirection: 'column',
    },
    switchTitle: {
      fontWeight: 600,
    },
    singleInput: {
      maxWidth: 391,
    },
    inputSubText: {},
    docContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
    },
    visibilityTitle: {},
    visibilitySubTitle: {
      fontSize: 10,
    },
    reactQuill: {
      '& .ql-toolbar.ql-snow': {
        borderRadius: '5px 5px 0px 0px',
      },
      '& .ql-container.ql-snow': {
        minHeight: 110,
        borderRadius: '0px 0px 5px 5px ',
      },
    },
    switch: {
      display: 'flex',
      alignItems: 'center',
    },
    startEndDate: {
      '& > div:nth-child(1), & > div:nth-child(4)': {
        marginRight: 10,
      },
      '& > span': {
        margin: '0px 30px',
      },
    },
  }),
);

export default useStyles;
