import { useApolloClient, useLazyQuery } from '@apollo/react-hooks';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { debounce } from 'lodash';
import moment from 'moment';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { ME_me } from '../../../../graphql/Authentication/types/ME';
import { DO_SEARCH_IDEE_BONNE_PRATIQUE } from '../../../../graphql/IdeeBonnePratique';
import {
  SEARCH_IDEE_BONNE_PRATIQUE,
  SEARCH_IDEE_BONNE_PRATIQUEVariables,
} from '../../../../graphql/IdeeBonnePratique/types/SEARCH_IDEE_BONNE_PRATIQUE';
import { getGroupement, getUser } from '../../../../services/LocalStorage';
import {
  IdeeOuBonnePratiqueOrigine,
  IdeeOuBonnePratiqueStatus,
} from '../../../../types/graphql-global-types';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop';
import LeftSidebarAndMainPage from '../../../Common/Layouts/LeftSidebarAndMainPage';
import FormationFilter from './FormationFilter';
import FormationMain from './FormationMain';
import useStyles from './styles';
import { PARTAGE_IDEE_BONNE_PRATIQUE_URL } from '../../../../Constant/url';
import FormationForm from './FormationForm';
export interface FilterInterface {
  checkedStatus: IdeeOuBonnePratiqueStatus[];
  checkedClassification: string[];
  checkedOrigine: IdeeOuBonnePratiqueOrigine[];
  date: MaterialUiPickersDate;
  order: 'asc' | 'desc';
  sort: string;
}

export const defaultFilterBy = [{ term: { isRemoved: false } }];

export const defaultOrderBy = [{ dateCreation: { order: 'desc' } }];

export const defaultQueryVariables: SEARCH_IDEE_BONNE_PRATIQUEVariables = {
  type: ['ideeoubonnepratique'],
  take: 10,
  filterBy: defaultFilterBy,
  sortBy: defaultOrderBy,
};

const Formation: FC<RouteComponentProps> = ({ location: { pathname } }) => {
  const classes = useStyles({});
  const user: ME_me = getUser();

  const groupement = getGroupement();
  const idGroupement = (groupement && groupement.id) || '';
  const fictiveDAta = 'fictive';
  const client = useApolloClient();
  const isOnCreate: Boolean = pathname.includes(`${PARTAGE_IDEE_BONNE_PRATIQUE_URL}/create`);
  const isOnEdit: Boolean = pathname.includes(`${PARTAGE_IDEE_BONNE_PRATIQUE_URL}/edit/:ideeId`);
  const noContentValues = {
    list: {
      title: 'Aucune idée ou bonne pratique dans la liste',
      subtitle: "Crées en une en cliquant sur le bouton d'en haut.",
    },
    content: {
      title: 'Aperçu détaillé des idées et bonnes pratiques',
      subtitle:
        "Choisissez une idée ou bonne pratique dans la liste de gauche pour l'afficher en détails dans cette partie.",
    },
  };

  const [activeItem, setActiveItem] = React.useState<any>(null);

  const [searchKeyWord, setSearchKeyWord] = React.useState<string>('');

  const [filter, setFilter] = React.useState<FilterInterface>({
    checkedStatus: [],
    checkedClassification: [],
    checkedOrigine: [],
    date: null,
    order: 'desc',
    sort: 'dateCreation',
  });

  const [queryVariables, setQueryVariables] = React.useState<SEARCH_IDEE_BONNE_PRATIQUEVariables>(
    defaultQueryVariables,
  );

  const [searchIdeeOuPratique, { data, loading, fetchMore }] = useLazyQuery<
    SEARCH_IDEE_BONNE_PRATIQUE,
    SEARCH_IDEE_BONNE_PRATIQUEVariables
  >(DO_SEARCH_IDEE_BONNE_PRATIQUE, {
    variables: queryVariables,
    fetchPolicy: 'cache-and-network',
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  // Execute query
  React.useEffect(() => {
    searchIdeeOuPratique({
      variables: {
        ...queryVariables,
        filterBy: [...queryVariables.filterBy, { term: { 'groupement.id': idGroupement } }],
      },
    });
  }, []);

  React.useEffect(() => {
    searchIdeeOuPratique({
      variables: {
        ...queryVariables,
        filterBy: [...queryVariables.filterBy, { term: { 'groupement.id': idGroupement } }],
      },
    });
  }, [queryVariables]);

  const debouncedSearch = React.useRef(
    debounce((text: string) => {
      if (text.length > 0) {
        const newVariables: SEARCH_IDEE_BONNE_PRATIQUEVariables = {
          ...queryVariables,
          query: {
            query: {
              bool: {
                must: [
                  {
                    query_string: {
                      query: text.startsWith('*') || text.endsWith('*') ? text : `*${text}*`,
                      analyze_wildcard: true,
                    },
                  },
                ],
              },
            },
          },
        };
        setQueryVariables(newVariables);
      } else {
        setQueryVariables(queryVariables);
      }
    }, 1000),
  );

  // Debounced Search
  React.useEffect(() => {
    debouncedSearch.current(searchKeyWord);
  }, [searchKeyWord]);

  // filter
  React.useEffect(() => {
    const statusFilter = { terms: { status: filter.checkedStatus } };
    const typeFilter = { terms: { TypeDeFormation: filter.checkedOrigine } };
    const classifFilter = { terms: { 'classification.id': filter.checkedClassification } };
    const newFilterBy: any[] = [];
    if (filter.checkedStatus.length > 0) {
      newFilterBy.push(statusFilter);
    }

    if (filter.checkedOrigine.length > 0) {
      newFilterBy.push(typeFilter);
    }

    if (filter.checkedClassification.length > 0) {
      newFilterBy.push(classifFilter);
    }

    if (filter.date) {
      const dateFilter = { term: { dateCreation: moment(filter.date).format('YYYY-MM-DD') } };
      newFilterBy.push(dateFilter);
    }

    const newVars: SEARCH_IDEE_BONNE_PRATIQUEVariables = {
      ...queryVariables,
      filterBy: [...defaultFilterBy, ...newFilterBy],
      sortBy: [{ [filter.sort]: { order: filter.order } }],
    };
    setQueryVariables(newVars);
  }, [filter]);

  return (
    <div className={classes.root}>
      {loading && <Backdrop />}
      {isOnEdit || isOnCreate ? (
        <FormationForm data={fictiveDAta} />
      ) : (
        <LeftSidebarAndMainPage
          drawerTitle="Formation"
          sidebarChildren={<FormationFilter filter={filter} setFilter={setFilter} />}
          mainChildren={
            <FormationMain
              data={data}
              searchKeyWord={searchKeyWord}
              setSearchKeyWord={setSearchKeyWord}
              setActiveItem={setActiveItem}
              activeItem={activeItem}
              queryVariables={queryVariables}
              setQueryVariables={setQueryVariables}
              filter={filter}
              setFilter={setFilter}
              fetchMore={fetchMore}
            />
          }
        />
      )}
    </div>
  );
};

export default withRouter(Formation);
