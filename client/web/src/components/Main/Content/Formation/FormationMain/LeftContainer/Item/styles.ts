import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      cursor: 'pointer',
      padding: '20px 15px 23px',
      border: '3px solid #fff',
      borderRadius: 6,
      borderBottom: '1px solid #E0E0E0',
    },
    title: {
      color: theme.palette.secondary.main,
      fontSize: '0.875rem',
      fontWeight: 'bold',
    },
    subTitle: {
      fontSize: '1rem',
      fontWeight: 'bold',
      color: '#212121',
      marginRight: theme.spacing(1.75),
    },
    isActive: {
      border: '3px solid #27272F',
    },
    flexRow: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    titleContainer: {
      display: 'flex',
      alignItems: 'flex-start',
      paddingRight: theme.spacing(3),
    },
    dateFromNow: {
      minWidth: 'fit-content',
      color: '#9E9E9E',
      fontSize: '0.75rem',
    },
    stat: {
      display: 'flex',
      alignItems: 'center',
      fontSize: '0.875rem',
      color: '#616161',
      fontWeight: 'bold',
      '& .MuiSvgIcon-root': {
        fontSize: 18,
        marginRight: 4,
        color: '#424242',
      },
    },
    date: {
      justifyContent: 'flex-start',
      fontSize: '0.875rem',
      color: '#616161',
      fontWeight: 'bold',
      '& .MuiSvgIcon-root': {
        fontSize: 18,
        marginRight: 6,
      },
    },
    labelContent: {
      fontSize: '0.75rem',
      color: '#616161',
    },
    labelContentValue: {
      fontSize: '0.75rem',
      fontWeight: 'bold',
      color: '#616161',
      marginLeft: theme.spacing(1),
      width: 64,
    },
    lenghtValueName: {
      width: 106,
    },
    widthLabel69: {
      width: 69,
    },
    widthLabel81: {
      width: 81,
    },
  }),
);

export default useStyles;
