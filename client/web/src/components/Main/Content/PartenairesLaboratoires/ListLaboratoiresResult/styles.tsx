import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      background: 'rgb(255, 255, 255)',
      height: 'calc(100vh - 86px)',
      borderRight: '1px solid #E5E5E5',
      overflow: 'auto',
      minWidth: 286,
    },
    lien: {
      height: '30px',
    },
    none: {
      textAlign: 'center',
      height: '100%',
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      ' & > * ': {
        margin: theme.spacing(1),
      },
    },
    imgDefault: {
      maxWidth: '400px',
      width: '100%',
    },
  }),
);

export default useStyles;
