import { useMutation } from '@apollo/react-hooks';
import { Snackbar, Switch } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import ListItem from '@material-ui/core/ListItem';
import Typography from '@material-ui/core/Typography';
import classnames from 'classnames';
import React, { FC, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import image from '../../../../../../assets/img/pas_image.png';
import { SET_SORTIE_LABORATOIRE } from '../../../../../../graphql/Laboratoire/mutation';
import { SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire } from '../../../../../../graphql/Laboratoire/types/SEARCH_LABORATOIRE_PARTENAIRE';
import { setLaboratoireSortieVariables } from '../../../../../../graphql/Laboratoire/types/setLaboratoireSortie';
import useStyles from './styles';

interface LaboratoireProps {
  laboratoire: SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire;
  onClick: (labo: SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire) => void;
  active: boolean;
}

const Laboratoire: FC<LaboratoireProps & RouteComponentProps> = ({
  laboratoire,
  onClick,
  active,
}) => {
  const classes = useStyles({});
  const [out, setout] = useState(laboratoire.sortie ? true : false);
  const [openSnackBar, setopenSnackBar] = useState(false);
  const [saveLaboratoireSortie] = useMutation<
    SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire,
    setLaboratoireSortieVariables
  >(SET_SORTIE_LABORATOIRE, {
    onCompleted: ({ id, sortie }) => {
      setout(sortie ? true : false);
    },
    onError: error => {
      setopenSnackBar(true);
    },
  });

  const handleClick = () => {
    onClick(laboratoire);
  };

  /* const onChangeSwitch = (event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => {
    event.preventDefault();
    event.stopPropagation();
    saveLaboratoireSortie({ variables: { id: laboratoire.id, sortie: out ? 0 : 1 } });
  }; */

  return (
    <ListItem
      alignItems="flex-start"
      onClick={handleClick}
      className={classnames(classes.item, active ? classes.active : '')}
    >
      <Box className={classes.imageContainer}>
        <img
          src={(laboratoire && laboratoire.photo && laboratoire.photo.publicUrl) || image}
          className={classes.image}
        />
      </Box>
      <Box
        display="flex"
        alignItems="start"
        flexDirection="column"
        justifyContent="space-between"
        style={{ paddingLeft: '20px', minHeight: '69px' }}
      >
        <Typography className={classes.listTitle}>{laboratoire.nomLabo}</Typography>
        <Box display="flex" alignItems="center">
          <Typography className={classnames(classes.colorGrey, classes.subListTitle)}>
            Canal :
          </Typography>
          <Typography className={classes.subListTitle}>{''}</Typography>
        </Box>
        <Box display="flex" alignItems="center">
          <Typography className={classnames(classes.colorGrey, classes.subListTitle)}>
            Marché :
          </Typography>
          <Typography className={classes.subListTitle}>{laboratoire.type}</Typography>
        </Box>
      </Box>
      {/*  <Switch
        checked={out}
        value={`${laboratoire.nomLabo}.check`}
        color="default"
        inputProps={{ 'aria-label': 'modifier la sortie' }}
        className={classes.switch}
        onChange={onChangeSwitch}
      /> */}
      <Snackbar
        open={openSnackBar}
        message="Echec de l'opération"
        onClose={() => setopenSnackBar(false)}
      />
    </ListItem>
  );
};

export default withRouter(Laboratoire);
