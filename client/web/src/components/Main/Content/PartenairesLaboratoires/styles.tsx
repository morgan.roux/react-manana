import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      height: 'calc(100vh - 86px)',
      overflow: 'hidden',
      flexWrap: 'nowrap',
    },
    root2: {
      background: 'green',
    },
    none: {
      textAlign: 'center',
      height: 'auto',
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      ' & > * ': {
        margin: theme.spacing(1),
      },
    },
    imgDefault: {
      maxWidth: '400px',
      width: '100%',
    },
    drawer: {
      position: 'sticky',
      top: 0,
      width: 275,
      flexShrink: 0,
      height: 'calc(100vh - 86px)',
      borderRight: '1px solid #E5E5E5',
    },
    userInput: {
      marginTop: -24,
    },
    userInputCompteRendu: {
      marginBottom: -24,
    },
  }),
);

export default useStyles;
