import { useLazyQuery } from '@apollo/react-hooks';
import { PilotageProps } from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/Pilotage';
import moment from 'moment';
import { useEffect, useState } from 'react';
import {
  GET_REMUNERATION_MENSUELLE,
  GET_REMUNERATION_SUIVI_OPERATIONNEL,
} from '../../../../../../federation/partenaire-service/pilotage/query';
import {
  GET_REMUNERATION_MENSUELLE as GET_REMUNERATION_MENSUELLE_TYPE,
  GET_REMUNERATION_MENSUELLEVariables,
} from '../../../../../../federation/partenaire-service/pilotage/types/GET_REMUNERATION_MENSUELLE';
import { GET_PLANNING_MARKETINGS } from '../../../../../../federation/partenaire-service/PRTPlanningMarketing/query';
import {
  PLANNING_MARKETINGS,
  PLANNING_MARKETINGSVariables,
} from '../../../../../../federation/partenaire-service/PRTPlanningMarketing/types/PLANNING_MARKETINGS';
import { getPharmacie } from '../../../../../../services/LocalStorage';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { usePlanningPlanMarketing } from '../usePlanningPlanMarketing';
import { useEvolutionRemuneration } from './useEvolutionRemuneration';
import { useSuiviRemuneration } from './useSuiviRemuneration';

export const usePilotage = ({ tab, idLabo, currentLaboPlanningProps, push }) => {
  /** *----------------------------PilotageProps ----------------------------------------------------- */

  const pharmacie = getPharmacie();

  const [startDate, setFilterStartDate] = useState<any>(moment(new Date()).add(-4, 'month'));
  const [endDate, setFilterEndDate] = useState<any>(moment(new Date()));
  const [filterAnneeComparer, setFilterAnneeComparer] = useState<number | undefined>();
  const [viewPilotage, setViewPilotage] = useState<string>('PILOTAGES');
  const [takeSuiviRemuneration, setTakeSuiviRemuneration] = useState<number>(12);
  const [skipSuiviRemuneration, setSkipSuiviRemuneration] = useState<number>(0);
  const [filterOrderTable, setFilterOrderTable] = useState<any>();

  const [loadRemunerationMensuelle, loadingRemunerationMensuelle] = useLazyQuery<
    GET_REMUNERATION_MENSUELLE_TYPE,
    GET_REMUNERATION_MENSUELLEVariables
  >(GET_REMUNERATION_MENSUELLE, { client: FEDERATION_CLIENT });

  const [loadPlanningPlanMarketings, loadingPlanningPlanMarketings] = useLazyQuery<
    PLANNING_MARKETINGS,
    PLANNING_MARKETINGSVariables
  >(GET_PLANNING_MARKETINGS, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
  });

  const handleGoBack = () => {
    push('/laboratoires');
  };

  /** *------------------------REMUNERATION PAR MOIS ---------------------------------- */

  useEffect(() => {
    if (tab === 'pilotage' || tab === 'pilotages') {
      loadRemunerationMensuelle({
        variables: {
          input: {
            idPartenaireTypeAssocie: tab === 'pilotage' ? idLabo : tab === 'pilotages' ? '' : '',
            partenaireType: 'LABORATOIRE',
            dateDebut: moment(startDate).startOf('day'),
            dateFin: moment(endDate).endOf('day'),
          },
        },
      });
    }
  }, [tab, startDate, endDate]);

  const handleRequestFilter = ({
    startDate,
    endDate,
    viewPilotage,
    filterOrderTable,
    filterAnneeComparer,
    skipSuiviRemuneration,
    takeSuiviRemuneration,
  }) => {
    console.log('ENDDATE', startDate, endDate);
    const start = moment(startDate).isSameOrBefore(moment(endDate))
      ? moment(startDate)
      : moment(endDate).add(-4, 'month');
    setFilterStartDate(start.toDate());
    setFilterEndDate(moment(endDate).toDate());
    setFilterAnneeComparer(filterAnneeComparer);
    setFilterOrderTable(filterOrderTable);
    setViewPilotage(viewPilotage);
    setSkipSuiviRemuneration(skipSuiviRemuneration);
    setTakeSuiviRemuneration(takeSuiviRemuneration);
  };

  const handlePilotagePlanMarketing = date => {
    loadPlanningPlanMarketings({
      variables: {
        input: {
          dateDebut: date,
          dateFin: moment(date)
            .add(6, 'days')
            .toDate(),
          partenaireType: 'LABORATOIRE',
          idPharmacie: pharmacie?.id,
          idPartenaireTypeAssocie: tab === 'pilotage' ? idLabo : '',
        },
      },
    });
  };

  /*** ------------------------ REMUNERATION MENSUELLE ---------------------------------------------------------- */
  const dataRemunerationMensuelle = loadingRemunerationMensuelle.data?.remunerationMensuelle;

  console.log('DATA MENSUEL: ', dataRemunerationMensuelle);

  const seriesChiffreParMois = [
    {
      name: 'Prévue',
      type: 'column',
      data: dataRemunerationMensuelle?.data.map(element => {
        return element.totalRemunerationPrevu;
      }) as any,
    },
    {
      name: 'Réalisée',
      type: 'column',
      data: dataRemunerationMensuelle?.data.map(element => {
        return element.totalRemunerationReglement;
      }) as any,
    },
  ];

  const MoisAnnee = dataRemunerationMensuelle?.data.map(element => {
    return moment(element.month).format('MMMM');
  }) as any;

  const optionsChiffreParMois = {
    title: 'Rémunération mensuelle',
    xaxis: MoisAnnee,
    colorBar: ['#FFE43A', '#63B8DD'],
  };

  /** *-------------------- EVOLUTION DE LA REMUNERATION ----------------------------------------------- */

  const {
    evolutionRemunerationProps: totalOperationCommercialProps,
    data: dataEvolution,
  } = useEvolutionRemuneration({
    tab,
    idLabo,
    filterAnneeComparer,
  });

  /** *-----------------------SUIVI REMUNERTION ----------------------------------------- */

  const { suiviRemunerationProps, loadingRemunerationSuiviOperationnelle } = useSuiviRemuneration({
    tab,
    idLabo,
    viewPilotage,
    takeSuiviRemuneration,
    skipSuiviRemuneration,
    filterOrderTable,
  });

  /** *-----------------------SUIVI OPEREATIONNEL ----------------------------------------- */
  const barLineSuiviOperationnel = [
    {
      option: {
        title: 'titre1',
      },
      bar: {
        libelle: '1',
        intervalle: [
          {
            startDate: moment('2021-03-01').toDate(),
            endDate: moment('2021-03-02').toDate(),
          },
          {
            startDate: moment('2021-03-04').toDate(),
            endDate: moment('2021-03-07').toDate(),
          },
        ],
        color: '#63B8DD',
      },
    },
    {
      option: {
        title: 'titre2',
      },
      bar: {
        libelle: '2',
        intervalle: [
          {
            startDate: moment('2021-03-02').toDate(),
            endDate: moment('2021-03-03').toDate(),
          },
          {
            startDate: moment('2021-03-05').toDate(),
            endDate: moment('2021-03-06').toDate(),
          },
        ],
        color: '#F460A5',
      },
    },
  ];

  const totalChiffreParMoisProps = {
    series: seriesChiffreParMois,
    options: optionsChiffreParMois,
  };

  const suiviOperationnelProps = {
    currentDate: new Date(),
    barLine: barLineSuiviOperationnel,
    titreChart: 'Suivi opérationnel',
  };

  const {
    handleOnRequestActionsAndProduits,
    produitData,
    actionData,
    onRequestChangeStatutAction,
  } = currentLaboPlanningProps;

  const pilotageProps: PilotageProps = {
    totalChiffreParMoisProps,
    totalOperationCommercialProps,
    suiviRemunerationProps: suiviRemunerationProps as any,
    suiviOperationnelProps,
    onRequestFilter: handleRequestFilter,
    loading:
      loadingRemunerationMensuelle.loading &&
      (loadingRemunerationSuiviOperationnelle.loading as any) &&
      dataEvolution.loadingRemunerationCompareCurrentDate.loading &&
      dataEvolution.loadingRemunerationCompareDate.loading,
    error:
      loadingRemunerationMensuelle.error &&
      (loadingRemunerationSuiviOperationnelle.error as any) &&
      dataEvolution.loadingRemunerationCompareCurrentDate.loading &&
      dataEvolution.loadingRemunerationCompareDate.loading,

    planningPageProps:
      'pilotage' === tab
        ? { ...currentLaboPlanningProps, hideFirstColumn: true }
        : currentLaboPlanningProps,
    dataPilotagePlanMarketing: loadingPlanningPlanMarketings.data?.pRTPlanningMarketing.map(
      planning => ({ ...planning, prestataire: planning.partenaireTypeAssocie?.nom }),
    ),
    onRequestPilotagePlanMarketing: handlePilotagePlanMarketing,
    hideFirstColumn: 'pilotage' === tab,
    planningDetail: {
      onRequestChangeStatutAction,
      onRequestProduitsAndActions: handleOnRequestActionsAndProduits,
      produits: {
        loading: produitData.loading,
        error: produitData.error,
        data: (produitData.data?.search?.data || []).map(produit => ({
          code: produit.produit?.produitCode?.code || '',
          famille: produit.produit?.famille?.libelleFamille || '',
          nom: produit.produit?.libelle || '',
        })),
      },
      // actions: {
      //   loading: actionData.loading,
      //   error: actionData.error,
      //   data: actionData.data?.todoActions || [],
      // },
    },
    onRequestGoBack: handleGoBack,
  };

  return pilotageProps;
};
