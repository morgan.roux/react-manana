import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import {
  CREATE_CONDITION,
  DELETE_CONDITION,
  UPDATE_CONDITION,
  CHANGE_STATUS_CONDITION,
} from '../../../../../federation/partenaire-service/laboratoire/condition/mutation';
import {
  CREATE_CONDITION as CREATE_CONDITION_TYPE,
  CREATE_CONDITIONVariables,
} from '../../../../../federation/partenaire-service/laboratoire/condition/types/CREATE_CONDITION';
import { GET_CONDITIONS } from '../../../../../federation/partenaire-service/laboratoire/condition/query';
import {
  GET_CONDITIONS as GET_CONDITIONS_TYPE,
  GET_CONDITIONSVariables,
} from '../../../../../federation/partenaire-service/laboratoire/condition/types/GET_CONDITIONS';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import React, { ReactNode, useCallback, useEffect, useState } from 'react';
import { formatFilename } from '../../../../Common/Dropzone/Dropzone';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_GET_PRESIGNED_URL, DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import { uploadToS3 } from '../../../../../services/S3';
import { FichierInput } from '../../../../../types/graphql-global-types';
import { GET_CANALS } from '../../../../../federation/partenaire-service/laboratoire/condition/canal/query';
import {
  GET_CANALS as GET_CANALS_TYPE,
  GET_CANALSVariables,
  GET_CANALS_canals_nodes,
} from '../../../../../federation/partenaire-service/laboratoire/condition/canal/types/GET_CANALS';
import { GET_TYPES_CONDITION } from '../../../../../federation/partenaire-service/laboratoire/condition/type/query';
import {
  GET_TYPES_CONDITION as GET_TYPES_CONDITION_TYPE,
  GET_TYPES_CONDITIONVariables,
  GET_TYPES_CONDITION_pRTConditionCommercialeTypes_nodes,
} from '../../../../../federation/partenaire-service/laboratoire/condition/type/types/GET_TYPES_CONDITION';
import { GET_STATUS_CONDITION } from '../../../../../federation/partenaire-service/laboratoire/condition/status/query';
import {
  GET_STATUS_CONDITION as GET_STATUS_CONDITION_TYPE,
  GET_STATUS_CONDITIONVariables,
  GET_STATUS_CONDITION_pRTConditionCommercialeStatuts_nodes,
} from '../../../../../federation/partenaire-service/laboratoire/condition/status/types/GET_STATUS_CONDITION';
import { GET_ROW_CONDITIONS } from '../../../../../federation/partenaire-service/laboratoire/condition/query';
import {
  GET_ROW_CONDITIONS as GET_ROW_CONDITION_TYPES,
  GET_ROW_CONDITIONSVariables,
} from '../../../../../federation/partenaire-service/laboratoire/condition/types/GET_ROW_CONDITIONS';
import {
  DELETE_CONDITION as DELETE_CONDITION_TYPE,
  DELETE_CONDITIONVariables,
} from '../../../../../federation/partenaire-service/laboratoire/condition/types/DELETE_CONDITION';
import {
  UPDATE_CONDITION as UPDATE_CONDITION_TYPE,
  UPDATE_CONDITIONVariables,
} from '../../../../../federation/partenaire-service/laboratoire/condition/types/UPDATE_CONDITION';
import {
  CHANGE_STATUS as CHANGE_STATUS_TYPE,
  CHANGE_STATUSVariables,
} from '../../../../../federation/partenaire-service/laboratoire/condition/types/CHANGE_STATUS';
import { PRTConditionCommercialeFilter } from '../../../../../types/federation-global-types';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { getGroupement, getPharmacie } from '../../../../../services/LocalStorage';
import { ConditionCommandeItem } from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/ConditionCommerciale/ConditionCommerciale';
import UserAction from '../../../../Common/UserAction';
import { GET_SMYLEYS, GET_USER_SMYLEYS } from '../../../../../graphql/Smyley';
import {
  USER_SMYLEYS,
  USER_SMYLEYSVariables,
} from '../../../../../graphql/Smyley/types/USER_SMYLEYS';
import { GET_COMMENTS } from '../../../../../graphql/Comment';
import { COMMENTS, COMMENTSVariables } from '../../../../../graphql/Comment/types/COMMENTS';
import { SMYLEYS, SMYLEYSVariables } from '../../../../../graphql/Smyley/types/SMYLEYS';
import {
  CREATE_GET_PRESIGNED_URL,
  CREATE_GET_PRESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_GET_PRESIGNED_URL';
import { ErrorPage, Loader } from '@app/ui-kit';
import CommentList from '../../../../Comment/CommentList';
import { Comment } from '../../../../Comment';
import moment from 'moment';
import { endTime, startTime } from '../../DemarcheQualite/util';
import { truncate } from 'lodash';

export interface TypeCanalStatusCondition {
  type?: GET_TYPES_CONDITION_pRTConditionCommercialeTypes_nodes[];
  status?: GET_STATUS_CONDITION_pRTConditionCommercialeStatuts_nodes[];
  canal?: GET_CANALS_canals_nodes[];
}

export const useConditionCommerciale = (
  idLabo: string,
): [
  (data: any) => void,
  (skip: number, take: number, searchText: string, filters: any[]) => void,
  (id: string) => void,
  (id: string, value: string) => void,
  () => void,
  TypeCanalStatusCondition,
  number,
  any,
  (newValue: boolean) => any,
  boolean,
  boolean,
  {
    request: (condition: ConditionCommandeItem, comment: string) => void;
    saving?: boolean;
    saved?: boolean;
    setSaved?: (newSaved: boolean) => void;
  },
  {
    request: (idCondition?: string) => void;
    saving?: boolean;
    saved?: boolean;
    setSaved?: (newSaved: boolean) => void;
  },
  ReactNode,
  (item?: ConditionCommandeItem) => void,
  {
    request: (idCondition?: string) => void;
    saving?: boolean;
    saved?: boolean;
    setSaved?: (newSaved: boolean) => void;
  },
] => {
  const pharmacie = getPharmacie();

  const client = useApolloClient();
  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const [savingConditionCancel, setSavingConditionCancel] = useState<boolean>(false);
  const [savedConditionCloture, setSavedConditionCloture] = useState<boolean>(false);
  const [savingRestaureCondition, setSavingRestaureCondition] = useState<boolean>(false);
  const [savedRestaureCondition, setSavedRestaureCondition] = useState<boolean>(false);
  const [savedConditionCancel, setSavedConditionCancel] = useState<boolean>(false);
  const [conditionCommercialeToShow, setConditionCommercialeToShow] = useState<any>();

  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
  });

  const [changeStatus, changingStatus] = useMutation<CHANGE_STATUS_TYPE, CHANGE_STATUSVariables>(
    CHANGE_STATUS_CONDITION,
    {
      client: FEDERATION_CLIENT,
      onCompleted: () => {
        displaySnackBar(client, {
          message: 'Le changement de statut a été effectué avec succès.',
          type: 'SUCCESS',
          isOpen: true,
        });
        loadingConditions.refetch();
        // loadingRowConditions.refetch();
        setSavedConditionCloture(true);
        setSaving(false);
        setSaved(true);
      },
      onError: () => {
        displaySnackBar(client, {
          message: "Des erreurs se sont survenues pendant l'ajout de cette condition commerciale.",
          type: 'ERROR',
          isOpen: true,
        });
      },
    },
  );

  const loadCanal = useQuery<GET_CANALS_TYPE, GET_CANALSVariables>(GET_CANALS, {
    client: FEDERATION_CLIENT,
  });

  const loadType = useQuery<GET_TYPES_CONDITION_TYPE, GET_TYPES_CONDITIONVariables>(
    GET_TYPES_CONDITION,
    {
      client: FEDERATION_CLIENT,
    },
  );

  const loadStatus = useQuery<GET_STATUS_CONDITION_TYPE, GET_STATUS_CONDITIONVariables>(
    GET_STATUS_CONDITION,
    {
      client: FEDERATION_CLIENT,
    },
  );

  const [loadRowConditions, loadingRowConditions] = useLazyQuery<
    GET_ROW_CONDITION_TYPES,
    GET_ROW_CONDITIONSVariables
  >(GET_ROW_CONDITIONS, {
    client: FEDERATION_CLIENT,
  });

  const [createCondtion, creatationCondition] = useMutation<
    CREATE_CONDITION_TYPE,
    CREATE_CONDITIONVariables
  >(CREATE_CONDITION, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Cette condition commerciale a été ajoutée avec succès.',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingConditions.refetch();
      loadingRowConditions.refetch();
      setSaving(false);
      setSaved(true);
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs se sont survenues pendant l'ajout de cette condition commerciale.",
        type: 'ERROR',
        isOpen: true,
      });
    },
  });

  const [loadCondtions, loadingConditions] = useLazyQuery<
    GET_CONDITIONS_TYPE,
    GET_CONDITIONSVariables
  >(GET_CONDITIONS, {
    client: FEDERATION_CLIENT,
  });

  const [deleteCondition, deletingCondition] = useMutation<
    DELETE_CONDITION_TYPE,
    DELETE_CONDITIONVariables
  >(DELETE_CONDITION, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Cette condition commerciale a été suprimée avec succès.',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingConditions.refetch();
      loadingRowConditions.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message:
          'Des erreurs se sont survenues pendant la supression de cette condition commerciale.',
        type: 'ERROR',
        isOpen: true,
      });
    },
  });

  const [updateCondition, updatingCondition] = useMutation<
    UPDATE_CONDITION_TYPE,
    UPDATE_CONDITIONVariables
  >(UPDATE_CONDITION, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Cette condition commerciale a été modifiée avec succès.',
        type: 'SUCCESS',
        isOpen: true,
      });
      setSaving(false);
      setSaved(true);
    },
    onError: () => {
      displaySnackBar(client, {
        message:
          'Des erreurs se sont survenues pendant la modification de cette condition commerciale.',
        type: 'ERROR',
        isOpen: true,
      });
    },
  });

  const [updateConditionStatut, updatingConditionStatut] = useMutation<
    UPDATE_CONDITION_TYPE,
    UPDATE_CONDITIONVariables
  >(UPDATE_CONDITION, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Cette condition commerciale a été annulée avec succès.',
        type: 'SUCCESS',
        isOpen: true,
      });
    },
    onError: () => {
      displaySnackBar(client, {
        message:
          'Des erreurs se sont survenues pendant la modification de statut de la condition commerciale.',
        type: 'ERROR',
        isOpen: true,
      });
    },
  });

  const [doCreateGetPresignedUrls, doCreateGetPresignedUrlsResult] = useMutation<
    CREATE_GET_PRESIGNED_URL,
    CREATE_GET_PRESIGNED_URLVariables
  >(DO_CREATE_GET_PRESIGNED_URL);

  const getSmyleys = useQuery<SMYLEYS, SMYLEYSVariables>(GET_SMYLEYS, {
    variables: {
      idGroupement: getGroupement().id,
    },

    onCompleted: smyleysResult => {
      if (smyleysResult.smyleys) {
        const filePaths: string[] = smyleysResult.smyleys
          .filter(item => !!item?.photo)
          .map((item: any) => item.photo);

        if (filePaths.length > 0) {
          doCreateGetPresignedUrls({ variables: { filePaths } });
        }
      }
    },

    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        });
      });
    },
  });

  const [loadUserSmyleys, loadingUserSmyles] = useLazyQuery<USER_SMYLEYS, USER_SMYLEYSVariables>(
    GET_USER_SMYLEYS,
  );

  const [loadCommentaires, loadingCommentaire] = useLazyQuery<COMMENTS, COMMENTSVariables>(
    GET_COMMENTS,
  );

  useEffect(() => {
    if (conditionCommercialeToShow) {
      loadCommentaires({
        variables: {
          codeItem: 'CONDITION_COMMERCIALE',
          idItemAssocie: conditionCommercialeToShow.id,
        },
      });
      loadUserSmyleys({
        variables: {
          codeItem: 'CONDITION_COMMERCIALE',
          idSource: conditionCommercialeToShow.id,
        },
      });
    }
  }, [conditionCommercialeToShow]);

  const handleFetchMoreConditions = () => {
    const fetchMore = loadingConditions.fetchMore;
    const variables = loadingConditions.variables;
    const paging = variables.paging;
    if (loadingConditions.data?.pRTConditionCommerciales.nodes) {
      fetchMore({
        variables: {
          ...variables,
          paging: {
            ...paging,
            offset: loadingConditions.data?.pRTConditionCommerciales.nodes,
          },
        },

        updateQuery: (prev: any, { fetchMoreResult }) => {
          if (prev?.pRTConditionCommerciales && fetchMoreResult?.pRTConditionCommerciales.nodes) {
            return {
              ...prev,
              pRTConditionCommerciales: {
                ...prev.pRTConditionCommerciales,
                nodes: [
                  ...prev.pRTConditionCommerciales.nodes,
                  ...fetchMoreResult.pRTConditionCommerciales.nodes,
                ],
              },
            };
          }

          return prev;
        },
      });
    }
  };

  const handleRequestSearch = ({ skip, take, searchText, filter: filters, sortTable }: any) => {
    const keywords = ['idType', 'idCanal', 'idStatut'];
    if (idLabo) {
      const _filters = keywords
        .map(keyword => {
          const tempObject = { or: [] };
          for (let filter of filters) {
            if (filter.keyword === keyword) {
              tempObject.or.push({
                [keyword]: {
                  eq: filter.id,
                },
              } as never);
            }
          }
          return tempObject.or.length > 0 ? tempObject : undefined;
        })
        .filter(e => e);

      const filterAnd: PRTConditionCommercialeFilter[] = [
        {
          partenaireType: {
            eq: 'LABORATOIRE',
          },
        },
        {
          idPartenaireTypeAssocie: {
            eq: idLabo,
          },
        },
        {
          idPharmacie: {
            eq: pharmacie.id,
          },
        },
        {
          or: [
            {
              titre: {
                iLike: `%${searchText}%`,
              },
            },
            {
              description: {
                iLike: `%${searchText}%`,
              },
            },
          ],
        },
      ];

      if (_filters.length > 0) {
        _filters.map(filter => {
          filterAnd.push(filter as any);
        });
      }

      const sortTableFilter = sortTable.column
        ? [
            {
              field: sortTable.column === 'periode' ? 'dateDebut' : sortTable.column,
              direction: sortTable.direction,
            },
          ]
        : [];

      loadCondtions({
        variables: {
          filter: {
            and: filterAnd,
          },
          paging: {
            offset: skip,
            limit: take,
          },
          sorting: sortTableFilter,
        },
      });
      loadRowConditions({
        variables: {
          filter: {
            and: filterAnd,
          },
        },
      });
    }
  };

  const handleOnRequestSave = (data: any) => {
    setSaving(false);
    setSaved(true);
    if (data.fichiers && data.fichiers.length > 0) {
      const filePaths = data.fichiers
        .filter(file => file && file instanceof File)
        .map(file => formatFilename(file, ``));
      if (filePaths.length > 0) {
        doCreatePutPresignedUrl({
          variables: {
            filePaths,
          },
        }).then(async response => {
          if (response.data?.createPutPresignedUrls) {
            try {
              const fichiers = await Promise.all(
                response.data.createPutPresignedUrls.map((item, index) => {
                  if (item?.presignedUrl) {
                    return data.fichiers
                      ? uploadToS3(data.fichiers[index], item?.presignedUrl).then(uploadResult => {
                          if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                            return {
                              chemin: item.filePath,
                              nomOriginal: data.fichiers ? data.fichiers[index].name : '',
                              type: data.fichiers ? data.fichiers[index].type : '',
                            };
                          }
                        })
                      : null;
                  }
                }),
              );
              const oldFiles = (data.fichiers || []).filter(
                file => file && !(file instanceof File),
              );
              saveCondition(
                data,
                fichiers.concat(
                  oldFiles.map(file => ({
                    nomOriginal: file.nomOriginal,
                    chemin: file.chemin,
                    type: file.type,
                  })),
                ) as FichierInput[],
              );
            } catch (error) {}
          } else {
          }
        });
      } else {
        const oldFiles = (data.fichiers || []).filter(file => file && !(file instanceof File));
        saveCondition(
          data,
          oldFiles.map(file => ({
            nomOriginal: file.nomOriginal,
            chemin: file.chemin,
            type: file.type,
          })) as FichierInput[],
        );
      }
    } else {
      saveCondition(data, undefined);
    }
  };

  const saveCondition = (data: any, fichiers: FichierInput[] | undefined) => {
    console.log(
      'condition commerciale: ',
      moment(data.periode.debut)
        .startOf('day')
        .utc(true)
        .format(),
      moment(data.periode.fin)
        .startOf('day')
        .utc(true)
        .format(),
    );
    if (idLabo) {
      if (data.id) {
        updateCondition({
          variables: {
            id: data.id,
            input: {
              idPartenaireTypeAssocie: idLabo,
              partenaireType: 'LABORATOIRE',
              titre: data.libelle,
              description: data.details,
              dateDebut: moment(data.periode.debut)
                .startOf('day')
                .utc(true)
                .format(),
              dateFin: moment(data.periode.fin)
                .startOf('day')
                .utc(true)
                .format(),
              fichiers,
              // idCanal: data.idCanal,
              idType: data.idType,
            },
          },
        });
      } else {
        createCondtion({
          variables: {
            input: {
              idPartenaireTypeAssocie: idLabo,
              partenaireType: 'LABORATOIRE',
              titre: data.libelle,
              description: data.details,
              dateDebut: moment(data.periode.debut)
                .startOf('day')
                .utc(true)
                .format(),
              dateFin: moment(data.periode.fin)
                .startOf('day')
                .utc(true)
                .format(),
              fichiers,
              // idCanal: data.idCanal,
              idType: data.idType,
            },
          },
        });
      }
    } else {
    }
  };

  const handleDelete = (id: string) => {
    deleteCondition({ variables: { id } });
  };

  const handleChangeStatus = (id: string, value: string) => {
    changeStatus({
      variables: {
        id,
        idStatut: value,
      },
    });
  };

  const handleCancelCondition = (condition: ConditionCommandeItem, comment: string) => {
    setSavedConditionCancel(false);
    setSavingConditionCancel(true);
    const annulerStatut = loadStatus.data?.pRTConditionCommercialeStatuts?.nodes.filter(
      statut => statut.code === 'ANNULER',
    );
    if (annulerStatut?.length && condition.id && idLabo) {
      updateConditionStatut({
        variables: {
          id: condition.id,
          input: {
            idPartenaireTypeAssocie: idLabo,
            partenaireType: 'LABORATOIRE',
            titre: condition.libelle,
            description: comment,
            dateDebut: condition.periode.debut,
            dateFin: condition.periode.fin,
            fichiers: condition.fichiers,
            // idCanal: condition.canal.id,
            idType: condition.type.id,
            idStatut: annulerStatut.length ? annulerStatut[0].id : '',
          },
        },
      })
        .then(() => {
          setSavedConditionCancel(true);
        })
        .finally(() => {
          setSavingConditionCancel(false);
        });
    } else {
      setSavingConditionCancel(false);
    }
  };

  const handleClotureCondition = (idCondition?: string) => {
    setSavedConditionCloture(false);
    const cloturerStatut = loadStatus.data?.pRTConditionCommercialeStatuts?.nodes.filter(
      statut => statut.code === 'CLOTURER',
    );
    if (cloturerStatut?.length) {
      changeStatus({
        variables: {
          id: idCondition || '',
          idStatut: cloturerStatut?.length ? cloturerStatut[0].id : '',
        },
      });
    }
  };

  const handleRestaureCondition = (idCondition?: string) => {
    setSavedRestaureCondition(false);
    const restaureStatut = loadStatus.data?.pRTConditionCommercialeStatuts?.nodes.filter(
      statut => statut.code === 'EN_COURS',
    );
    if (restaureStatut?.length) {
      changeStatus({
        variables: {
          id: idCondition || '',
          idStatut: restaureStatut?.length ? restaureStatut[0].id : '',
        },
      });
    }
  };

  const handleRefecthSmyles = () => () => {
    loadingUserSmyles.refetch();
  };

  const handleFecthMoreComments = () => {};

  const handleRefecthComments = useCallback(() => {
    loadingCommentaire.refetch();
  }, [loadingCommentaire]);

  return [
    handleOnRequestSave,
    handleRequestSearch,
    handleDelete,
    handleChangeStatus,
    handleFetchMoreConditions,
    {
      type: loadType.data?.pRTConditionCommercialeTypes.nodes,
      status: loadStatus.data?.pRTConditionCommercialeStatuts.nodes,
      canal: loadCanal.data?.canals.nodes,
    },
    (loadingRowConditions.data?.pRTConditionCommercialeAggregate.length || 0) > 0
      ? loadingRowConditions.data?.pRTConditionCommercialeAggregate[0].count?.id || 0
      : 0,
    loadingConditions,
    setSaved,
    saving,
    saved,
    {
      request: handleCancelCondition,
      saving: savingConditionCancel,
      saved: savedConditionCancel,
      setSaved: setSavingConditionCancel,
    },
    {
      request: handleClotureCondition,
      // saving: false,
      // saved: savedConditionCloture,
      // setSaved: setSavedConditionCloture,
    },
    conditionCommercialeToShow ? (
      <>
        <UserAction
          refetchSmyleys={handleRefecthSmyles}
          codeItem="CONDITION_COMMERCIALE"
          idSource={conditionCommercialeToShow.id}
          nbComment={conditionCommercialeToShow.nombreCommentaires}
          nbSmyley={conditionCommercialeToShow.nombreReaction}
          userSmyleys={loadingUserSmyles.data?.userSmyleys as any}
          presignedUrls={
            doCreateGetPresignedUrlsResult &&
            doCreateGetPresignedUrlsResult.data &&
            doCreateGetPresignedUrlsResult.data.createGetPresignedUrls
          }
          smyleys={getSmyleys && getSmyleys.data && getSmyleys.data.smyleys}
          hidePartage
          hideRecommandation
        />
        {loadingCommentaire.loading ? (
          <Loader />
        ) : loadingCommentaire.error ? (
          <ErrorPage />
        ) : (
          <CommentList
            key={`comment_list`}
            comments={loadingCommentaire.data?.comments || []}
            fetchMoreComments={handleFecthMoreComments}
          />
        )}
        <Comment
          refetch={handleRefecthComments}
          codeItem="CONDITION_COMMERCIALE"
          idSource={conditionCommercialeToShow?.id}
        />
      </>
    ) : (
      undefined
    ),
    item => {
      setConditionCommercialeToShow(item);
    },
    {
      request: handleRestaureCondition,
      // saving: false,
      // saved: savedRestaureCondition,
      // setSaved: setSavedRestaureCondition,
    },
  ];
};
