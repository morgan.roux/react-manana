import { useApolloClient, useLazyQuery, useMutation } from '@apollo/react-hooks';
import { Box } from '@material-ui/core';
import React, { FC, useEffect } from 'react';
import { CREATE_ACTIVITE } from '../../../../../../federation/basis/mutation';
import {
  CREATE_ACTIVITE as CREATE_ACTIVITE_TYPE,
  CREATE_ACTIVITEVariables,
} from '../../../../../../federation/basis/types/CREATE_ACTIVITE';
import { TOGGLE_DOCUMENT_TO_FAVORITE } from '../../../../../../federation/ged/document/mutation';
import {
  TOGGLE_DOCUMENT_TO_FAVORITE as TOGGLE_DOCUMENT_TO_FAVORITE_TYPE,
  TOGGLE_DOCUMENT_TO_FAVORITEVariables,
} from '../../../../../../federation/ged/document/types/TOGGLE_DOCUMENT_TO_FAVORITE';
import { GET_COMMENTS } from '../../../../../../graphql/Comment/query';
import { COMMENTS, COMMENTSVariables } from '../../../../../../graphql/Comment/types/COMMENTS';
import { GET_USER_SMYLEYS } from '../../../../../../graphql/Smyley/query';
import { SMYLEYS_smyleys } from '../../../../../../graphql/Smyley/types/SMYLEYS';
import {
  USER_SMYLEYS,
  USER_SMYLEYSVariables,
} from '../../../../../../graphql/Smyley/types/USER_SMYLEYS';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import Content from '../../../EspaceDocumentaire/Content';
import { DocumentCategorie } from '../../../EspaceDocumentaire/EspaceDocumentaire';
import { useStyles } from './styles';

export interface DetailFactureProps {
  saving: boolean;
  saved: boolean;
  setSaved: (newValue: boolean) => void;
  activeDocument?: any;
  onRequestReplace: (document: any) => void;
  onRequestDelete: (document: any) => void;
  tvas: {
    loading: boolean;
    error: Error;
    data: any[];
  };
}

export const DetailFacture: FC<DetailFactureProps> = ({
  saving,
  saved,
  setSaved,
  activeDocument,
  onRequestReplace,
  onRequestDelete,
  tvas,
}) => {
  const classes = useStyles();
  const client = useApolloClient();

  const [
    getCommentaires,
    { loading: commentsLoading, error: commentsError, data: comments, refetch: refetchComments },
  ] = useLazyQuery<COMMENTS, COMMENTSVariables>(GET_COMMENTS);

  const [loadUserSmyleys, loadingUserSmyleys] = useLazyQuery<USER_SMYLEYS, USER_SMYLEYSVariables>(
    GET_USER_SMYLEYS,
  );

  const [
    toggleDocumentFavorite,
    { loading: toggleLoading, error: toggleError, data: toggle },
  ] = useMutation<TOGGLE_DOCUMENT_TO_FAVORITE_TYPE, TOGGLE_DOCUMENT_TO_FAVORITEVariables>(
    TOGGLE_DOCUMENT_TO_FAVORITE,
    {
      onError: () => {
        displaySnackBar(client, {
          message: "Des erreurs se sont survenues pendant l'ajout du document aux favoris!",
          type: 'ERROR',
          isOpen: true,
        });
      },

      client: FEDERATION_CLIENT,
    },
  );

  const [addActivite] = useMutation<CREATE_ACTIVITE_TYPE, CREATE_ACTIVITEVariables>(
    CREATE_ACTIVITE,
    {
      client: FEDERATION_CLIENT,
      // onCompleted: () => {
      //   refetchDocument();
      // },
    },
  );

  useEffect(() => {
    if (toggle?.toggleToGedDocumentFavoris && !toggleLoading && !toggleError) {
      displaySnackBar(client, {
        message: `Le document a été ${
          toggle?.toggleToGedDocumentFavoris?.favoris ? 'ajouté aux' : 'retiré des'
        } favoris!`,
        type: 'SUCCESS',
        isOpen: true,
      });
    }
  }, [toggle, toggleLoading, toggleError]);

  const handleRefetchComments = () => {
    refetchComments();
  };

  useEffect(() => {
    if (activeDocument) {
      getCommentaires({
        variables: { codeItem: 'GED_DOCUMENT', idItemAssocie: activeDocument.id },
      });
      loadUserSmyleys({
        variables: {
          codeItem: 'GED_DOCUMENT',
          idSource: activeDocument.id,
        },
      });
    }
  }, [activeDocument]);

  const handleRefecthSmyleys = () => {
    loadingUserSmyleys.refetch();
  };

  const handleToggleDocumentToOrFromFavorite = (document: DocumentCategorie): void => {
    toggleDocumentFavorite({
      variables: {
        idDocument: document?.id as any,
      },
    });
  };

  const handleFetchMoreComments = (document: DocumentCategorie): void => {};
  const handleDeplace = (document: DocumentCategorie): void => {};

  const handleCompleteDownload = (document: DocumentCategorie): void => {
    addActivite({
      variables: {
        input: {
          activiteTypeCode: 'DOWNLOAD',
          itemCode: 'GED_DOCUMENT',
          idItemAssocie: document.id as any,
        },
      },
    });
  };

  console.log('comments facture', comments)

  return (
    <Box className={classes.container}>
      <Content
        saving={saving}
        saved={saved}
        setSaved={setSaved}
        activeSousCategorie={undefined}
        activeDocument={activeDocument}
        mobileView={window.innerWidth < 1024}
        tabletView={window.innerWidth < 1366}
        validation
        content={{ loading: false, error: false as any, data: activeDocument }}
        commentaires={{ loading: commentsLoading, error: commentsError as any, data: comments?.comments }}
        refetchComments={handleRefetchComments}
        handleSidebarMenu={() => {}}
        handleFilterMenu={() => {}}
        onRequestCreateDocument={(document: any) => {}}
        onRequestAddToFavorite={handleToggleDocumentToOrFromFavorite}
        onRequestDelete={onRequestDelete}
        onRequestRemoveFromFavorite={handleToggleDocumentToOrFromFavorite}
        onRequestReplace={onRequestReplace}
        onRequestFetchMoreComments={handleFetchMoreComments}
        onRequestLike={(document: DocumentCategorie, smyley: SMYLEYS_smyleys | null) => {}}
        onRequestDeplace={handleDeplace}
        onCompleteDownload={handleCompleteDownload}
        refetchSmyleys={handleRefecthSmyleys}
        hidePartage
        hideRecommandation
        refetchDocument={() => {}}
        onRequestDocument={(id: string) => {}}
        onRequestFetchAll={(withSearch: boolean) => {}}
        noValidation
        tvas={tvas}
      />
    </Box>
  );
};
