import React from 'react';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { useState, useEffect } from 'react';
import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';

import {
  GET_PRT_CONTACT as GET_PRT_CONTACT_TYPE,
  GET_PRT_CONTACTVariables,
} from '../../../../../federation/partenaire-service/contact/types/GET_PRT_CONTACT';
import {
  UPDATE_PRT_CONTACT as UPDATE_PRT_CONTACT_TYPE,
  UPDATE_PRT_CONTACTVariables,
} from '../../../../../federation/partenaire-service/contact/types/UPDATE_PRT_CONTACT';
import {
  DELETE_PRT_CONTACT as DELETE_PRT_CONTACT_TYPE,
  DELETE_PRT_CONTACTVariables,
} from '../../../../../federation/partenaire-service/contact/types/DELETE_PRT_CONTACT';
import {
  CREATE_PRT_CONTACT as CREATE_PRT_CONTACT_TYPE,
  CREATE_PRT_CONTACTVariables,
} from '../../../../../federation/partenaire-service/contact/types/CREATE_PRT_CONTACT';

import { GET_PRT_CONTACT } from '../../../../../federation/partenaire-service/contact/query';

import {
  CREATE_PRT_CONTACT,
  UPDATE_PRT_CONTACT,
  DELETE_PRT_CONTACT,
} from '../../../../../federation/partenaire-service/contact/mutation';

import { FONCTIONS } from '../../../../../federation/basis/fonction/query';

import {
  FONCTIONS as FONCTIONS_TYPE,
  FONCTIONSVariables,
} from '../../../../../federation/basis/fonction/types/FONCTIONS';

import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import { uploadToS3 } from '../../../../../services/S3';
import { formatFilename } from '../../../../Common/Dropzone/Dropzone';
import {
  DepartementSortFields,
  PRTContactFilter,
  SortDirection,
} from '../../../../../types/federation-global-types';
import { getGroupement, getPharmacie } from '../../../../../services/LocalStorage';
import { FichierInput, TypeFichier } from '../../../../../types/graphql-global-types';
import { DEPARTEMENTS } from '../../../../../federation/basis/departement/query';
import {
  DEPARTEMENTS as DEPARTEMENTS_TYPE,
  DEPARTEMENTSVariables,
} from '../../../../../federation/basis/departement/types/DEPARTEMENTS';

import { SEARCH_PRT_CONTACT } from '../../../../../federation/search/query';
import {
  SEARCH_PRT_CONTACT as SEARCH_PRT_CONTACT_TYPE,
  SEARCH_PRT_CONTACTVariables,
} from '../../../../../federation/search/types/SEARCH_PRT_CONTACT';

const CIVILITE_LIST = [
  {
    id: 'M.',
    code: 'MONSIEUR',
    libelle: 'Monsieur',
  },
  {
    id: 'Mme',
    code: 'Madame',
    libelle: 'Madame',
  },
  {
    id: 'Mlle',
    code: 'MADEMOISELLE',
    libelle: 'Mademoiselle',
  },
];

export const useContact = (idCurrentLabo: string, AvatarComponent: any, push: any) => {
  const groupement = getGroupement();
  const pharmacie = getPharmacie();

  const client = useApolloClient();
  const [skipContact, setSkipContact] = useState(0);
  const [takeContact, setTakeContact] = useState(10);
  const [searchTextContact, setSearchTextContact] = useState('');
  const [photoContact, setPhotoContact] = useState<any>(null);
  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const [sortTable, setSortTable] = useState<any>({
    column: null,
    direction: 'DESC',
  });

  const [filterPolicy, setFilterPolicy] = useState<'PHARMACIE' | 'TOUT'>('PHARMACIE');

  useEffect(() => {
    if (idCurrentLabo) {
      onRequestContactSearch();
      loadFonctions();
      loadDepartements();
    }
  }, [skipContact, takeContact, searchTextContact, sortTable]);

  // QUERY
  const [loadContacts, loadingContacts] = useLazyQuery<
    SEARCH_PRT_CONTACT_TYPE,
    SEARCH_PRT_CONTACTVariables
  >(SEARCH_PRT_CONTACT, {
    client: FEDERATION_CLIENT,
  });

  const [loadFonctions, loadingFonctions] = useLazyQuery<FONCTIONS_TYPE, FONCTIONSVariables>(
    FONCTIONS,
    {
      client: FEDERATION_CLIENT,
    },
  );

  const [loadDepartements, loadingDepartements] = useLazyQuery<
    DEPARTEMENTS_TYPE,
    DEPARTEMENTSVariables
  >(DEPARTEMENTS, {
    client: FEDERATION_CLIENT,
    variables: {
      paging: { offset: 0, limit: 1000 },
      sorting: [
        {
          field: DepartementSortFields.code,
          direction: SortDirection.ASC,
        },
      ],
    },
  });

  /**create */
  const [createContact, creatingContact] = useMutation<
    CREATE_PRT_CONTACT_TYPE,
    CREATE_PRT_CONTACTVariables
  >(CREATE_PRT_CONTACT, {
    onCompleted: () => {
      setPhotoContact(null);
      displaySnackBar(client, {
        message: ' Le contact a été crée avec succés !',
        type: 'SUCCESS',
        isOpen: true,
      });

      loadingContacts.refetch();
      setSaving(false);
      setSaved(true);
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Erreurs lors de la création du Contact.',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  /**update plan Marketing */
  const [updateContact, updatingContact] = useMutation<
    UPDATE_PRT_CONTACT_TYPE,
    UPDATE_PRT_CONTACTVariables
  >(UPDATE_PRT_CONTACT, {
    onCompleted: () => {
      setPhotoContact(null);
      displaySnackBar(client, {
        message: 'Le Contact a été modifié avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });

      loadingContacts.refetch();
      setSaving(false);
      setSaved(true);
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la modification du contact de laboratoire',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  /**delete Plan Marketing */
  const [deleteContact, deletingContact] = useMutation<
    DELETE_PRT_CONTACT_TYPE,
    DELETE_PRT_CONTACTVariables
  >(DELETE_PRT_CONTACT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le Contact a été supprimé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingContacts.refetch();
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression du contact',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  // FICHIERS
  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
  });

  //   civilite: contact.civilite,
  //               nom: contact.nom,
  //               prenom: contact.prenom,
  //               fonction: contact.fonction,
  //               idPartenaireTypeAssocie: contact.idPartenaireTypeAssocie,
  //               partenaireType: contact.partenaireType,
  //               photo: Photo,
  //               contact: contact.contact

  const onRequestContactSearch = () => {
    if (idCurrentLabo) {
      const must: any[] = [
        {
          term: {
            idPartenaireTypeAssocie: idCurrentLabo,
          },
        },
        {
          term: {
            partenaireType: 'LABORATOIRE',
          },
        },
      ];
      let should: any = undefined;
      let sort: any = undefined;

      if (searchTextContact) {
        must.push({
          query_string: {
            query: `*${searchTextContact}*`,
            fields: [],
            analyze_wildcard: true,
          },
        });
      }

      if (filterPolicy === 'PHARMACIE') {
        should = [
          {
            term: {
              idPharmacie: pharmacie.id,
            },
          },
          {
            term: {
              'departements.id': pharmacie.departement.id,
            },
          },
        ];
      } /* else {
        must.push({
          term: {
            idPharmacie: pharmacie.id,
          },
        });
      }*/

      if (sortTable.column) {
        sort = [
          {
            [sortTable.column]: {
              order: sortTable.direction.toLowerCase(),
            },
          },
        ];
      }

      loadContacts({
        variables: {
          index: ['prtcontact'],
          query: {
            query: {
              bool: {
                must,
                minimum_should_match: should ? 1 : undefined,
                // sort: [{ post_date: { order: 'asc' } }],
                should,
              },
            },
          },
        },
      });
    }
  };

  useEffect(() => {
    onRequestContactSearch();
  }, [filterPolicy]);

  const saveContact = (contactInput: any, userPhoto: any) => {
    console.log('detection', userPhoto);
    const contact = contactInput.contact;
    setSaving(true);
    setSaved(false);
    if (contactInput.id) {
      updateContact({
        variables: {
          id: contactInput.id,
          input: {
            civilite: contactInput.civilite,
            idFonction: contactInput.idFonction,
            nom: contactInput.nom,
            prenom: contactInput.prenom,
            idDepartements: contactInput.idDepartements,
            contact: {
              adresse1: contact.adresse1,
              mailPerso: contact.adresseEmail,
              mailProf: contact.adresseEmailProf,
              telProf: contact.bureauTelephone,
              cp: contact.cp,
              telPerso: contact.domicileTelephone,
              telMobPerso: contact.mobileTelephone,
              urlFacebookPerso: contact.facebook,
              urlLinkedInPerso: contact.linkedIn,
              urlMessenger: contact.messenger,
              sitePerso: contact.pageWeb,
              ville: contact.ville,
              whatsappMobPerso: contact.whatsApp,
              urlYoutube: contact.youTube,
            },
            photo: userPhoto ? userPhoto[0] : undefined,
            idPartenaireTypeAssocie: idCurrentLabo,
            partenaireType: 'LABORATOIRE',
          },
        },
      });
    } else {
      createContact({
        variables: {
          input: {
            civilite: contactInput.civilite,
            idFonction: contactInput.idFonction,
            idDepartements: contactInput.idDepartements,
            nom: contactInput.nom,
            prenom: contactInput.prenom,
            contact: {
              adresse1: contact.adresse1,
              mailPerso: contact.adresseEmail,
              mailProf: contact.adresseEmailProf,
              telProf: contact.bureauTelephone,
              cp: contact.cp,
              telPerso: contact.domicileTelephone,
              telMobPerso: contact.mobileTelephone,
              urlFacebookPerso: contact.facebook,
              urlLinkedInPerso: contact.linkedIn,
              urlMessenger: contact.messenger,
              sitePerso: contact.pageWeb,
              ville: contact.ville,
              whatsappMobPerso: contact.whatsApp,
              urlYoutube: contact.youTube,
            },
            photo: userPhoto ? userPhoto[0] : undefined,
            idPartenaireTypeAssocie: idCurrentLabo,
            partenaireType: 'LABORATOIRE',
          },
        },
      });
    }
  };

  const handleSaveContact = (contactInput: any) => {
    console.log('Contact to save', contactInput);

    if (photoContact && photoContact.size) {
      const inputFiles = [photoContact];
      // Presigned file
      const filePathsTab: string[] = [];
      filePathsTab.push(
        `users/photos/${groupement && groupement.id}/${formatFilename(photoContact)}`,
      );
      doCreatePutPresignedUrl({ variables: { filePaths: filePathsTab } }).then(async response => {
        if (response.data?.createPutPresignedUrls) {
          try {
            const fichiers = await Promise.all(
              response.data.createPutPresignedUrls.map((item, index) => {
                if (item?.presignedUrl) {
                  return uploadToS3(inputFiles[index], item?.presignedUrl).then(uploadResult => {
                    if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                      return {
                        chemin: item.filePath,
                        nomOriginal: inputFiles[index].name,
                        type: TypeFichier.PHOTO,
                      };
                    }
                  });
                }
              }),
            );
            saveContact(contactInput, fichiers);
          } catch (error) {}
        } else {
        }
      });
    } else {
      let userPhoto: FichierInput | null = null;
      if (photoContact && photoContact.id) {
        userPhoto = {
          chemin: photoContact.chemin,
          nomOriginal: photoContact.nomOriginal,
          type: TypeFichier.AVATAR,
        };
        saveContact(contactInput, [userPhoto]);
      } else {
        saveContact(contactInput, null);
      }
    }
  };

  // Delete
  const handleDeleteContact = (contact: any) => {
    console.log('contact to delete', contact);
    deleteContact({
      variables: { id: contact.id },
    });
  };

  // Recherche
  const handleContactSearch = ({ skip, take, searchText, sortTable }) => {
    setSkipContact(skip);
    setTakeContact(take);
    setSearchTextContact(searchText);
    setSortTable(sortTable);
  };

  const handleGoBack = () => {
    if (idCurrentLabo) {
      push('/laboratoires');
    } else {
      push('/');
    }
  };

  const contactProps = {
    loading: loadingContacts.loading,
    error: loadingContacts.error as any,
    rowsTotal: 2,
    onRequestSave: handleSaveContact,
    onRequestDelete: handleDeleteContact,
    saving,
    saved,
    setSaved: (newSaved: boolean) => setSaved(newSaved),
    contacts: {
      error: loadingContacts.error as any,
      loading: loadingContacts.loading,
      data: loadingContacts.data?.search?.data || [],
    },
    AvatarComponent,
    civilites: CIVILITE_LIST,
    fonctions: {
      loading: loadingFonctions.loading,
      error: loadingFonctions.error,
      data: loadingFonctions.data?.fonctions.nodes,
    },
    departements: {
      loading: loadingDepartements.loading,
      error: loadingDepartements.error,
      data: (loadingDepartements.data?.departements.nodes || []) as any,
      defaultValue: [pharmacie.departement],
    },

    filterPolicy,
    onChangeFilterPolicy: (p: any) => {
      setFilterPolicy(p);
    },
    onRequestSearch: handleContactSearch,
    onRequestGoBack: handleGoBack,
  };

  return { onRequestContactSearch, contactProps, photoContact, setPhotoContact };
};
