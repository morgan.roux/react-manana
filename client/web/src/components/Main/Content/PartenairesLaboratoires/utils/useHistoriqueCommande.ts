import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import { Historique } from '@app/ui-kit';
import {
  CommandeStatut,
  FilterHistorique,
} from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/Historique/Historique';
import { OnRequestSaveProps } from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/Historique/HistoriqueForm/HistoriqueForm';
import { TotauxFacture } from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/HistoriqueFacture/HistoriqueFacture';
import moment from 'moment';
import { useEffect, useState } from 'react';
import { QueryResult } from 'react-apollo/Query';
import {
  CHANGE_PRT_COMMANDE_STATUT,
  CREATE_PRT_COMMANDE,
  DELETE_PRT_COMMANDE,
  UPDATE_PRT_COMMANDE,
} from '../../../../../federation/partenaire-service/historique-commande/mutation';
import {
  GET_COMMANDES_FOR_DOCUMENT,
  GET_COMMANDES_LIGNES,
  GET_COMMANDE_FOR_DOCUMENT_TOTAL_ROWS,
  GET_HISTORIQUES_COMMADES,
  GET_PRT_COMMANDE_STATUTS,
  GET_ROWS_TOTAL,
  GET_TOTAUX_COMMANDES,
  GET_TOTAUX_COMMANDES_FOR_DOCUMENT,
} from '../../../../../federation/partenaire-service/historique-commande/query';
import {
  CHANGE_PRT_COMMANDE_STATUT as CHANGE_PRT_COMMANDE_STATUT_TYPE,
  CHANGE_PRT_COMMANDE_STATUTVariables,
} from '../../../../../federation/partenaire-service/historique-commande/types/CHANGE_PRT_COMMANDE_STATUT';
import {
  COMMANDES_FOR_DOCUMENT,
  COMMANDES_FOR_DOCUMENTVariables,
} from '../../../../../federation/partenaire-service/historique-commande/types/COMMANDES_FOR_DOCUMENT';
import {
  COMMANDE_FOR_DOCUMENT_TOTAL_ROWS,
  COMMANDE_FOR_DOCUMENT_TOTAL_ROWSVariables,
} from '../../../../../federation/partenaire-service/historique-commande/types/COMMANDE_FOR_DOCUMENT_TOTAL_ROWS';
import {
  CREATE_PRT_COMMANDE as CREATE_PRT_COMMANDE_TYPE,
  CREATE_PRT_COMMANDEVariables,
} from '../../../../../federation/partenaire-service/historique-commande/types/CREATE_PRT_COMMANDE';
import {
  DELETE_PRT_COMMANDE as DELETE_PRT_COMMANDE_TYPE,
  DELETE_PRT_COMMANDEVariables,
} from '../../../../../federation/partenaire-service/historique-commande/types/DELETE_PRT_COMMANDE';
import {
  GET_COMMANDES_LIGNES as GET_COMMANDES_LIGNES_TYPE,
  GET_COMMANDES_LIGNESVariables,
} from '../../../../../federation/partenaire-service/historique-commande/types/GET_COMMANDES_LIGNES';
import {
  GET_HISTORIQUES_COMMADES as GET_HISTORIQUES_COMMADES_TYPE,
  GET_HISTORIQUES_COMMADESVariables,
} from '../../../../../federation/partenaire-service/historique-commande/types/GET_HISTORIQUES_COMMADES';
import {
  GET_ROWS_TOTAL as GET_ROWS_TOTAL_TYPE,
  GET_ROWS_TOTALVariables,
} from '../../../../../federation/partenaire-service/historique-commande/types/GET_ROWS_TOTAL';
import {
  GET_TOTAUX_COMMANDES as GET_TOTAUX_COMMANDES_TYPE,
  GET_TOTAUX_COMMANDESVariables,
} from '../../../../../federation/partenaire-service/historique-commande/types/GET_TOTAUX_COMMANDES';
import {
  PRT_COMMANDE_STATUTS,
  PRT_COMMANDE_STATUTSVariables,
} from '../../../../../federation/partenaire-service/historique-commande/types/PRT_COMMANDE_STATUTS';
import {
  TOTAUX_COMMANDES_FOR_DOCUMENT,
  TOTAUX_COMMANDES_FOR_DOCUMENTVariables,
} from '../../../../../federation/partenaire-service/historique-commande/types/TOTAUX_COMMANDES_FOR_DOCUMENT';
import {
  UPDATE_PRT_COMMANDE as UPDATE_PRT_COMMANDE_TYPE,
  UPDATE_PRT_COMMANDEVariables,
} from '../../../../../federation/partenaire-service/historique-commande/types/UPDATE_PRT_COMMANDE';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { getPharmacie } from '../../../../../services/LocalStorage';
import { uploadToS3 } from '../../../../../services/S3';
import {
  PRTCommandeFilter,
  PRTCommandeSortFields,
  SortDirection,
} from '../../../../../types/federation-global-types';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { formatFilename } from '../../../../Common/Dropzone/Dropzone';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { endTime, startTime } from '../../DemarcheQualite/util';

const COLUMN_FIELD_COMMANDE = {
  referenceCommande: PRTCommandeSortFields.nbrRef,
  dateCommande: PRTCommandeSortFields.dateCommande,
  totalHT: PRTCommandeSortFields.prixNetTotalHT,
  totalTTC: PRTCommandeSortFields.prixNetTotalTTC,
  totalTVA: PRTCommandeSortFields.totalTVA,
};

export interface useHistoriqueCommandeTypes {
  commandes: {
    onRequestSearch: (filtter: FilterHistorique) => void;
    loadingCommandes: QueryResult<GET_HISTORIQUES_COMMADES_TYPE, GET_HISTORIQUES_COMMADESVariables>;
    loadingHistoriqueCommande: boolean;
    errorHistoriqueCommande: Error | undefined;
    dataHistoriqueCommande: Historique[];
    rowsTotal: number;
    totaux?: TotauxFacture;
  };
  ligneCommande: {
    onRequestLignesCommandes: (idCommande: string) => void;
    loadingLignesCommandes: QueryResult<GET_COMMANDES_LIGNES_TYPE, GET_COMMANDES_LIGNESVariables>;
  };
  loading: {
    loadingTotalRowsForDocument: QueryResult<
      COMMANDE_FOR_DOCUMENT_TOTAL_ROWS,
      COMMANDE_FOR_DOCUMENT_TOTAL_ROWSVariables
    >;
    loadingCommandesForDocument: QueryResult<
      COMMANDES_FOR_DOCUMENT,
      COMMANDES_FOR_DOCUMENTVariables
    >;
    loadingTotauxCommandesForDocument: QueryResult<
      TOTAUX_COMMANDES_FOR_DOCUMENT,
      TOTAUX_COMMANDES_FOR_DOCUMENTVariables
    >;
  };

  onRequestSaveCommande?: (historique?: OnRequestSaveProps) => void;
  onRequestDeleteCommande?: (historique: Historique) => void;
  savedCommande?: boolean;
  setSavedCommande?: (newSaved: boolean) => void;
  savingCommande?: boolean;
  onRequestChangeStatut?: (id?: string, statut?: string) => void;
  commandeStatuts?: CommandeStatut[];
  onRequestGoBack?: () => void;
}

export const useHistoriqueCommande = (
  idlabo: string,
  item: string,
  push: any,
): useHistoriqueCommandeTypes => {
  const client = useApolloClient();
  const pharmacie = getPharmacie();

  const [isHistoriqueFacture, setIsHistoriqueFacture] = useState<boolean>(false);
  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);

  useEffect(() => {
    if (item === 'factures') {
      setIsHistoriqueFacture(true);
    } else {
      setIsHistoriqueFacture(false);
    }
  }, [item]);

  const [loadCommandes, loadingCommandes] = useLazyQuery<
    GET_HISTORIQUES_COMMADES_TYPE,
    GET_HISTORIQUES_COMMADESVariables
  >(GET_HISTORIQUES_COMMADES, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
  });

  const [loadTotauxCommande, loadingTotauxCommande] = useLazyQuery<
    GET_TOTAUX_COMMANDES_TYPE,
    GET_TOTAUX_COMMANDESVariables
  >(GET_TOTAUX_COMMANDES, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
    variables: {
      idLaboratoire: idlabo,
      idPharmacie: pharmacie.id,
    },
  });

  useEffect(() => {
    if (idlabo) {
      loadTotauxCommande();
    }
  }, [idlabo, loadTotauxCommande]);

  const [loadTotalRows, loadingTotalRows] = useLazyQuery<
    GET_ROWS_TOTAL_TYPE,
    GET_ROWS_TOTALVariables
  >(GET_ROWS_TOTAL, {
    client: FEDERATION_CLIENT,
  });

  const [loadCommandeStatuts, loadingCommandeStatuts] = useLazyQuery<
    PRT_COMMANDE_STATUTS,
    PRT_COMMANDE_STATUTSVariables
  >(GET_PRT_COMMANDE_STATUTS, {
    client: FEDERATION_CLIENT,
  });

  const [loadLignesCommandes, loadingLignesCommandes] = useLazyQuery<
    GET_COMMANDES_LIGNES_TYPE,
    GET_COMMANDES_LIGNESVariables
  >(GET_COMMANDES_LIGNES, {
    client: FEDERATION_CLIENT,
  });

  const [loadTotalRowsForDocument, loadingTotalRowsForDocument] = useLazyQuery<
    COMMANDE_FOR_DOCUMENT_TOTAL_ROWS,
    COMMANDE_FOR_DOCUMENT_TOTAL_ROWSVariables
  >(GET_COMMANDE_FOR_DOCUMENT_TOTAL_ROWS, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
  });

  const [loadTotauxCommandesForDocument, loadingTotauxCommandesForDocument] = useLazyQuery<
    TOTAUX_COMMANDES_FOR_DOCUMENT,
    TOTAUX_COMMANDES_FOR_DOCUMENTVariables
  >(GET_TOTAUX_COMMANDES_FOR_DOCUMENT, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
  });

  const [loadCommandesForDocument, loadingCommandesForDocument] = useLazyQuery<
    COMMANDES_FOR_DOCUMENT,
    COMMANDES_FOR_DOCUMENTVariables
  >(GET_COMMANDES_FOR_DOCUMENT, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
  });

  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
  });

  const [createCommande] = useMutation<CREATE_PRT_COMMANDE_TYPE, CREATE_PRT_COMMANDEVariables>(
    CREATE_PRT_COMMANDE,
    {
      onCompleted: () => {
        displaySnackBar(client, {
          message: 'La commande a été ajoutée avec succès !',
          type: 'SUCCESS',
          isOpen: true,
        });

        loadingCommandes.refetch();

        loadingTotauxCommandesForDocument.refetch();
        loadingTotauxCommande.refetch();
        setSaving(false);
        setSaved(true);
      },

      onError: () => {
        displaySnackBar(client, {
          message: 'Des erreurs se sont survenues pendant la création de la nouvelle commande',
          type: 'ERROR',
          isOpen: true,
        });

        setSaving(false);
      },
      client: FEDERATION_CLIENT,
    },
  );

  const [updateCommande] = useMutation<UPDATE_PRT_COMMANDE_TYPE, UPDATE_PRT_COMMANDEVariables>(
    UPDATE_PRT_COMMANDE,
    {
      onCompleted: () => {
        displaySnackBar(client, {
          message: 'La commande a été modifiée avec succès !',
          type: 'SUCCESS',
          isOpen: true,
        });

        setSaving(false);
        setSaved(true);
        loadingTotauxCommandesForDocument.refetch();
        loadingTotauxCommande.refetch();
      },

      onError: () => {
        displaySnackBar(client, {
          message: 'Des erreurs se sont survenues pendant la modification de la commande',
          type: 'ERROR',
          isOpen: true,
        });

        setSaving(false);
      },
      client: FEDERATION_CLIENT,
    },
  );

  const [deleteCommande] = useMutation<DELETE_PRT_COMMANDE_TYPE, DELETE_PRT_COMMANDEVariables>(
    DELETE_PRT_COMMANDE,
    {
      onCompleted: () => {
        displaySnackBar(client, {
          message: 'La commande a été supprimée avec succès !',
          type: 'SUCCESS',
          isOpen: true,
        });
        loadingCommandes.refetch();
        loadingTotauxCommandesForDocument.refetch();
        loadingTotauxCommande.refetch();
      },

      onError: () => {
        displaySnackBar(client, {
          message: 'Des erreurs se sont survenues pendant la suppression de la commande',
          type: 'ERROR',
          isOpen: true,
        });
      },
      client: FEDERATION_CLIENT,
    },
  );

  const [changeCommandeStatut] = useMutation<
    CHANGE_PRT_COMMANDE_STATUT_TYPE,
    CHANGE_PRT_COMMANDE_STATUTVariables
  >(CHANGE_PRT_COMMANDE_STATUT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le statut a été modifié avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingCommandes.refetch();
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant le changement du statut',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });
  const handleOnRequestSearch = ({
    skip,
    take,
    searchText,
    filterFacture,
    sorting,
  }: FilterHistorique) => {
    const filterAnd: PRTCommandeFilter[] = [
      {
        idLaboratoire: {
          eq: idlabo,
        },
        idPharmacie: {
          eq: pharmacie.id,
        },
      },
    ];

    if (searchText != '' && moment(searchText, 'DD/MM/YYYY').isValid()) {
      filterAnd.push({
        dateCommande: {
          between: {
            lower: startTime(moment(searchText, 'DD/MM/YYYY').add(1, 'day')),
            upper: endTime(moment(searchText, 'DD/MM/YYYY').add(1, 'day')),
          },
        },
      });
    }

    if (
      searchText !== '' &&
      !moment(searchText, 'DD/MM/YYYY').isValid() &&
      !isNaN(parseInt(searchText))
    ) {
      filterAnd.push({
        nbrRef: {
          eq: parseInt(searchText),
        },
      });
    }

    if (idlabo) {
      loadTotauxCommandesForDocument({
        variables: {
          idTypeAssocie: idlabo,
          //idDocument: filterFacture?.idDocument,
          //searchText,
        },
      });
      if (isHistoriqueFacture) {
        if (filterFacture?.idDocument) {
          loadCommandesForDocument({
            variables: {
              idTypeAssocie: idlabo,
              idDocument: filterFacture.idDocument,
              searchText,
              skip,
              take,
            },
          });
          loadTotalRowsForDocument({
            variables: {
              idTypeAssocie: idlabo,
              idDocument: filterFacture.idDocument,
              searchText,
            },
          });
        }
        // loadTotauxCommandesForDocument({
        //   variables: {
        //     idTypeAssocie: idlabo,
        //     idDocument: filterFacture?.idDocument,
        //     searchText,
        //   },
        // });
      } else {
        if (sorting?.field && sorting?.direction) {
          loadCommandes({
            variables: {
              paging: { offset: skip, limit: take },
              filter: {
                and: filterAnd,
              },
              sorting: [
                {
                  field: COLUMN_FIELD_COMMANDE[sorting.field],
                  direction: sorting.direction === 'ASC' ? SortDirection.ASC : SortDirection.DESC,
                },
              ],
            },
          });
        } else {
          loadCommandes({
            variables: {
              paging: { offset: skip, limit: take },
              filter: {
                and: filterAnd,
              },
            },
          });
        }

        loadTotalRows({
          variables: {
            filter: {
              and: filterAnd,
            },
          },
        });
        loadCommandeStatuts();
      }
    }
  };

  const handleOnRequestLignesCommandes = (idCommande: string) => {
    loadLignesCommandes({
      variables: {
        idCommande,
      },
    });
  };

  const saveCommande = (commande?: OnRequestSaveProps, fichiers?: any[]) => {
    if (commande?.id && idlabo) {
      updateCommande({
        variables: {
          input: {
            id: commande.id,
            update: {
              idLaboratoire: idlabo,
              nbrRef: commande.numeroCommande,
              dateCommande: commande.date,
              prixNetTotalHT: commande.totalHT,
              totalTVA: commande.totalTVA,
              prixNetTotalTTC: commande.totalTTC,
              source: 'PLATEFORME',
              fichiers,
            },
          },
        },
      });
    } else {
      createCommande({
        variables: {
          input: {
            pRTCommande: {
              idLaboratoire: idlabo,
              nbrRef: commande?.numeroCommande,
              dateCommande: commande?.date,
              prixNetTotalHT: commande?.totalHT,
              totalTVA: commande?.totalTVA,
              prixNetTotalTTC: commande?.totalTTC,
              source: 'PLATEFORME',
              fichiers,
            },
          },
        },
      });
    }
  };

  const handleRequestSave = (commande?: OnRequestSaveProps) => {
    setSaving(true);
    setSaved(false);
    if (commande?.fichiers && commande.fichiers.length > 0) {
      const newFiles = commande.fichiers.filter(file => file && file instanceof File);

      if (newFiles?.length > 0) {
        const filePaths = newFiles.map(file => formatFilename(file, ``));
        doCreatePutPresignedUrl({
          variables: {
            filePaths,
          },
        }).then(async response => {
          if (response.data?.createPutPresignedUrls) {
            try {
              const fichiers = await Promise.all(
                response.data.createPutPresignedUrls.map((item, index) => {
                  if (item?.presignedUrl) {
                    return commande.fichiers
                      ? uploadToS3(commande.fichiers[index], item?.presignedUrl).then(
                          uploadResult => {
                            if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                              return {
                                chemin: item.filePath,
                                nomOriginal: commande.fichiers ? commande.fichiers[index].name : '',
                                type: commande.fichiers ? commande.fichiers[index].type : '',
                              };
                            }
                          },
                        )
                      : null;
                  }
                }),
              );
              saveCommande(commande, fichiers);
            } catch (error) {
              setSaving(false);
            }
          } else {
          }
        });
      } else {
        saveCommande(commande, undefined);
      }
    } else {
      saveCommande(commande, undefined);
    }
  };

  const handleRequestDelete = (historique: Historique) => {
    if (historique.id) {
      deleteCommande({
        variables: {
          input: {
            id: historique.id,
          },
        },
      });
    }
  };

  const handleChangeStatut = (id?: string, statut?: string) => {
    if (id && statut) {
      changeCommandeStatut({
        variables: {
          id,
          statut,
        },
      });
    }
  };

  const handleGoBack = () => {
    push('/laboratoires');
  };

  return {
    commandes: {
      onRequestSearch: handleOnRequestSearch,
      loadingCommandes,
      loadingHistoriqueCommande: isHistoriqueFacture
        ? loadingCommandesForDocument?.loading
        : loadingCommandes?.loading,
      errorHistoriqueCommande: isHistoriqueFacture
        ? loadingCommandesForDocument?.error
        : loadingCommandes?.error,
      dataHistoriqueCommande: (
        (isHistoriqueFacture
          ? loadingCommandesForDocument.data?.getCommandeForDocument
          : loadingCommandes.data?.pRTCommandes.nodes) || []
      ).map(commande => ({
        id: commande.id,
        referenceCommande: commande.nbrRef || 0,
        date: commande.dateCommande,
        totalHT: parseFloat(commande.prixNetTotalHT?.toFixed(4) || '0'),
        totalTTC: parseFloat((commande.prixNetTotalTTC || 0).toFixed(4)),
        totalTVA: parseFloat((commande.totalTVA || 0).toFixed(4)),
        status: commande.statut || '',
        source: commande.source || '',
      })),
      rowsTotal: isHistoriqueFacture
        ? loadingTotalRowsForDocument.data?.getCommandeForDocumentRowsTotal || 0
        : (loadingTotalRows.data?.pRTCommandeAggregate.length || 0) > 0
        ? loadingTotalRows.data?.pRTCommandeAggregate[0].count?.id || 0
        : 0,
      totaux: isHistoriqueFacture
        ? {
            totalHT:
              loadingTotauxCommandesForDocument.data?.getCommandesForDocumentTotaux?.totalHT || 0,
            totalTVA:
              loadingTotauxCommandesForDocument.data?.getCommandesForDocumentTotaux?.totalTVA || 0,
            totalTTC:
              loadingTotauxCommandesForDocument.data?.getCommandesForDocumentTotaux?.totalTTC || 0,
          }
        : {
            totalHT: parseFloat(
              parseFloat(loadingTotauxCommande.data?.getTotauxCommandes?.totalHT || '0').toFixed(4),
            ),
            totalTVA: parseFloat(
              parseFloat(loadingTotauxCommande.data?.getTotauxCommandes?.totalTVA || '0').toFixed(
                4,
              ),
            ),
            totalTTC: parseFloat(
              parseFloat(loadingTotauxCommande.data?.getTotauxCommandes?.totalTTC || '0').toFixed(
                4,
              ),
            ),
          },
    },
    ligneCommande: {
      onRequestLignesCommandes: handleOnRequestLignesCommandes,
      loadingLignesCommandes: loadingLignesCommandes,
    },
    loading: {
      loadingCommandesForDocument,
      loadingTotalRowsForDocument,
      loadingTotauxCommandesForDocument,
    },
    onRequestSaveCommande: handleRequestSave,
    onRequestDeleteCommande: handleRequestDelete,
    savedCommande: saved,
    setSavedCommande: setSaved,
    savingCommande: saving,
    onRequestChangeStatut: handleChangeStatut,
    commandeStatuts: loadingCommandeStatuts.data?.pRTCommandeStatuts.nodes,
    onRequestGoBack: handleGoBack,
  };
};
