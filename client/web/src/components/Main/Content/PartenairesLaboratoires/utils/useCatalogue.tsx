import React from 'react';
import {
  GET_SEARCH_CUSTOM_CONTENT_PRODUIT,
  GET_SEARCH_PRODUIT_CANAL,
} from '../../../../../graphql/ProduitCanal/query';
import WithSearch from '../../../../Common/newWithSearch/withSearch';
import Produit from '../../Produits';

export const useCatalogue = (idCurrentLabo: string, view: 'list' | 'card') => {
  if (idCurrentLabo) {
    return [
      <WithSearch
        type="produitcanal"
        WrappedComponent={Produit}
        searchQuery={
          view && view === 'list' ? GET_SEARCH_CUSTOM_CONTENT_PRODUIT : GET_SEARCH_PRODUIT_CANAL
        }
        defaultSort={[{ 'produit.libelle': { order: 'asc' } }]}
        optionalMust={[
          { term: { isRemoved: false } },
          { term: { isActive: true } },
          {
            terms: {
              'produit.produitTechReg.laboExploitant.id': [idCurrentLabo],
            },
          },
        ]}
      />,
    ];
  } else {
    return [<div></div>];
  }
};
