import React from 'react';
import { AvatarGroup } from '@material-ui/lab';
import Avatar from '@material-ui/core/Avatar';
import moment from 'moment';
import { Column } from '@app/ui-kit';

export const suiviOperationnelColumn: Column[] = [
  {
    name: 'type',
    label: 'Type',
    renderer: (row: any) => {
      return row.type ? row.type.libelle : '';
    },
  },
  {
    name: 'titre',
    label: 'Titre',
    renderer: (row: any) => {
      return row.titre ? row.titre : '';
    },
  },
  {
    name: 'collaborateurs',
    label: 'collaborateurs',
    renderer: (row: any) => {
      return row.participants && row.participants.length > 0 ? (
        <AvatarGroup max={4}>
          {row.participants.map(user => (
            <Avatar
              key={user.id}
              alt={(user as any).fullName}
              src={user.photo?.publicUrl || user.photo?.avatar?.fichier?.publicUrl}
            />
          ))}
        </AvatarGroup>
      ) : (
        ''
      );
    },
  },
  {
    name: 'reponsable',
    label: 'Responsable',
    renderer: (row: any) => {
      return row.contacts && row.contacts.length > 0 ? (
        <AvatarGroup max={4}>
          {row.contacts.map(responsable => (
            <Avatar
              key={responsable.id}
              alt={responsable.nom}
              src={
                (responsable as any).photo?.publicUrl ||
                (responsable as any).photo?.avatar?.fichier?.publicUrl
              }
            />
          ))}
        </AvatarGroup>
      ) : (
        ''
      );
    },
  },
  {
    name: 'date',
    label: 'Date',
    renderer: (row: any) => {
      return row.dateHeure ? moment(row.dateHeure) : '';
    },
  },
];
