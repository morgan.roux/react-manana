import React from 'react';
import { GET_SEARCH_ACTUALITE } from '../../../../../graphql/Actualite/query';
import WithSearch from '../../../../Common/newWithSearch/withSearch';
import { Actualite } from '../../Actualite';

export const useActualite = (idCurrentLabo: string) => {
  if (idCurrentLabo) {
    return [
      <WithSearch
        type="actualite"
        WrappedComponent={Actualite}
        searchQuery={GET_SEARCH_ACTUALITE}
        optionalMust={[
          { term: { isRemoved: false } },
          {
            terms: {
              'laboratoiresCible.id': [idCurrentLabo],
            },
          },
        ]}
      />,
    ];
  } else {
    return [<div></div>];
  }
};
