import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import { Box } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import {
  GET_DOCUMENTS,
  GET_DOCUMENTS_CATEGORIE,
  GET_DOCUMENT_CATEGORIE,
} from '../../../../../../federation/ged/document/query';
import {
  GET_DOCUMENTS as GET_DOCUMENTS_TYPE,
  GET_DOCUMENTSVariables,
} from '../../../../../../federation/ged/document/types/GET_DOCUMENTS';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { DocumentCategorie, IDocumentSearch } from '../../../EspaceDocumentaire/EspaceDocumentaire';
import { MainContainer } from '../../../EspaceDocumentaire/MainContainer';
import {
  CREATE_DOCUMENT_CATEGORIE,
  DELETE_DOCUMENT_CATEGORIE,
  // DELETE_DOCUMENT_CATEGORIE,
  TOGGLE_DOCUMENT_TO_FAVORITE,
  UPDATE_DOCUMENT_CATEGORIE,
} from '../../../../../../federation/ged/document/mutation';
import {
  TOGGLE_DOCUMENT_TO_FAVORITE as TOGGLE_DOCUMENT_TO_FAVORITE_TYPE,
  TOGGLE_DOCUMENT_TO_FAVORITEVariables,
} from '../../../../../../federation/ged/document/types/TOGGLE_DOCUMENT_TO_FAVORITE';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import { useStyles } from './styles';
import { CREATE_ACTIVITE } from '../../../../../../federation/basis/mutation';
import {
  CREATE_ACTIVITE as CREATE_ACTIVITE_TYPE,
  CREATE_ACTIVITEVariables,
} from '../../../../../../federation/basis/types/CREATE_ACTIVITE';
import {
  GET_DOCUMENT_CATEGORIE as GET_DOCUMENT_CATEGORIE_TYPE,
  GET_DOCUMENT_CATEGORIEVariables,
} from '../../../../../../federation/ged/document/types/GET_DOCUMENT_CATEGORIE';
import { COMMENTS, COMMENTSVariables } from '../../../../../../graphql/Comment/types/COMMENTS';
import { GET_COMMENTS } from '../../../../../../graphql/Comment/query';
// import { GET_TVAS } from '../../../../../../graphql/TVA/query';
// import { TVAS } from '../../../../../../graphql/TVA/types/TVAS';
import {
  USER_SMYLEYS,
  USER_SMYLEYSVariables,
} from '../../../../../../graphql/Smyley/types/USER_SMYLEYS';
import { GET_USER_SMYLEYS } from '../../../../../../graphql/Smyley/query';
import { SMYLEYS_smyleys } from '../../../../../../graphql/Smyley/types/SMYLEYS';
import useOrigines from '../../../../../hooks/basis/useOrigines';
import { getPharmacie } from '../../../../../../services/LocalStorage';
import { ContratPageProps } from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/Contrat';
import { formatFilename } from '../../../../../Common/Dropzone/Dropzone';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../../graphql/S3';
import { uploadToS3 } from '../../../../../../services/S3';
import {
  CREATE_DOCUMENT_CATEGORIE as CREATE_DOCUMENT_CATEGORIE_TYPE,
  CREATE_DOCUMENT_CATEGORIEVariables,
} from '../../../../../../federation/ged/document/types/CREATE_DOCUMENT_CATEGORIE';
import {
  UPDATE_DOCUMENT_CATEGORIE as UPDATE_DOCUMENT_CATEGORIE_TYPE,
  UPDATE_DOCUMENT_CATEGORIEVariables,
} from '../../../../../../federation/ged/document/types/UPDATE_DOCUMENT_CATEGORIE';
import {
  DELETE_DOCUMENT_CATEGORIE as DELETE_DOCUMENT_CATEGORIE_TYPE,
  DELETE_DOCUMENT_CATEGORIEVariables,
} from '../../../../../../federation/ged/document/types/DELETE_DOCUMENT_CATEGORIE';
import {
  GET_DOCUMENTS_CATEGORIE as GET_DOCUMENTS_CATEGORIE_TYPE,
  GET_DOCUMENTS_CATEGORIEVariables,
} from '../../../../../../federation/ged/document/types/GET_DOCUMENTS_CATEGORIE';
// import { DELETE_DOCUMENT_CATEGORIE as DELETE_DOCUMENT_CATEGORIE_TYPE, DELETE_DOCUMENT_CATEGORIEVariables } from '../../../../../../federation/ged/document/types/DELETE_DOCUMENT_CATEGORIE';

export interface useContratProps {
  idCurrentLabo?: string;
  push?: (url: string) => void;
  idDocument?: string;
  defaultCorrespondant?: any;
}

export const useContrat = ({
  idCurrentLabo,
  push,
  idDocument,
  defaultCorrespondant,
}: useContratProps): ContratPageProps => {
  const classes = useStyles();

  const client = useApolloClient();
  const pharmacie = getPharmacie();
  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  // const [mode, setMode] = useState<'creation' | 'remplacement'>('creation');
  const [searchParameters, setSearchParameters] = useState<IDocumentSearch>({ type: 'CONTRAT' });

  const origines = useOrigines();
  const idOrigineLaboratoire = (origines.data?.origines.nodes || []).find(
    ({ code }) => code === 'LABORATOIRE',
  )?.id;

  useEffect(() => {
    setSearchParameters(prev => ({ ...prev, idOrigine: idOrigineLaboratoire }));
  }, [idOrigineLaboratoire]);

  useEffect(() => {
    if (idDocument) {
      getDoc(idDocument);
    }
  }, [idDocument]);

  useEffect(() => {
    if (idCurrentLabo && idOrigineLaboratoire) {
      getDocuments({
        variables: {
          filter: {
            and: [
              {
                idOrigine: {
                  eq: idOrigineLaboratoire,
                },
              },
              {
                idOrigineAssocie: {
                  eq: idCurrentLabo,
                },
              },
              {
                idPharmacie: {
                  eq: pharmacie.id,
                },
              },
              {
                type: {
                  eq: 'CONTRAT',
                },
              },
            ],
          },
        },
      });
    }
  }, [idCurrentLabo, idOrigineLaboratoire]);

  const [getDocuments, gettingDocuments] = useLazyQuery<GET_DOCUMENTS_TYPE, GET_DOCUMENTSVariables>(
    GET_DOCUMENTS,
    {
      fetchPolicy: 'cache-and-network',
      client: FEDERATION_CLIENT,
    },
  );

  const [
    toggleDocumentFavorite,
    { loading: toggleLoading, error: toggleError, data: toggle },
  ] = useMutation<TOGGLE_DOCUMENT_TO_FAVORITE_TYPE, TOGGLE_DOCUMENT_TO_FAVORITEVariables>(
    TOGGLE_DOCUMENT_TO_FAVORITE,
    {
      onError: () => {
        displaySnackBar(client, {
          message: "Des erreurs se sont survenues pendant l'ajout du document aux favoris!",
          type: 'ERROR',
          isOpen: true,
        });
      },

      client: FEDERATION_CLIENT,
    },
  );

  const [
    getDocument,
    { loading: documentLoading, error: documentError, data: document, refetch: refetchDocument },
  ] = useLazyQuery<GET_DOCUMENT_CATEGORIE_TYPE, GET_DOCUMENT_CATEGORIEVariables>(
    GET_DOCUMENT_CATEGORIE,
    {
      client: FEDERATION_CLIENT,
      fetchPolicy: 'network-only',
    },
  );

  const [addActivite] = useMutation<CREATE_ACTIVITE_TYPE, CREATE_ACTIVITEVariables>(
    CREATE_ACTIVITE,
    {
      client: FEDERATION_CLIENT,
      onCompleted: () => {
        refetchDocument();
      },
    },
  );

  const [
    getCommentaires,
    { loading: commentsLoading, error: commentsError, data: comments, refetch: refetchComments },
  ] = useLazyQuery<COMMENTS, COMMENTSVariables>(GET_COMMENTS);

  const [loadUserSmyleys, loadingUserSmyleys] = useLazyQuery<USER_SMYLEYS, USER_SMYLEYSVariables>(
    GET_USER_SMYLEYS,
  );

  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
  });

  const [addDocumentCategorie] = useMutation<
    CREATE_DOCUMENT_CATEGORIE_TYPE,
    CREATE_DOCUMENT_CATEGORIEVariables
  >(CREATE_DOCUMENT_CATEGORIE, {
    onCompleted: data => {
      setSaving(false);
      setSaved(true);

      gettingDocuments.refetch();
      displaySnackBar(client, {
        message: 'Le contrat a été ajoutée avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs se sont survenues pendant l'ajout du contrat!",
        type: 'ERROR',
        isOpen: true,
      });

      setSaving(false);
    },
    client: FEDERATION_CLIENT,
  });

  const [updateDocumentCategorie] = useMutation<
    UPDATE_DOCUMENT_CATEGORIE_TYPE,
    UPDATE_DOCUMENT_CATEGORIEVariables
  >(UPDATE_DOCUMENT_CATEGORIE, {
    onCompleted: () => {
      setSaving(false);
      setSaved(true);

      gettingDocuments.refetch();
      displaySnackBar(client, {
        message: 'La Facture a été modifiée avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la modification de la Facture!',
        type: 'ERROR',
        isOpen: true,
      });
      setSaving(false);
    },
    client: FEDERATION_CLIENT,
  });

  const [deleteDocumentCategorie] = useMutation<
    DELETE_DOCUMENT_CATEGORIE_TYPE,
    DELETE_DOCUMENT_CATEGORIEVariables
  >(DELETE_DOCUMENT_CATEGORIE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le document a été supprimé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });

      handleRefetchAll();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression du document !',
        type: 'ERROR',
        isOpen: true,
      });
    },
    update: (cache, deletionResult) => {
      if (deletionResult?.data?.deleteOneGedDocument?.id) {
        const previousList = cache.readQuery<
          GET_DOCUMENTS_CATEGORIE_TYPE,
          GET_DOCUMENTS_CATEGORIEVariables
        >({
          query: GET_DOCUMENTS_CATEGORIE,
        })?.gedDocuments;

        const deletedId = deletionResult.data.deleteOneGedDocument.id;

        cache.writeData<GET_DOCUMENTS_CATEGORIE_TYPE>({
          data: {
            gedDocuments: {
              ...(previousList || {}),
              nodes: (previousList?.nodes || []).filter(item => item?.id !== deletedId),
            } as any,
          },
        });
      }
    },
    client: FEDERATION_CLIENT,
  });

  const getDoc = (id: string) => {
    getDocument({
      variables: {
        id: id,
      },
    });

    addActivite({
      variables: {
        input: {
          activiteTypeCode: 'VISIT',
          itemCode: 'GED_DOCUMENT',
          idItemAssocie: id,
        },
      },
    });

    getCommentaires({
      variables: {
        codeItem: 'GED_DOCUMENT',
        idItemAssocie: id,
      },
    });
  };

  const showError = () => {
    displaySnackBar(client, {
      message: `Une erreur est survenue pendant le chargement du document`,
      type: 'ERROR',
      isOpen: true,
    });
  };

  useEffect(() => {
    if (toggle?.toggleToGedDocumentFavoris && !toggleLoading && !toggleError) {
      displaySnackBar(client, {
        message: `Le document a été ${
          toggle?.toggleToGedDocumentFavoris?.favoris ? 'ajouté aux' : 'retiré des'
        } favoris!`,
        type: 'SUCCESS',
        isOpen: true,
      });
    }
  }, [toggle, toggleLoading, toggleError]);

  const upsertDocument = ({
    id,
    idSousCategorie,
    description,
    nomenclature,
    numeroVersion,
    idUserVerificateur,
    idUserRedacteur,
    dateHeureParution,
    dateHeureDebutValidite,
    dateHeureFinValidite,
    motCle1,
    motCle2,
    motCle3,
    fichier,
    idOrigine,
    idOrigineAssocie,
    dateFacture,
    hT,
    tva,
    ttc,
    type,

    numeroFacture,
    factureDateReglement,
    idReglementMode,
    numeroCommande,
    isGenererCommande,
    avoirType,
    avoirCorrespondants,
    nombreJoursPreavis,
    isRenouvellementTacite,
    isMultipleTva,
    factureTvas,
  }): void => {
    if (!id) {
      // setMode('creation');
      addDocumentCategorie({
        variables: {
          input: {
            idSousCategorie: idSousCategorie,
            description: description,
            nomenclature: nomenclature,
            numeroVersion: numeroVersion,
            dateHeureParution: new Date(dateHeureParution),
            dateHeureDebutValidite: new Date(dateHeureDebutValidite),
            dateHeureFinValidite: new Date(dateHeureFinValidite),
            idUserVerificateur: idUserVerificateur,
            idUserRedacteur: idUserRedacteur,
            motCle1: motCle1,
            motCle2: motCle2,
            motCle3: motCle3,
            fichier: fichier,
            idOrigine: idOrigine,
            idOrigineAssocie: idOrigineAssocie,
            factureDate: dateFacture,
            factureTotalHt: hT,
            factureTva: tva,
            factureTotalTtc: ttc,
            type: type,
            statut: 'APPROUVE',

            numeroFacture,
            factureDateReglement,
            idReglementMode,
            numeroCommande,
            isGenererCommande,
            avoirType,
            avoirCorrespondants,
            nombreJoursPreavis,
            isRenouvellementTacite,
            isMultipleTva,
            facturesTva: factureTvas,
          },
        },
      }).then(() => {
        gettingDocuments.refetch();
      });
    } else {
      // setMode('remplacement');
      updateDocumentCategorie({
        variables: {
          input: {
            description: description,
            nomenclature: nomenclature,
            numeroVersion: numeroVersion,
            dateHeureParution: dateHeureParution,
            dateHeureDebutValidite: dateHeureDebutValidite,
            dateHeureFinValidite: dateHeureFinValidite,
            idSousCategorie: idSousCategorie,
            idUserVerificateur: idUserVerificateur,
            idUserRedacteur: idUserRedacteur,
            motCle1: motCle1,
            motCle2: motCle2,
            motCle3: motCle3,
            fichier: fichier,
            idOrigine: idOrigine,
            idOrigineAssocie: idOrigineAssocie,
            factureDate: dateFacture,
            factureTotalHt: hT,
            factureTva: tva,
            factureTotalTtc: ttc,
            type: type,
            statut: 'APPROUVE',

            numeroFacture,
            factureDateReglement,
            idReglementMode,
            numeroCommande,
            isGenererCommande,
            avoirType,
            avoirCorrespondants,
            nombreJoursPreavis,
            isRenouvellementTacite,
            isMultipleTva,
            facturesTva: factureTvas,
          },
          id: id,
        },
      }).then(() => {
        gettingDocuments.refetch();
      });
    }
  };

  const handleRefecthSmyleys = () => {
    loadingUserSmyleys.refetch();
  };

  const handleDetailsDocument = (document: any): void => {
    push && push(`/laboratoires/${idCurrentLabo}/contrat/${document.id}`);
  };

  const handleDeplace = (document: DocumentCategorie): void => {};

  const handleFetchMoreComments = (document: DocumentCategorie): void => {};

  const handleRefetchAll = () => {
    gettingDocuments.refetch();
  };

  const handleToggleDocumentToOrFromFavorite = (document: DocumentCategorie): void => {
    toggleDocumentFavorite({
      variables: {
        idDocument: document?.id as any,
      },
    });
  };

  const handleDeleteDocument = (document: DocumentCategorie): void => {
    console.log('delete contrat');
    deleteDocumentCategorie({
      variables: {
        input: {
          id: document?.id as any,
        },
      },
    });
  };

  const handleScroll = (): void => {
    const fetchMore = gettingDocuments.fetchMore;
    const variables = gettingDocuments.variables;
    const paging = variables.paging;
    if (gettingDocuments.data?.gedDocuments.nodes) {
      fetchMore({
        variables: {
          ...variables,
          paging: {
            ...paging,
            offset: gettingDocuments.data?.gedDocuments.nodes,
          },
        },

        updateQuery: (prev: any, { fetchMoreResult }) => {
          if (prev?.gedDocuments && fetchMoreResult?.gedDocuments.nodes) {
            return {
              ...prev,
              gedDocuments: {
                ...prev.gedDocuments,
                nodes: [...prev.gedDocuments.nodes, ...fetchMoreResult.gedDocuments.nodes],
              },
            };
          }

          return prev;
        },
      });
    }
  };

  const handleRefresh = (): void => {
    gettingDocuments.refetch();
    push && push(`/laboratoires/${idCurrentLabo}/contrat`);
  };

  const handleOnRequestDocument = (id: string) => {
    getDoc(id);
  };

  const handleCompleteDownload = (document: DocumentCategorie): void => {
    addActivite({
      variables: {
        input: {
          activiteTypeCode: 'DOWNLOAD',
          itemCode: 'GED_DOCUMENT',
          idItemAssocie: document.id as any,
        },
      },
    });
  };

  const handleGoBack = () => {
    push && push(`/laboratoires`);
  };

  const handleReplaceDocument = (doc: any): void => {
    setSaving(true);
    setSaved(false);
    if (doc.launchUpload) {
      const filePaths = [doc.selectedFile].map(fichier => formatFilename(fichier, ``));
      doCreatePutPresignedUrl({
        variables: {
          filePaths,
        },
      })
        .then(async response => {
          if (response.data?.createPutPresignedUrls) {
            try {
              const presignedUrl = response.data.createPutPresignedUrls[0];
              let fichier: any = undefined;
              if (presignedUrl) {
                try {
                  const uploadResult = await uploadToS3(
                    doc.selectedFile,
                    presignedUrl.presignedUrl,
                  );
                  if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                    fichier = {
                      chemin: presignedUrl.filePath,
                      nomOriginal: doc.selectedFile.name,
                      type: doc.selectedFile.type,
                    };
                  }
                } catch (error) {
                  showError();
                  return;
                }
              }
              upsertDocument({
                id: doc.id,
                idSousCategorie: doc.idSousCategorie,
                description: doc.description,
                nomenclature: doc.nomenclature,
                numeroVersion: doc.numeroVersion,
                idUserVerificateur: doc.idUserVerificateur,
                idUserRedacteur: doc.idUserRedacteur,
                dateHeureParution: doc.dateHeureParution,
                dateHeureDebutValidite: doc.dateHeureDebutValidite,
                dateHeureFinValidite: doc.dateHeureFinValidite,
                motCle1: doc.motCle1,
                motCle2: doc.motCle2,
                motCle3: doc.motCle3,
                fichier: fichier,
                idOrigine: doc?.origine?.id || doc?.origine,
                idOrigineAssocie: doc.correspondant.id,
                dateFacture: doc.dateFacture,
                hT: doc.hT ? parseFloat(doc.hT) : undefined,
                tva: doc.tva ? parseFloat(doc.tva) : undefined,
                ttc: doc.ttc ? parseFloat(doc.ttc) : undefined,
                type: doc.type,

                numeroFacture: doc?.numeroFacture,
                factureDateReglement: doc?.factureDateReglement,
                idReglementMode: doc?.idReglementMode,
                numeroCommande: doc?.numeroCommande,
                isGenererCommande: doc?.isGenererCommande,
                avoirType: doc?.avoirType,
                avoirCorrespondants: doc?.avoirCorrespondants,
                nombreJoursPreavis: doc?.nombreJoursPreavis,
                isRenouvellementTacite: doc?.isRenouvellementTacite,
                isMultipleTva: doc?.isMultipleTva,
                factureTvas: doc?.factureTvas,
              });
              return;
            } catch (error) {
              showError();
              setSaving(false);
            }
          }
        })
        .catch(() => showError());
    } else {
      const { nomOriginal, chemin, type } = doc.previousFichier || {};

      upsertDocument({
        id: doc.id,
        idSousCategorie: doc.idSousCategorie,
        description: doc.description,
        nomenclature: doc.nomenclature,
        numeroVersion: doc.numeroVersion,
        idUserVerificateur: doc.idUserVerificateur,
        idUserRedacteur: doc.idUserRedacteur,
        dateHeureParution: doc.dateHeureParution,
        dateHeureDebutValidite: doc.dateHeureDebutValidite,
        dateHeureFinValidite: doc.dateHeureFinValidite,
        motCle1: doc.motCle1,
        motCle2: doc.motCle2,
        motCle3: doc.motCle3,
        fichier: doc.previousFichier ? { nomOriginal, chemin, type } : undefined,
        idOrigine: doc.origine?.id,
        idOrigineAssocie: doc.correspondant?.id,
        dateFacture: 'FACTURE' === doc.type ? doc.dateFacture : undefined,
        hT: 'FACTURE' === doc.type && doc.hT ? parseFloat(doc.hT) : undefined,
        tva: 'FACTURE' === doc.type && doc.tva ? parseFloat(doc.tva) : undefined,
        ttc: 'FACTURE' === doc.type && doc.ttc ? parseFloat(doc.ttc) : undefined,
        type: doc.type,

        numeroFacture: doc?.numeroFacture,
        factureDateReglement: doc?.factureDateReglement,
        idReglementMode: doc?.idReglementMode,
        numeroCommande: doc?.numeroCommande,
        isGenererCommande: doc?.isGenererCommande,
        avoirType: doc?.avoirType,
        avoirCorrespondants: doc?.avoirCorrespondants,
        nombreJoursPreavis: doc?.nombreJoursPreavis,
        isRenouvellementTacite: doc?.isRenouvellementTacite,
        isMultipleTva: doc?.isMultipleTva,
        factureTvas: doc?.factureTvas,
      });
    }
  };

  console.log('active document', document?.gedDocument);

  return {
    contratComponent: (
      <Box className={classes.container}>
        <MainContainer
          sideBarProps={{
            saving: saving,
            saved: saved,
            setSaved: setSaved,
            activeSousCategorie: undefined,
            documents: {
              loading: gettingDocuments.loading,
              error: gettingDocuments.error as any,
              data: gettingDocuments.data?.gedDocuments.nodes || ([] as any),
              total: gettingDocuments.data?.gedDocuments.totalCount || 0,
            },
            searchVariables: searchParameters,
            setActiveDocument: handleDetailsDocument,
            onRequestDeplace: handleDeplace,
            onRequestAddToFavorite: handleToggleDocumentToOrFromFavorite,
            onRequestDelete: handleDeleteDocument,
            onRequestDetails: handleDetailsDocument,
            onRequestRemoveFromFavorite: handleToggleDocumentToOrFromFavorite,
            onRequestReplace: handleReplaceDocument,
            onRequestScroll: handleScroll,
            onRequestGetComments: () => {},
            setSearchVariables: setSearchParameters,
            onRequestCreateDocument: handleReplaceDocument,
            onRequestRefresh: handleRefresh,
            document: document?.gedDocument as any,
            espaceType: 'espace-documentaire',
            tvas: {
              loading: false,
              error: false as any,
              data: [],
            },
            defaultCorrespondant,
          }}
          contentProps={{
            saving: saving,
            saved: saved,
            setSaved: setSaved,
            activeSousCategorie: undefined,
            activeDocument: document?.gedDocument,
            mobileView: window.innerWidth < 1024,
            tabletView: window.innerWidth < 1366,
            validation: true,
            content: {
              loading: documentLoading,
              error: documentError as any,
              data: document?.gedDocument as any,
            },
            commentaires: {
              loading: commentsLoading,
              error: commentsError as any,
              data: comments?.comments as any,
            },
            onRequestDocument: handleOnRequestDocument,
            refetchComments: refetchComments,
            handleSidebarMenu: () => {},
            handleFilterMenu: () => {},
            onRequestCreateDocument: handleReplaceDocument,
            onRequestAddToFavorite: handleToggleDocumentToOrFromFavorite,
            onRequestDelete: handleDeleteDocument,
            onRequestRemoveFromFavorite: handleToggleDocumentToOrFromFavorite,
            onRequestReplace: handleReplaceDocument,
            onRequestFetchMoreComments: handleFetchMoreComments,
            onRequestLike: (document: DocumentCategorie, smyley: SMYLEYS_smyleys | null) => {},
            onRequestDeplace: handleDeplace,
            onCompleteDownload: handleCompleteDownload,
            refetchSmyleys: handleRefecthSmyleys,
            refetchDocument: refetchDocument,
            onRequestFetchAll: handleRefetchAll,
            setSearchVariables: setSearchParameters,
            searchVariables: searchParameters,
            tvas: {
              loading: false,
              error: false as any,
              data: [],
            },
          }}
        />
      </Box>
    ),
    onRequestGoBack: handleGoBack,
  };
};
