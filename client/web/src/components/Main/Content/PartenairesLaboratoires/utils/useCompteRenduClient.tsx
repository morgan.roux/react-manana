import React, { useCallback, useEffect, useState } from 'react';
import { useLazyQuery, useMutation, useApolloClient } from '@apollo/react-hooks';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import {
  PRT_COMPTE_RENDUS as PRT_COMPTE_RENDUS_TYPE,
  PRT_COMPTE_RENDUSVariables,
} from '../../../../../federation/partenaire-service/compte-rendu/types/PRT_COMPTE_RENDUS';
import {
  PRT_COMPTE_RENDU_AGGREGATES as PRT_COMPTE_RENDU_AGGREGATES_TYPE,
  PRT_COMPTE_RENDU_AGGREGATESVariables,
} from '../../../../../federation/partenaire-service/compte-rendu/types/PRT_COMPTE_RENDU_AGGREGATES';
import {
  GET_PRT_COMPTE_RENDUS,
  GET_PRT_COMPTE_RENDU_AGGREGATES,
  GET_PRT_CONDITION_COMMERCIALE_NON_RATTACHE,
  GET_PRT_PLAN_MARKETING_NON_RATTACHE,
} from '../../../../../federation/partenaire-service/compte-rendu/query';
import {
  DELETE_ONE_PRT_COMPTE_RENDU as DELETE_ONE_PRT_COMPTE_RENDU_TYPE,
  DELETE_ONE_PRT_COMPTE_RENDUVariables,
} from '../../../../../federation/partenaire-service/compte-rendu/types/DELETE_ONE_PRT_COMPTE_RENDU';
import {
  DELETE_ONE_PRT_COMPTE_RENDU,
  UPDATE_PRT_COMPTE_RENDU,
  CREATE_PRT_COMPTE_RENDU,
  UPDATE_ONE_PRT_COMPTE_RENDU_NOTIFICATION_lOGS,
} from '../../../../../federation/partenaire-service/compte-rendu/mutation';
import {
  UPDATE_PRT_COMPTE_RENDU_NOTIFICATION_LOGS as UPDATE_PRT_COMPTE_RENDU_NOTIFICATION_lOGS_TYPE,
  UPDATE_PRT_COMPTE_RENDU_NOTIFICATION_LOGSVariables,
} from './../../../../../federation/partenaire-service/compte-rendu/types/UPDATE_PRT_COMPTE_RENDU_NOTIFICATION_LOGS';
import {
  UPDATE_PRT_COMPTE_RENDU as UPDATE_PRT_COMPTE_RENDU_TYPE,
  UPDATE_PRT_COMPTE_RENDUVariables,
} from '../../../../../federation/partenaire-service/compte-rendu/types/UPDATE_PRT_COMPTE_RENDU';
import {
  CREATE_PRT_COMPTE_RENDU as CREATE_PRT_COMPTE_RENDU_TYPE,
  CREATE_PRT_COMPTE_RENDUVariables,
} from '../../../../../federation/partenaire-service/compte-rendu/types/CREATE_PRT_COMPTE_RENDU';
import {
  GET_RENDEZ_VOUS_SUJETS_VISITES as GET_RENDEZ_VOUS_SUJETS_VISITES_TYPE,
  GET_RENDEZ_VOUS_SUJETS_VISITESVariables,
} from '../../../../../federation/partenaire-service/rendez-vous/rendez-vous-sujet-visite/types/GET_RENDEZ_VOUS_SUJETS_VISITES';
import { GET_RENDEZ_VOUS_SUJETS_VISITES } from '../../../../../federation/partenaire-service/rendez-vous/rendez-vous-sujet-visite/query';
import { CompteRenduComponentProps } from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/CompteRendu';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import UserInput from '../../../../Common/UserInput';
import {
  GET_LIST_PLAN_MARKETING_TYPES,
  GET_LIST_MISE_AVANT,
} from '../../../../../federation/partenaire-service/planMarketing/query';
import {
  GET_LIST_PLAN_MARKETING_TYPES as GET_LIST_PLAN_MARKETING_TYPES_TYPE,
  GET_LIST_PLAN_MARKETING_TYPESVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/GET_LIST_PLAN_MARKETING_TYPES';
import {
  LIST_MISE_AVANT,
  LIST_MISE_AVANTVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/LIST_MISE_AVANT';
import { GET_TYPES_CONDITION } from '../../../../../federation/partenaire-service/laboratoire/condition/type/query';
import {
  GET_TYPES_CONDITION as GET_TYPES_CONDITION_TYPE,
  GET_TYPES_CONDITIONVariables,
} from '../../../../../federation/partenaire-service/laboratoire/condition/type/types/GET_TYPES_CONDITION';
import { getGroupement, getPharmacie, getUser } from '../../../../../services/LocalStorage';
import {
  CompteRenduFormOuptput,
  CompteRenduFormProps,
} from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/CompteRendu/CompteRenduForm/interface';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { GET_RENDEZ_VOUS } from '../../../../../federation/partenaire-service/rendez-vous/query';
import {
  GET_RENDEZ_VOUS as GET_RENDEZ_VOUS_TYPE,
  GET_RENDEZ_VOUSVariables,
} from '../../../../../federation/partenaire-service/rendez-vous/types/GET_RENDEZ_VOUS';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import { uploadToS3 } from '../../../../../services/S3';
import { formatFilename } from '../../../../Common/Dropzone/Dropzone';
import { FichierInput } from '../../../../../types/graphql-global-types';
import { RendezVousFormOutput } from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/About/RendezVousForm/RendezVousForm';
import {
  // PRTConditionCommercialeFilter,
  PRTRendezVousSortFields,
  SortDirection,
} from '../../../../../types/federation-global-types';
import {
  CREATE_RENDEZ_VOUS,
  UPDATE_RENDEZ_VOUS,
} from '../../../../../federation/partenaire-service/rendez-vous/mutation';
import {
  CREATE_RENDEZ_VOUS as CREATE_RENDEZ_VOUS_TYPE,
  CREATE_RENDEZ_VOUSVariables,
} from '../../../../../federation/partenaire-service/rendez-vous/types/CREATE_RENDEZ_VOUS';
import {
  UPDATE_RENDEZ_VOUS as UPDATE_RENDEZ_VOUS_TYPE,
  UPDATE_RENDEZ_VOUSVariables,
} from '../../../../../federation/partenaire-service/rendez-vous/types/UPDATE_RENDEZ_VOUS';
import { SEND_EMAIL } from '../../../../../federation/notification/notification-email/mutation';
import {
  SEND_EMAIL as SEND_EMAIL_TYPE,
  SEND_EMAILVariables,
} from '../../../../../federation/notification/notification-email/types/SEND_EMAIL';
import { GET_PHARMACIE } from '../../../../../graphql/Pharmacie';
import { PHARMACIE, PHARMACIEVariables } from '../../../../../graphql/Pharmacie/types/PHARMACIE';
import { GET_LABORATOIRE } from '../../../../../federation/partenaire-service/laboratoire/query';
import {
  GET_LABORATOIRE as GET_LABORATOIRE_TYPE,
  GET_LABORATOIREVariables,
} from '../../../../../federation/partenaire-service/laboratoire/types/GET_LABORATOIRE';
import useStyles from '../styles';
import {
  GET_CONDITIONSVariables,
  GET_CONDITIONS as GET_CONDITIONS_TYPE,
} from '../../../../../federation/partenaire-service/laboratoire/condition/types/GET_CONDITIONS';
import { GET_CONDITIONS } from '../../../../../federation/partenaire-service/laboratoire/condition/query';
import {
  PRT_PLAN_MARKETING_NON_RATTACHEVariables,
  PRT_PLAN_MARKETING_NON_RATTACHE as PRT_PLAN_MARKETING_NON_RATTACHE_TYPE,
} from '../../../../../federation/partenaire-service/compte-rendu/types/PRT_PLAN_MARKETING_NON_RATTACHE';
import {
  PRT_CONDITION_COMMERCIALE_NON_RATTACHEVariables,
  PRT_CONDITION_COMMERCIALE_NON_RATTACHE as PRT_CONDITION_COMMERCIALE_NON_RATTACHE_TYPE,
} from '../../../../../federation/partenaire-service/compte-rendu/types/PRT_CONDITION_COMMERCIALE_NON_RATTACHE';
import {
  CREATE_CONDITION as CREATE_CONDITION_TYPE,
  CREATE_CONDITIONVariables,
} from '../../../../../federation/partenaire-service/laboratoire/condition/types/CREATE_CONDITION';
import { CREATE_CONDITION } from '../../../../../federation/partenaire-service/laboratoire/condition/mutation';
import {
  ConditionCommercialeAccords,
  PlanMarketingAccords,
} from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/CompteRendu/CompteRenduForm/SecondScreen/types';
import {
  CREATE_PLAN_MARKETING as CREATE_PLAN_MARKETING_TYPE,
  CREATE_PLAN_MARKETINGVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/CREATE_PLAN_MARKETING';
import { CREATE_PLAN_MARKETING } from '../../../../../federation/partenaire-service/planMarketing/mutation';
import moment from 'moment';
import { startTime } from '../../DemarcheQualite/util';
import { OnCompteRenduDetailProps } from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/CompteRendu/interface';
import { CompteRenduDetail, SendEmailInput } from './CompteRenduDetail/CompteRenduDetail';
import { contentEmailCR } from './file/contentEmailCR';
import {
  NOTIFICATION_LOGSVariables,
  NOTIFICATION_LOGS as NOTIFICATION_LOGS_TYPE,
} from '../../../../../federation/notification/notification-email/notification-logs/types/NOTIFICATION_LOGS';
import { NOTIFICATION_LOGS } from '../../../../../federation/notification/notification-email/notification-logs/mutation';
import {
  GET_LIST_PARTENARIATVariables,
  GET_LIST_PARTENARIAT as GET_LIST_PARTENARIAT_TYPE,
} from '../../../../../federation/partenaire-service/partenariat/types/GET_LIST_PARTENARIAT';
import { GET_PARTENARIATS } from '../../../../../federation/partenaire-service/partenariat/query';
import { AppAuthorization } from '../../../../../services/authorization';

export const useCompteRenduClient = ({ params, push }) => {
  const client = useApolloClient();
  const classes = useStyles();
  const user = getUser();
  const pharmacie = getPharmacie();
  const groupement = getGroupement();
  const auth = new AppAuthorization(user);
  const [openModalCollaborateur, setOpenModalCollaborateur] = useState<boolean>(false);
  const [userParticipants, setUserParticipants] = useState<any[]>([]);
  const [openModalResponsables, setOpenModalResponsables] = useState<boolean>(false);
  const [responsables, setResponsables] = useState<any[]>([]);
  const [success, setSuccess] = useState<boolean>(false);
  const [successRdv, setSuccessRdv] = useState<boolean>(false);
  const [openCollaborateurRdv, setOpenCollaborateurRdv] = useState<boolean>(false);
  const [collaborateursRdv, setCollaborateursRdv] = useState<any[]>([]);
  const [responsableRdv, setResponsablesRdv] = useState<any[]>([]);
  const [openResponsableRdv, setOpenResponsableRdv] = useState<boolean>(false);
  const [isSendingEmail, setSendingEmail] = useState<boolean>(false);
  const [compteRenduToEmail, setCompteRenduToEmail] = useState<any>();

  const [detail, setDetail] = useState<OnCompteRenduDetailProps>();

  const [loadCompteRendu, loadingCompteRendu] = useLazyQuery<
    PRT_COMPTE_RENDUS_TYPE,
    PRT_COMPTE_RENDUSVariables
  >(GET_PRT_COMPTE_RENDUS, {
    client: FEDERATION_CLIENT,
  });

  const [countCompteRendu, countingCompteRendu] = useLazyQuery<
    PRT_COMPTE_RENDU_AGGREGATES_TYPE,
    PRT_COMPTE_RENDU_AGGREGATESVariables
  >(GET_PRT_COMPTE_RENDU_AGGREGATES, {
    client: FEDERATION_CLIENT,
  });

  const [deleteCompteRendu, deletingCompteRendu] = useMutation<
    DELETE_ONE_PRT_COMPTE_RENDU_TYPE,
    DELETE_ONE_PRT_COMPTE_RENDUVariables
  >(DELETE_ONE_PRT_COMPTE_RENDU, {
    onCompleted: () => {
      setSuccess(true);
      displaySnackBar(client, {
        message: 'Le compte rendu a été supprimé avec succès!',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingCompteRendu.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression du compte rendu',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [updateCompteRendu, updatingCompteRendu] = useMutation<
    UPDATE_PRT_COMPTE_RENDU_TYPE,
    UPDATE_PRT_COMPTE_RENDUVariables
  >(UPDATE_PRT_COMPTE_RENDU, {
    onCompleted: () => {
      setSuccess(true);
      displaySnackBar(client, {
        message: 'Le compte rendu a été modifié avec succès!',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingCompteRendu.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la modification du compte rendu',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [createCompteRendu, creatingCompteRendu] = useMutation<
    CREATE_PRT_COMPTE_RENDU_TYPE,
    CREATE_PRT_COMPTE_RENDUVariables
  >(CREATE_PRT_COMPTE_RENDU, {
    onCompleted: () => {
      setSuccess(true);
      displaySnackBar(client, {
        message: 'Le compte rendu a été ajouté avec succès!',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingCompteRendu.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la création du compte rendu',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [loadSubject, loadingSubject] = useLazyQuery<
    GET_RENDEZ_VOUS_SUJETS_VISITES_TYPE,
    GET_RENDEZ_VOUS_SUJETS_VISITESVariables
  >(GET_RENDEZ_VOUS_SUJETS_VISITES, {
    client: FEDERATION_CLIENT,
  });

  const [loadPlanMarketingTypes, loadingPlanMarketingTypes] = useLazyQuery<
    GET_LIST_PLAN_MARKETING_TYPES_TYPE,
    GET_LIST_PLAN_MARKETING_TYPESVariables
  >(GET_LIST_PLAN_MARKETING_TYPES, { client: FEDERATION_CLIENT });

  const handleRequestDelete = (id: string) => {
    deleteCompteRendu({
      variables: { id },
    });
  };

  const [createRdv, creatingRdv] = useMutation<
    CREATE_RENDEZ_VOUS_TYPE,
    CREATE_RENDEZ_VOUSVariables
  >(CREATE_RENDEZ_VOUS, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      setSuccessRdv(true);
      loadingRendezVous.refetch();
      displaySnackBar(client, {
        message: 'Rendez-vous planifié.',
        type: 'SUCCESS',
        isOpen: true,
      });
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs se sont survenues pendant l'ajout du rendez-vous",
        type: 'ERROR',
        isOpen: true,
      });
    },
  });

  const [updateRdv, updatingRdv] = useMutation<
    UPDATE_RENDEZ_VOUS_TYPE,
    UPDATE_RENDEZ_VOUSVariables
  >(UPDATE_RENDEZ_VOUS, {
    onCompleted: () => {
      setSuccessRdv(true);
      displaySnackBar(client, {
        message: 'Le rendez-vous a été reporté avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la modification du rendez-vous',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [loadMiseEnAvants, loadingMiseEnAvants] = useLazyQuery<
    LIST_MISE_AVANT,
    LIST_MISE_AVANTVariables
  >(GET_LIST_MISE_AVANT, { client: FEDERATION_CLIENT });

  const [loadConditionTypes, loadingConditionTypes] = useLazyQuery<
    GET_TYPES_CONDITION_TYPE,
    GET_TYPES_CONDITIONVariables
  >(GET_TYPES_CONDITION, { client: FEDERATION_CLIENT });

  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
  });

  const [loadRendezVous, loadingRendezVous] = useLazyQuery<
    GET_RENDEZ_VOUS_TYPE,
    GET_RENDEZ_VOUSVariables
  >(GET_RENDEZ_VOUS, { client: FEDERATION_CLIENT, fetchPolicy: 'cache-and-network' });

  useEffect(() => {
    setSuccess(false);
  }, [loadingCompteRendu.data?.pRTCompteRendus]);

  const [loadConditionCommerciale, loadingConditionCommerciale] = useLazyQuery<
    PRT_CONDITION_COMMERCIALE_NON_RATTACHE_TYPE,
    PRT_CONDITION_COMMERCIALE_NON_RATTACHEVariables
  >(GET_PRT_CONDITION_COMMERCIALE_NON_RATTACHE, { client: FEDERATION_CLIENT });

  const [createCondtion, creatingCondition] = useMutation<
    CREATE_CONDITION_TYPE,
    CREATE_CONDITIONVariables
  >(CREATE_CONDITION, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      displaySnackBar(client, {
        // message: 'Cette condition commerciale a été ajoutée avec succès.',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingConditionCommerciale.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        // message: "Des erreurs se sont survenues pendant l'ajout de cette condition commerciale.",
        type: 'ERROR',
        isOpen: true,
      });
    },
  });

  const [createPlan, creatingPlan] = useMutation<
    CREATE_PLAN_MARKETING_TYPE,
    CREATE_PLAN_MARKETINGVariables
  >(CREATE_PLAN_MARKETING, {
    onCompleted: () => {
      displaySnackBar(client, {
        // message: ' Le plan Marketing a été crée avec succés !',
        type: 'SUCCESS',
        isOpen: true,
      });

      loadingPlanMarketing.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        // message: 'Erreurs lors de la création du plan Marketing ',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [createNotificationLogs, creatingNotificationLogs] = useMutation<
    NOTIFICATION_LOGS_TYPE,
    NOTIFICATION_LOGSVariables
  >(NOTIFICATION_LOGS, {
    client: FEDERATION_CLIENT,
    onCompleted: data => {
      updateCompteRenduNotification({
        variables: {
          input: {
            idNotificationLogs: data.createOneNotificationLogs.id,
            compteRendu: {
              id: compteRenduToEmail.id,
              idPartenaireTypeAssocie: params.idLabo,
              idPharmacie: compteRenduToEmail.idPharmacie,
            },
          },
        },
      });
      setCompteRenduToEmail(undefined);
    },
  });

  const [loadPlanMarketing, loadingPlanMarketing] = useLazyQuery<
    PRT_PLAN_MARKETING_NON_RATTACHE_TYPE,
    PRT_PLAN_MARKETING_NON_RATTACHEVariables
  >(GET_PRT_PLAN_MARKETING_NON_RATTACHE, { client: FEDERATION_CLIENT });

  const [updateCompteRenduNotification, updatingCompteRenduNotification] = useMutation<
    UPDATE_PRT_COMPTE_RENDU_NOTIFICATION_lOGS_TYPE,
    UPDATE_PRT_COMPTE_RENDU_NOTIFICATION_LOGSVariables
  >(UPDATE_ONE_PRT_COMPTE_RENDU_NOTIFICATION_lOGS, { client: FEDERATION_CLIENT });

  const [loadPartenariat, loadingPartenariat] = useLazyQuery<
    GET_LIST_PARTENARIAT_TYPE,
    GET_LIST_PARTENARIATVariables
  >(GET_PARTENARIATS, { client: FEDERATION_CLIENT });

  const handleSaveCondition = async (data: any) => {
    if (data.fichiers && data.fichiers.length > 0) {
      const filePaths = data.fichiers
        .filter(file => file && file instanceof File)
        .map(file => formatFilename(file, ``));
      if (filePaths.length > 0) {
        doCreatePutPresignedUrl({
          variables: {
            filePaths,
          },
        }).then(async response => {
          if (response.data?.createPutPresignedUrls) {
            try {
              const fichiers = await Promise.all(
                response.data.createPutPresignedUrls.map((item, index) => {
                  if (item?.presignedUrl) {
                    return data.fichiers
                      ? uploadToS3(data.fichiers[index], item?.presignedUrl).then(uploadResult => {
                          if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                            return {
                              chemin: item.filePath,
                              nomOriginal: data.fichiers ? data.fichiers[index].name : '',
                              type: data.fichiers ? data.fichiers[index].type : '',
                            };
                          }
                        })
                      : null;
                  }
                }),
              );
              const oldFiles = (data.fichiers || []).filter(
                file => file && !(file instanceof File),
              );
              saveCondition(
                data,
                fichiers.concat(
                  oldFiles.map(file => ({
                    nomOriginal: file.nomOriginal,
                    chemin: file.chemin,
                    type: file.type,
                  })),
                ) as FichierInput[],
              );
            } catch (error) {}
          } else {
          }
        });
      } else {
        const oldFiles = (data.fichiers || []).filter(file => file && !(file instanceof File));
        saveCondition(
          data,
          oldFiles.map(file => ({
            nomOriginal: file.nomOriginal,
            chemin: file.chemin,
            type: file.type,
          })) as FichierInput[],
        );
      }
    } else {
      saveCondition(data, undefined);
    }
  };

  const handleSavePlan = (data: PlanMarketingAccords) => {
    if (data.fichiers && data.fichiers.length > 0) {
      const filePaths = data.fichiers
        .filter(file => file && file instanceof File)
        .map(file => formatFilename(file, ``));
      if (filePaths.length > 0) {
        doCreatePutPresignedUrl({
          variables: {
            filePaths,
          },
        }).then(async response => {
          if (response.data?.createPutPresignedUrls) {
            try {
              const fichiers = await Promise.all(
                response.data.createPutPresignedUrls.map((item, index) => {
                  if (item?.presignedUrl) {
                    return data.fichiers
                      ? uploadToS3(data.fichiers[index], item?.presignedUrl).then(uploadResult => {
                          if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                            return {
                              chemin: item.filePath,
                              nomOriginal: data.fichiers ? data.fichiers[index].name : '',
                              type: data.fichiers ? data.fichiers[index].type : '',
                            };
                          }
                        })
                      : null;
                  }
                }),
              );
              const oldFiles = (data.fichiers || []).filter(
                file => file && !(file instanceof File),
              );
              savePlan(
                data,
                fichiers.concat(
                  oldFiles.map(file => ({
                    nomOriginal: file.nomOriginal,
                    chemin: file.chemin,
                    type: file.type,
                  })),
                ) as FichierInput[],
              );
            } catch (error) {}
          } else {
          }
        });
      } else {
        const oldFiles = (data.fichiers || []).filter(file => file && !(file instanceof File));
        savePlan(
          data,
          oldFiles.map(file => ({
            nomOriginal: file.nomOriginal,
            chemin: file.chemin,
            type: file.type,
          })) as FichierInput[],
        );
      }
    } else {
      savePlan(data, undefined);
    }
  };

  const saveCondition = (
    data: ConditionCommercialeAccords,
    fichiers: FichierInput[] | undefined,
  ) => {
    if (params.idLabo) {
      if (!data.id) {
        createCondtion({
          variables: {
            input: {
              idPartenaireTypeAssocie: params.idLabo,
              partenaireType: 'LABORATOIRE',
              titre: data.titre,
              description: data.description,
              dateDebut: data.dateDebut,
              dateFin: data.dateFin,
              fichiers,
              // idCanal: data.idCanal,
              idType: data.idType,
            },
          },
        });
      }
    }
  };

  const savePlan = (planMarketing: any, fichiers: any) => {
    if (planMarketing.id) {
      createPlan({
        variables: {
          input: {
            titre: planMarketing.titre,
            description: planMarketing.description,
            dateDebut: planMarketing.dateDebut,
            dateFin: planMarketing.dateFin,
            partenaireType: 'LABORATOIRE',
            idPartenaireTypeAssocie: params.idLabo,
            idType: planMarketing.type.id,
            fichiers,
            idStatut: '',
            idMiseAvants: planMarketing?.miseAvants,
            idProduits: planMarketing?.idProduit || null,
          } as any,
        },
      });
    }
  };

  const handleRequestSave = async (data: CompteRenduFormOuptput) => {
    console.log('ajout compte rendu: ', data);
    if (data.id) {
      updateCompteRendu({
        variables: {
          id: data.id,
          idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
          input: {
            idPartenaireTypeAssocie: params.idLabo,
            partenaireType: 'LABORATOIRE',
            titre: data.titre,
            remiseEchantillon: data.remiseEchantillon,
            gestionPerime: data.gestionPerime,
            conclusion: data.conclusion,
            rapportVisite: data.rapportVisite,
            idResponsables: data.idResponsables,
            idUserParticipants: data.idUserParticipants,
            idSuiviOperationnels: data.idSuiviOperationnels,
            conditionCommerciales: await Promise.all(
              (data.conditionCommerciales || []).map(async condition => ({
                ...condition,
                idPartenaireTypeAssocie: params.idLabo,
                partenaireType: 'LABORATOIRE',
                fichiers: await getFile(condition),
              })),
            ),
            planMarketings: await Promise.all(
              (data.planMarketings || []).map(async plan => ({
                ...plan,
                idPartenaireTypeAssocie: params.idLabo,
                partenaireType: 'LABORATOIRE',
                fichiers: await getFile(plan),
                typeRemuneration: 'euro', //plan.typeRemuneration,
                remuneration: 'forfaitaire', //plan.remuneration,
              })),
            ),
          },
        },
      });
    } else {
      createCompteRendu({
        variables: {
          idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
          input: {
            idPartenaireTypeAssocie: params.idLabo,
            partenaireType: 'LABORATOIRE',
            titre: data.titre,
            remiseEchantillon: data.remiseEchantillon,
            gestionPerime: data.gestionPerime,
            conclusion: data.conclusion,
            rapportVisite: data.rapportVisite,
            idResponsables: data.idResponsables,
            idUserParticipants: data.idUserParticipants,
            idSuiviOperationnels: data.idSuiviOperationnels,
            conditionCommerciales: await Promise.all(
              (data.conditionCommerciales || []).map(async condition => ({
                ...condition,
                idPartenaireTypeAssocie: params.idLabo,
                partenaireType: 'LABORATOIRE',
                fichiers: await getFile(condition),
              })),
            ),
            planMarketings: await Promise.all(
              (data.planMarketings || []).map(async plan => ({
                ...plan,
                idPartenaireTypeAssocie: params.idLabo,
                partenaireType: 'LABORATOIRE',
                fichiers: await getFile(plan),
                typeRemuneration: 'euro', //plan.typeRemuneration,
                remuneration: 'forfaitaire', //plan.remuneration,
              })),
            ),
          },
        },
      });
    }
  };

  const handleRequestSearch = ({ skip, take, searchText, filter, sortTable }: any) => {
    console.log('----------------SEARCH COMPTE RENDU----------------------: ', searchText, filter);
    const sortTableFilter = sortTable.column
      ? [
          {
            field: sortTable.column,
            direction: sortTable.direction,
          },
        ]
      : [];
    loadCompteRendu({
      variables: {
        idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
        filter: {
          idPharmacie: {
            eq: pharmacie.id,
          },
          idPartenaireTypeAssocie: {
            eq: params.idLabo,
          },
          rapportVisite: {
            iLike: `%${searchText}%`,
          },
        },
        paging: {
          limit: take,
          offset: skip,
        },
        sorting: sortTableFilter,
      },
    });
    countCompteRendu({
      variables: {
        filter: {
          idPharmacie: {
            eq: getPharmacie().id,
          },
          idPartenaireTypeAssocie: {
            eq: params.idLabo,
          },
        },
      },
    });
  };

  const [loadPharmacie, loadingPharmacie] = useLazyQuery<PHARMACIE, PHARMACIEVariables>(
    GET_PHARMACIE,
  );

  const [loadLabo, loadingLabo] = useLazyQuery<GET_LABORATOIRE_TYPE, GET_LABORATOIREVariables>(
    GET_LABORATOIRE,
    {
      client: FEDERATION_CLIENT,
    },
  );

  useEffect(() => {
    if (
      params.tab === 'ajout-compte-rendu' ||
      params.tab === 'edit-compte-rendu' ||
      params.tab === 'compte-rendu'
    ) {
      loadLabo({
        variables: {
          id: params.idLabo,
          idPharmacie: getPharmacie().id,
        },
      });
      loadPharmacie({ variables: { id: getPharmacie().id } });
      loadSubject();
      loadMiseEnAvants();
      loadConditionTypes();
      loadPlanMarketingTypes({
        variables: {
          idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
          filter: { idGroupement: { eq: groupement.id } },
        },
      });
      loadRendezVous({
        variables: {
          filter: {
            and: [
              {
                idPartenaireTypeAssocie: {
                  eq: params.idLabo,
                },
              },
              {
                idPharmacie: {
                  eq: getPharmacie().id,
                },
              },
              {
                or: [
                  {
                    statut: { eq: 'REPORTE' },
                  },
                  {
                    statut: { eq: 'PLANIFIE' },
                  },
                ],
              },
              {
                dateRendezVous: {
                  gte: startTime(moment(new Date())),
                },
              },
            ],
          },
          sorting: [
            { field: PRTRendezVousSortFields.dateRendezVous, direction: SortDirection.ASC },
          ],
        },
      });
      loadConditionCommerciale({
        variables: {
          input: {
            idPartenaireTypeAssocie: params.idLabo,
            partenaireType: 'LABORATOIRE',
          },
        },
      });
      loadPlanMarketing({
        variables: {
          input: {
            idPartenaireTypeAssocie: params.idLabo,
            partenaireType: 'LABORATOIRE',
          },
        },
      });
    }
  }, [params]);

  const conditionCommerciales = (
    loadingConditionCommerciale.data?.compteRenduConditionCommerciale || []
  ).map(element => {
    return {
      id: element.id,
      type: element.type,
      canal: element.canal,
      status: element.statut,
      libelle: element.titre,
      details: element.description,
      fichiers: element.fichiers,
      periode: {
        debut: element.dateDebut,
        fin: element.dateFin,
      },
    };
  });

  const getFile = async (data: any) => {
    if (data.fichiers && data.fichiers.length > 0) {
      const filePaths = data.fichiers
        .filter(file => file && file instanceof File)
        .map(file => formatFilename(file, ``));
      if (filePaths.length > 0) {
        doCreatePutPresignedUrl({
          variables: {
            filePaths,
          },
        }).then(async response => {
          if (response.data?.createPutPresignedUrls) {
            try {
              const fichiers = await Promise.all(
                response.data.createPutPresignedUrls.map((item, index) => {
                  if (item?.presignedUrl) {
                    return data.fichiers
                      ? uploadToS3(data.fichiers[index], item?.presignedUrl).then(uploadResult => {
                          if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                            return {
                              chemin: item.filePath,
                              nomOriginal: data.fichiers ? data.fichiers[index].name : '',
                              type: data.fichiers ? data.fichiers[index].type : '',
                            };
                          }
                        })
                      : null;
                  }
                }),
              );
              const oldFiles = (data.fichiers || []).filter(
                file => file && !(file instanceof File),
              );
              return fichiers.concat(
                oldFiles.map(file => ({
                  nomOriginal: file.nomOriginal,
                  chemin: file.chemin,
                  type: file.type,
                })),
              ) as FichierInput[];
            } catch (error) {
              return [];
            }
          } else {
            return [];
          }
        });
      } else {
        const oldFiles = (data.fichiers || []).filter(file => file && !(file instanceof File));
        return oldFiles.map(file => ({
          nomOriginal: file.nomOriginal,
          chemin: file.chemin,
          type: file.type,
        })) as FichierInput[];
      }
    } else {
      return [];
    }
  };

  const compteRenduFormProps: CompteRenduFormProps = {
    collaborateurPiker: (
      <UserInput
        className={classes.userInputCompteRendu}
        openModal={openModalCollaborateur}
        withNotAssigned={false}
        setOpenModal={setOpenModalCollaborateur}
        selected={userParticipants}
        setSelected={setUserParticipants}
        label="Collaborateur"
      />
    ),
    responsablePiker: (
      <UserInput
        className={classes.userInputCompteRendu}
        openModal={openModalResponsables}
        idPartenaireTypeAssocie={params.idLabo}
        withAssignTeam={false}
        withNotAssigned={false}
        partenaireType="LABORATOIRE"
        category="CONTACT"
        setOpenModal={setOpenModalResponsables}
        selected={responsables}
        setSelected={setResponsables}
        label="responsables"
        title="Responsables"
      />
    ),
    selectedCollaborateursIds: userParticipants.map(d => d.id),
    selectedResponsablesIds: responsables.map(d => d.id),
    conditionCommercialeTypes: (
      loadingConditionTypes.data?.pRTConditionCommercialeTypes.nodes || []
    ).map(item => ({
      id: item.id,
      libelle: item.libelle,
      code: item.code,
    })),
    planMarketinMiseEnAvants: (loadingMiseEnAvants.data?.pRTMiseAvants.nodes || []).map(item => ({
      id: item.id,
      libelle: item.libelle,
      code: item.code,
    })),
    planMarketingTypes: (loadingPlanMarketingTypes.data?.pRTPlanMarketingTypes.nodes || []).map(
      item => ({
        id: item.id,
        libelle: item.libelle,
        code: item.code,
      }),
    ),
  };

  const setResponsablesWhenEdit = (responsables: any[]) => {
    setResponsables(responsables);
  };

  const setCollabolateursWhenEdit = (collaborateur: any[]) => {
    setUserParticipants(collaborateur);
  };

  const handleSaveRdv = useCallback(
    (rdv: RendezVousFormOutput) => {
      if (rdv.id) {
        updateRdv({
          variables: {
            id: rdv.id,
            input: {
              typeReunion: rdv.typeReunion,
              idPartenaireTypeAssocie: params.idLabo,
              dateRendezVous: rdv.dateRendezVous,
              heureDebut: rdv.heureDebut,
              heureFin: rdv.heureFin,
              //idSubject: rdv.idSubject,
              idInvites: rdv.idInvites,
              idUserParticipants: rdv.idUserParticipants,
              note: rdv.note,
              partenaireType: 'LABORATOIRE',
              statut: rdv.statut,
            },
          },
        });
      } else {
        createRdv({
          variables: {
            input: {
              typeReunion: rdv.typeReunion,
              idPartenaireTypeAssocie: params.idLabo,
              dateRendezVous: rdv.dateRendezVous,
              heureDebut: rdv.heureDebut,
              heureFin: rdv.heureFin,
              //idSubject: rdv.idSubject,
              idInvites: rdv.idInvites,
              idUserParticipants: rdv.idUserParticipants,
              note: rdv.note,
              partenaireType: 'LABORATOIRE',
              statut: rdv.statut,
            },
          },
        });
      }
    },
    [params],
  );

  const [sendEmail, sendingEmail] = useMutation<SEND_EMAIL_TYPE, SEND_EMAILVariables>(SEND_EMAIL, {
    client: FEDERATION_CLIENT,
  });

  const handleSendMail = (email: SendEmailInput) => {
    console.log('email input: ', email);
    setSendingEmail(true);
    setCompteRenduToEmail(email.compteRendu);
    sendEmail({
      variables: {
        input: {
          subject: email.subject,
          to: email.to,
          html: contentEmailCR(),
          attachment: email.attachment
            ? [
                {
                  content: email.attachment.content,
                  filename: email.attachment.filename,
                  type: email.attachment.type,
                },
              ]
            : undefined,
        },
      },
    }).then(result => {
      if (result.data?.sendEmail?.code !== '200') {
        displaySnackBar(client, {
          message: 'Email non Envoyé.',
          type: 'ERROR',
          isOpen: true,
        });
        setSendingEmail(false);
        return;
      }

      displaySnackBar(client, {
        message: 'Email Envoyé.',
        type: 'SUCCESS',
        isOpen: true,
      });
      createNotificationLogs({
        variables: {
          input: {
            titre: email.subject || '',
            message: contentEmailCR(),
            type: 'SEND_EMAIL',
            recepteurs: email.to,
            expediteur: getUser().id,
            fichiers: [],
          },
        },
      }).finally(() => {
        setSendingEmail(false);
      });
      return;
    });
  };

  const handleGoBack = () => {
    const returnBack =
      params.tab === 'compte-rendu'
        ? '/laboratoires'
        : params.tab === 'ajout-compte-rendu' ||
          params.tab === 'edit-compte-rendu' ||
          params.tab === 'detail-compte-rendu'
        ? `/laboratoires/${params.idLabo}/compte-rendu`
        : '/laboratoires';
    push(returnBack);
  };

  const isEnvoiEmail =
    (loadingPartenariat?.data?.pRTPartenariats.nodes.length || 0) > 0
      ? loadingPartenariat?.data?.pRTPartenariats.nodes[0].isEnvoiEmail
      : false;

  const compteRenduDetail = (
    <CompteRenduDetail
      compteRenduValue={detail?.compteRenduValue}
      laboratoireInformation={detail?.laboratoireInformation}
      pharmaciInformation={detail?.pharmacieInformation}
      dateProchainRendezVous={detail?.dateProchainRendezVous}
      onRequestSendMail={handleSendMail}
      push={push}
      idLabo={params?.idLabo}
      sendingEmail={isSendingEmail}
      isEnvoiEmail={isEnvoiEmail}
    />
  );

  const handleCompteRenduDetail = (props?: OnCompteRenduDetailProps) => {
    push(`/laboratoires/${params.idLabo}/detail-compte-rendu`);
    loadPartenariat({
      variables: {
        paging: {
          limit: 1,
        },
        filter: {
          and: [
            {
              idPartenaireTypeAssocie: {
                eq: params.idLabo,
              },
            },
            {
              partenaireType: {
                eq: 'LABORATOIRE',
              },
            },
            {
              idPharmacie: {
                eq: props?.compteRenduValue?.idPharmacie,
              },
            },
          ],
        },
      },
    });
    setDetail(props);
  };

  const compteRenduProps: CompteRenduComponentProps = {
    compteRendus: {
      loading: loadingCompteRendu.loading,
      error: loadingCompteRendu.error as any,
      data: (loadingCompteRendu.data?.pRTCompteRendus.nodes || []).map(item => ({
        id: item.id,
        titre: item.titre,
        remiseEchantillon: item.remiseEchantillon,
        gestionPerime: item.gestionPerime || undefined,
        responsables: item.responsables || [],
        collaborateurs: item.collaborateurs,
        planMarketings: (item.planMarketings || []).map(plan => ({
          ...plan,
          idType: plan.type.id,
        })),
        suiviOperationnelIds: (item.suiviOperationnels || []).map(d => d.id),
        createdAt: item.createdAt,
        conditionsCommerciales: (item.conditionsCommerciales || []) as any,
        suiviOperationnels: item.suiviOperationnels || [],
        conclusion: item.conclusion || undefined,
        rapportVisite: item.rapportVisite || undefined,
        notificationLogs: item.notificationLogs,
        idPharmacie: item.idPharmacie,
      })) as any,
    },
    rowsTotal:
      (countingCompteRendu.data?.pRTCompteRenduAggregate.length || 0) > 0
        ? countingCompteRendu.data?.pRTCompteRenduAggregate[0].count?.id || 0
        : 0,
    onRequestSave: handleRequestSave,
    onRequestDelete: handleRequestDelete,
    onRequestSearch: handleRequestSearch,
    compteRenduForm: compteRenduFormProps,
    saving: {
      error: updatingCompteRendu.error || creatingCompteRendu.error,
      loading: updatingCompteRendu.loading || creatingCompteRendu.loading,
      success,
    },
    deletingSuccess: success,
    setResponsables: setResponsablesWhenEdit,
    setCollabolateurs: setCollabolateursWhenEdit,
    rendezVous: {
      onRequestSave: handleSaveRdv,
      rendezVous: loadingRendezVous.data?.pRTRendezVous.nodes[0],
      subjects: loadingSubject.data?.pRTRendezVousSujetVisites.nodes || [],
      saving: {
        loading: updatingRdv.loading || creatingRdv.loading,
        error: updatingRdv.error || creatingRdv.error,
        success: successRdv,
      },
      selectedInvites: responsableRdv,
      selectedCollaborateurs: collaborateursRdv,
      collaborateurPiker: (
        <UserInput
          openModal={openCollaborateurRdv}
          withNotAssigned={false}
          setOpenModal={setOpenCollaborateurRdv}
          selected={collaborateursRdv}
          setSelected={setCollaborateursRdv}
          label="Collaborateur"
        />
      ),
      responsablePiker: (
        <UserInput
          openModal={openResponsableRdv}
          idPartenaireTypeAssocie={params.idLabo}
          withAssignTeam={false}
          withNotAssigned={false}
          partenaireType="LABORATOIRE"
          category="CONTACT"
          setOpenModal={setOpenResponsableRdv}
          selected={responsableRdv}
          setSelected={setResponsablesRdv}
          label="Invité"
          title="Invité"
        />
      ),
      changeSuccess: () => {
        setSuccessRdv(false);
      },
      setSelectedInvites: selecteds => {
        setResponsablesRdv(selecteds);
      },
      setSelectedParticipants: selecteds => {
        setCollaborateursRdv(selecteds);
      },
    },
    detailinformtionCompteRendu: {
      onRequestSendMail: (input: any) => handleSendMail(input),
      pharmaciInformation:
        loadingPharmacie.data && loadingPharmacie.data.pharmacie
          ? {
              nom: loadingPharmacie.data?.pharmacie?.nom || '',
              adresse: loadingPharmacie.data?.pharmacie?.adresse1 || '',
              telephone:
                loadingPharmacie.data?.pharmacie?.contact?.telMobPerso ||
                loadingPharmacie.data?.pharmacie?.contact?.telMobProf ||
                '',
              email:
                loadingPharmacie.data?.pharmacie?.contact?.mailPerso ||
                loadingPharmacie.data?.pharmacie?.contact?.mailProf ||
                '',
            }
          : undefined,

      laboratoireInformation:
        loadingLabo.data && loadingLabo.data.laboratoire
          ? {
              nom: loadingLabo.data.laboratoire.nom,
              adresse: loadingLabo.data.laboratoire.laboratoireSuite?.adresse || '',
              telephone: loadingLabo.data.laboratoire.laboratoireSuite?.telephone || '',
              email: loadingLabo.data.laboratoire.laboratoireSuite?.email || '',
            }
          : undefined,
      sendingEmail: isSendingEmail,
    },
    onRequestGoBack: handleGoBack,
    planMarketings: {
      onSavePlan: handleSavePlan,
      saving: {
        loading: creatingPlan.loading,
        error: creatingPlan.error,
        success,
      },
      data: loadingPlanMarketing.data?.compteRenduPlanMarketing || [],
      loading: loadingPlanMarketing.loading,
      error: loadingPlanMarketing.error as any,
    },
    conditionCommerciales: {
      onSaveCondition: handleSaveCondition,
      saving: {
        loading: creatingCondition.loading,
        error: creatingCondition.error,
        success,
      },
      data: (loadingConditionCommerciale.data?.compteRenduConditionCommerciale as any) || [],
      loading: loadingConditionCommerciale.loading,
      error: loadingConditionCommerciale.error as any,
    },
    onCompteRenduDetail: handleCompteRenduDetail,
    compteRenduDetail,
  };
  return compteRenduProps;
};
