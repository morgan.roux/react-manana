import React, { ChangeEvent } from 'react';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { useEffect, useState } from 'react';
import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';

import {
  GET_PLAN_MARKETINGS as GET_PLAN_MARKETINGS_TYPE,
  GET_PLAN_MARKETINGSVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/GET_PLAN_MARKETINGS';
import {
  GET_LIST_PLAN_MARKETING_TYPES as GET_LIST_PLAN_MARKETING_TYPES_TYPE,
  GET_LIST_PLAN_MARKETING_TYPESVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/GET_LIST_PLAN_MARKETING_TYPES';
import {
  GET_LIST_PLAN_MARKETING_STATUT as GET_LIST_PLAN_MARKETING_STATUT_TYPE,
  GET_LIST_PLAN_MARKETING_STATUTVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/GET_LIST_PLAN_MARKETING_STATUT';
import {
  CHANGE_PLAN_MARKETING_STATUS as CHANGE_PLAN_MARKETING_STATUS_TYPE,
  CHANGE_PLAN_MARKETING_STATUSVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/CHANGE_PLAN_MARKETING_STATUS';
import {
  UPDATE_PLAN_MARKETING as UPDATE_PLAN_MARKETING_TYPE,
  UPDATE_PLAN_MARKETINGVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/UPDATE_PLAN_MARKETING';
import {
  DELETE_ONE_PLAN_MARKETING as DELETE_ONE_PLAN_MARKETING_TYPE,
  DELETE_ONE_PLAN_MARKETINGVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/DELETE_ONE_PLAN_MARKETING';
import {
  CREATE_PLAN_MARKETING as CREATE_PLAN_MARKETING_TYPE,
  CREATE_PLAN_MARKETINGVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/CREATE_PLAN_MARKETING';

import {
  GET_LIST_PLAN_MARKETING_TYPES,
  GET_PLAN_MARKETINGS,
  GET_LIST_PLAN_MARKETING_STATUT,
  GET_LIST_MISE_AVANT,
} from '../../../../../federation/partenaire-service/planMarketing/query';

import {
  DELETE_ONE_PLAN_MARKETING,
  CHANGE_PLAN_MARKETING_STATUS,
  CREATE_PLAN_MARKETING,
  UPDATE_PLAN_MARKETING,
} from '../../../../../federation/partenaire-service/planMarketing/mutation';

import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import { uploadToS3 } from '../../../../../services/S3';
import { formatFilename } from '../../../../Common/Dropzone/Dropzone';
import { PRTPlanMarketingFilter } from '../../../../../types/federation-global-types';
import { getGroupement, getPharmacie, getUser } from '../../../../../services/LocalStorage';
import {
  LIST_MISE_AVANT,
  LIST_MISE_AVANTVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/LIST_MISE_AVANT';
import { GET_SEARCH_PLAN_MARKETING_PRODUIT_CANALS } from '../../../../../graphql/ProduitCanal/query';
import {
  SEARCH_PLAN_MARKETING_PRODUIT_CANALS as SEARCH_PLAN_MARKETING_PRODUIT_CANALS_TYPE,
  SEARCH_PLAN_MARKETING_PRODUIT_CANALSVariables,
} from '../../../../../graphql/ProduitCanal/types/SEARCH_PLAN_MARKETING_PRODUIT_CANALS';
import { TableChange } from '@app/ui-kit/components/atoms';
import { CustomFormMessage } from '../../../../Common/CustomFormMessage';
import {
  GET_PRT_CONTACTVariables,
  GET_PRT_CONTACT as GET_PRT_CONTACT_TYPE,
} from '../../../../../federation/partenaire-service/contact/types/GET_PRT_CONTACT';
import { GET_PRT_CONTACT } from '../../../../../federation/partenaire-service/contact/query';
import {
  GET_PRT_REGLEMENT_MODEVariables,
  GET_PRT_REGLEMENT_MODE as GET_PRT_REGLEMENT_MODE_TYPE,
} from '../../../../../federation/partenaire-service/remuneration/types/GET_PRT_REGLEMENT_MODE';
import { GET_PRT_REGLEMENT_MODE } from '../../../../../federation/partenaire-service/remuneration/query';
import moment from 'moment';
import { AppAuthorization } from '../../../../../services/authorization';
import { endTime, startTime } from '../../DemarcheQualite/util';
import { AWS_HOST } from '../../../../../config';

export const usePlanMarketing = (idCurrentLabo: string, push: any, tab: string) => {
  const pharmacie = getPharmacie();
  const groupement = getGroupement();
  const user = getUser();
  const auth = new AppAuthorization(user);
  const client = useApolloClient();
  const [skipPlan, setSkipPlan] = useState(0);
  const [takePlan, setTakePlan] = useState(10);
  const [searchTextPlan, setSearchTextPlan] = useState('');
  const [filterPlan, setFilterPlan] = useState<any[]>();
  const [planToEdit, setPlanToEdit] = useState<any>(undefined);
  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const [sortTable, setSortTable] = useState<any>({
    column: 'periode',
    direction: 'ASC',
  });
  const [savingStatut, setSavingStatut] = useState<boolean>(false);
  const [savedStatut, setSavedStatut] = useState<boolean>(false);
  const [useCataloguePharmacie, setUseCataloguePharmacie] = useState<boolean>(true);

  const [recepteur, setRecepteur] = useState<any[]>([]);
  const [valueFacture, setValueFacture] = useState<{
    message: string | undefined;
    objet: string | undefined;
    recepteurs: any[];
    selectedFiles: any[];
  }>({
    message: undefined,
    objet: undefined,
    recepteurs: [],
    selectedFiles: [],
  });

  // console.log('data Plan', loadingPlanMarketing.data?.pRTPlanMarketings);

  useEffect(() => {
    if (idCurrentLabo) {
      onRequestPlanSearch();
    }
  }, [skipPlan, takePlan, searchTextPlan, filterPlan, sortTable]);

  // Query PlanMarketing
  const planMarketingTypes = useQuery<
    GET_LIST_PLAN_MARKETING_TYPES_TYPE,
    GET_LIST_PLAN_MARKETING_TYPESVariables
  >(GET_LIST_PLAN_MARKETING_TYPES, {
    client: FEDERATION_CLIENT,
    variables: {
      idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
      filter: { idGroupement: { eq: groupement.id } },
    },
  });

  const loadMiseAvant = useQuery<LIST_MISE_AVANT, LIST_MISE_AVANTVariables>(GET_LIST_MISE_AVANT, {
    client: FEDERATION_CLIENT,
  });

  // QUERY LISTE DES STATUTS
  const planMarketingStatut = useQuery<
    GET_LIST_PLAN_MARKETING_STATUT_TYPE,
    GET_LIST_PLAN_MARKETING_STATUTVariables
  >(GET_LIST_PLAN_MARKETING_STATUT, { client: FEDERATION_CLIENT });

  // FICHIERS
  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
  });

  // QUERY
  const [loadPlanMarketing, loadingPlanMarketing] = useLazyQuery<
    GET_PLAN_MARKETINGS_TYPE,
    GET_PLAN_MARKETINGSVariables
  >(GET_PLAN_MARKETINGS, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
  });

  console.log('LOAD PLAN', loadingPlanMarketing);

  const [loadProduits, loadingProduits] = useLazyQuery<
    SEARCH_PLAN_MARKETING_PRODUIT_CANALS_TYPE,
    SEARCH_PLAN_MARKETING_PRODUIT_CANALSVariables
  >(GET_SEARCH_PLAN_MARKETING_PRODUIT_CANALS);

  const [loadPlanMarketingProduits, loadingPlanMarketingProduits] = useLazyQuery<
    SEARCH_PLAN_MARKETING_PRODUIT_CANALS_TYPE,
    SEARCH_PLAN_MARKETING_PRODUIT_CANALSVariables
  >(GET_SEARCH_PLAN_MARKETING_PRODUIT_CANALS);

  /**create */
  const [createPlanMarketing, creatingPlanMarketing] = useMutation<
    CREATE_PLAN_MARKETING_TYPE,
    CREATE_PLAN_MARKETINGVariables
  >(CREATE_PLAN_MARKETING, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: ' Le plan Marketing a été crée avec succés !',
        type: 'SUCCESS',
        isOpen: true,
      });

      loadingPlanMarketing.refetch();
      setSaving(false);
      setSaved(true);
    },
    onError: error => {
      console.log('error: ', error);
      displaySnackBar(client, {
        message: 'Erreurs lors de la création du plan Marketing ',
        type: 'ERROR',
        isOpen: true,
      });

      setSaving(false);
    },
    client: FEDERATION_CLIENT,
  });

  /**update plan Marketing */
  const [updatePlanMarketing, updatingPlanMarketing] = useMutation<
    UPDATE_PLAN_MARKETING_TYPE,
    UPDATE_PLAN_MARKETINGVariables
  >(UPDATE_PLAN_MARKETING, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le plan Marketing a été modifié avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });

      setSaving(false);
      setSaved(true);
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la modification du plan Marketing',
        type: 'ERROR',
        isOpen: true,
      });

      setSaving(false);
    },
    client: FEDERATION_CLIENT,
  });

  /**delete Plan Marketing */
  const [deletePlanMarketing, deletingPlanMarketing] = useMutation<
    DELETE_ONE_PLAN_MARKETING_TYPE,
    DELETE_ONE_PLAN_MARKETINGVariables
  >(DELETE_ONE_PLAN_MARKETING, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le plan Marketing a été supprimé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingPlanMarketing.refetch();
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression du plan Marketing',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  //Change Statut
  const [changePlanStatut, changingPlanStatut] = useMutation<
    CHANGE_PLAN_MARKETING_STATUS_TYPE,
    CHANGE_PLAN_MARKETING_STATUSVariables
  >(CHANGE_PLAN_MARKETING_STATUS, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le statut a été modifié avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingPlanMarketing.refetch();
      setSavingStatut(false);
      setSavedStatut(true);
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant le changement du statut',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [loadContact, loadingContact] = useLazyQuery<
    GET_PRT_CONTACT_TYPE,
    GET_PRT_CONTACTVariables
  >(GET_PRT_CONTACT, {
    client: FEDERATION_CLIENT,
  });

  const [loadReglementMode, loadingReglementMode] = useLazyQuery<
    GET_PRT_REGLEMENT_MODE_TYPE,
    GET_PRT_REGLEMENT_MODEVariables
  >(GET_PRT_REGLEMENT_MODE, { client: FEDERATION_CLIENT });

  useEffect(() => {
    if (tab === 'plan') {
      loadReglementMode();
    }
  }, [tab]);

  const handleFetchMorePlans = () => {
    const fetchMore = loadingPlanMarketing.fetchMore;
    const variables = loadingPlanMarketing.variables;
    const paging = variables.paging;
    if (loadingPlanMarketing.data?.pRTPlanMarketings.nodes) {
      fetchMore({
        variables: {
          ...variables,
          paging: {
            ...paging,
            offset: loadingPlanMarketing.data?.pRTPlanMarketings.nodes,
          },
        },

        updateQuery: (prev: any, { fetchMoreResult }) => {
          if (prev?.pRTPlanMarketings && fetchMoreResult?.pRTPlanMarketings.nodes) {
            return {
              ...prev,
              pRTPlanMarketings: {
                ...prev.pRTPlanMarketings,
                nodes: [
                  ...prev.pRTPlanMarketings.nodes,
                  ...fetchMoreResult.pRTPlanMarketings.nodes,
                ],
              },
            };
          }

          return prev;
        },
      });
    }
  };

  const handleRequestSearchProduits = (
    change?: TableChange | null,
    ids?: string[],
    cataloguePharmacie?: boolean,
  ) => {
    console.log('****************Request search');
    let must: any = [
      {
        term: {
          'commandeCanal.code': 'PFL',
        },
      },
      {
        term: {
          'produit.produitTechReg.laboExploitant.id': idCurrentLabo,
        },
      },
    ];

    if (typeof cataloguePharmacie === 'undefined' || cataloguePharmacie) {
      must.push({ terms: { 'produit.pharmacieReferences': [pharmacie.id] } });
      setUseCataloguePharmacie(true);
    } else {
      setUseCataloguePharmacie(false);
    }

    if (ids && ids.length) {
      must.push({
        terms: {
          _id: ids,
        },
      });
    }

    if (change) {
      const searchText = change.searchText.replace(/\s+/g, '\\ ').replace('/', '\\/');
      if (searchText) {
        must.push({
          query_string: {
            query:
              searchText.startsWith('*') || searchText.endsWith('*')
                ? searchText
                : `*${searchText}*`,
            fields: [],
            analyze_wildcard: true,
          },
        });
      }
    }

    const variables: SEARCH_PLAN_MARKETING_PRODUIT_CANALSVariables = {
      type: ['produitcanal'],
      query: {
        query: {
          bool: {
            must,
          },
        },
      },
      take: change?.take || 12,
      skip: change?.skip || 0,
    };

    if (ids && ids.length) {
      loadPlanMarketingProduits({ variables });
    }
    //if (change) {
    loadProduits({ variables });
    //}
  };

  const saveplanMarketing = (planMarketing: any, fichiers: any) => {
    console.log('plan marketing ajouter: ', planMarketing);
    if (planMarketing.id) {
      updatePlanMarketing({
        variables: {
          idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
          id: planMarketing.id,
          input: {
            titre: planMarketing.titre,
            description: planMarketing.description,
            dateDebut: startTime(planMarketing.dateDebut),
            dateFin: endTime(planMarketing.dateFin),
            partenaireType: 'LABORATOIRE',
            idPartenaireTypeAssocie: idCurrentLabo,
            idType:
              planMarketingTypes.data?.pRTPlanMarketingTypes.nodes.find(
                el => el.code === planMarketing.typePlan,
              )?.id || '',
            idStatut: '',
            fichiers,
            idMiseAvants: planMarketing?.idMiseAvants || null,
            produits:
              (planMarketing?.produits || []).map(element => {
                return {
                  idProduit: element?.produit?.id,
                  quantitePrevue: parseFloat(element?.produit?.quantitePrevue),
                  prixUnitaire: parseFloat(element?.produit?.prixUnitaire),
                  remisePrevue: parseFloat(element?.produit?.remisePrevue),
                  quantiteRealise: parseFloat(element?.produit?.quantiterealise),
                };
              }) || null,
            montant: parseFloat(planMarketing?.montant),
            remise: parseFloat(planMarketing?.remise),
            typeRemuneration: planMarketing.typeRemuneration,
            remuneration: planMarketing.remuneration,
          },
        },
      });
    } else {
      createPlanMarketing({
        variables: {
          idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
          input: {
            titre: planMarketing.titre,
            description: planMarketing.description,
            dateDebut: startTime(planMarketing.dateDebut),
            dateFin: endTime(planMarketing.dateFin),
            partenaireType: 'LABORATOIRE',
            idPartenaireTypeAssocie: idCurrentLabo,
            idType:
              planMarketingTypes.data?.pRTPlanMarketingTypes.nodes.find(
                el => el.code === planMarketing.typePlan,
              )?.id || '',
            fichiers,
            idStatut: '',
            idMiseAvants: planMarketing?.idMiseAvants,
            produits:
              (planMarketing?.produits || []).map(element => {
                return {
                  idProduit: element.produit.id,
                  quantitePrevue: parseFloat(element.produit.quantitePrevue) || 0,
                  prixUnitaire: parseFloat(element.produit.prixUnitaire) || 0,
                  remisePrevue: parseFloat(element.produit.remisePrevue) || 0,
                  quantiteRealise: parseFloat(element.produit.quantiteRealise) || 0,
                };
              }) || null,
            montant: parseFloat(planMarketing?.montant) || 0,
            remise: parseFloat(planMarketing.remise),
            typeRemuneration: planMarketing.typeRemuneration,
            remuneration: planMarketing.remuneration,
          },
        },
      });
    }
  };

  // Plan marketing Props
  const handlePlanSave = (planMarketing: any) => {
    setSaving(true);
    setSaved(false);
    if (planMarketing?.selectedFiles?.length > 0) {
      console.log('detection');
      const newFiles = planMarketing.selectedFiles.filter(file => file && file instanceof File);
      const oldFiles = planMarketing.selectedFiles
        .filter(file => file && !(file instanceof File))
        .map(file => {
          if (file) {
            return {
              chemin: file.chemin,
              nomOriginal: file.nomOriginal,
              type: file.type,
            };
          }
        });
      console.log('Plan fichier newFiles', newFiles, 'oldFiles', oldFiles);

      if (newFiles?.length > 0) {
        const filePaths = newFiles.map(file => formatFilename(file, ``));
        doCreatePutPresignedUrl({
          variables: {
            filePaths,
          },
        }).then(async response => {
          if (response.data?.createPutPresignedUrls) {
            try {
              const fichiers = await Promise.all(
                response.data.createPutPresignedUrls.map((item, index) => {
                  if (item?.presignedUrl) {
                    return planMarketing.selectedFiles
                      ? uploadToS3(planMarketing.selectedFiles[index], item?.presignedUrl).then(
                          uploadResult => {
                            if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                              return {
                                chemin: item.filePath,
                                nomOriginal: planMarketing.selectedFiles
                                  ? planMarketing.selectedFiles[index].name
                                  : '',
                                type: planMarketing.selectedFiles
                                  ? planMarketing.selectedFiles[index].type
                                  : '',
                              };
                            }
                          },
                        )
                      : null;
                  }
                }),
              );
              saveplanMarketing(planMarketing, [...fichiers, ...oldFiles]);
            } catch (error) {
              setSaving(false);
              return;
            }
          } else {
            setSaving(false);
            return;
          }
        });
      } else {
        if (oldFiles?.length > 0) {
          saveplanMarketing(planMarketing, oldFiles);
        } else {
          saveplanMarketing(planMarketing, undefined);
        }
      }
    } else {
      saveplanMarketing(planMarketing, undefined);
    }
  };

  // Plan to edit
  const handlePlanEdit = (planMarketing: any) => {
    console.log('--------------------PLAN TO EDIT---------------------: ', planMarketing);
    setPlanToEdit(planMarketing);
  };

  // Delete
  const handlePlanDelete = (planMarketing: any) => {
    deletePlanMarketing({
      variables: {
        input: { id: planMarketing.id },
      },
    });
  };

  // Recherche
  const handlePlanSearch = ({ skip, take, searchText, filter, sortTable }: any) => {
    setSkipPlan(skip);
    setTakePlan(take);
    setSearchTextPlan(searchText);
    setFilterPlan(filter);
    setSortTable(sortTable);
  };

  // Changement de Statut
  const handlePlanChangeStatut = (id: string, idStatut: string) => {
    const statut = planMarketingStatut.data?.pRTPlanMarketingStatuts.nodes.find(
      el => el.id === idStatut,
    );
    console.log('idPlan: ', id);
    if (statut) {
      changePlanStatut({
        variables: {
          id,
          input: {
            statut: {
              libelle: statut.libelle,
              code: statut.code,
            },
          },
        },
      });
    }
  };

  const handleGoBack = () => {
    push('/laboratoires');
  };

  // Plan Search
  const onRequestPlanSearch = () => {
    const keywords = ['idType', 'idStatut'];
    if (idCurrentLabo) {
      const _filters = keywords
        .map(keyword => {
          const tempObject = { or: [] };
          for (let filter of filterPlan || []) {
            if (filter.keyword === keyword) {
              tempObject.or.push({
                [keyword]: {
                  eq: filter.id,
                },
              } as never);
            }
          }
          return tempObject.or.length > 0 ? tempObject : undefined;
        })
        .filter(e => e);

      const filterAnd: PRTPlanMarketingFilter[] = [
        {
          partenaireType: {
            eq: 'LABORATOIRE',
          },
        },
        {
          idPartenaireTypeAssocie: {
            eq: idCurrentLabo,
          },
        },
        {
          idPharmacie: {
            eq: pharmacie.id,
          },
        },
        {
          or: [
            {
              titre: {
                iLike: `%${searchTextPlan}%`,
              },
            },
            {
              description: {
                iLike: `%${searchTextPlan}%`,
              },
            },
          ],
        },
      ];

      if (_filters.length > 0) {
        _filters.map(filter => {
          filterAnd.push(filter as any);
        });
      }

      const sortColumn = sortTable.column || 'periode';

      const sortTableFilter =
        sortColumn === 'periode'
          ? [
              {
                field: 'dateDebut',
                direction: sortTable.direction,
              },
              {
                field: 'dateFin',
                direction: sortTable.direction,
              },
            ]
          : [
              {
                field: sortTable.column,
                direction: sortTable.direction,
              },
            ];

      // idPharmacie
      loadPlanMarketing({
        variables: {
          idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
          filter: {
            and: filterAnd,
          },
          paging: {
            offset: skipPlan,
            limit: takePlan,
          },
          sorting: sortTableFilter,
        },
      });
    }
  };

  const saveChangeStatut = ({ operation, idPlanMarketing, type, fichiers, produits }) => {
    console.log('-------------CHANGEMENT STATUS---------------------: ', produits);
    const statut = planMarketingStatut.data?.pRTPlanMarketingStatuts.nodes.find(
      el => el.code === type,
    );
    const modeReglement = loadingReglementMode.data?.pRTReglementModes.nodes.find(
      el => el.code === operation?.idModeReglement,
    );
    if (statut) {
      setSaving(true);
      setSaved(false);
      console.log('FACURATION');
      changePlanStatut({
        variables: {
          id: idPlanMarketing,
          input: {
            statut: {
              libelle: statut.libelle,
              code: statut.code,
            },
            descriptionStatutRealise: operation?.description,
            dateStatutCloture: moment(operation?.date).toISOString(),
            dateStatutRealise: moment(new Date()).toISOString(),
            montantStatutCloture: parseFloat(operation?.montant) || 0,
            montantStatutRealise: parseFloat(operation?.montantStatutRealise) || 0,
            objetStatutFacture: valueFacture?.objet || '',
            messageStatutFacture: valueFacture?.message || '',
            idModeReglement: modeReglement?.id,
            fichiers,
            produits:
              type === 'REALISE'
                ? (produits || []).map(element => {
                    return {
                      id: element.id,
                      produit: {
                        id: element.produit.id,
                        prixUnitaire: element.produit.prixUnitaire,
                        quantitePrevue: element.produit.quantitePrevue,
                        quantiteRealise: element.produit.quantiteRealise,
                        remisePrevue: element.produit.remisePrevue,
                      },
                    };
                  })
                : [],
            idContact:
              (valueFacture.recepteurs || []).map(element => {
                return element.id;
              }) || [],
          },
        },
      });
    }
  };

  const handleSubmitOperationFacture = idPlanMarketing => {
    const fichiers = valueFacture.selectedFiles;
    if (valueFacture.selectedFiles.length > 0) {
      const newFiles = valueFacture.selectedFiles.filter(file => file && file instanceof File);
      if (newFiles.length > 0) {
        const filePaths = newFiles.map(file => formatFilename(file, ``));
        doCreatePutPresignedUrl({
          variables: {
            filePaths,
          },
        }).then(async response => {
          if (response.data?.createPutPresignedUrls) {
            try {
              const fichierToAdd = await Promise.all(
                response.data.createPutPresignedUrls.map((item, index) => {
                  if (item?.presignedUrl) {
                    return fichiers
                      ? uploadToS3(fichiers[index], item?.presignedUrl).then(uploadResult => {
                          if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                            return {
                              chemin: item.filePath,
                              nomOriginal: fichiers ? fichiers[index].name : '',
                              type: fichiers ? fichiers[index].type : '',
                              publicUrl: `${AWS_HOST}/${item.filePath}`,
                            };
                          }
                        })
                      : null;
                  }
                }),
              );
              saveChangeStatut({
                operation: undefined,
                idPlanMarketing,
                type: 'FACTURE',
                fichiers: fichierToAdd,
                produits: undefined,
              });
            } catch (error) {}
          }
        });
      }
    }
  };

  const handleSubmitOperationStatut = ({
    operation,
    fichiers,
    idPlanMarketing,
    type,
    produits,
  }: any) => {
    if (type === 'FACTURE') {
      handleSubmitOperationFacture(idPlanMarketing);
      return;
    }
    if (fichiers?.length > 0) {
      const newFiles = fichiers.filter(file => file && file instanceof File);
      if (newFiles.length > 0) {
        const filePaths = newFiles.map(file => formatFilename(file, ``));
        doCreatePutPresignedUrl({
          variables: {
            filePaths,
          },
        }).then(async response => {
          if (response.data?.createPutPresignedUrls) {
            try {
              const fichierToAdd = await Promise.all(
                response.data.createPutPresignedUrls.map((item, index) => {
                  if (item?.presignedUrl) {
                    return fichiers
                      ? uploadToS3(fichiers[index], item?.presignedUrl).then(uploadResult => {
                          if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                            return {
                              chemin: item.filePath,
                              nomOriginal: fichiers ? fichiers[index].name : '',
                              type: fichiers ? fichiers[index].type : '',
                            };
                          }
                        })
                      : null;
                  }
                }),
              );
              saveChangeStatut({
                operation,
                idPlanMarketing,
                type,
                fichiers: fichierToAdd,
                produits,
              });
            } catch (error) {}
          }
        });
      }
    } else {
      saveChangeStatut({ operation, idPlanMarketing, type, fichiers: undefined, produits });
    }
  };

  const handleOpenListModal = () => {
    loadContact({
      variables: {
        filter: {
          and: [
            {
              partenaireType: {
                eq: 'LABORATOIRE',
              },
            },
            {
              idPartenaireTypeAssocie: {
                eq: idCurrentLabo,
              },
            },
            {
              idPharmacie: {
                eq: pharmacie.id,
              },
            },
          ],
        },
      },
    });
  };

  const handleSaveFacture = (value: any) => {
    console.log('--O O--: ', value);
    setValueFacture(value);
  };

  const isFactureOperationValid = () => {
    return (
      valueFacture.message !== '' &&
      valueFacture.objet !== '' &&
      valueFacture.recepteurs.length > 0 &&
      valueFacture.selectedFiles.length > 0
    );
  };

  //Liste des Types
  const miseAvants = {
    error: loadMiseAvant.error as any,
    loading: loadMiseAvant.loading,
    data: loadMiseAvant.data?.pRTMiseAvants.nodes || ([] as any),
  };

  console.log(
    '------------------------ PRODUITS--------------------------: ',
    loadingProduits.data?.search?.data,
    loadingPlanMarketing.data?.pRTPlanMarketings.nodes,
  );

  const produits = {
    error: loadingProduits.error as any,
    loading: loadingProduits.loading as any,
    data: loadingProduits.data?.search?.data,
    rowsTotal: loadingProduits.data?.search?.total || 0,
    onRequestSearch: handleRequestSearchProduits,
    planMarketingProduits: loadingPlanMarketingProduits.data?.search?.data || [],
    pharmacieCatalogue: useCataloguePharmacie,
  };

  const listTypePlan = {
    error: planMarketingTypes.error as any,
    loading: planMarketingTypes.loading,
    data: !auth.isSupAdminOrIsGpmAdmin
      ? (planMarketingTypes.data?.pRTPlanMarketingTypes.nodes || []).filter(
          planMarketingType => planMarketingType.active === true,
        )
      : planMarketingTypes.data?.pRTPlanMarketingTypes.nodes || [],
  };

  // Liste des statuts
  const listStatutPlan = planMarketingStatut.data?.pRTPlanMarketingStatuts.nodes || [];

  console.log(
    'MODE PLAN',
    loadingPlanMarketing.data?.pRTPlanMarketings.nodes,
    loadingPlanMarketingProduits.data?.search?.data,
  );

  const factureOperationComponent = (
    <CustomFormMessage
      onOpenUserModal={handleOpenListModal}
      recepteurs={{
        data: recepteur,
        error: loadingContact.error as any,
        loading: loadingContact.loading,
        type: 'LABORATOIRE',
        id: idCurrentLabo,
        category: 'CONTACT',
        setRecepteur,
      }}
      onRequestSave={handleSaveFacture}
    />
  );

  const laboratoirePlan = {
    plan: {
      error: loadingPlanMarketing.error as any,
      loading: loadingPlanMarketing.loading,
      data:
        loadingPlanMarketing.data?.pRTPlanMarketings.nodes ||
        [] /*.map(pm => {
        // Changer le type de plan marketing
        const currentType = listTypePlan.data.find(tpm => tpm.code === pm.type.code);

        return currentType
          ? {
              ...pm,
              type: currentType,
            }
          : pm;
      })*/,
    },
    onRequest: {
      save: handlePlanSave,
      edit: handlePlanEdit,
      delete: handlePlanDelete,
      next: handlePlanSearch,
      changeStatut: handlePlanChangeStatut,
      fetchMore: handleFetchMorePlans,
    },
    listType: listTypePlan,
    listStatutPlan,
    listModeReglement: loadingReglementMode.data?.pRTReglementModes.nodes,
    planToEdit: planToEdit,
    saving: saving,
    saved: saved,
    setSaved: () => {
      setSaved(false);
      setSaving(false);
    },
    miseAvants,
    produits,
    onRequestGoBack: handleGoBack,
    onRequestSubmitOperationStatut: handleSubmitOperationStatut,
    onSavedStatut: {
      saved: savedStatut,
      setSaved: (newSaved: boolean) => setSavedStatut(newSaved),
      saving: savingStatut,
    },
    factureOperationComponent: {
      component: factureOperationComponent,
      isValid: isFactureOperationValid(),
    },
  };

  return { onRequestPlanSearch, laboratoirePlan };
};
