import { useLazyQuery } from '@apollo/react-hooks';
import React, { useEffect } from 'react';
import { GET_REMUNERATION_SUIVI_OPERATIONNEL } from '../../../../../../federation/partenaire-service/pilotage/query';
import {
  GET_REMUNERATION_SUIVI_OPERATIONNELVariables,
  GET_REMUNERATION_SUIVI_OPERATIONNEL as GET_REMUNERATION_SUIVI_OPERATIONNEL_TYPE,
} from '../../../../../../federation/partenaire-service/pilotage/types/GET_REMUNERATION_SUIVI_OPERATIONNEL';
import { GET_PRESTATION_TYPE } from '../../../../../../federation/partenaire-service/remuneration/query';
import {
  GET_PRESTATION_TYPEVariables,
  GET_PRESTATION_TYPE as GET_PRESTATION_TYPE_TYPE,
} from '../../../../../../federation/partenaire-service/remuneration/types/GET_PRESTATION_TYPE';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';

export const useEvolutionRemuneration = ({ tab, idLabo, filterAnneeComparer }) => {
  const [loadRemunerationCompareCurrentDate, loadingRemunerationCompareCurrentDate] = useLazyQuery<
    GET_REMUNERATION_SUIVI_OPERATIONNEL_TYPE,
    GET_REMUNERATION_SUIVI_OPERATIONNELVariables
  >(GET_REMUNERATION_SUIVI_OPERATIONNEL, { client: FEDERATION_CLIENT });

  const [loadRemunerationCompareDate, loadingRemunerationCompareDate] = useLazyQuery<
    GET_REMUNERATION_SUIVI_OPERATIONNEL_TYPE,
    GET_REMUNERATION_SUIVI_OPERATIONNELVariables
  >(GET_REMUNERATION_SUIVI_OPERATIONNEL, { client: FEDERATION_CLIENT });

  const [loadPrestationType, loadingPrestationType] = useLazyQuery<
    GET_PRESTATION_TYPE_TYPE,
    GET_PRESTATION_TYPEVariables
  >(GET_PRESTATION_TYPE, { client: FEDERATION_CLIENT });

  const dataRemunerationEvolutionCurrent =
    loadingRemunerationCompareCurrentDate.data?.remunerationSuiviOperationnel;
  const dataRemunerationEvolutionPrecedent =
    loadingRemunerationCompareDate.data?.remunerationSuiviOperationnel;

  useEffect(() => {
    if (tab === 'pilotage' || tab === 'pilotages') {
      loadRemunerationCompareCurrentDate({
        variables: {
          input: {
            idPartenaireTypeAssocie: tab === 'pilotage' ? idLabo : tab === 'pilotages' ? '' : '',
            partenaireType: 'LABORATOIRE',
            dateDebut: `${filterAnneeComparer}-01-01`,
          },
        },
      });
      loadRemunerationCompareDate({
        variables: {
          input: {
            idPartenaireTypeAssocie: tab === 'pilotage' ? idLabo : tab === 'pilotages' ? '' : '',
            partenaireType: 'LABORATOIRE',
            dateDebut: `${filterAnneeComparer - 1}-01-01`,
          },
        },
      });
      loadPrestationType();
    }
  }, [tab, filterAnneeComparer]);

  const array_vide = new Array(6);

  const serieRealise = (loadingPrestationType.data?.prestationTypes.nodes || []).map(type => {
    const realisePrecedent = (dataRemunerationEvolutionPrecedent?.types || []).map(element => {
      if (element.prestation.id === type.id) {
        return element.totalMontantReglementPrestation;
      }
    });
    const realiseCurrent = (dataRemunerationEvolutionCurrent?.types || []).map(element => {
      if (element.prestation.id === type.id) {
        return element.totalMontantReglementPrestation;
      }
    });
    return [realisePrecedent, realiseCurrent];
  });

  const prevuRealise = (loadingPrestationType.data?.prestationTypes.nodes || []).map(type => {
    const prevuPrecedent = (dataRemunerationEvolutionPrecedent?.types || []).map(element => {
      if (element.prestation.id === type.id) {
        return element.totalMontantPrevuPrestation;
      }
    });
    const prevuCurrent = (dataRemunerationEvolutionCurrent?.types || []).map(element => {
      if (element.prestation.id === type.id) {
        return element.totalMontantPrevuPrestation;
      }
    });
    return [prevuPrecedent, prevuCurrent];
  });

  const totalPrevu = {
    precedentPrevu: dataRemunerationEvolutionPrecedent?.totalMontantPrevues || 0,
    currentPrevu: dataRemunerationEvolutionCurrent?.totalMontantPrevues || 0,
  };

  const totalRealise = {
    precedentRealise: dataRemunerationEvolutionPrecedent?.totalMontantReglements || 0,
    currentRealise: dataRemunerationEvolutionCurrent?.totalMontantReglements || 0,
  };

  const series = [
    {
      name: 'Prévue',
      type: 'column',
      data: prevuRealise
        .flat(4)
        .filter(el => {
          return el != null;
        })
        .concat([totalPrevu.precedentPrevu, totalPrevu.currentPrevu]) as any,
    },
    {
      name: 'Réalisée',
      type: 'column',
      data: serieRealise
        .flat(4)
        .filter(el => {
          return el != null;
        })
        .concat([totalRealise.precedentRealise, totalRealise.currentRealise]) as any,
    },
  ];

  const xaxis = Array.from(array_vide, (_, i) =>
    i % 2 === 0 ? filterAnneeComparer - 1 : filterAnneeComparer,
  );

  const optionsOperationCommercialCurrent = {
    title: 'Évolution de la rémunération',
    xaxis,
    colorBar: ['#FFE43A', '#63B8DD'],
    withRefPrecedent: true,
  };

  const totalOperationCommercialCurrentProps = {
    series,
    options: optionsOperationCommercialCurrent,
  };

  const evolutionRemunerationProps = {
    totalOperationCommercialCurrentProps,
  };

  const data = {
    loadingRemunerationCompareCurrentDate,
    loadingRemunerationCompareDate,
  };

  return { evolutionRemunerationProps, data };
};
