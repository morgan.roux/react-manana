import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    partenaireContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      '& > p': {
        fontFamily: 'Montserrat',
      },
    },
    partenaireName: {
      letterSpacing: 0,
      fontSize: 24,
      fontWeight: 600,
    },
    partenaireAdresse: {
      letterSpacing: 0.24,
      fontSize: 14,
    },
    imgFormContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'center',
      '& > div > div': {
        height: 300,
        border: '1px dashed #DCDCDC',
        alignItems: 'flex-start',
        '& img': {
          width: 225,
          height: 225,
          borderRadius: '50%',
        },
      },
      [theme.breakpoints.down('sm')]: {
        '& .MuiFormGroup-root': {
          flexDirection: 'column',
          margin: 'auto',
        },
      },
    },
    radioGroup: {
      display: 'flex',
      alignItems: 'center',
      '& label, & p': {
        marginRight: 50,
      },
      marginBottom: 15,
    },
    mutationSuccessContainer: {
      width: '100%',
      minHeight: 'calc(100vh - 156px)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
      '& img': {
        width: 350,
        height: 270,
      },

      '& p': {
        textAlign: 'center',
        maxWidth: 600,
      },
      '& > div > p:nth-child(2)': {
        fontSize: 30,
        fontWeight: 'bold',
      },
      '& > div > p:nth-child(3)': {
        fontSize: 16,
        fontWeight: 'normal',
      },
      '& button': {
        marginTop: 30,
      },
    },
  }),
);

export default useStyles;
