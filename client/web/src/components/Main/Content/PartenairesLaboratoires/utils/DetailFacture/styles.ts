import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      paddingLeft: 24,
      backgroundColor: theme.palette.common.white,
    },
  }),
);
