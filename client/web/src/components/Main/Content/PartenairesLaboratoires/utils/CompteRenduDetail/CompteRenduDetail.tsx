import React, { FC, memo, useCallback, useEffect, useRef, useState } from 'react';
import { useStyles } from './styles';
import { GetApp, Reply } from '@material-ui/icons';
import 'moment/locale/fr';
import moment from 'moment';
import { Avatar, Box } from '@material-ui/core';
import { AvatarGroup } from '@material-ui/lab';
import { Column, CustomButton, Table } from '@app/ui-kit/components/atoms';
import { CompteRendu } from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/CompteRendu/interface';
import { drawDOM, exportPDF } from '@progress/kendo-drawing';
import { CustomContainer } from '@app/ui-kit';
import { savePDF } from '@progress/kendo-react-pdf';
import '@progress/kendo-theme-default/dist/all.css';

const conditionAndPlanColumn: Column[] = [
  {
    name: 'type',
    label: 'Type',
  },
  {
    name: 'titre',
    label: 'Titre',
  },
  {
    name: 'periode',
    label: 'Période',
  },
  {
    name: 'description',
    label: 'Déscription',
  },
];

const suiviColumn: Column[] = [
  {
    name: 'type',
    label: 'Type',
  },
  {
    name: 'titre',
    label: 'Titre',
  },
  {
    name: 'participants',
    label: 'Collaborateurs',
  },
  {
    name: 'responsables',
    label: 'Représentant Labo',
  },
  {
    name: 'date',
    label: 'Date',
  },
  {
    name: 'description',
    label: 'Déscription',
  },
];

const makePdf = (html: any, fileName: string) => {
  return savePDF(html, {
    paperSize: 'auto',
    fileName: `${fileName}.pdf`,
    margin: 3,
  });
};

const getBase64pdf = (element: any) => {
  return drawDOM(element, {
    paperSize: 'auto',
  })
    .then(group => {
      return exportPDF(group);
    })
    .then(dataUri => {
      return dataUri.split(';base64,')[1];
    });
};

export interface PharmacieInformation {
  nom: string;
  adresse: string;
  email?: string;
  telephone?: string;
}

export interface LaboratoireInformation {
  nom: string;
  adresse: string;
  email?: string;
  telephone?: string;
  representantLabo?: {
    nom: string;
    telephone?: string;
  };
}

export interface SendEmailInput {
  subject?: string;
  to: string[];
  from?: string;
  cc?: string;
  html?: string;
  templateId?: string;
  templateData?: any;
  attachment?: AttachmentInput;
  compteRendu: CompteRendu;
}

export interface AttachmentInput {
  content: string;
  filename: string;
  type: string;
}

export interface CompteRenduDetailProps {
  compteRenduValue?: CompteRendu;
  laboratoireInformation?: LaboratoireInformation;
  pharmaciInformation?: PharmacieInformation;
  dateProchainRendezVous?: Date;
  onRequestSendMail?: (email: SendEmailInput) => void;
  push?: any;
  idLabo?: any;
  onSideNavListClick?: (route: string) => void;
  sendingEmail?: boolean;
  isEnvoiEmail?: boolean;
}
const keyTel = 'Tél';
const keyTel2 = 'Tél#';
const showVide = true;

const CompteRenduDetailComponnent: FC<CompteRenduDetailProps> = ({
  compteRenduValue,
  laboratoireInformation,
  pharmaciInformation,
  dateProchainRendezVous,
  onRequestSendMail,
  push,
  idLabo,
  onSideNavListClick,
  sendingEmail,
  isEnvoiEmail,
}) => {
  const classes = useStyles();
  const [pharmacieInformationValue, setPharmacieInformation] = useState<any>();
  const [laboInfo, setLaboInfo] = useState<any>();
  const [compteRendu, setCompteRendu] = useState<CompteRendu | undefined>();

  console.log('--------------sendingEmail-----------: ', sendingEmail);

  useEffect(() => {
    moment.locale('fr');
    setCompteRendu(compteRenduValue);
    // if (!compteRenduValue) {
    //   onSideNavListClick && onSideNavListClick('compte-rendu');
    // }
  }, [compteRenduValue]);

  useEffect(() => {
    setPharmacieInformation({
      Adresse: pharmaciInformation?.adresse || '',
      Email: pharmaciInformation?.email || '',
      [keyTel]: pharmaciInformation?.telephone || '',
    });
  }, [pharmaciInformation]);

  useEffect(() => {
    setLaboInfo({
      Laboratoire: laboratoireInformation?.nom,
      Adresse: laboratoireInformation?.adresse || '',
      Email: laboratoireInformation?.email || '',
      [keyTel]: laboratoireInformation?.telephone || '',
      'Représentant Labo':
        compteRendu &&
        compteRendu.responsables &&
        compteRendu.responsables.length > 0 &&
        compteRendu.responsables[0].nom,
      [keyTel2]:
        (compteRendu &&
          compteRendu.responsables &&
          compteRendu.responsables.length > 0 &&
          compteRendu.responsables[0].contact?.telPerso) ||
        compteRendu?.responsables[0].contact?.telProf ||
        '',
    });
  }, [laboratoireInformation, compteRendu]);

  const componentToPrint = useRef(null);

  const handleDownloadClick = useCallback(() => {
    makePdf(
      componentToPrint.current,
      `compteRendu${moment(compteRendu?.createdAt).format('DD-MM-YYYY')}`,
    );
  }, [compteRendu, componentToPrint]);

  const handleSendMail = useCallback(
    async compteRendu => {
      const base64pdf = await getBase64pdf(componentToPrint.current);
      onRequestSendMail &&
        onRequestSendMail({
          subject: 'Compte rendu du visite de laboratoire',
          to: [
            compteRendu.responsables[0].contact?.mailPerso || '',
            compteRendu.responsables[0].contact?.mailProf || '',
          ].filter(e => e !== ''),
          html: compteRendu.rapportVisite,
          attachment: {
            type: 'application/pdf',
            content: base64pdf,
            filename: `compte-rendu-${moment(compteRendu.createdAt).format('DD-MM-YYYY')}`,
          },
          compteRendu,
        });
    },
    [compteRendu, componentToPrint],
  );

  return (
    <div>
      <CustomContainer
        filled
        bannerContentStyle={{
          justifyContent: 'center',
        }}
        bannerBack
        onBackClick={() => onSideNavListClick && onSideNavListClick('compte-rendu')}
        bannerTitle="Détail compte rendu"
        contentStyle={{
          width: '100%',
          padding: '0 !important',
        }}
        onlyBackAndTitle
      >
        <div className={classes.root}>
          <div className={classes.btnContainer}>
            <CustomButton
              style={{ marginRight: 24 }}
              onClick={handleDownloadClick}
              startIcon={<GetApp />}
              color="secondary"
            >
              TELECHARGER
            </CustomButton>
            {compteRendu &&
              compteRendu.responsables &&
              compteRendu.responsables.length > 0 &&
              isEnvoiEmail && (
                <CustomButton
                  color="secondary"
                  onClick={() => {
                    handleSendMail(compteRendu);
                  }}
                  startIcon={<Reply style={{ transform: 'scaleX(-1)' }} />}
                >
                  ENVOYER PAR EMAIL
                </CustomButton>
              )}
          </div>
          <div className={classes.page}>
            <div className={classes.pdfContainer} ref={componentToPrint}>
              <h1>COMPTE RENDU DU VISITE DE LABORATOIRE</h1>
              <div className={classes.headerPage}>
                <div className={classes.pharmacieInformation}>
                  <div className={classes.bordered}>
                    <h2>{pharmaciInformation?.nom}</h2>
                    {pharmacieInformationValue &&
                      Object.entries(pharmacieInformationValue).map(([label, value]) => {
                        return value || showVide ? (
                          <div key={label}>
                            <span className={classes.label}>{label}&nbsp;:</span>&nbsp;&nbsp;
                            <span className={classes.value}>{value as any}</span>
                          </div>
                        ) : (
                          ''
                        );
                      })}
                  </div>
                  <div className={classes.dateContainer}>
                    Le {moment(compteRendu?.createdAt).format('LL')}
                  </div>
                </div>
                <div className={classes.laboratoireInformation}>
                  {laboInfo &&
                    Object.entries(laboInfo).map(([label, value]) => {
                      return value || showVide ? (
                        <div key={label}>
                          <span className={classes.label}>{label.replace('#', '')}&nbsp;:</span>
                          &nbsp;&nbsp;
                          <span className={classes.value}>{value as any}</span>
                        </div>
                      ) : (
                        ''
                      );
                    })}
                  <br />
                  <div>
                    <span className={classes.label}>Prochain rendez-vous&nbsp;:</span>
                    &nbsp;&nbsp;
                    <span className={classes.value}>
                      {moment(dateProchainRendezVous).format('LL')}
                    </span>
                  </div>
                </div>
              </div>
              <div className={classes.bodyContainer}>
                <div className={classes.containerInformation}>
                  <h3>Rapport de visite :</h3>
                  <span dangerouslySetInnerHTML={{ __html: compteRendu?.rapportVisite || '' }} />
                </div>
                <h3 className={classes.titre3}>1 - Actions</h3>
                <div className={classes.ActionContainer}>
                  <Table
                    search={false}
                    columns={suiviColumn}
                    data={(compteRendu?.suiviOperationnels || []).map(item => ({
                      type: <span className={classes.textInTable}>{item.type?.libelle}</span>,
                      titre: <span className={classes.textInTable}>{item.titre}</span>,
                      participants:
                        item.participants && item.participants.length > 0 ? (
                          <AvatarGroup max={4}>
                            {item.participants.map((user: any) => (
                              <Avatar
                                key={user.id}
                                src={user.photo.publicUrl || user.photo.avatar?.fichier?.publicUrl}
                                alt={user.id}
                              />
                            ))}
                          </AvatarGroup>
                        ) : (
                          ''
                        ),
                      responsables:
                        item.contacts && item.contacts.length > 0 ? (
                          <Box display="flex" alignItems="center">
                            {/* <Avatar src={item.contacts[0].photo?.publicUrl} /> */}
                            &nbsp;&nbsp;
                            <span>
                              {item.contacts[0].nom}&nbsp;{item.contacts[0].prenom}
                            </span>
                          </Box>
                        ) : (
                          ''
                        ),
                      date: (
                        <span className={classes.textInTable}>
                          {moment(item.dateHeure).format('DD/MM/YYYY')}
                        </span>
                      ),
                    }))}
                    notablePagination
                  />
                </div>
                <h3 className={classes.titre3}>2 - Accords</h3>
                <div className={classes.AccordContainer}>
                  <h4 className={classes.titre3}>a - Conditions commerciales</h4>
                  <Table
                    search={false}
                    columns={conditionAndPlanColumn}
                    data={(compteRendu?.conditionsCommerciales || []).map(item => ({
                      type: <span className={classes.textInTable}>{item.type?.libelle || ''}</span>,
                      titre: <span className={classes.textInTable}>{item.titre || ''}</span>,
                      periode: (
                        <span className={classes.textInTable}>{`Du ${moment(item.dateDebut).format(
                          'DD/MM/YYYY',
                        )} au ${moment(item.dateFin).format('DD/MM/YYYY')}`}</span>
                      ),
                      description: (
                        <span dangerouslySetInnerHTML={{ __html: item?.description || '' }} />
                      ),
                    }))}
                    notablePagination
                    style={{ marginBottom: 32 }}
                  />
                  <h4 className={classes.titre3}>b - Plan Marketing</h4>
                  <Table
                    search={false}
                    columns={conditionAndPlanColumn}
                    data={(compteRendu?.planMarketings || []).map(item => ({
                      type: <span className={classes.textInTable}>{item.type?.libelle || ''}</span>,
                      titre: <span className={classes.textInTable}>{item.titre || ''}</span>,
                      periode: (
                        <span className={classes.textInTable}>{`Du ${moment(item.dateDebut).format(
                          'DD/MM/YYYY',
                        )} au ${moment(item.dateFin).format('DD/MM/YYYY')}`}</span>
                      ),
                      description: (
                        <span dangerouslySetInnerHTML={{ __html: item?.description || '' }} />
                      ),
                    }))}
                    notablePagination
                  />
                </div>
                <div className={classes.containerInformation}>
                  <h3>Nouveauté :</h3>
                  <span>
                    Remise d'échantillon : {compteRendu?.remiseEchantillon ? 'Oui' : 'Non'}
                  </span>
                </div>
                <div className={classes.containerInformation}>
                  <h3>Gestion des périmés :</h3>
                  <span dangerouslySetInnerHTML={{ __html: compteRendu?.gestionPerime || '' }} />
                </div>
                <div className={classes.containerInformation}>
                  <h3>Commentaire :</h3>
                  <span dangerouslySetInnerHTML={{ __html: compteRendu?.conclusion || '' }} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </CustomContainer>
    </div>
  );
};

export const CompteRenduDetail = memo(CompteRenduDetailComponnent);
