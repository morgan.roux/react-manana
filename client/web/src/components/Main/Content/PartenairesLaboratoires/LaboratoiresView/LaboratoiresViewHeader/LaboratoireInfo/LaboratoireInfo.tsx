import React, { FC, useContext, useState } from 'react';
import { LocationOn, Phone, PhoneAndroid, Language, Person } from '@material-ui/icons';
import useStyles from './styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import IconFax from '../../../../../../../assets/img/Icon-awesome-fax.svg';
import image from '../../../../../../../assets/img/pas_image.png';
import { useDropzone } from 'react-dropzone';
import { SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire } from '../../../../../../../graphql/Laboratoire/types/SEARCH_LABORATOIRE_PARTENAIRE';
import { useApolloClient, useMutation } from '@apollo/react-hooks';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../../../graphql/S3';
import { CustomModal } from '../../../../../../Common/CustomModal';
import { CustomReactEasyCrop } from '../../../../../../Common/CustomReactEasyCrop';
import { uploadToS3 } from '../../../../../../../services/S3';
import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';
import { DO_CREATE_UPDATE_PHOTO_LABORATOIRE } from '../../../../../../../graphql/Laboratoire/mutation';
import {
  CREATE_UPDATE_PHOTO_LABORATOIRE,
  CREATE_UPDATE_PHOTO_LABORATOIREVariables,
} from '../../../../../../../graphql/Laboratoire/types/CREATE_UPDATE_PHOTO_LABORATOIRE';
import { TypeFichier } from '../../../../../../../types/graphql-global-types';
import { formatFilenameWithoutDate } from '../../../../../../../utils/filenameFormater';
import { ContentContext, ContentStateInterface } from '../../../../../../../AppContext';
import { last } from 'lodash';
import Backdrop from '../../../../../../Common/Backdrop';
import CustomButton from '../../../../../../Common/CustomButton';
import Form from '../../../../../../Dashboard/Laboratoire/CreateUpdate/Form/Form';

interface LaboratoireInfoProps {
  currentLabo: any | undefined;
  refetch?: () => void;
}

const LaboratoireInfo: FC<LaboratoireInfoProps> = ({ currentLabo, refetch }) => {
  const classes = useStyles({});
  const [openForm, setOpenForm] = useState<boolean>(false);
  const [openResize, setOpenResize] = useState<boolean>(false);
  const [croppedImgUrl, setCroppedImgUrl] = useState<any>('');
  const [savedCroppedImage, setSavedCroppedImage] = useState<boolean>(false);
  const [croppedFiles, setCroppedFiles] = useState<File[]>([]);
  const [file, setFile] = useState<File>();
  const client = useApolloClient();

  const avatarImageToS3 = croppedFiles && croppedFiles.length > 0 ? croppedFiles[0] : undefined;

  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (
        data &&
        data.createPutPresignedUrls &&
        data.createPutPresignedUrls.length > 0 &&
        avatarImageToS3
      ) {
        const photo = data.createPutPresignedUrls[0];
        if (photo && photo.filePath && photo.presignedUrl) {
          await uploadToS3(avatarImageToS3, photo.presignedUrl)
            .then(async result => {
              if (result && result.status === 200) {
                await createUpdatePhoto({
                  variables: {
                    id: (currentLabo && currentLabo.id) || '',
                    photo: {
                      chemin: photo.filePath,
                      nomOriginal: avatarImageToS3.name,
                      type: TypeFichier.AVATAR,
                    },
                  },
                }).then(_ => refetch && refetch());
                setOpenResize(false);
                setFile(undefined);
                setCroppedFiles([]);
                setSavedCroppedImage(false);
              }
            })
            .catch(error => {
              console.error(error);
              setOpenResize(false);
              setFile(undefined);
              setCroppedFiles([]);
              setSavedCroppedImage(false);
              displaySnackBar(client, {
                type: 'ERROR',
                message:
                  "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
                isOpen: true,
              });
            });
        }
      }
    },
  });

  const [createUpdatePhoto, { loading }] = useMutation<
    CREATE_UPDATE_PHOTO_LABORATOIRE,
    CREATE_UPDATE_PHOTO_LABORATOIREVariables
  >(DO_CREATE_UPDATE_PHOTO_LABORATOIRE, {
    onCompleted: data => {
      if (data && data.updatePhotoLaboratoire) {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: `Photo modifié avec succès`,
          isOpen: true,
        });
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
      displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
    },
  });

  React.useEffect(() => {
    if (!openResize) {
      setOpenResize(false);
      setFile(undefined);
      setCroppedFiles([]);
      setSavedCroppedImage(false);
    }
  }, [openResize]);

  React.useEffect(() => {
    if (croppedFiles && croppedFiles.length > 0 && file) {
      const filePathsTab: string[] = [];
      croppedFiles.map((file: File) => {
        filePathsTab.push(
          `laboratoires/${currentLabo && currentLabo.id}/${formatFilenameWithoutDate(file)}`,
        );
      });
      doCreatePutPresignedUrl({ variables: { filePaths: filePathsTab } });
    }
  }, [croppedFiles]);

  const onClickSave = () => {
    setSavedCroppedImage(true);
  };

  const onDropAccepted = (acceptedFiles: File[]) => {
    if (acceptedFiles && acceptedFiles.length) {
      setFile(acceptedFiles[0]);
      setOpenResize(true);
    }
  };

  const { getRootProps, getInputProps, open } = useDropzone({
    disabled: false,
    noClick: true,
    noKeyboard: true,
    multiple: true,
    accept: 'image/*',
    onDropAccepted,
  });

  const avatarImageSrc = (file && URL.createObjectURL(file)) || '';

  const handleWebSiteClick = () => {
    if (currentLabo && currentLabo.laboSuite && currentLabo.laboSuite.webSiteUrl) {
      window.open(currentLabo.laboSuite.webSiteUrl, '_blank');
    }
  };

  return (
    <Box style={{ padding: '20px', display: 'flex' }}>
      {(loading || savedCroppedImage) && <Backdrop />}
      <Box className={classes.contentAvatar} onClick={open}>
        <img
          src={(currentLabo && currentLabo.photo && currentLabo.photo.publicUrl) || image}
          className={classes.avatar}
        />
        <div {...getRootProps()} className={classes.dropzone}>
          <input {...getInputProps()} />
        </div>
      </Box>
      <Box
        display="flex"
        flexDirection="column"
        justifyContent="space-between"
        style={{ paddingLeft: '20px', width: '100%' }}
      >
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <Typography variant="h2" className={classes.title}>
            {currentLabo ? currentLabo.nomLabo : ''}
          </Typography>
          <CustomButton color="secondary" onClick={() => setOpenForm(true)}>
            Modifier
          </CustomButton>
        </div>
        <Box display="flex" alignItems="center">
          <Typography className={classes.span}>
            <LocationOn className={classes.toGrey} fontSize="small" />
            {currentLabo && currentLabo.laboSuite && currentLabo.laboSuite.adresse
              ? currentLabo.laboSuite.adresse
              : 'Non renseigné'}
          </Typography>
          <Typography className={classes.span}>
            <Phone className={classes.toGrey} fontSize="small" />{' '}
            {currentLabo && currentLabo.laboSuite && currentLabo.laboSuite.telephone
              ? currentLabo.laboSuite.telephone
              : 'Non renseigné'}
          </Typography>
        </Box>
        <Box className={classes.links}>
          <Button variant="outlined" className={classes.bouton} onClick={handleWebSiteClick}>
            <Language className={classes.toWhite} fontSize="small" />
            Site internet
          </Button>
          <Button variant="outlined" className={classes.bouton}>
            <Person className={classes.toWhite} fontSize="small" />
            Représentant
          </Button>
        </Box>
      </Box>
      <CustomModal
        open={openResize}
        setOpen={setOpenResize}
        title="Redimensionnement"
        onClickConfirm={onClickSave}
        actionButton="Enregistrer"
        closeIcon={true}
        centerBtns={true}
      >
        {avatarImageSrc ? (
          <CustomReactEasyCrop
            src={avatarImageSrc}
            withZoom={true}
            croppedImage={croppedImgUrl}
            setCroppedImage={setCroppedImgUrl}
            setCropedFiles={setCroppedFiles}
            savedCroppedImage={savedCroppedImage}
            customCropSize={{ width: 300, height: 300 }}
          />
        ) : (
          <div>Aucune Image</div>
        )}
      </CustomModal>
      <CustomModal
        open={openForm}
        setOpen={setOpenForm}
        title="Modifier laboratoire"
        actionButton="Enregistrer"
        withBtnsActions={false}
      >
        <Form
          laboratoire={currentLabo}
          isEdit={true}
          goBack={() => {
            setOpenForm(false);
            if (refetch) refetch();
          }}
        />
      </CustomModal>
    </Box>
  );
};

export default LaboratoireInfo;
