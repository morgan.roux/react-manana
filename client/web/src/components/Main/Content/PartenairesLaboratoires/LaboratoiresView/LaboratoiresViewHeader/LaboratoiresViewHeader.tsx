import React, { FC } from 'react';
import useStyles from './styles';
import Divider from '@material-ui/core/Divider';
import LaboratoireInfo from './LaboratoireInfo/LaboratoireInfo';
import LaboratoireMenu from './LaboratoireMenu/LaboratoireMenu';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire } from '../../../../../../graphql/Laboratoire/types/SEARCH_LABORATOIRE_PARTENAIRE';

interface LaboratoiresViewHeaderProps {
  currentLabo: any | undefined;
  currentView: string;
  refetch?: () => void;
}

const LaboratoiresViewHeader: FC<LaboratoiresViewHeaderProps &
  RouteComponentProps<any, any, any>> = ({
  currentLabo,
  currentView,
  match: { params },
  history,
  refetch,
}) => {
  const classes = useStyles({});
  const { idLabo } = params;

  const handleChange = (view: string) => {
    history.push(`/laboratoires/${idLabo}/${view}`);
  };

  return (
    <div style={{ background: 'rgb(0, 43, 81)' }}>
      <LaboratoireInfo currentLabo={currentLabo} refetch={refetch} />
      <LaboratoireMenu
        currentLabo={currentLabo}
        handleClick={handleChange}
        currentView={currentView}
      />
    </div>
  );
};

export default withRouter(LaboratoiresViewHeader);
