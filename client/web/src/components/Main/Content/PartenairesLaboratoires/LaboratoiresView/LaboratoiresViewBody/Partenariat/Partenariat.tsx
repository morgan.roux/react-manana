import {
  List,
  ListItem,
  Divider,
  ListItemIcon,
  ListItemText,
  Box,
  Typography,
} from '@material-ui/core';
import moment from 'moment';
import React, { FC, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { StatutPartenaire, TypePartenaire } from '../../../../../../../types/graphql-global-types';
import CustomButton from '../../../../../../Common/CustomButton';
import DefinePartenariat from './Modal/DefinePartenariat';
import useStyles from './styles';

interface PartenariatProps {
  laboratoire: any;
  refetchLabo?: any;
  open: boolean;
  setOpen: (value: boolean) => void;
}

const Partenariat: FC<PartenariatProps & RouteComponentProps> = ({
  laboratoire,
  refetchLabo,
  open,
  setOpen,
}) => {
  const classes = useStyles({});
  const partenaire = laboratoire && laboratoire.laboratoirePartenaire;

  const typePartenaire =
    partenaire && partenaire.typePartenaire === TypePartenaire.NEGOCIATION_EN_COURS
      ? 'Négociation en cours'
      : partenaire && partenaire.typePartenaire === TypePartenaire.NON_PARTENAIRE
      ? 'Non partenaire'
      : partenaire && partenaire.typePartenaire === TypePartenaire.NOUVEAU_REFERENCEMENT
      ? 'Nouveau Référencement'
      : 'Partenariat actif';
  const handleClick = () => {
    setOpen(true);
  };

  return (
    <>
      <Box className={classes.tableContainer}>
        <Box width="100%" display="flex" flexDirection="row" justifyContent="space-between">
          <CustomButton color="secondary" onClick={handleClick}>
            {partenaire && partenaire.id ? 'Modifier le partenariat' : 'Définir comme partenaire'}
          </CustomButton>
        </Box>
        {partenaire && (
          <List className={classes.list}>
            <Box>
              <ListItem className={classes.listItem}>
                <ListItemIcon>Date début</ListItemIcon>
                <ListItemText
                  primary={
                    (partenaire &&
                      partenaire.debutPartenaire &&
                      moment(partenaire.debutPartenaire).format('L')) ||
                    '-'
                  }
                />
              </ListItem>
              <Divider />
            </Box>
            <Box>
              <ListItem className={classes.listItem}>
                <ListItemIcon>Date Fin</ListItemIcon>
                <ListItemText
                  primary={
                    (partenaire &&
                      partenaire.finPartenaire &&
                      moment(partenaire.finPartenaire).format('L')) ||
                    '-'
                  }
                />
              </ListItem>
              <Divider />
            </Box>
            <Box>
              <ListItem className={classes.listItem}>
                <ListItemIcon>Type de partenariat</ListItemIcon>
                <ListItemText primary={typePartenaire || '-'} />
              </ListItem>
              <Divider />
            </Box>
          </List>
        )}
      </Box>
      <DefinePartenariat
        laboratoire={laboratoire || ''}
        open={open}
        setOpen={setOpen}
        refetchLabo={refetchLabo}
      />
    </>
  );
};
export default withRouter(Partenariat);
