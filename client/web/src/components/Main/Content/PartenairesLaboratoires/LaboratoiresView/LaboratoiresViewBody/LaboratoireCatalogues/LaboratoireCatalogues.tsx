import React, { FC, useState } from 'react';
import useStyles from './styles';
import Grid from '@material-ui/core/Grid';
import Article from '../../../../Panier/Article/Article';
import { Box, CircularProgress, Typography } from '@material-ui/core';
import { useQuery } from '@apollo/react-hooks';
import { GET_CURRENT_PHARMACIE } from '../../../../../../../graphql/Pharmacie/local';
import { SEARCH } from '../../../../../../../graphql/search/query';
import ICurrentPharmacieInterface from '../../../../../../../Interface/CurrentPharmacieInterface';
import {
  SEARCH as SEARCH_Interface,
  SEARCHVariables,
} from '../../../../../../../graphql/search/types/SEARCH';
import { myPanier, myPanierVariables } from '../../../../../../../graphql/Panier/types/myPanier';
import { GET_MY_PANIER } from '../../../../../../../graphql/Panier/query';
import { Panier } from '../../../../../../../graphql/Panier/types/Panier';
import CaptureIne from '../../../../../../../assets/img/image_default22.png';
import { SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire } from '../../../../../../../graphql/Laboratoire/types/SEARCH_LABORATOIRE_PARTENAIRE';

interface LaboratoireCataloguesProps {
  currentLabo: SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire | undefined;
}

const LaboratoireCatalogues: FC<LaboratoireCataloguesProps> = ({ currentLabo }) => {
  const classes = useStyles({});

  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(12);

  // take currentPharmacie
  const { data: pharmacieData } = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

  const currentPharmacie = pharmacieData ? pharmacieData.pharmacie : null;
  const myPanierResult = useQuery<myPanier, myPanierVariables>(GET_MY_PANIER, {
    variables: {
      idPharmacie: (currentPharmacie && currentPharmacie.id) || '',
      take: 10,
      skip: 0,
    },
    fetchPolicy: 'cache-and-network',
  });

  // currentPanier
  const currentPanier: Panier | null =
    (myPanierResult && myPanierResult.data && myPanierResult.data.myPanier) || null;

  const { data: listProductData, loading, error } = useQuery<SEARCH_Interface, SEARCHVariables>(
    SEARCH,
    {
      variables: {
        type: ['produitcanal'],
        skip,
        take,
        query: {
          query: {
            constant_score: {
              filter: {
                bool: {
                  must: [
                    {
                      terms: {
                        'produit.produitTechReg.laboExploitant.id': currentLabo
                          ? [currentLabo.id]
                          : [],
                      },
                    },
                    {
                      term: { isActive: true },
                    },
                  ],
                },
              },
            },
          },
          sort: [{ 'produit.libelle': { order: 'asc' } }],
        },
        idPharmacie: (currentPharmacie && currentPharmacie.id) || '',
      },
      skip: !currentPharmacie,
    },
  );

  if (loading) {
    return (
      <Box display="flex" justifyContent="center" alignItems="center" height="100%" width="100%">
        <CircularProgress variant="indeterminate" color="secondary" />
      </Box>
    );
  }

  return (
    <Grid className={classes.cont}>
      {listProductData &&
      listProductData.search &&
      listProductData.search.data &&
      listProductData.search.data.length > 0 ? (
        <Box className={classes.panierListe}>
          {listProductData.search.data.map((produitCanal: any | null) => (
            <Article
              key={produitCanal.id}
              currentPanier={currentPanier}
              currentPharmacie={currentPharmacie}
              currentCanalArticle={produitCanal}
            />
          ))}
        </Box>
      ) : (
        <Box
          display="flex"
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
          height="100%"
          width="100%"
        >
          <img className={classes.imgDefault} src={CaptureIne} />
          <Box>
            <Typography className={classes.title}>Aucun élément</Typography>
            <Typography className={classes.subTitle}>
              Cette section ne contient pas encore d'élément. <br /> Navré pour ce désagrement.
            </Typography>
          </Box>
        </Box>
      )}
    </Grid>
  );
};

export default LaboratoireCatalogues;
