import { Box, FormControlLabel, Radio, RadioGroup, Typography } from '@material-ui/core';
import { last } from 'lodash';
import { any } from 'prop-types';
import React, {
  Dispatch,
  FC,
  Fragment,
  SetStateAction,
  useCallback,
  useEffect,
  useState,
} from 'react';
import { AWS_HOST } from '../../../../../../../../../utils/s3urls';
import ChoiceAvatarForm from '../../../../../../../../Common/ChoiceAvatarForm';
import { CustomModal } from '../../../../../../../../Common/CustomModal';
import { CustomReactEasyCrop } from '../../../../../../../../Common/CustomReactEasyCrop';
import CustomSelect from '../../../../../../../../Common/CustomSelect';
import { CustomFormTextField } from '../../../../../../../../Common/CustomTextField';
import Dropzone from '../../../../../../../../Common/Dropzone';
import useStyles from '../styles';
import { address } from 'faker';
import { CIVILITE_LIST, SEXE_LIST } from '../../../../../../../../../Constant/user';
import useCommonStyles from '../../../../../../../../Dashboard/commonStyles';
import {
  FormInputInterface,
  InputInterface,
} from '../../../../../../../../Dashboard/GroupeClient/GroupeClientForm/GroupeClientForm';

interface FormStepOneProps {
  photo: any;
  setPhoto: Dispatch<SetStateAction<any>>;
  values: any;
  handleChange: any;
}

const FormStepOne: FC<FormStepOneProps> = ({ photo, setPhoto, values, handleChange }) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const {
    civilite,
    nom,
    prenom,
    sexe,
    fonction,
    idPartenaire,
    idPartenaireType,
    afficherComme,
    contact,
  } = values;

  const [openResize, setOpenResize] = useState<boolean>(false);
  const [croppedImgUrl, setCroppedImgUrl] = useState<any>('');
  const [savedCroppedImage, setSavedCroppedImage] = useState<boolean>(false);

  const [imgType, setImgType] = useState<'AVATAR' | 'IMPORT'>('AVATAR');
  const [selectedAvatar, setSelectedAvatar] = useState<any>(null);
  const [avatarList, setAvatarList] = useState<any[]>([]);

  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);
  const [croppedFiles, setCroppedFiles] = useState<File[]>([]);
  const [imageUrl, setImageUrl] = useState<string | null | undefined>(undefined);

  const [userPhotoSrc, setUserPhotoSrc] = useState<any>(null);

  useEffect(() => {
    if (photo && !photo.avatar) {
      setImgType('IMPORT');
    } else {
      setSelectedAvatar(photo && photo.avatar);
    }
  }, [photo]);

  const handleChangeRadioGroup = (event: React.ChangeEvent<HTMLInputElement>) => {
    setImgType((event.target as HTMLInputElement).value as any);
    setSelectedFiles([]);
    setCroppedFiles([]);
    setPhoto(null);
  };

  const deleteAvatar = () => {};

  const handleClickResize = useCallback(() => {
    setOpenResize(true);
    // Reset cropped image
    setSavedCroppedImage(false);
    setCroppedImgUrl(null);
  }, []);

  const onClickAvatar = (avatar: any) => {
    if (avatar) {
      const photo = { ...avatar.fichier, idAvatar: avatar.id };
      if (photo) setPhoto(photo as any);
    }
  };

  const saveResizedImage = () => {
    setOpenResize(false);
    setSavedCroppedImage(true);
  };

  /**
   * Set set userPhotoSrc
   */
  useEffect(() => {
    const image = selectedFiles && selectedFiles.length > 0 && last(selectedFiles);
    const photo = imageUrl || (image && URL.createObjectURL(image));
    if (photo) {
      setUserPhotoSrc(photo);
    }
  }, [imageUrl, selectedFiles]);

  /**
   * Set user photo variables
   */
  useEffect(() => {
    let newPhoto: File | undefined;
    if (croppedFiles && croppedFiles.length > 0) {
      newPhoto = last(croppedFiles);
    } else if (selectedFiles && selectedFiles.length > 0) {
      newPhoto = last(selectedFiles);
    }
    if (newPhoto) {
      setPhoto(newPhoto);
    }
  }, [croppedFiles, selectedFiles]);

  /**
   * Reset preview
   */
  useEffect(() => {
    if (selectedFiles && selectedFiles.length > 0) {
      // Reset cropped image
      setCroppedImgUrl(null);
      // Open Resize
      handleClickResize();
    }
  }, [selectedFiles]);

  /**
   * Set image url
   */
  useEffect(() => {
    if (photo && photo.chemin) {
      setImageUrl(`${AWS_HOST}/${photo.chemin}`);
    }

    if (selectedFiles && selectedFiles.length > 0) {
      const file = last(selectedFiles);
      if (file) {
        const url = URL.createObjectURL(file);
        setImageUrl(url);
      }
    }
  }, [photo, selectedFiles]);

  const FORM_INPUTS: FormInputInterface[] = [
    {
      title: 'Informations personnelles',
      inputs: [
        {
          label: 'Civilité',
          placeholder: 'Choisissez une civilité',
          type: 'select',
          name: 'civilite',
          value: civilite, // TODO
          inputType: 'text',
          selectOptions: CIVILITE_LIST, // TODO
          required: true,
        },
        {
          label: 'Nom',
          placeholder: 'Nom de la personne',
          type: 'text',
          name: 'nom',
          value: nom, // TODO
          required: true,
        },
        {
          label: 'Prénom',
          placeholder: 'Prénom de la personne',
          type: 'text',
          name: 'prenom',
          value: prenom, // TODO
        },
        {
          label: 'Fonction',
          placeholder: 'Fonction de la personne',
          type: 'text',
          name: 'fonction',
          value: fonction, // TODO
        },
      ],
    },
    {
      title: 'Coordonnées',
      inputs: [
        {
          label: 'Adresse',
          placeholder: 'Son adresse',
          type: 'text',
          name: 'contact.adresse1',
          value: contact && contact.adresse1, // TODO
        },
        {
          label: 'Ville',
          placeholder: 'Sa ville',
          type: 'text',
          name: 'contact.ville',
          value: contact && contact.ville, // TODO
        },
        {
          label: 'code postal',
          placeholder: 'Son code postal',
          type: 'text',
          name: 'contact.cp',
          value: contact && contact.cp, // TODO
        },
      ],
    },
  ];

  return (
    <Box maxWidth="816px" width="100%">
      <div className={classes.imgFormContainer}>
        <Typography className={commonClasses.inputTitle}>Photo</Typography>
        <RadioGroup
          row={true}
          aria-label="imgType"
          name="imgType"
          value={imgType}
          onChange={handleChangeRadioGroup}
          className={classes.radioGroup}
        >
          <FormControlLabel
            value="AVATAR"
            control={<Radio color="primary" />}
            label="Sélectionner un avatar"
          />
          <Typography>ou</Typography>
          <FormControlLabel
            value="IMPORT"
            control={<Radio color="primary" />}
            label="Télécharger une photo"
          />
        </RadioGroup>
        {imgType === 'AVATAR' ? (
          <ChoiceAvatarForm
            selectedAvatar={selectedAvatar}
            setSelectedAvatar={setSelectedAvatar}
            onClickAvatar={onClickAvatar}
            avatarList={avatarList}
            setAvatarList={setAvatarList}
          />
        ) : (
          <Dropzone
            contentText="Glissez et déposez ici votre photo"
            selectedFiles={selectedFiles}
            setSelectedFiles={setSelectedFiles}
            multiple={false}
            acceptFiles="image/*"
            withFileIcon={false}
            where="inUserSettings"
            withImagePreview={true}
            withImagePreviewCustomized={true}
            onClickDelete={deleteAvatar}
            fileAlreadySetUrl={croppedImgUrl || (imageUrl as any)}
            onClickResize={handleClickResize}
          />
        )}
      </div>
      <div className={commonClasses.formContainer}>
        {FORM_INPUTS.map((item: FormInputInterface, index: number) => {
          return (
            <Fragment key={`partenaire_contact_form_item_${index}`}>
              <Typography className={commonClasses.inputTitle}>{item.title}</Typography>
              <div className={commonClasses.inputsContainer}>
                {item.inputs.map((input: InputInterface, inputIndex: number) => {
                  return (
                    <div
                      className={commonClasses.formRow}
                      key={`partenaire_contact_form_input_${inputIndex}`}
                    >
                      <Typography className={commonClasses.inputLabel}>
                        {input.label} {input.required && <span>*</span>}
                      </Typography>
                      {input.type === 'select' ? (
                        <CustomSelect
                          label=""
                          list={input.selectOptions || []}
                          listId="id"
                          index="value" // TODO
                          name={input.name}
                          value={input.value}
                          onChange={handleChange}
                          shrink={false}
                          placeholder={input.placeholder}
                          withPlaceholder={true}
                        />
                      ) : (
                        <CustomFormTextField
                          name={input.name}
                          value={input.value}
                          onChange={handleChange}
                          placeholder={input.placeholder}
                          type={input.inputType}
                        />
                      )}
                    </div>
                  );
                })}
              </div>
            </Fragment>
          );
        })}
      </div>

      {/* Modal crop image */}
      <CustomModal
        open={openResize}
        setOpen={setOpenResize}
        title="Redimensionnement"
        onClickConfirm={saveResizedImage}
        actionButton="Enregistrer"
        closeIcon={true}
        centerBtns={true}
      >
        {userPhotoSrc ? (
          <CustomReactEasyCrop
            src={userPhotoSrc}
            withZoom={true}
            croppedImage={croppedImgUrl}
            setCroppedImage={setCroppedImgUrl}
            setCropedFiles={setCroppedFiles}
            savedCroppedImage={savedCroppedImage}
          />
        ) : (
          <div>Aucune Image</div>
        )}
      </CustomModal>
    </Box>
  );
};

export default FormStepOne;
