import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import { Box, IconButton, Typography } from '@material-ui/core';
import React, { ChangeEvent, FC, Fragment, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import CustomContent from '../../../../../../Common/newCustomContent';
import SearchInput from '../../../../../../Common/newCustomContent/SearchInput';
import WithSearch from '../../../../../../Common/newWithSearch/withSearch';
import useStyles from './styles';
import moment from 'moment';
import { getGroupement, getUser } from '../../../../../../../services/LocalStorage';
import { ME_me } from '../../../../../../../graphql/Authentication/types/ME';
import { AWS_HOST } from '../../../../../../../utils/s3urls';
import { Visibility } from '@material-ui/icons';
import { CustomModal } from '../../../../../../Common/CustomModal';
import { GET_SEARCH_CUSTOM_CONTENT_TICKET } from '../../../../../../../graphql/Ticket';
import {
  SEARCH_TICKET,
  SEARCH_TICKETVariables,
} from '../../../../../../../graphql/Ticket/types/SEARCH_TICKET';
import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';
import { GET_SEARCH_TICKET } from '../../../../../../../graphql/Ticket/query';
import TicketDetails from '../../../../../../Dashboard/AideEtSupport/Tickets/TicketDetails';
import Status from '../../../../../../Dashboard/AideEtSupport/Tickets/TicketStatus';
import CustomTable, { CustomTableColumn } from '../../../../Pilotage/Dashboard/Table/CustomTable';
import TicketForm from '../../../../../../Dashboard/AideEtSupport/Tickets/Form/TicketForm';
import CustomButton from '../../../../../../Common/CustomButton';

interface ReclamationsProps {
  idLaboratoire: string | null;
}

const Reclamations: FC<ReclamationsProps & RouteComponentProps> = ({
  location: { pathname },
  history: { push },
  idLaboratoire,
}) => {
  const classes = useStyles({});

  const groupement = getGroupement();
  const client = useApolloClient();

  const currentUser: ME_me = getUser();
  const currentUserName = (currentUser && currentUser.userName) || '';
  const [openModalForm, setOpenModalForm] = useState<boolean>(false);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [ticket, setTicket] = useState<any>(null);
  const [tickets, setTickets] = useState<any[]>([]);
  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(5);
  const [take, setTake] = useState<number>(5);
  const [skip, setSkip] = useState<number>(0);

  const { data, fetchMore } = useQuery<SEARCH_TICKET, SEARCH_TICKETVariables>(GET_SEARCH_TICKET, {
    fetchPolicy: 'cache-and-network',
    variables: {
      filterBy: [
        { term: { 'groupement.id': (groupement && groupement.id) || '' } },
        { term: { typeTicket: 'RECLAMATION' } },
        { term: { origineType: 'FOURNISSEUR' } },
        { term: { idOrganisation: idLaboratoire } },
      ],
      take,
      skip,
    },
    onError: error => {
      console.log('error :>> ', error);
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  React.useEffect(() => {
    if (data && data.search && data.search.data) {
      setTickets((data && data.search && data.search.data) || []);
    }
  }, [data]);

  const fetchMoreData = (toTake: number = 0) => () => {
    fetchMore({
      variables: {
        filterBy: [
          { term: { 'groupement.id': (groupement && groupement.id) || '' } },
          { term: { typeTicket: 'RECLAMATION' } },
          { term: { origineType: 'FOURNISSEUR' } },
          { term: { idOrganisation: idLaboratoire } },
        ],
        take,
        skip,
      },
      updateQuery: (prev: any, { fetchMoreResult }: any) => {
        setTake(take + toTake);
        setTickets(
          (fetchMoreResult && fetchMoreResult.search && fetchMoreResult.search.data) || [],
        );
        return fetchMoreResult as any;
      },
    });
  };

  const getOrigine = (origine: string) => {
    switch (origine) {
      case 'INTERNE':
        return 'Interne';
      case 'PATIENT':
        return 'Patient';
      case 'GROUPE_AMIS':
        return `Groupe d'amis`;
      case 'GROUPEMENT':
        return 'Groupement';
      case 'FOURNISSEUR':
        return 'Laboratoire';
      case 'PRESTATAIRE':
        return 'Prestataire';

      default:
        return '';
    }
  };

  const getPriority = (priority: number) => {
    switch (priority) {
      case 1:
        return 'Normal';
      case 2:
        return 'Important';
      case 3:
        return `Bloquant`;

      default:
        return '';
    }
  };

  const viewDetail = (value: any) => () => {
    setTicket(value);
    setOpenModal(true);
  };

  const columns: CustomTableColumn[] = [
    {
      key: 'dateHeureSaisie',
      label: 'Date de saisie',
      disablePadding: false,
      numeric: false,
      renderer: (value: any) => {
        return value.dateHeureSaisie ? moment(value.dateHeureSaisie).format('DD/MM/YYYY') : '-';
      },
    },
    {
      key: 'dateHeureSaisie',
      label: 'Heure de saisie',
      disablePadding: false,
      numeric: false,
      renderer: (value: any) => {
        return value.dateHeureSaisie ? moment(value.dateHeureSaisie).format('HH:mm') : '-';
      },
    },
    {
      key: 'origineType',
      label: 'Origine',
      disablePadding: false,
      numeric: false,
      renderer: (value: any) => {
        return value.origineType ? getOrigine(value.origineType) : '-';
      },
    },
    {
      key: 'priority',
      label: 'Priorité',
      disablePadding: false,
      numeric: false,
      renderer: (value: any) => {
        return value.priority ? getPriority(value.priority) : '-';
      },
    },
    {
      key: 'declarant.userName',
      label: 'Declarant',
      disablePadding: false,
      numeric: false,
      renderer: (value: any) => {
        return (
          (value.declarant && value.declarant.userName
            ? value.declarant.userName === currentUserName
              ? 'Moi'
              : value.declarant.userName
            : '-') || '-'
        );
      },
    },
    /* {
      key: '',
      label: 'Concerné(e)',
      disablePadding: false,
      numeric: false,
      renderer: (value: any) => {
        const concernees =
          value.usersConcernees && value.usersConcernees.length
            ? value.usersConcernees.map(user => user.userName).join(', ')
            : '';
        return concernees;
      },
    }, */
    /*  {
      key: 'smyley.nom',
      label: 'Smyley',
      disablePadding: false,
      numeric: false,
      renderer: (value: any) => {
        if (value.smyley && value.smyley.photo) {
          return (
            <img
              src={`${AWS_HOST}/${value.smyley.photo}`}
              alt={value.smyley.nom}
              title={value.smyley.nom}
            />
          );
        } else {
          return '-';
        }
      },
    }, */
    {
      key: 'statusTicket',
      label: 'Statut',
      disablePadding: false,
      numeric: false,
      renderer: (value: any) => {
        return <Status id={value && value.id} currentStatus={value && value.statusTicket} />;
      },
    },
    {
      key: '',
      label: '-',
      disablePadding: false,
      numeric: false,
      renderer: (value: any) => {
        return (
          <IconButton onClick={viewDetail(value)}>
            <Visibility />
          </IconButton>
        );
      },
    },
  ];

  return (
    <Box className={classes.tableContainer}>
      <Box width="100%">
        <Typography className={classes.title}>Liste des réclamations</Typography>
        <Box className={classes.searchInputBox}>
          <Box>
            <CustomButton
              color="secondary"
              className={classes.btnAdd}
              onClick={() => setOpenModalForm(true)}
            >
              Ajouter reclamation
            </CustomButton>
          </Box>
        </Box>
        <Box width="100%">
          <div style={{ padding: 20 }}>
            <CustomTable
              columns={columns}
              showToolbar={false}
              selectable={false}
              data={tickets}
              rowCount={tickets.length}
              total={tickets.length}
              page={page}
              rowsPerPage={rowsPerPage}
              take={take}
              skip={skip}
              setTake={setTake}
              setSkip={setSkip}
              setPage={setPage}
              setRowsPerPage={setRowsPerPage}
            />
          </div>
        </Box>
        <CustomModal
          open={openModal}
          setOpen={setOpenModal}
          title={'Details Réclamation'}
          withBtnsActions={false}
          closeIcon={true}
          headerWithBgColor={true}
          fullWidth={true}
        >
          <TicketDetails ticket={ticket} isOnAppel={false} />
        </CustomModal>
        <TicketForm
          open={openModalForm}
          setOpen={setOpenModalForm}
          isAppelEntrant={false}
          type={'RECLAMATION'}
          refetch={fetchMoreData(0)}
          idLaboratoire={idLaboratoire || ''}
        />
      </Box>
    </Box>
  );
};
export default withRouter(Reclamations);
