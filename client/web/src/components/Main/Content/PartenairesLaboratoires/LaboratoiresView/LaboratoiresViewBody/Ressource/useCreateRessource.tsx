import { useMutation, useApolloClient } from '@apollo/react-hooks';
import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';
import {
  CREATE_UPDATE_LABORATOIRE_RESSOURCE,
  CREATE_UPDATE_LABORATOIRE_RESSOURCEVariables,
} from '../../../../../../../graphql/LaboratoireRessource/types/CREATE_UPDATE_LABORATOIRE_RESSOURCE';
import { DO_CREATE_UPDATE_LABORATOIRE_RESSOURCE } from '../../../../../../../graphql/LaboratoireRessource/mutation';

export const useCreateUpdateRessource = (
  isOnEdit: boolean,
  fetchMoreData: () => void,
): [any, boolean] => {
  const client = useApolloClient();

  const [createUpdateRessource, { loading }] = useMutation<
    CREATE_UPDATE_LABORATOIRE_RESSOURCE,
    CREATE_UPDATE_LABORATOIRE_RESSOURCEVariables
  >(DO_CREATE_UPDATE_LABORATOIRE_RESSOURCE, {
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
      displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
    },
    onCompleted: data => {
      if (data && data.createUpdateLaboratoireRessource) {
        fetchMoreData();
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: `Ressource ${isOnEdit ? 'modifiée' : 'crée'} avec succès`,
          isOpen: true,
        });
      }
    },
  });

  return [createUpdateRessource, loading];
};
