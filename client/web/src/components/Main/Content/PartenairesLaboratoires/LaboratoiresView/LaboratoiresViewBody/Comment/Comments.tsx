import { useApolloClient, useLazyQuery, useQuery } from '@apollo/react-hooks';
import { Box, Button, Link, Typography } from '@material-ui/core';
import React, { FC, Fragment, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { GET_SEARCH_COMMENT } from '../../../../../../../graphql/Comment/query';
import {
  SEARCH_COMMENT,
  SEARCH_COMMENTVariables,
} from '../../../../../../../graphql/Comment/types/SEARCH_COMMENT';
import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';
import { Comment } from '../../../../../../Comment';
import CustomButton from '../../../../../../Common/CustomButton';
import {
  FiltersInterface,
  GET_LOCAL_FILTERS,
} from '../../../../../../Common/newWithSearch/queryBuilder';
import CommentList from './List';
import useStyles from './styles';
interface ReclamationsProps {
  idLaboratoire: string | null;
}

const Comments: FC<ReclamationsProps & RouteComponentProps> = ({ idLaboratoire, history }) => {
  const classes = useStyles({});
  const client = useApolloClient();

  const [comments, setComments] = useState<any[]>([]);
  const [take, setTake] = useState<number>(5);
  const skip = 0;

  const query = {
    query: {
      bool: {
        must: [
          {
            term: { idItemAssocie: idLaboratoire },
          },
          {
            term: { 'item.code': 'LABO' },
          },
          {
            term: { isRemoved: false },
          },
        ],
      },
    },
    sort: [{ dateModification: { order: 'desc' } }],
  };

  const { data, fetchMore, loading } = useQuery<SEARCH_COMMENT, SEARCH_COMMENTVariables>(
    GET_SEARCH_COMMENT,
    {
      fetchPolicy: 'cache-and-network',
      variables: {
        query,
        take,
        skip,
      },
      onError: error => {
        console.log('error :>> ', error);
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );

  React.useEffect(() => {
    if (data && data.search && data.search.data) {
      setComments((data && data.search && data.search.data) || []);
    }
  }, [data]);

  const fetchMoreData = (toTake: number = 0) => () => {
    fetchMore({
      variables: {
        type: ['laboratoireressource'],
        query,
        take: take + toTake,
        skip,
      },
      updateQuery: (prev: any, { fetchMoreResult }: any) => {
        setTake(take + toTake);
        setComments(
          (fetchMoreResult && fetchMoreResult.search && fetchMoreResult.search.data) || [],
        );
        return fetchMoreResult as any;
      },
    });
  };

  const filters = useQuery<FiltersInterface>(GET_LOCAL_FILTERS);

  const goToCatalogue = () => {
    if (filters && filters.data && filters.data.filters)
      client.writeData({
        data: {
          filters: {
            ...filters.data.filters,
            idLaboratoires: [idLaboratoire],
            reaction: [0],
            __typename: 'local',
          },
        },
      });
    history.push({
      pathname: '/catalogue-produits/card',
      state: { idLaboratoire: idLaboratoire, comments: true },
    });
  };

  return (
    <Box className={classes.tableContainer}>
      <Box width="100%">
        <Box width="100%" display="flex" flexDirection="row" justifyContent="space-between">
          <Typography className={classes.title}>Liste des commentaires</Typography>
          <div className={classes.title} style={{ cursor: 'pointer' }} onClick={goToCatalogue}>
            Voir les commentaires des produits
          </div>
        </Box>
        <Box width="100%">
          <Comment codeItem="LABO" idSource={idLaboratoire || ''} refetch={fetchMoreData(0)} />
          <CommentList
            total={(data && data.search && data.search.total) || 0}
            comments={comments}
            loading={loading}
            fetchMoreComments={fetchMoreData(5)}
          />
        </Box>
      </Box>
    </Box>
  );
};
export default withRouter(Comments);
