import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import { Box, Typography } from '@material-ui/core';
import { Delete, Edit, Visibility } from '@material-ui/icons';
import { differenceBy } from 'lodash';
import moment from 'moment';
import React, { ChangeEvent, FC, Fragment, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { ContentContext, ContentStateInterface } from '../../../../../../../AppContext';
import { DO_DELETE_LABORATOIRE_RESSOURCES } from '../../../../../../../graphql/LaboratoireRessource/mutation';
import {
  DO_SEARCH_LABORATOIRE_RESSOURCE,
  GET_SEARCH_CUSTOM_CONTENT_LABORATOIRE_RESSOURCE,
} from '../../../../../../../graphql/LaboratoireRessource/query';
import {
  DELETE_LABORATOIRE_RESSOURCES,
  DELETE_LABORATOIRE_RESSOURCESVariables,
} from '../../../../../../../graphql/LaboratoireRessource/types/DELETE_LABORATOIRE_RESSOURCES';
import {
  SEARCH_CUSTOM_CONTENT_LABORATOIRE_RESSOURCE,
  SEARCH_CUSTOM_CONTENT_LABORATOIRE_RESSOURCEVariables,
} from '../../../../../../../graphql/LaboratoireRessource/types/SEARCH_CUSTOM_CONTENT_LABORATOIRE_RESSOURCE';
import {
  SEARCH_LABORATOIRE_RESSOURCE,
  SEARCH_LABORATOIRE_RESSOURCEVariables,
} from '../../../../../../../graphql/LaboratoireRessource/types/SEARCH_LABORATOIRE_RESSOURCE';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { getGroupement } from '../../../../../../../services/LocalStorage';
import { uploadToS3 } from '../../../../../../../services/S3';
import { TypeFichier } from '../../../../../../../types/graphql-global-types';
import { formatFilenameWithoutDate } from '../../../../../../../utils/filenameFormater';
import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';
import { ConfirmDeleteDialog } from '../../../../../../Common/ConfirmDialog';
import CustomButton from '../../../../../../Common/CustomButton';
import CustomContent from '../../../../../../Common/newCustomContent';
import SearchInput from '../../../../../../Common/newCustomContent/SearchInput';
import WithSearch from '../../../../../../Common/newWithSearch/withSearch';
import { Column } from '../../../../../../Dashboard/Content/Interface';
import TableActionColumn, {
  TableActionColumnMenu,
} from '../../../../../../Dashboard/TableActionColumn';
import CustomTable, { CustomTableColumn } from '../../../../Pilotage/Dashboard/Table/CustomTable';
import Form from './Form';
import useStyles from './styles';
import { useCreateUpdateRessource } from './useCreateRessource';

interface RessourceListProps {
  idLaboratoire: string | null;
}

const RessourceInitialState = {
  code: '',
  libelle: '',
  dateChargement: new Date(),
  typeRessource: '',
  idLaboratoire: '',
  idItem: '',
  url: '',
};

const RessourceList: FC<RessourceListProps & RouteComponentProps> = ({
  location: { pathname },
  history: { push },
  idLaboratoire,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();

  const [open, setOpen] = useState<boolean>(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [deleteRow, setDeleteRow] = useState<any>(null);
  const [selected, setSelected] = useState<any[]>([]);
  const [ressource, setRessource] = useState<any>(null);
  const [ressources, setRessources] = useState<any[]>([]);
  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(5);
  const [take, setTake] = useState<number>(5);
  const [skip, setSkip] = useState<number>(0);

  const [values, setValues] = useState<any>(RessourceInitialState);

  const onClickDelete = (_event: any, row: any) => {
    setOpenDeleteDialog(true);
    setDeleteRow(row);
  };

  const [itemToDelete, setItemToDelete] = useState<any>();

  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const fetchMoreData = (toTake: number = 0) => () => {
    fetchMore({
      variables: {
        type: ['laboratoireressource'],
        filterBy: [
          { term: { isRemoved: false } },
          { term: { 'laboratoire.id': idLaboratoire || '' } },
        ],
        take: take + toTake,
        skip,
      },
      updateQuery: (prev: any, { fetchMoreResult }: any) => {
        setTake(take + toTake);

        setRessources(
          (fetchMoreResult && fetchMoreResult.search && fetchMoreResult.search.data) || [],
        );
        return fetchMoreResult as any;
      },
    });
  };

  const [createUpdateRessource] = useCreateUpdateRessource(
    ressource ? true : false,
    fetchMoreData(0),
  );

  const groupement = getGroupement();

  const { data, fetchMore } = useQuery<
    SEARCH_LABORATOIRE_RESSOURCE,
    SEARCH_LABORATOIRE_RESSOURCEVariables
  >(DO_SEARCH_LABORATOIRE_RESSOURCE, {
    fetchPolicy: 'cache-and-network',
    variables: {
      filterBy: [{ term: { isRemoved: false } }, { term: { 'laboratoire.id': idLaboratoire } }],
      take,
      skip,
    },
    onError: error => {
      console.log('error :>> ', error);
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const [doCreatePutPresignedUrl, { loading: presignedLoading }] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (
        data &&
        data.createPutPresignedUrls &&
        data.createPutPresignedUrls.length > 0 &&
        values &&
        values.photo &&
        values.photo.size
      ) {
        const presignedPhoto = data.createPutPresignedUrls[0];
        if (presignedPhoto && presignedPhoto.filePath && presignedPhoto.presignedUrl) {
          await uploadToS3(values.photo, presignedPhoto.presignedUrl)
            .then(async result => {
              if (result && result.status === 200) {
                const type: TypeFichier = values.photo.type.includes('pdf')
                  ? TypeFichier.PDF
                  : values.photo.type.includes('xlsx')
                  ? TypeFichier.EXCEL
                  : TypeFichier.PHOTO;
                await createUpdateRessource({
                  variables: {
                    input: {
                      id: (ressource && ressource.id) || '',
                      typeRessource: values.typeRessource,
                      idItem: values && values.item && values.item.id,
                      code: values && values.code,
                      libelle: values && values.libelle,
                      dateChargement: values && values.dateChargement,
                      url: values && values.url,
                      fichier: {
                        chemin: presignedPhoto.filePath,
                        nomOriginal: values.photo.name,
                        type,
                      },
                      idLaboratoire,
                    },
                  },
                });
                setOpen(false);
              }
            })
            .catch(error => {
              console.error(error);
              displaySnackBar(client, {
                type: 'ERROR',
                message:
                  "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
                isOpen: true,
              });
            });
        }
      }
    },
    onError: errors => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      });
    },
  });

  const [deleteRessource] = useMutation<
    DELETE_LABORATOIRE_RESSOURCES,
    DELETE_LABORATOIRE_RESSOURCESVariables
  >(DO_DELETE_LABORATOIRE_RESSOURCES, {
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
    onCompleted: data => {
      if (data && data.softDeleteLaboratoireRessources) {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'Ressource(s) supprimée(s) avec succès',
          isOpen: true,
        });
        fetchMoreData(0)();
        setOpenDeleteDialog(false);
      }
    },
  });

  React.useEffect(() => {
    if (data && data.search && data.search.data) {
      setRessources((data && data.search && data.search.data) || []);
    }
  }, [data]);

  React.useEffect(() => {
    if (!open) {
      setValues(RessourceInitialState);
    }
  }, [open]);

  React.useEffect(() => {
    if (ressource) {
      setValues(prevState => ({
        ...prevState,
        code: ressource.code || '',
        libelle: ressource.libelle || '',
        dateChargement: ressource.dateChargement || new Date(),
        typeRessource: ressource.typeRessource || '',
        idLaboratoire,
        item: ressource.item || '',
        url: ressource.url || '',
        fichier: ressource.fichier || null,
      }));
    }
  }, [ressource]);

  const handleClickDelete = (row: any) => {
    setItemToDelete(row);
  };

  const handleClickEdit = (_event: any, row: any) => {
    setRessource(row);
    setOpen(true);
  };

  const menuItems: TableActionColumnMenu[] = [
    { label: 'Modifier', icon: <Edit />, onClick: handleClickEdit, disabled: false },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: onClickDelete,
      disabled: false,
    },
  ];

  const columns: CustomTableColumn[] = [
    { label: 'Type de ressource', key: 'typeRessource', disablePadding: false, numeric: false },
    {
      label: 'Item',
      key: 'item.name',
      disablePadding: false,
      numeric: false,
      renderer: (value: any) => {
        return (value && value.item && value.item.name) || '-';
      },
    },
    /* { label: 'Code', key: 'code', disablePadding: false, numeric: false }, */
    {
      label: 'Libellé',
      key: 'libelle',
      disablePadding: false,
      numeric: false,
    },
    {
      label: 'Date',
      key: 'dateChargement',
      disablePadding: false,
      numeric: false,
      renderer: (value: any) => {
        return value && value.dateChargement && moment(value.dateChargement).format('DD-MM-YYYY');
      },
    },
    /* {
      label: 'Fichier ou URL',
      key: 'url',
      disablePadding: false,
      numeric: false,
    }, */
    {
      key: '',
      label: '',
      sortable: false,
      disablePadding: false,
      numeric: false,
      renderer: (value: any) => {
        return (
          <Fragment>
            <TableActionColumn
              row={value}
              baseUrl=""
              showResendEmail={false}
              customMenuItems={menuItems}
              handleClickDelete={handleClickDelete}
            />
          </Fragment>
        );
      },
    },
  ];

  const tableProps = {
    isSelectable: false,
    paginationCentered: true,
    columns,
    selected,
    setSelected,
  };

  const DeleteDialogContent = () => {
    if (deleteRow) {
      const name = `${deleteRow.prenom || ''} ${deleteRow.nom || ''}`;
      return (
        <span>
          Êtes-vous sur de vouloir supprimer
          <span style={{ fontWeight: 'bold' }}> {name} </span>?
        </span>
      );
    }
    return <span>Êtes-vous sur de vouloir supprimer ces ressources ?</span>;
  };

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      setValues(prevState => ({ ...prevState, [name]: value }));
    }
  };

  const handleChangeAutocomplete = (inputValue: any) => {
    setValues(prevState => ({ ...prevState, item: inputValue }));
  };

  const handleChangeDate = (name: string) => (date: any) => {
    setValues(prevState => ({ ...prevState, [name]: date }));
  };

  const onClickConfirmDelete = () => {
    setOpenDeleteDialog(false);
    if (deleteRow) {
      deleteRessource({ variables: { ids: [deleteRow && deleteRow.id] } });
    } else if (selected && selected.length > 0) {
      deleteRessource({
        variables: { ids: selected.map(item => item.id) },
      });
    }
  };

  const handleCreateRessource = async () => {
    if (values && values.photo && values.photo.size) {
      // Presigned file
      const filePathsTab: string[] = [];
      filePathsTab.push(
        `users/photos/${groupement && groupement.id}/${formatFilenameWithoutDate(values.photo)}`,
      );
      doCreatePutPresignedUrl({ variables: { filePaths: filePathsTab } });
      return;
    }
    await createUpdateRessource({
      variables: {
        input: {
          id: (ressource && ressource.id) || '',
          idLaboratoire,
          typeRessource: values.typeRessource,
          idItem: values && values.item && values.item.id,
          code: values && values.code,
          libelle: values && values.libelle,
          dateChargement: values && values.dateChargement,
          url: values && values.url,
        },
      },
    });
    setOpen(false);
  };

  return (
    <Box className={classes.tableContainer}>
      <Box width="100%">
        <Typography className={classes.title}>Liste des ressources</Typography>
        <Box className={classes.searchInputBox}>
          <Box>
            <CustomButton
              color="secondary"
              className={classes.btnAdd}
              onClick={() => setOpen(true)}
            >
              Ajouter ressource
            </CustomButton>
          </Box>
        </Box>

        <div style={{ padding: 20 }}>
          <CustomTable
            columns={columns}
            showToolbar={false}
            selectable={false}
            data={ressources}
            rowCount={ressources.length}
            total={ressources.length}
            page={page}
            rowsPerPage={rowsPerPage}
            take={take}
            skip={skip}
            setTake={setTake}
            setSkip={setSkip}
            setPage={setPage}
            setRowsPerPage={setRowsPerPage}
          />
        </div>
        <ConfirmDeleteDialog
          open={openDeleteDialog}
          setOpen={setOpenDeleteDialog}
          content={<DeleteDialogContent />}
          onClickConfirm={onClickConfirmDelete}
        />
        <Form
          open={open}
          setOpen={setOpen}
          handleChange={handleChange}
          handleChangeAutocomplete={handleChangeAutocomplete}
          handleChangeDate={handleChangeDate}
          values={values}
          setValues={setValues}
          onClickConfirm={handleCreateRessource}
        />
      </Box>
    </Box>
  );
};
export default withRouter(RessourceList);
