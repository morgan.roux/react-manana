import React, { FC } from 'react';
import classnames from 'classnames';
import { Typography, Link, Box } from '@material-ui/core';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import useStyles from './styles';
import { SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire } from '../../../../../../../graphql/Laboratoire/types/SEARCH_LABORATOIRE_PARTENAIRE';
import { useApolloClient, useQuery } from '@apollo/react-hooks';
import {
  FiltersInterface,
  GET_LOCAL_FILTERS,
} from '../../../../../../Common/newWithSearch/queryBuilder';

interface MenuProps {
  handleClick: Function;
  currentView: string;
  currentLabo: any | undefined;
}

const LaboratoireMenu: FC<MenuProps & RouteComponentProps> = ({
  handleClick,
  currentView,
  currentLabo,
  history,
}) => {
  const classes = useStyles({});

  const client = useApolloClient();
  const filters = useQuery<FiltersInterface>(GET_LOCAL_FILTERS);
  const goToCatalogue = () => {
    if (filters && filters.data && filters.data.filters)
      client.writeData({
        data: {
          filters: {
            ...filters.data.filters,
            idLaboratoires: [currentLabo && currentLabo.id],
            __typename: 'local',
          },
        },
      });
    history.push({
      pathname: '/catalogue-produits/card',
      state: { idLaboratoire: currentLabo && currentLabo.id },
    });
  };

  return (
    <Box className={classes.allContainer}>
      <Typography className={classes.linkContainer}>
        <Link
          href="#"
          onClick={(e: any) => {
            e.preventDefault();
            e.stopPropagation();
            handleClick('partenaire');
          }}
          className={classnames(
            classes.linkElement,
            currentView === 'partenaire' ? classes.active : '',
          )}
        >
          Partenaire
        </Link>
        <Link
          href="#"
          onClick={(e: any) => {
            e.preventDefault();
            e.stopPropagation();
            handleClick('contacts');
          }}
          className={classnames(
            classes.linkElement,
            currentView === 'contacts' ? classes.active : '',
          )}
        >
          Contacts
        </Link>
        <Link
          href="#"
          onClick={(e: any) => {
            e.preventDefault();
            e.stopPropagation();
            handleClick('ressources');
          }}
          className={classnames(
            classes.linkElement,
            currentView === 'ressources' ? classes.active : '',
          )}
        >
          Ressources
        </Link>
        <Link
          href="#"
          onClick={(e: any) => {
            e.preventDefault();
            e.stopPropagation();
            handleClick('reclamation');
          }}
          className={classnames(
            classes.linkElement,
            currentView === 'reclamation' ? classes.active : '',
          )}
        >
          Réclamations
        </Link>
        <Link
          href="#"
          onClick={(e: any) => {
            e.preventDefault();
            e.stopPropagation();
            handleClick('comments');
          }}
          className={classnames(
            classes.linkElement,
            currentView === 'comments' ? classes.active : '',
          )}
        >
          Commentaires
        </Link>
        <Link
          href="#"
          onClick={(e: any) => {
            e.preventDefault();
            e.stopPropagation();
            handleClick('actualites');
          }}
          className={classnames(
            classes.linkElement,
            currentView === 'actualites' ? classes.active : '',
          )}
        >
          Actualités
        </Link>
        <Link
          href="#"
          onClick={(e: any) => {
            e.preventDefault();
            e.stopPropagation();
            goToCatalogue();
          }}
          className={classnames(
            classes.linkElement,
            currentView === 'catalogues' ? classes.active : '',
          )}
        >
          Catalogue produits
        </Link>
        <Link
          href="#"
          onClick={(e: any) => {
            e.preventDefault();
            e.stopPropagation();
            handleClick('operations');
          }}
          className={classnames(
            classes.linkElement,
            currentView === 'operations' ? classes.active : '',
          )}
        >
          Opérations en cours
        </Link>
      </Typography>
    </Box>
  );
};

export default withRouter(LaboratoireMenu);
