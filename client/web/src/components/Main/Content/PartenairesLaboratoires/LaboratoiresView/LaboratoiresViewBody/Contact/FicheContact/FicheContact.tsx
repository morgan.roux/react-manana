import { ArrowBack, Edit } from '@material-ui/icons';
import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import CustomAvatar from '../../../../../../../Common/CustomAvatar';
import useStyles from './styles';
import classnames from 'classnames';
import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { Box, IconButton, Link, Typography } from '@material-ui/core';
import { CustomModal } from '../../../../../../../Common/CustomModal';
import useCommonStyles from '../../../../../../../Dashboard/commonStyles';
import SectionContainer from '../SectionContainer';

interface FicheContactProps {
  representant: any;
  open: boolean;
  setOpen: (value: any) => void;
}

const FicheContact: React.FC<FicheContactProps & RouteComponentProps> = ({
  representant,
  open,
  setOpen,
}) => {
  const classes = useStyles({});
  const handleClick = () => {};
  const client = useApolloClient();
  const commonStyles = useCommonStyles();

  const data = representant;

  const sections = [
    {
      title: 'Coordonnées',
      content: [
        {
          subtitle: 'Adresse',
          item: data && data.contact ? data.contact.adresse1 : '-',
        },
        {
          subtitle: 'Code postal',
          item: data && data.contact ? data.contact.cp : '-',
        },
        {
          subtitle: 'Ville',
          item: data && data.contact ? data.contact.ville : '-',
        },
      ],
    },
    {
      title: 'Internet',
      content: [
        {
          subtitle: 'Adresse de messagerie',
          item: data && data.contact ? data.contact.mailPerso : '-',
        },
        {
          subtitle: 'Page web',
          item: data && data.contact ? data.contact.siteProf : '-',
        },
        {
          subtitle: 'Adresse de messagerie instantanée',
          item: data && data.contact ? data.contact.mailPerso : '-',
        },
      ],
    },
    {
      title: 'Numéros de téléphone',
      content: [
        {
          subtitle: 'Bureau',
          item: data && data.contact ? data.contact.telProf : '-',
        },
        {
          subtitle: 'Domicile',
          item: data && data.contact ? data.contact.telPerso : '-',
        },
        {
          subtitle: 'Télécopie (bureau)',
          item: data && data.contact ? data.contact.telProf : '-',
        },
        {
          subtitle: 'Téléphone mobile',
          item: data && data.contact ? data.contact.telMobProf : '-',
        },
      ],
    },
    {
      title: 'Réseaux sociaux',
      content: [
        {
          subtitle: 'LinkedIn',
          item: (
            <Link href={'https://' + (data?.contact?.urlLinkedinProf ?? '-') || ''} target="_blank">
              linkedin.com
            </Link>
          ),
        },
        {
          subtitle: 'Facebook',
          item: (
            <Link href={'https://' + (data?.contact?.urlFacebookProf ?? '-') || ''} target="_blank">
              facebook.com
            </Link>
          ),
        },
        {
          subtitle: 'WhatSapp',
          item: data && data.contact ? data.contact.whatsAppMobProf : '-',
        },
        {
          subtitle: 'Messenger',
          item: (
            <Link
              href={'https://' + (data?.contact?.urlMessenger ?? '-') || ''}
              target="_blank"
              color="primary"
            >
              messenger.com
            </Link>
          ),
        },
        {
          subtitle: 'Youtube',
          item: (
            <Link href={'https://' + (data?.contact?.urlYoutube ?? '-') || ''} target="_blank">
              youtube.com
            </Link>
          ),
        },
      ],
    },
  ];

  return (
    <CustomModal
      onClick={event => event.stopPropagation()}
      open={open}
      setOpen={setOpen}
      withBtnsActions={false}
      closeIcon={true}
      headerWithBgColor={true}
      fullWidth={true}
      actionButton={'Enregistrer'}
      withCancelButton={true}
      centerBtns={true}
      title={'Fiche détaillé du contact'}
    >
      <Box className={classes.ficheContactContainer}>
        <Box width="100%" padding="25px">
          <Box className={classnames(classes.contentSectionNote)}>
            <Box className={classes.contentSection}>
              <Box className={classes.nameContent}>
                <Box className={classes.sectionImage}>
                  <CustomAvatar name={data?.nom ?? 'Nom'} url={data?.photo?.publicUrl ?? 'url'} />
                  <Typography className={classes.titleName}>
                    Nom: {data?.nom ?? '-'} {data?.prenom ?? '-'}
                    <Typography className={classes.contentLabel}>
                      <Typography className={classes.labelTitle}>Fonction :</Typography>
                      <Typography className={classes.labelResult}>
                        {data ? data.fonction : '-'}
                      </Typography>
                    </Typography>
                    <Typography className={classes.contentLabel}>
                      <Typography className={classes.labelTitle}>Civilité :</Typography>
                      <Typography className={classes.labelResult}>
                        {data ? data.civilite : '-'}
                      </Typography>
                    </Typography>
                  </Typography>
                </Box>
              </Box>
            </Box>
          </Box>
          <Box className={classes.sections}>
            {sections.map(section => (
              <SectionContainer
                title={section.title}
                children={
                  <Box>
                    {section.content.map(content => (
                      <Box style={{ marginTop: 10 }}>
                        <Typography className={classes.titleSub}>{content.subtitle}</Typography>
                        <Typography className={classes.subtitle}>
                          {content.item === '' ? '-' : content.item}
                        </Typography>
                      </Box>
                    ))}
                  </Box>
                }
              />
            ))}
          </Box>
        </Box>
      </Box>
    </CustomModal>
  );
};
export default withRouter(FicheContact);
