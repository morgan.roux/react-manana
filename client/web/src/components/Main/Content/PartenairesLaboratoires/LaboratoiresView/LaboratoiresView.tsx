import React, { FC, useState } from 'react';
import { Box, Grid, Typography } from '@material-ui/core';
import useStyles from './styles';
import CaptureIne from '../../../../../assets/img/image_default22.png';
import LaboratoiresViewHeader from './LaboratoiresViewHeader/LaboratoiresViewHeader';
import LaboratoiresViewBody from './LaboratoiresViewBody/LaboratoiresViewBody';
import { SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire } from '../../../../../graphql/Laboratoire/types/SEARCH_LABORATOIRE_PARTENAIRE';

interface LaboratoiresViewProps {
  currentLabo: any | undefined;
  currentTab: string;
  refetch?: () => void;
}
const LaboratoiresView: FC<LaboratoiresViewProps> = ({ currentLabo, currentTab, refetch }) => {
  const classes = useStyles({});

  if (!currentLabo) {
    return (
      <Box className={classes.none}>
        <Box>
          <Box>
            <img className={classes.imgDefault} src={CaptureIne} />
            <Box>
              <Typography className={classes.title}>Aucun élément</Typography>
              <Typography className={classes.subTitle}>
                Cette section ne contient pas encore d'élément. <br /> Navré pour ce désagrement.
              </Typography>
            </Box>
          </Box>
        </Box>
      </Box>
    );
  }

  return (
    <Grid item={true} xs={9} className={classes.root}>
      <LaboratoiresViewHeader
        currentView={currentTab}
        currentLabo={currentLabo}
        refetch={refetch}
      />
      <LaboratoiresViewBody currentLabo={currentLabo} currentView={currentTab} refetch={refetch} />
    </Grid>
  );
};

export default LaboratoiresView;
