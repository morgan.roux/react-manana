import React, { FC } from 'react';
import useStyles from './styles';
import Grid from '@material-ui/core/Grid';
import { useQuery } from '@apollo/react-hooks';
import {
  SEARCH as SEARCH_Interface,
  SEARCHVariables,
} from '../../../../../../../graphql/search/types/SEARCH';
import { SEARCH } from '../../../../../../../graphql/search/query';
import ICurrentPharmacieInterface from '../../../../../../../Interface/CurrentPharmacieInterface';
import { GET_CURRENT_PHARMACIE } from '../../../../../../../graphql/Pharmacie/local';
import { getGroupement } from '../../../../../../../services/LocalStorage';
import {
  Box,
  CircularProgress,
  Typography,
  Card,
  CardActionArea,
  CardMedia,
  Divider,
  CardContent,
  CardActions,
} from '@material-ui/core';
import CaptureIne from '../../../../../../../assets/img/image_default22.png';
import moment from 'moment';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire } from '../../../../../../../graphql/Laboratoire/types/SEARCH_LABORATOIRE_PARTENAIRE';
interface LaboratoireOperationsProps {
  currentLabo: SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire | undefined;
}

const LaboratoireOperations: FC<LaboratoireOperationsProps & RouteComponentProps> = ({
  currentLabo,
  history,
}) => {
  const classes = useStyles({});
  const groupement = getGroupement();
  // take currentPharmacie
  const { data: pharmacieData } = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

  const currentPharmacie = pharmacieData ? pharmacieData.pharmacie : null;

  const handleSelect = (oc: any) => {
    if (oc && oc.id) {
      history.push(`/operations-commerciales/${oc.id}`);
    }
  };
  const { data: listOcData, loading, error } = useQuery<SEARCH_Interface, SEARCHVariables>(SEARCH, {
    variables: {
      type: ['operation'],
      query: {
        query: {
          constant_score: {
            filter: {
              bool: {
                must: [
                  {
                    terms: {
                      'laboratoire.id': currentLabo ? [currentLabo.id] : [],
                    },
                  },
                  {
                    term: { isRemoved: false },
                  },
                  { term: { idGroupement: groupement.id } },
                ],
              },
            },
          },
        },
        sort: [{ dateModification: { order: 'asc' } }],
      },
      idPharmacie: (currentPharmacie && currentPharmacie.id) || '',
    },
    skip: !currentPharmacie,
  });

  if (loading) {
    return (
      <Box display="flex" justifyContent="center" alignItems="center" height="100%" width="100%">
        <CircularProgress variant="indeterminate" color="secondary" />
      </Box>
    );
  }

  return (
    <Grid container={true} className={classes.cont}>
      <Grid item={true} xs={12}>
        <Grid container={true} spacing={2}>
          {listOcData &&
          listOcData.search &&
          listOcData.search.data &&
          listOcData.search.data.length > 0 ? (
            <div>
              {listOcData.search.data.map((oc: any | null, index) => {
                if (oc) {
                  return (
                    <Grid item={true} key={`oc_card_${index}`}>
                      <Card className={classes.card}>
                        <CardActionArea onClick={() => handleSelect(oc)}>
                          <CardContent className={classes.ocCardContent}>
                            <Typography
                              gutterBottom={true}
                              variant="h5"
                              component="h4"
                              className={classes.ocTitle}
                            >
                              {oc
                                ? oc.libelle && oc.libelle.length > 19
                                  ? `${oc.libelle.substr(0, 16)}...`
                                  : oc.libelle
                                : 'Oc 1'}
                            </Typography>
                            <Box className={classes.descBox}>{oc && oc.description}</Box>
                          </CardContent>
                          <CardActions className={classes.dateContainer}>
                            <span>
                              {oc ? moment(oc.dateCreation).format('LL') : '19 Juillet 2017'}
                            </span>
                          </CardActions>
                        </CardActionArea>
                      </Card>
                    </Grid>
                  );
                }
              })}
            </div>
          ) : (
            <Box
              display="flex"
              flexDirection="column"
              justifyContent="center"
              alignItems="center"
              height="100%"
              width="100%"
            >
              <img className={classes.imgDefault} src={CaptureIne} />
              <Box>
                <Typography className={classes.title}>Aucun élément</Typography>
                <Typography className={classes.subTitle}>
                  Cette section ne contient pas encore d'élément. <br /> Navré pour ce désagrement.
                </Typography>
              </Box>
            </Box>
          )}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default withRouter(LaboratoireOperations);
