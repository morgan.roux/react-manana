import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    tableContainer: {
      width: '100%',
      maxWidth: '768px',
      margin: '8px auto',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      '& <div': {
        BoxShadow: '0px 0px 4px 0px rgba(0,0,0,0)',
      },
    },
    title: {
      textAlign: 'left',
      fontWeight: 'bold',
      fontSize: 18,
      fontFamily: 'Montserrat',
      letterSpacing: 0,
      color: theme.palette.secondary.main,
      opacity: 1,
    },
    tableau: {
      display: 'flex',
      justifyContent: 'space-between',
      width: '100%',
    },
    h5: {
      textAlign: 'right',
      font: 'Medium 16px/33px Montserrat',
      letterSpacing: 0,
      opacity: 1,
      color: '#1D1D1D',
    },

    list: {
      border: '1px solid #DCDCDC',
      borderRadius: 12,
      marginTop: 15,
      paddingTop: 0,
      paddingBottom: 0,
      width: '100%',
    },
    listItem: {
      paddingTop: 10,
      paddingBottom: 10,
      paddingLeft: 20,
      paddingRight: 20,
      fontWeight: 'bold',
      display: 'flex',
      justifyContent: 'space-between',
      '& .MuiListItemText-root': {
        flex: 'inherit',
        color: '#1D1D1D',
        fontSize: 16,
        fontWeight: 600,
      },
    },
  }),
);

export default useStyles;
