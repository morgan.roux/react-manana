import React, { FC } from 'react';
import { SelectBoxPage, SelectBoxItemProps, SelectBoxItem } from '@app/ui-kit';
import { RouteComponentProps } from 'react-router';
import autoEvaluationIcon from '../../../../assets/img/Demarche-qualite/demarches-qualites/autoevaluation.svg';
import referentielQualiteIcon from '../../../../assets/img/Demarche-qualite/demarches-qualites/Referentiel-Qualite.svg';
import outilsIcon from '../../../../assets/img/Demarche-qualite/demarches-qualites/Outils.svg';

const DemarcheQualite: FC<RouteComponentProps> = ({ history }) => {
  const title: string = 'Démarche qualité';
  const subTitle: string = "Optimiser l'organisation et le fonctionnement de votre officine";

  const handleClick = (path: string): void => {
    history.push(`/demarche-qualite/${path}`);
  };

  const data: SelectBoxItemProps[] = [
    {
      image: {
        src: `${autoEvaluationIcon}`,
        alt: 'Auto-évaluation',
      },

      text: 'Auto-évaluation',
      onClick: handleClick.bind(null, 'auto-evaluation'),
    },
    {
      image: {
        src: `${referentielQualiteIcon}`,
        alt: 'Référentiel Qualité',
      },

      text: 'Référentiel Qualité',
      onClick: handleClick.bind(null, 'referentiel-qualite'),
    },
    {
      image: {
        src: `${outilsIcon}`,
        alt: 'Outils',
      },

      text: 'Outils',
      onClick: handleClick.bind(null, 'outils'),
    },
  ];

  return (
    <SelectBoxPage title={title} subTitle={subTitle}>
      {data.map((currentData: SelectBoxItemProps, index: number) => (
        <SelectBoxItem
          key={index}
          image={currentData.image}
          text={currentData.text}
          onClick={currentData.onClick}
        />
      ))}
    </SelectBoxPage>
  );
};

export default DemarcheQualite;
