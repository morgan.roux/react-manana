import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import {
  ActionOperationnelle,
  FicheAmelioration,
  FicheIncident,
  FicheIncidentFormPage,
  RequestSavingFicheIncident,
} from '@app/ui-kit';
import { Box } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import {
  CREATE_ACTION_OPERATIONNELLE,
  UPDATE_ACTION_OPERATIONNELLE,
} from '../../../../../../federation/demarche-qualite/action-operationnelle/mutation';
import { GET_ACTION_OPERATIONNELLE } from '../../../../../../federation/demarche-qualite/action-operationnelle/query';
import { CREATE_ACTION_OPERATIONNELLE as CREATE_ACTION_OPERATIONNELLE_TYPE } from '../../../../../../federation/demarche-qualite/action-operationnelle/types/CREATE_ACTION_OPERATIONNELLE';
import {
  GET_ACTIONS_OPERATIONNELLES,
  GET_ACTIONS_OPERATIONNELLES as GET_ACTIONS_OPERATIONNELLES_TYPE,
} from '../../../../../../federation/demarche-qualite/action-operationnelle/types/GET_ACTIONS_OPERATIONNELLES';
import { GET_ACTION_OPERATIONNELLEVariables } from '../../../../../../federation/demarche-qualite/action-operationnelle/types/GET_ACTION_OPERATIONNELLE';
import {
  UPDATE_ACTION_OPERATIONNELLE as UPDATE_ACTION_OPERATIONNELLE_TYPE,
  UPDATE_ACTION_OPERATIONNELLEVariables,
} from '../../../../../../federation/demarche-qualite/action-operationnelle/types/UPDATE_ACTION_OPERATIONNELLE';
import {
  CREATE_FICHE_AMELIORATION,
  UPDATE_FICHE_AMELIORATION,
} from '../../../../../../federation/demarche-qualite/fiche-amelioration/mutation';
import {
  GET_FICHE_AMELIORATION,
  GET_FICHE_AMELIORATIONS,
} from '../../../../../../federation/demarche-qualite/fiche-amelioration/query';
import { CREATE_FICHE_AMELIORATION as CREATE_FICHE_AMELIORATION_TYPE } from '../../../../../../federation/demarche-qualite/fiche-amelioration/types/CREATE_FICHE_AMELIORATION';
import { GET_FICHE_AMELIORATIONVariables } from '../../../../../../federation/demarche-qualite/fiche-amelioration/types/GET_FICHE_AMELIORATION';
import { GET_FICHE_AMELIORATIONS as GET_FICHE_AMELIORATIONS_TYPE } from '../../../../../../federation/demarche-qualite/fiche-amelioration/types/GET_FICHE_AMELIORATIONS';
import {
  UPDATE_FICHE_AMELIORATION as UPDATE_FICHE_AMELIORATION_TYPE,
  UPDATE_FICHE_AMELIORATIONVariables,
} from '../../../../../../federation/demarche-qualite/fiche-amelioration/types/UPDATE_FICHE_AMELIORATION';
import { GET_FICHE_CAUSES } from '../../../../../../federation/demarche-qualite/fiche-cause/query';
import {
  FICHE_CAUSES,
  FICHE_CAUSESVariables,
} from '../../../../../../federation/demarche-qualite/fiche-cause/types/FICHE_CAUSES';
import {
  CREATE_FICHE_INCIDENT,
  UPDATE_FICHE_INCIDENT,
} from '../../../../../../federation/demarche-qualite/fiche-incident/mutation';
import {
  GET_FICHE_INCIDENT,
  GET_FICHE_INCIDENTS,
} from '../../../../../../federation/demarche-qualite/fiche-incident/query';
import { CREATE_FICHE_INCIDENT as CREATE_FICHE_INCIDENT_TYPE } from '../../../../../../federation/demarche-qualite/fiche-incident/types/CREATE_FICHE_INCIDENT';
import { GET_FICHE_INCIDENTVariables } from '../../../../../../federation/demarche-qualite/fiche-incident/types/GET_FICHE_INCIDENT';
import { GET_FICHE_INCIDENTS as GET_FICHE_INCIDENTS_TYPE } from '../../../../../../federation/demarche-qualite/fiche-incident/types/GET_FICHE_INCIDENTS';
import {
  UPDATE_FICHE_INCIDENT as UPDATE_FICHE_INCIDENT_TYPE,
  UPDATE_FICHE_INCIDENTVariables,
} from '../../../../../../federation/demarche-qualite/fiche-incident/types/UPDATE_FICHE_INCIDENT';
import { GET_FICHE_TYPES } from '../../../../../../federation/demarche-qualite/fiche-type/query';
import {
  FICHE_TYPES,
  FICHE_TYPESVariables,
} from '../../../../../../federation/demarche-qualite/fiche-type/types/FICHE_TYPES';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../../graphql/S3';
import { CREATE_PUT_PESIGNED_URL } from '../../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { getPharmacie, getUser } from '../../../../../../services/LocalStorage';
import { uploadToS3 } from '../../../../../../services/S3';
import { FichierInput } from '../../../../../../types/graphql-global-types';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import AvatarInput from '../../../../../Common/AvatarInput';
import Backdrop from '../../../../../Common/Backdrop';
import CommonFieldsForm from '../../../../../Common/CommonFieldsForm';
import AssignTaskUserModal from '../../../../../Common/CustomSelectUser';
import { formatFilename } from '../../../../../Common/Dropzone/Dropzone';
import OrigineInput from '../../../../../Common/OrigineInput/OrigineInput';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS as GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS_TYPE } from '../../../../../../federation/demarche-qualite/origine/types/GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS';
import { GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS } from '../../../../../../federation/demarche-qualite/origine/query';

interface CreateIncidentProps {
  match: {
    params: {
      id: string | undefined;
      idIncident?: string;
    };
  };
  incidentToEdit?: FicheIncident;
  ameliorationToEdit?: FicheAmelioration;
  actionToEdit?: ActionOperationnelle;
  ameliorationToCreate?: any;
  setOpenFormModal?: (value: boolean) => void;
  refetch?: () => void;
}

const FicheIncidentForm: FC<CreateIncidentProps & RouteComponentProps> = ({
  history: { push },
  location: { pathname },
  match: {
    params: { id, idIncident },
  },
  incidentToEdit,
  ameliorationToEdit,
  ameliorationToCreate,
  actionToEdit,
  setOpenFormModal,
  refetch,
}) => {
  const client = useApolloClient();
  const currentUser = getUser();
  const pharmacie = getPharmacie();

  const type = pathname.includes('fiche-amelioration')
    ? 'amelioration'
    : pathname.includes('action-operationnelle')
    ? 'action'
    : 'incident';

  const [ficheIncident, setFicheIncident] = useState<FicheIncident | undefined>();
  const [ficheAmelioration, setFicheAmelioration] = useState<FicheAmelioration | undefined>();
  const [ficheAction, setFicheAction] = useState<ActionOperationnelle | undefined>();
  const [mutationLoading, setMutationLoading] = useState<boolean>(false);
  const [idImportance, setIdImportance] = useState<string>('');
  const [tache, setTache] = useState<any>();
  const [tacheAInformer, setTacheAInformer] = useState<any>();
  const [auteur, setAuteur] = useState<any>([currentUser]);
  const [userSuivi, setUserSuivi] = useState<any>([]);
  const [userParticipants, setUserParticipants] = useState<any[]>([]);
  const [userParticipantsAInformer, setUserParticipantsAInformer] = useState<any[]>([]);
  const [openAuteurModal, setOpenAuteurModal] = useState<boolean>(false);
  const [openSuiviParModal, setOpenSuiviParModal] = useState<boolean>(false);
  const [idFicheIncident, setIdFicheIncident] = useState<string | undefined>();
  const [origine, setOrigine] = useState<any>();
  const [origineAssocie, setOrigineAssocie] = useState<any>();
  const [isPrivate, setIsPrivate] = useState<boolean>(false);

  const isNewFiche =
    ficheAction?.id ||
    ficheAmelioration?.id ||
    ficheIncident?.id ||
    incidentToEdit?.id ||
    ameliorationToEdit?.id ||
    actionToEdit?.id
      ? false
      : id == 'new';

  const [loadFicheIncident, incidentQuery] = useLazyQuery<any, GET_FICHE_INCIDENTVariables>(
    GET_FICHE_INCIDENT,
    {
      client: FEDERATION_CLIENT,
      fetchPolicy: 'network-only',
    },
  );

  const originesQuery = useQuery<GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS_TYPE, any>(
    GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS,
    {
      client: FEDERATION_CLIENT,
      fetchPolicy: 'cache-and-network',
    },
  );

  const origines = originesQuery.data?.origines.nodes || [];

  const causesQuery = useQuery<FICHE_CAUSES, FICHE_CAUSESVariables>(GET_FICHE_CAUSES, {
    client: FEDERATION_CLIENT,
  });

  const typesQuery = useQuery<FICHE_TYPES, FICHE_TYPESVariables>(GET_FICHE_TYPES, {
    client: FEDERATION_CLIENT,
  });

  const [loadFicheAmelioration, ameliorationQuery] = useLazyQuery<
    any,
    GET_FICHE_AMELIORATIONVariables
  >(GET_FICHE_AMELIORATION, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'network-only',
  });

  const [loadAction, actionQuery] = useLazyQuery<any, GET_ACTION_OPERATIONNELLEVariables>(
    GET_ACTION_OPERATIONNELLE,
    {
      client: FEDERATION_CLIENT,
      fetchPolicy: 'network-only',
    },
  );

  const loading =
    type === 'incident'
      ? incidentQuery.loading
      : type === 'action'
      ? actionQuery.loading
      : idIncident
      ? ameliorationQuery.loading || incidentQuery.loading
      : ameliorationQuery.loading;

  const called =
    type === 'incident'
      ? incidentQuery.called
      : type === 'action'
      ? actionQuery.called
      : idIncident
      ? ameliorationQuery.called || incidentQuery.called
      : ameliorationQuery.called;

  useEffect(() => {
    if (!loading) {
      if (!isNewFiche) {
        if (!called) {
          if (incidentToEdit?.id || (id && type === 'incident')) {
            loadFicheIncident({
              variables: {
                id: incidentToEdit?.id || id || '',
              },
            });
          }
          if (type === 'amelioration' && (id || ameliorationToEdit?.id)) {
            loadFicheAmelioration({
              variables: {
                id: ameliorationToEdit?.id || id || '',
              },
            });
          }
          if (type === 'action' && (id || actionToEdit?.id)) {
            loadAction({
              variables: {
                id: actionToEdit?.id || id || '',
              },
            });
          }
        } else if (
          incidentQuery.data?.dQFicheIncident ||
          ameliorationQuery.data?.dQFicheAmelioration ||
          actionQuery.data?.dQActionOperationnelle
        ) {
          const incident = incidentQuery.data?.dQFicheIncident;
          const amelioration = ameliorationQuery.data?.dQFicheAmelioration;
          const action = actionQuery.data?.dQActionOperationnelle;
          const data = incident || amelioration || action;
          const idFicheIncident = incident?.id;
          const tache = data?.tache;
          const fonction = data?.fonction;
          const tacheAInformer = data?.tacheAInformer;
          const fonctionAInformer = data?.fonctionAInformer;
          const declarant = data?.declarant || data?.auteur;
          const userParticipants = data?.participants;
          const userParticiapantsAInformer = data.participantsAInformer;
          const importance = data?.importance;
          const userSuivi = data?.suiviPar;
          const origineAssocie = data?.origineAssocie;
          const origine = (origines || []).find(origine => origine.id === data?.origine?.id);
          const isPrivate =
            incidentQuery.data?.dQFicheIncident.isPrivate ||
            ameliorationQuery.data?.dQFicheAmelioration.isPrivate ||
            actionQuery.data?.dQActionOperationnelle.isPrivate ||
            false;

          setIsPrivate(isPrivate);
          if (action) {
            setFicheAction(action);
          }
          if (incident) setFicheIncident(incident);
          if (amelioration) {
            setFicheAmelioration(amelioration);
          }
          if (declarant) {
            setAuteur([declarant]);
          }
          if (userParticipants) {
            setUserParticipants(userParticipants);
          }
          if (userParticiapantsAInformer) {
            setUserParticipantsAInformer(userParticiapantsAInformer);
          }

          if (tache || fonction) {
            setTache({
              ...(tache || {}),
              idFonction: fonction?.id,
              idTache: tache?.id,
              id: tache?.idProjet || fonction?.idProjet,
            });
          }
          if (tacheAInformer || fonctionAInformer) {
            setTacheAInformer({
              ...(tacheAInformer || {}),
              idFonction: fonctionAInformer?.id,
              idTache: tacheAInformer?.id,
              id: tacheAInformer?.idProjet || fonctionAInformer?.idProjet,
            });
          }

          if (importance) {
            setIdImportance(importance.id);
          }
          if (idFicheIncident && ameliorationToCreate) {
            setIdFicheIncident(idFicheIncident);
          }
          if (userSuivi) {
            setUserSuivi([userSuivi]);
          }
          if (origineAssocie) {
            setOrigineAssocie(origine?.code === 'INTERNE' ? [origineAssocie] : origineAssocie);
          }
          if (origine) {
            setOrigine(origine);
          }
        }
      } else if (type === 'amelioration' && idIncident) {
        if (!called) {
          loadFicheIncident({
            variables: {
              id: idIncident,
            },
          });
        } else {
          const result = incidentQuery.data?.dQFicheIncident;
          if (result) {
            setFicheIncident(result);

            const tache = result?.tache;
            const fonction = result?.fonction;
            const tacheAInformer = result?.tacheAInformer;
            const fonctionAInformer = result?.fonctionAInformer;

            const declarant = result?.declarant || result?.auteur;
            const userParticipants = result?.participants;
            const userParticipantsAInformer = result?.participantsAInformer;
            const importance = result?.importance;
            const origineAssocie = result?.origineAssocie;
            const origine = (origines || []).find(origine => origine.id === result?.origine?.id);
            if (declarant) {
              setAuteur([declarant]);
            }
            if (userParticipants) {
              setUserParticipants(userParticipants);
            }
            if (userParticipantsAInformer) {
              setUserParticipantsAInformer(userParticipantsAInformer);
            }
            if (tache || fonction) {
              setTache({
                ...(tache || {}),
                idFonction: fonction?.id,
                idTache: tache?.id,
                id: tache?.idProjet || fonction?.idProjet,
              });
            }
            if (tacheAInformer || fonctionAInformer) {
              setTacheAInformer({
                ...(tacheAInformer || {}),
                idFonction: fonctionAInformer?.id,
                idTache: tacheAInformer?.id,
                id: tacheAInformer?.idProjet || fonctionAInformer?.idProjet,
              });
            }

            if (importance) {
              setIdImportance(importance.id);
            }
            if (origineAssocie) {
              setOrigineAssocie(origine?.code === 'INTERNE' ? [origineAssocie] : origineAssocie);
            }
            if (origine) {
              setOrigine(origine);
            }
          }
        }
      }
    }
  }, [
    id,
    idIncident,
    loading,
    origines,
    incidentToEdit,
    ameliorationToEdit,
    actionToEdit,
    ameliorationToCreate,
  ]);

  const [addFicheIncident, addingFicheIncident] = useMutation<CREATE_FICHE_INCIDENT_TYPE, any>(
    ameliorationToCreate ? CREATE_FICHE_AMELIORATION : CREATE_FICHE_INCIDENT,
    {
      onCompleted: () => {
        displaySnackBar(client, {
          message: `La fiche incident a été ${
            ameliorationToCreate ? 'transformée' : 'crée'
          } avec succès !`,
          type: 'SUCCESS',
          isOpen: true,
        });
        setMutationLoading(false);
        if (setOpenFormModal && refetch) {
          refetch();
          setOpenFormModal(false);
        }
        push('/demarche-qualite/fiche-incident');
      },
      onError: () => {
        displaySnackBar(client, {
          message: `Des erreurs se sont survenues pendant l'ajout de la fiche d'incident'`,
          isOpen: true,
          type: 'ERROR',
        });
        setMutationLoading(false);
      },
      update: (cache, dataResult) => {
        if (dataResult.data && dataResult.data.createOneDQFicheIncident) {
          const previousList = cache.readQuery<GET_FICHE_INCIDENTS_TYPE, any>({
            query: GET_FICHE_INCIDENTS,
          })?.dQFicheIncidents;

          cache.writeData<GET_FICHE_INCIDENTS_TYPE>({
            data: {
              dQFicheIncidents: {
                ...(previousList || {}),
                nodes: [...(previousList?.nodes || []), dataResult.data.createOneDQFicheIncident],
              } as any,
            },
          });
        }
      },
      client: FEDERATION_CLIENT,
    },
  );

  const [addFicheAmelioration, addingFicheAmelioration] = useMutation<
    CREATE_FICHE_AMELIORATION_TYPE,
    any
  >(CREATE_FICHE_AMELIORATION, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: "La fiche d'amélioration a été crée avec succès",
        type: 'SUCCESS',
        isOpen: true,
      });
      setMutationLoading(false);
      if (setOpenFormModal && refetch) {
        refetch();
        setOpenFormModal(false);
      }
      push('/demarche-qualite/fiche-amelioration');
    },
    onError: () => {
      displaySnackBar(client, {
        message: `Des erreurs se sont survenues pendant l'ajout de la fiche d'amélioration'`,
        isOpen: true,
        type: 'ERROR',
      });
      setMutationLoading(false);
    },
    update: (cache, dataResult) => {
      if (dataResult.data && dataResult.data.createOneDQFicheAmelioration) {
        const previousList = cache.readQuery<GET_FICHE_AMELIORATIONS_TYPE, any>({
          query: GET_FICHE_AMELIORATIONS,
        })?.dQFicheAmeliorations;

        cache.writeData<GET_FICHE_AMELIORATIONS_TYPE>({
          data: {
            dQFicheAmeliorations: {
              ...(previousList || {}),
              nodes: [...(previousList?.nodes || []), dataResult.data.createOneDQFicheAmelioration],
            } as any,
          },
        });
      }
    },
    client: FEDERATION_CLIENT,
  });

  const [addActionOperationnelle, addingActionOperationnelle] = useMutation<
    CREATE_ACTION_OPERATIONNELLE_TYPE,
    any
  >(CREATE_ACTION_OPERATIONNELLE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: "L'action opérationnelle a été crée avec succès",
        type: 'SUCCESS',
        isOpen: true,
      });
      setMutationLoading(false);
      if (setOpenFormModal && refetch) {
        refetch();
        setOpenFormModal(false);
      }
      push('/demarche-qualite/action-operationnelle');
    },
    onError: () => {
      displaySnackBar(client, {
        message: `Des erreurs se sont survenues pendant l'ajout de l'action opérationnelle'`,
        isOpen: true,
        type: 'ERROR',
      });
      setMutationLoading(false);
    },
    update: (cache, dataResult) => {
      if (dataResult.data && dataResult.data.createOneDQActionOperationnelle) {
        const previousList = cache.readQuery<GET_ACTIONS_OPERATIONNELLES_TYPE, any>({
          query: GET_ACTION_OPERATIONNELLE,
        })?.dQActionOperationnelles;

        cache.writeData<GET_ACTIONS_OPERATIONNELLES>({
          data: {
            dQActionOperationnelles: {
              ...(previousList || {}),
              nodes: [
                ...(previousList?.nodes || []),
                dataResult.data.createOneDQActionOperationnelle,
              ],
            } as any,
          },
        });
      }
    },
    client: FEDERATION_CLIENT,
  });

  const [updateFicheIncident, updatingFicheIncident] = useMutation<
    UPDATE_FICHE_INCIDENT_TYPE,
    UPDATE_FICHE_INCIDENTVariables
  >(UPDATE_FICHE_INCIDENT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: `La fiche incident a été modifiée avec succès !`,
        isOpen: true,
        type: 'SUCCESS',
      });
      if (setOpenFormModal && refetch) {
        refetch();
        setOpenFormModal(false);
      }
      push('/demarche-qualite/fiche-incident');
      setMutationLoading(false);
    },
    onError: () => {
      displaySnackBar(client, {
        message: `Des erreurs se sont survenues pendant la modification de la fiche d'incident`,
        isOpen: true,
        type: 'ERROR',
      });
      setMutationLoading(false);
    },
    client: FEDERATION_CLIENT,
  });

  const [updateFicheAmelioration, updatingFicheAmelioration] = useMutation<
    UPDATE_FICHE_AMELIORATION_TYPE,
    UPDATE_FICHE_AMELIORATIONVariables
  >(UPDATE_FICHE_AMELIORATION, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: `La fiche d'amélioration a été modifiée avec succès !`,
        isOpen: true,
        type: 'SUCCESS',
      });
      if (setOpenFormModal && refetch) {
        refetch();
        setOpenFormModal(false);
      }
      push('/demarche-qualite/fiche-amelioration');
      setMutationLoading(false);
    },
    onError: () => {
      displaySnackBar(client, {
        message: `Des erreurs se sont survenues pendant la modification de la fiche d'amélioration`,
        isOpen: true,
        type: 'ERROR',
      });
      setMutationLoading(false);
    },
    client: FEDERATION_CLIENT,
  });

  const [updateActionOperationnelle, updatingActionOperationnelle] = useMutation<
    UPDATE_ACTION_OPERATIONNELLE_TYPE,
    UPDATE_ACTION_OPERATIONNELLEVariables
  >(UPDATE_ACTION_OPERATIONNELLE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: `L' action opérationnelle a été modifiée avec succès !`,
        isOpen: true,
        type: 'SUCCESS',
      });
      if (setOpenFormModal && refetch) {
        refetch();
        setOpenFormModal(false);
      }
      push('/demarche-qualite/action-operationnelle');
      setMutationLoading(false);
    },
    onError: () => {
      displaySnackBar(client, {
        message: `Des erreurs se sont survenues pendant la modification de l'action opérationnelle'`,
        isOpen: true,
        type: 'ERROR',
      });
      setMutationLoading(false);
    },
    client: FEDERATION_CLIENT,
  });

  const handleSave = (ficheToSave: RequestSavingFicheIncident) => {
    setMutationLoading(true);
    const fichiers = (ficheToSave.fichiers || [])
      .filter(file => !file.size)
      .map((fichier: any) => ({
        chemin: fichier.chemin,
        nomOriginal: fichier.name,
        type: fichier.type,
      }));
    const fileToUpload = (ficheToSave.fichiers || []).filter(file => !!file.size);

    if (fileToUpload.length > 0) {
      handleUploadFiles(fileToUpload, ficheToSave, fichiers);
    } else {
      upsertFicheIncident(ficheToSave, fichiers);
    }
  };

  const [doCreatePutPresignedUrl] = useMutation<CREATE_PUT_PESIGNED_URL, any>(
    DO_CREATE_PUT_PESIGNED_URL,
    {
      client: FEDERATION_CLIENT,
    },
  );

  const handleUploadFiles = (
    fileToUpload: File[],
    ficheToSave: RequestSavingFicheIncident,
    fichiers: FichierInput[],
  ): void => {
    const filePaths = fileToUpload.map(fichier => formatFilename(fichier, ``));
    doCreatePutPresignedUrl({
      variables: {
        filePaths,
      },
    })
      .then(async response => {
        if (response.data?.createPutPresignedUrls) {
          try {
            await Promise.all(
              response.data.createPutPresignedUrls.map(async (presignedUrl, index) => {
                let fichier: any = undefined;
                if (presignedUrl) {
                  const file = fileToUpload[index];
                  const uploadResult = await uploadToS3(file, presignedUrl.presignedUrl);
                  if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                    fichier = {
                      chemin: presignedUrl.filePath,
                      nomOriginal: file.name,
                      type: file.type,
                    };
                  }
                }
                return fichier;
              }),
            ).then(fichiersFromUpload =>
              upsertFicheIncident(ficheToSave, [...fichiersFromUpload, ...fichiers]),
            );
          } catch (error) {
            displaySnackBar(client, {
              message: `Des erreurs se sont survenues pendant l'upload des fichiers''`,
              isOpen: true,
              type: 'ERROR',
            });
            setMutationLoading(false);
          }
        }
      })
      .catch(() =>
        displaySnackBar(client, {
          message: `Des erreurs se sont survenues pendant l'upload des fichiers''`,
          isOpen: true,
          type: 'ERROR',
        }),
      );
  };

  const upsertFicheIncident = (
    ficheToSave: Omit<RequestSavingFicheIncident, 'fichiers'>,
    fichiers: FichierInput[],
  ) => {
    if (idFicheIncident) {
      addFicheIncident({
        variables: {
          idFicheIncident,
          input: {
            description: ficheToSave.description,
            idOrigine: ficheToSave.idOrigine,
            idUserAuteur: ficheToSave.idUserDeclarant,
            fichiers,
            idImportance,
            dateIncident: ficheToSave.dateFiche,
            dateEcheance: ficheToSave.dateEcheance,
            idFonction: tache?.idFonction,
            idTache: tache?.idTache,
            idUserParticipants: [
              ...idUserParticipants.map(idUser => ({ idUser, type: 'PARTICIPANT_EN_CHARGE' })),
              ...idUserParticipantsAInformer.map(idUser => ({
                idUser,
                type: 'PARTICIPANT_A_INFORMER',
              })),
            ],
            idFonctionAInformer: tacheAInformer.idFonction,
            idTacheAInformer: tacheAInformer?.idTache,
            isPrivate,
          },
        },
      });
    } else {
      if (isNewFiche || id === 'new') {
        if (type === 'incident') {
          addFicheIncident({
            variables: {
              input: {
                ...ficheToSave,
                fichiers,
                idUserParticipants: [
                  ...idUserParticipants.map(idUser => ({ idUser, type: 'PARTICIPANT_EN_CHARGE' })),
                  ...idUserParticipantsAInformer.map(idUser => ({
                    idUser,
                    type: 'PARTICIPANT_A_INFORMER',
                  })),
                ],
                dateIncident: ficheToSave.dateFiche,
                dateFiche: undefined,
                isPrivate,
              },
            },
          });
        }
        if (type === 'amelioration') {
          addFicheAmelioration({
            variables: {
              idFicheIncident: idIncident,
              input: {
                ...ficheToSave,
                fichiers,
                idImportance,
                idFonction: tache?.idFonction,
                idUserAuteur: ficheToSave.idUserDeclarant,
                idUserDeclarant: undefined,
                idTache: tache?.idTache,
                idUserParticipants: [
                  ...idUserParticipants.map(idUser => ({ idUser, type: 'PARTICIPANT_EN_CHARGE' })),
                  ...idUserParticipantsAInformer.map(idUser => ({
                    idUser,
                    type: 'PARTICIPANT_A_INFORMER',
                  })),
                ],
                idFonctionAInformer: tacheAInformer?.idFonction,
                idTacheAInformer: tacheAInformer?.idTache,
                dateAmelioration: ficheToSave.dateFiche,
                dateFiche: undefined,
                isPrivate,
              },
            },
          });
        }
        if (type === 'action') {
          addActionOperationnelle({
            variables: {
              input: {
                ...ficheToSave,
                fichiers,
                idImportance,
                idFonction: tache?.idFonction,
                idUserSuivi: ficheToSave.idUserDeclarant,
                idUserAuteur: ficheToSave.idUserDeclarant,
                idUserDeclarant: undefined,
                idTache: tache?.idTache,
                idUserParticipants: [
                  ...idUserParticipants.map(idUser => ({ idUser, type: 'PARTICIPANT_EN_CHARGE' })),
                  ...idUserParticipantsAInformer.map(idUser => ({
                    idUser,
                    type: 'PARTICIPANT_A_INFORMER',
                  })),
                ],
                idFonctionAInformer: tacheAInformer?.idFonction,
                idTacheAInformer: tacheAInformer?.idTache,
                dateAction: ficheToSave.dateFiche,
                dateEcheance: ficheToSave.dateEcheance,
                dateFiche: undefined,
                isPrivate,
              },
            },
          });
        }
      } else {
        if (type === 'incident') {
          updateFicheIncident({
            variables: {
              id: ficheToSave.id as any,
              update: {
                ...(ficheToSave as any),
                idUserParticipants: [
                  ...idUserParticipants.map(idUser => ({ idUser, type: 'PARTICIPANT_EN_CHARGE' })),
                  ...idUserParticipantsAInformer.map(idUser => ({
                    idUser,
                    type: 'PARTICIPANT_A_INFORMER',
                  })),
                ],
                fichiers,
                id: undefined,
                dateIncident: ficheToSave.dateFiche,
                dateFiche: undefined,
                isPrivate,
              },
            },
          });
        }
        if (type === 'amelioration') {
          updateFicheAmelioration({
            variables: {
              id: ficheToSave.id as any,
              update: {
                ...(ficheToSave as any),
                idUserParticipants: [
                  ...idUserParticipants.map(idUser => ({ idUser, type: 'PARTICIPANT_EN_CHARGE' })),
                  ...idUserParticipantsAInformer.map(idUser => ({
                    idUser,
                    type: 'PARTICIPANT_A_INFORMER',
                  })),
                ],
                fichiers,
                idUserAuteur: (ficheToSave as any).idUserAuteur || ficheToSave.idUserDeclarant,
                idUserDeclarant: undefined,
                id: undefined,
                dateAmelioration: ficheToSave.dateFiche,
                dateFiche: undefined,
                isPrivate,
              },
            },
          });
        }
        if (type === 'action') {
          updateActionOperationnelle({
            variables: {
              id: ficheToSave.id as any,
              update: {
                ...(ficheToSave as any),
                idUserParticipants: [
                  ...idUserParticipants.map(idUser => ({ idUser, type: 'PARTICIPANT_EN_CHARGE' })),
                  ...idUserParticipantsAInformer.map(idUser => ({
                    idUser,
                    type: 'PARTICIPANT_A_INFORMER',
                  })),
                ],
                fichiers,
                idUserAuteur: ficheToSave.idUserDeclarant,
                idUserSuivi: ficheToSave.idUserDeclarant,
                idUserDeclarant: undefined,
                id: undefined,
                dateAction: ficheToSave.dateFiche,
                dateEcheance: ficheToSave.dateEcheance,
                dateFiche: undefined,
                isPrivate,
              },
            },
          });
        }
      }
    }
  };

  const handleGoBack = (): void => {
    setOpenFormModal && setOpenFormModal(false);
    const outil =
      type === 'action'
        ? 'action-operationnelle'
        : type === 'amelioration'
        ? 'fiche-amelioration'
        : 'fiche-incident';
    push(`/demarche-qualite/${outil}`);
  };

  const handleOpenAuteur = event => {
    event.stopPropagation();
    setOpenAuteurModal(true);
  };

  const handleOpenSuiviPar = event => {
    event.stopPropagation();
    setOpenSuiviParModal(true);
  };

  const handleSelectedAuteur = (selected: any) => {
    setAuteur(selected);
  };

  const handleSelectedUserSuivi = (selected: any) => {
    setUserSuivi(selected);
  };

  const idUserDeclarant = auteur && auteur[0]?.id;
  const idUserSuivi = userSuivi && userSuivi[0]?.id;
  const idUserParticipants = userParticipants.map(userParticipant => userParticipant.id);
  const idUserParticipantsAInformer = userParticipantsAInformer.map(
    userParticipant => userParticipant.id,
  );

  const auteurComponent = (
    <Box onClick={handleOpenAuteur} width="100%">
      <AvatarInput list={auteur} label={type === 'amelioration' ? 'Déclarant' : 'Auteur'} />
      <AssignTaskUserModal
        openModal={openAuteurModal}
        setOpenModal={setOpenAuteurModal}
        withNotAssigned={false}
        selected={auteur}
        setSelected={handleSelectedAuteur}
        title="Auteur"
        searchPlaceholder="Rechercher..."
        withAssignTeam={false}
        singleSelect={true}
      />
    </Box>
  );

  const suiviParComponent = (
    <Box onClick={handleOpenSuiviPar} width="100%">
      <AvatarInput list={userSuivi} label="Suivi Par" />
      <AssignTaskUserModal
        openModal={openSuiviParModal}
        setOpenModal={setOpenSuiviParModal}
        withNotAssigned={false}
        selected={userSuivi}
        setSelected={handleSelectedUserSuivi}
        title="Suivi Par"
        searchPlaceholder="Rechercher..."
        withAssignTeam={false}
        singleSelect={true}
      />
    </Box>
  );

  const origineInput = (
    <OrigineInput
      onChangeOrigine={setOrigine}
      onChangeOrigineAssocie={setOrigineAssocie}
      origineAssocie={origineAssocie}
      origine={origine}
    />
  );

  return (
    <>
      <Backdrop
        open={mutationLoading}
        value={`${isNewFiche || id === 'new' ? 'Création' : 'Modification'} en cours ...`}
      />
      <FicheIncidentFormPage
        type={type}
        mode={isNewFiche || id === 'new' ? 'creation' : 'modification'}
        loading={loading}
        saving={mutationLoading}
        causes={{
          data: causesQuery.data?.dQFicheCauses.nodes || [],
          error: causesQuery.error as any,
          loading: causesQuery.loading,
        }}
        types={{
          data: typesQuery.data?.dQFicheTypes.nodes || [],
          error: typesQuery.error as any,
          loading: typesQuery.loading,
        }}
        idUserSuivi={idUserSuivi}
        idFicheIncident={idFicheIncident}
        originInput={origineInput}
        idOrigine={origine ? (typeof origine === 'string' ? origine : origine?.id) : ''}
        idOrigineAssocie={
          origineAssocie
            ? typeof origineAssocie === 'string'
              ? origineAssocie
              : Array.isArray(origineAssocie)
              ? origineAssocie[0]?.id
              : origineAssocie.id
            : ''
        }
        idImportance={idImportance}
        idUserDeclarant={idUserDeclarant}
        idFonction={tache?.idFonction}
        idTache={tache?.idTache}
        idUserParticipants={idUserParticipants}
        selectAuteurComponent={auteurComponent}
        commonFieldsComponent={
          <CommonFieldsForm
            selectedUsers={userParticipants}
            urgence={null}
            urgenceProps={{
              useCode: false,
            }}
            usersModalProps={{
              withNotAssigned: false,
              searchPlaceholder: 'Rechercher...',
              withAssignTeam: false,
              singleSelect: false,
              title: 'Choix de collaborateurs',
            }}
            isPrivate={isPrivate}
            onChangePrivate={setIsPrivate}
            selectUsersFieldLabel="Collaborateurs"
            projet={tache}
            onChangeUsersSelection={setUserParticipants}
            onChangeProjet={projet => {
              setTache(projet);
            }}
          />
        }
        commonFieldsParticipantsAInformerComponent={
          <CommonFieldsForm
            selectedUsers={userParticipantsAInformer}
            urgence={null}
            urgenceProps={{
              useCode: false,
            }}
            usersModalProps={{
              withNotAssigned: false,
              searchPlaceholder: 'Rechercher...',
              withAssignTeam: false,
              singleSelect: false,
            }}
            projet={tacheAInformer}
            importance={idImportance}
            onChangeUsersSelection={setUserParticipantsAInformer}
            onChangeProjet={projet => {
              setTacheAInformer(projet);
            }}
            onChangeImportance={importance => setIdImportance(importance.id)}
            radioImportance={true}
          />
        }
        selectSuiviParComponent={suiviParComponent}
        ficheIncident={
          !isNewFiche || (type === 'amelioration' && idIncident) ? ficheIncident : undefined
        }
        ficheAmelioration={!isNewFiche ? ficheAmelioration : undefined}
        ficheAction={!isNewFiche ? ficheAction : undefined}
        onRequestGoBack={handleGoBack}
        onRequestSave={handleSave}
      />
    </>
  );
};

export default withRouter(FicheIncidentForm);
