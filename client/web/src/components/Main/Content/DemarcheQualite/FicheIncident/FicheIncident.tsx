import { useApolloClient, useLazyQuery, useMutation } from '@apollo/react-hooks';
import { FicheIncident, FicheIncidentChange, FicheIncidentPage, CustomModal } from '@app/ui-kit';
import { Box, CssBaseline, Hidden, IconButton } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import FilterAlt from '../../../../../assets/icons/todo/filter_alt_white.svg';
import {
  DELETE_FICHE_INCIDENT,
  UPDATE_FICHE_INCIDENT,
} from '../../../../../federation/demarche-qualite/fiche-incident/mutation';
import {
  GET_FICHE_INCIDENTS,
  GET_FICHE_INCIDENT_AGGREGATE,
} from '../../../../../federation/demarche-qualite/fiche-incident/query';
import {
  DELETE_FICHE_INCIDENT as DELETE_FICHE_INCIDENT_TYPE,
  DELETE_FICHE_INCIDENTVariables,
} from '../../../../../federation/demarche-qualite/fiche-incident/types/DELETE_FICHE_INCIDENT';
import {
  GET_FICHE_INCIDENTS as GET_FICHE_INCIDENTS_TYPE,
  GET_FICHE_INCIDENTSVariables,
} from '../../../../../federation/demarche-qualite/fiche-incident/types/GET_FICHE_INCIDENTS';
import {
  GET_FICHE_INCIDENT_AGGREGATE as GET_FICHE_INCIDENT_AGGREGATE_TYPE,
  GET_FICHE_INCIDENT_AGGREGATEVariables,
} from '../../../../../federation/demarche-qualite/fiche-incident/types/GET_FICHE_INCIDENT_AGGREGATE';
import { UPDATE_FICHE_INCIDENT as UPDATE_FICHE_INCIDENT_TYPE } from '../../../../../federation/demarche-qualite/fiche-incident/types/UPDATE_FICHE_INCIDENT';
import { GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS } from '../../../../../federation/demarche-qualite/origine/query';
import { GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS as GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS_TYPE } from '../../../../../federation/demarche-qualite/origine/types/GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS';
import { GET_STATUTS_ORDER_BY_NOMBRE_FICHE_INCIDENTS } from '../../../../../federation/demarche-qualite/statut/query';
import { GET_STATUTS_ORDER_BY_NOMBRE_FICHE_INCIDENTS as GET_STATUTS_ORDER_BY_NOMBRE_FICHE_INCIDENTS_TYPE } from '../../../../../federation/demarche-qualite/statut/types/GET_STATUTS_ORDER_BY_NOMBRE_FICHE_INCIDENTS';
import { getPharmacie } from '../../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import StatutInput from '../../../../Common/StatutInput';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { useImportance, useUrgence } from '../../../../hooks';
import ImportanceFilter from '../../TodoNew/Common/ImportanceFilter';
import { ImportanceInterface } from '../../TodoNew/Common/ImportanceFilter/ImportanceFilterList';
import UrgenceFilter from '../../TodoNew/Common/UrgenceFilter';
import { UrgenceInterface } from '../../TodoNew/Common/UrgenceFilter/UrgenceFilterList';
import { sortStatuts } from '../util';
import FicheIncidentForm from './FicheIncidentForm/FicheIncidentForm';
import useStyles from './styles';

const FicheIncidentManagement: FC<RouteComponentProps> = ({ history: { push } }) => {
  const client = useApolloClient();
  const idPharmacie = getPharmacie().id;

  const loadingImportances = useImportance();
  const loadingUrgences = useUrgence();

  const classes = useStyles();

  const [filterChange, setFilterChange] = useState<FicheIncidentChange>();
  const [urgence, setUrgence] = useState<UrgenceInterface[] | null | undefined>();
  const [importances, setImportances] = useState<ImportanceInterface[] | null | undefined>([]);

  const [openFormModal, setOpenFormModal] = useState<boolean>(false);

  const [ficheToEdit, setFicheToEdit] = useState<FicheIncident>();

  const [ameliorationToCreate, setAmeliorationToCreate] = useState<any>();

  const importancesData = loadingImportances.data?.importances.nodes;
  const urgencesData = loadingUrgences.data?.urgences.nodes;

  useEffect(() => {
    if (!loadingUrgences.loading && urgencesData) {
      setUrgence((urgencesData || []) as any);
    }
  }, [loadingUrgences.loading, urgencesData]);

  useEffect(() => {
    if (!loadingImportances.loading && importancesData) {
      setImportances((importancesData || []) as any);
    }
  }, [loadingImportances.loading, importancesData]);

  useEffect(() => {
    handleRequestSearch();
  }, [filterChange, urgence, importances]);

  const [searchFichesIncident, searchingFichesIncident] = useLazyQuery<
    GET_FICHE_INCIDENTS_TYPE,
    GET_FICHE_INCIDENTSVariables
  >(GET_FICHE_INCIDENTS, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const [loadOrigines, origines] = useLazyQuery<
    GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS_TYPE,
    any
  >(GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const [loadStatuts, statuts] = useLazyQuery<
    GET_STATUTS_ORDER_BY_NOMBRE_FICHE_INCIDENTS_TYPE,
    any
  >(GET_STATUTS_ORDER_BY_NOMBRE_FICHE_INCIDENTS, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const [loadTotalFilteredFicheIncident, loadingFilteredTotal] = useLazyQuery<
    GET_FICHE_INCIDENT_AGGREGATE_TYPE,
    GET_FICHE_INCIDENT_AGGREGATEVariables
  >(GET_FICHE_INCIDENT_AGGREGATE, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const [loadTotalFicheIncidents, loadingTotal] = useLazyQuery<
    GET_FICHE_INCIDENT_AGGREGATE_TYPE,
    GET_FICHE_INCIDENT_AGGREGATEVariables
  >(GET_FICHE_INCIDENT_AGGREGATE, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const [
    loadTotalHasFicheAmeliorationFicheIncidents,
    loadingTotalHasFicheAmelioration,
  ] = useLazyQuery<GET_FICHE_INCIDENT_AGGREGATE_TYPE, GET_FICHE_INCIDENT_AGGREGATEVariables>(
    GET_FICHE_INCIDENT_AGGREGATE,
    {
      client: FEDERATION_CLIENT,
      fetchPolicy: 'cache-and-network',
    },
  );

  const handleRefetch = () => {
    origines.refetch();
    statuts.refetch();
    loadingFilteredTotal.refetch();
    searchingFichesIncident.refetch();
    loadingTotalHasFicheAmelioration.refetch();
    loadingTotal.refetch();
  };

  const [deleteFicheIncident] = useMutation<
    DELETE_FICHE_INCIDENT_TYPE,
    DELETE_FICHE_INCIDENTVariables
  >(DELETE_FICHE_INCIDENT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: "La fiche d'incident a été supprimée avec succès !",
        type: 'SUCCESS',
        isOpen: true,
      });
      handleRefetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs se sont survenues pendant la suppression de la fiche d'incident",
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [updateFicheIncident] = useMutation<UPDATE_FICHE_INCIDENT_TYPE, any>(
    UPDATE_FICHE_INCIDENT,
    {
      onCompleted: () => {
        displaySnackBar(client, {
          isOpen: true,
          message: 'Le statut de la fiche incident a été modifiée avec succès !',
          type: 'SUCCESS',
        });

        statuts.refetch();
      },
      onError: () => {
        displaySnackBar(client, {
          message:
            'Des erreurs se sont survenues pendant la modification de statut de la fiche incident !',
          isOpen: true,
          type: 'ERROR',
        });
      },
      client: FEDERATION_CLIENT,
    },
  );

  const handleRequestGoBack = (): void => {
    push(`/demarche-qualite/outils`);
  };

  const handleRequestAddFicheAmelioration = (fiche: FicheIncident) => {
    push(`/demarche-qualite/fiche-amelioration/new/${fiche.id}`);
  };

  const handleRequestShowDetailFicheAmelioration = (fiche: any): void => {
    push(`/demarche-qualite/fiche-amelioration-detail/${fiche.idFicheAmelioration}`);
  };

  const handleRequestAdd = (): void => {
    push(`/demarche-qualite/fiche-incident/new`);
  };

  const handleRequestShowDetail = (fiche: FicheIncident): void => {
    push({
      pathname: `/demarche-qualite/fiche-incident-detail/${fiche.id}`,
      state: { incident: fiche },
    });
  };

  const handleRequestDelete = (fiches: FicheIncident[]): void => {
    Promise.all(
      fiches.map(({ id }) => {
        return deleteFicheIncident({
          variables: {
            input: {
              id,
            },
          },
        });
      }),
    );
  };

  const handleRequestEdit = (fiche: FicheIncident): void => {
    push(`/demarche-qualite/fiche-incident/${fiche.id}`);
  };

  const handleRequestSearch = (): void => {
    if (filterChange) {
      if (filterChange.searchTextChanged) {
        loadOrigines({
          variables: {
            searchText: filterChange.searchText,
          },
        });

        loadStatuts({
          variables: {
            searchText: filterChange.searchText,
          },
        });

        loadTotalHasFicheAmeliorationFicheIncidents({
          variables: {
            filter: {
              and: [
                {
                  idPharmacie: {
                    eq: idPharmacie,
                  },
                },
                {
                  idFicheAmelioration: {
                    isNot: null,
                  },
                },
                {
                  description: {
                    iLike: `%${filterChange.searchText}%`,
                  },
                },
              ],
            },
          },
        });

        loadTotalFicheIncidents({
          variables: {
            filter: {
              and: [
                {
                  idPharmacie: {
                    eq: idPharmacie,
                  },
                },
                {
                  description: {
                    iLike: `%${filterChange.searchText}%`,
                  },
                },
              ],
            },
          },
        });
      }

      let filterList: any[] | undefined = [
        {
          idPharmacie: {
            eq: idPharmacie,
          },
        },
      ];

      const urgenceIds = urgence
        ?.filter(urgence => urgence && urgence.id && urgence.checked)
        .map(urgence => urgence && urgence.id) as any;
      const importancesIds = importances
        ?.filter(importance => importance.checked)
        .map(importance => importance.id);

      if (urgenceIds && urgenceIds.length > 0) {
        filterList = [
          ...(filterList || []),
          {
            idUrgence: {
              in: urgenceIds,
            },
          },
        ];
      }

      if (importancesIds && importancesIds.length > 0) {
        filterList = [
          ...(filterList || []),
          {
            idImportance: {
              in: importancesIds,
            },
          },
        ];
      }

      if (filterChange.originesFiltersSelected.length > 0) {
        filterList = [
          ...(filterList || []),
          {
            idOrigine: {
              in: filterChange.originesFiltersSelected.map(({ id }) => id),
            },
          },
        ];
      }

      if (filterChange.statutsFiltersSelected.length > 0) {
        filterList = [
          ...(filterList || []),
          {
            idStatut: {
              in: filterChange.statutsFiltersSelected.map(({ id }) => id),
            },
          },
        ];
      }

      if (filterChange.searchText) {
        filterList = [
          ...(filterList || []),
          {
            description: {
              iLike: `%${filterChange.searchText}%`,
            },
          },
        ];
      }

      if (filterChange.hasFicheAmeliorationFilterSelected !== 'ALL') {
        if (filterChange.hasFicheAmeliorationFilterSelected === 'NO') {
          filterList = [
            ...(filterList || []),
            {
              idFicheAmelioration: {
                is: null,
              },
            },
          ];
        } else {
          filterList = [
            ...(filterList || []),
            {
              idFicheAmelioration: {
                isNot: null,
              },
            },
          ];
        }
      }

      const parameters = filterList
        ? {
            variables: {
              filter: {
                and: filterList,
              },
            },
          }
        : {};

      searchFichesIncident({
        variables: {
          ...(parameters.variables || {}),
          paging: {
            offset: filterChange.skip,
            limit: filterChange.take,
          },
        },
      });
      loadTotalFilteredFicheIncident(parameters);
    }
  };

  const handleRequestStatutChange = (currentIncident: FicheIncident, newId: string): void => {
    updateFicheIncident({
      variables: {
        id: currentIncident.id,
        update: {
          idStatut: newId,
          idOrigine: currentIncident.origine.id,
          idUserDeclarant: currentIncident.declarant.id,
          description: currentIncident.description,
          idImportance: currentIncident.importance.id,
          idFonction: currentIncident.fonction?.id,
          idTache: currentIncident.tache?.id,
          idUserParticipants: [
            ...(currentIncident.participants || []).map(({ id }) => ({
              idUser: id,
              type: 'PARTICIPANT_EN_CHARGE',
            })),
            ...(currentIncident.participantsAInformer || []).map(({ id }) => ({
              idUser: id,
              type: 'PARTICIPANT_A_INFORMER',
            })),
          ],
          dateEcheance: currentIncident.dateEcheance,
          dateIncident: currentIncident.dateIncident,
          idOrigineAssocie: currentIncident.idOrigineAssocie,
          idType: currentIncident.type.id,
          idCause: currentIncident.cause.id,
        },
      },
    });
  };

  const handleRequestOpenFormModal = (fiche: FicheIncident | undefined) => {
    if (fiche) {
      setFicheToEdit(fiche);
      setOpenFormModal(true);
    }
  };

  const handleRequestOpenAmeliorationFormModal = (fiche: FicheIncident | undefined) => {
    if (fiche) {
      setAmeliorationToCreate(fiche);
      setOpenFormModal(true);
    }
  };

  const handleCloseFormModal = (value: boolean) => {
    setOpenFormModal(value);
    setFicheToEdit(undefined);
    setAmeliorationToCreate(undefined);
  };

  const fiche: any = searchingFichesIncident.data?.dQFicheIncidents.nodes[0];

  const handleStatutInputChange = (event: any) => {
    const newId = event?.target?.value;
    if (newId && fiche) {
      handleRequestStatutChange(fiche, newId);
    }
  };

  const urgenceFilter = (
    <UrgenceFilter withDropdown={false} urgence={urgence} setUrgence={setUrgence} />
  );

  const importanceFilter = (
    <ImportanceFilter
      withDropdown={false}
      importances={importances}
      setImportances={setImportances}
    />
  );

  const statutsInput = (
    <StatutInput
      value={fiche?.statut}
      handleChange={handleStatutInputChange}
      filterType="INCIDENT"
    />
  );

  const totalFicheIncidents =
    (loadingTotal.data?.dQFicheIncidentAggregate.length || 0) > 0
      ? loadingTotal.data?.dQFicheIncidentAggregate[0].count?.id || 0
      : 0;
  const totalFicheIncidentsAvecFicheAmeliorations =
    (loadingTotalHasFicheAmelioration.data?.dQFicheIncidentAggregate.length || 0) > 0
      ? loadingTotalHasFicheAmelioration.data?.dQFicheIncidentAggregate[0].count?.id || 0
      : 0;

  const handleCreateClick = () => {
    setFicheToEdit(undefined);
    setOpenFormModal(true);
  };
  return (
    <>
      <CssBaseline />
      <FicheIncidentPage
        loading={searchingFichesIncident.loading || loadingFilteredTotal.loading}
        error={searchingFichesIncident.error || loadingFilteredTotal.error}
        origines={{
          ...origines,
          data: origines.data?.origines.nodes || [],
        }}
        statuts={{
          ...statuts,
          data: sortStatuts(
            (statuts.data?.dQStatutsOrderByNombreFicheIncidents || []).filter(
              ({ type }: any) => type === 'INCIDENT',
            ) as any,
          ),
        }}
        ficheAmeliorationStats={{
          loading: loadingTotal.loading || loadingTotalHasFicheAmelioration.loading,
          error: loadingTotalHasFicheAmelioration.error,
          data: [
            {
              id: 'ALL',
              libelle: 'Tous',
              total: totalFicheIncidents,
            },
            {
              id: 'YES',
              libelle: 'Oui',
              total: totalFicheIncidentsAvecFicheAmeliorations,
            },
            {
              id: 'NO',
              libelle: 'No',
              total: totalFicheIncidents - totalFicheIncidentsAvecFicheAmeliorations,
            },
          ],
        }}
        rowsTotal={
          (loadingFilteredTotal.data?.dQFicheIncidentAggregate.length || 0) > 0
            ? loadingFilteredTotal.data?.dQFicheIncidentAggregate[0].count?.id || 0
            : 0
        }
        onRequestAdd={handleRequestAdd}
        onRequestGoBack={handleRequestGoBack}
        onRequestDelete={handleRequestDelete}
        onRequestEdit={handleRequestEdit}
        onRequestShowDetails={handleRequestShowDetail}
        onRequestShowFicheAmeliorationDetails={handleRequestShowDetailFicheAmelioration}
        onRequestAddFicheAmelioration={handleRequestAddFicheAmelioration}
        onRequestSearch={setFilterChange}
        onRequestStatutChange={handleRequestStatutChange}
        onRequestOpenFormModal={handleRequestOpenFormModal}
        onRequestOpenAmeliorationFormModal={handleRequestOpenAmeliorationFormModal}
        data={searchingFichesIncident.data?.dQFicheIncidents.nodes as any}
        urgenceFilter={urgenceFilter}
        importanceFilter={importanceFilter}
        statutsInput={statutsInput}
        filterIcon={<img src={FilterAlt} />}
        onRequestGoReunion={item =>
          item.reunion?.id && push(`/demarche-qualite/compte-rendu/details/${item.reunion.id}`)
        }
      />
      <Hidden mdUp={true} implementation="css">
        <Box className={classes.createButton}>
          <IconButton onClick={handleCreateClick}>
            <Add />
          </IconButton>
        </Box>
      </Hidden>
      <CustomModal
        title={
          ameliorationToCreate
            ? 'Transformation incident'
            : ficheToEdit
            ? 'Modification incident'
            : 'Nouvel incident'
        }
        open={openFormModal}
        setOpen={handleCloseFormModal}
        closeIcon={true}
        headerWithBgColor={true}
        withBtnsActions={false}
        fullScreen={true}
        maxWidth="xl"
        noDialogContent={true}
      >
        <FicheIncidentForm
          incidentToEdit={ficheToEdit}
          ameliorationToCreate={ameliorationToCreate}
          setOpenFormModal={handleCloseFormModal}
          refetch={handleRefetch}
        />
      </CustomModal>
    </>
  );
};

export default withRouter(FicheIncidentManagement);
