import React, { FC, useState, ChangeEvent, useEffect } from 'react';
import {
  DetailsFicheAmelioration as DetailsAmelioration,
  FicheAmelioration,
  RequestSavingFicheAmeliorationAction,
} from '@app/ui-kit';
import { RouteComponentProps } from 'react-router';
import { useApolloClient, useMutation, useQuery, useLazyQuery } from '@apollo/react-hooks';

import {
  CREATE_FICHE_AMELIORATION_ACTION as CREATE_FICHE_AMELIORATION_ACTION_TYPE,
  CREATE_FICHE_AMELIORATION_ACTIONVariables,
} from '../../../../../../federation/demarche-qualite/fiche-amelioration/types/CREATE_FICHE_AMELIORATION_ACTION';
import {
  GET_FICHE_AMELIORATION as GET_FICHE_AMELIORATION_TYPE,
  GET_FICHE_AMELIORATIONVariables,
} from '../../../../../../federation/demarche-qualite/fiche-amelioration/types/GET_FICHE_AMELIORATION';
import {
  GET_FICHE_AMELIORATION_ACTION as GET_FICHE_AMELIORATION_ACTION_TYPE,
  GET_FICHE_AMELIORATION_ACTIONVariables,
} from '../../../../../../federation/demarche-qualite/fiche-amelioration/types/GET_FICHE_AMELIORATION_ACTION';

import {
  GET_FICHE_AMELIORATION,
  GET_FICHE_AMELIORATION_ACTION,
  GET_FICHE_AMELIORATIONS,
} from '../../../../../../federation/demarche-qualite/fiche-amelioration/query';
import { DELETE_FICHE_AMELIORATION } from '../../../../../../federation/demarche-qualite/fiche-amelioration/mutation';
import { CREATE_FICHE_AMELIORATION_ACTION } from '../../../../../../federation/demarche-qualite/fiche-amelioration/mutation';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import Image from '../../../../../../assets/img/Demarche-qualite/ficheAmelioration/success.png';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import {
  DELETE_FICHE_AMELIORATION as DELETE_FICHE_AMELIORATION_TYPE,
  DELETE_FICHE_AMELIORATIONVariables,
} from '../../../../../../federation/demarche-qualite/fiche-amelioration/types/DELETE_FICHE_AMELIORATION';
import {
  GET_FICHE_AMELIORATIONS as GET_FICHE_AMELIORATIONS_TYPE,
  GET_FICHE_AMELIORATIONSVariables,
} from '../../../../../../federation/demarche-qualite/fiche-amelioration/types/GET_FICHE_AMELIORATIONS';
import { uploadToS3 } from '../../../../../../services/S3';
import { formatFilename } from '../../../../../Common/Dropzone/Dropzone';
import { getPharmacie } from '../../../../../../services/LocalStorage';

const DetailsFicheAmelioration: FC<RouteComponentProps> = ({
  history: { push },
  match: { params },
}) => {
  const pharmacie = getPharmacie()

  const ficheAmeliorationId = (params as any).id;
  const [savingAmeliorationAction, setSavingAmeliorationAction] = useState<boolean>(false);

  const client = useApolloClient();

  const showError = (
    message: string = "Des erreurs se sont survenues pendant l'ajout de l'action",
  ): void => {
    displaySnackBar(client, {
      type: 'ERROR',
      message: message,
      isOpen: true,
    });
  };

  const [loadAction, loadingAction] = useLazyQuery<
    GET_FICHE_AMELIORATION_ACTION_TYPE,
    GET_FICHE_AMELIORATION_ACTIONVariables
  >(GET_FICHE_AMELIORATION_ACTION, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'network-only',
  });

  const { loading, data, error } = useQuery<
    GET_FICHE_AMELIORATION_TYPE,
    GET_FICHE_AMELIORATIONVariables
  >(
    GET_FICHE_AMELIORATION,

    {
      onCompleted: (ficheResult) => {
        if (ficheResult.dQFicheAmelioration?.idAction) {
          loadAction({
            variables: {
              id: ficheResult.dQFicheAmelioration.idAction,
            },
          });
        }
      },
      variables: {
        id: ficheAmeliorationId
      },
      client: FEDERATION_CLIENT,
      fetchPolicy: 'network-only',
    },
  );

  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
  });

  const [createFicheAmeliorationAction] = useMutation<
    CREATE_FICHE_AMELIORATION_ACTION_TYPE,
    CREATE_FICHE_AMELIORATION_ACTIONVariables
  >(CREATE_FICHE_AMELIORATION_ACTION, {
    onCompleted: (result) => {
      displaySnackBar(client, {
        message: "L'action d'amélioration a été ajoutée avec succès !",
        type: 'SUCCESS',
        isOpen: true,
      });

      // Reload action
      loadAction({
        variables: {
          id: result.createOneDQFicheAmeliorationAction.id,
        },
      });

      setSavingAmeliorationAction(false);
    },

    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs se sont survenues pendant l'ajout de l'action d'amélioration",
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [deleteFicheAmelioration, deletionFicheAmelioration] = useMutation<
    DELETE_FICHE_AMELIORATION_TYPE,
    DELETE_FICHE_AMELIORATIONVariables
  >(DELETE_FICHE_AMELIORATION, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La fiche amélioration a été supprimée avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });

      push('/demarche-qualite/fiche-amelioration');
    },
    update: (cache, deletionResult) => {
      if (
        deletionResult.data &&
        deletionResult.data.deleteOneDQFicheAmelioration &&
        deletionResult.data.deleteOneDQFicheAmelioration.id
      ) {
        const previousList = cache.readQuery<
          GET_FICHE_AMELIORATIONS_TYPE,
          GET_FICHE_AMELIORATIONSVariables
        >({
          query: GET_FICHE_AMELIORATIONS,
        })?.dQFicheAmeliorations;

        const deletedId = deletionResult.data.deleteOneDQFicheAmelioration.id;

        cache.writeData<GET_FICHE_AMELIORATIONS_TYPE>({
          data: {

              dQFicheAmeliorations: {  ...(previousList || {}),  nodes: (previousList?.nodes || []).filter((item) => item?.id !== deletedId)} as any,
          },
        });
      }
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression de la fiche amélioration',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  useEffect(() => {
    if (data?.dQFicheAmelioration?.idAction) {
      loadAction({
        variables: {
          id: data.dQFicheAmelioration.idAction,
        },
      });
    }
  }, [data]);

  const handleDelete = (fiche: FicheAmelioration) => {
    deleteFicheAmelioration({
      variables: {
        input: {
          id: fiche.id,
        },
      },
    });
  };

  const handleSaveAction = (
    fiche: FicheAmelioration,
    action: RequestSavingFicheAmeliorationAction,
  ) => {
    setSavingAmeliorationAction(true);
    if (action.fichiers.length > 0) {
      const filePaths = action.fichiers.map((fichier) => formatFilename(fichier, ``));
      doCreatePutPresignedUrl({
        variables: {
          filePaths,
        },
      })
        .then(async (response) => {
          if (response.data?.createPutPresignedUrls) {
            try {
              const fichiers = await Promise.all(
                response.data.createPutPresignedUrls.map((item, index) => {
                  //FIXME : Make sur the order of presigned urls not change
                  if (item?.presignedUrl) {
                    return uploadToS3(action.fichiers[index], item?.presignedUrl).then(
                      (uploadResult) => {
                        if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                          return {
                            chemin: item.filePath,
                            nomOriginal: action.fichiers[index].name,
                            type: action.fichiers[index].type,
                          };
                        }
                      },
                    );
                  }
                }),
              );

              createFicheAmeliorationAction({
                variables: {
                  idFicheAmelioration: fiche.id,
                  action: {
                    ...action,
                    fichiers: fichiers as any,
                  },
                },
              });

              return;
            } catch (error) {}
          }

          showError();
        })
        .catch(() => showError());
    } else {
      createFicheAmeliorationAction({
        variables: {
          idFicheAmelioration: fiche.id,
          action: {
            ...action,
            fichiers: [],
          },
        },
      });
    }
  };

  const handleEdit = (fiche: FicheAmelioration): void => {
    push(`/demarche-qualite/fiche-amelioration/${fiche.id}`);
  };

  const handleGoBack = (): void => {
    push(`/demarche-qualite/fiche-amelioration`);
  };

  return (
    <DetailsAmelioration
      noContentActionImageSrc={Image}
      loading={loading || deletionFicheAmelioration.loading}
      error={error || deletionFicheAmelioration.error}
      ficheAmelioration={data?.dQFicheAmelioration as any}
      ficheAmeliorationAction={{
        ...loadingAction,
        data: loadingAction.data?.dQFicheAmeliorationAction as any,
        saving: savingAmeliorationAction,
      }}
      onRequestEdit={handleEdit}
      onRequestGoBack={handleGoBack}
      onRequestDelete={handleDelete}
      onRequestSaveAction={handleSaveAction}
    />
  );
};

export default DetailsFicheAmelioration;
