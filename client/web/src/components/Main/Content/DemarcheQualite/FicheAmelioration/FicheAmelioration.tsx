import { useApolloClient, useLazyQuery, useMutation } from '@apollo/react-hooks';
import {
  CustomModal,
  FicheAmelioration,
  FicheAmeliorationChange,
  FicheAmeliorationPage,
} from '@app/ui-kit';
import { Box, Hidden, IconButton } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router';
import FilterAlt from '../../../../../assets/icons/todo/filter_alt_white.svg';
import { GET_ACTIVITES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS } from '../../../../../federation/demarche-qualite/activite/query';
import {
  GET_ACTIVITES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS as GET_ACTIVITES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS_TYPE,
  GET_ACTIVITES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONSVariables,
} from '../../../../../federation/demarche-qualite/activite/types/GET_ACTIVITES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS';
import {
  DELETE_FICHE_AMELIORATION,
  UPDATE_FICHE_AMELIORATION,
} from '../../../../../federation/demarche-qualite/fiche-amelioration/mutation';
import {
  GET_FICHE_AMELIORATIONS,
  GET_FICHE_AMELIORATION_AGGREGATE,
} from '../../../../../federation/demarche-qualite/fiche-amelioration/query';
import {
  DELETE_FICHE_AMELIORATION as DELETE_FICHE_AMELIORATION_TYPE,
  DELETE_FICHE_AMELIORATIONVariables,
} from '../../../../../federation/demarche-qualite/fiche-amelioration/types/DELETE_FICHE_AMELIORATION';
import {
  GET_FICHE_AMELIORATIONS as GET_FICHE_AMELIORATIONS_TYPE,
  GET_FICHE_AMELIORATIONSVariables,
} from '../../../../../federation/demarche-qualite/fiche-amelioration/types/GET_FICHE_AMELIORATIONS';
import {
  GET_FICHE_AMELIORATION_AGGREGATE as GET_FICHE_AMELIORATION_AGGREGATE_TYPE,
  GET_FICHE_AMELIORATION_AGGREGATEVariables,
} from '../../../../../federation/demarche-qualite/fiche-amelioration/types/GET_FICHE_AMELIORATION_AGGREGATE';
import {
  UPDATE_FICHE_AMELIORATION as UPDATE_FICHE_AMELIORATION_TYPE,
  UPDATE_FICHE_AMELIORATIONVariables,
} from '../../../../../federation/demarche-qualite/fiche-amelioration/types/UPDATE_FICHE_AMELIORATION';
import { GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS } from '../../../../../federation/demarche-qualite/origine/query';
import { GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS as GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS_TYPE } from '../../../../../federation/demarche-qualite/origine/types/GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS';
import { GET_STATUTS_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS } from '../../../../../federation/demarche-qualite/statut/query';
import {
  GET_STATUTS_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS as GET_STATUTS_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS_TYPE,
  GET_STATUTS_ORDER_BY_NOMBRE_FICHE_AMELIORATIONSVariables,
} from '../../../../../federation/demarche-qualite/statut/types/GET_STATUTS_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS';
import { getPharmacie } from '../../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import StatutInput from '../../../../Common/StatutInput';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { useImportance, useUrgence } from '../../../../hooks';
import ImportanceFilter from '../../TodoNew/Common/ImportanceFilter';
import { ImportanceInterface } from '../../TodoNew/Common/ImportanceFilter/ImportanceFilterList';
import UrgenceFilter from '../../TodoNew/Common/UrgenceFilter';
import { UrgenceInterface } from '../../TodoNew/Common/UrgenceFilter/UrgenceFilterList';
import FicheIncidentForm from '../FicheIncident/FicheIncidentForm/FicheIncidentForm';
import { sortStatuts } from '../util';
import useStyles from './styles';

const FicheAmeliorationManagement: FC<RouteComponentProps> = ({ history: { push } }) => {
  const client = useApolloClient();
  const idPharmacie = getPharmacie().id;

  const loadingImportances = useImportance();
  const loadingUrgences = useUrgence();

  const classes = useStyles();

  const [filterChange, setFilterChange] = useState<FicheAmeliorationChange>();
  const [urgence, setUrgence] = useState<UrgenceInterface[] | null | undefined>();
  const [importances, setImportances] = useState<ImportanceInterface[] | null | undefined>([]);

  const [openFormModal, setOpenFormModal] = useState<boolean>(false);

  const [ficheToEdit, setFicheToEdit] = useState<FicheAmelioration>();

  const importancesData = loadingImportances.data?.importances.nodes;
  const urgencesData = loadingUrgences.data?.urgences.nodes;

  useEffect(() => {
    if (!loadingUrgences.loading && urgencesData) {
      setUrgence((urgencesData || []) as any);
    }
  }, [loadingUrgences.loading, urgencesData]);

  useEffect(() => {
    if (!loadingImportances.loading && importancesData) {
      setImportances((importancesData || []) as any);
    }
  }, [loadingImportances.loading, importancesData]);

  useEffect(() => {
    handleRequestSearch();
  }, [filterChange, urgence, importances]);

  const [loadOrigines, origines] = useLazyQuery<
    GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS_TYPE,
    any
  >(GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const [loadStatuts, statuts] = useLazyQuery<
    GET_STATUTS_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS_TYPE,
    GET_STATUTS_ORDER_BY_NOMBRE_FICHE_AMELIORATIONSVariables
  >(GET_STATUTS_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  //LazyQuery
  const [searchFicheAmeliorations, searchingFicheAmeliorations] = useLazyQuery<
    GET_FICHE_AMELIORATIONS_TYPE,
    GET_FICHE_AMELIORATIONSVariables
  >(GET_FICHE_AMELIORATIONS, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const [loadTotalFilteredFicheAmeliorations, loadingFilteredTotal] = useLazyQuery<
    GET_FICHE_AMELIORATION_AGGREGATE_TYPE,
    GET_FICHE_AMELIORATION_AGGREGATEVariables
  >(GET_FICHE_AMELIORATION_AGGREGATE, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const [loadTotalFicheAmeliorations, loadingTotal] = useLazyQuery<
    GET_FICHE_AMELIORATION_AGGREGATE_TYPE,
    GET_FICHE_AMELIORATION_AGGREGATEVariables
  >(GET_FICHE_AMELIORATION_AGGREGATE, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const [loadTotalHasActionFicheAmeliorations, loadingTotalHasAction] = useLazyQuery<
    GET_FICHE_AMELIORATION_AGGREGATE_TYPE,
    GET_FICHE_AMELIORATION_AGGREGATEVariables
  >(GET_FICHE_AMELIORATION_AGGREGATE, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const [updateFicheAmelioration] = useMutation<
    UPDATE_FICHE_AMELIORATION_TYPE,
    UPDATE_FICHE_AMELIORATIONVariables
  >(UPDATE_FICHE_AMELIORATION, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: "La fiche d'amélioration a été modifiée avec succès !",
        isOpen: true,
        type: 'SUCCESS',
      });

      statuts.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message:
          "Des erreurs se sont survenues lors de la modification de la fiche d'amélioration !",
        isOpen: true,
        type: 'ERROR',
      });
    },
    client: FEDERATION_CLIENT,
  });

  const handleRefetch = () => {
    origines.refetch();
    loadingFilteredTotal.refetch();
    searchingFicheAmeliorations.refetch();
    loadingTotal.refetch();
    loadingTotalHasAction.refetch();
  };

  const [deleteFicheAmelioration] = useMutation<
    DELETE_FICHE_AMELIORATION_TYPE,
    DELETE_FICHE_AMELIORATIONVariables
  >(DELETE_FICHE_AMELIORATION, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La fiche amélioration a été supprimée avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      handleRefetch();
    },
    /*
    TODO : Update cache then remove all refetches
    
   update: (cache, deletionResult) => {
      if (
        deletionResult.data &&
        deletionResult.data.deleteOneDQFicheAmelioration &&
        deletionResult.data.deleteOneDQFicheAmelioration.id
      ) {
        const previousList = cache.readQuery<
          GET_FICHE_AMELIORATIONS_TYPE,
          GET_FICHE_AMELIORATIONSVariables
        >({
          query: GET_FICHE_AMELIORATIONS,
        })?.dQFicheAmeliorations;

        const deletedId = deletionResult.data.deleteOneDQFicheAmelioration.id;

        cache.writeData<GET_FICHE_AMELIORATIONS_TYPE>({
          data: {
            dQFicheAmeliorations: (previousList || []).filter((item) => item?.id !== deletedId),
          },
        });
      }
    },*/
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression de la fiche amélioration',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const onRequestGoBack = (): void => {
    push('/demarche-qualite/outils');
  };

  const onRequestAdd = (): void => {
    push(`/demarche-qualite/fiche-amelioration/new`);
  };

  const handleRequestShowDetail = (fiche: FicheAmelioration): void => {
    push(`/demarche-qualite/fiche-amelioration-detail/${fiche.id}`);
  };

  const handleRequestDelete = (values: FicheAmelioration[]): void => {
    Promise.all(
      values.map(({ id }) => {
        return deleteFicheAmelioration({
          variables: {
            input: {
              id,
            },
          },
        });
      }),
    );
  };

  const handleRequestEdit = (fiche: FicheAmelioration): void => {
    push(`/demarche-qualite/fiche-amelioration/${fiche.id}`);
  };

  const handleRequestStatutChange = (fiche: FicheAmelioration, newId: string): void => {
    updateFicheAmelioration({
      variables: {
        id: fiche.id,
        update: {
          idOrigine: fiche.origine.id,
          idUserAuteur: fiche.auteur.id,
          description: fiche.description,
          idStatut: newId,
          idImportance: fiche.importance.id,
          idOrigineAssocie: fiche.idOrigineAssocie,
          idFonction: fiche.fonction?.id,
          idTache: fiche.tache?.id,
          idCause: fiche.cause.id,
          idType: fiche.type.id,
          dateAmelioration: fiche.dateAmelioration,
          dateEcheance: fiche.dateEcheance,
          idUserParticipants: [
            ...(fiche.participants || []).map(({ id }) => ({
              idUser: id,
              type: 'PARTICIPANT_EN_CHARGE',
            })),
            ...(fiche.participantsAInformer || []).map(({ id }) => ({
              idUser: id,
              type: 'PARTICIPANT_A_INFORMER',
            })),
          ],
        } as any,
      },
    });
  };

  const handleRequestSearch = (): void => {
    if (filterChange) {
      if (filterChange.searchTextChanged) {
        loadOrigines({
          variables: {
            searchText: filterChange.searchText,
          },
        });

        loadStatuts({
          variables: {
            searchText: filterChange.searchText,
          },
        });

        loadTotalHasActionFicheAmeliorations({
          variables: {
            filter: {
              and: [
                {
                  idPharmacie: {
                    eq: idPharmacie,
                  },
                },

                {
                  idAction: {
                    isNot: null,
                  },
                },
                {
                  description: {
                    iLike: `%${filterChange.searchText}%`,
                  },
                },
              ],
            },
          },
        });

        loadTotalFicheAmeliorations({
          variables: {
            filter: {
              and: [
                {
                  idPharmacie: {
                    eq: idPharmacie,
                  },
                },
                {
                  description: {
                    iLike: `%${filterChange.searchText}%`,
                  },
                },
              ],
            },
          },
        });
      }

      let filterList: any[] | undefined = [
        {
          idPharmacie: {
            eq: idPharmacie,
          },
        },
      ];
      const urgenceIds = urgence
        ?.filter(urgence => urgence && urgence.id && urgence.checked)
        .map(urgence => urgence && urgence.id) as any;
      const importancesIds = importances
        ?.filter(importance => importance.checked)
        .map(importance => importance.id);

      if (urgenceIds && urgenceIds.length > 0) {
        filterList = [
          ...(filterList || []),
          {
            idUrgence: {
              in: urgenceIds,
            },
          },
        ];
      }

      if (importancesIds && importancesIds.length > 0) {
        filterList = [
          ...(filterList || []),
          {
            idImportance: {
              in: importancesIds,
            },
          },
        ];
      }

      if (filterChange.originesFiltersSelected.length > 0) {
        filterList = [
          ...(filterList || []),
          {
            idOrigine: {
              in: filterChange.originesFiltersSelected.map(({ id }) => id),
            },
          },
        ];
      }

      if (filterChange.statutsFiltersSelected.length > 0) {
        filterList = [
          ...(filterList || []),
          {
            idStatut: {
              in: filterChange.statutsFiltersSelected.map(({ id }) => id),
            },
          },
        ];
      }

      if (filterChange.searchText) {
        filterList = [
          ...(filterList || []),
          {
            description: {
              iLike: `%${filterChange.searchText}%`,
            },
          },
        ];
      }

      if (filterChange.hasActionFilterSelected !== 'ALL') {
        if (filterChange.hasActionFilterSelected === 'NO') {
          filterList = [
            ...(filterList || []),
            {
              idAction: {
                is: null,
              },
            },
          ];
        } else {
          filterList = [
            ...(filterList || []),
            {
              idAction: {
                isNot: null,
              },
            },
          ];
        }
      }

      const parameters = filterList
        ? {
            variables: {
              filter: {
                and: filterList,
              },
            },
          }
        : {};

      searchFicheAmeliorations({
        variables: {
          ...(parameters.variables || {}),
          paging: {
            offset: filterChange.skip,
            limit: filterChange.take,
          },
        },
      });
      loadTotalFilteredFicheAmeliorations(parameters);
    }
  };

  const fiche: any = searchingFicheAmeliorations.data?.dQFicheAmeliorations.nodes[0];

  const handleStatutInputChange = (event: any) => {
    const newId = event?.target?.value;
    if (newId && fiche) {
      handleRequestStatutChange(fiche, newId);
    }
  };

  const urgenceFilter = (
    <UrgenceFilter withDropdown={false} urgence={urgence} setUrgence={setUrgence} />
  );

  const importanceFilter = (
    <ImportanceFilter
      withDropdown={false}
      importances={importances}
      setImportances={setImportances}
    />
  );

  const statutsInput = (
    <StatutInput
      value={fiche?.statut}
      handleChange={handleStatutInputChange}
      filterType="AMELIORATION"
    />
  );

  const handleRequestOpenFormModal = (fiche: FicheAmelioration | undefined) => {
    if (fiche) {
      setFicheToEdit(fiche);
      setOpenFormModal(true);
    }
  };

  const handleCloseFormModal = (value: boolean) => {
    setOpenFormModal(value);
    setFicheToEdit(undefined);
  };

  const totalFicheAmeliorations =
    (loadingTotal.data?.dQFicheAmeliorationAggregate.length || 0) > 0
      ? loadingTotal.data?.dQFicheAmeliorationAggregate[0].count?.id || 0
      : 0;
  const totalFicheAmeliorationsAvecActions =
    (loadingTotalHasAction.data?.dQFicheAmeliorationAggregate.length || 0) > 0
      ? loadingTotalHasAction.data?.dQFicheAmeliorationAggregate[0].count?.id || 0
      : 0;

  const handleCreateClick = () => {
    setOpenFormModal(true);
  };

  return (
    <>
      <FicheAmeliorationPage
        statuts={{
          ...statuts,
          data: sortStatuts(
            (statuts.data?.dQStatutsOrderByNombreFicheAmeliorations || []).filter(
              ({ type }: any) => type === 'AMELIORATION',
            ) as any,
          ),
        }}
        onRequestStatutChange={handleRequestStatutChange}
        onRequestGoBack={onRequestGoBack}
        onRequestAdd={onRequestAdd}
        onRequestEdit={handleRequestEdit}
        onRequestDelete={handleRequestDelete}
        onRequestShowDetail={handleRequestShowDetail}
        onRequestSearch={setFilterChange}
        data={searchingFicheAmeliorations.data?.dQFicheAmeliorations.nodes as any}
        loading={searchingFicheAmeliorations.loading || loadingFilteredTotal.loading}
        error={searchingFicheAmeliorations.error || loadingFilteredTotal.error}
        origines={{
          ...origines,
          data: origines.data?.origines.nodes || [],
        }}
        actionStats={{
          loading: loadingFilteredTotal.loading || loadingTotalHasAction.loading,
          error: loadingTotalHasAction.error,
          data: [
            {
              id: 'ALL',
              libelle: 'Tous',
              total: totalFicheAmeliorations,
            },
            {
              id: 'YES',
              libelle: 'Oui',
              total: totalFicheAmeliorationsAvecActions,
            },
            {
              id: 'NO',
              libelle: 'No',
              total: totalFicheAmeliorations - totalFicheAmeliorationsAvecActions,
            },
          ],
        }}
        rowsTotal={
          (loadingFilteredTotal.data?.dQFicheAmeliorationAggregate.length || 0) > 0
            ? loadingFilteredTotal.data?.dQFicheAmeliorationAggregate[0].count?.id || 0
            : 0
        }
        onRequestOpenFormModal={handleRequestOpenFormModal}
        urgenceFilter={urgenceFilter}
        importanceFilter={importanceFilter}
        statutsInput={statutsInput}
        filterIcon={<img src={FilterAlt} />}
        onRequestGoReunion={item =>
          item.reunion?.id && push(`/demarche-qualite/compte-rendu/details/${item.reunion.id}`)
        }
      />
      <Hidden mdUp={true} implementation="css">
        <Box className={classes.createButton}>
          <IconButton onClick={handleCreateClick}>
            <Add />
          </IconButton>
        </Box>
      </Hidden>
      <CustomModal
        title={ficheToEdit ? 'Modification amélioration' : 'Nouvelle amélioration'}
        open={openFormModal}
        setOpen={handleCloseFormModal}
        closeIcon={true}
        headerWithBgColor={true}
        withBtnsActions={false}
        fullScreen={true}
        maxWidth="xl"
        noDialogContent={true}
      >
        <FicheIncidentForm
          ameliorationToEdit={ficheToEdit}
          setOpenFormModal={handleCloseFormModal}
          refetch={handleRefetch}
        />
      </CustomModal>
    </>
  );
};

export default FicheAmeliorationManagement;
