import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import { CompteRenduPage, ReunionChange } from '@app/ui-kit';
import { Reunion } from '@app/ui-kit/components/pages/CompteRenduFormPage/types';
import { Box, Hidden, IconButton } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router';
import FilterAlt from '../../../../../assets/icons/todo/filter_alt_white.svg';
import { DELETE_REUNION } from '../../../../../federation/demarche-qualite/reunion/mutation';
import {
  GET_REUNIONS,
  GET_REUNION_AGGREGATE,
} from '../../../../../federation/demarche-qualite/reunion/query';
import {
  DELETE_REUNION as DELETE_REUNION_TYPE,
  DELETE_REUNIONVariables,
} from '../../../../../federation/demarche-qualite/reunion/types/DELETE_REUNION';
import {
  GET_REUNIONS as GET_REUNIONS_TYPE,
  GET_REUNIONSVariables,
} from '../../../../../federation/demarche-qualite/reunion/types/GET_REUNIONS';
import {
  GET_REUNION_AGGREGATE as GET_REUNION_AGGREGATE_TYPE,
  GET_REUNION_AGGREGATEVariables,
} from '../../../../../federation/demarche-qualite/reunion/types/GET_REUNION_AGGREGATE';
import { getPharmacie, getUser } from '../../../../../services/LocalStorage';
import { DQReunionFilter } from '../../../../../types/federation-global-types';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { CustomModal } from '../../../../Common/CustomModal';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { ImportanceInterface } from '../../TodoNew/Common/ImportanceFilter/ImportanceFilterList';
import { UrgenceInterface } from '../../TodoNew/Common/UrgenceFilter/UrgenceFilterList';
import { endTime, startTime } from './../util';
import FicheCompteRenduMobileForm from './FicheCompteRenduMobileForm';
import useStyles from './styles';

const FicheCompteRendu: FC<RouteComponentProps> = ({ history: { push } }) => {
  const client = useApolloClient();

  const classes = useStyles();

  const pharmacie: any = getPharmacie();

  const currentUser = getUser();

  const [urgence, setUrgence] = useState<UrgenceInterface[] | null | undefined>();

  const [etiquette, setEtiquette] = useState<ImportanceInterface[] | null | undefined>();

  const [openFormModal, setOpenFormModal] = useState<boolean>(false);

  const [ficheToEdit, setFicheToEdit] = useState<Reunion>();

  const [currentReunions, setCurrentReunions] = useState<any[]>();

  useEffect(() => {
    const urgenceIds = urgence
      ?.filter(urgence => urgence && urgence.id && urgence.checked)
      .map(urgence => urgence && urgence.id) as any;
    const etiquetteIds = etiquette
      ?.filter(etiquette => etiquette.checked)
      .map(etiquette => etiquette.id);

    let variables = searchingReunions.variables;

    //urgence filters
    if (urgenceIds && urgenceIds.length > 0) {
      const filterList = addFilter(
        variables?.filter?.and,
        { 'typeAssocie.idUrgence': { in: urgenceIds } },
        'idUrgence',
      );
      variables = { ...variables, filter: { and: filterList } };
    } else {
      const cleared = clearFilter(variables?.filter?.and, 'idUrgence');
      variables = { ...variables, filter: { and: cleared } };
    }

    //etiquette filters
    if (etiquetteIds && etiquetteIds.length > 0) {
      const filterList = addFilter(
        variables?.filter?.and,
        { idEtiquette: { in: etiquetteIds } },
        'idEtiquette',
      );
      variables = { ...variables, filter: { and: filterList } };
    } else {
      const cleared = clearFilter(variables?.filter?.and, 'idEtiquette');
      variables = { ...variables, filter: { and: cleared } };
    }

    searchReunions({ variables });
  }, [urgence, etiquette]);

  const clearFilter = (filterList: DQReunionFilter[] | null | undefined, key: string) => {
    if (filterList && filterList.length > 0) {
      return filterList.filter(filterItem => !filterItem[key]);
    }
  };

  const addFilter = (
    filterList: DQReunionFilter[] | null | undefined,
    object: any,
    key: string,
  ) => {
    if (filterList && filterList.length > 0) {
      const cleared = clearFilter(filterList, key);
      return cleared?.concat(object) || [];
    } else {
      return [object];
    }
  };

  const [searchReunions, searchingReunions] = useLazyQuery<
    GET_REUNIONS_TYPE,
    GET_REUNIONSVariables
  >(GET_REUNIONS, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const {
    loading: reunionAgregateLoading,
    error: reunionAgregateError,
    data: reunionAgregateData,
    refetch: reunionAgregateRefetch,
  } = useQuery<GET_REUNION_AGGREGATE_TYPE, GET_REUNION_AGGREGATEVariables>(GET_REUNION_AGGREGATE, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const [deleteCompteRendu] = useMutation<DELETE_REUNION_TYPE, DELETE_REUNIONVariables>(
    DELETE_REUNION,
    {
      onCompleted: () => {
        displaySnackBar(client, {
          message: 'La réunion a été supprimé avec succès',
          type: 'SUCCESS',
          isOpen: true,
        });

        searchingReunions.refetch();
        reunionAgregateRefetch();
      },
      onError: () => {
        displaySnackBar(client, {
          message: 'Des erreurs se sont survenues pendant la suppression de la réunion !',
          type: 'ERROR',
          isOpen: true,
        });
      },
      update: (cache, deletionResult) => {
        if (deletionResult?.data?.deleteOneDQReunion?.id) {
          const previousList = cache.readQuery<GET_REUNIONS_TYPE, GET_REUNIONSVariables>({
            query: GET_REUNIONS,
          })?.dQReunions;

          const deletedId = deletionResult.data.deleteOneDQReunion.id;

          cache.writeData<GET_REUNIONS_TYPE>({
            data: {
              dQReunions: {
                ...(previousList || {}),
                nodes: (previousList?.nodes || []).filter(element => element.id !== deletedId),
              } as any,
            },
          });
        }
      },
      client: FEDERATION_CLIENT,
    },
  );

  useEffect(() => {
    searchReunions();
  }, []);

  useEffect(() => {
    if (
      !searchingReunions.loading &&
      !searchingReunions.error &&
      searchingReunions.data?.dQReunions
    ) {
      setCurrentReunions(searchingReunions.data?.dQReunions.nodes);
    }
  }, [searchingReunions.loading, searchingReunions.error, searchingReunions.data]);

  const handleGoBack = (): void => {
    push(`/demarche-qualite/outils`);
  };

  const handleAdd = (): void => {
    push('/demarche-qualite/compte-rendu/new');
  };

  const handleEdit = (fiche: any): void => {
    push(`/demarche-qualite/compte-rendu/${fiche.id}`);
  };

  const handleShowDetails = (fiche: any): void => {
    push(`/demarche-qualite/compte-rendu/details/${fiche.id}`);
  };

  const handleDelete = (fiches: any[]): void => {
    deleteCompteRendu({
      variables: {
        input: {
          id: fiches[0].id,
        },
      },
    });
  };

  const handleSearch = (change: ReunionChange): void => {
    let filterList: any[] | undefined = undefined;

    const urgenceIds = urgence
      ?.filter(urgence => urgence && urgence.id && urgence.checked)
      .map(urgence => urgence && urgence.id) as any;
    const etiquetteIds = etiquette
      ?.filter(etiquette => etiquette.checked)
      .map(etiquette => etiquette.id);

    if (urgenceIds && urgenceIds.length > 0) {
      filterList = [
        ...(filterList || []),
        {
          idUrgence: {
            in: urgenceIds,
          },
        },
      ];
    }

    if (etiquetteIds && etiquetteIds.length > 0) {
      filterList = [
        ...(filterList || []),
        {
          idEtiquette: {
            in: etiquetteIds,
          },
        },
      ];
    }

    if (change.searchText) {
      filterList = [
        ...(filterList || []),
        {
          description: {
            iLike: `%${change.searchText}%`,
          },
        },
      ];
    }
    if (change.dateCreationFiltersSelected) {
      filterList = [
        ...(filterList || []),
        {
          createdAt: {
            between: {
              lower: startTime(change.dateCreationFiltersSelected),
              upper: endTime(change.dateCreationFiltersSelected),
            },
          },
        },
      ];
    }

    const parameters = filterList
      ? {
          variables: {
            filter: {
              and: filterList,
            },
          },
        }
      : {};

    searchReunions(parameters);
  };

  const handleRequestOpenFormModal = (fiche: Reunion | undefined) => {
    if (fiche) {
      setFicheToEdit(fiche);
      setOpenFormModal(true);
    }
  };

  const handleCloseFormModal = (value: boolean) => {
    setOpenFormModal(value);
    setFicheToEdit(undefined);
  };

  const handleCreateClick = () => {
    setOpenFormModal(true);
  };

  return (
    <>
      <CompteRenduPage
        data={searchingReunions.data?.dQReunions.nodes as any}
        loading={searchingReunions.loading}
        error={searchingReunions.error}
        onRequestAdd={handleAdd}
        onRequestDelete={handleDelete}
        onRequestEdit={handleEdit}
        onRequestGoBack={handleGoBack}
        onRequestSearch={handleSearch}
        onRequestShowDetail={handleShowDetails}
        rowsTotal={
          (reunionAgregateData?.dQReunionAggregate.length || 0) > 0
            ? (reunionAgregateData?.dQReunionAggregate[0].count?.id as any)
            : 0
        }
        onRequestOpenFormModal={handleRequestOpenFormModal}
        filterIcon={<img src={FilterAlt} />}
      />
      <Hidden mdUp={true} implementation="css">
        <Box className={classes.createButton}>
          <IconButton onClick={handleCreateClick}>
            <Add />
          </IconButton>
        </Box>
      </Hidden>
      <CustomModal
        title={ficheToEdit ? 'Modification réunion' : 'Nouvelle réunion'}
        open={openFormModal}
        setOpen={handleCloseFormModal}
        closeIcon={true}
        headerWithBgColor={true}
        withBtnsActions={false}
        fullScreen={true}
        maxWidth="xl"
        noDialogContent={true}
      >
        <FicheCompteRenduMobileForm
          reunionToEdit={ficheToEdit}
          setOpenFormModal={handleCloseFormModal}
        />
      </CustomModal>
    </>
  );
};

export default FicheCompteRendu;
