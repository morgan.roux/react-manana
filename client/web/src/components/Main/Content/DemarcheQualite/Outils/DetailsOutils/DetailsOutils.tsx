import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import { Backdrop } from '@app/ui-kit';
import { Add } from '@material-ui/icons';
import React, { ChangeEvent, FC, useState, useEffect } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { CREATE_OUTIL } from '../../../../../../federation/demarche-qualite/outil/mutation';
import { GET_OUTILS } from '../../../../../../federation/demarche-qualite/outil/query';
import {
  CREATE_OUTIL as CREATE_OUTIL_TYPE,
  CREATE_OUTILVariables,
} from '../../../../../../federation/demarche-qualite/outil/types/CREATE_OUTIL';
import {
  GET_OUTILS as GET_OUTILS_TYPE,
  GET_OUTILSVariables,
} from '../../../../../../federation/demarche-qualite/outil/types/GET_OUTILS';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import { LeftOperationHeaderButtonInterface } from '../../../../../Common/LeftOperationHeaderButton/LeftOperationHeaderButton';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { ThemePage } from '../ThemePage';
import AddOutil from '../ThemePage/AddOutil/AddOutil';
import { noContentValuesByTypologie, terminaison } from './fonction';
import { formatFilename } from '../../../../../Common/Dropzone/Dropzone';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../../graphql/S3/mutation';
import { uploadToS3 } from '../../../../../../services/S3';

interface DetailsOutils {
  match: {
    params: {
      outilType: string | undefined;
    };
  };
}

const DetailsOutils: FC<DetailsOutils & RouteComponentProps> = ({ match }) => {
  const matches = match && match.params && match.params.outilType ? match.params.outilType : '';
  const [open, setOpen] = useState<boolean>(false);
  const [startFileUpload, setStartFileUpload] = useState<boolean>(false);
  const [selectedFile, setSelectedFile] = useState<File[]>([]);
  const [numeroOrdre, setNumeroOrdre] = useState<string>('');
  const [intitule, setIntitule] = useState<string>('');
  const [mutationLoading, setMutationLoading] = useState<boolean>(false);
  //const directory : string = `outil/${matches}`;
  const directory: string = ``;

  const client = useApolloClient();

  const handleNumeroOrdreChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setNumeroOrdre((event.target as HTMLInputElement).value);
  };

  const handleIntituleChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setIntitule((event.target as HTMLInputElement).value);
  };

  const reInit = (): void => {
    setNumeroOrdre('');
    setIntitule('');
    setSelectedFile([]);
  };

  const { loading, data, error } = useQuery<GET_OUTILS_TYPE, GET_OUTILSVariables>(GET_OUTILS, {
    variables: {
      typologie: matches,
    },
    client: FEDERATION_CLIENT,
  });

  const showError = (
    message: string = "Des erreurs se sont survenues pendant l'ajout de l'outil",
  ): void => {
    displaySnackBar(client, {
      type: 'ERROR',
      message: message,
      isOpen: true,
    });
  };

  const [addOutil] = useMutation<CREATE_OUTIL_TYPE, CREATE_OUTILVariables>(CREATE_OUTIL, {
    onCompleted: () => {
      setMutationLoading(false);
      setOpen(false);
      reInit();
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: "L'outil a été ajouté avec succès !",
        isOpen: true,
      });
    },
    onError: () => {
      setMutationLoading(false);
      displaySnackBar(client, {
        type: 'ERROR',
        message: "Des erreurs se sont survenues pendant l'ajout de l'outil",
        isOpen: true,
      });
    },
    update: (cache, creationResult) => {
      if (creationResult.data && creationResult.data.createDQOutil) {
        const previousList = cache.readQuery<GET_OUTILS_TYPE, GET_OUTILSVariables>({
          query: GET_OUTILS,
          variables: {
            typologie: matches,
          },
        })?.dqOutils;

        cache.writeQuery<GET_OUTILS_TYPE>({
          query: GET_OUTILS,
          variables: {
            typologie: matches,
          },
          data: {
            dqOutils: [...(previousList || []), creationResult.data.createDQOutil],
          },
        });
      }
    },
    client: FEDERATION_CLIENT,
  });

  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
    onCompleted: (data) => {
      selectedFile.map((file) => {
        if (data && data.createPutPresignedUrls) {
          const outilFichier = data.createPutPresignedUrls[0];
          if (outilFichier) {
            uploadToS3(file, outilFichier.presignedUrl)
              .then((result) => {
                if (
                  result &&
                  result.status === 200 &&
                  result.statusText === 'OK' &&
                  setStartFileUpload
                ) {
                  setStartFileUpload(false);
                  setMutationLoading(true);
                  addOutil({
                    variables: {
                      typologie: matches,
                      outilInput: {
                        ordre: parseInt(numeroOrdre),
                        libelle: intitule,
                        fichier: {
                          chemin: outilFichier.filePath,
                          nomOriginal: file.name,
                          type: file.type,
                        },
                      },
                    },
                  });
                } else {
                  showError();
                }
              })
              .catch(() => {
                showError();
              });
          } else {
            showError();
          }
        } else {
          showError();
        }
      });
    },
  });

  useEffect(() => {
    if (startFileUpload) {
      const filePathTab: string[] = [];
      selectedFile.map((file: File) => {
        filePathTab.push(formatFilename(file, `${directory}`));
      });

      doCreatePutPresignedUrl({
        variables: {
          filePaths: filePathTab,
        },
      });
    }
  }, [startFileUpload]);

  const openModal = (): void => {
    setOpen(!open);
  };

  const addButton: LeftOperationHeaderButtonInterface = {
    text: `Créer un${terminaison(matches)} ${matches}`,
    color: 'pink',
    icon: <Add />,
    onClick: () => {
      reInit();
      openModal();
    },
  };

  const listItemFields = {
    url: `/demarche-qualite/outils/${matches}`,
    title: 'typologie',
    dateCreation: true,
    dateModification: true,
  };

  const submit = (): void => {
    if (selectedFile.length > 0 && numeroOrdre !== '' && intitule !== '') {
      setStartFileUpload(true);
    }
  };

  if (loading) return <Backdrop value="Récuperation de données" open={loading} />;

  return (
    <>
      {!loading && !error && data && data.dqOutils && (
        <>
          <ThemePage
            typologie={matches}
            noContentValues={noContentValuesByTypologie(matches)}
            addButton={addButton}
            outilsData={(data && data.dqOutils) || []}
            listItemFields={listItemFields}
          />
          <AddOutil
            title={`Ajout ${matches === 'affiche' ? "d'" : 'de '}${matches}`}
            open={open}
            numeroOrdre={numeroOrdre}
            intitule={intitule}
            selectedFile={selectedFile}
            loading={startFileUpload || mutationLoading}
            setNumeroOrdre={handleNumeroOrdreChange}
            setIntitule={handleIntituleChange}
            setSelectedFile={setSelectedFile}
            setOpen={setOpen}
            onSubmit={submit}
          />
        </>
      )}
    </>
  );
};

export default withRouter(DetailsOutils);
