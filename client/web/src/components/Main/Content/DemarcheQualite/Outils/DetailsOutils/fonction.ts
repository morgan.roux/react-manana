import { INoContentValues } from "../ThemePage/ThemePage";

    export const terminaison = ( typologie : string ) : string => {
        let terminaison : string = '';
        if( typologie === 'affiche' || typologie === 'procedure' ) terminaison = 'e';

        return terminaison;
    };

    export const noContentValuesByTypologie = ( typologie : string ) : INoContentValues => {
        const end : string = terminaison(typologie);

        return {
            list : {
                title : `Aucun${end} ${typologie} dans la liste.`,
                subtitle : `Créez-en un${end} en cliquant sur le bouton d'en haut`
            },

            content : {
                title : `Aperçu détaillé des ${typologie}s`,
                subtitle : `Choisissez un${end} ${typologie} dans la liste de gauche pour voir son contenu dans cette partie`
            }
        }
    };