import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { FicheAmelioration } from '@app/ui-kit'
import {
    CREATE_FICHE_AMELIORATION,
    UPDATE_FICHE_AMELIORATION,
} from '../../../../../federation/demarche-qualite/fiche-amelioration/mutation';
import {
    GET_FICHE_AMELIORATIONS,
} from '../../../../../federation/demarche-qualite/fiche-amelioration/query';
import { CREATE_FICHE_AMELIORATION as CREATE_FICHE_AMELIORATION_TYPE } from '../../../../../federation/demarche-qualite/fiche-amelioration/types/CREATE_FICHE_AMELIORATION';
import { GET_FICHE_AMELIORATIONS as GET_FICHE_AMELIORATIONS_TYPE } from '../../../../../federation/demarche-qualite/fiche-amelioration/types/GET_FICHE_AMELIORATIONS';

import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import {
    UPDATE_FICHE_AMELIORATIONVariables,
    UPDATE_FICHE_AMELIORATION as UPDATE_FICHE_AMELIORATION_TYPE,
} from '../../../../../federation/demarche-qualite/fiche-amelioration/types/UPDATE_FICHE_AMELIORATION';




interface UseFicheAmeliorationOptions {
    onCreated?: (fiche: FicheAmelioration) => void
    onCreateError?: () => void
    onUpdated?: (fiche: FicheAmelioration) => void
    onUpdateError?: () => void
}

const useFicheAmelioration = (options?: UseFicheAmeliorationOptions) => {
    const client = useApolloClient();

    const [createFicheAmelioration, creationFicheAmelioration] = useMutation<
        CREATE_FICHE_AMELIORATION_TYPE,
        any
    >(CREATE_FICHE_AMELIORATION, {
        onCompleted: (result) => {
            displaySnackBar(client, {
                message: "La fiche d'amélioration a été crée avec succès",
                type: 'SUCCESS',
                isOpen: true,
            });

            if (options?.onCreated) {
                options.onCreated(result.createOneDQFicheAmelioration as any)
            }
        },
        onError: () => {
            displaySnackBar(client, {
                message: `Des erreurs se sont survenues pendant l'ajout de la fiche d'amélioration'`,
                isOpen: true,
                type: 'ERROR',
            });

            if (options?.onCreateError) {
                options.onCreateError()
            }
        },
        update: (cache, dataResult) => {
            if (dataResult.data && dataResult.data.createOneDQFicheAmelioration) {
                const previousList = cache.readQuery<GET_FICHE_AMELIORATIONS_TYPE, any>({
                    query: GET_FICHE_AMELIORATIONS,
                })?.dQFicheAmeliorations;

                cache.writeData<GET_FICHE_AMELIORATIONS_TYPE>({
                    data: {
                        dQFicheAmeliorations: { 
                            ...(previousList || {}),
                            nodes: [
                            ...(previousList?.nodes || []),
                            dataResult.data.createOneDQFicheAmelioration,
                        ]} as any,
                    },
                });
            }
        },
        client: FEDERATION_CLIENT,
    });


    const [updateFicheAmelioration, updatingFicheAmelioration] = useMutation<
        UPDATE_FICHE_AMELIORATION_TYPE,
        UPDATE_FICHE_AMELIORATIONVariables
    >(UPDATE_FICHE_AMELIORATION, {
        onCompleted: (result) => {
            displaySnackBar(client, {
                message: `La fiche d'amélioration a été modifiée avec succès !`,
                isOpen: true,
                type: 'SUCCESS',
            });

            if (options?.onUpdated) {
                options.onUpdated(result.updateOneDQFicheAmelioration as any)
            }


        },
        onError: () => {
            displaySnackBar(client, {
                message: `Des erreurs se sont survenues pendant la modification de la fiche d'amélioration`,
                isOpen: true,
                type: 'ERROR',
            });

            if (options?.onUpdateError) {
                options.onUpdateError()
            }
        },
        client: FEDERATION_CLIENT,
    });


    return {
        createFicheAmelioration,
        creationFicheAmelioration,
        updateFicheAmelioration,
        updatingFicheAmelioration
    }
}

export default useFicheAmelioration;
