import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { FicheAmelioration, FicheIncident, ActionOperationnelle } from '@app/ui-kit';
import { ReunionAction } from '@app/ui-kit/components/pages/CompteRenduFormPage/types';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';

import { UPDATE_FICHE_AMELIORATION } from '../../../../../federation/demarche-qualite/fiche-amelioration/mutation';
import {
  UPDATE_FICHE_AMELIORATION as UPDATE_FICHE_AMELIORATION_TYPE,
  UPDATE_FICHE_AMELIORATIONVariables,
} from '../../../../../federation/demarche-qualite/fiche-amelioration/types/UPDATE_FICHE_AMELIORATION';
import { UPDATE_ACTION_OPERATIONNELLE } from '../../../../../federation/demarche-qualite/action-operationnelle/mutation';
import {
  UPDATE_ACTION_OPERATIONNELLE as UPDATE_ACTION_OPERATIONNELLE_TYPE,
  UPDATE_ACTION_OPERATIONNELLEVariables,
} from '../../../../../federation/demarche-qualite/action-operationnelle/types/UPDATE_ACTION_OPERATIONNELLE';
import { UPDATE_FICHE_INCIDENT } from '../../../../../federation/demarche-qualite/fiche-incident/mutation';
import {
  UPDATE_FICHE_INCIDENT as UPDATE_FICHE_INCIDENT_TYPE,
  UPDATE_FICHE_INCIDENTVariables,
} from '../../../../../federation/demarche-qualite/fiche-incident/types/UPDATE_FICHE_INCIDENT';

const useReunionAction = () => {
  const client = useApolloClient();

  const [updateFicheAmelioration] = useMutation<
    UPDATE_FICHE_AMELIORATION_TYPE,
    UPDATE_FICHE_AMELIORATIONVariables
  >(UPDATE_FICHE_AMELIORATION, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La fiche amélioration a été modifiée avec succès',
        type: 'SUCCESS',
        isOpen: true,
      });
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la modification de la fiche amélioration',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [updateActionOperationnelle] = useMutation<
    UPDATE_ACTION_OPERATIONNELLE_TYPE,
    UPDATE_ACTION_OPERATIONNELLEVariables
  >(UPDATE_ACTION_OPERATIONNELLE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: "L'action opération a été modifiée avec succès !",
        type: 'SUCCESS',
        isOpen: true,
      });
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs se sont survenues pendant la modification de l'action opérationnelle",
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [updateFicheIncident] = useMutation<
    UPDATE_FICHE_INCIDENT_TYPE,
    UPDATE_FICHE_INCIDENTVariables
  >(UPDATE_FICHE_INCIDENT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: `La fiche incident a été modifiée avec succès !`,
        isOpen: true,
        type: 'SUCCESS',
      });
    },
    onError: () => {
      displaySnackBar(client, {
        message: `Des erreurs se sont survenues pendant la modification de la fiche d'incident`,
        isOpen: true,
        type: 'ERROR',
      });
    },
    client: FEDERATION_CLIENT,
  });

  const updateReunionAction = (reunionAction: ReunionAction, newValues: any) => {
    if (reunionAction.type === 'AMELIORATION') {
      const fiche: FicheAmelioration = reunionAction.typeAssocie as FicheAmelioration;
      updateFicheAmelioration({
        variables: {
          id: fiche.id,
          update: {
            description: fiche.description,
            idOrigine: fiche.origine.id,
            idUserAuteur: fiche.auteur.id,
            fichiers: fiche.fichiers,
            idImportance: fiche.importance.id,
            idFonction: fiche.fonction?.id,
            idTache: fiche.tache?.id,
            idStatut: fiche.statut.id,
            idUserParticipants: [
              ... (fiche.participants || []).map(({ id }) => ({ idUser: id, type: 'PARTICIPANT_EN_CHARGE' })),
              ... (fiche.participantsAInformer || []).map(({ id }) => ({ idUser: id, type: 'PARTICIPANT_A_INFORMER' }))
            ],
            idCause: fiche.cause?.id,
            idOrigineAssocie: fiche.idOrigineAssocie,
            idType: fiche.type?.id,
            dateAmelioration: fiche.dateAmelioration,
            dateEcheance: fiche.dateEcheance,
            ...newValues,
          },
        },
      });
    } else if (reunionAction.type === 'INCIDENT') {
      const fiche: FicheIncident = reunionAction.typeAssocie as FicheIncident;
      updateFicheIncident({
        variables: {
          id: fiche.id,
          update: {
            description: fiche.description,
            idOrigine: fiche.origine.id,
            idUserDeclarant: fiche.declarant.id,
            fichiers: fiche.fichiers,
            idImportance: fiche.importance.id,
            idFonction: fiche.fonction?.id,
            idTache: fiche.tache?.id,
            idStatut: fiche.statut.id,
            idUserParticipants: [
              ... (fiche.participants || []).map(({ id }) => ({ idUser: id, type: 'PARTICIPANT_EN_CHARGE' })),
              ... (fiche.participantsAInformer || []).map(({ id }) => ({ idUser: id, type: 'PARTICIPANT_A_INFORMER' }))
            ],
            idCause: fiche.cause?.id,
            idOrigineAssocie: fiche.idOrigineAssocie,
            idType: fiche.type?.id,
            dateIncident: fiche.dateIncident,
            dateEcheance: fiche.dateEcheance,
            ...newValues,
          },
        },
      });
    } else if (reunionAction.type === 'OPERATION') {
      const fiche: ActionOperationnelle = reunionAction.typeAssocie as ActionOperationnelle;

      updateActionOperationnelle({
        variables: {
          id: fiche.id || '',
          update: {
            description: fiche.description,
            idOrigine: fiche.origine.id,
            idUserAuteur: fiche.auteur?.id || '',
            idUserSuivi: fiche.suiviPar?.id || '',
            fichiers: fiche.fichiers,
            idImportance: fiche.importance.id,
            idFonction: fiche.fonction?.id,
            idTache: fiche.tache?.id,
            idStatut: fiche.statut.id,
            idUserParticipants: [
              ... (fiche.participants || []).map(({ id }) => ({ idUser: id, type: 'PARTICIPANT_EN_CHARGE' })),
              ... (fiche.participantsAInformer || []).map(({ id }) => ({ idUser: id, type: 'PARTICIPANT_A_INFORMER' }))
            ],
            idCause: fiche.cause?.id,
            idOrigineAssocie: fiche.idOrigineAssocie,
            idType: fiche.type?.id,
            dateAction: fiche.dateAction,
            dateEcheance: fiche.dateEcheance,
            ...newValues,
          },
        },
      });
    }
  };

  return { updateReunionAction };
};

export default useReunionAction;
