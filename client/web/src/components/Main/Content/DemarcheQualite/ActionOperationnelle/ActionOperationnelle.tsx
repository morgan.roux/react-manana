import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import {
  ActionOperationnelle,
  ActionOperationnelleChange,
  ActionOperationnellePage,
} from '@app/ui-kit';
import { Box, Hidden, IconButton } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router';
import FilterAlt from '../../../../../assets/icons/todo/filter_alt_white.svg';
import {
  DELETE_ACTION_OPERATIONNELLE,
  UPDATE_ACTION_OPERATIONNELLE,
} from '../../../../../federation/demarche-qualite/action-operationnelle/mutation';
import { GET_ACTIONS_OPERATIONNELLES } from '../../../../../federation/demarche-qualite/action-operationnelle/query';
import {
  DELETE_ACTION_OPERATIONNELLE as DELETE_ACTION_OPERATIONNELLE_TYPE,
  DELETE_ACTION_OPERATIONNELLEVariables,
} from '../../../../../federation/demarche-qualite/action-operationnelle/types/DELETE_ACTION_OPERATIONNELLE';
import { GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS as GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS_TYPE } from '../../../../../federation/demarche-qualite/origine/types/GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS';
import {
  GET_ACTIONS_OPERATIONNELLES as GET_ACTIONS_OPERATIONNELLES_TYPE,
  GET_ACTIONS_OPERATIONNELLESVariables,
} from '../../../../../federation/demarche-qualite/action-operationnelle/types/GET_ACTIONS_OPERATIONNELLES';
import {
  UPDATE_ACTION_OPERATIONNELLE as UPDATE_ACTION_OPERATIONNELLE_TYPE,
  UPDATE_ACTION_OPERATIONNELLEVariables,
} from '../../../../../federation/demarche-qualite/action-operationnelle/types/UPDATE_ACTION_OPERATIONNELLE';
import { GET_STATUTS_ORDER_BY_NOMBRE_ACTION_OPERATIONNELLES } from '../../../../../federation/demarche-qualite/statut/query';
import {
  GET_STATUTS_ORDER_BY_NOMBRE_ACTION_OPERATIONNELLES as GET_STATUTS_ORDER_BY_NOMBRE_ACTION_OPERATIONNELLES_TYPE,
  GET_STATUTS_ORDER_BY_NOMBRE_ACTION_OPERATIONNELLESVariables,
} from '../../../../../federation/demarche-qualite/statut/types/GET_STATUTS_ORDER_BY_NOMBRE_ACTION_OPERATIONNELLES';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { CustomModal } from '../../../../Common/CustomModal';
import StatutInput from '../../../../Common/StatutInput';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import ImportanceFilter from '../../TodoNew/Common/ImportanceFilter/ImportanceFilter';
import { ImportanceInterface } from '../../TodoNew/Common/ImportanceFilter/ImportanceFilterList';
import UrgenceFilter, { UrgenceInterface } from '../../TodoNew/Common/UrgenceFilter/UrgenceFilter';
import FicheIncidentForm from '../FicheIncident/FicheIncidentForm/FicheIncidentForm';
import { endTime, sortStatuts, startTime } from '../util';
import useStyles from './styles';
import { useImportance, useUrgence } from '../../../../hooks';
import { GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS } from '../../../../../federation/demarche-qualite/origine/query';
import { getPharmacie } from '../../../../../services/LocalStorage';

const ActionOperationnelleComponent: FC<RouteComponentProps> = ({ history: { push } }) => {
  const client = useApolloClient();
  const idPharmacie = getPharmacie().id

  const loadingImportances = useImportance();
  const loadingUrgences = useUrgence();

  const classes = useStyles();

  const [filterChange, setFilterChange] = useState<ActionOperationnelleChange>();
  const [urgence, setUrgence] = useState<UrgenceInterface[] | null | undefined>();
  const [importances, setImportances] = useState<ImportanceInterface[] | null | undefined>([]);

  const [openFormModal, setOpenFormModal] = useState<boolean>(false);

  const [ficheToEdit, setFicheToEdit] = useState<ActionOperationnelle>();

  const importancesData = loadingImportances.data?.importances.nodes;
  const urgencesData = loadingUrgences.data?.urgences.nodes;

  useEffect(() => {
    if (!loadingUrgences.loading && urgencesData) {
      setUrgence((urgencesData || []) as any);
    }
  }, [loadingUrgences.loading, urgencesData]);

  useEffect(() => {
    if (!loadingImportances.loading && importancesData) {
      setImportances((importancesData || []) as any);
    }
  }, [loadingImportances.loading, importancesData]);

  useEffect(() => {
    handleSearch();
  }, [filterChange, urgence, importances]);

  const [searchActionsOperationnelles, searchingActionsOperationnelles] = useLazyQuery<
    GET_ACTIONS_OPERATIONNELLES_TYPE,
    GET_ACTIONS_OPERATIONNELLESVariables
  >(GET_ACTIONS_OPERATIONNELLES, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const [loadOrigines, origines] = useLazyQuery<
    GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS_TYPE,
    any
  >(GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const [loadStatuts, statuts] = useLazyQuery<
    GET_STATUTS_ORDER_BY_NOMBRE_ACTION_OPERATIONNELLES_TYPE,
    GET_STATUTS_ORDER_BY_NOMBRE_ACTION_OPERATIONNELLESVariables
  >(GET_STATUTS_ORDER_BY_NOMBRE_ACTION_OPERATIONNELLES, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const [updateActionOperationnelle] = useMutation<
    UPDATE_ACTION_OPERATIONNELLE_TYPE,
    UPDATE_ACTION_OPERATIONNELLEVariables
  >(UPDATE_ACTION_OPERATIONNELLE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: "L'action opération a été modifiée avec succès !",
        type: 'SUCCESS',
        isOpen: true,
      });

      statuts.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs se sont survenues pendant la modification de l'action opérationnelle",
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const handleRefetch = () => {
    searchingActionsOperationnelles.refetch();
  };

  const [deleteActionOperationnelle] = useMutation<
    DELETE_ACTION_OPERATIONNELLE_TYPE,
    DELETE_ACTION_OPERATIONNELLEVariables
  >(DELETE_ACTION_OPERATIONNELLE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: "L'action opérationnelle a été supprimée avec succès !",
        isOpen: true,
        type: 'SUCCESS',
      });

      handleSearch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs se sont survenues pendant la suppression de l'action opérationnelle",
        isOpen: true,
        type: 'ERROR',
      });
    } /*,
    update: (cache, deletionResult) => {
      if (deletionResult?.data?.deleteOneDQActionOperationnelle?.id) {
        const previousList = cache.readQuery<
          GET_ACTIONS_OPERATIONNELLES_TYPE,
          GET_ACTIONS_OPERATIONNELLESVariables
        >({
          query: GET_ACTIONS_OPERATIONNELLES,
        })?.dQActionOperationnelles;

        const deletedId = deletionResult?.data?.deleteOneDQActionOperationnelle?.id;

        cache.writeData<GET_ACTIONS_OPERATIONNELLES_TYPE>({
          data: {
            dQActionOperationnelles: (previousList || []).filter(
              element => element?.id !== deletedId,
            ),
          },
        });
      }
    }*/,
    client: FEDERATION_CLIENT,
  });

  const handleGoBack = (): void => {
    push('/demarche-qualite/outils');
  };

  const handleAdd = (): void => {
    push('/demarche-qualite/action-operationnelle/new');
  };

  const handleShowDetails = (action: ActionOperationnelle): void => {
    push(`/demarche-qualite/action-operationnelle-detail/${action.id}`);
  };

  const handleEdit = (action: ActionOperationnelle): void => {
    push(`/demarche-qualite/action-operationnelle/${action.id}`);
  };

  const handleRequestStatutChange = (currentAction: ActionOperationnelle, newId: string): void => {
    updateActionOperationnelle({
      variables: {
        id: currentAction.id as any,
        update: {
          idOrigine: currentAction.origine.id,
          description: currentAction.description,
          idStatut: newId,
          idUserAuteur: currentAction.auteur?.id as any,
          idUserSuivi: currentAction.suiviPar?.id as any,
          idImportance: currentAction.importance.id,
          idTache: currentAction.tache?.id,
          idFonction: currentAction.fonction?.id,
          idOrigineAssocie: currentAction.idOrigineAssocie,
          dateAction: currentAction.dateAction,
          dateEcheance: currentAction.dateEcheance,
          idType: currentAction.type.id,
          idCause: currentAction.cause.id,
          idUserParticipants: [
            ... (currentAction.participants || []).map(({ id }) => ({ idUser: id, type: 'PARTICIPANT_EN_CHARGE' })),
            ... (currentAction.participantsAInformer || []).map(({ id }) => ({ idUser: id, type: 'PARTICIPANT_A_INFORMER' }))
          ],
        } as any,
      },
    });
  };

  const handleDelete = (actions: ActionOperationnelle[]): void => {
    deleteActionOperationnelle({
      variables: {
        input: {
          id: actions[0].id as any,
        },
      },
    });
  };

  const handleSearch = (): void => {
    if (filterChange) {
      let filterList: any[] | undefined = [
        {
          idPharmacie: {
            eq: idPharmacie
          }
        }
      ];

      const urgenceIds = urgence
        ?.filter(urgence => urgence && urgence.id && urgence.checked)
        .map(urgence => urgence && urgence.id) as any;
      const importancesIds = importances
        ?.filter(importance => importance.checked)
        .map(importance => importance.id);

      if (urgenceIds && urgenceIds.length > 0) {
        filterList = [
          ...(filterList || []),
          {
            idUrgence: {
              in: urgenceIds,
            },
          },
        ];
      }

      if (importancesIds && importancesIds.length > 0) {
        filterList = [
          ...(filterList || []),
          {
            idImportance: {
              in: importancesIds,
            },
          },
        ];
      }

      if (filterChange.searchTextChanged) {
        loadOrigines({
          variables: {
            searchText: filterChange.searchText,
          },
        });
        loadStatuts({
          variables: {
            searchText: filterChange.searchText,
          },
        });
      }

      if (filterChange.searchText) {
        filterList = [
          ...(filterList || []),
          {
            description: {
              iLike: `%${filterChange.searchText}%`,
            },
          },
        ];
      }
      if (filterChange.dateCreationFiltersSelected) {
        filterList = [
          ...(filterList || []),
          {
            createdAt: {
              between: {
                lower: startTime(filterChange.dateCreationFiltersSelected),
                upper: endTime(filterChange.dateCreationFiltersSelected),
              },
            },
          },
        ];
      }

      if (filterChange.statutsFiltersSelected.length > 0) {
        filterList = [
          ...(filterList || []),
          {
            idStatut: {
              in: filterChange.statutsFiltersSelected.map(({ id }) => id),
            },
          },
        ];
      }

      const parameters = filterList
        ? {
          variables: {
            filter: {
              and: filterList,
            },
          },
        }
        : {};

      searchActionsOperationnelles(parameters);
    }
  };

  React.useEffect(() => {
    searchActionsOperationnelles();
  }, []);

  const fiche: any = searchingActionsOperationnelles.data?.dQActionOperationnelles.nodes[0];

  const handleStatutInputChange = (event: any) => {
    const newId = event?.target?.value;
    if (newId && fiche) {
      handleRequestStatutChange(fiche, newId);
    }
  };

  const urgenceFilter = (
    <UrgenceFilter withDropdown={false} urgence={urgence} setUrgence={setUrgence} />
  );

  const importanceFilter = (
    <ImportanceFilter
      withDropdown={false}
      importances={importances}
      setImportances={setImportances}
    />
  );

  const statutsInput = (
    <StatutInput
      value={fiche?.statut}
      handleChange={handleStatutInputChange}
      filterType="OPERATION"
    />
  );

  const handleRequestOpenFormModal = (fiche: ActionOperationnelle | undefined) => {
    if (fiche) {
      setFicheToEdit(fiche);
      setOpenFormModal(true);
    }
  };

  const handleCloseFormModal = (value: boolean) => {
    setOpenFormModal(value);
    setFicheToEdit(undefined);
  };

  const handleCreateClick = () => {
    setOpenFormModal(true);
  };

  return (
    <>
      <ActionOperationnellePage
        origines={{
          ...origines,
          data: origines.data?.origines.nodes || [],
        }}
        statuts={{
          ...statuts,
          data: sortStatuts((statuts.data?.dQStatutsOrderByNombreActionOperationnelles || []).filter(
            ({ type }: any) => type === 'OPERATION',
          ) as any),
        }}
        loading={searchingActionsOperationnelles.loading}
        error={searchingActionsOperationnelles.error}
        data={searchingActionsOperationnelles.data?.dQActionOperationnelles.nodes as any}
        onRequestGoBack={handleGoBack}
        onRequestAdd={handleAdd}
        onRequestShowDetail={handleShowDetails}
        onRequestEdit={handleEdit}
        onRequestStatutChange={handleRequestStatutChange}
        onRequestDelete={handleDelete}
        onRequestSearch={setFilterChange}
        rowsTotal={searchingActionsOperationnelles.data?.dQActionOperationnelles?.nodes.length || 0}
        onRequestOpenFormModal={handleRequestOpenFormModal}
        urgenceFilter={urgenceFilter}
        importanceFilter={importanceFilter}
        statutsInput={statutsInput}
        filterIcon={<img src={FilterAlt} />}
        onRequestGoReunion={item => item.reunion?.id && push(`/demarche-qualite/compte-rendu/details/${item.reunion.id}`)}
      />
      <Hidden mdUp={true} implementation="css">
        <Box className={classes.createButton}>
          <IconButton onClick={handleCreateClick}>
            <Add />
          </IconButton>
        </Box>
      </Hidden>
      <CustomModal
        title={
          ficheToEdit ? 'Modification action opérationnelle' : 'Nouvelle action opérationnelle'
        }
        open={openFormModal}
        setOpen={handleCloseFormModal}
        closeIcon={true}
        headerWithBgColor={true}
        withBtnsActions={false}
        fullScreen={true}
        maxWidth="xl"
        noDialogContent={true}
      >
        <FicheIncidentForm
          actionToEdit={ficheToEdit}
          setOpenFormModal={handleCloseFormModal}
          refetch={handleRefetch}
        />
      </CustomModal>
    </>
  );
};

export default ActionOperationnelleComponent;
