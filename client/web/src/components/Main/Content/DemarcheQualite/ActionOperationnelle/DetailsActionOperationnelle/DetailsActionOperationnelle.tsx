import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import { ActionOperationnelle, DetailsActionOperationnelle } from '@app/ui-kit';
import React, { FC } from 'react';
import { RouteComponentProps } from 'react-router';
import { DELETE_ACTION_OPERATIONNELLE } from '../../../../../../federation/demarche-qualite/action-operationnelle/mutation';
import { GET_ACTION_OPERATIONNELLE } from '../../../../../../federation/demarche-qualite/action-operationnelle/query';
import {
  DELETE_ACTION_OPERATIONNELLE as DELETE_ACTION_OPERATIONNELLE_TYPE,
  DELETE_ACTION_OPERATIONNELLEVariables,
} from '../../../../../../federation/demarche-qualite/action-operationnelle/types/DELETE_ACTION_OPERATIONNELLE';
import { GET_ACTION_OPERATIONNELLE as GET_ACTION_OPERATIONNELLE_TYPE } from '../../../../../../federation/demarche-qualite/action-operationnelle/types/GET_ACTION_OPERATIONNELLE';
import { getPharmacie } from '../../../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';

const DetailsActionOperationnelleComponent: FC<RouteComponentProps> = ({
  history: { push },
  match: { params },
}) => {
  const actionOperationnelleId = (params as any).id;

  const pharmacie = getPharmacie();
  const client = useApolloClient();

  const showError = (
    message: string = "Des erreurs se sont survenues pendant l'ajout de l'action",
  ): void => {
    displaySnackBar(client, {
      type: 'ERROR',
      message: message,
      isOpen: true,
    });
  };

  const { loading, data, error } = useQuery<GET_ACTION_OPERATIONNELLE_TYPE, any>(
    GET_ACTION_OPERATIONNELLE,
    {
      variables: {
        id: actionOperationnelleId,
      },
      client: FEDERATION_CLIENT,
      fetchPolicy: 'network-only',
    },
  );

  const [deleteActionOperationnelle, deletionActionOperationnelle] = useMutation<
    DELETE_ACTION_OPERATIONNELLE_TYPE,
    DELETE_ACTION_OPERATIONNELLEVariables
  >(DELETE_ACTION_OPERATIONNELLE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: "L'action a été supprimée avec succès !",
        type: 'SUCCESS',
        isOpen: true,
      });

      push('/demarche-qualite/action-operationnelle');
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs se sont survenues pendant la suppression de l'action",
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const handleDelete = (action: ActionOperationnelle) => {
    if (action.id) {
      deleteActionOperationnelle({
        variables: {
          input: {
            id: action.id,
          },
        },
      });
    }
  };

  const handleEdit = (action: ActionOperationnelle): void => {
    push(`/demarche-qualite/action-operationnelle/${action.id}`);
  };

  const handleGoBack = (): void => {
    push(`/demarche-qualite/action-operationnelle`);
  };

  return (
    <DetailsActionOperationnelle
      loading={loading || deletionActionOperationnelle.loading}
      error={error || deletionActionOperationnelle.error}
      action={data?.dQActionOperationnelle as any}
      onRequestEdit={handleEdit}
      onRequestGoBack={handleGoBack}
      onRequestDelete={handleDelete}
    />
  );
};

export default DetailsActionOperationnelleComponent;
