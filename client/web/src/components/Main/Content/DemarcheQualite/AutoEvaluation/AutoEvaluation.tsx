import React, { FC } from 'react';
import { RouteComponentProps } from 'react-router';
import CustomButton from '../../../../Common/CustomButton';
import NoItemContentImage from '../../../../Common/NoItemContentImage';

const AutoEvaluation: FC<RouteComponentProps> = ({ history }) => {
  const title: string = 'Auto Evaluation';
  const subTitle: string = 'Fonction non encore disponible';

  const handleClick = (): void => {
    history.push(`/demarche-qualite`);
  };

  return (
    <NoItemContentImage
      title={title}
      subtitle={subTitle}
      children={<CustomButton onClick={handleClick}>Retour</CustomButton>}
    />
  );
};

export default AutoEvaluation;
