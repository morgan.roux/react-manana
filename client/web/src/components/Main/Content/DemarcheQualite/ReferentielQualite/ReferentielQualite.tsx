import { useLazyQuery, useQuery } from '@apollo/react-hooks';
import { ISousTheme, ReferentielQualitePage } from '@app/ui-kit';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import {
  GET_FULL_SOUS_THEME,
  GET_FULL_THEMES,
  GET_THEMES,
} from '../../../../../federation/demarche-qualite/theme/query';
import {
  GET_FULL_SOUS_THEME as GET_FULL_SOUS_THEME_TYPE,
  GET_FULL_SOUS_THEMEVariables,
} from '../../../../../federation/demarche-qualite/theme/types/GET_FULL_SOUS_THEME';
import {
  GET_THEMES as GET_THEMES_TYPE,
  GET_THEMES_dqThemes,
} from '../../../../../federation/demarche-qualite/theme/types/GET_THEMES';
import NoItemContentImage from '../../../../Common/NoItemContentImage';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { ExigenceComment } from '../../../../Dashboard/DemarcheQualite/ExigenceComment';

const ReferentielQualite: FC<RouteComponentProps> = ({ history }) => {
  const { loading, error, data } = useQuery<any>(GET_FULL_THEMES, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'network-only',
  });

  const [loadSousTheme, loadingSousTheme] = useLazyQuery<
    GET_FULL_SOUS_THEME_TYPE,
    GET_FULL_SOUS_THEMEVariables
  >(GET_FULL_SOUS_THEME, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'network-only',
  });

  const onRequestGoBack = (): void => {
    history.push('/demarche-qualite');
  };

  const onRequestExigences = (sousTheme: ISousTheme) => {
    loadSousTheme({
      variables: {
        id: sousTheme.id,
      },
    });
  };

  const handleOutilClick = (typologie: string, id: string) => {
    if (typologie === 'checklist') {
      history.push(`/demarche-qualite/outils/checklist`);
    } else {
      history.push(`/demarche-qualite/outils/${typologie}/${id}`);
    }
  };

  console.log('+++data', data?.dqThemes || ([] as any));

  return (
    <ReferentielQualitePage
      exigenceCommentComponentRenderer={exigence => <ExigenceComment exigenceId={exigence.id} />}
      loadingThemes={loading}
      themes={data?.dqThemes || ([] as any)}
      errorLoadingThemes={error}
      exigences={loadingSousTheme.data?.dqSousTheme?.exigences || ([] as any)}
      loadingExigences={loadingSousTheme.called && loadingSousTheme.loading}
      errorLoadingExigences={loadingSousTheme.called ? loadingSousTheme.error : undefined}
      onRequestGoBack={onRequestGoBack}
      onRequestExigences={onRequestExigences}
      onOutilClick={handleOutilClick}
      noItemSelectedComponent={
        <NoItemContentImage
          title="Aucun élément séléctionné."
          subtitle="Séléctionner un sous-thème de la partie gauche pour voir le contenu dans cette partie."
        />
      }
      emptyListComponent={
        <NoItemContentImage
          title="Aucun thème à afficher."
          subtitle="Ajouter-en un en allant dans la page de création de celui-ci."
        />
      }
    />
  );
};

export default withRouter(ReferentielQualite);
