import React, { FC, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { useQuery, useMutation, useLazyQuery, useApolloClient } from '@apollo/react-hooks';
import NoItemListImage from '../../../../Common/NoItemListImage';
import NoItemContentImage from '../../../../Common/NoItemContentImage';
import {
  ChecklistEvaluationPage,
  Checklist,
  ChecklistEvaluation,
  ChecklistEvaluationSectionItem,
} from '@app/ui-kit';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { GET_CHECKLISTS } from './../../../../../federation/demarche-qualite/checklist/query';
import {
  GET_CHECKLISTS as GET_CHECKLISTS_TYPE,
  GET_CHECKLISTSVariables,
} from './../../../../../federation/demarche-qualite/checklist/types/GET_CHECKLISTS';
import {
  CHECKLIST_EVALUATIONS as CHECKLIST_EVALUATIONS_TYPE,
  CHECKLIST_EVALUATIONSVariables,
} from './../../../../../federation/demarche-qualite/evaluation/checklist-evaluation/types/CHECKLIST_EVALUATIONS';
import {
  TOGGLE_CHECKLIST_SECTION_ITEM_EVALUATION as TOGGLE_CHECKLIST_SECTION_ITEM_EVALUATION_TYPE,
  TOGGLE_CHECKLIST_SECTION_ITEM_EVALUATIONVariables,
} from './../../../../../federation/demarche-qualite/evaluation/checklist-evaluation/types/TOGGLE_CHECKLIST_SECTION_ITEM_EVALUATION';

import {
  CHECKLIST_EVALUATION as CHECKLIST_EVALUATION_TYPE,
  CHECKLIST_EVALUATIONVariables,
} from './../../../../../federation/demarche-qualite/evaluation/checklist-evaluation/types/CHECKLIST_EVALUATION';

import {
  ENCOURS_CHECKLIST_EVALUATIONS as ENCOURS_CHECKLIST_EVALUATIONS_TYPE,
  ENCOURS_CHECKLIST_EVALUATIONS_dqEncoursChecklistEvaluations,
} from './../../../../../federation/demarche-qualite/evaluation/checklist-evaluation/types/ENCOURS_CHECKLIST_EVALUATIONS';

import {
  CLOTURE_CHECKLIST_EVALUATIONS as CLOTURE_CHECKLIST_EVALUATIONS_TYPE,
  CLOTURE_CHECKLIST_EVALUATIONS_dqClotureChecklistEvaluations,
} from './../../../../../federation/demarche-qualite/evaluation/checklist-evaluation/types/CLOTURE_CHECKLIST_EVALUATIONS';

import {
  CREATE_CHECKLIST_EVALUATION as CREATE_CHECKLIST_EVALUATION_TYPE,
  CREATE_CHECKLIST_EVALUATIONVariables,
} from './../../../../../federation/demarche-qualite/evaluation/checklist-evaluation/types/CREATE_CHECKLIST_EVALUATION';
import {
  UPDATE_CHECKLIST_EVALUATION as UPDATE_CHECKLIST_EVALUATION_TYPE,
  UPDATE_CHECKLIST_EVALUATIONVariables,
} from './../../../../../federation/demarche-qualite/evaluation/checklist-evaluation/types/UPDATE_CHECKLIST_EVALUATION';
import {
  CLOTURE_CHECKLIST_EVALUATION as CLOTURE_CHECKLIST_EVALUATION_TYPE,
  CLOTURE_CHECKLIST_EVALUATIONVariables,
} from './../../../../../federation/demarche-qualite/evaluation/checklist-evaluation/types/CLOTURE_CHECKLIST_EVALUATION';

import {
  DELETE_CHECKLIST_EVALUATION as DELETE_CHECKLIST_EVALUATION_TYPE,
  DELETE_CHECKLIST_EVALUATIONVariables,
} from './../../../../../federation/demarche-qualite/evaluation/checklist-evaluation/types/DELETE_CHECKLIST_EVALUATION';

import {
  CHECKLIST_EVALUATIONS,
  CHECKLIST_EVALUATION,
  ENCOURS_CHECKLIST_EVALUATIONS,
  CLOTURE_CHECKLIST_EVALUATIONS,
} from './../../../../../federation/demarche-qualite/evaluation/checklist-evaluation/query';
import {
  TOGGLE_CHECKLIST_SECTION_ITEM_EVALUATION,
  UPDATE_CHECKLIST_EVALUATION,
  CREATE_CHECKLIST_EVALUATION,
  DELETE_CHECKLIST_EVALUATION,
  CLOTURE_CHECKLIST_EVALUATION
} from './../../../../../federation/demarche-qualite/evaluation/checklist-evaluation/mutation';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { AppAuthorization } from '../../../../../services/authorization';
import { getUser, getPharmacie } from '../../../../../services/LocalStorage';
import { formatFilename } from '../../../../Common/Dropzone/Dropzone';
import { uploadToS3 } from '../../../../../services/S3';

interface ChecklistEvaluationComponentProps {
  match: {
    params: {
      id: string;
    };
  };
}

const ChecklistEvaluationComponent: FC<ChecklistEvaluationComponentProps & RouteComponentProps> = ({
  history,
  match,
}) => {
  const [togglingEvaluationSectionItemIds, setTogglingEvaluationSectionItemIds] = useState<
    string[]
  >([]);
  const [selectedChecklistId, setSelectedChecklistId] = useState<string>();
  const [selectedEvaluationId, setSelectedEvaluationId] = useState<string>();
  const [loadedEvalutionsType, setLoadedEvaluationsType] = useState<
    'checklist_evaluations' | 'encours_evalutions' | 'cloture_evalutions'
  >('checklist_evaluations');

  const [clotureSaving, setClotureSaving] = useState<boolean>(false);
  const [clotureSaved, setClotureSaved] = useState<boolean>(false);

  const client = useApolloClient();
  const user = getUser();
  const auth = new AppAuthorization(user);

  const variables = !auth.isSupAdminOrIsGpmAdmin
    ? { idPharmacie: getPharmacie().id, onlyChecklistsPharmacie: false }
    : undefined;

  const id = match?.params?.id;

  const { loading, error, data } = useQuery<GET_CHECKLISTS_TYPE, GET_CHECKLISTSVariables>(
    GET_CHECKLISTS,
    { variables, client: FEDERATION_CLIENT },
  );

  const [loadEvaluations, loadingEvaluations] = useLazyQuery<
    CHECKLIST_EVALUATIONS_TYPE,
    CHECKLIST_EVALUATIONSVariables
  >(CHECKLIST_EVALUATIONS, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  React.useEffect(() => {
    if (id) {
      setSelectedEvaluationId(id);
      loadEvaluations({
        variables: {
          idChecklist: id,
        },
      });
    }
  }, [id]);

  const [loadEncoursEvaluations, loadingEncoursEvaluations] = useLazyQuery<
    ENCOURS_CHECKLIST_EVALUATIONS_TYPE,
    ENCOURS_CHECKLIST_EVALUATIONS_dqEncoursChecklistEvaluations
  >(ENCOURS_CHECKLIST_EVALUATIONS, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const [loadClotureEvaluations, loadingClotureEvaluations] = useLazyQuery<
    CLOTURE_CHECKLIST_EVALUATIONS_TYPE,
    CLOTURE_CHECKLIST_EVALUATIONS_dqClotureChecklistEvaluations
  >(CLOTURE_CHECKLIST_EVALUATIONS, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const [loadEvaluation, loadingEvaluation] = useLazyQuery<
    CHECKLIST_EVALUATION_TYPE,
    CHECKLIST_EVALUATIONVariables
  >(CHECKLIST_EVALUATION, {
    client: FEDERATION_CLIENT,
  });

  const [toggleEvaluationSectionItem] = useMutation<
    TOGGLE_CHECKLIST_SECTION_ITEM_EVALUATION_TYPE,
    TOGGLE_CHECKLIST_SECTION_ITEM_EVALUATIONVariables
  >(TOGGLE_CHECKLIST_SECTION_ITEM_EVALUATION, {
    client: FEDERATION_CLIENT,
    onCompleted: (togglingResult) => {
      if (togglingResult.toggleDQChecklistSectionItemEvaluation?.id) {
        setTogglingEvaluationSectionItemIds(
          // TODO : Filter the valid id
          togglingEvaluationSectionItemIds.slice(1),
        );
      }
    },
  });

  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
  });

  const [createChecklistEvaluation, creationChecklistEvaluation] = useMutation<
    CREATE_CHECKLIST_EVALUATION_TYPE,
    CREATE_CHECKLIST_EVALUATIONVariables
  >(CREATE_CHECKLIST_EVALUATION, {
    update: (cache, creationResult) => {
      if (
        selectedChecklistId &&
        creationResult.data &&
        creationResult.data.createDQChecklistEvaluation
      ) {
        const previousList = cache.readQuery<
          CHECKLIST_EVALUATIONS_TYPE,
          CHECKLIST_EVALUATIONSVariables
        >({
          query: CHECKLIST_EVALUATIONS,
          variables: {
            idChecklist: selectedChecklistId,
          },
        })?.dqChecklistEvaluations;

        cache.writeQuery<CHECKLIST_EVALUATIONS_TYPE, CHECKLIST_EVALUATIONSVariables>({
          query: CHECKLIST_EVALUATIONS,
          variables: {
            idChecklist: selectedChecklistId,
          },
          data: {
            dqChecklistEvaluations: [
              creationResult.data.createDQChecklistEvaluation,
              ...(previousList || []),
            ],
          },
        });
      }
    },
    client: FEDERATION_CLIENT,
  });

  const [updateChecklistEvaluation, modificationChecklistEvaluation] = useMutation<
    UPDATE_CHECKLIST_EVALUATION_TYPE,
    UPDATE_CHECKLIST_EVALUATIONVariables
  >(UPDATE_CHECKLIST_EVALUATION, {
    client: FEDERATION_CLIENT,
    onCompleted: () => { },
  });

  const [clotureChecklistEvaluation] = useMutation<
    CLOTURE_CHECKLIST_EVALUATION_TYPE,
    CLOTURE_CHECKLIST_EVALUATIONVariables
  >(CLOTURE_CHECKLIST_EVALUATION, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La checklist a été clôturée avec succès!',
        isOpen: true,
        type: 'SUCCESS',
      });

      setClotureSaved(true)
      setClotureSaving(false)
    },

    onError: () => {
      showError()
      setClotureSaving(false)
    }
  });



  const [deleteChecklistEvaluation, deletionChecklistEvaluation] = useMutation<
    DELETE_CHECKLIST_EVALUATION_TYPE,
    DELETE_CHECKLIST_EVALUATIONVariables
  >(DELETE_CHECKLIST_EVALUATION, { client: FEDERATION_CLIENT });

  const handleGoBack = () => {
    history.push('/demarche-qualite/outils');
  };

  const handleRequestEvaluations = ({ id }: Checklist) => {
    if (id) {
      history.push(`/demarche-qualite/outils/checklist/${id}`);
      setLoadedEvaluationsType('checklist_evaluations');
      loadEvaluations({
        variables: {
          idChecklist: id,
        },
      });

      setSelectedChecklistId(id);
    }
  };

  const handleRequestEvaluationsByStatus = (cloture: boolean) => {
    if (cloture) {
      setLoadedEvaluationsType('cloture_evalutions');
      loadClotureEvaluations();
    } else {
      setLoadedEvaluationsType('encours_evalutions');
      loadEncoursEvaluations();
    }
  };

  const handleRequestEvaluation = (id: string) => {
    if (id) {
      loadEvaluation({
        variables: {
          id,
        },
      });
    }
  };

  const handleRequestSaveEvaluation = (id: string | undefined, ordre: number, libelle: string) => {
    if (id) {
      updateChecklistEvaluation({
        variables: {
          id,
          checklistEvaluationInput: {
            ordre,
            libelle,
          },
        },
      });
    } else if (selectedChecklistId) {
      createChecklistEvaluation({
        variables: {
          idChecklist: selectedChecklistId,
          checklistEvaluationInput: {
            ordre,
            libelle,
          },
        },
      });
    }
  };

  const handleDeleteEvaluation = (evaluation: ChecklistEvaluation) => {
    deleteChecklistEvaluation({
      variables: {
        id: evaluation.id,
      },

      update: (cache, deletionResult) => {
        if (selectedChecklistId && deletionResult?.data?.deleteDQChecklistEvaluation?.id) {

          loadingEncoursEvaluations.refetch()
          loadingClotureEvaluations.refetch()
          loadingEvaluations.refetch()

          /*const previousList = cache.readQuery<
            CHECKLIST_EVALUATIONS_TYPE,
            CHECKLIST_EVALUATIONSVariables
          >({
            query: CHECKLIST_EVALUATIONS,
            variables: {
              idChecklist: selectedChecklistId,
            },
          })?.dqChecklistEvaluations;

          cache.writeQuery<CHECKLIST_EVALUATIONS_TYPE, CHECKLIST_EVALUATIONSVariables>({
            query: CHECKLIST_EVALUATIONS,
            variables: {
              idChecklist: selectedChecklistId,
            },
            data: {
              dqChecklistEvaluations: (previousList || []).filter(
                (evaluation) =>
                  evaluation?.id !==
                  deletionChecklistEvaluation.data?.deleteDQChecklistEvaluation?.id,
              ),
            },
          });*/
        }
      },
    });
  };

  const handleToggleItem = (
    evaluation: ChecklistEvaluation,
    evaluationSectionItem: ChecklistEvaluationSectionItem,
  ) => {
    setTogglingEvaluationSectionItemIds([
      ...togglingEvaluationSectionItemIds,
      evaluationSectionItem.id,
    ]);

    toggleEvaluationSectionItem({
      variables: {
        idChecklistEvaluation: evaluation.id,
        idChecklistSectionItem: evaluationSectionItem.idChecklistSectionItem,
      },
    });
  };


  const showError = (
    message: string = "Des erreurs se sont survenues pendant la clôture de la checklist",
  ): void => {
    displaySnackBar(client, {
      type: 'ERROR',
      message: message,
      isOpen: true,
    });
  };


  const doClose = ({ evaluation, commentaire, fichiers }: any) => {
    clotureChecklistEvaluation({
      variables: {
        id: evaluation.id,
        checklistEvaluationInput: {
          commentaire,
          fichiers
        },
      },
    });

  }

  const handleCloseEvaluation = ({ evaluation, commentaire, fichiers }: any): void => {

    setClotureSaved(false)
    setClotureSaving(true)
    if (fichiers) {
      const filePaths = fichiers.map((fichier: File) => formatFilename(fichier, ``));
      doCreatePutPresignedUrl({
        variables: {
          filePaths,
        },
      })
        .then(async (response) => {
          if (response.data?.createPutPresignedUrls) {
            try {


              const uplodatedFichiers = await Promise.all(response.data?.createPutPresignedUrls.map(async (presignedUrl, index) => {

                if (presignedUrl) {
                  const uploadResult = await uploadToS3(fichiers[index], presignedUrl.presignedUrl);
                  if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                    return {
                      chemin: presignedUrl.filePath,
                      nomOriginal: fichiers[index].name,
                      type: fichiers[index].type,
                    };
                  }
                }

                return null

              }))

              doClose({ evaluation, commentaire, fichiers: uplodatedFichiers.filter((uf => !!uf)) });

              return;
            } catch (error) { }
          }

          showError();
        })
        .catch(() => showError());
    }
    else {

      doClose({ evaluation, commentaire });

    }



  };

  return (
    <ChecklistEvaluationPage
      selectedChecklistId={selectedChecklistId}
      selectedEvaluationId={selectedEvaluationId}
      loadingChecklists={loading}
      errorLoadingChecklists={error}
      idFromUrl={id}
      checklists={data?.dqChecklists as any}
      loadingEvaluations={
        loadedEvalutionsType === 'checklist_evaluations'
          ? loadingEvaluations.loading
          : loadedEvalutionsType === 'encours_evalutions'
            ? loadingEncoursEvaluations.loading
            : loadedEvalutionsType === 'cloture_evalutions'
              ? loadingEncoursEvaluations.loading
              : undefined
      }
      errorLoadingEvaluations={
        loadedEvalutionsType === 'checklist_evaluations'
          ? loadingEvaluations.error
          : loadedEvalutionsType === 'encours_evalutions'
            ? loadingEncoursEvaluations.error
            : loadedEvalutionsType === 'cloture_evalutions'
              ? loadingClotureEvaluations.error
              : undefined
      }
      evaluations={
        loadedEvalutionsType === 'checklist_evaluations'
          ? (loadingEvaluations.data?.dqChecklistEvaluations as any)
          : loadedEvalutionsType === 'encours_evalutions'
            ? loadingEncoursEvaluations.data?.dqEncoursChecklistEvaluations
            : loadedEvalutionsType === 'cloture_evalutions'
              ? loadingClotureEvaluations.data?.dqClotureChecklistEvaluations
              : undefined
      }
      togglingEvaluationSectionItemIds={togglingEvaluationSectionItemIds}
      loadingEvaluation={loadingEvaluation.called && loadingEvaluation.loading}
      errorLoadingEvaluation={loadingEvaluation.called ? loadingEvaluation.error : undefined}
      loadedEvaluation={
        loadingEvaluation.called
          ? (loadingEvaluation.data?.dqChecklistEvaluation as any)
          : undefined
      }
      savingEvaluation={
        creationChecklistEvaluation.loading ||
        modificationChecklistEvaluation.loading ||
        deletionChecklistEvaluation.loading
      }
      errorSavingEvaluation={
        creationChecklistEvaluation.error ||
        modificationChecklistEvaluation.error ||
        deletionChecklistEvaluation.error
      }
      emptyListComponent={<NoItemListImage />}
      noItemSelectedComponent={<NoItemContentImage title="" subtitle="" />}
      onRequestGoBack={handleGoBack}
      onRequestEvaluations={handleRequestEvaluations}
      onRequestEvaluationsByStatus={handleRequestEvaluationsByStatus}
      onRequestEvaluation={handleRequestEvaluation}
      onRequestToggleItem={handleToggleItem}
      onRequestSaveEvaluation={handleRequestSaveEvaluation}
      onRequestDeleteEvaluation={handleDeleteEvaluation}
      onRequestCloseEvaluation={handleCloseEvaluation}
      cloture={{
        setSaved: () => setClotureSaved(false),
        saved: clotureSaved,
        saving: clotureSaving
      }}
    />
  );
};

export default withRouter(ChecklistEvaluationComponent);
