import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import {
  CREATE_OUTIL,
  DELETE_OUTIL,
  UPDATE_OUTIL,
} from '../../../../../federation/demarche-qualite/outil/mutation';
import { GET_OUTIL, GET_OUTILS } from '../../../../../federation/demarche-qualite/outil/query';
import {
  CREATE_OUTIL as CREATE_OUTIL_TYPE,
  CREATE_OUTILVariables,
} from '../../../../../federation/demarche-qualite/outil/types/CREATE_OUTIL';
import {
  DELETE_OUTIL as DELETE_OUTIL_TYPE,
  DELETE_OUTILVariables,
} from '../../../../../federation/demarche-qualite/outil/types/DELETE_OUTIL';
import {
  GET_OUTILS as GET_OUTILS_TYPE,
  GET_OUTILSVariables,
} from '../../../../../federation/demarche-qualite/outil/types/GET_OUTILS';
import {
  UPDATE_OUTIL as UPDATE_OUTIL_TYPE,
  UPDATE_OUTILVariables,
} from '../../../../../federation/demarche-qualite/outil/types/UPDATE_OUTIL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { uploadToS3 } from '../../../../../services/S3';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { formatFilename } from '../../../../Common/Dropzone/Dropzone';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import {
  GET_OUTIL as GET_OUTIL_TYPE,
  GET_OUTILVariables,
} from '../../../../../federation/demarche-qualite/outil/types/GET_OUTIL';
import NoItemContentImage from '../../../../Common/NoItemContentImage';
import { FichePage, noContentValues } from '@app/ui-kit';

interface ToolsProps {
  match: {
    params: {
      typologie: string;
      id: string;
    };
  };
}

const Tools: FC<ToolsProps & RouteComponentProps> = ({
  history: { push },
  match: {
    params: { typologie, id },
  },
}) => {
  const client = useApolloClient();
  const [mutationLoading, setMutationLoading] = useState<boolean>(false);
  useEffect( () => {
    if(id){
      getOutil({
        variables: {
          typologie: typologie,
          id: id,
        },
      });
    }
  } , [ id ] );

  // Get all tools by typologie
  const { loading, data, error } = useQuery<GET_OUTILS_TYPE, GET_OUTILSVariables>(GET_OUTILS, {
    variables: {
      typologie: typologie,
    },
    client: FEDERATION_CLIENT,
  });

  const [
    getOutils,
    { loading: getOutilsLoading, data: getOutilsData, error: getOutilsError },
  ] = useLazyQuery<GET_OUTILS_TYPE, GET_OUTILSVariables>(GET_OUTILS, {
    client: FEDERATION_CLIENT,
  });

  const [
    getOutil,
    { loading: getOutilLoading, data: getOutilData, error: getOutilError },
  ] = useLazyQuery<GET_OUTIL_TYPE, GET_OUTILVariables>(GET_OUTIL, {
    client: FEDERATION_CLIENT,
  });

  const showError = (
    message: string = "Des erreurs se sont survenues pendant l'ajout de l'outil",
  ): void => {
    displaySnackBar(client, {
      type: 'ERROR',
      message: message,
      isOpen: true,
    });
  };

  const [addOutil, creationOutil] = useMutation<CREATE_OUTIL_TYPE, CREATE_OUTILVariables>(
    CREATE_OUTIL,
    {
      onCompleted: () => {
        setMutationLoading(false);
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: "L'outil a été ajouté avec succès !",
          isOpen: true,
        });
      },
      onError: () => {
        setMutationLoading(false);
        displaySnackBar(client, {
          type: 'ERROR',
          message: "Des erreurs se sont survenues pendant l'ajout de l'outil",
          isOpen: true,
        });
      },
      update: (cache, creationResult) => {
        if (creationResult.data && creationResult.data.createDQOutil) {
          const previousList = cache.readQuery<GET_OUTILS_TYPE, GET_OUTILSVariables>({
            query: GET_OUTILS,
            variables: {
              typologie: typologie,
            },
          })?.dqOutils;

          cache.writeQuery<GET_OUTILS_TYPE>({
            query: GET_OUTILS,
            variables: {
              typologie: typologie,
            },
            data: {
              dqOutils: [...(previousList || []), creationResult.data.createDQOutil],
            },
          });
        }
      },
      client: FEDERATION_CLIENT,
    },
  );

  const [updateOutil, modificationOutil] = useMutation<UPDATE_OUTIL_TYPE, UPDATE_OUTILVariables>(
    UPDATE_OUTIL,
    {
      onCompleted: () => {
        setMutationLoading(false);
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: "L'outil a été modifié avec succès !",
          isOpen: true,
        });
      },
      onError: () => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: "Des erreurs se sont survenues pendant la modification de l'outil !",
          isOpen: true,
        });
      },
      client: FEDERATION_CLIENT,
    },
  );

  const [deleteOutil] = useMutation<DELETE_OUTIL_TYPE, DELETE_OUTILVariables>(DELETE_OUTIL, {
    onCompleted: () => {
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: "L'outil a été supprimé avec succès !",
        isOpen: true,
      });
    },
    onError: () => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: "Des erreurs se sont survenues pendant la suppression de l'outil !",
        isOpen: true,
      });
    },
    update: (cache, deletionResult) => {
      if (
        deletionResult.data &&
        deletionResult.data.deleteDQOutil &&
        deletionResult.data.deleteDQOutil.id
      ) {
        const previousList = cache.readQuery<GET_OUTILS_TYPE, GET_OUTILSVariables>({
          query: GET_OUTILS,
          variables: {
            typologie: typologie,
          },
        })?.dqOutils;

        const deletedId = deletionResult.data.deleteDQOutil.id;

        cache.writeQuery<GET_OUTILS_TYPE>({
          query: GET_OUTILS,
          variables: {
            typologie: typologie,
          },
          data: {
            dqOutils: (previousList || []).filter((item) => item?.id !== deletedId),
          },
        });
      }
    },
    client: FEDERATION_CLIENT,
  });

  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
  });

  const handleGoBack = (): void => {
    push(`/demarche-qualite/outils`);
  };

  const upsertOutil = ({ id, ordre, libelle, fichier }: any) => {
    if (id) {
      updateOutil({
        variables: {
          id: id,
          typologie: typologie,
          outilInput: fichier
            ? { ordre, libelle, fichier }
            : {
                ordre,
                libelle,
              },
        },
      });
    } else {
      addOutil({
        variables: {
          typologie: typologie,
          outilInput: {
            ordre,
            libelle,
            fichier,
          },
        },
      });
    }
  };

  const handleSave = ({ id, ordre, libelle, selectedFile, launchUpload }: any): void => {
    setMutationLoading(true);
    if (launchUpload) {
      const filePaths = [selectedFile].map((fichier) => formatFilename(fichier, ``));
      doCreatePutPresignedUrl({
        variables: {
          filePaths,
        },
      })
        .then(async (response) => {
          if (response.data?.createPutPresignedUrls) {
            try {
              const presignedUrl = response.data.createPutPresignedUrls[0];
              let fichier: any = undefined;
              if (presignedUrl) {
                const uploadResult = await uploadToS3(selectedFile, presignedUrl.presignedUrl);
                if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                  fichier = {
                    chemin: presignedUrl.filePath,
                    nomOriginal: selectedFile.name,
                    type: selectedFile.type,
                  };
                }
              }

              upsertOutil({ id, ordre, libelle, fichier });

              return;
            } catch (error) {}
          }

          showError();
        })
        .catch(() => showError());
    } else {
      upsertOutil({ id, ordre, libelle });
    }
  };

  const handleDelete = (fiche: any): void => {
    deleteOutil({
      variables: {
        id: fiche.id,
        typologie: typologie,
      },
    });
  };

  const handleShowDetail = (fiche: any): void => {
    push(`/demarche-qualite/outils/${typologie}/${fiche.id}`);
    getOutil({
      variables: {
        typologie: typologie,
        id: fiche.id,
      },
    });
  };

  return (
    <FichePage
      typologie={typologie}
      saving={mutationLoading}
      idFromUrl={id}
      savingCompleted={!creationOutil.loading && !modificationOutil.loading && !mutationLoading}
      fiches={{
        loading: loading || getOutilsLoading,
        error: (error || getOutilsError) as any,
        data: data?.dqOutils as any,
      }}
      detailsRenderer={{
        loading: getOutilLoading,
        error: getOutilError as any,
        currentData: getOutilData?.dqOutil as any,
      }}
      emptyListComponent={<NoItemContentImage {...noContentValues(typologie).list} />}
      noItemSelectedComponent={<NoItemContentImage {...noContentValues(typologie).content} />}
      onRequestDelete={handleDelete}
      onRequestGoBack={handleGoBack}
      onRequestSave={handleSave}
      onRequestShowDetail={handleShowDetail}
    />
  );
};

export default Tools;
