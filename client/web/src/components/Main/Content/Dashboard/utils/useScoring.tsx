import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { useLazyQuery } from '@apollo/react-hooks';
import { useCallback, useEffect } from 'react';
import {
  GET_ITEM_SCORINGS,
  GET_ITEM_SCORINGS_AGGREGATES,
} from '../../../../../federation/tools/itemScoring/query';
import {
  GET_ITEM_SCORINGS as GET_ITEM_SCORINGS_TYPE,
  GET_ITEM_SCORINGSVariables,
  GET_ITEM_SCORINGS_itemScorings_nodes,
} from '../../../../../federation/tools/itemScoring/types/GET_ITEM_SCORINGS';
import {
  GET_ITEM_SCORINGS_AGGREGATES as GET_ITEM_SCORINGS_AGGREGATES_TYPE,
  GET_ITEM_SCORINGS_AGGREGATESVariables,
} from '../../../../../federation/tools/itemScoring/types/GET_ITEM_SCORINGS_AGGREGATES';
import { endTime, startTime } from '../../DemarcheQualite/util';
import {
  ItemScoringFilter,
  ItemScoringSort,
  ItemScoringSortFields,
  SortDirection,
} from '../../../../../types/federation-global-types';
import { QueryResult } from 'react-apollo';
import { ApolloError } from 'apollo-client';
import { isToday, isTodayBetween } from './util';

export const useScoring = (
  filters,
  user,
  pharmacie,
): [
  GET_ITEM_SCORINGS_itemScorings_nodes[],
  boolean,
  ApolloError | undefined,
  number | undefined | null,
  QueryResult<GET_ITEM_SCORINGS_TYPE, GET_ITEM_SCORINGSVariables>,
  QueryResult<GET_ITEM_SCORINGS_AGGREGATES_TYPE, GET_ITEM_SCORINGS_AGGREGATESVariables>,
] => {
  const [loadScoring, loadingScoring] = useLazyQuery<
    GET_ITEM_SCORINGS_TYPE,
    GET_ITEM_SCORINGSVariables
  >(GET_ITEM_SCORINGS, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
  });

  const [countScoring, countingScoring] = useLazyQuery<
    GET_ITEM_SCORINGS_AGGREGATES_TYPE,
    GET_ITEM_SCORINGS_AGGREGATESVariables
  >(GET_ITEM_SCORINGS_AGGREGATES, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
  });

  const getSort = useCallback(() => {
    console.log(filters);
    const sort: ItemScoringSort[] = [];
    if (filters && filters.order) {
      if (filters.order.field === 'importance') {
        sort.push({
          field: ItemScoringSortFields.coefficientImportance,
          direction: filters.order.direction,
        });
      } else if (filters.order.field === 'urgence') {
        sort.push({
          field: ItemScoringSortFields.coefficientUrgence,
          direction: filters.order.direction,
        });
      } else if (filters.order.field === 'statut') {
        sort.push({
          field: ItemScoringSortFields.ordreStatut,
          direction: filters.order.direction,
        });
      } else if (filters.order.field === 'source') {
        sort.push({
          field: ItemScoringSortFields.nomItem,
          direction: filters.order.direction,
        });
      } else if (filters.order.field === 'retard') {
        sort.push({
          field: ItemScoringSortFields.nombreJoursRetard,
          direction: filters.order.direction,
        });
      } else if (filters.order.field === 'score') {
        sort.push({
          field: ItemScoringSortFields.score,
          direction: filters.order.direction,
        });
      }
    }
    if (sort.length === 0) {
      sort.push({ field: ItemScoringSortFields.score, direction: SortDirection.DESC });
    }
    return sort;
  }, [filters]);

  useEffect(() => {
    const filterList: ItemScoringFilter[] = [
      {
        idUser: {
          eq: user.id,
        },
      },
      {
        idPharmacie: {
          eq: pharmacie.id,
        },
      },
    ];

    if (filters.items && filters.items.length > 0) {
      filterList.push({
        idItem: {
          in: (filters.items || []).map(element => {
            return element.id;
          }),
        },
      });
    }

    if (filters.searchText) {
      filterList.push({
        description: {
          iLike: `%${filters.searchText}%`,
        },
      });
    }

    if (filters.importance) {
      filterList.push({
        idImportance: {
          eq: filters.importance.id,
        },
      });
    }

    if (filters.urgence) {
      filterList.push({
        idUrgence: {
          eq: filters.urgence.id,
        },
      });
    }

    if (filters.date) {
      if (isToday(filters.date)) {
        // Importer les informations en retard
        filterList.push({
          dateEcheance: {
            lte: endTime(filters.date),
          },
        });
      } else {
        filterList.push({
          dateEcheance: {
            between: {
              lower: startTime(filters.date),
              upper: endTime(filters.date),
            },
          },
        });
      }
    }

    if (filters.dateDebut && filters.dateFin) {
      if (isTodayBetween(filters.dateDebut, filters.dateFin)) {
        // importer les informations en retard (ignorer la date début)
        filterList.push({
          dateEcheance: {
            lte: endTime(filters.dateFin),
          },
        });
      } else {
        filterList.push({
          dateEcheance: {
            between: {
              lower: startTime(filters.dateDebut),
              upper: endTime(filters.dateFin),
            },
          },
        });
      }
    }

    loadScoring({
      variables: {
        filter: {
          and: filterList,
        },
        paging: {
          offset: filters.paging.offset,
          limit: filters.paging.limit,
        },
        sorting: getSort(),
      },
    });
    countScoring({
      variables: {
        filter: {
          and: filterList,
        },
      },
    });
  }, [filters]);

  return [
    loadingScoring.data?.itemScorings.nodes || [],
    loadingScoring.loading || countingScoring.loading,
    loadingScoring.error || countingScoring.error,
    (countingScoring.data?.itemScoringAggregate.length || 0) > 0
      ? countingScoring.data?.itemScoringAggregate[0].count?.id || 0
      : 0,
    loadingScoring,
    countingScoring,
  ];
};
