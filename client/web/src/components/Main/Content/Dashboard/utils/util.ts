import moment from 'moment';
import { startTime } from '../../DemarcheQualite/util';

export const isToday = (date: any) => {
  return startTime(moment()) === startTime(date);
};

export const isTodayBetween = (dateDebut: any, dateFin: any) => {
  return moment().isBetween(dateDebut, dateFin);
};
