import { useApolloClient, useLazyQuery, useMutation } from '@apollo/react-hooks';
import _ from 'lodash';
import {
  GET_RENDEZ_VOUS,
  GET_ROW_RENDEZ_VOUS,
} from '../../../../../federation/partenaire-service/rendez-vous/query';
import { UPDATE_RENDEZ_VOUS } from '../../../../../federation/partenaire-service/rendez-vous/mutation';
import {
  UPDATE_RENDEZ_VOUS as UPDATE_RENDEZ_VOUS_TYPE,
  UPDATE_RENDEZ_VOUSVariables,
} from '../../../../../federation/partenaire-service/rendez-vous/types/UPDATE_RENDEZ_VOUS';
import {
  GET_RENDEZ_VOUS as GET_RENDEZ_VOUS_TYPE,
  GET_RENDEZ_VOUSVariables,
  GET_RENDEZ_VOUS_pRTRendezVous_nodes,
} from '../../../../../federation/partenaire-service/rendez-vous/types/GET_RENDEZ_VOUS';
import {
  GET_ROW_RENDEZ_VOUS as GET_ROW_RENDEZ_VOUS_TYPE,
  GET_ROW_RENDEZ_VOUSVariables,
} from '../../../../../federation/partenaire-service/rendez-vous/types/GET_ROW_RENDEZ_VOUS';
import { getPharmacie } from '../../../../../services/LocalStorage';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { useEffect, useState } from 'react';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';

interface OnRequestNextRdvProps {
  take: number;
  skip: number;
}

export const useRendezVous = (): [
  (paging: OnRequestNextRdvProps) => void,
  (data: any) => void,
  GET_RENDEZ_VOUS_pRTRendezVous_nodes[] | undefined,
  boolean,
  Error | undefined,
  number,
] => {
  const [data, setData] = useState<GET_RENDEZ_VOUS_pRTRendezVous_nodes[]>([]);
  const client = useApolloClient();

  const [loadRdv, loadingRdv] = useLazyQuery<GET_RENDEZ_VOUS_TYPE, GET_RENDEZ_VOUSVariables>(
    GET_RENDEZ_VOUS,
    {
      client: FEDERATION_CLIENT,
    },
  );

  useEffect(() => {
    if (loadingRdv.data?.pRTRendezVous?.nodes) {
      const temp = loadingRdv.data.pRTRendezVous.nodes;
      setData(prev =>
        Object.values(
          (prev || []).concat(temp).reduce((acc, obj) => ((acc[obj.id] = obj), acc), {}),
        ),
      );
    }
  }, [loadingRdv.data]);

  const [loadRowRdv, loadingRowRdv] = useLazyQuery<
    GET_ROW_RENDEZ_VOUS_TYPE,
    GET_ROW_RENDEZ_VOUSVariables
  >(GET_ROW_RENDEZ_VOUS, {
    client: FEDERATION_CLIENT,
  });

  const [updateRdv, updatingRdv] = useMutation<
    UPDATE_RENDEZ_VOUS_TYPE,
    UPDATE_RENDEZ_VOUSVariables
  >(UPDATE_RENDEZ_VOUS, {
    client: FEDERATION_CLIENT,
    onCompleted: updated => {
      setData(prev =>
        (prev || []).map(item => {
          if (item.id === updated.updateOnePRTRendezVous.id) {
            return updated.updateOnePRTRendezVous;
          }
          return item;
        }),
      );
      displaySnackBar(client, {
        message: 'Le rendez-vous est reporté',
        isOpen: true,
        type: 'SUCCESS',
      });
    },
    onError: error => {
      displaySnackBar(client, {
        message: error.message,
        isOpen: true,
        type: 'ERROR',
      });
    },
  });

  const handleOnRequest = (paging: OnRequestNextRdvProps) => {
    loadRdv({
      variables: {
        filter: {
          statut: {
            notIn: ['REALISE', 'ANNULE'],
          },
          idPharmacie: {
            eq: getPharmacie().id,
          },
        },
        paging: {
          offset: paging.skip,
          limit: paging.take,
        },
      },
    });
    loadRowRdv({
      variables: {
        filter: {
          statut: {
            notIn: ['REALISE', 'ANNULE'],
          },
          idPharmacie: {
            eq: getPharmacie().id,
          },
        },
      },
    });
  };

  const handleSave = (data: any) => {
    updateRdv({
      variables: {
        id: data.id,
        input: {
          typeReunion: data.typeReunion,
          dateRendezVous: data.dateRendezVous,
          heureDebut: data.heureDebut,
          heureFin: data.heureFin,
          idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
          partenaireType: data.partenaireType,
          idUserParticipants: (data.participants || []).map(({ id }) => id),
          idInvites: (data.invites || []).map(({ id }) => id) as any,
          idSubject: data.idSubject,
          statut: 'REPORTER',
        },
      },
    });
  };

  return [
    handleOnRequest,
    handleSave,
    data,
    loadingRdv.loading,
    loadingRdv.error,
    (loadingRowRdv.data?.pRTRendezVousAggregate.length || 0) > 0
      ? loadingRowRdv.data?.pRTRendezVousAggregate[0].count?.id || 0
      : 0,
  ];
};
