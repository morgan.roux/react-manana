import { useQuery } from '@apollo/react-hooks';
import { SeriesItem } from '@app/ui-kit/components/pages/DashboardPage/DashboardRepartition';
import { GET_ITEM_STATISTIQUES } from '../../../../../federation/tools/itemScoring/query';
import {
  ITEM_STATISTIQUES,
  ITEM_STATISTIQUESVariables,
} from '../../../../../federation/tools/itemScoring/types/ITEM_STATISTIQUES';
import { getUser } from '../../../../../services/LocalStorage';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';

export const useStatistique = () => {
  const loadStatistique = useQuery<ITEM_STATISTIQUES, ITEM_STATISTIQUESVariables>(
    GET_ITEM_STATISTIQUES,
    {
      fetchPolicy: 'cache-and-network',
      variables: { idUser: getUser().id },
      client: FEDERATION_CLIENT,
    },
  );

  const dashboardFilter = {
    data: (loadStatistique?.data?.itemStatistiques || []).map(itemStatistique => {
      return {
        ...itemStatistique.item,
        occurence: itemStatistique.occurence,
      };
    }),
    loading: loadStatistique.loading,
    error: loadStatistique.error,
  };

  const statistiques = loadStatistique?.data?.itemStatistiques?.map(itemStatistique => {
    return {
      categorie: itemStatistique.item.name,
      valeur: itemStatistique.percentage,
    };
  });

  statistiques?.sort((a: SeriesItem, b: SeriesItem) => b.valeur - a.valeur);

  const series = statistiques?.filter(
    (statistique, index) => statistique.valeur !== 0 && index < 3,
  );

  const otherSerie = statistiques
    ?.filter((_statistique, index) => index > 1)
    .reduce(
      (previousSerie: SeriesItem, currentSerie: SeriesItem) => {
        return { categorie: 'Autres', valeur: previousSerie.valeur + currentSerie.valeur };
      },
      { categorie: 'Autres', valeur: 0 },
    );

  series && series.length > 2 && (series[2] = otherSerie || { categorie: 'Autres', valeur: 0 });

  return {
    series,
    dashboardFilter,
    loadStatistique,
  };
};
