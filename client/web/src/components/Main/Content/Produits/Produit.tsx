import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import { IconButton, Tooltip } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import { Delete, Edit } from '@material-ui/icons';
import classNames from 'classnames';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { UPDATE_ONE_PRODUIT_CANAL } from '../../../../federation/partenaire-service/produit-canal/mutation';
import {
  UPDATE_ONE_PRODUIT_CANAL as UPDATE_ONE_PRODUIT_CANALTypes,
  UPDATE_ONE_PRODUIT_CANALVariables,
} from '../../../../federation/partenaire-service/produit-canal/types/UPDATE_ONE_PRODUIT_CANAL';
import {
  GET_OPERATION_ARTICLES,
  GET_OPERATION_COMMERCIALE,
} from '../../../../graphql/OperationCommerciale/query';
import {
  OPERATION_ARTICLES,
  OPERATION_ARTICLESVariables,
} from '../../../../graphql/OperationCommerciale/types/OPERATION_ARTICLES';
import {
  OPERATION_COMMERCIALE,
  OPERATION_COMMERCIALEVariables,
} from '../../../../graphql/OperationCommerciale/types/OPERATION_COMMERCIALE';
import { GET_MY_PANIERS } from '../../../../graphql/Panier/query';
import { myPaniers, myPaniersVariables } from '../../../../graphql/Panier/types/myPaniers';
import { Panier } from '../../../../graphql/Panier/types/Panier';
import { GET_CURRENT_PHARMACIE } from '../../../../graphql/Pharmacie/local';
import { UPDATE_PRODUIT_DATE_SUPPRIMER } from '../../../../graphql/Produit/mutation';
import { GET_GESTION_PRODUIT_CANAL } from '../../../../graphql/Produit/query';
import {
  updateProduitSupprimer,
  updateProduitSupprimerVariables,
} from '../../../../graphql/Produit/types/updateProduitSupprimer';
import { ProduitCanalInfo } from '../../../../graphql/ProduitCanal/types/ProduitCanalInfo';
import { DO_CREATE_GET_PRESIGNED_URL } from '../../../../graphql/S3';
import {
  CREATE_GET_PRESIGNED_URL,
  CREATE_GET_PRESIGNED_URLVariables,
} from '../../../../graphql/S3/types/CREATE_GET_PRESIGNED_URL';
import { SEARCH } from '../../../../graphql/search/query';
import {
  SEARCH as SEARCH_INTERFACE,
  SEARCHVariables,
} from '../../../../graphql/search/types/SEARCH';
import { GET_SMYLEYS } from '../../../../graphql/Smyley/query';
import { SMYLEYS, SMYLEYSVariables } from '../../../../graphql/Smyley/types/SMYLEYS';
import ICurrentPharmacieInterface from '../../../../Interface/CurrentPharmacieInterface';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { AppAuthorization } from '../../../../services/authorization';
import { getGroupement, getUser } from '../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import CustomButton from '../../../Common/CustomButton';
import { CustomDatePicker } from '../../../Common/CustomDateTimePicker';
import { CustomModal } from '../../../Common/CustomModal';
import CustomContent from '../../../Common/newCustomContent';
import { GET_SKIP_AND_TAKE } from '../../../Common/newWithSearch/withSearch';
import NoItemContentImage from '../../../Common/NoItemContentImage';
import PhotoModal from '../../../Common/PhotoModal';
import { resetSearchFilters } from '../../../Common/withSearch/withSearch';
import { Loader } from '../../../Dashboard/Content/Loader';
import { FEDERATION_CLIENT } from '../../../Dashboard/DemarcheQualite/apolloClientFederation';
import CreationProduit from '../../../Dashboard/GestionProduits/CreationProduit/CreationProduit';
import Footer from '../Footer/Footer';
import CatalogueProduitOcInfo from '../OperationCommerciale/CatalogueProduitOcInfo';
import { ProduitCard } from '../Panier';
import Article from '../Panier/Article/Article';
import { ArticleOCArray, GET_ARTICLE_OC_ARRAY } from '../Panier/Article/Quantite/Quantite';
import ProduitColumns from '../Pilotage/Cible/Columns/Produit';
import { AppBarFilter } from './AppBarFilter';
import Pagination from './Pagination/Pagination';
import { getColumns } from './ProduitListView/ProduitListView';
import useStyles from './styles';

interface ProduitProps {
  match: {
    params: {
      type: string | undefined;
      id: string | undefined;
      view: string;
      idOperation: string | undefined;
      idRemise: string | undefined;
    };
  };
  listResult: any;
  currentTake?: number;
  currentSkip?: number;
}

const Produit: FC<ProduitProps & RouteComponentProps> = ({
  history: {
    location: { state },
    push,
  },
  match,
  location,
  listResult,
  currentSkip,
  currentTake,
}) => {
  const classes = useStyles({});
  const isOnCreate = location.pathname.includes('/create');
  const currentView = location.pathname.includes('/card') ? 'card' : 'list';
  const idOperation = match.params?.idOperation;
  const idRemise = match.params?.idRemise;
  const idProduit = match.params?.id;
  const client = useApolloClient();
  const [parentPanier, setParentPanier] = useState<any>(null);
  const groupement = getGroupement();
  const isInGestionProduits = location.pathname.includes('/gestion-produits');
  const isInLaboratoireProduits = location.pathname.includes('/laboratoires');
  const [openPhoto, setOpenPhoto] = useState<boolean>(false);
  const [currentPhoto, setCurrentPhoto] = useState<any>();
  const [openDialog, setOpenDialog] = useState(false);
  const [dateSupprimer, setDateSupprimer] = useState(null);
  const [openFormModal, setOpenFormModal] = useState(
    (isInGestionProduits && idProduit) || isOnCreate ? true : false,
  );
  const [selectedProduits, setSelectedProduits] = useState<any>([]);

  const user = getUser();
  const auth = new AppAuthorization(user);

  // Get smyleys by groupement
  const getSmyleys = useQuery<SMYLEYS, SMYLEYSVariables>(GET_SMYLEYS, {
    variables: {
      idGroupement: groupement && groupement.id,
    },
  });

  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const currentPharmacie = (myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie) || null;

  const operationArticlesResult = useQuery<OPERATION_ARTICLES, OPERATION_ARTICLESVariables>(
    GET_OPERATION_ARTICLES,
    {
      variables: {
        id: idOperation || '',
        idPharmacie: (currentPharmacie && currentPharmacie.id) || '',
        /* idOperation, */
        idRemiseOperation: idRemise,
      },
      skip: !idOperation || !currentPharmacie,
    },
  );

  const operationResult = useQuery<OPERATION_COMMERCIALE, OPERATION_COMMERCIALEVariables>(
    GET_OPERATION_COMMERCIALE,
    {
      variables: {
        id: idOperation || '',
        idPharmacie: (currentPharmacie && currentPharmacie.id) || '',
        /* idOperation, */
        idRemiseOperation: idRemise,
      },
      skip: !idOperation || !currentPharmacie,
    },
  );

  const operationArticles =
    (operationArticlesResult &&
      operationArticlesResult.data &&
      operationArticlesResult.data.operation &&
      operationArticlesResult.data.operation.operationArticlesWithQte) ||
    [];

  const [doCreateGetPresignedUrls, doCreateGetPresignedUrlsResult] = useMutation<
    CREATE_GET_PRESIGNED_URL,
    CREATE_GET_PRESIGNED_URLVariables
  >(DO_CREATE_GET_PRESIGNED_URL);

  const [loadProduitCanal, loadingProduitCanal] = useLazyQuery(GET_GESTION_PRODUIT_CANAL, {
    fetchPolicy: 'network-only',
  });

  const [updateProduitCanal, updatingProduitCanal] = useMutation<
    UPDATE_ONE_PRODUIT_CANALTypes,
    UPDATE_ONE_PRODUIT_CANALVariables
  >(UPDATE_ONE_PRODUIT_CANAL, {
    client: FEDERATION_CLIENT,
    onCompleted: data => {
      if (data && data.updateOneProduitCanal) {
        loadProduitCanal({ variables: { id: data.updateOneProduitCanal.id } });
      }
    },
    onError: error => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: 'Erreur lors de la modification du produit',
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const smyleys = getSmyleys && getSmyleys.data && getSmyleys.data.smyleys;
  const presignedUrls =
    doCreateGetPresignedUrlsResult &&
    doCreateGetPresignedUrlsResult.data &&
    doCreateGetPresignedUrlsResult.data.createGetPresignedUrls;

  useEffect(() => {
    const filePaths: string[] = [];
    if (smyleys) {
      smyleys.map(smyley => {
        if (smyley) filePaths.push(smyley.photo);
      });
      if (filePaths.length > 0) {
        // create presigned
        doCreateGetPresignedUrls({ variables: { filePaths } });
      }
    }
  }, [getSmyleys.data]);

  const [take, setTake] = useState<number>(12);
  const [skip, setSkip] = useState<number>(0);

  const zoomImage = (photo: string) => {
    setCurrentPhoto(photo);
    setOpenPhoto(true);
  };

  const [articleToDelete, setArticleToDelete] = useState<any>();

  const handleOpenConfirmDialog = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    article: any,
  ) => {
    e.stopPropagation();
    setArticleToDelete(article);
    setOpenDialog(true);
  };

  useEffect(() => {
    client.writeData({
      data: {
        checkedsProduit: null,
      },
    });
  }, [currentView]);

  const myPanierResult = useQuery<myPaniers, myPaniersVariables>(GET_MY_PANIERS, {
    variables: {
      idPharmacie:
        (myPharmacie &&
          myPharmacie.data &&
          myPharmacie.data.pharmacie &&
          myPharmacie.data.pharmacie.id) ||
        '',
      take: 10,
      skip: 0,
    },
    fetchPolicy: 'cache-and-network',
    skip: listResult && listResult.data && !listResult.data.search,
  });

  const familleResult = useQuery<SEARCH_INTERFACE, SEARCHVariables>(SEARCH, {
    variables: {
      type: ['famille'],
      skip: 0,
      take: 2000,
      idPharmacie: currentPharmacie && currentPharmacie.id,
    },
    fetchPolicy: 'cache-first',
  });

  const handleEdit = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    produitCanal: ProduitCanalInfo,
  ) => {
    e.stopPropagation();
    if (produitCanal.produit?.id) {
      push(`/gestion-produits/edit/${produitCanal.produit?.id}`);
      setOpenFormModal(true);
    }
  };

  const imageColumn = [
    {
      name: '',
      label: 'Photo',
      renderer: (value: any) => {
        return value &&
          value.produitPhoto &&
          value.produitPhoto.fichier &&
          value.produitPhoto.fichier.publicUrl ? (
          <img
            className={classes.image}
            onClick={() => zoomImage(value.produitPhoto.fichier)}
            src={value.produitPhoto.fichier.publicUrl}
          />
        ) : (
          <img className={classes.image} src={''} />
        );
      },
    },
  ];

  const controlColumns = [
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <>
            <Tooltip title="Modifier">
              <IconButton
                onClick={e => handleEdit(e, value)}
                color="secondary"
                disabled={!auth.isAuthorizedToEditProduct()}
              >
                <Edit />
              </IconButton>
            </Tooltip>
            <Tooltip title="Supprimer">
              <IconButton
                onClick={e => handleOpenConfirmDialog(e, value)}
                disabled={!auth.isAuthorizedToDeleteProduct()}
              >
                <Delete />
              </IconButton>
            </Tooltip>
          </>
        );
      },
    },
  ];

  // currentPanier
  const currentPanier: Panier | null =
    ({
      panierLignes:
        (myPanierResult &&
          myPanierResult.data &&
          myPanierResult.data.myPaniers &&
          myPanierResult.data.myPaniers
            .map(panier => panier && panier.panierLignes)
            .reduce((total, value) => {
              return total && total.concat(value);
            }, [])) ||
        [],
    } as Panier) || null;
  // change view mode : card or list

  const updateSkipAndTake = (skip: number, take: number) => {
    client.writeData({
      data: {
        skipAndTake: {
          skip,
          take,
          __typename: 'SkipAndTake',
        },
      },
    });
  };

  const skipAndTakeQuery = useQuery(GET_SKIP_AND_TAKE);

  const skipAndTakeResult =
    skipAndTakeQuery && skipAndTakeQuery.data && skipAndTakeQuery.data.skipAndTake;

  useEffect(() => {
    if (skipAndTakeResult) {
      setSkip(skipAndTakeResult.skip);
      setTake(skipAndTakeResult.take);
    }
  }, [skipAndTakeResult]);

  const changeView = () => {
    if (isInGestionProduits) {
      if (currentView === 'card') {
        push(`/gestion-produits/list`);
      } else {
        push(`/gestion-produits/card`);
      }
    } else if (idOperation) {
      const url = idRemise
        ? `/operation-produits/list/${idOperation}/${idRemise}`
        : `/operation-produits/card/${idOperation}`;
      if (currentView === 'card') {
        push(url);
      } else {
        push(url);
      }
    } else if (isInLaboratoireProduits) {
      const pathname = location.pathname;
      if (currentView === 'card') {
        push(pathname.replace('card', 'list'));
      } else {
        push(pathname.replace('list', 'card'));
      }
    } else {
      if (currentView === 'card') {
        push('/catalogue-produits/list');
      } else {
        push('/catalogue-produits/card');
      }
    }
  };

  const onTakeChanged = (value: number) => {
    setTake(value);
    // updateTake(value);
    updateSkipAndTake(0, value);
    setSkip(0);
    // updateSkip(0);
  };

  const onSkipChanged = (value: number) => {
    setSkip(value);
    updateSkipAndTake(value, take);
    // updateSkip(value);
  };

  const getQteMin = produit => {
    const operationArticle = operationArticles.find(
      operationArticle =>
        produit &&
        operationArticle &&
        operationArticle.produitCanal &&
        operationArticle.produitCanal.id === produit.id,
    );
    return (operationArticle && operationArticle.quantite) || 0;
  };

  const articleOCArray = useQuery<ArticleOCArray>(GET_ARTICLE_OC_ARRAY);

  useEffect(() => {
    const list: any = [];
    if (idOperation) {
      client.writeData({
        data: {
          selectedOC: { id: idOperation, __typename: 'operationId' },
        },
      });
    }
    operationArticles.map(ligne => {
      if (
        ligne &&
        ligne.quantite &&
        ligne.produitCanal &&
        ligne.quantite > 0 &&
        ligne.produitCanal.qteStock &&
        ligne.produitCanal.qteStock > 0
      ) {
        const article = {
          id: ligne.produitCanal.id,
          quantite: ligne.quantite,
          produitCanal: {
            id: ligne.produitCanal.id,
            prixPhv: ligne.produitCanal.prixPhv,
            remises: ligne.produitCanal.remises,
            __typename: 'remises',
          },
          __typename: 'ArticleOCInterface',
        };

        list.push(article);
      }
    });

    if (
      (articleOCArray && !articleOCArray.data) ||
      (articleOCArray &&
        articleOCArray.data &&
        articleOCArray.data.articleOcArray &&
        articleOCArray.data.articleOcArray.length === 0)
    ) {
      client.writeData({
        data: {
          articleOcArray: list,
        },
      });
    }
  }, [operationArticles]);

  useEffect(() => {
    return () => {
      resetSearchFilters(client);
      client.writeData({
        data: {
          checkedsProduit: null,
        },
      });
    };
  }, []);

  useEffect(() => {
    if (
      articleOCArray &&
      articleOCArray.data &&
      articleOCArray.data.articleOcArray &&
      articleOCArray.data.articleOcArray.length > 0
    ) {
      const panier = {
        panierLignes: articleOCArray.data.articleOcArray,
        __typename: 'OperationPanierLignes',
      };
      client.writeData({
        data: {
          operationPanier: panier,
        },
      });
      setParentPanier(panier);
    }
  }, [articleOCArray]);

  useEffect(() => {
    if (articleToDelete && articleToDelete.supprimer) setDateSupprimer(articleToDelete.supprimer);
  }, [articleToDelete]);

  const onChange = (date: any) => {
    setDateSupprimer(date);
  };

  const [doUpdateDateSupprimer, resultProduitDateSupprimer] = useMutation<
    updateProduitSupprimer,
    updateProduitSupprimerVariables
  >(UPDATE_PRODUIT_DATE_SUPPRIMER, {
    update: (cache, { data }) => {
      if (
        listResult &&
        listResult.variables &&
        data &&
        data.updateProduitSupprimer &&
        data.updateProduitSupprimer.id
      ) {
        const req = cache.readQuery<SEARCH_INTERFACE, SEARCHVariables>({
          query: SEARCH,
          variables: listResult.variables,
        });
        if (req && req.search && req.search.data) {
          cache.writeQuery({
            query: SEARCH,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.map((item: any) => {
                    if (
                      item &&
                      item.id &&
                      data.updateProduitSupprimer &&
                      data.updateProduitSupprimer.id === item.id
                    ) {
                      return {
                        ...item,
                        supprimer: data.updateProduitSupprimer.supprimer,
                      };
                    }

                    return item;
                  }),
                },
              },
            },
            variables: listResult && listResult.variables,
          });
        }
      }
    },
    onCompleted: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'SUCCESS',
        message: 'Article supprimé avec succès',
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const handleUpdateDateSupprimer = () => {
    // do modif
    const date: any = dateSupprimer && dateSupprimer !== null ? dateSupprimer : '';
    doUpdateDateSupprimer({
      variables: { id: articleToDelete.id, date },
    });

    setOpenDialog(false);
  };

  const handleCloseDialog = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.stopPropagation();
    setDateSupprimer(null);
    setOpenDialog(false);
  };

  if (listResult && listResult.data && !listResult.data.search) {
    return (
      <Box>
        <Box>
          {
            <Box className={classes.panierListe}>
              {listResult.data.map((item: any | null) => {
                const produit = item && item.article;
                return (
                  <Article
                    key={produit.id}
                    currentPanier={parentPanier}
                    currentPharmacie={currentPharmacie}
                    currentCanalArticle={produit}
                    presignedUrls={presignedUrls}
                    smyleys={smyleys}
                    qteMin={(item && item.quantite) || 0}
                  />
                );
              })}
            </Box>
          }
        </Box>
        <Box className={classNames(classes.flexRow, classes.justifyCenter)}>
          <Pagination
            take={currentTake || 12}
            skip={currentSkip || 0}
            onSkipChanged={onSkipChanged}
            onTakeChanged={onTakeChanged}
            total={(listResult && listResult.total) || 0}
          />
        </Box>
      </Box>
    );
  }

  const handleRequestEditProduit = (produit: any) => {
    const stv = produit.stv ? parseInt(produit.stv) : undefined;
    const prixPhv = produit.prixPhv
      ? parseFloat(parseFloat(produit.prixPhv).toFixed(2))
      : undefined;
    const unitePetitCond = produit.unitePetitCond ? parseInt(produit.unitePetitCond) : undefined;
    updateProduitCanal({
      variables: { input: { id: produit.id, update: { stv, prixPhv, unitePetitCond } } },
    });
  };

  return (
    <Box width="100%">
      <Box>
        {idOperation && (
          <CatalogueProduitOcInfo
            operation={operationResult && operationResult.data && operationResult.data.operation}
          />
        )}
        <AppBarFilter
          changeView={changeView}
          view={currentView}
          listResult={listResult}
          showGestionProduitsBtns={selectedProduits && selectedProduits.length > 0 ? true : false}
          selected={selectedProduits}
          setSelected={setSelectedProduits}
        />
        {(myPharmacie && myPharmacie.loading) ||
        (listResult && !listResult.data) ||
        (listResult && listResult.loading) ||
        (myPanierResult && myPanierResult.loading) ||
        (operationArticlesResult && operationArticlesResult.loading) ? (
          <Loader />
        ) : listResult.data?.search?.data?.length ? (
          currentView === 'card' ? (
            <Box className={idOperation ? classes.operationListe : classes.panierListe}>
              {listResult.data.search.data.map((produit: any | null) =>
                idOperation ? (
                  <Article
                    key={produit.id}
                    currentPanier={parentPanier}
                    currentPharmacie={currentPharmacie}
                    currentCanalArticle={produit}
                    presignedUrls={presignedUrls}
                    smyleys={smyleys}
                    qteMin={getQteMin(produit)}
                    familleResult={familleResult}
                  />
                ) : isInGestionProduits ? (
                  <ProduitCard
                    key={produit.produit.id}
                    currentPanier={currentPanier}
                    currentPharmacie={currentPharmacie}
                    currentCanalArticle={produit.produit}
                    presignedUrls={presignedUrls}
                    smyleys={smyleys}
                    listResult={listResult}
                    familleResult={familleResult}
                  />
                ) : (
                  <Article
                    key={produit.id}
                    currentPanier={currentPanier}
                    currentPharmacie={currentPharmacie}
                    currentCanalArticle={produit}
                    presignedUrls={presignedUrls}
                    smyleys={smyleys}
                    listResult={listResult}
                    familleResult={familleResult}
                  />
                ),
              )}
            </Box>
          ) : isInGestionProduits ? (
            <Box>
              <CustomContent
                selected={selectedProduits}
                setSelected={setSelectedProduits}
                listResult={listResult}
                isSelectable={true}
                columns={[...imageColumn, ...ProduitColumns, ...controlColumns]}
                hidePagination={true}
                unResetFilter={true}
                onRequestEdit={handleRequestEditProduit}
                mutationLoading={loadingProduitCanal.loading || updatingProduitCanal.loading}
              />
              <PhotoModal
                open={openPhoto}
                setOpen={setOpenPhoto}
                url={(currentPhoto && currentPhoto.publicUrl) || ''}
                libelle={(currentPhoto && currentPhoto.nomOriginal) || 'Image par défaut'}
              />
            </Box>
          ) : (
            <CustomContent
              listResult={listResult}
              isSelectable={false}
              columns={getColumns(operationArticles)}
              hidePagination={true}
              unResetFilter={true}
              onRequestEdit={handleRequestEditProduit}
              mutationLoading={loadingProduitCanal.loading || updatingProduitCanal.loading}
            />
          )
        ) : (
          <NoItemContentImage
            title={'Aucun produit trouvé selon vos critères de recherche'}
            subtitle={"Merci d'essayer d'autres filtres."}
          />
        )}
      </Box>
      {
        <Box className={classNames(classes.flexRow, classes.justifyCenter)}>
          <Pagination
            take={(listResult && listResult.variables && listResult.variables.take) || 12}
            skip={skip}
            onSkipChanged={onSkipChanged}
            onTakeChanged={onTakeChanged}
            total={
              listResult && listResult.data && listResult.data.search
                ? listResult.data.search.total
                : 0
            }
          />
        </Box>
      }
      <CustomModal
        open={openDialog}
        setOpen={setOpenDialog}
        title={`Modifier la date de suppression du produit`}
        withBtnsActions={false}
        closeIcon={false}
        headerWithBgColor={true}
        maxWidth={`xl`}
      >
        <Box>
          <Box style={{ margin: '15px 0px 22px 0px' }}>
            <CustomDatePicker
              label="Date suppression"
              placeholder="Date suppression"
              onChange={onChange}
              name="dateSupprimer"
              value={dateSupprimer}
              required={true}
            />
          </Box>
          <Box>
            <CustomButton
              color="default"
              onClick={handleCloseDialog}
              style={{ marginRight: '25px' }}
            >
              Annuler
            </CustomButton>
            <CustomButton
              color="secondary"
              onClick={handleUpdateDateSupprimer}
              disabled={!dateSupprimer}
            >
              Modifier
            </CustomButton>
          </Box>
        </Box>
      </CustomModal>
      <Footer />

      <CreationProduit
        openFormModal={openFormModal}
        setOpenFormModal={setOpenFormModal}
        refetch={listResult.refetch}
      />
    </Box>
  );
};

export default withRouter(Produit);
