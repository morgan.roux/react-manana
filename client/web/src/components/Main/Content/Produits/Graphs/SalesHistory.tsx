import React, { FC, useEffect, useState } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  Typography,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  IconButton,
  Box,
  TextField,
} from '@material-ui/core';
import { LineChart, Line, XAxis, YAxis, Tooltip, ResponsiveContainer } from 'recharts';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { useLazyQuery, useQuery } from '@apollo/react-hooks';
import { GET_SALES } from '../../../../../graphql/Sale';
import { SALES, SALESVariables } from '../../../../../graphql/Sale/types/SALES';
import { Loader } from '../../../../Dashboard/Content/Loader';
import moment from 'moment';
import classnames from 'classnames';
import CloseIcon from '@material-ui/icons/Close';
import CustomSelect from '../../../../Common/CustomSelect';
import { GET_PRODUIT_HISTORIQUE } from '../../../../../graphql/Produit/query';
import {
  PRODUIT_HISTORIQUE,
  PRODUIT_HISTORIQUEVariables,
  PRODUIT_HISTORIQUE_produitHistorique,
} from '../../../../../graphql/Produit/types/PRODUIT_HISTORIQUE';
import { ChangeEvent } from 'react';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    dialogTitle: {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
      '& h2': {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
      },
    },
    title: {
      color: theme.palette.primary.main,
      fontSize: 22,
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      letterSpacing: 0,
    },
    subTitle: {
      fontSize: 18,
      fontFamily: 'Roboto',
      color: '#616161',
    },
    chart: {
      width: '100%',
      height: 300,
    },
    lastDate: {
      fontWeight: 'bold',
    },
    navbar: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      background: theme.palette.common.black,
      color: theme.palette.common.white,
      padding: 16,
    },
    navbarTitle: {
      fontSize: 30,
      fontFamily: 'Roboto',
      fontWeight: 'bold',
    },
    closeIcon: {
      color: theme.palette.common.white,
    },
  }),
);

interface SalesHistoryProps {
  open: boolean;
  onClose: (e: any) => void;
  sendColor: (color: string) => void;
  produit: any;
  pharmacie: any;
}

const SalesHistory: FC<SalesHistoryProps> = ({ open, onClose, sendColor, produit, pharmacie }) => {
  const classes = useStyles({});

  const [annee, setAnnee] = useState<number>(moment().year());

  const [getHistoriques, { data: historique, loading }] = useLazyQuery<
    PRODUIT_HISTORIQUE,
    PRODUIT_HISTORIQUEVariables
  >(GET_PRODUIT_HISTORIQUE);

  const [chartData, setChartData] = useState<any[]>();

  useEffect(() => {
    if (pharmacie && produit)
      getHistoriques({
        variables: {
          idPharmacie: pharmacie && pharmacie.id,
          idProduit: produit && produit.id,
          year: +moment().year(),
        },
      });
  }, [produit, pharmacie]);

  // Format chart data for Rechart.js
  useEffect(() => {
    if (
      historique &&
      historique.produitHistorique &&
      historique.produitHistorique.data &&
      historique.produitHistorique.data.length &&
      annee
    ) {
      const data = historique.produitHistorique.data.filter(
        data => data && data.date && +data.date.split('-')[0] === annee,
      );
      setChartData(data);
    }
  }, [historique, annee]);

  /* if (data && data.sales && !chartData) {
    setChartData(data.sales as any[]);
  } */

  // Set chart line color
  let chartColor = '#F6D945';
  const month1 =
    (historique &&
      historique.produitHistorique &&
      historique.produitHistorique.data &&
      historique.produitHistorique.data.length >= 3 &&
      historique.produitHistorique.data[historique.produitHistorique.data.length - 1]) ||
    null;
  const month2 =
    (historique &&
      historique.produitHistorique &&
      historique.produitHistorique.data &&
      historique.produitHistorique.data.length >= 3 &&
      historique.produitHistorique.data[historique.produitHistorique.data.length - 2]) ||
    null;
  const month3 =
    (historique &&
      historique.produitHistorique &&
      historique.produitHistorique.data &&
      historique.produitHistorique.data.length >= 3 &&
      historique.produitHistorique.data[historique.produitHistorique.data.length - 3]) ||
    null;

  if (month1 && month1.value && month2 && month2.value && month3 && month3.value) {
    const month1Value = month1.value;
    const month2Value = month2.value;
    const month3Value = month3.value;
    // check difference
    if (month1Value > month2Value && month2Value > month3Value) chartColor = '#8CC63F';
    if (month1Value < month2Value && month2Value < month3Value) chartColor = '#CC154E';
  }

  sendColor(chartColor);

  const annees = [{ annee: moment().year() }, { annee: moment().year() - 1 }];

  const _xAxisDateFormatter = (value: string) => {
    return moment(value).format('MMM');
  };

  const _yAxisDateFormatter = (value: string) => {
    console.log('log ', value);
    return value;
  };

  const _tooltipDateFormatter = (value: string | number) => {
    return moment(value).format('MMMM YYYY');
  };
  const handleChange = (e: ChangeEvent<any>) => {
    const { name, value } = e.target ? e.target : e;
    setAnnee(+value);
  };

  if (loading) return <Loader />;

  return (
    <Dialog
      aria-labelledby="simple-dialog-title"
      maxWidth="md"
      fullWidth={true}
      open={open}
      onClose={onClose}
    >
      <div className={classes.navbar}>
        <Typography className={classes.navbarTitle}>Historique</Typography>
        <div>
          {onClose ? (
            <IconButton aria-label="close" className={classes.closeIcon} onClick={onClose}>
              <CloseIcon />
            </IconButton>
          ) : null}
        </div>
      </div>
      <DialogTitle id="simple-dialog-title" className={classes.dialogTitle}>
        <div>
          <Typography className={classes.title}>{(produit && produit.libelle) || ''}</Typography>
        </div>
      </DialogTitle>
      <Box
        display="flex"
        width="100%"
        justifyContent="space-between"
        paddingLeft="24px"
        paddingRight="24px"
      >
        <Typography className={classnames(classes.title, classes.subTitle)}>
          Historique des ventes
          <span className={classes.lastDate}>{` au ${(historique &&
            historique.produitHistorique &&
            historique.produitHistorique.dateHistorique &&
            moment(historique.produitHistorique.dateHistorique).format(' DD MMMM YYYY')) ||
            ''}`}</span>
        </Typography>
        <Box width="155px">
          <CustomSelect
            list={annees}
            value={annee}
            index={'annee'}
            listId={'annee'}
            label={"Sélectionner l'année"}
            onClick={handleChange}
          />
        </Box>
      </Box>
      <DialogContent>
        <div className={classes.chart}>
          {chartData && chartData.length ? (
            <ResponsiveContainer>
              <LineChart
                data={
                  (chartData && chartData.length && chartData.sort((a, b) => a.order - b.order)) ||
                  []
                }
                margin={{ top: 50, bottom: 20, right: 20 }}
              >
                <XAxis
                  type="category"
                  dataKey="date"
                  tickFormatter={value => _xAxisDateFormatter(value)}
                />
                <YAxis
                  type="number"
                  dataKey="value"
                  tickFormatter={value => _yAxisDateFormatter(value)}
                />
                <Tooltip labelFormatter={value => _tooltipDateFormatter(value)} />
                <Line
                  type="monotone"
                  dataKey="value"
                  stroke={chartColor}
                  activeDot={{
                    r: 5,
                  }}
                />
              </LineChart>
            </ResponsiveContainer>
          ) : (
            <p>Aucune historique</p>
          )}
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default SalesHistory;
