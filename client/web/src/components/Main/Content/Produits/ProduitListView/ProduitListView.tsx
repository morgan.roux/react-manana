import React, { FC } from 'react';
import useStyles from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import ButtonAddToCard from '../../Panier/Article/Button/Button';
import { Panier } from '../../../../../graphql/Panier/types/Panier';
import RemiseCell from './RemiseCell';
import Quantite from '../../Panier/Article/Quantite/Quantite';
import { Column } from '../../../../Dashboard/Content/Interface';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { useQuery } from '@apollo/react-hooks';
import SortInterface from '../../../../../Interface/SortInterface';
import GET_LOCAL_SORT from '../../../../../localStates/SortLocal';
import ArticleHistorique from '../../Panier/Article/Historique/Historique';
import BlockTwoToneIcon from '@material-ui/icons/BlockTwoTone';
import { Box } from '@material-ui/core';
import CustomContent from '../../../../Common/CustomContent';
import { PharmacieMinimInfo } from '../../../../../graphql/Pharmacie/types/PharmacieMinimInfo';
interface ProduitListViewProps {
  produitList: any;
  currentPanier: Panier | null;
  currentPharmacie: PharmacieMinimInfo | null;
  operationArticles?: any;
  hideRightPagination?: boolean;
}

export const getColumns = (operationArticles?: any) => {
  const getQteMin = produit => {
    const operationArticle =
      operationArticles &&
      operationArticles.find(
        operationArticle =>
          produit &&
          operationArticle &&
          operationArticle.produitCanal &&
          operationArticle.produitCanal.id === produit.id,
      );
    return (operationArticle && operationArticle.quantite) || 0;
  };

  return [
    {
      name: 'produit.produitCode.code',
      label: 'Code',
      renderer: (value: any) => {
        return (
          (value && value.produit && value.produit.produitCode && value.produit.produitCode.code) ||
          '-'
        );
      },
    },
    {
      name: 'produit.libelle',
      label: 'Libellé',
      renderer: (value: any) => {
        return (value && value.produit && value.produit.libelle) || '-';
      },
    },
    {
      name: 'produit.produitTechReg.laboExploitant.nomLabo',
      label: 'Labo',
      renderer: (value: any) => {
        return (
          (value &&
            value.produit &&
            value.produit.produitTechReg &&
            value.produit.produitTechReg.laboExploitant &&
            value.produit.produitTechReg.laboExploitant.nomLabo) ||
          '-'
        );
      },
    },
    {
      name: 'stv',
      label: 'STV',
      editable: true,
      sortable: true,
    },
    {
      name: 'unitePetitCond',
      label: 'Nbr/Carton',
      editable: true,
      renderer: (value: any) => {
        console.log('*************************unitePetitCond value', value);
        return value?.unitePetitCond || '-';
      },
    },
    {
      name: '',
      label: 'Stock plateforme',
      renderer: (value: any) => {
        if (value.qteStock > 0) {
          return <CheckCircleIcon />;
        } else {
          return <BlockTwoToneIcon color="secondary" />;
        }
      },
      centered: true,
    },
    {
      name: '',
      label: 'Quantité commandée',
      renderer: (value: any) =>
        operationArticles ? (
          <Quantite
            currentCanalArticle={value ? value : null}
            qteMin={getQteMin(value)}
            acceptZero={true}
          />
        ) : (
          <Quantite currentCanalArticle={value ? value : null} acceptZero={true} />
        ),
      centered: true,
    },
    {
      name: '',
      label: 'Remises',
      renderer: (value: any) => {
        return <RemiseCell currentCanalArticle={value ? value : null} type="remise" />;
      },
      centered: true,
    },
    {
      name: 'prixPhv',
      label: 'Prix Achat',
      editable: true,
      renderer: (value: any) => {
        return value.prixPhv ? value.prixPhv + '€' : '-';
      },
      centered: true,
    },
    {
      name: '',
      label: 'Prix net',
      renderer: (value: any) => {
        return <RemiseCell currentCanalArticle={value ? value : null} type="prixNet" />;
      },
      centered: true,
    },
    {
      name: '',
      label: 'Total net',
      renderer: (value: any) => {
        return <RemiseCell currentCanalArticle={value ? value : null} type="totalNet" />;
      },
      centered: true,
    },
    {
      name: '',
      label: 'Unités gratuites',
      renderer: (value: any) => {
        return <RemiseCell currentCanalArticle={value ? value : null} type="uG" />;
      },
      centered: true,
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <Box display="flex" alignItems="center">
            <ArticleHistorique view="table" produit={value && value.produit} />
            <ButtonAddToCard view="table" currentCanalArticle={value} />
          </Box>
        );
      },
    },
  ];
};

const ProduitListView: FC<ProduitListViewProps & RouteComponentProps<any, any, any>> = ({
  produitList,
  currentPanier,
  currentPharmacie,
  hideRightPagination,
  operationArticles,
  history,
}) => {
  const classes = useStyles({});
  const localSort = useQuery<SortInterface>(GET_LOCAL_SORT);
  const pathname = history.location.pathname;
  const getQteMin = produit => {
    const operationArticle =
      operationArticles &&
      operationArticles.find(
        operationArticle =>
          produit &&
          operationArticle &&
          operationArticle.article &&
          operationArticle.article.id === produit.id,
      );
    return (operationArticle && operationArticle.quantite) || 0;
  };

  const goToDetails = (produit: any) => {
    if (produit && produit.id) {
      history.push(`/catalogue-produits/card/${produit.id}`, { from: history.location.pathname });
    }
  };

  const columns: Column[] = [
    {
      name: 'codeReference',
      label: 'Code',
    },
    {
      name: 'nomCanalArticle',
      label: 'Libellé',
    },
    {
      name: 'laboratoire.nomLabo',
      label: 'Labo',
      sortable: true,
      renderer: (value: any) => {
        return value && value.laboratoire && value.laboratoire.nomLabo
          ? value.laboratoire.nomLabo
          : '-';
      },
    },
    {
      name: 'stv',
      label: 'STV',
      sortable: true,
    },
    {
      name: 'unitePetitCond',
      label: 'Carton',
      centered: true,
    },
    {
      name: '',
      label: 'Stock plateforme',
      renderer: (value: any) => {
        if (value.qteStock > 0) {
          return <CheckCircleIcon />;
        } else {
          return <BlockTwoToneIcon color="secondary" />;
        }
      },
      centered: true,
    },
    {
      name: 'qteStock',
      label: 'Stock pharmacie',
      centered: true,
    },
    {
      name: '',
      label: 'Quantité commandée',
      renderer: (value: any) =>
        operationArticles ? (
          <Quantite
            currentCanalArticle={value ? value : null}
            qteMin={getQteMin(value)}
            acceptZero={true}
          />
        ) : (
          <Quantite currentCanalArticle={value ? value : null} acceptZero={true} />
        ),
      centered: true,
    },
    {
      name: '',
      label: 'Remises',
      renderer: (value: any) => {
        return <RemiseCell currentCanalArticle={value ? value : null} type="remise" />;
      },
      centered: true,
    },
    {
      name: '',
      label: 'Tarif HT',
      renderer: (value: any) => {
        return value.prixPhv ? value.prixPhv + '€' : '-';
      },
      centered: true,
    },
    {
      name: '',
      label: 'Prix net',
      renderer: (value: any) => {
        return <RemiseCell currentCanalArticle={value ? value : null} type="prixNet" />;
      },
      centered: true,
    },
    {
      name: '',
      label: 'Total net',
      renderer: (value: any) => {
        return <RemiseCell currentCanalArticle={value ? value : null} type="totalNet" />;
      },
      centered: true,
    },
    {
      name: '',
      label: 'Unités gratuites',
      renderer: (value: any) => {
        return <RemiseCell currentCanalArticle={value ? value : null} type="uG" />;
      },
      centered: true,
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <Box display="flex" alignItems="center">
            <ArticleHistorique
              view="table"
              produit={value && value.produit}
              pharmacie={currentPharmacie}
            />
            <ButtonAddToCard view="table" currentCanalArticle={value} />
          </Box>
        );
      },
    },
  ];

  return (
    <div
      className={
        pathname.startsWith('/operation-produits')
          ? classes.ocProduit
          : pathname.startsWith('/catalogue-produits')
          ? classes.catalogueProduits
          : classes.root
      }
    >
      {/* <Content
        searchPlaceholder="Rechercher un produit"
        type="produitList"
        datas={produitList}
        columns={columns}
        localSort={localSort}
        hideRightPagination={hideRightPagination}
      /> */}
      <CustomContent
        // sortBy={[{ 'produit.libelle': { order: 'asc' } }]}
        searchPlaceholder={`Rechercher`}
        type="produitcanal"
        columns={columns as any}
        enableGlobalSearch={true}
        inSelectContainer={false}
        searchInFullWidth={true}
        noPrefixIcon={true}
        paginationCentered={true}
        datas={produitList}
        fieldsOptions={{
          exclude: ['pharmacieRemisePanachees', 'pharmacieRemisePaliers', 'sousGammeCommercial'],
        }}
        showResetFilters={true}
        hideSearchBar={true}
        onClickRow={goToDetails}
      />
    </div>
  );
};
export default withRouter(ProduitListView);
