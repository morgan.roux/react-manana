import React, { FC } from 'react';
import useStyles from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import classnames from 'classnames';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CloseCircleIcon from '@material-ui/icons/Cancel';
interface BodyCellProps {
  value: string;
  pink: boolean;
  libelle?: boolean;
  tarif?: boolean;
  underlined?: boolean;
  plateforme?: boolean;
}
const BodyCell: FC<BodyCellProps & RouteComponentProps<any, any, any>> = ({
  value,
  pink,
  libelle,
  tarif,
  underlined,
  plateforme,
}) => {
  const classes = useStyles({});
  if (libelle) {
    return (
      <td className={classnames(classes.panierListeItemDescription, classes.textCenter)}>
        {value}
      </td>
    );
  }
  if (plateforme && parseInt(value) > 0) {
    return (
      <td className={classes.textCenter}>
        <CheckCircleIcon className={classes.green} />
      </td>
    );
  }
  if (plateforme && parseInt(value) === 0) {
    return (
      <td className={classes.textCenter}>
        <CloseCircleIcon className={classes.pink} />
      </td>
    );
  }
  return (
    <td
      className={classnames(
        tarif ? classes.MontserratBold : classes.MontserratRegular,
        classes.medium,
        pink ? classes.pink : classes.colorDefault,
        underlined ? classes.underlined : '',
        classes.textCenter,
      )}
    >
      {value}
    </td>
  );
};
export default withRouter(BodyCell);
