import React, { FC } from 'react';
import useStyles from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';

interface HeadCellProps {
  value: string;
}
const HeadCell: FC<HeadCellProps & RouteComponentProps<any, any, any>> = ({ value }) => {
  const classes = useStyles({});
  return <th className={classes.headCell}>{value}</th>;
};
export default withRouter(HeadCell);
