import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    MontserratRegular: {
      fontFamily: 'Montserrat',
      fontWeight: 400,
    },
    pink: {
      // color: theme.palette.pink.main,
    },
    small: {
      // fontSize: theme.typography.small.fontSize,
    },
    gray: {
      // color: theme.palette.gray.main,
    },
    medium: {
      // fontSize: theme.typography.medium.fontSize,
    },
    colorDefault: {
      // color: theme.palette.default.main,
    },
    green: {
      // color: theme.palette.green.main,
    },
    panierListeItemDescription: {
      textAlign: 'center',
      fontFamily: 'Montserrat',
      fontSize: '12px',
      fontWeight: 600,
      letterSpacing: 0,
      color: '#004354',
      opacity: 1,
      marginTop: '10px',
      marginBottom: '14px',
    },
    MontserratBold: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
    },
    underlined: {
      textDecoration: 'underline',
    },
    textCenter: {
      textAlign: 'center',
    },
  }),
);

export default useStyles;
