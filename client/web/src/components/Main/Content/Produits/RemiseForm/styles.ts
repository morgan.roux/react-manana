import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    rowContainer: {
      display: 'flex',
      '& .MuiFormControl-root:nth-child(1)': {
        marginRight: 16,
      },
    },
    columnContainer: {
      display: 'flex',
      flexDirection: 'column',
      padding: '25px 150px',
      '& .MuiFormControl-root:nth-child(1)': {
        marginBottom: 16,
      },
    },
  }),
);

export default useStyles;
