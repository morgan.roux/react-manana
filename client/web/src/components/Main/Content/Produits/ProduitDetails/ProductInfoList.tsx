import React, { FC, useState } from 'react';
import { List, ListItem, ListItemText, makeStyles, Theme, createStyles } from '@material-ui/core';
import CheckIcon from '@material-ui/icons/CheckCircle';
import CloseIcon from '@material-ui/icons/Cancel';
import { PRODUIT_CANAL_produitCanal } from '../../../../../graphql/ProduitCanal/types/PRODUIT_CANAL';
import SalesHistory from '../Graphs';
import ICurrentPharmacieInterface from '../../../../../Interface/CurrentPharmacieInterface';
import { useQuery } from '@apollo/react-hooks';
import { GET_CURRENT_PHARMACIE } from '../../../../../graphql/Pharmacie/local';
import { useEffect } from 'react';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    aligRigtn: {
      textAlign: 'right',
    },
    checkIcon: {
      color: '#8CC63F',
    },
    closeIcon: {
      color: '#F11957',
    },
  }),
);

interface ProductInfoListProps {
  produit: PRODUIT_CANAL_produitCanal;
}

const ProductInfoList: FC<ProductInfoListProps> = ({ produit }) => {
  const classes = useStyles({});
  const [{ openSalesHistory }, setOpenSalesHistory] = React.useState({ openSalesHistory: false });
  const [currentPharmacie, setCurrentPharmacie] = useState<any>();
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

  const toggleSalesHistory = () => {
    setOpenSalesHistory(prevState => ({
      ...prevState,
      openSalesHistory: !prevState.openSalesHistory,
    }));
  };

  useEffect(() => {
    if (myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie) {
      setCurrentPharmacie(myPharmacie.data.pharmacie);
    }
  }, [myPharmacie]);

  const data = [
    {
      value:
        produit &&
        produit.produit &&
        produit.produit.produitCode &&
        produit.produit.produitCode.code
          ? produit.produit.produitCode.code
          : '',
      label: 'Code',
    },
    {
      value:
        produit &&
        produit.produit &&
        produit.produit.produitTechReg &&
        produit.produit.produitTechReg.laboExploitant &&
        produit.produit.produitTechReg.laboExploitant.id
          ? produit.produit.produitTechReg.laboExploitant.id
          : '',
      label: 'Laboratoire',
    },
    {
      value: produit.stv,
      label: 'STV',
    },
    // TODO
    {
      value: 'xx',
      label: 'Carton',
    },
    // TODO
    {
      value: 'xx',
      label: 'Quantité',
    },
    {
      value: 'xx',
      label: 'NPFM',
    },
    {
      value: produit.prixPhv + '€',
      label: 'Tarif HT',
    },

    // TODO
    {
      value: 'x% à partir du x achetés',
      label: 'Remise',
    },
    {
      value: produit.prixPhv + '€',
      label: 'Prix Net',
    },
    {
      value: produit.prixPhv + '€',
      label: 'Total Net',
    },
    {
      value: produit.qteStock,
      label: 'Stock plateforme',
      code: 'STK_PL',
    },
    {
      value: produit.qteStock,
      label: 'Stock Pharmacie',
      code: 'STK_PH',
    },
    {
      value: '',
      label: 'Historique des ventes',
      code: 'HDV',
    },
  ];

  const handleSendColor = (color: string) => {
    console.log('color : ', color);
  };
  return (
    <>
      <List dense={true}>
        {data.map((item, key) => (
          <ListItem key={key}>
            <ListItemText
              onClick={item.code && item.code === 'HDV' ? toggleSalesHistory : () => {}}
            >
              {item.label}
            </ListItemText>
            {item.code && item.code !== 'HDV' ? (
              <ListItemText className={classes.aligRigtn}>
                {item.value === 0 ? (
                  <CloseIcon className={classes.closeIcon} />
                ) : (
                  <CheckIcon className={classes.checkIcon} />
                )}
              </ListItemText>
            ) : (
              <ListItemText className={classes.aligRigtn}>{item.value}</ListItemText>
            )}
          </ListItem>
        ))}
      </List>

      <SalesHistory
        open={openSalesHistory}
        onClose={toggleSalesHistory}
        sendColor={handleSendColor}
        produit={produit}
        pharmacie={currentPharmacie}
      />
    </>
  );
};

export default ProductInfoList;
