import React, { FC, useEffect, useState, useContext } from 'react';
import { Box, IconButton } from '@material-ui/core';
import { useStyles } from './styles';
import { Comment } from '../../../../Comment';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { GET_PRODUIT_CANAL } from '../../../../../graphql/ProduitCanal';
import {
  PRODUIT_CANAL,
  PRODUIT_CANALVariables,
} from '../../../../../graphql/ProduitCanal/types/PRODUIT_CANAL';
import { Loader } from '../../../../Dashboard/Content/Loader';
import UserAction from '../../../../Common/UserAction';
import CommentList from '../../../../Comment/CommentList';
import ICurrentPharmacieInterface from '../../../../../Interface/CurrentPharmacieInterface';
import { GET_CURRENT_PHARMACIE } from '../../../../../graphql/Pharmacie/local';
import { setPharmacie, getGroupement } from '../../../../../services/LocalStorage';
import ArticleListBox from '../../Panier/Article/ListBox/ListBox';
import { MY_PHARMACIE_me_pharmacie } from '../../../../../graphql/Pharmacie/types/MY_PHARMACIE';
import ArticleButton from '../../Panier/Article/Button/Button';
import ArticleHistorique from '../../Panier/Article/Historique/Historique';
import { GET_SMYLEYS } from '../../../../../graphql/Smyley';
import { SMYLEYSVariables, SMYLEYS } from '../../../../../graphql/Smyley/types/SMYLEYS';
import {
  CREATE_GET_PRESIGNED_URL,
  CREATE_GET_PRESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_GET_PRESIGNED_URL';
import { DO_CREATE_GET_PRESIGNED_URL } from '../../../../../graphql/S3';
import { GET_MY_PANIER } from '../../../../../graphql/Panier/query';
import {
  myPanierVariables,
  myPanier,
  myPanier_myPanier,
} from '../../../../../graphql/Panier/types/myPanier';
import { TarifHTComponent, TooltipContent } from '../../Panier/Article/Article';
import TooltipComponent from '../../Panier/Tooltip/Tooltip';
import InfoContainer from '../../Panier/Article/InfoContainer';
import { ChevronRight, ChevronLeft } from '@material-ui/icons';
import ChatBubbleIcon from '@material-ui/icons/ChatBubble';
import { CustomFullScreenModal } from '../../../../Common/CustomModal';
import CreationProduit from '../../../../Dashboard/GestionProduits/CreationProduit';
import CustomButton from '../../../../Common/CustomButton';

interface ProduitDetailsProps {
  match?: {
    params: { id: string };
  };
  idProduct?: string;
}

const ProduitDetails: FC<RouteComponentProps<any, any, any> & ProduitDetailsProps> = ({
  match,
  idProduct,
}) => {
  const classes = useStyles({});
  const id = match && match.params && match.params.id;
  const [openFormModal, setOpenFormModal] = useState(false);
  const [produitComments, setProduitComments] = useState<any>();

  // take currentPharmacie
  const [currentPharmacie, setCurrentPharmacie] = useState<MY_PHARMACIE_me_pharmacie | null>(null);
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

  // const handleOpenUpdateProduit = (isOpen: boolean) => {
  //   setOpenUpdateProduit(isOpen);
  //   if (!isOpen) refetch();
  // };

  useEffect(() => {
    if (myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie) {
      setCurrentPharmacie(myPharmacie.data.pharmacie);
      setPharmacie(myPharmacie.data.pharmacie);
    }
  }, [myPharmacie]);

  const { data, loading, fetchMore, refetch } = useQuery<PRODUIT_CANAL, PRODUIT_CANALVariables>(
    GET_PRODUIT_CANAL,
    {
      variables: {
        id: id || idProduct,
        idPharmacie: currentPharmacie ? currentPharmacie.id : '',
        take: 10,
        skip: 0,
      },
      skip: !currentPharmacie || (currentPharmacie && !currentPharmacie.id) ? true : false,
      fetchPolicy: 'cache-and-network',
    },
  );
  const produitCanal = data ? data.produitCanal : null;
  const produitPhoto =
    data &&
    data.produitCanal &&
    data.produitCanal.produit &&
    data.produitCanal.produit.produitPhoto &&
    data.produitCanal.produit.produitPhoto.fichier;

  // take currentPanier
  const [currentPanier, setCurrentPanier] = useState<myPanier_myPanier | null>(null);

  const myPanier = useQuery<myPanier, myPanierVariables>(GET_MY_PANIER, {
    variables: {
      idPharmacie: (currentPharmacie && currentPharmacie.id) || '',
      codeCanal: produitCanal && produitCanal.commandeCanal && produitCanal.commandeCanal.code,
      take: 10,
      skip: 0,
    },
    skip:
      !produitCanal || !currentPharmacie || (currentPharmacie && !currentPharmacie.id)
        ? true
        : false,
  });

  useEffect(() => {
    if (myPanier && myPanier.data && myPanier.data.myPanier) {
      setCurrentPanier(myPanier.data.myPanier);
    }
  }, [myPanier]);

  const fetchMoreComments = () => {
    if (produitCanal && produitCanal.comments && produitCanal.comments.data) {
      fetchMore({
        variables: { id: id || idProduct, skip: +produitCanal.comments.data.length, take: 10 },
        updateQuery: (prev: any, { fetchMoreResult }: any) => {
          if (!fetchMoreResult) return prev;
          const next = {
            ...prev,
            canalArticle: {
              ...prev.canalArticle,
              comments: {
                ...((prev && prev.canalArticle && prev.canalArticle.comments) || {}),
                total:
                  (prev &&
                    prev.canalArticle &&
                    prev.canalArticle.comments &&
                    prev.canalArticle.comments.total) ||
                  0,
                data: [
                  ...((fetchMoreResult &&
                    fetchMoreResult.canalArticle &&
                    fetchMoreResult.canalArticle.comments &&
                    fetchMoreResult.canalArticle.comments.data) ||
                    []),
                  ...((prev &&
                    prev.canalArticle &&
                    prev.canalArticle.comments &&
                    prev.canalArticle.comments.data) ||
                    []),
                ],
              },
            },
          };

          return next as any;
        },
      });
    }
  };

  const nbSmyley =
    (produitCanal && produitCanal.userSmyleys && produitCanal.userSmyleys.total) || 0;

  const groupement = getGroupement();
  // Get smyleys by groupement
  const getSmyleys = useQuery<SMYLEYS, SMYLEYSVariables>(GET_SMYLEYS, {
    variables: {
      idGroupement: groupement && groupement.id,
    },
  });

  const [doCreateGetPresignedUrls, doCreateGetPresignedUrlsResult] = useMutation<
    CREATE_GET_PRESIGNED_URL,
    CREATE_GET_PRESIGNED_URLVariables
  >(DO_CREATE_GET_PRESIGNED_URL);

  const smyleys = getSmyleys && getSmyleys.data && getSmyleys.data.smyleys;
  const presignedUrls =
    doCreateGetPresignedUrlsResult &&
    doCreateGetPresignedUrlsResult.data &&
    doCreateGetPresignedUrlsResult.data.createGetPresignedUrls;

  useEffect(() => {
    const filePaths: string[] = [];
    if (smyleys) {
      smyleys.map((smyley: any) => {
        if (smyley) filePaths.push(smyley.photo);
      });
      if (filePaths.length > 0) {
        // create presigned
        doCreateGetPresignedUrls({ variables: { filePaths } });
      }
    }
  }, [getSmyleys.data]);

  useEffect(() => {
    if (produitCanal && produitCanal.comments) {
      setProduitComments(produitCanal.comments);
    }
  }, [produitCanal]);

  if (loading && !data) return <Loader />;
  // if (!produit) return <div>Aucun produit corespondant à l'id</div>;

  return produitCanal && produitCanal.produit ? (
    <Box className={classes.root}>
      {/* Product Title */}
      {/* Btn Preview, Image, description and Btn Next */}
      <Box className={classes.row}>
        <IconButton className={classes.btnPrevNext} disabled={true}>
          <ChevronLeft />
        </IconButton>
        <Box className={classes.contentDetailsProduits}>
          <Box display="flex" flexDirection="column" marginBottom="24px">
            <TarifHTComponent currentCanalArticle={produitCanal} />
            <TooltipComponent
              titleLabel={produitCanal.produit.libelle || ''}
              famille={
                (produitCanal.produit &&
                  produitCanal.produit.famille &&
                  produitCanal.produit.famille.libelleFamille) ||
                ''
              }
              component={<TooltipContent data={produitCanal || null} />}
            />
            <InfoContainer canalArticle={produitCanal} column={true} />
            <ArticleHistorique
              produit={produitCanal && produitCanal.produit}
              pharmacie={currentPharmacie}
            />
            <CustomButton
              //onClick={() => setOpenUpdateProduit(true)}
              style={{ margin: '15px 0 0 0' }}
            >
              Voir Fiche produit
            </CustomButton>
          </Box>
          <Box className={classes.imageAndBtnAddToCartContainer}>
            {produitPhoto && produitPhoto.urlPresigned && (
              <img src={produitPhoto.urlPresigned} alt="" />
            )}
          </Box>
          <Box className={classes.detailsPrix}>
            <ArticleListBox
              currentPanier={currentPanier}
              currentCanalArticle={produitCanal}
              currentPharmacie={currentPharmacie}
            />
            <Box
              className={classes.fullWidthButton}
              marginTop="16px"
              display="flex"
              justifyContent="center"
            >
              <ArticleButton currentCanalArticle={produitCanal} />
            </Box>
          </Box>
        </Box>

        <IconButton className={classes.btnPrevNext} disabled={true}>
          <ChevronRight />
        </IconButton>
      </Box>
      <Box display="flex" width="100%" paddingLeft="180px" paddingRight="180px" marginTop="64px">
        <hr className={classes.hr} />
      </Box>
      <Box className={classes.userActionSection}>
        {/* Emoji, comment and share btn */}
        <UserAction
          codeItem="PROD"
          idSource={produitCanal.id}
          nbComment={produitCanal.comments ? produitCanal.comments.total : 0}
          nbSmyley={nbSmyley}
          idArticle={produitCanal.id}
          userSmyleys={(produitCanal && produitCanal.userSmyleys) || undefined}
          presignedUrls={presignedUrls}
          smyleys={smyleys}
        />

        {/* Comment section*/}
        <Box className={classes.commentSection}>
          {produitComments && (
            <CommentList
              comments={produitComments}
              fetchMoreComments={fetchMoreComments}
              loading={loading}
            />
          )}
          <Comment codeItem="PROD" idSource={produitCanal.id} refetch={refetch} />
          <Box display="flex" justifyContent="center" alignItems="center" marginTop="16px">
            <ChatBubbleIcon className={classes.panierListeSocialIcon} />
            <Box marginLeft="8px">
              {(produitCanal && produitCanal.comments && produitCanal.comments.total) || 0}{' '}
              {produitCanal && produitCanal.comments && produitCanal.comments.total > 1
                ? 'Commentaires'
                : 'Commentaire'}
            </Box>
          </Box>
        </Box>
      </Box>
      <CreationProduit
        openFormModal={openFormModal}
        setOpenFormModal={setOpenFormModal}
        refetch={null}
      />
    </Box>
  ) : (
    <div className={classes.root}></div>
  );
};

export default withRouter(ProduitDetails);
