import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Checkbox,
  List,
  ListItem,
  ListItemText,
  Radio,
  Typography,
} from '@material-ui/core';
import Box from '@material-ui/core/Box';
import { ExpandMore } from '@material-ui/icons';
import React, { FC, useEffect, useState } from 'react';
import useStyles from './styles';

interface DropdownComponentProps {
  title: string;
  list: any[];
  idsActiveItem: string[];
  handleChange: (event: any, item: any) => void;
  name: string;
  useRadio?: boolean;
  itemWidth?: string;
}

export const canals = [
  {
    id: '1',
    libelle: 'Grossiste',
    code: 'GROSSISTE',
  },
  {
    id: '2',
    libelle: 'Direct Labo',
    code: 'DIRECT',
  },
  {
    id: '3',
    libelle: 'Plateforme',
    code: 'PFL',
  },
];

export const reactions = [
  {
    id: '1',
    libelle: 'Commentaire',
    name: 0,
  },
  {
    id: '2',
    libelle: 'Smyleys',
    name: 1,
  },
];

export const remises = [
  {
    id: '1',
    libelle: 'Remise par palier',
    name: 0,
  },
  {
    id: '2',
    libelle: 'Remise panachée',
    name: 1,
  },
];

export const tris = [
  {
    id: '1',
    libelle: 'Code Référence',
    name: 'produit.produiCode.code',
  },
  {
    id: '2',
    libelle: 'Libellé',
    name: 'produit.libelle',
  },
  {
    id: '3',
    libelle: 'Laboratoire',
    name: 'laboratoire.nomLabo',
  },
  {
    id: '4',
    libelle: 'Stv',
    name: 'stv',
  },
  {
    id: '5',
    libelle: 'Tarif HT',
    name: 'prixPhv',
  },
  {
    id: '6',
    libelle: 'Stock plateforme',
    name: 'qteStock',
  },
];

export const DropdownComponent: FC<DropdownComponentProps> = ({
  title,
  list,
  handleChange,
  idsActiveItem,
  name,
  useRadio,
  itemWidth,
}) => {
  const classes = useStyles({});

  return (
    <Box className={classes.root}>
      <Accordion defaultExpanded={false}>
        <AccordionSummary expandIcon={<ExpandMore />}>
          <Typography className={classes.heading}>{title}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          {itemWidth ? (
            <Box display="flex" flexDirection="row" flexWrap="wrap" width="100%">
              {list?.map(
                (item, index) =>
                  item && (
                    <ListItem
                      role={undefined}
                      dense={true}
                      button={true}
                      key={`${index}-${item.id}-${name}`}
                      onClick={event => handleChange(name, item)}
                      style={{ width: itemWidth }}
                    >
                      <Box display="flex" alignItems="center">
                        {useRadio ? (
                          <Radio
                            checked={idsActiveItem.includes(item.id)}
                            tabIndex={-1}
                            disableRipple={true}
                          />
                        ) : (
                          <Checkbox
                            checked={idsActiveItem.includes(item.id)}
                            tabIndex={-1}
                            disableRipple={true}
                          />
                        )}
                        <ListItemText primary={item.libelle} />
                      </Box>
                    </ListItem>
                  ),
              )}
            </Box>
          ) : (
            <List>
              {list?.map(
                (item, index) =>
                  item && (
                    <ListItem
                      role={undefined}
                      dense={true}
                      button={true}
                      key={`${index}-${item.id}-${name}`}
                      onClick={event => handleChange(name, item)}
                    >
                      <Box display="flex" alignItems="center">
                        {useRadio ? (
                          <Radio
                            checked={idsActiveItem.includes(item.id)}
                            tabIndex={-1}
                            disableRipple={true}
                          />
                        ) : (
                          <Checkbox
                            checked={idsActiveItem.includes(item.id)}
                            tabIndex={-1}
                            disableRipple={true}
                          />
                        )}
                        <ListItemText primary={item.libelle} />
                      </Box>
                    </ListItem>
                  ),
              )}
            </List>
          )}
        </AccordionDetails>
      </Accordion>
    </Box>
  );
};

interface ProduitFilterProps {
  setFilters: (filters: any) => void;
  filters: any;
}

const ProduitFilter: FC<ProduitFilterProps> = ({ setFilters, filters }) => {
  const classes = useStyles({});

  const handleDrowpdownChange = (name: string, item: any) => {
    const radioFields = ['canal', 'tri'];
    setFilters(prevState => ({
      ...prevState,
      [name]: radioFields.includes(name) ? [item] : checkIfChecked(item, filters[name]),
    }));
  };

  const checkIfChecked = (item: any, array: any[]) => {
    if (array.map(el => el.id).includes(item.id)) {
      return array.filter(el => el.id !== item.id);
    } else {
      return [...array, item];
    }
  };

  return (
    <Box className={classes.root}>
      <DropdownComponent
        title="Canal"
        list={canals}
        handleChange={handleDrowpdownChange}
        idsActiveItem={filters.canal.map((item: any) => item.id)}
        name="canal"
        useRadio={true}
      />
      <DropdownComponent
        title="Reaction"
        list={reactions}
        handleChange={handleDrowpdownChange}
        idsActiveItem={filters.reactions.map((item: any) => item.id)}
        name="reactions"
        itemWidth="50%"
      />
      <DropdownComponent
        title="Remise"
        list={remises}
        handleChange={handleDrowpdownChange}
        idsActiveItem={filters.remises.map((item: any) => item.id)}
        name="remises"
        itemWidth="50%"
      />
      <DropdownComponent
        title="Trier par"
        list={tris}
        handleChange={handleDrowpdownChange}
        idsActiveItem={filters.tri.map((item: any) => item.id)}
        name="tri"
        itemWidth="33.33%"
        useRadio={true}
      />
    </Box>
  );
};

export default ProduitFilter;
