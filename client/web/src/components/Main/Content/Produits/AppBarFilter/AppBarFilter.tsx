import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import { Filter } from '@app/ui-kit/components/pages/LaboratoirePage/common/filter';
import { FormControlLabel, Switch } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {
  Add,
  ArrowBack,
  Delete,
  FilterList,
  Replay,
  ViewList,
  ViewModule,
} from '@material-ui/icons';
import ViewListIcon from '@material-ui/icons/ViewList';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import classNames from 'classnames';
import _ from 'lodash';
import React, { ChangeEvent, FC, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { ContentContext, ContentStateInterface } from '../../../../../AppContext';
import FilterAlt from '../../../../../assets/icons/todo/filter_alt.svg';
import { GET_CHECKEDS_PRODUIT } from '../../../../../graphql/ProduitCanal/local';
import { DELETE_CANAL_ARTICLES } from '../../../../../graphql/ProduitCanal/mutation';
import { GET_SEARCH_GESTION_PRODUIT_CANAL } from '../../../../../graphql/ProduitCanal/query';
import {
  softDeleteCanalArticles,
  softDeleteCanalArticlesVariables,
} from '../../../../../graphql/ProduitCanal/types/softDeleteCanalArticles';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { AppAuthorization } from '../../../../../services/authorization';
import { getUser } from '../../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { ConfirmDeleteDialog } from '../../../../Common/ConfirmDialog';
import CustomButton from '../../../../Common/CustomButton';
import { CustomFullScreenModal } from '../../../../Common/CustomModal';
import SearchContent from '../../../../Common/Filter/SearchContent';
import ButtonGoToCommand from '../../OperationCommerciale/ButtonGoToCommand';
import RemiseForm, { RemiseFormValues } from '../RemiseForm';
import ProduitFilter from './ProduitFilter';
import { canals, tris } from './ProduitFilter/ProduitFilter';
import useStyles from './styles';

import { CREATE_ONE_REMISE } from '../../../../../federation/partenaire-service/remise/mutation';
import {
  CREATE_ONE_REMISE as CREATE_ONE_REMISE_TYPE,
  CREATE_ONE_REMISEVariables,
} from '../../../../../federation/partenaire-service/remise/types/CREATE_ONE_REMISE';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import CreationProduit from '../../../../Dashboard/GestionProduits/CreationProduit/CreationProduit';

interface ChipsInterface {
  id: number;
  nom: string;
  nbArticle: number;
  __typename: string;
}

interface AppBarFilterProps {
  changeView: () => void;
  view: string;
  match: {
    params: {
      type: string | undefined;
      id: string | undefined;
      view: string;
      idOperation: string | undefined;
    };
  };
  listResult?: any;
  history: {
    location: { state: any };
  };
  showGestionProduitsBtns?: boolean;
  selected?: any;
  setSelected?: (values: any) => void;
  refetch?: any;
}

const AppBarFilter: FC<AppBarFilterProps & RouteComponentProps> = ({
  changeView,
  view,
  location,
  match,
  history: {
    location: { state },
    push,
  },
  listResult,
  showGestionProduitsBtns,
  selected,
  setSelected,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const idOperation = match && match.params && match.params.idOperation;
  const isInLaboratoireProduits = location.pathname.includes('/laboratoires');
  const isInGestionProduits = location.pathname.includes('/gestion-produits');
  const isInOperationCommande = window.location.hash.includes('#/operation-produits');
  const [openConfirmDialog, setOpenConfirmDialog] = useState(false);
  const [openFormModal, setOpenFormModal] = useState(false);
  const [checkedMyPharmacie, setCheckedMyPharmacie] = useState<boolean>(true);
  const [sort, setSort] = useState<'asc' | 'desc'>('asc');

  const [remiseSource, setRemiseSource] = useState<'PERMANENT' | 'OPERATION'>('PERMANENT');
  const [openRemiseModal, setOpenRemiseModal] = useState<boolean>(false);
  const defaultRemiseFormValues: RemiseFormValues = {
    dateDebut: null,
    dateFin: null,
    nom: '',
    model: 'LIGNE',
    remiseDetails: [],
  };
  const [remiseFormValues, setRemiseFormValues] = useState<RemiseFormValues>(
    defaultRemiseFormValues,
  );

  const [createRemise, creationRemise] = useMutation<
    CREATE_ONE_REMISE_TYPE,
    CREATE_ONE_REMISEVariables
  >(CREATE_ONE_REMISE, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'SUCCESS',
        message: 'La remise a été créée avec succès',
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
      setRemiseFormValues(defaultRemiseFormValues);
      setSelected && setSelected([]);
      refetch && refetch();
      setOpenRemiseModal(false);
    },
    onError: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: "Une erreur s'est produite lors de création de la remise",
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const {
    content: { refetch },
  } = useContext<ContentStateInterface>(ContentContext);

  const [filters, setFilters] = useState({
    canal: [canals[2]],
    reactions: [],
    remises: [],
    tri: [tris[1]],
  });

  const user = getUser();
  const auth = new AppAuthorization(user);

  // change view mode : card or list
  const handleChangeView = () => {
    changeView();
  };

  const checkedsQueryResult = useQuery(GET_CHECKEDS_PRODUIT);
  const checkedItems =
    (checkedsQueryResult && checkedsQueryResult.data && checkedsQueryResult.data.checkedsProduit) ||
    [];

  const handleCreateProduit = () => {
    push(`/gestion-produits/create`);
    setOpenFormModal(true);
  };

  const handleOpenRemiseModal = (source: 'PERMANENT' | 'OPERATION') => {
    setRemiseSource(source);
    setOpenRemiseModal(true);
  };

  const [doDeleteArticles, doDeleteArticlesResult] = useMutation<
    softDeleteCanalArticles,
    softDeleteCanalArticlesVariables
  >(DELETE_CANAL_ARTICLES, {
    update: (cache, { data }) => {
      if (data && data.softDeleteCanalArticles) {
        if (listResult && listResult.variables) {
          const query = cache.readQuery<any, any>({
            query: GET_SEARCH_GESTION_PRODUIT_CANAL,
            variables: listResult.variables,
          });
          if (query && query.search && query.search.data) {
            cache.writeQuery({
              query: GET_SEARCH_GESTION_PRODUIT_CANAL,
              data: {
                search: {
                  ...query.search,
                  ...{
                    data: _.differenceWith(query.search.data, checkedItems, _.isEqual),
                  },
                },
              },
              variables: listResult.variables,
            });
          }
        }
        client.writeData({
          data: {
            checkedsProduit: null,
          },
        });
      }
    },
    onCompleted: data => {
      const snackBarData: SnackVariableInterface = {
        type: 'SUCCESS',
        message: 'Article supprimé avec succès',
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
      setOpenConfirmDialog(false);
    },
  });

  const handleOpenConfirmDialog = () => {
    setOpenConfirmDialog(true);
  };

  const handleDelete = () => {
    doDeleteArticles({
      variables: { ids: checkedItems && checkedItems.map(item => item.id) },
    });
  };

  const clickBack = () => {
    if (state.idLaboratoire)
      push(`/laboratoires/${state.idLaboratoire}/${'partenaire'}`, {
        goBack: true,
      });
  };

  const handleFilterChange = (data: any) => {
    client.writeData({
      data: {
        sort: {
          sortItem: {
            label: 'tri',
            name: filters.tri[0].name,
            direction: sort,
            active: true,
            __typename: 'sortItem',
          },
          __typename: 'sort',
        },
        filters: {
          idCheckeds: [],
          idLaboratoires: [],
          idFamilles: [],
          idGammesCommercials: [],
          idCommandeCanal: filters.canal[0].code,
          idCommandeCanals: [],
          idCommandeTypes: [],
          idActualiteOrigine: null,
          idOcCommande: null,
          idSeen: null,
          idTodoType: null,
          sortie: null,
          laboType: null,
          reaction: filters.reactions.length > 0 ? filters.reactions.map((el: any) => el.name) : [],
          idRemise: filters.remises.length > 0 ? filters.remises.map((el: any) => el.name) : [],
          __typename: 'filters',
        },
      },
    });
  };

  const handleSortChange = () => {
    setSort(prevState => (prevState === 'asc' ? 'desc' : 'asc'));
    handleFilterChange(null);
  };

  const handleMyPharmacieChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { checked } = event.target;
    setCheckedMyPharmacie(checked);
    client.writeData({
      data: {
        myPharmacieFilter: checked,
      },
    });
  };

  const handleCreateRemise = () => {
    createRemise({
      variables: {
        input: {
          //TODO : idProduitCanals
          idProduitCanals: (selected || []).map(produitCanal => produitCanal.id),
          source: remiseSource,
          ...remiseFormValues,
          remiseDetails: remiseFormValues.remiseDetails.map(remiseDetail => ({
            ...remiseDetail,
            id: undefined,
          })),
        },
      },
    });
  };

  const btnWithSelection = (
    <>
      <Box>
        <CustomButton
          color={view === 'card' ? 'primary' : 'inherit'}
          variant={view === 'card' ? 'contained' : 'outlined'}
          onClick={handleChangeView}
          startIcon={<ViewModule />}
          className={classes.viewButton}
        >
          Vue colonne
        </CustomButton>
        <CustomButton
          color={view === 'list' ? 'primary' : 'inherit'}
          variant={view === 'list' ? 'contained' : 'outlined'}
          onClick={handleChangeView}
          startIcon={<ViewList />}
        >
          Vue tableau
        </CustomButton>
      </Box>
      <Box>
        {checkedItems && checkedItems.length > 0 && (
          <CustomButton
            className={classes.deleteButton}
            startIcon={<Delete />}
            onClick={handleOpenConfirmDialog}
            disabled={!auth.isAuthorizedToDeleteProduct()}
          >
            Supprimer la selection
          </CustomButton>
        )}
        {}

        {isInGestionProduits && (
          <Box display="flex">
            {auth.isAuthorizedToAddProduct() && (
              <CustomButton
                color="secondary"
                style={{ marginLeft: 5 }}
                startIcon={<Add />}
                onClick={handleCreateProduit}
                disabled={!auth.isAuthorizedToAddProduct()}
              >
                Ajouter un produit
              </CustomButton>
            )}
            {showGestionProduitsBtns && (
              <>
                <CustomButton
                  color="secondary"
                  startIcon={<Add />}
                  style={{ marginLeft: 16 }}
                  name="remise"
                  onClick={() => handleOpenRemiseModal('PERMANENT')}
                >
                  Ajouter une remise
                </CustomButton>
                <CustomButton
                  color="secondary"
                  style={{ marginLeft: 16 }}
                  startIcon={<Add />}
                  name="promotion"
                  onClick={() => handleOpenRemiseModal('OPERATION')}
                >
                  Ajouter une promotion
                </CustomButton>
              </>
            )}
          </Box>
        )}
        {isInOperationCommande && <ButtonGoToCommand />}

        <ConfirmDeleteDialog
          title={`Suppression de la sélection (${(checkedItems && checkedItems.length) || 0})`}
          content="Êtes-vous sûr de vouloir supprimer ces produits"
          open={openConfirmDialog}
          setOpen={setOpenConfirmDialog}
          onClickConfirm={handleDelete}
          isLoading={doDeleteArticlesResult.loading}
        />
      </Box>
    </>
  );

  const btnWithoutSelection = (
    <Box display="flex">
      {!isInLaboratoireProduits && (
        <Box display="flex" alignItems="center">
          {idOperation && <ButtonGoToCommand />}
          <Typography className={classes.affichage}>Type d'affichage :</Typography>
        </Box>
      )}

      <Box marginLeft="10px" alignItems="center" display="flex">
        <IconButton
          color={view === 'card' ? 'primary' : 'default'}
          edge="start"
          className={classNames(classes.menuButton)}
          onClick={handleChangeView}
        >
          <ViewModuleIcon />
        </IconButton>
        <IconButton
          color={view === 'list' ? 'primary' : 'default'}
          edge="start"
          className={classNames(classes.menuButton)}
          onClick={handleChangeView}
        >
          <ViewListIcon />
        </IconButton>
        {isInLaboratoireProduits && (
          <>
            <IconButton onClick={handleSortChange}>
              {sort === 'asc' ? (
                <FilterList />
              ) : (
                <FilterList style={{ transform: 'rotate(180deg)' }} />
              )}
            </IconButton>
            <Box margin="0px 16px">
              <Filter
                filters={[]}
                children={<ProduitFilter setFilters={setFilters} filters={filters} />}
                onChange={handleFilterChange}
                filterIcon={<img src={FilterAlt} />}
              />
            </Box>

            <IconButton onClick={() => refetch()} disabled={!refetch}>
              <Replay />
            </IconButton>
            <SearchContent placeholder="Rechercher un produit" />
          </>
        )}
      </Box>
    </Box>
  );

  return (
    <Box className={classes.root}>
      <AppBar position="static" className={classes.appbar}>
        <Toolbar className={classes.toolbar}>
          {state && state.idLaboratoire && (
            <ArrowBack style={{ cursor: 'pointer' }} onClick={clickBack} />
          )}
          <Typography className={classes.title}>
            {isInGestionProduits || idOperation
              ? 'Liste des produits'
              : 'Liste de tous les produits'}
          </Typography>
          <Box display="flex">
            <FormControlLabel
              //disabled={listResult.loading}
              control={<Switch checked={checkedMyPharmacie} onChange={handleMyPharmacieChange} />}
              label="Ma pharmacie"
            />
            {listResult?.data?.search?.data && (isInGestionProduits || idOperation) ? (
              btnWithSelection
            ) : listResult?.data?.search?.data ? (
              btnWithoutSelection
            ) : (
              <></>
            )}
          </Box>
        </Toolbar>
      </AppBar>
      <CreationProduit
        openFormModal={openFormModal}
        setOpenFormModal={setOpenFormModal}
        refetch={listResult.refetch}
      />

      <CustomFullScreenModal
        open={openRemiseModal}
        setOpen={setOpenRemiseModal}
        title={remiseSource === 'PERMANENT' ? 'Remise' : 'Promotion'}
        withBtnsActions={true}
        disabledButton={creationRemise.loading}
        actionButton="Ajouter"
        onClickConfirm={handleCreateRemise}
      >
        <RemiseForm values={remiseFormValues} onChange={setRemiseFormValues} />
      </CustomFullScreenModal>
    </Box>
  );
};

export default withRouter(AppBarFilter);
