import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'center',
      flexWrap: 'wrap',
      padding: theme.spacing(0.5),
      '& .MuiChip-root': {
        backgroundColor: '#ffffff',
        borderRadius: 4,
        boxShadow: '0px 0px 4px 0px rgba(0,0,0,0.25)',
      },
      '& .MuiChip-label': {
        fontWeight: 500,
        fontSize: '0.875rem',
      },
      '& .MuiSvgIcon-root': {
        color: '#5D5D5D',
      },
      '& .MuiChip-deleteIcon:hover': {
        opacity: 0.9,
      },
    },
    chip: {
      margin: theme.spacing(0.5),
    },
  }),
);

export default useStyles;
