import { Box, CssBaseline, Hidden, IconButton, Typography } from '@material-ui/core';
import { Tune } from '@material-ui/icons';
import AddIcon from '@material-ui/icons/Add';
import SettingsIcon from '@material-ui/icons/Settings';
import { debounce } from 'lodash';
import React, { FC, useEffect, useMemo, useRef, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories } from '../../../../../graphql/Parametre/types/PARAMETERS_GROUPES_CATEGORIES';
import { getUser } from '../../../../../services/LocalStorage';
import { isMobile } from '../../../../../utils/Helpers';
import ClipboardTodo from '../Common/Icon/clipboardTodo';
import AjoutTaches from '../Modals/AjoutTache';
import { CountTodosProps } from '../TodoNew';
import Content from './Content';
import { hiddenTypeProject } from './filter';
import Search from './Search';
import useStyles from './styles';

interface FilterProps {
  filters: any;
  setFilters: (value: any) => void;
  parameters?: (PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories | null)[] | null;
  refetchAll?: () => void;
}

const Filters: FC<FilterProps & RouteComponentProps & CountTodosProps> = props => {
  const classes = useStyles({});
  const me = getUser();
  const [openModalTache, setOpenModalTache] = useState<boolean>(false);
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const handleSetOpenModalTache = () => {
    setOpenModalTache(!openModalTache);
  };

  const {
    history: { push },
    filters,
    setFilters,
    countTodos,
    parameters,
    refetchCountTodos,
    refetchAll,
  } = props;

  const [typeProjectData, setTypeProjectData] = useState<any[]>([
    {
      nom: 'Professionnel',
      value: 'PROFESSIONNEL',
      checked: true,
      count: 0,
      key: 'professionnel',
      hidden: false,
      disable: true,
    },
  ]);

  const isAssignedTerms = (me && me.id) || '';
  const [typeTaskData, setTypeTaskData] = useState([
    {
      nom: 'Mes tâches',
      checked: true,
      value: {
        bool: {
          should: [
            { terms: { 'assignedUsers.id': [isAssignedTerms] } },
            { term: { 'userCreation.id': me.id } },
          ],
          minimum_should_match: 1,
        },
      },
      count: 0,
      key: 'myActions',
    },
    {
      nom: 'Tâches des autres',
      checked: false,
      value: {
        bool: {
          must: [
            { term: { 'userCreation.id': me && me.id } },
            {
              term: {
                isAssigned: true,
              },
            },
          ],
        },
      },
      count: 0,
      key: 'actionsOfOthers',
    },
    {
      nom: 'Tâches non-assignées',
      checked: false,
      value: null,
      count: 0,
      key: 'notAssignedActions',
    },
  ]);

  const [taskPriorityData, setTaskPriorityData] = useState([
    {
      nom: isMobile() ? 'P1' : 'Priorité 1',
      checked: false,
      value: 1,
      count: 0,
      key: 'priorityOne',
    },
    {
      nom: isMobile() ? 'P2' : 'Priorité 2',
      checked: false,
      value: 2,
      count: 0,
      key: 'priorityTwo',
    },
    {
      nom: isMobile() ? 'P3' : 'Priorité 3',
      checked: false,
      value: 3,
      count: 0,
      key: 'priorityThree',
    },
    {
      nom: isMobile() ? 'P4' : 'Priorité 4',
      checked: false,
      value: 4,
      count: 0,
      key: 'priorityFour',
    },
  ]);

  /*
  const [taskEcheanceData, setTaskEcheanceData] = useState([
    {
      nom: 'Avec date',
      checked: false,
      value: true,
      count: 0,
      key: 'withdateDebut',
    },
    {
      nom: 'Sans date',
      checked: false,
      value: false,
      count: 0,
      key: 'withoutdateDebut',
    },
  ]);
  */

  const [taskStatus, setTaskStatus] = useState([
    {
      nom: 'A faire',
      checked: true,
      value: 'ACTIVE',
      count: 0,
      key: 'taskActive',
    },
    {
      nom: 'Terminée',
      checked: false,
      value: 'DONE',
      count: 0,
      key: 'taskDone',
    },
  ]);

  // Update items count
  React.useMemo(() => {
    if (countTodos) {
      setTypeProjectData(prev => {
        return prev.map(i => {
          return { ...i, count: countTodos.project[i.key] };
        });
      });
      /* setTypeTaskData(prev => {
        return prev.map(i => {
          return { ...i, count: countTodos.action[i.key] };
        });
      }); */
      setTaskPriorityData(prev => {
        return prev.map(i => {
          return { ...i, count: countTodos.action[i.key] };
        });
      });
      setTaskStatus(prev => {
        return prev.map(i => {
          return { ...i, count: countTodos.action[i.key] };
        });
      });
    }
  }, [countTodos]);

  const handleSetPriorityData = (value: any) => {
    setTaskPriorityData(value);
    if (refetchCountTodos) refetchCountTodos();
  };

  useEffect(() => {
    //  const countTaskEcheance = taskEcheanceData.filter(echeance => echeance.checked).length;
    /*  const valueEcheance =
        countTaskEcheance > 1 || countTaskEcheance === 0
          ? {}
          : { taskEcheance: taskEcheanceData.filter(echeance => echeance.checked)[0] };
  */
    const query = {
      typeProject:
        typeProjectData &&
        typeProjectData.length &&
        typeProjectData.filter(type => type.checked).map(type => type.value),
      taskPriority: taskPriorityData
        .filter(priority => priority.checked)
        .map(priority => priority.value),
      taskType: typeTaskData.filter(type => type.checked).map(type => type.value)[0],
      taskTypeChecked: typeTaskData.find(type => type.checked),
      //  withDateChecked: taskEcheanceData.find(item => item.key === 'withdateDebut')?.checked,
      /* withoutDateChecked: taskEcheanceData.find(item => item.key === 'withoutdateDebut')
         ?.checked,
         */
      taskStatus: taskStatus.filter(type => type.checked).map(item => item.value)[0],
      // ...valueEcheance,
    };
    debounceFilter.current(query);
  }, [typeProjectData, taskPriorityData, taskStatus]);

  useMemo(() => {
    if (parameters && parameters.length) {
      setTypeProjectData([
        {
          nom: 'Personnel',
          value: 'PERSONNEL',
          checked: false,
          count: 0,
          key: 'personnel',
          hidden: hiddenTypeProject(
            'PERSONNEL',
            parameters && parameters.length ? parameters : ([] as any),
          ),
        },
        {
          nom: 'Groupement',
          value: 'GROUPEMENT',
          checked: false,
          count: 0,
          key: 'groupement',
          hidden: hiddenTypeProject(
            'GROUPEMENT',
            parameters && parameters.length ? parameters : ([] as any),
          ),
        },
        {
          nom: 'Professionnel',
          value: 'PROFESSIONNEL',
          checked: true,
          count: 0,
          key: 'professionnel',
          hidden: false,
          disable:
            hiddenTypeProject(
              'PERSONNEL',
              parameters && parameters.length ? parameters : ([] as any),
            ) &&
              hiddenTypeProject(
                'GROUPEMENT',
                parameters && parameters.length ? parameters : ([] as any),
              )
              ? true
              : false,
        },
      ]);
    }
  }, [parameters]);

  const debounceFilter = useRef(
    debounce((query: any) => {
      setFilters(prev => ({ ...prev, ...query }));
    }, 1000),
  );

  const goTo = (menu: string) => () => {
    push(menu);
  };

  return (
    <Box className={classes.contentFilter}>
      <Box display="flex" justifyContent="space-between" alignItems="center" padding="0px 0px 25px">
        <Box display="flex" justifyContent="start">
          <ClipboardTodo />
          <Typography className={classes.title}>To do</Typography>
        </Box>
        <CssBaseline />

        <Box display="flex" alignItems="center">
          <Hidden mdDown={true} implementation="css">
            <IconButton
              className={classes.iconAction}
              color="inherit"
              aria-label="settings"
              edge="start"
              onClick={() => setOpenModalTache(true)}
            >
              <AddIcon />
            </IconButton>

            <IconButton
              className={classes.iconAction}
              color="inherit"
              aria-label="settings"
              edge="start"
              onClick={goTo('/db/colors')}
            >
              <SettingsIcon />
            </IconButton>
          </Hidden>
          <Hidden mdUp={true} implementation="css">
            <IconButton
              className={classes.iconAction}
              color="inherit"
              aria-label="settings"
              edge="start"
              onClick={() => setShowFilters(true)}
            >
              <Tune />
            </IconButton>
          </Hidden>
        </Box>
      </Box>
      <Search placeholder={'Rechercher des tâches'} refetchAll={refetchAll} />
      <Hidden mdDown={typeProjectData.length <= 1 ? true : false} implementation="css">
        <Content title="Type de projet" datas={typeProjectData} updateDatas={setTypeProjectData} />
      </Hidden>

      <Content
        title="Status tâches"
        datas={taskStatus}
        updateDatas={setTaskStatus}
        useRadios={true}
      />
      <Content
        title="Priorité des tâches"
        datas={taskPriorityData}
        updateDatas={handleSetPriorityData}
      />

      <AjoutTaches
        openModalTache={openModalTache}
        setOpenTache={handleSetOpenModalTache}
        refetchAll={refetchAll}
      />
    </Box>
  );
};

export default withRouter(Filters);
