import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: { flexWrap: 'nowrap' },
    contentFilter: {
      //padding: theme.spacing(3, 2.5, 10.75),
      '& .MuiRadio-root': {
        color: '#000000',
      },
      '& .MuiCheckbox-colorSecondary.Mui-checked': {
        color: `${theme.palette.secondary.main} !important`,
      },
      '& .MuiRadio-colorSecondary.Mui-checked': {
        color: `${theme.palette.secondary.main} !important`,
      },
      '& .MuiSvgIcon-root': {
        '& #Tracé_2726': {
          fill: theme.palette.secondary.main,
        },
        '& #Tracé_2725': {
          fill: theme.palette.secondary.main,
        },
      },
      // [theme.breakpoints.down('md')]: {
      //   padding: theme.spacing(3, 2.5, 0),
      // },
    },
    title: {
      marginLeft: 9,
      fontSize: 18,
      color: theme.palette.secondary.main,
      fontWeight: 'bold',
    },
    iconAction: {
      padding: theme.spacing(0.5),
      margin: theme.spacing(0, 0.65, 0, 0),
    },
  }),
);

export default useStyles;
