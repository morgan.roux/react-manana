import {
  Box,
  CssBaseline,
  Hidden,
  IconButton,
  ListItem,
  ListItemText,
  Typography,
} from '@material-ui/core';
import { Close, EventBusy, Inbox } from '@material-ui/icons';
import { debounce } from 'lodash';
import React, { FC, useEffect, useMemo, useRef, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories } from '../../../../../../graphql/Parametre/types/PARAMETERS_GROUPES_CATEGORIES';
import { getUser } from '../../../../../../services/LocalStorage';
import { CountTodosProps } from '../../TodoNew';
import Content from '../Content';
import { hiddenTypeProject } from '../filter';
import useStyles from './styles';

interface MobileFiltersProps {
  filters: any;
  setFilters: (value: any) => void;
  parameters?: (PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories | null)[] | null;
  refetchAll?: () => void;
  onClose: (close: any) => void;
  menuFilters: any;
  setMenuFilters: (values: any) => void;
  taskStatus: any;
  setTaskStatus: (values: any) => void;
  taskPriorityData: any;
  setTaskPriorityData: (values: any) => void;
  termologie: string;
}

const MobileFilters: FC<MobileFiltersProps & RouteComponentProps & CountTodosProps> = props => {
  const classes = useStyles({});
  const me = getUser();
  const [openModalTache, setOpenModalTache] = useState<boolean>(false);

  const {
    history: { push },
    filters,
    setFilters,
    countTodos,
    parameters,
    refetchCountTodos,
    refetchAll,
    onClose,
    menuFilters,
    setMenuFilters,
    taskStatus,
    setTaskStatus,
    taskPriorityData,
    setTaskPriorityData,
    termologie,
  } = props;

  const [typeProjectData, setTypeProjectData] = useState<any[]>([
    {
      nom: 'Professionnel',
      value: 'PROFESSIONNEL',
      checked: true,
      count: 0,
      key: 'professionnel',
      hidden: false,
      disable: true,
    },
  ]);

  const isAssignedTerms = (me && me.id) || '';
  const [typeTaskData, setTypeTaskData] = useState([
    {
      nom: 'Mes tâches',
      checked: true,
      value: {
        bool: {
          should: [
            { terms: { 'assignedUsers.id': [isAssignedTerms] } },
            { term: { 'userCreation.id': me.id } },
          ],
          minimum_should_match: 1,
        },
      },
      count: 0,
      key: 'myActions',
    },
    {
      nom: 'Tâches des autres',
      checked: false,
      value: {
        bool: {
          must: [
            { term: { 'userCreation.id': me && me.id } },
            {
              term: {
                isAssigned: true,
              },
            },
          ],
        },
      },
      count: 0,
      key: 'actionsOfOthers',
    },
    {
      nom: 'Tâches non-assignées',
      checked: false,
      value: null,
      count: 0,
      key: 'notAssignedActions',
    },
  ]);

  /* const [taskEcheanceData, setTaskEcheanceData] = useState([
     {
       nom: 'Avec date',
       checked: false,
       value: true,
       count: 0,
       key: 'withdateDebut',
     },
     {
       nom: 'Sans date',
       checked: false,
       value: false,
       count: 0,
       key: 'withoutdateDebut',
     },
   ]);
   */

  // Update items count
  React.useMemo(() => {
    if (countTodos) {
      setTypeProjectData(prev => {
        return prev.map(i => {
          return { ...i, count: countTodos.project[i.key] };
        });
      });
      /* setTypeTaskData(prev => {
        return prev.map(i => {
          return { ...i, count: countTodos.action[i.key] };
        });
      }); */
      setTaskPriorityData(prev => {
        return prev.map(i => {
          return { ...i, count: countTodos.action[i.key] };
        });
      });
      setTaskStatus(prev => {
        return prev.map(i => {
          return { ...i, count: countTodos.action[i.key] };
        });
      });
    }
  }, [countTodos]);

  const handleSetPriorityData = (value: any) => {
    setTaskPriorityData(value);
    if (refetchCountTodos) refetchCountTodos();
  };

  useEffect(() => {
    /* const countTaskEcheance = taskEcheanceData.filter(echeance => echeance.checked).length;
     const valueEcheance =
       countTaskEcheance > 1 || countTaskEcheance === 0
         ? {}
         : { taskEcheance: taskEcheanceData.filter(echeance => echeance.checked)[0] };
 */
    const query = {
      typeProject:
        typeProjectData &&
        typeProjectData.length &&
        typeProjectData.filter(type => type.checked).map(type => type.value),
      taskPriority: taskPriorityData
        .filter(priority => priority.checked)
        .map(priority => priority.value),
      taskType: typeTaskData.filter(type => type.checked).map(type => type.value)[0],
      taskTypeChecked: typeTaskData.find(type => type.checked),
      // withDateChecked: taskEcheanceData.find(item => item.key === 'withdateDebut')?.checked,
      /* withoutDateChecked: taskEcheanceData.find(item => item.key === 'withoutdateDebut')
         ?.checked,*/
      taskStatus: taskStatus.filter(type => type.checked).map(item => item.value)[0],
      // ...valueEcheance,
    };
    debounceFilter.current(query);
  }, [typeProjectData, taskPriorityData, taskStatus]);

  useMemo(() => {
    if (parameters && parameters.length) {
      setTypeProjectData([
        {
          nom: 'Personnel',
          value: 'PERSONNEL',
          checked: false,
          count: 0,
          key: 'personnel',
          hidden: hiddenTypeProject(
            'PERSONNEL',
            parameters && parameters.length ? parameters : ([] as any),
          ),
        },
        {
          nom: 'Groupement',
          value: 'GROUPEMENT',
          checked: false,
          count: 0,
          key: 'groupement',
          hidden: hiddenTypeProject(
            'GROUPEMENT',
            parameters && parameters.length ? parameters : ([] as any),
          ),
        },
        {
          nom: 'Professionnel',
          value: 'PROFESSIONNEL',
          checked: true,
          count: 0,
          key: 'professionnel',
          hidden: false,
          disable:
            hiddenTypeProject(
              'PERSONNEL',
              parameters && parameters.length ? parameters : ([] as any),
            ) &&
              hiddenTypeProject(
                'GROUPEMENT',
                parameters && parameters.length ? parameters : ([] as any),
              )
              ? true
              : false,
        },
      ]);
    }
  }, [parameters]);

  const debounceFilter = useRef(
    debounce((query: any) => {
      setFilters(prev => ({ ...prev, ...query }));
    }, 1000),
  );

  const title = 'Filtre To-Do';

  const handleClose = () => {
    onClose(false);
  };

  const handleSansDateClick = () => {
    onClose('/todo/sans-date');
  };

  const handleToutVoirClick = () => {
    onClose('/todo/tout-voir');
  };

  const handleStatus = (values: any) => {
    setTaskStatus(values)
    const done = values.find(value => value.value === "DONE")?.checked
    if (done) {
      handleToutVoirClick()
    }
    else {
      handleClose()
    }
  }

  return (
    <Box display="flex" flexDirection="column" width="100%">
      <CssBaseline />
      <Hidden mdUp={true} implementation="css">
        <Box className={classes.topBar}>
          <Typography color="inherit">{title}</Typography>
          <IconButton color="inherit" onClick={handleClose}>
            <Close />
          </IconButton>
        </Box>
      </Hidden>

      {/* <Hidden mdDown={typeProjectData.length <= 1 ? true : false} implementation="css">
        <Content
          title={`Type de ${termologie}`}
          datas={typeProjectData}
          updateDatas={setTypeProjectData}
        />
      </Hidden>
      */}
      <Content
        title="Status tâches"
        datas={taskStatus}
        updateDatas={handleStatus}
        useRadios={true}
      />
      <Box borderBottom="1px solid rgb(227, 227, 227)" marginBottom="16px">
        {/* <Content
          title="Priorité des tâches"
          datas={taskPriorityData}
          updateDatas={handleSetPriorityData}
        /> */}
      </Box>

      {/*<ListItem
        className={classes.listItem}
        role={undefined}
        dense={true}
        button={true}
        onClick={handleSansDateClick}
      >
        <Box width="100%" display="flex" alignItems="center" justifyContent="space-between">
          <Box display="flex" alignItems="center">
            <EventBusy color="secondary" />
            <ListItemText primary="Sans Date" />
          </Box>
          <Typography className={classes.count}>
            {(countTodos && countTodos.action && countTodos.action.withoutDateEcheance) || 0}
          </Typography>
        </Box>
      </ListItem>*/}
      <ListItem
        className={classes.listItem}
        role={undefined}
        dense={true}
        button={true}
        onClick={handleToutVoirClick}
      >
        <Box width="100%" display="flex" alignItems="center" justifyContent="space-between">
          <Box display="flex" alignItems="center">
            <Inbox color="secondary" />
            <ListItemText primary="Tout voir" />
          </Box>
          <Typography className={classes.count}>
            {(countTodos && countTodos.action && countTodos.action.all) || 0}
          </Typography>
        </Box>
      </ListItem>
    </Box>
  );
};

export default withRouter(MobileFilters);
