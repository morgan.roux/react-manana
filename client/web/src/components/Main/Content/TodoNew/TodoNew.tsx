import { useApolloClient, useQuery, useLazyQuery } from '@apollo/react-hooks';
import { ListItem, ListItemText, Radio } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import { useTheme } from '@material-ui/core/styles';
import { Search as SearchIcon } from '@material-ui/icons';
import { ApolloQueryResult } from 'apollo-client';
import React, { useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import FilterAlt from '../../../../assets/icons/todo/filter_alt.svg';
import { GET_SEARCH_ACTION } from '../../../../graphql/Action';
import { SEARCH_ACTIONVariables } from '../../../../graphql/Action/types/SEARCH_ACTION';
import { ME_me } from '../../../../graphql/Authentication/types/ME';
import { GET_PARAMETERS_GROUPES_CATEGORIES } from '../../../../graphql/Parametre/query';
import {
  PARAMETERS_GROUPES_CATEGORIES,
  PARAMETERS_GROUPES_CATEGORIESVariables,
} from '../../../../graphql/Parametre/types/PARAMETERS_GROUPES_CATEGORIES';
import { GET_COUNT_TODOS, GET_SEARCH_CUSTOM_CONTENT_PROJECT } from '../../../../graphql/Project';
import {
  COUNT_TODOS,
  COUNT_TODOSVariables,
  COUNT_TODOS_countTodos,
} from '../../../../graphql/Project/types/COUNT_TODOS';
import {
  SEARCH_CUSTOM_CONTENT_PROJECT,
  SEARCH_CUSTOM_CONTENT_PROJECTVariables,
} from '../../../../graphql/Project/types/SEARCH_CUSTOM_CONTENT_PROJECT';
import { GET_SEARCH_TODO_SECTION } from '../../../../graphql/TodoSection/query';
import { SEARCH_TODO_SECTIONVariables } from '../../../../graphql/TodoSection/types/SEARCH_TODO_SECTION';
import { getGroupement, getPharmacie, getUser } from '../../../../services/LocalStorage';
import { ActionStatus, ParamCategory, TypeProject } from '../../../../types/graphql-global-types';
import { useValueParameterAsBoolean } from '../../../../utils/getValueParameter';
import { isMd, isMobile } from '../../../../utils/Helpers';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import { CustomModal } from '../../../Common/CustomModal';
import ConcernedParticipant from '../InteligenceCollective/Modals/ConcernedParticipant';
import { UrgenceInterface } from './Common/UrgenceFilter/UrgenceFilter';
import { ImportanceInterface } from './Common/ImportanceFilter/ImportanceFilterList';

import MobileFilters from './Filters/MobileFilters';
import Search from './Filters/Search';
import MainContent from './MainContent/MainContent';
import Sider from './NavBar/Sider';
import useStyles from './styles';
import { buildTermActions } from './termActions';
import { findProject } from './util';
import { useImportance, useUrgence } from '../../../hooks';

interface TodoProps {
  window?: () => Window;
  match: {
    params: {
      filter: string;
      filterId: string;
    };
  };
}

export interface CountTodosProps {
  refetchCountTodos?: (
    variables?: COUNT_TODOSVariables | undefined,
  ) => Promise<ApolloQueryResult<COUNT_TODOS>>;
  countTodos?: COUNT_TODOS_countTodos;
}

export const defaultDateTerm = {
  range: { dateDebut: { gt: 'now/d' } },
}; /* {
  term: {
    dateDebut: {
      gt: 'now/d'
    },
  },
};*/

const ResponsiveDrawerTodo: React.FC<RouteComponentProps & TodoProps> = props => {
  const {
    match: {
      params: { filter, filterId },
    },
    location: { pathname },
    history: { push },
  } = props;

  const classes = useStyles({});
  const theme = useTheme();
  const enableMatriceResponsabilite = useValueParameterAsBoolean('0501');
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [fetchingMore, setFetchingMore] = useState<boolean>(false);
  const [triage, setTriage] = useState<any[]>([{ priority: { order: 'asc' } }]);
  const [taskListVariables, setTaskListVariables] = useState({});
  const [showMainContent, setShowMainContent] = useState<boolean>(
    pathname === '/todo/aujourdui' ? false : true,
  );
  const [skip, setSkip] = useState<number>(0);
  const [todaySkip, setTodaySkip] = useState<number>(0);

  const [showMobileFilters, setShowMobileFilters] = useState<boolean>(false);

  const [menuFilters, setMenuFilters] = useState<any>({
    equipe: pathname.includes('equipe') ? true : false,
  });

  const [mobileTaskStatus, setMobileTaskStatus] = useState([
    {
      nom: 'A faire',
      checked: true,
      value: 'ACTIVE',
      count: 0,
      key: 'taskActive',
    },
    {
      nom: 'Terminée(s)',
      checked: false,
      value: 'DONE',
      count: 0,
      key: 'taskDone',
    },
  ]);

  const [mobileTaskPriorityData, setMobileTaskPriorityData] = useState([
    {
      nom: isMobile() ? 'P1' : 'Priorité 1',
      checked: false,
      value: 1,
      count: 0,
      key: 'priorityOne',
    },
    {
      nom: isMobile() ? 'P2' : 'Priorité 2',
      checked: false,
      value: 2,
      count: 0,
      key: 'priorityTwo',
    },
    {
      nom: isMobile() ? 'P3' : 'Priorité 3',
      checked: false,
      value: 3,
      count: 0,
      key: 'priorityThree',
    },
    {
      nom: isMobile() ? 'P4' : 'Priorité 4',
      checked: false,
      value: 4,
      count: 0,
      key: 'priorityFour',
    },
  ]);

  const [urgence, setUrgence] = useState<UrgenceInterface[] | null | undefined>();
  const [importances, setImportances] = useState<ImportanceInterface[] | null | undefined>([]);
  const loadingImportances = useImportance();
  const loadingUrgences = useUrgence();

  const importancesData = loadingImportances.data?.importances.nodes;
  const urgencesData = loadingUrgences.data?.urgences.nodes;

  useEffect(() => {
    if (!loadingUrgences.loading && urgencesData) {
      setUrgence((urgencesData || []) as any);
    }
  }, [loadingUrgences.loading, urgencesData]);

  useEffect(() => {
    if (!loadingImportances.loading && importancesData) {
      setImportances((importancesData || []) as any);
    }
  }, [loadingImportances.loading, importancesData]);

  const client = useApolloClient();
  const user: ME_me = getUser();
  const userId = (user && user.id) || '';

  const [filters, setFilters] = useState<any>(null);

  const typesProjects =
    filters && filters.typeProject && filters.typeProject.length > 0
      ? filters.typeProject
      : [TypeProject.PROFESSIONNEL];
  const myTask =
    filters && filters.taskTypeChecked && filters.taskTypeChecked.key === 'myActions'
      ? true
      : false;
  const taskOfOthers =
    filters && filters.taskTypeChecked && filters.taskTypeChecked.key === 'actionsOfOthers'
      ? true
      : false;
  const unassignedTask =
    filters && filters.taskTypeChecked && filters.taskTypeChecked.key === 'notAssignedActions'
      ? true
      : false;
  const withDate = (filters && filters.withDateChecked) || false;
  const withoutDate = (filters && filters.withoutDateChecked) || false;
  const taskStatus = (filters && filters.taskStatus) || ActionStatus.ACTIVE;

  const defaultCountTodosVariable: COUNT_TODOSVariables = {
    input: {
      idUser: userId,
      typesProjects,
      myTask,
      taskOfOthers,
      unassignedTask,
      withDate,
      withoutDate,
      taskStatus,
      useMatriceResponsabilite: enableMatriceResponsabilite,
    },
  };

  useEffect(() => {
    if (pathname === '/todo/aujourdui') {
      setShowMainContent(false);
    }
  }, [pathname]);

  const [loadCount, { data: countTodosData, refetch: refetchCountTodos }] = useLazyQuery<
    COUNT_TODOS,
    COUNT_TODOSVariables
  >(GET_COUNT_TODOS, {
    fetchPolicy: 'cache-and-network',
    variables: defaultCountTodosVariable,
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const { data: dataParameters } = useQuery<
    PARAMETERS_GROUPES_CATEGORIES,
    PARAMETERS_GROUPES_CATEGORIESVariables
  >(GET_PARAMETERS_GROUPES_CATEGORIES, {
    fetchPolicy: 'cache-and-network',
    variables: {
      categories: [
        ParamCategory.GROUPEMENT_SUPERADMIN,
        ParamCategory.GROUPEMENT,
        ParamCategory.USER,
      ],
      groupes: ['TODO'],
    },
  });

  const me: ME_me = getUser();
  const groupement = getGroupement();
  const pharmacie = getPharmacie();
  const myId = (me && me.id) || '';

  const isOnNextWeek = filter === 'sept-jours';
  const useActionListFilters = isOnNextWeek;
  const isOnNextWeekTeam = filter === 'sept-jours-equipe';

  const isOnSevenDays: boolean =
    pathname.includes('/sept-jours') && !pathname.includes('/sept-jours-equipe');
  const isOnSeptJoursEquipe: boolean = pathname.includes('/sept-jours-equipe');

  const isInbox = pathname.includes('recu') && !pathname.includes('recu-equipe');
  const isInboxTeam = pathname.includes('recu-equipe');
  const isTodayOnly = pathname.includes('aujourdhui') && !pathname.includes('aujourdhui-equipe');
  const isTodayTeam = pathname.includes('aujourdhui-equipe');
  const isProject = pathname.includes('projet');
  const isEtiquette = pathname.includes('etiquette');
  const withoutDatePath: boolean = pathname.includes('sans-date');
  const isAllSee: boolean = pathname.includes('/tout-voir');
  const isOnProject = pathname.includes('projet');

  const termActive = filters && filters.taskStatus ? filters.taskStatus : 'ACTIVE';

  const [actionListFilters, setActionListFilters] = useState(
    isOnNextWeek || isOnNextWeekTeam ? defaultDateTerm : {},
  );

  const termologie = enableMatriceResponsabilite ? 'fonction' : 'projet';

  const typeProjectFilters =
    filters?.typeProject?.length > 0
      ? filters.typeProject
      : ['GROUPEMENT', 'PERSONNEL', 'PROFESSIONNEL'];

  const defaultMust = [
    {
      term: { isRemoved: false },
    },
    { terms: { typeProject: [...typeProjectFilters, 'PERSONNEL'] } },
    {
      bool: enableMatriceResponsabilite
        ? {
            should: [
              {
                bool: {
                  must: [
                    { term: { typeProject: 'PERSONNEL' } },
                    {
                      term: { 'user.id': user.id },
                    },
                  ],
                },
              },
              {
                bool: {
                  must: [
                    { term: { typeProject: 'PROFESSIONNEL' } },
                    { term: { 'participants.id': user.id } },
                  ],
                },
              },
            ],
          }
        : {
            should: [
              {
                term: { 'user.id': user.id },
              },
              { term: { 'participants.id': user.id } },
            ],
            minimum_should_match: 1,
          },
    },
  ];

  const projectList = useQuery<
    SEARCH_CUSTOM_CONTENT_PROJECT,
    SEARCH_CUSTOM_CONTENT_PROJECTVariables
  >(GET_SEARCH_CUSTOM_CONTENT_PROJECT, {
    variables: {
      type: ['project'],
      query: {
        query: { bool: { must: defaultMust } },
      },
      sortBy: [{ ordre: { order: 'asc' } }],
      actionStatus: termActive,
    },
  });

  const selectedProject = isProject
    ? findProject(projectList.data?.search?.data || [], filterId)
    : undefined;

  const termActions = buildTermActions({
    filters,
    useActionListFilters,
    actionListFilters,
    isOnNextWeekTeam,
    isProject,
    filterId,
    isEtiquette,
    isTodayOnly,
    isTodayTeam,
    isInboxTeam,
    isOnSevenDays,
    isOnSeptJoursEquipe,
    isInbox,
    withoutDate: withoutDatePath,
    groupement,
    myId,
    isAllSee,
    me,
    useMatriceResponsabilite: enableMatriceResponsabilite,
    urgence,
    importances,
    selectedProject,
  });

  const query = termActions.query;
  const todayQuery = termActions.todayQuery;

  const [
    loadActions,
    {
      data: dataActions,
      loading: loadingTask,
      variables,
      refetch: refetchTask,
      fetchMore: fetchMoreTask,
    },
  ] = useLazyQuery<any, SEARCH_ACTIONVariables>(GET_SEARCH_ACTION, {
    fetchPolicy: 'cache-and-network',
  });

  const [
    loadTodayActions,
    {
      data: dataTodayActions,
      loading: loadingTodayTask,
      variables: variablesToday,
      refetch: refetchTodayTask,
      fetchMore: fetchMoreTodayTask,
    },
  ] = useLazyQuery<any, SEARCH_ACTIONVariables>(GET_SEARCH_ACTION, {
    fetchPolicy: 'cache-and-network',
  });

  useEffect(() => {
    if (variables) setTaskListVariables(variables);
  }, [variables]);

  const sectionQueryVariables = id =>
    id && isProject
      ? {
          query: {
            bool: {
              must: [{ term: { 'project.id': id } }, { term: { isRemoved: false } }],
            },
          },
        }
      : isInboxTeam
      ? {
          query: {
            bool: {
              must: [{ term: { isRemoved: false } }, { term: { isInInboxTeam: true } }],
              must_not: [
                {
                  exists: {
                    field: 'project',
                  },
                },
              ],
            },
          },
        }
      : isInbox
      ? {
          query: {
            bool: {
              must: [{ term: { isRemoved: false } }, { term: { isInInbox: true } }],
              must_not: [
                {
                  exists: {
                    field: 'project',
                  },
                },
              ],
            },
          },
        }
      : {
          query: {
            bool: {
              must: [
                { term: { isRemoved: false } },
                { term: { isInInbox: false } },
                { term: { isInInboxTeam: false } },
              ],
              must_not: [
                {
                  exists: {
                    field: 'project',
                  },
                },
              ],
            },
          },
        };

  const [loadSections, { data: sectionQuery, refetch: refetchSection }] = useLazyQuery<
    any,
    SEARCH_TODO_SECTIONVariables
  >(GET_SEARCH_TODO_SECTION, {
    variables: {
      type: ['todosection'],
      query: sectionQueryVariables(filterId),
    },
    // skip: !isOnProject && !isAllSee && !isInbox && !isInboxTeam,
  });

  useEffect(() => {
    if (!loadingImportances.loading && !projectList.loading && !loadingUrgences.loading) {
      loadActions({
        variables: {
          type: ['action'],
          query,
          sortBy: triage,
          take: !isOnSevenDays ? 12 : undefined,
          skip: !isOnSevenDays ? skip : undefined,
        },
      });
      if (!isOnSevenDays)
        loadTodayActions({
          variables: {
            type: ['action'],
            query: todayQuery,
            sortBy: triage,
            take: 12,
            skip: todaySkip,
          },
        });
      loadCount();

      if (isOnProject || isAllSee || isInbox || isInboxTeam) {
        loadSections();
      }
    }
  }, [
    loadingImportances.loading,
    projectList.loading,
    loadingUrgences.loading,
    pathname,
    urgence,
    importances,
  ]);

  const countTodos: COUNT_TODOS_countTodos | undefined =
    countTodosData && countTodosData.countTodos;

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const [openConcernedParticipantModal, setOpenConcernedParticipantModal] = useState<boolean>(
    false,
  );

  const parameters =
    dataParameters &&
    dataParameters.parametersGroupesCategories &&
    dataParameters.parametersGroupesCategories.length
      ? dataParameters.parametersGroupesCategories.map(groupe => {
          return {
            code: groupe?.code,
            value: groupe && groupe.value && groupe.value.id ? groupe.value.value : 'false',
          };
        })
      : [];

  const refetchAll = () => {
    if (refetchCountTodos) refetchCountTodos();
    if (refetchSection) refetchSection();
    handleRefetchTask();
    if (projectList && projectList.refetch) projectList.refetch();
    if (loadingImportances && loadingImportances.refetch) loadingImportances.refetch();
  };

  const handleRefetchTask = () => {
    refetchTask();
    refetchTodayTask();
  };

  const handleFetchMoreActions = () => {
    setFetchingMore(true);

    fetchMoreTask({
      query: GET_SEARCH_ACTION,
      updateQuery: (prev: any, { fetchMoreResult }) => {
        if (
          prev &&
          prev.search &&
          prev.search.data &&
          fetchMoreResult &&
          fetchMoreResult.search &&
          fetchMoreResult.search.data
        ) {
          const { data: currentData } = prev.search;
          setFetchingMore(false);
          return {
            ...prev,
            search: {
              ...prev.search,
              data: [...currentData, ...fetchMoreResult.search.data],
              total: fetchMoreResult.search.total,
            },
          };
        }
        return prev;
      },
      variables: { type: ['action'], query, sortBy: triage, take: 12, skip: skip + 12 },
    }).finally(() => {
      setFetchingMore(false);
      setSkip(skip + 12);
    });
  };

  const handleFetchMoreTodayActions = () => {
    setFetchingMore(true);
    fetchMoreTodayTask({
      query: GET_SEARCH_ACTION,
      updateQuery: (prev: any, { fetchMoreResult }) => {
        if (
          prev &&
          prev.search &&
          prev.search.data &&
          fetchMoreResult &&
          fetchMoreResult.search &&
          fetchMoreResult.search.data
        ) {
          const { data: currentData } = prev.search;
          return {
            ...prev,
            search: {
              ...prev.search,
              data: [...currentData, ...fetchMoreResult.search.data],
              total: fetchMoreResult.search.total,
            },
          };
        }
        return prev;
      },
      variables: {
        type: ['action'],
        query: todayQuery,
        sortBy: triage,
        take: 12,
        skip: todaySkip + 12,
      },
    }).finally(() => {
      setFetchingMore(false);
      setTodaySkip(todaySkip + 12);
    });
  };

  const handleShowMobileFiltersClick = () => {
    setShowMobileFilters(!showMobileFilters);
  };

  const handleMobileFilterClose = (value: any) => {
    if (typeof value === 'boolean') {
      setShowMobileFilters(value);
    } else {
      setShowMobileFilters(false);
      push(value);
      setShowMainContent(true);
    }
  };

  const handleEquipeFilterClick = () => {
    setMenuFilters(prevState => ({ ...prevState, equipe: !prevState.equipe }));
    if (menuFilters.equipe) {
      push(pathname.replace('-equipe', ''));
    } else {
      push(`${pathname}-equipe`);
    }
  };

  const header = (
    <>
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <CssBaseline />
        <ListItem role={undefined} dense={true} button={true} onClick={handleEquipeFilterClick}>
          <Box display="flex" alignItems="center">
            <Radio checked={menuFilters.equipe === false} tabIndex={-1} disableRipple={true} />
            <ListItemText primary="Ma To-Do" />
          </Box>
        </ListItem>
        <ListItem role={undefined} dense={true} button={true} onClick={handleEquipeFilterClick}>
          <Box display="flex" alignItems="center">
            <Radio checked={menuFilters.equipe === true} tabIndex={-1} disableRipple={true} />
            <ListItemText primary="Equipe" />
          </Box>
        </ListItem>

        {/*<Hidden lgDown={true}>
          <IconButton
            className={classes.iconAction}
            color="inherit"
            aria-label="settings"
            edge="start"
            onClick={goTo('/db/colors')}
          >
            <Settings />
          </IconButton>
  </Hidden>*/}
        <IconButton
          className={classes.iconAction}
          color="inherit"
          aria-label="settings"
          edge="start"
          onClick={handleShowMobileFiltersClick}
        >
          <img src={FilterAlt} />
        </IconButton>
      </Box>
      {!showMobileFilters && (
        <Search
          placeholder={'Rechercher des tâches'}
          refetchAll={refetchAll}
          popupIcon={<SearchIcon />}
        />
      )}
    </>
  );

  return (
    <Box className={classes.root}>
      <CssBaseline />

      {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
      <Hidden smDown={true} implementation="css">
        <nav className={classes.drawerWeb} aria-label="mailbox folders">
          <Box className={classes.drawerContentRoot}>
            {header}
            <Box style={{ display: showMobileFilters ? 'unset' : 'none' }}>
              <MobileFilters
                filters={filters}
                setFilters={setFilters}
                countTodos={countTodos}
                refetchCountTodos={refetchCountTodos}
                refetchAll={refetchAll}
                parameters={
                  dataParameters &&
                  dataParameters.parametersGroupesCategories &&
                  dataParameters.parametersGroupesCategories.length
                    ? dataParameters.parametersGroupesCategories
                    : []
                }
                taskStatus={mobileTaskStatus}
                onClose={handleMobileFilterClose}
                menuFilters={menuFilters}
                setMenuFilters={setMenuFilters}
                setTaskStatus={setMobileTaskStatus}
                taskPriorityData={mobileTaskPriorityData}
                setTaskPriorityData={setMobileTaskPriorityData}
                termologie={termologie}
              />
            </Box>
            {!showMobileFilters && (
              <Sider
                projectList={projectList}
                setActionListFilters={setActionListFilters}
                refetchCountTodos={refetchCountTodos}
                countTodos={countTodos}
                parameters={parameters}
                setShowMainContent={setShowMainContent}
                menuFilters={menuFilters}
                termologie={termologie}
                useMatriceResponsabilite={enableMatriceResponsabilite}
                urgence={urgence}
                setUrgence={setUrgence}
                importances={importances}
                setImportances={setImportances}
                setTriage={setTriage}
              />
            )}
          </Box>
        </nav>
      </Hidden>
      <Hidden mdUp={true} implementation="css">
        {!showMainContent && (
          <nav className={classes.drawer} aria-label="mailbox folders">
            <Box className={classes.drawerContentRoot}>
              {header}
              <Sider
                projectList={projectList}
                setActionListFilters={setActionListFilters}
                refetchCountTodos={refetchCountTodos}
                countTodos={countTodos}
                parameters={parameters}
                setShowMainContent={setShowMainContent}
                menuFilters={menuFilters}
                termologie={termologie}
                useMatriceResponsabilite={enableMatriceResponsabilite}
                urgence={urgence}
                setUrgence={setUrgence}
                importances={importances}
                setImportances={setImportances}
                setTriage={setTriage}
              />
            </Box>
          </nav>
        )}
      </Hidden>

      {isMd() && (
        <Drawer
          container={window.document.body}
          variant="temporary"
          anchor={theme.direction === 'rtl' ? 'right' : 'left'}
          open={mobileOpen}
          onClose={handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          <nav className={classes.drawerWeb} aria-label="mailbox folders">
            <Box className={classes.drawerContentRoot}>
              {header}
              {showMobileFilters ? (
                <MobileFilters
                  filters={filters}
                  setFilters={setFilters}
                  countTodos={countTodos}
                  refetchCountTodos={refetchCountTodos}
                  refetchAll={refetchAll}
                  parameters={
                    dataParameters &&
                    dataParameters.parametersGroupesCategories &&
                    dataParameters.parametersGroupesCategories.length
                      ? dataParameters.parametersGroupesCategories
                      : []
                  }
                  taskStatus={mobileTaskStatus}
                  onClose={handleMobileFilterClose}
                  menuFilters={menuFilters}
                  setMenuFilters={setMenuFilters}
                  setTaskStatus={setMobileTaskStatus}
                  taskPriorityData={mobileTaskPriorityData}
                  setTaskPriorityData={setMobileTaskPriorityData}
                  termologie={termologie}
                />
              ) : (
                <Sider
                  projectList={projectList}
                  setActionListFilters={setActionListFilters}
                  refetchCountTodos={refetchCountTodos}
                  countTodos={countTodos}
                  parameters={parameters}
                  setShowMainContent={setShowMainContent}
                  menuFilters={menuFilters}
                  termologie={termologie}
                  useMatriceResponsabilite={enableMatriceResponsabilite}
                  urgence={urgence}
                  setUrgence={setUrgence}
                  importances={importances}
                  setImportances={setImportances}
                  setTriage={setTriage}
                />
              )}
            </Box>
          </nav>
        </Drawer>
      )}

      {(!isMobile() || showMainContent) && (
        <main className={classes.content}>
          <MainContent
            filters={filters}
            refetchCountTodos={refetchCountTodos}
            countTodos={countTodos}
            parameters={parameters}
            actions={dataActions}
            todayActions={dataTodayActions}
            setActionListFilters={setActionListFilters}
            actionListFilters={actionListFilters}
            setTriage={setTriage}
            triage={triage}
            setTaskListVariables={setTaskListVariables}
            taskListVariables={taskListVariables}
            loadingTask={fetchingMore || loadingTask || loadingTodayTask}
            refetchTask={handleRefetchTask}
            projectList={projectList}
            sectionQuery={sectionQuery}
            refetchSection={refetchSection}
            refetchAll={refetchAll}
            handleDrawerToggle={handleDrawerToggle}
            useMatriceResponsabilite={enableMatriceResponsabilite}
            fetchingMoreActions={fetchingMore}
            fetchMoreActions={handleFetchMoreActions}
            fetchMoreTodayActions={handleFetchMoreTodayActions}
          />
        </main>
      )}

      {isMobile() && (
        <CustomModal
          open={showMobileFilters}
          setOpen={setShowMobileFilters}
          fullScreen={true}
          closeIcon={true}
          withBtnsActions={false}
          noDialogContent={true}
        >
          <MobileFilters
            filters={filters}
            setFilters={setFilters}
            countTodos={countTodos}
            refetchCountTodos={refetchCountTodos}
            refetchAll={refetchAll}
            parameters={
              dataParameters &&
              dataParameters.parametersGroupesCategories &&
              dataParameters.parametersGroupesCategories.length
                ? dataParameters.parametersGroupesCategories
                : []
            }
            taskStatus={mobileTaskStatus}
            onClose={handleMobileFilterClose}
            menuFilters={menuFilters}
            setMenuFilters={setMenuFilters}
            setTaskStatus={setMobileTaskStatus}
            taskPriorityData={mobileTaskPriorityData}
            setTaskPriorityData={setMobileTaskPriorityData}
            termologie={termologie}
          />
        </CustomModal>
      )}

      <ConcernedParticipant
        isOpen={openConcernedParticipantModal}
        setOpen={setOpenConcernedParticipantModal}
        title="Collègue(s) Concernée(s)"
        isOnEquip={true}
      />
    </Box>
  );
};

export default withRouter(ResponsiveDrawerTodo);
