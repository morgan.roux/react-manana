export const filterSubProjects = (
  project: any,
  useMatriceResponsabilite: boolean,
  user: any,
  applyFilterParticipant: boolean,
) => {
  return (project.subProjects || []).filter(
    subProject =>
      subProject &&
      !subProject.isArchived &&
      (!applyFilterParticipant ||
        (subProject.typeProject === 'PERSONNEL' && subProject.user?.id === user.id) ||
        (subProject.typeProject !== 'PERSONNEL' &&
          (applyFilterParticipant
            ? !useMatriceResponsabilite ||
              (useMatriceResponsabilite &&
                subProject.participants.some(participant => participant.id === user.id))
            : true))),
  );
};

export const findProject = (projects: any[], idProjet: string): any => {
  for (const projet of projects) {
    if (projet.id === idProjet) {
      return projet;
    } else if (projet.subProjects) {
      const subProject = findProject(projet.subProjects, idProjet);
      if (subProject) {
        return subProject;
      }
    }
  }

  return undefined;
};

export const extractProjectIds = (project: any, user: any) => {
  if (!project) {
    return [];
  }
  const subProjects = filterSubProjects(project, true, user, true);
  if (subProjects) {
    return [
      project.id,
      ...subProjects.reduce(
        (acc: any, currentProject: any) => [...acc, ...extractProjectIds(currentProject, user)],
        [],
      ),
    ];
  }
  return [project.id];
};

export const extractPersoProject = (project: any, user: any) => {
  if (project?.typeProject === 'PERSONNEL') {
    return project;
  } else if (project) {
    const subProjects = filterSubProjects(project, true, user, true);
    if (subProjects.length === 1 && subProjects[0].typeProject === 'PERSONNEL') {
      return subProjects[0];
    }
  }
  return undefined;
};

export const isTraitementAutomatiqueAction = (action: any): boolean => {
  return action?.item?.code === 'TA_EXECUTION';
};

const REUNION_QUALITE_ACTION_CODES = ['DQ_AMELIORATION', 'DQ_INCIDENT', 'DQ_ACTION_OPERATIONNELLE'];
export const isReunionQualiteAction = (action: any): boolean => {
  return action?.item?.code && REUNION_QUALITE_ACTION_CODES.includes(action.item.code);
};

export const isTradeMarketingAction = (action: any) => {
  return action?.item?.code === 'OPERATION_MARKETING';
};

export const isManuelAction = (action: any) => {
  return (
    !isTraitementAutomatiqueAction(action) &&
    !isReunionQualiteAction(action) &&
    !isTradeMarketingAction(action)
  );
};
