import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
const drawerWidth = 280;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      height: '100%',
    },
    zIndex: {
      zIndex: 2000,
    },
    iconAction: {
      padding: theme.spacing(0.5),
      margin: theme.spacing(0, 0.65, 0, 0),
    },
    drawer: {
      width: '100vw',
      height: '100%',
      borderRight: '1px solid rgba(0, 0, 0, 0.12)',
      overflow: 'auto',
      [theme.breakpoints.up('md')]: {
        width: drawerWidth,
        flexShrink: 0,
        /* marginTop: 86, */
      },
    },
    drawerWeb: {
      width: '100vw',
      height: '100%',
      overflow: 'auto',
      borderRight: '1px solid rgba(0, 0, 0, 0.12)',
      [theme.breakpoints.up('md')]: {
        width: 500,
        flexShrink: 0,
        /* marginTop: 86, */
      },

      [theme.breakpoints.down('md')]: {
        width: 400,
        flexShrink: 0,
        /* marginTop: 86, */
      },
    },

    drawerContentRoot: {
      padding: "24px 24px",
      height: "calc(100vh - 156px)",
      overflow: "auto"
    },
    contentFilter: {
      width: '100%',
      padding: theme.spacing(3, 2.5, 10.75),
    },
    iconContentButton: {
      position: 'absolute',
      right: '-32px',
      top: '-18px',
    },
    appBar: {
      display: 'block',
      marginTop: 86,
      zIndex: 1,
      [theme.breakpoints.up('lg')]: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        display: 'none',
      },
    },
    toolbarTitle: {
      color: theme.palette.common.white,
      fontSize: 30,
      textAlign: 'center',
      width: '100%',
      fontWeight: 'bold',
    },
    menuButton: {
      marginRight: theme.spacing(2),
      background: '#e34168',
      [theme.breakpoints.up('lg')]: {
        display: 'none',
      },
    },

    iconButton: {
      marginRight: 16,
    },
    // necessary for content to be below app bar
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
      width: 500,
      top: 86,
      boxShadow: 'none!important',
    },
    content: {
      width: '100%',
      height: '100%',
    },
    title: {
      marginLeft: 9,
      fontSize: 18,
      color: theme.palette.secondary.main,
      fontWeight: 'bold',
    },
    alertModalTitle: {
      font: 'normal normal bold 30px/37px Roboto',
      letterSpacing: '0px',
      color: '#27272F',
      textAlign: 'center',
      marginTop: 25,
      marginBottom: 25,
    },

    alertModalButton: {
      display: 'flex',
      justifyContent: 'center',
      minWidth: 200,
      minHeight: 50,
    },
    alertModalImages: {
      display: 'flex',
      justifyContent: 'center',
      width: '100%',
      height: '100%',
    },
    rootDialog: {
      '& .MuiDialog-paper': {
        padding: '8px 0 25px',
      },
      '& .MuiDialogContent-root': {
        padding: '15px 40px',
      },
      '& .MuiListItem-root': {
        paddingLeft: 0,
        paddingRight: 0,
        paddingBottom: 0,
      },
      '& .MuiSvgIcon-fontSizeSmall': {
        fontSize: '0.75rem',
        marginRight: 8,
      },
    },
    label: {
      marginLeft: 5,
      fontWeight: 'bold',
    },
    mobileFilters: {
      padding: 0,
      '& .MuiDialogContent-root:first-child': {
        padding: 0,
      },
    },
    row: {
      '& .MuiListItem-gutters': {
        padding: 0,
      },
      '& .MuiListItem-root': {
        width: 'auto',
      },
    },
  }),
);

export default useStyles;
