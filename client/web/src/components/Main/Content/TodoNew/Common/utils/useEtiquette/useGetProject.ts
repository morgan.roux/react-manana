import { useQuery } from '@apollo/react-hooks';
import { User } from '../../../../../../../graphql/Pharmacie/types/User';
import { GET_SEARCH_CUSTOM_CONTENT_PROJECT } from '../../../../../../../graphql/Project';
export const useGetProject = (typeProjectFilters: any, user: User) => {
  const defaultMust = [
    {
      term: { isRemoved: false },
    },
    { terms: { typeProject: typeProjectFilters } },
    {
      bool: {
        should: [
          {
            term: { 'user.id': user.id },
          },
          { term: { 'participants.id': user.id } },
        ],
        minimum_should_match: 1,
      },
    },
  ];
  const { data: projectList, refetch: refetchProject } = useQuery<any>(
    GET_SEARCH_CUSTOM_CONTENT_PROJECT,
    {
      variables: {
        type: ['project'],
        query: {
          query: { bool: { must: defaultMust, must_not: [{ exists: { field: 'projetParent' } }] } },
        },
        sortBy: [{ ordre: { order: 'asc' } }],
      },
    },
  );
  return {
    projectList,
    refetchProject,
  };
};
