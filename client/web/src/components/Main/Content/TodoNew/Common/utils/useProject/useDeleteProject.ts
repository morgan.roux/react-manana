import { useMutation, useApolloClient } from '@apollo/react-hooks';
import { ApolloQueryResult } from 'apollo-client';
import { differenceBy } from 'lodash';
import { any } from 'prop-types';
import { DO_DELETE_PROJECT } from '../../../../../../../graphql/Project/mutation';
import { GET_SEARCH_CUSTOM_CONTENT_PROJECT } from '../../../../../../../graphql/Project/query';
import {
  COUNT_TODOSVariables,
  COUNT_TODOS,
} from '../../../../../../../graphql/Project/types/COUNT_TODOS';
import {
  DELETE_PROJECTS,
  DELETE_PROJECTSVariables,
} from '../../../../../../../graphql/Project/types/DELETE_PROJECTS';
import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';

export const useDeleteProject = (
  queryVariable,
  setOpenDeleteDialog: (value: boolean) => any,
  refetch,
  refetchCountTodos?: (
    variables?: COUNT_TODOSVariables | undefined,
  ) => Promise<ApolloQueryResult<COUNT_TODOS>>,
) => {
  const client = useApolloClient();
  const [
    removeMutation,
    { loading: deleteProjectLoading, error: deleteProjectError },
  ] = useMutation<DELETE_PROJECTS, DELETE_PROJECTSVariables>(DO_DELETE_PROJECT, {
    // update: (cache, { data }) => {
    //   if (data && data.softDeleteProjects) {
    //     if (queryVariable) {
    //       const req: any = cache.readQuery({
    //         query: GET_SEARCH_CUSTOM_CONTENT_PROJECT,
    //         variables: queryVariable,
    //       });
    //       if (req && req.search && req.search.data) {
    //         const source = req.search.data;
    //         const result = data.softDeleteProjects;
    //         const dif = differenceBy(source, result, 'id');
    //         cache.writeQuery({
    //           query: GET_SEARCH_CUSTOM_CONTENT_PROJECT,
    //           data: {
    //             search: {
    //               ...req.search,
    //               ...{
    //                 data: dif,
    //               },
    //             },
    //           },
    //           variables: queryVariable,
    //         });
    //       }
    //     }
    //   }
    // },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
    onCompleted: data => {
      if (data && data.softDeleteProjects) {
        if (refetch) refetch();
        if (refetchCountTodos) refetchCountTodos();
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Suppression réussie',
        });
      }
      setOpenDeleteDialog(false);
    },
  });

  const removeProject = row => {
    removeMutation({
      variables: {
        ids: [row.id],
      },
    });
  };
  return [removeProject];
};
