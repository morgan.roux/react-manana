import { useMutation, useApolloClient } from '@apollo/react-hooks';
import { DO_CREATE_UPDATE_ETIQUETTE } from '../../../../../../../graphql/Etiquette';
import {
  CREATE_UPDATE_TODO_ETIQUETTE,
  CREATE_UPDATE_TODO_ETIQUETTEVariables,
} from '../../../../../../../graphql/Etiquette/types/CREATE_UPDATE_TODO_ETIQUETTE';
import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';

export const useUpdateEtiquette = (setOpenAddToFavorite: (value: boolean) => void) => {
  const client = useApolloClient();
  const [switchEtiquette] = useMutation<
    CREATE_UPDATE_TODO_ETIQUETTE,
    CREATE_UPDATE_TODO_ETIQUETTEVariables
  >(DO_CREATE_UPDATE_ETIQUETTE, {
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
    onCompleted: data => {
      if (data && data.createUpdateTodoEtiquette) {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Opération réussie',
        });
      }
      setOpenAddToFavorite(false);
    },
  });

  const switchFavs = row => {
    switchEtiquette({
      variables: {
        input: {
          id: row && row.id,
          nom: row && row._name,
          isFavoris: !(row && row.isInFavoris),
        },
      },
    });
  };
  return [switchFavs];
};
