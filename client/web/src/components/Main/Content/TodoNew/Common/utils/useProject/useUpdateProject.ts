import { useMutation, useApolloClient } from '@apollo/react-hooks';
import { ME_me } from '../../../../../../../graphql/Authentication/types/ME';
import { DO_CREATE_PROJECT } from '../../../../../../../graphql/Project/mutation';
import {
  CREATE_PROJECT,
  CREATE_PROJECTVariables,
} from '../../../../../../../graphql/Project/types/CREATE_PROJECT';
import { getPharmacie, getUser } from '../../../../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';

export const useUpdateProject = (
  setOpenAddToFavorite: (value: boolean) => void,
  setOpenArchived: (value: boolean) => void,
  row: any,
) => {
  const currentUser: ME_me = getUser();
  const currentPharmacie = getPharmacie();
  const client = useApolloClient();
  const [createUpdateProject] = useMutation<CREATE_PROJECT, CREATE_PROJECTVariables>(
    DO_CREATE_PROJECT,
    {
      onError: errors => {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
        });
      },
      onCompleted: data => {
        if (data && data.createUpdateProject) {
          displaySnackBar(client, {
            isOpen: true,
            type: 'SUCCESS',
            message: 'Opération réussie',
          });
        }
        setOpenAddToFavorite(false);
        setOpenArchived(false);
      },
    },
  );

  const projectInputVariables = {
    id: row && row.id,
    name: row && row._name,
    typeProject: row && row.typeProject,
    idUser: currentUser && currentUser.id,
    idPharmacie: currentPharmacie && currentPharmacie.id,
  };

  const switchFavs = row => {
    createUpdateProject({
      variables: {
        input: {
          ...projectInputVariables,
          isFavoris: !(row && row.isInFavoris),
        },
      },
    });
  };
  const switchArchived = row => {
    createUpdateProject({
      variables: {
        input: {
          ...projectInputVariables,
          isArchived: !(row && row.isArchived),
        },
      },
    });
  };
  return [switchFavs, switchArchived];
};
