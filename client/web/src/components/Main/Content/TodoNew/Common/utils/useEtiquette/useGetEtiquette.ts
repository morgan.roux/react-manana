import { useQuery } from '@apollo/react-hooks';
import { GET_SEARCH_CUSTOM_CONTENT_PROJECT } from '../../../../../../../graphql/Project';

export const useGetEtiquette = () => {
  const { data: projectList, refetch: refetchProject } = useQuery<any>(
    GET_SEARCH_CUSTOM_CONTENT_PROJECT,
  );
  return { projectList, refetchProject };
};
