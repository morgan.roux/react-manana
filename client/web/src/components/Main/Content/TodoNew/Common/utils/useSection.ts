import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { ChangeEvent, useState } from 'react';
import { DO_CREATE_UPDATE_SECTION } from '../../../../../../graphql/Section/mutation';
import { DO_DELETE_TODO_SECTION } from '../../../../../../graphql/TodoSection/mutation';
import { DELETE_TODO_SECTION, DELETE_TODO_SECTIONVariables } from '../../../../../../graphql/TodoSection/types/DELETE_TODO_SECTION';
import SnackVariableInterface from '../../../../../../Interface/SnackVariableInterface';
import { getUser } from '../../../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';

export const useCreateUpdateSection = (
  values: any,
  refetch: any,
  filter: string | undefined,
  filterId: string | undefined,
  setValues: (value: string) => void,
  setOpenAddSection: (value: boolean) => void,
): [any] => {
  const isOnProject = filterId;
  const client = useApolloClient();
  const user = getUser();

  const isInbox = filter === 'recu';
  const isInboxTeam = filter === 'recu-equipe';

  const defaultVariable = {
    libelle: values && values.libelle,
    idUser: user.id,
  };

  const variables = isOnProject
    ? {
        idProject: filterId,
        ...defaultVariable,
      }
    : isInbox
    ? {
        isInInbox: true,
        ...defaultVariable,
      }
    : isInboxTeam
    ? {
        isInInboxTeam: true,
        ...defaultVariable,
      }
    : defaultVariable;

  const [doCreateUpdateSection] = useMutation(DO_CREATE_UPDATE_SECTION, {
    variables: {
      input: { id: values.id, ...variables },
    },
    onCompleted: data => {
      if (refetch) refetch();
      const snackBarData: SnackVariableInterface = {
        type: 'SUCCESS',
        message: 'Section ajoutée avec succès',
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
      setValues('');
      setOpenAddSection(false);
    },
  });
  return [doCreateUpdateSection];
};

export const useSection = () => {
  const [values, setValues] = useState<any>({});
  const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement | any>) => {
    const { name, value } = e.target;
    setValues(prevState => ({
      ...prevState,
      [name]: value,
    }));
  };
  return [values, setValues, handleChange];
};

export const useDeleteSection = (
  id: string,
  refetch: any,
  setOpenDeleteDialog: (open: boolean) => void,
) => {
  const client = useApolloClient();
  const [doDeleteTodoSection] = useMutation<DELETE_TODO_SECTION, DELETE_TODO_SECTIONVariables>(
    DO_DELETE_TODO_SECTION,
    {
      variables: {
        ids: [id],
      },
      onCompleted: data => {
        if (refetch) refetch();
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: 'Section supprimée avec succès',
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
        setOpenDeleteDialog(false);
      },
    },
  );
  return [doDeleteTodoSection];
};
