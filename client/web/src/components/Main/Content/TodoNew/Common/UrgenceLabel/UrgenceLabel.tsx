import { Box, Typography } from '@material-ui/core';
import { Error } from '@material-ui/icons';
import React, { FC } from 'react';
import { GET_URGENCES } from '../../../../../../graphql/Urgence/query';
import { URGENCES } from '../../../../../../graphql/Urgence/types/URGENCES';
import { useStyles } from './styles';
import { useQuery } from '@apollo/react-hooks';
import moment from 'moment';


export const computeCodeFromdateDebut = (input: any): string => {
    const today = moment();
    const dateDebut = moment(input);
    const nextSunday = moment()
        .endOf('isoWeek')
        .add(1, 'week');

    if (dateDebut.day() === today.day() || dateDebut.isSameOrBefore(today)) {
        return 'A';
    }
    if (dateDebut > today && dateDebut <= nextSunday) {
        return 'B';
    }
    return 'C';
}



interface UrgenceLabelProps {
    code?: string
    dateDebut?: any // Code or dateDebut
}

const UrgenceLabel: FC<UrgenceLabelProps> = ({
    code,
    dateDebut
}) => {

    const urgenceCode = code || computeCodeFromdateDebut(dateDebut)

    const classes = useStyles({});
    const urgenceQuery = useQuery<URGENCES>(GET_URGENCES);
    const urgenceList = (urgenceQuery?.data?.urgences || [])
    const urgence: any = urgenceList?.find((urgence: any) => urgence?.code === urgenceCode);

    return urgence ? (
        <Box display="flex" alignItems="center">
            <Error style={{ color: urgence.couleur || 'black' }} />
            <Typography
                className={classes.title}
                style={{ color: urgence.couleur || 'black' }}
            >
                {`${urgence?.code} : ${urgence?.libelle}`}
            </Typography>
        </Box>

    ) : null;
};

export default UrgenceLabel;
