export const findProject = (id?: string, projects?: any[]): any => {
  if (!projects || !id) {
    return undefined;
  }

  for (const project of projects) {
    if (project.id === id) {
      return project;
    }
    if (project.subProjects) {
      const foundSubproject = findProject(id, project.subProjects);
      if (foundSubproject) {
        return foundSubproject;
      }
    }
  }
};
