import { Avatar, Button, Box } from '@material-ui/core';
import React, { FC, useContext, useState } from 'react';
import { useStyles } from './styles';
import DeleteIcon from '@material-ui/icons/Delete';
import { useApolloClient, useMutation } from '@apollo/react-hooks';
import {
  REMOVE_PARTICIPANT,
  REMOVE_PARTICIPANTVariables,
} from '../../../../../../../graphql/Todo/types/REMOVE_PARTICIPANT';
import { DELETE_PARTICIPANT_FROM_PROJECT } from '../../../../../../../graphql/Todo/mutation';
import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';
import { DO_SEARCH_PARTICIPANT } from '../../../../../../../graphql/Todo/query';
import { differenceBy } from 'lodash';
import { ConfirmDeleteDialog } from '../../../../../../Common/ConfirmDialog';
import { ContentContext, ContentStateInterface } from '../../../../../../../AppContext';
import { RouteComponentProps, withRouter } from 'react-router-dom';
interface ParticipantListProps {
  listResult?: any;
  match: {
    params: {
      filterId: string | undefined;
    };
  };
}

const ParticipantList: FC<ParticipantListProps & RouteComponentProps> = ({
  listResult,
  match: {
    params: { filterId },
  },
}) => {
  const classes = useStyles({});
  const client = useApolloClient();

  // const variables = listResult && listResult.variables;

  const {
    content: { variables, operationName, refetch },
  } = useContext<ContentStateInterface>(ContentContext);

  const participants =
    (listResult &&
      listResult.data &&
      listResult.data.search &&
      listResult.data.search.data &&
      listResult.data.search.data[0] &&
      listResult.data.search.data[0].participants) ||
    [];
  const [removeParticipant, result] = useMutation<REMOVE_PARTICIPANT, REMOVE_PARTICIPANTVariables>(
    DELETE_PARTICIPANT_FROM_PROJECT,
    {
      update: (cache, { data }) => {
        if (data && data.removeParticipantFromProject) {
          if (variables) {
            const req: any = cache.readQuery({
              query: DO_SEARCH_PARTICIPANT,
              variables,
            });
            if (req && req.search && req.search.data) {
              const result = data.removeParticipantFromProject;
              cache.writeQuery({
                query: DO_SEARCH_PARTICIPANT,
                data: {
                  search: {
                    ...req.search,
                    ...{
                      data: result,
                    },
                  },
                },
                variables,
              });
            }
          }
        }
      },
      onCompleted: data => {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Participant supprimé avec succès',
        });
        setdeletedId(null);
      },
      onError: error => {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );
  const removeParticipantFromProject = idParticipantToDelete => {
    removeParticipant({
      variables: {
        input: { idProject: filterId || '', idParticipant: idParticipantToDelete },
      },
    });
  };
  // ouvrir confirm dialog
  const [openModal, setOpenModal] = useState(false);

  const [deletedId, setdeletedId] = useState<string | null>(null);

  const handleDelete = (id: string) => () => {
    setOpenModal(true);
    setdeletedId(id);
  };

  const onConfirmDelete = () => {
    if (deletedId) {
      removeParticipantFromProject(deletedId);
    }
    setOpenModal(false);
  };

  return (
    <Box className={classes.rootListParticipant}>
      {participants.map(participant => (
        <Box className={classes.contentRootListParticipant}>
          <Box display="flex" alignItems="center">
            <Box>
              <Avatar alt={participant.userName} className={classes.avatar} />
            </Box>
            <Box className={classes.description}>
              <Box className={classes.title}>{participant.userName}</Box>
              <Box className={classes.content}>{participant.role && participant.role.nom}</Box>
              <Box className={classes.content}>{participant.email}</Box>
            </Box>
          </Box>
          <Box className={classes.icon}>
            <Button onClick={handleDelete(participant.id)}>
              <DeleteIcon />
              <ConfirmDeleteDialog
                open={openModal}
                onClickConfirm={onConfirmDelete}
                setOpen={setOpenModal}
              />
            </Button>
          </Box>
        </Box>
      ))}
    </Box>
  );
};
export default withRouter(ParticipantList);
