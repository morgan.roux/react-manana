import { useLazyQuery, useApolloClient, QueryLazyOptions, useQuery } from '@apollo/react-hooks';
import { UserInfo } from '../../../../../../../graphql/User/types/UserInfo';
import { USER, USERVariables } from '../../../../../../../graphql/User/types/USER';
import { DO_SEARCH_USERS, GET_USER } from '../../../../../../../graphql/User/query';
import {
  SEARCH_USERS,
  SEARCH_USERSVariables,
} from '../../../../../../../graphql/User/types/SEARCH_USERS';
import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';
import { RouteComponentProps } from 'react-router';
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import { getGroupement, getPharmacie } from '../../../../../../../services/LocalStorage';
import { SUPER_ADMINISTRATEUR } from '../../../../../../../Constant/roles';
import SearchInput from '../../../../../../Common/newCustomContent/SearchInput';
import CustomContent from '../../../../../../Common/newCustomContent';
import { useStyles } from '../styles';
import CustomAvatar from '../../../../../../Common/CustomAvatar';
import { Column } from '../../../../../../Dashboard/Content/Interface';
import SubToolbar from '../../../../../../Common/newCustomContent/SubToolbar';
import WithSearch from '../../../../../../Common/newWithSearch/withSearch';
import { ME, ME_me } from '../../../../../../../graphql/Authentication/types/ME';
import { GET_ME } from '../../../../../../../graphql/Authentication/query';

interface FormUserProps {
  listResult?: any;
  valueBtn?: any;
}

const GetUser = props => {
  const { valueBtn, listResult } = props;
  const client = useApolloClient();
  const grp = getGroupement();
  const phrm = getPharmacie();

  const idpharm = phrm && phrm.id;
  const idGrp = (grp && grp.id) || '';
  const classes = useStyles({});
  //const phrmREgion = (phrm && phrm.)
  let [should, setShould] = useState<[]>();

  const columns: Column[] = [
    {
      name: '',
      label: 'Photo',
      renderer: (value: any) => {
        const valueUser =
          value.users &&
          value.pharmacies &&
          value.pharmacies.length === 1 &&
          value.users.length === 1
            ? value.users[0]
            : null;
        return (
          <CustomAvatar
            name={value && value.fullName}
            url={
              valueUser &&
              valueUser.userPhoto &&
              valueUser.userPhoto.fichier &&
              valueUser.userPhoto.fichier.publicUrl
            }
          />
        );
      },
    },
    {
      name: 'userName',
      label: 'Nom',
    },
    {
      name: 'role.nom',
      label: 'Fonction',
      renderer: (value: any) => {
        return value && value.role && value.role.nom ? value.role.nom : '-';
      },
    },
    {
      name: 'email',
      label: 'email',
    },
  ];
  const { loading, error, data: loadDataMe } = useQuery<ME, ME_me>(GET_ME);
  const [maRegion, setMaRegion] = useState(loadDataMe?.me?.pharmacie?.departement?.region?.nom);

  const [PresidentRegion, setPresidentRegion] = useState(
    loadDataMe?.me?.userTitulaire?.isPresident,
  );
  const [LaboPartenaire, setLaboPartenaire] = useState(loadDataMe?.me?.userLaboratoire?.id);
  const [PartenaireService, setPartenaireService] = useState(loadDataMe?.me?.userPartenaire?.id);

  const [
    searchUsers,
    { data: userData, loading: userLoading, refetch: refreshData },
  ] = useLazyQuery<SEARCH_USERS, SEARCH_USERSVariables>(DO_SEARCH_USERS, {
    fetchPolicy: 'cache-and-network',
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const [selected, setSelected] = useState();

  const filters =
    valueBtn === 1
      ? [{ term: { status: 'ACTIVATED' } }]
      : valueBtn === 2
      ? [{ term: { status: 'ACTIVATED' } }, { term: { 'pharmacie.id': idpharm } }]
      : valueBtn === 3
      ? [{ term: { status: 'ACTIVATED' } }, { term: { idGroupement: idGrp } }]
      : [{ term: { status: 'ACTIVATED' } }, { term: {} }];

  const tableProps = {
    isSelectable: true,
    paginationCentered: true,
    columns,
    selected,
    setSelected,
  };

  return (
    <div className={classes.container}>
      <div className={classes.listUserRoot}>
        <div className={classes.searchInputBox}>
          <SearchInput searchPlaceholder="Rechercher un contact" />
        </div>
        <WithSearch
          type="user"
          props={tableProps}
          WrappedComponent={CustomContent}
          searchQuery={DO_SEARCH_USERS}
          optionalShould={filters}
          minimumShouldMatch={0}
          optionalMust={[]}
        />
      </div>
    </div>
  );
};
export default GetUser;
