import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import { differenceBy } from 'lodash';
import React, { FC, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { ContentContext, ContentStateInterface } from '../../../../../../AppContext';
import { ADMINISTRATEUR_GROUPEMENT, SUPER_ADMINISTRATEUR } from '../../../../../../Constant/roles';
import { ME_me } from '../../../../../../graphql/Authentication/types/ME';
import { DO_ADD_PARTICIPANTS_TO_ETIQUETTE } from '../../../../../../graphql/Etiquette';
import {
  ADD_PARTICIPANTS_TO_ETIQUETTE,
  ADD_PARTICIPANTS_TO_ETIQUETTEVariables,
} from '../../../../../../graphql/Etiquette/types/ADD_PARTICIPANTS_TO_ETIQUETTE';
import { GET_PROJECT } from '../../../../../../graphql/Project';
import { PROJECT, PROJECTVariables } from '../../../../../../graphql/Project/types/PROJECT';
import { ADD_PARTICIPANT_TO_PROJECT } from '../../../../../../graphql/Todo/mutation';
import { DO_SEARCH_PARTICIPANT } from '../../../../../../graphql/Todo/query';
import {
  ADD_PARTICIPANT,
  ADD_PARTICIPANTVariables,
} from '../../../../../../graphql/Todo/types/ADD_PARTICIPANT';
import {
  SEARCH_PARTCIPANT,
  SEARCH_PARTCIPANTVariables,
} from '../../../../../../graphql/Todo/types/SEARCH_PARTCIPANT';
import { getUser } from '../../../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import { CustomModal } from '../../../../../Common/CustomModal';
import { UserSelect } from '../../../../../User/UserSelect';
import { useStyles } from './styles';

interface UsersModalProps {
  openModalParticipant: boolean;
  setOpenParticipant: (opne: boolean) => any;
  userIds?: string[];
  setUserIds?: (value: any) => void;
  idProjectTask?: string;
  idEtiquette?: string;
  ajouteTache?: boolean;
  createUpdateActionEquipe?: (value: any) => void;
  disableAllFilter?: boolean;
}

const AjoutParticipant: FC<UsersModalProps & RouteComponentProps> = ({
  openModalParticipant,
  setOpenParticipant,
  userIds,
  setUserIds,
  idProjectTask,
  idEtiquette,
  ajouteTache,
  createUpdateActionEquipe,
  disableAllFilter,
}) => {
  const title = `Ajout des personnes`;
  const classes = useStyles({});
  const client = useApolloClient();

  const [userIdsSelected, setUserIdsSelected] = useState<any[]>(
    userIds && userIds.length ? userIds : [],
  );

  const currentUser: ME_me = getUser();

  // mutation add participant
  const {
    content: { variables },
  } = useContext<ContentStateInterface>(ContentContext);

  const [getProject, resultProject] = useLazyQuery<PROJECT, PROJECTVariables>(GET_PROJECT);

  React.useEffect(() => {
    if (idProjectTask && openModalParticipant) {
      getProject({
        variables: {
          id: idProjectTask,
        },
      });
    }
  }, [idProjectTask, openModalParticipant]);

  React.useEffect(() => {
    if (
      resultProject &&
      resultProject.data &&
      resultProject.data.project &&
      resultProject.data.project.participants &&
      resultProject.data.project.participants.length
    ) {
      setUserIdsSelected(
        resultProject.data.project.participants.map(participant => participant.id) || [],
      );
    } else {
      setUserIdsSelected(userIds || []);
    }
  }, [resultProject, userIds]);

  const [addParticipant] = useMutation<ADD_PARTICIPANT, ADD_PARTICIPANTVariables>(
    ADD_PARTICIPANT_TO_PROJECT,
    {
      update: (cache, { data }) => {
        if (
          data &&
          data.addParticipantsToProject &&
          data.addParticipantsToProject.participants &&
          data.addParticipantsToProject.participants.length > 0
        ) {
          const req = cache.readQuery<SEARCH_PARTCIPANT, SEARCH_PARTCIPANTVariables>({
            query: DO_SEARCH_PARTICIPANT,
            variables,
          });
          if (req && req.search && req.search.data) {
            const source = req.search.data || [];
            const result = data.addParticipantsToProject;
            const concat = [...source, result];
            cache.writeQuery({
              query: DO_SEARCH_PARTICIPANT,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: concat,
                  },
                },
              },
              variables,
            });
          }
        }
      },
      onCompleted: data => {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Participant ajouté avec succès',
        });
      },
      onError: error => {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );

  const [doAddParticipantsToEtiquette] = useMutation<
    ADD_PARTICIPANTS_TO_ETIQUETTE,
    ADD_PARTICIPANTS_TO_ETIQUETTEVariables
  >(DO_ADD_PARTICIPANTS_TO_ETIQUETTE, {
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const handleConfirm = () => {
    //ajout tache + participant
    if (ajouteTache) {
      createUpdateActionEquipe && createUpdateActionEquipe(userIdsSelected);
      setOpenParticipant(!openModalParticipant);
    }

    // Add participant to etiquette
    if (idEtiquette) {
      doAddParticipantsToEtiquette({
        variables: { input: { idEtiquette, idsParticipants: userIdsSelected || [] } },
      });
    }

    // addparticipant selected
    if (idProjectTask) {
      addParticipant({
        variables: { input: { idProject: idProjectTask, idsParticipants: userIdsSelected || [] } },
      });
    }

    if (userIdsSelected && setUserIds) {
      setUserIds(userIdsSelected);
      setOpenParticipant(false);
    }
  };

  const handleClose = (open: boolean) => {
    if (userIds) setUserIdsSelected(userIds && userIds.length ? userIds : []);
    setOpenParticipant(open);
  };

  return (
    <CustomModal
      open={openModalParticipant}
      setOpen={handleClose}
      title={title}
      withBtnsActions={true}
      closeIcon={true}
      headerWithBgColor={true}
      fullWidth={true}
      className={classes.usersModalRoot}
      centerBtns={true}
      onClickConfirm={handleConfirm}
      actionButton="Ajouter"
      withCancelButton={false}
    >
      <UserSelect
        userIdsSelected={userIdsSelected}
        setUserIdsSelected={setUserIdsSelected}
        filtersSelected={
          (currentUser.userTitulaire && currentUser.userTitulaire.id) ||
          (currentUser.userPpersonnel && currentUser.userPpersonnel.id)
            ? ['PHARMACIE', 'MY_PHARMACIE']
            : ['ALL']
        }
        disableAllFilter={true}
        activeAllContact={
          currentUser.role &&
          currentUser.role.code &&
          [ADMINISTRATEUR_GROUPEMENT, SUPER_ADMINISTRATEUR].includes(currentUser.role.code)
            ? true
            : false
        }
      />
    </CustomModal>
  );
};
export default withRouter(AjoutParticipant);
