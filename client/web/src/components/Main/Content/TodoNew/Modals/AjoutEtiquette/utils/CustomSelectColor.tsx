import React, { FC, useState } from 'react';
import { withStyles, createStyles, Theme } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import classnames from 'classnames';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';

const styles = (theme: Theme) =>
  createStyles({
    formControl: {
      width: '100%',
      marginBottom: 16,
      '& .MuiSelect-selectMenu': {
        minHeight: '30px !important',
        display: 'flex',
        alignItems: 'center',
      },
      '& label': {
        '& .MuiFormLabel-asterisk': {
          color: 'red',
        },
      },
      '&:hover label, & label.Mui-focused': {
        color: theme.palette.primary.main,
      },
      '&.Mui-focused fieldset': {
        border: `2px solid ${theme.palette.primary.main} !important`,
      },
      '& input:invalid + fieldset': {
        borderColor: 'red',
      },
      '& .Mui-focused .MuiIconButton-label svg': {
        color: `${theme.palette.primary.main} !important`,
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: theme.palette.common.white,
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          // borderColor: theme.palette.common.black,
        },
        '&:hover fieldset': {
          borderColor: theme.palette.primary.main,
        },
        '&.Mui-focused fieldset': {
          borderColor: theme.palette.primary.main,
        },
      },
      transition:
        'padding-left 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms,border-color 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms,border-width 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms',
    },
    label: {
      background: theme.palette.common.white,
      // color: theme.palette.common.black,
      padding: '0 4px',
    },
    noLabel: {
      '& fieldset > legend > span': {
        display: 'none',
      },
    },
  });

const CustomSelect: FC<any> = ({ ...props }) => {
  const {
    classes,
    label,
    index,
    listId,
    list,
    error,
    variant,
    shrink,
    required,
    withNoneValue,
    placeholder,
    withPlaceholder,
    disabled,
  } = props;

  return (
    <FormControl
      required={required}
      className={!label ? classnames(classes.formControl, classes.noLabel) : classes.formControl}
      variant={variant}
      error={error}
      disabled={disabled}
    >
      <InputLabel shrink={shrink} htmlFor="name" className={classes.label}>
        {label}
      </InputLabel>
      <Select
        {...props}
        className={classes.select}
        MenuProps={{
          getContentAnchorEl: null,
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'left',
          },
        }}
      >
        {list &&
          list.length > 0 &&
          list.map(item => (
            <MenuItem value={item.id}>
              <FiberManualRecordIcon
                style={{
                  marginRight: '10px',
                }}
                htmlColor={item && item.code ? item.code : 'gray'}
              />
              {item.libelle}
            </MenuItem>
          ))}
      </Select>
    </FormControl>
  );
};
export default withStyles(styles, { withTheme: true })(CustomSelect);

CustomSelect.defaultProps = {
  list: [],
  label: '',
  variant: 'outlined',
  error: false,
  shrink: true,
  required: false,
  withNoneValue: false,
  withPlaceholder: false,
};
