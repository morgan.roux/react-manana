import React, { ChangeEvent, useEffect, useState } from 'react';
import { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { CustomModal } from '../../../../../Common/CustomModal';
import CustomSelect from '../../../../../Common/CustomSelect';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import { useStyles } from './styles';
import Switch from '@material-ui/core/Switch';
import { Button, Grid, Typography } from '@material-ui/core';
import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import { GET_COULEURS } from '../../../../../../graphql/Couleurs/query';
import { couleurs } from '../../../../../../graphql/Couleurs/types/couleurs';
import { DO_CREATE_UPDATE_ETIQUETTE } from '../../../../../../graphql/Etiquette/mutation';
import {
  CREATE_UPDATE_TODO_ETIQUETTE,
  CREATE_UPDATE_TODO_ETIQUETTEVariables,
} from '../../../../../../graphql/Etiquette/types/CREATE_UPDATE_TODO_ETIQUETTE';
import { GET_SEARCH_ETIQUETTE } from '../../../../../../graphql/Etiquette/query';
import {
  SEARCH_ETIQUETTE,
  SEARCH_ETIQUETTEVariables,
  SEARCH_ETIQUETTE_search_data_TodoEtiquette,
} from '../../../../../../graphql/Etiquette/types/SEARCH_ETIQUETTE';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import Backdrop from '../../../../../Common/Backdrop';
import CustomSelectColor from './utils/CustomSelectColor';
import { operationVariable } from '../../MainContent/MainContent';
interface UsersModalProps {
  open: boolean;
  handleOpen: (value: boolean) => any;
  projectQueryVariable?: any;
  etiquette: any;
  resetEtiquette?: () => void;
  operationName?: string;
}

interface FormStateInterface {
  nom: string;
  idCouleur: string;
  isFavoris: boolean;
}

const AjoutEtiquette: FC<UsersModalProps & RouteComponentProps> = props => {
  const {
    open,
    handleOpen,
    projectQueryVariable,
    etiquette,
    operationName,
    resetEtiquette,
  } = props;
  const classes = useStyles({});
  const defaultFormState = {
    nom: '',
    idCouleur: '',
    isFavoris: false,
  };
  const [formState, setFormState] = useState<FormStateInterface>(defaultFormState);

  const client = useApolloClient();
  const handleChange = e => {
    const { name, value } = e.target;
    setFormState(prevState => ({ ...prevState, [name]: value }));
  };

  const handleFavorise = () => {
    setFormState(prevState => ({ ...prevState, isFavoris: !prevState.isFavoris }));
  };

  const { loading, data } = useQuery<couleurs>(GET_COULEURS, { fetchPolicy: 'cache-and-network' });

  const couleurList =
    (data &&
      data.couleurs &&
      data.couleurs.filter(item => {
        if (!item.isRemoved) {
          return { id: item.id, libelle: item.libelle, code: item.code };
        }
      })) ||
    [];

  const emptyEtiquette = {
    id: '',
    nom: '',
    idCouleur: (etiquette && etiquette.couleur && etiquette.couleur.idCouleur) || 1,
    isFavoris: false,
  };

  // console.log('PREV-ETIQUETTE', prevEtiquette && prevEtiquette.ordre);
  // console.log('NEXT-ETIQUETTE', nextEtiquette && nextEtiquette.ordre);
  useEffect(() => {
    if (!open && resetEtiquette) {
      setFormState(defaultFormState);
      resetEtiquette();
    }
  }, [open]);

  useEffect(() => {
    if (etiquette && operationName) {
      if (operationName === operationVariable.edit) {
        const etiqueteValueInitialState = {
          id: etiquette && etiquette.id,
          nom: etiquette && etiquette._name,
          idCouleur: (etiquette && etiquette.couleur && etiquette.couleur.id) || 1,
          isFavoris: (etiquette && etiquette.isInFavoris) || false,
        };
        setFormState(etiqueteValueInitialState);
      }
      if (operationName === operationVariable.addUpper) {
        const etiqueteValueInitialState = {
          ...emptyEtiquette,
          ordre: etiquette && etiquette.ordre === 1 ? 1 : etiquette.ordre - 1,
        };
        setFormState(etiqueteValueInitialState);
      }
      if (operationName === operationVariable.addDown) {
        const etiqueteValueInitialState = {
          ...emptyEtiquette,
          ordre: etiquette && etiquette.ordre + 1,
        };
        setFormState(etiqueteValueInitialState);
      }
    }
  }, [etiquette, operationName]);

  const isEdit: boolean = operationName === operationVariable.edit ? true : false;
  const successMessage = isEdit
    ? `Etiquette modifiée avec succès`
    : `Etiquette ajoutée avec succès`;

  const [save, { error, loading: updateEtiquetteLoading }] = useMutation<
    CREATE_UPDATE_TODO_ETIQUETTE & CREATE_UPDATE_TODO_ETIQUETTEVariables
  >(DO_CREATE_UPDATE_ETIQUETTE, {
    update: (cache, { data }) => {
      if (data && data.createUpdateTodoEtiquette) {
        const req = cache.readQuery<SEARCH_ETIQUETTE, SEARCH_ETIQUETTEVariables>({
          query: GET_SEARCH_ETIQUETTE,
          variables: projectQueryVariable,
        });
        if (req && req.search && req.search.data) {
          if (req && req.search && req.search.data) {
            if (!isEdit) {
              cache.writeQuery({
                query: GET_SEARCH_ETIQUETTE,
                data: {
                  search: {
                    ...req.search,
                    ...{
                      total: req.search.total + 1,
                      data: [...req.search.data, data.createUpdateTodoEtiquette],
                    },
                  },
                },
                variables: projectQueryVariable,
              });
            }
            displaySnackBar(client, {
              type: 'SUCCESS',
              message: successMessage,
              isOpen: true,
            });
            setFormState(emptyEtiquette);
            handleOpen(false);
          }
        }
      }
    },
  });

  const handleSubmit = e => {
    save({
      variables: { input: { ...formState } },
    });
  };
  const { nom, idCouleur } = formState;
  const fieldValidator = () => {
    if (!nom) {
      return true;
    }
    if (!idCouleur) {
      return true;
    }
  };

  return (
    <CustomModal
      open={open}
      setOpen={handleOpen}
      title={isEdit ? `Modification d'étiquette` : `Ajout d'étiquette`}
      withBtnsActions={true}
      closeIcon={true}
      headerWithBgColor={true}
      maxWidth="sm"
      fullWidth={true}
      className={classes.usersModalRoot}
      centerBtns={true}
      actionButton={isEdit ? `Modifier` : `Ajouter`}
      onClickConfirm={handleSubmit}
      disabledButton={fieldValidator()}
      withCancelButton={false}
    >
      {<Backdrop open={updateEtiquetteLoading} />}
      <div className={classes.tableContainer}>
        <CustomFormTextField
          label="Nom"
          name="nom"
          placeholder="Nom "
          required={true}
          className={classes.espaceHaut}
          onChange={handleChange}
          value={formState.nom}
          autoFocus={true}
        />
        <CustomSelectColor
          label="Couleur de l'étiquette"
          list={couleurList}
          name="idCouleur"
          className={classes.espaceHaut}
          onChange={handleChange}
          required={true}
          listId="id"
          index="libelle"
          value={formState.idCouleur}
        />
        <div className={classes.espacedroite}>
          <Typography>Ajouter au favoris</Typography>
          <Switch
            checked={formState.isFavoris}
            name="isFavoris"
            inputProps={{ 'aria-label': 'secondary checkbox' }}
            onChange={handleFavorise}
          />
        </div>
      </div>
    </CustomModal>
  );
};
export default withRouter(AjoutEtiquette);
