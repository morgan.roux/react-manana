import React,{useState} from 'react';
import { RouteChildrenProps, RouteComponentProps, withRouter } from 'react-router';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CustomButton from '../../../../Common/CustomButton';
import { Theme,createStyles, makeStyles } from '@material-ui/core/styles';

interface Menuprops {
    title?: string;
    handleChange:(e:React.ChangeEvent<any>)=>void;
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    
   
  }),
);
const Modals:React.FC<Menuprops & RouteComponentProps> = props =>{
    const{
        title,
        handleChange
    }=props;
    const [open, setOpen] = useState(true);

    const handleClickOpen = () => {
        setOpen(true);
    };
        const handleClose = () => {
        setOpen(false);
    };
    return (<div>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="draggable-dialog-title"
            >
                <AppBar position="static">
                    <Toolbar>
                        <Typography variant="h6" >
                            {title}
                        </Typography >
                        <Button color="inherit" onClick={handleClose}>x</Button>
                    </Toolbar>
                </AppBar>
                <DialogContent>
                    <DialogContentText>
                        {props.children}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                        <CustomButton color="secondary" onClick={handleChange}>
                            Ajouter
                        </CustomButton>
                        <CustomButton color="default" onClick={handleClose}>
                            Annuler
                        </CustomButton>
                </DialogActions>
            </Dialog>
        </div>
       
    );
    
}

export default withRouter(Modals);






