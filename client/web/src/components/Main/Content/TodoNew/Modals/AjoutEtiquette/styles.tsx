import { createStyles, Theme } from '@material-ui/core/styles';
import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    usersModalRoot: {
      '& .MuiDialogActions-root .MuiButtonBase-root': {
        width: 219,
        height: 50,
      },
      '& .MuiDialogActions-root': {
        [theme.breakpoints.down('md')]: {
          width: '100vw',
          padding: 8,
          justifyContent: 'left',
        },
      },
      [theme.breakpoints.down('md')]: {
        '& .MuiDialog-scrollPaper': {
          justifyContent: 'left',
        },
        '& .MuiDialog-paperFullWidth': {
          width: '100%',
          maxWidth: '100%',
          minHeight: '100%',
        },
        '& .MuiDialog-paper': {
          margin: 0,
        },
        '& .MuiDialogContent-root': {
          padding: 0,
        },
        '& .MuiDialogTitle-root': {
          background: theme.palette.secondary.main,
        },
      },
    },
    tableContainer: {
      width: '100%',
      [theme.breakpoints.down('md')]: {
        padding: '25px 8px 0px 8px',
      },
    },
    espaceHaut: {
      margin: '50px 0',
    },
    espacedroite: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
  }),
);
