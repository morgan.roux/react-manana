import { useApolloClient, useLazyQuery, useMutation } from '@apollo/react-hooks';
import { Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Delete, Edit, MoreHoriz } from '@material-ui/icons';
import PlaylistAddIcon from '@material-ui/icons/PlaylistAdd';
import React, { useState } from 'react';
import { DO_DELETE_ACTIONS } from '../../../../../../../graphql/ActionNew/mutation';
import {
  DELETE_ACTIONS,
  DELETE_ACTIONSVariables,
} from '../../../../../../../graphql/ActionNew/types/DELETE_ACTIONS';
import { GET_UPDATE_ITEM_SCORING_FOR_ITEM } from '../../../../../../../federation/tools/itemScoring/query';
import {
  UPDATE_ITEM_SCORING_FOR_ITEM,
  UPDATE_ITEM_SCORING_FOR_ITEMVariables,
} from '../../../../../../../federation/tools/itemScoring/types/UPDATE_ITEM_SCORING_FOR_ITEM';
import { getUser } from '../../../../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';
import Backdrop from '../../../../../../Common/Backdrop';
import { ConfirmDeleteDialog } from '../../../../../../Common/ConfirmDialog';
import { ActionButtonMenu } from '../../../Common/ActionButton';
import AjoutTaches from '../../../Modals/AjoutTache';
import { CountTodosProps } from '../../../TodoNew';
import { isManuelAction } from '../../../util';

import { operationVariable } from '../../MainContent';
import { FEDERATION_CLIENT } from '../../../../../../Dashboard/DemarcheQualite/apolloClientFederation';
interface TaskListActionButtonProps {
  task: any;
  refetch: any;
  refetchEtiquette: any;
  refetchProject: any;
  parameters: any;
  refetchAll?: () => void;
}
const TaskListActionButton: React.FC<TaskListActionButtonProps & CountTodosProps> = props => {
  const {
    task,
    refetch,
    refetchCountTodos,
    refetchEtiquette,
    refetchProject,
    parameters,
    refetchAll,
  } = props;

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = anchorEl ? true : false;
  const [openModalTache, setOpenModalTache] = useState<boolean>(false);
  const [openDeleteDialogTask, setOpenDeleteDialogTask] = useState<boolean>(false);
  const [operationName, setOperationName] = useState<string>('');

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    if (event) {
      event.stopPropagation();
      setAnchorEl(event.currentTarget);
    }
  };

  const user = getUser();

  const assigns: any[] =
    task && task.assignedUsers && task.assignedUsers.length
      ? task.assignedUsers.map(user => user.id)
      : [];
  const idCreation = task && task.userCreation ? task.userCreation.id : '';

  const handlesetOpenDialogDelete = () => {
    setOpenDeleteDialogTask(!openDeleteDialogTask);
  };

  const client = useApolloClient();

  const handleClose = () => {
    setAnchorEl(null);
  };

  const { addDown, addSubs, edit, addUpper } = operationVariable;
  const handleOpenModalTacheAction = () => {
    setOpenModalTache(!openModalTache);
    setOperationName(edit);
  };

  const handleOpenAddSubTask = () => {
    setOpenModalTache(!openModalTache);
    setOperationName(addSubs);
  };
  const handleOpenAddUpperTask = () => {
    setOpenModalTache(!openModalTache);
    setOperationName(addUpper);
  };
  const handleOpenAddDownTask = () => {
    setOpenModalTache(!openModalTache);
    setOperationName(addDown);
  };

  const [updateItemScoring, updatingItemScoring] = useLazyQuery<
    UPDATE_ITEM_SCORING_FOR_ITEM,
    UPDATE_ITEM_SCORING_FOR_ITEMVariables
  >(GET_UPDATE_ITEM_SCORING_FOR_ITEM, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
  });

  const [removeTache, { loading: loadingDelete }] = useMutation<
    DELETE_ACTIONS,
    DELETE_ACTIONSVariables
  >(DO_DELETE_ACTIONS, {
    onCompleted: data => {
      updateItemScoring({
        variables: {
          idItemAssocie: data.softDeleteActions[0].id,
          codeItem: 'L',
        },
      });
      displaySnackBar(client, {
        isOpen: true,
        type: 'SUCCESS',
        message: 'Tâche supprimée avec succés.',
      });
      refetch();
      if (refetchCountTodos) {
        refetchCountTodos();
      }
      if (refetchEtiquette) {
        refetchEtiquette();
      }
      if (refetchProject) {
        refetchProject();
      }
    },
  });

  const onClickConfirmDeleteTask = () => {
    removeTache({
      variables: {
        ids: task && task.id,
      },
    });
    setOpenDeleteDialogTask(!openDeleteDialogTask);
  };

  const tasklistMenuItems: ActionButtonMenu[] = [
    /*{
      label: 'Créer une sous-tâche',
      icon: <AddIcon />,
      onClick: () => handleOpenAddSubTask(),
      disabled: false,
      hide:
        task && task.actionParent
          ? true
          : parameters &&
            parameters.length &&
            parameters.filter(parameter => parameter.code === '0120' && parameter.value === 'true')
              .length
          ? false
          : true,
    },
    {
      label: 'Ajouter une tâche au-dessus',
      icon: <PlaylistAddIcon />,
      onClick: () => handleOpenAddUpperTask(),
      disabled: task && task.actionParent ? true : false,
      hide: task && task.actionParent ? true : false,
    },
    {
      label: 'Ajouter une tâche en-dessous',
      icon: <PlaylistAddIcon />,
      onClick: () => handleOpenAddDownTask(),
      disabled: task && task.actionParent ? true : false,
      hide: task && task.actionParent ? true : false,
    },
    {
      label: 'Déplacer',
      icon: <ExitToAppIcon />,
      onClick: () => handleOpenAddDownTask(),
      disabled: false,
    }, */
    {
      label: 'Modifier',
      icon: <Edit />,
      onClick: () => handleOpenModalTacheAction(),
      disabled: false,
      hide: !isManuelAction(task),
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: handlesetOpenDialogDelete,
      disabled:
        user && user.id && (assigns.includes(user.id) || idCreation === user.id) ? false : true,
      hide: !isManuelAction(task),
    },
  ];

  return (
    <>
      {loadingDelete && <Backdrop value="Suppression en cours, veuillez patienter..." />}
      <ConfirmDeleteDialog
        open={openDeleteDialogTask}
        setOpen={handlesetOpenDialogDelete}
        onClickConfirm={onClickConfirmDeleteTask}
      />
      <IconButton aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        <MoreHoriz />
      </IconButton>
      <Menu
        // tslint:disable-next-line: jsx-no-lambda
        onClick={event => {
          event.preventDefault();
          event.stopPropagation();
        }}
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        {tasklistMenuItems &&
          tasklistMenuItems.length > 0 &&
          tasklistMenuItems.map(
            (i: ActionButtonMenu, index: number) =>
              !i.hide && (
                <MenuItem
                  // tslint:disable-next-line: jsx-no-lambda
                  onClick={event => {
                    event.preventDefault();
                    event.stopPropagation();
                    handleClose();
                    i.onClick(event);
                  }}
                  key={`table_menu_item_${index}`}
                  disabled={i.disabled}
                >
                  <ListItemIcon>{i.icon}</ListItemIcon>
                  <Typography variant="inherit">{i.label}</Typography>
                </MenuItem>
              ),
          )}
      </Menu>
      <AjoutTaches
        openModalTache={openModalTache}
        setOpenTache={setOpenModalTache}
        task={task}
        operationName={operationName}
        refetchCountTodos={refetchCountTodos}
        refetchTasks={refetch}
        refetchEtiquette={refetchEtiquette}
        refetchProject={refetchProject}
        refetchAll={refetchAll}
      />
    </>
  );
};

export default TaskListActionButton;
