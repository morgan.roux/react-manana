import { Box, Icon, IconButton, Typography } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import { ChatBubble, Dashboard, EventNote, Repeat, Today } from '@material-ui/icons';
import BlockIcon from '@material-ui/icons/Block';
import CircleChecked from '@material-ui/icons/CheckCircleOutline';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import { ApolloQueryResult } from 'apollo-client';
import classnames from 'classnames';
import moment from 'moment';
import React, { useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import PrivateIcon from '../../../../../../../../assets/icons/todo/private.svg';
import {
  SEARCH_ACTION,
  SEARCH_ACTIONVariables,
  SEARCH_ACTION_search_data_Action,
} from '../../../../../../../../graphql/Action/types/SEARCH_ACTION';
import { ME_me } from '../../../../../../../../graphql/Authentication/types/ME';
import { nl2br } from '../../../../../../../../services/Html';
import { getUser } from '../../../../../../../../services/LocalStorage';
import { ActionStatus } from '../../../../../../../../types/graphql-global-types';
import { useValueParameterAsBoolean } from '../../../../../../../../utils/getValueParameter';
import CustomAvatarGroup from '../../../../../../../Common/CustomAvatarGroup';
import UrgenceImportanceBadge from '../../../../../../../Common/UrgenceImportanceBadge/UrgenceImportanceBadge';
import { CountTodosProps } from '../../../../TodoNew';
import {
  isManuelAction,
  isReunionQualiteAction,
  isTraitementAutomatiqueAction,
  isTradeMarketingAction,
} from '../../../../util';
import { computeCodeFromdateDebut } from './../../../../Common/UrgenceLabel/UrgenceLabel';
import ActionButton from './../TaskListActionButton';
import useStyles from './styles';
import TaskContentPopover from './TaskContentPopover';
import TradeMarketingIcon from './../../../../../../../../assets/icons/todo/otm.jpeg';

interface TaskListProps {
  task: any;
  onClickCheckbox: (value: any) => (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  handleOpenModal?: (
    task: SEARCH_ACTION_search_data_Action,
  ) => (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  refetch?: (
    variables?: SEARCH_ACTIONVariables | undefined,
  ) => Promise<ApolloQueryResult<SEARCH_ACTION>>;
  refetchEtiquette: any;
  refetchProject: any;
  parameters: any;
  isSubTask?: boolean;
  refetchAll?: () => void;
  setOpenTaskDetail?: (openTaskDetail: boolean) => void;
}

const Task: React.FC<TaskListProps & RouteComponentProps & CountTodosProps> = ({
  task,
  onClickCheckbox,
  handleOpenModal,
  refetch,
  refetchCountTodos,
  refetchEtiquette,
  refetchProject,
  location: { pathname },
  parameters,
  isSubTask,
  refetchAll,
  setOpenTaskDetail,
}) => {
  const classes = useStyles({});
  const me: ME_me = getUser();
  /* const firstName = task && task.userCreation && task.userCreation.userName ? task.userCreation.userName ?.split(' ')[0] : '-'; */
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);

  const isTM = task?.item?.code === 'PLAN_MARKETING_TYPE';

  const [openPopover, setOpenPopover] = useState<boolean>(false);

  const [popoverItem, setPopoverItem] = useState<string>('');

  const [popoverSection, setPopoverSection] = useState<string>();

  const popoverId = openPopover ? popoverItem : undefined;

  const useMatriceResponsabilite = useValueParameterAsBoolean('0501');

  const disabled =
    (task.assignedUsers &&
      !task.assignedUsers.length &&
      task.userCreation &&
      me &&
      task.userCreation.id === me.id) ||
    (task.assignedUsers &&
      task.assignedUsers.length &&
      me &&
      task.assignedUsers.map(assign => assign.id).includes(me.id))
      ? false
      : !useMatriceResponsabilite;

  const isInboxTeam = pathname.includes('recu-equipe');

  const handleDateClick = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    //event.stopPropagation();
    setAnchorEl(event.currentTarget);
    setPopoverSection('DATE');
    //setOpenPopover(true);
  };

  const handlePriorityClick = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    //event.stopPropagation();
    setAnchorEl(event.currentTarget);
    setPopoverSection('PRIORITY');
    //setOpenPopover(true);
  };

  const handleEtiquetteClick = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    //event.stopPropagation();
    setAnchorEl(event.currentTarget);
    setPopoverSection('ETIQUETTE');
    //setOpenPopover(true);
  };

  const handleCommentClick = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    if (setOpenTaskDetail) setOpenTaskDetail(true);
    event.stopPropagation();
  };

  const bottomItems = (
    <Box className={classes.contentLabels}>
      {task.dateDebut && (
        <Box className={classes.labels} onClick={handleDateClick}>
          <Icon color="secondary">
            <Today />
          </Icon>
          <Typography className={classnames(classes.labelText, classes.date)}>
            {task.dateFin
              ? `du ${moment.utc(task.dateDebut).format('DD MMM')} au ${moment
                  .utc(task.dateFin)
                  .format('DD MMM')}`
              : moment.utc(task.dateDebut).format('DD MMM')}
          </Typography>
        </Box>
      )}
      <Box className={classes.labels}>
        <Icon onClick={handleCommentClick}>
          <ChatBubble />
        </Icon>
        <Typography className={classnames(classes.labelText)}>{task.nbComment}</Typography>
      </Box>
      <Box className={classes.labels}>
        <UrgenceImportanceBadge
          importanceOrdre={task?.importance?.ordre || 2}
          urgenceCode={computeCodeFromdateDebut(task.dateDebut)}
        />
      </Box>
      {/* <Box className={classes.labels} onClick={handlePriorityClick}>
        <AssistantPhotoIcon
          style={{
            color:
              task.priority === 1
                ? 'red'
                : task.priority === 2
                ? 'yellow'
                : task.priority === 3
                ? 'green'
                : 'gray',
          }}
        />
        <Typography className={classnames(classes.labelText)}>Priorité {task.priority}</Typography>
      </Box> */}

      {/*<Box className={classes.labels}>
        <UrgenceLabel dateDebut={task.dateDebut} />
      </Box>
      {task.etiquettes &&
        task.etiquettes.length > 0 &&
        task.etiquettes.map(etiquette => (
          <Box className={classes.labels} key={etiquette.id} onClick={handleEtiquetteClick}>
            <Icon>
              <Error
                htmlColor={etiquette && etiquette.couleur ? etiquette.couleur.code : '#C1CAD6'}
              />
            </Icon>
            <Typography className={classnames(classes.bold, classes.labelText)}>
              {etiquette.nom}
            </Typography>
          </Box>
        ))*/}
      {/*task.origine && task.origine.libelle && (
        <Box className={classes.labels}>
          <Typography className={classnames(classes.labelOrigine)}>Origine</Typography>:
          <Typography className={classnames(classes.labelText)}>{task.origine.libelle}</Typography>
        </Box>
      )*/}
      {task.project && task.project.name && (
        <Box className={classes.labels}>
          <Typography className={classnames(classes.labelOrigine)}>Fonction</Typography>:
          <Typography className={classnames(classes.labelText)}>{task.project.name}</Typography>
        </Box>
      )}
      {task.isPrivate && (
        <Box className={classes.labels}>
          <img src={PrivateIcon} style={{ width: 28 }} />
        </Box>
      )}
      {isTM && (
        <Box className={classes.labels}>
          <Dashboard />
        </Box>
      )}
      {isTraitementAutomatiqueAction(task) ? (
        <Box className={classes.labels}>
          <Repeat />
        </Box>
      ) : isReunionQualiteAction(task) ? (
        <Box className={classes.labels}>
          <EventNote />
        </Box>
      ) : isTradeMarketingAction(task) ? (
        <Box className={classes.labels}>
          <img style={{ width: 80 }} src={TradeMarketingIcon} />
        </Box>
      ) : null}
      {/*<Box className={classes.labels}>
        <Typography className={classnames(classes.labelOrigine)}>Crée le:</Typography>:
        <Typography className={classnames(classes.labelText)}>
          {moment(task.dateCreation).format('DD MMM YYYY')}
        </Typography>
      </Box>*/}
      {/*task.userCreation && task.userCreation.userName && (
        <Box className={classes.labels}>
          <Typography className={classnames(classes.labelOrigine)}>Crée par:</Typography>:
          <Typography className={classnames(classes.labelText)}>
            {task.userCreation && task.userCreation.userName}
          </Typography>
        </Box>
      )*/}
    </Box>
  );

  return (
    task && (
      <>
        <Box className={!isSubTask ? classes.tasks : classes.subTasks}>
          <Box display="flex" justifyContent="space-between" alignItems="center" width="100%">
            <Box display="flex" alignItems="center">
              <Checkbox
                icon={
                  disabled ? <BlockIcon fontSize="large" /> : <CircleUnchecked fontSize="large" />
                }
                checkedIcon={<CircleChecked fontSize="large" />}
                onClick={onClickCheckbox(task)}
                checked={task.status === ActionStatus.DONE}
                disabled={disabled}
              />
              <Typography
                className={classes.taskTitle}
                dangerouslySetInnerHTML={{ __html: nl2br(task.description) } as any}
              />
            </Box>

            <Box
              display="flex"
              justifyContent="center"
              alignItems="center"
              // tslint:disable-next-line: jsx-no-lambda
              onClick={event => {
                event.stopPropagation();
                event.preventDefault();
              }}
            >
              <Box
                onClick={event => {
                  event.stopPropagation();
                  event.preventDefault();
                }}
                display="flex"
              >
                {
                  <IconButton
                    onClick={
                      isManuelAction(task) && handleOpenModal ? handleOpenModal(task) : undefined
                    }
                  >
                    {task.assignedUsers && task.assignedUsers.length > 0 ? (
                      <CustomAvatarGroup users={task.assignedUsers} max={5} />
                    ) : isManuelAction(task) ? (
                      <PersonAddIcon />
                    ) : null}
                  </IconButton>
                }

                <ActionButton
                  task={task}
                  refetch={refetch}
                  refetchCountTodos={refetchCountTodos}
                  refetchEtiquette={refetchEtiquette}
                  refetchProject={refetchProject}
                  parameters={parameters}
                  refetchAll={refetchAll}
                />
              </Box>
            </Box>
          </Box>
          {bottomItems}
        </Box>
        <TaskContentPopover
          id={popoverId}
          anchorEl={anchorEl}
          open={openPopover}
          setOpen={setOpenPopover}
          section={popoverSection}
          task={task}
        />
      </>
    )
  );
};

export default withRouter(Task);
