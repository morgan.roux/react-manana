import { Edit } from '@material-ui/icons';
import RetirerFavoris from '../../../../../../../assets/icons/todo/retirer_favoris.svg';
import { ActionButtonMenu } from '../../../Common/ActionButton';
import React from 'react';
export const useFavorisList = (
  editFavoriteClick: (row: any) => void,
  removeFavoriteClick: (row: any) => void,
  setProjecToEdit: (row: any) => void,
  useMatriceResponsabilite: boolean
) => {
  const favorisMenuItems: ActionButtonMenu[] = [
    {
      label: 'Modifier',
      icon: <Edit />,
      onClick: editFavoriteClick,
      setState: setProjecToEdit,
      hide:useMatriceResponsabilite,
      disabled: false,
    },
    {
      label: 'Retirer des favoris',
      icon: <img src={RetirerFavoris} />,
      onClick: removeFavoriteClick,
      disabled: false,
    },
  ];
  return [favorisMenuItems];
};
