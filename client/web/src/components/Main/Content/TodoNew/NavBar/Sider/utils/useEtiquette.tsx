import { Delete, Edit, Favorite } from '@material-ui/icons';
import AjouterDessous from '../../../../../../../assets/icons/todo/ajouter_dessous.svg';
import AjouterDessus from '../../../../../../../assets/icons/todo/ajouter_dessus.svg';
import EtiquettePartagee from '../../../../../../../assets/icons/todo/deplacer_etiquette_partage.svg';
import { ActionButtonMenu } from '../../../Common/ActionButton';
import React from 'react';
import { operationVariable } from '../../../MainContent/MainContent';

export const useEtiquetteList = (
  addUpperEtiquetteClick: (row: any) => void,
  addDownEtiquetteClick: (row: any) => void,
  editEtiquetteClick: (row: any) => void,
  addToFavoriteEtiquetteClick: (row: any) => void,
  shareEtiquetteClick: (row: any) => void,
  deleteEtiquetteClick: (row: any) => void,
  etiquette: any,
) => {
  const etiquetteMenuItems: ActionButtonMenu[] = [
    // {
    //   label: 'Ajouter une étiquette au-dessus',
    //   icon: <img src={AjouterDessus} />,
    //   onClick: addUpperEtiquetteClick,
    //   disabled: false,
    // },
    // {
    //   label: 'Ajouter une étiquette au-dessous',
    //   icon: <img src={AjouterDessous} />,
    //   onClick: addDownEtiquetteClick,
    //   disabled: false,
    // },
    // {
    //   label: 'Modifier',
    //   icon: <Edit />,
    //   onClick: editEtiquetteClick,
    //   disabled: false,
    // },
    {
      label: 'Ajouter au favoris',
      icon: <Favorite />,
      onClick: addToFavoriteEtiquetteClick,
      disabled: etiquette && etiquette.isInFavoris,
    },
    // {
    //   label: 'Déplacer vers étiquettes partagées',
    //   icon: <img src={EtiquettePartagee} />,
    //   onClick: shareEtiquetteClick,
    //   disabled: etiquette && etiquette.isShared,
    // },
    // {
    //   label: 'Supprimer',
    //   icon: <Delete />,
    //   onClick: deleteEtiquetteClick,
    //   disabled: etiquette && !etiquette.activeActions ? false : true,
    // },
  ];
  return [etiquetteMenuItems];
};
