import { Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { MoreHoriz } from '@material-ui/icons';
import React, { FC, ReactNode, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories } from '../../../../../../graphql/Parametre/types/PARAMETERS_GROUPES_CATEGORIES';
import { CountTodosProps } from '../../TodoNew';

export interface ActionButtonMenu {
  label: any;
  icon: ReactNode;
  disabled: boolean;
  onClick: (event) => any;
  setState?: (value: any) => void;
  hide?: boolean;
}

export interface ActionButtonProps {
  menuItems: ActionButtonMenu[];
  row?: any; // this will remain optional till we get the shared etiquette
  match: {
    params: {
      filter: string | undefined;
    };
  };
  setTriage?: (triage: any[]) => void;
  refetchSection?: any;
  setClickedRow: (row: any) => void;
}

const Actionbutton: FC<ActionButtonProps & RouteComponentProps & CountTodosProps> = ({
  row,
  menuItems,
  setClickedRow,
}) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = anchorEl ? true : false;
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    if (event) {
      event.stopPropagation();
      setAnchorEl(event.currentTarget);
      setClickedRow(row);
    }
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <>
      <IconButton aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        <MoreHoriz />
      </IconButton>
      <Menu
        // tslint:disable-next-line: jsx-no-lambda
        onClick={event => {
          event.preventDefault();
          event.stopPropagation();
        }}
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        {menuItems &&
          menuItems.length > 0 &&
          menuItems.map(
            (i: ActionButtonMenu, index: number) =>
              !i.hide && (
                <MenuItem
                  // tslint:disable-next-line: jsx-no-lambda
                  onClick={event => {
                    event.preventDefault();
                    event.stopPropagation();
                    handleClose();
                    i.onClick(event);
                    if (i.setState) {
                      i.setState(row);
                    }
                  }}
                  key={`table_menu_item_${index}`}
                  disabled={i.disabled}
                >
                  <ListItemIcon>{i.icon}</ListItemIcon>
                  <Typography variant="inherit">{i.label}</Typography>
                </MenuItem>
              ),
          )}
      </Menu>
    </>
  );
};

export default withRouter(Actionbutton);
