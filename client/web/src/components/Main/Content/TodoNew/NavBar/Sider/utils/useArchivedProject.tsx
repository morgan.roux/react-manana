import { Delete } from '@material-ui/icons';
import Archiver from '../../../../../../../assets/icons/todo/archiver.svg';
import { ActionButtonMenu } from '../../../Common/ActionButton';
import React from 'react';
export const useArchivedProjectList = (
  unarchiveProjectClick: (row: any) => void,
  deleteProjectClick: (row: any) => void,
) => {
  const archivedMenuItems: ActionButtonMenu[] = [
    {
      label: 'Désarchiver',
      icon: <img src={Archiver} />,
      onClick: unarchiveProjectClick,
      disabled: false,
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: deleteProjectClick,
      disabled: false,
    },
  ];
  return [archivedMenuItems];
};
