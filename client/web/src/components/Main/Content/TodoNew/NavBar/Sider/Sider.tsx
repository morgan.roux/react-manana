import {
  DateRange,
  Error,
  FiberManualRecord,
  Inbox,
  LocalOffer,
  MoveToInbox,
  SupervisedUserCircle,
  Today,
} from '@material-ui/icons';
import _, { concat, filter } from 'lodash';
import React, { useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { getUser } from '../../../../../../services/LocalStorage';
import { TypeProject } from '../../../../../../types/graphql-global-types';
import { capitalizeFirstLetter } from '../../../../../../utils/capitalizeFirstLetter';
import Backdrop from '../../../../../Common/Backdrop';
import ConfirmDialog, { ConfirmDeleteDialog } from '../../../../../Common/ConfirmDialog';
import DropdownDraggable from '../../Common/DropdownDraggable/';
import UrgenceFilter from '../../Common/UrgenceFilter';
import { UrgenceInterface } from '../../Common/UrgenceFilter/UrgenceFilter';
import ImportanceFilter from '../../Common/ImportanceFilter/ImportanceFilter';
import { ImportanceInterface } from '../../Common/ImportanceFilter/ImportanceFilterList';
import { useDeleteEtiquette, useUpdateEtiquette } from '../../Common/utils/useEtiquette';
import { useDeleteProject, useUpdateProject } from '../../Common/utils/useProject';
import { defaultDateTerm, operationVariable } from '../../MainContent/MainContent';
import AjoutEtiquette from '../../Modals/AjoutEtiquette/AjoutEtiquette';
import AjoutParticipant from '../../Modals/AjoutParticipants';
import AjoutProjet from '../../Modals/AjoutProjet/AjoutProjet';
import { CountTodosProps } from '../../TodoNew';
import FavorisHeader from '../FavorisHeader';
import { filterSubProjects } from './../../util';
import ActionButton from './SiderActionButton';
import { useArchivedProjectList } from './utils/useArchivedProject';
import { useEtiquetteList } from './utils/useEtiquette';
import { useFavorisList } from './utils/useFavoris';
import { useProjectList } from './utils/useProject';
import { useSharedEtiquetteList } from './utils/useSharedEtiquette';
import { useNiveauMatriceFonctions } from '../../../../../Dashboard/DemarcheQualite/MatriceTache/hooks';

const computeProjetTypeLabel = (project: any, useMatriceResponsabilite: boolean): string => {
  return useMatriceResponsabilite
    ? ''
    : project && project.typeProject === TypeProject.GROUPEMENT
    ? 'Groupe'
    : project.typeProject === TypeProject.PROFESSIONNEL
    ? 'Pro'
    : project.typeProject === TypeProject.PERSONNEL
    ? 'Perso'
    : '';
};

interface SidebarProps {
  projectList: any;
  setActionListFilters: (value: any) => void;
  etiquetteList?: any;
  setClickedRow?: (value: any) => void;
  row?: any;
  parameters: any[];
  match: {
    params: {
      filterId: string;
    };
  };
  setShowMainContent?: (value: boolean) => void;
  menuFilters?: any;
  termologie: string;
  useMatriceResponsabilite: boolean;
  urgence: UrgenceInterface[] | null | undefined;
  setUrgence: (urgence: UrgenceInterface[] | null | undefined) => void;
  importances: ImportanceInterface[] | null | undefined;
  setImportances: (importances: ImportanceInterface[] | null | undefined) => void;
  setTriage: (triage: any) => void;
}

const Sider: React.FC<SidebarProps & RouteComponentProps & CountTodosProps> = ({
  setActionListFilters,
  history: { push },
  location: { pathname },
  match: {
    params: { filterId },
  },
  refetchCountTodos,
  countTodos,
  etiquetteList,
  projectList,
  parameters,
  setShowMainContent,
  menuFilters,
  termologie,
  useMatriceResponsabilite,
  urgence,
  setUrgence,
  importances,
  setImportances,
  setTriage,
}) => {
  const [openAddProject, setOpenAddProject] = useState<boolean>(false);
  const [openEtiquette, setOpenEtiquette] = useState<boolean>(false);
  const handleAddProject = () => {
    setOpenAddProject(!openAddProject);
  };

  const handleAddEtiquette = () => {
    setOpenEtiquette(!openEtiquette);
  };

  const handleNextWeek = () => {
    // setActionListFilters({ next: true });
    setActionListFilters(defaultDateTerm);
    setTriage([{ priority: { order: 'asc' } }]);
  };

  const handleFavorisClick = (favoris: any) => {
    push(`/todo/${favoris.path}`);
    if (favoris.onClick) {
      favoris.onClick();
    }
  };

  const handleProjectClick = (project: any) => {
    push(`/todo/projet/${project.id}`);
    if (project.onClick) {
      project.onClick();
    }
  };

  const handleSubProjectClick = (subProjects: any) => {
    // console.log('subs id', subProjects && subProjects.id);
    push(`/todo/projet/${subProjects.id}`);
    if (subProjects.onClick) {
      subProjects.onClick();
    }
  };

  const handleEtiquetteClick = (etiquette: any) => {
    push(`/todo/etiquette/${etiquette.id}`);
    /* if (project.onClick) {
      project.onClick();
    } */
  };

  // state of modals (open or not)
  const [openAddToFavorite, setOpenAddToFavorite] = useState<boolean>(false);
  const [openArchiveProject, setOpenArchiveProject] = useState<boolean>(false);
  const [openDeleteProject, setOpenDeleteProject] = useState<boolean>(false);
  const [openDeleteEtiquette, setOpenDeleteEtiquette] = useState<boolean>(false);
  const [openShare, setOpenShare] = useState<boolean>(false);
  const [openShareEtiquette, setOpenShareEtiquette] = useState<boolean>(false);

  const [selectedUsersIds, setselectedUsersIds] = useState<string[]>([]);
  // Modals state handler
  const addToFavoriteclick = () => {
    setOpenAddToFavorite(!openAddToFavorite);
  };

  const addToFavoriteEtiquetteClick = () => {
    setOpenAddToFavorite(!openAddToFavorite);
  };

  const deleteEtiquetteClick = () => {
    setOpenDeleteEtiquette(!openDeleteEtiquette);
  };
  const archiveProjectClick = () => {
    setOpenArchiveProject(!openArchiveProject);
  };

  const deleteProjectClick = () => {
    setOpenDeleteProject(!openDeleteProject);
  };

  const editEtiquetteClick = () => {
    setOpenEtiquette(!openEtiquette);
    setOperationName(operationVariable.edit);
  };

  const shareProjectClick = () => {
    setOpenShare(true);
  };

  const handleClickShareEtiquette = () => {
    setOpenShareEtiquette(true);
  };

  const [sharedEtiquetteMenuItems] = useSharedEtiquetteList(
    editEtiquetteClick,
    addToFavoriteclick,
    // removeFromSharedClick,
    deleteEtiquetteClick,
  );

  const [clickedRow, setClickedRow] = useState<any>();

  // console.log('ClickedRow', clickedRow);
  // console.log('openDeleteDialog', openDeleteEtiquette);

  const editFavoriteClick = () => {
    // console.log('this get called', clickedRow);
    if (clickedRow && clickedRow.type === 'project') {
      setOpenAddProject(!openAddProject);
    } else {
      // console.log('this is not  a project'); // TODO
    }
  };

  const [switchProjectFavs, switchArchived] = useUpdateProject(
    setOpenAddToFavorite,
    setOpenArchiveProject,
    clickedRow,
  );
  const [switchEtiquetteFavs] = useUpdateEtiquette(setOpenAddToFavorite);
  const [projectToEdit, setProjectToEdit] = useState<any>();
  const [parentProject, setParentProject] = useState<any>();
  const [prevProject, setPrevProject] = useState<any>();
  const [nextproject, setNextProject] = useState<any>();
  const [operationName, setOperationName] = useState<string>('');

  const [favorisMenuItems] = useFavorisList(
    editFavoriteClick,
    addToFavoriteclick,
    setProjectToEdit,
    useMatriceResponsabilite,
  );

  const addUperEtiquetteClick = () => {
    setOpenEtiquette(!openEtiquette);
    setOperationName(operationVariable.addUpper);
  };
  const addDownEtiquetteClick = () => {
    setOpenEtiquette(!openEtiquette);
    setOperationName(operationVariable.addDown);
  };

  const [archivedMenuItems] = useArchivedProjectList(archiveProjectClick, deleteProjectClick);

  const [etiquetteMenuItems] = useEtiquetteList(
    addUperEtiquetteClick,
    addDownEtiquetteClick,
    editEtiquetteClick,
    addToFavoriteclick,
    handleClickShareEtiquette,
    deleteEtiquetteClick,
    clickedRow,
  );

  const [favorisMenuEtiquetteItems] = useFavorisList(
    editFavoriteClick,
    addToFavoriteclick,
    editEtiquetteClick,
    useMatriceResponsabilite,
  );

  const switchFavs = clickedRow => {
    if (clickedRow && clickedRow.type === 'project') {
      return switchProjectFavs(clickedRow);
    } else {
      switchEtiquetteFavs(clickedRow);
    }
  };

  const [projectMenuItems] = useProjectList(
    handleAddProject,
    handleAddProject,
    handleAddProject,
    shareProjectClick,
    addToFavoriteclick,
    archiveProjectClick,
    deleteProjectClick,
    setProjectToEdit,
    setParentProject,
    setPrevProject,
    setNextProject,
    clickedRow,
    parameters,
    useMatriceResponsabilite,
  );

  const isTodayOnly = pathname.includes('aujourdhui') && !pathname.includes('aujourdhui-equipe');
  const isOnAujourhduiEquipe: boolean = pathname.includes('/aujourdhui-equipe');
  const isOnSeptJours: boolean =
    pathname.includes('/sept-jours') && !pathname.includes('sept-jours-equipe');
  const isInInbox: boolean = pathname.includes('recu') && !pathname.includes('recu-equipe');
  const isInboxTeam: boolean = pathname.includes('recu-equipe');
  const withoutDate: boolean = pathname.includes('sans-date');
  const isAllSee: boolean = pathname.includes('/tout-voir');
  const isOnSeptJoursEquipe: boolean = pathname.includes('/sept-jours-equipe');
  const niveauMatriceFonctions = useNiveauMatriceFonctions();

  const favorisList =
    menuFilters && menuFilters.equipe
      ? [
          {
            icon: <Today />,
            title: 'Aujourd’hui de l’équipe',
            count: (countTodos && countTodos.action && countTodos.action.todayTeam) || 0,
            path: 'aujourdhui-equipe',
            isActive: isOnAujourhduiEquipe,
          } /*,
          {
            icon: <MoveToInbox />,
            title: 'Boîte de réception de l’équipe',
            count: (countTodos && countTodos.action && countTodos.action.inboxTeam) || 0,
            path: 'recu-equipe',
            isActive: isInboxTeam,
          }*/,
          {
            icon: <DateRange />,
            title: 'Prochains jours de l’équipe',
            count: (countTodos && countTodos.action && countTodos.action.nextSeventDaysTeam) || 0,
            path: 'sept-jours-equipe',
            isActive: isOnSeptJoursEquipe,
            onClick: handleNextWeek,
          },
        ]
      : [
          {
            icon: <Today />,
            title: 'Aujourd’hui',
            count: (countTodos && countTodos.action && countTodos.action.today) || 0,
            path: 'aujourdhui',
            isActive: isTodayOnly,
            onClick: () => setTriage([{ priority: { order: 'asc' } }]),
          } /*,
          {
            icon: <Inbox />,
            title: 'Boîte de réception',
            count: (countTodos && countTodos.action && countTodos.action.inbox) || 0,
            path: 'recu',
            isActive: isInInbox,
          }*/,
          {
            icon: <DateRange />,
            title: 'Prochains jours',
            count: (countTodos && countTodos.action && countTodos.action.nextSeventDays) || 0,
            path: 'sept-jours',
            isActive: isOnSeptJours,
            onClick: handleNextWeek,
          },
        ];

  const user = getUser();

  const projectResult = projectList?.data?.search?.data || [];
  const etiquetteResult =
    etiquetteList &&
    etiquetteList.data &&
    etiquetteList.data.search &&
    etiquetteList.data.search.data;

  /* const refetchEtiquette = etiquetteList && etiquetteList.refetch; */
  const refetchProject = projectList && projectList.refetch;

  const filteredProject = filter(projectResult, project => project.isArchived === false);

  const projectParents = _.filter(filteredProject || [], project => project.projetParent === null);
  const projectParentIds = projectParents.map(parent => parent.id);

  let listFilterProjects = (niveauMatriceFonctions === 1
    ? projectParents
    : [
        ...projectParents,
        ...filteredProject.filter(
          project =>
            project.projetParent &&
            project.projetParent.id &&
            !projectParentIds.includes(project.projetParent.id),
        ),
      ]
  ).sort((p1, p2) => p1.ordre - p2.ordre);

  if (niveauMatriceFonctions === 1) {
    listFilterProjects = listFilterProjects.sort((p1, p2) =>
      p2.typeProject.localeCompare(p1.typeProject),
    );
  }

  const archivedProject = filter(projectResult, project => project.isArchived === true);
  const projectArchiveParents = _.filter(
    archivedProject || [],
    project => project.projetParent === null,
  );
  const projectArchiveParentIds = projectArchiveParents.map(parent => parent.id);

  const listArchiveFilterProjects = [
    ...projectArchiveParents,
    ...archivedProject.filter(
      project =>
        project.projetParent &&
        project.projetParent.id &&
        !projectArchiveParentIds.includes(project.projetParent.id),
    ),
  ];

  const sharedEtiquette = filter(etiquetteResult, etiquette => etiquette.isShared === true);

  const allFavoris = concat(
    filter(filteredProject, project => project.isInFavoris === true),
    filter(etiquetteResult, etiquette => etiquette.isInFavoris === true),
  );

  // Query Variables
  const etiquetteVariable = etiquetteList && etiquetteList.variables;
  const projectVariable = projectList && projectList.variables;

  const [removeProject] = useDeleteProject(
    projectVariable,
    setOpenDeleteProject,
    refetchProject,
    refetchCountTodos,
  );
  const [removeEtiquette] = useDeleteEtiquette(etiquetteVariable, setOpenDeleteEtiquette);

  const favorisDropDown = {
    title: 'Favoris',
    element:
      allFavoris &&
      allFavoris.map(favoris => ({
        id: favoris && favoris.id,
        actionButton: (
          <ActionButton
            row={favoris}
            menuItems={
              favoris.__typename === 'TodoEtiquette' ? favorisMenuEtiquetteItems : favorisMenuItems
            }
            setClickedRow={setClickedRow}
            refetchCountTodos={refetchCountTodos}
          />
        ),
        title: favoris && favoris._name,
        typeName: favoris && favoris.__typename,
        count: favoris && favoris.nbAction,
        type: computeProjetTypeLabel(favoris, useMatriceResponsabilite),
        icon:
          favoris && favoris.isShared ? (
            <SupervisedUserCircle
              htmlColor={(favoris && favoris.couleur && favoris.couleur.code) || '#A3B1B0'}
            />
          ) : !favoris.typeProject ? (
            <LocalOffer
              htmlColor={(favoris && favoris.couleur && favoris.couleur.code) || '#DF0A6E'}
            />
          ) : (
            <FiberManualRecord
              htmlColor={(favoris && favoris.couleur && favoris.couleur.code) || '#A3B1B0'}
            />
          ),
        subProjects:
          favoris.__typename === 'Project'
            ? filterSubProjects(favoris, useMatriceResponsabilite, user, true)
                .sort((p1, p2) => p1.ordre - p2.ordre)
                .map(subProject => ({
                  id: subProject && subProject.id,
                  typeName: 'Project',
                  actionButton: (
                    <ActionButton
                      row={subProject}
                      menuItems={projectMenuItems}
                      setClickedRow={setClickedRow}
                      refetchCountTodos={refetchCountTodos}
                    />
                  ),
                  title: subProject && subProject._name,
                  count: subProject && subProject.nbAction,
                  type: computeProjetTypeLabel(subProject, useMatriceResponsabilite),
                  isActive: filterId === subProject && subProject.id,
                  icon:
                    subProject && subProject.isShared ? (
                      <SupervisedUserCircle
                        htmlColor={
                          (subProject && subProject.couleur && subProject.couleur.code) || '#A3B1B0'
                        }
                      />
                    ) : (
                      <FiberManualRecord
                        htmlColor={
                          (subProject && subProject.couleur && subProject.couleur.code) || '#A3B1B0'
                        }
                      />
                    ),
                }))
            : null,
      })),
    withAddAction: false,
  };

  const projectDropDown = {
    title: `${capitalizeFirstLetter(termologie)}s`,
    element: listFilterProjects.map(project => ({
      id: project && project.id,
      typeName: 'Project',
      actionButton: (
        <ActionButton
          row={project}
          menuItems={projectMenuItems}
          setClickedRow={setClickedRow}
          refetchCountTodos={refetchCountTodos}
        />
      ),
      title: project && project._name,
      count: project && project.nbAction,
      type: computeProjetTypeLabel(project, useMatriceResponsabilite),
      isActive: filterId === project && project.id,
      icon:
        project && project.isShared ? (
          <SupervisedUserCircle
            htmlColor={(project && project.couleur && project.couleur.code) || '#A3B1B0'}
          />
        ) : (
          <FiberManualRecord
            htmlColor={(project && project.couleur && project.couleur.code) || '#A3B1B0'}
          />
        ),
      subProjects:
        niveauMatriceFonctions === 1
          ? []
          : filterSubProjects(project, useMatriceResponsabilite, user, true)
              .sort((p1, p2) => p1.ordre - p2.ordre)
              .map(subProject => ({
                id: subProject && subProject.id,
                typeName: 'Project',
                actionButton: (
                  <ActionButton
                    row={subProject}
                    menuItems={projectMenuItems}
                    setClickedRow={setClickedRow}
                    refetchCountTodos={refetchCountTodos}
                  />
                ),
                title: subProject && subProject._name,
                count: subProject && subProject.nbAction,
                type: computeProjetTypeLabel(subProject, useMatriceResponsabilite),
                isActive: filterId === subProject && subProject.id,
                icon:
                  subProject && subProject.isShared ? (
                    <SupervisedUserCircle
                      htmlColor={
                        (subProject && subProject.couleur && subProject.couleur.code) || '#A3B1B0'
                      }
                    />
                  ) : (
                    <FiberManualRecord
                      htmlColor={
                        (subProject && subProject.couleur && subProject.couleur.code) || '#A3B1B0'
                      }
                    />
                  ),
              })),
    })),
    addAction: handleAddProject,
    withAddAction: !useMatriceResponsabilite,
  };

  const archivedProjectDropDown = {
    title: `${capitalizeFirstLetter(termologie)}s archivés`,
    element:
      listArchiveFilterProjects &&
      listArchiveFilterProjects.map(project => ({
        id: project && project.id,
        typeName: 'Project',
        actionButton: (
          <ActionButton
            row={project}
            menuItems={archivedMenuItems}
            setClickedRow={setClickedRow}
            refetchCountTodos={refetchCountTodos}
          />
        ),
        title: project && project._name,
        count: project && project.nbAction,
        type: computeProjetTypeLabel(project, useMatriceResponsabilite),
        icon:
          project && project.isShared ? (
            <SupervisedUserCircle
              htmlColor={(project && project.couleur && project.couleur.code) || '#A3B1B0'}
            />
          ) : (
            <FiberManualRecord
              htmlColor={(project && project.couleur && project.couleur.code) || '#A3B1B0'}
            />
          ),
        subProjects:
          project &&
          project.subProjects &&
          project.subProjects.length &&
          project.subProjects.filter(
            subProject => subProject && subProject.isArchived && !subProject.isArchived.isRemoved,
          ).length
            ? project.subProjects
                .filter(
                  subProject =>
                    subProject &&
                    subProject.isArchived &&
                    (!useMatriceResponsabilite ||
                      (useMatriceResponsabilite &&
                        subProject.participants.some(participant => participant.id === user.id))),
                )
                .map(subProject => ({
                  id: subProject && subProject.id,
                  typeName: 'Project',
                  actionButton: (
                    <ActionButton
                      row={subProject}
                      menuItems={projectMenuItems}
                      setClickedRow={setClickedRow}
                      refetchCountTodos={refetchCountTodos}
                    />
                  ),
                  title: subProject && subProject._name,
                  count: subProject && subProject.nbAction,
                  type: computeProjetTypeLabel(subProject, useMatriceResponsabilite),
                  isActive: filterId === subProject && subProject.id,
                  icon:
                    subProject && subProject.isShared ? (
                      <SupervisedUserCircle
                        htmlColor={
                          (subProject && subProject.couleur && subProject.couleur.code) || '#A3B1B0'
                        }
                      />
                    ) : (
                      <FiberManualRecord
                        htmlColor={
                          (subProject && subProject.couleur && subProject.couleur.code) || '#A3B1B0'
                        }
                      />
                    ),
                }))
            : [],
        isActive: filterId === project && project.id,
      })),
    withAddAction: false,
  };

  const etiquetteDropDown = {
    title: 'Importance',
    element:
      etiquetteResult &&
      etiquetteResult.map(result => ({
        id: result && result.id,
        typeName: 'TodoEtiquette',
        //useMatriceResponsabilite ? null : (
        actionButton: null,
        // (
        //   <ActionButton
        //     row={result}
        //     menuItems={etiquetteMenuItems}
        //     setClickedRow={setClickedRow}
        //     refetchCountTodos={refetchCountTodos}
        //   />
        // ),
        title: result && result._name,
        count: result.nbAction,
        icon: <Error htmlColor={(result && result.couleur && result.couleur.code) || '#DF0A6E'} />,
      })),
    addAction: handleAddEtiquette,
    withAddAction: !useMatriceResponsabilite,
  };

  const sharedEtiquetteDropDown = {
    title: 'Étiquettes partagées',
    element:
      sharedEtiquette &&
      sharedEtiquette.map(shared => ({
        id: shared && shared.id,
        typeName: 'TodoEtiquette',
        actionButton: (
          <ActionButton
            row={shared}
            menuItems={sharedEtiquetteMenuItems}
            setClickedRow={setClickedRow}
            refetchCountTodos={refetchCountTodos}
          />
        ),
        title: shared._name,
        count: shared.nbAction,
        icon: <LocalOffer htmlColor={shared?.couleur?.code ?? '#DF0A6E'} />,
      })),

    withAddAction: false,
  };

  const DeleteDialogContent = () => {
    return <span>Êtes-vous sur de vouloir supprimer ?</span>;
  };

  return (
    <>
      <AjoutEtiquette
        open={openEtiquette}
        handleOpen={handleAddEtiquette}
        projectQueryVariable={etiquetteList && etiquetteList.variables}
        operationName={operationName}
        etiquette={clickedRow}
        resetEtiquette={() => {
          setClickedRow(null);
          setOperationName('');
        }}
      />
      <AjoutProjet
        open={openAddProject}
        setOpen={handleAddProject}
        queryVariables={projectList && projectList.variables}
        projectToEdit={projectToEdit}
        setProjectToEdit={setProjectToEdit}
        prevProject={prevProject}
        setPrevProject={setPrevProject}
        nextProject={nextproject}
        setNextProject={setNextProject}
        parentProject={parentProject}
        setParentProject={setParentProject}
        refetch={refetchProject}
        refetchCountTodos={refetchCountTodos}
        termologie={termologie}
      />

      <FavorisHeader
        headerList={favorisList}
        handleFavorisClick={handleFavorisClick}
        setShowMainContent={setShowMainContent}
      />
      <DropdownDraggable
        list={favorisDropDown}
        clickable={false}
        niveauMatriceFonctions={niveauMatriceFonctions}
        defaultExpanded={true}
        handleProjectClick={handleProjectClick}
        handleSubProjectClick={handleSubProjectClick}
        handleEtiquetteClick={handleEtiquetteClick}
        setShowMainContent={setShowMainContent}
      />

      <DropdownDraggable
        disabled={useMatriceResponsabilite && niveauMatriceFonctions === 2}
        niveauMatriceFonctions={niveauMatriceFonctions}
        list={projectDropDown}
        handleProjectClick={handleProjectClick}
        handleSubProjectClick={handleSubProjectClick}
        clickable={false}
        setShowMainContent={setShowMainContent}
      />
      {parameters &&
      parameters.length &&
      parameters.filter(parameter => parameter.code === '0121' && parameter.value === 'true')
        .length ? (
        <DropdownDraggable
          list={archivedProjectDropDown}
          clickable={false}
          handleProjectClick={handleProjectClick}
          handleSubProjectClick={handleSubProjectClick}
          setShowMainContent={setShowMainContent}
        />
      ) : (
        <></>
      )}
      <UrgenceFilter withDropdown={true} urgence={urgence} setUrgence={setUrgence} />
      <ImportanceFilter importances={importances} setImportances={setImportances} />
      {/*<DropdownDraggable
        list={etiquetteDropDown}
        handleEtiquetteClick={handleEtiquetteClick}
        setShowMainContent={setShowMainContent}
        clickable={false}
      />*/}
      {/* {!useMatriceResponsabilite && (
        <DropdownDraggable
          list={sharedEtiquetteDropDown}
          clickable={false}
          handleEtiquetteClick={handleEtiquetteClick}
          setShowMainContent={setShowMainContent}
        />
      )} */}

      <ConfirmDialog
        open={openAddToFavorite}
        message={
          clickedRow && clickedRow.isInFavoris
            ? 'Êtes-vous sûr de vouloir retirer des favoris ?'
            : 'Êtes-vous sûr de vouloir ajouter au favoris ?'
        }
        handleValidate={() => switchFavs(clickedRow)}
        handleClose={addToFavoriteclick}
      />

      <ConfirmDialog
        open={openArchiveProject}
        message={
          clickedRow && clickedRow.isArchived
            ? 'Êtes-vous sûr de vouloir désarchiver ?'
            : 'Êtes-vous sûr de vouloir archiver ?'
        }
        handleValidate={() => switchArchived(clickedRow)}
        handleClose={archiveProjectClick}
      />
      <ConfirmDeleteDialog
        open={openDeleteProject}
        setOpen={setOpenDeleteProject}
        content={<DeleteDialogContent />}
        // tslint:disable-next-line: jsx-no-lambda
        onClickConfirm={() => removeProject(clickedRow)}
      />
      <ConfirmDeleteDialog
        open={openDeleteEtiquette}
        setOpen={setOpenDeleteEtiquette}
        content={<DeleteDialogContent />}
        // tslint:disable-next-line: jsx-no-lambda
        onClickConfirm={() => removeEtiquette(clickedRow)}
      />
      <AjoutParticipant
        openModalParticipant={openShare}
        setOpenParticipant={setOpenShare}
        userIds={selectedUsersIds}
        setUserIds={setselectedUsersIds}
        idProjectTask={(clickedRow && clickedRow.id) || ''}
        disableAllFilter={true}
      />

      <AjoutParticipant
        openModalParticipant={openShareEtiquette}
        setOpenParticipant={setOpenShareEtiquette}
        idEtiquette={(clickedRow && clickedRow.id) || ''}
        userIds={selectedUsersIds}
        setUserIds={setselectedUsersIds}
        disableAllFilter={true}
      />
      <Backdrop open={projectList && projectList.loading} />
    </>
  );
};
export default withRouter(Sider);
