import { Add, Delete, Edit, Favorite, PersonAdd } from '@material-ui/icons';
import AjouterDessous from '../../../../../../../assets/icons/todo/ajouter_dessous.svg';
import AjouterDessus from '../../../../../../../assets/icons/todo/ajouter_dessus.svg';
import Archiver from '../../../../../../../assets/icons/todo/archiver.svg';
import { ActionButtonMenu } from '../../../Common/ActionButton';
import React from 'react';

export const useProjectList = (
  addSubProjectClick: (row: any) => void,
  addTopProjectClick: (row: any) => void,
  // addBottomProjectClick: (row: any) => void,
  editdProjectClick: (row: any) => void,
  shareProjectClick: (row: any) => void,
  addProjectToFavoriteclick: () => void,
  archiveProjectClick: (row: any) => void,
  deleteProjectClick: (row: any) => void,
  setProjectToEdit: (row: any) => void,
  setParentProject: (row: any) => void,
  setPrevProject: (row: any) => void,
  setNextProject: (row: any) => void,
  project: any,
  parameters: any[],
  useMatriceResponsabilite: boolean,
) => {
  const projectMenuItems: ActionButtonMenu[] = [
    {
      label: 'Créer un sous-projet',
      icon: <Add />,
      onClick: addSubProjectClick,
      setState: setParentProject,
      disabled: false,
      hide:
        useMatriceResponsabilite || (project && project.projetParent)
          ? true
          : parameters &&
            parameters.length &&
            parameters.filter(
              (parameter) => parameter.code === '0120' && parameter.value === 'true',
            ).length
          ? false
          : true,
    },
    {
      label: 'Ajouter un projet au-dessus',
      icon: <img src={AjouterDessus} />,
      onClick: addTopProjectClick,
      setState: setPrevProject,
      disabled: false,
      hide:
        useMatriceResponsabilite || (project && project.projetParent)
          ? true
          : parameters &&
            parameters.length &&
            parameters.filter(
              (parameter) => parameter.code === '0120' && parameter.value === 'true',
            ).length
          ? false
          : true,
    },
    {
      label: 'Ajouter un projet au-dessous',
      icon: <img src={AjouterDessous} />,
      onClick: addTopProjectClick,
      setState: setNextProject,
      disabled: false,
      hide:
        useMatriceResponsabilite || (project && project.projetParent)
          ? true
          : parameters &&
            parameters.length &&
            parameters.filter(
              (parameter) => parameter.code === '0120' && parameter.value === 'true',
            ).length
          ? false
          : true,
    },
    {
      label: 'Modifier',
      icon: <Edit />,
      onClick: editdProjectClick,
      setState: setProjectToEdit,
      hide: useMatriceResponsabilite,
      disabled: false,
    },
    {
      label: 'Partager',
      icon: <PersonAdd />,
      onClick: shareProjectClick,
      hide: useMatriceResponsabilite,
      disabled: false,
    },
    {
      label: 'Ajouter au favoris',
      icon: <Favorite />,
      onClick: addProjectToFavoriteclick,
      disabled: project && project.isInFavoris,
    },
    {
      label: 'Archiver',
      icon: <img src={Archiver} />,
      onClick: archiveProjectClick,
      hide: useMatriceResponsabilite,

      disabled:
        (project && project.isArchived) ||
        (parameters &&
          parameters.length &&
          parameters.filter((parameter) => parameter.code === '0121' && parameter.value === 'true')
            .length)
          ? false
          : true,
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: deleteProjectClick,
      hide: useMatriceResponsabilite,

      disabled: project && !project.activeActions ? false : true,
    },
  ];
  return [projectMenuItems];
};
