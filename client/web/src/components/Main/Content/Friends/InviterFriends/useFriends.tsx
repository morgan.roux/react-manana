import { Box, Link, MenuItem, Select } from '@material-ui/core';
import moment from 'moment';
import React, { Dispatch, SetStateAction, useState, useMemo, Fragment } from 'react';
import { Column } from '../../../../Common/newCustomContent/interfaces';
import { useTheme } from '@material-ui/core/styles';

// import { useUpdateStatus } from './useUpdateStatus';

export const useFriends = (setOpenDeleteDialog: Dispatch<SetStateAction<boolean>>) => {
  const [selected, setSelected] = useState<any[]>([]);
  const [deleteRow, setDeleteRow] = useState<any>(null);
  const theme = useTheme();

  //   const { deleteIdea, loading } = useDeleteIdea(
  //     deleteRow ? [deleteRow] : selected,
  //     setSelected,
  //     deleteRow,
  //     setDeleteRow,
  //   );

  //   const { updateStatus, loading: updateStatusLoading } = useUpdateStatus();
  //   useMemo(() => {
  //     if (selected && selected.length > 0) {
  //       if (deleteRow !== null) setDeleteRow(null);
  //     }
  //   }, [selected]);

  const onClickDelete = (row: any) => {
    setDeleteRow(row);
  };

  const onClickConfirmDelete = () => {
    setOpenDeleteDialog(false);
    if (selected.length > 0) {
      //   deleteIdea();
    }

    if (deleteRow && deleteRow.id) {
      //   deleteIdea();
    }
  };

  const handleSelectchange = (event: any, row: any): void => {
    // updateStatus({
    //   variables: {
    //     id: row && row.id,
    //     status: event?.target?.value ?? '',
    //   },
    // });
  };

  const columns: Column[] = [
    {
      name: 'cip',
      label: 'CIP',
      renderer: (value: any) => {
        console.log('cip', value);

        return value.cip ? value.cip : '-';
      },
    },
    {
      name: 'departement.nom',
      label: 'Platforme',
      renderer: (value: any) => {
        return value.departement && value.departement.nom ? value.departement.nom : '-';
      },
    },
    {
      name: 'contact',
      label: 'Contrat',
      renderer: (value: any) => {
        return value.contact ? value.contact.mailProf : '-';
      },
    },
    {
      name: 'nom',
      label: 'Nom Pharmacie',
      renderer: (value: any) => {
        return value.nom ? value.nom : '-';
      },
    },
    {
      name: 'titulaire.fullName',
      label: 'Titulaire(s)',
      renderer: (value: any) => {
        const titulaires: any[] = value.titulaires || [];
        return titulaires.map((t: any, index: number) => {
          const isMultiple: boolean = titulaires.length > 1 && index < titulaires.length - 1;
          if (t) {
            return (
              <Fragment key={`titulaire_link_${index}`}>
                {t && `${t.fullName}`}
                {isMultiple && (
                  <>
                    ,<span style={{ visibility: 'hidden' }}>&nbsp;</span>
                  </>
                )}
              </Fragment>
            );
          } else {
            return '-';
          }
        });
      },
    },
    {
      name: 'adresse1',
      label: 'Adresse',
      renderer: (value: any) => {
        return value.adresse1 ? value.adresse1 : '-';
      },
    },
    {
      name: 'ville',
      label: 'Ville',
      renderer: (value: any) => {
        return value.ville ? value.ville : '-';
      },
    },
  ];

  return {
    columns,
    deleteRow,
    onClickConfirmDelete,
    //loading,
    selected,
    setSelected,
    //updateStatusLoading,
  };
};
