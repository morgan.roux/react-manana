import { FormControl, FormLabel, RadioGroup, FormControlLabel, Radio } from '@material-ui/core';
import moment from 'moment';
import React, { FC } from 'react';
import { Column } from '../../../../Dashboard/Content/Interface';

export const columns: Column[] = [
  {
    name: 'nom',
    label: `Groupes d'amis`,
  },
  {
    name: 'nbPharmacie',
    label: 'Nombre de pharmacies',
    renderer: (value: any) => {
      return value && value.nbPharmacie ? value.nbPharmacie : '0';
    },
  },
  {
    name: 'dateCreation',
    label: 'Date de création',
    renderer: (row: any) => {
      return moment(row.dateCreation).format('DD/MM/YYYY') || '-';
    },
  },
];

export const getColumnsPharmacie: Column[] = [
  {
    name: 'nom',
    label: 'Nom',
  },
  {
    name: 'cp',
    label: 'Code Postal',
  },
  {
    name: 'ville',
    label: 'Ville',
  },
];

export const columnsFilterFriends = (isDisabled?: (code: string) => boolean) => [
  {
    name: 'Pharmacie',
    code: 'My_PHARMACIE',
    open: true,
    disable: false,
  },
  {
    name: 'Groupeamis',
    code: 'MY_GROUPE_AMIS',
    open: false,
    disable: false,
  },
];

interface Tableprops {
  setValue: (values: any) => void;
  value: any;
}
const TablepartageGauche: FC<Tableprops> = ({ setValue, value }) => {
  const handleRadioChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue((event.target as HTMLInputElement).value);
  };

  return (
    <FormControl component="fieldset">
      <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleRadioChange}>
        <FormControlLabel value="Pharmacie" control={<Radio />} label="Pharmacie" />
        <FormControlLabel value="GroupesAmis" control={<Radio />} label="Groupes d'amis" />
      </RadioGroup>
    </FormControl>
  );
};
export default TablepartageGauche;
