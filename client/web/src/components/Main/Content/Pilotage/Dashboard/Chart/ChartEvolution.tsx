import React, { FC, useState, useMemo } from 'react';
import { useQuery } from '@apollo/react-hooks';
import {
  GET_BUSINESS_ACHIEVED_IN_YEAR,
  GET_FEEDBACK_ACHIEVED_IN_YEAR,
} from '../../../../../../graphql/Pilotage';
import {
  BUSINESS_ACHIEVED_IN_YEAR,
  BUSINESS_ACHIEVED_IN_YEARVariables,
} from '../../../../../../graphql/Pilotage/types/BUSINESS_ACHIEVED_IN_YEAR';
import {
  PilotageBusiness,
  BusinessIndicateur,
  PilotageFeedback,
  FeedbackIndicateur,
} from '../../../../../../types/graphql-global-types';
import Chart from 'react-apexcharts';
import { useTheme } from '@material-ui/core';
import { ChartInterface } from '../Dashboard';
import { roundNumber } from '../../../../../../utils/Helpers';
import moment from 'moment';
import {
  FEEDBACK_ACHIEVED_IN_YEAR,
  FEEDBACK_ACHIEVED_IN_YEARVariables,
} from '../../../../../../graphql/Pilotage/types/FEEDBACK_ACHIEVED_IN_YEAR';

interface ChartEvolutionProps {
  pilotage: PilotageBusiness | PilotageFeedback;
  indicateur: BusinessIndicateur | FeedbackIndicateur;
  year: number;
  idOperations: string[];
  idPharmacies: string[];
  idPharmaciesSelected: string[];
  chartSubTitle: string;
  activeSemestre: number;
  showEvolution: boolean;
  isOnBusiness: boolean;
  isOnFeedback: boolean;
  codeItem: string;
  idItemAssocies: string[];
}

interface StateInterface {
  data: number[];
  categories: string[];
  colors: string[];
}

const ChartEvolution: FC<ChartEvolutionProps> = ({
  pilotage,
  indicateur,
  year,
  idOperations,
  idPharmacies,
  idPharmaciesSelected,
  chartSubTitle,
  activeSemestre,
  showEvolution,
  isOnBusiness,
  isOnFeedback,
  codeItem,
  idItemAssocies,
}) => {
  const theme = useTheme();

  const defaultColors: string[] = [];
  const defaultCat: string[] = [];
  const defaultData: number[] = [];

  for (let i = 1; i < 12; i++) {
    defaultColors.push(theme.palette.secondary.main);
    defaultCat.push(new Date(`${moment().format(`YYYY-${i}-01`)}`).toString());
    defaultData.push(0);
  }

  const [state, setState] = useState<StateInterface>({
    data: defaultData,
    categories: defaultCat,
    colors: defaultColors,
  });

  const defaultChartOptions = {
    chart: {
      type: 'bar',
      toolbar: {
        show: false,
      },
      events: {
        click: (chart, w, e) => {
          console.log(chart, w, e);
        },
      },
    },
    grid: {
      show: true,
      position: 'back',
      strokeDashArray: 5,
      xaxis: {
        lines: {
          show: true,
        },
      },
      yaxis: {
        lines: {
          show: true,
        },
      },
    },
    xaxis: {
      type: 'category',
      categories: state.categories,
      labels: {
        show: true,
        style: {
          fontSize: '14px',
        },
        formatter: (value, timestamp, index) => {
          return moment(new Date(value)).format('MMM YYYY');
        },
      },
      axisBorder: {
        show: false,
      },
    },
    dataLabels: {
      enabled: false,
      formatter: (value: any) => {
        return Math.round(value);
      },
    },
    colors: state.colors,
    legend: { show: false },
    plotOptions: {
      bar: {
        dataLabels: {
          position: 'top',
        },
      },
    },
  };

  const chartSeries = [
    {
      data: state.data,
      name: chartSubTitle,
    },
  ];

  const [chartConfig, setChartConfig] = useState<ChartInterface>({
    options: defaultChartOptions,
    series: chartSeries,
  });

  const { data: dataBusiness } = useQuery<
    BUSINESS_ACHIEVED_IN_YEAR,
    BUSINESS_ACHIEVED_IN_YEARVariables
  >(GET_BUSINESS_ACHIEVED_IN_YEAR, {
    fetchPolicy: 'cache-and-network',
    variables: {
      pilotage: pilotage as any,
      indicateur: indicateur as any,
      year,
      idOperations,
      idPharmacies,
      idPharmaciesSelected,
    },
    skip: !showEvolution || !isOnBusiness,
  });

  const { data: dataFeedback } = useQuery<
    FEEDBACK_ACHIEVED_IN_YEAR,
    FEEDBACK_ACHIEVED_IN_YEARVariables
  >(GET_FEEDBACK_ACHIEVED_IN_YEAR, {
    fetchPolicy: 'cache-and-network',
    variables: {
      codeItem,
      pilotage: pilotage as any,
      indicateur: indicateur as any,
      year,
      idItemAssocies,
      idPharmacies,
    },
    skip: !showEvolution || !isOnFeedback,
  });

  /**
   * Set chart data
   */
  useMemo(() => {
    if (
      dataBusiness &&
      dataBusiness.businessAchievedInYear &&
      dataBusiness.businessAchievedInYear.length > 0
    ) {
      const newData: number[] = dataBusiness.businessAchievedInYear.map(d =>
        d && d.value ? roundNumber(parseFloat(d.value)) : 0,
      );
      const newCat: string[] = dataBusiness.businessAchievedInYear.map(d =>
        d && d.key ? d.key : '',
      );
      setState(prevState => ({ ...prevState, data: newData, categories: newCat }));
    }

    if (
      dataFeedback &&
      dataFeedback.feedbackAchievedInYear &&
      dataFeedback.feedbackAchievedInYear.length > 0
    ) {
      const newData: number[] = dataFeedback.feedbackAchievedInYear.map(d =>
        d && d.value ? roundNumber(parseFloat(d.value)) : 0,
      );
      const newCat: string[] = dataFeedback.feedbackAchievedInYear.map(d =>
        d && d.key ? d.key : '',
      );
      setState(prevState => ({ ...prevState, data: newData, categories: newCat }));
    }
  }, [dataBusiness, dataFeedback]);

  /**
   * Cut data and categories
   */
  useMemo(() => {
    if (state.data.length > 6 && state.categories.length > 6) {
      if (activeSemestre === 1) {
        setState(prevState => ({
          ...prevState,
          data: prevState.data.slice(0, 6),
          categories: prevState.categories.slice(0, 6),
        }));
      }
      if (activeSemestre === 2) {
        setState(prevState => ({
          ...prevState,
          data: prevState.data.slice(6),
          categories: prevState.categories.slice(6),
        }));
      }
    }
  }, [activeSemestre, state]);

  /**
   * Update chart series
   */
  useMemo(() => {
    setChartConfig({ options: defaultChartOptions, series: chartSeries });
  }, [chartSubTitle, state, activeSemestre]);

  return (
    <Chart
      options={chartConfig.options}
      series={chartConfig.series}
      type="bar"
      height="430"
      width="550"
    />
  );
};

export default ChartEvolution;
