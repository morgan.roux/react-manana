import { useApolloClient, useLazyQuery, useQuery } from '@apollo/react-hooks';
import {
  Box,
  Card,
  CardContent,
  CardHeader,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
  useTheme,
} from '@material-ui/core';
import { ArrowBack, ArrowDownward, ArrowUpward } from '@material-ui/icons';
import { ApolloClient } from 'apollo-client';
import classnames from 'classnames';
import { isInteger, isNumber, last } from 'lodash';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import Chart from 'react-apexcharts';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import CaMoyennePharmaIcon from '../../../../../assets/icons/pilotage/business/ca_moyen_par_pharmacie.svg';
import CaTotalIcon from '../../../../../assets/icons/pilotage/business/ca_total_en_euro.svg';
import NbrCmdIcon from '../../../../../assets/icons/pilotage/business/nbre_de_commandes.svg';
import NbrLigneIcon from '../../../../../assets/icons/pilotage/business/nbre_de_lignes.svg';
import NbrPharmaIcon from '../../../../../assets/icons/pilotage/business/nbre_de_pharmacies.svg';
import NbrProductIcon from '../../../../../assets/icons/pilotage/business/nbre_de_produits.svg';
import RemisePercentIcon from '../../../../../assets/icons/pilotage/business/remise_en_pourcentage.svg';
import TotalRemiseIcon from '../../../../../assets/icons/pilotage/business/total_remise_en_euro.svg';
import AdoreIcon from '../../../../../assets/icons/pilotage/feedback/adore.svg';
import AimeIcon from '../../../../../assets/icons/pilotage/feedback/aime.svg';
import CibleIcon from '../../../../../assets/icons/pilotage/feedback/cible.svg';
import CommentsIcon from '../../../../../assets/icons/pilotage/feedback/commentaires.svg';
import ContentIcon from '../../../../../assets/icons/pilotage/feedback/content.svg';
import DetesteIcon from '../../../../../assets/icons/pilotage/feedback/deteste.svg';
import LuIcon from '../../../../../assets/icons/pilotage/feedback/lu.svg';
import MecontentIcon from '../../../../../assets/icons/pilotage/feedback/mecontent.svg';
import RecommandationIcon from '../../../../../assets/icons/pilotage/feedback/recommandation.svg';
import ShareIcon from '../../../../../assets/icons/pilotage/feedback/share.svg';
import {
  GET_INDICATEUR_STATISTIQUE_BUSINESS,
  GET_INDICATEUR_STATISTIQUE_FEEDBACK,
  GET_STATISTIQUE_BUSINESS,
  GET_STATISTIQUE_FEEDBACK,
} from '../../../../../graphql/Pilotage';
import {
  GET_PILOTAGE_ACTU_SOURCE,
  GET_PILOTAGE_CIBLES,
  GET_PILOTAGE_OC_SOURCE,
} from '../../../../../graphql/Pilotage/local';
import {
  INDICATEUR_STATISTIQUE_BUSINESS,
  INDICATEUR_STATISTIQUE_BUSINESSVariables,
} from '../../../../../graphql/Pilotage/types/INDICATEUR_STATISTIQUE_BUSINESS';
import {
  INDICATEUR_STATISTIQUE_FEEDBACK,
  INDICATEUR_STATISTIQUE_FEEDBACKVariables,
} from '../../../../../graphql/Pilotage/types/INDICATEUR_STATISTIQUE_FEEDBACK';
import {
  STATISTIQUE_BUSINESS,
  STATISTIQUE_BUSINESSVariables,
} from '../../../../../graphql/Pilotage/types/STATISTIQUE_BUSINESS';
import {
  STATISTIQUE_FEEDBACK,
  STATISTIQUE_FEEDBACKVariables,
} from '../../../../../graphql/Pilotage/types/STATISTIQUE_FEEDBACK';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { AppAuthorization } from '../../../../../services/authorization';
import { getUser } from '../../../../../services/LocalStorage';
import {
  BusinessIndicateur,
  FeedbackIndicateur,
  OrederByBusiness,
  PilotageBusiness,
  PilotageFeedback,
} from '../../../../../types/graphql-global-types';
import { roundNumber } from '../../../../../utils/Helpers';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import Backdrop from '../../../../Common/Backdrop';
import CustomButton from '../../../../Common/CustomButton';
import { PilotageSuiviPublicitaire } from '../../../../Common/newWithSearch/ComponentInitializer';
import { ActualiteInfo } from '../../Actualite/ActualiteInfo';
import CatalogueProduitOcInfo from '../../OperationCommerciale/CatalogueProduitOcInfo';
import { ChartEvolution } from './Chart';
import useQueryBusinessTable from './hooks/useQueryBusinessTable';
import useQueryFeedbackTable from './hooks/useQueryFeedbackTable';
import useStyles from './styles';
import { TableGlobal, TableItem, TableOperation, TablePharmacie } from './Table';
import { setDataTable } from './utils';

export interface ChartInterface {
  options: object;
  series: any[];
}

interface StateInterface {
  chartTitle: string;
  chartSubTitle: string;
  statTitle: string;
  tableTitle: string;
  statData: any;
  tableData: any[];
  tableTotal: number;
  activeIndicatorCode: BusinessIndicateur | FeedbackIndicateur;
  idPharmaciesSelected: string[];
  pharmacieIds: string[];
  operationIds: string[];
  codeItem: string | null;
  idItemAssocies: string[];
  tableActiveRow: any;
  chartTargetValue: number;
  chartCurrentValue: number;
  nbPharmacieSmeyley: number;
  jaugeOneValue: number;
  jaugeTwoValue: number;
  showEvolution: boolean;
}

interface IndicatorItem {
  id: number;
  code: BusinessIndicateur | FeedbackIndicateur;
  value: number;
  label: string;
  iconSrc: string;
}

export enum TypePilotage {
  BUSINESS,
  FEEDBACK,
}

export interface DashboardProps {
  typePilotage: TypePilotage;
}

export const BASE_URL = '/pilotage/dashboard';
export const BASE_URL_BUSINESS = `${BASE_URL}/business`;
export const BASE_URL_FEEDBACK = `${BASE_URL}/feedback`;
export const BASE_URL_PUBLICITE = `${BASE_URL}/publicite`;

export const GLOBAL_URL_BUSINESS = `${BASE_URL_BUSINESS}/global`;
export const OP_URL_BUSINESS = `${BASE_URL_BUSINESS}/operation`;
export const PHARMA_URL_BUSINESS = `${BASE_URL_BUSINESS}/pharmacie`;

export const GLOBAL_URL_FEEDBACK = `${BASE_URL_FEEDBACK}/global`;
export const PHARMA_URL_FEEDBACK = `${BASE_URL_FEEDBACK}/pharmacie`;
export const ITEM_URL_FEEDBACK = `${BASE_URL_FEEDBACK}/item`;

export const initPilotageApolloLocalState = (client: ApolloClient<object>) => {
  client.writeData({ data: { pilotageOcSource: null } });
  client.writeData({ data: { pilotageActuSource: null } });
  client.writeData({ data: { pilotageCibles: null } });
};

export const formatOrderBy = (value: string): string => {
  const valueArray = value.split('.');
  if (valueArray.length > 1) {
    const val = last(valueArray);
    return val ? val : value;
  }
  return value;
};

const Dashboard: FC<DashboardProps & RouteComponentProps> = ({
  // typePilotage,
  location: { pathname },
  history: { push },
}) => {
  const classes = useStyles({});
  const theme = useTheme();
  const client = useApolloClient();

  const user = getUser();
  const auth = new AppAuthorization(user);

  const { BUSINESS, FEEDBACK } = TypePilotage;

  const isOnBusiness: boolean = pathname.includes(BASE_URL_BUSINESS);
  const isOnFeedback: boolean = pathname.includes(BASE_URL_FEEDBACK);

  const isGlobalView = pathname === GLOBAL_URL_BUSINESS || pathname === GLOBAL_URL_FEEDBACK;
  const isOpView = pathname === OP_URL_BUSINESS;
  const isPharmaView = pathname === PHARMA_URL_BUSINESS || pathname === PHARMA_URL_FEEDBACK;
  const isItemView = pathname === ITEM_URL_FEEDBACK;
  const isPublicite = pathname === BASE_URL_PUBLICITE;
  const title = isOnBusiness
    ? 'Pilotage business'
    : isPublicite
    ? 'Pilotage des espaces publicitaires'
    : 'Pilotage feedback';
  const defaultTableTitle = 'Liste des Pharmacies';
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(5);
  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(5);

  const [order, setOrder] = useState<any>('asc');
  const [orderBy, setOrderBy] = useState<any>('');
  const [activeSemestre, setActiveSemestre] = useState<number>(2);

  const defaultActiveIndicatorCode: BusinessIndicateur | FeedbackIndicateur = isOnBusiness
    ? BusinessIndicateur.CA
    : FeedbackIndicateur.CIBLE;
  const defaultChartTitle: string = 'Graphique';
  const defaultChartSubTitle: string = 'CA total en €';

  const [state, setState] = useState<StateInterface>({
    chartTitle: defaultChartTitle,
    chartSubTitle: defaultChartSubTitle,
    statTitle: 'Statistiques globales',
    tableTitle: defaultTableTitle,
    statData: null,
    tableData: [],
    tableTotal: 0,
    activeIndicatorCode: defaultActiveIndicatorCode,
    idPharmaciesSelected: [],
    pharmacieIds: [],
    operationIds: [],
    codeItem: null,
    idItemAssocies: [],
    tableActiveRow: null,
    chartTargetValue: 1000,
    chartCurrentValue: 500.2,
    nbPharmacieSmeyley: 0,
    jaugeOneValue: 0,
    jaugeTwoValue: 0,
    showEvolution: false,
  });

  const {
    chartTitle,
    chartSubTitle,
    statTitle,
    tableTitle,
    activeIndicatorCode,
    statData,
    tableData,
    tableTotal,
    pharmacieIds,
    operationIds,
    codeItem,
    idItemAssocies,
    chartTargetValue,
    chartCurrentValue,
    nbPharmacieSmeyley,
    jaugeOneValue,
    jaugeTwoValue,
    tableActiveRow,
    idPharmaciesSelected,
    showEvolution,
  } = state;

  const queryVarPilotageBusiness = isPharmaView
    ? PilotageBusiness.PHARMACIE
    : isOpView
    ? PilotageBusiness.OPERATION
    : PilotageBusiness.GLOBAL;
  const queryVarPilotageFeedback = isPharmaView
    ? PilotageFeedback.PHARMACIE
    : isItemView
    ? PilotageFeedback.ITEM
    : PilotageFeedback.GLOBAL;

  const jaugeColorPalette = {
    bad: '#C24C46',
    low: '#D87E55',
    average: '#E3C348',
    good: '#90A85D',
    excellent: '#488D65',
  };
  const { bad, low, average, good, excellent } = jaugeColorPalette;
  const jaugeColor =
    jaugeOneValue <= 20
      ? bad
      : jaugeOneValue > 20 && jaugeOneValue <= 40
      ? low
      : jaugeOneValue > 40 && jaugeOneValue <= 60
      ? average
      : jaugeOneValue > 60 && jaugeOneValue <= 80
      ? good
      : excellent;

  const initActiveIndicatorCode = () => {
    if (activeIndicatorCode !== defaultActiveIndicatorCode) {
      setState(prevState => ({
        ...prevState,
        activeIndicatorCode: defaultActiveIndicatorCode,
        chartSubTitle: defaultChartTitle,
      }));
    }
  };

  const initClickedTableRow = () => {
    if (tableActiveRow !== null) {
      setState(prevState => ({ ...prevState, tableActiveRow: null, pharmacieIds: [] }));
    }
  };

  let chartEvolutionValue = (chartCurrentValue * 100) / chartTargetValue;
  chartEvolutionValue =
    isNumber(chartEvolutionValue) && !isNaN(chartEvolutionValue) && chartEvolutionValue !== Infinity
      ? roundNumber(chartEvolutionValue)
      : 0;

  const INDICATOR_LIST_BUSINESS: IndicatorItem[] = [
    {
      id: 1,
      code: BusinessIndicateur.CA,
      value: (statData && statData.caTotal) || 0,
      label: 'CA total en €',
      iconSrc: CaTotalIcon,
    },
    {
      id: 2,
      code: BusinessIndicateur.NB_COMMANDE,
      value: (statData && statData.nbCommandes) || 0,
      label: 'Nbre de Commandes',
      iconSrc: NbrCmdIcon,
    },
    {
      id: 3,
      code: BusinessIndicateur.NB_PHARMACIE,
      value: (statData && statData.nbPharmacies) || 0,
      label: 'Nbre de pharmacies',
      iconSrc: NbrPharmaIcon,
    },
    {
      id: 4,
      code: BusinessIndicateur.NB_PRODUIT,
      value: (statData && statData.nbProduits) || 0,
      label: 'Nbre de produits',
      iconSrc: NbrProductIcon,
    },
    {
      id: 5,
      code: BusinessIndicateur.NB_LIGNE,
      value: (statData && statData.nbLignes) || 0,
      label: 'Nbre de lignes',
      iconSrc: NbrLigneIcon,
    },
    {
      id: 6,
      code: BusinessIndicateur.CA_MOYEN,
      value: (statData && statData.caMoyenParPharmacie) || 0,
      label: 'CA moyen par pharmacie',
      iconSrc: CaMoyennePharmaIcon,
    },
    {
      id: 7,
      code: BusinessIndicateur.TOTAL_REMISE,
      value: (statData && statData.totalRemise) || 0,
      label: 'Total remise en €',
      iconSrc: TotalRemiseIcon,
    },
    {
      id: 8,
      code: BusinessIndicateur.REMISE,
      value: (statData && statData.globaliteRemise) || 0,
      label: 'Remise en %',
      iconSrc: RemisePercentIcon,
    },
  ];

  const INDICATOR_LIST_FEEDBACK: IndicatorItem[] = [
    {
      id: 1,
      code: FeedbackIndicateur.CIBLE,
      value: (statData && statData.cibles) || 0,
      label: 'Cible',
      iconSrc: CibleIcon,
    },
    {
      id: 2,
      code: FeedbackIndicateur.LU,
      value: (statData && statData.lu) || 0,
      label: 'Lu',
      iconSrc: LuIcon,
    },
    {
      id: 3,
      code: FeedbackIndicateur.ADORE,
      value: (statData && statData.adore) || 0,
      label: 'Adore',
      iconSrc: AdoreIcon,
    },
    {
      id: 4,
      code: FeedbackIndicateur.CONTENT,
      value: (statData && statData.content) || 0,
      label: 'Content',
      iconSrc: ContentIcon,
    },
    {
      id: 5,
      code: FeedbackIndicateur.AIME,
      value: (statData && statData.aime) || 0,
      label: 'Aime',
      iconSrc: AimeIcon,
    },
    {
      id: 6,
      code: FeedbackIndicateur.MECONTENT,
      value: (statData && statData.mecontent) || 0,
      label: 'Mécontent',
      iconSrc: MecontentIcon,
    },
    {
      id: 7,
      code: FeedbackIndicateur.DETESTE,
      value: (statData && statData.deteste) || 0,
      label: 'Déteste',
      iconSrc: DetesteIcon,
    },
    {
      id: 8,
      code: FeedbackIndicateur.COMMENTAIRE,
      value: (statData && statData.commentaires) || 0,
      label: 'Commentaires',
      iconSrc: CommentsIcon,
    },
    {
      id: 9,
      code: FeedbackIndicateur.PARTAGE,
      value: (statData && statData.partages) || 0,
      label: 'Partages',
      iconSrc: ShareIcon,
    },
    {
      id: 10,
      code: FeedbackIndicateur.RECOMMANDATION,
      value: (statData && statData.recommandations) || 0,
      label: 'Recommandations',
      iconSrc: RecommandationIcon,
    },
  ];

  const INDICATOR_DADA: IndicatorItem[] = isOnBusiness
    ? INDICATOR_LIST_BUSINESS
    : INDICATOR_LIST_FEEDBACK;

  const ocSourceResult = useQuery(GET_PILOTAGE_OC_SOURCE);
  const actuSourceResult = useQuery(GET_PILOTAGE_ACTU_SOURCE);

  const [source, setSource] = useState<any>(null);
  /**
   * Set source
   */
  useEffect(() => {
    if (ocSourceResult && ocSourceResult.data && ocSourceResult.data.pilotageOcSource) {
      setSource(ocSourceResult.data.pilotageOcSource);
    }

    if (actuSourceResult && actuSourceResult.data && actuSourceResult.data.pilotageActuSource) {
      setSource(actuSourceResult.data.pilotageActuSource);
    }
  }, [ocSourceResult, actuSourceResult]);

  const pilotageCiblesResult = useQuery(GET_PILOTAGE_CIBLES);

  console.log('pilotageCiblesResult : ', pilotageCiblesResult);
  /**
   * Update table title
   */
  useEffect(() => {
    if (isOpView) {
      setState(prevState => ({
        ...prevState,
        tableTitle: 'Liste des Opération Commerciales',
      }));
    }
    if (isPharmaView) {
      setState(prevState => ({ ...prevState, tableTitle: 'Liste des Pharmacies' }));
    }
    if (isItemView) {
      setState(prevState => ({ ...prevState, tableTitle: 'Liste des items' }));
    }
    if (isGlobalView) {
      setState(prevState => ({ ...prevState, tableTitle: defaultTableTitle }));
    }
  }, [isOpView, isPharmaView, isItemView, isGlobalView]);

  const onClickBack = () => {
    push('/pilotage');
    initPilotageApolloLocalState(client);
  };

  const editTarget = () => {
    if (isOnBusiness) push('/pilotage/business/operation');
    if (isOnFeedback) push('/pilotage/feedback');
    initPilotageApolloLocalState(client);
  };

  const handleViewGlobal = () => {
    initActiveIndicatorCode();
    initClickedTableRow();
    updatePharmaAndOpIds();
    if (isOnBusiness) {
      push(GLOBAL_URL_BUSINESS);
    }
    if (isOnFeedback) {
      push(GLOBAL_URL_FEEDBACK);
    }
  };

  const handleViewByOperation = () => {
    initActiveIndicatorCode();
    initClickedTableRow();
    updatePharmaAndOpIds();
    if (isOnBusiness) {
      push(OP_URL_BUSINESS);
    }
  };

  const handleViewByItem = () => {
    initActiveIndicatorCode();
    initClickedTableRow();
    updatePharmaAndOpIds();
    if (isOnFeedback) {
      push(ITEM_URL_FEEDBACK);
    }
  };

  const handleViewByPharma = () => {
    initActiveIndicatorCode();
    initClickedTableRow();
    updatePharmaAndOpIds();
    if (isOnBusiness) {
      push(PHARMA_URL_BUSINESS);
    }
    if (isOnFeedback) {
      push(PHARMA_URL_FEEDBACK);
    }
  };

  const handleClickIndicator = (item: IndicatorItem) => () => {
    setState(prevState => ({
      ...prevState,
      activeIndicatorCode: item.code,
      chartSubTitle: item.label,
    }));

    if (tableActiveRow) {
      const newPharmaIds: string[] =
        tableActiveRow.pharmacie && tableActiveRow.pharmacie.id
          ? [tableActiveRow.pharmacie.id]
          : [];
      const items: any[] =
        pilotageCiblesResult &&
        pilotageCiblesResult.data &&
        pilotageCiblesResult.data.pilotageCibles &&
        pilotageCiblesResult.data.pilotageCibles.items;
      const feedbackOpIds: any[] = (items && items.map(i => i.id)) || [];
      const newOpIds: string[] =
        feedbackOpIds && feedbackOpIds.length > 0
          ? feedbackOpIds
          : source
          ? [source.id]
          : tableActiveRow.operation && tableActiveRow.operation.id
          ? [tableActiveRow.operation.id]
          : [];

      setState(prevState => ({
        ...prevState,
        pharmacieIds: newPharmaIds,
        operationIds: newOpIds,
      }));
    }
  };

  const onClickTableRow = (row: any) => {
    if (row) {
      initActiveIndicatorCode();
      const itemName =
        (row.pharmacie && row.pharmacie.nom) ||
        (row.operation && row.operation.nom) ||
        (row.itemAssocie && row.itemAssocie.libelle);
      const newStatTitle = `Statistiques ${itemName ? itemName : ''}`;

      setState(prevState => ({
        ...prevState,
        tableActiveRow: row,
        statTitle: newStatTitle,
      }));

      const items: any[] =
        pilotageCiblesResult &&
        pilotageCiblesResult.data &&
        pilotageCiblesResult.data.pilotageCibles &&
        pilotageCiblesResult.data.pilotageCibles.items;

      const feedbackOpIds: any[] = (items && items.map(i => i.id)) || [];

      const newPharmaIds: string[] = isOpView
        ? idPharmaciesSelected
        : row.pharmacie && row.pharmacie.id
        ? [row.pharmacie.id]
        : [];

      const newOpIds: string[] =
        !isOpView && feedbackOpIds && feedbackOpIds.length > 0
          ? feedbackOpIds
          : source
          ? [source.id]
          : row.operation && row.operation.id
          ? [row.operation.id]
          : [];

      const codeCodeItem: string =
        (row.item && row.item.codeItem) ||
        (pilotageCiblesResult &&
          pilotageCiblesResult.data &&
          pilotageCiblesResult.data.pilotageCibles &&
          pilotageCiblesResult.data.pilotageCibles.item &&
          pilotageCiblesResult.data.pilotageCibles.item.codeItem);

      const newCodeItem =
        (codeCodeItem && codeCodeItem.startsWith('A')
          ? 'OPERACOM'
          : row.item && row.item.parent && row.item.parent.code) ||
        (row.item && row.item.code) ||
        (row.itemAssocie && row.itemAssocie.item && row.itemAssocie.item.code);

      const newIdItemAssocies: string[] =
        row.itemAssocie && row.itemAssocie.id
          ? [row.itemAssocie.id]
          : feedbackOpIds && feedbackOpIds.length > 0
          ? feedbackOpIds
          : [];

      // Execute query for getting data of clicked row
      if (newPharmaIds && newOpIds && isOnBusiness) {
        queryBusiness({
          variables: {
            pilotage: queryVarPilotageBusiness,
            idOperations: newOpIds,
            idPharmacies: newPharmaIds,
            idPharmaciesSelected,
          },
        });

        queryBusinessChart({
          variables: {
            pilotage: queryVarPilotageBusiness,
            idOperations: newOpIds,
            idPharmacies: newPharmaIds,
            indicateur: activeIndicatorCode as any,
            idPharmaciesSelected,
          },
        });
      }

      if (newPharmaIds && newCodeItem && isOnFeedback && newIdItemAssocies) {
        queryFeedback({
          variables: {
            pilotage: queryVarPilotageFeedback,
            codeItem: newCodeItem,
            idItemAssocies: newIdItemAssocies,
            idPharmacies: newPharmaIds,
          },
        });

        queryFeedbackChart({
          variables: {
            pilotage: queryVarPilotageFeedback,
            codeItem: newCodeItem,
            idItemAssocies: newIdItemAssocies,
            idPharmacies: newPharmaIds,
            indicateur: activeIndicatorCode as any,
          },
        });
      }
    }
  };

  const handleShowEvolution = () => {
    if (!showEvolution) {
      setState(prevState => ({ ...prevState, showEvolution: true }));
    }
  };

  const updateActiveSemestre = (value: number) => () => {
    setActiveSemestre(value);
  };

  const handleHideEvolution = () => {
    if (showEvolution) {
      setState(prevState => ({ ...prevState, showEvolution: false }));
    }
  };

  /**
   * Update pharmacieIds, operationIds, idPharmaciesSelected
   */
  const updatePharmaAndOpIds = () => {
    if (pilotageCiblesResult) {
      const { data } = pilotageCiblesResult;
      if (data && data.pilotageCibles) {
        const pharmaIds: string[] =
          data.pilotageCibles.pharmacies && data.pilotageCibles.pharmacies.length > 0
            ? data.pilotageCibles.pharmacies.map((item: any) => item.id)
            : [];
        if (pharmaIds && pharmaIds.length > 0) {
          setState(prevState => ({
            ...prevState,
            pharmacieIds: pharmaIds,
            idPharmaciesSelected: pharmaIds,
          }));
        }
      }

      if (data && data.pilotageCibles && data.pilotageCibles.pilotageType === 'business') {
        const operationIds: string[] =
          data.pilotageCibles.items && data.pilotageCibles.items.length > 0
            ? data.pilotageCibles.items.map((item: any) => item.id)
            : [];
        if (operationIds && operationIds.length > 0) {
          setState(prevState => ({ ...prevState, operationIds }));
        }
      }

      if (data && data.pilotageCibles && data.pilotageCibles.pilotageType === 'feedback') {
        const newIdItemAssocies: string[] =
          data.pilotageCibles.items && data.pilotageCibles.items.length > 0
            ? data.pilotageCibles.items.map((item: any) => item.id)
            : [];

        const codeCodeItem: string =
          data.pilotageCibles && data.pilotageCibles.item && data.pilotageCibles.item.codeItem;
        const newCodeItem =
          codeCodeItem && codeCodeItem.startsWith('A')
            ? 'OPERACOM'
            : (data.pilotageCibles &&
                data.pilotageCibles.item &&
                data.pilotageCibles.item.parent &&
                data.pilotageCibles.item.parent.code) ||
              (data.pilotageCibles && data.pilotageCibles.item && data.pilotageCibles.item.code);

        if (newIdItemAssocies && newIdItemAssocies.length > 0 && newCodeItem) {
          setState(prevState => ({
            ...prevState,
            idItemAssocies: newIdItemAssocies,
            codeItem: newCodeItem,
          }));
        }
      }
    }
  };

  useEffect(() => {
    updatePharmaAndOpIds();
  }, [pilotageCiblesResult]);

  /**
   * QUERY INDICATEUR STATISTIQUE BUSINESS
   */
  const [queryBusiness, queryBusinessResult] = useLazyQuery<
    INDICATEUR_STATISTIQUE_BUSINESS,
    INDICATEUR_STATISTIQUE_BUSINESSVariables
  >(GET_INDICATEUR_STATISTIQUE_BUSINESS, {
    fetchPolicy: 'network-only',
    onCompleted: data => {},
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  /**
   * QUERY INDICATEUR STATISTIQUE FEEDBACK
   */
  const [queryFeedback, queryFeedbackResult] = useLazyQuery<
    INDICATEUR_STATISTIQUE_FEEDBACK,
    INDICATEUR_STATISTIQUE_FEEDBACKVariables
  >(GET_INDICATEUR_STATISTIQUE_FEEDBACK, {
    fetchPolicy: 'network-only',
    onCompleted: data => {},
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  /**
   * Query PHARMACIE_OPERATION_STATISTIQUE_BUSINESS [Tableau]
   */
  const { queryBusinessTable, queryBusinessTableResult } = useQueryBusinessTable();

  /**
   * Query GET_PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK [Tableau]
   */
  const { queryFeedbackTable, queryFeedbackTableResult } = useQueryFeedbackTable();

  /**
   * Query Business Chart
   */
  const [queryBusinessChart, { data: businessChartData }] = useLazyQuery<
    STATISTIQUE_BUSINESS,
    STATISTIQUE_BUSINESSVariables
  >(GET_STATISTIQUE_BUSINESS, { fetchPolicy: 'network-only' });

  /**
   * Query Feedback Chart
   */
  const [queryFeedbackChart, { data: feedbackChartData }] = useLazyQuery<
    STATISTIQUE_FEEDBACK,
    STATISTIQUE_FEEDBACKVariables
  >(GET_STATISTIQUE_FEEDBACK, { fetchPolicy: 'network-only' });

  /**
   * Set orderBy
   */
  useEffect(() => {
    if (isGlobalView || isPharmaView) {
      setOrderBy(OrederByBusiness.nom);
    } else {
      setOrderBy(OrederByBusiness.libelle);
    }
  }, [isGlobalView, isPharmaView]);

  /**
   * Execute query indicators
   */
  useEffect(() => {
    if (pharmacieIds.length > 0 && operationIds.length > 0) {
      if (isOnBusiness) {
        queryBusiness({
          variables: {
            pilotage: queryVarPilotageBusiness,
            idOperations: operationIds,
            idPharmacies: pharmacieIds,
            idPharmaciesSelected,
          },
        });
      }
    }
    if (pharmacieIds.length > 0 && idItemAssocies.length > 0 && codeItem) {
      if (isOnFeedback) {
        queryFeedback({
          variables: {
            pilotage: queryVarPilotageFeedback,
            codeItem,
            idItemAssocies,
            idPharmacies: pharmacieIds,
          },
        });
      }
    }
  }, [
    isOnBusiness,
    isOnFeedback,
    queryVarPilotageBusiness,
    queryVarPilotageFeedback,
    pharmacieIds,
    operationIds,
    idItemAssocies,
    codeItem,
  ]);

  /**
   * Execute query for tables
   */
  useEffect(() => {
    if (!tableActiveRow) {
      if (pharmacieIds.length > 0 && operationIds.length > 0) {
        if (isOnBusiness) {
          queryBusinessTable({
            variables: {
              pilotage: queryVarPilotageBusiness,
              idOperations: operationIds,
              idPharmacies: pharmacieIds,
              take,
              skip,
              orderBy: formatOrderBy(orderBy) as any,
              isAsc: order === 'asc' ? true : false,
            },
          });
        }
      }
      if (pharmacieIds.length > 0 && idItemAssocies.length > 0 && codeItem) {
        if (isOnFeedback) {
          queryFeedbackTable({
            variables: {
              codeItem,
              pilotage: queryVarPilotageFeedback,
              idItemAssocies,
              idPharmacies: pharmacieIds,
              take,
              skip,
              orderBy: formatOrderBy(orderBy) as any,
              isAsc: order === 'asc' ? true : false,
            },
          });
        }
      }
    }
  }, [
    isOnBusiness,
    isOnFeedback,
    queryVarPilotageBusiness,
    queryVarPilotageFeedback,
    pharmacieIds,
    operationIds,
    idItemAssocies,
    codeItem,
    take,
    skip,
    orderBy,
    order,
    tableActiveRow,
  ]);

  /**
   * Execute query for chart
   */
  useEffect(() => {
    if (pharmacieIds.length > 0 && operationIds.length > 0) {
      if (isOnBusiness) {
        queryBusinessChart({
          variables: {
            pilotage: queryVarPilotageBusiness,
            indicateur: activeIndicatorCode as any,
            idOperations: operationIds,
            idPharmacies: pharmacieIds,
            idPharmaciesSelected,
          },
        });
      }
    }
    if (pharmacieIds.length > 0 && idItemAssocies.length > 0 && codeItem) {
      if (isOnFeedback) {
        queryFeedbackChart({
          variables: {
            codeItem,
            indicateur: activeIndicatorCode as any,
            pilotage: queryVarPilotageFeedback,
            idItemAssocies,
            idPharmacies: pharmacieIds,
          },
        });
      }
    }
  }, [
    isOnBusiness,
    isOnFeedback,
    queryVarPilotageBusiness,
    queryVarPilotageFeedback,
    pharmacieIds,
    operationIds,
    idItemAssocies,
    codeItem,
    activeIndicatorCode,
  ]);

  /**
   * SET STAT DATA
   */
  useEffect(() => {
    const { data } = queryBusinessResult;
    const feedbackData = queryFeedbackResult.data;
    if (data && data.indicateurStatistiqueBusiness) {
      setState(prevState => ({
        ...prevState,
        statData: data.indicateurStatistiqueBusiness,
      }));
    }
    if (feedbackData && feedbackData.indicateurStatistiqueFeedback) {
      setState(prevState => ({
        ...prevState,
        statData: feedbackData.indicateurStatistiqueFeedback,
      }));
    }
  }, [queryBusinessResult, queryFeedbackResult]);

  /**
   * SET TABLE DATA
   */
  useEffect(() => {
    setDataTable(queryBusinessTableResult, queryFeedbackTableResult, setState);
  }, [queryBusinessTableResult, queryFeedbackTableResult]);

  /**
   * SET CHART DATA
   */
  useEffect(() => {
    if (isOnBusiness && businessChartData && businessChartData.statistiqueBusiness) {
      const data = businessChartData.statistiqueBusiness;
      setState(prevState => ({
        ...prevState,
        chartTargetValue: parseFloat(data.objectif as any),
        chartCurrentValue: parseFloat(data.value as any),
      }));
    }

    if (isOnFeedback && feedbackChartData && feedbackChartData.statistiqueFeedback) {
      const data = feedbackChartData.statistiqueFeedback;
      setState(prevState => ({
        ...prevState,
        chartTargetValue: parseFloat(data.objectif as any),
        chartCurrentValue: parseFloat(data.value as any),
        nbPharmacieSmeyley: parseFloat(data.nbPharmacieSmeyley as any),
      }));
    }
  }, [businessChartData, feedbackChartData, isOnBusiness, isOnFeedback]);

  const disabledViewByOp = (): boolean => {
    if (source || !auth.isAuthorizedToViewPilotageByOperation()) {
      return true;
    }
    return false;
  };

  const disabledViewByPharma = (): boolean => {
    if (!auth.isAuthorizedToViewPilotageByPharmacie()) {
      return true;
    }
    return false;
  };

  const Head = () => {
    return (
      <div className={classes.pilotageHead}>
        <CustomButton
          color="inherit"
          startIcon={<ArrowBack />}
          variant="text"
          onClick={onClickBack}
          className={classes.btnBack}
          disabled={source ? true : false}
        >
          Retour au choix de pilotage
        </CustomButton>

        <Typography className={classes.pilotageHeadTitle}>{title}</Typography>
        {!isPublicite ? (
          <CustomButton color="default" onClick={editTarget} disabled={source ? true : false}>
            Redefinir mes cibles
          </CustomButton>
        ) : (
          <div></div>
        )}
      </div>
    );
  };

  const ViewTypeBtns = () => {
    return (
      <div className={classes.viewTypeContainer}>
        <CustomButton
          variant={isGlobalView ? 'contained' : 'outlined'}
          color={isGlobalView ? 'secondary' : 'inherit'}
          className={
            isGlobalView ? classes.btnView : classnames(classes.btnView, classes.btnOutlined)
          }
          onClick={handleViewGlobal}
        >
          Gobal
        </CustomButton>
        {isOnBusiness && (
          <CustomButton
            variant={isOpView ? 'contained' : 'outlined'}
            color={isOpView ? 'secondary' : 'inherit'}
            className={
              isOpView ? classes.btnView : classnames(classes.btnView, classes.btnOutlined)
            }
            onClick={handleViewByOperation}
            disabled={disabledViewByOp()}
          >
            Par opération commerciale
          </CustomButton>
        )}
        {isOnFeedback && (
          <CustomButton
            variant={isItemView ? 'contained' : 'outlined'}
            color={isItemView ? 'secondary' : 'inherit'}
            className={
              isItemView ? classes.btnView : classnames(classes.btnView, classes.btnOutlined)
            }
            onClick={handleViewByItem}
            disabled={source ? true : false}
          >
            Par Item
          </CustomButton>
        )}
        <CustomButton
          variant={isPharmaView ? 'contained' : 'outlined'}
          color={isPharmaView ? 'secondary' : 'default'}
          className={
            isPharmaView ? classes.btnView : classnames(classes.btnView, classes.btnOutlined)
          }
          onClick={handleViewByPharma}
          disabled={disabledViewByPharma()}
        >
          Par pharmacie
        </CustomButton>
      </div>
    );
  };

  const IndicatorCard = () => {
    return (
      <Card className={classes.indicatorCard}>
        <CardHeader title="Indicateurs" className={classes.cardTitle} />
        <CardContent className={classes.cardContent}>
          <List component="nav">
            {INDICATOR_DADA.map((item, index) => {
              const { code } = item;
              return (
                <ListItem
                  key={`indicator_data_item_${index}`}
                  button={true}
                  className={
                    code === activeIndicatorCode
                      ? classnames(classes.listItem, classes.listItemActive)
                      : classes.listItem
                  }
                  onClick={handleClickIndicator(item)}
                >
                  <ListItemIcon>
                    <img src={item.iconSrc} alt={item.label} className={classes.icon} />
                  </ListItemIcon>
                  <ListItemText primary={item.label} />
                </ListItem>
              );
            })}
          </List>
        </CardContent>
      </Card>
    );
  };

  const isNoTarget = (): boolean => {
    if (
      isOnBusiness &&
      (activeIndicatorCode === BusinessIndicateur.NB_COMMANDE ||
        activeIndicatorCode === BusinessIndicateur.NB_PRODUIT ||
        activeIndicatorCode === BusinessIndicateur.REMISE ||
        activeIndicatorCode === BusinessIndicateur.TOTAL_REMISE)
    ) {
      return true;
    }
    return false;
  };

  const defaultChartOptions = {
    chart: {
      type: 'bar',
      toolbar: {
        show: false,
      },
      events: {
        click: (chart, w, e) => {
          console.log(chart, w, e);
        },
      },
    },
    grid: {
      show: true,
      position: 'back',
      strokeDashArray: 5,
      xaxis: {
        lines: {
          show: true,
        },
      },
      yaxis: {
        lines: {
          show: true,
        },
      },
    },
    xaxis: {
      categories: isNoTarget() ? ['Réalisé'] : ['Objectif', 'Réalisé'],
      // categories: ['Jan'],
      labels: {
        style: {
          colors: isNoTarget() ? ['#E34168'] : [theme.palette.primary.main, '#E34168'],
          fontSize: '14px',
        },
      },
      axisBorder: {
        show: false,
      },
    },
    dataLabels: {
      enabled: true,
      formatter: (value: any) => {
        return Math.round(value);
      },
    },
    // labels: ['Objectif', 'Réalisé'],
    colors: isNoTarget() ? ['#E34168'] : [theme.palette.primary.main, '#E34168'],
    legend: { show: false },
    plotOptions: {
      bar: {
        columnWidth: isNoTarget() ? '17%' : '35%',
        distributed: true,
        // distributed: false,
      },
    },
  };

  const chartSeries = [
    {
      data: isNoTarget()
        ? [roundNumber(chartCurrentValue)]
        : [roundNumber(chartTargetValue), roundNumber(chartCurrentValue)],
      name: chartSubTitle,
    },
  ];

  // const chartSeries = [
  //   { data: [chartTargetValue], name: 'Objectif' },
  //   { data: [chartCurrentValue], name: 'Réalisé' },
  // ];

  const [chartConfig, setChartConfig] = useState<ChartInterface>({
    options: defaultChartOptions,
    series: chartSeries,
  });

  /**
   * Update chart series
   */
  useEffect(() => {
    setChartConfig({ options: defaultChartOptions, series: chartSeries });
  }, [chartSubTitle, chartCurrentValue, chartTargetValue, nbPharmacieSmeyley, isNoTarget()]);

  const ChartCard = () => {
    return (
      <Card className={classes.chartCard}>
        <div className={classes.switchChartBtnsContainer}>
          <div className={classes.chartTypesBtns}>
            <CustomButton
              variant="text"
              className={showEvolution ? classes.btnText : classes.btnTextActive}
              onClick={handleHideEvolution}
            >
              Objectif vs Réalisé
            </CustomButton>
            <CustomButton
              variant="text"
              className={showEvolution ? classes.btnTextActive : classes.btnText}
              onClick={handleShowEvolution}
            >
              Evolution
            </CustomButton>
          </div>
          {showEvolution && (
            <div className={classes.semestreBtns}>
              <CustomButton
                variant="text"
                className={activeSemestre === 2 ? classes.btnText : classes.btnTextActive}
                onClick={updateActiveSemestre(1)}
              >
                S1
              </CustomButton>
              <CustomButton
                variant="text"
                className={activeSemestre === 2 ? classes.btnTextActive : classes.btnText}
                onClick={updateActiveSemestre(2)}
              >
                S2
              </CustomButton>
            </div>
          )}
        </div>

        <CardHeader
          title={chartTitle}
          subheader={chartSubTitle}
          className={classes.chartCardTitle}
        />
        <CardContent className={classes.cardContent}>
          {!showEvolution && (
            <>
              {!isNoTarget() && (
                <div className={classes.evolutionValue} style={{ color: jaugeColor }}>
                  {chartEvolutionValue < 50 && <ArrowDownward />}
                  {chartEvolutionValue > 50 && <ArrowUpward />}
                  <Typography>{`${chartEvolutionValue} %`}</Typography>
                </div>
              )}
              <Chart
                options={chartConfig.options as any}
                series={chartConfig.series}
                type="bar"
                height="460"
                width="550"
              />
            </>
          )}
          {showEvolution && (
            <ChartEvolution
              pilotage={queryVarPilotageBusiness}
              indicateur={activeIndicatorCode as any}
              year={moment(new Date()).year()}
              idOperations={operationIds}
              idPharmacies={pharmacieIds}
              idPharmaciesSelected={idPharmaciesSelected}
              chartSubTitle={chartSubTitle}
              activeSemestre={activeSemestre}
              showEvolution={showEvolution}
              isOnBusiness={isOnBusiness}
              isOnFeedback={isOnFeedback}
              codeItem={codeItem || ''}
              idItemAssocies={idItemAssocies}
            />
          )}
        </CardContent>
      </Card>
    );
  };

  const jaugeOptions = {
    chart: {
      height: 280,
      type: 'radialBar',
    },
    plotOptions: {
      radialBar: {
        hollow: {
          size: '50%',
        },
        track: {
          background: '#F5F6FA',
        },
        dataLabels: {
          show: true,
          name: {
            show: true,
            color: theme.palette.common.black,
            offsetY: -10,
            fontSize: '10px',
          },
          value: {
            show: true,
            fontSize: '20px',
            fontWeight: 900,
            fontFamily: 'Roboto',
            color: theme.palette.common.black,
            offsetY: 10,
          },
        },
      },
    },
  };

  const jaugeOneOptions = {
    ...jaugeOptions,
    labels: [chartSubTitle === 'CA moyen par pharmacie' ? 'CA moyen' : chartSubTitle],
    colors: [jaugeColor],
  };

  const jaugeTwoOptions = {
    ...jaugeOptions,
    labels: ['% nbr. pharma'],
    colors: [jaugeColor],
  };

  const isSingleJauge = (): boolean => {
    if (
      isOnBusiness ||
      (isOnFeedback &&
        (activeIndicatorCode === FeedbackIndicateur.CIBLE ||
          activeIndicatorCode === FeedbackIndicateur.LU ||
          activeIndicatorCode === FeedbackIndicateur.COMMENTAIRE ||
          activeIndicatorCode === FeedbackIndicateur.PARTAGE ||
          activeIndicatorCode === FeedbackIndicateur.RECOMMANDATION))
    ) {
      return true;
    }
    return false;
  };

  const showJauge = (): boolean => {
    if (
      isOnBusiness &&
      (activeIndicatorCode === BusinessIndicateur.NB_COMMANDE ||
        activeIndicatorCode === BusinessIndicateur.NB_PRODUIT ||
        activeIndicatorCode === BusinessIndicateur.REMISE ||
        activeIndicatorCode === BusinessIndicateur.TOTAL_REMISE)
    ) {
      return false;
    }
    return true;
  };

  /**
   * Set Jauge values
   */
  useEffect(() => {
    if (isSingleJauge()) {
      const val = (chartCurrentValue / chartTargetValue) * 100;
      if (val !== jaugeOneValue) {
        setState(prevState => ({ ...prevState, jaugeOneValue: val }));
      }
    } else {
      const val = (chartCurrentValue / nbPharmacieSmeyley) * 100;
      if (val !== jaugeTwoValue) {
        setState(prevState => ({ ...prevState, jaugeTwoValue: val }));
      }
    }

    // setState(prevState => ({ ...prevState, jaugeOneValue: 0, jaugeTwoValue: 0 }));
  }, [chartTargetValue, chartCurrentValue, nbPharmacieSmeyley, activeIndicatorCode]);

  const formatJaugeValue = (value: number): number => {
    if (value !== Infinity && !isNaN(value)) {
      return isInteger(value) ? value : parseFloat(value.toFixed(2));
    }
    return 0;
  };

  const Stat = () => {
    return (
      <div className={classes.statRoot}>
        <Typography className={classes.statTitle}>{statTitle}</Typography>
        <div
          className={
            showJauge()
              ? classes.chiffreContainer
              : classnames(classes.chiffreContainer, classes.noJauge)
          }
        >
          {INDICATOR_DADA.map((item, index) => {
            return (
              <div key={`indicator_data_stat_${index}`} className={classes.statItem}>
                <img src={item.iconSrc} alt={item.label} className={classes.iconInStat} />
                <Typography className={classes.statItemLabel}>{item.label}</Typography>
                <Typography className={classes.statItemValue}>
                  {Math.round(roundNumber(item.value))}
                </Typography>
              </div>
            );
          })}
        </div>

        {showJauge() && (
          <div className={classes.gaugeContainer}>
            <Typography className={classes.gaugeTitle}>Jauge de performance</Typography>
            <div className={classes.gaugeChatsContainer}>
              <Chart
                options={jaugeOneOptions as any}
                series={[formatJaugeValue(jaugeOneValue)]}
                type="radialBar"
                height="280"
                width="280"
              />
              {!isSingleJauge() && (
                <Chart
                  options={jaugeTwoOptions as any}
                  series={[formatJaugeValue(jaugeTwoValue)]}
                  type="radialBar"
                  height="280"
                  width="280"
                />
              )}
            </div>
          </div>
        )}
      </div>
    );
  };

  const loading = (): boolean => {
    if (
      (isOnBusiness && queryBusinessResult.loading) ||
      (isOnBusiness && queryBusinessTableResult.loading) ||
      (isOnFeedback && queryFeedbackResult.loading) ||
      (isOnFeedback && queryFeedbackTableResult.loading)
    ) {
      return true;
    }
    return false;
  };

  if (isPublicite) {
    return (
      <div className={classes.pilotageDashboardRoot}>
        {loading() && <Backdrop />}
        <Head />
        <Box className={classes.tableContainer}>{PilotageSuiviPublicitaire}</Box>
      </div>
    );
  }

  return (
    <div className={classes.pilotageDashboardRoot}>
      {loading() && <Backdrop />}
      <Head />
      {/* {source && <OperationResume data={source} />} */}
      <ViewTypeBtns />
      {ocSourceResult.data && ocSourceResult.data.pilotageOcSource && (
        <CatalogueProduitOcInfo operation={source} />
      )}
      {actuSourceResult.data && actuSourceResult.data.pilotageActuSource && (
        <ActualiteInfo actu={source} />
      )}
      <div className={classes.mainCntainer}>
        <IndicatorCard />
        <ChartCard />
        <Stat />
      </div>
      <div className={classes.tableContainer}>
        <Typography className={classes.tableTitle}>{tableTitle}</Typography>
        {isPublicite && <div>Publicite</div>}
        {isGlobalView && (
          <TableGlobal
            typePilotage={isOnBusiness ? BUSINESS : FEEDBACK}
            data={tableData}
            rowCount={tableData.length}
            total={tableTotal}
            skip={skip}
            take={take}
            page={page}
            rowsPerPage={rowsPerPage}
            setSkip={setSkip}
            setTake={setTake}
            setPage={setPage}
            setRowsPerPage={setRowsPerPage}
            order={order}
            setOrder={setOrder}
            orderBy={orderBy}
            setOrderBy={setOrderBy}
            codeItem={codeItem || ''}
          />
        )}
        {isPharmaView && (
          <TablePharmacie
            typePilotage={isOnBusiness ? BUSINESS : FEEDBACK}
            onClickTableRow={onClickTableRow}
            data={tableData}
            rowCount={tableData.length}
            total={tableTotal}
            skip={skip}
            take={take}
            page={page}
            rowsPerPage={rowsPerPage}
            setSkip={setSkip}
            setTake={setTake}
            setPage={setPage}
            setRowsPerPage={setRowsPerPage}
            order={order}
            setOrder={setOrder}
            orderBy={orderBy}
            setOrderBy={setOrderBy}
            codeItem={codeItem || ''}
            pharmacieIds={pharmacieIds}
            operationIds={operationIds}
            idItemAssocies={idItemAssocies}
            isOnBusiness={isOnBusiness}
            isOnFeedback={isOnFeedback}
          />
        )}
        {isOpView && (
          <TableOperation
            typePilotage={isOnBusiness ? BUSINESS : FEEDBACK}
            onClickTableRow={onClickTableRow}
            data={tableData}
            rowCount={tableData.length}
            total={tableTotal}
            skip={skip}
            take={take}
            page={page}
            rowsPerPage={rowsPerPage}
            setSkip={setSkip}
            setTake={setTake}
            setPage={setPage}
            setRowsPerPage={setRowsPerPage}
            order={order}
            setOrder={setOrder}
            orderBy={orderBy}
            setOrderBy={setOrderBy}
            codeItem={codeItem || ''}
            idsPharmacies={pharmacieIds}
          />
        )}
        {isItemView && (
          <TableItem
            typePilotage={isOnBusiness ? BUSINESS : FEEDBACK}
            onClickTableRow={onClickTableRow}
            data={tableData}
            rowCount={tableData.length}
            total={tableTotal}
            skip={skip}
            take={take}
            page={page}
            rowsPerPage={rowsPerPage}
            setSkip={setSkip}
            setTake={setTake}
            setPage={setPage}
            setRowsPerPage={setRowsPerPage}
            order={order}
            setOrder={setOrder}
            orderBy={orderBy}
            setOrderBy={setOrderBy}
            codeItem={codeItem || ''}
            idsPharmacies={pharmacieIds}
          />
        )}
      </div>
    </div>
  );
};

Dashboard.defaultProps = {
  typePilotage: TypePilotage.BUSINESS,
};

export default withRouter(Dashboard);
