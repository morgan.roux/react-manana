import { useApolloClient, useLazyQuery } from '@apollo/react-hooks';
import {
  PHARMACIE_OPERATION_STATISTIQUE_BUSINESS,
  PHARMACIE_OPERATION_STATISTIQUE_BUSINESSVariables,
} from '../../../../../../graphql/Pilotage/types/PHARMACIE_OPERATION_STATISTIQUE_BUSINESS';
import { GET_PHARMACIE_OPERATION_STATISTIQUE_BUSINESS } from '../../../../../../graphql/Pilotage';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';

const useQueryBusinessTable = () => {
  const client = useApolloClient();
  const [queryBusinessTable, queryBusinessTableResult] = useLazyQuery<
    PHARMACIE_OPERATION_STATISTIQUE_BUSINESS,
    PHARMACIE_OPERATION_STATISTIQUE_BUSINESSVariables
  >(GET_PHARMACIE_OPERATION_STATISTIQUE_BUSINESS, {
    fetchPolicy: 'network-only',
    onCompleted: data => {},
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        });
      });
    },
  });

  return { queryBusinessTable, queryBusinessTableResult };
};

export default useQueryBusinessTable;
