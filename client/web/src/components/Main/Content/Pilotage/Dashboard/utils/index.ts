import { QueryResult } from 'react-apollo';
import {
  PHARMACIE_OPERATION_STATISTIQUE_BUSINESS,
  PHARMACIE_OPERATION_STATISTIQUE_BUSINESSVariables,
} from '../../../../../../graphql/Pilotage/types/PHARMACIE_OPERATION_STATISTIQUE_BUSINESS';
import {
  PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK,
  PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACKVariables,
} from '../../../../../../graphql/Pilotage/types/PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK';
import { Dispatch, SetStateAction } from 'react';

/**
 * SET TABLE DATA
 */
const setDataTable = (
  queryBusinessTableResult: QueryResult<
    PHARMACIE_OPERATION_STATISTIQUE_BUSINESS,
    PHARMACIE_OPERATION_STATISTIQUE_BUSINESSVariables
  >,
  queryFeedbackTableResult: QueryResult<
    PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK,
    PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACKVariables
  >,
  setState: Dispatch<SetStateAction<any>>,
) => {
  const { data } = queryBusinessTableResult;
  const feedbackData = queryFeedbackTableResult.data;
  if (
    data &&
    data.pharmacieOperationStatistiqueBusiness &&
    data.pharmacieOperationStatistiqueBusiness.data &&
    data.pharmacieOperationStatistiqueBusiness.data.length > 0
  ) {
    const queryData = data.pharmacieOperationStatistiqueBusiness.data;
    const total = data.pharmacieOperationStatistiqueBusiness.total;
    setState(prevState => ({
      ...prevState,
      tableData: queryData || [],
      tableTotal: total,
    }));
  }

  if (
    feedbackData &&
    feedbackData.pharmacieItemAssocieStatistiqueFeedback &&
    feedbackData.pharmacieItemAssocieStatistiqueFeedback.data &&
    feedbackData.pharmacieItemAssocieStatistiqueFeedback.data.length > 0
  ) {
    const queryData = feedbackData.pharmacieItemAssocieStatistiqueFeedback.data;
    const total = feedbackData.pharmacieItemAssocieStatistiqueFeedback.total;
    setState(prevState => ({
      ...prevState,
      tableData: queryData || [],
      tableTotal: total,
    }));
  }
};

export { setDataTable };
