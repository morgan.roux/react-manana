import TableGlobal from './TableGlobal';
import TablePharmacie from './TablePharmacie';
import TableOperation from './TableOperation';
import TableItem from './TableItem';

export { TableGlobal, TablePharmacie, TableOperation, TableItem };
