import { Tooltip } from '@material-ui/core';
import { CheckCircle, BlockTwoTone } from '@material-ui/icons';
import moment from 'moment';
import React from 'react';
const ActuColumns = [
  {
    name: 'libelle',
    label: 'LIBELLE',
    renderer: (value: any) => {
      return value && value.description ? (
        <Tooltip title={value.description} aria-label="add">
          <div>{value.libelle}</div>
        </Tooltip>
      ) : (
        <div>{value.libelle}</div>
      );
    },
  },
];

export default ActuColumns;
