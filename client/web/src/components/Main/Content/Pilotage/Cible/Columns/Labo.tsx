import { Tooltip } from '@material-ui/core';
import { CheckCircle, BlockTwoTone } from '@material-ui/icons';
import moment from 'moment';
import React from 'react';
const LaboColumns = [
  {
    name: 'nom',
    label: 'NOM',
  },
  {
    name: 'code',
    label: 'CODE',
  },
];

export default LaboColumns;
