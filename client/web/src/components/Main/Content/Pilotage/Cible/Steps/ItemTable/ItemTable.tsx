import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { Box } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { GET_CURRENT_PHARMACIE } from '../../../../../../../graphql/Pharmacie/local';
import ICurrentPharmacieInterface from '../../../../../../../Interface/CurrentPharmacieInterface';
import { getGroupement } from '../../../../../../../services/LocalStorage';
import {
  ActualiteCibleTable,
  CommandeCibleTable,
  CreateOperationProduit,
  LaboratoireCibleTable,
  OperationCibleTable,
} from '../../../../../../Common/newWithSearch/ComponentInitializer';
import { useStyles } from './styles';
import Validator from './Validator';

interface ItemTableProps {
  match: {
    params: { id: string };
  };
  setAllValues: (values: any) => void;
  allValues: any;
  setNextBtnDisabled?: (disabled: boolean) => void;
  columns: any;
  isDeletable?: boolean;
}
const ItemTable: FC<ItemTableProps & RouteComponentProps<any, any, any>> = ({
  match,
  history,
  allValues,
  setAllValues,
  setNextBtnDisabled,
  columns,
  isDeletable,
}) => {
  const classes = useStyles({});
  const pathName = history.location.pathname;
  const [checkedItems, setCheckedItems] = useState<any[]>([]);
  const [query, setQuery] = useState();
  const client = useApolloClient();
  const groupement = getGroupement();
  const idGroupement = groupement.id;
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const currentPharmacie = myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie;
  const filterByGroupement = [{ term: { idGroupement } }];

  const getItem = (item: any) => {
    if (item && item.codeItem) {
      let component: any = null;
      let type = '';
      switch (item.codeItem.startsWith('A')) {
        case true:
          // filterBy = [{ prefix: { 'item.codeItem': item.codeItem } }];
          client.writeData({
            data: {
              codeItemPrefix: item.codeItem,
            },
          });
          component = OperationCibleTable;
          type = 'operation';
          break;
      }
      switch (item.codeItem.startsWith('C')) {
        case true:
          component = LaboratoireCibleTable;
          type = 'laboratoire';
          break;
      }
      switch (item.codeItem.startsWith('B')) {
        case true:
          component = ActualiteCibleTable;
          type = 'actualite';
          break;
      }
      switch (item.codeItem.startsWith('K')) {
        case true:
          component = CreateOperationProduit;
          type = 'produitcanal';
          break;
      }
      switch (item.codeItem.startsWith('J')) {
        case true:
          component = CommandeCibleTable;
          type = 'commande';
          break;
      }
      switch (item.codeItem.startsWith('L')) {
        case true:
          component = '';
          type = 'project';
          break;
      }
      return { component, type };
    }
  };

  const item = (allValues && allValues.item && getItem(allValues.item)) || {
    component: OperationCibleTable,
    type: 'operation',
  };

  useEffect(() => {
    if (pathName.startsWith('/pilotage/business') && pathName !== '/business') {
      history.push('/pilotage/business/operation');
    }
    if (pathName.startsWith('/pilotage/feedback') && item && item.type) {
      history.push(`/pilotage/feedback/${item.type}`);
    }
    if (allValues) {
      setCheckedItems(allValues.items || []);
      if (setNextBtnDisabled) {
        setNextBtnDisabled(Validator(allValues));
      }
    }
  }, [allValues]);

  return <Box>{item.component}</Box> || null;
};

export default withRouter(ItemTable);
