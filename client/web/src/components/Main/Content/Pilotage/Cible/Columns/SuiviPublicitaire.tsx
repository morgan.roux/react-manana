import moment from 'moment';

const SuiviPublicitaireColumns = [
  {
    name: 'publicite.libelle',
    label: 'Nom',
    renderer: (value: any) => {
      return (value && value.publicite && value.publicite.libelle) || '-';
    },
  },
  {
    name: 'publicite.dateDebut',
    label: 'Date début',
    renderer: (value: any) => {
      return (
        (value &&
          value.publicite &&
          value.publicite.dateDebut &&
          moment(value.publicite.dateDebut).format('DD/MM/YYYY')) ||
        '-'
      );
    },
  },
  {
    name: 'publicite.dateFin',
    label: 'Date fin',
    renderer: (value: any) => {
      return (
        (value &&
          value.publicite &&
          value.publicite.dateFin &&
          moment(value.publicite.dateFin).format('DD/MM/YYYY')) ||
        '-'
      );
    },
  },
  {
    name: 'nbView',
    label: 'Affichage(s)',
    centered: true,
    renderer: (value: any) => {
      return (value && value.nbView) || '0';
    },
  },
  {
    name: 'nbClick',
    label: 'Click(s)',
    centered: true,
    renderer: (value: any) => {
      return (value && value.nbClick) || '0';
    },
  },
  {
    name: 'nbAccesOp',
    label: 'Accès aux OP',
    centered: true,
    renderer: (value: any) => {
      return (value && value.nbAccesOp) || '0';
    },
  },
  {
    name: 'nbCommande',
    centered: true,
    label: 'Conversions en Commandes',
    renderer: (value: any) => {
      return (value && value.nbCommande) || '0';
    },
  },
];

export default SuiviPublicitaireColumns;
