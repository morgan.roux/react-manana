import { Tooltip } from '@material-ui/core';
import { CheckCircle, BlockTwoTone } from '@material-ui/icons';
import moment from 'moment';
import React from 'react';
const OcColumns = [
  {
    name: 'libelle',
    label: 'Libellé',
    renderer: (value: any) => {
      return value && value.description ? (
        <Tooltip title={value.description} aria-label="add">
          <div>{value.libelle}</div>
        </Tooltip>
      ) : (
        <div>{value.libelle}</div>
      );
    },
  },
  {
    name: 'commandeType.libelle',
    label: 'Type de commande',
    renderer: (value: any) => {
      return (
        <>
          {value && value.commandeType && value.commandeType.libelle
            ? value.commandeType.libelle
            : '-'}
        </>
      );
    },
  },
  {
    name: 'commandeCanal.libelle',
    label: 'Canal de commande',
    renderer: (value: any) => {
      return (
        <>
          {value && value.commandeCanal && value.commandeCanal.libelle
            ? value.commandeCanal.libelle
            : '-'}
        </>
      );
    },
  },
  {
    name: 'laboratoire.nom',
    label: 'Labo',
    renderer: (value: any) => {
      return (
        <>{value && value.laboratoire && value.laboratoire.nom ? value.laboratoire.nom : '-'}</>
      );
    },
  },
  {
    name: 'dateDebut',
    label: 'Date début',
    renderer: (value: any) => {
      return <>{moment(value.dateDebut).format('L')}</>;
    },
  },
  {
    name: 'dateDebut',
    label: 'Date fin',
    renderer: (value: any) => {
      return <>{moment(value.dateFin).format('L')}</>;
    },
  },
  {
    name: 'CATotal',
    label: 'CA global',
  },
  {
    name: 'CAMoyenParPharmacie',
    label: 'CA moyenne',
  },
];

export default OcColumns;
