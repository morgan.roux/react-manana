const ProduitColumns = [
  {
    name: 'produitCode.code',
    label: 'Code',
    renderer: (value: any) => {
      return value?.produit?.produitCode?.code || '-';
    },
  },
  {
    name: 'libelle',
    label: 'Libellé',
    renderer: (value: any) => {
      return value?.produit?.libelle || '-';
    },
  },
  {
    name: 'produitTechReg.laboExploitant.nomLabo',
    label: 'Labo',
    renderer: (value: any) => {
      return value?.produit?.produitTechReg?.laboExploitant?.nomLabo || '-';
    },
  },
  {
    name: 'stv',
    label: 'STV',
    editable: true,
    renderer: (value: any) => {
      return value?.stv ? `${value.stv}` : '-';
    },
  },
  {
    name: 'prixPhv',
    label: 'Prix Achat',
    editable: true,
    renderer: (value: any) => {
      return value?.prixPhv ? `${value.prixPhv} €` : '-';
    },
  },
  {
    name: 'unitePetitCond',
    label: 'Nbr/Carton',
    editable: true,
    renderer: (value: any) => {
      return value?.unitePetitCond || '-';
    },
  },
];

export default ProduitColumns;
