import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
    },
    fieldsetLegend: {
      marginLeft: 5,
    },
    fieldsetLabel: {
      color: '#616161',
      fontFamily: 'Roboto',
      fontSize: 12,
    },
    fieldset: {
      width: '100%',
      padding: '0px 24px',
      '& > fieldset': {
        border: '1px solid rgba(0, 0, 0, 0.23)',
        borderRadius: 5,
        padding: '11px 26px 20px 18px',
      },
      '& .MuiCheckbox-root, & .MuiCheckbox-colorSecondary.Mui-checked': {
        color: theme.palette.primary.main,
        padding: '0px 9px',
      },
      '& .MuiInputBase-root.Mui-disabled': {
        color: '#ccc',
        '&:hover fieldset': {
          border: '1px solid rgba(0, 0, 0, 0.38)',
          // borderColor: 'rgba(0, 0, 0, 0.38)',
        },
        '& .Mui-focused fieldset': {
          border: '1px solid rgba(0, 0, 0, 0.38)',
          // borderColor: 'rgba(0, 0, 0, 0.38)',
        },
      },
    },
  }),
);
