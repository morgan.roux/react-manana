import React, { FC, useState, useEffect } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import Validator from './Validator';
import CustomTree from '../../../../../../Common/CustomTree';
import { FormLabel } from '@material-ui/core';

interface ItemSelectionProps {
  match: {
    params: { id: string };
  };
  setAllValues: (values: any) => void;
  allValues: any;
  setNextBtnDisabled: (disabled: boolean) => void;
  itemLabel?: string;
  type?: string;
  nodeKey?: string;
  nodeLabel?: string;
  nodeParent?: string;
  nodeIndex?: string;
  multiple?: boolean;
  allValuesKey?: string;
  itemKey?: string;
  disabled?: boolean;
}
const ItemSelection: FC<ItemSelectionProps & RouteComponentProps<any, any, any>> = ({
  match,
  history,
  allValues,
  setAllValues,
  setNextBtnDisabled,
  itemLabel,
  type,
  nodeKey,
  nodeLabel,
  nodeParent,
  nodeIndex,
  multiple,
  allValuesKey,
  itemKey,
  disabled,
}) => {
  const classes = useStyles({});
  const pathName = history.location.pathname;
  const [checkedItem, setCheckedItem] = useState<any>(
    (allValues && itemKey && allValues[itemKey]) || [],
  );
  useEffect(() => {
    if (pathName.includes('/pilotage/business')) {
      history.push('/pilotage/business/operation');
    }
    if (pathName.includes('/pilotage/feedback')) {
      history.push('/pilotage/feedback');
    }
    if (allValues) {
      setNextBtnDisabled(Validator(allValues));
    }
    if (allValues && allValues.item) {
      setCheckedItem(allValues && allValues.item);
    }
    if (allValues && itemKey && allValues[itemKey]) {
      setCheckedItem(allValues[itemKey]);
    }
  }, [allValues]);

  const handleCheckedItems = (item: any) => {
    if (allValuesKey) {
      setAllValues(prevState => ({
        ...prevState,
        [allValuesKey]: item ? item[nodeKey || 'codeItem'] : '',
      }));
      setCheckedItem(item);
    } else {
      setAllValues(prevState => ({
        ...prevState,
        item,
        items: [],
      }));
      setCheckedItem(item);
    }
    if (itemKey) {
      setAllValues(prevState => ({
        ...prevState,
        [itemKey]: item,
      }));
      setCheckedItem(item);
    }
  };

  return (
    <div className={classes.root}>
      <div className={classes.fieldset}>
        <fieldset>
          <legend className={classes.fieldsetLegend}>
            <FormLabel className={classes.fieldsetLabel}>
              {itemLabel || "Choix de l'item"}
            </FormLabel>
          </legend>
          <CustomTree
            type={type || 'item'}
            nodeKey={nodeKey || 'codeItem'}
            nodeLabel={nodeLabel || 'name'}
            nodeParent={nodeParent || 'parent'}
            nodeIndex={nodeIndex || ''}
            multiple={multiple || false}
            setChecked={handleCheckedItems}
            checked={checkedItem}
            disabled={disabled}
          />
        </fieldset>
      </div>
    </div>
  );
};

export default withRouter(ItemSelection);
