import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { CircularProgress, Typography } from '@material-ui/core';
import { ArrowForward } from '@material-ui/icons';
import { Pagination } from '@material-ui/lab';
import { differenceBy } from 'lodash';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { CAHIER_LIAISON_URL } from '../../../../../Constant/url';
import { ME_me } from '../../../../../graphql/Authentication/types/ME';
import { DO_SEARCH_INFORMATION_LIAISON } from '../../../../../graphql/InformationLiaison';
import {
  SEARCH_INFORMATION_LIAISON,
  SEARCH_INFORMATION_LIAISONVariables,
} from '../../../../../graphql/InformationLiaison/types/SEARCH_INFORMATION_LIAISON';
import { nl2br } from '../../../../../services/Html';
import { getGroupement, getUser } from '../../../../../services/LocalStorage';
import { trimmedString } from '../../../../../utils/Helpers';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import CustomButton from '../../../../Common/CustomButton';
import { CustomModal } from '../../../../Common/CustomModal';
import useStyles from '../PopupActualite/styles';

export interface PopupInfoLiaisonProps {
  open: boolean;
  setOpen: (open: boolean) => void;
  handleCloseActuPopup?: () => void;
}

const PopupInfoLiaison: FC<PopupInfoLiaisonProps & RouteComponentProps> = ({
  open,
  setOpen,
  history: { push },
  handleCloseActuPopup,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const groupement = getGroupement();
  const user: ME_me = getUser();
  const userId = (user && user.id) || '';

  const infoFromStorage = localStorage.getItem('infosBloquantes');

  let infosBloquantes: any[] = [];
  if (infoFromStorage) {
    infosBloquantes = JSON.parse(infoFromStorage);
  }

  const [info, setInfo] = React.useState<any>(null);
  const [filteredData, setFilteredData] = React.useState<any[]>([]);
  const [page, setPage] = React.useState<number>(0);

  const { data, loading } = useQuery<
    SEARCH_INFORMATION_LIAISON,
    SEARCH_INFORMATION_LIAISONVariables
  >(DO_SEARCH_INFORMATION_LIAISON, {
    variables: {
      type: ['informationliaison'],
      query: {
        query: {
          bool: {
            must: [
              { term: { isRemoved: false } },
              { term: { 'groupement.id': groupement.id } },
              { term: { bloquant: true } },
              //{ term: { priority: 3 } }, // Bloquant
              //  { terms: { 'colleguesConcernees.userConcernee.id': [userId] } },

              {
                terms: { 'colleguesConcernees.coupleStatutUserConcernee': [`${userId}-NON_LUES`] },
              },
              // { terms: { 'colleguesConcernees.lu': [false] } },
              {
                script: {
                  script: {
                    source: "doc['colleguesConcernees.userConcernee.id'].length >= 1",
                    lang: 'painless',
                  },
                },
              },
            ],
          },
        },
      },
      sortBy: [{ dateCreation: { order: 'desc' } }],
    },
    fetchPolicy: 'cache-and-network',
    onError: error => {
      console.log('error :>> ', error);
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  // const total = (data && data.search && data.search.total) || 0;

  let realData: any[] = [];
  if (data && data.search && data.search.data && data.search.data.length > 0) {
    realData = data.search.data.filter(
      (d: any) =>
        d.colleguesConcernees &&
        d.colleguesConcernees.some((cc: any) => cc && cc.userConcernee.id === userId),
    );
  }

  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value - 1);
  };

  const goToInfo = () => {
    const infos = (data && data.search && data.search.data) || [];
    localStorage.setItem('infosBloquantes', JSON.stringify(infos));
    push(`${CAHIER_LIAISON_URL}/recue/${info && info.id}`);
  };

  React.useMemo(() => {
    console.log('******************************USERMEMO', data, '*******REAL', realData);

    if (data?.search?.data?.length && realData.length > 0) {
      if (infosBloquantes && infosBloquantes.length > 0) {
        const diff = differenceBy(realData, infosBloquantes, 'id');
        if (diff && diff.length > 0 && diff[page]) {
          setFilteredData(diff);
          setInfo(diff[page]);
        }
      } else if (realData[page]) {
        setFilteredData(realData);
        setInfo(realData[page]);
      }
    }
  }, [data, page]);

  React.useMemo(() => {
    if (data && data.search && data.search.data && data.search.data.length > 0) {
      // if (infosBloquantes && infosBloquantes.length > 0) {
      //   const diff = differenceBy(data.search.data, infosBloquantes, 'id');
      //   if (diff && diff.length > 0) {
      //     setOpen(true);
      //   }
      // } else {
      //   setOpen(true);
      // }

      if (realData && realData.length > 0) {
        setOpen(true);
      }
    }
  }, [data]);

  console.log('***********************************INFO', open, info);
  return (
    <CustomModal
      open={open && info}
      setOpen={setOpen}
      title={`Vous avez ${filteredData.length} information(s) bloquante(s)`}
      closeIcon={true}
      withBtnsActions={false}
      headerWithBgColor={true}
      fullWidth={true}
      maxWidth="md"
      customHandleClose={handleCloseActuPopup}
    >
      <div className={classes.popupActuRoot}>
        <div className={classes.actuInfoContainer}>
          <Typography className={classes.origine}>
            Origine : <span>{(info && info.origine) || '-'}</span>
            {loading && <CircularProgress size={20} />}
          </Typography>
          <Typography
            className={classes.description}
            dangerouslySetInnerHTML={
              { __html: nl2br(trimmedString((info && info.description) || '', 100)) } as any
            }
          />
          <CustomButton
            className={classes.btn}
            color="secondary"
            variant="outlined"
            endIcon={<ArrowForward />}
            onClick={goToInfo}
          >
            Consulter
          </CustomButton>
        </div>
        <Pagination
          count={filteredData.length}
          variant="outlined"
          color="primary"
          onChange={handleChange}
          showFirstButton={true}
          showLastButton={true}
        />
      </div>
    </CustomModal>
  );
};

export default withRouter(PopupInfoLiaison);
