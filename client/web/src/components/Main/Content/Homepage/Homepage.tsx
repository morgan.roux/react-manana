import React, { FC, useState, useEffect, useMemo } from 'react';
import useStyles, { SERVICE_CARD_WIDTH } from './styles';
import Footer from '../Footer/Footer';
import {
  Typography,
  CardContent,
  Card,
  Box,
  Hidden,
  Fab,
  Grow,
  CardActions,
  Badge,
} from '@material-ui/core';
import { SERVICES_LIST } from '../../../../Constant/services';
import { Settings } from 'react-slick';
import faker from 'faker';
import { CustomSlider } from '../../../Common/CustomSlider';
import { CustomSliderPub } from '../../../Common/CustomSlider/CustomSliderPub';
import { SliderData } from '../../../Common/CustomSlider/CustomSlider';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { getGroupement, getUser } from '../../../../services/LocalStorage';
import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import { SEARCH, SEARCHVariables } from '../../../../graphql/search/types/SEARCH';
import { SEARCH as SEARCH_QUERY } from '../../../../graphql/search/query';
import { Loader } from '../../../Dashboard/Content/Loader';
// import ImgSlider2 from '../../../../assets/img/photo3.jpg';
import {
  CREATE_SUIVI_PUBLICITAIREVariables,
  CREATE_SUIVI_PUBLICITAIRE,
} from '../../../../graphql/Publicite/types/CREATE_SUIVI_PUBLICITAIRE';
import { DO_CREATE_SUIVI_PUBLICITAIRE } from '../../../../graphql/Publicite';
import { DO_SEARCH_ACTUALITES } from '../../../../graphql/Actualite';
import {
  SEARCH_ACTUALITES,
  SEARCH_ACTUALITESVariables,
} from '../../../../graphql/Actualite/types/SEARCH_ACTUALITES';
import { PopupActualite } from './PopupActualite';
import { ME_me } from '../../../../graphql/Authentication/types/ME';
import { actualiteFilter } from '../../../../services/actualiteFilter';
import { SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT } from '../../../../Constant/roles';
import {
  DO_INCREMENT_PUBLICITE_CLICK_COUNT,
  DO_INCREMENT_PUBLICITE_OPERATION_CLICK_COUNT,
} from '../../../../graphql/SuiviPublicitaire/mutation';
import {
  incrementPubliciteClickCountVariables,
  incrementPubliciteClickCount,
} from '../../../../graphql/SuiviPublicitaire/types/incrementPubliciteClickCount';
import {
  incrementPubliciteOperationClickCount,
  incrementPubliciteOperationClickCountVariables,
} from '../../../../graphql/SuiviPublicitaire/types/incrementPubliciteOperationClickCount';
import moment from 'moment';
import DailyAlert from '../TodoNew/DailyAlert';
import PopupInfoLiaison from './PopupInfoLiaison';
import RgpdPopup from '../RgpdPopup';
import { DO_CHECK_ACCEPT_RGPD } from '../../../../graphql/Rgpd';
import {
  CHECK_ACCEPT_RGPD,
  CHECK_ACCEPT_RGPDVariables,
} from '../../../../graphql/Rgpd/types/CHECK_ACCEPT_RGPD';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import { useValueParameterByView } from '../../../../utils/getValueParameter';
import { GET_SEARCH_ACTION } from '../../../../graphql/Action';
import { getQueryParameter } from '../TodoNew/FilterSearch';
import { isMobile } from '../../../../utils/Helpers';
import { ContactPhone, Dashboard, PhoneCallback } from '@material-ui/icons';
import useStats from './useStats/useStats';
import { AppAuthorization } from '../../../../services/authorization';

// 0102
export const Homepage: FC<RouteComponentProps<any, any, any>> = ({ history, location }) => {
  const userStats = useStats();
  const [enableServices, setEnableServices] = useState<any[]>([]);
  const enablePublicite = useValueParameterByView('0200', '0700', isMobile());
  const enablePromotionEncours = useValueParameterByView('0201', '0701', isMobile());
  const user: ME_me = getUser();
  const auth = new AppAuthorization(user);

  const enableTodo = useValueParameterByView('0207', '0702', isMobile());
  const enablePartageIdees = useValueParameterByView('0206', '0703', isMobile());
  const enableActualites = useValueParameterByView('0301', '0704', isMobile());
  const enableOperations = useValueParameterByView('0302', '0705', isMobile());
  const enableCataloguesProduits = useValueParameterByView('0303', '0706', isMobile());
  const enableSuiviCommandes = useValueParameterByView('0304', '0707', isMobile());
  const enableLaboratoire = useValueParameterByView('0305', '0708', isMobile());
  const enablePartenairesServices = useValueParameterByView('0306', '0709', isMobile());
  const enableServiceAuxPharmacies = useValueParameterByView('0307', '0710', isMobile());
  const enableEvenements = useValueParameterByView('0308', '0711', isMobile());
  const enableFormations = useValueParameterByView('0309', '0712', isMobile());
  const enableNewsLetter = useValueParameterByView('0310', '0713', isMobile());
  const enableAchatGroupe = useValueParameterByView('0311', '0714', isMobile());
  const enableCahierLiaison = useValueParameterByView('0312', '0715', isMobile());
  const enableDemarcheQualite = useValueParameterByView('0313', '0716', isMobile());
  const enableGed = useValueParameterByView('0314', '0717', isMobile());
  const enableFeedBack = useValueParameterByView('0315', '0718', isMobile());
  const enableGroupePharmacies = useValueParameterByView('0316', '0719', isMobile());
  const enableMessagerie = useValueParameterByView('0317', '0720', isMobile());
  const enableReleveTemperature = useValueParameterByView('0825', '0724', isMobile());
  const enableDashboard = useValueParameterByView('0210', '0745', isMobile());
  const enableShowInBootDashboad = useValueParameterByView('0378', '0779', isMobile());
  const onlyGedEnabled = auth.onlyGedIsAuthorized();

  const client = useApolloClient();

  const [displayDashBoardFab, setDisplayDashBoardFab] = useState<boolean>(false);
  React.useEffect(() => {
    console.log('==================', enableDashboard);
    if (location.pathname === '/' && enableDashboard) {
      setDisplayDashBoardFab(true);
    } else {
      setDisplayDashBoardFab(false);
    }
  }, [location.pathname, enableDashboard]);

  React.useEffect(() => {}, [enableShowInBootDashboad]);

  const groupement = getGroupement();
  const dateNow = moment();

  const [openPopupInfo, setOpenPopupInfo] = useState(false);

  const isLocalDate: boolean = localStorage.getItem('lastDate') ? true : false;

  const lastDate = localStorage.getItem('lastDate');
  const dateIsOver: boolean = dateNow.diff(lastDate, 'days') >= 1;

  const [openRgpd, setOpenRgpd] = useState<boolean>(false);
  const [openDailyAlert, setOpenDailyAlert] = useState<boolean>(false);
  const isAdmin: boolean =
    user &&
    user.role &&
    (user.role.code === SUPER_ADMINISTRATEUR || user.role.code === ADMINISTRATEUR_GROUPEMENT)
      ? true
      : false;

  const idPharmacieUser =
    (user &&
      user.userPpersonnel &&
      user.userPpersonnel.pharmacie &&
      user.userPpersonnel.pharmacie.id) ||
    (user &&
      user.userTitulaire &&
      user.userTitulaire.titulaire &&
      user.userTitulaire.titulaire.pharmacieUser &&
      user.userTitulaire.titulaire.pharmacieUser.id);

  const actuFilters = actualiteFilter(user, groupement && groupement.id);

  const publicitySliderSettings: Settings = {
    arrows: true,
    autoplay: true,
    dots: false,
    speed: 500,
  };

  const promotionSliderSettings: Settings = {
    arrows: true,
    slidesToShow: 3,
    slidesToScroll: 3,
  };

  const [sliderDatas, setsliderDatas] = useState<SliderData[]>([]);
  const [filteredActu, setFilteredActu] = useState<any[]>([]);
  const [actuIndex, setActuIndex] = useState<number>(0);
  const [openActuPopup, setOpenActuPopup] = useState<boolean>(false);
  const [popupData, setPopupData] = useState<any>(null);

  const sliderData: SliderData[] = [
    { id: 1, image: faker.image.avatar(), url: faker.image.avatar() },
    { id: 2, image: faker.image.avatar(), url: faker.image.avatar() },
    { id: 3, image: faker.image.avatar(), url: faker.image.avatar() },
    { id: 4, image: faker.image.avatar(), url: faker.image.avatar() },
  ];

  /**
   *
   * Get pubs
   */
  const [queryState, setqueryState] = useState<any>({
    query: {
      query: '',
    },
    sortBy: [{ ordre: { order: 'asc' } }],
    filterBy: [
      { term: { isRemoved: false } },
      { term: { 'groupement.id': groupement.id } },
      { range: { dateFin: { gte: moment().format() } } },
    ],
  });

  /**
   * Query publicite
   */
  const { query, sortBy, filterBy } = queryState;
  const queryVariables = {
    type: ['publicite'],
    query,
    sortBy,
    filterBy,
  };
  const { loading, data } = useQuery<SEARCH, SEARCHVariables>(SEARCH_QUERY, {
    variables: queryVariables,
    fetchPolicy: 'cache-and-network',
  });

  const [doCreateSuiviPublicitaire] = useMutation<
    CREATE_SUIVI_PUBLICITAIRE,
    CREATE_SUIVI_PUBLICITAIREVariables
  >(DO_CREATE_SUIVI_PUBLICITAIRE);

  /**
   * Query actualite blocquant
   */
  const { data: dataActu, loading: loadingActu } = useQuery<
    SEARCH_ACTUALITES,
    SEARCH_ACTUALITESVariables
  >(DO_SEARCH_ACTUALITES, {
    variables: {
      type: ['actualite'],
      query: { query: { bool: { ...actuFilters } } },
      filterBy: [
        { term: { isRemoved: false } },
        { term: { idGroupement: groupement.id } },
        { term: { niveauPriorite: 3 } },
      ],
      idPharmacieUser: idPharmacieUser ? idPharmacieUser : null,
      userId: user && user.id,
    },
    fetchPolicy: 'cache-and-network',
  });

  /**
   * Query todo daily
   */

  const isAssignedTerms = (user && user.id) || '';
  const result = useQuery<any>(GET_SEARCH_ACTION, {
    variables: {
      filterBy: [{ term: { isRemoved: false } }],
      query: getQueryParameter(isAssignedTerms),
    },
  });

  const getItem = (item: any) => {
    let type = '';
    switch (item.codeItem.startsWith('A')) {
      case true:
        type = 'operation';
        // history.push('/pilotage/feedback/operation');
        break;
    }
    switch (item.codeItem.startsWith('C')) {
      case true:
        type = 'laboratoire';
        break;
    }
    switch (item.codeItem.startsWith('B')) {
      case true:
        type = 'actualite';
        break;
    }
    switch (item.codeItem.startsWith('K')) {
      case true:
        type = 'produitcanal';
        break;
    }
    switch (item.codeItem.startsWith('L')) {
      case true:
        type = 'project';
        break;
    }
    return type;
  };

  const [doIncrementPubliciteClickCount, doIncrementPubliciteClickCountResult] = useMutation<
    incrementPubliciteClickCount,
    incrementPubliciteClickCountVariables
  >(DO_INCREMENT_PUBLICITE_CLICK_COUNT);

  const [
    doIncrementPubliciteOperationClickCount,
    doIncrementPubliciteOperationClickCountResult,
  ] = useMutation<
    incrementPubliciteOperationClickCount,
    incrementPubliciteOperationClickCountVariables
  >(DO_INCREMENT_PUBLICITE_OPERATION_CLICK_COUNT);

  const { data: checkRgpdData, loading: checkRgpdLoading } = useQuery<
    CHECK_ACCEPT_RGPD,
    CHECK_ACCEPT_RGPDVariables
  >(DO_CHECK_ACCEPT_RGPD, {
    fetchPolicy: 'cache-and-network',
    variables: { idUser: (user && user.id) || '' },
    onError: error => {
      console.log('error :>> ', error);
      error.graphQLErrors.map(err =>
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message }),
      );
    },
  });

  /**
   * Check if the user has accepted RGPD
   */
  React.useMemo(() => {
    console.log('*******************************MEMO', checkRgpdData);

    if (checkRgpdData && !checkRgpdData.checkAcceptRgpd && !openRgpd) {
      setOpenRgpd(!checkRgpdData.checkAcceptRgpd);
    } else {
      setOpenPopupInfo(true);
    }
  }, [checkRgpdData]);

  const onClickImage = (url: string, imageItem: any) => {
    doIncrementPubliciteClickCount({ variables: { idPublicite: imageItem.id } });
    // if (
    //   imageItem &&
    //   imageItem.item &&
    //   imageItem.item.codeItem &&
    //   imageItem.item.codeItem.startsWith('A')
    // ) {
    //   doIncrementPubliciteOperationClickCount({ variables: { idPublicite: imageItem.id } });
    // }
    if (url) {
      window.open(url.startsWith('http') ? url : `http://${url}`, '_blank');
    } else {
      const element: any =
        data &&
        data.search &&
        data.search.data &&
        data.search.data.find((item: any) => item && item.id === imageItem.id);

      if (element && element.item) {
        switch (element.item.codeItem.startsWith('A')) {
          case true:
            history.push(`/operations-commerciales/${element && element.idItemSource}`, {
              idPublicite: imageItem.id,
            });
            break;
        }
        switch (element.item.codeItem.startsWith('C')) {
          case true:
            history.push(`/laboratoires/${element && element.idItemSource}/actualites`);
            break;
        }
        switch (element.item.codeItem.startsWith('B')) {
          case true:
            history.push(`/actualite/${element && element.idItemSource}`);
            break;
        }
        switch (element.item.codeItem.startsWith('K')) {
          case true:
            history.push(`/catalogue-produits/card/${element && element.idItemSource}`);
            break;
        }
        switch (element.item.codeItem.startsWith('L')) {
          case true:
            history.push(`/project/${element && element.idItemSource}`);
            break;
        }
      }
    }

    //
    /**
     * Create suivi publicitaire
     */
    // doCreateSuiviPublicitaire({ variables: { idPublicite: id } });
  };

  const handleClickService = (url: string) => {
    history.push(url);
  };

  useEffect(() => {
    let serviceCodes: string[] = [];
    if (enableTodo) {
      serviceCodes.push('0207');
    }
    if (enablePartageIdees) {
      serviceCodes.push('0206');
    }
    if (enableActualites) {
      serviceCodes.push('0301');
    }
    if (enableOperations) {
      serviceCodes.push('0302');
    }
    if (enableCataloguesProduits) {
      serviceCodes.push('0303');
    }

    if (enableSuiviCommandes) {
      serviceCodes.push('0304');
    }
    if (enableLaboratoire) {
      serviceCodes.push('0305');
    }
    if (enablePartenairesServices) {
      serviceCodes.push('0306');
    }
    if (enableServiceAuxPharmacies) {
      serviceCodes.push('0307');
    }
    if (enableMessagerie) {
      serviceCodes.push('0317');
    }
    if (enableEvenements) {
      serviceCodes.push('0308');
    }
    if (enableFormations) {
      serviceCodes.push('0309');
    }

    if (enableNewsLetter) {
      serviceCodes.push('0310');
    }
    if (enableAchatGroupe) {
      serviceCodes.push('0311');
    }
    if (enableCahierLiaison) {
      serviceCodes.push('0312');
    }
    if (enableDemarcheQualite) {
      serviceCodes.push('0313');
    }
    if (enableGed) {
      serviceCodes.push('0314');
    }
    if (enableFeedBack) {
      serviceCodes.push('0315');
    }
    if (enableGroupePharmacies) {
      serviceCodes.push('0316');
    }
    if (enableReleveTemperature) {
      serviceCodes.push('0825');
    }
    if (onlyGedEnabled) {
      serviceCodes = ['0314'];
    }

    setEnableServices(
      SERVICES_LIST.filter(({ activationParameters }) =>
        serviceCodes.includes(activationParameters),
      ),
    );
  }, [
    enableTodo,
    enablePartageIdees,
    enableActualites,
    enableOperations,
    enableCataloguesProduits,
    enableSuiviCommandes,
    enableLaboratoire,
    enablePartenairesServices,
    enableServiceAuxPharmacies,
    enableEvenements,
    enableFormations,
    enableNewsLetter,
    enableAchatGroupe,
    enableCahierLiaison,
    enableDemarcheQualite,
    enableGed,
    enableFeedBack,
    enableGroupePharmacies,
    enableMessagerie,
    enableReleveTemperature,
    onlyGedEnabled,
  ]);

  /**
   * Set sliderDatas
   */
  useEffect(() => {
    if (data && data.search && data.search.data) {
      const newData = data.search.data.map((item: any) => {
        if (item) {
          const newItme: SliderData = {
            id: item.id,
            image: (item.image && item.image.publicUrl) || '',
            url: item.url as any,
            item: item.item,
          };
          return newItme;
        }
      });
      setsliderDatas(newData as any);
    }
  }, [data]);

  /**
   * Show popup pub and filter actu by seen
   */
  useEffect(() => {
    if (dataActu && dataActu.search && dataActu.search.data && dataActu.search.data.length > 0) {
      const d: any[] = dataActu.search.data.filter((i: any) => i.seen === false);
      if (d && d.length > 0) {
        // setOpen(true);
        setFilteredActu(d);
      }
    }
  }, [dataActu]);

  /**
   * Set popup data
   */
  useMemo(() => {
    if (filteredActu && filteredActu.length > 0) {
      if (filteredActu[actuIndex]) setPopupData(filteredActu[actuIndex]);
    }
  }, [filteredActu, actuIndex]);

  const classes = useStyles({});

  const computeContainerWidth = services => {
    const computedWidth = (SERVICE_CARD_WIDTH + 30) * Math.floor(services.length / 2);
    return computedWidth > window.outerWidth || computedWidth === 0 ? undefined : computedWidth;
  };

  const NoPublicities = () => {
    return (
      <Box className={classes.noPubContainer}>
        <Typography className={classes.noPubTitle}>Espace publicitaire</Typography>
        <Typography className={classes.noPubTexte}>
          Des publicités seront affichées dans cette zone. Restez informé des nouveautés avec
          Digital4win.
        </Typography>
      </Box>
    );
  };

  if ((loading && !data && sliderDatas.length === 0) || checkRgpdLoading) return <Loader />;

  const handleCloseDailyAlert = () => {
    localStorage.setItem('lastDate', dateNow.format('dddd Do MMMM YYYY'));
    setOpenDailyAlert(false);
  };
  const handleCloseActualite = () => {
    setOpenActuPopup(false);
    if (!isLocalDate || dateIsOver) {
      setOpenDailyAlert(true);
    }
  };
  const handleCloseActuPopup = () => {
    setOpenPopupInfo(false);
    if (!isLocalDate || dateIsOver) {
      setOpenDailyAlert(true);
    }
  };

  const handleMenuClick = (value: string) => {
    if (value === 'appleRecu') {
      history.push('/db/aide-support/suivi-appel');
    } else {
    }
  };

  const handleDashboardClick = () => {
    history.push('/dashboard');
  };

  console.log('***********************************STATE', !openRgpd, !openActuPopup, openPopupInfo);
  return (
    <Box className={classes.homepageRoot}>
      <RgpdPopup open={openRgpd} setOpen={setOpenRgpd} />

      {!openRgpd && !openActuPopup && (
        <PopupInfoLiaison
          open={openPopupInfo}
          setOpen={setOpenPopupInfo}
          handleCloseActuPopup={handleCloseActuPopup}
        />
      )}

      {!openRgpd && popupData && !isAdmin && (
        <PopupActualite
          open={openActuPopup}
          setOpen={setOpenActuPopup}
          data={popupData}
          total={filteredActu.length}
          setPage={setActuIndex}
          loading={loadingActu}
          handleCloseActualite={handleCloseActualite}
        />
      )}
      {!openRgpd && !openActuPopup && !openPopupInfo && result?.data?.search?.data?.length > 0 && (
        <DailyAlert
          openDailyAlert={openDailyAlert}
          handleCloseDailyAlert={handleCloseDailyAlert}
          resultAction={result}
        />
      )}
      {enablePublicite && !isMobile() && (
        <Box className={classes.homepagePublicity}>
          {sliderDatas.length === 0 ? (
            <NoPublicities />
          ) : (
            <CustomSliderPub
              {...publicitySliderSettings}
              onClickImage={onClickImage}
              data={sliderDatas}
            />
          )}
        </Box>
      )}
      <Box className={classes.homepageService}>
        <Hidden mdDown={true}>
          <Box className={classes.titleSectionHome}>
            <Typography className={classes.homepageSectionTitle}>
              Nos services pour vous accompagner{' '}
            </Typography>
          </Box>
        </Hidden>

        {/*<Typography className={classes.homepageSectionSubTitle}>
          Parcourez une multitude de services spécialement créés pour vous.
        </Typography>*/}
        <Box
          className={classes.homepageServiceList}
          style={{ width: computeContainerWidth(enableServices) }}
        >
          {enableServices.map(service => (
            <Card
              key={`homepage_service_item_${service.id}`}
              className={classes.homepageServiceCard}
              // tslint:disable-next-line: jsx-no-lambda
              onClick={() => handleClickService(service.url)}
            >
              <CardContent>
                <Typography
                  align="center"
                  variant="subtitle2"
                  color="textSecondary"
                  gutterBottom={true}
                >
                  {service.title}
                </Typography>
                <img className={classes.imgService} src={service.src} alt="" />
              </CardContent>
              {service.statsKey && userStats[service.statsKey]?.total > 0 && (
                <CardActions className={classes.badgeContainer}>
                  <Badge
                    color="secondary"
                    badgeContent={userStats[service.statsKey]?.total}
                    className={classes.badge}
                    max={999}
                  />
                </CardActions>
              )}
            </Card>
          ))}
        </Box>
      </Box>
      {enablePromotionEncours && !isMobile() && (
        <Box className={classes.homepagePromotion}>
          <Box className={classes.titleSectionHome}>
            <Typography style={{ maxWidth: 342 }} className={classes.homepageSectionTitle}>
              Nos Promotions en cours
            </Typography>
          </Box>
          {/*<Typography className={classes.homepageSectionSubTitle}>
          Retrouvez nos produits promotionnels mis en avant pour vous.
          </Typography>*/}
          <Box className={classes.homepagePromotionSlider}>
            <CustomSlider
              {...promotionSliderSettings}
              data={sliderData}
              onClickImage={onClickImage}
            />
          </Box>
        </Box>
      )}
      <Hidden mdDown={true}>
        <Footer />
      </Hidden>
      {isMobile() && <MenuSuiviAppel onMenuClick={handleMenuClick} />}
      <Box className={classes.fabsContainer}>
        {/*displayDashBoardFab && (
          <Fab color="secondary" onClick={() => history.push('/dashboard')}>
            <Dashboard />
          </Fab>
        )*/}
      </Box>
    </Box>
  );
};

interface MenuSuiviAppelProps {
  onMenuClick: (value: string) => void;
}

const MenuSuiviAppel: FC<MenuSuiviAppelProps> = ({ onMenuClick }) => {
  const [open, setOpen] = useState<boolean>(false);
  const [display, setDisplay] = useState<boolean>(false);
  const styles = useStyles();

  return (
    <div className={styles.menuAppel}>
      <div style={{ display: display ? 'flex' : 'none' }} className={styles.menuAppelItemContainer}>
        <Grow in={open} style={{ transformOrigin: '0 0 0' }} {...(open ? { timeout: 1000 } : {})}>
          <div className={styles.menuAppelItem} onClick={() => onMenuClick('appleRecu')}>
            <div className={styles.menuAppelLabel}>Liste des appels reçus</div>
            <div className={styles.menuAppelFab}>
              <Fab size="small" color="primary">
                <PhoneCallback />
              </Fab>
            </div>
          </div>
        </Grow>
        <Grow in={open}>
          <div className={styles.menuAppelItem} onClick={() => onMenuClick('carnetContact')}>
            <div className={styles.menuAppelLabel}>Carnet des contacts</div>
            <div className={styles.menuAppelFab}>
              <Fab size="small" color="primary">
                <ContactPhone />
              </Fab>
            </div>
          </div>
        </Grow>
      </div>
    </div>
  );
};

export default withRouter(Homepage);
