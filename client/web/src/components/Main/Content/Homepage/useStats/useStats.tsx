import { useQuery } from '@apollo/react-hooks';
import { ME_me } from '../../../../../graphql/Authentication/types/ME';
import { DO_SEARCH_INFORMATION_LIAISON } from '../../../../../graphql/InformationLiaison';
import {
  SEARCH_INFORMATION_LIAISON,
  SEARCH_INFORMATION_LIAISONVariables,
} from '../../../../../graphql/InformationLiaison/types/SEARCH_INFORMATION_LIAISON';
import { GET_COUNT_TODOS } from '../../../../../graphql/Project';
import {
  COUNT_TODOS,
  COUNT_TODOSVariables,
} from '../../../../../graphql/Project/types/COUNT_TODOS';
import { getGroupement, getUser } from '../../../../../services/LocalStorage';
import { ActionStatus, TypeProject } from '../../../../../types/graphql-global-types';
import { useValueParameterAsBoolean } from '../../../../../utils/getValueParameter';

interface StatResult {
  liaison: {
    loading?: boolean;
    error?: Error;
    total?: number;
  };
  actionTodo: {
    loading?: boolean;
    error?: Error;
    total?: number;
  };
}

const useStats = (): StatResult => {
  const groupement = getGroupement();
  const user: ME_me = getUser();
  const userId = (user && user.id) || '';

  const loadLiaison = useQuery<SEARCH_INFORMATION_LIAISON, SEARCH_INFORMATION_LIAISONVariables>(
    DO_SEARCH_INFORMATION_LIAISON,
    {
      variables: {
        type: ['informationliaison'],
        query: {
          query: {
            bool: {
              must: [
                { term: { isRemoved: false } },
                { term: { 'groupement.id': groupement.id } },
                //{ term: { bloquant: true } },
                //{ term: { priority: 3 } }, // Bloquant
                {
                  terms: {
                    'colleguesConcernees.coupleStatutUserConcernee': [`${userId}-NON_LUES`],
                  },
                },
                ,
                //{ terms: { 'colleguesConcernees.lu': [false] } },
                {
                  script: {
                    script: {
                      source: "doc['colleguesConcernees.userConcernee.id'].length >= 1",
                      lang: 'painless',
                    },
                  },
                },
              ],
            },
          },
        },
        sortBy: [{ dateCreation: { order: 'desc' } }],
      },
      fetchPolicy: 'cache-and-network',
    },
  );

  const enableMatriceResponsabilite = useValueParameterAsBoolean('0501');

  const defaultCountTodosVariable: COUNT_TODOSVariables = {
    input: {
      idUser: userId,
      typesProjects: [TypeProject.PROFESSIONNEL],
      myTask: true,
      taskOfOthers: false,
      unassignedTask: false,
      withDate: false,
      withoutDate: false,
      taskStatus: ActionStatus.ACTIVE,
      useMatriceResponsabilite: enableMatriceResponsabilite,
    },
  };

  const countTodos = useQuery<COUNT_TODOS, COUNT_TODOSVariables>(GET_COUNT_TODOS, {
    fetchPolicy: 'cache-and-network',
    variables: defaultCountTodosVariable,
  });

  return {
    liaison: {
      loading: loadLiaison.loading,
      error: loadLiaison.error,
      total: loadLiaison.data?.search?.total,
    },
    actionTodo: {
      loading: countTodos.loading,
      error: countTodos.error,
      total: countTodos.data?.countTodos.action.today,
    },
  };
};

export default useStats;
