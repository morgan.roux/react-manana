import React, { FC, MouseEvent, useState, useCallback } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import classnames from 'classnames';
import ListItem from '@material-ui/core/ListItem';
import useStyles from '../../styles';
import { ACTUALITE_actualite } from '../../../../../../graphql/Actualite/types/ACTUALITE';
import { Typography, Box, ListItemText } from '@material-ui/core';
import moment from 'moment';
import MoreOptions from '../../../../../Common/MoreOptions';
import { MoreOptionsItem } from '../../../../../Common/MoreOptions/MoreOptions';
import Backdrop from '../../../../../Common/Backdrop';
import { ConfirmDeleteDialog } from '../../../../../Common/ConfirmDialog';
import { useMutation, useApolloClient } from '@apollo/react-hooks';
import {
  DELETE_ACTUALITE,
  DELETE_ACTUALITEVariables,
} from '../../../../../../graphql/Actualite/types/DELETE_ACTUALITE';
import {
  DO_DELETE_ACTUALITE,
  DO_MARK_ACTUALITE_NOT_SEEN,
} from '../../../../../../graphql/Actualite';
import { stopEvent } from '../../../../../../services/DOMEvent';
import { ME_me } from '../../../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../../../services/LocalStorage';
import { userIsAuthorized } from '../../../../../../services/actualiteFilter';
import {
  MARK_ACTUALITE_NOT_SEEN,
  MARK_ACTUALITE_NOT_SEENVariables,
} from '../../../../../../graphql/Actualite/types/MARK_ACTUALITE_NOT_SEEN';
import { SEARCH as SEARCH_QUERY } from '../../../../../../graphql/search/query';
import { SEARCH, SEARCHVariables } from '../../../../../../graphql/search/types/SEARCH';
import SnackVariableInterface from '../../../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';

interface ActualiteListItemProps {
  actualite: ACTUALITE_actualite;
  active: boolean;
  listResult: any;
  setCurrentActualite: (actualite: ACTUALITE_actualite) => void;
}

const ActualiteListItem: FC<ActualiteListItemProps & RouteComponentProps> = ({
  actualite,
  setCurrentActualite,
  active,
  history,
  listResult,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [openConfirmDialog, setOpenConfirmDialog] = useState(false);
  const [openBackdrop, setOpenBackdrop] = useState(false);
  const currentUser: ME_me = getUser();
  const idPharmacieUser =
    (currentUser &&
      currentUser.userPpersonnel &&
      currentUser.userPpersonnel.pharmacie &&
      currentUser.userPpersonnel.pharmacie.id) ||
    (currentUser &&
      currentUser.userTitulaire &&
      currentUser.userTitulaire.titulaire &&
      currentUser.userTitulaire.titulaire.pharmacieUser &&
      currentUser.userTitulaire.titulaire.pharmacieUser.id);
  const nbPharmacieSeen: number = actualite.nbSeenByPharmacie || 0;

  const [doDeleteActualite] = useMutation<DELETE_ACTUALITE, DELETE_ACTUALITEVariables>(
    DO_DELETE_ACTUALITE,
    {
      variables: { id: actualite.id, idPharmacieUser, userId: currentUser && currentUser.id },
      update: (cache, { data }) => {
        if (data && data.deleteActualite) {
          const actu = data.deleteActualite;
          const req = cache.readQuery<SEARCH, SEARCHVariables>({
            query: SEARCH_QUERY,
            variables: listResult && listResult.variables,
          });
          if (req && req.search && req.search.data) {
            cache.writeQuery({
              query: SEARCH_QUERY,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: req.search.data.filter((item: any) => {
                      if (item && item.id !== actu.id) {
                        return item;
                      }
                    }),
                  },
                },
              },
              variables: listResult && listResult.variables,
            });
          }
        }
      },
      onError: errors => {
        errors.graphQLErrors.map(err => {
          const snackBarData: SnackVariableInterface = {
            type: 'ERROR',
            message: err.message,
            isOpen: true,
          };
          displaySnackBar(client, snackBarData);
        });
      },
    },
  );

  const editActualite = (event: MouseEvent<any>) => {
    stopEvent(event);
    if (actualite.operation && actualite.operation.id) {
      // Edit operation
      history.push(`/edit/operations-commerciales/${actualite.operation.id}`);
    } else {
      history.push(`/edit/actualite/${actualite.id}`);
    }
  };

  const deleteActualite = (event: MouseEvent<any>) => {
    stopEvent(event);
    // Close dialog and Open backdrop
    setOpenConfirmDialog(false);
    setOpenBackdrop(true);
    // Delete actualite
    doDeleteActualite();
  };

  const markActualiteNotSeen = (event: MouseEvent<any>) => {
    stopEvent(event);
    event.preventDefault();
    event.stopPropagation();
    if (actualite.seen === true) {
      doMarkActualiteNotSeen({
        variables: { id: actualite.id, idPharmacieUser, userId: currentUser && currentUser.id },
      });
    }
  };

  const openDialog = (event: MouseEvent<any>) => {
    stopEvent(event);
    setOpenConfirmDialog(true);
  };

  let moreOptionsItems: MoreOptionsItem[] = [];
  if (userIsAuthorized(currentUser)) {
    if (actualite && actualite.operation) {
      moreOptionsItems = [{ title: 'Modifier', onClick: editActualite }, ...moreOptionsItems];
    } else {
      moreOptionsItems = [
        { title: 'Modifier', onClick: editActualite },
        { title: 'Supprimer', onClick: openDialog },
        ...moreOptionsItems,
      ];
    }
  }

  const handleClick = useCallback(() => {
    setCurrentActualite(actualite);
    window.history.pushState(null, '', `/#/actualite/${actualite.id}`);
  }, []);

  const [doMarkActualiteNotSeen] = useMutation<
    MARK_ACTUALITE_NOT_SEEN,
    MARK_ACTUALITE_NOT_SEENVariables
  >(DO_MARK_ACTUALITE_NOT_SEEN, {
    update: (cache, { data }) => {
      if (data && data.markAsNotSeenActualite) {
        const req = cache.readQuery<SEARCH, SEARCHVariables>({
          query: SEARCH_QUERY,
          variables: listResult && listResult.variables,
        });
        if (req && req.search && req.search.data) {
          cache.writeQuery({
            query: SEARCH_QUERY,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.map((item: any) => {
                    if (
                      item &&
                      data.markAsNotSeenActualite &&
                      item.id === data.markAsNotSeenActualite.id
                    ) {
                      item.seen = data.markAsNotSeenActualite.seen;
                      return item;
                    }
                  }),
                },
              },
            },
            variables: listResult && listResult.variables,
          });
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  return (
    <>
      <ListItem
        onClick={handleClick}
        button={true}
        className={classnames(
          classes.flexColumn,
          classes.padding24,
          active
            ? classes.activeBgColorRed
            : actualite.seen
            ? classes.seenBgColor
            : classes.activeBgColorBlue,
        )}
      >
        <Box display="flex" justifyContent="space-between" width="100%">
          <ListItemText
            className={classnames(classes.MontserratRegular, classes.annonceMessage)}
            primary={actualite && actualite.libelle}
          />
          {moreOptionsItems && moreOptionsItems.length > 0 && (
            <MoreOptions items={moreOptionsItems} />
          )}
        </Box>
        <Box
          marginTop="12px"
          width="100%"
          display="flex"
          justifyContent="space-between"
          flexDirection="row"
        >
          <Typography className={classnames(classes.annonceTime)}>
            {actualite.dateCreation !== actualite.dateModification
              ? `Modifié ${moment(actualite.dateModification).fromNow()}`
              : `Créé ${moment(actualite.dateModification).fromNow()}`}
          </Typography>
        </Box>
        <Box
          marginTop="5px"
          width="100%"
          display="flex"
          justifyContent="space-between"
          flexDirection="row"
        >
          <Typography className={classnames(classes.textList, classes.MontserratRegular)}>
            {currentUser && (currentUser.userPpersonnel || currentUser.userTitulaire)
              ? actualite && actualite.seen === true
                ? `Consultée par la Pharmacie`
                : `Non encore consultée`
              : `Consultée par ${
                  nbPharmacieSeen <= 1
                    ? `${nbPharmacieSeen} Pharmacie`
                    : `${nbPharmacieSeen} Pharmacies`
                }`}
          </Typography>
        </Box>
        <Box
          marginTop="5px"
          width="100%"
          display="flex"
          justifyContent="space-between"
          flexDirection="row"
        >
          <Typography className={classnames(classes.textList, classes.MontserratRegular)}>
            {actualite.operation && actualite.operation.commandePassee
              ? `Commande Passée`
              : actualite.seen
              ? `Lu`
              : `Non lu`}
          </Typography>
        </Box>
      </ListItem>

      {/* Dialog delete actu */}
      <ConfirmDeleteDialog
        title="Suppression d'actualité"
        content={`Êtes-vous sûr de vouloir supprimer l'actualité « ${actualite.libelle} » ?`}
        open={openConfirmDialog}
        setOpen={setOpenConfirmDialog}
        onClickConfirm={deleteActualite}
      />
      {/* Backdrop */}
      <Backdrop
        open={openBackdrop}
        value={"Suppression de l'actualité en cours, veuillez patienter..."}
      />
    </>
  );
};

export default withRouter(ActualiteListItem);
