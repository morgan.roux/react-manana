import React, { FC, useState } from 'react';
import useStyles from './styles';
import CustomAutocomplete from '../../../../Common/CustomAutocomplete';
import { useQuery, useApolloClient } from '@apollo/react-hooks';
import CustomSelect from '../../../../Common/CustomSelect';
import { FormControlLabel, Checkbox } from '@material-ui/core';
import classnames from 'classnames';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { ACTUALITE_actualite } from '../../../../../graphql/Actualite/types/ACTUALITE';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import { DO_SEARCH_PROJECT } from '../../../../../graphql/Project';
import {
  SEARCH_PROJECT,
  SEARCH_PROJECTVariables,
} from '../../../../../graphql/Project/types/SEARCH_PROJECT';
import { CustomDatePicker } from '../../../../Common/CustomDateTimePicker';

interface TodoGeneratorProps {
  generateAction: boolean;
  actionDescription?: string;
  handleChangeProjectAutocomplete: (inputValue: any) => void;
  handleChangeActionDescription: any;
  handleChangeActionDueDate: any;
  handleChangeCheckbox: any;
  project?: any;
  actionPriorite?: any;
  actionDueDate?: any;
  handleChange: any;
}

const TodoGenerator: FC<TodoGeneratorProps & RouteComponentProps> = ({
  generateAction,
  actionDescription,
  handleChangeProjectAutocomplete,
  handleChangeActionDescription,
  handleChangeActionDueDate,
  handleChangeCheckbox,
  project,
  actionPriorite,
  actionDueDate,
  handleChange,
  history,
}) => {
  console.log('project : ', project);
  const classes = useStyles({});
  const client = useApolloClient();
  const pathname = history.location.pathname;
  const isOnEdit: boolean = pathname.includes('/edit');
  const [actu, setActu] = useState<ACTUALITE_actualite | null>(null);
  const noProject = (): boolean => {
    return searchProjectsResult.data &&
      searchProjectsResult.data.search &&
      searchProjectsResult.data.search.data &&
      searchProjectsResult.data.search.data.length === 0
      ? true
      : false;
  };
  const disabledActionInputs = (): boolean => {
    return generateAction ? false : true;
  };
  const listProjectPriority = [
    { id: 1, value: 'P1' },
    { id: 2, value: 'P2' },
    { id: 3, value: 'P3' },
    { id: 4, value: 'P4' },
  ];
  // Get projects list
  const searchProjectsResult = useQuery<SEARCH_PROJECT, SEARCH_PROJECTVariables>(
    DO_SEARCH_PROJECT,
    {
      variables: {
        type: ['project'],
      },
      fetchPolicy: 'cache-and-network',
      skip: !generateAction,
      onError: errors => {
        errors.graphQLErrors.map(err => {
          const snackBarData: SnackVariableInterface = {
            type: 'ERROR',
            message: err.message,
            isOpen: true,
          };
          displaySnackBar(client, snackBarData);
        });
      },
    },
  );

  const disabledActionProject = (): boolean => {
    if (isOnEdit) {
      return true;
    }
    return false;
  };
  return (
    <div
      className={
        generateAction
          ? classes.generateActionContainer
          : classnames(classes.generateActionContainer, classes.generateActionDisabled)
      }
    >
      <fieldset>
        <legend className={classes.fieldsetLegend} onClick={handleChangeCheckbox}>
          <FormControlLabel
            className={classes.formControlLabel}
            control={
              <Checkbox
                checked={generateAction || false}
                name="generateAction"
                value="generateAction"
              />
            }
            label="Générer une action dans la TODO liste de la pharmacie"
          />
        </legend>
        <div className={classes.actionFormContainer}>
          <CustomAutocomplete
            id="idProject"
            options={
              (searchProjectsResult &&
                searchProjectsResult.data &&
                searchProjectsResult.data.search &&
                searchProjectsResult.data.search.data) ||
              []
            }
            loading={searchProjectsResult && searchProjectsResult.loading}
            optionLabelKey="name"
            value={project}
            onAutocompleteChange={handleChangeProjectAutocomplete}
            label="Projet"
            required={!disabledActionInputs()}
            disabled={disabledActionProject() || disabledActionInputs()}
          />
          <ReactQuill
            className={
              disabledActionInputs()
                ? classnames(classes.customReactQuillDisable, classes.customReactQuill)
                : classes.customReactQuill
            }
            theme="snow"
            value={actionDescription || ''}
            onChange={handleChangeActionDescription}
            readOnly={noProject() ? true : disabledActionInputs()}
          />
          <CustomSelect
            label="Priorité"
            list={listProjectPriority}
            listId="id"
            index="value"
            name="actionPriorite"
            value={actionPriorite}
            onChange={handleChange}
            required={!disabledActionInputs()}
            disabled={noProject() ? true : disabledActionInputs()}
          />
          <CustomDatePicker
            label="Date limite de la tâche"
            onChange={handleChangeActionDueDate}
            name="actionDueDate"
            value={actionDueDate}
            disabled={noProject() ? true : disabledActionInputs()}
            required={!disabledActionInputs()}
            disablePast={true}
          />
        </div>
      </fieldset>
    </div>
  );
};

export default withRouter(TodoGenerator);
