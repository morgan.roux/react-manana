import React, { FC } from 'react';
import { Typography, Button } from '@material-ui/core';
import { ActualiteViewProps } from '../../ActualiteView';
import useStyles from './styles';

const AppPresentation: FC<ActualiteViewProps> = ({ actualite }) => {
  const classes = useStyles({});
  return (
    <div className={classes.root}>
      <div className={classes.appContainer}>
        <Typography>App preview container</Typography>
      </div>
      <div className={classes.adviceText}>
        <Typography>- Commandez des produits, livrés chez vous ou à retirer sur place</Typography>
        <Typography>
          - Envoyez vos ordonnances pour un retrait rapide des produits en pharmacie
        </Typography>
        <Typography>- Programmez un entretien individuel avec votre pharmacien</Typography>
      </div>
      <Typography className={classes.presentationText}>
        Chez Leader Santé comme dans la plupart des groupements, la vaccination à l’officine a signé
        un virage décisif dans la formation des équipes. Le groupement qui a accueilli plus de 190
        de ses adhérents au sein de sa Leader santé Academy, a souhaité de ne pas en rester là. «
        Nous avons décidé d’investir dans la montée en compétences et en expertises des équipes
        officinales, et ce de manière inédite. Trop souvent la formation, qu’elle soit dispensée
        lors de soirées...
      </Typography>
      <Button className={classes.btnDownloadApp}>Télécharger cette application</Button>
    </div>
  );
};

export default AppPresentation;
