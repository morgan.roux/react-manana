import React, { FC, useState, useEffect } from 'react';
import { Typography, Button } from '@material-ui/core';
import { ActualiteViewProps } from '../../ActualiteView';
import useStyles from './styles';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import { ACTUALITE_actualite_fichierPresentations } from '../../../../../../../graphql/Actualite/types/ACTUALITE';
import ReactIframe from 'react-iframe';
import CustomButton from '../../../../../../Common/CustomButton';
import { PictureAsPdf } from '@material-ui/icons';
import PdfIcon from '../../../../../../../assets/img/pdfIcon';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';

export const GET_LOCAL_HIDE_PRESENTATION = gql`
  {
    hidePresentation @client
  }
`;
const AttachedFiles: FC<ActualiteViewProps> = ({ actualite }) => {
  const classes = useStyles({});
  const defaultFile: ACTUALITE_actualite_fichierPresentations | null =
    (actualite.fichierPresentations &&
      actualite.fichierPresentations.length > 0 &&
      actualite.fichierPresentations[0]) ||
    null;

  const [clickedFile, setClickedFile] = useState<ACTUALITE_actualite_fichierPresentations | null>(
    null,
  );
  const [actuId, setActuId] = useState<string | null>(null);

  const hidePresentationResult = useQuery(GET_LOCAL_HIDE_PRESENTATION);

  const [showFile, setShowFile] = useState<boolean>(true);

  useEffect(() => {
    if (
      hidePresentationResult &&
      hidePresentationResult.data &&
      hidePresentationResult.data.hidePresentation === true
    ) {
      setShowFile(false);
    } else if (
      hidePresentationResult &&
      hidePresentationResult.data &&
      hidePresentationResult.data.hidePresentation === false &&
      showFile
    ) {
      setShowFile(false);
    }
  }, [hidePresentationResult]);

  const toggleShowFile = (file: ACTUALITE_actualite_fichierPresentations) => {
    setClickedFile(file);
    if (clickedFile && clickedFile.id === file.id) {
      setShowFile(!showFile);
    } else {
      setShowFile(true);
    }
  };

  const fichierPresentations = actualite.fichierPresentations;

  if (actuId !== actualite.id) {
    setActuId(actualite.id);
  }

  useEffect(() => {
    setClickedFile(defaultFile);
    setShowFile(true);
  }, [actuId]);

  return (
    <div className={classes.root}>
      <div className={classes.btnsContainerIfViewPdf}>
        {/* Buttons */}
        <div className={classes.btnsContainer}>
          {fichierPresentations &&
            fichierPresentations.length &&
            fichierPresentations.map(
              fichier =>
                fichier && (
                  // <Button
                  //   key={`btn_${fichier.id}`}
                  //   variant="contained"
                  //   className={classes.btnFile}
                  //   fullWidth={false}
                  //   onClick={() => toggleShowFile(fichier)}
                  // >
                  //   {fichier.nomOriginal}
                  //   {clickedFile && clickedFile.id === fichier.id && showFile ? (
                  //     <VisibilityIcon />
                  //   ) : (
                  //     <VisibilityOffIcon />
                  //   )}
                  // </Button>
                  <CustomButton
                    color={
                      clickedFile && clickedFile.id === fichier.id && showFile
                        ? 'secondary'
                        : 'default'
                    }
                    key={`btn_${fichier.id}`}
                    children={fichier.nomOriginal}
                    onClick={() => toggleShowFile(fichier)}
                    className={
                      clickedFile && clickedFile.id === fichier.id && showFile
                        ? classes.fileButtonActive
                        : classes.fileButton
                    }
                    startIcon={
                      clickedFile && clickedFile.id === fichier.id && showFile ? (
                        <PdfIcon fill={'#FFF'} />
                      ) : (
                        <PdfIcon fill={'#424242'} />
                      )
                    }
                  />
                ),
            )}
        </div>
        {/* Iframe */}
        {clickedFile && showFile && (
          <ReactIframe
            id={clickedFile.id}
            key={clickedFile.id}
            className={classes.seeContentPdf}
            width="100%"
            url={`${clickedFile.publicUrl}`}
          />
        )}
      </div>
    </div>
  );
};

export default AttachedFiles;
