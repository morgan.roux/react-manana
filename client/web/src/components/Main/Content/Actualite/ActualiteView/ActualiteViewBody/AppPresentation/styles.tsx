import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      letterSpacing: 0,
      fontFamily: 'Montserrat',
      '& > div': {
        width: '100%',
      },
    },
    appContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      height: 400,
      border: '1px solid #0000001A',
      borderRadius: 6,
      opacity: 1,
    },
    adviceText: {
      marginTop: 20,
      textAlign: 'left',
      fontFamily: 'Roboto',
      fontWeight: 500,
      fontSize: 14,
      color: '#00000080',
      opacity: 1,
    },
    presentationText: {
      marginTop: 20,
      textAlign: 'justify',
      color: '#000000',
      opacity: 1,
      fontWeight: 600,
      fontSize: 16,
    },
    btnDownloadApp: {
      marginTop: 20,
      color: '#ffffff',
      textAlign: 'center',
      textTransform: 'uppercase',
      opacity: 1,
      fontSize: 14,
      fontWeight: 'bold',
      background: '#8CC63F 0% 0% no-repeat padding-box',
      boxShadow: '0px 2px 2px #00000040',
      borderRadius: 3,
      padding: '10px 30px',
      '&:hover': {
        background: '#388e3c 0% 0% no-repeat padding-box',
      },
    },
  }),
);

export default useStyles;
