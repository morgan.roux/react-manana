import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      marginTop: 50,
    },
    attachFile: {
      fontFamily: 'Roboto',
      fontSize: 16,
      fontWeight: 'bold',
      letterSpacing: 0.01,
      color: '#1D1D1D',
      opacity: 1,
    },
    hr: {
      border: '1px solid #878787',
      opacity: 1,
      height: 0,
      width: '100%',
    },
    btnFile: {
      marginBottom: 20,
      textTransform: 'lowercase',
      width: 'fit-content',
      color: '#ffffff',
      background: '#5D5D5D 0% 0% no-repeat padding-box',
      borderRadius: 4,
      opacity: 1,
      '& > span > svg': {
        marginLeft: 10,
      },
      '&:hover': {
        background: '#757575 0% 0% no-repeat padding-box',
      },
    },
    seeContentPdf: {
      height: 'calc(100vh - 351px)',
    },
    btnsContainer: {
      display: 'flex',
      marginBottom: 20,
      '& > button:not(:nth-child(1))': {
        marginLeft: 20,
      },
    },
    btnsContainerIfViewPdf: {
      flexDirection: 'column',
      '& > div:not(:nth-child(1))': {
        marginTop: 10,
      },
    },
    fileButton: {
      background: theme.palette.common.white,
      border: '1px solid #616161',
      boxShadow: 'none',
      '&:hover': {
        boxShadow: 'none',
      },
    },
    fileButtonActive: {
      // background: theme.palette.primary.main,
      // color: theme.palette.common.white,
      // borderRadius: 6,
    },
  }),
);

export default useStyles;
