import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'flex-start',
      flexDirection: 'column',
      // paddingTop: 20,
      // paddingLeft: 10,
      '& .MuiListItem-container': {
        width: '100%',
      },
      '& .MuiListItemIcon-root': {
        minWidth: 35,
      },
      '& .MuiListItem-gutters': {
        // paddingLeft: 0,
      },
      '& .MuiCheckbox-root': {
        paddingTop: '0px !important',
        paddingBottom: '0px !important',
        color: '#000',
      },
      '& .MuiCheckbox-colorSecondary.Mui-checked': {
        color: '#000',
      },
    },
    cibleContainer: {
      width: '100%',
      // paddingTop: 35,
      '& .MuiTypography-h6': {
        fontSize: 14,
      },
      '& .MuiListItemIcon-root': {
        minWidth: 'auto',
      },
      '& > ul, & > ul > li': {
        paddingLeft: 0,
        width: '100%',
      },
      '& .MuiListItem-secondaryAction': {
        paddingRight: 70,
        paddingLeft: 10,
      },
    },
    headerContainer: {
      height: 70,
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      borderBottom: '1px solid #E5E5E5',
      paddingLeft: 10,
    },

    number: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginRight: 10,
      marginLeft: 10,
      '& > span': {
        fontSize: 12,
      },
    },
  }),
);

export default useStyles;
