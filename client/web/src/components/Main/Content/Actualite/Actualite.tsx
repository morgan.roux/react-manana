import React, { FC } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import MainContainer from '../../../Common/MainContainer';
import AddIcon from '@material-ui/icons/Add';
import { SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT } from '../../../../Constant/roles';
import { Visibility, ThumbUp, ChatBubble } from '@material-ui/icons';
import { LayoutType } from '../../../Common/MainContainer/MainContainer';
import MoreOptions from '../../../Common/MainContainer/TwoColumnsContainer/LeftListContainer/MoreOptions/Actualite';
import RightContentContainer from '../../../Common/MainContainer/TwoColumnsContainer/RightContentContainer';
import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { IconButton } from '@material-ui/core';
import { GET_LOCAL_HIDE_PRESENTATION } from './ActualiteView/ActualiteViewBody/AttachedFiles/AttachedFiles';
import { SideMenuButtonInterface } from '../../../Common/SideMenuButton/SideMenuButton';
import { ME_me } from '../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../services/LocalStorage';
import { ACTU_CREATE_CODE } from '../../../../Constant/authorization';

interface ActualiteProps {
  listResult: any;
  handleScroll: any;
  match: { params: { id: string | undefined } };
}

const Actualite: FC<ActualiteProps & RouteComponentProps> = ({
  listResult,
  handleScroll,
  location: { pathname },
}) => {
  const user: ME_me = getUser();
  const userCodesTraitements: string[] = (user && (user.codeTraitements as any)) || [];
  const isInPartenaireLaboratoire = pathname.includes('/laboratoires');

  const addActuButton: SideMenuButtonInterface = {
    text: 'créer actualite',
    url: '/create/actualite',
    color: 'pink',
    icon: <AddIcon />,
    authorized: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT],
    userIsAuthorized: userCodesTraitements.includes(ACTU_CREATE_CODE),
  };

  const client = useApolloClient();
  const hidePresentationResult = useQuery(GET_LOCAL_HIDE_PRESENTATION);
  const hidePresentationValue =
    (hidePresentationResult &&
      hidePresentationResult.data &&
      hidePresentationResult.data.hidePresentation) ||
    false;
  const hidePresentation = () => {
    client.writeData({
      data: {
        hidePresentation: !hidePresentationValue,
      },
    });
  };

  const listItemFields = {
    url: '/actualite',
    type: 'item.name',
    title: 'libelle',
    infoList: [
      { key: 'Origine', value: 'origine.libelle' },
      { key: 'Labo', value: 'laboratoire.nomLabo' },
      { key: 'Créée le', value: 'dateCreation', dateFormat: 'L' },
    ],
    dateDebutFin: true,
    iconList: [
      { icon: <Visibility />, value: 'nbSeenByPharmacie', position: 'start' },
      {
        icon: (
          <IconButton onClick={hidePresentation}>
            <ThumbUp />
          </IconButton>
        ),
        value: 'userSmyleys.total',
      },
      {
        icon: (
          <IconButton onClick={hidePresentation}>
            <ChatBubble />
          </IconButton>
        ),
        value: 'comments.total',
        position: 'flex-end',
      },
    ],
  };

  const noContentValues = {
    list: {
      title: 'Aucune actualité dans la liste',
      subtitle: "Crées en une en cliquant sur le bouton d'en haut.",
    },
    content: {
      title: 'Aperçu détaillé de vos actualités',
      subtitle:
        "Choisissez une actualité dans la liste de droite pour l'afficher en détails dans cette partie.",
    },
  };

  // if ((listResult && listResult.loading && !listResult.data) || !listResult || !actualites) {
  //   return <Loader />;
  // }

  return (
    <MainContainer
      noContentValues={noContentValues}
      layout={isInPartenaireLaboratoire ? LayoutType.Empty : LayoutType.TwoColumns}
      listResult={listResult}
      handleScroll={handleScroll}
      moreOptionsItem={<MoreOptions listResult={listResult} />}
      listItemFields={listItemFields}
      rightContentChildren={<RightContentContainer listResult={listResult} />}
      listButton={!isInPartenaireLaboratoire ? addActuButton : undefined}
    />
  );
};

export default withRouter(Actualite);
