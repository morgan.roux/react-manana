import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      color: '#1D1D1D',
      fontFamily: 'Montserrat',
      letterSpacing: 0,
      opacity: 1,
      '& > div': {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      },
      '& .MuiRadio-root, & .MuiRadio-colorSecondary.Mui-checked': {
        color: '#000000',
      },
    },
    title: {
      fontSize: 20,
      fontWeight: 600,
      marginBottom: '10px',
    },
    radioBtnContainer: {
      margin: '10px 0px',
      '& .MuiFormControlLabel-root': {
        fontSize: 16,
        fontWeight: 500,
        fontFamily: 'Roboto',
      },
    },
    content: {
      '& > div': {
        width: '100%',
      },
    },
    selectionCibleContainer: {
      color: '#00000080',
      fontWeight: 600,
      fontSize: 16,
      padding: '150px 0px',
      textAlign: 'center',
    },
    fichierCibleContainer: {
      marginTop: 10,
      maxWidth: 650,
    },
  }),
);

export default useStyles;
