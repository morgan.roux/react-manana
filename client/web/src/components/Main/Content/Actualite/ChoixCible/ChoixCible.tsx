import React, { ChangeEvent, useEffect, FC, Dispatch, SetStateAction } from 'react';
import useStyles from './styles';
import { Typography, Radio, FormControlLabel } from '@material-ui/core';
import Dropzone from '../../../../Common/Dropzone';
import CibleTable from '../CibleTable';
import { SelectedInterface } from '../Interface';
import { ActuRadioCible } from '../Create/CreateActualite';
import { COUNT_ACTUALITES_CIBLES_countActualitesCibles } from '../../../../../graphql/Actualite/types/COUNT_ACTUALITES_CIBLES';

export interface ChoixCibleProps {
  actuRadioCible: ActuRadioCible;
  setActuRadioCible: Dispatch<SetStateAction<ActuRadioCible>>;
  showSubSidebar: boolean;
  setShowSubSidebar: Dispatch<SetStateAction<boolean>>;
  showSelectableCible: boolean;
  setShowSelectableCible: Dispatch<SetStateAction<boolean>>;
  clickedCible: COUNT_ACTUALITES_CIBLES_countActualitesCibles | null;
  setClickedCible: Dispatch<SetStateAction<COUNT_ACTUALITES_CIBLES_countActualitesCibles | null>>;
  fichiersCibles?: File[];
  setFichiersCibles?: Dispatch<SetStateAction<File[]>>;
  where?: string;
}

const ChoixCible: FC<ChoixCibleProps & SelectedInterface> = ({
  actuRadioCible,
  setActuRadioCible,
  showSubSidebar,
  // setShowSubSidebar,
  fichiersCibles,
  setFichiersCibles,
  where,
  // showSelectableCible,
  setShowSelectableCible,
  clickedCible,
  // setClickedCible,
  selectedCollabo,
  setSelectedCollabo,
  selectedPharma,
  setSelectedPharma,
  selectedPharmaRoles,
  setSelectedPharmaRoles,
  selectedPresident,
  setSelectedPresident,
  presidentType,
  setPresidentType,
  selectedLabo,
  setSelectedLabo,
  selectedPartenaire,
  setSelectedPartenaire,
  selectedRoles,
  setSelectedRoles,
  selectedParent,
  setSelectedParent,
}) => {
  const classes = useStyles({});

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target && e.target.value) {
      setActuRadioCible(e.target.value as any);
    }
  };

  // Show or Hide selectable cible
  // SET CHOIX
  useEffect(() => {
    switch (actuRadioCible) {
      case 'globalite-user':
        setShowSelectableCible(false);
        break;
      case 'selection-cible':
        if (where !== 'inResume') {
          setShowSelectableCible(true);
        }
        break;
      case 'fichier-cible':
        setShowSelectableCible(false);
        break;
      default:
        setShowSelectableCible(false);
        break;
    }
  }, [actuRadioCible]);

  return (
    <div className={classes.root}>
      {where !== 'inResume' && (
        <Typography className={classes.title}>Cible de l'actualité</Typography>
      )}

      <div className={classes.radioBtnContainer}>
        <FormControlLabel
          control={
            <Radio
              checked={actuRadioCible === 'globalite-user'}
              value="globalite-user"
              onChange={handleChange}
              name="radio-globalite-user"
              inputProps={{ 'aria-label': 'Globalité des utilisateurs du réseau' }}
              disabled={where === 'inResume' ? true : false}
            />
          }
          label="Globalité des utilisateurs du réseau"
        />
        <FormControlLabel
          control={
            <Radio
              checked={actuRadioCible === 'selection-cible'}
              value="selection-cible"
              onChange={handleChange}
              name="radio-selection-cible"
              inputProps={{ 'aria-label': 'Choix de la cible' }}
              disabled={where === 'inResume' ? true : false}
            />
          }
          label="Choix de la cible"
        />
        <FormControlLabel
          control={
            <Radio
              checked={actuRadioCible === 'fichier-cible'}
              value="fichier-cible"
              onChange={handleChange}
              name="radio-fichier-cible"
              inputProps={{ 'aria-label': 'Fichier des cibles' }}
              color="default"
              disabled={where === 'inResume' ? true : false}
            />
          }
          label="Fichier des cibles"
        />
      </div>

      {where !== 'inResume' && (
        <div className={classes.content}>
          {actuRadioCible === 'globalite-user' && (
            <div className={classes.selectionCibleContainer}>
              Globalité des utilisateurs du réseau : Pharmacies et collaborateurs du groupement 
            </div>
          )}
          {actuRadioCible === 'selection-cible' && (
            <div>
              {clickedCible ? (
                <CibleTable
                  cible={clickedCible}
                  selectedCollabo={selectedCollabo}
                  setSelectedCollabo={setSelectedCollabo}
                  selectedPharma={selectedPharma}
                  setSelectedPharma={setSelectedPharma}
                  selectedPharmaRoles={selectedPharmaRoles}
                  setSelectedPharmaRoles={setSelectedPharmaRoles}
                  selectedPresident={selectedPresident}
                  setSelectedPresident={setSelectedPresident}
                  presidentType={presidentType}
                  setPresidentType={setPresidentType}
                  selectedLabo={selectedLabo}
                  setSelectedLabo={setSelectedLabo}
                  selectedPartenaire={selectedPartenaire}
                  setSelectedPartenaire={setSelectedPartenaire}
                  selectedRoles={selectedRoles}
                  setSelectedRoles={setSelectedRoles}
                  selectedParent={selectedParent}
                  setSelectedParent={setSelectedParent}
                />
              ) : (
                <div className={classes.selectionCibleContainer}>
                  Choissisez vos cibles sur le menu du gauche
                </div>
              )}
            </div>
          )}
          {actuRadioCible === 'fichier-cible' && (
            <div className={classes.fichierCibleContainer}>
              <Dropzone
                contentText="Glissez et déposez le fichier contenant la liste des cibles de l'actualité (au format EXCEL)"
                setSelectedFiles={setFichiersCibles}
                selectedFiles={fichiersCibles}
                multiple={false}
                acceptFiles="application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
              />
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default ChoixCible;
