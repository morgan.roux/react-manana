import React, { FC, useEffect, useState } from 'react';
import ChoixCible from '../ChoixCible';
import Backdrop from '../../../../Common/Backdrop';
import FinalUserTable from '../FinalUserTable';
import { Box } from '@material-ui/core';
import {
  PharmacieRolesInput,
  PresidentRegionsInput,
  TypeFichier,
} from '../../../../../types/graphql-global-types';
import { useLazyQuery, useMutation, useApolloClient } from '@apollo/react-hooks';
import {
  ACTUALITE_ALL_USERS_CIBLE,
  ACTUALITE_ALL_USERS_CIBLEVariables,
} from '../../../../../graphql/Actualite/types/ACTUALITE_ALL_USERS_CIBLE';
import { GET_ACTUALITE_ALL_USERS_CIBLE } from '../../../../../graphql/Actualite';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import { uploadToS3 } from '../../../../../services/S3';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { formatFilenameWithoutDate } from '../../../../../utils/filenameFormater';
import { ChoixCibleProps } from '../ChoixCible/ChoixCible';
import { SelectedInterface } from '../Interface';

interface ResumeProps {
  codeServices?: string[] | null;
  idPartenaires?: string[] | null;
  idLaboratoires?: string[] | null;
  codeRoles?: string[] | null;
  pharmacieRoles?: PharmacieRolesInput | null;
  presidentRegions?: PresidentRegionsInput | null;
  fichierCible?: File;
}

const Resume: FC<ResumeProps & SelectedInterface & ChoixCibleProps> = ({
  codeServices,
  idPartenaires,
  idLaboratoires,
  // codeRoles,
  pharmacieRoles,
  presidentRegions,
  fichierCible,
  actuRadioCible,
  setActuRadioCible,
  showSubSidebar,
  setShowSubSidebar,
  showSelectableCible,
  setShowSelectableCible,
  clickedCible,
  setClickedCible,
  selectedCollabo,
  setSelectedCollabo,
  selectedPharma,
  setSelectedPharma,
  selectedPharmaRoles,
  setSelectedPharmaRoles,
  selectedPresident,
  setSelectedPresident,
  presidentType,
  setPresidentType,
  selectedLabo,
  setSelectedLabo,
  selectedPartenaire,
  setSelectedPartenaire,
  selectedRoles,
  setSelectedRoles,
  selectedParent,
  setSelectedParent,
}) => {
  const [resumeText, setResumeText] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);
  const client = useApolloClient();

  // Get final users cibles
  const [getActualiteAllUsersCible, getActualiteAllUsersCibleResult] = useLazyQuery<
    ACTUALITE_ALL_USERS_CIBLE,
    ACTUALITE_ALL_USERS_CIBLEVariables
  >(GET_ACTUALITE_ALL_USERS_CIBLE, {
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  // console.log('getActualiteAllUsersCibleResult => ', getActualiteAllUsersCibleResult);

  const finalUsersCibles =
    getActualiteAllUsersCibleResult &&
    getActualiteAllUsersCibleResult.data &&
    getActualiteAllUsersCibleResult.data.actualiteAllUsersCible;

  const [doCreatePutPresignedUrl, doCreatePutPresignedUrlResult] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: data => {
      // On Complete presigned url, Upload to S3
      if (data && data.createPutPresignedUrls) {
        data.createPutPresignedUrls.map((presignedFile, index) => {
          // Upload to s3
          if (presignedFile && fichierCible) {
            setLoading(true);
            uploadToS3(fichierCible, presignedFile.presignedUrl)
              .then(result => {
                if (result && result.status === 200) {
                  setLoading(false);
                  // Get Actualite All Users Cibles
                  getActualiteAllUsersCible({
                    variables: {
                      fichierCible: {
                        chemin: presignedFile.filePath,
                        type: TypeFichier.EXCEL,
                        nomOriginal: fichierCible.name,
                      },
                    },
                  });
                }
              })
              .catch(error => {
                console.log(error);
                const snackBarData: SnackVariableInterface = {
                  type: 'ERROR',
                  message:
                    "Erreur lors de l'envoye du fichier cible. Si vous utilisez AdBlocker, désactivez-le et réessayez.",
                  isOpen: true,
                };
                displaySnackBar(client, snackBarData);
              });
          }
        });
      }
    },
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message:
          "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  /**
   * Ectouter actuRadioCible ou fichierCible et lancher la requête getActualiteAllUsersCible
   */
  useEffect(() => {
    if (actuRadioCible === 'selection-cible') {
      getActualiteAllUsersCible({
        variables: {
          codeServices,
          idPartenaires,
          idLaboratoires,
          pharmacieRoles,
          presidentRegions,
        },
      });
    }

    if (actuRadioCible === 'fichier-cible' && fichierCible) {
      // Create presigned url for upload to S3
      const filePath = `temp/${formatFilenameWithoutDate(fichierCible)}`;
      doCreatePutPresignedUrl({ variables: { filePaths: [filePath] } });
    }
  }, [actuRadioCible, fichierCible]);

  // SET RESUME
  useEffect(() => {
    if (actuRadioCible) {
      switch (actuRadioCible) {
        case 'globalite-user':
          setResumeText('Créer une actualité pour la globalité des utilisateurs sur le réseau ?');
          break;
        case 'selection-cible':
          setResumeText('Créer une actualité pour les utilisateurs sélectionnés ?');
          break;
        case 'fichier-cible':
          setResumeText('Créer une actualité pour les utilisateurs dans le fichier ?');
          break;
        default:
          break;
      }
    }
  }, [actuRadioCible]);

  return (
    <Box display="flex" flexDirection="column" alignItems="center">
      {(getActualiteAllUsersCibleResult.loading ||
        doCreatePutPresignedUrlResult.loading ||
        loading) && <Backdrop />}
      <ChoixCible
        where="inResume"
        actuRadioCible={actuRadioCible}
        setActuRadioCible={setActuRadioCible}
        showSubSidebar={showSubSidebar}
        setShowSubSidebar={setShowSubSidebar}
        showSelectableCible={showSelectableCible}
        setShowSelectableCible={setShowSelectableCible}
        clickedCible={clickedCible}
        setClickedCible={setClickedCible}
        selectedCollabo={selectedCollabo}
        setSelectedCollabo={setSelectedCollabo}
        selectedPharma={selectedPharma}
        setSelectedPharma={setSelectedPharma}
        selectedPharmaRoles={selectedPharmaRoles}
        setSelectedPharmaRoles={setSelectedPharmaRoles}
        selectedPresident={selectedPresident}
        setSelectedPresident={setSelectedPresident}
        presidentType={presidentType}
        setPresidentType={setPresidentType}
        selectedLabo={selectedLabo}
        setSelectedLabo={setSelectedLabo}
        selectedPartenaire={selectedPartenaire}
        setSelectedPartenaire={setSelectedPartenaire}
        selectedRoles={selectedRoles}
        setSelectedRoles={setSelectedRoles}
        selectedParent={selectedParent}
        setSelectedParent={setSelectedParent}
      />
      <p>{resumeText}</p>
      {(actuRadioCible === 'selection-cible' || actuRadioCible === 'fichier-cible') &&
        finalUsersCibles &&
        finalUsersCibles.length > 0 && <FinalUserTable data={finalUsersCibles} />}
    </Box>
  );
};

export default Resume;
