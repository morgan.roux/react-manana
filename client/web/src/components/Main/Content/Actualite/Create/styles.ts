import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    createActuRoot: {
      display: 'flex',
      flexDirection: 'row',
      width: '100%',
      '& > div:nth-child(1)': {
        // paddingRight: 20,
      },
    },
    leftSidebarConainer: {
      position: 'sticky',
      top: 0,
      width: 275,
      flexShrink: 0,
      height: 'calc(100vh - 86px)',
      borderRight: '1px solid #E5E5E5',
    },
  }),
);

export default useStyles;
