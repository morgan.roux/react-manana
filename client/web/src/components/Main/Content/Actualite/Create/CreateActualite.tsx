import React, { FC, useState, useEffect } from 'react';
import useStyles from './styles';
import { Step } from '../../../../Common/Stepper/Stepper';
import Stepper from '../../../../Common/Stepper';
import ActualiteForm from '../Form';
import { AxiosResponse } from 'axios';
import ChoixCible from '../ChoixCible';
import { useMutation, useApolloClient } from '@apollo/react-hooks';
import {
  DO_CREATE_AND_UPDATE_ACTUALITE,
  DO_SEND_NOTIFICATION_ACTUALITE_TO_CIBLE,
} from '../../../../../graphql/Actualite';
import {
  CREATE_AND_UPDATE_ACTUALITE,
  CREATE_AND_UPDATE_ACTUALITEVariables,
} from '../../../../../graphql/Actualite/types/CREATE_AND_UPDATE_ACTUALITE';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import { uploadToS3 } from '../../../../../services/S3';
import { RouteComponentProps, withRouter } from 'react-router-dom';
// import { SEARCH as SEARCH_QUERY } from '../../../../../graphql/search/query';
// import { SEARCHVariables, SEARCH } from '../../../../../graphql/search/types/SEARCH';
import LeftSidebar from '../LeftSidebar/LeftSidebar';
import {
  TypeFichier,
  FichierInput,
  TypePresidentCible,
} from '../../../../../types/graphql-global-types';
import Resume from '../Resume';
import { getGroupement } from '../../../../../services/LocalStorage';
import {
  SEND_NOTIFICATION_ACTUALITE_TO_CIBLEVariables,
  SEND_NOTIFICATION_ACTUALITE_TO_CIBLE,
} from '../../../../../graphql/Actualite/types/SEND_NOTIFICATION_ACTUALITE_TO_CIBLE';
import Backdrop from '../../../../Common/Backdrop';
import { formatFilenameWithoutDate } from '../../../../../utils/filenameFormater';
import { strippedString } from '../../../../../utils/Helpers';
import { COUNT_ACTUALITES_CIBLES_countActualitesCibles } from '../../../../../graphql/Actualite/types/COUNT_ACTUALITES_CIBLES';
import useActualiteForm from '../Form/useActualiteForm';

export type ActuRadioCible = 'globalite-user' | 'selection-cible' | 'fichier-cible';

const CreateActualite: FC<RouteComponentProps & {
  listResult: any;
  history: {
    location: { state: any };
  };
}> = ({
  history: {
    location: { state },
    push,
  },
  location: { pathname },
}) => {
  const classes = useStyles({});
  const groupement = getGroupement();
  const isOnCreate: boolean = pathname === '/create/actualite';
  const isOnEdit: boolean = pathname.includes('/edit/actualite/');
  const [activeStep, setActiveStep] = useState<number | undefined>(0);

  const [
    clickedCible,
    setClickedCible,
  ] = useState<COUNT_ACTUALITES_CIBLES_countActualitesCibles | null>(null);

  const [showSubSidebar, setShowSubSidebar] = useState<boolean>(false);
  const [actuRadioCible, setActuRadioCible] = useState<ActuRadioCible>('globalite-user');
  const [showSelectableCible, setShowSelectableCible] = useState<boolean>(false);

  let uploadResult: AxiosResponse<any> | null = null;
  const [fichierPresentations, setFichierPresentations] = useState<File[]>([]);
  const [fichierPresentationsWithChemin, setFichierPresentationsWithChemin] = useState<{
    files: FichierInput[];
  }>({ files: [] });
  const [fichiersCibles, setFichiersCibles] = useState<File[]>([]);
  let fichiersCibleChemin: string | null = null;
  const [allFiles, setAllFiles] = useState<File[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  const [selectedCollabo, setSelectedCollabo] = useState<any[]>([]);
  const [selectedPharma, setSelectedPharma] = useState<any[]>([]);
  const [selectedPharmaRoles, setSelectedPharmaRoles] = useState<any[]>([]);
  const [selectedPresident, setSelectedPresident] = useState<any[]>([]);
  const [presidentType, setPresidentType] = useState<TypePresidentCible | null>(null);
  const [selectedLabo, setSelectedLabo] = useState<any[]>([]);
  const [selectedPartenaire, setSelectedPartenaire] = useState<any[]>([]);
  const [selectedRoles, setSelectedRoles] = useState<any[]>([]);
  const [generateAction, setGenerateAction] = useState<boolean>(false);
  const [selectedParent, setSelectedParent] = useState<any[]>([]);

  const client = useApolloClient();

  const {
    values,
    setValues,
    handleChange,
    handleChangeLaboratoireAutocomplete,
    handleChangeProjectAutocomplete,
    handleChangeActionDescription,
    handleChangeActionDueDate,
  } = useActualiteForm();

  const {
    dateDebut,
    dateFin,
    codeOrigine,
    idLaboratoire,
    fichierCible,
    idProject,
    actionDescription,
    actionDueDate,
    actionPriorite,
    niveauPriorite,
    libelle,
    description,
  } = values;

  // Create presigned url for files
  const [doCreatePutPresignedUrl, doCreatePutPresignedUrlResult] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      // const allFilesFormatedName = allFiles.map(file => file && formatFilename(file))
      // On Complete presigned url, Upload to S3
      if (data && data.createPutPresignedUrls) {
        Promise.all(
          data.createPutPresignedUrls.map(async (presignedFile, index) => {
            // Upload to s3
            if (
              presignedFile &&
              allFiles &&
              allFiles.length > 0 &&
              allFiles.map(file => presignedFile.filePath.includes(formatFilenameWithoutDate(file)))
            ) {
              // Set fichier presentation chemin and upload to s3
              if (presignedFile && fichierPresentations && fichierPresentations.length > 0) {
                fichierPresentations.map(item => {
                  if (presignedFile.filePath.includes(formatFilenameWithoutDate(item))) {
                    const newFile: FichierInput = {
                      type: TypeFichier.PDF,
                      nomOriginal: item.name,
                      chemin: presignedFile.filePath,
                    };
                    setFichierPresentationsWithChemin(prevState => {
                      if (
                        !prevState.files.map(item => item && item.chemin).includes(newFile.chemin)
                      ) {
                        prevState.files = [...prevState.files, newFile];
                      }
                      return prevState;
                    });
                  }
                });
              }

              // Set fichier cible chemin
              if (
                presignedFile &&
                fichiersCibles.map(file =>
                  presignedFile.filePath.includes(formatFilenameWithoutDate(file)),
                )
              ) {
                fichiersCibleChemin = presignedFile.filePath;
              }

              await uploadToS3(allFiles[index], presignedFile.presignedUrl)
                .then(result => {
                  if (result && result.status === 200) {
                    uploadResult = result;
                  }
                })
                .catch(error => {
                  setLoading(false);
                  console.log(error);
                  const snackBarData: SnackVariableInterface = {
                    type: 'ERROR',
                    message:
                      "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
                    isOpen: true,
                  };
                  displaySnackBar(client, snackBarData);
                });
            }
          }),
        ).then(_ => {
          // Create actualite
          if (
            dateDebut &&
            dateFin &&
            codeOrigine &&
            allFiles &&
            allFiles.length > 0 &&
            fichierPresentations &&
            fichierPresentations.length > 0 &&
            fichierPresentationsWithChemin.files &&
            uploadResult &&
            uploadResult.status === 200 &&
            (fichierPresentationsWithChemin.files.length === fichierPresentations.length ||
              (isOnEdit &&
                fichierPresentationsWithChemin.files.length <= fichierPresentations.length))
          ) {
            let presentationFiles: FichierInput[] = fichierPresentationsWithChemin.files;
            const cibleFile: FichierInput | null =
              fichiersCibleChemin && fichiersCibles[0] && fichiersCibles[0].name
                ? {
                    type: TypeFichier.EXCEL,
                    nomOriginal: fichiersCibles[0] && fichiersCibles[0].name,
                    chemin: fichiersCibleChemin,
                  }
                : fichierCible
                ? fichierCible
                : null;

            // If edit
            if (isOnEdit) {
              if (values.fichierPresentations && values.fichierPresentations.length > 0) {
                presentationFiles = [
                  ...fichierPresentationsWithChemin.files,
                  ...values.fichierPresentations,
                ];
              }
            }

            doCreateUpdateActualite({
              variables: {
                ...values,
                id: values.id || null,
                codeItem: 'ACTUALITE',
                dateDebut: dateDebut.format(),
                dateFin: dateFin.format(),
                globalite: actuRadioCible === 'globalite-user' ? true : false,
                idLaboratoire,
                fichierPresentations: presentationFiles,
                fichierCible: cibleFile,
                // roles: selectedRoles,
                pharmacieRoles: {
                  pharmacies: selectedPharma.map((item: any) => item && item.id),
                  roles: selectedPharmaRoles,
                },
                presidentRegions: {
                  presidents: selectedPresident.map((item: any) => item && item.id),
                  type: presidentType ? presidentType : TypePresidentCible.PRESIDENT,
                },
                services: selectedCollabo.map((item: any) => item && item.code),
                laboratoires: selectedLabo.map((item: any) => item && item.id),
                partenaires: selectedPartenaire.map((item: any) => item && item.id),
                idProject,
                actionPriorite,
                actionDescription,
                actionDueDate,
                niveauPriorite: niveauPriorite || 1,
              },
            });
          }
        });
      }
    },
    onError: errors => {
      setLoading(false);
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message:
          "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  // Mutation  create actualite
  const [doCreateUpdateActualite, doCreateUpdateActualiteResult] = useMutation<
    CREATE_AND_UPDATE_ACTUALITE,
    CREATE_AND_UPDATE_ACTUALITEVariables
  >(DO_CREATE_AND_UPDATE_ACTUALITE, {
    onCompleted: ({ createUpdateActualite }) => {
      if (createUpdateActualite) {
        // Send notification
        let message = 'Actualité modifiée avec succès';
        if (isOnCreate) {
          message = 'Actualité créée avec succès';
        }

        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);

        setActuRadioCible('globalite-user');
        setShowSubSidebar(false);
        setClickedCible(null);
        setGenerateAction(false);
        setLoading(false);

        if (state && state.idLaboratoire) {
          push(`/laboratoires/${state.idLaboratoire}/${'actualites'}`, {
            goBack: true,
          });
        } else {
          push('/actualites');
        }
      }
    },
    onError: errors => {
      setLoading(false);
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  const [doSendNotificationActualiteToCible] = useMutation<
    SEND_NOTIFICATION_ACTUALITE_TO_CIBLE,
    SEND_NOTIFICATION_ACTUALITE_TO_CIBLEVariables
  >(DO_SEND_NOTIFICATION_ACTUALITE_TO_CIBLE, {
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: "Erreur lors de l'envoye de(s) notification(s)",
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  /**
   * Set fichier cible on update
   */
  useEffect(() => {
    if (isOnEdit && values && values.fichierCible) {
      const fakeFileCible: File = { name: values.fichierCible.nomOriginal } as any;
      setFichiersCibles([fakeFileCible]);
    }
  }, [values, isOnEdit]);

  const submitCreate = () => {
    // Start loading
    setLoading(true);
    if (dateDebut && dateFin && codeOrigine && allFiles && allFiles.length > 0) {
      // Presigned file
      const filePathsTab: string[] = [];
      allFiles.map((file: File) => {
        filePathsTab.push(
          `actualites/${groupement && groupement.id}/${formatFilenameWithoutDate(file)}`,
        );
      });
      // Create presigned url
      doCreatePutPresignedUrl({
        variables: { filePaths: filePathsTab },
      });
    }

    // Edit actualite
    if (isOnEdit && allFiles.length === 0) {
      if (dateDebut && dateFin && codeOrigine && fichierPresentations) {
        doCreateUpdateActualite({
          variables: {
            ...values,
            id: values.id || null,
            codeItem: 'ACTUALITE',
            dateDebut: dateDebut.format(),
            dateFin: dateFin.format(),
            globalite: actuRadioCible === 'globalite-user' ? true : false,
            idLaboratoire,
            fichierPresentations: values.fichierPresentations || [],
            fichierCible: fichierCible ? fichierCible : null,
            // roles: selectedRoles,
            pharmacieRoles: {
              pharmacies: selectedPharma.map((item: any) => item && item.id),
              roles: selectedPharmaRoles,
            },
            presidentRegions: {
              presidents: selectedPresident.map((item: any) => item && item.id),
              type: presidentType ? presidentType : TypePresidentCible.PRESIDENT,
            },
            services: selectedCollabo.map((item: any) => item && item.code),
            laboratoires: selectedLabo.map((item: any) => item && item.id),
            partenaires: selectedPartenaire.map((item: any) => item && item.id),
            idProject,
            actionPriorite,
            actionDescription,
            actionDueDate,
            niveauPriorite: niveauPriorite || 1,
          },
        });
      }
    }
  };

  const finalStep = {
    buttonLabel: 'Confirmer mes sélections',
    action: submitCreate,
  };

  const steps: Step[] = [
    {
      title: "Décrire l'actualité",
      pageTitle: isOnEdit ? 'Modifier une actualité' : 'Créer une actualité',
      content: (
        <ActualiteForm
          loading={
            loading ||
            doCreatePutPresignedUrlResult.loading ||
            doCreateUpdateActualiteResult.loading
          }
          selectedFiles={fichierPresentations}
          setSelectedFiles={setFichierPresentations}
          actuRadioCible={actuRadioCible}
          setActuRadioCible={setActuRadioCible}
          values={values}
          setValues={setValues}
          handleChange={handleChange}
          handleChangeLaboratoireAutocomplete={handleChangeLaboratoireAutocomplete}
          handleChangeProjectAutocomplete={handleChangeProjectAutocomplete}
          handleChangeActionDescription={handleChangeActionDescription}
          handleChangeActionDueDate={handleChangeActionDueDate}
          generateAction={generateAction}
          setGenerateAction={setGenerateAction}
          selectedCollabo={selectedCollabo}
          setSelectedCollabo={setSelectedCollabo}
          selectedPharma={selectedPharma}
          setSelectedPharma={setSelectedPharma}
          selectedPharmaRoles={selectedPharmaRoles}
          setSelectedPharmaRoles={setSelectedPharmaRoles}
          selectedPresident={selectedPresident}
          setSelectedPresident={setSelectedPresident}
          presidentType={presidentType}
          setPresidentType={setPresidentType}
          selectedLabo={selectedLabo}
          setSelectedLabo={setSelectedLabo}
          selectedPartenaire={selectedPartenaire}
          setSelectedPartenaire={setSelectedPartenaire}
          selectedRoles={selectedRoles}
          setSelectedRoles={setSelectedRoles}
          selectedParent={selectedParent}
          setSelectedParent={setSelectedParent}
          showSelectableCible={showSelectableCible}
          setShowSelectableCible={setShowSelectableCible}
        />
      ),
    },
    {
      title: 'Choix des cibles',
      pageTitle: isOnEdit ? 'Modifier les cibles' : 'Choisir les cibles',
      content: (
        <ChoixCible
          fichiersCibles={fichiersCibles}
          setFichiersCibles={setFichiersCibles}
          actuRadioCible={actuRadioCible}
          setActuRadioCible={setActuRadioCible}
          showSubSidebar={showSubSidebar}
          setShowSubSidebar={setShowSubSidebar}
          showSelectableCible={showSelectableCible}
          setShowSelectableCible={setShowSelectableCible}
          clickedCible={clickedCible}
          setClickedCible={setClickedCible}
          selectedCollabo={selectedCollabo}
          setSelectedCollabo={setSelectedCollabo}
          selectedPharma={selectedPharma}
          setSelectedPharma={setSelectedPharma}
          selectedPharmaRoles={selectedPharmaRoles}
          setSelectedPharmaRoles={setSelectedPharmaRoles}
          selectedPresident={selectedPresident}
          setSelectedPresident={setSelectedPresident}
          presidentType={presidentType}
          setPresidentType={setPresidentType}
          selectedLabo={selectedLabo}
          setSelectedLabo={setSelectedLabo}
          selectedPartenaire={selectedPartenaire}
          setSelectedPartenaire={setSelectedPartenaire}
          selectedRoles={selectedRoles}
          setSelectedRoles={setSelectedRoles}
          selectedParent={selectedParent}
          setSelectedParent={setSelectedParent}
        />
      ),
    },
  ];

  const clickBack = () => {
    if (state && state.idLaboratoire) {
      push(`/laboratoires/${state.idLaboratoire}/${'actualites'}`, {
        goBack: true,
      });
    } else {
      push('/actualites');
    }
  };

  // SET ALL FILES
  useEffect(() => {
    const realPresentationFiles = fichierPresentations.filter(
      file => file && file.name && file.size,
    );
    const realCibleFiles = fichiersCibles.filter(file => file && file.name && file.size);
    if (realPresentationFiles && realPresentationFiles.length > 0) {
      setAllFiles(realPresentationFiles.concat(realCibleFiles));
    }
  }, [fichierPresentations, fichiersCibles]);

  /**
   * Initialize fichier cible
   */
  useEffect(() => {
    if (actuRadioCible) {
      switch (actuRadioCible) {
        case 'globalite-user':
          setFichiersCibles([]);
          break;
        case 'selection-cible':
          setFichiersCibles([]);
          break;
        case 'fichier-cible':
          break;
        default:
          break;
      }
    }
  }, [actuRadioCible]);

  // Disable btn next
  const disableBtnNext = (): boolean => {
    if (
      dateDebut === null ||
      dateFin === null ||
      codeOrigine === '' ||
      libelle === '' ||
      description === ''
    ) {
      return true;
    }

    if (codeOrigine === 'LABO' && idLaboratoire === null) {
      return true;
    }

    if (fichierPresentations.length === 0) {
      return true;
    }

    if (actuRadioCible === 'fichier-cible' && fichiersCibles.length === 0 && activeStep === 1) {
      return true;
    }

    if (
      actuRadioCible === 'selection-cible' &&
      selectedRoles.length === 0 &&
      selectedCollabo.length === 0 &&
      selectedPharma.length === 0 &&
      selectedPresident.length === 0 &&
      selectedLabo.length === 0 &&
      selectedPartenaire.length === 0 &&
      activeStep === 1
    ) {
      return true;
    }

    if (
      actuRadioCible === 'selection-cible' &&
      activeStep === 1 &&
      ((selectedPharma.length > 0 && selectedPharmaRoles.length === 0) ||
        (selectedPharma.length === 0 && selectedPharmaRoles.length > 0))
    ) {
      return true;
    }

    return false;
  };

  /**
   * Désactiver ou activer le bouton suivant si on change le formulaire de génération d'action
   */
  const [formsIsInvalid, setFormsIsInvalid] = useState<boolean>(false);

  useEffect(() => {
    const isInvalid =
      !idProject ||
      !actionPriorite ||
      !actionDueDate ||
      !actionDescription ||
      (actionDescription && strippedString(actionDescription).length === 0);

    generateAction
      ? isInvalid
        ? setFormsIsInvalid(true)
        : setFormsIsInvalid(false)
      : isOnEdit && idProject && isInvalid
      ? setFormsIsInvalid(true)
      : setFormsIsInvalid(false);
  }, [generateAction, values, isOnEdit]);

  return (
    <div className={classes.createActuRoot}>
      {activeStep === 1 && actuRadioCible === 'selection-cible' && (
        <div className={classes.leftSidebarConainer}>
          <LeftSidebar
            clickedCible={clickedCible}
            setClickedCible={setClickedCible}
            showSubSidebar={showSubSidebar}
            setShowSubSidebar={setShowSubSidebar}
            selectedCollabo={selectedCollabo}
            setSelectedCollabo={setSelectedCollabo}
            selectedPharma={selectedPharma}
            setSelectedPharma={setSelectedPharma}
            selectedPharmaRoles={selectedPharmaRoles}
            setSelectedPharmaRoles={setSelectedPharmaRoles}
            selectedPresident={selectedPresident}
            setSelectedPresident={setSelectedPresident}
            presidentType={presidentType}
            setPresidentType={setPresidentType}
            selectedLabo={selectedLabo}
            setSelectedLabo={setSelectedLabo}
            selectedPartenaire={selectedPartenaire}
            setSelectedPartenaire={setSelectedPartenaire}
            selectedRoles={selectedRoles}
            setSelectedRoles={setSelectedRoles}
            selectedParent={selectedParent}
            setSelectedParent={setSelectedParent}
          />
        </div>
      )}
      <Stepper
        key="create_actualite_key"
        title="Creation d'actualité"
        steps={steps}
        backToHome={clickBack}
        disableNextBtn={disableBtnNext() || formsIsInvalid}
        // disableNextBtn={false}
        finalStep={finalStep}
        fullWidth={false}
        finished={false}
        resume={
          <Resume
            codeServices={selectedCollabo.map((item: any) => item && item.code)}
            idPartenaires={selectedPartenaire.map((item: any) => item && item.id)}
            idLaboratoires={selectedLabo.map((item: any) => item && item.id)}
            codeRoles={selectedRoles}
            pharmacieRoles={{
              pharmacies: selectedPharma.map((item: any) => item && item.id),
              roles: selectedPharmaRoles,
            }}
            presidentRegions={{
              presidents: selectedPresident.map((item: any) => item && item.id),
              type: presidentType ? presidentType : TypePresidentCible.PRESIDENT,
            }}
            fichierCible={fichiersCibles[0]}
            actuRadioCible={actuRadioCible}
            setActuRadioCible={setActuRadioCible}
            showSubSidebar={showSubSidebar}
            setShowSubSidebar={setShowSubSidebar}
            showSelectableCible={showSelectableCible}
            setShowSelectableCible={setShowSelectableCible}
            clickedCible={clickedCible}
            setClickedCible={setClickedCible}
            selectedCollabo={selectedCollabo}
            setSelectedCollabo={setSelectedCollabo}
            selectedPharma={selectedPharma}
            setSelectedPharma={setSelectedPharma}
            selectedPharmaRoles={selectedPharmaRoles}
            setSelectedPharmaRoles={setSelectedPharmaRoles}
            selectedPresident={selectedPresident}
            setSelectedPresident={setSelectedPresident}
            presidentType={presidentType}
            setPresidentType={setPresidentType}
            selectedLabo={selectedLabo}
            setSelectedLabo={setSelectedLabo}
            selectedPartenaire={selectedPartenaire}
            setSelectedPartenaire={setSelectedPartenaire}
            selectedRoles={selectedRoles}
            setSelectedRoles={setSelectedRoles}
            selectedParent={selectedParent}
            setSelectedParent={setSelectedParent}
          />
        }
        onConfirm={submitCreate}
        loading={
          loading || doCreatePutPresignedUrlResult.loading || doCreateUpdateActualiteResult.loading
        }
        where="inCreateActualite"
        activeStepFromProps={activeStep}
        setActiveStepFromProps={setActiveStep}
      />
      <Backdrop open={loading} value="Validation en cours, veuillez patienter..." />
    </div>
  );
};

export default withRouter(CreateActualite);
