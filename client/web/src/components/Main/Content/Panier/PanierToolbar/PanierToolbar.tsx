import React, { FC, useState, useEffect } from 'react';
import { Typography, Toolbar, Button } from '@material-ui/core';
import { useStyles } from '../styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import LocationCityIcon from '@material-ui/icons/LocationCity';
import PlaceIcon from '@material-ui/icons/Place';
import CallIcon from '@material-ui/icons/Call';
import LocalGroceryStoreIcon from '@material-ui/icons/LocalGroceryStore';
import { getUser, getGroupement } from '../../../../../services/LocalStorage';
import { useQuery, useApolloClient } from '@apollo/react-hooks';
import ChangeParmacieModal from '../ChangeParmacieModal/ChangeParmacieModal';
import { GET_CURRENT_PHARMACIE } from '../../../../../graphql/Pharmacie/local';
import { MY_PHARMACIE_me_pharmacie } from '../../../../../graphql/Pharmacie/types/MY_PHARMACIE';
import ICurrentPharmacieInterface from '../../../../../Interface/CurrentPharmacieInterface';
import { SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT } from '../../../../../Constant/roles';

interface PanierToolbarProps {
  choosePharmacie: boolean;
}

const PanierToolbar: FC<PanierToolbarProps & RouteComponentProps<any, any, any>> = ({
  location,
  history,
  choosePharmacie,
}) => {
  const classes = useStyles({});
  const user = getUser();
  const groupement = getGroupement();
  const userRole = user.role.code ? user.role.code : null;
  const client = useApolloClient();

  const pathName = location.pathname;

  // take currentPharmacie
  const [currentPharmacie, setCurrentPharmacie] = useState<MY_PHARMACIE_me_pharmacie | null>(null);
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

  useEffect(() => {
    setCurrentPharmacie(
      myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie
        ? myPharmacie.data.pharmacie
        : null,
    );
  }, [myPharmacie]);

  const [openModifierDialog, setOpenModifierDialog] = useState<boolean>(false);

  const openDialog = (value: boolean) => () => {
    setOpenModifierDialog(value);
    history.push(value ? pathName + '/editOptions' : cutPath(pathName));
  };

  const cutPath = (pathName: string) => {
    const newPath = pathName.replace('/editOptions', '');
    return newPath;
  };

  useEffect(() => {
    setOpenModifierDialog(choosePharmacie);
  }, [choosePharmacie]);

  return (
    <Toolbar className={classes.toolbar}>
      <div className={classes.toolbarItem}>
        <LocationCityIcon />
        <Typography className={classes.toolbarText}>
          {currentPharmacie ? currentPharmacie.nom : ''}
        </Typography>
      </div>
      <div className={classes.toolbarItem}>
        <PlaceIcon />
        <Typography className={classes.toolbarText}>
          {currentPharmacie ? currentPharmacie.ville : ''}
        </Typography>
      </div>
      <div className={classes.toolbarItem}>
        <LocalGroceryStoreIcon />
        <Typography className={classes.toolbarText}>Plateforme</Typography>
      </div>
      {(userRole === SUPER_ADMINISTRATEUR || userRole === ADMINISTRATEUR_GROUPEMENT) && (
        <div className={classes.toolbarItem}>
          <Button className={classes.toolbarButton} onClick={openDialog(true)}>
            modifier
          </Button>
        </div>
      )}
      {openModifierDialog && (
        <div className={classes.toolbarItem}>
          <ChangeParmacieModal
            openDialog={openDialog}
            open={true}
            setOpen={setOpenModifierDialog}
          />
        </div>
      )}
    </Toolbar>
  );
};

export default withRouter(PanierToolbar);
