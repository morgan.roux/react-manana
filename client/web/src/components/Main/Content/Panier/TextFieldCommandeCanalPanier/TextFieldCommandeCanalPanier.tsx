import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Box from '@material-ui/core/Box';
import React, { useState, FC } from 'react';

import { commandeCanals } from '../../../../../graphql/CommandeCanal/types/commandeCanals';
import { useStyles } from './styles';

interface TextFieldPanierProps {
  currencies: commandeCanals | undefined;
  label: string;
  value: string;
  handleChange: (event: any) => void;
}

export const TextFieldCommandeCanalPanier: FC<TextFieldPanierProps> = ({
  handleChange,
  currencies,
  label,
  value,
}) => {
  const classes = useStyles({});

  return (
    <Box className={classes.form}>
      <TextField
        select={true}
        label={label}
        variant="outlined"
        value={value}
        onChange={handleChange}
        fullWidth={true}
      >
        {currencies &&
          currencies.commandeCanals &&
          currencies.commandeCanals.map(option => (
            <MenuItem key={(option && option.code) || ''} value={(option && option.code) || ''}>
              {option && option.libelle}
            </MenuItem>
          ))}
      </TextField>
    </Box>
  );
};
