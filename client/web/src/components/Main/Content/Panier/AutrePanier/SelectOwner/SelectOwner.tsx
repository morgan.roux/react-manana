import React, { FC, useState } from 'react';
import { FormControl, MenuItem, Select } from '@material-ui/core';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { getPharmaciePanier_pharmaciePanier } from '../../../../../../graphql/Panier/types/getPharmaciePanier';

interface SelectOwnerProps {
  autrePanier: Array<getPharmaciePanier_pharmaciePanier | null> | null;
  handleSelectedOwner: (id: string) => void;
}

const SelectOwner: FC<SelectOwnerProps & RouteComponentProps<any, any, any>> = ({
  autrePanier,
  handleSelectedOwner,
}) => {
  const [selectedOwner, setSelectedOwner] = useState<string | null>('0');
  const onOwnerChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    const id = event.target.value as string;
    setSelectedOwner(id);
    handleSelectedOwner(id);
  };
  return (
    <FormControl variant="outlined" fullWidth={true}>
      <Select
        value={selectedOwner}
        disableUnderline={true}
        onChange={onOwnerChange}
        fullWidth={true}
        MenuProps={{
          getContentAnchorEl: null,
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'left',
          },
        }}
      >
        <MenuItem value={'0'}>Tout le monde</MenuItem>
        {autrePanier &&
          autrePanier.map((ligne: getPharmaciePanier_pharmaciePanier | null) => (
            <MenuItem key={ligne ? ligne.id : ''} value={ligne && ligne.id ? ligne.id : undefined}>
              {ligne && ligne.owner && ligne.owner.userName ? ligne.owner.userName : ''}
            </MenuItem>
          ))}
      </Select>
    </FormControl>
  );
};

export default withRouter(SelectOwner);
