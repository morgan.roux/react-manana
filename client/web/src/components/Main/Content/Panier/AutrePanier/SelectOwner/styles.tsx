import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    pinkBackground: {
      // background: theme.palette.pink.main,
    },
    greenBackground: { backgroundColor: '#8CC63F' },
    btnAddPanier: {
      textTransform: 'lowercase',

      color: '#ffffff',
      fontFamily: 'Montserrat',
      maxWidth: 120,
      fontSize: '0.75rem',
      lineHeight: 1,
      '&:hover': {
        backgroundColor: '#8CC63F',
        opacity: 0.9,
      },
    },
  }),
);
