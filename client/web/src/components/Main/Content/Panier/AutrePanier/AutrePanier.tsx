import React, { FC, useState, useEffect, Fragment } from 'react';
import { Box, CircularProgress } from '@material-ui/core';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import Article from '../Article/Article';
import PanierHeader from '../PanierHeader/PanierHeader';
import { MY_PHARMACIE_me_pharmacie } from '../../../../../graphql/Pharmacie/types/MY_PHARMACIE';
import { useQuery, useApolloClient, useMutation } from '@apollo/react-hooks';
import ICurrentPharmacieInterface from '../../../../../Interface/CurrentPharmacieInterface';
import { Panier } from '../../../../../graphql/Panier/types/Panier';
import { GET_CURRENT_PHARMACIE } from '../../../../../graphql/Pharmacie/local';
import { GET_PHARMACIE_PANIER, GET_MY_PANIER } from '../../../../../graphql/Panier/query';
import SelectOwner from './SelectOwner/SelectOwner';
import {
  getPharmaciePanier,
  getPharmaciePanierVariables,
  getPharmaciePanier_pharmaciePanier,
  getPharmaciePanier_pharmaciePanier_panierLignes,
} from '../../../../../graphql/Panier/types/getPharmaciePanier';
import { getUser, getGroupement } from '../../../../../services/LocalStorage';
import {
  myPanier,
  myPanierVariables,
  myPanier_myPanier,
} from '../../../../../graphql/Panier/types/myPanier';
import { GET_SMYLEYS } from '../../../../../graphql/Smyley/query';
import { SMYLEYS, SMYLEYSVariables } from '../../../../../graphql/Smyley/types/SMYLEYS';
import {
  CREATE_GET_PRESIGNED_URL,
  CREATE_GET_PRESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_GET_PRESIGNED_URL';
import { DO_CREATE_GET_PRESIGNED_URL } from '../../../../../graphql/S3';

const AutrePanier: FC<RouteComponentProps<any, any, any>> = ({ history }) => {
  const classes = useStyles({});
  const user = getUser();
  const client = useApolloClient();

  // take currentPharmacie
  const [currentPharmacie, setCurrentPharmacie] = useState<MY_PHARMACIE_me_pharmacie | null>(null);
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

  useEffect(() => {
    if (myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie) {
      setCurrentPharmacie(myPharmacie.data.pharmacie);
    }
  }, [myPharmacie]);

  // take currentPanier
  const [currentPanier, setCurrentPanier] = useState<Panier | null>(null);

  // take panier list
  const panierList = useQuery<getPharmaciePanier, getPharmaciePanierVariables>(
    GET_PHARMACIE_PANIER,
    {
      variables: { idPharmacie: currentPharmacie ? currentPharmacie.id : '', take: 10, skip: 0 },
      fetchPolicy: 'cache-and-network',
    },
  );

  // myPanier initialisation
  const myPanierQuery = useQuery<myPanier, myPanierVariables>(GET_MY_PANIER, {
    variables: { idPharmacie: currentPharmacie ? currentPharmacie.id : '' },
    fetchPolicy: 'cache-and-network',
  });

  const updateCurrentPanier = (panier: myPanier_myPanier | null) => {
    client.writeData({
      data: { panier },
    });
  };

  useEffect(() => {
    const myPanierResult =
      myPanierQuery && myPanierQuery.data && myPanierQuery.data.myPanier
        ? myPanierQuery.data.myPanier
        : null;
    updateCurrentPanier(myPanierResult);
    setCurrentPanier(myPanierResult);
  }, [myPanierQuery]);

  const getAllLignes = () => {
    let lignes: Array<getPharmaciePanier_pharmaciePanier_panierLignes | null> = [];
    if (panierList && panierList.data && panierList.data.pharmaciePanier) {
      panierList.data.pharmaciePanier
        .filter(panier => panier && panier.owner && panier.owner.id !== user.id)
        .map((panier: getPharmaciePanier_pharmaciePanier | null) => {
          if (panier && panier.panierLignes) lignes = lignes.concat(panier.panierLignes);
        });
      return lignes;
    } else return lignes;
  };

  const [
    allLignes,
    setAllLignes,
  ] = useState<Array<getPharmaciePanier_pharmaciePanier_panierLignes | null> | null>(
    getAllLignes(),
  );

  const [
    selectedLignes,
    setSelectedLignes,
  ] = useState<Array<getPharmaciePanier_pharmaciePanier_panierLignes | null> | null>(allLignes);

  const getOneLignes = (id: string) => {
    if (panierList && panierList.data && panierList.data.pharmaciePanier) {
      const result = panierList.data.pharmaciePanier.find(
        (panier: getPharmaciePanier_pharmaciePanier | null) => panier && panier.id === id,
      );
      if (result && result.panierLignes) setSelectedLignes(result.panierLignes);
    } else return null;
  };

  useEffect(() => {
    const allLignes = getAllLignes();
    setAllLignes(allLignes);
    setSelectedLignes(allLignes);
  }, [panierList]);

  const handleSelectedOwner = (id: string) => {
    if (id === '0') {
      setSelectedLignes(allLignes);
    } else getOneLignes(id);
  };

  const groupement = getGroupement();
  // Get smyleys by groupement
  const getSmyleys = useQuery<SMYLEYS, SMYLEYSVariables>(GET_SMYLEYS, {
    variables: {
      idGroupement: groupement && groupement.id,
    },
  });

  const [doCreateGetPresignedUrls, doCreateGetPresignedUrlsResult] = useMutation<
    CREATE_GET_PRESIGNED_URL,
    CREATE_GET_PRESIGNED_URLVariables
  >(DO_CREATE_GET_PRESIGNED_URL);

  const smyleys = getSmyleys && getSmyleys.data && getSmyleys.data.smyleys;
  const presignedUrls =
    doCreateGetPresignedUrlsResult &&
    doCreateGetPresignedUrlsResult.data &&
    doCreateGetPresignedUrlsResult.data.createGetPresignedUrls;

  useEffect(() => {
    const filePaths: string[] = [];
    if (smyleys) {
      smyleys.map((smyley: any) => {
        if (smyley) filePaths.push(smyley.photo);
      });
      if (filePaths.length > 0) {
        // create presigned
        doCreateGetPresignedUrls({ variables: { filePaths } });
      }
    }
  }, [getSmyleys.data]);
  return (
    <Fragment>
      {/*<PanierToolbar choosePharmacie={false} />*/}
      <PanierHeader
        currentPanier={currentPanier}
        autrePanier={
          panierList && panierList.data && panierList.data.pharmaciePanier
            ? panierList.data.pharmaciePanier
            : null
        }
      />
      <div className={classes.container}>
        {(myPanierQuery && myPanierQuery.loading) || (panierList && panierList.loading) ? (
          <Box width="100%" height="100%" justifyContent="center" alignItems="center">
            <CircularProgress />
          </Box>
        ) : (
          <div>
            <div className={classes.form}>
              <SelectOwner
                autrePanier={
                  panierList && panierList.data && panierList.data.pharmaciePanier
                    ? panierList.data.pharmaciePanier.filter(
                        panier => panier && panier.owner && panier.owner.id !== user.id,
                      )
                    : null
                }
                handleSelectedOwner={handleSelectedOwner}
              />
            </div>
            <div className={classes.productList}>
              {selectedLignes &&
                selectedLignes.map(
                  ligne =>
                    ligne &&
                    ligne.produitCanal &&
                    !ligne.produitCanal.inMyPanier && (
                      <Article
                        key={ligne.id}
                        currentPharmacie={currentPharmacie}
                        currentPanier={currentPanier}
                        currentCanalArticle={ligne.produitCanal}
                        presignedUrls={presignedUrls}
                        smyleys={smyleys}
                      />
                    ),
                )}
            </div>
          </div>
        )}
      </div>
    </Fragment>
  );
};

export default withRouter(AutrePanier);
