import Panier from './Panier';
import Article from './Article/Article';
import ChangeParmacieModal from './ChangeParmacieModal/ChangeParmacieModal';
import PanierValidation from './PanierResume/PanierResume';
import PanierToolbar from './PanierToolbar/PanierToolbar';
import AutrePanier from './AutrePanier/AutrePanier';
import Recapitulatif from './Recapitulatif/Recapitulatif';
import ProduitCard from './ProduitCard/ProduitCard';
export {
  Article,
  PanierValidation,
  PanierToolbar,
  AutrePanier,
  ChangeParmacieModal,
  Recapitulatif,
  ProduitCard,
};

export default Panier;
