import React, { FC, useState, useEffect } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import classnames from 'classnames';
import { Dialog, DialogContent, Box, Typography } from '@material-ui/core';
import { useQuery, useLazyQuery } from '@apollo/react-hooks';
import CloseIcon from '@material-ui/icons/Close';
import {
  ProduitCanal,
  ProduitCanal_remises,
} from '../../../../../graphql/ProduitCanal/types/ProduitCanal';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CloseCircleIcon from '@material-ui/icons/Cancel';
import {
  articleSamePanachees,
  articleSamePanacheesVariables,
} from '../../../../../graphql/Panier/types/articleSamePanachees';
import { Article_Same_Panachees_articleSamePanachees } from '../../../../../graphql/ProduitCanal/types/Article_Same_Panachees';
import { GET_ARTICLE_SAME_PANACHEES, GET_MY_PANIERS } from '../../../../../graphql/Panier/query';
import { Loader } from '../../../../Dashboard/Content/Loader';
import { Panier } from '../../../../../graphql/Panier/types/Panier';
import { Panier_PanierLignes_panierLignes } from '../../../../../graphql/Panier/types/Panier_PanierLignes';
import Quantite from '../Article/Quantite/Quantite';
import {
  SEARCH as SEARCH_Interface,
  SEARCHVariables,
} from '../../../../../graphql/search/types/SEARCH';
import { SEARCH } from '../../../../../graphql/search/query';
import { BlockTwoTone } from '@material-ui/icons';
import ICurrentPharmacieInterface from '../../../../../Interface/CurrentPharmacieInterface';
import { GET_CURRENT_PHARMACIE } from '../../../../../graphql/Pharmacie/local';
import { myPaniers, myPaniersVariables } from '../../../../../graphql/Panier/types/myPaniers';
import { GET_OPERATION_PANIER } from '../../../../../graphql/OperationCommerciale/local';
import { PharmacieMinimInfo } from '../../../../../graphql/Pharmacie/types/PharmacieMinimInfo';
import Backdrop from '../../../../Common/Backdrop';

interface RemisePalierProps {
  handleCloseModal: () => void;
  article: ProduitCanal | null;
  acceptZero?: boolean;
  match: {
    params: {
      idOperation: string | undefined;
      idRemise: string | undefined;
    };
  };
  isView?: boolean;
}

const RemisePalier: FC<RemisePalierProps & RouteComponentProps<any, any, any>> = ({
  handleCloseModal,
  article,
  match,
  isView,
}) => {
  const classes = useStyles({});
  const [isOpen, setIsOpen] = useState<boolean>(true);
  const [loadingRemise, setLoadingRemise] = useState<boolean>(true);
  const remises =
    article && article.remises
      ? article.remises.sort((a, b) => (a?.quantiteMin || 0) - (b?.quantiteMin || 0))
      : null;
  const idOperation = match && match.params && match.params.idOperation;
  const idRemise = match && match.params && match.params.idRemise;

  const operationPanierResult = useQuery(GET_OPERATION_PANIER);
  const operationPanier =
    operationPanierResult &&
    operationPanierResult.data &&
    operationPanierResult.data.operationPanier;

  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const myPanierResult = useQuery<myPaniers, myPaniersVariables>(GET_MY_PANIERS, {
    variables: {
      idPharmacie:
        (myPharmacie &&
          myPharmacie.data &&
          myPharmacie.data.pharmacie &&
          myPharmacie.data.pharmacie.id) ||
        '',
      take: 10,
      skip: 0,
    },
    //skip: listResult && listResult.data && !listResult.data.search,
  });

  // currentPharmacie
  const currentPharmacie: PharmacieMinimInfo | null =
    (myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie) || null;

  // currentPanier
  const currentPanier: Panier | null = operationPanier
    ? operationPanier
    : ({
        panierLignes:
          (myPanierResult &&
            myPanierResult.data &&
            myPanierResult.data.myPaniers &&
            myPanierResult.data.myPaniers
              .map(panier => panier && panier.panierLignes)
              .reduce((total, value) => {
                return total && total.concat(value);
              }, [])) ||
          [],
      } as Panier) || null;

  const articleSamePanachees = useQuery<articleSamePanachees, articleSamePanacheesVariables>(
    GET_ARTICLE_SAME_PANACHEES,
    {
      variables: {
        id: article ? article.id : '',
        idPharmacie: currentPharmacie ? currentPharmacie.id : '',
      },
      skip: idRemise,
    },
  );

  const [articleSamePanacheesList, setArticleSamePanacheesList] = useState<any>(null);
  // query state
  const [queryState, setQueryState] = useState<any>(null);

  // query for comparing active produit
  const [getProduits, listProduit] = useLazyQuery<SEARCH_Interface, SEARCHVariables>(SEARCH, {
    variables: {
      type: ['produitcanal'],
      query: queryState ? queryState : null,
      idPharmacie: currentPharmacie ? currentPharmacie.id : '',
    },
  });

  // qte total panachees state
  const [totalPanachees, setTotalPanachees] = useState<number>(0);

  // produit active
  useEffect(() => {
    if (
      articleSamePanachees &&
      articleSamePanachees.data &&
      articleSamePanachees.data.produitCanal &&
      articleSamePanachees.data.produitCanal.articleSamePanachees
    ) {
      const ids = articleSamePanachees.data.produitCanal.articleSamePanachees
        .map(article => (article ? article.id : null))
        .filter(id => id);

      const query = {
        query: {
          constant_score: {
            filter: {
              bool: {
                must: [
                  {
                    terms: {
                      _id: ids,
                    },
                  },
                  {
                    term: {
                      isActive: true,
                    },
                  },
                ],
              },
            },
          },
        },
      };
      setQueryState(query);
      getProduits();
      setLoadingRemise(false);
    } else if (articleSamePanachees && !articleSamePanachees.loading) {
      setLoadingRemise(false);
    }
  }, [articleSamePanachees]);

  useEffect(() => {
    if (
      listProduit &&
      listProduit.data &&
      listProduit.data.search &&
      listProduit.data.search.data
    ) {
      const produitListArticle = listProduit.data.search.data;
      setArticleSamePanacheesList(produitListArticle);
      if (currentPanier && currentPanier.panierLignes) {
        const ids = produitListArticle
          .map((article: any) => (article && article.id ? article.id : null))
          .filter(id => id);
        const result = currentPanier.panierLignes.filter(ligne =>
          ids.includes(
            ligne && ligne.produitCanal && ligne.produitCanal.id ? ligne.produitCanal.id : '',
          ),
        );

        let quantite = 0;
        result.map(ligne => (ligne && ligne.quantite ? (quantite += ligne.quantite) : null));

        setTotalPanachees(quantite);
      }
    }
  }, [listProduit, currentPanier]);

  const getPanierLigneItem = (article: Article_Same_Panachees_articleSamePanachees | null) => {
    const panier = idOperation ? operationPanier : currentPanier;
    if (article && panier && panier.panierLignes) {
      const currentLigne = panier.panierLignes.find(
        (ligne: Panier_PanierLignes_panierLignes | null) =>
          ligne && ligne.produitCanal && ligne.produitCanal.id === article.id,
      );
      return currentLigne ? currentLigne : null;
    } else return null;
  };

  const getPrixHT = (article: Article_Same_Panachees_articleSamePanachees | null) => {
    const ligne = getPanierLigneItem(article);
    return ligne && ligne.prixBaseHT
      ? ligne.prixBaseHT
      : article && article.prixPhv
      ? article.prixPhv
      : 0;
  };

  const getRemise = (article: Article_Same_Panachees_articleSamePanachees | null) => {
    const ligne = getPanierLigneItem(article);
    return checkRemise(ligne && ligne.quantite ? ligne.quantite : 0, article);
  };

  const getPrixNet = (article: Article_Same_Panachees_articleSamePanachees | null) => {
    const ligne = getPanierLigneItem(article);
    const prixHT = getPrixHT(article);
    const pourcentage = getPourcentage(
      ligne && ligne.quantite ? ligne.quantite : article && article.stv ? article.stv : 0,
      article,
    );

    return ligne && ligne.prixNetUnitaireHT
      ? parseFloat(ligne.prixNetUnitaireHT.toFixed(2))
      : parseFloat((prixHT - (prixHT * pourcentage) / 100).toFixed(2));
  };

  const getTotalNet = (article: Article_Same_Panachees_articleSamePanachees | null) => {
    const ligne = getPanierLigneItem(article);
    const prixNet = getPrixNet(article);
    return ligne && ligne.prixTotalHT
      ? ligne.prixTotalHT
      : article && article.stv
      ? parseFloat((article.stv * prixNet).toFixed(2))
      : 0;
  };

  const getPourcentage = (
    quantite: number,
    article: Article_Same_Panachees_articleSamePanachees | null,
  ) => {
    const remises = article && article.remises ? article.remises : [];
    let pourcentage = 0;
    remises.map((remise: ProduitCanal_remises | null) => {
      if (
        remise &&
        remise.quantiteMin &&
        remise.quantiteMax &&
        quantite >= remise.quantiteMin &&
        quantite <= remise.quantiteMax
      ) {
        pourcentage = remise.pourcentageRemise ? remise.pourcentageRemise : 0;
      }
    });
    return pourcentage;
  };
  const checkRemise = (quantite: number, article: any) => {
    const panier = idOperation ? operationPanier : currentPanier;
    const remises = (article && article.remises ? article.remises : []).sort(
      (a: any, b: any) => a.quantiteMin - b.quantiteMin,
    );
    const minimalRemise = remises && remises[0];
    let pourcentage = 0;
    let qteMin = 0;
    let qteRemisePanachee = 0;

    if (article && article.articleSamePanachees) {
      article.articleSamePanachees.map(article => {
        if (panier && panier.panierLignes) {
          panier.panierLignes.map(ligne => {
            if (
              article &&
              ligne &&
              ligne.quantite &&
              ligne.produitCanal &&
              ligne.produitCanal.id &&
              ligne.produitCanal.id === article.id
            ) {
              qteRemisePanachee += ligne.quantite;
            }
          });
        }
      });
    }
    if (qteRemisePanachee === 0) {
      qteRemisePanachee = quantite;
    }
    remises.map((remise: ProduitCanal_remises | null) => {
      if (
        remise &&
        remise.quantiteMin &&
        remise.quantiteMax &&
        quantite > 0 &&
        qteRemisePanachee >= remise.quantiteMin &&
        qteRemisePanachee <= remise.quantiteMax
      ) {
        pourcentage = remise.pourcentageRemise ? remise.pourcentageRemise : 0;
        qteMin = remise.quantiteMin;
      }
    });

    const result =
      pourcentage > 0
        ? `${pourcentage}%`
        : minimalRemise
        ? `${minimalRemise && minimalRemise.pourcentageRemise}% à partir de ${minimalRemise &&
            minimalRemise.quantiteMin} achetés`
        : '0%';
    return result;
  };

  const qteMin =
    remises &&
    remises
      .map((remise: ProduitCanal_remises | null) =>
        remise && remise.quantiteMin ? remise.quantiteMin : 0,
      )
      .shift();
  const closeModal = (e: any) => {
    e.stopPropagation();
    handleCloseModal();
  };

  const checkIfInOC = (article: ProduitCanal) => {
    if (idOperation) {
      return article &&
        article.operations &&
        article.operations.some(e => e && e.id === idOperation)
        ? true
        : false;
    } else {
      return true;
    }
  };

  const loading =
    (articleSamePanachees && articleSamePanachees.loading) ||
    (listProduit && listProduit.loading) ||
    loadingRemise
      ? true
      : false;

  console.log('remises', idRemise, remises);

  return (
    <Dialog open={isOpen} fullWidth={true} maxWidth="lg" onClick={e => e.stopPropagation()}>
      {loading && <Backdrop />}
      <Box className={classes.navBar}>
        <Typography className={classes.navBarTitle}>Remise</Typography>
        <CloseIcon onClick={closeModal} className={classes.cursorPointer} />
      </Box>
      <DialogContent
        style={{
          background:
            (articleSamePanachees && articleSamePanachees.loading) ||
            (listProduit && listProduit.loading)
              ? 'rgba(0,0,0,0.5)'
              : 'white',
        }}
      >
        {
          <Box>
            <Box className={classes.rowTypo}>
              <Typography className={classes.defalutFont}>
                <span className={classes.pink}>
                  Remise{' '}
                  {articleSamePanachees &&
                  articleSamePanachees.data &&
                  articleSamePanachees.data.produitCanal &&
                  articleSamePanachees.data.produitCanal.articleSamePanachees &&
                  articleSamePanachees.data.produitCanal.articleSamePanachees.length > 0
                    ? 'Panachée'
                    : 'Palier'}
                </span>{' '}
                {/*à partir du 03/05/2020*/}
              </Typography>
            </Box>
            <Box className={classes.rowTypo} marginBottom="16px" marginTop="8px">
              <Box>
                {remises &&
                  remises.map((remise: ProduitCanal_remises | null) => (
                    <Box key={remise ? remise.id : 0}>
                      De{' '}
                      <span className={classes.defalutFont}>{remise ? remise.quantiteMin : 0}</span>{' '}
                      unités à{' '}
                      <span className={classes.defalutFont}>{remise ? remise.quantiteMax : 0}</span>{' '}
                      unités ,{' '}
                      <span className={classes.defalutFont}>
                        {remise ? remise.pourcentageRemise : 0}%
                      </span>{' '}
                      de remise{' '}
                      {remise && remise.nombreUg
                        ? remise.nombreUg === 0
                          ? 'sans'
                          : remise.nombreUg
                        : 0}{' '}
                      UG
                    </Box>
                  ))}
              </Box>
              <Box>
                {articleSamePanachees &&
                  articleSamePanachees.data &&
                  articleSamePanachees.data.produitCanal &&
                  articleSamePanachees.data.produitCanal.articleSamePanachees &&
                  articleSamePanachees.data.produitCanal.articleSamePanachees.length > 0 && (
                    <Box display="flex" justifyContent="flex-end" textAlign="right">
                      <Typography className={classes.defalutFont}>
                        Quantité totale actuelle de votre remise panachée :{' '}
                      </Typography>
                      <Typography className={classnames(classes.pink, classes.defalutFont)}>
                        {totalPanachees}
                      </Typography>
                    </Box>
                  )}
                {qteMin && (
                  <Box textAlign="right" className={classes.defalutFont}>
                    {qteMin} quantité(s) minimum
                  </Box>
                )}
              </Box>
            </Box>
            <Box paddingBottom="12px" borderBottom="1px solid #E5E5E5">
              <table className={classes.table}>
                <thead className={classes.tableHead}>
                  <tr className={classes.tableHeadRow}>
                    <th className={classnames(classes.textLeft, classes.th)}>Code</th>
                    <th className={classnames(classes.px10, classes.th)} style={{ minWidth: 135 }}>
                      Libellé
                    </th>
                    <th className={classnames(classes.th)}>Labo</th>
                    <th className={classnames(classes.th)}>STV</th>
                    <th className={classnames(classes.th)}>Carton</th>
                    <th className={classnames(classes.th)}>Stock plateforme</th>
                    <th className={classnames(classes.px10, classes.th)}>Stock pharmacie</th>
                    {!isView && <th className={classnames(classes.th)}>Quantité commandée</th>}
                    <th className={classnames(classes.th)} style={{ minWidth: 92 }}>
                      Remises
                    </th>
                    <th style={{ minWidth: 76 }} className={classnames(classes.px10, classes.th)}>
                      Tarif HT
                    </th>
                    {!isView && (
                      <th className={classnames(classes.th)} style={{ minWidth: 54 }}>
                        Prix Net
                      </th>
                    )}
                    {!isView && (
                      <th style={{ minWidth: 83 }} className={classnames(classes.px10, classes.th)}>
                        Unités gratuits
                      </th>
                    )}
                  </tr>
                </thead>
                <tbody className={classes.tableBody}>
                  {((articleSamePanachees &&
                    articleSamePanachees.data &&
                    articleSamePanachees.data.produitCanal &&
                    articleSamePanachees.data.produitCanal.articleSamePanachees &&
                    articleSamePanachees.data.produitCanal.articleSamePanachees.length === 0) ||
                    idRemise) && (
                    <tr>
                      <td>
                        {article && article.produit && article.produit.produitCode
                          ? article.produit.produitCode.code
                          : ''}
                      </td>
                      <td className={classnames(classes.textCenter, classes.px10)}>
                        {article && article.produit ? article.produit.libelle : ''}
                      </td>
                      <td className={classnames(classes.textCenter)}>
                        {article &&
                        article.produit &&
                        article.produit.produitTechReg &&
                        article.produit.produitTechReg.laboExploitant
                          ? article.produit.produitTechReg.laboExploitant.nomLabo
                          : ''}
                      </td>
                      <td className={classnames(classes.textCenter)}>{article && article.stv}</td>
                      <td className={classnames(classes.textCenter)}>-</td>
                      <td className={classnames(classes.textCenter, classes.p10)}>
                        {article &&
                        article.qteStock &&
                        article.stv &&
                        article.qteStock > article.stv ? (
                          <CheckCircleIcon className={classes.quantiteButton} />
                        ) : (
                          <CloseCircleIcon className={classes.pink} />
                        )}
                      </td>
                      <td className={classnames(classes.textCenter, classes.p10)}>
                        {article ? article.qteStock : 0}
                      </td>
                      {!isView && (
                        <td className={classnames(classes.textCenter)}>
                          <Quantite currentCanalArticle={article} acceptZero={true} />
                        </td>
                      )}
                      <td className={classnames(classes.textCenter, classes.pink)}>
                        {getRemise(article)}
                      </td>
                      <td className={classnames(classes.textCenter, classes.p10)}>
                        {getPrixHT(article)}€
                      </td>
                      {!isView && (
                        <td className={classnames(classes.textCenter, classes.p10)}>
                          {getPrixNet(article)}€
                        </td>
                      )}
                      {!isView && (
                        <td className={classnames(classes.textCenter, classes.p10)}>-</td>
                      )}
                    </tr>
                  )}

                  {articleSamePanacheesList &&
                    articleSamePanacheesList.map((article: any | null) => (
                      <tr key={article && article.id ? article.id : ''}>
                        <td>
                          {article && article.produit && article.produit.produitCode
                            ? article.produit.produitCode.code
                            : ''}
                        </td>
                        <td className={classnames(classes.textCenter, classes.px10)}>
                          {article && article.produit ? article.produit.libelle : ''}
                        </td>
                        <td className={classnames(classes.textCenter)}>
                          {article &&
                          article.produit &&
                          article.produit.produitTechReg &&
                          article.produit.produitTechReg.laboExploitant
                            ? article.produit.produitTechReg.laboExploitant.nomLabo
                            : ''}
                        </td>
                        <td className={classnames(classes.textCenter)}>{article && article.stv}</td>
                        <td className={classnames(classes.textCenter)}>-</td>
                        <td className={classnames(classes.textCenter, classes.p10)}>
                          {article &&
                          article.qteStock &&
                          article.stv &&
                          article.qteStock > article.stv ? (
                            <CheckCircleIcon className={classes.secondary} />
                          ) : (
                            <BlockTwoTone className={classes.primary} />
                          )}
                        </td>
                        <td className={classnames(classes.textCenter, classes.p10)}>
                          {article ? article.qteStock : 0}
                        </td>
                        {!isView && (
                          <td className={classes.textCenter}>
                            {idOperation && checkIfInOC(article) ? (
                              <Quantite currentCanalArticle={article} acceptZero={true} />
                            ) : !idOperation ? (
                              <Quantite currentCanalArticle={article} acceptZero={true} />
                            ) : (
                              'Hors OC'
                            )}
                          </td>
                        )}
                        <td className={classnames(classes.textCenter, classes.pink)}>
                          {getRemise(article)}
                        </td>
                        <td className={classnames(classes.textCenter, classes.p10)}>
                          {getPrixHT(article)}€
                        </td>
                        {!isView && (
                          <td className={classnames(classes.textCenter, classes.p10)}>
                            {getPrixNet(article)}€
                          </td>
                        )}
                        {!isView && (
                          <td className={classnames(classes.textCenter, classes.p10)}>-</td>
                        )}
                      </tr>
                    ))}
                </tbody>
              </table>
            </Box>
          </Box>
        }
      </DialogContent>
    </Dialog>
  );
};

export default withRouter(RemisePalier);
