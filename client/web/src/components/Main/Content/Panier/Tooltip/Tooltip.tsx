import React, { FC, Fragment } from 'react';
import { HtmlTooltip, useStyles } from './styles';
import { Button } from '@material-ui/core';
import classnames from 'classnames';
interface TooltipComponentProps {
  component: any;
  famille: string;
  titleLabel: string;
}

const TooltipComponent: FC<TooltipComponentProps> = ({ component, titleLabel, famille }) => {
  const classes = useStyles({});
  return (
    <HtmlTooltip title={<Fragment>{component}</Fragment>}>
      <div className={classes.container}>
        <div className={classnames(classes.RobotoBold, classes.title)}>{titleLabel}</div>
        <div className={classnames(classes.RobotoBold, classes.title)}>{famille}</div>
      </div>
    </HtmlTooltip>
  );
};

export default TooltipComponent;
