import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    /* TOOLBAR */

    /* PANIER LISTE */

    panierListeItem: {
      /*width: '23%',*/
      width: '342px',
      display: 'flex',
      flexDirection: 'column',
      padding: '10px 12px 14px',
      margin: '0 0 24px 40px',
      '@media (max-width: 1200px)': {
        width: '43%',
        minWidth: 300,
      },
      '@media (max-width: 768px)': {
        width: '48%',
      },
      '@media (max-width: 599px)': {
        width: 320,
      },
      '@media (max-width: 375px)': {
        width: 290,
      },
    },

    borderUserAction: {
      borderBottom: '1px solid #E5E5E5',
      borderTop: '1px solid #E5E5E5',
    },
    rowTypo: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },

    panierListePrixContainer: {
      marginTop: '16px',
      display: 'flex',
      flexDirection: 'row',
    },
    panierListePrixLeft: {
      flex: 1,
      textAlign: 'left',
      fontFamily: 'Montserrat',
      fontWeight: 'normal',
      fontSize: '12px',
      color: '#878787',
    },
    panierListePrixRight: {
      flex: 1,
      textAlign: 'right',
      fontFamily: 'Montserrat',
      fontWeight: 'normal',
      fontSize: '12px',
    },
    panierListeButtonLeft: {
      background: '#EFEFEF',
      textTransform: 'uppercase',
      fontFamily: 'Montserrat',
      fontSize: '0.75rem',
      marginRight: theme.spacing(1),
      minWidth: 116,
    },
    contentRS: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginTop: '12px',
      marginLeft: '-12px',
      marginRight: '-12px',
      paddingLeft: '12px',
      paddingRight: '12px',
      boxShadow: '0px 0px 4px 0px rgba(0,0,0,0.25)',
      height: '40px',
    },
    btnAddPanier: {
      textTransform: 'none',
      backgroundColor: '#8CC63F',
      color: '#ffffff',
      fontFamily: 'Montserrat',
      maxWidth: 120,
      fontSize: '0.75rem',
      lineHeight: 1,
      '&:hover': {
        backgroundColor: '#8CC63F',
        opacity: 0.9,
      },
    },
    stockIcon: {
      width: '20px',
      height: '20px',
    },

    /* FONT STYLE */
    MontserratRegular: {
      fontFamily: 'Montserrat',
      fontWeight: 400,
    },
    MontserratBold: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
    },
    RobotoRegular: {
      fontFamily: 'Roboto',
      fontWeight: 'normal',
    },
    RobotoBold: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
    },
    prixHt: {
      fontSize: '20',
    },

    underlined: {
      textDecoration: 'underline',
    },

    /*FONT SIZE */
    small: {
      // fontSize: theme.typography.small.fontSize,
    },
    medium: {
      // fontSize: theme.typography.medium.fontSize,
    },
    big: {
      // fontSize: theme.typography.big.fontSize,
    },

    /* COLORS*/
    colorDefault: {
      // color: theme.palette.default.main,
    },

    pink: {
      // color: theme.palette.pink.main,
    },
    darkLight: {
      // color: theme.palette.darklight.main,
    },
    /* COLORS */
    gray: {
      // color: theme.palette.gray.main,
    },
    pinkBackground: {
      // background: theme.palette.pink.main,
    },
    green: {
      // color: theme.palette.green.light,
    },
    white: {
      // color: theme.palette.white.main,
    },
    yellowBackground: {
      // background: theme.palette.yellow.main,
      borderRadius: '50%',
    },
    orangeBackground: {
      // background: theme.palette.orange.main,
      borderRadius: '50%',
    },
    dark: {
      color: '#1D1D1D',
    },

    /* FLEX */
    flex: {
      flex: 1,
    },
    flexColumn: {
      display: 'flex',
      flexDirection: 'column',
    },
    flexRow: {
      display: 'flex',
      flexDirection: 'row',
    },
    flexRowReverse: {
      display: 'flex',
      flexDirection: 'row-reverse',
    },
    inline: {
      display: 'inline',
    },
    panierListeQuantite: {
      border: '1px solid',
      // borderColor: theme.palette.gray.main,
      display: 'flex',
      flexDirection: 'row',
      borderRadius: '4px',
    },
    quantiteButton: {
      // color: theme.palette.green.main,
    },

    blueButton: {
      color: '#004354',
    },

    quantiteTextField: {
      borderLeft: '1px solid',
      // borderLeftColor: theme.palette.gray.main,
      borderRight: '1px solid',
      // borderRightColor: theme.palette.gray.main,
      width: '48px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& > div > input': {
        textAlign: 'center',
        fontSize: '0.875rem',
      },
    },
    quantiteSaveButton: {
      borderLeft: '1px solid',
      // borderLeftColor: theme.palette.gray.main,
    },
    underline: {
      '& > *': {
        fontFamily: 'Roboto',
        fontWeight: 'bold',
      },
      '&&&:before': {
        borderBottom: 'none',
      },
      '&&:after': {
        borderBottom: 'none',
      },
    },
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
    content: {
      display: 'flex',
      justifyContent: 'center',
    },
    contained: {
      display: 'flex',
      flexDirection: 'column',
      fontSize: '15px',
      marginTop: '15px',
    },
  }),
);
