import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    btnDelete: {
      textTransform: 'uppercase',
      fontFamily: 'Roboto',
      maxWidth: 154,
      minWidth: 124,
      fontSize: 14,
      maxHeight: 36,
      fontWeight: 'bold',
    },
  }),
);
