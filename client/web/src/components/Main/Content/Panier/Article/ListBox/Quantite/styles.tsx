import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    rowTypo: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginBottom: 8,
    },
    MontserratRegular: {
      fontFamily: 'Montserrat',
      fontWeight: 400,
    },
    RobotoMedium: {
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 14,
    },
    pink: {
      // color: theme.palette.text.primary,
    },
    small: {
      // fontSize: theme.typography.small.fontSize,
    },
    gray: {
      color: '#616161',
    },
    medium: {
      // fontSize: theme.typography.medium.fontSize,
    },
    colorDefault: {
      // color: theme.palette.text.primary,
    },
  }),
);
