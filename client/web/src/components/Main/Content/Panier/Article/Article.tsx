import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { Box, Paper, Typography } from '@material-ui/core';
import classnames from 'classnames';
import React, { FC, Fragment, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import NoImage from '../../../../../assets/img/product_no_image.jpg';
import { Panier } from '../../../../../graphql/Panier/types/Panier';
import { PharmacieMinimInfo } from '../../../../../graphql/Pharmacie/types/PharmacieMinimInfo';
import { DELETE_CANAL_ARTICLE } from '../../../../../graphql/ProduitCanal/mutation';
import { ProduitCanal } from '../../../../../graphql/ProduitCanal/types/ProduitCanal';
import {
  softDeleteCanalArticle,
  softDeleteCanalArticleVariables,
} from '../../../../../graphql/ProduitCanal/types/softDeleteCanalArticle';
import { SEARCH } from '../../../../../graphql/search/query';
import {
  SEARCH as SEARCH_INTERFACE,
  SEARCHVariables,
} from '../../../../../graphql/search/types/SEARCH';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { AppAuthorization } from '../../../../../services/authorization';
import { getUser } from '../../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { ConfirmDeleteDialog } from '../../../../Common/ConfirmDialog';
import CustomButton from '../../../../Common/CustomButton';
import CustomFullScreenModal from '../../../../Common/CustomModal/CustomFullScreenModal';
import { ProduitDetails } from '../../Produits/ProduitDetails';
import TooltipComponent from '../Tooltip';
import ArticleButton from './Button/Button';
import ArticleHistorique from './Historique/Historique';
import ArticleImage from './Image/Image';
import InfoContainer from './InfoContainer';
import ArticleListBox from './ListBox/ListBox';
import { useStyles } from './styles';
interface ArticleProps {
  currentPharmacie: PharmacieMinimInfo | null;
  currentPanier: Panier | null;
  currentCanalArticle: ProduitCanal | null | any;
  presignedUrls?: any;
  smyleys?: any;
  qteMin?: number;
  listResult?: any;
  familleResult?: any;
}

interface TooltipContentProps {
  data: any;
  familleResult?: any;
}

export const TarifHTComponent = props => {
  const { currentCanalArticle } = props;
  const classes = useStyles({});
  return (
    <Box display="flex" flexDirection="row">
      <Typography className={classnames(classes.RobotoBold, classes.prixHt)}>
        {(currentCanalArticle && currentCanalArticle.prixPhv) || 0}
      </Typography>
      <Typography className={classnames(classes.RobotoRegular, classes.prixHt)}>€ HT</Typography>
    </Box>
  );
};

export const TooltipContent: FC<TooltipContentProps> = ({ data, familleResult }) => {
  const classes = useStyles({});
  const [familles, setFamilles] = useState<any>(null);
  /* const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const currentPharmacie = myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie; */

  const arborescenceCodeFamille = (codeFamille: string[]): string[] => {
    let letter: string = '';
    const famille: string[] = [];
    codeFamille.map((fam, index) => {
      letter += fam;
      famille[index] = letter;
    });
    return famille;
  };

  const arborescence = (codeFamille: string): string | null => {
    if (codeFamille && familles) {
      const objet = familles.find((data: any) => data.codeFamille === codeFamille);
      return (objet && objet.libelleFamille) || 'Famille non définie';
    }

    return null;
  };

  useEffect(() => {
    if (
      familleResult &&
      familleResult.data &&
      familleResult.data.search &&
      familleResult.data.search
    ) {
      setFamilles(familleResult.data.search.data);
    }
  }, [familleResult]);

  return (
    <div className={classes.paper}>
      {data ? (
        <>
          <h2>Famille</h2>
          <div className={classes.content}>
            <div>
              <div className={classes.contained}>
                {data && data.produit && data.produit.famille
                  ? arborescenceCodeFamille(Array.from(data.produit.famille.codeFamille)).map(
                      (letter, index) => {
                        return (
                          <div key={`${letter}-${index}`}>
                            <div>{arborescence(letter)}</div>
                            {index !== data.produit.famille.codeFamille.length - 1 ? (
                              <div>&dArr;</div>
                            ) : null}
                          </div>
                        );
                      },
                    )
                  : null}
              </div>
            </div>
          </div>
        </>
      ) : null}
    </div>
  );
};

const Article: FC<ArticleProps & RouteComponentProps<any, any, any>> = ({
  history,
  location,
  currentPanier,
  currentPharmacie,
  currentCanalArticle,
  qteMin,
  listResult,
  familleResult,
}) => {
  const classes = useStyles({});
  const article = currentCanalArticle;
  const client = useApolloClient();
  const nomCanalArticle =
    article && article.produit && article.produit.libelle ? article.produit.libelle : '';
  const isInGestionProduits = location.pathname.includes('/gestion-produits');
  const [openConfirmDialog, setOpenConfirmDialog] = useState(false);
  const [openModalDetail, setOpenModalDetail] = useState(false);

  const user = getUser();
  const auth = new AppAuthorization(user);

  const goToDetails = () => {
    if (article) {
      setOpenModalDetail(true);
      // history.push(`/catalogue-produits/card/${article.id}`, { from: location.pathname });
    }
  };

  const handleEdit = (canalArticle: ProduitCanal) => (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ) => {
    e.stopPropagation();
    history.push(
      `/gestion-produits/create/${canalArticle && canalArticle.produit && canalArticle.produit.id}`,
    );
  };

  const handleOpenConfirmDialog = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.stopPropagation();
    setOpenConfirmDialog(true);
  };

  const [doDeleteArticle, doDeleteArticleResult] = useMutation<
    softDeleteCanalArticle,
    softDeleteCanalArticleVariables
  >(DELETE_CANAL_ARTICLE, {
    update: (cache, { data }) => {
      if (
        listResult &&
        listResult.variables &&
        data &&
        data.softDeleteCanalArticle &&
        data.softDeleteCanalArticle.id
      ) {
        const req = cache.readQuery<SEARCH_INTERFACE, SEARCHVariables>({
          query: SEARCH,
          variables: listResult.variables,
        });
        if (req && req.search && req.search.data) {
          cache.writeQuery({
            query: SEARCH,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.filter(
                    (item: any) =>
                      item &&
                      item.id &&
                      data.softDeleteCanalArticle &&
                      item.id !== data.softDeleteCanalArticle.id,
                  ),
                },
              },
            },
            variables: listResult && listResult.variables,
          });
        }
      }
    },
    onCompleted: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'SUCCESS',
        message: 'Article supprimé avec succès',
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const handleDelete = (canalArticle: ProduitCanal) => (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ) => {
    if (canalArticle && canalArticle.id) {
      doDeleteArticle({ variables: { id: canalArticle.id } });
    }

    e.stopPropagation();
    setOpenConfirmDialog(false);
  };

  return (
    <Fragment>
      <Paper className={classes.panierListeItem}>
        <ArticleImage
          src={
            currentCanalArticle &&
            currentCanalArticle.produit &&
            currentCanalArticle.produit.produitPhoto
              ? currentCanalArticle.produit.produitPhoto.fichier.publicUrl
              : NoImage
          }
        />
        <TarifHTComponent currentCanalArticle={currentCanalArticle} />
        <TooltipComponent
          titleLabel={nomCanalArticle}
          famille={
            currentCanalArticle &&
            currentCanalArticle.produit &&
            currentCanalArticle.produit.famille &&
            currentCanalArticle.produit.famille.libelleFamille
          }
          component={<TooltipContent data={currentCanalArticle} familleResult={familleResult} />}
        />
        <div onClick={goToDetails} style={{ cursor: 'pointer' }}>
          <InfoContainer canalArticle={currentCanalArticle} />
        </div>
        {!isInGestionProduits && (
          <ArticleListBox
            currentPanier={currentPanier}
            currentCanalArticle={currentCanalArticle}
            currentPharmacie={currentPharmacie}
            qteMin={qteMin}
            goToDetails={goToDetails}
          />
        )}
        <Box>
          <Box display="flex" flexDirection="row" marginTop="16px" justifyContent="space-between">
            <ArticleHistorique
              produit={currentCanalArticle && currentCanalArticle.produit}
              pharmacie={currentPharmacie}
            />
            {isInGestionProduits ? (
              <Box width="100%" marginLeft="8px" marginBottom="16px">
                <CustomButton
                  id={currentCanalArticle.id}
                  name={currentCanalArticle.nomCanalArticle}
                  fullWidth={true}
                  onClick={handleOpenConfirmDialog}
                  disabled={!auth.isAuthorizedToDeleteProduct()}
                >
                  Supprimer
                </CustomButton>
              </Box>
            ) : (
              <ArticleButton currentCanalArticle={currentCanalArticle} />
            )}
          </Box>
          {isInGestionProduits && (
            <CustomButton
              fullWidth={true}
              color="secondary"
              onClick={handleEdit(currentCanalArticle)}
              disabled={!auth.isAuthorizedToEditProduct()}
            >
              Modifier
            </CustomButton>
          )}

          <ConfirmDeleteDialog
            title={'Suppression'}
            content="Êtes vous sur de vouloir supprimer ce produit"
            open={openConfirmDialog}
            setOpen={setOpenConfirmDialog}
            onClickConfirm={handleDelete(currentCanalArticle)}
          />
        </Box>
      </Paper>
      {openModalDetail && (
        <CustomFullScreenModal
          open={openModalDetail}
          setOpen={setOpenModalDetail}
          title={`Produit`}
          withBtnsActions={false}
          fullScreen={true}
          style={{ margin: '30px !important' }}
        >
          <ProduitDetails idProduct={article.id} />
        </CustomFullScreenModal>
      )}
    </Fragment>
  );
};

export default withRouter(Article);
