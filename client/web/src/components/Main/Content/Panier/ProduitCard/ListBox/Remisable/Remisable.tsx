import React, { FC, useState, useEffect, Fragment } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import classnames from 'classnames';
import { Typography, Box } from '@material-ui/core';
import { Panier } from '../../../../../../../graphql/Panier/types/Panier';
import { Panier_PanierLignes_panierLignes } from '../../../../../../../graphql/Panier/types/Panier_PanierLignes';
import {
  ProduitCanal,
  ProduitCanal_remises,
} from '../../../../../../../graphql/ProduitCanal/types/ProduitCanal';
import RemisePalier from '../../../RemisePalier/RemisePalier';
import { useQuery } from '@apollo/react-hooks';
import { GET_OPERATION_PANIER } from '../../../../../../../graphql/OperationCommerciale/local';
import { PharmacieMinimInfo } from '../../../../../../../graphql/Pharmacie/types/PharmacieMinimInfo';

interface ArticleRemisableProps {
  currentCanalArticle: ProduitCanal | null;
  currentPanier: Panier | null;
  currentPharmacie: PharmacieMinimInfo | null;
  match: {
    params: {
      idOperation: string | undefined;
    };
  };
}

const ArticleRemisable: FC<ArticleRemisableProps & RouteComponentProps<any, any, any>> = ({
  currentCanalArticle,
  currentPanier,
  // currentPharmacie,
  match,
}) => {
  const classes = useStyles({});
  const idOperation = match && match.params && match.params.idOperation;
  const [prixNet, setPrixNet] = useState<number>(0);
  const [totalNet, setTotalNet] = useState<number>(0);
  const [uG, setUG] = useState<number>(0);

  const operationPanierResult = useQuery(GET_OPERATION_PANIER);
  const operationPanier =
    operationPanierResult &&
    operationPanierResult.data &&
    operationPanierResult.data.operationPanier;

  const getPanierLigneItem = () => {
    const panier = idOperation ? operationPanier : currentPanier;
    if (currentCanalArticle && panier && panier.panierLignes) {
      const currentLigne = panier.panierLignes.find(
        (ligne: Panier_PanierLignes_panierLignes | null) =>
          ligne && ligne.produitCanal && ligne.produitCanal.id === currentCanalArticle.id,
      );
      return currentLigne ? currentLigne : null;
    } else return null;
  };

  const handleCloseModal = () => {
    if (isRemiseOpen) setIsRemiseOpen(false);
  };

  const ligne = getPanierLigneItem();
  const [currentRemise, setCurrentRemise] = useState<string>('');
  const article = ligne && ligne.produitCanal ? ligne.produitCanal : currentCanalArticle;
  const [isRemiseOpen, setIsRemiseOpen] = useState<boolean>(false);

  const checkRemise = (quantite: number) => {
    const panier = idOperation ? operationPanier : currentPanier;
    const remises = article && article.remises ? article.remises : [];
    const prixPhv = article && article.prixPhv ? article.prixPhv : 0;
    let pourcentage = 0;
    let qteMin = 0;
    let qteRemisePanachee = 0;

    if (article && article.articleSamePanachees) {
      article.articleSamePanachees.map(article => {
        if (panier && panier.panierLignes) {
          panier.panierLignes.map(ligne => {
            if (
              article &&
              ligne &&
              ligne.quantite &&
              ligne.produitCanal &&
              ligne.produitCanal.id &&
              ligne.produitCanal.id === article.id
            ) {
              qteRemisePanachee += ligne.quantite;
            }
          });
        }
      });
    }
    if (qteRemisePanachee === 0) {
      qteRemisePanachee = quantite;
    }
    const minimalRemise = remises && remises[0];
    remises.map((remise: ProduitCanal_remises | null) => {
      const pourcentageRemise = remise && remise.pourcentageRemise ? remise.pourcentageRemise : 0;
      const quantiteMin = remise && remise.quantiteMin ? remise.quantiteMin : 0;
      const uG = remise && remise.nombreUg ? remise.nombreUg : 0;
      if (
        remise &&
        remise.quantiteMin &&
        remise.quantiteMax &&
        quantite > 0 &&
        qteRemisePanachee >= remise.quantiteMin &&
        qteRemisePanachee <= remise.quantiteMax
      ) {
        pourcentage = pourcentageRemise;
        qteMin = quantiteMin;
        setUG(uG);
      }
    });
    const prixNet =
      quantite > 0 ? parseFloat((prixPhv - (prixPhv * pourcentage) / 100).toFixed(2)) : 0;
    const totalNet = parseFloat((prixNet * quantite).toFixed(2));
    setPrixNet(prixNet);
    setTotalNet(totalNet);
    const result =
      pourcentage > 0
        ? `${pourcentage}%`
        : minimalRemise
        ? `${minimalRemise && minimalRemise.pourcentageRemise}% à partir de ${minimalRemise &&
            minimalRemise.quantiteMin} achetés`
        : '0%';
    return setCurrentRemise(result);
  };

  useEffect(() => {
    const ligne = getPanierLigneItem();
    if (ligne && ligne.quantite) {
      checkRemise(ligne.quantite);
    } else {
      checkRemise(0);
    }
  }, [currentPanier, operationPanier]);

  const handleRemiseButton = (e: any) => {
    e.stopPropagation();
    setIsRemiseOpen(true);
  };
  return (
    <Fragment>
      <Box className={classes.rowTypo}>
        <Typography className={classnames(classes.RobotoMedium)}>Remise</Typography>
        <Typography
          className={classnames(
            classes.RobotoBold,
            classes.secondary,
            classes.underlined,
            classes.pointer,
          )}
          onClick={handleRemiseButton}
        >
          {currentRemise}
        </Typography>
      </Box>
      <Box className={classes.rowTypo}>
        <Typography className={classnames(classes.RobotoMedium)}>Prix Net</Typography>
        <Typography
          className={classnames(classes.RobotoBold, classes.colorDefault)}
          onClick={handleRemiseButton}
        >
          {prixNet}€
        </Typography>
      </Box>
      <Box className={classes.rowTypo}>
        <Typography className={classnames(classes.RobotoMedium)}>Total Net</Typography>
        <Typography className={classnames(classes.RobotoBold, classes.secondary)}>
          {totalNet}€
        </Typography>
      </Box>
      <Box className={classes.rowTypo}>
        <Typography className={classnames(classes.RobotoMedium)}>Unités gratuites</Typography>
        <Typography className={classnames(classes.RobotoBold)} onClick={handleRemiseButton}>
          {uG}
        </Typography>
      </Box>
      {isRemiseOpen && (
        <RemisePalier handleCloseModal={handleCloseModal} article={currentCanalArticle} />
      )}
    </Fragment>
  );
};

export default withRouter(ArticleRemisable);
