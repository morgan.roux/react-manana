import React, { FC } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import classnames from 'classnames';
import { Box, IconButton } from '@material-ui/core';
import { Panier } from '../../../../../../graphql/Panier/types/Panier';
import { ProduitCanal } from '../../../../../../graphql/ProduitCanal/types/ProduitCanal';
import ArticleRemisable from './Remisable/Remisable';
import ArticleListQuantite from './Quantite/Quantite';
import { ThumbUp, ChatBubble } from '@material-ui/icons';
import { PharmacieMinimInfo } from '../../../../../../graphql/Pharmacie/types/PharmacieMinimInfo';

interface ArticleListBoxProps {
  currentCanalArticle: ProduitCanal | null;
  currentPanier: Panier | null;
  currentPharmacie: PharmacieMinimInfo | null;
  qteMin?: number;
  match: {
    params: {
      id: string | undefined;
    };
  };
}
const ArticleListBox: FC<ArticleListBoxProps & RouteComponentProps<any, any, any>> = ({
  currentPanier,
  currentCanalArticle,
  currentPharmacie,
  qteMin,
  match,
}) => {
  const classes = useStyles({});
  const idCanalArticle = match && match.params && match.params.id;

  return (
    <Box className={classnames(classes.flex, classes.gray)}>
      <ArticleListQuantite
        currentPharmacie={currentPharmacie}
        currentCanalArticle={currentCanalArticle}
        currentPanier={currentPanier}
        label={'Quantités'}
        qteMin={qteMin}
      />
      <ArticleRemisable
        currentPanier={currentPanier}
        currentCanalArticle={currentCanalArticle}
        currentPharmacie={currentPharmacie}
      />
      {!idCanalArticle && (
        <Box display="flex" justifyContent="space-between">
          <div className={classes.root}>
            <IconButton>
              <ThumbUp />
            </IconButton>
            <div>
              {(currentCanalArticle &&
                currentCanalArticle.userSmyleys &&
                currentCanalArticle.userSmyleys.total) ||
                0}
            </div>
          </div>
          <div className={classes.root}>
            <IconButton>
              <ChatBubble />
            </IconButton>
            <div>
              {(currentCanalArticle &&
                currentCanalArticle.comments &&
                currentCanalArticle.comments.total) ||
                0}
            </div>
          </div>
        </Box>
      )}
    </Box>
  );
};

export default withRouter(ArticleListBox);
