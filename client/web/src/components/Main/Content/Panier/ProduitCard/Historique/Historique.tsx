import React, { FC, useState } from 'react';
// import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import SalesHistory from '../../../Produits/Graphs';
import { Tooltip, Fade, IconButton, Box } from '@material-ui/core';
import { History } from '@material-ui/icons';
import CustomButton from '../../../../../Common/CustomButton';
import { getUser } from '../../../../../../services/LocalStorage';
import { AppAuthorization } from '../../../../../../services/authorization';

interface ArticleHistoriqueProps {
  view?: string;
  produit: any;
  pharmacie: any;
}

const ArticleHistorique: FC<ArticleHistoriqueProps & RouteComponentProps<any, any, any>> = ({
  view,
  location,
  produit,
  pharmacie,
}) => {
  // const classes = useStyles({});
  const [{ openSalesHistory }, setOpenSalesHistory] = React.useState({ openSalesHistory: false });
  const [historyColor, setHistoryColor] = useState<string>('');
  const isInGestionProduits = location.pathname.includes('/gestion-produits');

  const user = getUser();
  const auth = new AppAuthorization(user);

  const toggleSalesHistory = (e: any) => {
    e.stopPropagation();
    setOpenSalesHistory(prevState => ({
      ...prevState,
      openSalesHistory: !prevState.openSalesHistory,
    }));
  };

  const handleSendColor = (color: string) => {
    setHistoryColor(color);
  };
  return (
    <Box width={isInGestionProduits ? '100%' : 'auto'}>
      {view === 'table' ? (
        <Tooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Historique">
          <IconButton
            onClick={toggleSalesHistory}
            disabled={!auth.isAuthorizedToViewProductSaleHistory()}
          >
            <History />
          </IconButton>
        </Tooltip>
      ) : (
        <CustomButton
          color={isInGestionProduits ? 'primary' : 'default'}
          onClick={toggleSalesHistory}
          fullWidth={isInGestionProduits ? true : false}
          disabled={!auth.isAuthorizedToViewProductSaleHistory()}
        >
          HISTORIQUE
        </CustomButton>
      )}
      <SalesHistory
        open={openSalesHistory}
        onClose={toggleSalesHistory}
        sendColor={handleSendColor}
        produit={produit}
        pharmacie={pharmacie}
      />
    </Box>
  );
};

export default withRouter(ArticleHistorique);
