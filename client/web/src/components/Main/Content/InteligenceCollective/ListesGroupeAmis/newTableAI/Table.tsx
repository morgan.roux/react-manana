import React, { FC, useState, MouseEvent } from 'react';
import TableToolbar from './TableToolbar';
import TableHead from './TableHead';
import { TableProps, ItemSelected } from './interfaces';
import { useStyles } from './styles';
import { useApolloClient, useQuery } from '@apollo/react-hooks';
import Radio from '@material-ui/core/Radio';
import TablePaginationActions from './TablePaginationActions';
import {
  TableContainer,
  Table,
  TableBody,
  TableCell,
  Checkbox,
  TableRow,
  TablePagination,
} from '@material-ui/core';
import { GET_SEARCH_TOTAL } from '../../../../../../graphql/search/query';
import { NULL_QUERY } from '../../../../../Common/newWithSearch/withSearch';
import Backdrop from '../../../../../Common/Backdrop/Backdrop';

const EnhancedTable: FC<TableProps> = ({
  listResult,
  isSelectable,
  columns,
  hidePagination,
  checkedItemsQuery,
  paginationCentered,
  selected,
  setSelected,
  onClickRow,
  activeRow,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [dense, setDense] = useState(false);
  const isInOperationCommande = window.location.hash.includes('#/operation-produits');

  const take = (listResult && listResult.variables && listResult.variables.take) || 12;
  const skip = (listResult && listResult.variables && listResult.variables.skip) || 0;

  const checkedItemsResult = useQuery(
    (checkedItemsQuery && checkedItemsQuery.query) || NULL_QUERY,
    {
      skip: !checkedItemsQuery ? true : false,
    },
  );

  const skipAllTotalQuery = (): boolean => {
    if (selected && setSelected) return false;
    if (!checkedItemsQuery || (listResult && listResult.variables && !listResult.variables.query)) {
      return true;
    }
    return false;
  };

  const allTotalResult = useQuery(GET_SEARCH_TOTAL, {
    variables: listResult && listResult.variables,
    skip: skipAllTotalQuery(),
    //fetchPolicy: 'cache-and-network',
  });

  const allTotal =
    (allTotalResult &&
      allTotalResult.data &&
      allTotalResult.data.search &&
      allTotalResult.data.search.total) ||
    0;

  const checkedItems =
    selected ||
    (checkedItemsQuery &&
      checkedItemsResult.data &&
      checkedItemsQuery &&
      checkedItemsResult.data[checkedItemsQuery.name]) ||
    [];

  const [showCheckeds, setShowChecked] = useState<boolean>(false);

  const data =
    listResult && listResult.data && listResult.data.search && listResult.data.search.data;

  const isLoading =
    (listResult && listResult.loading) || (listResult && !listResult.data) ? true : false;

  const searchIndex = (item: ItemSelected) => {
    if (checkedItems && checkedItems.length) {
      const ids = checkedItems && checkedItems.map(item => item && item.id);
      const selectedIndex = ids && ids.indexOf(item.id);
      return selectedIndex;
    }
    return -1;
  };

  const selectedIds = (selected && selected.map(item => item && item.id)) || [];

  const handleClick = (row: any, rowId: string, item: any) => (event: MouseEvent<unknown>) => {
    event.preventDefault();
    event.stopPropagation();

    const checkeds = [item.id];
    if (setSelected) {
      setSelected(checkeds);
    }
  };

  const handleChangeDense = (event: React.ChangeEvent<HTMLInputElement>) => {
    // setDense(event.target.checked);
  };

  const isSelected = (item: ItemSelected, rowId?: string) => {
    return searchIndex(item) !== -1;
  };

  const selectionLength: number = 0;
  // isSelectable && selected && handleSelection ? selected.length : -1;

  const handleClickRow = (row: any) => (event: MouseEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    if (onClickRow) {
      onClickRow(row);
    }
  };

  const clearSelection = () => {
    if (selected && setSelected) {
      setSelected([]);
    }

    if (isSelectable && checkedItemsQuery && checkedItemsQuery.name) {
      client.writeData({
        data: {
          [checkedItemsQuery.name]: null,
        },
      });
      setShowChecked(false);
    }
  };

  const updateSkipAndTake = (skip: number, take: number) => {
    client.writeData({
      data: {
        skipAndTake: {
          skip,
          take,
          __typename: 'SkipAndTake',
        },
      },
    });
  };

  const ROWS_PER_PAGE_OPTIONS = [12, 25, 50, 100, 250, 500, 1000, 2000];
  return (
    <div className={classes.root}>
      {isSelectable && (
        <TableToolbar
          checkedItems={checkedItems}
          listResult={listResult}
          clearSelection={clearSelection}
          setShowCheckeds={setShowChecked}
          showCheckeds={showCheckeds}
          allTotal={allTotal}
          checkedItemsQuery={checkedItemsQuery}
          selected={selected}
          setSelected={setSelected}
        />
      )}
      <TableContainer className={classes.tableWrapper}>
        <Table
          className={classes.table}
          aria-labelledby="tableTitle"
          size={dense ? 'small' : 'medium'}
          stickyHeader={true}
        >
          <TableHead
            checkedItemsQuery={checkedItemsQuery}
            isSelectable={isSelectable}
            checkedItems={checkedItems}
            listResult={listResult}
            columns={columns}
            selected={selected}
            setSelected={setSelected}
            allTotal={allTotal}
            isAllCheckable={false}
          />
          <TableBody>
            {data &&
              data.map((row: any, indexInArray: any) => {
                if (row) {
                  const index = row.id || indexInArray;
                  const itemDesc: ItemSelected = { ...row };
                  const isItemSelected = isSelected(itemDesc, row.id);
                  const labelId = `enhanced-table-checkbox-${index}`;
                  const isActive: boolean = activeRow && activeRow.id === row.id;

                  const selectSection = isSelectable ? (
                    <TableCell padding="checkbox" key={indexInArray}>
                      <Radio
                        onClick={handleClick(row, row.id, itemDesc)}
                        checked={selected?.includes(row.id)}
                        inputProps={{ 'aria-labelledby': labelId }}
                      />
                    </TableCell>
                  ) : null;

                  return (
                    <TableRow
                      hover={true}
                      onClick={handleClickRow(row)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={`row-${index}`}
                      selected={isItemSelected}
                      style={{ cursor: onClickRow ? 'pointer' : 'default' }}
                      className={isActive ? classes.activeRow : ''}
                    >
                      {selectSection}

                      {columns
                        ? columns.map((column, columnIndex) => {
                            return (
                              <TableCell
                                key={`row-${index}-${columnIndex}`}
                                className={classes.tableCellPadding}
                                align={column.centered ? 'center' : 'left'}
                              >
                                {column.renderer
                                  ? column.renderer(row)
                                  : row[column.name]
                                  ? row[column.name]
                                  : '-'}
                              </TableCell>
                            );
                          })
                        : null}
                    </TableRow>
                  );
                }
              })}
          </TableBody>
        </Table>
        <div>
          {isLoading ? (
            <Backdrop />
          ) : data && data.length === 0 ? (
            <div className={classes.alignCenter}>Aucun résultat correspondant</div>
          ) : (
            ''
          )}
        </div>
      </TableContainer>

      {!hidePagination && data && data.length > 0 && (
        <TablePagination
          className={paginationCentered ? classes.tablePagination : ''}
          rowsPerPageOptions={ROWS_PER_PAGE_OPTIONS}
          labelDisplayedRows={({ from, to, count }) =>
            `${from}-${to === -1 ? count : to} sur ${count !== -1 ? count : to}`
          }
          labelRowsPerPage="Nombre par page"
          component="div"
          count={
            listResult && listResult.data && listResult.data.search && listResult.data.search.total
          }
          rowsPerPage={take}
          page={skip / take}
          backIconButtonProps={{
            'aria-label': 'Page Précédente',
          }}
          nextIconButtonProps={{
            'aria-label': 'Page suivante',
          }}
          onChangePage={(_, newPage) => updateSkipAndTake(newPage * take, take)}
          onChangeRowsPerPage={e => updateSkipAndTake(skip, parseInt(e.target.value, 10))}
          ActionsComponent={TablePaginationActions}
        />
      )}

      {/* <FormControlLabel
        control={<Switch checked={dense} onChange={handleChangeDense} />}
        label="Interligne"
      /> */}
    </div>
  );
};

export default EnhancedTable;
