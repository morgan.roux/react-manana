import React, { FC } from 'react';
import useStyles from './styles';
import { useValueParameterAsBoolean } from '../../../../../../utils/getValueParameter';
import {
  GROUP_FRIEND_FILTER_NAME,
  GROUP_FRIEND_NB_MEMBER,
  GROUP_FRIEND_FILTER_NB_PARTAGE_CODE,
  GROUP_FRIEND_FILTER_NB_RECOMMANDATION,
  GROUP_FRIEND_FILTER_NB_PHARMACIE,
  SEARCH_FIELDS_OPTIONS,
} from '../constants';
import { FieldsOptions } from '../../../../../Common/CustomContent/interfaces';
import SearchFilter from '../../../../../Common/newCustomContent/SearchFilter';
interface ListesGroupeAmisFilterProps {
  fieldsState: any;
  handleFieldChange: (event: any) => void;
  handleRunSearch: (query: any) => void;
  handleResetFields: () => void;
}

const ListesGroupeAmisFilter: FC<ListesGroupeAmisFilterProps> = ({
  fieldsState,
  handleFieldChange,
  handleRunSearch,
  handleResetFields,
}) => {
  const classes = useStyles({});

  const objectState = {
    nomState: useValueParameterAsBoolean(GROUP_FRIEND_FILTER_NAME),
    nbMemberState: useValueParameterAsBoolean(GROUP_FRIEND_NB_MEMBER),
    nbPartageState: useValueParameterAsBoolean(GROUP_FRIEND_FILTER_NB_PARTAGE_CODE),
    nbRecommandationState: useValueParameterAsBoolean(GROUP_FRIEND_FILTER_NB_RECOMMANDATION),
    nbPharmacieState: useValueParameterAsBoolean(GROUP_FRIEND_FILTER_NB_PHARMACIE),
  };

  const filterSearchFields = (objectState: any, searchFields: FieldsOptions[]): FieldsOptions[] => {
    const activeStateKeys = Object.keys(objectState).filter(key => objectState[key] === true);
    const activeSearchFields = searchFields.filter((field: FieldsOptions) =>
      activeStateKeys.includes(`${field.name}State`),
    );
    return activeSearchFields;
  };

  const activeSearcFields: FieldsOptions[] = filterSearchFields(objectState, SEARCH_FIELDS_OPTIONS);

  return (
    <SearchFilter
      fieldsState={fieldsState}
      handleFieldChange={handleFieldChange}
      searchInputs={activeSearcFields}
      labelWidth={55}
      handleRunSearch={handleRunSearch}
      handleResetFields={handleResetFields}
    />
  );
};

export default ListesGroupeAmisFilter;
