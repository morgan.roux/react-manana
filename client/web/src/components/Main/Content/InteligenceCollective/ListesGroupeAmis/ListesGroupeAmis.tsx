import { RouteComponentProps, withRouter } from 'react-router';
import React, { FC, useState } from 'react';
import WithSearch from '../../../../Common/newWithSearch/withSearch';
import useStyles from './styles';
import CustomContent from './Children/CustomContent';
import { Column } from '../../../../Dashboard/Content/Interface';
import SearchInput from '../../../../Common/newCustomContent/SearchInput';
import { Box, Button, Typography } from '@material-ui/core';
import SubToolbar from '../../../../Common/newCustomContent/SubToolbar';
import { SHAREINFO_DETAILS_URL, SHAREINFO_ITEMS_CHOICE_URL } from '../../../../../Constant/url';

import moment from 'moment';
import { DO_SEARCH_GROUPES_AMIS } from '../../../../../graphql/GroupAmis';
import { getGroupement } from '../../../../../services/LocalStorage';
import { GroupementInfo } from '../../../../../graphql/Groupement/types/GroupementInfo';

interface ListeGroupeAmisProps {
  listResult?: any;
  valueBtn?: any;
  match: {
    params: {
      itemId: string | undefined;
    };
  };
}

const ListesGroupeAmis: FC<ListeGroupeAmisProps & RouteComponentProps> = ({
  history: { push },
  match: {
    params: { itemId },
  },
}) => {
  const classes = useStyles({});

  const columns: Column[] = [
    {
      name: 'nom',
      label: `Groupes d'amis`,
    },
    {
      name: 'nbPharmacie',
      label: 'Nombre de pharmacies',
    },
    {
      name: 'nbMember',
      label: 'Nombre de membres',
    },
    {
      name: 'nbPartage',
      label: 'Nombre de partages',
    },
    {
      name: 'nbRecommandation',
      label: 'Nombre de recommandations',
    },
    {
      name: 'dateCreation',
      label: 'Date de création',
      renderer: (row: any) => {
        return moment(row.dateCreation).format('DD/MM/YYYY') || '-';
      },
    },
  ];

  const [selected, setSelected] = useState(itemId ? [itemId] : []);
  const tableProps = {
    isSelectable: true,
    paginationCentered: false,
    columns,
    selected,
    setSelected,
  };
  const groupement: GroupementInfo = getGroupement();

  const must = [{ term: { 'groupement.id': groupement && groupement.id } }];
  const handleBack = () => {
    push(SHAREINFO_ITEMS_CHOICE_URL);
  };

  const handleClick = event => {
    event.stopPropagation();
    push({
      pathname: `${SHAREINFO_DETAILS_URL}/GROUPE_AMIS/comments/${selected[0]}`,
    });
  };

  return (
    <Box className={classes.container}>
      <SubToolbar
        dark={false}
        withBackBtn={true}
        onClickBack={handleBack}
        title="Partage des informations"
      >
        {selected && selected.length > 0 && (
          <Button
            variant="contained"
            color="secondary"
            className={classes.buttonValider1}
            // tslint:disable-next-line: jsx-no-lambda
            onClick={event => handleClick(event)}
            disabled={selected && selected.length < 0}
          >
            VALIDER MON CHOIX
          </Button>
        )}
      </SubToolbar>

      <Box className={classes.listRoot}>
        <Box className={classes.noPubContainer}>
          <Typography className={classes.noPubTitle}>Liste des groupes d'amis</Typography>
          <Typography className={classes.noPubTexte}>
            Choisissez un groupe d'amis parmi la liste.
          </Typography>
        </Box>
        <Box className={classes.searchInputBox}>
          <SearchInput searchPlaceholder="Rechercher un groupe" />
        </Box>
        <WithSearch
          type="groupeamis"
          props={tableProps}
          WrappedComponent={CustomContent}
          searchQuery={DO_SEARCH_GROUPES_AMIS}
          minimumShouldMatch={0}
          optionalMust={must}
        />
      </Box>
    </Box>
  );
};
export default withRouter(ListesGroupeAmis);
