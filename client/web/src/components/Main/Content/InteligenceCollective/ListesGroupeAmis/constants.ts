import { FieldsOptions } from './Children/interfaces';

export const GROUP_FRIEND_FILTER_NAME: string = '0029';
export const GROUP_FRIEND_NB_MEMBER: string = '0030';
export const GROUP_FRIEND_FILTER_NB_PARTAGE_CODE: string = '0031';
export const GROUP_FRIEND_FILTER_NB_RECOMMANDATION: string = '0033';
export const GROUP_FRIEND_FILTER_NB_PHARMACIE: string = '0034';

const placeholder = 'Tapez ici';

/**
 * Search fields
 *
 */
export const SEARCH_FIELDS_OPTIONS: FieldsOptions[] = [
  {
    name: 'nom',
    label: "Groupes d'amis",
    value: '',
    type: 'Search',
    placeholder,
    ordre: 1,
  },
  {
    name: 'nbMember',
    label: 'Nombre de membres',
    value: '',
    type: 'Search',
    placeholder,
    ordre: 2,
  },
  {
    name: 'nbPartage',
    label: 'Nombre de partages',
    value: '',
    type: 'Search',
    filterType: 'StartsWith',
    placeholder,
    ordre: 3,
  },
  {
    name: 'nbRecommandation',
    label: 'Nombre de recommandations',
    value: '',
    type: 'Search',
    filterType: 'StartsWith',
    placeholder,
    ordre: 4,
  },
  {
    name: 'nbPharmacie',
    label: 'Nombre de pharmacies',
    value: '',
    type: 'Search',
    placeholder,
    ordre: 5,
  },
];
