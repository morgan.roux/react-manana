import React, { FC, useEffect, Dispatch, SetStateAction, ReactNode, useContext } from 'react';
import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import { Theme } from '@material-ui/core/styles';
import { useApolloClient } from '@apollo/react-hooks';
import Table from '../newTableAI/Table';
import { Column } from './interfaces';
import { lighten } from '@material-ui/core';
// import { resetSearchFilters } from '../../PartageInformations/newTableAI/';
import { CheckItemsQueryInterface } from '../newTableAI/interfaces';
import { ContentContext, ContentStateInterface } from '../../../../../../AppContext';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
  root: {
    width: '100%',
    '& a': {
      cursor: 'pointer',
    },
  },
  searchBar: {
    top: 0,
    width: '100%',
    opacity: 1,
    zIndex: 10,
    position: 'sticky',
    display: 'flex',
    height: 70,
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '0px 24px 0 24px',
    background: lighten(theme.palette.primary.main, 0.1),
    '@media (max-width: 692px)': {
      flexWrap: 'wrap',
      justifyContent: 'center',
    },
  },
  search: {
    display: 'flex',
    alignItems: 'center',
    '@media (max-width: 580px)': {
      marginBottom: 15,
    },
  },
  filterButton: {
    marginLeft: theme.spacing(3),
    color: theme.palette.common.white,
  },

  filterButtonLabel: {
    fontFamily: 'Roboto',
    fontSize: 14,
    color: theme.palette.common.white,
  },

  alignCenter: {
    textAlign: 'center',
    margin: '40px 0px',
    fontSize: 20,
    fontWeight: 'bold',
  },
  formControl: {
    width: 240,
    marginRight: theme.spacing(6),
    marginBottom: theme.spacing(3),
  },
  cssOutlinedInput: {
    '&$cssFocused $notchedOutline': {
      borderColor: `${theme.palette.primary.main} !important`,
    },
  },
  notchedOutline: {
    borderWidth: '1px',
    // borderColor: `${theme.palette.primary.dark} !important`,
  },
  searchButton: {
    width: 240,
    height: 51,
    marginRight: theme.spacing(6),
  },
  resetButton: {
    width: 240,
    height: 51,
  },
});

interface CustomContentProps {
  columns?: Column[];
  paginationCentered?: boolean;
  checkedItemsQuery?: CheckItemsQueryInterface;
  listResult?: any;
  isSelectable?: boolean;
  hidePagination?: boolean;
  selected?: any[];
  setSelected?: Dispatch<SetStateAction<any[]>>;
  activeRow?: any;
  onClickRow?: (row: any) => void;
  unResetFilter?: boolean;
}

// const ROWS_PER_PAGE_OPTIONS = [12, 25, 50, 100, 250, 500, 1000, 2000];

const CustomContent: FC<CustomContentProps & WithStyles> = ({
  classes,
  columns,
  listResult,
  paginationCentered,
  isSelectable,
  hidePagination,
  checkedItemsQuery,
  selected,
  setSelected,
  activeRow,
  onClickRow,
  unResetFilter,
}) => {
  const client = useApolloClient();

  const {
    content: { page, rowsPerPage, searchPlaceholder, type },
    setContent,
  } = useContext<ContentStateInterface>(ContentContext);

  useEffect(() => {
    return () => {
      setContent({
        page,
        type,
        searchPlaceholder,
        rowsPerPage,
        contextQuery: null,
      });
      // if (!unResetFilter) resetSearchFilters(client);
    };
  }, []);

  return (
    <Table
      listResult={listResult}
      isSelectable={isSelectable}
      columns={columns}
      hidePagination={hidePagination}
      checkedItemsQuery={checkedItemsQuery}
      paginationCentered={paginationCentered}
      selected={selected}
      setSelected={setSelected}
      onClickRow={onClickRow}
      activeRow={activeRow}
    />
  );
};

export default withStyles(styles)(CustomContent);
