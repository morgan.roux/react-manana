import { FieldsOptions } from './Children/interfaces';

export const SERVICE_PARTNER_FILTER_NAME: string = '0029';
export const SERVICE_PARTNER_ADDRESS: string = '0030';
export const SERVICE_PARTNER_FILTER_CP: string = '0031';
export const SERVICE_PARTNER_FILTER_CITY: string = '0038';
export const SERVICE_PARTNER_FILTER_PHONE: string = '0039';
export const SERVICE_PARTNER_FILTER_SERVICNE_NAME: string = '0040';
export const SERVICE_PARTNER_FILTER_START_DATE: string = '0041';
export const SERVICE_PARTNER_FILTER_END_DATE: string = '0042';
export const SERVICE_PARTNER_FILTER_TYPE: string = '0043';
export const SERVICE_PARTNER_FILTER_STATE: string = '0033';

const placeholder = 'Tapez ici';

/**
 * Search fields
 *
 */
export const SEARCH_FIELDS_OPTIONS: FieldsOptions[] = [
  {
    name: 'partenaireService',
    label: 'Prestataire de service',
    value: '',
    type: 'Search',
    placeholder,
    extraNames: ['partenaireServiceSuite.nom'],
    ordre: 1,
  },
  {
    name: 'adresse',
    label: 'Adresse',
    value: '',
    type: 'Search',
    extraNames: ['laboSuite.adresse'],
    placeholder,
    ordre: 2,
  },
  {
    name: 'codePostal',
    label: 'CP',
    value: '',
    type: 'Search',
    extraNames: ['laboSuite.codePostal'],
    filterType: 'StartsWith',
    placeholder,
    ordre: 3,
  },
  {
    name: 'city',
    label: 'Ville',
    value: '',
    type: 'Search',
    extraNames: ['laboSuite.ville'],
    placeholder,
    ordre: 4,
  },
  {
    name: 'telephone',
    label: 'Téléphone',
    value: '',
    type: 'Search',
    extraNames: ['partenaireServiceSuite.telephone'],
    placeholder,
    ordre: 5,
  },
  {
    name: 'dateDebut',
    label: 'Date début de Partenariat',
    value: '',
    type: 'Search',
    extraNames: ['partenaireServicePartenaire.dateDebutPartenaire'],
    placeholder,
    ordre: 6,
  },
  {
    name: 'dateFin',
    label: 'Date fin de Partenariat',
    value: '',
    type: 'Search',
    extraNames: ['partenaireServicePartenaire.dateFinPartenaire'],
    placeholder,
    ordre: 7,
  },
  {
    name: 'type',
    label: 'Type de Partenariat',
    value: '',
    type: 'Search',
    extraNames: ['partenaireServicePartenaire.typePartenaire'],
    placeholder,
    ordre: 8,
  },
  {
    name: 'statut',
    label: 'Statut de Partenariat',
    value: '',
    type: 'Search',
    extraNames: ['partenaireServicePartenaire.statutPartenaire'],
    placeholder,
    ordre: 9,
  }
];
