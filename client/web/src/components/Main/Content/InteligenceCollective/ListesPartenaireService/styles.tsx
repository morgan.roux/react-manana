import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      width: '100%',
    },
    listRoot: {
      maxWidth: 1300,
      width: '100%',
      margin: '0 auto',
      '& .MuiTableRow-head': {
        '& > th:nth-child(6), & > th:nth-child(7)': {
          minWidth: 116,
        },
        '& > th:nth-child(10), & > th:nth-child(11)': {
          minWidth: 128,
        },
      },
    },
    searchInputBox: {
      padding: '0 24px',
    },
    noPubContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      width: '100%',
      padding: '24px',
    },
    noPubTitle: {
      fontSize: 30,
      fontWeight: 'bold',
      color: '#616161',
    },
    noPubTexte: {
      fontSize: 16,
      fontWeight: 'normal',
      color: '#9E9E9E',
      marginTop: 15,
    },
    buttonValider1: {
      display: 'inline-block',
      float: 'right',
    },
    header: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      padding: '0px 25px',
      height: 60,
      position: 'relative',
      borderBottom: '1px solid #E0E0E0',
    },
    headerTitle: {
      position: 'absolute',
      left: '50vw',
    },
  }),
);

export default useStyles;
