import React, { FC } from 'react';
import useStyles from './styles';
import { useValueParameterAsBoolean } from '../../../../../../utils/getValueParameter';
import {
  SERVICE_PARTNER_FILTER_NAME,
  SERVICE_PARTNER_ADDRESS,
  SERVICE_PARTNER_FILTER_CP,
  SERVICE_PARTNER_FILTER_CITY,
  SERVICE_PARTNER_FILTER_PHONE,
  SERVICE_PARTNER_FILTER_SERVICNE_NAME,
  SERVICE_PARTNER_FILTER_START_DATE,
  SERVICE_PARTNER_FILTER_END_DATE,
  SERVICE_PARTNER_FILTER_TYPE,
  SERVICE_PARTNER_FILTER_STATE,
  SEARCH_FIELDS_OPTIONS,
} from '../constants';
import { FieldsOptions } from '../../../../../Common/CustomContent/interfaces';
import SearchFilter from '../../../../../Common/newCustomContent/SearchFilter';

interface ListesPartenaireServiceFilterProps {
  fieldsState: any;
  handleFieldChange: (event: any) => void;
  handleRunSearch: (query: any) => void;
  handleResetFields: () => void;
}

const ListesPartenaireServiceFilter: FC<ListesPartenaireServiceFilterProps> = ({
  fieldsState,
  handleFieldChange,
  handleRunSearch,
  handleResetFields,
}) => {
  const classes = useStyles({});

  const objectState = {
    partenaireServiceState: useValueParameterAsBoolean(SERVICE_PARTNER_FILTER_NAME),
    adresseState: useValueParameterAsBoolean(SERVICE_PARTNER_ADDRESS),
    codePostalState: useValueParameterAsBoolean(SERVICE_PARTNER_FILTER_CP),
    cityState: useValueParameterAsBoolean(SERVICE_PARTNER_FILTER_CITY),
    telephoneState: useValueParameterAsBoolean(SERVICE_PARTNER_FILTER_PHONE),
    // servicesState: useValueParameterAsBoolean(SERVICE_PARTNER_FILTER_SERVICNE_NAME),
    dateDebutState: useValueParameterAsBoolean(SERVICE_PARTNER_FILTER_START_DATE),
    dateFinState: useValueParameterAsBoolean(SERVICE_PARTNER_FILTER_END_DATE),
    typeState: useValueParameterAsBoolean(SERVICE_PARTNER_FILTER_TYPE),
    statutState: useValueParameterAsBoolean(SERVICE_PARTNER_FILTER_STATE),
  };

  const filterSearchFields = (objectState: any, searchFields: FieldsOptions[]): FieldsOptions[] => {
    const activeStateKeys = Object.keys(objectState).filter(key => objectState[key] === true);
    const activeSearchFields = searchFields.filter((field: FieldsOptions) =>
      activeStateKeys.includes(`${field.name}State`),
    );
    return activeSearchFields;
  };

  const activeSearcFields: FieldsOptions[] = filterSearchFields(objectState, SEARCH_FIELDS_OPTIONS);

  return (
    <SearchFilter
      fieldsState={fieldsState}
      handleFieldChange={handleFieldChange}
      searchInputs={activeSearcFields}
      labelWidth={55}
      handleRunSearch={handleRunSearch}
      handleResetFields={handleResetFields}
    />
  );
};

export default ListesPartenaireServiceFilter;
