import { useLazyQuery, useApolloClient, QueryLazyOptions, useQuery } from '@apollo/react-hooks';
// import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';
import { RouteComponentProps, withRouter } from 'react-router';
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import WithSearch from '../../../../Common/newWithSearch/withSearch';
import useStyles from './styles';
import { Column } from '../../../../Dashboard/Content/Interface';
import { DO_SEARCH_USERS } from '../../../../../graphql/User';
import {
  SEARCH_USERS,
  SEARCH_USERSVariables,
} from '../../../../../graphql/User/types/SEARCH_USERS';
import SearchInput from '../../../../Common/newCustomContent/SearchInput';
import { Box, Button, Typography } from '@material-ui/core';
import SubToolbar from '../../../../Common/newCustomContent/SubToolbar';
import CustomContent from '../ListesGroupeAmis/Children/CustomContent';
import { GET_CHECKEDS_PARTENAIRE_SERVICE } from '../../../../../graphql/PartenairesServices/local';
import { PartenaireServiceDashboard } from '../../../../Common/newWithSearch/ComponentInitializer';
import { DO_SEARCH_PARTENAIRE } from '../../../../../graphql/IntelligenceCollective/query';
import {
  SEARCH_PARTENAIRE,
  SEARCH_PARTENAIREVariables,
} from '../../../../../graphql/IntelligenceCollective/types/SEARCH_PARTENAIRE';
import moment from 'moment';
import { SHAREINFO_DETAILS_URL, SHAREINFO_ITEMS_CHOICE_URL } from '../../../../../Constant/url';

interface ListesLaboPartenaireProps {
  listResult?: any;
  valueBtn?: any;
  props?: any;
  match: {
    params: {
      itemId: string | undefined;
    };
  };
}

const ListesLaboPartenaire: FC<ListesLaboPartenaireProps & RouteComponentProps> = ({
  history: { goBack, push },
  props,
  match: {
    params: { itemId },
  },
}) => {
  const client = useApolloClient();
  const classes = useStyles({});

  const [should, setShould] = useState<[]>();

  const statut = {
    ACTIVER: 'Activé',
    BLOQUER: 'Désactivé',
  };

  const type = {
    NON_PARTENAIRE: 'Non partenaire',
    NEGOCIATION_EN_COURS: 'Négociation en cours',
    NOUVEAU_REFERENCEMENT: 'Nouveau réferencement',
    PARTENARIAT_ACTIF: 'Partenariat actif',
  };

  const columns: Column[] = [
    {
      name: 'nom',
      label: 'Prestataire de service',
      renderer: (row: any) => {
        return row.nom || '-';
      },
    },
    {
      name: 'adresse1',
      label: 'Adresse',
      renderer: (row: any) => {
        return (row.partenaireServiceSuite && row.partenaireServiceSuite.adresse1) || '-';
      },
    },
    {
      name: 'codePostal',
      label: 'CP',
      renderer: (row: any) => {
        return (row.partenaireServiceSuite && row.partenaireServiceSuite.codePostal) || '-';
      },
    },
    {
      name: 'ville',
      label: 'Ville',
      renderer: (row: any) => {
        return (row.partenaireServiceSuite && row.partenaireServiceSuite.ville) || '-';
      },
    },
    {
      name: 'country',
      label: 'Pays',
    },
    {
      name: 'telephone',
      label: 'Téléphone',
      renderer: (row: any) => {
        return (row.partenaireServiceSuite && row.partenaireServiceSuite.telephone) || '-';
      },
    },
    {
      name: 'type',
      label: 'Services',
      renderer: (row: any) => {
        if (row.services && row.services.length > 0) {
          return row.services.map((s, index) => {
            if (index !== row.services.length - 1) return s.nom + ' / ';
            return s.nom;
          });
        }
        return '-';
      },
    },
    {
      name: 'dateDebutPartenaire',
      label: 'Date début de partenariat',
      renderer: (row: any) => {
        return (
          moment(
            row.partenaireServicePartenaire && row.partenaireServicePartenaire.dateDebutPartenaire,
          ).format('DD/MM/YYYY') || '-'
        );
      },
    },
    {
      name: 'dateFinPartenaire',
      label: 'Date fin de partenariat',
      renderer: (row: any) => {
        return (
          moment(
            row.partenaireServicePartenaire && row.partenaireServicePartenaire.dateFinPartenaire,
          ).format('DD/MM/YYYY') || '-'
        );
      },
    },
    {
      name: 'typePartenaire',
      label: 'Type de partenariat',
      renderer: (row: any) => {
        return (
          (row.partenaireServicePartenaire &&
            row.partenaireServicePartenaire.typePartenaire &&
            type[row.partenaireServicePartenaire.typePartenaire]) ||
          '-'
        );
      },
    },
    {
      name: 'statutPartenaire',
      label: 'Statut de partenariat',
      renderer: (row: any) => {
        return (
          (row.partenaireServicePartenaire &&
            row.partenaireServicePartenaire.statutPartenaire &&
            statut[row.partenaireServicePartenaire.statutPartenaire]) ||
          '-'
        );
      },
    },
  ];

  const [selected, setSelected] = useState(itemId ? [itemId] : []);

  const tableProps = {
    isSelectable: true,
    paginationCentered: false,
    columns,
    selected,
    setSelected,
  };
  const handleBack = () => {
    push(SHAREINFO_ITEMS_CHOICE_URL);
  };
  const handleClick = () => {
    push({
      pathname: `${SHAREINFO_DETAILS_URL}/PSERVICE/comments/${selected[0]}`,
    });
  };
  return (
    <Box className={classes.container}>
      <SubToolbar
        dark={false}
        withBackBtn={true}
        onClickBack={handleBack}
        title="Partage des informations"
      >
        {selected && selected.length > 0 && (
          <Button
            variant="contained"
            color="secondary"
            className={classes.buttonValider1}
            onClick={handleClick}
            disabled={selected && selected.length < 0}
          >
            VALIDER MON CHOIX
          </Button>
        )}
      </SubToolbar>

      <Box className={classes.listRoot}>
        <Box className={classes.noPubContainer}>
          <Typography className={classes.noPubTitle}>Liste des partenaires de services</Typography>
          <Typography className={classes.noPubTexte}>
            Choisissez un partenaire de service parmi la liste.
          </Typography>
        </Box>
        <Box className={classes.searchInputBox}>
          <SearchInput searchPlaceholder="Rechercher un contact" />
        </Box>
        <WithSearch
          type="partenaire"
          props={tableProps}
          WrappedComponent={CustomContent}
          searchQuery={DO_SEARCH_PARTENAIRE}
          // optionalShould={filters}
          minimumShouldMatch={0}
          optionalMust={[]}
        />
        {/* <PartenaireServiceDashboard tableOnly={true} /> */}
      </Box>
    </Box>
  );
};
export default withRouter(ListesLaboPartenaire);
