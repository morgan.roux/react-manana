import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import CommentItem from '../../../../../../Comment/CommentItem';
// import { useStyles } from './styles';

interface PartageInfoCommentItemProps {
  comment: any;
}

const PartageInfoCommentItem: FC<PartageInfoCommentItemProps & RouteComponentProps> = ({
  comment,
  history: { push },
}) => {
  // const classes = useStyles({});

  const handleClickVisibility = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    event.stopPropagation();
    if (comment && comment.idItemAssocie) {
      push(`/catalogue-produits/card/${comment.idItemAssocie}`);
    }
  };

  return (
    <CommentItem
      key={comment && comment.id}
      comment={comment}
      isVisibile={true}
      onClickVisibility={handleClickVisibility}
    />
  );
};

export default withRouter(PartageInfoCommentItem);
