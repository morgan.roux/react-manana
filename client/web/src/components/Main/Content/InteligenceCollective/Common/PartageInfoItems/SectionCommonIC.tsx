import {
  Avatar,
  Box,
  IconButton,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from '@material-ui/core';
import { AccountCircle, Visibility } from '@material-ui/icons';
import classnames from 'classnames';
import moment from 'moment';
import React, { FC } from 'react';
import useCommonStyles from '../../../../../Dashboard/commonStyles';

import { useStyles } from './styles';

interface CommentItemProps {
  comment: any;
}

const SectionIC: FC<CommentItemProps> = ({ comment }) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();

  // const userPhotoUrl =
  // comment &&
  // comment.user &&
  // comment.user.userPhoto &&
  // comment.user.userPhoto.fichier &&
  // comment.user.userPhoto.fichier.publicUrl;

  const userRole = (comment && comment.user && comment.user.role && comment.user.role.nom) || '';
  const userPharmacie =
    (comment && comment.user && comment.user.pharmacie && comment.user.pharmacie.nom) || '';

  const userName = comment && comment.user && comment.user.userName;

  // Subtract 30s for different on server and client
  const dateCreation =
    comment &&
    moment(comment.dateCreation)
      .subtract(30, 'seconds')
      .fromNow();

  //const fichiers = (comment && comment.fichiers) || [];

  const openFile = (url: string) => () => {
    window.open(url, '_blank');
  };
  const tab = ['Origine', 'Motif'];
  return (
    <ListItem className={classes.contentListItem}>
      <ListItemText className={classes.contentMessage}>
        <Box display="flex" flexDirection="column" width="100%">
          {/* <IconButton className={classes.visibility}>
    <Visibility/>
    </IconButton> */}
          <Typography variant="body2">{comment ? comment.content : ''}</Typography>
          <Box className={classes.contentLabels}>
            {/* <Box className={classes.labels}>
    <Typography className={classnames(classes.labelOrigine)}>Origine</Typography>:
    <Typography className={classnames(classes.labelText)}></Typography>
    </Box>
    <Box className={classes.labels}>
    <Typography className={classnames(classes.labelOrigine)}>Motif</Typography>:
    <Typography className={classnames(classes.labelText)}></Typography>
    </Box> */}

            {tab.map(Items => {
              return (
                <Box className={classes.labels}>
                  <Typography className={classnames(classes.labelOrigine)}>{Items}</Typography>:
                  <Typography className={classnames(classes.labelText)}></Typography>
                </Box>
              );
            })}
            {/* <Box className={classes.labels}>
    <Typography className={classnames(classes.labelOrigine)}>Date</Typography>:
    <Typography className={classnames(classes.labelText)}></Typography>
    </Box>
    <Box className={classes.labels}>
    <Typography className={classnames(classes.labelOrigine)}>Statut</Typography>:
    <Typography className={classnames(classes.labelText)}></Typography>
    </Box> */}
          </Box>
        </Box>
      </ListItemText>
    </ListItem>
  );
};

export default SectionIC;
