import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginBottom: 10,
    },
    contentRoot: {
      background: '#F5F6FA',
      borderRadius: 12,
    },
  }),
);
