import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    contentListItem: {
      border: '1px solid #E3E3E3',
      borderRadius: 6,
      marginBottom: 10,
      display: 'flex',
      flexDirection: 'column',
      padding: '13px 15px',
    },
    contentMessage: {},
    contentLabels: {
      display: 'flex',
      marginTop: 5,
    },
    labels: {
      display: 'flex',
      marginRight: 24,
    },
    labelOrigine: {},
    labelText: {},
  }),
);

export default useStyles;
