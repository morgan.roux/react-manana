import { useApolloClient } from '@apollo/react-hooks';
import { Avatar, Box, Icon, IconButton, List, ListItem, Typography } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import { ChatBubble, LocalOffer, Today, Visibility } from '@material-ui/icons';
import AssistantPhotoIcon from '@material-ui/icons/AssistantPhoto';
import CircleChecked from '@material-ui/icons/CheckCircleOutline';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import { ApolloQueryResult } from 'apollo-client';
import classnames from 'classnames';
import { uniqBy } from 'lodash';
import moment from 'moment';
import React, { Fragment } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import {
  SEARCH_ACTION,
  SEARCH_ACTIONVariables,
  SEARCH_ACTION_search_data_Action,
} from '../../../../../../../graphql/Action/types/SEARCH_ACTION';
import { nl2br } from '../../../../../../../services/Html';
import { ActionStatus } from '../../../../../../../types/graphql-global-types';
import { stringToAvatar } from '../../../../../../../utils/Helpers';
import { CustomModal } from '../../../../../../Common/CustomModal';
import ActionButton from '../../../../TodoNew/Common/ActionButton';
import TaskDetails from '../../../../TodoNew/Task/TaskDetails';
import { CountTodosProps } from '../../../../TodoNew/TodoNew';
import useStyles from './styles copy';

interface TaskListProps {
  tasks: SEARCH_ACTION_search_data_Action[];
  refetch?: (
    variables?: SEARCH_ACTIONVariables | undefined,
  ) => Promise<ApolloQueryResult<SEARCH_ACTION>>;
  isLoading: boolean;
}

const TaskList: React.FC<TaskListProps & RouteComponentProps & CountTodosProps> = ({
  tasks,
  refetch,
  refetchCountTodos,
}) => {
  const classes = useStyles({});

  const [open, setOpen] = React.useState<boolean>(false);
  const [clickedTask, setClickedTask] = React.useState<SEARCH_ACTION_search_data_Action | null>(
    null,
  );

  const handleClickVisibility = (task: any, event: any) => {
    event.stopPropagation();
    setClickedTask(task);
    setOpen(true);
  };

  React.useMemo(() => {
    if (!open && clickedTask !== null) {
      setClickedTask(null);
    }
  }, [open]);

  return (
    <Fragment>
      <List className={classes.list}>
        {tasks &&
          tasks.length > 0 &&
          tasks.map((task, index) => (
            <Box key={index}>
              <ListItem button={true} className={classes.list} key={task.id}>
                <Box className={classes.tasks}>
                  <Checkbox
                    icon={<CircleUnchecked fontSize="large" />}
                    checkedIcon={<CircleChecked fontSize="large" />}
                    checked={task.status === ActionStatus.DONE}
                  />

                  <Box display="flex" flexDirection="column" width="100%">
                    <Box
                      display="flex"
                      alignItems="center"
                      justifyContent="space-between"
                      marginBottom="24px"
                    >
                      <Typography
                        className={classes.taskTitle}
                        dangerouslySetInnerHTML={{ __html: nl2br(task.description) } as any}
                      />
                      <Box
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                        // tslint:disable-next-line: jsx-no-lambd
                      >
                        {task &&
                        task.project &&
                        task.assignedUsers &&
                        task.assignedUsers.length > 0 ? (
                          <Box display="flex" flexDirection="row">
                            {uniqBy(task.assignedUsers, 'id').map(u => {
                              const photo =
                                u.userPhoto && u.userPhoto.fichier && u.userPhoto.fichier.publicUrl;
                              return photo ? (
                                <img src={photo} style={{ width: 30, height: 30 }} />
                              ) : (
                                <Avatar style={{ width: 30, height: 30, fontSize: 12 }} key={u.id}>
                                  {stringToAvatar(u.userName || '')}
                                </Avatar>
                              );
                            })}
                          </Box>
                        ) : (
                          <PersonAddIcon />
                        )}
                        <IconButton
                          // tslint:disable-next-line: jsx-no-lambda
                          onClick={event => handleClickVisibility(task, event)}
                        >
                          <Visibility />
                        </IconButton>
                      </Box>
                    </Box>
                    <Box className={classes.contentLabels}>
                      {task.dateDebut && (
                        <Box className={classes.labels}>
                          <Icon color="secondary">
                            <Today />
                          </Icon>
                          <Typography className={classnames(classes.labelText, classes.date)}>
                            {moment(task.dateDebut).format('DD MMM YYYY')}
                          </Typography>
                        </Box>
                      )}
                      <Box className={classes.labels}>
                        <Icon>
                          <ChatBubble />
                        </Icon>
                        <Typography className={classnames(classes.labelText)}>
                          {task.nbComment}
                        </Typography>
                      </Box>
                      {/*<Box className={classes.labels}>
                        <AssistantPhotoIcon
                          style={{
                            color:
                              task.priority === 1
                                ? 'red'
                                : task.priority === 2
                                ? 'yellow'
                                : task.priority === 3
                                ? 'green'
                                : 'gray',
                          }}
                        />
                        <Typography className={classnames(classes.labelText)}>
                          Priorité {task.priority}
                        </Typography>
                      </Box>*/}
                      {task.etiquettes &&
                        task.etiquettes.length > 0 &&
                        task.etiquettes.map(etiquette => (
                          <Box className={classes.labels} key={etiquette.id}>
                            <Icon>
                              <LocalOffer
                                htmlColor={
                                  etiquette && etiquette.couleur
                                    ? etiquette.couleur.code
                                    : '#C1CAD6'
                                }
                              />
                            </Icon>
                            <Typography className={classnames(classes.bold, classes.labelText)}>
                              {etiquette.nom}
                            </Typography>
                          </Box>
                        ))}
                      {task.origine && task.origine.libelle && (
                        <Box className={classes.labels}>
                          <Typography className={classnames(classes.labelOrigine)}>
                            Origine
                          </Typography>
                          :
                          <Typography className={classnames(classes.labelText)}>
                            {task.origine.libelle}
                          </Typography>
                        </Box>
                      )}
                      {task.project && task.project.name && (
                        <Box className={classes.labels}>
                          <Typography className={classnames(classes.labelOrigine)}>
                            Projet
                          </Typography>
                          :
                          <Typography className={classnames(classes.labelText)}>
                            {task.project.name}
                          </Typography>
                        </Box>
                      )}
                    </Box>
                  </Box>
                </Box>
              </ListItem>
              {task &&
                task.subActions &&
                task.subActions.length > 0 &&
                task.subActions.map(subAction => (
                  <ListItem button={true} className={classes.list} key={subAction.id}>
                    <Box className={classes.subTasks}>
                      <Checkbox
                        icon={<CircleUnchecked fontSize="large" />}
                        checkedIcon={<CircleChecked fontSize="large" />}
                        checked={subAction.status === ActionStatus.DONE}
                      />
                      <Box display="flex" flexDirection="column" width="100%">
                        <Box
                          display="flex"
                          alignItems="center"
                          justifyContent="space-between"
                          marginBottom="24px"
                        >
                          <Typography
                            className={classes.taskTitle}
                            dangerouslySetInnerHTML={
                              { __html: nl2br(subAction.description) } as any
                            }
                          />
                          <Box
                            display="flex"
                            justifyContent="center"
                            alignItems="center"
                            // tslint:disable-next-line: jsx-no-lambda
                          >
                            <ActionButton
                              task={task}
                              refetch={refetch}
                              refetchCountTodos={refetchCountTodos}
                            />
                          </Box>
                        </Box>
                        <Box className={classes.contentLabels}>
                          {task.dateDebut && (
                            <Box className={classes.labels}>
                              <Icon color="secondary">
                                <Today />
                              </Icon>
                              <Typography className={classnames(classes.labelText, classes.date)}>
                                {moment(task.dateDebut).format('DD MMM YYYY')}
                              </Typography>
                            </Box>
                          )}
                          <Box className={classes.labels}>
                            <Icon>
                              <ChatBubble />
                            </Icon>
                            <Typography className={classnames(classes.labelText)}>
                              {task.nbComment}
                            </Typography>
                          </Box>
                          {/*<Box className={classes.labels}>
                            <AssistantPhotoIcon
                              style={{
                                color:
                                  task.priority === 1
                                    ? 'red'
                                    : task.priority === 2
                                    ? 'yellow'
                                    : task.priority === 3
                                    ? 'green'
                                    : 'gray',
                              }}
                            />
                            <Typography className={classnames(classes.labelText)}>
                              Priorité {task.priority}
                            </Typography>
                            </Box>*/}
                          {task.etiquettes &&
                            task.etiquettes.length > 0 &&
                            task.etiquettes.map(etiquette => (
                              <Box className={classes.labels} key={etiquette.id}>
                                <Icon>
                                  <LocalOffer
                                    htmlColor={
                                      etiquette && etiquette.couleur
                                        ? etiquette.couleur.code
                                        : '#C1CAD6'
                                    }
                                  />
                                </Icon>
                                <Typography className={classnames(classes.bold, classes.labelText)}>
                                  {etiquette.nom}
                                </Typography>
                              </Box>
                            ))}
                          {task.origine && task.origine.libelle && (
                            <Box className={classes.labels}>
                              <Typography className={classnames(classes.labelOrigine)}>
                                Origine
                              </Typography>
                              :
                              <Typography className={classnames(classes.labelText)}>
                                {task.origine.libelle}
                              </Typography>
                            </Box>
                          )}
                        </Box>
                      </Box>
                    </Box>
                  </ListItem>
                ))}
            </Box>
          ))}
      </List>
      <CustomModal
        open={open}
        setOpen={setOpen}
        title="Détail de la tâche"
        withBtnsActions={false}
        closeIcon={true}
        headerWithBgColor={true}
        maxWidth="md"
        fullWidth={true}
        className={classes.taskDetailsModal}
      >
        {clickedTask && <TaskDetails task={clickedTask} disabledActions={true} />}
      </CustomModal>
    </Fragment>
  );
};

export default withRouter(TaskList);
