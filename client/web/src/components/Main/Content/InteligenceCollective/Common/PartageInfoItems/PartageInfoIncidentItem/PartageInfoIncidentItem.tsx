import {
  Avatar,
  Box,
  IconButton,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from '@material-ui/core';
import { AccountCircle, Visibility } from '@material-ui/icons';
import classnames from 'classnames';
import moment from 'moment';
import React, { FC } from 'react';
import useCommonStyles from '../../../../../../Dashboard/commonStyles';

import useStyles from './styles';

interface PartageInfoIncidentItemProps {
  incident: any;
  bottomItems: any[];
}

const PartageInfoIncidentItem: FC<PartageInfoIncidentItemProps> = ({ incident, bottomItems }) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const userRole =
    (incident && incident.user && incident.user.role && incident.user.role.nom) || '';
  const userPharmacie =
    (incident && incident.user && incident.user.pharmacie && incident.user.pharmacie.nom) || '';

  const userName = incident && incident.user && incident.user.userName;

  // Subtract 30s for different on server and client
  const dateCreation =
    incident &&
    moment(incident.dateCreation)
      .subtract(30, 'seconds')
      .fromNow();

  //const fichiers = (incident && incident.fichiers) || [];

  const openFile = (url: string) => () => {
    window.open(url, '_blank');
  };
  const tab = ['Origine', 'Motif'];
  return (
    <Box className={classes.contentListItem}>
      <Box display="flex" justifyContent="space-between">
        <Box display="flex" marginBottom="15px">
          <Typography>Declarant: </Typography>
          <Typography>{incident.declarant}</Typography>
        </Box>
        <IconButton>
          <Visibility />
        </IconButton>
      </Box>
      <Typography>{incident.content}</Typography>
      <Box className={classes.contentLabels}>
        {bottomItems.map(item => {
          return (
            <Box className={classes.labels}>
              <Typography className={classnames(classes.labelOrigine)}>{item.label}</Typography>:
              <Typography className={classnames(classes.labelText)}>
                {incident[item.key]}
              </Typography>
            </Box>
          );
        })}
      </Box>
    </Box>
  );
};

export default PartageInfoIncidentItem;
