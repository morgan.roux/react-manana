import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { Box, Checkbox, Fab, Grid, Tooltip } from '@material-ui/core';
import CircleChecked from '@material-ui/icons/CheckCircleOutline';
import PeopleIcon from '@material-ui/icons/People';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import React, { FC, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { DO_SEARCH_USERS } from '../../../../../../graphql/User';
import { SEARCH_USERSVariables } from '../../../../../../graphql/User/types/SEARCH_USERS';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import Backdrop from '../../../../../Common/Backdrop';
import { CustomModal } from '../../../../../Common/CustomModal';
import SearchInput from '../../../../../Common/newCustomContent/SearchInput';
import WithSearch from '../../../../../Common/newWithSearch/withSearch';
import CustomSelectTask from '../../../TodoNew/Common/Task/CustomSelectTask';
import ConcernedParticipantChild from './ConcernedParticipantsChild/ConcernedParticipantChild';
import { useStyles } from './styles';

interface ConcernedParticipantProps {
  title: string;
  isOnEquip: boolean;
  isOpen: boolean;
  setOpen: any;
  userIdSelected?: (user: any) => void;
  userNameSelected?: (user: any) => void;
  setUsers?: (users: any[]) => void;
  withBtnsActions?: boolean;
  allTeamIsCheckedByDefault?: boolean;
}

const ConcernedParticipant: FC<ConcernedParticipantProps & RouteComponentProps> = ({
  title,
  userIdSelected,
  setUsers,
  userNameSelected,
  isOnEquip,
  isOpen = true,
  withBtnsActions = true,
  allTeamIsCheckedByDefault = true,
  setOpen,
}) => {
  const classes = useStyles({});
  const projectList = [
    { label: 'Tous les types', value: 'Tous' },
    { label: 'Pharmacie', value: 'pharmacie' },
    { label: 'Présidents de régions', value: 'presidentregion' },
    { label: 'Partenaires de services', value: 'partenaire' },
    { label: 'Laboratoire partenaire', value: 'laboratoire' },
  ];

  interface DATAFORM {
    searchValue: string;
    sortBy: string;
  }

  const initialFormValue: DATAFORM = {
    searchValue: '',
    sortBy: 'Tous',
  };

  const [dataParticipantList, setDataParticipantList] = useState(initialFormValue);

  const handleChange = event => {
    const { name, value } = event.target;
    setDataParticipantList({ ...dataParticipantList, [name]: value });
  };

  const handleChecked = event => {
    event.stopPropagation();
    setUserChecked('all');
    if (userIdSelected) userIdSelected('all');
    if (actionGetUserObject) actionGetUserObject('all');
  };

  const client = useApolloClient();

  const { data: usersData, loading: isloading } = useQuery<any, SEARCH_USERSVariables>(
    DO_SEARCH_USERS,
    {
      variables: {},
      onError: error => {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );

  const USERS = (usersData && usersData.search && usersData.search.data) || [];

  const userIds = USERS.map(user => {
    if (user && user !== null) return user.id;
  });

  if (setUsers) setUsers(userIds);

  const optionalMust =
    dataParticipantList.sortBy === 'Tous'
      ? []
      : [
          {
            exists: {
              field: dataParticipantList.sortBy,
            },
          },
        ];

  // checked
  const [userChecked, setUserChecked] = useState<string>(
    (allTeamIsCheckedByDefault && 'all') || '',
  );

  const actionGetUserObject = userName => {
    if (userNameSelected) userNameSelected(userName);
  };

  const action = (event, idUser) => {
    event.stopPropagation();
    if (userChecked.includes(idUser)) {
      setUserChecked('');
    } else {
      setUserChecked('');
      setUserChecked(idUser);
      if (userIdSelected) userIdSelected(idUser);
    }
  };

  return (
    <CustomModal
      open={isOpen}
      setOpen={setOpen}
      title={title}
      withBtnsActions={withBtnsActions}
      closeIcon={true}
      headerWithBgColor={true}
      maxWidth="md"
      fullWidth={true}
      className={classes.usersModalRoot}
    >
      <Box className={classes.tableContainer}>
        <Box className={classes.section}>
          <Grid container={true} spacing={3}>
            <Grid item={true} xs={8}>
              <SearchInput searchPlaceholder="Rechercher un participant..." />
            </Grid>
            <Grid item={true} xs={4}>
              <CustomSelectTask
                label="Type de contact"
                list={projectList}
                name="sortBy"
                className={classes.espaceHaut}
                onChange={handleChange}
              />
            </Grid>
          </Grid>
        </Box>
        {isOnEquip ? (
          <Box className={classes.section}>
            <Grid container={true} spacing={3}>
              <Grid item={true} xs={1}>
                <Tooltip
                  title="Ajouter des personnes"
                  aria-label="add"
                  className={classes.iconCustom}
                >
                  <Fab>
                    <PeopleIcon />
                  </Fab>
                </Tooltip>
              </Grid>
              <Grid item={true} xs={10} className={classes.label}>
                Tout l'équipe
              </Grid>
              <Grid item={true} xs={1} className={classes.label}>
                <Checkbox
                  icon={<CircleUnchecked fontSize="large" />}
                  checkedIcon={<CircleChecked fontSize="large" />}
                  // tslint:disable-next-line: jsx-no-lambda
                  onClick={event => handleChecked(event)}
                  checked={userChecked === 'all'}
                />
              </Grid>
            </Grid>
          </Box>
        ) : null}

        <Box className={classes.section}>
          <WithSearch
            type="user"
            WrappedComponent={ConcernedParticipantChild}
            searchQuery={DO_SEARCH_USERS}
            optionalMust={optionalMust}
            props={{ action, userChecked, setUserChecked, actionGetUserObject }}
          />
        </Box>
      </Box>
      <Backdrop open={isloading} value="chargement en cours" />
    </CustomModal>
  );
};

export default withRouter(ConcernedParticipant);
