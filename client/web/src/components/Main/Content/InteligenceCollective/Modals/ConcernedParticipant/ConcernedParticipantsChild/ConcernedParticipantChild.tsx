import { Avatar, Box, Checkbox } from '@material-ui/core';
import CircleChecked from '@material-ui/icons/CheckCircleOutline';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import React, { FC, useContext } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { ContentContext, ContentStateInterface } from '../../../../../../../AppContext';
import { useStyles } from './styles';

interface ParticipantListToAssignProps {
  listResult?: any;
  action: any;
  setUserChecked: any;
  userChecked: string;
  actionGetUserObject?: (arg: any) => void;
}

const ConcernedParticipantChild: FC<ParticipantListToAssignProps & RouteComponentProps> = ({
  listResult,
  actionGetUserObject,
  action,
  userChecked,
  setUserChecked,
}) => {
  const classes = useStyles({});

  const {
    content: { variables, operationName, refetch },
  } = useContext<ContentStateInterface>(ContentContext);

  const handleClickUser = (event, user) => {
    action(event, user.id);
    if (actionGetUserObject) actionGetUserObject(user.userName);
  };

  const USERS =
    (listResult && listResult.data && listResult.data.search && listResult.data.search.data) || [];

  return (
    <Box className={classes.rootListParticipant}>
      {USERS.map(user => (
        <Box className={classes.contentRootListParticipant}>
          <Box display="flex" alignItems="center">
            <Box>
              <Avatar alt={user.userName} className={classes.avatar} />
            </Box>
            <Box className={classes.description}>
              <Box className={classes.title}>{user.userName}</Box>
              <Box className={classes.content}>{user.role && user.role.nom}</Box>
              <Box className={classes.content}>{user.email}</Box>
            </Box>
          </Box>
          <Box marginLeft="auto">
            <Checkbox
              icon={<CircleUnchecked fontSize="large" />}
              checkedIcon={<CircleChecked fontSize="large" />}
              // tslint:disable-next-line: jsx-no-lambda
              onClick={event => handleClickUser(event, user)}
              checked={userChecked === user.id}
            />
          </Box>
          <Box className={classes.icon} />
        </Box>
      ))}
    </Box>
  );
};

export default withRouter(ConcernedParticipantChild);
