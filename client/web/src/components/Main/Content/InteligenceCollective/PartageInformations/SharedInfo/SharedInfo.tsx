import React, { FC } from 'react';
import PartageInfoItem from '../../Common/PartageInfoItems/PartageInfoItem';
import ShareNoResult from '../ShareNoResult/ShareNoResult';
import { BOTTON_ITEMS } from '../utils/constants';
import GetData from '../utils/GetData';
interface SharedInfoProps {
  listResult: any;
  goToDetail: (any) => void;
}
const SharedInfo: FC<SharedInfoProps> = ({ listResult, goToDetail }) => {
  return (
    <>
      {(listResult &&
        listResult.length > 0 &&
        listResult.map((actuality) => (
          <PartageInfoItem
            handleVisibility={() => goToDetail(actuality.id)}
            incident={GetData(actuality)}
            bottomItems={BOTTON_ITEMS}
            infoKey="declarant"
            infoLabel="Déclarant"
          />
        ))) || <ShareNoResult />}
    </>
  );
};
export default SharedInfo;
