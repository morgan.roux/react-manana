export const BOTTON_ITEMS = [
  { label: 'Origine', key: 'origine' },
  { label: 'Motif', key: 'motif' },
  { label: 'Date', key: 'dateCreation' },
  { label: 'Status', key: 'statut' },
];

export const BOTTON_ITEMS_AMELIORATION = [
  { label: 'Origine', key: 'origine' },
  { label: 'Activité concerné', key: 'activite' },
  { label: 'Date', key: 'dateCreation' },
  { label: `Action d'amélioration`, key: 'statut' },
];
