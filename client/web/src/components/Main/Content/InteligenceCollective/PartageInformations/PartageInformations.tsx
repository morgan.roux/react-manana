import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import {
  SHAREINFO_FRIEND_GROUP_URL,
  SHAREINFO_LABO_PARTENAIRE_URL,
  SHAREINFO_PARTENAIRE_SERVICE_URL,
} from '../../../../../Constant/url';
import FriendsGroup from '../../../../../assets/img/intelligence_collective/groupe_damis.png';
import LaboratoirePartenaires from '../../../../../assets/img/intelligence_collective/laboratoire_partenaire.png';
import PartenaireServices from '../../../../../assets/img/intelligence_collective/partenaire_de_service.png';
import ChoicePage, { ChoicePageItem } from '../../../../Common/ChoicePage/ChoicePage';

import useStyles from './styles';

import ListesGroupeAmis from '../ListesGroupeAmis';

const PartageInformations: FC<RouteComponentProps> = ({ history: { push } }) => {
  const handlePartageFriends = () => push(SHAREINFO_FRIEND_GROUP_URL);

  const handleGotoLaboPartenaire = () =>
    push(`/laboratoires`); /* push(SHAREINFO_LABO_PARTENAIRE_URL); */

  const handleGotoLaboServices = () => push(SHAREINFO_PARTENAIRE_SERVICE_URL);

  const items: ChoicePageItem[] = [
    { text: `Groupes d'amis `, img: FriendsGroup, onClick: handlePartageFriends },
    {
      text: 'Laboratoires partenaires',
      img: LaboratoirePartenaires,
      onClick: handleGotoLaboPartenaire,
    },
    {
      text: 'Partenaires de services',
      img: PartenaireServices,
      onClick: handleGotoLaboServices,
    },
  ];
  const handleGoback = () => {
    push('/intelligence-collective');
  };

  return (
    <ChoicePage
      items={items}
      withHeader={false}
      headerTitle="Partage d'informations"
      title="Partage d'informations"
      subTitle="Choisissez un item pour extraire les informations que vous recherchez "
      onClickArrowBack={handleGoback}
    />
  );
};

export default withRouter(PartageInformations);
