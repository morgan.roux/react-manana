import moment from 'moment';

const origin = {
  INTERNE: 'Interne',
  PATIENT: 'Patient',
  GROUPE_AMIS: "Groupe d'amis",
  GROUPEMENT: 'Groupement',
  FOURNISSEUR: 'Fournisseur',
  PRESTATAIRE: 'Prestataire',
  DIGITAL4WIN: 'Digital4win',

};

const statut = {
  NOUVELLE: 'Nouvelle',
  EN_COURS: 'En Cours',
  CLOTURE: 'Clôturé',
};

const GetData = (obj: any) => {
  const userName = (obj && obj.declarant && obj.declarant.userName) || '';
  return {
    id: (obj && obj.id) || '',
    content: (obj && obj.description) || '',
    declarant: userName,
    auteur: userName,
    ouvert: userName,
    dateCreation: (obj && obj.dateCreation && moment(obj.dateCreation).format('DD/MM/YYYY')) || '',
    dateModification: (obj && obj.dateModification) || '',
    fichiers:
      (obj &&
        obj.prisEnCharge &&
        obj.prisEnCharge.fichiersJoints !== null &&
        obj.prisEnCharge.fichiersJoints.length > 0 &&
        obj.prisEnCharge.fichiersJoints) ||
      [],
    origine: (obj && obj.origine && origin[obj.origine]) || '',
    motif: '',
    statut: (obj && obj.status && statut[obj.status]) || '',
  };
};

export default GetData;
