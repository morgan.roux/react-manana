import React, { FC } from 'react';
import useStyles from './styles';
import { useValueParameterAsBoolean } from '../../../../../../utils/getValueParameter';
import {
  PARTNER_LABORATORY_FILTER_NAME,
  PARTNER_LABORATORY_ADDRESS,
  PARTNER_LABORATORY_FILTER_CP,
  PARTNER_LABORATORY_FILTER_CITY,
  SEARCH_FIELDS_OPTIONS,
} from '../constants';
import { FieldsOptions } from '../../../../../Common/CustomContent/interfaces';
import SearchFilter from '../../../../../Common/newCustomContent/SearchFilter';

interface ListesLaboPartenaireFilterProps {
  fieldsState: any;
  handleFieldChange: (event: any) => void;
  handleRunSearch: (query: any) => void;
  handleResetFields: () => void;
}

const ListesLaboPartenaireFilter: FC<ListesLaboPartenaireFilterProps> = ({
  fieldsState,
  handleFieldChange,
  handleRunSearch,
  handleResetFields,
}) => {
  const classes = useStyles({});

  const objectState = {
    nomLaboState: useValueParameterAsBoolean(PARTNER_LABORATORY_FILTER_NAME),
    adresseState: useValueParameterAsBoolean(PARTNER_LABORATORY_ADDRESS),
    codePostalState: useValueParameterAsBoolean(PARTNER_LABORATORY_FILTER_CP),
    cityState: useValueParameterAsBoolean(PARTNER_LABORATORY_FILTER_CITY),
  };

  const filterSearchFields = (objectState: any, searchFields: FieldsOptions[]): FieldsOptions[] => {
    const activeStateKeys = Object.keys(objectState).filter(key => objectState[key] === true);
    const activeSearchFields = searchFields.filter((field: FieldsOptions) =>
      activeStateKeys.includes(`${field.name}State`),
    );
    return activeSearchFields;
  };

  const activeSearcFields: FieldsOptions[] = filterSearchFields(objectState, SEARCH_FIELDS_OPTIONS);

  return (
    <SearchFilter
      fieldsState={fieldsState}
      handleFieldChange={handleFieldChange}
      searchInputs={activeSearcFields}
      labelWidth={55}
      handleRunSearch={handleRunSearch}
      handleResetFields={handleResetFields}
    />
  );
};

export default ListesLaboPartenaireFilter;
