import { useLazyQuery, useApolloClient, QueryLazyOptions, useQuery } from '@apollo/react-hooks';
// import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';
import { RouteComponentProps, withRouter } from 'react-router';
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import WithSearch from '../../../../Common/newWithSearch/withSearch';
import useStyles from './styles';
import { Column } from '../../../../Dashboard/Content/Interface';
import { DO_SEARCH_USERS } from '../../../../../graphql/User';
import {
  SEARCH_USERS,
  SEARCH_USERSVariables,
} from '../../../../../graphql/User/types/SEARCH_USERS';
import SearchInput from '../../../../Common/newCustomContent/SearchInput';
import { Box, Button, Typography } from '@material-ui/core';
import SubToolbar from '../../../../Common/newCustomContent/SubToolbar';
import CustomContent from '../ListesGroupeAmis/Children/CustomContent';
import {
  DO_SEARCH_LABORATOIRE,
  GET_LIST_LABORATOIRE_PARTENAIRE,
} from '../../../../../graphql/Laboratoire/query';
import {
  LIST_LABORATOIRE_PARTENAIRE,
  LIST_LABORATOIRE_PARTENAIREVariables,
} from '../../../../../graphql/Laboratoire/types/LIST_LABORATOIRE_PARTENAIRE';

import { SHAREINFO_DETAILS_URL, SHAREINFO_ITEMS_CHOICE_URL } from '../../../../../Constant/url';

interface ListesLaboPartenaireProps {
  listResult?: any;
  valueBtn?: any;
  match: {
    params: {
      itemId: string | undefined;
    };
  };
}

const ListesLaboPartenaire: FC<ListesLaboPartenaireProps & RouteComponentProps> = ({
  history: { goBack, push },
  match: {
    params: { itemId },
  },
}) => {
  const client = useApolloClient();
  const classes = useStyles({});

  const [should, setShould] = useState<[]>();

  const columns: Column[] = [
    {
      name: 'nomLabo',
      label: 'Nom laboratoire',
    },
    {
      name: 'adresse',
      label: 'Adresse',
      renderer: (row: any) => {
        return (row.laboSuite && row.laboSuite.adresse) || '-';
      },
    },
    {
      name: 'codePostal',
      label: 'CP',
      renderer: (row: any) => {
        return (row.laboSuite && row.laboSuite.codePostal) || '-';
      },
    },
    {
      name: 'ville',
      label: 'Ville',
      renderer: (row: any) => {
        return (row.laboSuite && row.laboSuite.ville) || '-';
      },
    },
    {
      name: 'country',
      label: 'Pays',
    },
    {
      name: 'dateDebut',
      label: 'Date début de partenariat',
    },
    {
      name: 'dateFin',
      label: 'Date fin de partenariat',
    },
    {
      name: 'typePart',
      label: 'Type de partenariat',
    },
    {
      name: 'statut',
      label: 'Statut de partenariat',
      renderer: (row: any) => {
        return (row.user && row.user.statut) || '-';
      },
    },
  ];

  const { data: userData, loading: userLoading, refetch: refreshData } = useQuery<
    LIST_LABORATOIRE_PARTENAIRE,
    LIST_LABORATOIRE_PARTENAIREVariables
  >(GET_LIST_LABORATOIRE_PARTENAIRE, {
    fetchPolicy: 'network-only',
    onError: error => {
      error.graphQLErrors.map(err => {
        // displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const [selected, setSelected] = useState(itemId ? [itemId] : []);

  const tableProps = {
    isSelectable: true,
    paginationCentered: false,
    columns,
    selected,
    setSelected,
  };
  const handleBack = () => {
    push(SHAREINFO_ITEMS_CHOICE_URL);
  };
  const handleClick = () => {
    push({
      pathname: `${SHAREINFO_DETAILS_URL}/LABO/comments/${selected[0]}`,
    });
  };
  return (
    <Box className={classes.container}>
      <SubToolbar
        dark={false}
        withBackBtn={true}
        onClickBack={handleBack}
        title="Partage des informations"
      >
        {selected && selected.length > 0 && (
          <Button
            variant="contained"
            color="secondary"
            className={classes.buttonValider1}
            disabled={selected && selected.length < 0}
            onClick={handleClick}
          >
            VALIDER MON CHOIX
          </Button>
        )}
      </SubToolbar>
      <Box className={classes.listRoot}>
        <Box className={classes.noPubContainer}>
          <Typography className={classes.noPubTitle}>Liste des laboratoires partenaires</Typography>
          <Typography className={classes.noPubTexte}>
            Choisissez un laboratoire parmi la liste.
          </Typography>
        </Box>
        <Box className={classes.searchInputBox}>
          <SearchInput searchPlaceholder="Rechercher un contact" />
        </Box>
        <WithSearch
          type="laboratoire"
          props={tableProps}
          WrappedComponent={CustomContent}
          searchQuery={GET_LIST_LABORATOIRE_PARTENAIRE}
          // optionalShould={filters}
          minimumShouldMatch={0}
          optionalMust={[]}
        />
      </Box>
    </Box>
  );
};
export default withRouter(ListesLaboPartenaire);
