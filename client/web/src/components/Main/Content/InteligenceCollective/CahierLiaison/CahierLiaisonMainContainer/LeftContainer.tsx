import { DebouncedSearchInput } from '@app/ui-kit';
import {
  Box,
  CssBaseline,
  FormControlLabel,
  Hidden,
  InputAdornment,
  OutlinedInput,
  Switch,
} from '@material-ui/core';
import { Add, Search } from '@material-ui/icons';
import classnames from 'classnames';
import { uniqBy } from 'lodash';
import React, { FC, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { CAHIER_LIAISON_URL } from '../../../../../../Constant/url';
import {
  SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON,
  SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISONVariables,
} from '../../../../../../graphql/CahierLiaison/types/SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON';
import CustomButton from '../../../../../Common/CustomButton';
import NoItemListImage from '../../../../../Common/NoItemListImage';
import useStyles from '../../PartageIdeeBonnePratique/PartageIdeeBonnePratiqueMain/LeftContainer/styles';
import { IStateForm } from '../CahierLiaisonMain';
import Item from './Item';

export interface LeftContainerProps {
  data: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON | undefined;
  activeItem: any;
  fetchMore: any;
  refetch: any;
  queryVariables: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISONVariables;
  handleClickItem: (item: any, event: any) => void;
  handleSearchTxt: (arg0: any) => void;
  setOpenFormModal?: (value: boolean) => void;
  setState?: (value: any) => void;
  setFichiersJoints?: (value: any) => void;
  activateFilter: boolean;
  setActivateFilter: (activate: boolean) => void;
  initialStateValue: IStateForm;
}

const LeftContainer: FC<LeftContainerProps & RouteComponentProps> = ({
  data,
  history,
  activeItem,
  fetchMore,
  refetch,
  queryVariables,
  handleClickItem,
  handleSearchTxt,
  setOpenFormModal,
  setState,
  setFichiersJoints,
  activateFilter,
  setActivateFilter,
  initialStateValue,
}) => {
  const classes = useStyles();
  const { push } = history;

  const list = (data && data.search && data.search.data) || [];
  const total = (data && data.search && data.search.total) || 0;

  const [searchTxt, setSearchTxt] = useState('');

  const handleSearch = value => {
    setSearchTxt(value);
    handleSearchTxt(value);
  };

  const handleClickAdd = () => {
    push(`${CAHIER_LIAISON_URL}/create`);
    if (setState) setState(initialStateValue);
    if (setFichiersJoints) setFichiersJoints([]);
  };

  const realList = uniqBy([...list], 'id');

  if (realList.length > 0 && realList.length < total) {
    const el = document.getElementById('leftContainerId-451');
    if (el && realList.length > 0 && realList.length < total) {
      el.addEventListener('scroll', () => {
        if (el.scrollHeight - el.scrollTop === el.clientHeight) {
          if (fetchMore) {
            fetchMore({
              variables: { ...queryVariables, skip: realList.length },
              updateQuery: (prev: any, { fetchMoreResult }) => {
                if (!fetchMoreResult) return prev;
                const prevData = (prev.search && prev.search.data) || [];
                const nextData =
                  (fetchMoreResult && fetchMoreResult.search && fetchMoreResult.search.data) || [];
                const newData = [...prevData, ...nextData];

                return { ...prev, search: { ...prev.search, data: newData } } as any;
              },
            });
          }
        }
      });
    }
  }

  return (
    <Box className={classes.leftContainer}>
      <CssBaseline />
      <Hidden smDown={true} implementation="css">
        <Box
          className={classnames(classes.mainContainerHeader, classes.leftContainerHeader)}
          justifyContent="center !important"
        >
          <CustomButton color="secondary" startIcon={<Add />} onClick={handleClickAdd}>
            Nouvelle information
          </CustomButton>
        </Box>
      </Hidden>
      <Hidden mdUp={true} implementation="css">
        <Box width="100vw" display="flex" justifyContent="center">
          <DebouncedSearchInput placeholder={'Rechercher ...'}
            wait={500} outlined onChange={handleSearch} value={searchTxt} />


           {/* <OutlinedInput
            placeholder={'Rechercher ...'}
            endAdornment={
              <InputAdornment position="end">
                <Search />
              </InputAdornment>
            }
            value={searchTxt}
            onChange={handleSearch}
            labelWidth={0}
          />
          <FormControlLabel
            control={
              <Switch
                size="small"
                checked={activateFilter}
                onChange={() => setActivateFilter(!activateFilter)}
              />
            }
            label="Fitlré"
            labelPlacement="bottom"
          />*/}
        </Box>
      </Hidden>

      <Box className={classes.itemsContainer} id="leftContainerId-451">
        {realList.length > 0 ? (
          realList.map((item, index) => {
            return (
              <Item
                key={(item as any)?.id || index}
                item={item}
                activeItem={activeItem}
                handleClickItem={handleClickItem}
                refetch={refetch}
                setOpenFormModal={setOpenFormModal}
                setState={setState}
                setFichiersJoints={setFichiersJoints}
              />
            );
          })
        ) : (
          <Box
            minHeight="calc(100vh - 162px)"
            width="100%"
            display="flex"
            alignItems="center"
            justifyContent="center"
          >
            <NoItemListImage
              title="Aucune information dans la liste"
              subtitle="Crées en une en cliquant sur le bouton d'en haut."
            />
          </Box>
        )}
      </Box>
    </Box>
  );
};

export default withRouter(LeftContainer);
