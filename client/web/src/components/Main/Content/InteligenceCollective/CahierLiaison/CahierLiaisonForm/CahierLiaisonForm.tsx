import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import { Box, Typography } from '@material-ui/core';
import { AxiosResponse } from 'axios';
import classnames from 'classnames';
import { debounce, uniqBy } from 'lodash';
import React, {
  ChangeEvent,
  Dispatch,
  FC,
  Fragment,
  SetStateAction,
  useEffect,
  useRef,
  useState,
} from 'react';
import ReactQuill from 'react-quill';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import mutationSuccessImg from '../../../../../../assets/img/mutation-success.png';
import { GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS } from '../../../../../../federation/demarche-qualite/origine/query';
import { ME_me } from '../../../../../../graphql/Authentication/types/ME';
import { DO_SEARCH_GROUPES_AMIS_WITH_MINIM_INFO } from '../../../../../../graphql/GroupAmis';
import {
  SEARCH_GROUPES_AMIS_WITH_MINIM_INFO,
  SEARCH_GROUPES_AMIS_WITH_MINIM_INFOVariables,
} from '../../../../../../graphql/GroupAmis/types/SEARCH_GROUPES_AMIS_WITH_MINIM_INFO';
import { DO_SEARCH_LABORATOIRE } from '../../../../../../graphql/Laboratoire/query';
import {
  SEARCH_LABORATOIRE,
  SEARCH_LABORATOIREVariables,
} from '../../../../../../graphql/Laboratoire/types/SEARCH_LABORATOIRE';
import { GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE } from '../../../../../../graphql/PartenairesServices/query';
import {
  SEARCH_CUSTOM_CONTENT_PARTENAIRE,
  SEARCH_CUSTOM_CONTENT_PARTENAIREVariables,
} from '../../../../../../graphql/PartenairesServices/types/SEARCH_CUSTOM_CONTENT_PARTENAIRE';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { GET_MINIM_SERVICES } from '../../../../../../graphql/Service';
import { MINIM_SERVICES } from '../../../../../../graphql/Service/types/MINIM_SERVICES';
import SnackVariableInterface from '../../../../../../Interface/SnackVariableInterface';
import { getGroupement, getUser } from '../../../../../../services/LocalStorage';
import { uploadToS3 } from '../../../../../../services/S3';
import {
  FichierInput,
  TakeChargeInformationLiaisonInput,
  TypeFichier,
} from '../../../../../../types/graphql-global-types';
import { formatFilenameWithoutDate } from '../../../../../../utils/filenameFormater';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import Backdrop from '../../../../../Common/Backdrop';
import CommonFieldsForm from '../../../../../Common/CommonFieldsForm';
import CustomAutocomplete from '../../../../../Common/CustomAutocomplete';
import CustomButton from '../../../../../Common/CustomButton';
import { CustomCheckbox } from '../../../../../Common/CustomCheckbox';
import CustomSelect from '../../../../../Common/CustomSelect';
import AssignTaskUserModal from '../../../../../Common/CustomSelectUser';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import Dropzone from '../../../../../Common/Dropzone';
import FormValuesButtons from '../../../../../Common/FormButtons';
import NoItemContentImage from '../../../../../Common/NoItemContentImage';
import PartenaireInput from '../../../../../Common/PartenaireInput';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { ActualiteInterface, ProjectInterface } from '../../../Actualite/Interface';
import Autocomplete from '../../../Messagerie/SideBar/FormMessage/Autocomplete';
import { LaboratoireInterface } from '../../../OperationCommerciale/CreateOC/Interface/AllValuesInterface';
import PriseEnCharge from '../DetailsInformation/Modals/PriseEnCharge';
import useTakeChargeInformationLiaison from '../utils/useTakeChargeInformationLiaison';
import { AppAuthorization } from './../../../../../../services/authorization';
import { GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS as GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS_TYPE } from '../../../../../../federation/demarche-qualite/origine/types/GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS';
import useStyles from './styles';
import { GET_INFORMATION_LIAISON_TYPES } from '../../../../../../federation/basis/information-liaison/query';
import {
  INFORMATION_LIAISON_TYPES,
  INFORMATION_LIAISON_TYPESVariables,
} from '../../../../../../federation/basis/information-liaison/types/INFORMATION_LIAISON_TYPES';
import OrigineInput from '../../../../../Common/OrigineInput/OrigineInput';
import { FormButtons } from '@app/ui-kit';

export interface InputInterface {
  name: string;
  value: any;
  autocompleteValue?: any;
  label?: string;
  type?: 'select' | 'date' | 'text' | 'checkbox';
  placeholder?: string;
  inputType?: string;
  selectOptions?: any[];
  selectOptionIdKey?: string;
  selectOptionValueKey?: string;
  required?: boolean;
}

export interface PrestataireInterface {
  id: string;
  nom: string;
}

export interface FormInputInterface {
  title: string;
  inputs: InputInterface[];
}

export interface CahierLiaisonFormProps {
  isOnCreate: boolean;
  isOnEdit: boolean;
  disabledConfirm: boolean;
  state: any;
  informationLiaison: any;
  fichiersJoints: File[];
  setFichiersJoints: Dispatch<SetStateAction<File[]>>;
  stateFormData: (arg0: any) => void;
  setState: (arg0: any) => void;
  tache: any;
  setTache: (tache: any) => void;
  onConfirm: () => void;
  onCancel: () => void;
  withButtons?: boolean;
}

const initialLaboratoire: LaboratoireInterface = {
  id: '',
  nom: '',
  __typename: 'Laboratoire',
};

const initialProject: ProjectInterface = {
  id: '',
  name: '',
  __typename: 'Project',
};

const CahierLiaisonForm: FC<CahierLiaisonFormProps & RouteComponentProps> = ({
  stateFormData,
  state,
  setState,
  history: { push },
  informationLiaison,
  fichiersJoints,
  setFichiersJoints,
  tache,
  setTache,
  onConfirm,
  onCancel,
  disabledConfirm,
  withButtons = true,
}) => {
  const currentUser: ME_me = getUser();
  const currentUserId = (currentUser && currentUser.id) || '';
  const currentUserName = (currentUser && currentUser.userName) || '';
  const auth = new AppAuthorization(currentUser);

  const initialState: ActualiteInterface = {
    libelle: '',
    laboratoire: initialLaboratoire,
    idLaboratoire: null,
    description: '',
    dateDebut: null,
    dateFin: null,
    codeOrigine: '',
    id: undefined,
    actionAuto: undefined,
    niveauPriorite: undefined,
    globalite: undefined,
    idGroupement: undefined,
    project: initialProject,
    idProject: null,
    actionPriorite: null,
    actionDescription: null,
    actionDueDate: null,
    fichierPresentations: undefined,
    fichierCible: undefined,
    idTache: null,
    idFonction: null,
    idUrgence: '',
    idImportance: '',
    tache: null,
    selectedCollegues: [],
    isCheckedTeam: false,
  };

  const initialStatePrestataire: PrestataireInterface = {
    id: '',
    nom: '',
  };

  const classes = useStyles({});
  const client = useApolloClient();
  const [valueBloquant, setvalueBloquant] = useState<boolean>();

  const groupement = getGroupement();
  const groupementId = (groupement && groupement.id) || '';

  const informationLiaisonTypesQuery = useQuery<
    INFORMATION_LIAISON_TYPES,
    INFORMATION_LIAISON_TYPESVariables
  >(GET_INFORMATION_LIAISON_TYPES, { client: FEDERATION_CLIENT });
  const informationLiaisonTypes = informationLiaisonTypesQuery.data?.informationLiaisonTypes.nodes || [];

  const typebloquantList = [
    { id: 1, nom: 'Oui', checked: false },
    { id: 2, nom: 'Non', checked: true },
  ];

  const originesQuery = useQuery<GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS_TYPE, any>(
    GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS,
    {
      client: FEDERATION_CLIENT,
      fetchPolicy: 'cache-and-network',
    },
  );

  const originList = originesQuery.data?.origines;

  // if (auth.isSupAdmin) {
  //   originList.push({
  //     id: '7',
  //     nom: 'Digital4win',
  //   });
  // }

  const statutList = [
    {
      nom: 'Non lues',
      id: 'NON_LUES',
    },
    {
      nom: 'Lues',
      id: 'LUES',
    },
    // {
    //   nom: 'En charge',
    //   id: 'EN_CHARGE',
    // },
    // {
    //   nom: 'Clôturé',
    //   id: 'CLOTURE',
    // },
  ];

  const formChange = event => {
    if (event.target) {
      const { value, name, checked } = event.target;
      // if (name === 'statutId' && value === 'EN_CHARGE')
      if (name === 'statut' && value === 'CLOTURE') {
        setopenModal(!openModal);
      } else if (name === 'bloquant') {
        setState(prevState => ({ ...prevState, [name]: checked }));
      } else {
        setState(prevState => ({ ...prevState, [name]: value }));
      }
    }
  };

  const handleBloquantState = value => {
    setBloquantState(value);
    let data = '';
    value.map(v => {
      if (v.checked) data = v.nom;
    });
    if (data !== '') {
      const bloquant = data === 'Oui';
      setvalueBloquant(bloquant);
      setState(prevState => ({ ...prevState, ['bloquant']: bloquant }));
    }
  };

  const [values, setValues] = useState<ActualiteInterface>(initialState);
  const [valuePrestataire, setValuesPrestataire] = useState<PrestataireInterface>(
    initialStatePrestataire,
  );
  const [showSelectableCible, setShowSelectableCible] = useState<boolean>(false);

  const [queryTextLaboratoire, setQueryTextLaboratoire] = useState<string>('');
  const [queryTextPartenaire, setQueryTextPartenaire] = useState<string>('');
  const laboSortBy = [{ nomLabo: { order: 'asc' } }];
  const laboFilterBy = [{ term: { sortie: 0 } }];
  const laboType = ['laboratoire'];
  const partenaireType = ['partenaire'];

  const groupeAmisType = ['groupeamis'];
  const [queryTextGroupeAmis, setQueryTextGroupeAmis] = useState<string>('');
  const groupeAmisFilterBy = [
    { term: { isRemoved: false } },
    { term: { 'groupement.id': groupementId } },
  ];

  const [groupeAmis, setGroupeAmis] = useState<any>(null);

  const [openConcernedParticipantModal, setOpenConcernedParticipantModal] = useState<boolean>(
    false,
  );

  const { data: dataService, loading: loadingService } = useQuery<MINIM_SERVICES>(
    GET_MINIM_SERVICES,
  );

  const [openDeclarantModal, setOpenDeclarantModal] = useState<boolean>(false);

  const [selectedDeclarant, setSelectedDeclarant] = useState<any[]>([currentUser]);
  const [selectedCollegues, setSelectedCollegues] = useState<any[]>([]);
  const [isCheckedTeam, setIsCheckedTeam] = useState<boolean>(false);

  const [openModal, setopenModal] = useState<boolean>(false);

  const [selectStatueValue, setSelectStatueValue] = useState(state.status);

  const [bloquantState, setBloquantState] = useState(typebloquantList);

  const [loading, setLoading] = useState<boolean>(false);
  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);
  let files: FichierInput[] = [];
  let uploadResult: AxiosResponse<any> | null = null;

  const [stateTakeCharge, setStateTakeCharge] = useState({
    date: '',
    description: '',
    files,
  });

  const takeCharge: TakeChargeInformationLiaisonInput = {
    id: state && state.id,
    date: stateTakeCharge.date,
    description: stateTakeCharge.description,
    files: stateTakeCharge.files,
  };

  // Get laboratoires list
  const [searchLaboratoires, searchLaboratoiresResult] = useLazyQuery<
    SEARCH_LABORATOIRE,
    SEARCH_LABORATOIREVariables
  >(DO_SEARCH_LABORATOIRE, {
    fetchPolicy: 'cache-first',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        /*const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
        */
      });
    },
  });

  // Get laboratoires partenaire list
  const [searchPartenaires, searchPartenairesResult] = useLazyQuery<
    SEARCH_CUSTOM_CONTENT_PARTENAIRE,
    SEARCH_CUSTOM_CONTENT_PARTENAIREVariables
  >(GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE, {
    fetchPolicy: 'cache-first',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        /* const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
        */
      });
    },
  });

  // Get groupe amis list
  const [searchGroupeAmis, { data: groupeAmisData, loading: groupeAmisLoading }] = useLazyQuery<
    SEARCH_GROUPES_AMIS_WITH_MINIM_INFO,
    SEARCH_GROUPES_AMIS_WITH_MINIM_INFOVariables
  >(DO_SEARCH_GROUPES_AMIS_WITH_MINIM_INFO, {
    fetchPolicy: 'cache-first',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        });
      });
    },
  });

  const debouncedSearch = useRef(
    debounce((query: string, dataType: 'laboratoire' | 'partenaire') => {
      if (query.length > 0) {
        switch (dataType) {
          case 'laboratoire':
            searchLaboratoires({
              variables: {
                type: laboType,
                query,
                sortBy: laboSortBy,
                filterBy: laboFilterBy,
              },
            });
            break;
          case 'partenaire': {
            searchPartenaires({
              variables: {
                type: laboType,
                query,
              },
            });
          }
        }
      }
    }, 1000),
  );

  const debouncedSearchGroupeAmis = useRef(
    debounce((keyword: string) => {
      searchGroupeAmis({
        variables: {
          type: groupeAmisType,
          filterBy:
            keyword.length > 0
              ? [...groupeAmisFilterBy, { wildcard: { nom: `*${keyword}*` } }]
              : groupeAmisFilterBy,
          take: null,
        },
      });
    }, 1000),
  );

  // Debounced Search Laboratoire
  useEffect(() => {
    debouncedSearch.current(queryTextLaboratoire, 'laboratoire');
  }, [queryTextLaboratoire]);

  // HIDE SELECTABLE CIBLE
  useEffect(() => {
    setShowSelectableCible(false);
    searchLaboratoires({
      variables: {
        type: laboType,
        take: -1,
        query: queryTextLaboratoire,
        sortBy: laboSortBy,
        filterBy: laboFilterBy,
      },
    });

    searchGroupeAmis({
      variables: {
        type: groupeAmisType,
        take: 10,
        filterBy: groupeAmisFilterBy,
      },
    });
  }, []);

  // Debounced Search Partenaire
  useEffect(() => {
    debouncedSearch.current(queryTextPartenaire, 'partenaire');
  }, [queryTextPartenaire]);

  // HIDE SELECTABLE CIBLE
  useEffect(() => {
    setShowSelectableCible(false);
    searchPartenaires({
      variables: {
        type: partenaireType,
        take: -1,
        query: queryTextPartenaire,
      },
    });
  }, []);

  useEffect(() => {
    stateFormData(state);
  }, [state]);

  const [uploadFiles, uploadFilesResult] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (data && data.createPutPresignedUrls && data.createPutPresignedUrls.length > 0) {
        Promise.all(
          data.createPutPresignedUrls.map(async (presigned, index) => {
            if (presigned && selectedFiles.length > 0) {
              await Promise.all(
                selectedFiles.map(async file => {
                  if (presigned.filePath.includes(formatFilenameWithoutDate(file))) {
                    const type: TypeFichier =
                      file.type && file.type.includes('pdf')
                        ? TypeFichier.PDF
                        : file.type.includes('xlsx')
                          ? TypeFichier.EXCEL
                          : TypeFichier.PHOTO;
                    const newFile: FichierInput = {
                      id: '',
                      type,
                      nomOriginal: file.name,
                      chemin: presigned.filePath,
                    };

                    files = uniqBy([...files, newFile], 'chemin');

                    setStateTakeCharge(prevState => ({ ...prevState, ['files']: files }));

                    await uploadToS3(selectedFiles[index], presigned.presignedUrl)
                      .then(result => {
                        if (result && result.status === 200) {
                          uploadResult = result;
                        }
                      })
                      .catch(error => {
                        setLoading(false);
                        console.log('error :>> ', error);
                        displaySnackBar(client, {
                          type: 'ERROR',
                          message: "Erreur lors de l'envoye de(s) fichier(s)",
                          isOpen: true,
                        });
                      });
                  }
                }),
              );
            }
          }),
        ).then(_ => {
          // Execute doCreateUpdate
          if ((uploadResult && uploadResult.status === 200) || files) {
            takeChargeInformationLiaison();
            setLoading(false);
          } else {
            setLoading(false);
          }
        });
      } else {
        takeChargeInformationLiaison();
      }
    },
    onError: errors => {
      setLoading(false);
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const {
    takeChargeInformationLiaison,
    mutationTakeChargeSuccess,
  } = useTakeChargeInformationLiaison(takeCharge);

  const handleAddClickTakeCharge = () => {
    setLoading(true);
    if (selectedFiles && selectedFiles.length > 0) {
      // Presigned file
      const filePathsTab: string[] = [];
      selectedFiles.map((file: File) => {
        if (file.type) {
          filePathsTab.push(
            `priseencharge/${state && state.id}/${formatFilenameWithoutDate(file)}`,
          );
        }
      });
      // Create presigned url
      uploadFiles({ variables: { filePaths: filePathsTab } });
    } else {
      takeChargeInformationLiaison();
    }
    setSelectStatueValue('CLOTURE');
  };

  const handleOpenDeclarant = event => {
    event.stopPropagation();
    setOpenDeclarantModal(true);
  };

  const mutationSuccessTitle = `${"Modification de l'information réussie"}`;
  const goBack = () => {
    push('/intelligence-collective/cahier-de-liaison');
  };

  const ActionSuccess = () => {
    return (
      <Box className={classes.mutationSuccessContainer}>
        <NoItemContentImage src={mutationSuccessImg} title={mutationSuccessTitle} subtitle={''}>
          <CustomButton color="default" onClick={goBack}>
            Retour à la liste
          </CustomButton>
        </NoItemContentImage>
      </Box>
    );
  };

  /* const handleChangeLaboratoireAutocomplete = (
    e: ChangeEvent<any>,
    inputValue: LaboratoireInterface & string,
  ) => {
    if (e && e.type === 'click' && inputValue && inputValue.id) {
      setValues(prevState => ({ ...prevState, laboratoire: inputValue }));
      setValues(prevState => ({ ...prevState, idLaboratoire: inputValue.id }));
      setState(prevState => ({ ...prevState, ['provider']: inputValue.id }));
    }
    if (e && e.type === 'change' && inputValue !== undefined && typeof inputValue === 'string') {
      setValues(prevState => ({
        ...prevState,
        laboratoire: { id: '', nomLabo: inputValue, __typename: 'Laboratoire' },
      }));
      setValues(prevState => ({ ...prevState, idLaboratoire: null }));
      setState(prevState => ({ ...prevState, ['provider']: '' }));
    }
  };
*/

  const handleChangePartenaireAutocomplete = (inputValue: any) => {
    setState(prevState => ({ ...prevState, ['serviceProvider']: inputValue.id }));
    const data = {
      id: inputValue.id,
      nom: inputValue.nom,
    };
    setValuesPrestataire(prevState => ({ ...prevState, ...data }));
  };

  const handleChangeAutocomplete = (name: string) => (e: React.ChangeEvent<any>, value: any) => {
    setGroupeAmis(value);
  };

  const handleChangeGroupeAmisText = (name: string) => (e: ChangeEvent<any>, value: string) => {
    debouncedSearchGroupeAmis.current(value);
  };

  // Set groupe amis id
  React.useEffect(() => {
    if (groupeAmis && groupeAmis.id) {
      setState(prev => ({ ...prev, friendGroups: groupeAmis.id }));
    } else {
      setState(prev => ({ ...prev, friendGroups: '' }));
    }
  }, [groupeAmis]);

  // set declaringName
  React.useEffect(() => {
    const declarant = selectedDeclarant[0];
    if (declarant) {
      setState(prevState => ({ ...prevState, declaringName: declarant.userName }));
      setState(prevState => ({ ...prevState, declaringId: declarant.id }));
    }
  }, [selectedDeclarant]);

  // isCheckedTeam, selectedCollegues
  React.useEffect(() => {

    if (isCheckedTeam) {
      setState(prevState => ({
        ...prevState,
        concernedColleagueName: 'all',
        teamAll: true,
        idColleguesConcernees: selectedCollegues
          .map(user => user.id),
      }));
    }
    else if (selectedCollegues.length > 0) {
      setState(prevState => ({
        ...prevState,
        teamAll: false,
        idColleguesConcernees: selectedCollegues.map(i => i.id),
        concernedColleagueName: selectedCollegues.map(i => i.userName).join(', '),
      }));
    }


  }, [isCheckedTeam, selectedCollegues]);

  // Set groupes amis on edit
  React.useEffect(() => {
    if (informationLiaison && informationLiaison.groupeAmis) {
      setGroupeAmis(informationLiaison.groupeAmis);
    }
    if (
      informationLiaison &&
      informationLiaison.colleguesConcernees &&
      informationLiaison.colleguesConcernees.length > 0
    ) {
      const newSelectedCollegues = informationLiaison.colleguesConcernees.map(i => i.userConcernee);
      setSelectedCollegues(newSelectedCollegues);
    }
  }, [informationLiaison]);

  const handleChangeDescription = (content: string) => {
    setState(prevState => ({ ...prevState, description: content }));
  };

  if (mutationTakeChargeSuccess) return <ActionSuccess />;

  return (
    <div className={classes.cahierLiaisonFormRoot}>
      {(loading || uploadFilesResult.loading) && <Backdrop value={'Opération en cours...'} />}
      <PriseEnCharge
        open={openModal}
        setSelectedFiles={setSelectedFiles}
        selectedFiles={selectedFiles}
        stateTakeCharge={stateTakeCharge}
        setStateTakeCharge={setStateTakeCharge}
        setOpen={setopenModal}
        handleAddClickTakeCharge={handleAddClickTakeCharge}
      />
      <div className={classes.formContainer}>
        <Fragment key={`cahier_liaison_form_item_1`}>
          <Typography className={classes.inputTitle}>Information de liaison</Typography>

          <CustomSelect
            label="Type"
            list={informationLiaisonTypes}
            listId="id"
            index="libelle"
            name="idType"
            value={state.idType}
            onChange={formChange}
            required={true}
          />

          <OrigineInput
            origine={state.idOrigine}
            origineAssocie={state.idOrigineAssocie}
            onChangeOrigine={(origine) => formChange({
              target: {
                value: origine?.id, name: 'idOrigine'
              }
            })}

            onChangeOrigineAssocie={(origineAssocie) => formChange({
              target: {
                value: origineAssocie?.id, name: 'idOrigineAssocie'
              }
            })}
          />

          {/*<CustomSelect
            label="Origine"
            list={originList}
            listId="id"
            index="libelle"
            name="originId"
            value={state.originId}
            onChange={formChange}
            required={true}
          />*/}

          <Box width="100%" marginBottom="16px">
            <CommonFieldsForm
              style={{ padding: 0 }}
              selectedUsers={selectedCollegues}
              urgence={state.idUrgence}
              urgenceProps={{
                useCode: false,
              }}
              selectUsersFieldLabel="Collègue(s) concerné(s)"
              usersModalProps={{
                withNotAssigned: false,
                title: 'Collègue(s) concerné(s)',
                searchPlaceholder: 'Rechercher...',
                assignTeamText: "Toute l'équipe",
                withAssignTeam: true,
                singleSelect: false,
                isCheckedTeam: isCheckedTeam,
                setIsCheckedTeam: setIsCheckedTeam,
              }}
              projet={tache?.id || state.idTache || state.idFonction}
              importance={state.idImportance}
              onChangeUsersSelection={setSelectedCollegues}
              onChangeUrgence={urgence =>
                formChange({ target: { name: 'idUrgence', value: urgence?.id } })
              }
              onChangeImportance={importance =>
                formChange({ target: { name: 'idImportance', value: importance?.id } })
              }
              onChangeProjet={projet => {
                setState(prevState => ({
                  ...prevState,
                  idTache: projet?.idTache,
                  idFonction: projet?.idFonction,
                }));
                setTache(projet);
              }}
            />
          </Box>

          {state.id !== '' && (
            <CustomSelect
              label="Statut"
              list={statutList}
              listId="id"
              index="nom"
              name="statut"
              value={state.statut}
              onChange={formChange}
            />
          )}

          <CustomFormTextField
            name="titre"
            label="Titre"
            value={state.titre}
            onChange={formChange}
            required={true}
          />

          <div className={classnames(classes.marginRight, classes.w100)}>
            <ReactQuill
              className={classes.customizedReactQuill}
              theme="snow"
              value={state.description}
              onChange={handleChangeDescription}
            />
          </div>
          <CustomCheckbox
            name="bloquant"
            label="Bloquante"
            value={state.bloquant}
            checked={state.bloquant}
            onChange={formChange}
          />
        </Fragment>

        <Fragment key={`cahier_liaison_form_item_2`}>
          <Typography className={classes.inputTitle}>Intervenants</Typography>

          <AssignTaskUserModal
            openModal={openDeclarantModal}
            setOpenModal={setOpenDeclarantModal}
            withNotAssigned={false}
            selected={selectedDeclarant}
            setSelected={setSelectedDeclarant}
            title="Déclarant"
            searchPlaceholder="Rechercher..."
            assignTeamText="Toute l'équipe"
            withAssignTeam={false}
            singleSelect={true}
          />

          <CustomFormTextField
            name="declarant"
            label="Déclarant"
            value={
              currentUserName !== '' && state.declaringName === currentUserName
                ? 'Moi'
                : state.declaringName
            }
            onClick={handleOpenDeclarant}
            required={true}
          />

          {/* <ConcernedParticipant
            isOpen={openConcernedParticipantModal}
            setOpen={setOpenConcernedParticipantModal}
            title="Collègue(s) concerné(s)"
            userIdSelected={handleUserConcernedIdSelected}
            userNameSelected={handleUserConcernedNameSelected}
            setUsers={handleSetUsers}
            isOnEquip={true}
            withBtnsActions={false}
            allTeamIsCheckedByDefault={false}
          /> */}

          {/*state.originId === '3' && (
            <Autocomplete
              value={groupeAmis || ''}
              name="groupeAmis"
              multiple={false}
              handleChange={handleChangeAutocomplete}
              handleInputChange={handleChangeGroupeAmisText}
              options={
                (groupeAmisData && groupeAmisData.search && groupeAmisData.search.data) || []
              }
              optionLabel="nom"
              label="Groupe d'amis"
              required={true}
              // handleOpenAutocomplete={onOpenUserAutocomplete}
              loading={groupeAmisLoading}
              variant="outlined"
              style={{ width: '100%' }}
              className={classes.customAutomcomplete}
            />
            )*/}

          {/*state.originId === '4' && (
            <CustomSelect
              list={(dataService && dataService.services) || []}
              listId="id"
              index="nom"
              name="serviceConcerned"
              value={state.serviceConcerned}
              onChange={formChange}
              label="Service concerné"
              required={true}
            />
          )*/}

          {/*state.originId === '5' && (
             <CustomAutocomplete
              id="idProvider"
              options={
                (searchLaboratoiresResult &&
                  searchLaboratoiresResult.data &&
                  searchLaboratoiresResult.data.search &&
                  (searchLaboratoiresResult.data.search.data as any)) ||
                []
              }
              optionLabelKey="nomLabo"
              inputValue={(values && values.laboratoire && values.laboratoire.nomLabo) || ''}
              onAutocompleteChange={handleChangeLaboratoireAutocomplete}
              label="Fournisseur"
              required={true}
              disabled={false}
            />

            <PartenaireInput
              index="laboratoire"
              label="Fournisseur"
              value={values?.laboratoire?.nom}
              onChange={laboratoire => setValues(prev => ({ ...prev }))}
            />
            )*/}

          {/*state.originId === '6' && (
            <CustomAutocomplete
              id="idPartenaire"
              options={
                (searchPartenairesResult &&
                  searchPartenairesResult.data &&
                  searchPartenairesResult.data.search &&
                  (searchPartenairesResult.data.search.data as any)) ||
                []
              }
              optionLabelKey="nom"
              value={valuePrestataire}
              onAutocompleteChange={handleChangePartenaireAutocomplete}
              label="Prestataire"
              required={true}
              disabled={false}
            />
            )*/}

          <div
            className={classnames(classes.marginRight, classes.w100)}
            style={{ marginTop: state.originId !== '4' && state.originId !== '' ? 15 : 'auto' }}
          >
            <Dropzone
              contentText="Glissez et déposez vos documents"
              selectedFiles={fichiersJoints}
              setSelectedFiles={setFichiersJoints}
              multiple={true}
            // acceptFiles="application/pdf"
            />
            {withButtons && (
              <FormButtons
                onClickCancel={onCancel}
                onClickConfirm={onConfirm}
                disableConfirm={disabledConfirm}
              />
            )}
          </div>
        </Fragment>
      </div>
    </div>
  );
};

export default withRouter(CahierLiaisonForm);
