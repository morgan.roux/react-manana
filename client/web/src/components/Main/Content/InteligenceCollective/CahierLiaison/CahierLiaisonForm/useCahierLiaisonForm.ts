import { head, last, split } from 'lodash';
import { ChangeEvent, useState } from 'react';
import { CREATE_UPDATE_INFORMATION_LIAISONVariables } from '../../../../../../graphql/CahierLiaison/types/CREATE_UPDATE_INFORMATION_LIAISON';


export const initialState: CREATE_UPDATE_INFORMATION_LIAISONVariables = {
  input: {
    idType: '',
    idItem: '',
    idItemAssocie: '',
    idOrigine:'',
    idOrigineAssocie:'',
    statut: 'EN_COURS',
    titre: '',
    description: '',
    idDeclarant: '',
    idColleguesConcernees: [],
    bloquant: false,
    idFonction: '',
    idTache: '',
    idUrgence: '',
    idImportance: '',
  },
};

const useCahierLiaisonForm = (defaultState?: CREATE_UPDATE_INFORMATION_LIAISONVariables) => {
  const initValues: CREATE_UPDATE_INFORMATION_LIAISONVariables = defaultState || initialState;
  const [input, setInput] = useState<CREATE_UPDATE_INFORMATION_LIAISONVariables>(initValues);

  const otherHandleChange = (name: string, value: any, checked: boolean) => {
    const nameArray = split(name, '.');
    const nameKey: any = head(nameArray);
    const exactName: any = last(nameArray);
    if (name.includes('checkbox')) {
      if (nameKey === exactName && exactName !== 'cap') {
        setInput(prevState => ({
          ...prevState,
          [exactName]: checked,
        }));
      } else {
        setInput(prevState => ({
          ...prevState,
          [nameKey]: { ...prevState[nameKey], [exactName]: checked },
        }));
      }
    } else {
      setInput(prevState => ({
        ...prevState,
        [nameKey]: { ...prevState[nameKey], [exactName]: value },
      }));
    }
  };

  const handleChangeArray = (name: string, value: any) => {
    const exactName = head(split(name, '_'));
    const position = last(split(name, '_'));

    if (position && exactName) {
      setInput(prevState => {
        const old: any[] = prevState[exactName];
        if (old) {
          // Spice old
          old.splice(Number(position), 1, value);
          const nextState = {
            ...prevState,
            [exactName]: old.filter(i => i !== '' && i !== undefined && i !== null),
          };
          return nextState;
        } else {
          const nextState = {
            ...prevState,
            [exactName]: [value].filter(i => i !== '' && i !== undefined && i !== null),
          };
          return nextState;
        }
      });
      return;
    }
  };

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value, checked, type } = e.target;
      const isNumber = type === 'number';
      const elementValue = isNumber && parseInt(value, 10) < 0 ? '0' : value;
      if (name.includes('.')) {
        otherHandleChange(name, elementValue, checked);
        return;
      }

      if (name.includes('_')) {
        handleChangeArray(name, elementValue);
        return;
      }

      setInput(prevState => ({ ...prevState, [name]: elementValue }));
    }
  };

  const handleChangeDate = (name: string) => (date: any) => {
    if (name.includes('.')) {
      otherHandleChange(name, date, false);
      return;
    }
    setInput(prevState => ({ ...prevState, [name]: date }));
  };

  const handleChangeAutoComplete = (fakeName: string, name: string) => (
    e: ChangeEvent<any>,
    newValue: any,
  ) => {
    if (e && e.target) {
      setInput(prevState => ({ ...prevState, [fakeName]: newValue }));

      if (newValue && newValue.id) {
        if (name.includes('.')) {
          otherHandleChange(name, newValue.id, false);
          return;
        }

        // Set idGrossistes, idGeneriqueurs, ...etc
        if (name.includes('_')) {
          handleChangeArray(name, newValue.id);
          return;
        }

        setInput(prevState => ({ ...prevState, [name]: newValue.id }));
      }
    }
  };

  const handleChangeInputAutoComplete = (fakeName: string, name: string) => (
    e: ChangeEvent<any>,
    newInputValue: string,
  ) => {
    if (e && e.target) {
      // setInput(prevState => ({ ...prevState, [fakeName]: newValue }));

      if (name.includes('.')) {
        otherHandleChange(name, newInputValue, false);
        return;
      }

      // Set idGrossistes, idGeneriqueurs, ...etc
      if (name.includes('_')) {
        handleChangeArray(name, newInputValue);
        return;
      }

      setInput(prevState => ({ ...prevState, [name]: newInputValue }));
    }
  };

  return {
    handleChange,
    handleChangeDate,
    handleChangeAutoComplete,
    handleChangeInputAutoComplete,
    input,
    setInput,
  };
};

export default useCahierLiaisonForm;
