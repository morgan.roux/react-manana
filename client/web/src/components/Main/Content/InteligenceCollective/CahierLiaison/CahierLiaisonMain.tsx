import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import { Box, Hidden, IconButton } from '@material-ui/core';
import { Create } from '@material-ui/icons';
import { AxiosResponse } from 'axios';
import classnames from 'classnames';
import { uniqBy } from 'lodash';
import moment from 'moment';
import React, { Fragment, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { CAHIER_LIAISON_URL } from '../../../../../Constant/url';
import { GET_INFORMATION_LIAISON_TYPES } from '../../../../../federation/basis/information-liaison/query';
import {
  INFORMATION_LIAISON_TYPES,
  INFORMATION_LIAISON_TYPESVariables,
} from '../../../../../federation/basis/information-liaison/types/INFORMATION_LIAISON_TYPES';
import { ME_me } from '../../../../../graphql/Authentication/types/ME';
import { DO_READ_INFORMATION_LIAISON_BY_USER_CONCERNED } from '../../../../../graphql/CahierLiaison';
import {
  GET_INFORMATION_LIAISON,
  GET_SEARCH_CUSTOM_INFORMATION_LIAISON,
} from '../../../../../graphql/CahierLiaison/query';
import {
  INFORMATION_LIAISON,
  INFORMATION_LIAISONVariables,
} from '../../../../../graphql/CahierLiaison/types/INFORMATION_LIAISON';
import {
  READ_INFORMATION_LIAISON_BY_USER_CONCERNED,
  READ_INFORMATION_LIAISON_BY_USER_CONCERNEDVariables,
} from '../../../../../graphql/CahierLiaison/types/READ_INFORMATION_LIAISON_BY_USER_CONCERNED';
import {
  SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON,
  SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISONVariables,
} from '../../../../../graphql/CahierLiaison/types/SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { getGroupement, getPharmacie, getUser } from '../../../../../services/LocalStorage';
import { uploadToS3 } from '../../../../../services/S3';
import {
  FichierInput,
  InformationLiaisonInput,
  TypeFichier,
} from '../../../../../types/graphql-global-types';
import { formatFilenameWithoutDate } from '../../../../../utils/filenameFormater';
import { isMobile } from '../../../../../utils/Helpers';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import Backdrop from '../../../../Common/Backdrop';
import { ConfirmDeleteDialog } from '../../../../Common/ConfirmDialog';
import { CustomModal } from '../../../../Common/CustomModal';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import useCommonStyles from '../PartageIdeeBonnePratique/PartageIdeeBonnePratiqueMain/styles';
import CahierLiaisonForm from './CahierLiaisonForm';
import { LeftContainer, MainContainer } from './CahierLiaisonMainContainer';
import useStyles from './styles';
import { useCreateUpdateCahierLiaison, useDeleteCahierLiaison } from './utils';
import { removeEmptyString, useButtonHeadAction } from './utils/utils';

interface ICahierLiaisonProps {
  searchTxt?: string;
  statuts?: any[];
  startDate?: any;
  endDate?: any;
  stateCollegeConcernees?: any;
  ckeckedMyInfo: boolean;
  handleSearchTxt: (arg0: any) => void;
  informations: string;
  filters: any;
  activateFilter: boolean;
  setActivateFilter: (activate: boolean) => void;
  refetchCount: () => void;
}

export interface IStateForm {
  id: string;
  idType: string;
  idOrigine: string;
  idOrigineAssocie: string;
  idItem: string;
  idItemAssocie: string;
  bloquant: boolean;
  statut: string;
  titre: string;
  description: string;
  idUserDeclarant: string;
  declaringName: string;
  idColleguesConcernees: string[];
  teamAll: boolean;
  fichiers: FichierInput[];
  idImportance: string;
  idUrgence: string;
  idFonction: string;
  idTache: string;
}

const CahierLiaisonMain: React.FC<ICahierLiaisonProps & RouteComponentProps> = ({
  searchTxt,
  statuts,
  stateCollegeConcernees,
  startDate,
  endDate,
  history,
  location: { pathname },
  match: { params },
  ckeckedMyInfo,
  handleSearchTxt,
  informations,
  filters,
  activateFilter,
  setActivateFilter,
  refetchCount,
}) => {
  let files: FichierInput[] = [];
  let uploadResult: AxiosResponse<any> | null = null;

  const classes = useStyles();
  const commonClasses = useCommonStyles();

  const client = useApolloClient();
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const { push } = history;

  const [openFormModal, setOpenFormModal] = useState<boolean>(false);

  const { idliaison, from } = params as any;

  const currentUser: ME_me = getUser();
  const currentUserId = (currentUser && currentUser.id) || '';

  const groupement = getGroupement();

  const isOnCreate = pathname === `${CAHIER_LIAISON_URL}/create`;
  const isOnEdit: boolean = pathname.startsWith(`${CAHIER_LIAISON_URL}/edit`);
  // const isOnList = pathname === `${CAHIER_LIAISON_URL}`;
  const isOnList = !isOnCreate && !isOnEdit;

  // const EDIT_URL = `${CAHIER_LIAISON_URL}/edit/${idliaison}`;

  const initialStateValue: IStateForm = {
    id: '',
    idType: '',
    idOrigine: '',
    idOrigineAssocie: '',
    idItem: '',
    idItemAssocie: '',
    bloquant: false,
    statut: 'EN_COURS',
    titre: '',
    description: '',
    idUserDeclarant: (currentUser && currentUser.id) || '',
    declaringName: 'Moi',
    idColleguesConcernees: [],
    teamAll: true,
    fichiers: [],
    idFonction: '',
    idTache: '',
    idImportance: '',
    idUrgence: '',
  };

  const [state, setState] = useState(initialStateValue);

  const mutationLoadingMsg = `${!state.id || isOnCreate || (openFormModal && !idliaison)
    ? 'Création'
    : state.id || isOnEdit || (openFormModal && idliaison)
      ? 'Modification'
      : 'Suppression'
    } en cours...`;

  const [fichiersJoints, setFichiersJoints] = useState<File[]>([]);

  const [loading, setLoading] = useState<boolean>(false);

  const [activeItem, setActiveItem] = React.useState<any>(null);

  const [showMainContainer, setShowMainContainer] = useState<boolean>(isMobile() ? false : true);

  const [tache, setTache] = useState<any>();

  /*const informationLiaisonTypesQuery = useQuery<
    INFORMATION_LIAISON_TYPES,
    INFORMATION_LIAISON_TYPESVariables
  >(GET_INFORMATION_LIAISON_TYPES, { client: FEDERATION_CLIENT });
  const informationLiaisonTypes = informationLiaisonTypesQuery.data?.informationLiaisonTypes || [];
*/
  const handleClickItem = (item: any, event: any) => {
    if (item) {
      const collegueConcernee = item?.colleguesConcernees?.find(
        i => i.userConcernee.id === currentUserId,
      );

      if (collegueConcernee?.id) {
        readInfoByUserConcerned({ variables: { id: collegueConcernee.id, lu: true } });
      }
    }

    if (event) {
      event.stopPropagation();
    }
    setActiveItem(item);

    window.history.pushState(
      null,
      '',
      item
        ? `#${CAHIER_LIAISON_URL}/${informations === 'coupleStatutDeclarant' ? 'emise' : 'recue'}/${item.id
        }`
        : `#${CAHIER_LIAISON_URL}/${informations === 'coupleStatutDeclarant' ? 'emise' : 'recue'}`,
    );
    setShowMainContainer(true);
  };

  const handleChange = state => {
    setState(state);
  };


  const input: InformationLiaisonInput = {
    id: state.id,
    idType: state.idType,
    idOrigine: state.idOrigine,
    idOrigineAssocie: state.idOrigineAssocie,
    statut: state.statut,
    bloquant: state.bloquant,
    titre: state.titre,
    description: state.description,
    idDeclarant: state.idUserDeclarant,
    idColleguesConcernees:
      (state?.idColleguesConcernees?.length || 0) > 0
        ? state.idColleguesConcernees
        : tache?.participants?.map(({ id }) => id) || [],
    idFicheIncident: '',
    idFicheAmelioration: '',
    idFicheReclamation: '',
    idTodoAction: '',
    codeMaj: '',
    isRemoved: false,
    fichiers: state.fichiers,
    idImportance: state.idImportance,
    idUrgence: state.idUrgence,
    idFonction: state.idFonction,
    idTache: state.idTache,
  };


  const {
    createUpdateCahierLiaison,
    mutationLoading,
    mutationSuccess,
  } = useCreateUpdateCahierLiaison({ input: removeEmptyString(input) }, setOpenFormModal, setLoading);
  const { goToAddInformation, goBack } = useButtonHeadAction(push);

  const [readInfoByUserConcerned] = useMutation<
    READ_INFORMATION_LIAISON_BY_USER_CONCERNED,
    READ_INFORMATION_LIAISON_BY_USER_CONCERNEDVariables
  >(DO_READ_INFORMATION_LIAISON_BY_USER_CONCERNED, {
    onCompleted: data => {
      if (data && data.readInformationLiaisonByUserConcerned) {
        handleRefetch(false);
      }
    },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const { data: infoData, loading: getLoading, refetch: refetchInformation } = useQuery<
    INFORMATION_LIAISON,
    INFORMATION_LIAISONVariables
  >(GET_INFORMATION_LIAISON, {
    fetchPolicy: 'network-only',
    variables: { id: idliaison },
    skip: !idliaison,
    onCompleted: data => {
      if (data?.informationLiaison && !activeItem) {
        setActiveItem(data.informationLiaison);
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        });
      });
    },
  });

  const informationLiaison = infoData && infoData.informationLiaison;

  const [uploadFiles, { loading: uploadLoading }] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (data && data.createPutPresignedUrls && data.createPutPresignedUrls.length > 0) {
        Promise.all(
          data.createPutPresignedUrls.map(async (presigned, index) => {
            if (presigned && fichiersJoints.length > 0) {
              await Promise.all(
                fichiersJoints.map(async file => {
                  if (presigned.filePath.includes(formatFilenameWithoutDate(file))) {
                    const type: TypeFichier =
                      file.type && file.type.includes('pdf')
                        ? TypeFichier.PDF
                        : file.type.includes('xlsx')
                          ? TypeFichier.EXCEL
                          : TypeFichier.PHOTO;
                    const newFile: FichierInput = {
                      id: '',
                      type,
                      nomOriginal: file.name,
                      chemin: presigned.filePath,
                    };

                    files = uniqBy([...files, newFile], 'chemin');

                    await uploadToS3(fichiersJoints[index], presigned.presignedUrl)
                      .then(result => {
                        if (result && result.status === 200) {
                          uploadResult = result;
                        }
                      })
                      .catch(error => {
                        setLoading(false);
                        console.log('error :>> ', error);
                        displaySnackBar(client, {
                          type: 'ERROR',
                          message: "Erreur lors de l'envoye de(s) fichier(s)",
                          isOpen: true,
                        });
                      });
                  }
                }),
              );
            }
          }),
        ).then(_ => {
          // Execute doCreateUpdate
          if ((uploadResult && uploadResult.status === 200) || files) {
            createUpdateCahierLiaison({
              variables: { input: { ...(removeEmptyString(input)), fichiers: [...files, ...state.fichiers] } },
            });
            setLoading(false);
          } else {
            setLoading(false);
          }
        });
      } else {
        createUpdateCahierLiaison({ variables: { input: { ...(removeEmptyString(input)) } } });
      }
    },
    onError: errors => {
      setLoading(false);
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const isBtnCreateDisabled =
    !state.titre || !state.idImportance || !state.idUrgence ? true : false;

  const onClickDefaultBtn = () => {
    if (!isOnList) {
      goBack();
      return;
    }
    setOpenDeleteDialog(true);
  };

  const DeleteDialogContent = () => {
    return <Fragment>Êtes-vous sûr de vouloir supprimer ces informations de liaison ?</Fragment>;
  };

  const onClickSecondaryBtn = () => {
    if (!isOnList || isMobile()) {
      if (state.idType === '') {
        displaySnackBar(client, {
          type: 'ERROR',
          message: 'Veuillez choisir le type',
          isOpen: true,
        });
      } else if (state.idOrigine === '') {
        displaySnackBar(client, {
          type: 'ERROR',
          message: "Veuillez choisir l'origine",
          isOpen: true,
        });
      } else if (state.description === '') {
        displaySnackBar(client, {
          type: 'ERROR',
          message: 'Veuillez remplir les détails',
          isOpen: true,
        });
      } else if (state.idUserDeclarant === '') {
        displaySnackBar(client, {
          type: 'ERROR',
          message: 'Veuillez choisir le déclarant',
          isOpen: true,
        });
      } else if (state.idColleguesConcernees.length === 0 && tache?.participants?.length === 0) {
        displaySnackBar(client, {
          type: 'ERROR',
          message: 'Veuillez choisir le(s) collègue(s) concernée(s)',
          isOpen: true,
        });
      }
      /*
      else if (state.idOrigine === '4' && state.serviceConcerned === '') {
        displaySnackBar(client, {
          type: 'ERROR',
          message: 'Veuillez choisir le service concerné',
          isOpen: true,
        });
      } else if (state.originId === '5' && state.provider === '') {
        displaySnackBar(client, {
          type: 'ERROR',
          message: 'Veuillez choisir le fournisseur',
          isOpen: true,
        });
      } else if (state.originId === '6' && state.serviceProvider === '') {
        displaySnackBar(client, {
          type: 'ERROR',
          message: 'Veuillez choisir le prestataire',
          isOpen: true,
        });
      }*/ else {
        setLoading(true);
        if (fichiersJoints && fichiersJoints.length > 0) {
          const filePathsTab: string[] = [];
          fichiersJoints.map((file: File) => {
            if (file.type) {
              filePathsTab.push(
                `cahiersliaisons/${groupement && groupement.id}/${formatFilenameWithoutDate(file)}`,
              );
            }
          });
          // Create presigned url
          uploadFiles({ variables: { filePaths: filePathsTab } });
        } else {
          createUpdateCahierLiaison();
        }
      }
      return;
    }
    goToAddInformation();
  };

  const [selected, setSelected] = useState<any[]>([]);

  let ids: any[] = [];
  ids = selected.map(s => {
    return s.id;
  });
  const { deleteCahierLiaison, mutationDeleteSuccess } = useDeleteCahierLiaison({ ids });

  const pharmacie: any = getPharmacie();

  const defaultMust: any[] = [
    { term: { isRemoved: false } },
    { term: { 'pharmacie.id': pharmacie.id } },
  ];

  const must: any[] = [{ term: { isRemoved: false } }, { term: { 'pharmacie.id': pharmacie.id } }];
  const should: any[] = [];

  if (searchTxt && searchTxt !== '') {
    searchTxt = searchTxt.replaceAll(' ', '\\ ');
    must.push({
      query_string: {
        query: `*${searchTxt}*`,
        analyze_wildcard: true,
      },
    });
    defaultMust.push({
      query_string: {
        query: `*${searchTxt}*`,
        analyze_wildcard: true,
      },
    });
  }

  if (filters?.idImportances?.length > 0) {
    must.push({ terms: { 'importance.id': filters.idImportances } });
  }
  if (filters?.idUrgences?.length > 0) {
    must.push({ terms: { 'urgence.id': filters.idUrgences } });
  }

  if (startDate && startDate !== '' && endDate && endDate !== '') {
    must.push({
      range: {
        dateCreation: {
          gte: moment(startDate).format('YYYY-MM-DD'),
          lte: moment(endDate).format('YYYY-MM-DD'),
        },
      },
    });
  }

  if (stateCollegeConcernees?.concernedColleaguesIds?.length > 0) {
    (statuts && statuts.length > 0
      ? statuts
      : informations === 'coupleStatutDeclarant'
        ? ['EN_COURS', 'CLOTURE']
        : ['NON_LUES', 'LUES', 'CLOTURE']
    ).map(statut => {
      should.push({
        terms: {
          [informations]: stateCollegeConcernees?.concernedColleaguesIds.map(
            id => `${id}-${statut}`,
          ),
        },
      });
    });
  }

  // My infos filter
  if (ckeckedMyInfo) {
    must.push({ term: { 'declarant.id': currentUserId } });
  }

  const sort =
    informations === 'coupleStatutDeclarant'
      ? [{ dateCreation: { order: 'desc' } }, { statut: { order: 'desc' } }]
      : [{ priority: { order: 'asc' } }, { 'colleguesConcernees.statut': { order: 'desc' } }];

  const queryVariables: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISONVariables = {
    type: ['informationliaison'],
    query: {
      query: activateFilter
        ? { bool: should.length > 0 ? { must, should, minimum_should_match: 1 } : { must } }
        : { bool: { must: defaultMust } },
    },
    take: 10,
    skip: 0,
    sortBy: sort,
  };

  const { data, loading: queryLoading, fetchMore, refetch } = useQuery<
    SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON,
    SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISONVariables
  >(GET_SEARCH_CUSTOM_INFORMATION_LIAISON, {
    fetchPolicy: 'cache-and-network',
    variables: queryVariables,
    onCompleted: data => {
      /* if (data?.search?.data && data.search.data.length > 0 && activeItem) {
        const item: any = data.search.data[0];
        setActiveItem(item);
        window.history.pushState(
          null,
          '',
          `${CAHIER_LIAISON_URL}/${informations === 'coupleStatutDeclarant' ? 'emise' : 'recue'}/${item.id
          }`,
        );

      }*/

      if (data.search?.data?.length === 0 && activeItem) {
        setActiveItem(null);
        window.history.pushState(
          null,
          '',
          `#${CAHIER_LIAISON_URL}/${informations === 'coupleStatutDeclarant' ? 'emise' : 'recue'}`,
        );
      }
    },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  /*
  React.useEffect(() => {
    const list = data?.search?.data;
    if (list && list.length > 0 && activeItem) {
      const item: any = list[0];
      if (!list.some((el: any) => el.id === activeItem.id)) {
        window.history.pushState(
          null,
          '',
          `${CAHIER_LIAISON_URL}/${informations === 'coupleStatutDeclarant' ? 'emise' : 'recue'}/${item.id
          }`,
        );
        setActiveItem(item);
      }
    }
  }, [informations, data]);
  */


  /**
   * Set values on edit
   */
  React.useEffect(() => {
    if (informationLiaison) {
      const p = informationLiaison || activeItem;

      const fakeFiles: File[] = [];
      const fichiers: FichierInput[] = [];
      if (p.fichiersJoints && p.fichiersJoints.length > 0) {
        p.fichiersJoints.map(fichierJoint => {
          const fichierInput: FichierInput = {
            id: fichierJoint.fichier.id,
            chemin: fichierJoint.fichier.chemin,
            nomOriginal: fichierJoint.fichier.nomOriginal,
            type: fichierJoint.fichier.type as any,
          };

          const fakeFile: File = { name: fichierJoint.fichier.nomOriginal } as any;

          if (!fichiers.map(elem => elem.chemin).includes(fichierJoint.fichier.chemin)) {
            fichiers.push(fichierInput);
          }

          if (!fakeFiles.map(elem => elem.name).includes(fichierJoint.fichier.nomOriginal)) {
            fakeFiles.push(fakeFile);
          }
        });
      }

      setFichiersJoints(fakeFiles);

      const initialStateValue: IStateForm = {
        id: p.id || '',
        idOrigine: p.idOrigine || '',
        idOrigineAssocie: p.idOrigineAssocie || '',
        idType: p.idType || '',
        idItem: p.idItem || '',
        idItemAssocie: p.idItemAssocie || '',
        statut: p.statut || '',
        bloquant: p.bloquant || false,
        titre: p.titre,
        description: p.description || '',
        idUserDeclarant: (p.declarant && p.declarant.id) || '',
        declaringName: (p.declarant && p.declarant.userName) || '',
        idColleguesConcernees:
          (p.colleguesConcernees &&
            p.colleguesConcernees.length > 0 &&
            p.colleguesConcernees.map(i => i.userConcernee.id)) ||
          [],
        teamAll: false,
        fichiers,
        idImportance: p.importance?.id || '',
        idUrgence: (p.urgence && p.urgence.id) || '',
        idFonction: p.idFonction || '',
        idTache: p.idTache || '',
      };

      if (p.idTache) {
        setTache({ ...p, id: undefined });
      }

      setState(initialStateValue);
    }
  }, [informationLiaison]);

  const onClickConfirmDelete = () => {
    deleteCahierLiaison();
    setOpenDeleteDialog(false);
  };

  const handleCreateClick = () => {
    setState(initialStateValue);
    setOpenFormModal(true);
  };

  const handleRefetch = (withInformation: boolean = true) => {
    if (withInformation) {
      refetch();
    } else {
      refetchInformation({ id: activeItem?.id }).then(response => {
        if (response?.data?.informationLiaison) {
          setActiveItem(response.data.informationLiaison);
        }
      });
    }
    refetchCount();
  };

  if (mutationSuccess && !isMobile()) {
    goBack();
  }

  return (
    <Box
      className={
        !isOnList
          ? classnames(classes.container)
          : classnames(classes.container, classes.marginTop64)
      }
    >
      {(mutationLoading || loading || getLoading || uploadLoading || queryLoading) && (
        <Backdrop value={mutationLoading || loading ? mutationLoadingMsg : ''} />
      )}

      {isOnList ? (
        <Fragment>
          <Box className={classnames(classes.contentMain)}>
            <Box className={commonClasses.root}>
              {(!isMobile() || !showMainContainer) && (
                <Box>
                  <Hidden mdUp={true} implementation="css">
                    <Box className={classes.createButton}>
                      <IconButton onClick={handleCreateClick}>
                        <Create />
                      </IconButton>
                    </Box>
                  </Hidden>
                  <LeftContainer
                    data={data}
                    activeItem={activeItem}
                    handleClickItem={handleClickItem}
                    fetchMore={fetchMore}
                    queryVariables={queryVariables}
                    refetch={handleRefetch}
                    handleSearchTxt={handleSearchTxt}
                    setOpenFormModal={setOpenFormModal}
                    setState={setState}
                    activateFilter={activateFilter}
                    setActivateFilter={setActivateFilter}
                    initialStateValue={initialStateValue}
                    setFichiersJoints={setFichiersJoints}
                  />
                  <CustomModal
                    title={`${state.id ? 'Modification' : 'Ajout'} d'information`}
                    open={openFormModal}
                    setOpen={setOpenFormModal}
                    closeIcon={true}
                    headerWithBgColor={true}
                    withBtnsActions={true}
                    fullScreen={true}
                    maxWidth="xl"
                    noDialogContent={true}
                    actionButton={state.id ? 'Modifier' : 'Créer'}
                    withCancelButton={false}
                    onClickConfirm={onClickSecondaryBtn}
                  >
                    <CahierLiaisonForm
                      state={state}
                      setState={setState}
                      isOnCreate={state.id ? false : true}
                      isOnEdit={state.id ? true : false}
                      stateFormData={handleChange}
                      informationLiaison={informationLiaison}
                      fichiersJoints={fichiersJoints}
                      setFichiersJoints={setFichiersJoints}
                      tache={tache}
                      setTache={setTache}
                      disabledConfirm={isBtnCreateDisabled}
                      onCancel={onClickDefaultBtn}
                      onConfirm={onClickSecondaryBtn}
                      withButtons={false}
                    />
                  </CustomModal>
                </Box>
              )}

              {!isMobile() && <MainContainer activeItem={activeItem} refetch={handleRefetch} />}
            </Box>
          </Box>
        </Fragment>
      ) : (
        <Fragment>
          <Box className={classes.contentMain}>
            <CahierLiaisonForm
              state={state}
              setState={setState}
              isOnCreate={isOnCreate}
              isOnEdit={isOnEdit}
              stateFormData={handleChange}
              informationLiaison={informationLiaison}
              fichiersJoints={fichiersJoints}
              setFichiersJoints={setFichiersJoints}
              tache={tache}
              setTache={setTache}
              disabledConfirm={isBtnCreateDisabled}
              onCancel={onClickDefaultBtn}
              onConfirm={onClickSecondaryBtn}
            />
          </Box>
        </Fragment>
      )}
      <CustomModal
        open={isMobile() && showMainContainer}
        setOpen={setShowMainContainer}
        fullScreen={true}
        closeIcon={true}
        withBtnsActions={false}
        noDialogContent={true}
      >
        <MainContainer
          activeItem={activeItem}
          refetch={handleRefetch}
          handleDrawerToggle={() => setShowMainContainer(false)}
        />
      </CustomModal>

      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
    </Box>
  );
};

export default withRouter(CahierLiaisonMain);
