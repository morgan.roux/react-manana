import { useApolloClient, useLazyQuery } from '@apollo/react-hooks';
import { GET_INFORMATION_LIAISON } from '../../../../../../graphql/CahierLiaison';
import {
  INFORMATION_LIAISON,
  INFORMATION_LIAISONVariables,
  INFORMATION_LIAISON_informationLiaison,
} from '../../../../../../graphql/CahierLiaison/types/INFORMATION_LIAISON';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';

/**
 *  get information liaison hooks
 */
const useGetCahierLiaison = (
  id: string,
  fetchPolicy?:
    | 'cache-first'
    | 'network-only'
    | 'cache-only'
    | 'no-cache'
    | 'standby'
    | 'cache-and-network'
    | undefined,
) => {
  const client = useApolloClient();

  const [getCahierLiaison, { data, loading }] = useLazyQuery<
    INFORMATION_LIAISON,
    INFORMATION_LIAISONVariables
  >(GET_INFORMATION_LIAISON, {
    fetchPolicy,
    variables: { id },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        });
      });
    },
  });

  const informationLiaison: INFORMATION_LIAISON_informationLiaison | null =
    (data && data.informationLiaison) || null;

  return { getCahierLiaison, informationLiaison, loading };
};

export default useGetCahierLiaison;
