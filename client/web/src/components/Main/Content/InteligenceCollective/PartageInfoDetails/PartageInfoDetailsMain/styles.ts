import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import { opacity } from 'html2canvas/dist/types/css/property-descriptors/opacity';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    expandBtnContainer: {
      marginRight: 0,
    },
    container: {
      width: '100%',
    },
    btnGoItem: {
      display: 'inline-block',
      float: 'right',
    },
    titleItem: {
      fontSize: '20px',
      fontFamily: 'Montserrat',
      color: '#212121',
      fontWeight: 500,
    },
    titleItemValue: {
      fontSize: '20px',
      fontFamily: 'Montserrat',
      color: theme.palette.secondary.main,
      fontWeight: 500,
      marginLeft: 12,
    },
    title: {
      fontSize: '0.875rem',
      color: '#9E9E9E',
    },
    fichePharmacieMainContent: {
      '& .MuiOutlinedInput-input': {
        padding: '7px 14px !important',
      },
      '& .MuiOutlinedInput-adornedEnd': {
        paddingRight: 50,
      },

      '& .MuiTabs-root': {
        width: '100%',
      },
      '& #customTabsAppBarId': {
        marginBottom: 25,
      },
      '& .MuiTabs-flexContainer ': {
        justifyContent: 'space-between',
      },
    },
  }),
);

export default useStyles;
