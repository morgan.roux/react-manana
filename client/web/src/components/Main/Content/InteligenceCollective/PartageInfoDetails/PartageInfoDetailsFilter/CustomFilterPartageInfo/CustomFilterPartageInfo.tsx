import React, { FC, useState } from 'react';
import {
  Box,
  Checkbox,
  ListItem,
  ListItemText,
  IconButton,
  Typography,
  Radio,
} from '@material-ui/core';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import useStyles from './styles';

interface FilterContentProps {
  title: string;
  datas: any;
  updateDatas: (list: any) => void;
  useRadios?: boolean;
  isDisabled?: boolean;
  counts?: any[];
}

const CustomFilterPartageInfo: FC<FilterContentProps> = ({
  title,
  datas,
  updateDatas,
  useRadios,
  counts,
  isDisabled = true,
}) => {
  const classes = useStyles({});
  const [expandedMore, setExpandedMore] = useState<boolean>(true);

  const onGetValue = itemIndex => {
    const newList = datas.map((item: any, index) => {
      if (itemIndex === index) {
        item.checked = !item.checked;
        return item;
      }
      return item;
    });
    updateDatas(newList);
  };
  console.log('>>', isDisabled);

  const onGetRadioValue = (itemIndex: number) => {
    const newList = datas.map((item: any, index) => {
      if (itemIndex === index) {
        item.checked = true;
        return item;
      } else {
        item.checked = false;
        return item;
      }
    });
    updateDatas(newList);
  };

  return (
    <Box borderTop="1px solid #E3E3E3" padding="16px 0">
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        marginBottom="8px"
        onClick={() => setExpandedMore(!expandedMore)}
      >
        <Typography>{title}</Typography>
        <IconButton size="small">{expandedMore ? <ExpandLess /> : <ExpandMore />}</IconButton>
      </Box>

      <Box className={classes.noStyle}>
        {expandedMore &&
          datas &&
          datas.map(
            (item: any, index) =>
              item && (
                <ListItem
                  role={undefined}
                  dense={true}
                  button={true}
                  onClick={() => onGetValue(index)}
                  key={`${index}-${item.nom}`}
                  disabled={isDisabled}
                >
                  <Box className={classes.checkBoxLeftName} display="flex" alignItems="center">
                    <Checkbox tabIndex={-1} checked={item.checked} />
                    <ListItemText primary={item.nom} />
                  </Box>
                  {
                    <Box>
                      <Typography className={classes.nbrProduits}>
                        {counts && counts[index] ? counts[index] : 0}
                      </Typography>
                    </Box>
                  }
                </ListItem>
              ),
          )}
      </Box>
    </Box>
  );
};

export default CustomFilterPartageInfo;
