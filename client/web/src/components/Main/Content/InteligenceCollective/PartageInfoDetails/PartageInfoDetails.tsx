import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { Box } from '@material-ui/core';
import { INTELLIGENCE_COLLECTIVE_URL } from '../../../../../Constant/url';
import LeftSidebarAndMainPage from '../../../../Common/Layouts/LeftSidebarAndMainPage';
import PartageInfoDetailsMain from './PartageInfoDetailsMain/PartageInfoDetailsMain';
import PartageInfoDetailsFilter from './PartageInfoDetailsFilter/PartageInfoDetailsFilter';
import useStyles from './styles';

// interface IPharmacieProps {
//   listResult?: any;
// }

const PartageInfoDetails: React.FC<RouteComponentProps> = ({
  history: { push },
  location,
  match: {
    params: {},
  },
}) => {
  const classes = useStyles({});
  const loc = location && location.state && (location.state as any);
  const idselected = loc && loc.length && loc.map(items => items.id);
  const [path, setPath] = React.useState('');
  const [isClickTodo, setClickTodo] = React.useState(false);
  const [isClickComment, setClickComment] = React.useState(true);
  const [statusTodo, setStatusTodo] = React.useState([]);
  const [listResult, setListResult] = React.useState([]);
  const [statuts, setStatuts] = React.useState([]);
  const [startDate, setStartDate] = React.useState<any>('');
  const [endDate, setEndDate] = React.useState<any>('');

  return (
    <Box className={classes.root}>
      <LeftSidebarAndMainPage
        sidebarChildren={
          <PartageInfoDetailsFilter
            path={path}
            isClickTodo={isClickTodo}
            isClickComment={isClickComment}
            setStatusTodo={setStatusTodo}
            handleStatus={setStatuts}
            listResult={listResult}
            handleEndDate={setEndDate}
            handleStartDate={setStartDate}
          />
        }
        mainChildren={
          <PartageInfoDetailsMain
            setPath={setPath}
            setClickTodo={setClickTodo}
            isClickTodo={isClickTodo}
            setClickComment={setClickComment}
            isClickComment={isClickComment}
            statuts={statuts}
            statusTodo={statusTodo}
            startDate={startDate}
            endDate={endDate}
            setListResult={setListResult}
          />
        }
      />
    </Box>
  );
};

export default withRouter(PartageInfoDetails);
