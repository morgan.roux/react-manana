import { Box, IconButton, Typography } from '@material-ui/core';
import React, { useState, FC, useEffect } from 'react';
import useStyles from './styles';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import DateRangePicker from '../../../../../Common/DateRangePicker';
import { RouteComponentProps, withRouter } from 'react-router';
import CustomFilterPartageInfo from './CustomFilterPartageInfo/CustomFilterPartageInfo';

interface PartageInfoDetailsProps {
  path: string;
  isClickTodo: boolean;
  isClickComment: boolean;
  setStatusTodo: (any) => void;
  listResult: any[];
  handleStatus: (any) => void;
  handleStartDate: (any) => void;
  handleEndDate: (any) => void;
}
const PartageInfoDetails: FC<PartageInfoDetailsProps & RouteComponentProps> = ({
  path,
  isClickTodo,
  isClickComment,
  setStatusTodo,
  listResult,
  handleStartDate,
  handleEndDate,
  handleStatus,
}) => {
  const classes = useStyles({});

  const [statutData, setStatutData] = useState([
    {
      nom: 'Nouvelle',
      checked: false,
      value: 'NOUVELLE',
    },
    {
      nom: 'Encours',
      checked: false,
      value: 'EN_COURS',
    },
    {
      nom: 'Clôturé',
      checked: false,
      value: 'CLOTURE',
    },
  ]);
  const [todoActionData, setTodoActionData] = useState([
    {
      nom: 'Terminé',
      checked: false,
      value: 'DONE',
    },
    {
      nom: 'Nom Terminé',
      checked: false,
      value: 'ACTIVE',
    },
  ]);

  const statusCounts: any[] = [];
  const statusTodoCounts: any[] = [];
  if (isClickTodo) {
    for (let i = 0; i < 2; i++) {
      const count =
        listResult &&
        listResult.length > 0 &&
        listResult.filter(value => value.status === todoActionData[i].value).length;
      statusTodoCounts.push(count);
    }
  } else {
    for (let i = 0; i < 3; i++) {
      const count =
        listResult &&
        listResult.length > 0 &&
        listResult.filter(value => value.status === statutData[i].value).length;
      statusCounts.push(count);
    }
  }

  const handleTodoAction = value => {
    setTodoActionData(value);
    const status: any[] = [];
    value.map(v => {
      if (v.checked) status.push(v.value);
    });

    if (status && status.length > 0) setStatusTodo(status);
    else setStatusTodo([]);
  };

  const [expandedMorePeriodeFilter, setExpandedMorePeriodeFilter] = useState<boolean>(true);
  const [expandedMore, setExpandedMore] = useState<boolean>(true);
  const [focusedInput, onFocusChange] = React.useState<'startDate' | 'endDate' | null>(null);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [searchTxt, setSearchTxt] = useState('');

  const handleGetDates = (startDate, endDate) => {
    setStartDate(startDate);
    setEndDate(endDate);
    handleStartDate(startDate);
    handleEndDate(endDate);
  };

  const handleStatutData = value => {
    setStatutData(value);
    const status: any[] = [];
    value.map(v => {
      if (v.checked) status.push(v.value);
    });

    if (status && status.length > 0) handleStatus(status);
    else handleStatus([]);
  };

  useEffect(() => {
    console.log('path===>>', path);
  }, [path]);

  return (
    <Box>
      <Typography className={classes.title}>Filtre de recherche</Typography>
      <Box borderTop="1px solid #E3E3E3" padding="16px 0">
        <Box
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          marginBottom="18px"
          onClick={() => setExpandedMorePeriodeFilter(!expandedMorePeriodeFilter)}
        >
          <Typography className={classes.titleFilter}>Intervalle de date</Typography>
          <IconButton size="small">
            {expandedMorePeriodeFilter ? <ExpandLess /> : <ExpandMore />}
          </IconButton>
        </Box>
        <Box className={classes.noStyle}>
          {expandedMorePeriodeFilter && (
            <DateRangePicker
              getDates={handleGetDates}
              startDate={startDate}
              endDate={endDate}
              startDatePlaceholderText="Du"
              endDatePlaceholderText="Au"
              label="Période"
              variant="outlined"
              numberOfMonths={1}
              startDateId="startCreationDateUniqueId"
              endDateId="endCreationDateUniqueId"
              showDefaultInputIcon={false}
              isOutsideRange={() => false}
              focusedInput={focusedInput}
              onFocusChange={onFocusChange}
            />
          )}
        </Box>
      </Box>

      <CustomFilterPartageInfo
        title="Statut"
        datas={statutData}
        counts={statusCounts}
        updateDatas={handleStatutData}
        isDisabled={isClickTodo || isClickComment}
      ></CustomFilterPartageInfo>

      <CustomFilterPartageInfo
        title="Statut todo"
        datas={todoActionData}
        counts={statusTodoCounts}
        updateDatas={handleTodoAction}
        isDisabled={!isClickTodo}
      ></CustomFilterPartageInfo>
    </Box>
  );
};

export default withRouter(PartageInfoDetails);
