import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      maxWidth: 720,
      margin: '0 auto',
      alignItems: 'center',

      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
    },
    listItemMenu: {
      padding: theme.spacing(0),
      minHeight: 16,
    },
    contentIntervenant: {
      width: '100%',
      '& .MuiTextField-root': {
        marginBottom: 24,
      },
    },
    mutationSuccessContainer: {
      width: '100%',
      minHeight: 'calc(100vh - 156px)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
      '& img': {
        width: 350,
        height: 270,
      },

      '& p': {
        textAlign: 'center',
        maxWidth: 600,
      },
      '& > div > p:nth-child(2)': {
        fontSize: 30,
        fontWeight: 'bold',
      },
      '& > div > p:nth-child(3)': {
        fontSize: 16,
        fontWeight: 'normal',
      },
      '& button': {
        marginTop: 30,
      },
    },
    margin: {
      margin: theme.spacing(1),
      width: '100%',
    },
    inputTitle: {
      fontWeight: 'bold',
      fontSize: 18,
      color: theme.palette.secondary.main,
      marginBottom: 15,
      marginLeft: 0,
      textAlign: 'left',
      width: '100%',
    },
    container: {
      width: '100%',
    },
    autocomplete: {
      '& .children': {
        marginLeft: 25,
      },
    },
  }),
);

export default useStyles;
