import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      margin: '0 auto',
      alignItems: 'center',
      maxWidth: 720,

      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
    },
    margin: {
      margin: theme.spacing(1),
      width: '100%',
    },
    inputTitle1: {
      fontWeight: 'bold',
      fontSize: 18,
      color: theme.palette.secondary.main,
      marginBottom: 15,
    },
    inputTitle: {
      fontWeight: 'bold',
      fontSize: 18,
      color: theme.palette.secondary.main,
      marginBottom: 15,
      marginTop: 60,
    },
    container: {
      width: '100%',
    },
    hauteur: {
      height: 222,
    },
  }),
);

export default useStyles;
