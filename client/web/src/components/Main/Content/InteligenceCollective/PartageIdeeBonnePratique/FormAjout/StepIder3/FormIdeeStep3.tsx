import { Box, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import ReactQuill from 'react-quill';
import { RouteComponentProps, withRouter } from 'react-router';
import useStyles from '../StepIder2/styles';

interface IFormProps {
  handleChangeCommentaire: (content: string, name: string) => void;
  values?: any;
}

const FormIdeeStep3: FC<IFormProps & RouteComponentProps> = ({
  handleChangeCommentaire,
  values,
}) => {
  const classes = useStyles({});

  return (
    <Box className={classes.root}>
      <Box className={classes.container}>
        <Typography className={classes.inputTitle1}>Résultats</Typography>
        <ReactQuill
          className={classes.hauteur}
          theme="snow"
          value={values.resultats}
          onChange={content => {
            handleChangeCommentaire(content, 'resultats');
          }}
        />
      </Box>
      <Box className={classes.container}>
        <Typography className={classes.inputTitle}>Facteurs clés de succès</Typography>
        <ReactQuill
          className={classes.hauteur}
          theme="snow"
          value={values.succ}
          onChange={content => {
            handleChangeCommentaire(content, 'succ');
          }}
        />
      </Box>
      <Box className={classes.container}>
        <Typography className={classes.inputTitle}>Contraintes</Typography>
        <ReactQuill
          className={classes.hauteur}
          theme="snow"
          value={values.contrainte}
          onChange={content => {
            handleChangeCommentaire(content, 'contrainte');
          }}
        />
      </Box>
    </Box>
  );
};
export default withRouter(FormIdeeStep3);
