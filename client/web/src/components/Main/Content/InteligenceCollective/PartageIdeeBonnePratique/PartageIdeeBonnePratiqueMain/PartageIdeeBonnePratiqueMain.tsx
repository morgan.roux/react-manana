import { Box } from '@material-ui/core';
import React, { Dispatch, FC, SetStateAction } from 'react';
import { push } from 'react-intl/locale-data/af';
import { RouteComponentProps, withRouter } from 'react-router';
import { PARTAGE_IDEE_BONNE_PRATIQUE_URL } from '../../../../../../Constant/url';
import {
  SEARCH_IDEE_BONNE_PRATIQUE,
  SEARCH_IDEE_BONNE_PRATIQUEVariables,
} from '../../../../../../graphql/IdeeBonnePratique/types/SEARCH_IDEE_BONNE_PRATIQUE';
import { FilterInterface } from '../PartageIdeeBonnePratique';
import LeftContainer from './LeftContainer';
import { ActiveItemProps } from './LeftContainer/Item/Item';
import { LeftContainerProps } from './LeftContainer/LeftContainer';
import MainContainer from './MainContainer';
import useStyles from './styles';

export interface PartageIdeeBonnePratiqueMainProps {
  data: SEARCH_IDEE_BONNE_PRATIQUE | undefined;
}

export interface QueryProps {
  queryVariables: SEARCH_IDEE_BONNE_PRATIQUEVariables;
  setQueryVariables: Dispatch<SetStateAction<SEARCH_IDEE_BONNE_PRATIQUEVariables>>;
}

export interface FilterProps {
  filter: FilterInterface;
  setFilter: Dispatch<SetStateAction<FilterInterface>>;
}

const PartageIdeeBonnePratiqueMain: FC<RouteComponentProps &
  PartageIdeeBonnePratiqueMainProps &
  LeftContainerProps &
  ActiveItemProps> = ({
  data,
  searchKeyWord,
  setSearchKeyWord,
  setActiveItem,
  activeItem,
  queryVariables,
  setQueryVariables,
  filter,
  setFilter,
  fetchMore,
  history: { push}
}) => {
  const classes = useStyles({});
const handleClick = () => {
  push(`${PARTAGE_IDEE_BONNE_PRATIQUE_URL}/create`)
}
  return (
    <Box className={classes.root}>
      <LeftContainer
        data={data}
        searchKeyWord={searchKeyWord}
        setSearchKeyWord={setSearchKeyWord}
        setActiveItem={setActiveItem}
        activeItem={activeItem}
        queryVariables={queryVariables}
        setQueryVariables={setQueryVariables}
        filter={filter}
        setFilter={setFilter}
        fetchMore={fetchMore}
        onClickAdd={handleClick}
        
      />
      <MainContainer setActiveItem={setActiveItem} activeItem={activeItem} />
    </Box>
  );
};

export default withRouter(PartageIdeeBonnePratiqueMain);
