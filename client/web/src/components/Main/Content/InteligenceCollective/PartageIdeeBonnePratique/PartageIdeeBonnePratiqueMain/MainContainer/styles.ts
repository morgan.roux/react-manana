import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
    },
    mainContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      [theme.breakpoints.only('md')]: {
        marginTop: 64,
      },
      [theme.breakpoints.down('md')]: {
        marginTop: 50,
      },
    },
    mainContent: {
      height: 'calc(100vh - 162px)',
      overflow: 'auto',
      [theme.breakpoints.down('md')]: {
        height: '100%',
      },
    },
    title: {
      color: theme.palette.secondary.main,
      fontSize: '1rem',
      fontWeight: 'bold',
    },
    labelTitle: {
      fontSize: '1rem',
      color: '#212121',
      fontWeight: 'bold',
      marginTop: 23,
    },
    labelContent: {
      fontSize: '1rem',
      color: '#212121',
    },
    mainContainerHeader: {
      width: '100%',
      height: 70,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-end',
      borderBottom: '1px solid #E5E5E5',
      padding: '0px 20px',
    },
    mainContainerHeaderBtns: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      '& > button:nth-child(2)': {
        marginLeft: 10,
      },
    },
    detailsContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      padding: '15px 28px',
    },
    moreContainer: {
      display: 'flex',
      width: '100%',
      justifyContent: 'flex-end',
    },
    mobileInfoSupContainer: {
      display: 'flex',
      flexDirection: 'column',
      maxHeight: 175,
      overflowY: 'auto',
      background: 'rgba(248, 248, 248, 1)',
      padding: 16,
      marginTop: 16,
      '& .MuiAvatar-root': {
        width: 24,
        height: 24,
      },
    },
    mobileInfoSupLabel: {
      fontFamily: 'Roboto',
      fontSize: 12,
      color: 'rgba(158, 158, 158, 1)',
    },
    mobileInfoSupValue: {
      fontFamily: 'Roboto',
      fontSize: 12,
    },
  }),
);

export default useStyles;
