import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
    divider: {
      margin: '20px 0px',
    },
    filterContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginBottom: '8px',
    },
    nested: {
      paddingLeft: theme.spacing(4),
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    fontSize14: {
      fontSize: '0.875rem',
    },
    count: {
      background: '#E5E5E5',
      borderRadius: 2,
      width: 20,
      height: 20,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: 10,
      right: 16,
    },
    noStyle: {
      listStyleType: 'none',
      '& .MuiListItem-root': {
        padding: '4px 0',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        '& .MuiListItemText-root': {
          color: '#212121',
          fontSize: '0.875rem',
          '& .MuiTypography-root': {
            color: '#212121',
            fontSize: '0.875rem',
          },
        },
      },
      '& .MuiCheckbox-root': {
        padding: '0 8px 0 9px!important',
      },
    },
    subListItem: {
      marginLeft: 8,
    },
    margeCount: {
      right: 30,
    },
    listItemSecondaryAction: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      right: 30,
    },
    toolbarTitle: {
      color: theme.palette.secondary.main,
      fontSize: 20,
      marginLeft: 16,
      fontWeight: 'bold',
    },
  }),
);

export default useStyles;
