import React, { Dispatch, FC, SetStateAction } from 'react';
import useStyles from './styles';
import classnames from 'classnames';
import { IconButton, Typography, Box } from '@material-ui/core';
import { MoreHoriz, Event, Visibility, ThumbUp, ChatBubble } from '@material-ui/icons';
import { RouteComponentProps, withRouter } from 'react-router';
import moment from 'moment';
import { PARTAGE_IDEE_BONNE_PRATIQUE_URL } from '../../../../../../../../Constant/url';
import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { DO_CREATE_UPDATE_IDEE_BONNE_PRATIQUE_LU } from '../../../../../../../../graphql/IdeeBonnePratique';
import {
  CREATE_UPDATE_IDEE_BONNE_PRATIQUE_LU,
  CREATE_UPDATE_IDEE_BONNE_PRATIQUE_LUVariables,
} from '../../../../../../../../graphql/IdeeBonnePratique/types/CREATE_UPDATE_IDEE_BONNE_PRATIQUE_LU';
import { displaySnackBar } from '../../../../../../../../utils/snackBarUtils';
import { getUser } from '../../../../../../../../services/LocalStorage';

export interface ItemProps {
  item: any;
}

export interface ActiveItemProps {
  activeItem: any;
  setActiveItem: Dispatch<SetStateAction<any>>;
}

const Item: FC<ItemProps & RouteComponentProps & ActiveItemProps> = ({
  item,
  setActiveItem,
  activeItem,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const user = getUser();

  const isActive = item && activeItem && item.id === activeItem.id;

  const [readIdeeOuBonnePratique] = useMutation<
    CREATE_UPDATE_IDEE_BONNE_PRATIQUE_LU,
    CREATE_UPDATE_IDEE_BONNE_PRATIQUE_LUVariables
  >(DO_CREATE_UPDATE_IDEE_BONNE_PRATIQUE_LU, {
    variables: {
      input: {
        idIdeeOuBonnePratique: (item && item.id) || '',
        idUser: (user && user.id) || '',
        isRemoved: false,
      },
    },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const handleClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    event.stopPropagation();
    // push(`${PARTAGE_IDEE_BONNE_PRATIQUE_URL}/${item && item.id}`);
    window.history.pushState(null, '', `#${PARTAGE_IDEE_BONNE_PRATIQUE_URL}/${item && item.id}`);
    setActiveItem(item);
    if (item && item.lu !== true) {
      readIdeeOuBonnePratique();
    }
  };

  return (
    <Box
      className={isActive ? classnames(classes.root, classes.isActive) : classes.root}
      onClick={handleClick}
    >
      <Box className={classnames(classes.flexRow)}>
        <Typography className={classes.title}>{(item && item._origine) || '-'}</Typography>
        <IconButton size="small">
          <MoreHoriz />
        </IconButton>
      </Box>
      <Box className={classnames(classes.flexRow, classes.titleContainer)}>
        <Typography className={classes.subTitle}>{(item && item.title) || '-'}</Typography>
        <Typography className={classnames(classes.dateFromNow)}>
          {(item && item.dateCreation && moment(item.dateCreation).fromNow()) || '-'}
        </Typography>
      </Box>
      <Box paddingRight="24px">
        <Box className={classnames(classes.flexRow)}>
          <Box display="flex" alignItems="center">
            <Typography className={classnames(classes.labelContent, classes.widthLabel69)}>
              Rédacteur :
            </Typography>
            <Typography className={classnames(classes.labelContentValue, classes.lenghtValueName)}>
              {(item && item.auteur && item.auteur.userName) || '-'}
            </Typography>
          </Box>
          <Box display="flex" alignItems="center">
            <Typography className={classes.labelContent}>Classification :</Typography>
            <Typography className={classes.labelContentValue}>
              {(item && item.classification && item.classification.nom) || '-'}
            </Typography>
          </Box>
        </Box>
        <Box className={classnames(classes.flexRow)}>
          <Box display="flex" alignItems="center">
            <Typography className={classes.labelContent}>Verificateur :</Typography>
            <Typography className={classnames(classes.labelContentValue, classes.lenghtValueName)}>
              {(item && item.verificateur && item.verificateur.userName) || '-'}
            </Typography>
          </Box>
          <Box display="flex" alignItems="center">
            <Typography className={classnames(classes.labelContent, classes.widthLabel81)}>
              Statut :
            </Typography>
            <Typography className={classes.labelContentValue}>
              {(item && item.status) || '-'}
            </Typography>
          </Box>
        </Box>
      </Box>
      <Box
        marginTop="2px"
        paddingRight="24px"
        className={classnames(classes.flexRow, classes.date)}
      >
        <Event />
        {(item && item.dateCreation && moment(item.dateCreation).format('DD/MM/YYYY')) || '-'}
      </Box>
      <Box marginTop="2px" paddingRight="24px" className={classnames(classes.flexRow)}>
        <Typography className={classnames(classes.stat)}>
          <Visibility /> {(item && item.nbLu) || 0}
        </Typography>
        <Typography className={classnames(classes.stat)}>
          <ThumbUp /> {(item && item.nbSmyley) || 0}
        </Typography>
        <Typography className={classnames(classes.stat)}>
          <ChatBubble /> {(item && item.nbComment) || 0}
        </Typography>
      </Box>
    </Box>
  );
};

export default withRouter(Item);
