import { createStyles, Theme } from '@material-ui/core/styles';
import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
    },
    tableContainer: {
      width: '100%',
      padding: '2%',
    },
    section: {
      '& .ql-container.ql-snow': {
        minHeight: 97,
      },
      '& .quill.customized-title': {
        marginBottom: 33,
      },
      '& .MuiMenuItem-root': {
        minHeight: '40px!important',
      },

      '&.MuiListItem-root.Mui-selected': {
        minHeight: '40px !important',
        '&:hover': {
          minHeight: '40px !important',
        },
      },
      [theme.breakpoints.down('md')]: {
        margin: 0,
        width: '100%',
      },
    },
    collaborateurInput: {
      height: 57,
      borderRadius: 5,
      border: `1px solid ${theme.palette.primary.dark}`,
      color: theme.palette.primary.dark,
      display: 'flex',
      width: '100%',
      '& legend': {
        marginLeft: 12,
      },
    },
    collaborateurBox: {
      display: 'flex',
      justifyContent: 'space-between',
      width: '100%',
    },
  }),
);
