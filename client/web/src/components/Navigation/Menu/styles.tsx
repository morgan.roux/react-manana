import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { availableThemes } from '../../../Theme';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    iconBtn: {
      color: theme.palette.common.white,
      opacity: 1,
      maxHeight: 50,
    },
    grid: ({ userTheme }: any) => {
      return {
        display: 'flex',
        flexWrap: 'wrap',
        marginTop: theme.spacing(3),
        padding: theme.spacing(2),
        backgroundColor:
          availableThemes[userTheme] && availableThemes[userTheme].palette.primary.main,
      };
    },
  }),
);
