import React, { FC, useState, useEffect, Fragment } from 'react';
import useStyles from './styles';
import { Button, Tooltip } from '@material-ui/core';
import LocalPharmacyIcon from '../../../assets/img/localPharmacy.svg';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import ChangeParmacieModal from '../../Main/Content/Panier/ChangeParmacieModal/ChangeParmacieModal';
import { useQuery } from '@apollo/react-hooks';
import ICurrentPharmacieInterface from '../../../Interface/CurrentPharmacieInterface';
import { MY_PHARMACIE_me_pharmacie } from '../../../graphql/Pharmacie/types/MY_PHARMACIE';
import { GET_CURRENT_PHARMACIE } from '../../../graphql/Pharmacie/local';
import { getUser } from '../../../services/LocalStorage';
import {
  SUPER_ADMINISTRATEUR,
  ADMINISTRATEUR_GROUPEMENT,
  COLLABORATEUR_COMMERCIAL,
  COLLABORATEUR_NON_COMMERCIAL,
  GROUPEMENT_AUTRE,
  PARTENAIRE_SERVICE,
} from '../../../Constant/roles';
import ArrowRight from '@material-ui/icons/ArrowRight';
import { CustomFullScreenModal } from '../../Common/CustomModal';
import FichePharmacie from '../../Dashboard/Pharmacies/FichePharmacie';

const PERMIT_ROLES = [
  SUPER_ADMINISTRATEUR,
  ADMINISTRATEUR_GROUPEMENT,
  COLLABORATEUR_COMMERCIAL,
  COLLABORATEUR_NON_COMMERCIAL,
  GROUPEMENT_AUTRE,
  PARTENAIRE_SERVICE,
];

const Pharmacie: FC<RouteComponentProps<any, any, any>> = ({ history }) => {
  const classes = useStyles({});
  const user = getUser();
  const userRole = user && user.role && user.role.code ? user.role.code : null;

  const [openDialog, setOpenDialog] = useState<boolean>(false);
  const [openPharmaDetail, setOpenPharmaDetail] = useState<boolean>(false);

  const handleOpenDialog = (value: boolean) => () => {
    if (PERMIT_ROLES.includes(userRole)) setOpenDialog(value);
  };

  // take currentPharmacie
  const [currentPharmacie, setCurrentPharmacie] = useState<MY_PHARMACIE_me_pharmacie | null>(null);
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

  useEffect(() => {
    setCurrentPharmacie(
      myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie
        ? myPharmacie.data.pharmacie
        : null,
    );
  }, [myPharmacie]);

  return (
    <Fragment>
      <Tooltip title={currentPharmacie ? currentPharmacie.nom || '' : '...'}>
        <Button className={classes.root} disabled={currentPharmacie ? false : true}>
          <img src={LocalPharmacyIcon} alt="logo pharmacie" />
          <p className={classes.threeDots} onClick={() => setOpenPharmaDetail(true)}>
            {currentPharmacie ? currentPharmacie.nom : '...'}
          </p>
          <ArrowRight onClick={handleOpenDialog(true)} />
        </Button>
      </Tooltip>
      <ChangeParmacieModal
        openDialog={handleOpenDialog}
        open={openDialog}
        setOpen={setOpenDialog}
      />
      {openPharmaDetail && (
        <CustomFullScreenModal
          open={openPharmaDetail}
          setOpen={setOpenPharmaDetail}
          title={`Pharmacie`}
          withBtnsActions={false}
        >
          <FichePharmacie
            id={
              myPharmacie &&
              myPharmacie.data &&
              myPharmacie.data.pharmacie &&
              myPharmacie.data.pharmacie.id
            }
            inModalView={true}
            handleClickBack={() => setOpenPharmaDetail(false)}
          />
        </CustomFullScreenModal>
      )}
    </Fragment>
  );
};

export default withRouter(Pharmacie);
