import React, { useState, useEffect } from 'react';
import { Box } from '@material-ui/core';
import { useStyles } from './style';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import { Laboratoire } from './Filter/Laboratoire';
import { Famille } from './Filter/Famille';
import { GammeCommercial } from './Filter/GammeCommercial';

import { useQuery, useApolloClient } from '@apollo/react-hooks';
import { FILTER_PARAMETER } from '../../../Interface/Filter';
import { GET_PARAMETER_LOCAL } from '../../../localStates/filter';

const Produits = () => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [searchText, setSearchText] = useState('');
  const filter = useQuery<FILTER_PARAMETER>(GET_PARAMETER_LOCAL);

  useEffect(() => {
    if (filter.data && filter.data.parameter && filter.data.parameter) {
      const { searchText } = filter.data.parameter;
      setSearchText(searchText);
    }
  }, [filter]);

  const onChange = (value: string) => {
    setLocalState(value);
    setSearchText(value);
  };

  const setLocalState = (value: string) => {
    client.writeData({
      data: {
        parameter: {
          idLaboratoires:
            filter && filter.data && filter.data.parameter && filter.data.parameter.idLaboratoires
              ? filter.data.parameter.idLaboratoires
              : [],
          laboratoires:
            filter && filter.data && filter.data.parameter && filter.data.parameter.laboratoires
              ? filter.data.parameter.laboratoires
              : [],
          idGammeCommercials:
            filter &&
            filter.data &&
            filter.data.parameter &&
            filter.data.parameter.idGammeCommercials
              ? filter.data.parameter.idGammeCommercials
              : [],
          idFamilles:
            filter && filter.data && filter.data.parameter && filter.data.parameter.idFamilles
              ? filter.data.parameter.idFamilles
              : [],
          gammes:
            filter && filter.data && filter.data.parameter && filter.data.parameter.gammes
              ? filter.data.parameter.gammes
              : [],
          familles:
            filter && filter.data && filter.data.parameter && filter.data.parameter.familles
              ? filter.data.parameter.familles
              : [],
          searchText: value,
          __typename: 'local',
        },
      },
    });
  };

  return (
    <Box className={classes.root}>
      <Box className={classes.search}>
        <InputBase
          placeholder="Quel produit cherchez-vous?"
          type="search"
          value={searchText}
          onChange={evt => onChange(evt.target.value)}
          classes={{
            root: classes.inputRoot,
            input: classes.inputInput,
          }}
          inputProps={{ 'aria-label': 'search' }}
        />
        <Box className={classes.searchIcon}>
          <SearchIcon />
        </Box>
      </Box>
      <Box marginTop="28px">
        <Famille />
        <Laboratoire />
        <GammeCommercial />
      </Box>
    </Box>
  );
};

export default Produits;
