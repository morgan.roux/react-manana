import React, { FC, useEffect, useState } from 'react';
import { Box } from '@material-ui/core';
import { useStyles } from '../../../style';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import { DATA } from '../../../../../../Interface/Filter';

interface PROPS {
  onGetValue: (value: number) => void;
  data: DATA;
  values: number[];
}

const FilterItem: FC<PROPS> = props => {
  const classes = useStyles({});
  const [checked, setChecked] = useState<number>();
  const [localValues, setLocalValues] = useState<number[]>([]);
  const { onGetValue, data, values } = props;

  const handleToggle = (value: number) => () => {
    onGetValue(value);
    setChecked(value);
  };

  useEffect(() => {
    setLocalValues(values);
  }, [values]);

  const labelId = `checkbox-list-label-${data.id}`;
  let isChecked = false;
  if (checked) {
    isChecked = localValues.indexOf(checked) !== -1;
  } else if (checked === 0) {
    isChecked = localValues.indexOf(checked) !== -1;
  }

  return (
    <>
      <ListItem role={undefined} dense={true} button={true} onClick={handleToggle(data.id)}>
        <Box className={classes.checkBoxLeftName} display="flex" alignItems="center">
          <Checkbox
            checked={isChecked}
            tabIndex={-1}
            disableRipple={true}
            inputProps={{ 'aria-labelledby': labelId }}
          />
          <ListItemText id={labelId} primary={data.nom} />
        </Box>

        <ListItemSecondaryAction>
          <Typography className={classes.nbrProduits}>{data.nbArticle}</Typography>
        </ListItemSecondaryAction>
      </ListItem>
    </>
  );
};

interface ROOT_PROPS {
  onGetValue: (value: number) => void;
  data: DATA;
  checked: boolean;
}

const FilterItemRoot: FC<ROOT_PROPS> = props => {
  const classes = useStyles({});
  const [checkedProps, setCheckedProps] = useState<boolean>(false);
  const { checked, data, onGetValue } = props;

  useEffect(() => {
    setCheckedProps(checked);
  }, [checked]);

  return (
    <>
      <ListItem role={undefined} dense={true} button={true} onClick={() => onGetValue(0)}>
        <Box className={classes.checkBoxLeftName} display="flex" alignItems="center">
          <Checkbox checked={checkedProps} tabIndex={-1} disableRipple={true} />
          <ListItemText primary={data.nom} />
        </Box>

        <ListItemSecondaryAction>
          <Typography className={classes.nbrProduits}>{data.nbArticle}</Typography>
        </ListItemSecondaryAction>
      </ListItem>
    </>
  );
};

export { FilterItem, FilterItemRoot };
