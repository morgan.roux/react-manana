import React, { FC } from 'react';
import { useStyles } from '../../../style';
import List from '@material-ui/core/List';

interface PROPS {
  open: boolean;
}

const ContainerItem: FC<PROPS> = props => {
  const classes = useStyles({});
  const { open, children } = props;

  return (
    <List
      className={open ? classes.listContentNameCheckBox : classes.listContentNameCheckBoxActive}
    >
      {children}
    </List>
  );
};

export default ContainerItem;
