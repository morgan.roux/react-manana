import React, { FC, ReactNode, useState } from 'react';
import { Box } from '@material-ui/core';
import { useStyles } from '../../style';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

interface ICONTENT_BOX {
  name?: string;
  children?: ReactNode;
  open?: boolean;
  fetchMore: () => void;
}

const ContentBox: FC<ICONTENT_BOX> = props => {
  const classes = useStyles({});
  const { name, children, open, fetchMore } = props;

  const [expandedMore, setExpandedMore] = useState(true);

  const handleOpen = () => {
    if (open) {
      fetchMore();
    }
  };

  return (
    <Box marginTop="50">
      <Box className={classes.contentListProduitItems}>
        <Box
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          marginBottom="8px"
          onClick={() => setExpandedMore(!expandedMore)}
        >
          <Typography className={classes.name}>{name}</Typography>
          {expandedMore ? <ExpandLess /> : <ExpandMore />}
        </Box>
        {expandedMore && children}
        {expandedMore && (
          <Box display="flex" justifyContent="center">
            {name !== 'Gamme' && (
              <Button variant="contained" className={classes.btnVoir} onClick={handleOpen}>
                {open ? ' Voir plus' : 'Voir moins'}
              </Button>
            )}
          </Box>
        )}
      </Box>
    </Box>
  );
};

export default ContentBox;

ContentBox.defaultProps = {
  name: '',
  open: true,
};
