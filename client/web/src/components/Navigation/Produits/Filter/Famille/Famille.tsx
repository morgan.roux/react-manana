import React, { useState, useEffect } from 'react';
import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { ContentBox } from '../ContentBox';
import { SEARCH } from '../../../../../graphql/search/query';
import {
  SEARCH as SEARCH_Interface,
  SEARCHVariables,
} from '../../../../../graphql/search/types/SEARCH';
import { ContainerItem, FilterItem } from '../Item';
import { DATA, FILTER_PARAMETER } from '../../../../../Interface/Filter';
import { GET_PARAMETER_LOCAL } from '../../../../../localStates/filter';
import { makeElementsUnique } from './../../../../../utils/Helpers';

interface ChipsInterface {
  id: number;
  nom: string;
  nbArticle: number;
  __typename: string;
}
const Famille = (props: any) => {
  const [checkeds, setCheckeds] = useState<number[]>([]);
  const [familles, setFamilles] = useState<DATA[]>([]);
  const client = useApolloClient();
  const [skip, setSkip] = useState<number>(3);

  const results = useQuery<SEARCH_Interface, SEARCHVariables>(SEARCH, {
    variables: {
      type: ['famille'],
      skip: 0,
      take: 10000,
      query: {
        query: {
          bool: { must: { range: { countCanalArticles: { gte: 1 } } } },
        },
      },
      sortBy: [{ countCanalArticles: { order: 'desc' } }],
      idPharmacie: '',
    },
  });

  const filter = useQuery<FILTER_PARAMETER>(GET_PARAMETER_LOCAL);

  useEffect(() => {
    if (filter.data && filter.data.parameter && filter.data.parameter) {
      const { idFamilles } = filter.data.parameter;
      setCheckeds(idFamilles);
    }
  }, [filter]);

  useEffect(() => {
    if (results && results.data && results.data.search && results.data.search.data) {
      const query = makeElementsUnique(results.data.search.data, 'id').slice(0, skip);
      setFamilles(
        query.map((famille: any) => {
          return {
            id: parseInt(famille.id, 10),
            nom: famille.nomFamille,
            code: famille.codeFamille,
            nbArticle: parseInt(famille.countCanalArticles),
            __typename: 'famille',
          };
        }),
      );
    }
  }, [results]);

  const onChange = (value: number) => {
    if (value === 0) {
      if (checkeds.length && checkeds.indexOf(value) === 0) {
        setCheckeds([]);
        return [];
      }
      const val = [0, ...familles.map(famille => famille.id)];
      setCheckeds(val);
      return val;
    }

    const currentIndex = checkeds.indexOf(value);
    const newChecked = [...checkeds];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    client.writeData({
      data: {
        parameter: {
          idFamilles: newChecked,
          familles,
          idLaboratoires:
            filter && filter.data && filter.data.parameter && filter.data.parameter.idLaboratoires
              ? filter.data.parameter.idLaboratoires
              : [],
          idGammeCommercials:
            filter &&
            filter.data &&
            filter.data.parameter &&
            filter.data.parameter.idGammeCommercials
              ? filter.data.parameter.idGammeCommercials
              : [],
          laboratoires:
            filter && filter.data && filter.data.parameter && filter.data.parameter.laboratoires
              ? filter.data.parameter.laboratoires
              : [],
          gammes:
            filter && filter.data && filter.data.parameter && filter.data.parameter.gammes
              ? filter.data.parameter.gammes
              : [],
          searchText:
            filter && filter.data && filter.data.parameter && filter.data.parameter.searchText
              ? filter.data.parameter.searchText
              : '',
          __typename: 'local',
        },
      },
    });
    setCheckeds(newChecked);
    return newChecked;
  };
  const fetchMore = () => {
    if (results && results.data && results.data.search && results.data.search.data) {
      const nextSkip = skip + 3;
      setSkip(nextSkip);
      const query = makeElementsUnique(results.data.search.data, 'id').slice(0, nextSkip);
      setFamilles(
        query.map((famille: any) => {
          return {
            id: parseInt(famille.id, 10),
            nom: famille.nomFamille,
            code: famille.codeFamille, // Only article
            nbArticle: parseInt(famille.countCanalArticles),
            __typename: 'famille',
          };
        }),
      );
    }
  };

  return (
    <ContentBox name="Familles" fetchMore={fetchMore}>
      <ContainerItem open={true}>
        {familles &&
          familles.map(famille => (
            <FilterItem data={famille} key={famille.id} values={checkeds} onGetValue={onChange} />
          ))}
      </ContainerItem>
    </ContentBox>
  );
};

export default Famille;
