import React, { useRef, useState, useEffect } from 'react';
import { useStyles } from './style';
import SearchIcon from '@material-ui/icons/Search';
import { useWindowSize } from '../../Common/WindowSize';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import { handleSetValue } from '../../../utils/hookUtils';
import { SEARCH } from '../../../graphql/search/query';
import { useQuery } from '@apollo/react-hooks';

import SearchResult from './SearchResult';
import {
  Dialog,
  DialogContent,
  DialogProps,
  DialogTitle,
  IconButton,
  TextField,
  Typography,
} from '@material-ui/core';
import { Close, Search } from '@material-ui/icons';

// TODO: Filter by groupement

const ACCEPTED_TYPES = ['produitcanal', 'laboratoire', 'partenaire', 'operation', 'actualite'];

const MobileContainer = (props: {
  dialogProps: DialogProps;
  mobile: boolean;
  handleOpen(): void;
  children: any;
}) => {
  const { mobile, handleOpen, children, dialogProps } = props;
  const classes = useStyles({});

  return mobile ? (
    <>
      <IconButton onClick={handleOpen}>
        <Search htmlColor="#FFF" />
      </IconButton>
      <Dialog {...dialogProps} maxWidth="md" fullWidth PaperProps={{ className: classes.dialog }}>
        {children}
      </Dialog>
    </>
  ) : (
    <>{children}</>
  );
};

function SearchInput() {
  const classes = useStyles({});

  const containerSearchRef = useRef<HTMLDivElement>(null);

  const { width } = useWindowSize();

  const [type, setType] = useState(ACCEPTED_TYPES);
  const [query, setQuery] = useState<string>('');
  const [searchText, setSearchText] = useState<string>('');

  const [listSearchPositions, setListSearchPositions] = useState<ClientRect | DOMRect | undefined>(
    undefined,
  );

  const [isListSearchPositionOpened, setIsListSearchPositionOpened] = useState(false);

  const handleTextChanges = (text: string) => {
    const count = (text.match(/:/g) || []).length;
    setSearchText(text);
    if (count === 1) {
      if (text.startsWith('prod:')) {
        setType(['produitcanal']);
      } else if (text.startsWith('lab:')) {
        setType(['laboratoire']);
      } else if (text.startsWith('part:')) {
        setType(['partenaire']);
      } else if (text.startsWith('oc:')) {
        setType(['operation']);
      } else if (text.startsWith('actu:')) {
        setType(['actualite']);
      } else {
        setType(ACCEPTED_TYPES);
      }
      setQuery(text.split(':')[1]);
    } else if (count === 0) {
      setType(ACCEPTED_TYPES);
      setQuery(text);
    } else {
      setQuery('');
      return;
    }
  };

  useEffect(() => {
    if (containerSearchRef.current) {
      setListSearchPositions(containerSearchRef.current.getBoundingClientRect());
    }
  }, [width, containerSearchRef]);

  const { loading, error, data } = useQuery(SEARCH, {
    variables: {
      type,
      skip: 0,
      take: 20,
      query,
      idPharmacie: '',
    },
  });

  const [mobile, setMobile] = useState<boolean>(window.innerWidth < 1366);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const handleOpen = () => {
    setOpenModal(prev => !prev);
  };
  const handleClose = () => {
    setOpenModal(false);
  };

  useEffect(() => {
    window.addEventListener('resize', () => {
      if (window.innerWidth < 1366) setMobile(true);
      else setMobile(false);
    });
  });

  return (
    <ClickAwayListener onClickAway={handleSetValue(setIsListSearchPositionOpened, false)}>
      <MobileContainer
        dialogProps={{
          open: openModal,
          onClose: handleClose,
          children: null,
        }}
        mobile={mobile}
        handleOpen={handleOpen}
      >
        <div className={classes.search} ref={containerSearchRef}>
          <TextField
            placeholder="Que recherchez-vous ?"
            value={searchText}
            type="search"
            onChange={evt => handleTextChanges(evt.target.value)}
            onFocus={handleSetValue(setIsListSearchPositionOpened, true)}
            onKeyDown={evt => {
              if (evt.key === 'Tab') {
                setIsListSearchPositionOpened(false);
              }
            }}
            InputProps={{
              endAdornment: <SearchIcon />,
            }}
            variant="outlined"
            fullWidth
          />
          {listSearchPositions && isListSearchPositionOpened && query && (
            <SearchResult
              loading={loading}
              error={error}
              data={(data && data.search && data.search.data) || []}
              mobile={mobile}
            />
          )}
        </div>
      </MobileContainer>
    </ClickAwayListener>
  );
}

export default SearchInput;
