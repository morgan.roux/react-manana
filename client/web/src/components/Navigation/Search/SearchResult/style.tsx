import { Theme, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => ({
  paperAutocompleteSearch: ({ mobile }: any) => ({
    width: '100%',
    position: mobile ? 'relative' : 'absolute',
    top: 'calc(100% + 8px)',
    backgroundColor: '#fff',
    boxShadow: '1px 4px 6px -1px rgba(0,0,0,0.25)',
    maxHeight: 'calc(100vh - 194px)',
    borderRadius: 6,
    overflowY: 'auto',
  }),
  listSearch: {
    padding: 0,
    '& .MuiListItem-root': {
      paddingTop: 12,
      paddingBottom: 12,
    },
  },
}));
