import React, { FC } from 'react';
import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import { Theme } from '@material-ui/core/styles';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
    root: {
        width: '100%',
        height: 50,
        textAlign: 'center',
        paddingTop: theme.spacing(2.5)
    },
});


interface MessageProps {
    message: string
}

const Message: FC<WithStyles & MessageProps> = ({ classes, message }: any) => {
    return (
        <div className={classes.root}>
            {message}
        </div>
    );
};



export default withStyles(styles)(Message);
