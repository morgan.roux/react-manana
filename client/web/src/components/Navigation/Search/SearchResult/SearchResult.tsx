import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import Loader from './Loader';
import Message from './Message';

import Box from '@material-ui/core/Box';
import List from '@material-ui/core/List';
import { useStyles } from './style';
import Item from './SearchResultItem';
import ImgSearch from '../../../../assets/img/panier-liste-1.png';
import { PHARMACIE_URL } from '../../../../Constant/url';

interface SearchResultItemProps {
  data: any; // Array of search data.
  loading: boolean;
  error: any;
  setUserId?: React.Dispatch<React.SetStateAction<string | null>>;
  mobile: boolean;
}

const SearchResultItem: FC<SearchResultItemProps & RouteComponentProps<any, any, any>> = ({
  data,
  loading,
  error,
  setUserId,
  mobile,
}) => {
  const classes = useStyles({ mobile });

  return (
    <Box className={classes.paperAutocompleteSearch}>
      {loading ? (
        <Loader />
      ) : error ? (
        <Message message={'Erreur de chargement'} />
      ) : data.length === 0 ? (
        <Message message={'Aucun résultat correspondant'} />
      ) : (
        <List component="nav" aria-labelledby="nested-list-search" className={classes.listSearch}>
          {data
            .map((item: any) => {
              if (item.type === 'produitcanal') {
                return (
                  <Item
                    key={`${item.type}-${item.id}`}
                    label={item.nomCanalArticle}
                    iconPath={ImgSearch}
                    link={`/produits/${item.id}`}
                    type={item.type}
                  />
                );
              }

              if (item.type === 'pharmacie') {
                return (
                  <Item
                    key={`${item.type}-${item.id}`}
                    label={item.nom}
                    link={`/${PHARMACIE_URL}/${item.id}`}
                    type={item.type}
                  />
                );
              }

              if (item.type === 'laboratoire') {
                return (
                  <Item
                    key={`${item.type}-${item.id}`}
                    label={item.nom}
                    link={`/laboratoires/${item.id}`}
                    type={item.type}
                  />
                );
              }

              if (item.type === 'partenaire') {
                return (
                  <Item
                    key={`${item.type}-${item.id}`}
                    label={item.nom}
                    link={`/partenaires/${item.id}`}
                    type={item.type}
                  />
                );
              }

              if (item.__typename === 'User') {
                return (
                  <Item
                    key={`${item.id}`}
                    label={item.userName}
                    link={`${item.id}`}
                    type={item.__typename}
                    userId={item.id}
                    setUserId={setUserId}
                  />
                );
              }

              return null;
            })
            .filter((item: any) => item != null)}
        </List>
      )}
    </Box>
  );
};

export default withRouter(SearchResultItem);
