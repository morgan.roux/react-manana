import React, { FC, useState, useEffect, Fragment } from 'react';
import { RouteComponentProps, withRouter, Redirect } from 'react-router-dom';

import { useStyles } from './style';
import Box from '@material-ui/core/Box';

import ListItem from '@material-ui/core/ListItem';
import Typography from '@material-ui/core/Typography';

interface SearchResultItemProps {
  iconPath?: string;
  label: string;
  type: string;
  link: string;
  setUserId?: React.Dispatch<React.SetStateAction<string | null>>;
  userId?: string;
}

const TYPES_TO_NAME: any = {
  produit: 'produitcanal',
  pharmacie: 'Pharmacie',
  laboratoire: 'Laboratoire',
  partenaire: 'Partenaire',
  User: 'Utilisateur',
};

const SearchResultItem: FC<SearchResultItemProps & RouteComponentProps<any, any, any>> = ({
  history,
  iconPath,
  label,
  type,
  link,
  setUserId,
  userId,
}) => {
  const classes = useStyles({});
  const [followLink, setFollowLink] = useState(false);

  const typeName = TYPES_TO_NAME[type];
  if (!typeName) {
    return null;
  }

  return followLink ? (
    <Redirect to={link} />
  ) : (
    <ListItem
      className={classes.contentList}
      button={true}
      onClick={() => (setUserId ? setUserId(userId || null) : setFollowLink(true))}
    >
      <Box className={classes.contentLeftList}>
        {iconPath && (
          <Box className={classes.contentImgSearch}>
            <img src={iconPath} alt="" />
          </Box>
        )}
        <Typography>{label}</Typography>
      </Box>
      <Typography className={classes.textContentRight}>{typeName}</Typography>
    </ListItem>
  );
};

export default withRouter(SearchResultItem);
