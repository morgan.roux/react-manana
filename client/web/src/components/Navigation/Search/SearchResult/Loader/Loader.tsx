import React, { FC } from 'react';
import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import { Theme } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
  root: {
    display: 'flex',
    width: '100%',
    height: 50,
  },

  progress: {
    // color: theme.palette.text.secondary,
    margin: 'auto',
  },
});

const Loader: FC<WithStyles> = ({ classes }: any) => {
  return (
    <div className={classes.root}>
      <CircularProgress size={25} disableShrink={true} className={classes.progress} />
    </div>
  );
};

export default withStyles(styles)(Loader);
