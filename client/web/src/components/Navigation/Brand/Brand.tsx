import React, { FC } from 'react';
import logo from '../../../assets/img/logo-gcr_pharma.svg';
import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import { Theme } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
  logo: {
    [theme.breakpoints.up('md')]: {
      paddingRight: 30,
      marginRight: 30,
      borderRight: '1px solid white',
      height: 66,
    },
    [theme.breakpoints.down('md')]: {
      height: 36,
    },
  },
});

const Brand: FC<WithStyles> = ({ classes }) => {
  return (
    <Link to="/">
      <img className={classes.logo} src={logo} />
    </Link>
  );
};

export default withStyles(styles)(Brand);
