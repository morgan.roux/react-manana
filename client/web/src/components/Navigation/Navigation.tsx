import { useApolloClient, useQuery, useSubscription } from '@apollo/react-hooks';
import {
  Avatar,
  Box,
  Divider,
  Fab,
  Hidden,
  ListItemIcon,
  PopperProps,
  Toolbar,
  useTheme,
} from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Fade from '@material-ui/core/Fade';
import IconButton from '@material-ui/core/IconButton';
// import Hidden from '@material-ui/core/Hidden';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
// import logo from '../../assets/img/logo-gcr_pharma.svg';
import { makeStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
// import IconPeople from '@material-ui/icons/People';
import Typography from '@material-ui/core/Typography';
import DashboardIcon from '@material-ui/icons/Dashboard';
import AccessibilityIcon from '@material-ui/icons/Accessibility';
import {
  Assessment,
  Email,
  Equalizer,
  Face,
  Grade,
  GroupAdd,
  GroupWork,
  Home,
  LibraryBooks,
  ListAlt,
  LocalMall,
  MonetizationOn,
  Settings,
  SettingsApplications,
  ShoppingCart,
  ViewModule,
} from '@material-ui/icons';
import TitulaireIcon from '@material-ui/icons/AccountBox';
import BasisCrudIcon from '@material-ui/icons/Add';
import UtilityIcon from '@material-ui/icons/Build';
import PartenaireIcon from '@material-ui/icons/BusinessCenter';
import CategoryIcon from '@material-ui/icons/Category';
import CloseIcon from '@material-ui/icons/Close';
import GroupIcon from '@material-ui/icons/Group';
import LiveHelpIcon from '@material-ui/icons/LiveHelp';
import PharmacieIcon from '@material-ui/icons/LocalHospital';
import ThemeIcon from '@material-ui/icons/Palette';
import MyAccountIcon from '@material-ui/icons/Person';
import PresidentIcon from '@material-ui/icons/PersonPin';
import SettingsApplicationsIcon from '@material-ui/icons/SettingsApplications';
import GroupementIcon from '@material-ui/icons/SupervisedUserCircle';
import OptionGroupementIcon from '@material-ui/icons/Tune';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import PubIcon from '@material-ui/icons/ViewCarousel';
import LaboIcon from '@material-ui/icons/WbIncandescent';
import classnames from 'classnames';
import gql from 'graphql-tag';
import { FridgeOutline } from 'mdi-material-ui';
import React, { FC, Fragment, useEffect, useState } from 'react';
import { useLocation } from 'react-router';
import { Redirect, RouteComponentProps, withRouter } from 'react-router-dom';
import GestionMarcheIcon from '../../assets/icons/marche/gestion_marche.svg';
import GestionPromoIcon from '../../assets/icons/promotion/gestion_promotion.svg';
import BusinessIcon from '../../assets/img/business_center-24px.svg';
import { APP_SSO_URL } from '../../config';
import {
  ADMINISTRATEUR_GROUPEMENT,
  PARTENAIRE_LABORATOIRE,
  PARTENAIRE_SERVICE,
  pharmacieAllRoles,
  PRESIDENT_REGION,
  SUPER_ADMINISTRATEUR,
  TITULAIRE_PHARMACIE,
} from '../../Constant/roles';
import {
  CATEGORIE_URL,
  COLORS_URL,
  ESPACE_DOCUMENTAIRE_URL,
  GROUPES_CLIENTS_URL,
  GROUPE_AMIS,
  INTELLIGENCE_COLLECTIVE_URL,
  LABORATOIRE_URL,
  PARAMETRE_FRIGO_URL,
  PARTAGE_IDEE_URL,
  PARTENAIRE_SERVICE_URL,
  PERSONNEL_GROUPEMENT_URL,
  PERSONNEL_PHARMACIE_URL,
  PHARMACIE_URL,
  PRESIDENT_REGION_URL,
  TITULAIRE_PHARMACIE_URL,
  TRAITEMENT_AUTOMATIQUE_URL,
  DASHBOARD_ITEM_SCORING_URL,
  MODELE_OPERATION_MARKETING_URL,
  OPERATION_MARKETING_URL,
  REMISE_URL,
} from '../../Constant/url';
import { ME_me } from '../../graphql/Authentication/types/ME';
import { GET_GROUPEMENT } from '../../graphql/Groupement/query';
import { GROUPEMENT, GROUPEMENTVariables } from '../../graphql/Groupement/types/GROUPEMENT';
import { GET_SEND_MESSAGE_SUBSCRIPTION } from '../../graphql/Messagerie';
import {
  GET_TOTAL_MESSAGERIES,
  GET_TOTAL_MESSAGERIE_NON_LUS,
} from '../../graphql/Messagerie/query';
import {
  GET_MESSAGE_LU_SUBSCRIPTION,
  GET_MESSAGE_SUPPRIMEE_SUBSCRIPTION,
} from '../../graphql/Messagerie/subscription';
import { MESSAGE_LU_SUBSCRIPTION } from '../../graphql/Messagerie/types/MESSAGE_LU_SUBSCRIPTION';
import { MESSAGE_SUPPRIMEE_SUBSCRIPTION } from '../../graphql/Messagerie/types/MESSAGE_SUPPRIMEE_SUBSCRIPTION';
import { SEND_MESSAGE_SUBSCRIPTION } from '../../graphql/Messagerie/types/SEND_MESSAGE_SUBSCRIPTION';
import {
  TOTAL_MESSAGERIES,
  TOTAL_MESSAGERIESVariables,
} from '../../graphql/Messagerie/types/TOTAL_MESSAGERIES';
import { TOTAL_MESSAGERIE_NON_LUS } from '../../graphql/Messagerie/types/TOTAL_MESSAGERIE_NON_LUS';
// import { GET_TOTAL_MESSAGERIES, GET_TOTAL_MESSAGERIE_LUS } from '../../graphql/Messagerie/query';
// import {
//   GET_MESSAGE_LU_SUBSCRIPTION,
//   GET_MESSAGE_SUPPRIMEE_SUBSCRIPTION,
// } from '../../graphql/Messagerie/subscription';
// import { MESSAGE_LU_SUBSCRIPTION } from '../../graphql/Messagerie/types/MESSAGE_LU_SUBSCRIPTION';
// import { MESSAGE_SUPPRIMEE_SUBSCRIPTION } from '../../graphql/Messagerie/types/MESSAGE_SUPPRIMEE_SUBSCRIPTION';
// import { SEND_MESSAGE_SUBSCRIPTION } from '../../graphql/Messagerie/types/SEND_MESSAGE_SUBSCRIPTION';
// import {
//   TOTAL_MESSAGERIES,
//   TOTAL_MESSAGERIESVariables,
// } from '../../graphql/Messagerie/types/TOTAL_MESSAGERIES';
// import { TOTAL_MESSAGERIE_LUS } from '../../graphql/Messagerie/types/TOTAL_MESSAGERIE_LUS';
import { GET_PARAMETERS_BY_CATEGORIES } from '../../graphql/Parametre/query';
import {
  PARAMETERS_BY_CATEGORIES,
  PARAMETERS_BY_CATEGORIESVariables,
} from '../../graphql/Parametre/types/PARAMETERS_BY_CATEGORIES';
import { GET_MY_PHARMACIE } from '../../graphql/Pharmacie/query';
import {
  MY_PHARMACIE,
  MY_PHARMACIE_me_pharmacie,
} from '../../graphql/Pharmacie/types/MY_PHARMACIE';
import { GET_PILOTAGE_ACTU_SOURCE, GET_PILOTAGE_OC_SOURCE } from '../../graphql/Pilotage/local';
import { GET_USER_ROLES } from '../../graphql/Role/query';
import { USER_ROLES } from '../../graphql/Role/types/USER_ROLES';
import { AppAuthorization } from '../../services/authorization';
import {
  getAccessToken,
  getGroupement,
  getPharmacie,
  getUser,
  setPharmacie,
} from '../../services/LocalStorage';
import { MessagerieType, ParamCategory } from '../../types/graphql-global-types';
import { useValueParameterAsBoolean } from '../../utils/getValueParameter';
import { isMobile, stringToAvatar } from '../../utils/Helpers';
import { AWS_HOST } from '../../utils/s3urls';
// import Filter from '../Common/Filter';
import Applications from '../Applications';
import { Loader } from '../Dashboard/Content/Loader';
import DashboardRouter from '../Dashboard/DashboardRouter';
import { MARCHE_BASE_URL, PROMO_BASE_URL } from '../Dashboard/Marche/constant';
import PsychologyIcon from '../Main/Content/InteligenceCollective/Common/Icon/psychologyWhite';
import { initPilotageApolloLocalState } from '../Main/Content/Pilotage/Dashboard/Dashboard';
import MainRouter from '../Main/MainRouter';
import OutilsDigitaux from '../Outils';
import { Brand } from './Brand';
import { HeaderMenu } from './Menu';
import { Profile } from './Profile';
import Search from './Search';
import useStyles from './styles';

// import moment from 'moment';
// import DailyAlert from '../Main/Content/TodoNew/DailyAlert';

const SHOW_PILOTAGE_PATH = [
  '/demarche-qualite',
  '/demarche-qualite/outils',
  // '/demarche-qualite/fiche-incident',
  // '/demarche-qualite/fiche-amelioration',
  // '/demarche-qualite/action-operationnelle',
];

// sidebar local state schema
export const GET_CURRENT_PANIER = gql`
  {
    sidebar @client {
      state
    }
  }
`;

const sideMenuStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
});

const handleDrawerToggle = (setMobileOpen: (isOpen: boolean) => void, isOpen: boolean) => () => {
  setMobileOpen(!isOpen);
};

interface MenusProps {
  match: {
    params: { idOperation: string | undefined };
  };
}

const Menus: FC<MenusProps &
  PopperProps & { mobile: boolean; userTheme: string } & RouteComponentProps<any, any, any> &
  any> = props => {
  const {
    history,
    location: { pathname },
    user,
    match: {
      params: { idOperation },
    },
  } = props;

  const location = useLocation();
  const classes = useStyles(props);
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const client = useApolloClient();
  const currentUser: ME_me = getUser();
  const token = getAccessToken();

  const auth = new AppAuthorization(currentUser);

  // const { parameters } = useContext<ParameterStateInterface>(ParametersContext);

  // sidebar state position
  const [state, setState] = React.useState({
    top: false,
    left: true,
    bottom: false,
    right: false,
  });

  // const { parameters } = useContext<ParameterStateInterface>(ParametersContext);

  // take pharmacies list from groupement
  const groupement = getGroupement();

  const groupementResult = useQuery<GROUPEMENT, GROUPEMENTVariables>(GET_GROUPEMENT, {
    variables: {
      id: (groupement && groupement.id) || '',
    },
  });

  const groupementLogo =
    groupementResult.data &&
    groupementResult.data.groupement &&
    groupementResult.data.groupement.groupementLogo &&
    groupementResult.data.groupement.groupementLogo.fichier &&
    groupementResult.data.groupement.groupementLogo.fichier.chemin;

  const pharmaciePartenaires =
    currentUser.pharmaciePartenaires && currentUser.pharmaciePartenaires.length > 0
      ? currentUser.pharmaciePartenaires
      : null;

  // const userPhoto = user && user.userPhoto && user.userPhoto.fichier && user.userPhoto.fichier.chemin;

  const ocSourceResult = useQuery(GET_PILOTAGE_OC_SOURCE);
  const actuSourceResult = useQuery(GET_PILOTAGE_ACTU_SOURCE);

  const [pilotageSource, setPilotageSource] = useState<any>(null);
  /**
   * Set pilotageSource
   */
  useEffect(() => {
    if (ocSourceResult && ocSourceResult.data && ocSourceResult.data.pilotageOcSource) {
      setPilotageSource(ocSourceResult.data.pilotageOcSource);
    }

    if (actuSourceResult && actuSourceResult.data && actuSourceResult.data.pilotageActuSource) {
      setPilotageSource(actuSourceResult.data.pilotageActuSource);
    }
  }, [ocSourceResult, actuSourceResult]);

  // myPharmacie initialisation
  const myPharmacie = useQuery<MY_PHARMACIE>(GET_MY_PHARMACIE);

  const setPharmaciesList = (pharmacies: Array<any | null> | null) => {
    client.writeData({
      data: { pharmacies },
    });
  };

  const displayRapportGroupement = useValueParameterAsBoolean('0379');
  const displayRapportPharmacie = useValueParameterAsBoolean('0780');

  const [displayRapportFab, setDisplayRapportFab] = React.useState<boolean>();
  useEffect(() => {
    setDisplayRapportFab(
      displayRapportGroupement && displayRapportPharmacie && SHOW_PILOTAGE_PATH.includes(pathname),
    );
  }, [pathname, displayRapportGroupement, displayRapportPharmacie]);
  // take pharmacies list from groupement

  // get parameters
  let getParametersVariables = [
    ParamCategory.USER,
    ParamCategory.PLATEFORME,
    ParamCategory.GROUPEMENT,
  ];

  // For super admin
  if (currentUser && currentUser.role && currentUser.role.code === SUPER_ADMINISTRATEUR) {
    getParametersVariables = [
      ...getParametersVariables,
      ParamCategory.GROUPEMENT_SUPERADMIN,
      ParamCategory.GROUPEMENT,
      ParamCategory.PHARMACIE,
    ];
  }

  // For admin groupement
  if (currentUser && currentUser.role && currentUser.role.code === ADMINISTRATEUR_GROUPEMENT) {
    getParametersVariables = [...getParametersVariables, ParamCategory.GROUPEMENT];
  }

  // For personnel pharmacie and titulaire
  if (
    currentUser &&
    currentUser.role &&
    currentUser.role.code &&
    (pharmacieAllRoles.includes(currentUser.role.code) ||
      currentUser.role.code === PARTENAIRE_SERVICE)
  ) {
    getParametersVariables = [...getParametersVariables, ParamCategory.PHARMACIE];
  }

  const getParameters = useQuery<PARAMETERS_BY_CATEGORIES, PARAMETERS_BY_CATEGORIESVariables>(
    GET_PARAMETERS_BY_CATEGORIES,
    { variables: { categories: getParametersVariables } },
  );

  if (getParameters) {
    if (getParameters && getParameters.data && getParameters.data.parametersByCategories) {
      const values = getParameters.data.parametersByCategories.map((parameter: any) => {
        let newParameter = parameter;

        if (newParameter && newParameter.value === null) {
          newParameter.value = { id: null, value: null, __typename: 'ParameterValue' };
        }

        if (parameter && parameter.parameterType === null) {
          newParameter = {
            ...newParameter,
            parameterType: { id: null, code: '', nom: '', __typename: 'parameterType' },
          };
        }

        if (parameter && parameter.parameterGroupe === null) {
          newParameter = {
            ...newParameter,
            parameterGroupe: { id: null, code: '', nom: '', __typename: 'ParameterGroupe' },
          };
        }

        return newParameter;
      });

      client.writeData({
        data: {
          parameters: values.map(value => {
            return {
              ...value,
              options: value.options || [
                { label: 'Désactivé', value: 'disabled', __typename: 'ParameterOption' }, //Why : option par défaut sinon error dans le store
              ],
            };
          }),
          __typename: 'parameters',
        },
      });
    }
  }

  useEffect(() => {
    const myPharmacieResult =
      myPharmacie && myPharmacie.data && myPharmacie.data.me && myPharmacie.data.me.pharmacie
        ? myPharmacie.data.me.pharmacie
        : null;

    const pharmaciesListResult = pharmaciePartenaires
      ? pharmaciePartenaires
      : groupementResult?.data?.groupement?.defaultPharmacie
      ? [groupementResult?.data?.groupement?.defaultPharmacie]
      : null;

    const defaultPharmacie =
      pharmaciesListResult && pharmaciesListResult[0] ? pharmaciesListResult[0] : null;

    console.log(
      '********************LIST PHARMA***************',
      pharmaciesListResult,
      defaultPharmacie,
    );
    const storagePharmacie = getPharmacie();

    setPharmaciesList(pharmaciesListResult);
    if (
      storagePharmacie?.id
      // currentUser &&
      // currentUser.role &&
      // currentUser.role.code === SUPER_ADMINISTRATEUR &&
      // pharmaciesListResult?.length
      // pharmaciesListResult.map(pharma => pharma && pharma.id).includes(storagePharmacie.id)
    ) {
      console.log('********************STORAGE PHARMA***************', storagePharmacie);

      //const currentPharma = pharmaciesListResult.find(pharma => pharma?.id === storagePharmacie.id)
      //if (currentPharma) {
      setCurrentPharmacie(storagePharmacie);
      setPharmacie(storagePharmacie);
      //}
    } else if (pharmaciesListResult?.length) {
      const pharma = myPharmacieResult || defaultPharmacie;
      setCurrentPharmacie(pharma);
      setPharmacie(pharma);
    }
  }, [myPharmacie, groupementResult]);

  const setCurrentPharmacie = (pharmacie: MY_PHARMACIE_me_pharmacie | null) => {
    client.writeData({ data: { pharmacie } });
  };

  const { data: msgNonLus, refetch: refetchMsg } = useQuery<TOTAL_MESSAGERIE_NON_LUS>(
    GET_TOTAL_MESSAGERIE_NON_LUS,
    {
      fetchPolicy: 'cache-and-network',
    },
  );

  const { data: msgData } = useQuery<TOTAL_MESSAGERIES, TOTAL_MESSAGERIESVariables>(
    GET_TOTAL_MESSAGERIES,
    {
      fetchPolicy: 'cache-and-network',
      variables: { isRemoved: false, typeMessagerie: MessagerieType.R },
    },
  );

  // Refetch msg
  useEffect(() => {
    if (pathname === '/') {
      refetchMsg();
    }
  }, [pathname]);

  const nbMsg = (msgData && msgData.messageries && msgData.messageries.total) || 0;
  const nbMsgNonLus =
    (msgNonLus && msgNonLus.messagerieNonLus && msgNonLus.messagerieNonLus.total) || 0;

  /* Listener for messages */

  useSubscription<SEND_MESSAGE_SUBSCRIPTION>(GET_SEND_MESSAGE_SUBSCRIPTION, {
    onSubscriptionData: ({ client, subscriptionData }) => {
      if (subscriptionData.data && subscriptionData.data.messagerieEnvoyee) {
        const oldMsg = msgData && msgData.messageries && msgData.messageries;
        if (oldMsg) {
          client.writeQuery<TOTAL_MESSAGERIES, TOTAL_MESSAGERIESVariables>({
            query: GET_TOTAL_MESSAGERIES,
            data: {
              messageries: {
                ...oldMsg,
                ...{
                  total: oldMsg && oldMsg.total ? +oldMsg.total + 1 : nbMsg,
                },
              },
            },
            variables: { isRemoved: false, typeMessagerie: MessagerieType.R },
          });
        }

        const oldMsgNonLus = msgNonLus && msgNonLus.messagerieNonLus && msgNonLus.messagerieNonLus;
        if (oldMsgNonLus) {
          client.writeQuery<TOTAL_MESSAGERIE_NON_LUS>({
            query: GET_TOTAL_MESSAGERIE_NON_LUS,
            data: {
              messagerieNonLus: {
                ...oldMsgNonLus,
                ...{
                  total: oldMsgNonLus && oldMsgNonLus.total ? oldMsgNonLus.total + 1 : nbMsgNonLus,
                },
              },
            },
          });
        }
      }
    },
  });

  useSubscription<MESSAGE_LU_SUBSCRIPTION>(GET_MESSAGE_LU_SUBSCRIPTION, {
    onSubscriptionData: ({ client, subscriptionData }) => {
      if (subscriptionData.data && subscriptionData.data.messagerieLu) {
        const oldMsgNonLus = msgNonLus && msgNonLus.messagerieNonLus && msgNonLus.messagerieNonLus;
        if (oldMsgNonLus) {
          client.writeQuery<TOTAL_MESSAGERIE_NON_LUS>({
            query: GET_TOTAL_MESSAGERIE_NON_LUS,
            data: {
              messagerieNonLus: {
                ...oldMsgNonLus,
                ...{
                  total: oldMsgNonLus && oldMsgNonLus.total ? oldMsgNonLus.total - 1 : nbMsgNonLus,
                },
              },
            },
          });
        }
      }
    },
  });

  useSubscription<MESSAGE_SUPPRIMEE_SUBSCRIPTION>(GET_MESSAGE_SUPPRIMEE_SUBSCRIPTION, {
    onSubscriptionData: ({ client, subscriptionData }) => {
      if (subscriptionData.data && subscriptionData.data.messagerieSupprimee) {
        const oldMsg = msgData && msgData.messageries && msgData.messageries;

        if (oldMsg) {
          client.writeQuery<TOTAL_MESSAGERIES, TOTAL_MESSAGERIESVariables>({
            query: GET_TOTAL_MESSAGERIES,
            data: {
              messageries: {
                ...oldMsg,
                ...{
                  total: oldMsg && oldMsg.total ? +oldMsg.total - 1 : nbMsg,
                },
              },
            },
            variables: { isRemoved: false, typeMessagerie: MessagerieType.R },
          });
        }
      }
    },
  });

  // Dasboard menu
  const messagerieRoutePrefix = '/messagerie';
  const dashboardRoutePrefix = '/db/';
  const panierRoutePrefix = '/panier';
  const autrepanierRoutePrefix = '/autrepanier';
  const produitDetailsRoutePrefix = '/catalogue-produits/card/';
  const gestionProduitsRoutePrefix = '/gestion-produits';
  const pilotageRoutePrefix = '/pilotage';
  const aideSupportRoutePrefix = '/aide-support';
  const marcheRoutePrefix = MARCHE_BASE_URL;
  const promotionRoutePrefix = PROMO_BASE_URL;
  const intelligenceCollectiveRoutePrefix = INTELLIGENCE_COLLECTIVE_URL;

  const onMessagerie = pathname.startsWith(messagerieRoutePrefix);
  const onDashboard = pathname.startsWith(dashboardRoutePrefix);
  const onPanier = pathname.startsWith(panierRoutePrefix);
  const onAutrePanier = pathname.startsWith(autrepanierRoutePrefix);
  const onProduitDetails = pathname.startsWith(produitDetailsRoutePrefix);
  const onPilotage = pathname.startsWith(pilotageRoutePrefix);
  const onGestionProduits = pathname.startsWith(gestionProduitsRoutePrefix);
  const onMarche = pathname.startsWith(marcheRoutePrefix);
  const onPromotion = pathname.startsWith(promotionRoutePrefix);
  const isSuperAdmin = user.role.code === SUPER_ADMINISTRATEUR;
  const onPharmacieDetails = pathname.includes('/pharmacies/fiche/');
  const onPrestataireDetails = pathname.includes('/partenaires-services/fiche/');
  const onLaboPartenaireDetails = pathname.includes('/partenaires-laboratoires/fiche/');
  const isOnIdeaDtails = pathname.includes(`${PARTAGE_IDEE_URL}/fiche`);
  const onAideSupport = pathname.includes(aideSupportRoutePrefix);
  const onIntelligenceCollective = pathname.includes(intelligenceCollectiveRoutePrefix);

  const mobileUrls = [
    '/todo/recu',
    '/todo/recu-equipe',
    '/todo/aujourdhui',
    '/todo/aujourdhui-equipe',
    '/todo/sept-jours',
    '/todo/sept-jours-equipe',
    '/todo/sans-date',
    '/todo/tout-voir',
  ];

  const { loading, data } = useQuery<USER_ROLES>(GET_USER_ROLES);
  const initialRoles: string[] = [];
  const [roles, setRoles] = useState(initialRoles);
  const helloidParam = useValueParameterAsBoolean('0046');
  const activeDirectoryParam = useValueParameterAsBoolean('0049');
  const ftpParam = useValueParameterAsBoolean('0050');
  const applicationsParam = useValueParameterAsBoolean('0051');

  /*
        Removed on menu after client feedback
        {
          label: 'Groupements',
          path: `${dashboardRoutePrefix}groupements`,
          authorizeRoles: roles.filter(value => value === SUPER_ADMINISTRATEUR),
        },
        */

  //  Daily Alert related to the todo

  // const dateNow = moment();
  // const isLocalDate: boolean = localStorage.getItem('lastDate') ? true : false;

  // const lastDate = localStorage.getItem('lastDate');
  // const dateIsOver: boolean = dateNow.diff(lastDate, 'days') >= 1;

  // const [openDailyAlert, setOpenDailyAlert] = useState<boolean>(!isLocalDate || dateIsOver);

  // const handleCloseDailyAlert = () => {
  //   localStorage.setItem('lastDate', dateNow.format('dddd Do MMMM YYYY'));
  //   setOpenDailyAlert(false);
  // };

  const handleSwipeMenu = () => {
    client.writeData({ data: { sidebar: { state: true, __typename: 'sidebarstate' } } });
  };

  // const handleClickFabDashboard = () => {
  //   history.push('/dashboard');
  // };

  let menuItems: any[] = onDashboard
    ? [
        {
          label: 'Mon compte',
          path: `${dashboardRoutePrefix}user-settings`,
          authorizeRoles: roles,
          disabled: false,
          authorizedParameter: true,
          icon: <MyAccountIcon />,
        },
        {
          label: 'Thème',
          path: `${dashboardRoutePrefix}theme`,
          authorizeRoles: roles,
          disabled: !auth.isAuthorizedToChangeTheme(),
          authorizedParameter: true,
          icon: <ThemeIcon />,
        },
        {
          label: 'Utilitaire',
          path: `${dashboardRoutePrefix}utilitaire`,
          authorizeRoles: roles.filter(
            value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
          ),
          disabled: !auth.isAuthorizedToReIndex(),
          authorizedParameter: true,
          icon: <UtilityIcon />,
        },
        {
          label: 'Espace publicitaire',
          path: `${dashboardRoutePrefix}espace-publicitaire`,
          authorizeRoles: roles.filter(
            value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
          ),
          disabled: false,
          authorizedParameter: true,
          icon: <PubIcon />,
        },
        {
          label: "Gestion d'avatar",
          path: `${dashboardRoutePrefix}avatar`,
          authorizeRoles: roles.filter(
            value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
          ),
          disabled: false,
          authorizedParameter: true,
          icon: <Face />,
        },
        {
          label: 'Todo',
          path: `${dashboardRoutePrefix}${COLORS_URL}`,
          authorizeRoles: roles.filter(
            value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
          ),
          disabled: false,
          authorizedParameter: true,
          icon: <ListAlt />,
        },

        {
          label: 'Espace documentaire',
          path: `${dashboardRoutePrefix}${CATEGORIE_URL}`,
          authorizeRoles: roles.filter(
            value =>
              value === SUPER_ADMINISTRATEUR ||
              value === ADMINISTRATEUR_GROUPEMENT ||
              value === TITULAIRE_PHARMACIE,
          ),
          disabled: false,
          authorizedParameter: true,
          icon: <CategoryIcon />,
          divider: false,
        },
        {
          label: 'Modèle opération marketing',
          path: `${dashboardRoutePrefix}${MODELE_OPERATION_MARKETING_URL}`,
          authorizeRoles: roles.filter(
            value =>
              value === SUPER_ADMINISTRATEUR ||
              value === ADMINISTRATEUR_GROUPEMENT ||
              value === TITULAIRE_PHARMACIE,
          ),
          disabled: false,
          authorizedParameter: true,
          icon: <DashboardIcon />,
          divider: false,
        },
        {
          label: 'Remises',
          path: `${dashboardRoutePrefix}${REMISE_URL}`,
          authorizeRoles: roles.filter(value => value === SUPER_ADMINISTRATEUR),
          disabled: false,
          authorizedParameter: true,
          icon: <MonetizationOn />,
          divider: false,
        },
        /*{
          label: 'Opération marketing',
          path: `${dashboardRoutePrefix}${OPERATION_MARKETING_URL}`,
          authorizeRoles: roles.filter(
            value =>
              value === SUPER_ADMINISTRATEUR ||
              value === ADMINISTRATEUR_GROUPEMENT ||
              value === TITULAIRE_PHARMACIE,
          ),
          disabled: false,
          authorizedParameter: true,
          icon: <AccessibilityIcon />,
          divider: false,
        },*/
        {
          label: 'Tâche Récurrente',
          path: `${dashboardRoutePrefix}${TRAITEMENT_AUTOMATIQUE_URL}`,
          authorizeRoles: roles,
          disabled: false,
          authorizedParameter: true,
          icon: <SettingsApplications />,
          divider: true,
        },

        {
          label: 'Approbation des documents',
          path: `${dashboardRoutePrefix}${ESPACE_DOCUMENTAIRE_URL}`,
          authorizeRoles: roles.filter(
            value =>
              value === SUPER_ADMINISTRATEUR ||
              value === ADMINISTRATEUR_GROUPEMENT ||
              value === TITULAIRE_PHARMACIE,
          ),
          disabled: false,
          authorizedParameter: true,
          icon: <LibraryBooks />,
          divider: false,
        },
        {
          label: `Groupe d'amis`,
          path: `${dashboardRoutePrefix}${GROUPE_AMIS}`,
          authorizeRoles: roles.filter(
            value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
          ),
          disabled: false,
          authorizedParameter: true,
          icon: <GroupAdd />,
          divider: false,
        },

        {
          label: 'Partage idées/bonnes pratiques',
          path: `${dashboardRoutePrefix}partage-idee`,
          authorizeRoles: roles.filter(
            value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
          ),
          disabled: false,
          authorizedParameter: true,
          icon: <PsychologyIcon color="inherit" />,
          divider: false,
        },
        {
          label: 'RGPD',
          path: `${dashboardRoutePrefix}rgpd`,
          authorizeRoles: roles.filter(
            value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
          ),
          disabled: false,
          authorizedParameter: true,
          icon: <PsychologyIcon color="inherit" />,
          divider: true,
        },

        {
          label: 'Matrice des fonctions de Base',
          path: `${dashboardRoutePrefix}matrice_taches`,
          authorizeRoles: roles.filter(
            value =>
              value === SUPER_ADMINISTRATEUR ||
              value === ADMINISTRATEUR_GROUPEMENT ||
              value === TITULAIRE_PHARMACIE,
          ),
          disabled: false,
          authorizedParameter: true,
          icon: <ViewModule />,
        },
        {
          label: 'Démarche qualité',
          path: `${dashboardRoutePrefix}demarche_qualite`,
          authorizeRoles: roles.filter(
            value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
          ),
          disabled: false,
          authorizedParameter: true,
          icon: <Face />,
        },
        {
          label: 'Check-list qualité',
          path: `${dashboardRoutePrefix}check-list-qualite`,
          authorizeRoles: roles.filter(
            value =>
              value === SUPER_ADMINISTRATEUR ||
              value === ADMINISTRATEUR_GROUPEMENT ||
              value === TITULAIRE_PHARMACIE,
          ),
          disabled: false,
          authorizedParameter: true,
          icon: <Face />,
          divider: true,
        },
        {
          label: 'Groupement',
          path: `${dashboardRoutePrefix}groupement`,
          authorizeRoles: roles.filter(value => value === SUPER_ADMINISTRATEUR),
          disabled: false,
          authorizedParameter: true,
          icon: <GroupementIcon />,
        },
        {
          label: 'Liste du Personnel du groupement',
          path: `${dashboardRoutePrefix}${PERSONNEL_GROUPEMENT_URL}`,
          authorizeRoles: roles.filter(
            value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
          ),
          disabled: false,
          authorizedParameter: true,
          icon: <GroupIcon />,
        },
        {
          label: 'Options Groupement',
          path: `${dashboardRoutePrefix}options-groupement`,
          authorizeRoles: roles.filter(
            value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
          ),
          disabled: !auth.isAuthorizedToViewGroupementOptions(),
          authorizedParameter: true,
          icon: <OptionGroupementIcon />,
          divider: true,
        },
        {
          label: 'Groupes de clients',
          path: `${dashboardRoutePrefix}${GROUPES_CLIENTS_URL}`,
          authorizeRoles: roles.filter(
            value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
          ),
          disabled: false, // !auth.isAuthorizedToViewPharmacieList()
          authorizedParameter: true,
          icon: <GroupWork />,
        },
        {
          label: 'Commande orale',
          path: `${dashboardRoutePrefix}commande-orale`,
          authorizeRoles: roles,
          disabled: false, // !auth.isAuthorizedToViewPharmacieList()
          authorizedParameter: true,
          icon: <LocalMall />,
        },
        {
          label: 'Pharmacies',
          path: `${dashboardRoutePrefix}${PHARMACIE_URL}`,
          authorizeRoles: roles.filter(
            value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
          ),
          disabled: !auth.isAuthorizedToViewPharmacieList(),
          authorizedParameter: true,
          icon: <PharmacieIcon />,
        },
        {
          label:
            user && user.role && user.role.code === TITULAIRE_PHARMACIE
              ? 'Titulaires de la pharmacie'
              : 'Titulaires des pharmacies',
          path: `${dashboardRoutePrefix}${TITULAIRE_PHARMACIE_URL}`,
          authorizeRoles: roles.filter(
            value =>
              value === SUPER_ADMINISTRATEUR ||
              value === ADMINISTRATEUR_GROUPEMENT ||
              value === TITULAIRE_PHARMACIE,
          ),
          disabled: !auth.isAuthorizedToViewTitulairePharmacieList(),
          authorizedParameter: true,
          icon: <TitulaireIcon />,
        },
        {
          label:
            currentUser?.role?.code === TITULAIRE_PHARMACIE
              ? 'Liste du Personnel de la Pharmacie'
              : 'Liste des Personnel des pharmacies',
          path: `${dashboardRoutePrefix}${PERSONNEL_PHARMACIE_URL}`,
          authorizeRoles: roles.filter(
            value =>
              value === SUPER_ADMINISTRATEUR ||
              value === ADMINISTRATEUR_GROUPEMENT ||
              value === TITULAIRE_PHARMACIE ||
              value === PRESIDENT_REGION,
          ),
          disabled: !auth.isAuthorizedToViewPersonnelPharmacieList(),
          authorizedParameter: true,
          icon: <GroupIcon />,
        },
        {
          label: "Score des Fiches d'Incidents",
          path: `${dashboardRoutePrefix}${DASHBOARD_ITEM_SCORING_URL}`,
          authorizeRoles: roles.filter(
            value =>
              value === SUPER_ADMINISTRATEUR ||
              value === ADMINISTRATEUR_GROUPEMENT ||
              value === TITULAIRE_PHARMACIE,
          ),
          disabled: false,
          authorizedParameter: true,
          icon: <Grade />,
        },

        {
          label: 'Options Pharmacie',
          path: `${dashboardRoutePrefix}options-pharmacie`,
          authorizeRoles: roles.filter(
            value =>
              value === SUPER_ADMINISTRATEUR ||
              value === ADMINISTRATEUR_GROUPEMENT ||
              value === TITULAIRE_PHARMACIE,
          ),
          disabled: false,
          authorizedParameter: true,
          icon: <OptionGroupementIcon />,
          divider: true,
        },
        {
          label: 'Président des régions',
          path: `${dashboardRoutePrefix}${PRESIDENT_REGION_URL}`,
          authorizeRoles: roles.filter(
            value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
          ),
          disabled: !auth.isAuthorizedToViewPresidentRegionList(),
          authorizedParameter: true,
          icon: <PresidentIcon />,
        },
        {
          label: 'Laboratoires',
          path: `${dashboardRoutePrefix}${LABORATOIRE_URL}`,
          authorizeRoles: roles.filter(
            value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
          ),
          disabled: !auth.isAuthorizedToViewLaboratoirePartenaireList(),
          authorizedParameter: true,
          icon: <LaboIcon />,
        },
        /* {
        label: 'Laboratoires',
        path: `${dashboardRoutePrefix}${PARTENAIRE_LABORATOIRE_URL}`,
        authorizeRoles: roles.filter(
          value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
        ),
        disabled: !auth.isAuthorizedToViewLaboratoirePartenaireList(),
        authorizedParameter: true,
        icon: <LaboIcon />,
      }, */
        {
          label: 'Partenaires de Services',
          path: `${dashboardRoutePrefix}${PARTENAIRE_SERVICE_URL}`,
          authorizeRoles: roles.filter(
            value =>
              value === SUPER_ADMINISTRATEUR ||
              value === ADMINISTRATEUR_GROUPEMENT ||
              value === TITULAIRE_PHARMACIE,
          ),
          disabled: !auth.isAuthorizedToViewPartenaireServiceList(),
          authorizedParameter: true,
          icon: <PartenaireIcon />,
        },
        {
          label: 'Paramètre SSO',
          path: `${dashboardRoutePrefix}sso-parameter`,
          authorizeRoles: roles.filter(
            value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
          ),
          disabled: false,
          authorizedParameter: activeDirectoryParam || applicationsParam || helloidParam,
          icon: <SettingsApplicationsIcon />,
        },
        {
          label: 'Rôles',
          path: `${dashboardRoutePrefix}roles`,
          authorizeRoles: roles.filter(
            value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
          ),
          disabled: false,
          authorizedParameter: true,
          icon: <VerifiedUserIcon />,
        },

        {
          label: 'Gestion de données de base',
          path: `${dashboardRoutePrefix}basis`,
          authorizeRoles: roles.filter(value => value === SUPER_ADMINISTRATEUR),
          disabled: false,
          authorizedParameter: true,
          icon: <BasisCrudIcon />,
        },

        {
          label: 'Paramètres Frigo',
          path: `${dashboardRoutePrefix}${PARAMETRE_FRIGO_URL}`,
          authorizeRoles: roles,
          disabled: false,
          authorizedParameter: true,
          icon: <FridgeOutline />,
          divider: true,
        },
        // {
        //   label: 'FTP',
        //   path: `${dashboardRoutePrefix}ftp`,
        //   authorizeRoles: roles.filter(
        //     value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
        //   ),
        //   disabled: false,
        //   authorizedParameter: ftpParam,
        // },
        // {
        //   label: 'HelloID',
        //   path: `${dashboardRoutePrefix}helloid-parameter`,
        //   authorizeRoles: roles.filter(
        //     value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
        //   ),
        //   disabled: false,
        //   authorizedParameter: helloidParam,
        // },
        // {
        //   label: 'Gestion des applications',
        //   path: `${dashboardRoutePrefix}manage-applications`,
        //   authorizeRoles: roles.filter(
        //     value => value === SUPER_ADMINISTRATEUR || value === ADMINISTRATEUR_GROUPEMENT,
        //   ),
        //   disabled: false,
        //   authorizedParameter: applicationsParam || helloidParam,
        // },
      ]
    : [
        {
          label: 'Actualités',
          path: `/actualites`,
          authorizeRoles: roles,
          disabled: false,
          authorizedParameter: true,
        },
        {
          label: 'Laboratoires',
          path: `/laboratoires`,
          authorizeRoles: roles,
          disabled: false,
          authorizedParameter: true,
        },

        {
          label: 'Opérations Commerciales',
          path: `/operations-commerciales`,
          authorizeRoles: roles,
          disabled:
            (currentUser && currentUser.role && currentUser.role.code === PARTENAIRE_LABORATOIRE) ||
            (currentUser && currentUser.role && currentUser.role.code === PARTENAIRE_SERVICE)
              ? true
              : false,
          authorizedParameter: true,
        },
        {
          label: 'Prestataires Partenaires',
          path: `/prestataires-partenaires`,
          authorizeRoles: roles,
          disabled: true,
          authorizedParameter: true,
        },
        {
          label: 'Services aux Pharmacies',
          path: `/services-pharmacies`,
          authorizeRoles: roles,
          disabled: true,
          authorizedParameter: true,
        },

        {
          label: 'Evènements',
          path: `/evenements`,
          authorizeRoles: roles,
          disabled: true,
          authorizedParameter: true,
        },

        {
          label: 'Formations',
          path: `/formations`,
          authorizeRoles: roles,
          disabled: true,
          authorizedParameter: true,
        },

        {
          label: 'Espace Documentaire',
          path: `/espace-documentaire`,
          authorizeRoles: roles,
          disabled: true,
          authorizedParameter: true,
        },
        {
          label: 'Modèle opération marketing',
          path: `/modele-operation-marketing`,
          authorizeRoles: roles,
          disabled: true,
          authorizedParameter: true,
        },
        {
          label: 'Démarche Qualité',
          path: `/demarche-qualite`,
          authorizeRoles: roles,
          disabled: true,
          authorizedParameter: true,
        },

        {
          label: 'Newsletters',
          path: `/newsletters`,
          authorizeRoles: roles,
          disabled: true,
          authorizedParameter: true,
        },
        {
          label: 'Suivi de Commandes',
          path: `/suivi-commandes`,
          authorizeRoles: roles,
          disabled: false,
          authorizedParameter: true,
        },
        {
          label: 'Catalogue des Produits',
          path: `/catalogue-produits/card`,
          authorizeRoles: roles,
          disabled: false,
          authorizedParameter: true,
        },
        {
          label: 'Todo',
          path: `/project`,
          authorizeRoles: roles,
          disabled: false,
          authorizedParameter: true,
        },
        {
          label: 'Thème',
          path: `/theme`,
          authorizeRoles: roles,
          disabled: false,
        },
      ];

  // Filltrer menu selon le rôle de l'utilisateur
  useEffect(() => {
    if (!loading && data && data.roles && data.roles.length) {
      const allRoles: string[] = [];
      data.roles.map(role => {
        if (role && role.code) {
          allRoles.push(role.code);
        }
      });
      setRoles(allRoles);
    }
  }, [data]);

  menuItems = menuItems.filter(menuItem => {
    if (menuItem.authorizeRoles && menuItem.authorizedParameter) {
      return menuItem.authorizeRoles.includes(user.role.code) && menuItem.authorizedParameter;
    }
  });

  const handleClickCloseBtn = () => {
    if (pathname.includes('panier')) {
      history.push(`/catalogue-produits/card`);
    } else if (pathname.includes('catalogue-produits') || onProduitDetails) {
      history.goBack();
    } else if (pathname.includes('/pilotage/dashboard/') && pilotageSource) {
      if (ocSourceResult.data && ocSourceResult.data.pilotageOcSource) {
        history.push(`/operations-commerciales/${pilotageSource.id}`);
      }
      if (actuSourceResult.data && actuSourceResult.data.pilotageActuSource) {
        history.push(`/actualite/${pilotageSource.id}`);
      }
      initPilotageApolloLocalState(client);
    } else {
      history.push('/');
    }
  };

  const DashboardDrawer = (
    <Box>
      <Box
        className={
          isSuperAdmin
            ? classes.contentLeft
            : `${classes.contentLeft} ${classes.contentLeftNotSuperAdmin}`
        }
      >
        <List className={classes.paddingNav} dense={true} disablePadding={true}>
          {menuItems.map((item, index) => {
            const pathName = pathname.split('/')[2];
            const itemPath = item.path.split('/')[2];
            return (
              <ListItem
                key={`menu-${index}`}
                className={classnames(
                  classes.listItem,
                  (pathname === item.path || (pathName && itemPath && pathName === itemPath)) &&
                    classes.listItemActive,
                )}
                button={true}
                onClick={() => {
                  setMobileOpen(false);
                  history.push(item.path);
                }}
                disabled={item.disabled}
              >
                <ListItemText>{item.label}</ListItemText>
              </ListItem>
            );
          })}
        </List>
      </Box>
    </Box>
  );

  const GroupementAndCloseButton = () => {
    return (
      <Box className={classes.groupementAndCloseButtonContainer}>
        {onPanier || onAutrePanier ? (
          <Fragment>
            <ShoppingCart fontSize="large" />
            <Typography variant="h2">Commandes</Typography>
          </Fragment>
        ) : onGestionProduits ? (
          <Fragment>
            <img src={BusinessIcon} />
            <Typography variant="h2">Gestion de produits</Typography>
          </Fragment>
        ) : onProduitDetails ? (
          <Typography variant="h2"> Details des produits</Typography>
        ) : onPilotage ? (
          <Fragment>
            <Assessment fontSize="large" />
            <Typography variant="h2">Pilotage de l'activité</Typography>
          </Fragment>
        ) : onAideSupport ? (
          <Fragment>
            <LiveHelpIcon fontSize="large" />
            <Typography variant="h2">Aide et support</Typography>
          </Fragment>
        ) : (
          onDashboard && (
            <Fragment>
              <Settings fontSize="large" />
              <Typography variant="h2">Paramètres</Typography>
            </Fragment>
          )
        )}
        {onPromotion && (
          <Fragment>
            <img src={GestionPromoIcon} alt="Promotion" />
            <Typography variant="h2">Gestion des promotions</Typography>
          </Fragment>
        )}
        {onMarche && (
          <Fragment>
            <img src={GestionMarcheIcon} alt="Marché" />
            <Typography variant="h2">Gestion des marchés</Typography>
          </Fragment>
        )}
        {onMessagerie && (
          <Fragment>
            <Email fontSize="large" />
            <Typography variant="h2">Ma messagerie</Typography>
          </Fragment>
        )}

        {onIntelligenceCollective && (
          <Fragment>
            <PsychologyIcon className={classes.navBarGroupementIcon} fontSize="large" />
            <Typography className={classes.fullScreenTitle}>Intelligence collective</Typography>
          </Fragment>
        )}

        <Tooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Quitter">
          <IconButton color="inherit" onClick={handleClickCloseBtn}>
            <CloseIcon />
          </IconButton>
        </Tooltip>
      </Box>
    );
  };

  const [openMobileProfileDrawer, setOpenMobileProfileDrawer] = useState<boolean>(false);

  const handleShowMobileProfile = () => {
    setOpenMobileProfileDrawer(true);
  };

  const container = window !== undefined ? () => window.document.body : undefined;

  const theme = useTheme();

  const memorized = React.useMemo(
    () => (
      <main style={{ width: '100%', overflowY: 'auto' }} className={classes.content}>
        <DashboardRouter key="dashboardrouter" user={user} />
        <MainRouter key="mainrouter" user={user} />
      </main>
    ),
    [pathname, groupementResult],
  );

  if (pathname === dashboardRoutePrefix) {
    const path = menuItems.length > 0 ? menuItems[0].path : '/';
    return <Redirect to={{ pathname: path }} />;
  }

  if (loading) return <Loader />;

  if (location.search) {
    const url = `${APP_SSO_URL}/open-helloid/${(groupement && groupement.id) || ''}/${token}${
      location.search
    }`;
    window.location.href = url;
    return null;
  }

  return (
    <Fragment>
      <Drawer
        variant="temporary"
        anchor="left"
        open={mobileOpen}
        onClose={handleDrawerToggle(setMobileOpen, mobileOpen)}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Box className={classes.flexMobLogo}>
          Menu
          <IconButton
            color="inherit"
            aria-label="Close drawer"
            edge="start"
            onClick={handleDrawerToggle(setMobileOpen, mobileOpen)}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        {DashboardDrawer}
      </Drawer>
      <Box
        className={classes.root}
        style={{
          paddingTop:
            (isMobile() && mobileUrls.includes(pathname)) ||
            (isMobile() && pathname.startsWith('/messagerie'))
              ? '50px'
              : '86px',
        }}
      >
        <CssBaseline />
        <Hidden
          smDown={
            mobileUrls.includes(pathname) ||
            pathname.startsWith('/todo/projet') ||
            pathname.startsWith('/todo/etiquette') ||
            pathname.startsWith('/messagerie')
              ? true
              : false
          }
          implementation="css"
        >
          <AppBar position="fixed" className={classnames(classes.appBar)}>
            <Toolbar className={classes.flexToolbar}>
              {!onDashboard &&
                !onPanier &&
                !onAutrePanier &&
                !onProduitDetails &&
                !onPilotage &&
                !onAideSupport &&
                !onPromotion &&
                !onMarche && (
                  <Fragment>
                    <Avatar className={classes.navigationAvatar} onClick={handleShowMobileProfile}>
                      {stringToAvatar(user.userName)}
                    </Avatar>

                    <Brand />
                    {!isMobile() && (
                      <IconButton style={{ color: '#FFF' }} onClick={() => history.push('/')}>
                        <Home />
                      </IconButton>
                    )}
                    <Hidden smDown={true}>
                      {user.groupement ? (
                        <Fragment>
                          {groupementLogo ? (
                            <img
                              className={classes.groupementLogo}
                              src={`${AWS_HOST}/${groupementLogo}`}
                              alt="Groupement Avatar"
                            />
                          ) : null /* (
                                <Box display="flex" alignItems="center" mr={2}>
                                  <GroupementIcon style={{ marginRight: 8 }} />
                                  <Typography className={classes.groupementName}>
                                    {user.groupement.nom}
                                  </Typography>
                                </Box>
                            )*/}
                        </Fragment>
                      ) : null}
                    </Hidden>
                  </Fragment>
                )}
              {onDashboard ||
              onPanier ||
              onAutrePanier ||
              onProduitDetails ||
              onPilotage ||
              onAideSupport ||
              onPromotion ||
              onMarche ? (
                <GroupementAndCloseButton />
              ) : (
                <Fragment>
                  <Hidden smDown={true}>
                    <Search />
                  </Hidden>
                  <Box display="flex">
                    <div className={classes.mobileSearch}>
                      <Search />
                    </div>

                    <Box className={classes.flexAppBar}>
                      <HeaderMenu nbMessage={nbMsgNonLus} />
                      <Profile />
                    </Box>
                  </Box>
                </Fragment>
              )}
            </Toolbar>
          </AppBar>
        </Hidden>
        <CssBaseline />
        {onDashboard &&
          !onPharmacieDetails &&
          !onPrestataireDetails &&
          !onLaboPartenaireDetails &&
          !onAideSupport &&
          !isOnIdeaDtails && (
            <nav className={classes.drawer} aria-label="Menus">
              <List className={classes.paddingNav} dense={true} disablePadding={true}>
                {menuItems.map((item, index) => {
                  const pathName = pathname.split('/')[2];
                  const itemPath = item.path.split('/')[2];
                  return (
                    <Fragment key={index}>
                      <ListItem
                        key={`menu-${index}`}
                        className={classnames(
                          classes.listItem,
                          (pathname === item.path ||
                            (pathName && itemPath && pathName === itemPath)) &&
                            classes.listItemActive,
                        )}
                        button={true}
                        // tslint:disable-next-line: jsx-no-lambda
                        onClick={() => history.push(item.path)}
                        disabled={item.disabled}
                      >
                        <ListItemIcon>{item.icon}</ListItemIcon>
                        <ListItemText>{item.label}</ListItemText>
                      </ListItem>

                      {item.divider && (
                        <Divider
                          className={classes.listItemDivider}
                          variant="inset"
                          component="li"
                        />
                      )}
                    </Fragment>
                  );
                })}
              </List>
            </nav>
          )}
        {(helloidParam || applicationsParam) && <Applications />}
        {!isMobile() && <OutilsDigitaux />}
        {memorized}
        <Drawer
          container={container}
          variant="temporary"
          anchor={theme.direction === 'rtl' ? 'right' : 'left'}
          open={openMobileProfileDrawer}
          onClose={() => setOpenMobileProfileDrawer(false)}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          <Profile inDrawer={true} onDrawerClose={() => setOpenMobileProfileDrawer(false)} />
        </Drawer>
      </Box>
      <Box className={classes.fabsContainer}>
        {displayRapportFab && (
          <Fab
            color="secondary"
            className={classes.rapportFab}
            onClick={() => history.push('/rapport', { from: pathname })}
          >
            <Equalizer />
          </Fab>
        )}
      </Box>
    </Fragment>
  );
};

export default withRouter(Menus);
