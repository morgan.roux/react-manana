import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper, { PaperProps } from '@material-ui/core/Paper';
import Draggable from 'react-draggable';
import { detect } from 'detect-browser';
import { APP_SSO_URL, FIREFOX_EXTENSION_LINK } from '../../../config';
 
const browser = detect();

export interface DialogProps{
  open : boolean,
  hrefLink : string | null
  setOpen : React.Dispatch<React.SetStateAction<boolean>>
}
export enum DialogAction{
  close,
  goToExtension,
  continueAnyway
}

function PaperComponent(props: PaperProps) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

export default function DraggableDialog(props : DialogProps) {
  // const [open, setOpen] = React.useState(false);
  const { open , setOpen, hrefLink } = props;

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = (action : DialogAction) => {
    switch (action) {
      case DialogAction.goToExtension:
        switch (browser && browser.name) {
          case 'chrome':
            console.log("install extension for chrome");
            window.open(`${APP_SSO_URL}/extension/download/chrome`, "_blank");
            setOpen(false);
            break;
          case 'firefox':
            console.log("install extension for firefox ", FIREFOX_EXTENSION_LINK);
            window.open(FIREFOX_EXTENSION_LINK || `${APP_SSO_URL}/extension/download/firefox`, "_blank");
            setOpen(false);
            break;
          case 'edge':
            console.log("install extension for edge");
            window.open(`${APP_SSO_URL}/extension/download/edge`, "_blank");
            setOpen(false);
            break;
          default:
            console.log('browser not supported');
            setOpen(false);
            break;
        }
        break;
      case DialogAction.continueAnyway:
        console.log("continue anyway")
        if (hrefLink) window.open(hrefLink, "_blank");
        setOpen(false);
        break;
      default:
        setOpen(false);
        break;
    }
    
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        PaperComponent={PaperComponent}
        aria-labelledby="draggable-dialog-title"
      >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          Installer l'extension
        </DialogTitle>
        <DialogContent>
          <DialogContentText color="inherit">
            Veuillez installer l'extension Digital4win pour automatiser la connexion avec les applications d'HelloId.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={(e) => {handleClose(DialogAction.close)}}  variant="contained">
            Annuler
          </Button>
          <Button autoFocus onClick={(e) => {handleClose(DialogAction.continueAnyway)}} variant="contained">
            Continuer
          </Button>
          <Button autoFocus onClick={(e) => {handleClose(DialogAction.goToExtension)}} variant="contained" color="secondary">
            Installer
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
