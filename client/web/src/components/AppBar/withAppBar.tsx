import React, { FC } from 'react';
import { useStyles } from './styles';
import AppBar from '.';
import { Grid } from '@material-ui/core';

const withAppBar = (wrappedComponent: any) => {


    return class extends React.Component {

        render = () => {
            return (
                <Grid container={true}>
                    <Grid item sm={12}>
                        <AppBar />
                    </Grid>
                    <Grid item sm={12}>
                        {wrappedComponent}
                    </Grid>
                </Grid>

            )
        }
    }
};

export default withAppBar;