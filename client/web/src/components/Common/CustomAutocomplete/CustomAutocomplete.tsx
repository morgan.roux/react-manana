import React, { FC, ChangeEvent, ReactNode, useEffect, useState, useRef } from 'react';
import { Autocomplete, AutocompleteProps } from '@material-ui/lab';
import useStyles from './styles';
import { LoaderSmall } from '../../Dashboard/Content/Loader';
import { CircularProgress, debounce, TextField } from '@material-ui/core';
import { TextFieldProps } from '@material-ui/core/TextField';
import { CustomFormTextField } from '../CustomTextField';

interface CustomAutocompleteProps {
  optionLabelKey: string;
  label?: string;
  required?: boolean;
  onAutocompleteChange: (value: any) => void;
  search?: (searchText: string) => void;
}

const CustomAutocomplete: FC<CustomAutocompleteProps &
  Partial<AutocompleteProps<any, any, any, any>>> = props => {
  const classes = useStyles({});
  const {
    renderInput,
    renderOption,
    optionLabelKey,
    loading,
    label,
    required,
    onAutocompleteChange,
    search,
    value,
    placeholder,
  } = props;

  const [searchText, setSearchText] = useState<string>('');

  const handleAutocompleteChange = (e: ChangeEvent<any>, inputValue: any) => {
    if (e && e.type === 'click') {
      onAutocompleteChange(inputValue);
    }
    if (e && e.type === 'change' && inputValue !== undefined && typeof inputValue === 'string') {
      onAutocompleteChange({ id: '', [optionLabelKey]: inputValue, __typename: '' });
      if (search) {
        setSearchText(inputValue);
      }
    }
  };

  const debouncedSearch = useRef(
    debounce((searchText?: string) => {
      if (search && searchText) {
        search(searchText);
      }
    }, 1000),
  );

  useEffect(() => {
    debouncedSearch.current(searchText);
  }, [searchText]);

  useEffect(() => {
    if (value) {
      setSearchText(value[optionLabelKey]);
    }
  }, [value]);

  return (
    <Autocomplete
      {...(props as any)}
      autoHighlight
      getOptionLabel={(option: any) => option[optionLabelKey]}
      className={classes.autocomplete}
      onInputChange={handleAutocompleteChange}
      onChange={handleAutocompleteChange}
      renderOption={
        renderOption || ((option: any) => <React.Fragment>{option[optionLabelKey]}</React.Fragment>)
      }
      inputValue={searchText}
      renderInput={
        renderInput ||
        (params => (
          <CustomFormTextField
            {...params}
            placeholder={placeholder}
            label={label}
            required={required}
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <React.Fragment>
                  {loading ? <CircularProgress color="inherit" size={20} /> : null}
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
            }}
          />
        ))
      }
    />
  );
};

export default CustomAutocomplete;
