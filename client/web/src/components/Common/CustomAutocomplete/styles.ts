import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    autocomplete: {
      width: '100%',
      '& .MuiAutocomplete-inputRoot input': {
        padding: '2.5px 14px !important',
      },
      '& input': {
        color: theme.palette.common.black,
      },
      '& label': {
        color: theme.palette.common.black,
        '& .MuiFormLabel-asterisk': {
          color: 'red',
        },
      },
      '&:hover label, &.Mui-focused label': {
        color: theme.palette.primary.main,
      },
      '&.Mui-focused fieldset': {
        border: `2px solid ${theme.palette.primary.main} !important`,
      },
      '& input:invalid + fieldset': {
        borderColor: 'red',
      },
      '& .Mui-focused .MuiIconButton-label svg': {
        color: `${theme.palette.primary.main} !important`,
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: theme.palette.common.white,
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          // borderColor: theme.palette.common.black,
        },
        '&:hover fieldset': {
          borderColor: theme.palette.primary.main,
        },
        '&.Mui-focused fieldset': {
          borderColor: theme.palette.primary.main,
        },
      },
      '& .Mui-disabled': {
        '& input': {
          // color: 'rgba(0, 0, 0, 0.38)',
        },
      },
      '&:hover .MuiOutlinedInput-notchedOutline': {
        borderColor: theme.palette.primary.main,
      },
    },
  }),
);

export default useStyles;
