import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    usersModalRoot: {
      width: '100%',
      '& .MuiButton-label': {
        zIndex: 'auto',
      },
    },
    tableContainer: {
      width: '100%',
    },
  }),
);

export default useStyles;
