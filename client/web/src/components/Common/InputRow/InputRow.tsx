import * as React from 'react';
import useStyles from './styles';
import { Typography } from '@material-ui/core';

interface IInputRowProps {
  title: string;
  required?: boolean;
}

const InputRow: React.FC<IInputRowProps> = props => {
  const classes = useStyles({});
  const { title, children, required } = props;
  return (
    <div className={classes.formRow}>
      <Typography className={required ? classes.inputLabel : undefined}>
        {title}
        {required ? <span>*</span> : null}
      </Typography>
      {children}
    </div>
  );
};

export default InputRow;
