import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ component: Component, ...rest }: any) => {
  return (
    <Route
      {...rest}
      render={props =>
        localStorage.getItem('access_token') ? (
          <Component {...props} />
        ) : (
            <Redirect to={{ pathname: '/signin', state: { from: props.location } }} />
          )
      }
    />
  );
};
export default PrivateRoute;
