import React from 'react';
import { Redirect } from 'react-router-dom';
import moment from 'moment';
import { ApolloClient } from 'apollo-client';

import { Query } from 'react-apollo';
import { GET_ME } from '../../../graphql/Authentication/query';
import createClient from './../../../apolloClient';
import { setUser, getUser, setGroupement, getGroupement } from './../../../services/LocalStorage';
import { SUPER_ADMINISTRATEUR } from '../../../Constant/roles';
import MobileQuickAccess from '../MobileQuickAccess';

interface WithUserOptions {
  withGroupement?: boolean;
  withLastPasswordChanged?: boolean;
  permitRoles?: string[];
}

const withUser = (WrappedComponent: any, options?: WithUserOptions) => {
  return class extends React.Component {
    handleComplete = (query: any) => {
      if (query && query.me) {
        setUser(query.me);
      }
    };

    render = () => {
      const authToken = localStorage.getItem('access_token');
      console.log('*******************************AUTH TOKEN***************', authToken)
      if (!authToken) {
        return this.redirectToSignin();
      }

      const user = getUser();
      if (user) {
        this.checkAuthToken(createClient(authToken), user);
        return this.goToNext(user);
      }

      const client = createClient(authToken);
      return (
        <Query query={GET_ME} client={client} onCompleted={this.handleComplete}>
          {({ error, loading, data }: any) => {
            if (error && window.location.hash !== '#/password/update') {
              console.log('*******************************REDIRECT ERROR QUERY***************', error)

              return this.redirectToSignin();
            } else if (loading || !data) {
              return null;
            }
            if (!loading && !data?.me && window.location.hash !== '#/password/update') {
              console.log('*******************************REDIRECT ERROR NULL DATA***************', data?.me)
              localStorage.clear();
              return this.redirectToSignin();
            }

            return this.goToNext(data.me);
          }}
        </Query>
      );
    };

    checkAuthToken = (client: ApolloClient<any>, currentUser: any) => {
      if (currentUser.groupement) {
        client
          .query({ query: GET_ME })
          .then(({ data, loading }) => {
            if (!loading && (data?.me?.id !== currentUser.id)) {
              this.clearUserAndReload();
            }
          })
          .catch((error) => {
            this.clearUserAndReload();
          });
      }
    };

    clearUserAndReload = () => {
      if (window.location.hash !== '#/password/update') {
        localStorage.clear();
        window.location.reload();
      }
    };

    goToNext = (user: any) => {
      if (options && options.permitRoles) {
        if (!options.permitRoles.includes(user.role.code)) {
          console.log('*******************************REDIRECT ERROR NO PERMIT***************', user.role.code, options.permitRoles)

          if (window.location.hash !== '#/password/update') {
            localStorage.clear();
            return this.redirectToSignin();
          }
        }
      }

      if (options && options.withGroupement && user.role.code === SUPER_ADMINISTRATEUR) {
        if (
          !user.lastPasswordChangedDate ||
          moment(Date.now()).diff(moment(user.lastPasswordChangedDate), 'day') > 180
          && window.location.hash !== '#/password/update'
        ) {
          return <Redirect to={`/password/update`} />;
        }

        const groupement = getGroupement();
        if (!groupement) {
          return this.redirectToSelectGroupement();
        }

        return (
          <div>
            <WrappedComponent user={{ ...user, groupement }} {...this.props} />
            <MobileQuickAccess />
          </div>
        );
      } else {
        if (user && user.groupement) setGroupement(user.groupement);

        localStorage.setItem('theme', (user && user.theme) || 'onyx');

        if (user && user.status === 'RESETED' && window.location.hash !== '#/password/update') {
          return <Redirect to={{ pathname: '/password/update' }} />;
        }
      }

      return (
        <>
          <WrappedComponent user={user} {...this.props} />
          <MobileQuickAccess />
        </>
      );
    };

    redirectToSignin = () => {
      return <Redirect to={{ pathname: '/signin' }} />;
    };

    redirectToSelectGroupement = () => {
      if (window.location.search) {
        return <Redirect to={`/groupement/choice${window.location.search}`} />;
      }
      return <Redirect to={{ pathname: '/groupement/choice' }} />;
    };
  };
};

export default withUser;
