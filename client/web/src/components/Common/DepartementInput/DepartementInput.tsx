import React, { FC, useEffect } from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { DepartementInfo } from '../../../federation/basis/departement/types/DepartementInfo';
import { DEPARTEMENTS } from '../../../federation/basis/departement/query';
import {
  DEPARTEMENTS as DEPARTEMENTS_TYPE,
  DEPARTEMENTSVariables,
} from '../../../federation/basis/departement/types/DEPARTEMENTS';
import { useQuery } from '@apollo/react-hooks';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import { getPharmacie } from '../../../services/LocalStorage';
import { DepartementSortFields, SortDirection } from '../../../types/federation-global-types';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: 500,
      '& > * + *': {
        marginTop: theme.spacing(3),
      },
    },
  }),
);

interface DepartementInputProps {
  label?: string;
  placeholder?: string;
  value?: any;
  defaultValue?: any;
  onChange: (value: any) => void;
}

const DepartementInput: FC<DepartementInputProps> = ({
  label = 'Départements',
  placeholder = 'Les départements aux quels est rattaché le contact',
  defaultValue,
  onChange,
  value,
}) => {
  const classes = useStyles();
  const pharmacie = getPharmacie();

  const departements = useQuery<DEPARTEMENTS_TYPE, DEPARTEMENTSVariables>(DEPARTEMENTS, {
    client: FEDERATION_CLIENT,
    variables: {
      paging: { offset: 0, limit: 1000 },
      sorting: [
        {
          field: DepartementSortFields.code,
          direction: SortDirection.ASC,
        },
      ],
    },
  });

  const currentDepartement =
    (departements.data?.departements.nodes || []).find(
      departement => departement.id === pharmacie.departement.id,
    ) || pharmacie.departement;

  const options = [
    currentDepartement,
    ...(departements.data?.departements.nodes || []).filter(
      departement => departement.id !== pharmacie.departement.id,
    ),
  ];

  /*const findValues = (ids: any[]): DepartementInfo[] => {
    if (!ids) {
      return [];
    }
    const optionValues = options.filter(opt => ids.includes(opt.id));
    return optionValues ? optionValues : [];
  };*/

  return (
    <div className={classes.root}>
      <Autocomplete
        loading={departements.loading}
        multiple
        options={options}
        getOptionLabel={(option: DepartementInfo) =>
          option?.code ? `${option.code} - ${option.nom}` : option?.nom
        }
        onChange={(event, newValue) => onChange(newValue)}
        value={value}
        renderInput={params => (
          <TextField {...params} variant="standard" label={label} placeholder={placeholder} />
        )}
      />
    </div>
  );
};

export default DepartementInput;
