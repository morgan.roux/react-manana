import { makeStyles, Theme, createStyles, darken, lighten } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    customSearchFilterRoot: {
      width: '100%',
      display: 'flex',
      alignItems: 'flex-start',
      justifyContent: 'flex-start',
      flexDirection: 'row',
      marginTop: 20,
    },
    inputsContainer: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      '@media (min-width: 1810px)': {
        display: 'grid',
        gridTemplateColumns: 'repeat(5, 1fr)',
        justifyContent: 'space-between',
      },
      '& > div': {
        maxWidth: 250,
        marginBottom: 30,
        marginRight: 20,
      },
      '& *': {
        fontWeight: 'normal',
        letterSpacing: 0,
      },
    },
    btnsContainer: {
      display: 'flex',
      flexDirection: 'column',
      '& > button': {
        minWidth: 200,
        height: 50,
      },
      '& > button:nth-child(1)': {
        marginBottom: 30,
      },
    },
  }),
);

export default useStyles;
