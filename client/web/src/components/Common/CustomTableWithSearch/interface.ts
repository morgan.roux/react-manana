import { Dispatch, SetStateAction } from 'react';
import { MarcheLaboratoireInput } from '../../../types/graphql-global-types';

export interface CustomTableWithSearchColumn {
  disablePadding: boolean;
  key: string;
  label: string;
  numeric: boolean;
  sortable?: boolean;
  renderer?: (row: any) => any;
}

export interface CustomTableWithSearchProps {
  searchType: string;
  columns: CustomTableWithSearchColumn[];
  showToolbar: boolean;
  isSelectable: boolean;
  selected?: any[];
  setSelected?: Dispatch<SetStateAction<any[]>>;
  defaultOrderByKey?: string;
  optionalMust?: any;
  idCommandeCanal?: string;
  withSelectionFilter?: boolean;
  selectionFilterValue?: string;
  withInputSearch?: boolean;
  withEmptyContent?: boolean;
  emptyTitle?: string;
  emptySubTitle?: string;
  emptyImgSrc?: string;
  dataFromProps?: any[];
  setDataFromProps?: Dispatch<SetStateAction<any[]>>;
  withCustomSearchFilter?: boolean;
  formattedLabo?: MarcheLaboratoireInput[];
  filterByFromPros?: any[];
  withFilterBtn?: boolean;
  fetchPolicy?:
    | 'cache-first'
    | 'network-only'
    | 'cache-only'
    | 'no-cache'
    | 'standby'
    | 'cache-and-network';
  onClickRow?: (row: any) => void;
  onClickBtnFilter?: () => void;
  addedGroup?: number;
}

export interface CustomTableWithSearchPaginationProps {
  total: number;
  page: number;
  rowsPerPage: number;
  take: number;
  skip: number;
  setPage: Dispatch<SetStateAction<number>>;
  setRowsPerPage: Dispatch<SetStateAction<number>>;
  setTake: Dispatch<SetStateAction<number>>;
  setSkip: Dispatch<SetStateAction<number>>;
}
