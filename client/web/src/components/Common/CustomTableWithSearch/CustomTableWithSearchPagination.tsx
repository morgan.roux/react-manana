import React, { MouseEvent, FC, ReactNode } from 'react';
import { IconButton, TablePagination, LabelDisplayedRowsArgs } from '@material-ui/core';
import { makeStyles, useTheme, Theme, createStyles } from '@material-ui/core/styles';
import { KeyboardArrowRight, KeyboardArrowLeft, LastPage, FirstPage } from '@material-ui/icons';
import { CustomTableWithSearchPaginationProps } from './interface';
import { ROWS_PER_PAGE_OPTIONS } from './constant';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexShrink: 0,
      // marginLeft: theme.spacing(2.5),
      '& button:nth-child(1)': {
        marginLeft: theme.spacing(2.5),
      },
      '& .MuiIconButton-root': {
        color: theme.palette.common.black,
      },
      '& .MuiIconButton-root.Mui-disabled': {
        color: 'rgba(0, 0, 0, 0.54)',
      },
    },
  }),
);

const CustomTableWithSearchPagination: FC<CustomTableWithSearchPaginationProps> = ({
  total,
  page,
  rowsPerPage,
  take,
  skip,
  setTake,
  setSkip,
  setPage,
  setRowsPerPage,
}) => {
  const classes = useStyles({});
  const theme = useTheme();

  const onChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newRowsPerPage = parseInt(event.target.value, 10);

    if (rowsPerPage !== newRowsPerPage) {
      setRowsPerPage(newRowsPerPage);
      setTake(newRowsPerPage);
      // setPage(0);
      if (skip === total - rowsPerPage) {
        setSkip(total - newRowsPerPage);
      }
    }
  };

  const handleClickFirstPage = (event: MouseEvent<HTMLButtonElement>) => {
    onChangePage(event, 0);
    if (skip !== 0) setSkip(0);
    if (take !== rowsPerPage) setTake(rowsPerPage);
  };

  const handleClickPrev = (event: MouseEvent<HTMLButtonElement>) => {
    onChangePage(event, page - 1);
    if (skip <= take) {
      const newSkip = 0;
      if (skip !== newSkip) setSkip(newSkip);
    } else {
      const newSkip = skip - rowsPerPage;
      if (skip !== newSkip) setSkip(newSkip);
    }
    if (take !== rowsPerPage) setTake(rowsPerPage);
  };

  const handleClickNext = (event: MouseEvent<HTMLButtonElement>) => {
    onChangePage(event, page + 1);
    const newSkip = skip + rowsPerPage;
    if (skip !== newSkip) setSkip(newSkip);
    if (take !== rowsPerPage) setTake(rowsPerPage);
  };

  const handleClickLastPage = (event: MouseEvent<HTMLButtonElement>) => {
    onChangePage(event, Math.max(0, Math.ceil(total / rowsPerPage) - 1));
    const newSkip = total - rowsPerPage;
    if (skip !== newSkip) setSkip(newSkip);
    if (take !== rowsPerPage) setTake(rowsPerPage);
  };

  const disabledPrev: boolean = skip === 0 || total === 0 || total <= take;
  const disabledNext: boolean = skip >= total - take || total === 0 || total <= take;

  const labelDisplayedRows = (paginationInfo: LabelDisplayedRowsArgs): ReactNode => {
    const { from, to, count } = paginationInfo;
    return `${from}-${to === -1 ? count : to} sur ${count}`;
  };

  const CustomTablePaginationActions = () => {
    return (
      <div className={classes.root}>
        <IconButton onClick={handleClickFirstPage} disabled={disabledPrev} aria-label="first page">
          {theme.direction === 'rtl' ? <LastPage /> : <FirstPage />}
        </IconButton>
        <IconButton onClick={handleClickPrev} disabled={disabledPrev} aria-label="previous page">
          {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
        </IconButton>
        <IconButton onClick={handleClickNext} disabled={disabledNext} aria-label="next page">
          {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
        </IconButton>
        <IconButton onClick={handleClickLastPage} disabled={disabledNext} aria-label="last page">
          {theme.direction === 'rtl' ? <FirstPage /> : <LastPage />}
        </IconButton>
      </div>
    );
  };

  return (
    <div className={classes.root}>
      <TablePagination
        component="div"
        rowsPerPageOptions={ROWS_PER_PAGE_OPTIONS}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={onChangePage}
        count={total}
        onChangeRowsPerPage={handleChangeRowsPerPage}
        SelectProps={{
          inputProps: { 'aria-label': 'lignes par page' },
          native: true,
        }}
        labelRowsPerPage="Nombre de lignes par page :"
        labelDisplayedRows={labelDisplayedRows}
        ActionsComponent={CustomTablePaginationActions}
      />
    </div>
  );
};

export default CustomTableWithSearchPagination;
