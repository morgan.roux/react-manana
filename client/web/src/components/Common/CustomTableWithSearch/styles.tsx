import { makeStyles, Theme, createStyles, darken, lighten } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    customTableroot: {
      width: '100%',
      '& *': {
        fontWeight: 'bold',
      },
      '& .MuiTableHead-root': {
        '& *': {
          color: theme.palette.common.white,
          fontSize: 12,
        },
        '& th': {
          background: lighten(theme.palette.primary.main, 0.1),
          minWidth: 130,
        },
      },
    },
    customTableContainer: {},
    toolbar: {
      display: 'flex',
      justifyContent: 'space-between',
      padding: 0,
    },
    toolbarInfo: {
      display: 'flex',
      '& .MuiTypography-root': {
        marginRight: 15,
      },
    },
    table: {
      minWidth: 750,
      '& svg': {
        width: 20,
        height: 20,
        zIndex: -1,
      },
    },
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
    activeRow: {
      backgroundColor: theme.palette.secondary.main,
      '& *': {
        color: `${theme.palette.common.white} !important`,
      },
      '&:hover': {
        backgroundColor: `${darken(theme.palette.secondary.main, 0.1)} !important`,
      },
    },
    cursorPointer: {
      cursor: 'pointer',
    },
    searchInputContainer: {
      maxWidth: 550,
      '& > div': {
        marginBottom: 0,
      },
    },
    searchInputContainerWithMargin: {
      marginBottom: 20,
    },
    emptyContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      height: 'calc(100vh - 186px)',
      '& img': {
        maxWidth: 400,
        maxHeight: 350,
      },
      '& > div > p:nth-last-child(1)': {
        fontWeight: 'normal',
      },
    },
    btn: {
      textTransform: 'none',
    },
    tableHeadCell: {
      padding: '6px 16px',
    },
  }),
);

export default useStyles;
