export const ROWS_PER_PAGE_OPTIONS: number[] = [5, 10, 25, 50, 100];

export const SELECTION_OPTIONS = [
  { id: 1, code: 'all', label: 'Tous' },
  { id: 2, code: 'selected', label: 'Sélectionné(s)' },
  { id: 3, code: 'notselected', label: 'Non sélectionné(s)' },
];
