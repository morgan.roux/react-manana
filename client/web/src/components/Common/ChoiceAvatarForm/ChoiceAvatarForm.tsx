import React, {
  useState,
  ChangeEvent,
  useEffect,
  Dispatch,
  SetStateAction,
  FC,
  Fragment,
} from 'react';
import useStyles from './styles';
import { CustomFormTextField } from '../CustomTextField';
import CustomSelect from '../CustomSelect';
import { LinearProgress, Box } from '@material-ui/core';
import { getGroupement } from '../../../services/LocalStorage';
import { Sexe } from '../../../types/graphql-global-types';
import { useLazyQuery } from '@apollo/react-hooks';
import {
  SEARCH_AVATARS,
  SEARCH_AVATARSVariables,
} from '../../../graphql/Avatar/types/SEARCH_AVATARS';
import { DO_SEARCH_AVATARS } from '../../../graphql/Avatar/query';

export interface ChoiceAvatarFormProps {
  selectedAvatar: any;
  setSelectedAvatar: Dispatch<SetStateAction<any>>;
  avatarList?: any[];
  setAvatarList?: Dispatch<SetStateAction<any[]>>;
  onClickAvatar: (avatar: any) => void;
}

const ChoiceAvatarForm: FC<ChoiceAvatarFormProps> = ({
  selectedAvatar,
  setSelectedAvatar,
  // avatarList,
  setAvatarList,
  onClickAvatar,
}) => {
  const classes = useStyles({});

  const groupement = getGroupement();
  const idGroupement = groupement && groupement.id;

  const [avatarFilters, setAvatarFilters] = useState({
    searchKeyWord: '',
    searchSexe: 'ALL',
  });
  const { searchKeyWord, searchSexe } = avatarFilters;

  const [take, setTake] = useState<number | null>(20);
  const defaultFilterBy = [{ term: { idGroupement } }];
  const [filterBy, setFilterBy] = useState<any[]>(defaultFilterBy);

  const [avatars, setAvatars] = useState<any[]>([]);

  const AVATAR_SEXE_LIST = [
    { id: 'ALL', value: 'Tous' },
    { id: Sexe.M, value: 'Homme' },
    { id: Sexe.F, value: 'Femme' },
  ];

  const handleClickAvatar = (avatar: any) => () => {
    // console.log('avatar :>> ', avatar);
    setSelectedAvatar(avatar);
    onClickAvatar(avatar);
  };

  const handleChangeFilter = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      setAvatarFilters(prevState => ({ ...prevState, [name]: value }));
      setTake(null);
      if (name === 'searchKeyWord') {
        const filter = value.length > 0 ? [{ wildcard: { description: `*${value}*` } }] : [];
        setFilterBy([...defaultFilterBy, ...filter]);
      }
      if (name === 'searchSexe') {
        if (value === 'M' || value === 'F') {
          const filter = [{ term: { codeSexe: value } }];
          setFilterBy([...defaultFilterBy, ...filter]);
        } else if (value === 'ALL') {
          const filter = [{ terms: { codeSexe: ['M', 'F'] } }];
          setFilterBy([...defaultFilterBy, ...filter]);
        }
      }
    }
  };

  /**
   * Get avatars list
   */
  const [
    searchAvatars,
    { data: avatarData, loading: avatarLoading, error: avatarError },
  ] = useLazyQuery<SEARCH_AVATARS, SEARCH_AVATARSVariables>(DO_SEARCH_AVATARS, {
    fetchPolicy: 'cache-and-network',
  });

  /**
   * Execute search avatars
   */
  useEffect(() => {
    searchAvatars({
      variables: {
        type: ['avatar'],
        skip: 0,
        take,
        query: '',
        sortBy: [{ description: { order: 'desc' } }],
        filterBy,
      },
    });
  }, []);

  useEffect(() => {
    searchAvatars({
      variables: {
        type: ['avatar'],
        skip: 0,
        take,
        query: '',
        sortBy: [{ description: { order: 'desc' } }],
        filterBy,
      },
    });
  }, [take, filterBy]);

  /**
   * Set avatarList
   */
  useEffect(() => {
    if (avatarData && avatarData.search && avatarData.search.data) {
      setAvatars(avatarData.search.data);
      if (setAvatarList) setAvatarList(avatarData.search.data);
    }
  }, [avatarData]);

  return (
    <Box className={classes.avatarListContainer}>
      <Box className={classes.avatarListFilterContainer}>
        <CustomFormTextField
          name="searchKeyWord"
          value={searchKeyWord}
          onChange={handleChangeFilter}
          placeholder="Mots clés"
        />
        <CustomSelect
          label="Sexe"
          list={AVATAR_SEXE_LIST}
          listId="id"
          index="value"
          name="searchSexe"
          value={searchSexe}
          onChange={handleChangeFilter}
          shrink={true}
        />
      </Box>
      <Box className={classes.avatarListImgContainer}>
        {avatarLoading && (
          <LinearProgress
            color="secondary"
            style={{ width: '100%', top: -15, position: 'absolute', height: 2 }}
          />
        )}
        {!avatarLoading && avatars.length > 0 && (
          <Box display="flex" alignItems="start" flexWrap="wrap">
            {avatars.map((item: any, index: number) => {
              console.log('item :>> ', item);
              return (
                <Box key={`avatar_list_${index}`} className={classes.avatarContainer}>
                  <img
                    src={(item.fichier && item.fichier.publicUrl) || ''}
                    onClick={handleClickAvatar(item)}
                    className={
                      selectedAvatar && selectedAvatar.id === item.id ? classes.selectedAvatar : ''
                    }
                  />
                  <span>{item.description}</span>
                </Box>
              );
            })}
          </Box>
        )}
        {!avatarLoading && !avatarError && avatars.length === 0 && (
          <span className={classes.textMsg}>Aucun avatar trouvé 😃</span>
        )}
        {avatarError && <span className={classes.textMsg}>Error 😩</span>}
      </Box>
    </Box>
  );
};

export default ChoiceAvatarForm;
