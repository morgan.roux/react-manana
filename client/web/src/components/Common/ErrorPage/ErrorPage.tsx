import React, { FC } from 'react';
import useStyles from './styles';
import { Box, Typography } from '@material-ui/core';
import notFoundImg from './../../../assets/img/404.png';

interface ErrorPageProps {
  message?: string;
  image?: string;
  title?: string;
}

const ErrorPage: FC<ErrorPageProps> = ({ message, image, title }) => {
  const classes = useStyles({});
  const errorMessage = message ? message : 'Contacter la hotline';
  const errorTitle = title ? title : 'Error 404 not found';
  const errorImage = image ? image : notFoundImg;
  return (
    <Box
      width="100%"
      height="100%"
      display="flex"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
    >
      <img src={errorImage} width="10%" height="10%" />
      <Typography>{errorTitle}</Typography>
      <Typography>{errorMessage}</Typography>
    </Box>
  );
};

export default ErrorPage;
