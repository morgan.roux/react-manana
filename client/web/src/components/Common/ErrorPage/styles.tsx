import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: '20px 0',
      border: '1px solid #000',
    },
    alignRight: {
      display: 'flex',
      justifyContent: 'flex-end',
    },
    content: {
      textAlign: 'center',
      margin: '20px 0',
    },
    foot: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    btnActions: {
      display: 'flex',
      alignItems: 'center',
      '& span': {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      },
      '& span:nth-child(2)': {
        marginLeft: '20px',
      },
    },
  }),
);
export default useStyles;
