import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
  root: {
    width: '100%'
  },
  field: {
    marginRight: 8
  },
  userInput: {
    width: '100%',
    marginTop: -8
  },
  userInputRoot: {
    margin: 0
  },

  userInputInput: {
    height: 40
  }
}));
