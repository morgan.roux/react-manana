import { AppBar, Typography } from '@material-ui/core';
import {
  AddCircleRounded,
  Assignment,
  Home,
  InfoRounded,
  WarningRounded,
} from '@material-ui/icons';
import React, { FC, ReactNode, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { CAHIER_LIAISON_URL, FICHE_INCIDENT_URL, TODO_URL } from '../../../Constant/url';
import AjoutTaches from '../../Main/Content/TodoNew/Modals/AjoutTache';
import { useStyles } from './styles';

interface MobileQuickAccessProps {}

interface MenuItem {
  icon: ReactNode;
  label: string;
  path?: string;
  modal?: ReactNode;
}

const MobileQuickAccess: FC<MobileQuickAccessProps & RouteComponentProps> = ({
  history: { push },
  location: { pathname },
}) => {
  const classes = useStyles({});
  const [openModal, setOpenModal] = useState<boolean>(false);

  const [activeIndex, setActiveIndex] = useState<number>(
    pathname === '/'
      ? 0
      : pathname.startsWith('/todo')
      ? 1
      : pathname.includes(CAHIER_LIAISON_URL)
      ? 3
      : pathname.includes(FICHE_INCIDENT_URL)
      ? 4
      : 2,
  );

  const menuItems: MenuItem[] = [
    { icon: <Home />, label: 'Accueil', path: '/' },
    { icon: <Assignment />, label: 'Ma To-Do', path: TODO_URL },
    {
      icon: <AddCircleRounded />,
      label: 'Tâche',
      modal: <AjoutTaches openModalTache={openModal} setOpenTache={setOpenModal} />,
    },
    { icon: <InfoRounded />, label: 'Liaison', path: CAHIER_LIAISON_URL },
    { icon: <WarningRounded />, label: 'Incident', path: FICHE_INCIDENT_URL },
  ];

  const handleMenuItemClick = (menuItem: MenuItem, index) => {
    setActiveIndex(index);
    if (menuItem.path) {
      push(menuItem.path);
    }
    if (menuItem.modal) {
      setOpenModal(true);
    }
  };

  return (
    <AppBar position="fixed" className={classes.root}>
      {menuItems.map((menuItem, index) => (
        <div
          key={`menuItem-${index}`}
          className={index === activeIndex ? classes.active : classes.menuItemRoot}
          onClick={() => handleMenuItemClick(menuItem, index)}
        >
          {menuItem.icon}
          <Typography>{menuItem.label}</Typography>
          {menuItem.modal}
        </div>
      ))}
    </AppBar>
  );
};

export default withRouter(MobileQuickAccess);
