import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    img: {
      marginBottom: '24px',
      // width: '30%',
      width: 329,
      height: 248,
      [theme.breakpoints.down('md')]: {
        width: '50%',
        height: 'auto',
      },
    },
    title: {
      fontSize: 22,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      color: '#4D4D4D',
      letterSpacing: 0.02,
      textAlign: 'center',
      marginBottom: 10,
    },
    subtitle: {
      width: 320,
      fontSize: 14,
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      color: '#4D4D4D',
      textAlign: 'center',
      letterSpacing: 0,
      [theme.breakpoints.down('md')]: {
        width: 'auto',
      },
    },
  }),
);

export default useStyles;
