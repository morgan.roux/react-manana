import { Avatar, Box, Checkbox } from '@material-ui/core';
import CircleChecked from '@material-ui/icons/CheckCircleOutline';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import React, { FC, useContext, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { ContentContext, ContentStateInterface } from '../../../../AppContext';
import { getUser } from '../../../../services/LocalStorage';
import { useStyles } from './styles';

interface ParticipantListToAssignProps {
  listResult?: any;
  match: {
    params: {
      filterId: string | undefined;
    };
  };
  idTask: any;
  handleAssignation: any;
  handleUnassignement: any;
  task: any;
  idAssignedUser: any;
}

const ParticipantListToAssign: FC<ParticipantListToAssignProps & RouteComponentProps> = ({
  listResult,
  match: {
    params: { filterId },
  },
  idTask,
  handleAssignation,
  handleUnassignement,
  task,
  idAssignedUser,
}) => {
  const classes = useStyles({});
  const user = getUser();

  const {
    content: { variables, operationName, refetch },
  } = useContext<ContentStateInterface>(ContentContext);
  const participants =
    (listResult &&
      listResult.data &&
      listResult.data.search &&
      listResult.data.search.data &&
      listResult.data.search.data[0] &&
      listResult.data.search.data[0].participants) ||
    [];

  const [checkedID, setCheckedID] = useState<string[]>(idAssignedUser);

  const handleChecked = (idUser, event) => {
    event.stopPropagation();
    setCheckedID(prev => {
      if (prev.includes(idUser)) {
        const next = prev.filter(i => i !== idUser);
        handleUnassignement(idUser);

        return next;
      } else {
        const next = [...prev, idUser];
        handleAssignation(idUser);
        return next;
      }
    });
  };

  const allParticipants = participants.filter(
    participant => participant && participant.id !== user.id,
  );

  return (
    <Box className={classes.rootListParticipant}>
      {allParticipants.map(participant => (
        <Box className={classes.contentRootListParticipant}>
          <Box display="flex" alignItems="center">
            <Box>
              <Avatar alt={participant.userName} className={classes.avatar} />
            </Box>
            <Box className={classes.description}>
              <Box className={classes.title}>{participant.userName}</Box>
              <Box className={classes.content}>{participant.role && participant.role.nom}</Box>
              <Box className={classes.content}>{participant.email}</Box>
            </Box>
          </Box>
          <Box marginLeft="auto">
            <Checkbox
              icon={<CircleUnchecked fontSize="large" />}
              checkedIcon={<CircleChecked fontSize="large" />}
              // tslint:disable-next-line: jsx-no-lambda
              onClick={event => handleChecked(participant.id, event)}
              checked={checkedID.includes(participant.id)}
            />
          </Box>
          <Box className={classes.icon}></Box>
        </Box>
      ))}
    </Box>
  );
};

export default withRouter(ParticipantListToAssign);
