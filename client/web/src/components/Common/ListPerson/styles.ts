import { createStyles, Theme } from '@material-ui/core/styles';
import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'block',
      height: 'calc(100vh - 320px)',
      overflowY: 'scroll',
      scrollbarWidth: 'none',
      '&::-webkit-scrollbar': {
        height: 0,
        width: 0,
      },
    },
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
    },
    description: {
      display: 'flex',
      flexDirection: 'column',
    },
    avatar: {
      width: theme.spacing(7),
      height: theme.spacing(7),
      marginRight: 8,
    },
    title: {
      fontWeight: 'bold',
      fontSize: '1.1rem',
      [theme.breakpoints.down('md')]: {
        fontSize: 14,
      },
    },
    content: {
      fontSize: '0.7rem',
    },
    icon: {
      marginTop: 15,
    },
    rowContent: {
      display: 'flex',
      alignItems: 'center',
      width: '100%',
      padding: '9px 0',
      justifyContent: 'space-between',
    },
    fab: {
      position: 'absolute',
      zIndex: 1000,
      bottom: 30,
      right: 30,
    },
  }),
);
