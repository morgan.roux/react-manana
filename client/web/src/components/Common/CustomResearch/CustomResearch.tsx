import React, { FC, Fragment, useState, useEffect } from 'react';
import { RouteComponentProps, withRouter, Redirect } from 'react-router-dom';
import { FormControl, OutlinedInput, InputAdornment, Fab, IconButton } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import useStyles from './styles';
import { handleSetValue } from '../../../utils/hookUtils';

interface CustomResearchProps {
  value?: String;
  placeholder?: string;
  handleGetData?: (txt: string) => void;
}

const CustomResearch: FC<CustomResearchProps & RouteComponentProps> = ({
  placeholder,
  handleGetData,
}) => {
  const classes = useStyles({});
  const [value, setValue] = useState<string>('');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
  };

  const handleKeyUp = event => {
    setValue(event.target.value);
    handleGetData && handleGetData(event.target.value);
  };

  return (
    <>
      <FormControl className={classes.formControlRoot} variant="outlined">
        <OutlinedInput
          placeholder={placeholder}
          endAdornment={
            <InputAdornment position="end">
              <SearchIcon />
            </InputAdornment>
          }
          onChange={handleChange}
          onKeyUp={e => handleKeyUp(e)}
          labelWidth={70}
        />
      </FormControl>
    </>
  );
};

export default withRouter(CustomResearch);
