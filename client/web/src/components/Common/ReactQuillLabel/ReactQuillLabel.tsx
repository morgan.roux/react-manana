import { Typography } from '@material-ui/core';
import React, { FC } from 'react';
import useStyles from './styles';

interface ReactQuillLabelProps {
  label: string;
  required?: boolean;
}

const ReactQuillLabel: FC<ReactQuillLabelProps> = ({ label, required }) => {
  const classes = useStyles();
  return (
    <Typography className={classes.root}>
      {label}
      {required && <span>*</span>}
    </Typography>
  );
};

export default ReactQuillLabel;
