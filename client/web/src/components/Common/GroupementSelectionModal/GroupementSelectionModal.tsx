import { useQuery } from '@apollo/react-hooks';
import { Groupement } from '@app/types';
import { Box, ListItemAvatar, ListItemText, MenuItem, MenuList } from '@material-ui/core';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { GET_GROUPEMENTS } from '../../../graphql/Groupement/query';
import { GROUPEMENTS } from '../../../graphql/Groupement/types/GROUPEMENTS';
import { CustomModal } from '../CustomModal';
import no_image from '../../../assets/img/pas_image.png';
import useStyles from './styles';
import Backdrop from '../Backdrop';
import { getGroupement } from '../../../services/LocalStorage';
interface GroupementSelectionModalProps {
  open: boolean;
  setOpen: (open: boolean) => void;
  onSelect: (groupement: Groupement) => void;
  saving?: boolean;
}

const GroupementSelectionModal: FC<GroupementSelectionModalProps & RouteComponentProps> = ({
  onSelect,
  open,
  setOpen,
  saving,
}) => {
  const classes = useStyles({});
  const groupement = getGroupement();
  const groupementsQuery = useQuery<GROUPEMENTS>(GET_GROUPEMENTS);
  const groupements = groupementsQuery.data?.groupements;

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title="Changement de groupement"
      withBtnsActions={false}
    >
      <Box maxHeight="200px">
        <Backdrop open={saving} value="Changement de groupement en cours ..." />
        <MenuList>
          {(groupements || [])
            .filter(el => el?.id !== groupement?.id)
            .map(groupement => (
              <MenuItem
                key={`groupement-${groupement?.id}`}
                onClick={() => onSelect(groupement as Groupement)}
              >
                <ListItemAvatar
                  children={
                    <img
                      width="64px"
                      src={groupement?.groupementLogo?.fichier?.publicUrl || no_image}
                    />
                  }
                />
                <ListItemText className={classes.groupement}>{groupement?.nom}</ListItemText>
              </MenuItem>
            ))}
        </MenuList>
      </Box>
    </CustomModal>
  );
};

export default withRouter(GroupementSelectionModal);
