import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    groupement: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 18,
      marginLeft: 16,
    },
  }),
);
export default useStyles;
