import React, { FC, useState, useEffect, useContext } from 'react';
import { useQuery, useApolloClient } from '@apollo/react-hooks';
import { SEARCH } from '../../../graphql/search/query';
import { SEARCH as SEARCH_Interface, SEARCHVariables } from '../../../graphql/search/types/SEARCH';
import ICurrentPharmacieInterface from '../../../Interface/CurrentPharmacieInterface';
import { GET_CURRENT_PHARMACIE } from '../../../graphql/Pharmacie/local';
import { MY_PHARMACIE_me_pharmacie } from '../../../graphql/Pharmacie/types/MY_PHARMACIE';
import gql from 'graphql-tag';
import { getUser } from '../../../services/LocalStorage';
import { ME_me } from '../../../graphql/Authentication/types/ME';
import { ApolloClient } from 'apollo-client';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import { DateFilterIterface } from '../Filter/FilterContent/PeriodeFilter/PeriodeFilter';
import { GET_FILTER_DATE } from '../../../graphql/Filter/local';
import moment from 'moment';
import { initializeStatusAndPeriodeFilters } from '../Filter/FilterContent/OtherContent/OtherContent';
import MainContainer from '../MainContainer';
import { LayoutType } from '../MainContainer/MainContainer';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { cpuUsage } from 'process';
import { ContentStateInterface, ContentContext } from '../../../AppContext';
export interface SortInterface {
  sort: {
    sortItem: {
      label: string;
      name: string;
      direction: string;
      active?: boolean;
      __typename: string;
    };
  };
}

export interface CodeCanalActiveInterface {
  codeCanalActive: string;
}

export interface FiltersInterface {
  filters: {
    idCheckeds: string[];
    idLaboratoires: number[];
    idFamilles: number[];
    idGammesCommercials: number[];
    idCommandeCanal: string | null;
    idCommandeCanals: string[];
    idCommandeTypes: number[];
    idActualiteOrigine: string[];
    idRemise: number[];
    idOcCommande: number[];
    idSeen: number[];
    idTodoType: number[];
    sortie: number[];
  };
}

export interface FrontFiltersInterface {
  frontFilters: {
    occommande: number[];
    seen: string;
    datefilter: { dateDebut: any; dateFin: any };
  };
}

export interface SearchInterface {
  searchText: {
    text: string;
  };
}

export const GET_LOCAL_SORT = gql`
  {
    sort @client {
      sortItem {
        label
        name
        direction
        active
      }
    }
  }
`;

export const GET_CODE_CANA_ACTIVE = gql`
  {
    codeCanalActive @client
  }
`;

export const GET_LOCAL_FILTERS = gql`
  {
    filters @client {
      idCheckeds
      idLaboratoires
      idFamilles
      idGammesCommercials
      idCommandeCanal
      idCommandeCanals
      idCommandeTypes
      idActualiteOrigine
      idRemise
      idOcCommande
      idSeen
      idTodoType
      sortie
    }
  }
`;

export const GET_LOCAL_FRONT_FILTERS = gql`
  {
    frontFilters @client {
      occommande
      seen
      datefilter {
        dateDebut
        dateFin
      }
    }
  }
`;

export const GET_LOCAL_SEARCH = gql`
  {
    searchText @client {
      text
    }
  }
`;

const takeAll = ['project'];

export const resetSearchFilters = (
  client: ApolloClient<object>,
  isAdmin?: boolean | null,
  dataType?: string,
) => {
  client.writeData({
    data: {
      filters: {
        idLaboratoires: [],
        idFamilles: [],
        idGammesCommercials: [],
        idCommandeTypes: [],
        idCommandeCanal: null,
        idCommandeCanals: [],
        idActualiteOrigine: [],
        idRemise: [],
        idOcCommande: [],
        idTodoType: [],
        idSeen: [],
        sortie: [],
        __typename: 'local',
      },
      searchText: {
        text: '',
        __typename: 'searchText',
      },
      sort: null,
      idCheckeds: null,
      myPharmacieFilter: false,
    },
  });

  client.writeData({
    data: {
      frontFilters: {
        occommande: [],
        seen: null,
        datefilter: null,
        __typename: 'localFront',
      },
    },
  });

  // Reset status filter and dateFilter
  /* if (isAdmin !== undefined) {
    initializeStatusAndPeriodeFilters(client, isAdmin, dataType);
  } */
};

interface SearchContainerProps {
  match: { params: { idOperation: string | undefined } };
}

const withSearch = (
  WrappedComponent: any,
  type: string,
  props?: any,
  optionalMust?: any,
  defaultSortName?: string,
  fieldsOptions?: { include?: string[]; exclude: string[] },
) => {
  const SearchContainer: FC<SearchContainerProps & RouteComponentProps> = ({ location, match }) => {
    const [query, setQuery] = useState<any>();
    const [take, setTake] = useState<number>(takeAll.includes(type) ? 10000 : 12);
    const [skip, setSkip] = useState<number>(0);
    const [isFetchingMore, setIsFetchingMore] = useState(false);
    const [idCanal, setIdCanal] = useState<string>();
    const isInGestionProduit = location.pathname.startsWith('/gestion-produits') ? true : false;
    const pathname = location.pathname;
    const customContentUrl = ['/pilotage'];
    const client = useApolloClient();
    // const groupement = getGroupement();
    const currentUser: ME_me = getUser();

    // get sort item from local state
    const sortItem = useQuery<SortInterface>(GET_LOCAL_SORT);

    const codeCanalActive = useQuery<CodeCanalActiveInterface>(GET_CODE_CANA_ACTIVE);

    // get filters id list from local state
    const filters = useQuery<FiltersInterface>(GET_LOCAL_FILTERS);

    // get front filters list from local state
    const frontFilters = useQuery<FrontFiltersInterface>(GET_LOCAL_FRONT_FILTERS);

    console.log('+++++ sortItem  : ', sortItem);

    // get search text from local state
    const searchText = useQuery<SearchInterface>(GET_LOCAL_SEARCH);

    // take currentPharmacie
    const [currentPharmacie, setCurrentPharmacie] = useState<MY_PHARMACIE_me_pharmacie | null>(
      null,
    );
    const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

    const idPharmacieUser =
      type === 'actualite' || type === 'operation'
        ? (currentUser &&
            currentUser.userPpersonnel &&
            currentUser.userPpersonnel.pharmacie &&
            currentUser.userPpersonnel.pharmacie.id) ||
          (currentUser &&
            currentUser.userTitulaire &&
            currentUser.userTitulaire.titulaire &&
            currentUser.userTitulaire.titulaire.pharmacieUser &&
            currentUser.userTitulaire.titulaire.pharmacieUser.id)
        : null;

    const skipSearchQuery: boolean =
      pathname === '/create/actualite' ||
      pathname.includes('/edit/actualite/') ||
      (type === 'produitcanal' && idCanal === '')
        ? true
        : false;

    console.log(
      'isInCustomContent : ',
      customContentUrl.find(url => pathname.startsWith(url)),
    );

    const listResult = useQuery<SEARCH_Interface, SEARCHVariables>(SEARCH, {
      variables: {
        type: [type],
        skip: skip ? skip : 0,
        take: take ? take : 12,
        query,
        idPharmacie:
          currentPharmacie && currentPharmacie.id ? currentPharmacie && currentPharmacie.id : '', // type === 'actualite' ? idPharmacie : currentPharmacie && currentPharmacie.id,
        idPharmacieUser,
        userId: currentUser && currentUser.id,
        ...(fieldsOptions || {}),
      },
      fetchPolicy: 'cache-and-network',
      skip:
        !(currentPharmacie && currentPharmacie.id) ||
        skipSearchQuery ||
        (type === 'produitcanal' && !query) ||
        customContentUrl.find(url => pathname.startsWith(url))
          ? true
          : false,
    });

    const {
      content: { page, rowsPerPage, searchPlaceholder },
      setContent,
    } = useContext<ContentStateInterface>(ContentContext);

    useEffect(() => {
      if (listResult && listResult.variables) {
        setContent({
          page,
          type,
          searchPlaceholder,
          rowsPerPage,
          variables: listResult.variables,
          operationName: SEARCH,
        });
      }
    }, [listResult]);

    console.log('idCanal : ', idCanal);

    const getDateFilter = useQuery<{ dateFilter: DateFilterIterface }>(GET_FILTER_DATE);
    const dateFilter = getDateFilter.data && getDateFilter.data.dateFilter;

    const canals = useQuery<SEARCH_Interface, SEARCHVariables>(SEARCH, {
      variables: {
        type: ['commandecanal'],
      },
      fetchPolicy: 'cache-and-network',
    });

    useEffect(() => {
      console.log('codeCanalActive >==========================', codeCanalActive);
      const codeActive =
        codeCanalActive && codeCanalActive.data && codeCanalActive.data.codeCanalActive
          ? codeCanalActive.data.codeCanalActive
          : 'PFL';
      if (
        canals &&
        canals.data &&
        canals.data.search &&
        canals.data.search.data &&
        canals.data.search.data.length
      ) {
        const currentCanals: any[] = canals.data.search.data.filter(
          (item: any) => item.code === codeActive,
        );
        console.log('canals ==========================>', canals.data.search.data);
        console.log('currentCanals ==========================>', currentCanals);
        if (currentCanals && currentCanals.length) setIdCanal(currentCanals[0].id);
      }
    }, [canals, codeCanalActive]);

    console.log('filters : ', filters);

    // update the search query filter parameters
    const updateQueryFilter = (
      must: any,
      order: string,
      sortName: string,
      text: string,
      should?: any,
    ) => {
      const produitTermActive = isInGestionProduit
        ? [
            {
              term: {
                isRemoved: false,
              },
            },
            {
              term: {
                'commandeCanal.id': idCanal,
              },
            },
          ]
        : [
            {
              term: {
                isActive: true,
              },
            },
            {
              term: {
                isRemoved: false,
              },
            },
            {
              term: {
                'commandeCanal.id': idCanal,
              },
            },
          ];
      // const queryFilter = {};
      // add search text to must if exist
      if (text && text.length > 0) {
        const textSearch = text.replace(/\s+/g, '\\ ').replace('/', '\\/');
        must.push({
          query_string: {
            query:
              textSearch.startsWith('*') || textSearch.endsWith('*')
                ? textSearch
                : `*${textSearch}*`,
            analyze_wildcard: true,
          },
        });
      }

      if (optionalMust) {
        if (optionalMust && optionalMust.must) {
          must = must.concat(optionalMust.must);
        }
      }

      if (type === 'produitcanal') {
        must = must.concat(...produitTermActive);
      }

      //Add date filter
      if (
        dateFilter &&
        dateFilter.startDate &&
        dateFilter.endDate &&
        (type === 'actualite' || type === 'operation')
      ) {
        const startDate = moment(dateFilter.startDate).format('YYYY-MM-DD');
        const endDate = moment(dateFilter.endDate).format('YYYY-MM-DD');
        // const newMust = [
        //   {
        //     bool: {
        //       should: [
        //         { range: { dateDebut: { gte: startDate } } },
        //         { range: { dateFin: { lte: endDate } } },
        //       ],
        //     },
        //   },
        // ];
        const newMust = [
          { range: { dateDebut: { lte: startDate } } },
          { range: { dateFin: { gte: endDate } } },
        ];
        must = must.concat(newMust);
      }

      let sortQuery = sortName ? [{ [sortName]: { order } }] : [];
      if (type === 'actualite' || type === 'operation') {
        sortQuery = [{ niveauPriorite: { order: 'desc' } }, ...sortQuery];
      }
      if (must.length > 0 && sortQuery.length > 0) {
        const queryFilter = {
          query: {
            bool: {
              must: [...must],
              should: (optionalMust && optionalMust.should) || [],
              minimum_should_match: optionalMust && optionalMust.minimum_should_match,
            },
          },
          sort: sortQuery,
        };
        if (idCanal) setQuery(queryFilter);
      } else if (sortQuery.length > 0) {
        const queryFilter =
          type === 'produitcanal'
            ? {
                query: {
                  bool: {
                    must: [...produitTermActive],
                  },
                },
                sort: sortQuery,
              }
            : {
                sort: sortQuery,
              };
        if (idCanal) setQuery(queryFilter);
      }
    };

    const handleScroll = (
      fetchMore: typeof listResult.fetchMore,
      data: typeof listResult.data,
      param: any,
    ) => {
      const gridScrollTop = (document.getElementById(param.id) as HTMLElement).scrollTop;
      const gridHeight = (document.getElementById(param.id) as HTMLElement).offsetHeight;
      const scrollHeight = (document.getElementById(param.id) as HTMLElement).scrollHeight;
      if (scrollHeight - (gridScrollTop + gridHeight) < 100 && !isFetchingMore) {
        setIsFetchingMore(true);
        const skipFetchMore =
          (data && data.search && data.search.data && data.search.data.length) || 0;
        if (
          skipFetchMore &&
          data &&
          data.search &&
          data.search.total &&
          data.search.data &&
          data.search.total > skipFetchMore
        ) {
          fetchMore({
            query: SEARCH,
            updateQuery: (prev: any, { fetchMoreResult }) => {
              if (
                prev &&
                prev.search &&
                prev.search.data &&
                fetchMoreResult &&
                fetchMoreResult.search &&
                fetchMoreResult.search.data
              ) {
                const { data: currentData } = prev.search;
                return {
                  ...prev,
                  search: {
                    ...prev.search,
                    data: [...currentData, ...fetchMoreResult.search.data],
                    total: fetchMoreResult.search.total,
                  },
                };
              }
              return prev;
            },
            variables: {
              type,
              skip: skipFetchMore,
              query,
              take,
              idPharmacie: currentPharmacie ? currentPharmacie.id : '',
              idPharmacieUser,
              userId: currentUser && currentUser.id,
            },
          }).finally(() => setIsFetchingMore(false));
        }
      } else if (scrollHeight - (gridScrollTop + gridHeight) > 100) {
        setIsFetchingMore(false);
      }
    };

    useEffect(() => {
      // set order from sortItem
      const order =
        sortItem && sortItem.data && sortItem.data.sort && sortItem.data.sort.sortItem
          ? sortItem.data.sort.sortItem.direction
          : 'asc';

      // set sortName from sortItem
      const sortName =
        defaultSortName ||
        (sortItem &&
          sortItem.data &&
          sortItem.data.sort &&
          sortItem.data.sort.sortItem &&
          sortItem.data.sort.sortItem.name) ||
        '';

      const text =
        searchText && searchText.data && searchText.data.searchText
          ? searchText.data.searchText.text
          : '';

      if (filters && filters.data && filters.data.filters) {
        const must: any = [];
        const should: any = [];

        if (type === 'produitcanal') {
          if (match && match.params && match.params.idOperation) {
            must.push({
              terms: {
                'operations.id': [match.params.idOperation],
              },
            });
          }
          if (filters.data.filters.idFamilles.length > 0) {
            const familles = filters.data.filters.idFamilles.map(codeFamille => ({
              prefix: {
                'famille.codeFamille': codeFamille,
              },
            }));

            must.push({ bool: { should: familles } });
          }
          if (filters.data.filters.idLaboratoires.length > 0) {
            must.push({
              terms: {
                'laboratoire.id': filters.data.filters.idLaboratoires,
              },
            });
          }
          if (filters.data.filters.idGammesCommercials.length > 0) {
            must.push({
              terms: {
                'sousGammeCommercial.gammeCommercial.idGamme':
                  filters.data.filters.idGammesCommercials,
              },
            });
          }
          if (
            filters.data.filters.idCommandeCanal &&
            filters.data.filters.idCommandeCanal.length > 0
          ) {
            must.push({
              terms: {
                'commandeCanal.id': filters.data.filters.idCommandeCanal,
              },
            });
          }
          if (filters.data.filters.idRemise) {
            if (filters.data.filters.idRemise.includes(0)) {
              must.push({
                terms: {
                  'pharmacieRemisePaliers.id': [currentPharmacie && currentPharmacie.id],
                },
              });
            }
            if (filters.data.filters.idRemise.includes(1)) {
              must.push({
                terms: {
                  'pharmacieRemisePanachees.id': [currentPharmacie && currentPharmacie.id],
                },
              });
            }
          }
        }

        if (type === 'operation') {
          if (
            filters.data.filters.idCommandeCanal &&
            filters.data.filters.idCommandeCanal.length > 0
          ) {
            must.push({
              term: {
                'commandeCanal.code': filters.data.filters.idCommandeCanal,
              },
            });
          }
          if (filters.data.filters.idCommandeTypes.length > 0) {
            must.push({
              terms: {
                'commandeType.id': filters.data.filters.idCommandeTypes,
              },
            });
          }
          if (filters.data.filters.idOcCommande) {
            if (filters.data.filters.idOcCommande.includes(0)) {
              must.push({
                term: {
                  commandePassee: true,
                },
              });
            }
            if (filters.data.filters.idOcCommande.includes(1)) {
              must.push({
                term: {
                  commandePassee: false,
                },
              });
            }
          }
        }

        if (type === 'actualite') {
          if (filters.data.filters.idActualiteOrigine.length > 0) {
            must.push({
              terms: {
                'origine.id': filters.data.filters.idActualiteOrigine,
              },
            });
          }
        }

        if (type === 'laboratoire') {
          if (filters.data.filters.sortie && filters.data.filters.sortie.length > 0) {
            must.push({
              terms: {
                sortie: filters.data.filters.sortie,
              },
            });
          }
        }

        if (type === 'project') {
          const typeProjectTerms: any[] = [];
          if (filters.data.filters.idTodoType && filters.data.filters.idTodoType.length > 0) {
            if (filters.data.filters.idTodoType.includes(0)) {
              typeProjectTerms.push('Personnel');
            }
            if (filters.data.filters.idTodoType.includes(1)) {
              typeProjectTerms.push('Groupement');
            }
            if (filters.data.filters.idTodoType.includes(2)) {
              typeProjectTerms.push('Pharmacie');
            }
          }

          if (typeProjectTerms.length > 0) {
            must.push({ terms: { typeProject: typeProjectTerms } });
          }
        }

        updateQueryFilter(must, order, sortName, text, should);
        // getResult();
      } else {
        updateQueryFilter([], order, sortName, text);
        // getResult();
      }
    }, [filters, sortItem, searchText, dateFilter, idCanal]);

    useEffect(() => {
      if (myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie) {
        setCurrentPharmacie(myPharmacie.data.pharmacie);
      }
    }, [myPharmacie]);

    const handleUpdateTake = (take: number) => {
      setTake(take);
      // getResult();
    };

    const handleUpdateSkip = (skip: number) => {
      setSkip(skip);
      // getResult();
    };

    const resetFilters = () => {
      client.writeData({
        data: {
          filters: {
            idLaboratoires: [],
            idFamilles: [],
            idGammesCommercials: [],
            idCommandeTypes: [],
            idCommandeCanal: [],
            idActualiteOrigine: [],
            idRemise: [],
            idTodoType: [],
            sortie: [],
            __typename: 'local',
          },
          searchText: {
            text: '',
            __typename: 'searchText',
          },
        },
      });
    };

    const formatNewList = (newList: any[]) => {
      return { data: { search: { data: newList } } };
    };
    const checkFrontFilters = () => {
      const filters = frontFilters && frontFilters.data && frontFilters.data.frontFilters;

      let newList =
        (listResult && listResult.data && listResult.data.search && listResult.data.search.data) ||
        [];
      if (filters) {
        // filters for operation commercial commandePassee (boolean)
        if (filters.occommande && filters.occommande.length > 0) {
          if (filters.occommande.length < 2) {
            filters.occommande.map(
              filterItem =>
                (newList = newList.filter(
                  (listItem: any) => listItem.commandePassee === filterItem,
                )),
            );
            return formatNewList(newList);
          } else {
            return listResult;
          }
        }
        if (filters.seen) {
          if (filters.seen === 'all') {
            return listResult;
          } else if (filters.seen === 'read') {
            newList = newList.filter((listItem: any) => listItem.seen === true);
          } else {
            newList = newList.filter((listItem: any) => listItem.seen === false);
          }

          return formatNewList(newList);
        }
        // if (filters.datefilter) {
        //   if (filters.datefilter.dateDebut) {
        //     newList = newList.filter(
        //       (listItem: any) =>
        //         listItem &&
        //         listItem.dateDebut &&
        //         moment(listItem.dateDebut).format() >=
        //           moment(filters.datefilter.dateDebut).format(),
        //     );
        //   }
        //   if (filters.datefilter.dateFin) {
        //     newList = newList.filter(
        //       (listItem: any) =>
        //         listItem &&
        //         listItem.dateFin &&
        //         moment(listItem.dateFin).format() <= moment(filters.datefilter.dateFin).format(),
        //     );
        //   }
        //   return formatNewList(newList);
        // }
      }
      return listResult;
    };

    // if (listResult && listResult.error) {
    //   console.log('error search : ', listResult.error.message);
    //   displaySnackBar(client, {
    //     isOpen: true,
    //     type: 'ERROR',
    //     message:
    //       "Une erreur est survenue, merci de recharger la page puis de contacter l'equipe technique si cela ne fonctionne pas mieux",
    //   });
    // }

    if (props && props.layout && props.layout === LayoutType.WithFilter) {
      return (
        <MainContainer
          layout={props.layout}
          mainItem={
            <WrappedComponent
              listResult={checkFrontFilters()}
              updateTake={handleUpdateTake}
              take={take}
              updateQueryFilter={updateQueryFilter}
              updateSkip={handleUpdateSkip}
              handleScroll={handleScroll.bind(null, listResult.fetchMore, listResult.data)}
              props={props}
              resetFilters={resetFilters}
            />
          }
        />
      );
    } else if (props && props.layout && props.layout === LayoutType.TwoColumns) {
      return (
        <MainContainer
          layout={props.layout}
          mainItem={
            <WrappedComponent
              listResult={checkFrontFilters()}
              updateTake={handleUpdateTake}
              take={take}
              updateQueryFilter={updateQueryFilter}
              updateSkip={handleUpdateSkip}
              handleScroll={handleScroll.bind(null, listResult.fetchMore, listResult.data)}
              props={props}
              resetFilters={resetFilters}
            />
          }
        />
      );
    } else {
      return (
        <WrappedComponent
          listResult={checkFrontFilters()}
          updateTake={handleUpdateTake}
          take={take}
          updateQueryFilter={updateQueryFilter}
          updateSkip={handleUpdateSkip}
          handleScroll={handleScroll.bind(null, listResult.fetchMore, listResult.data)}
          props={props}
          resetFilters={resetFilters}
        />
      );
    }
  };
  return withRouter(SearchContainer);
};
export default withSearch;
