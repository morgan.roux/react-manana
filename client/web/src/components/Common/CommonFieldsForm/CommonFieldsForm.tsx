import { Box, FormControl } from '@material-ui/core';
import React, { CSSProperties, FC, useEffect, useState } from 'react';
import { isMobile } from '../../../utils/Helpers';
import { CustomCheckbox } from '../CustomCheckbox';
import { UsersModalProps } from '../CustomSelectUser';
import ImportanceInput from '../ImportanceInput';
import ProjetInput, { findProjetById } from '../ProjectInput';
import UrgenceInput from '../UrgenceInput';
import UserInput from '../UserInput';
import { useStyles } from './styles';

interface CommonFieldsFormProps {
  style?: CSSProperties;
  urgenceProps?: {
    useCode?: boolean;
    noMinHeight?: boolean;
  };
  usersModalProps?: Partial<UsersModalProps>;
  labelImportance?: string;
  urgence?: any; // urgence or urgenceId
  importance?: any; // importance  or importanceId
  projet?: any; // projet or projetId
  isPrivate?: boolean;
  selectedUsers: any;

  selectUsersFieldLabel?: string;

  onlyMatriceFonctions?: boolean;
  disabled?: boolean;
  onChangeUrgence?: (urgence: any) => void;
  onChangeImportance?: (importance: any) => void;
  onChangeProjet?: (projet: any) => void;
  onChangeUsersSelection?: (users: any) => void;
  onChangePrivate?: (isPrivate: boolean) => void;
  radioImportance?: boolean;
  hideUserInput?: boolean;
  noMarginTop?: boolean;
}

const CommonFieldsForm: FC<CommonFieldsFormProps> = ({
  selectUsersFieldLabel,
  style,
  disabled,
  urgence,
  importance,
  projet,
  isPrivate,
  selectedUsers,
  urgenceProps = {},
  usersModalProps = {},
  onlyMatriceFonctions = true,
  onChangeUsersSelection,
  onChangeProjet,
  onChangeImportance,
  onChangeUrgence,
  onChangePrivate,
  radioImportance,
  hideUserInput,
  labelImportance,
  noMarginTop,
}) => {
  const idProjet = typeof projet === 'string' ? projet : projet?.id;

  const classes = useStyles();
  const [openSelectUserModal, setOpenSelectUserModal] = useState<boolean>(false);
  const [projets, setProjets] = useState<any>([]);
  const [idProjetParticipants, setIdProjetParticipants] = useState<string[] | undefined>();

  const activeProjet = findProjetById(projets, idProjet);
  const isPersonnalProject = activeProjet?.typeProject === 'PERSONNEL' ? true : false;

  const handleChangeUsersSelection = (users: any) => {
    if (onChangeUsersSelection) {
      onChangeUsersSelection(users);
    }
  };

  const handleIsPrivateChange = (e: any) => {
    e.stopPropagation();
    const { checked } = e.target;
    onChangePrivate && onChangePrivate(checked);
  };

  useEffect(() => {
    if (idProjet && activeProjet) {
      const idUserParticipants = activeProjet.participants?.map(({ id }) => id);
      if (idUserParticipants) {
        const newSelectedUsers = (selectedUsers || []).filter(({ id }) =>
          idUserParticipants.includes(id),
        );
        console.log('++++++++++++++++++++++++++++++++++++++ : ', selectedUsers, idUserParticipants);
        //onChangeUsersSelection && onChangeUsersSelection(newSelectedUsers);
        setIdProjetParticipants(idUserParticipants);
      }
    } else {
      setIdProjetParticipants(undefined);
    }
  }, [idProjet, projets]);

  return (
    <Box className={classes.tableContainer} style={style}>
      <Box style={{ margin: noMarginTop ? 0 : '15px 0 15px 0' }} className={classes.section}>
        {!isMobile() && onChangeProjet && (
          <ProjetInput
            disabled={disabled}
            onlyMatriceFonctions={onlyMatriceFonctions}
            value={idProjet}
            onChange={onChangeProjet}
            onLoaded={setProjets}
          />
        )}
      </Box>
      {!isPersonnalProject && !hideUserInput && (
        <Box className={classes.section}>
          <UserInput
            disabled={disabled}
            {...usersModalProps}
            label={selectUsersFieldLabel}
            idParticipants={idProjetParticipants}
            selected={selectedUsers}
            setSelected={handleChangeUsersSelection}
            openModal={openSelectUserModal}
            setOpenModal={setOpenSelectUserModal}
          />
        </Box>
      )}
      {onChangePrivate && (
        <FormControl style={{ marginBottom: '16px' }}>
          <CustomCheckbox
            value={isPrivate}
            checked={isPrivate}
            onClick={(e: any) => e.stopPropagation()}
            onChange={handleIsPrivateChange}
            label="Privée"
          />
        </FormControl>
      )}
      {(onChangeUrgence || onChangeImportance) && (
        <Box className={classes.section}>
          <Box className={classes.row}>
            {onChangeUrgence && (
              <UrgenceInput
                disabled={disabled}
                noMinHeight={false}
                {...urgenceProps}
                value={
                  typeof urgence === 'string'
                    ? urgence
                    : urgenceProps.useCode
                    ? urgence?.code
                    : urgence?.id
                }
                onChange={onChangeUrgence}
              />
            )}
            {onChangeImportance && (
              <ImportanceInput
                disabled={disabled}
                onChange={onChangeImportance}
                name="idImportance"
                value={typeof importance === 'string' ? importance : importance?.id}
                radioImportance={radioImportance}
                label={labelImportance}
              />
            )}
          </Box>
        </Box>
      )}
    </Box>
  );
};

export default CommonFieldsForm;
