import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    tableRoot: {
      height: 'auto',
      maxHeight: 'calc(100vh - 300px)',
      overflowY: 'auto',
      minWidth: '65%',
    },
    rootSelectionContainer: {
      '& .MuiCheckbox-root': {
        paddingTop: '0px !important',
        paddingBottom: '0px !important',
        color: theme.palette.primary.main,
      },
      '& .MuiCheckbox-colorSecondary.Mui-checked': {
        color: theme.palette.primary.main,
      },
    },
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
    root: {
      width: '100%',
      '& a': {
        cursor: 'pointer',
      },
    },
    searchBar: {
      top: 0,
      width: '100%',
      opacity: 1,
      zIndex: 10,
      position: 'sticky',
      display: 'flex',
      height: 70,
      alignItems: 'center',
      justifyContent: 'space-between',
      padding: '0px 24px 0 24px',
      background: theme.palette.primary.main,
      '@media (max-width: 692px)': {
        flexWrap: 'wrap',
        justifyContent: 'center',
      },
    },
    search: {
      display: 'flex',
      alignItems: 'center',
      '@media (max-width: 580px)': {
        marginBottom: 15,
      },
    },
    searchGlobal: {
      display: 'flex',
      justifyContent: 'center',
      '@media (max-width: 580px)': {
        marginBottom: 15,
      },
    },
    filterButton: {
      marginLeft: theme.spacing(3),
      color: theme.palette.common.white,
    },

    filterButtonLabel: {
      fontFamily: 'Roboto',
      fontSize: 14,
      color: theme.palette.common.white,
    },

    alignCenter: {
      textAlign: 'center',
    },
    formControl: {
      width: 240,
      marginRight: theme.spacing(6),
      marginBottom: theme.spacing(3),
    },
    cssOutlinedInput: {
      '&$cssFocused $notchedOutline': {
        borderColor: `${theme.palette.primary.main} !important`,
      },
    },
    notchedOutline: {
      borderWidth: '1px',
      // borderColor: `${theme.palette.primary.dark} !important`,
    },
    searchButton: {
      width: 240,
      height: 51,
      marginRight: theme.spacing(6),
    },
    resetButton: {
      width: 240,
      height: 51,
    },
    globalSearch: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
    },
  }),
);

export default useStyles;
