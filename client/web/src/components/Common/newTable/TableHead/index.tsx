import React, { FC, useState, ChangeEvent } from 'react';
import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import TableHead from '@material-ui/core/TableHead';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Checkbox from '@material-ui/core/Checkbox';
import { TableHeadProps } from '../interfaces';
import { useApolloClient } from '@apollo/react-hooks';
import { Theme, lighten } from '@material-ui/core/styles';
import { differenceBy, intersectionBy, uniqBy } from 'lodash';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
  root: {
    background: lighten(theme.palette.primary.main, 0.1),
    '& .MuiTableSortLabel-root.MuiTableSortLabel-active, & .MuiTableSortLabel-icon': {
      color: `${theme.palette.common.white} !important`,
    },
    '& .MuiTableCell-root': {
      padding: '0px 16px',
    },
    '& .MuiTableCell-head': {
      lineHeight: '1rem',
      height: 40,
    },
  },
  loader: {
    color: theme.palette.common.white,
    marginLeft: 12,
  },
  label: {
    color: theme.palette.common.white,
    fontSize: 12,
    '&:hover': {
      color: theme.palette.common.white,
      opacity: '0.6',
    },
  },
});

const EnhancedTableHead: FC<TableHeadProps & WithStyles> = ({
  classes,
  columns,
  isSelectable,
  checkedItems,
  checkedItemsQuery,
  listResult,
  selected,
  setSelected,
  allTotal,
}) => {
  const client = useApolloClient();
  const [order, setOrder] = useState<'asc' | 'desc' | undefined>('asc');
  const [columnSorted, setColumnSorted] = useState('');

  const handleNewSorting = (newColumn: any) => {
    setColumnSorted(newColumn);
    setOrder(order === 'desc' ? 'asc' : 'desc');
    client.writeData({
      data: {
        sort: {
          sortItem: {
            label: newColumn,
            name: newColumn,
            direction: order === 'desc' ? 'asc' : 'desc',
            active: true,
            __typename: 'sortItem',
          },
          __typename: 'sort',
        },
      },
    });
  };

  const data =
    (listResult && listResult.data && listResult.data.search && listResult.data.search.data) || [];

  const total = data.length;

  const intersection = intersectionBy(checkedItems, data, 'id');

  const numSelected = checkedItems
    ? checkedItems.length > allTotal
      ? allTotal
      : checkedItems.length
    : 0;
  const isIndeterminate: boolean = numSelected > 0 && numSelected < allTotal;

  const checkedAll = (): boolean => {
    return intersection && intersection.length && total === intersection.length ? true : false;
  };

  const handleChange = () => {
    if (setSelected && selected && !checkedAll()) {
      const checkeds = uniqBy([...checkedItems, ...data], 'id');
      setSelected(checkeds);
      return;
    } else if (setSelected && selected && checkedAll()) {
      const dif = differenceBy(checkedItems, data, 'id');
      setSelected(dif);
      return;
    }

    if (!checkedAll() && checkedItemsQuery && data) {
      const checkeds = uniqBy([...checkedItems, ...data], 'id');
      client.writeData({
        data: {
          [checkedItemsQuery.name]: checkeds.map(
            item => item && { id: item.id, __typename: item.__typename },
          ),
        },
      });
    } else if (checkedAll() && checkedItemsQuery) {
      const dif = differenceBy(checkedItems, data, 'id');
      client.writeData({
        data: {
          [checkedItemsQuery.name]: dif.map(
            (item: any) => item && item.id && { id: item.id, __typename: item.__typename },
          ),
        },
      });
    }
  };

  return (
    <TableHead className={classes.root}>
      <TableRow>
        {isSelectable ? (
          <TableCell style={{ padding: '0px 4px' }}>
            <Checkbox
              indeterminate={isIndeterminate}
              checked={checkedAll()}
              onChange={handleChange}
              inputProps={{ 'aria-label': 'Select all desserts' }}
              style={{ color: '#ffffff' }}
            />
          </TableCell>
        ) : null}

        {columns
          ? (columns || []).map((column, index) => (
              <TableCell
                key={`column-${index}`}
                align={column.centered ? 'center' : 'left'}
                sortDirection={columnSorted === column.name ? order : false}
              >
                <TableSortLabel
                  active={
                    columnSorted === column.name ? (column.name === '' ? false : true) : false
                  }
                  hideSortIcon={column.name === '' ? true : false}
                  direction={columnSorted && columnSorted === column.name ? order : undefined}
                  onClick={() => (column.name ? handleNewSorting(column.name) : null)}
                  className={classes.label}
                >
                  {column.label}
                </TableSortLabel>
              </TableCell>
            ))
          : null}
      </TableRow>
    </TableHead>
  );
};

export default withStyles(styles)(EnhancedTableHead);
