import React, { FC } from 'react';
import { Box } from '@material-ui/core';
import { RouteComponentProps, withRouter, Redirect } from 'react-router-dom';
import withSearch from '../../Common/withSearch';
import CreateOC from '../../Main/Content/OperationCommerciale/CreateOC';
import CreateActualite from '../../Main/Content/Actualite/Create';

const CreateComponent: FC<RouteComponentProps> = ({ match: { params } }) => {
  const { type } = params as any;

  switch (type) {
    case 'operations-commerciales':
      return <CreateOC title={"Création d'une opération commerciale"} />;
    case 'actualite':
      return <Box>{withSearch(CreateActualite, 'actualite')}</Box>;

    default:
      return <Redirect to="/" />;
  }
};

export default withRouter(CreateComponent);
