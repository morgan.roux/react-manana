import React, { FC } from 'react';
import useStyles from './styles';
import {
  Box,
  Typography,
  Dialog,
  DialogContent,
  DialogActions,
  Tooltip,
  Fade,
  IconButton,
  AppBar,
  Toolbar,
} from '@material-ui/core';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import CloseIcon from '@material-ui/icons/Close';
import logo from '../../../assets/img/logo-gcr_pharma.svg';
import { useQuery } from '@apollo/react-hooks';
import { GROUPEMENT, GROUPEMENTVariables } from '../../../graphql/Groupement/types/GROUPEMENT';
import { GET_GROUPEMENT } from '../../../graphql/Groupement/query';
import { getGroupement, getUser } from '../../../services/LocalStorage';
import { AWS_HOST } from '../../../utils/s3urls';
import classnames from 'classnames';
import IconPeople from '@material-ui/icons/People';
import { avatarStyle } from '../../../commonStyles/avatar';

interface FullScreenModalProps {
  open: boolean;
  setOpen: (value: boolean) => void;
  fullWidth?: boolean;
  content: any;
}

const FullScreenModal: FC<FullScreenModalProps & RouteComponentProps> = ({
  open,
  setOpen,
  fullWidth,
  location,
  content,
}) => {
  const classes = useStyles({});
  const pathname = location.pathname;
  const dashboardRoutePrefix = '/db/';
  const panierRoutePrefix = '/panier';
  const autrepanierRoutePrefix = '/autrepanier';
  const produitDetailsRoutePrefix = '/catalogue-produits/card/';

  const onDashboard = pathname.startsWith(dashboardRoutePrefix);
  const onPanier = pathname.startsWith(panierRoutePrefix);
  const onAutrePanier = pathname.startsWith(autrepanierRoutePrefix);
  const onProduitDetails = pathname.startsWith(produitDetailsRoutePrefix);

  const groupement = getGroupement();
  const user = getUser();
  const groupementResult = useQuery<GROUPEMENT, GROUPEMENTVariables>(GET_GROUPEMENT, {
    variables: {
      id: (groupement && groupement.id) || '',
    },
  });
  const avatarStyles = avatarStyle();
  return (
    <Dialog fullScreen={true} open={open} className={classes.root}>
      <AppBar
        position="fixed"
        className={classnames(
          classes.appBar,
          onPanier || onAutrePanier || onProduitDetails || fullWidth
            ? classnames(classes.whiteBackground, classes.fullWidth)
            : '',
        )}
      >
        <Toolbar className={classes.flexToolbar}>
          {onDashboard ||
            onPanier ||
            onAutrePanier ||
            onProduitDetails ||
            (fullWidth && (
              <div className={classes.groupementAndCloseButtonContainer}>
                {(onPanier || onAutrePanier || onProduitDetails || fullWidth) && (
                  <img src={logo} style={{ position: 'absolute', left: '32px' }} />
                )}
                <Tooltip
                  TransitionComponent={Fade}
                  TransitionProps={{ timeout: 600 }}
                  title="Quitter le menu paramètres"
                >
                  <IconButton color="inherit" onClick={() => setOpen(false)}>
                    <CloseIcon />
                  </IconButton>
                </Tooltip>
              </div>
            ))}
        </Toolbar>
      </AppBar>
      <Box className={classes.content}>{content}</Box>
    </Dialog>
  );
};

export default withRouter(FullScreenModal);
