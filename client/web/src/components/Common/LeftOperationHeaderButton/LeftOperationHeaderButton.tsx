import React, { FC, ReactNode , MouseEvent } from 'react';
import { Box } from '@material-ui/core';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { getUser } from '../../../services/LocalStorage';
import CustomButton from '../CustomButton';

export interface LeftOperationHeaderButtonProps {
  button?: LeftOperationHeaderButtonInterface;
}
export interface LeftOperationHeaderButtonInterface {
  url?: string;
  text: string;
  color: string;
  onClick? : () => void;
  icon?: ReactNode;
  authorized?: string[];
  userIsAuthorized?: boolean;
}

const LeftOperationHeaderButton : FC<LeftOperationHeaderButtonProps & RouteComponentProps<any, any, any>> = ({
  button,
  history,
}) => {

  console.log('****Bouton origine******' , button );
  
  const goToUrl = () => {
    if (button && button.url) {
      history.push(button.url);
    }
  };

  const fonction = () => {
    if(button){
      if(button.url){
        goToUrl()
      }
      else{
        button.onClick && button.onClick();
      }
    }
  };

  return (
    <Box
      display="flex"
      justifyContent="center"
      marginTop="24px"
      paddingBottom="16px"
      borderBottom="1px solid #E5E5E5"
    >
      {button && (
        <CustomButton color="secondary" onClick={fonction} startIcon={button.icon}>
          {button.text}
        </CustomButton>
      )}
    </Box>
  );
};

export default withRouter(LeftOperationHeaderButton);
