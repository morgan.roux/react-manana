import {
  SEARCHVariables,
  SEARCH as SEARCH_TYPE,
  SEARCH_search_data,
} from './../../../federation/search/types/SEARCH';
import { SEARCH } from './../../../federation/search/query';
import { FEDERATION_CLIENT } from './../../Dashboard/DemarcheQualite/apolloClientFederation';
import { useLazyQuery, QueryLazyOptions } from '@apollo/react-hooks';

interface SearchResult<T> {
  loading?: boolean;
  called?: boolean;
  total?: number;
  data?: T[];
  error?: Error;
  refetch: () => void;
}

const useSearch = <T extends SEARCH_search_data>(
  immediate: boolean = false,
): [(options: QueryLazyOptions<SEARCHVariables> | undefined) => void, SearchResult<T>] => {
  const [search, { loading, data, error, called, refetch }] = useLazyQuery<
    SEARCH_TYPE,
    SEARCHVariables
  >(SEARCH, {
    client: FEDERATION_CLIENT,
    fetchPolicy:'cache-and-network'
  });

  return [
    search,
    {
      loading,
      data: data?.search?.data as any,
      total: data?.search?.total,
      error,
      called,
      refetch,
    },
  ];
};

export default useSearch;
