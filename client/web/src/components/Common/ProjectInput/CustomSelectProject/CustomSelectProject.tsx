import React, { FC, ReactNode } from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Box from '@material-ui/core/Box';
import Icon from '@material-ui/core/Icon';
import Typography from '@material-ui/core/Typography';
import { CustomFormTextField } from '../../CustomTextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import useStyles from './styles';

interface CustomSelectProjectOption {
  id: string;
  title: string;
  icon: ReactNode;
  optionGroup?: CustomSelectProjectOption;
}

interface CustomSelectProjectProps {
  loading?: boolean;
  disabled?: boolean;
  value?: any;
  label: string;
  options: CustomSelectProjectOption[];
  onChange?: (value?: any) => void;
  defaultValue?: any;
  niveauMatriceFonctions: number
}

const CustomSelectProject: FC<CustomSelectProjectProps> = ({
  loading,
  options,
  label,
  value,
  disabled,
  onChange,
  defaultValue,
  niveauMatriceFonctions,
}) => {
  const classes = useStyles();

  const findValue = (id: any): CustomSelectProjectOption | null => {
    if (!id) {
      return null;
    }
    const optionValue = options.find(opt => opt.id === id);
    return optionValue ? optionValue : null;
  };

  const handleChage = (newValue?: any) => {
    if (onChange) {
      onChange(newValue ? (typeof newValue === 'string' ? newValue : newValue.id) : undefined);
    }
  };

  const renderOption = (option: CustomSelectProjectOption) => {
    return (
      <>
        <Box width="100%" display="flex" alignItems="center" justifyContent="space-between">
          <Box display="flex" alignItems="center">
            <Icon className={classes.icon}>{option.icon}</Icon>
            <Typography className={classes.title}>{option.title}</Typography>
          </Box>
        </Box>
      </>
    );
  };
  return (
    <Box className={classes.root}>
      <Autocomplete
        placeholder="Sélectionner la fonction"
        loading={loading}
        options={options}
        fullWidth={true}
        value={findValue(value)}
        defaultValue={defaultValue}
        disabled={disabled}
        onChange={(event, value) => handleChage(value)}
        renderGroup={niveauMatriceFonctions === 1 ? undefined : ({ group, children }) => {
          if (!group) {
            return children;
          }
          const option = options.find(({ optionGroup }) => optionGroup && optionGroup?.id === group)
            ?.optionGroup;
          if (!option) {
            return children;
          }

          return (
            <Box className={classes.group}>
              {renderOption(option)}
              <Box>{children}</Box>
            </Box>
          );
        }}
        renderOption={renderOption}
        groupBy={niveauMatriceFonctions === 1 ? undefined : option => option.optionGroup?.id || ''}
        getOptionLabel={option => option.title}
        renderInput={ params => (
          <CustomFormTextField
            {...params}
            label={label}
            placeholder="Sélectionner la fonction"
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <React.Fragment>
                  {loading ? <CircularProgress color="inherit" size={20} /> : null}
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
            }}
          />
        )}
      />
    </Box>
  );
};

export default CustomSelectProject;
