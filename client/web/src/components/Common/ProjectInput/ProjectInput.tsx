import { useQuery } from '@apollo/react-hooks';
import { FiberManualRecord, SupervisedUserCircle } from '@material-ui/icons';
import React, { FC, useEffect } from 'react';
import { ME_me } from '../../../graphql/Authentication/types/ME';
import { GET_SEARCH_CUSTOM_CONTENT_PROJECT } from '../../../graphql/Project';
import { getPharmacie, getUser } from '../../../services/LocalStorage';
import { capitalizeFirstLetter } from '../../../utils/capitalizeFirstLetter';
import { useValueParameterAsBoolean } from '../../../utils/getValueParameter';
import { useNiveauMatriceFonctions } from '../../Dashboard/DemarcheQualite/MatriceTache/hooks';
import CustomSelectProject from './CustomSelectProject';
import { filterSubProjects } from '../../Main/Content/TodoNew/util';
import { useStyles } from './styles';


// projetId or fonctionId or tacheId
export const findProjetById = (projets: any, id: any) => {
  return id ? projets?.find((item: any) => item.id === id || item.idTache === id || item.idFonction === id) : undefined
}


interface ProjectInputProps {
  value?: any; // projetId or fonctionId or tacheId
  disabled?: boolean;
  onChange?: (projet: any) => void;
  termologie?: string;
  onLoaded?: (projets: any) => void
  onlyMatriceFonctions: boolean;
}
const ProjectInput: FC<ProjectInputProps> = ({
  value,
  disabled,
  onChange,
  termologie,
  onLoaded,
  onlyMatriceFonctions
}) => {
  const classes = useStyles({});
  const niveauMatriceFonctions = useNiveauMatriceFonctions()
  const enableMatriceResponsabilite = useValueParameterAsBoolean('0501');

  const pharmacie: any = getPharmacie();

  const user: ME_me = getUser();

  const { data, loading } = useQuery<any>(GET_SEARCH_CUSTOM_CONTENT_PROJECT, {
    fetchPolicy: 'cache-and-network',
    variables: {
      type: ['project'],
      query: {
        query: {
          bool: {
            must: [
              {
                term: { isRemoved: false },
              },
              {
                term: { isArchived: false },
              },
              {
                bool: {
                  should:
                    enableMatriceResponsabilite && pharmacie
                      ? [
                        {
                          term: { 'pharmacie.id': pharmacie.id },
                        },
                      ]
                      : [
                        {
                          term: { 'user.id': user.id },
                        },
                        { term: { 'participants.id': user.id } },
                      ],
                  minimum_should_match: 1,
                },
              },
            ],
          },
        },
      },
      sortBy: [{ ordre: { order: 'asc' } }],
      actionStatus: 'ACTIVE',
    },

    onCompleted: (data) => {
      if (onLoaded) {
        onLoaded(data.search?.data)
      }

    }
  });

  const projetList = (data?.search?.data || []).filter((p) => onlyMatriceFonctions ? p.idTache || p.idFonction : true).sort((p1, p2) => p1.ordre - p2.ordre)


  const handleChange = (id: string) => {

    if (onChange) {
      const projet = id ? projetList.find(item => item.id === id) : undefined;
      onChange(projet);
    }
  };

  const project = findProjetById(projetList, value)

  const getProjetIcon = (projet: any) => projet.typeProject === 'PROFESSIONNEL' ? <SupervisedUserCircle htmlColor={projet.couleur?.code || '#A3B1B0'} /> :
    <FiberManualRecord
      htmlColor={(projet.couleur?.code) || '#A3B1B0'}
    />

  const options = niveauMatriceFonctions === 1 ?
    [
      ...projetList.filter(({ typeProject }) => typeProject === 'PROFESSIONNEL'),
      ...projetList.filter(({ typeProject }) => typeProject === 'PERSONNEL')
    ].map((projet) => ({
      id: projet.id,
      title: projet._name,
      icon: getProjetIcon(projet)
    }))
    : projetList
      .reduce((listProject, projectParent) => {
        const optionGroup = {
          id: projectParent.id,
          title: projectParent._name,
          icon: getProjetIcon(projectParent),
        };

        return [
          ...listProject,
          ...filterSubProjects(projectParent, enableMatriceResponsabilite, user, false).map(
            project => ({
              optionGroup,
              id: project.id,
              title: project._name,
              icon: getProjetIcon(project),
            }),
          ),
        ];
      }, [])

  return (
    <div className={classes.root}>
      <CustomSelectProject
        niveauMatriceFonctions={niveauMatriceFonctions}
        loading={loading}
        value={project?.id}
        disabled={disabled}
        onChange={handleChange}
        label={capitalizeFirstLetter(termologie || 'Fonction')}
        options={options}
      />
    </div>
  );
};

export default ProjectInput;
