import React, { FC, ChangeEvent } from 'react';
import {
  TextareaAutosize,
  FormControl,
  InputLabel,
  makeStyles,
  Theme,
  createStyles,
} from '@material-ui/core';
import { TextareaAutosizeProps } from '@material-ui/core/TextareaAutosize';
import { FormControlProps } from '@material-ui/core/FormControl';
import classnames from 'classnames';

interface CustomTextareaProps {
  label?: string;
  shrink?: boolean;
  onChangeTextarea: (event: ChangeEvent<HTMLTextAreaElement>) => void;
  disabled?: boolean;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      '& > textarea': {
        width: '100%',
        padding: '10.500px 14px',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.23)',
        minHeight: 50,
        outline: `none`,
      },
      '& > textarea:hover': {
        borderColor: `${theme.palette.primary.main} !important`,
      },
      '& > textarea:focus': {
        outline: `auto ${theme.palette.primary.main}`,
      },
      '&:hover label': {
        color: theme.palette.primary.main,
      },
      '& > * ': {
        color: theme.palette.common.black,
        fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
        lineHeight: '1.1875em',
        fontSize: '1rem',
      },
    },

    label: {
      left: -5,
      background: theme.palette.common.white,
      padding: '0px 5px',
      color: theme.palette.common.black,
      '& .MuiInputLabel-asterisk': {
        color: 'red',
      },
    },
  }),
);

const CustomTextarea: FC<TextareaAutosizeProps & FormControlProps & CustomTextareaProps> = ({
  rows,
  required = false,
  label = 'CustomTextarea',
  shrink = true,
  variant = 'outlined',
  onChangeTextarea,
  name,
  value,
  className,
  disabled,
  rowsMin,
  rowsMax,
  placeholder,
}) => {
  const classes = useStyles({});
  return (
    <FormControl
      required={required}
      className={classnames(classes.root, className)}
      variant={variant}
      disabled={disabled}
    >
      <InputLabel shrink={shrink} className={classes.label}>
        {label}
      </InputLabel>
      <TextareaAutosize
        rows={rows}
        onChange={onChangeTextarea}
        name={name}
        value={value}
        disabled={disabled}
        rowsMin={rowsMin}
        rowsMax={rowsMax}
        placeholder={placeholder}
      />
    </FormControl>
  );
};

export default CustomTextarea;
