import { Box, Button, IconButton, Typography, Paper } from '@material-ui/core';
import React, { FC, Fragment } from 'react';
import useStyles from './styles';
import classnames from 'classnames';
import { ArrowBack } from '@material-ui/icons';

export interface ChoicePageItem {
  text: string;
  img: string;
  onClick: () => void;
}

export interface ChoicePageProps {
  items: ChoicePageItem[];
  title?: string;
  subTitle?: string;
  withHeader?: boolean;
  headerTitle?: string;
  onClickArrowBack?: () => void;
}

const ChoicePage: FC<ChoicePageProps> = ({
  title,
  subTitle,
  items,
  withHeader = false,
  headerTitle = 'Title',
  onClickArrowBack,
}) => {
  const classes = useStyles({});
  return (
    <Fragment>
      {withHeader && (
        <Box className={classes.header}>
          <IconButton onClick={onClickArrowBack}>
            <ArrowBack />
          </IconButton>
          <Typography className={classes.headerTitle}>{headerTitle}</Typography>
        </Box>
      )}
      <Box
        className={classes.root}
        height={withHeader ? 'calc(100vh - 146px)' : 'calc(100vh - 86px)'}
      >
        {title && <Typography className={classes.title}>{title}</Typography>}
        {subTitle && <Typography className={classes.subTitle}>{subTitle}</Typography>}
        <Box className={classes.choiceContainer}>
          {items.map((item, index) => (
            <Paper className={classes.paper}>
              <Box className={classes.choice} onClick={item.onClick} key={index}>
                <img src={item.img} alt={item.text} className={classes.choiceImg} />
                <Typography className={classnames(classes.choiceText, classes.centerTextVertival)}>
                  {item.text}
                </Typography>
              </Box>
            </Paper>
          ))}
        </Box>
      </Box>
    </Fragment>
  );
};

export default ChoicePage;
