import React, { FC, useState } from 'react';
import { TablePagination } from '@material-ui/core';

interface CommonPaginationProps {
  take: number;
  skip: number;
  total: number;
  setTake(take: number): void;
  setSkip(skip: number): void;
  className?: string;
  disabledNextButton?: boolean;
}

const CommonPagination: FC<CommonPaginationProps> = props => {
  const { take, skip, total, setTake, setSkip, className, disabledNextButton: disabled } = props;
  const [page, setPage] = useState<number>(0);
  return (
    <TablePagination
      {...{ className }}
      rowsPerPageOptions={[5, 10, 25]}
      component="div"
      count={total}
      rowsPerPage={take}
      page={page}
      onChangePage={(event, newPage) => {
        const newSkip = page < newPage ? skip + take : skip - take;
        setSkip(newSkip < 0 ? 0 : newSkip);
        setPage(newPage);
      }}
      onChangeRowsPerPage={event => setTake(Number(event.target.value))}
      nextIconButtonProps={{ disabled }}
    />
  );
};

export default CommonPagination;
