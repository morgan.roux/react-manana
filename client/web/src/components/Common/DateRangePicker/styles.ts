import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      zIndex: 'auto',
      position: 'relative',
      width: '100%',
      border: '1px solid',
      borderColor: 'rgba(0, 0, 0, 0.23)',
      borderRadius: 4,
      '&:hover label': {
        color: theme.palette.primary.main,
      },
      '&:hover': {
        borderColor: theme.palette.primary.main,
        border: '1px solid',
      },

      '& > * ': {
        color: theme.palette.common.black,
        fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
        lineHeight: '1.1875em',
      },

      '& .DateRangePicker': {
        width: '100%',
        '& > div': {
          '& .DateRangePickerInput': {
            width: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
            // borderColor: theme.palette.common.black,
            height: 50,
            '&:hover': {
              // borderColor: theme.palette.primary.main,
            },
            '& > div': {
              marginBottom: 0,
              fontSize: '16px !important',
              '& > input': {
                fontSize: 14,
                color: `${theme.palette.common.black}`,
              },
            },
          },
          '& .DateRangePickerInput__withBorder': {
            borderRadius: 4,
            border: 'none',
          },
          '& .DateInput_input__focused': {
            color: `${theme.palette.secondary.main} !important`,
            border: 0,
          },
        },
        '& .CalendarDay__today': {
          background: theme.palette.secondary.main,
          border: `1px double ${theme.palette.secondary.main}`,
          color: theme.palette.common.white,
        },
        '& .CalendarDay__selected_start': {
          background: ' #00a699 !important',
          border: '1px double  #00a699 !important',
          color: theme.palette.common.white,
        },
      },
      '& .DateRangePicker_picker': {
        zIndex: 100,
      },
      '& .CalendarMonth_table': {
        marginTop: 10,
      },
    },
    label: {
      top: -26,
      left: -8,
      fontSize: 12.1,
      position: 'absolute',
      background: theme.palette.common.white,
      padding: '0px 4px',
      color: theme.palette.common.black,
      '& .MuiInputLabel-asterisk': {
        color: 'red',
      },
    },
    '&:hover label': {
      color: theme.palette.primary.main,
    },
  }),
);

export default useStyles;
