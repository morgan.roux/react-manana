import React, { FC, ReactNode } from 'react';
import { Box } from '@material-ui/core';
import { RouteComponentProps, withRouter } from 'react-router-dom';
// import useStyles from './styles';
import { getUser } from '../../../services/LocalStorage';
import CustomButton from '../CustomButton';
import SearchContent from '../Filter/SearchContent';
import SortContent from '../Filter/SortContent';
import CenteredSort from './CenteredSort';
export interface SideMenuButtonProps {
  button?: SideMenuButtonInterface;
  centeredSearch?: boolean;
}
export interface SideMenuButtonInterface {
  url: string;
  text: string;
  color: string;
  icon?: ReactNode;
  authorized?: string[];
  userIsAuthorized?: boolean;
}

const SideMenuButton: FC<SideMenuButtonProps & RouteComponentProps<any, any, any>> = ({
  button,
  history,
  centeredSearch,
}) => {
  // const classes = useStyles({});
  const user = getUser();
  const userRole = user.role.code;

  const goToUrl = () => {
    if (button && button.url) {
      history.push(button.url);
    }
  };

  const isAutorized = (): boolean => {
    if (button && button.authorized) {
      if (button.authorized.includes(userRole) || button.userIsAuthorized === true) {
        return true;
      }
    }
    return false;
  };

  if (!isAutorized()) {
    return null;
  }

  if (centeredSearch) {
    return (
      <Box display="flex">
        <SearchContent placeholder="Rechercher..." />
        <CenteredSort />
      </Box>
    );
  }

  return (
    <Box
      display="flex"
      justifyContent="center"
      marginTop="24px"
      paddingBottom="16px"
      borderBottom="1px solid #E5E5E5"
    >
      {button && (
        <CustomButton color="secondary" onClick={goToUrl} startIcon={button.icon}>
          {button.text}
        </CustomButton>
      )}
    </Box>
  );
};

export default withRouter(SideMenuButton);
