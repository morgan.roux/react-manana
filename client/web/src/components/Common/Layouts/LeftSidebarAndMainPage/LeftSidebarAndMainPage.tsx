import { Box, CssBaseline } from '@material-ui/core';
import React, { ReactNode } from 'react';

import Drawer from '../../Drawer';
import MobileTopBar from '../../MobileTopBar';
import useStyles from './styles';

interface LeftSidebarAndMainPageProps {
  drawerTitle?: string;
  minHeight?: string;
  sidebarChildren?: ReactNode;
  mainChildren?: ReactNode;
  optionBtn?: ReactNode[];
  openDrawer?: boolean;
  drawerBackUrl?: string;
  drawerCloseUrl?: string;
  setOpenDrawer?: React.Dispatch<React.SetStateAction<boolean>>;
  handleDrawerToggle?: () => void;
}

const LeftSidebarAndMainPage: React.FC<LeftSidebarAndMainPageProps> = ({
  drawerTitle,
  minHeight = 'calc(100vh - 86px)',
  sidebarChildren,
  mainChildren,
  optionBtn,
  openDrawer,
  handleDrawerToggle,
  drawerBackUrl,
  drawerCloseUrl
}) => {
  const classes = useStyles({});

  return (
    <Box className={classes.root} minHeight={minHeight}>
      <CssBaseline />
      <MobileTopBar
        title={drawerTitle}
        withBackBtn={true}
        handleDrawerToggle={handleDrawerToggle}
        optionBtn={optionBtn}
        withTopMargin={true}
        breakpoint="sm"
        backUrl={drawerBackUrl}
        closeUrl={drawerCloseUrl}
        onClickBack={handleDrawerToggle}
      />

      <Drawer
        mobileOpen={openDrawer}
        handleDrawerToggle={handleDrawerToggle}
        mobileTobBarTitle="Filtre"
        backUrl={drawerBackUrl}
        closeUrl={drawerCloseUrl}
      >
        <Box borderRight="1px solid #E5E5E5">
          <Box className={classes.sidebar}>{sidebarChildren}</Box>
        </Box>
      </Drawer>

      <Box className={classes.main}>{mainChildren}</Box>
    </Box>
  );
};

export default LeftSidebarAndMainPage;
