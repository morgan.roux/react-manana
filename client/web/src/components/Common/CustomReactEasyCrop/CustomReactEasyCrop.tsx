import React, { useState, useCallback, FC, useEffect } from 'react';
import Cropper from 'react-easy-crop';
import Slider from '@material-ui/core/Slider';
import Button from '@material-ui/core/Button';
import ImgDialog from './ImgDialog';
import getCroppedImg from './CropImage';
import { useStyles } from './styles';
import ZoomOutIcon from '@material-ui/icons/RemoveCircleOutline';
import ZoomInIcon from '@material-ui/icons/AddCircleOutline';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';
import RotateRightIcon from '@material-ui/icons/RotateRight';
import { Size } from 'react-easy-crop/types';

// const dogImg = 'https://img.huffingtonpost.com/asset/5ab4d4ac2000007d06eb2c56.jpeg?cache=sih0jwle4e&ops=1910_1000';

interface CustomReactEasyCropProps {
  src: string;
  withZoom: boolean;
  savedCroppedImage?: boolean;
  croppedImage: string | any;
  setCroppedImage: React.Dispatch<string | any>;
  withShowResultBtn?: boolean;
  withRotate?: boolean;
  setCropedFiles?: (files: File[]) => void;
  customCropSize?: Size;
}

const CustomReactEasyCrop: FC<CustomReactEasyCropProps> = ({
  src,
  withShowResultBtn,
  withRotate,
  withZoom,
  savedCroppedImage,
  setCroppedImage,
  setCropedFiles,
  customCropSize,
}) => {
  const classes = useStyles({});
  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [rotation, setRotation] = useState(0);
  const [zoom, setZoom] = useState(1);
  const [croppedAreaPixels, setCroppedAreaPixels] = useState(null);
  const [previewImage, setPreviewImage] = useState(null);

  const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
    setCroppedAreaPixels(croppedAreaPixels);
  }, []);

  const showPreviewImage = useCallback(async () => {
    try {
      const image: any = await getCroppedImg(src, croppedAreaPixels, rotation, setCropedFiles);
      setPreviewImage(image);
    } catch (e) {
      console.error(e);
    }
  }, [croppedAreaPixels, rotation]);

  const onClose = useCallback(() => {
    setPreviewImage(null);
  }, []);

  /**
   * Saved cropped image
   */
  useEffect(() => {
    if (savedCroppedImage) {
      console.log('sssssssssssss');

      save();
    }
  }, [savedCroppedImage]);

  const save = useCallback(async () => {
    try {
      console.log(
        'src, croppedAreaPixels, rotation, setCropedFiles',
        src,
        croppedAreaPixels,
        rotation,
        setCropedFiles,
      );
      const croppedImage = await getCroppedImg(src, croppedAreaPixels, rotation, setCropedFiles);
      console.log('croppedImage', croppedImage);
      setCroppedImage(croppedImage);
    } catch (e) {
      console.error(e);
    }
  }, [croppedAreaPixels, rotation]);

  return (
    <div>
      <div className={classes.cropContainer}>
        <Cropper
          image={src}
          crop={crop}
          rotation={rotation}
          zoom={zoom}
          aspect={4 / 3}
          onCropChange={setCrop}
          onRotationChange={setRotation}
          onCropComplete={onCropComplete}
          onZoomChange={setZoom}
          showGrid={true}
          restrictPosition={false}
          cropSize={customCropSize || undefined}
        />
      </div>
      <div className={classes.controls}>
        {withZoom && (
          <div className={classes.sliderContainer}>
            <ZoomOutIcon />
            <Slider
              value={zoom}
              min={1}
              max={3}
              step={0.1}
              aria-labelledby="Zoom"
              className={classes.slider}
              onChange={(e, zoom) => setZoom(zoom as any)}
            />
            <ZoomInIcon />
          </div>
        )}

        {withRotate && (
          <div className={classes.sliderContainer}>
            <RotateLeftIcon />
            <Slider
              value={rotation}
              min={0}
              max={360}
              step={1}
              aria-labelledby="Rotation"
              className={classes.slider}
              onChange={(e, rotation) => setRotation(rotation as any)}
            />
            <RotateRightIcon />
          </div>
        )}

        {withShowResultBtn && (
          <Button
            onClick={showPreviewImage}
            variant="contained"
            color="primary"
            className={classes.cropButton}
          >
            Afficher le résultat
          </Button>
        )}
      </div>
      <ImgDialog img={previewImage} onClose={onClose} />
    </div>
  );
};

CustomReactEasyCrop.defaultProps = {
  withShowResultBtn: false,
  withRotate: false,
  withZoom: true,
};

export default CustomReactEasyCrop;
