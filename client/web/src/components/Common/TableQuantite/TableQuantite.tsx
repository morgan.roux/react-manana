import React, { FC, useState } from 'react';
import { ProduitCanal } from '../../../graphql/ProduitCanal/types/ProduitCanal';
import { useStyles } from './styles';
import gql from 'graphql-tag';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { getUser } from '../../../services/LocalStorage';
import ICurrentPharmacieInterface from '../../../Interface/CurrentPharmacieInterface';
import { GET_CURRENT_PHARMACIE } from '../../../graphql/Pharmacie/local';
import { GET_OPERATION_PANIER } from '../../../graphql/OperationCommerciale/local';
import {
  COLLABORATEUR_COMMERCIAL,
  ADMINISTRATEUR_GROUPEMENT,
  COLLABORATEUR_NON_COMMERCIAL,
  GROUPEMENT_AUTRE,
} from '../../../Constant/roles';
import { Typography, Box, IconButton, InputBase } from '@material-ui/core';
import { Remove, Save, Add } from '@material-ui/icons';
import { GET_LOCAL_PANIER } from '../../../graphql/Panier/local';

interface QuantiteProps {
  currentCanalArticle: ProduitCanal | null;
}

export interface ArticleOC {
  id: string;
  quantite: number;
  produitCanal: {
    id: string;
    prixPhv: number;
    remises: {
      id: string;
      quantiteMin: number;
      quantiteMax: number;
      pourcentageRemise: number;
      nombreUg: number;
      codeCipUg: number;
      codeCip13Ug: string;
    };
  };
}

export interface ArticleOCArray {
  articleOcArray: ArticleOC[];
}
export const GET_ARTICLE_OC_ARRAY = gql`
  {
    articleOcArray @client {
      id
      quantite
      produitCanal {
        id
        prixPhv
        remises {
          id
          nombreUg
          pourcentageRemise
          quantiteMax
          quantiteMin
        }
      }
    }
  }
`;

const Quantite: FC<QuantiteProps & RouteComponentProps<any, any, any>> = ({
  currentCanalArticle,
  location,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();

  // get current user role
  const user = getUser();

  const localPanier = useQuery(GET_LOCAL_PANIER);

  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

  const operationPanierResult = useQuery(GET_OPERATION_PANIER);

  const isChangeDisabled =
    user &&
    user.role &&
    user.role.code &&
    user.role.code !== COLLABORATEUR_COMMERCIAL &&
    user.role.code !== ADMINISTRATEUR_GROUPEMENT &&
    user.role.code !== COLLABORATEUR_NON_COMMERCIAL &&
    user.role.code !== GROUPEMENT_AUTRE
      ? false
      : user.role.code === ADMINISTRATEUR_GROUPEMENT &&
        location.pathname.includes('operations-commerciales')
      ? false
      : true;

  const stv = currentCanalArticle && currentCanalArticle.stv ? currentCanalArticle.stv : 0;
  const qteStock =
    currentCanalArticle && currentCanalArticle.qteStock ? currentCanalArticle.qteStock : 0;

  const [showQteSave, setShowQteSave] = useState<boolean>(false);

  const quantiteStv = (stv: number, qte: number) => {
    if (qte === 0) return 0;
    return stv === 1 ? qte : qte < stv ? stv : qte - (qte % stv);
  };

  const panierLignes =
    (localPanier &&
      localPanier.data &&
      localPanier.data.localPanier &&
      localPanier.data.localPanier.panierLignes) ||
    [];

  const ligne = panierLignes.find(
    ligne =>
      ligne &&
      currentCanalArticle &&
      ligne.produitCanal &&
      ligne.produitCanal.id === currentCanalArticle.id,
  );
  const currentQuantite = (ligne && ligne.quantite) || 0;

  const incQuantite = (e: any) => {
    e.stopPropagation();
    const quantite = currentQuantite + stv;
    checkEditQuantite(quantite);
  };

  const decQuantite = (e: any) => {
    e.stopPropagation();
    const quantite = currentQuantite - stv;
    checkEditQuantite(quantite);
  };

  const handleChangeQuantite = (event: React.ChangeEvent<{ value: string }>) => {
    const quantite = parseInt(event.target.value, 10);
    checkEditQuantite(quantite);
    setShowQteSave(true);
  };

  const checkEditQuantite = (quantite: number) => {
    const qteStock =
      currentCanalArticle && currentCanalArticle.qteStock ? currentCanalArticle.qteStock : 0;
    if (isNaN(quantite)) {
      writePanier(0);
      return;
    } else {
      writePanier(quantite > 0 ? quantite : 0);
    }
  };

  const writePanier = (quantite: number) => {
    client.writeData({
      data: {
        localPanier: {
          panierLignes:
            quantite === 0
              ? panierLignes.filter(
                  ligne =>
                    ligne &&
                    ligne.produitCanal &&
                    ligne.produitCanal.id &&
                    currentCanalArticle &&
                    currentCanalArticle.id !== ligne.produitCanal.id,
                )
              : panierLignes.length > 0
              ? panierLignes.some(
                  ligne =>
                    ligne &&
                    ligne.produitCanal &&
                    ligne.produitCanal.id &&
                    currentCanalArticle &&
                    currentCanalArticle.id === ligne.produitCanal.id,
                )
                ? panierLignes.map(ligne =>
                    ligne &&
                    ligne.produitCanal &&
                    ligne.produitCanal.id &&
                    currentCanalArticle &&
                    currentCanalArticle.id === ligne.produitCanal.id
                      ? {
                          produitCanal: { ...currentCanalArticle },
                          quantite,
                          __typename: 'PanierLigne',
                        }
                      : ligne,
                  )
                : [
                    ...panierLignes,
                    {
                      produitCanal: { ...currentCanalArticle },
                      quantite,
                      __typename: 'PanierLigne',
                    },
                  ]
              : [{ produitCanal: { ...currentCanalArticle }, quantite, __typename: 'PanierLigne' }],
          __typename: 'PanierLignes',
        },
      },
    });
  };

  const saveQte = (e: any) => {
    e.stopPropagation();
    writePanier(quantiteStv(stv, currentQuantite));
    setShowQteSave(false);
  };

  return qteStock <= 0 ? (
    <Typography className={classes.ruptureText}>Rupture</Typography>
  ) : (
    <Box
      className={classes.panierListeRemiseQuantite}
      width={showQteSave && qteStock > 0 ? '152px' : '112px'}
    >
      <Box display="flex" justifyContent="center" alignItems="center">
        <IconButton size="small" onClick={decQuantite} disabled={qteStock <= 0 ? true : false}>
          <Remove className={classes.quantiteButton} />
        </IconButton>
      </Box>
      <Box className={classes.quantiteTextField}>
        <InputBase
          onChange={handleChangeQuantite}
          onClick={e => e.stopPropagation()}
          value={currentQuantite}
          inputProps={{ classes: classes.underline, input: classes.inputFont }}
          disabled={
            qteStock <= 0
              ? true
              : location.pathname === '/autrepanier' || isChangeDisabled
              ? true
              : false
          }
        />
      </Box>

      <Box display="flex" justifyContent="center" alignItems="center">
        <IconButton size="small" onClick={incQuantite} disabled={qteStock <= 0 ? true : false}>
          <Add className={classes.quantiteButton} />
        </IconButton>
      </Box>
      {showQteSave && qteStock > 0 && (
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          className={classes.quantiteSaveButton}
        >
          <IconButton size="small" onClick={saveQte}>
            <Save className={classes.blueButton} />
          </IconButton>
        </Box>
      )}
    </Box>
  );
};

export default withRouter(Quantite);
