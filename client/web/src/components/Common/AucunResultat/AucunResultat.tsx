import React, { FC } from 'react';
import useStyles from './styles';
import { Box } from '@material-ui/core';
import BlockIcon from '@material-ui/icons/Block';
interface AucunResultatProps {}
const AucunResultat: FC<AucunResultatProps> = ({}) => {
  const classes = useStyles({});
  return (
    <Box justifyContent="center" display="flex" flexDirection="row" alignItems="center">
      <BlockIcon />
      Aucun résultat
    </Box>
  );
};

export default AucunResultat;
