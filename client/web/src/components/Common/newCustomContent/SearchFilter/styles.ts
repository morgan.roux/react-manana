import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: theme.spacing(3),
      width: '100%',
    },
    label: {
      fontFamily: 'Roboto',
      fontSize: 20,
      fontWeight: 'bold',
      marginBottom: theme.spacing(3),
    },
    searchBar: {
      top: 0,
      width: '100%',
      opacity: 1,
      zIndex: 10,
      position: 'sticky',
      display: 'flex',
      height: 70,
      alignItems: 'center',
      justifyContent: 'space-between',
      padding: '0px 24px 0 24px',
      background: theme.palette.primary.main,
      '@media (max-width: 692px)': {
        flexWrap: 'wrap',
        justifyContent: 'center',
      },
    },
    search: {
      '@media (max-width: 580px)': {
        marginBottom: 15,
      },
    },
    alignCenter: {
      textAlign: 'center',
      margin: '40px 0px',
      fontSize: 20,
      fontWeight: 'bold',
    },
    formControl: {
      width: '100%',
      marginBottom: theme.spacing(3),
      padding: '6px 0 0 ',
    },
    cssOutlinedInput: {
      '&$cssFocused $notchedOutline': {
        borderColor: ` #E9E9E9 !important`,
      },
    },
    notchedOutline: {
      borderWidth: '1px',
      // borderColor: `${theme.palette.primary.dark} !important`,
    },
    searchButton: {
      width: 156,
      height: 50,
      marginBottom: theme.spacing(3),
    },
    resetButton: {
      width: 156,
      height: 50,
    },
    form: {
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
      // gridTemplateColumns: 'repeat(5, 1fr)',
      '& > div': {
        // width: 200,
        margin: 10,
      },
      marginBottom: 40,
      '@media (max-width:1200px)': {
        flexWrap: 'wrap',
        justifyContent: 'center',
        '& > div': {
          maxWidth: '30%',
        },
      },
      '@media (max-width:1024px)': {
        justifyContent: 'space-between',
        '& > div': {
          maxWidth: '46%',
        },
      },
      '@media (max-width:800px)': {
        justifyContent: 'center',
        '& > div': {
          maxWidth: '95%',
        },
      },
    },
    selectLabel: {
      transform: 'translate(14px, 0px) scale(0.75) !important',
      background: theme.palette.common.white,
    },
    cssOutlinedSelect: {
      width: '100%',
      minHeight: 30,
      display: 'flex',
      alignItems: 'center',
    },
    verticalDivider: {
      marginLeft: theme.spacing(8),
      padding: '0 0 24px 0',
    },
    title: {
      textAlign: 'center',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 22,
      color: '#616161',
      marginTop: 32,
      marginBottom: 24,
    },
    btnContainer: {
      display: 'flex',
      justifyContent: 'center',
      '& button:nth-child(1)': {
        marginRight: 25,
      },
    },
  }),
);

export default useStyles;
