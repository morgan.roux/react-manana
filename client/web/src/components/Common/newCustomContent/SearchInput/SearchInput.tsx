import React, { FC, useState, useRef, useEffect } from 'react';
import { useApolloClient, useQuery } from '@apollo/react-hooks';
import useStyles from './styles';
import { SearchInput } from '../../../Dashboard/Content/SearchInput';
import { debounce } from 'lodash';
import { GET_LOCAL_SEARCH, SearchInterface } from '../../withSearch/withSearch';
interface CustomContentSearchInputProps {
  fullWidth?: boolean;
  searchPlaceholder: string;
  disabled?: boolean;
}

const CustomContentSearchInput: FC<CustomContentSearchInputProps> = ({
  fullWidth,
  searchPlaceholder,
  disabled,
}) => {
  const classes = useStyles({});
  const [debouncedText, setDebouncedText] = useState<string>('');
  const client = useApolloClient();
  const onChangeSearchText = (value: string) => {
    setDebouncedText(value);
  };
  const searchTextQuery = useQuery<SearchInterface>(GET_LOCAL_SEARCH);
  const searchText =
    searchTextQuery &&
    searchTextQuery.data &&
    searchTextQuery.data.searchText &&
    searchTextQuery.data.searchText.text;

  const debouncedSearchText = useRef(
    debounce((value: string) => {
      client.writeData({
        data: { searchText: { text: value || '', __typename: 'searchText' } },
      });
    }, 1500),
  );

  useEffect(() => {
    setDebouncedText(searchText ? searchText : '');
  }, [searchText]);

  useEffect(() => {
    debouncedSearchText.current(debouncedText);
  }, [debouncedText]);

  return (
    <div className={fullWidth ? classes.fullWidth : classes.root}>
      <SearchInput
        value={debouncedText}
        searchInFullWidth={fullWidth}
        noPrefixIcon={true}
        onChange={onChangeSearchText}
        placeholder={searchPlaceholder}
        disabled={disabled}
        outlined={true}
      />
    </div>
  );
};

export default CustomContentSearchInput;
