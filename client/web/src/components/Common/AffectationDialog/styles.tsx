import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    personnelGroupementFilterRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: '50px 200px',
      position: 'relative',
    },
    personnelGroupementFilterTitle: {
      color: '#616161',
      fontFamily: 'Roboto',
      fontSize: 22,
      width: 280,
      textAlign: 'center',
      marginBottom: 35,
    },
    personnelGroupementFilterFormContainer: {
      width: 445,
      '& > div': {
        marginBottom: 40,
      },
    },
    personnelGroupementFilterBtnsContainer: {
      width: '100%',
      display: 'flex',
      justifyContent: 'space-between',
      margin: '0px !important',
      '& > button': {
        height: 50,
        width: 200,
      },
    },
    dateInput: {
      marginBottom: 25,
    },
  }),
);

export default useStyles;
