import React, { FC, useState, ReactElement, useEffect } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Box, Grid, Divider } from '@material-ui/core';
import Filter from '../Filter';
import NoItemListImage from '../NoItemListImage';
import NoItemContentImage from '../NoItemContentImage';
import { Loader } from '../../Dashboard/Content/Loader';
import { CardTheme, CardThemeList } from '@app/ui-kit/';
import { useApolloClient } from '@apollo/react-hooks';
import LeftOperationHeaderButton from '../LeftOperationHeaderButton/LeftOperationHeaderButton';
import { LeftOperationHeaderButtonInterface } from '../LeftOperationHeaderButton/LeftOperationHeaderButton';
import moment from 'moment';

export enum LayoutType {
  TwoColumns,
  WithFilter,
  Empty,
}

export interface ItemContainerProps {
  typologie: string;
  handleScroll?: any;
  noContentValues?: any;
  listItemFields?: any;
  headerButton?: LeftOperationHeaderButtonInterface;
  listResult?: any;
  leftListItem?: any;
  mainItem?: any;
  moreOptionsItem?: any;
  layout: LayoutType;
  leftListChildren?: ReactElement;
  rightContentChildren?: ReactElement;
  match: {
    params: { id: string | undefined; view: string | undefined };
  };
}

const ItemContainer: FC<ItemContainerProps & RouteComponentProps<any, any, any>> = ({
  typologie,
  handleScroll,
  noContentValues,
  listItemFields,
  headerButton,
  listResult,
  leftListChildren,
  rightContentChildren,
  moreOptionsItem,
  mainItem,
  layout,
  match,
  location: { pathname },
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [currentItem, setCurrentItem] = useState<any>();
  const data = listResult || [];

  const isActive = (item: any) => {
    return currentItem && item && currentItem.id === item.id;
  };

  const memorized = React.useMemo(
    () => (
      <nav className={classes.drawer}>
        <Filter />
      </nav>
    ),
    [],
  );

  const titleStart: string = typologie[0].toUpperCase();

  useEffect(() => {
    if (data) {
      console.log('*****currentitem******', currentItem);
      if (currentItem && currentItem.id) {
        const newValue = data.find(item => item && item.id === currentItem.id);
        setCurrentItem(newValue);
      } else if (match.params.id && currentItem === undefined) {
        const newValue = data.find(item => item && item.id === match.params.id);
        setCurrentItem(newValue);
      }
    }
  }, [data]);

  if (layout === LayoutType.WithFilter) {
    return (
      <Box className={classes.root}>
        {pathname !== '/pilotage/business/pharmacie' &&
          pathname !== '/pilotage/feedback/pharmacie' &&
          pathname !== '/pilotage/feedback' &&
          memorized}
        {mainItem}
      </Box>
    );
  }

  const scroll = (): void => {};

  return (
    <Box className={classes.root}>
      {memorized}
      <div className={classes.leftListContainer}>
        {headerButton && <LeftOperationHeaderButton button={headerButton} />}
        <Grid id="listLaboGrid" onScroll={() => scroll()} item={true} className={classes.leftList}>
          {listResult && listResult.loading ? (
            <Loader />
          ) : listItemFields && data && data.length ? (
            <CardThemeList>
              {data.map((item, index) => (
                <div key={index}>
                  <CardTheme
                    title={`${item.code}. ${item.libelle}`}
                    createDate={moment(new Date(item.createdAt)).format('l')}
                    updateDate={moment(new Date(item.updatedAt)).format('l')}
                    item={item}
                    moreOptionsItem={
                      moreOptionsItem
                        ? React.cloneElement(
                            moreOptionsItem,
                            {
                              active: isActive(item),
                              currentItem: item,
                              setCurrentItem,
                              url: listItemFields && listItemFields.url,
                            },
                            null,
                          )
                        : null
                    }
                    currentId={currentItem}
                    setCurrentId={setCurrentItem}
                    listItemFields={listItemFields}
                  />
                  <Box paddingLeft="16px" paddingRight="16px">
                    <Divider />
                  </Box>
                </div>
              ))}
            </CardThemeList>
          ) : (
            noContentValues && (
              <NoItemListImage
                title={
                  (noContentValues && noContentValues.list && noContentValues.list.title) || ''
                }
                subtitle={
                  (noContentValues && noContentValues.list && noContentValues.list.subtitle) || ''
                }
              />
            )
          )}
          {leftListChildren &&
            data &&
            data.map(item => (
              <div>
                {React.cloneElement(leftListChildren, {
                  item,
                  active: isActive(item),
                  setCurrentItem,
                })}
              </div>
            ))}
        </Grid>
      </div>
      <div className={classes.rightContainer}>
        {rightContentChildren && currentItem ? (
          React.cloneElement(rightContentChildren, {
            currentItem,
            setCurrentItem,
            url: listItemFields && listItemFields.url,
          })
        ) : (
          <NoItemContentImage
            title={
              (noContentValues && noContentValues.content && noContentValues.content.title) || ''
            }
            subtitle={
              (noContentValues && noContentValues.content && noContentValues.content.subtitle) || ''
            }
          />
        )}
      </div>
    </Box>
  );
};

export default withRouter(ItemContainer);
