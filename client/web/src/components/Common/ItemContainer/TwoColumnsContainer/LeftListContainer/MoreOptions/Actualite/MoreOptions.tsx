import React, { FC, useState, MouseEvent, Fragment } from 'react';
import { ConfirmDeleteDialog } from '../../../../../ConfirmDialog';
import { MoreOptionsItem } from '../../../../../MoreOptions/MoreOptions';
import { userIsAuthorized } from '../../../../../../../services/actualiteFilter';
import { useMutation, useApolloClient } from '@apollo/react-hooks';
import {
  MARK_ACTUALITE_NOT_SEEN,
  MARK_ACTUALITE_NOT_SEENVariables,
} from '../../../../../../../graphql/Actualite/types/MARK_ACTUALITE_NOT_SEEN';
import {
  DO_MARK_ACTUALITE_NOT_SEEN,
  DO_DELETE_ACTUALITE,
} from '../../../../../../../graphql/Actualite';
import { SEARCH, SEARCHVariables } from '../../../../../../../graphql/search/types/SEARCH';
import SnackVariableInterface from '../../../../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';
import { ME_me } from '../../../../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../../../../services/LocalStorage';
import {
  DELETE_ACTUALITE,
  DELETE_ACTUALITEVariables,
} from '../../../../../../../graphql/Actualite/types/DELETE_ACTUALITE';
import { stopEvent } from '../../../../../../../services/DOMEvent';
import { SEARCH as SEARCH_QUERY } from '../../../../../../../graphql/search/query';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import MoreOptionsComponent from '../../../../../MoreOptions';
import Backdrop from '../../../../../../Common/Backdrop';
import { AppAuthorization } from '../../../../../../../services/authorization';
interface MoreOptionsProps {
  active?: boolean;
  url?: string;
  currentItem?: any;
  setCurrentItem?: (id: string | null) => void;
  listResult?: any;
}

const MoreOptions: FC<MoreOptionsProps & RouteComponentProps> = ({
  active,
  currentItem,
  url,
  setCurrentItem,
  listResult,
  history,
}) => {
  const client = useApolloClient();
  const [openConfirmDialog, setOpenConfirmDialog] = useState(false);
  const currentUser: ME_me = getUser();
  const actualite = currentItem;
  const idPharmacieUser =
    (currentUser &&
      currentUser.userPpersonnel &&
      currentUser.userPpersonnel.pharmacie &&
      currentUser.userPpersonnel.pharmacie.id) ||
    (currentUser &&
      currentUser.userTitulaire &&
      currentUser.userTitulaire.titulaire &&
      currentUser.userTitulaire.titulaire.pharmacieUser &&
      currentUser.userTitulaire.titulaire.pharmacieUser.id);

  const auth = new AppAuthorization(currentUser);

  const [doDeleteActualite, doDeleteActualiteResult] = useMutation<
    DELETE_ACTUALITE,
    DELETE_ACTUALITEVariables
  >(DO_DELETE_ACTUALITE, {
    variables: { id: actualite.id, idPharmacieUser, userId: currentUser && currentUser.id },
    update: (cache, { data }) => {
      if (data && data.deleteActualite) {
        const actu = data.deleteActualite;
        const req = cache.readQuery<SEARCH, SEARCHVariables>({
          query: SEARCH_QUERY,
          variables: listResult && listResult.variables,
        });
        if (req && req.search && req.search.data) {
          cache.writeQuery({
            query: SEARCH_QUERY,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.filter((item: any) => {
                    if (item && item.id !== actu.id) {
                      return item;
                    }
                  }),
                },
              },
            },
            variables: listResult && listResult.variables,
          });
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  const editActualite = (event: MouseEvent<any>) => {
    stopEvent(event);
    history.push(`/edit${url}/${currentItem.id}`);
  };

  const deleteActualite = (event: MouseEvent<any>) => {
    stopEvent(event);
    setOpenConfirmDialog(false);
    doDeleteActualite();
  };

  const markActualiteNotSeen = (event: MouseEvent<any>) => {
    stopEvent(event);
    event.preventDefault();
    event.stopPropagation();
    if (actualite.seen === true) {
      doMarkActualiteNotSeen({
        variables: { id: actualite.id, idPharmacieUser, userId: currentUser && currentUser.id },
      });
    }
  };

  const openDialog = (event: MouseEvent<any>) => {
    stopEvent(event);
    setOpenConfirmDialog(true);
  };

  let moreOptionsItems: MoreOptionsItem[] =
    userIsAuthorized(currentUser) && actualite && actualite.seen
      ? [
          {
            title: 'Marquer comme non lue',
            onClick: markActualiteNotSeen,
            disabled: !auth.isAuthorizedToMarkSeenOrNotSeenActu(),
          },
        ]
      : [];

  if (userIsAuthorized(currentUser)) {
    if (actualite && actualite.operation) {
      moreOptionsItems = [
        { title: 'Modifier', onClick: editActualite, disabled: !auth.isAuthorizedToEditActu() },
        ...moreOptionsItems,
      ];
    } else {
      moreOptionsItems = [
        { title: 'Modifier', onClick: editActualite, disabled: !auth.isAuthorizedToEditActu() },
        { title: 'Supprimer', onClick: openDialog, disabled: !auth.isAuthorizedToDeleteActu() },
        ...moreOptionsItems,
      ];
    }
  }

  const [doMarkActualiteNotSeen, doMarkActualiteNotSeenResult] = useMutation<
    MARK_ACTUALITE_NOT_SEEN,
    MARK_ACTUALITE_NOT_SEENVariables
  >(DO_MARK_ACTUALITE_NOT_SEEN, {
    update: (cache, { data }) => {
      if (data && data.markAsNotSeenActualite) {
        const req = cache.readQuery<SEARCH, SEARCHVariables>({
          query: SEARCH_QUERY,
          variables: listResult && listResult.variables,
        });
        if (req && req.search && req.search.data) {
          if (active && setCurrentItem) {
            window.history.pushState(null, '', `#${url}`);
            setCurrentItem(null);
          }
          cache.writeQuery({
            query: SEARCH_QUERY,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.map((item: any) => {
                    if (
                      item &&
                      data.markAsNotSeenActualite &&
                      item.id === data.markAsNotSeenActualite.id
                    ) {
                      item.seen = data.markAsNotSeenActualite.seen;
                      return item;
                    }
                  }),
                },
              },
            },
            variables: listResult && listResult.variables,
          });
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  if (doMarkActualiteNotSeenResult.loading || doDeleteActualiteResult.loading) {
    return <Backdrop />;
  }

  return (
    <Fragment>
      {moreOptionsItems && moreOptionsItems.length > 0 && (
        <MoreOptionsComponent items={moreOptionsItems} active={active} />
      )}
      <ConfirmDeleteDialog
        title="Suppression d'actualité"
        content={`Etes-vous sûr de vouloir supprimer l'actualité « ${actualite.libelle} » ?`}
        open={openConfirmDialog}
        setOpen={setOpenConfirmDialog}
        onClickConfirm={deleteActualite}
      />
    </Fragment>
  );
};

export default withRouter(MoreOptions);
