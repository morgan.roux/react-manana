import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    hrHeight: {
      height: '1px !important',
      background: '#E6E6E6',
    },
  }),
);

export default useStyles;
