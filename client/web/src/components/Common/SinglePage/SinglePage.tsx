import { Typography } from '@material-ui/core/';
import { withStyles } from '@material-ui/core/styles';
import React, { FC, ReactNode } from 'react';
import logo from '../../../assets/img/gcr_pharma.svg';
import styles from './styles';

interface IPROPS {
  children?: ReactNode;
  title?: string;
  width?: string;
  classes: {
    container?: string;
    content?: string;
    top?: string;
    topTitle?: string;
    title?: string;
    img?: string;
  };
}

const SinglePage: FC<IPROPS> = props => {
  const { classes, children, title, width } = props;
  const mobileView = window.innerWidth < 768;
  if (mobileView) {
    return (
      <div className={classes.container}>
        <div className={classes.top}>
          <img src={logo} className={classes.img} alt="logo gcr_pharma" />
          <Typography variant="h6" gutterBottom={true} className={classes.title}>
            {title}
          </Typography>
        </div>
        {children}
      </div>
    );
  }
  return (
    <div className={classes.container}>
      <div>
        <div className={classes.top}>
          <img src={logo} className={classes.img} alt="logo gcr_pharma" />
          <Typography variant="h6" gutterBottom={true} className={classes.title}>
            {title}
          </Typography>
        </div>
        {children}
      </div>
    </div>
  );
};

export default withStyles(styles, { withTheme: true })(SinglePage);

SinglePage.defaultProps = {
  title: '',
  width: '400px',
};
