import { createStyles } from '@material-ui/core/styles';

export default () =>
  createStyles({
    container: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      height: '100%',
      width: '100%',
      overflow: 'auto',
      '@media (min-width: 768px)': {
        justifyContent: 'center',
        '& > div': {
          width: 400,
        },
      },
      '@media (max-width: 767px)': {
        padding: '24px 24px',
        height: 'calc(100% - 70px)',
      },
    },
    content: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      '@media (max-width: 442px)': {
        width: 300,
      },
      '@media (max-width: 356px)': {
        width: 270,
      },
    },
    top: {
      display: 'flex',
      flexDirection: 'column',
      marginBottom: 10,
    },
    title: {
      textAlign: 'center',
      letterSpacing: 0,
      color: '#1D1D1D',
      marginTop: 24,
      fontSize: '1.5rem',
      marginBottom: 24,
      '@media (max-width: 1024px)': {
        fontSize: '1.25rem',
      },
    },
    topTitle: {
      margin: 'auto',
      letterSpacing: 0,
      color: '#5D5D5D',
      opacity: 1,
    },
    mb25: {
      marginBottom: 25,
    },
  });
