import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
const drawerWidth = 275;
export const useStyles = makeStyles(theme =>
  createStyles({
    drawer: {
      position: 'sticky',
      top: 0,
      width: drawerWidth,
      flexShrink: 0,
      height: 'calc(100vh - 86px)',
      borderRight: '1px solid #E5E5E5',
    },
  }),
);
