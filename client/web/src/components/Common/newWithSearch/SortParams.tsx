export const produitCanalSortParam = [
  {
    label: 'Code Référence',
    name: 'produit.produiCode.code',
    direction: 'asc',
    __typename: 'sortItem',
  },
  {
    label: 'Libellé',
    name: 'produit.libelle',
    direction: 'asc',
    active: true,
    __typename: 'sortItem',
  },
  {
    label: 'Laboratoire',
    name: 'laboratoire.nomLabo',
    direction: 'desc',
    __typename: 'sortItem',
  },
  {
    label: 'Stv',
    name: 'stv',
    direction: 'desc',
    __typename: 'sortItem',
  },
  {
    label: 'Tarif HT',
    name: 'prixPhv',
    direction: 'desc',

    __typename: 'sortItem',
  },
  {
    label: 'Stock plateforme',
    name: 'qteStock',
    direction: 'desc',
    __typename: 'sortItem',
  },
];

export const produitSortParam = [
  {
    label: 'Code Référence',
    //name: 'produiCode.code',
    name: 'produit.produitCode.code',
    direction: 'asc',
    __typename: 'sortItem',
  },
  {
    label: 'Libellé',
    //name: 'libelle',
    name: 'produit.libelle',
    direction: 'asc',
    active: true,
    __typename: 'sortItem',
  },
  {
    label: 'Laboratoire',
    // name: 'produitTechReg.laboExploitant.nomLabo',
    name: 'produit.produitTechReg.laboExploitant.nomLabo',
    direction: 'desc',
    __typename: 'sortItem',
  },
];

export const operationSortParam = [
  {
    label: 'Libellé',
    name: 'libelle',
    direction: 'asc',
    __typename: 'sortItem',
  },
  {
    label: 'Date début',
    name: 'dateDebut',
    direction: 'asc',
    __typename: 'sortItem',
  },
  {
    label: 'Date fin',
    name: 'dateFin',
    direction: 'asc',
    __typename: 'sortItem',
  },
  {
    label: "Date d'ajout",
    name: 'dateModification',
    active: true,
    direction: 'desc',
    __typename: 'sortItem',
  },
  {
    label: 'Nombre de pharmacies qui ont consultées',
    name: 'nombrePharmaciesConsultes',
    direction: 'asc',
    __typename: 'sortItem',
  },
  {
    label: 'Nombre de pharmacies qui ont commandées',
    name: 'nombrePharmaciesCommandees',
    direction: 'asc',
    __typename: 'sortItem',
  },
  {
    label: 'Nombre de ligne de produits commandés en total',
    name: 'nombreProduitsCommandes',
    direction: 'asc',
    __typename: 'sortItem',
  },
  {
    label: 'Nombre de ligne de produits commandé en moyenne par pharmacie',
    name: 'nombreProduitsMoyenParPharmacie',
    direction: 'asc',
    __typename: 'sortItem',
  },
  {
    label: 'CA global de toutes les commandes passées',
    name: 'CATotal',
    direction: 'asc',
    __typename: 'sortItem',
  },
  {
    label: 'CA moyen des commandes passées par pharmacie',
    name: 'CAMoyenParPharmacie',
    direction: 'asc',
    __typename: 'sortItem',
  },
];

export const actualiteSortParam = [
  {
    label: 'Libellé',
    name: 'libelle',
    direction: 'asc',
    __typename: 'sortItem',
  },
  {
    label: 'Nom Laboratoire',
    name: 'laboratoire.nomLabo',
    direction: 'asc',
    __typename: 'sortItem',
  },
  {
    label: 'Date début',
    name: 'dateDebut',
    direction: 'asc',
    __typename: 'sortItem',
  },
  {
    label: 'Date fin',
    name: 'dateFin',
    direction: 'asc',
    __typename: 'sortItem',
  },
  {
    label: "Date d'ajout",
    name: 'dateCreation',
    direction: 'desc',
    __typename: 'sortItem',
  },
  {
    label: 'Date de modification',
    name: 'dateModification',
    active: true,
    direction: 'desc',
    __typename: 'sortItem',
  },
  {
    label: 'Nombre de smyleys',
    name: 'userSmyleys.total',
    direction: 'asc',
    __typename: 'sortItem',
  },
  {
    label: 'Nombre de commentaires',
    name: 'comments.total',
    direction: 'asc',
    __typename: 'sortItem',
  },
];

export const laboratoireSortParam = [
  {
    label: 'Date modification',
    name: 'dateModification',
    direction: 'desc',
    active: true,
    __typename: 'sortItem',
  },
  {
    label: 'Nom',
    name: 'nomLabo',
    direction: 'desc',
    active: false,
    __typename: 'sortItem',
  },
];

export const projectSortParam = [
  {
    label: 'Type',
    name: 'typeProject',
    direction: 'desc',
    active: true,
    __typename: 'sortItem',
  },
  {
    label: 'Nom',
    name: 'name',
    direction: 'desc',
    __typename: 'sortItem',
  },
];

export const groupeClientSortParam = [
  {
    label: 'Code',
    name: 'codeGroupe',
    direction: 'desc',
    active: true,
    __typename: 'sortItem',
  },
  {
    label: 'Nom',
    name: 'nomGroupe',
    direction: 'desc',
    __typename: 'sortItem',
  },
  {
    label: 'Nom Commercial',
    name: 'nomCommercial',
    direction: 'desc',
    __typename: 'sortItem',
  },
  {
    label: 'Date de validité',
    name: 'dateValidite',
    direction: 'desc',
    __typename: 'sortItem',
  },
];
