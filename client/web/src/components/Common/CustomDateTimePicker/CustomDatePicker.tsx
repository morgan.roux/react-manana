import React, { FC } from 'react';
import useStyles from './styles';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  KeyboardDatePickerProps,
} from '@material-ui/pickers';
import frLocale from 'date-fns/locale/fr';
import classnames from 'classnames';

const CustomDatePicker: FC<KeyboardDatePickerProps> = ({
  value,
  name,
  label,
  disableToolbar = true,
  onChange,
  disablePast,
  disableFuture,
  disabled,
  required,
  className,
  variant,
  format,
  margin,
  id,
  fullWidth = true,
  invalidDateMessage,
  placeholder,
  inputVariant,
  autoOk = true,
  minDate,
  maxDate,
  InputLabelProps,
  minDateMessage,
  maxDateMessage,
  cancelLabel = 'Annuler',
  okLabel = 'Valider',
  clearable,
}) => {
  const classes = useStyles({});

  return (
    <MuiPickersUtilsProvider locale={frLocale} utils={DateFnsUtils}>
      <KeyboardDatePicker
        className={classnames(classes.customDatePickerRoot, className)}
        disableToolbar={disableToolbar}
        variant={variant || 'inline'}
        format={format || 'dd/MM/yyyy'}
        margin={margin || 'none'}
        id={id}
        label={label}
        value={value}
        onChange={onChange}
        KeyboardButtonProps={{
          'aria-label': 'change date',
        }}
        name={name}
        fullWidth={fullWidth}
        minDateMessage={minDateMessage || 'La date ne doit pas être antérieure à la date minimale'}
        maxDateMessage={maxDateMessage || 'La date ne doit pas être postérieure à la date maximale'}
        invalidDateMessage={invalidDateMessage || 'Format de date non valide'}
        placeholder={placeholder}
        inputVariant={inputVariant || 'outlined'}
        autoOk={autoOk}
        disablePast={disablePast}
        disableFuture={disableFuture}
        disabled={disabled}
        required={required}
        minDate={minDate}
        maxDate={maxDate}
        InputLabelProps={InputLabelProps}
        cancelLabel={cancelLabel}
        okLabel={okLabel}
        // tslint:disable-next-line: jsx-no-lambda
        onClick={event => {
          event.preventDefault();
          event.stopPropagation();
        }}
        clearable={clearable}
      />
    </MuiPickersUtilsProvider>
  );
};

export default CustomDatePicker;
