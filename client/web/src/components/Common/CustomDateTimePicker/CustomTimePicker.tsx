import React, { FC } from 'react';
import useStyles from './styles';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePickerProps,
  KeyboardTimePicker,
} from '@material-ui/pickers';
import classnames from 'classnames';
import { Schedule } from '@material-ui/icons';

const CustomTimePicker: FC<KeyboardTimePickerProps> = ({
  value,
  name,
  label,
  onChange,
  disabled,
  required,
  className,
  margin,
  variant,
  id,
  fullWidth = true,
  invalidDateMessage,
  inputVariant,
  autoOk = false,
  keyboardIcon,
  disableToolbar = false,
  cancelLabel,
  placeholder,
  ampm = false,
}) => {
  const classes = useStyles({});

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <KeyboardTimePicker
        className={classnames(classes.customDatePickerRoot, className)}
        variant={variant || 'dialog'}
        margin={margin || 'none'}
        id={id}
        label={label}
        value={value}
        onChange={onChange}
        KeyboardButtonProps={{
          'aria-label': 'change date',
        }}
        name={name}
        fullWidth={fullWidth}
        invalidDateMessage={invalidDateMessage || "Format d'heure non valide"}
        placeholder={placeholder || 'Heure'}
        inputVariant={inputVariant || 'outlined'}
        autoOk={autoOk}
        disabled={disabled}
        required={required}
        keyboardIcon={keyboardIcon || <Schedule />}
        disableToolbar={disableToolbar}
        cancelLabel={cancelLabel || 'Annuler'}
        ampm={ampm}
      />
    </MuiPickersUtilsProvider>
  );
};

export default CustomTimePicker;
