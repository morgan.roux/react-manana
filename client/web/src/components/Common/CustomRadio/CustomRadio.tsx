import React, { FC, useState } from 'react';
import {
  Box,
  Checkbox,
  ListItem,
  ListItemText,
  IconButton,
  Typography,
  Radio,
  FormControl,
  InputLabel,
} from '@material-ui/core';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import useStyles from './styles';
import classnames from 'classnames';

interface FilterContentProps {
  title: string;
  name: string;
  datas: any;
  required: boolean;
  onChange: (list: any) => void;
  useRadios?: boolean;
  counts?: any[];
  shrink?: any;
  variant?: any;
  disabled?: boolean;
  value: boolean;
}

const CustomRadio: FC<FilterContentProps> = ({
  title,
  datas,
  name,
  onChange,
  useRadios,
  counts,
  required,
  variant,
  shrink,
  disabled,
  value,
}) => {
  const classes = useStyles({});
  const [expandedMore, setExpandedMore] = useState<boolean>(true);
  const [dataCheck, setDataCheck] = useState<any[]>();
  const onGetRadioValue = (itemIndex: number) => {
    const newList = datas.map((item: any, index) => {
      if (itemIndex === index) {
        item.checked = true;
        return item;
      } else {
        item.checked = false;
        return item;
      }
    });
    setDataCheck(newList);
    onChange(newList);
  };

  console.log('dataCheck', dataCheck);
  return (
    <FormControl
      required={required}
      className={!title ? classnames(classes.formControl, classes.noLabel) : classes.formControl}
      variant={variant}
      disabled={disabled}
    >
      <Typography className={classes.fontSize14}>{title}</Typography>

      {dataCheck && dataCheck.length !== 0
        ? dataCheck.map(
            (item: any, index) =>
              item && (
                <ListItem
                  role={undefined}
                  dense={true}
                  button={true}
                  key={`${index}-${item.nom}`}
                  onClick={() => onGetRadioValue(index)}
                >
                  <Box display="flex" alignItems="center">
                    <Radio
                      checked={value ? value : item.checked}
                      name={name}
                      tabIndex={-1}
                      disableRipple={true}
                    />
                    <ListItemText primary={item.nom} />
                  </Box>
                </ListItem>
              ),
          )
        : datas.map(
            (item: any, index) =>
              item && (
                <ListItem
                  role={undefined}
                  dense={true}
                  button={true}
                  key={`${index}-${item.nom}`}
                  onClick={() => onGetRadioValue(index)}
                >
                  <Box display="flex" alignItems="center">
                    <Radio checked={item.checked} name={name} tabIndex={-1} disableRipple={true} />
                    <ListItemText primary={item.nom} />
                  </Box>
                </ListItem>
              ),
          )}
    </FormControl>
  );
};

export default CustomRadio;
