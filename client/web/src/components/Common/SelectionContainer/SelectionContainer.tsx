import React, { FC, useState, useEffect, ReactNode } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import useStyles from './styles';
import { Box } from '@material-ui/core';
// import RadioButton from './RadioButton';
// import SearchContent from '../Filter/SearchContent';
// import Pagination from '../../../components/Main/Content/Produits/Pagination/Pagination';
import { LoaderSmall } from '../../../components/Dashboard/Content/Loader';
import { GET_MINIMAL_SEARCH } from '../../../graphql/search/query';
import {
  MINIMAL_SEARCH,
  MINIMAL_SEARCHVariables,
} from '../../../graphql/search/types/MINIMAL_SEARCH';
import { useQuery } from '@apollo/react-hooks';
import AucunResultat from '../AucunResultat';
import CustomPharmacie from '../CustomPharmacie';
import CustomContent from '../CustomContent';
import { getGroupement } from '../../../services/LocalStorage';

interface PropsInterface {
  searchPlaceholder: string;
  columns: any[];
  checkedItems: string[];
  checkedAll: boolean;
  content?: ReactNode;
  type: string;
  handleCheckItem?: (value: any) => void;
  handleCheckAll?: () => void;
  handleUncheckAll?: (event: any) => void;
}
interface SelectionContainerProps {
  props?: PropsInterface;
  listResult: any;
  updateTake: (take: number) => void;
  updateSkip: (skip: number) => void;
}
const SelectionContainer: FC<SelectionContainerProps & RouteComponentProps<any, any, any>> = ({
  props,
  listResult,
  updateSkip,
  updateTake,
}) => {
  const classes = useStyles({});
  const groupement = getGroupement();

  const [take, setTake] = useState<number>(12);
  const [skip, setSkip] = useState<number>(0);

  const [checkedAll, setCheckedAll] = useState<boolean>();
  const [minimalQuery, setMinimalQuery] = useState<any>();
  const [minimalTake, setMinimalTake] = useState<number>();
  const [minimalType, setMinimalType] = useState<string[]>();

  const listMinimalSearch = useQuery<MINIMAL_SEARCH, MINIMAL_SEARCHVariables>(GET_MINIMAL_SEARCH, {
    variables: {
      type: minimalType,
      take: minimalTake,
      query: minimalQuery,
    },
    // fetchPolicy: 'cache-and-network',
  });

  const onTakeChanged = (value: number) => {
    setTake(value);
    updateTake(value);
    setSkip(0);
    updateSkip(0);
  };

  const onSkipChanged = (value: number) => {
    setSkip(value);
    updateSkip(value);
  };

  const getFilter = (type: string): any => {
    return type === 'titulaire'
      ? {
          must: [{ term: { estPresidentRegion: true } }, { term: { idGroupement: groupement.id } }],
        }
      : type === 'pharmacie' || type === 'partenaire'
      ? { must: [{ term: { idGroupement: groupement.id } }] }
      : null;
  };

  useEffect(() => {
    if (
      listResult &&
      listResult.data &&
      listResult.data.search &&
      listResult.data.search.data &&
      listResult.variables &&
      listResult.variables.query &&
      listResult.variables.type &&
      listResult.variables.take &&
      listResult.data.search.total &&
      !listResult.loading &&
      props
    ) {
      setMinimalQuery(listResult.variables.query);
      setMinimalType(listResult.variables.type);
      setMinimalTake(listResult.data.search.total);

      const idList = listResult.data.search.data.map((item: any, index: number) => item.id);
      if (
        props &&
        props.checkedItems &&
        props.checkedItems.map((item: any) => item.id).filter(value => idList.includes(value))
          .length === idList.length
      ) {
        setCheckedAll(true);
      } else {
        setCheckedAll(false);
      }
    }
  }, [listResult, props]);

  return (
    <Box className={classes.rootSelectionContainer}>
      {listResult && listResult.loading ? (
        <Box display="flex" justifyContent="center" marginTop="70px" marginBottom="70px">
          <LoaderSmall />
        </Box>
      ) : (
        props &&
        props.columns && (
          <Box width="auto" display="flex" flexDirection="column">
            {listResult &&
            listResult.data &&
            listResult.data.search &&
            listResult.data.search.data &&
            listResult.data.search.data.length > 0 ? (
              props.type === 'pharmacie' ? (
                <CustomPharmacie
                  selectable={true}
                  inSelectContainer={true}
                  handleCheckItem={props.handleCheckItem}
                  handleCheckAll={props.handleCheckAll}
                  handleUncheckAll={props.handleUncheckAll}
                  checkedItems={props.checkedItems}
                  paginationCentered={true}
                >
                  {props && props.content}
                </CustomPharmacie>
              ) : (
                <CustomContent
                  searchPlaceholder={`Rechercher`}
                  type={props.type}
                  columns={props.columns}
                  filterBy={getFilter(props.type)}
                  sortBy={[{ nom: { order: 'desc' } }]}
                  // searchInputs={activeSearchFields}
                  enableGlobalSearch={true}
                  isSelectable={true}
                  checkedItems={props.checkedItems}
                  checkedAll={checkedAll || false}
                  handleCheckItem={props.handleCheckItem}
                  handleCheckAll={props.handleCheckAll}
                  handleUncheckAll={props.handleUncheckAll}
                  inSelectContainer={true}
                  searchInFullWidth={true}
                  noPrefixIcon={true}
                  paginationCentered={true}
                  datas={listResult.data.search.data}
                />
              )
            ) : (
              <Box display="flex" justifyContent="center" marginTop="70px" marginBottom="70px">
                <AucunResultat />
              </Box>
            )}
          </Box>
        )
      )}
    </Box>
  );
};

export default withRouter(SelectionContainer);
