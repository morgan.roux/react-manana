import React, { FC, Fragment, useState } from 'react';
import { RouteComponentProps, withRouter, Redirect } from 'react-router-dom';
import useStyles from './styles';
import { Radio, FormControlLabel } from '@material-ui/core';

interface RadioButtonProps {
  setSelection: (value: string) => void;
  active: boolean;
  value: string;
  label: string;
}
const RadioButton: FC<RadioButtonProps & RouteComponentProps<any, any, any>> = ({
  setSelection,
  label,
  active,
  value,
}) => {
  const handleSelection = (value: string) => {
    setSelection(value);
  };
  return (
    <FormControlLabel
      value={value}
      control={<Radio checked={active} onChange={() => handleSelection(value)} />}
      label={label}
    />
  );
};

export default withRouter(RadioButton);
