import React, { FC, useState, useEffect } from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import ListIcon from '@material-ui/icons/List';
import ListAltIcon from '@material-ui/icons/ListAlt';
import Box from '@material-ui/core/Box/Box';
import clsx from 'clsx';
import { useToolbarStyles } from './styles';
import { Typography, Button } from '@material-ui/core';
import ReplayIcon from '@material-ui/icons/Replay';
import { TableToolbarProps } from '../interfaces';
import { useApolloClient } from '@apollo/react-hooks';
import { resetSearchFilters } from '../../withSearch/withSearch';
import { SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT } from '../../../../Constant/roles';
import { ME_me } from '../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../services/LocalStorage';
import { withRouter, RouteComponentProps } from 'react-router-dom';

const EnhancedTableToolbar: FC<TableToolbarProps & RouteComponentProps> = ({
  total,
  isSelected,
  setIsSelected,
  numSelected,
  handleSelection,
  handleUncheckAll,
  count,
  location,
  showResetFilters,
}) => {
  const classes = useToolbarStyles();
  const client = useApolloClient();
  const currentUser: ME_me = getUser();
  const isAdmin =
    currentUser &&
    currentUser.role &&
    (currentUser.role.code === SUPER_ADMINISTRATEUR ||
      currentUser.role.code === ADMINISTRATEUR_GROUPEMENT);

  const isActualite: boolean =
    window.location.hash === '#/' || window.location.hash.startsWith('#/actualite');
  const isOperation: boolean = window.location.hash.startsWith('#/operations-commerciales');
  const dataType: string = isActualite ? 'actualite' : isOperation ? 'operation' : '';

  const handleResetFilter = () => {
    resetSearchFilters(client, isAdmin, dataType);
  };
  useEffect(() => {
    if (numSelected === 0 && setIsSelected) setIsSelected(false);
  }, [numSelected]);

  const clearSelection = () => {
    if (setIsSelected) setIsSelected(false);
    resetSearchFilters(client);
    if (handleUncheckAll) {
      handleUncheckAll(null);
    } else if (handleSelection) {
      handleSelection([]);
    }
  };

  const filterSelectedItems = () => {
    const status = !isSelected;
    if (setIsSelected) setIsSelected(!isSelected);
  };

  return (
    <div
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      <div className={classes.title}>
        {numSelected > 0 && (
          <Typography color="inherit" variant="subtitle1">
            {`${numSelected}/${total}`} {numSelected > 1 ? 'sélectionnés' : 'sélectionné'}{' '}
            {`(${count})`}
          </Typography>
        )}
      </div>
      <div className={classes.spacer} />
      <div className={classes.actions}>
        <Box className={classes.buttons}>
          {showResetFilters && (
            <IconButton size="small" onClick={handleResetFilter}>
              <Tooltip title="Réinitialiser les filtres">
                <ReplayIcon />
              </Tooltip>
            </IconButton>
          )}
          {numSelected > 0 && (
            <>
              <Tooltip title="Filtrer les éléments selectionnés">
                <IconButton aria-label="Filtrer" onClick={() => filterSelectedItems()}>
                  {isSelected ? <ListIcon /> : <ListAltIcon />}
                </IconButton>
              </Tooltip>
              <Tooltip title="Vider la sélection">
                <IconButton aria-label="Supprimer" onClick={() => clearSelection()}>
                  <DeleteIcon />
                </IconButton>
              </Tooltip>
            </>
          )}
        </Box>
      </div>
    </div>
  );
};

export default withRouter(EnhancedTableToolbar);
