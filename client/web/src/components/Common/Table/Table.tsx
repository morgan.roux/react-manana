import React, { FC, useEffect, useState } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';
import TableToolbar from './TableToolbar';
import TableHead from './TableHead';
import { TableProps, ItemSelected } from './interfaces';
import TableContainer from '@material-ui/core/TableContainer';
import { useStyles } from './styles';
import { Tooltip, IconButton } from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import { ConfirmDeleteDialog } from '../ConfirmDialog';
import { ProduitCanal } from '../../../graphql/ProduitCanal/types/ProduitCanal';
import { useMutation, useApolloClient } from '@apollo/react-hooks';
import { DELETE_CANAL_ARTICLE } from '../../../graphql/ProduitCanal/mutation';
import { SEARCHVariables, SEARCH as SEARCH_INTERFACE } from '../../../graphql/search/types/SEARCH';
import { SEARCH, GET_CUSTOM_CONTENT_SEARCH } from '../../../graphql/search/query';
import SnackVariableInterface from '../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import {
  softDeleteCanalArticle,
  softDeleteCanalArticleVariables,
} from '../../../graphql/ProduitCanal/types/softDeleteCanalArticle';

const EnhancedTable: FC<TableProps> = ({
  isSelectable,
  selected,
  handleSelection,
  handleCheckItem,
  handleCheckAll,
  handleUncheckAll,
  columns,
  columnSorted,
  order,
  count,
  data,
  page,
  onChangePage,
  rowsPerPage,
  onChangeRowsPerPage,
  rowsPerPageOptions,
  toggleSorting,
  densePadding,
  paginationCentered,
  total,
  hideRightPagination,
  showResetFilters,
  onClickRow,
  activeRow,
  isDeletable,
  type,
  variables,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [dense, setDense] = useState(false);
  const [isFilter, setIsFilter] = useState(selected && selected.length ? true : false);
  const [openConfirmDialog, setOpenConfirmDialog] = useState(false);
  const [articleToDelete, setArticleToDelete]: any = useState();
  const isInOperationCommande = window.location.hash.includes('#/operation-produits');

  useEffect(() => {
    setDense(densePadding);
  }, [densePadding]);

  const searchIndex = (item: ItemSelected) => {
    if (isSelectable && selected && handleSelection) {
      const ids = selected && selected.map(item => item && item.id);
      const selectedIndex = ids && ids.indexOf(item.id);
      return selectedIndex;
    }
    return -1;
  };

  const handleClick = (event: React.MouseEvent<HTMLTableRowElement, MouseEvent>, item: any) => {
    if (isSelectable && handleCheckItem) {
      handleCheckItem(item);
    } else if (isSelectable && selected && handleSelection) {
      const selectedIndex = searchIndex(item);
      let newSelected: any[] = [];

      if (selectedIndex === -1) {
        newSelected = newSelected.concat(selected, item);
      } else if (selectedIndex === 0) {
        newSelected = newSelected.concat(selected.slice(1));
      } else if (selectedIndex === selected.length - 1) {
        newSelected = newSelected.concat(selected.slice(0, -1));
      } else if (selectedIndex > 0) {
        newSelected = newSelected.concat(
          selected.slice(0, selectedIndex),
          selected.slice(selectedIndex + 1),
        );
      }

      handleSelection(newSelected);
    }
  };

  const handleChangeDense = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDense(event.target.checked);
  };

  const isSelected = (item: ItemSelected) => {
    if (!(isSelectable && selected && handleSelection)) return false;

    return searchIndex(item) !== -1;
  };

  const selectionLength: number =
    isSelectable && selected && handleSelection ? selected.length : -1;

  const handleClickRow = (row: any) => (
    event: React.MouseEvent<HTMLTableRowElement, MouseEvent>,
  ) => {
    event.preventDefault();
    event.stopPropagation();
    if (onClickRow) {
      onClickRow(row);
    }
  };

  console.log('activeRow :>> ', activeRow);

  const handleDelete = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    canalArticle: ProduitCanal,
  ) => {
    e.stopPropagation();
    if (canalArticle && canalArticle.id) {
      doDeleteArticle({ variables: { id: canalArticle.id } });
    }
  };

  const handleOpenConfirmDialog = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    article: any,
  ) => {
    e.stopPropagation();
    setArticleToDelete(article);
    setOpenConfirmDialog(true);
  };

  const controlColumns = [
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <>
            <Tooltip title="Supprimer">
              <IconButton onClick={e => handleOpenConfirmDialog(e, value)}>
                <Delete />
              </IconButton>
            </Tooltip>
          </>
        );
      },
    },
  ];

  const finalColumns: any = columns && isDeletable ? [...columns, ...controlColumns] : columns;

  const [doDeleteArticle, doDeleteArticleResult] = useMutation<
    softDeleteCanalArticle,
    softDeleteCanalArticleVariables
  >(DELETE_CANAL_ARTICLE, {
    update: (cache, { data }) => {
      if (variables && data && data.softDeleteCanalArticle && data.softDeleteCanalArticle.id) {
        const req = cache.readQuery<SEARCH_INTERFACE, SEARCHVariables>({
          query: GET_CUSTOM_CONTENT_SEARCH,
          variables: variables,
        });

        if (req && req.search && req.search.data) {
          console.log('req : ', req);
          cache.writeQuery({
            query: SEARCH,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.filter(
                    (item: any) =>
                      item &&
                      item.id &&
                      data.softDeleteCanalArticle &&
                      item.id !== data.softDeleteCanalArticle.id,
                  ),
                },
              },
            },
            variables: variables,
          });
        }
      }
    },
    onCompleted: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'SUCCESS',
        message: 'Article supprimé avec succès',
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
      setOpenConfirmDialog(false);
    },
  });

  return (
    <div className={classes.root}>
      <TableToolbar
        total={isFilter && selected ? selected.length : total}
        numSelected={total && total < selectionLength ? total : selectionLength}
        handleSelection={handleSelection}
        handleUncheckAll={handleUncheckAll}
        isSelected={isFilter}
        setIsSelected={setIsFilter}
        count={isFilter && selected ? selected.length : count}
        showResetFilters={showResetFilters}
      />
      <TableContainer className={classes.tableWrapper}>
        <Table
          className={classes.table}
          aria-labelledby="tableTitle"
          size={dense ? 'small' : 'medium'}
          stickyHeader={true}
        >
          <TableHead
            selected={selected}
            data={isFilter ? selected || [] : data}
            numSelected={selectionLength}
            order={order}
            orderBy={columnSorted}
            onSelectAllClick={handleCheckAll}
            onUncheckAll={handleUncheckAll}
            rowCount={data && data.length}
            isSelectable={isSelectable}
            columns={finalColumns}
            columnSorted={columnSorted}
            toggleSorting={toggleSorting}
            onChangePage={onChangePage}
          />
          <TableBody>
            {(isFilter ? selected || [] : data).map((row: any, indexInArray: any) => {
              if (row) {
                const index = row.id || indexInArray;
                const itemDesc: ItemSelected = { ...row };
                const isItemSelected = isSelected(itemDesc);
                const labelId = `enhanced-table-checkbox-${index}`;

                const selectSection = isSelectable ? (
                  <TableCell padding="checkbox" key={indexInArray}>
                    <Checkbox
                      onClick={(event: any) => handleClick(event, itemDesc)}
                      checked={isItemSelected}
                      inputProps={{ 'aria-labelledby': labelId }}
                    />
                  </TableCell>
                ) : null;
                // Check active row from pilotage dashboard (pharmacie)
                return (
                  <TableRow
                    hover={true}
                    onClick={handleClickRow(row)}
                    role="checkbox"
                    aria-checked={isItemSelected}
                    tabIndex={-1}
                    key={`row-${index}`}
                    selected={isItemSelected}
                    style={{ cursor: onClickRow ? 'pointer' : 'default' }}
                  >
                    {selectSection}

                    {finalColumns
                      ? finalColumns.map((column, columnIndex) => {
                          return (
                            <TableCell
                              key={`row-${index}-${columnIndex}`}
                              className={classes.tableCellPadding}
                              align={column.centered ? 'center' : 'left'}
                            >
                              {column.renderer
                                ? column.renderer(row)
                                : row[column.name]
                                ? row[column.name]
                                : '-'}
                            </TableCell>
                          );
                        })
                      : null}
                  </TableRow>
                );
              }
            })}
            <ConfirmDeleteDialog
              title={'Suppression'}
              content="Êtes vous sur de vouloir supprimer ce produit"
              open={openConfirmDialog}
              setOpen={setOpenConfirmDialog}
              onClickConfirm={e => handleDelete(e, articleToDelete)}
            />
          </TableBody>
        </Table>
      </TableContainer>
      {!hideRightPagination && !isInOperationCommande && (
        <TablePagination
          className={paginationCentered ? classes.tablePagination : ''}
          rowsPerPageOptions={rowsPerPageOptions}
          labelDisplayedRows={({ from, to, count }) =>
            `${from}-${to === -1 ? count : to} sur ${count !== -1 ? count : to}`
          }
          labelRowsPerPage="Nombre par page"
          component="div"
          count={count}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Page Précédente',
          }}
          nextIconButtonProps={{
            'aria-label': 'Page suivante',
          }}
          onChangePage={(_, newPage) => onChangePage(newPage)}
          onChangeRowsPerPage={e => onChangeRowsPerPage(parseInt(e.target.value, 10))}
        />
      )}
    </div>
  );
};

export default EnhancedTable;
