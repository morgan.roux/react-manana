import React, { FC } from 'react';
import { withStyles, createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import classnames from 'classnames';

const styles = (theme: Theme) =>
  createStyles({
    formControl: {
      marginBottom: 16,
      '& .MuiSelect-selectMenu': {
        minHeight: '30px !important',
        display: 'flex',
        alignItems: 'center',
        [theme.breakpoints.down('sm')]: {
        //  minHeight: 'unset !important',
        },
      },
      '& label': {
        '& .MuiFormLabel-asterisk': {
          color: 'red',
        },
      },
      '&:hover label, & label.Mui-focused': {
        color: theme.palette.primary.main,
      },
      '&.Mui-focused fieldset': {
        border: `2px solid ${theme.palette.primary.main} !important`,
      },
      '& input:invalid + fieldset': {
        borderColor: 'red',
      },
      '& .Mui-focused .MuiIconButton-label svg': {
        color: `${theme.palette.primary.main} !important`,
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: theme.palette.common.white,
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          // borderColor: theme.palette.common.black,
        },
        '&:hover fieldset': {
          borderColor: theme.palette.primary.main,
        },
        '&.Mui-focused fieldset': {
          borderColor: theme.palette.primary.main,
        },
      },
      '& .MuiInputBase-root': {
        [theme.breakpoints.down('sm')]: {
          fontSize: 12,
          fontFamily: 'Roboto',
        },
      },
      transition:
        'padding-left 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms,border-color 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms,border-width 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms',
    },
    label: {
      background: theme.palette.common.white,
      // color: theme.palette.common.black,
      padding: '0 4px',
    },
    noLabel: {
      '& fieldset > legend > span': {
        display: 'none',
      },
    },
    noMinHeight: {
      '& .MuiSelect-selectMenu': {
        minHeight: '30px !important',
        height: 30,
        display: 'flex',
        alignItems: 'center',
      },
    },
  });

const CustomSelect: FC<any> = ({ ...props }) => {
  const {
    classes,
    label,
    index,
    listId,
    list,
    error,
    variant,
    shrink,
    required,
    withNoneValue,
    placeholder,
    withPlaceholder,
    disabled,
    className,
    value,
    color,
    noMinheight,
    colorIndex,
    fullWidth,
  } = props;

  const selectRootClasses = makeStyles({ select: { color: color || '#000' } })();
  return (
    <FormControl
      required={required}
      className={
        !noMinheight
          ? !label
            ? classnames(classes.formControl, classes.noLabel, className)
            : classnames(classes.formControl, className)
          : classnames(classes.noMinHeight, className)
      }
      variant={variant}
      error={error}
      disabled={disabled}
      fullWidth={fullWidth !== undefined ? fullWidth : true}
    >
      <InputLabel shrink={shrink} htmlFor="name" className={classes.label}>
        {label}
      </InputLabel>
      <Select
        {...props}
        className={classes.select}
        MenuProps={{
          getContentAnchorEl: null,
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'left',
          },
        }}
        label={label}
        classes={{ select: selectRootClasses.select }}
      >
        {withNoneValue && (
          <MenuItem value="" disabled>
            <em>Aucun</em>
          </MenuItem>
        )}
        {withPlaceholder && (
          <MenuItem value="" disabled={true} selected={true}>
            {placeholder}
          </MenuItem>
        )}
        {list &&
          list.map((item: any, key: number) => {
            const val = typeof item === 'object' ? item[listId] : item;
            const show = typeof item === 'object' ? item[index] : item;
            if (val && show) {
              return (
                <MenuItem
                  key={`custom_select_menu_item_${key}`}
                  value={val}
                  className={classes.menuItem}
                  style={{ color: item[colorIndex] || '#000' }}
                >
                  {show}
                </MenuItem>
              );
            }
          })}
      </Select>
    </FormControl>
  );
};

export default withStyles(styles, { withTheme: true })(CustomSelect);

CustomSelect.defaultProps = {
  list: [],
  label: '',
  variant: 'outlined',
  error: false,
  shrink: true,
  required: false,
  withNoneValue: false,
  withPlaceholder: false,
  disabled: false,
  noMinheight: false,
};
