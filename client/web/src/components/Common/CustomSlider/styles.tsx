import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    customSliderRoot: {
      // minHeight: 280,
      // height: 450,
      // padding: '50px 100px',
      '& .slick-prev': {
        left: -35,
      },
      '& .slick-next': {
        right: -35,
      },
      '& .slick-prev:before, & .slick-next:before': {
        color: theme.palette.common.black,
      },
      '& .slick-prev, & .slick-next': {
        color: theme.palette.common.black,
        borderRadius: '50%',
      },
      '& .slick-prev:hover, & .slick-next:hover': {
        // background: '#F5F6FA 0% 0% no-repeat padding-box',
        background: theme.palette.common.black,
        color: theme.palette.common.white,
      },
      '& .slick-initialized .slick-slide': {
        display: 'flex !important',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
      },
      '& .slick-slide div': {
        width: '100%',
      },
      '& .slick-dots li button:before': {
        content: '""',
      },
      '& .slick-dots li': {
        background: '#E0E0E0 0% 0% no-repeat padding-box',
        width: 6,
        height: 6,
      },
      '& li.slick-active, & .slick-dots li:hover': {
        background: '#616161 0% 0% no-repeat padding-box',
        width: 10,
        height: 10,
      },
      '& ul.slick-dots': {
        bottom: -25,
        display: 'flex !important',
        alignItems: 'center',
        justifyContent: 'center',
      },
      '& img': {
        objectFit: 'cover',
        width: '100%',
        height: '100%',
      },
    },
    imgClickable: {
      '& img ': {
        cursor: 'pointer !important',
      },
    },
    itemPromotion: {
      maxHeight: 307,
      padding: 16,
    },
    contentItemImg: {
      padding: '19px 13px',
      '-webkit-box-shadow': '0px 2px 12px 0px rgba(20,20,20,0.16)',
      '-moz-box-shadow': '0px 2px 12px 0px rgba(20,20,20,0.16)',
      boxShadow: '0px 2px 12px 0px rgba(20,20,20,0.16)',
      borderRadius: 6,
    },
  }),
);

export default useStyles;
