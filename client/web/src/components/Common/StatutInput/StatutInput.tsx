import { useQuery } from '@apollo/react-hooks';
import React, { FC } from 'react';
import { GET_STATUTS } from '../../../federation/demarche-qualite/statut/query';
import {
  GET_STATUTS as GET_STATUTS_TYPE,
  GET_STATUTSVariables,
} from '../../../federation/demarche-qualite/statut/types/GET_STATUTS';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import CustomSelect from '../CustomSelect';
import { useStyles } from './styles';

interface StatutInputProps {
  name?: string;
  filterType: string;
  value: any;
  handleChange: any;
}
const StatusInput: FC<StatutInputProps> = ({ name, handleChange, value, filterType }) => {
  const classes = useStyles({});
  const statutsQuery = useQuery<GET_STATUTS_TYPE, GET_STATUTSVariables>(GET_STATUTS, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const statuts = statutsQuery.data?.dQStatuts?.nodes.filter(statut => statut.type === filterType);
  return (
    <CustomSelect
      key={value?.id}
      disabled={statutsQuery.loading || statutsQuery.error}
      list={statuts || []}
      listId="id"
      index="libelle"
      value={value?.id}
      variant="standard"
      //onChange={(e: any) => changeStatut(e, row)}
      onChange={handleChange}
      fullWidth={false}
    />
  );
};

export default StatusInput;
