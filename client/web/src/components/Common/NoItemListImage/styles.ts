import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    img: {
      height: '20%',
      marginBottom: '24px',
    },
    title: {
      fontSize: '20px',
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      color: '#616161',
      textAlign: 'center',
    },
    subtitle: {
      fontSize: '16px',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      color: '#9E9E9E',
      textAlign: 'center',
    },
  }),
);

export default useStyles;
