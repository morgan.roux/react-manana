import React, { FC } from 'react';
import { Box, Typography } from '@material-ui/core';
import useStyles from './styles';
import Image from '../../../assets/img/img_no_list_item2.png';

interface NoItemListImageProps {
  title?: string;
  subtitle?: string;
  src?: string;
}

const NoItemListImage: FC<NoItemListImageProps> = ({ title, subtitle, src }) => {
  const classes = useStyles({});
  return (
    <Box
      width="100%"
      height="100%"
      display="flex"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      padding="0px 20px"
    >
      <img src={src || Image} className={classes.img} />
      <Typography className={classes.title}>{title}</Typography>
      <Typography className={classes.subtitle}>{subtitle}</Typography>
    </Box>
  );
};

export default NoItemListImage;
