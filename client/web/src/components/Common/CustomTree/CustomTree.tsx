import React, { FC, useState, useMemo } from 'react';
import useStyles from './styles';
import { Checkbox, CircularProgress, Box, Typography } from '@material-ui/core';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { KeyboardArrowDown, KeyboardArrowRight, IndeterminateCheckBox } from '@material-ui/icons';
import { fade, withStyles, Theme, createStyles } from '@material-ui/core/styles';
import TreeView from '@material-ui/lab/TreeView';
import TreeItem, { TreeItemProps } from '@material-ui/lab/TreeItem';
import Collapse from '@material-ui/core/Collapse';
import { useSpring, animated } from 'react-spring/web.cjs'; // web.cjs is required for IE 11 support
import { TransitionProps } from '@material-ui/core/transitions';
import { useQuery } from '@apollo/react-hooks';
import { SEARCH as SEARCH_Interface, SEARCHVariables } from '../../../graphql/search/types/SEARCH';
import { SEARCH } from '../../../graphql/search/query';
import { useEffect } from 'react';
interface CustomTreeProps {
  type: string;
  nodeIndex?: string;
  nodeKey: string;
  nodeLabel: string;
  nodeParent: string;
  multiple: boolean;
  setChecked: (value: any) => void;
  onlyLastChild?: boolean;
  checked: any | any[];
  disabled?: boolean;
  formatedData?: any;
  expendAll?: boolean;
}

const CustomTree: FC<CustomTreeProps & RouteComponentProps> = ({
  type,
  nodeIndex,
  nodeKey,
  nodeLabel,
  nodeParent,
  multiple,
  setChecked,
  onlyLastChild,
  checked,
  disabled,
  formatedData,
  expendAll,
}) => {
  const classes = useStyles({});

  const query = nodeIndex
    ? { query: { bool: { must: [{ prefix: { [nodeKey]: nodeIndex } }] } } }
    : null;
  const searchResult = useQuery<SEARCH_Interface, SEARCHVariables>(SEARCH, {
    variables: { type: [type], query, take: 2000 },
    skip: formatedData ? true : false,
  });
  const data =
    formatedData ||
    (searchResult &&
      searchResult.data &&
      searchResult.data.search &&
      searchResult.data.search.data) ||
    [];

  const findChildren = (node: any) => {
    const children: any[] = [];
    if (data) {
      data.map(item => {
        if (
          item &&
          item[nodeKey] &&
          item[nodeKey].startsWith(node[nodeKey]) &&
          item[nodeKey].length === node[nodeKey].length + 1
        ) {
          children.push(item);
        }
      });
      return children;
    }
    return children;
  };

  const getTotal = (node: any) => {
    let total: number = 0;
    if (data) {
      data.map(item => {
        if (
          item &&
          item[nodeKey] &&
          item[nodeKey].startsWith(node[nodeKey]) &&
          item[nodeKey].length > node[nodeKey].length
        ) {
          total++;
        }
      });

      return total;
    }
    return total;
  };

  const getProfondeurMax = () => {
    let profondeurMax = 0;
    if (data) {
      data.map((item: any) => {
        if (item && item[nodeKey] && profondeurMax < item[nodeKey].length) {
          profondeurMax = item[nodeKey].length;
        }
      });
      return profondeurMax;
    }
    return profondeurMax;
  };

  const profondeurMax = getProfondeurMax();
  //console.log('profondeur : ', profondeurMax);

  function TransitionComponent(props: TransitionProps) {
    const style = useSpring({
      from: { opacity: 0, transform: 'translate3d(20px,0,0)' },
      to: { opacity: props.in ? 1 : 0, transform: `translate3d(${props.in ? 0 : 20}px,0,0)` },
    });

    return (
      <animated.div style={style}>
        <Collapse {...props} />
      </animated.div>
    );
  }

  const StyledTreeItem = withStyles((theme: Theme) =>
    createStyles({
      iconContainer: {
        '& .close': {
          opacity: 0.3,
        },
      },
      group: {
        marginLeft: 7,
        paddingLeft: 18,
        borderLeft: `1px dashed ${fade(theme.palette.text.primary, 0.4)}`,
      },
    }),
  )((props: TreeItemProps) => <TreeItem {...props} TransitionComponent={TransitionComponent} />);

  const [expanded, setExpanded] = useState<any[]>([]);

  useEffect(() => {
    if (
      searchResult &&
      searchResult.data &&
      searchResult.data.search &&
      searchResult.data.search.data &&
      searchResult.data.search.data.length &&
      expendAll
    ) {
      const toExpands =
        searchResult &&
        searchResult.data &&
        searchResult.data.search &&
        searchResult.data.search.data &&
        searchResult.data.search.data.map(item => item && item[nodeKey]);
      setExpanded(prevState => [...prevState, ...toExpands]);
    }
  }, [searchResult]);

  const handleExpand = (item: any) => {
    setExpanded(prevState => [...prevState, item[nodeKey]]);
    //getSousItem(item);
  };

  const handleCloseExpand = (item: any) => {
    setExpanded(prevState => prevState.filter(old => old !== item[nodeKey]));
  };

  const getAllChildren = (item: any) => {
    return (data && data.filter((x: any) => x[nodeKey].startsWith(item[nodeKey]))) || [];
  };

  const handleCheck = (item: any) => {
    if (multiple) {
      const children = getAllChildren(item);
      const value =
        checked && checked.find((x: any) => x[nodeKey] === item[nodeKey])
          ? checked.filter(
              (x: any) =>
                x[nodeKey] !== item[nodeKey] &&
                !children.map((child: any) => child[nodeKey]).includes(x[nodeKey]),
            )
          : [...checked, item, ...children];
      setChecked(value);
    } else {
      setChecked(
        checked && checked[nodeKey] && item && item[nodeKey] && checked[nodeKey] === item[nodeKey]
          ? null
          : item,
      );
    }
  };

  const isChecked = (item: any): boolean => {
    if (multiple) {
      return checked && checked.find((x: any) => x[nodeKey] === item[nodeKey]);
    } else {
      return checked && checked[nodeKey] === item[nodeKey];
    }
  };

  const isIndeterminate = (item: any): boolean => {
    if (multiple) {
      const checkedItem =
        checked &&
        checked.find(
          (x: any) =>
            x &&
            x[nodeKey] &&
            item &&
            item[nodeKey] &&
            item[nodeKey].length < x[nodeKey].length &&
            x[nodeKey].startsWith(item[nodeKey]),
        );
      return checkedItem && !isChecked(item) && isChecked(checkedItem) ? true : false;
    } else {
      return checked &&
        checked[nodeKey] &&
        item &&
        item[nodeKey] &&
        item[nodeKey].length < checked[nodeKey].length &&
        checked[nodeKey].startsWith(item[nodeKey])
        ? true
        : false;
    }
  };

  const recursive = root => {
    const children = findChildren(root);
    return (
      children &&
      children
        .map(item => ({ ...item, total: getTotal(item) }))
        .sort((node1: any, node2: any) => node2.total - node1.total)
        .map(item => (
          <div className={classes.nodeItem}>
            {(!onlyLastChild || (onlyLastChild && profondeurMax === item[nodeKey].length)) && (
              <Checkbox
                className={classes.checkbox}
                indeterminate={isIndeterminate(item)}
                indeterminateIcon={<IndeterminateCheckBox />}
                checked={isChecked(item)}
                onClick={() => handleCheck(item)}
                disabled={disabled}
              />
            )}

            {expanded.includes(item[nodeKey]) ? (
              <KeyboardArrowDown onClick={() => handleCloseExpand(item)} />
            ) : (
              item.total > 0 && <KeyboardArrowRight onClick={() => handleExpand(item)} />
            )}
            {item.total > 0 && <Typography className={classes.total}>{item.total}</Typography>}
            <StyledTreeItem nodeId={`${item[nodeKey]}`} label={item[nodeLabel]}>
              {recursive(item)}
            </StyledTreeItem>
          </div>
        ))
    );
  };

  const memorized = useMemo(() => {
    return (
      <TreeView className={classes.treeView} expanded={expanded}>
        {data
          .filter((item: any) => item[nodeParent] === null)
          .map(item => ({ ...item, total: getTotal(item) }))
          .sort((node1: any, node2: any) => node2.total - node1.total)
          .map((item: any) => (
            <div className={classes.nodeItem}>
              {(!onlyLastChild || (onlyLastChild && profondeurMax === item[nodeKey].length)) && (
                <Checkbox
                  className={classes.checkbox}
                  indeterminate={isIndeterminate(item)}
                  indeterminateIcon={<IndeterminateCheckBox />}
                  checked={isChecked(item)}
                  onClick={() => handleCheck(item)}
                  disabled={disabled}
                />
              )}
              {expanded.includes(item[nodeKey]) ? (
                <KeyboardArrowDown onClick={() => handleCloseExpand(item)} />
              ) : (
                item.total > 0 && <KeyboardArrowRight onClick={() => handleExpand(item)} />
              )}
              {item.total > 0 && <Typography className={classes.total}>{item.total}</Typography>}

              <StyledTreeItem nodeId={`${item[nodeKey]}`} label={item[nodeLabel]}>
                {recursive(item)}
              </StyledTreeItem>
            </div>
          ))}
      </TreeView>
    );
  }, [checked, data, expanded, disabled]);

  if (searchResult && searchResult.loading) {
    return (
      <Box display="flex" justifyContent="center">
        <CircularProgress />
      </Box>
    );
  }

  return <> {memorized} </>;
};

export default withRouter(CustomTree);
