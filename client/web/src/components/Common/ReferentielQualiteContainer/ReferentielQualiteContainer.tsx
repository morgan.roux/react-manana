import React, { FC, useState, ReactElement, useEffect } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Box, Grid, Divider } from '@material-ui/core';
import Filter from '../Filter';
import NoItemListImage from '../NoItemListImage';
import NoItemContentImage from '../NoItemContentImage';
import { Loader } from '../../Dashboard/Content/Loader';
import { SideMenuButtonInterface } from '../SideMenuButton/SideMenuButton';
import { CardTheme, CardThemeList } from '@app/ui-kit';
import { getUser } from '../../../services/LocalStorage';
import { AppAuthorization } from '../../../services/authorization';
import { useApolloClient } from '@apollo/react-hooks';
//import SideMenuButtonShowModal from '../SideMenuShowModal/SideMenuButtonShowModal';

export enum LayoutType {
  TwoColumns,
  WithFilter,
  Empty,
}

export interface ItemContainerProps {
  handleScroll?: any;
  noContentValues?: any;
  listItemFields?: any;
  listButton?: SideMenuButtonInterface;
  listResult?: any;
  leftListItem?: any;
  mainItem?: any;
  moreOptionsItem?: any;
  layout: LayoutType;
  leftListChildren?: ReactElement;
  rightContentChildren?: ReactElement;
  match: {
    params: { id: string | undefined; view: string | undefined };
  };
}

const ItemContainer: FC<ItemContainerProps & RouteComponentProps<any, any, any>> = ({
  handleScroll,
  noContentValues,
  listItemFields,
  listButton,
  listResult,
  leftListChildren,
  rightContentChildren,
  moreOptionsItem,
  mainItem,
  layout,
  match,
  location: { pathname },
  history,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [currentItem, setCurrentItem] = useState<any>();

  const isActive = (item: any) => {
    return currentItem && item && currentItem.id === item.id;
  };

  const memorized = React.useMemo(
    () => (
      <nav className={classes.drawer}>
        <Filter />
      </nav>
    ),
    [],
  );

  useEffect(() => {
    if (listResult && listResult.data && listResult.data.search && listResult.data.search.data) {
      if (currentItem && currentItem.id) {
        const newValue = listResult.data.search.data.find(
          item => item && item.id === currentItem.id,
        );
        setCurrentItem(newValue);
      } else if (match.params.id && currentItem === undefined) {
        const newValue = listResult.data.search.data.find(
          item => item && item.id === match.params.id,
        );
        setCurrentItem(newValue);
      }
    }
  }, [listResult]);

  const data =
    (listResult && listResult.data && listResult.data.search && listResult.data.search.data) || [];

  // if (listResult && listResult.loading) {
  //   return <Loader />;
  // }

  if (layout === LayoutType.WithFilter) {
    return (
      <Box className={classes.root}>
        {pathname !== '/pilotage/business/pharmacie' &&
          pathname !== '/pilotage/feedback/pharmacie' &&
          pathname !== '/pilotage/feedback' &&
          memorized}
        {mainItem}
      </Box>
    );
  }

  const user = getUser();
  const auth = new AppAuthorization(user);

  const isActualite: boolean = window.location.hash.includes('#actualite');
  const isOperatione: boolean = window.location.hash.includes('#operation');

  const handleClick = () => {
    // Check authorisation
    if (
      (isActualite && !auth.isAuthorizedToViewActu()) ||
      (isOperatione && !auth.isAuthorizedToViewOC())
    ) {
      history.push('/unauthorized');
      return;
    }

    if (currentItem && currentItem.id) {
      window.history.pushState(null, '', `#${listItemFields.url}/${currentItem.id}`);
    } else {
      window.history.pushState(null, '', `#${listItemFields.url}`);
      setCurrentItem(null); // Provisoire
    }
    client.writeData({
      data: {
        articleOcArray: [],
      },
    });
    setCurrentItem(currentItem);
  };

  return (
    <Box className={classes.root}>
      {memorized}
      {/*<div className={classes.leftListContainer}>
        {listButton && <SideMenuButtonShowModal button={listButton} />}
        <Grid
          id="listLaboGrid"
          onScroll={() => handleScroll({ id: 'listLaboGrid' })}
          item={true}
          className={classes.leftList}
        >
          {listResult && listResult.loading ? (
            <Loader />
          ) : listItemFields && data && data.length ? (
            <GroupTheme>
              {data.map((item, index) => (
                <div key={index} >
                  <div>
                    <Theme ordre={item.ordre} nom={item.nom} >
                        {
                          item.sousFamille.map( ( sousFamille : any , indice : number ) => (
                              <SousTheme 
                                  key={indice} 
                                  ordreParent={item.ordre} 
                                  ordre={sousFamille.ordre} 
                                  nom={sousFamille.nom}
                                  item={item}
                                  moreOptionsItem={
                                    moreOptionsItem
                                      ? React.cloneElement(
                                          moreOptionsItem,
                                          {
                                            active: isActive(item),
                                            currentItem: item,
                                            setCurrentItem,
                                            url: listItemFields && listItemFields.url,
                                          },
                                          null,
                                        )
                                      : null
                                  }
                                  setCurrentId={setCurrentItem}
                                  listItemFields={listItemFields}
                                  currentId={currentItem} 
                              />
                          ) )
                        }
                    </Theme>
                    <CardTheme
                        title={item.title} 
                        createDate={item.createDate} 
                        updateDate={item.updateDate}
                        item={item}
                        moreOptionsItem={
                          moreOptionsItem
                            ? React.cloneElement(
                                moreOptionsItem,
                                {
                                  active: isActive(item),
                                  currentItem: item,
                                  setCurrentItem,
                                  url: listItemFields && listItemFields.url,
                                },
                                null,
                              )
                            : null
                        }
                        setCurrentId={setCurrentItem}
                        listItemFields={listItemFields}
                        currentId={currentItem} 
                      />
                    </div>
                    <Box paddingLeft="16px" paddingRight="16px">
                      <Divider />
                    </Box>
                </div>
              ))}
            </GroupTheme>
          ) : (
            noContentValues && (
              <NoItemListImage
                title={
                  (noContentValues && noContentValues.list && noContentValues.list.title) || ''
                }
                subtitle={
                  (noContentValues && noContentValues.list && noContentValues.list.subtitle) || ''
                }
              />
            )
          )}
          {leftListChildren &&
            data &&
            data.map(item => (
              <div>
                {React.cloneElement(leftListChildren, {
                  item,
                  active: isActive(item),
                  setCurrentItem,
                })}
              </div>
            ))}
        </Grid>
      </div>
      <div className={classes.rightContainer}>
        {rightContentChildren && currentItem ? (
          React.cloneElement(rightContentChildren, {
            currentItem,
            setCurrentItem,
            url: listItemFields && listItemFields.url,
          })
        ) : (
          <NoItemContentImage
            title={
              (noContentValues && noContentValues.content && noContentValues.content.title) || ''
            }
            subtitle={
              (noContentValues && noContentValues.content && noContentValues.content.subtitle) || ''
            }
          />
        )}
          </div>*/}
    </Box>
  );
};

export default withRouter(ItemContainer);
