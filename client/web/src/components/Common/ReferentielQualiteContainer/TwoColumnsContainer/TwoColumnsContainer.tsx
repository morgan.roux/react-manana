import React, { FC } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Box } from '@material-ui/core';
interface TwoColumnsContainerProps {
  listResult?: any;
  leftSideComponent: any;
  rightSideComponent: any;
}

const TwoColumnsContainer: FC<TwoColumnsContainerProps & RouteComponentProps<any, any, any>> = ({
  listResult,
  leftSideComponent,
  rightSideComponent,
}) => {
  const classes = useStyles({});

  return (
    <Box className={classes.root}>
      <div>{leftSideComponent}</div>
      <div>{rightSideComponent}</div>
    </Box>
  );
};

export default withRouter(TwoColumnsContainer);
