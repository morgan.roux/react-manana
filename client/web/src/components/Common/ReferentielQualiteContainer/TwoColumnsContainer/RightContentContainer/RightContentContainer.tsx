import React, { FC, useEffect, useMemo, useState } from 'react';
import useStyles from './styles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import RightContentComment from './RightContentComment';
import { useMutation, useApolloClient, useQuery, useLazyQuery } from '@apollo/react-hooks';
import {
  DO_CREATE_OPERATION_VIEWER,
  DO_MARK_OPERATION_AS_SEEN,
  DO_MARK_OPERATION_AS_NOT_SEEN,
  DO_DELETE_OPERATION_VIEWER,
} from '../../../../../graphql/OperationCommerciale/mutation';
import {
  createOperationViewer,
  createOperationViewerVariables,
} from '../../../../../graphql/OperationCommerciale/types/createOperationViewer';
import { SEARCH, SEARCHVariables } from '../../../../../graphql/search/types/SEARCH';
import { SEARCH as SEARCH_QUERY } from '../../../../../graphql/search/query';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { getUser, getGroupement } from '../../../../../services/LocalStorage';
import { ME_me } from '../../../../../graphql/Authentication/types/ME';
import {
  markAsSeenOperation,
  markAsSeenOperationVariables,
} from '../../../../../graphql/OperationCommerciale/types/markAsSeenOperation';
import {
  markAsNotSeenOperation,
  markAsNotSeenOperationVariables,
} from '../../../../../graphql/OperationCommerciale/types/markAsNotSeenOperation';
import Backdrop from '../../../Backdrop';
import ICurrentPharmacieInterface from '../../../../../Interface/CurrentPharmacieInterface';
import { GET_CURRENT_PHARMACIE } from '../../../../../graphql/Pharmacie/local';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import CustomButton from '../../../CustomButton';
import { ShoppingCart, BarChart, Visibility, VisibilityOff } from '@material-ui/icons';
import {
  SEEN_BY_PHARMACIE,
  SEEN_BY_PHARMACIEVariables,
} from '../../../../../graphql/Actualite/types/SEEN_BY_PHARMACIE';
import {
  DO_SEEN_BY_PHARMACIE,
  DO_MARK_ACTUALITE_NOT_SEEN,
} from '../../../../../graphql/Actualite/mutation';
import {
  MARK_ACTUALITE_SEEN,
  MARK_ACTUALITE_SEENVariables,
} from '../../../../../graphql/Actualite/types/MARK_ACTUALITE_SEEN';
import { DO_MARK_ACTUALITE_SEEN } from '../../../../../graphql/Actualite';
import {
  MARK_ACTUALITE_NOT_SEEN,
  MARK_ACTUALITE_NOT_SEENVariables,
} from '../../../../../graphql/Actualite/types/MARK_ACTUALITE_NOT_SEEN';
import {
  deleteOperationViewer,
  deleteOperationViewerVariables,
} from '../../../../../graphql/OperationCommerciale/types/deleteOperationViewer';
import AttachedFiles from '../../../../Main/Content/Actualite/ActualiteView/ActualiteViewBody/AttachedFiles';
import { DO_SEARCH_ALL_PHARMACIES } from '../../../../../graphql/Pharmacie';
import {
  SEARCH_ALL_PHARMACIESVariables,
  SEARCH_ALL_PHARMACIES,
} from '../../../../../graphql/Pharmacie/types/SEARCH_ALL_PHARMACIES';
import { GET_ALL_ITEMS } from '../../../../../graphql/Item';
import { ALL_ITEMS, ALL_ITEMS_items } from '../../../../../graphql/Item/types/ALL_ITEMS';
import { AppAuthorization } from '../../../../../services/authorization';
// import { initPilotageApolloLocalState } from '../../../../Main/Content/Pilotage/Dashboard/Dashboard';

export enum ItemType {
  Actualite = 'Actualite',
  Operation = 'Operation',
}

interface ContentProps {
  url?: string;
  currentItem?: any;
  setCurrentItem?: (value: any) => void;
  listResult: any;
  history: {
    location: {
      state: {
        idPublicite: string | undefined;
      };
    };
  };
  match: {
    params: { idOperation: string | undefined; view: string | undefined };
  };
}

const NavTabs: FC<ContentProps & RouteComponentProps> = ({
  url,
  currentItem,
  listResult,
  setCurrentItem,
  history,
  location: { pathname },
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const user: ME_me = getUser();
  const groupement = getGroupement();
  const currentUser: ME_me = getUser();
  const idPharmacieUser =
    (currentUser &&
      currentUser.userPpersonnel &&
      currentUser.userPpersonnel.pharmacie &&
      currentUser.userPpersonnel.pharmacie.id) ||
    (currentUser &&
      currentUser.userTitulaire &&
      currentUser.userTitulaire.titulaire &&
      currentUser.userTitulaire.titulaire.pharmacieUser &&
      currentUser.userTitulaire.titulaire.pharmacieUser.id);

  const idGroupement = groupement && groupement.id;

  const isOp: boolean = window.location.hash.startsWith('#/operations-commerciales/');
  const isActu: boolean = window.location.hash.startsWith('#/actualite/');
  const idPublicite =
    history && history.location && history.location.state && history.location.state.idPublicite;
  const auth = new AppAuthorization(currentUser);

  const [actuItem, setActuItem] = useState<ALL_ITEMS_items | null>(null);

  /**
   * Items query
   */
  const [getAllItems, { data: allItems }] = useLazyQuery<ALL_ITEMS>(GET_ALL_ITEMS);
  // Execute items query
  useEffect(() => {
    if (isActu) getAllItems();
  }, []);

  /**
   * Set actuItem
   */
  useEffect(() => {
    if (allItems && allItems.items && allItems.items.length > 0) {
      const item = allItems.items.find(i => i && i.code === 'ACTUALITE');
      if (item) setActuItem(item);
    }
  }, [allItems]);

  /**
   * Get all pharmacies
   */
  const [searchAllPharma, { data: allPharmaData, loading: allPharmaLoading }] = useLazyQuery<
    SEARCH_ALL_PHARMACIES,
    SEARCH_ALL_PHARMACIESVariables
  >(DO_SEARCH_ALL_PHARMACIES, {
    fetchPolicy: 'network-only',
    variables: {
      type: ['pharmacie'],
      skip: 0,
      take: 1000,
      filterBy: [{ term: { idGroupement } }],
    },
  });

  /**
   * Execute search all pharma
   */
  useMemo(() => {
    if (
      (currentItem.operationPharmacie && currentItem.operationPharmacie.globalite) ||
      (currentItem.actualiteCible && currentItem.actualiteCible.globalite)
    ) {
      searchAllPharma();
    }
  }, [currentItem]);

  const goToCommande = () => {
    const id = currentItem && currentItem.operation ? currentItem.operation.id : currentItem.id;
    /* REMISE MARCHE OU PROMOTION */
    const idRemise =
      currentItem && currentItem.marche && currentItem.marche.remise
        ? currentItem.marche.remise.id
        : currentItem && currentItem.promotion && currentItem.promotion.remise
        ? currentItem.promotion.remise.id
        : null;

    const url = idRemise
      ? `/operation-produits/card/${id}/${idRemise}`
      : `/operation-produits/card/${id}`;
    client.writeData({
      data: {
        operationPanier: null,
      },
    });
    history.push(url, { idPublicite });
  };

  const goTostat = (url: string, item: any, pilotageType: string) => {
    if (currentItem && currentItem.id) {
      let pharmacies: any[] = [];

      // For operation
      if (isOp) {
        if (currentItem.operationPharmacie && currentItem.operationPharmacie.globalite) {
          pharmacies = (allPharmaData && allPharmaData.search && allPharmaData.search.data) || [];
        } else if (currentItem.pharmacieCible && currentItem.pharmacieCible.length > 0) {
          pharmacies = currentItem.pharmacieCible;
        }
      }

      // For actualite
      if (isActu) {
        if (currentItem.actualiteCible && currentItem.actualiteCible.globalite) {
          pharmacies = (allPharmaData && allPharmaData.search && allPharmaData.search.data) || [];
        } else if (currentItem.pharmaciesCible && currentItem.pharmaciesCible.length > 0) {
          pharmacies = currentItem.pharmaciesCible;
        }
      }

      if (pharmacies.length > 0) {
        client.writeData({
          data: {
            pilotageCibles: {
              item,
              items: [currentItem],
              pharmacies,
              pilotageType,
              __typename: 'pilotageCibles',
            },
          },
        });

        if (isOp) {
          client.writeData({
            data: {
              pilotageOcSource: {
                ...currentItem,
                __typename: 'pilotageOcSource',
              },
            },
          });
        }

        if (isActu) {
          client.writeData({
            data: {
              pilotageActuSource: {
                ...currentItem,
                __typename: 'pilotageActuSource',
              },
            },
          });
        }

        history.push(url);
      }
    }
  };

  const goToBusiness = () => {
    goTostat(`/pilotage/dashboard/business/global`, null, 'business');
  };

  const goToFeedback = () => {
    if (currentItem) {
      if (isOp && currentItem.item) {
        goTostat(`/pilotage/dashboard/feedback/global`, currentItem.item, 'feedback');
      }

      if (isActu && actuItem) {
        goTostat(`/pilotage/dashboard/feedback/global`, actuItem, 'feedback');
      }
    }
  };

  useEffect(() => {
    if (
      currentItem &&
      currentItem.id &&
      currentItem.niveauPriorite !== 3 &&
      currentItem.seen === false
    ) {
      if (currentItem.__typename === ItemType.Operation) {
        doMarkOperationAsSeen();
        if (idPharmacieUser) doCreateOperationViewer();
      } else if (currentItem.__typename === ItemType.Actualite) {
        doMarkActualiteSeen({
          variables: {
            id: currentItem.id,
            userId: currentUser && currentUser.id,
          },
        });
        if (currentUser && currentUser.role && idPharmacieUser) {
          doSeenByPharmacie({
            variables: {
              id: currentItem.id,
              idPharmacieUser,
              userId: currentUser && currentUser.id,
            },
          });
        }
      }
    }
  }, [currentItem]);

  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const currentPharmacie =
    myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie && myPharmacie.data.pharmacie;

  const [doCreateOperationViewer, { loading, data }] = useMutation<
    createOperationViewer,
    createOperationViewerVariables
  >(DO_CREATE_OPERATION_VIEWER, {
    variables: {
      idOperation: (currentItem && currentItem.id) || '',
      idPharmacie: currentPharmacie && currentPharmacie.id ? currentPharmacie.id : '',
      idPharmacieUser: idPharmacieUser ? idPharmacieUser : '',
      userId: user && user.id,
    },
    update: (cache, { data }) => {
      if (data && data.createOperationViewer && listResult && listResult.variables) {
        const req = cache.readQuery<SEARCH, SEARCHVariables>({
          query: SEARCH_QUERY,
          variables: listResult.variables,
        });
      }
    },
  });

  const [doDeleteOperationViewer, doDeleteOperationViewerResult] = useMutation<
    deleteOperationViewer,
    deleteOperationViewerVariables
  >(DO_DELETE_OPERATION_VIEWER, {
    variables: {
      idOperation: (currentItem && currentItem.id) || '',
      idPharmacie: currentPharmacie && currentPharmacie.id ? currentPharmacie.id : '',
      idPharmacieUser: idPharmacieUser ? idPharmacieUser : '',
      userId: user && user.id,
    },
    update: (cache, { data }) => {
      if (data && data.deleteOperationViewer && listResult && listResult.variables) {
        const req = cache.readQuery<SEARCH, SEARCHVariables>({
          query: SEARCH_QUERY,
          variables: listResult.variables,
        });
      }
    },
  });

  const [doMarkOperationAsSeen, doMarkOperationAsSeenResult] = useMutation<
    markAsSeenOperation,
    markAsSeenOperationVariables
  >(DO_MARK_OPERATION_AS_SEEN, {
    variables: {
      id: (currentItem && currentItem.id) || '',
      userId: user && user.id,
    },
    update: (cache, { data }) => {
      if (data && data.markAsSeenOperation && data.markAsSeenOperation.id) {
        const req = cache.readQuery<SEARCH, SEARCHVariables>({
          query: SEARCH_QUERY,
          variables: listResult && listResult.variables,
        });

        if (req && req.search && req.search.data) {
          cache.writeQuery({
            query: SEARCH_QUERY,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.map((item: any) => {
                    if (
                      item &&
                      data.markAsSeenOperation &&
                      item.id === data.markAsSeenOperation.id
                    ) {
                      item.seen = true;
                      return item;
                    } else {
                      return item;
                    }
                  }),
                },
              },
            },
            variables: listResult && listResult.variables,
          });
        }
      }
    },
    onCompleted: data => {
      if (data && data.markAsSeenOperation) {
        if (setCurrentItem) setCurrentItem({ ...currentItem, seen: true });
      }
    },
  });

  const [doSeenByPharmacie] = useMutation<SEEN_BY_PHARMACIE, SEEN_BY_PHARMACIEVariables>(
    DO_SEEN_BY_PHARMACIE,
    {
      update: (cache, { data }) => {
        if (data && data.actualiteSeenByPharmacie) {
          const req = cache.readQuery<SEARCH, SEARCHVariables>({
            query: SEARCH_QUERY,
            variables: listResult && listResult.variables,
          });
          if (req && req.search && req.search.data) {
            cache.writeQuery({
              query: SEARCH_QUERY,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: req.search.data.map((item: any) => {
                      if (
                        item &&
                        data.actualiteSeenByPharmacie &&
                        item.id === data.actualiteSeenByPharmacie.id
                      ) {
                        item.nbSeenByPharmacie = data.actualiteSeenByPharmacie.nbSeenByPharmacie;
                        return item;
                      }
                    }),
                  },
                },
              },
              variables: listResult && listResult.variables,
            });
          }
        }
      },
      onCompleted: data => {
        if (
          setCurrentItem &&
          data &&
          data.actualiteSeenByPharmacie &&
          data.actualiteSeenByPharmacie.nbSeenByPharmacie
        ) {
          setCurrentItem({
            ...currentItem,
            nbSeenByPharmacie: data.actualiteSeenByPharmacie.nbSeenByPharmacie,
          });
        }
      },
      onError: errors => {
        errors.graphQLErrors.map(err => {
          const snackBarData: SnackVariableInterface = {
            type: 'ERROR',
            message: err.message,
            isOpen: true,
          };
          displaySnackBar(client, snackBarData);
        });
      },
    },
  );

  const [doMarkActualiteSeen, doMarkActualiteSeenResult] = useMutation<
    MARK_ACTUALITE_SEEN,
    MARK_ACTUALITE_SEENVariables
  >(DO_MARK_ACTUALITE_SEEN, {
    update: (cache, { data }) => {
      if (data && data.markAsSeenActualite) {
        const req = cache.readQuery<SEARCH, SEARCHVariables>({
          query: SEARCH_QUERY,
          variables: listResult && listResult.variables,
        });
        if (req && req.search && req.search.data) {
          cache.writeQuery({
            query: SEARCH_QUERY,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.map((item: any) => {
                    if (
                      item &&
                      data.markAsSeenActualite &&
                      item.id === data.markAsSeenActualite.id
                    ) {
                      item.seen = data.markAsSeenActualite.seen;
                      return item;
                    } else {
                      return item;
                    }
                  }),
                },
              },
            },
            variables: listResult && listResult.variables,
          });
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  const [doMarkOperationAsNotSeen, doMarkOperationAsNotSeenResult] = useMutation<
    markAsNotSeenOperation,
    markAsNotSeenOperationVariables
  >(DO_MARK_OPERATION_AS_NOT_SEEN, {
    variables: {
      id: (currentItem && currentItem.id) || '',
      idPharmacieUser: idPharmacieUser ? idPharmacieUser : null,
      userId: user && user.id,
    },
    update: (cache, { data }) => {
      if (data && data.markAsNotSeenOperation && data.markAsNotSeenOperation.id) {
        const req = cache.readQuery<SEARCH, SEARCHVariables>({
          query: SEARCH_QUERY,
          variables: listResult && listResult.variables,
        });
        if (req && req.search && req.search.data) {
          // if (setCurrentItem) {
          //   window.history.pushState(null, '', url);
          //   setCurrentItem(null);
          // }
          cache.writeQuery({
            query: SEARCH_QUERY,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.map((item: any) => {
                    if (
                      item &&
                      data.markAsNotSeenOperation &&
                      item.id === data.markAsNotSeenOperation.id
                    ) {
                      item.seen = data.markAsNotSeenOperation.seen;
                      item.nombrePharmaciesConsultes =
                        data.markAsNotSeenOperation.nombrePharmaciesConsultes;
                      return item;
                    }
                  }),
                },
              },
            },
            variables: listResult && listResult.variables,
          });
        }
      }
    },
  });

  const [doMarkActualiteNotSeen, doMarkActualiteNotSeenResult] = useMutation<
    MARK_ACTUALITE_NOT_SEEN,
    MARK_ACTUALITE_NOT_SEENVariables
  >(DO_MARK_ACTUALITE_NOT_SEEN, {
    update: (cache, { data }) => {
      if (data && data.markAsNotSeenActualite) {
        const req = cache.readQuery<SEARCH, SEARCHVariables>({
          query: SEARCH_QUERY,
          variables: listResult && listResult.variables,
        });
        if (req && req.search && req.search.data) {
          if (setCurrentItem) {
            window.history.pushState(null, '', `#${url}`);
            setCurrentItem(null);
          }
          cache.writeQuery({
            query: SEARCH_QUERY,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.map((item: any) => {
                    if (
                      item &&
                      data.markAsNotSeenActualite &&
                      item.id === data.markAsNotSeenActualite.id
                    ) {
                      item.seen = data.markAsNotSeenActualite.seen;
                      return item;
                    } else {
                      return item;
                    }
                  }),
                },
              },
            },
            variables: listResult && listResult.variables,
          });
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  useEffect(() => {
    const operationGroupement =
      currentItem && currentItem.idGroupement ? currentItem.idGroupement : null;

    /*
      Verifier pour le super admin le groupement de l'operation
    */
    if (
      groupement &&
      groupement.id &&
      operationGroupement &&
      groupement.id !== operationGroupement
    ) {
      const snackBarData: SnackVariableInterface = {
        type: 'INFO',
        message: 'Cette operation est dans un autre Groupement',
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    } else if (
      currentItem &&
      groupement &&
      groupement.id &&
      operationGroupement &&
      groupement.id === operationGroupement
    ) {
      if (
        currentItem &&
        currentItem.operationPharmacie &&
        !currentItem.operationPharmacie.globalite
      ) {
        const pharmaciesCible =
          currentItem.operationPharmacie.pharmaciedetails &&
          currentItem.operationPharmacie.pharmaciedetails.length
            ? currentItem.operationPharmacie.pharmaciedetails.map((opPharma: any) =>
                opPharma && opPharma.pharmacie && opPharma.pharmacie.id
                  ? opPharma.pharmacie.id
                  : null,
              )
            : [];
        if (
          pharmaciesCible &&
          pharmaciesCible.length &&
          currentPharmacie &&
          !pharmaciesCible.includes(currentPharmacie.id)
        ) {
          const snackBarData: SnackVariableInterface = {
            type: 'INFO',
            message: `Votre pharmacie actuel n'est pas ciblé par cette operation`,
            isOpen: true,
          };
          displaySnackBar(client, snackBarData);
          if (setCurrentItem) {
            window.history.pushState(null, '', `#${url}`);
            setCurrentItem(null);
          }
        }
      }
    }
  }, [currentItem]);

  const markAsSeen = () => {
    switch (currentItem.__typename) {
      case ItemType.Actualite:
        doMarkActualiteSeen({
          variables: {
            id: currentItem.id,
            userId: currentUser && currentUser.id,
          },
        });
        if (currentUser && currentUser.role && idPharmacieUser) {
          doSeenByPharmacie({
            variables: {
              id: currentItem.id,
              idPharmacieUser,
              userId: currentUser && currentUser.id,
            },
          });
        }
        break;

      case ItemType.Operation:
        doMarkOperationAsSeen();
        if (idPharmacieUser) doCreateOperationViewer();
        break;

      default:
        break;
    }
  };

  const markAsNotSeen = () => {
    switch (currentItem.__typename) {
      case ItemType.Actualite:
        doMarkActualiteNotSeen({
          variables: { id: currentItem.id, idPharmacieUser, userId: currentUser && currentUser.id },
        });

        break;
      case ItemType.Operation:
        doMarkOperationAsNotSeen();
        if (idPharmacieUser) doDeleteOperationViewer();
        break;

      default:
        break;
    }
  };

  /**
   * Show msg for actu or oc bloquant
   */
  useEffect(() => {
    if (currentItem && !currentItem.seen) {
      if (pathname.startsWith('/actualite/')) {
        displaySnackBar(client, {
          type: 'INFO',
          message: `Vous consultez une actualité de type bloquant, cliquer "Marquer comme lue" pour marquer que vous l'avez bien lu.`,
          isOpen: true,
        });
      }
    }
  }, [currentItem, pathname]);

  const disabledMarkSeenOrNotSeenBtn = (): boolean => {
    if (isActu) return !auth.isAuthorizedToMarkSeenOrNotSeenActu();
    if (isOp) return !auth.isAuthorizedToMarkSeenOrNotSeenOC();
    return false;
  };

  const disabledBusinessBtn = (): boolean => {
    // if (isActu) return !auth.isAuthorizedToMarkSeenOrNotSeenActu();
    if (isOp) return !auth.isAuthorizedToViewPilotageBusinessOC();
    return false;
  };

  const disabledFeedbackBtn = (): boolean => {
    // if (isActu) return !auth.isAuthorizedToMarkSeenOrNotSeenActu();
    if (isOp) return !auth.isAuthorizedToViewPilotageFeedbackOC();
    return false;
  };

  return (
    <Box className={classes.root}>
      {(doMarkOperationAsSeenResult.loading ||
        doMarkOperationAsNotSeenResult.loading ||
        doMarkActualiteNotSeenResult.loading ||
        doMarkActualiteSeenResult.loading ||
        allPharmaLoading) && <Backdrop />}

      <Box className={classes.fixedAppBarTab}>
        <Box padding="30px 60px" display="flex" flexDirection="column">
          <Typography className={classes.libelle}>{currentItem && currentItem.libelle}</Typography>
          <Box className={classes.buttonContainer}>
            {currentItem &&
              !currentItem.accordCommercial &&
              (currentItem.operation || currentItem.operationArticles) && (
                <CustomButton
                  startIcon={<ShoppingCart />}
                  children="Passer commande"
                  color="primary"
                  className={classes.btnPasserCommande}
                  onClick={goToCommande}
                />
              )}
            {currentItem && currentItem.accordCommercial && (
              <CustomButton
                startIcon={<ShoppingCart />}
                children="Accord commerciale"
                color="primary"
                className={classes.btnPasserCommande}
              />
            )}
            {isOp && (
              <CustomButton
                startIcon={<BarChart />}
                children="Business"
                color="primary"
                className={classes.btnPasserCommande}
                onClick={goToBusiness}
                disabled={disabledBusinessBtn()}
              />
            )}
            <CustomButton
              startIcon={<BarChart />}
              children="Feedback"
              color="primary"
              className={classes.btnPasserCommande}
              onClick={goToFeedback}
              disabled={disabledFeedbackBtn()}
            />
            {currentItem &&
              currentItem.niveauPriorite &&
              currentItem.niveauPriorite === 3 &&
              !currentItem.seen && (
                <CustomButton
                  startIcon={<Visibility />}
                  children="Marquer comme lue"
                  color="primary"
                  className={classes.btnPasserCommande}
                  onClick={markAsSeen}
                  disabled={disabledMarkSeenOrNotSeenBtn()}
                />
              )}
            {currentItem &&
              currentItem.niveauPriorite &&
              currentItem.niveauPriorite === 3 &&
              currentItem.seen && (
                <CustomButton
                  startIcon={<VisibilityOff />}
                  children="Marquer comme non lue"
                  color="primary"
                  className={classes.btnPasserCommande}
                  onClick={markAsNotSeen}
                  disabled={disabledMarkSeenOrNotSeenBtn()}
                />
              )}
          </Box>

          <p className={classes.description}>{currentItem && currentItem.description}</p>
          {currentItem &&
            currentItem.fichierPresentations &&
            currentItem.fichierPresentations.length > 0 && (
              <AttachedFiles actualite={currentItem as any} />
            )}
        </Box>
        <RightContentComment
          listResult={listResult}
          codeItem={
            currentItem && currentItem.__typename === ItemType.Actualite ? 'ACTUALITE' : 'OPERACOM'
          }
          item={currentItem as any}
        />
      </Box>
    </Box>
  );
};
export default withRouter(NavTabs);
