import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: 'inline',
    },
  }),
);
