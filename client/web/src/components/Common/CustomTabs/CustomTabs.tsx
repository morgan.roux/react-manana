import React, { ReactNode, FC, useState, ChangeEvent, useEffect } from 'react';
import useStyles from './styles';
import { Box, Tabs, Tab, AppBar, IconButton, Badge } from '@material-ui/core';
import { ArrowBackIos, ArrowForwardIos } from '@material-ui/icons';

export interface TabInterface {
  id: number;
  label: string;
  content: ReactNode;
  clickHandlerParams?: string;
  count?: number;
}

export interface CustomTabsProps {
  tabs: TabInterface[];
  clickHandler?: (params: string) => void | undefined;
  activeStep?: number;
  hideArrow?: boolean;
  hasBadge?: boolean;
}

const a11yProps = (index: any) => {
  return {
    id: `custom-tab-${index}`,
    'aria-controls': `custom-tabpanel-${index}`,
  };
};

const CustomTabs: FC<CustomTabsProps> = ({
  tabs,
  clickHandler,
  activeStep,
  hideArrow,
  hasBadge,
}) => {
  const classes = useStyles({});
  const [value, setValue] = useState(activeStep || 0);

  const handleChange = (event: ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  const muiTabs = () => {
    const muiTabsScroller = document.querySelector('.MuiTabs-scroller');
    return {
      element: muiTabsScroller,
      scrollLeft: muiTabsScroller ? muiTabsScroller.scrollLeft : 0,
    };
  };

  const onClickLeftArrow = () => {
    const { element, scrollLeft } = muiTabs();
    if (element && scrollLeft > 0) {
      element.scrollLeft -= 194 * 2;
    }
  };

  const onClickRightArrow = () => {
    const { element } = muiTabs();
    if (element) {
      element.scrollLeft += 194 * 2;
    }
  };

  const handleClickEvent = () => {
    const activeTab = tabs.find(tab => tab.id === value);
    if (activeTab && activeTab.clickHandlerParams && clickHandler) {
      clickHandler(activeTab.clickHandlerParams);
    }
  };
  useEffect(() => {
    handleClickEvent();
  }, [value]);
  return (
    <Box className={classes.customTabsRoot}>
      <AppBar
        className={classes.customTabsAppBar}
        color="transparent"
        position="static"
        id="customTabsAppBarId"
      >
        {!hideArrow && (
          <IconButton onClick={onClickLeftArrow}>
            <ArrowBackIos />
          </IconButton>
        )}
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="customtabs tabs scrollable auto"
          variant="scrollable"
          scrollButtons="auto"
        >
          {tabs.map((tab: TabInterface, index: number) => {
            return (
              <Tab
                key={`tabs_tab_${index}`}
                label={
                  <Badge className={classes.countTabsItem} badgeContent={tab.count}>
                    {tab.label}
                  </Badge>
                }
                {...a11yProps(tab.id)}
              ></Tab>
            );
          })}
        </Tabs>
        {!hideArrow && (
          <IconButton onClick={onClickRightArrow}>
            <ArrowForwardIos />
          </IconButton>
        )}
      </AppBar>
      {tabs.map((tab: TabInterface, index: number) => {
        return (
          <Box
            key={`tabs_tabpanel_${index}`}
            role="tabpanel"
            hidden={value !== index}
            id={`custom-tabpanel-${index}`}
            aria-labelledby={`custom-tab-${index}`}
          >
            {value === index && <Box>{tab.content}</Box>}
          </Box>
        );
      })}
    </Box>
  );
};

export default CustomTabs;
