import React, { FC, useState, useEffect } from 'react';
import useStyles from './styles';
import {
  USER_SMYLEYS_userSmyleys,
  USER_SMYLEYS_userSmyleys_data_smyley,
} from '../../../../graphql/Smyley/types/USER_SMYLEYS';
import { useMutation } from '@apollo/react-hooks';
import {
  CREATE_GET_PRESIGNED_URL,
  CREATE_GET_PRESIGNED_URLVariables,
} from '../../../../graphql/S3/types/CREATE_GET_PRESIGNED_URL';
import { DO_CREATE_GET_PRESIGNED_URL } from '../../../../graphql/S3';
import { Box } from '@material-ui/core';
interface UserActionInfoProps {
  nbSmyley?: number;
  nbComment?: number;
  nbShare?: number;
  userSmyleys?: USER_SMYLEYS_userSmyleys;
  onCountClick?: () => void
}

const UserActionInfo: FC<UserActionInfoProps> = ({ nbSmyley, nbComment, nbShare, onCountClick, userSmyleys }) => {
  const classes = useStyles({});
  const [userSmylesPresigned, setUserSmylesPresigned] = useState<
    USER_SMYLEYS_userSmyleys_data_smyley[]
  >([]);

  const [doCreateGetPresignedUrls, doCreateGetPresignedUrlsResult] = useMutation<
    CREATE_GET_PRESIGNED_URL,
    CREATE_GET_PRESIGNED_URLVariables
  >(DO_CREATE_GET_PRESIGNED_URL);

  const presignedUrls =
    doCreateGetPresignedUrlsResult &&
    doCreateGetPresignedUrlsResult.data &&
    doCreateGetPresignedUrlsResult.data.createGetPresignedUrls;

  // Do presigned mutation
  useEffect(() => {
    const filePaths: string[] = [];
    if (userSmyleys && userSmyleys.data) {
      userSmyleys.data.map(userSmyley => {
        if (userSmyley && userSmyley.smyley) filePaths.push(userSmyley.smyley.photo);
      });
      if (filePaths.length > 0) {
        // create presigned
        doCreateGetPresignedUrls({ variables: { filePaths } });
      }
    }
  }, [userSmyleys && userSmyleys.data]);

  // console.log('userSmylesPresigned => ', userSmylesPresigned);

  // Set presigned smyleys
  useEffect(() => {
    const newUserSmyleys: USER_SMYLEYS_userSmyleys_data_smyley[] = [];
    if (presignedUrls) {
      presignedUrls.map(file => {
        if (userSmyleys && userSmyleys.data) {
          userSmyleys.data.map(userSmyley => {
            if (file && userSmyley && userSmyley.smyley) {
              if (file.filePath === userSmyley.smyley.photo) {
                const newFile: USER_SMYLEYS_userSmyleys_data_smyley = {
                  ...userSmyley.smyley,
                  photo: file.presignedUrl,
                };
                newUserSmyleys.push(newFile);
              }
            }
          });
        }
      });
    }

    const filteredArr = newUserSmyleys.reduce((prev: any, current: any) => {
      const x = prev.find((item: any) => item.id === current.id);
      if (!x) {
        return prev.concat([current]);
      } else {
        return prev;
      }
    }, []);

    setUserSmylesPresigned(filteredArr);
  }, [doCreateGetPresignedUrlsResult.data]);


  return (
    <div className={classes.root}>
      {userSmylesPresigned &&
        userSmylesPresigned.map(smyley => {
          return <img key={smyley.id} src={smyley.photo} alt={smyley.nom} />;
        })}
      <Box onClick={() => onCountClick && onCountClick()} className={classes.nbSmyley}>{nbSmyley}</Box>
    </div>
  );
};

export default UserActionInfo;
