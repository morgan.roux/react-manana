import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
// const drawerWidth = 275;
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingTop: 20,
      minWidth: 400,
    },
    commentContainer: {
      display: 'flex',
      color: '#212121',
      marginTop: 16,
      '& .MuiAvatar-root': {
        marginRight: 5,
      },
    },
    commentContent: {
      display: 'flex',
      flexDirection: 'column',
      borderRadius: 12,
      backgroundColor: '#F5F6FA',
      padding: 16,
    },
    spaceBetween: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    fullName: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 16,
      color: '#212121',
    },
    role: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 14,
      color: theme.palette.secondary.main,
    },
    pharmacie: {
      display: 'flex',
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 16,
      color: '#616161',
      '& .MuiTypography-root': {
        marginLeft: 3,
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        fontSize: 16,
        color: '#212121',
      },
    },
    comment: {
      fontFamily: 'Roboto',
      fontSize: 14,
      color: '#212121',
    },
    date: {
      fontFamily: 'Roboto',
      color: '#9E9E9E',
      fontSize: 12,
    },
    moreComments: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 16,
      color: '#212121',
      display: 'flex',
      '& .MuiTypography-root': {
        marginLeft: 3,
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        fontSize: 16,
        color: '#212121',
      },
    },
    emoticoneContainer: {
      height: 41,
      width: 146,
      borderRadius: 6,
      boxShadow: '0px 3px 6px #00000029',
      opacity: 1,
      backgroundColor: theme.palette.common.white,
      transform: 'translateY(-20px)',
    },
    actionContainer: {
      display: 'flex',
      marginTop: 15,
      justifyContent: 'space-between',
      paddingRight: 20,
      paddingLeft: 35,
    },
    loaderContainer: {
      width: '100%',
      marginTop: 20,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paginationContainer: {
      '& > *': {
        marginTop: theme.spacing(2),
      },
    },
  }),
);
