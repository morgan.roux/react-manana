import { useQuery } from '@apollo/react-hooks';
import { Box, CircularProgress, Typography } from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import moment from 'moment';
import React, { ChangeEvent, FC, Fragment, useState } from 'react';
import { GET_PILOTAGE_COMMENTS } from '../../../graphql/Pilotage';
import {
  PILOTAGE_COMMENTS,
  PILOTAGE_COMMENTSVariables,
} from '../../../graphql/Pilotage/types/PILOTAGE_COMMENTS';
import CustomAvatar from '../CustomAvatar';
import { useStyles } from './styles';

interface ModalCommentProps {
  codeItem: string;
  idSource: string;
  idsPharmacies: string[];
}

const ModalComment: FC<ModalCommentProps> = ({ codeItem, idSource, idsPharmacies }) => {
  const classes = useStyles({});
  const [take, setTake] = useState<number>(10);
  const [skip, setSkip] = useState<number>(0);

  const { data, loading } = useQuery<PILOTAGE_COMMENTS, PILOTAGE_COMMENTSVariables>(
    GET_PILOTAGE_COMMENTS,
    { variables: { codeItem, idSource, idsPharmacies, take, skip } },
  );

  const comments = data && data.pilotageComments && data.pilotageComments;
  const total = (comments && comments.total) || 0;

  const handleChange = (event: ChangeEvent<unknown>, value: number) => {
    setSkip(value * 10 - 10);
  };

  return (
    <div className={classes.root}>
      {comments && comments.data && comments.data.length > 0 ? (
        <Fragment>
          {comments.data.map((comment, index) => {
            const uerName = (comment && comment.user && comment.user.userName) || '';
            const dateModif =
              comment && comment.dateModification ? moment(comment.dateModification).fromNow() : '';
            const userRole =
              (comment && comment.user && comment.user.role && comment.user.role.nom) || '';
            const userPharma =
              (comment && comment.user && comment.user.pharmacie && comment.user.pharmacie.nom) ||
              '';
            const content = (comment && comment.content) || '';

            return (
              <div className={classes.commentContainer} key={`pilotage_comment_${index}`}>
                <CustomAvatar name={uerName} />
                <div>
                  <div className={classes.commentContent}>
                    <div className={classes.spaceBetween}>
                      <Typography className={classes.fullName}>{uerName}</Typography>
                      <Typography className={classes.date}>{dateModif}</Typography>
                    </div>
                    <Typography className={classes.role}>{userRole}</Typography>
                    <span className={classes.pharmacie}>
                      Pharmacie : <Typography>{userPharma}</Typography>
                    </span>
                    <Typography className={classes.comment}>{content}</Typography>
                  </div>
                  {/* // TODO : Comment response, comment reaction
                  <div className={classes.actionContainer}>
                    <div className={classes.moreComments}>
                      <SubdirectoryArrowRight />
                      <Typography>33 réponses</Typography>
                    </div>
                    <div className={classes.emoticoneContainer}>
                      <ThumbUp color="secondary" />
                    </div>
                  </div>
                  */}
                </div>
              </div>
            );
          })}
        </Fragment>
      ) : (
        <Box display="flex" justifyContent="center">
          Aucun commentaire
        </Box>
      )}
      {loading && (
        <div className={classes.loaderContainer}>
          <CircularProgress color="secondary" style={{ width: 25, height: 25 }} />
        </div>
      )}
      {total > 10 && (
        <div className={classes.paginationContainer}>
          <Pagination
            count={total}
            color="primary"
            variant="outlined"
            shape="rounded"
            showFirstButton={true}
            showLastButton={true}
            onChange={handleChange}
          />
        </div>
      )}
    </div>
  );
};

export default ModalComment;
