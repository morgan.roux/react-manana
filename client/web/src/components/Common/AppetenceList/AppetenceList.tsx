import { useApolloClient, useLazyQuery } from '@apollo/react-hooks';
import { Box, LinearProgress, Typography } from '@material-ui/core';
import React, { ChangeEvent, FC, useEffect, useMemo, useRef, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { GET_ITEMS } from '../../../federation/basis/item/query';
import { ITEMS, ITEMSVariables } from '../../../federation/basis/item/types/ITEMS';
import { getPharmacie } from '../../../services/LocalStorage';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import CustomButton from '../CustomButton';
import { CustomFormTextField } from '../CustomTextField';
import useStyles from './styles';

interface AppetenceListProps {
  itemList: any[];
  setItemList: (itemList: any[]) => void;
  loading: boolean;
  sorting?: boolean,
  appetences: any[];
  withTitle?: boolean;
  withSubTitle?: boolean;
  withInputSearch?: boolean;
  withAddBtn?: boolean;
  match: {
    params: {
      idRole: string | undefined;
    };
  };
  handleOrderChange: (id: string, event: any) => void;
}

const AppetenceList: FC<AppetenceListProps & RouteComponentProps> = ({
  itemList,
  setItemList,
  loading,
  appetences,
  sorting = false,
  withTitle,
  withSubTitle,
  withInputSearch,
  withAddBtn,
  history: { push },
  location: { pathname },
  match: {
    params: { idRole },
  },
  handleOrderChange,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [searchText, setSearchText] = useState<string>('');

  const handleChangeSearchText = (e: ChangeEvent<HTMLInputElement>) => {
    if (e && e.target) {
      setSearchText(e.target.value);
    }
  };

  const goToRoles = () => push('/db/roles');

  const [searchAllItems, { data: allData, loading: allLoading }] = useLazyQuery<
    ITEMS,
    ITEMSVariables
  >(GET_ITEMS, {
    client: FEDERATION_CLIENT,
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  useEffect(() => {
    searchAllItems({
      variables: {
        filter: { name: { iLike: `%${searchText}%` } },
        idPharmacie: getPharmacie().id,
      },
    });
  }, [searchText]);

  useEffect(() => {
    if (allData?.items && appetences) {

      let newItemList = allData.items.nodes
        .filter(e => e.active) // if item actived
        .map(item => {
          const appetence = appetences.find(appetence => appetence.idItem === item.id);
          return appetence ? { ...item, ordre: appetence.ordre } : item;
        })

      if (sorting) {
        newItemList = newItemList.sort((a: any, b: any) => (a.ordre || 1) - (b.ordre || 1)).reverse()
      }


      setItemList(
        newItemList
      );
    }
  }, [allData, appetences]);

  return (
    <div className={classes.infoPersoContainer}>
      {withTitle && <Typography className={classes.inputTitle}>Scores Pour Dashboard d’activité</Typography>}
      {withSubTitle && (
        <Typography className={classes.permissionAccessSubTite}>
          Définir un score selon l’importance de chacun de ces Items (De 1 à 10)
        </Typography>
      )}
      {withInputSearch && (
        <CustomFormTextField
          placeholder="Rechercher"
          onChange={handleChangeSearchText}
          value={searchText}
        />
      )}
      <LinearProgress
        color="secondary"
        style={{
          width: '100%',
          height: 3,
          marginBottom: 20,
          visibility: allLoading || loading ? 'visible' : 'hidden',
        }}
      />
      {itemList.length === 0 && !loading ? (
        <div className={classes.noDataContainer}>
          <span>Aucun score trouvé</span>
          {withAddBtn && (
            <CustomButton variant="outlined" color="secondary" onClick={goToRoles}>
              Définir scores
            </CustomButton>
          )}
        </div>
      ) : (
        <>
          <div className={classes.inputsContainer}>
            <div className={classes.appetenceFormRow}>
              {itemList.map((i: any, index) => {
                return (
                  <Box
                    display="flex"
                    width="100%"
                    justifyContent="space-between"
                    alignItems="center"
                    key={`appetence-${i.id}`}
                  >
                    <Typography>{i.name}</Typography>
                    <CustomFormTextField
                      type="number"
                      value={i.ordre}
                      onChange={event => handleOrderChange(i.id, event)}
                    />
                  </Box>
                );
              })}
            </div>
          </div>
        </>
      )}
    </div>
  );
};

AppetenceList.defaultProps = {
  withTitle: true,
  withSubTitle: true,
  withInputSearch: true,
  withAddBtn: true,
};

export default withRouter(AppetenceList);
