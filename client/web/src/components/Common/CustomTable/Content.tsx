import React, { FC, useContext, useEffect, useState } from 'react';
import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import { Theme } from '@material-ui/core/styles';
import { useLazyQuery } from '@apollo/react-hooks';
import { SEARCH } from '../../../graphql/search/query';
import Table from '../../Common/Table';
import { SearchInput } from '../../Dashboard/Content/SearchInput';
import { Loader } from '../../Dashboard/Content/Loader';
import { ContentContext, ContentStateInterface } from '../../../AppContext';
import {
  TextField,
  FormControl,
  Select,
  InputLabel,
  Button,
  OutlinedInput,
  MenuItem,
} from '@material-ui/core';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
  root: {
    padding: theme.spacing(3),
    paddingTop: theme.spacing(3),
    width: '100%',
  },
  searchBar: {
    display: 'flex',
    marginBottom: theme.spacing(5),
    '@media (max-width: 692px)': {
      flexWrap: 'wrap',
      justifyContent: 'center',
    },
  },
  search: {
    '@media (max-width: 580px)': {
      marginBottom: 15,
    },
  },
  alignCenter: {
    textAlign: 'center',
    margin: '40px 0px',
    fontSize: 20,
    fontWeight: 'bold',
  },
  formControl: {
    width: 240,
    marginRight: theme.spacing(6),
    marginBottom: theme.spacing(3),
  },
  searchButton: {
    width: 240,
    height: 51,
  },
});

interface Column {
  name: string;
  label: string;
  sortable?: boolean;
  renderer?: (row: any) => any;
}

interface ContentProps {
  type: string;
  enableGlobalSearch: boolean;
  searchPlaceholder: string;
  filterBy?: any;
  sortBy?: any;
  columns?: Column[];
  buttons?: any;
  withAvatar?: boolean;
  avatarObjectName?: string;
  searchInputs?: FieldsOptions[];
  isSelectable?: boolean;
  datas?: any[];
}

interface FieldsOptions {
  name: string;
  extraNames?: string[];
  label: string;
  value: any;
  placeholder?: string;
  inputLabelProps?: object;
  variant?: string;
  options?: Option[];
  type: string; // 'Select' | 'Search';
}

interface Option {
  label: string;
  value: any;
}

const ROWS_PER_PAGE_OPTIONS = [25, 50, 100, 250, 500, 1000, 2000];

// Generate elastic search query from fields value
const generateQuery = (fieldsState: any, searchInputs: FieldsOptions[]): any => {
  const must: any[] = [];

  if (searchInputs) {
    searchInputs.forEach((item: FieldsOptions) => {
      if (
        fieldsState[item.name] !== null &&
        fieldsState[item.name] !== undefined &&
        fieldsState[item.name] !== ''
      ) {
        if (item.type === 'Select') {
          must.push({
            match: {
              [item.name]: {
                query: fieldsState[item.name],
              },
            },
          });
        } else if (item.type === 'Search') {
          let q = `${fieldsState[item.name]}`
            .trim()
            // .replace(/[&\/\\#,+()$~%.'":\-?<>{}]/g, ' ')
            .split(' ')
            .filter(str => str.trim().length > 0)
            .join('\\ ');
          q = `*${q}*`;

          must.push({
            query_string: {
              query: q,
              fields: item.extraNames ? [item.name, ...item.extraNames] : [item.name],
            },
          });
        }
      }
    });
  }

  return {
    query: {
      bool: { must },
    },
  };
};

const Content: FC<ContentProps & WithStyles> = ({
  classes,
  type,
  columns,
  enableGlobalSearch,
  searchPlaceholder,
  filterBy,
  sortBy,
  buttons,
  withAvatar,
  avatarObjectName,
  searchInputs,
  isSelectable,
  datas,
}) => {
  const {
    content: { query: searchText },
    setContent,
  } = useContext<ContentStateInterface>(ContentContext);
  const [rowsPerPage, setRowsPerPage] = useState(ROWS_PER_PAGE_OPTIONS[0]);
  const [page, setPage] = useState(0);
  const [count, setCount] = useState(0);
  const [order, setOrder] = useState<'desc' | 'asc'>('desc');
  const [columnSorted, setColumnSorted] = useState<string>('');
  const [sorted, setSorted] = useState<any>();
  const [fieldsState, setFieldsState] = useState<any>({});
  const [query, setQuery] = useState<any>({});

  useEffect(() => {
    if (searchInputs) {
      (searchInputs || []).map((item: FieldsOptions) => {
        setFieldsState((prevState: any) => ({
          ...prevState,
          [item.name]: item.value,
        }));
      });
    }
  }, [searchInputs]);

  const [search, { loading, error, data }] = useLazyQuery(SEARCH, {
    variables: {
      type: [type],
      skip: page * rowsPerPage,
      take: rowsPerPage,
      query: enableGlobalSearch ? searchText : query,
      sortBy: sorted ? [sorted] : sortBy,
      filterBy,
      idPharmacie: '',
    },
  });

  useEffect(() => {
    if (!loading && data && data.search && data.search.total) {
      setCount(data.search.total);
      // console.log('dataResult ==> ', data);
    }
  }, [loading, data]);

  useEffect(() => {
    setContent(prevState => ({
      ...prevState,
      page: 0,
    }));
    search();
  }, [page, rowsPerPage]);

  const handleFieldChange = (event: any): void => {
    setFieldsState({
      ...fieldsState,
      [event.target.name]: event.target.value,
    });
  };

  const handleRunSearch = (event: any): void => {
    event.preventDefault();
    setContent(prevState => ({
      ...prevState,
      page: 0,
    }));
    if (searchInputs) {
      setQuery(generateQuery(fieldsState, searchInputs));
    }
    search();
  };

  const toggleSort = async (sortedBy: string) => {
    const ordered = order === 'desc' ? 'asc' : 'desc';
    setSorted({ [sortedBy]: { order: ordered } });
    setOrder(ordered);
  };

  useEffect(() => {
    // Reinitialiser la valeur de query si le type change
    setContent(prevState => ({
      ...prevState,
      query: '',
    }));
  }, [type]);

  useEffect(() => {
    if (sorted) {
      setColumnSorted(Object.keys(sorted)[0]);
    } else if (sortBy) {
      setColumnSorted(Object.keys(sortBy[0])[0]);
    }
    setContent(prevState => ({
      ...prevState,
      type,
      columns,
      searchPlaceholder,
      filterBy,
      sortBy: sorted ? [sorted] : sortBy,
      buttons,
    }));
    if (!datas) search();
  }, [type, columns, searchPlaceholder, filterBy, sortBy, sorted, buttons, datas]);

  return (
    <div className={classes.root}>
      <div className={classes.searchBar}>
        <div className={classes.search}>
          {enableGlobalSearch && (
            <SearchInput
              value={searchText}
              onChange={(value: string) => {
                setContent(prevState => ({
                  ...prevState,
                  page: 0,
                  query: value,
                }));
              }}
              placeholder={searchPlaceholder}
            />
          )}
          {!enableGlobalSearch && searchInputs && (
            <form onSubmit={handleRunSearch}>
              {searchInputs.map((inputItem: FieldsOptions, key) => {
                if (inputItem.type === 'Search') {
                  return (
                    <FormControl variant="outlined" key={key} className={classes.formControl}>
                      <TextField
                        name={inputItem.name}
                        label={inputItem.label}
                        value={fieldsState[inputItem.name]}
                        placeholder={inputItem.placeholder || 'Commençant par..'}
                        onChange={handleFieldChange}
                        InputLabelProps={inputItem.inputLabelProps || { shrink: true }}
                        variant={'outlined'}
                      />
                    </FormControl>
                  );
                } else if (inputItem.type === 'Select') {
                  return (
                    <FormControl variant="outlined" className={classes.formControl}>
                      <InputLabel htmlFor="sortie-select">{inputItem.label}</InputLabel>
                      <Select
                        name={inputItem.name}
                        value={fieldsState[inputItem.name] || 0}
                        onChange={handleFieldChange}
                        input={<OutlinedInput type="number" labelWidth={100} id="input-select" />}
                      >
                        {inputItem.options &&
                          (inputItem.options || []).map((menuItem: Option) => (
                            <MenuItem value={menuItem.value}>{menuItem.label}</MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                  );
                }
              })}
              <Button
                type="submit"
                variant="contained"
                color="secondary"
                className={classes.searchButton}
              >
                Rechercher
              </Button>
            </form>
          )}
        </div>
        {buttons}
      </div>

      {!datas && (loading || !data) ? (
        <Loader />
      ) : error ? (
        <div className={classes.alignCenter}>Erreur lors de chargement de la page</div>
      ) : (data && data.search && data.search.data && data.search.data.length > 0) || datas ? (
        <Table
          isSelectable={isSelectable || false}
          page={page}
          rowsPerPage={rowsPerPage}
          rowsPerPageOptions={ROWS_PER_PAGE_OPTIONS}
          onChangePage={(page: number) => setPage(page)}
          onChangeRowsPerPage={(rowsPerPage: number) => setRowsPerPage(rowsPerPage)}
          columns={columns}
          data={data.search.data}
          count={count}
          toggleSorting={toggleSort}
          order={order}
          columnSorted={columnSorted}
          densePadding={true}
          withAvatar={withAvatar}
          avatarObjectName={avatarObjectName}
        />
      ) : (
        <div className={classes.alignCenter}>Aucun résultat correspondant</div>
      )}
    </div>
  );
};

export default withStyles(styles)(Content);
