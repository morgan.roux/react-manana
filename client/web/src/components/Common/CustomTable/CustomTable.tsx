import React, { FC } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';

import useStyles from './styles';

interface CustomTable {
  page?: number;
  rowsPerPage?: number;
  rowsPerPageOptions?: number[];
}

const headCells = [
  { id: 'name', numeric: false, disablePadding: true, label: 'Dessert (100g serving)' },
  { id: 'calories', numeric: true, disablePadding: false, label: 'Calories' },
  { id: 'fat', numeric: true, disablePadding: false, label: 'Fat (g)' },
  { id: 'carbs', numeric: true, disablePadding: false, label: 'Carbs (g)' },
  { id: 'protein', numeric: true, disablePadding: false, label: 'Protein (g)' },
];

const ROWS_PER_PARGE_OPTIONS = [10, 30, 50, 70, 90, 110, 130, 150];

const CustomTable: FC<CustomTable> = ({ page, rowsPerPage, rowsPerPageOptions }) => {
  const classes = useStyles({});

  const [order, setOrder] = React.useState<'asc' | 'desc'>('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState<string[]>([]);
  const [tablePage, setPage] = React.useState(page || 0);
  const [tableRowsPerPage, setRowsPerPage] = React.useState(
    rowsPerPage || ROWS_PER_PARGE_OPTIONS[0],
  );

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <div className={classes.rootSelectionContainer}>
      <Table
        className={classes.tableRoot}
        aria-labelledby="tableTitle"
        size={'medium'}
        aria-label="enhanced table"
      >
        {/* <CustomTableHead
          classes={classes}
          numSelected={selected.length}
          order={order}
          orderBy={orderBy}
          onSelectAllClick={handleSelectAllClick}
          onRequestSort={handleRequestSort}
          rowCount={data.length}
          columns={headCells}
        /> */}
      </Table>

      <TablePagination
        rowsPerPageOptions={rowsPerPageOptions || ROWS_PER_PARGE_OPTIONS}
        component="div"
        count={0}
        rowsPerPage={tableRowsPerPage}
        page={tablePage}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </div>
  );
};

export default CustomTable;
