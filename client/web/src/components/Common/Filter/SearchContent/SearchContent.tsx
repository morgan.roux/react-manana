import React, { FC, useRef, useState, useEffect } from 'react';
import { useStyles } from './style';
import { InputBase, Box } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { GET_LOCAL_SEARCH, SearchInterface } from '../../withSearch/withSearch';
import { debounce } from 'lodash';

interface SearchContentProps {
  placeholder: string;
}
const SearchContent: FC<SearchContentProps> = ({ placeholder }) => {
  const classes = useStyles({});
  const client = useApolloClient();

  const [searchText, setSearchText] = useState('');

  const localSearchText = useQuery<SearchInterface>(GET_LOCAL_SEARCH);

  useEffect(() => {
    if (
      localSearchText &&
      localSearchText.data &&
      localSearchText.data.searchText &&
      localSearchText.data.searchText.text
    ) {
      setSearchText(localSearchText.data.searchText.text);
    } else {
      setSearchText('');
    }
  }, [localSearchText]);

  const onChange = (value: string) => {
    setSearchText(value);
  };

  const debouncedSearchText = useRef(
    debounce((value: string) => {
      updateLocalState(value || '');
    }, 1500),
  );

  useEffect(() => {
    debouncedSearchText.current(searchText);
  }, [searchText]);

  const updateLocalState = (value: string) => {
    client.writeData({ data: { searchText: { text: value, __typename: 'searchText' } } });
  };

  return (
    <Box className={classes.search}>
      <InputBase
        placeholder={placeholder}
        type="search"
        value={searchText}
        onChange={evt => onChange(evt.target.value)}
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput,
        }}
        inputProps={{ 'aria-label': 'search' }}
      />
      <Box className={classes.searchIcon}>
        <SearchIcon />
      </Box>
    </Box>
  );
};

export default SearchContent;
