import { createStyles, fade, Theme, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    search: {
      display: 'flex',
      position: 'relative',
      border: '1px solid #1D1D1D',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: theme.palette.common.white,
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.97),
      },
      marginTop: 8,
      marginLeft: 24,
      marginRight: 24,
      width: 'auto',
    },
    searchIcon: {
      width: 36,
      height: '100%',
      position: 'absolute',
      right: '0',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputInput: {
      padding: '15px 36px 15px 8px',
      width: '100%',
      fontSize: '0.75rem!important',
      '&::placeholder': {
        fontSize: '0.75rem!important',
      },
    },
    inputRoot: {
      color: 'inherit',
      width: '100%',
    },
  }),
);
