import React, { FC, useState } from 'react';
import {
  Box,
  Checkbox,
  ListItem,
  ListItemText,
  IconButton,
  Typography,
  Radio,
} from '@material-ui/core';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import useStyles from './styles';

interface FilterContentProps {
  title: string;
  datas: any;
  updateDatas: (list: any) => void;
  useRadios?: boolean;
  counts?: any[];
}

const CustomFilterContent: FC<FilterContentProps> = ({
  title,
  datas,
  updateDatas,
  useRadios,
  counts,
}) => {
  const classes = useStyles({});
  const [expandedMore, setExpandedMore] = useState<boolean>(true);

  const onGetValue = itemIndex => {
    const newList = datas.map((item: any, index) => {
      if (itemIndex === index) {
        item.checked = !item.checked;
        return item;
      }
      return item;
    });
    updateDatas(newList);
  };

  const onGetRadioValue = (itemIndex: number) => {
    const newList = datas.map((item: any, index) => {
      if (itemIndex === index) {
        item.checked = true;
        return item;
      } else {
        item.checked = false;
        return item;
      }
    });
    updateDatas(newList);
  };

  return (
    <Box>
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        paddingTop={2}
        onClick={() => setExpandedMore(!expandedMore)}
      >
        <Typography className={classes.fontSize14}>{title}</Typography>
        <IconButton size="small">{expandedMore ? <ExpandLess /> : <ExpandMore />}</IconButton>
      </Box>
      {useRadios ? (
        <Box className={classes.noStyle}>
          {datas.map(
            (item: any, index) =>
              item && (
                <ListItem
                  role={undefined}
                  dense={true}
                  button={true}
                  key={`${index}-${item.nom}`}
                  onClick={() => onGetRadioValue(index)}
                >
                  <Box display="flex" alignItems="center">
                    <Radio checked={item.checked} tabIndex={-1} disableRipple={true} />
                    <ListItemText primary={item.nom} />
                  </Box>
                  {counts && (
                    <Box>
                      <Typography className={classes.nbrProduits}>
                        {counts && counts[index] ? counts[index] : 0}
                      </Typography>
                    </Box>
                  )}
                </ListItem>
              ),
          )}
        </Box>
      ) : (
        <Box className={classes.noStyle}>
          {expandedMore &&
            datas &&
            datas.map(
              (item: any, index) =>
                item && (
                  <ListItem
                    role={undefined}
                    dense={true}
                    button={true}
                    onClick={() => onGetValue(index)}
                    key={`${index}-${item.nom}`}
                  >
                    <Box className={classes.checkBoxLeftName} display="flex" alignItems="center">
                      <Checkbox tabIndex={-1} checked={item.checked} disableRipple={true} />
                      <ListItemText primary={item.nom} />
                    </Box>
                    {counts && (
                      <Box>
                        <Typography className={classes.nbrProduits}>
                          {counts && counts[index] ? counts[index] : 0}
                        </Typography>
                      </Box>
                    )}
                  </ListItem>
                ),
            )}
        </Box>
      )}
    </Box>
  );
};

export default CustomFilterContent;
