import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
    expandBtnContainer: {
      marginRight: 0,
    },
    nbrProduits: {
      padding: '2px 6px',
      fontSize: 10,
      background: '#E0E0E0',
      borderRadius: 2,
      height: 20,
      minWidth: 20,
      textAlign: 'center',
      marginRight: 10,
    },
    fontSize14: {
      fontSize: 16,
      fontFamily: 'Roboto',
      fontWeight: 600,
    },
    noStyle: {
      listStyleType: 'none',
      '& .MuiListItem-root': {
        padding: '4px 0',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
      },
      '& .MuiRadio-root': {
        padding: '0 8px 0 0!important',
      },
    },
    checkBoxLeftName: {
      '& .MuiCheckbox-root': {
        padding: 0,
        color: '#000000',
        marginRight: 8,
      },
      '&$checked': {
        color: '#000000',
      },
    },
  }),
);

export default useStyles;
