import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    pink: {
      // color: theme.palette.text.primary,
    },
    noStyle: {
      listStyleType: 'none',
    },
  }),
);

export default useStyles;
