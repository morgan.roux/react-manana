import React, { FC, useState, useEffect } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Box, Typography, IconButton, Tooltip } from '@material-ui/core';
import ReplayIcon from '@material-ui/icons/Replay';
import useStyles from './styles';
import Content from './Content';
import { useApolloClient, useQuery } from '@apollo/react-hooks';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import SortIcon from '@material-ui/icons/Sort';
import SortInterface from '../../../../Interface/SortInterface';
import GET_LOCAL_SORT from '../../../../localStates/SortLocal';
import {
  SEARCH as SEARCH_Interface,
  SEARCHVariables,
  SEARCH_search_data_CommandeCanal,
} from '../../../../graphql/search/types/SEARCH';
import { SEARCH } from '../../../../graphql/search/query';
import { FiltersInterface, GET_LOCAL_FILTERS } from '../../withSearch/withSearch';

interface CanalInterface {
  code: string;
  libelle: string;
  active: boolean;
}

const SortContent: FC<RouteComponentProps<any, any, any>> = () => {
  const client = useApolloClient();
  const classes = useStyles({});
  const [expandedMore, setExpandedMore] = useState<boolean>(false);
  const [canals, setCanals] = useState<CanalInterface[]>([]);
  const [codeCanalActive, setCodeCanalActive] = useState<string>('PFL');

  const filters = useQuery<FiltersInterface>(GET_LOCAL_FILTERS);
  const filterResult = filters && filters.data && filters.data.filters;

  const listResult = useQuery<SEARCH_Interface, SEARCHVariables>(SEARCH, {
    variables: {
      type: ['commandecanal'],
    },
    fetchPolicy: 'cache-and-network',
  });

  useEffect(() => {
    if (
      listResult &&
      listResult.data &&
      listResult.data.search &&
      listResult.data.search.data &&
      listResult.data.search.data.length
    ) {
      setCanals(
        listResult.data.search.data.map((item: any) => {
          return {
            code: item.code,
            libelle: item.libelle,
            active: item.code === codeCanalActive,
          };
        }),
      );
    }
  }, [listResult, codeCanalActive]);

  useEffect(() => {
    client.writeData({
      data: {
        filters: filterResult
          ? {
              ...filterResult,
              idCommandeCanal: codeCanalActive,
            }
          : {
              idCommandeCanal: codeCanalActive,
              __typename: 'local',
            },
      },
    });
    /* client.writeData({
      data: {
        codeCanalActive,
      },
    }); */
  }, [codeCanalActive]);

  const handleSelectActive = (code: string) => {
    setCodeCanalActive(code);
  };

  return (
    <Box marginLeft="8px" borderTop="1px solid #E3E3E3" padding="16px 0">
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <Typography className={classes.name} onClick={() => setExpandedMore(!expandedMore)}>
          Choisir canal
        </Typography>
        <Box display="flex" flexDirection="row" alignItems="center">
          {expandedMore ? (
            <IconButton size="small" onClick={() => setExpandedMore(!expandedMore)}>
              <ExpandLess />
            </IconButton>
          ) : (
            <IconButton size="small" onClick={() => setExpandedMore(!expandedMore)}>
              <ExpandMore />
            </IconButton>
          )}
        </Box>
      </Box>
      {expandedMore &&
        canals &&
        canals.length &&
        canals.map((item: CanalInterface) => (
          <Content
            key={item.code}
            code={item.code}
            libelle={item.libelle}
            active={item.active}
            selectActive={handleSelectActive}
          />
        ))}
    </Box>
  );
};
export default withRouter(SortContent);
