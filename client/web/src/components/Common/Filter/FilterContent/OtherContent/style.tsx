import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
    itemContainer: {
      marginLeft: 16,
      borderTop: '1px solid #E3E3E3',
      '& > div ': {
        margin: '16px 0px',
      },
      '& .MuiRadio-root': {
        // color: theme.palette.primary.main,
      },
    },
    itemTitleContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    itemContentContainer: {
      display: 'flex',
      flexDirection: 'column',
    },
    title: {
      fontFamily: 'Roboto',
      fontSize: '14px',
      color: '#1D1D1D',
    },
    expandBtnContainer: {
      marginRight: 10,
    },
  }),
);

export default useStyles;
