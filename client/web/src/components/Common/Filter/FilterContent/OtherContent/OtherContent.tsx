import React, { useState, ReactNode, FC, useEffect } from 'react';
import { Box, Typography, IconButton } from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import useStyles from './style';
import StatusFilter from '../StatusFilter';
import PeriodeFilter from '../PeriodeFilter/PeriodeFilter';
import { useApolloClient } from '@apollo/react-hooks';
import { END_DAY_OF_CURRENT_MONTH, TODAY } from '../../../../../Constant/date';
import { ApolloClient } from 'apollo-client';
import { ME_me } from '../../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../../services/LocalStorage';
import { TITULAIRE_PHARMACIE } from '../../../../../Constant/roles';

export const ROLES_FILTER_DATES = [TITULAIRE_PHARMACIE];

interface Element {
  id: string;
  title: string;
  content: ReactNode;
}

export interface OtherContentProps {
  dataType: string;
  titleLegend?: string;
  startDatePlaceholderText?: string;
  endDatePlaceholderText?: string;
}

export const initializeStatusAndPeriodeFilters = (
  client: ApolloClient<object>,
  role: string,
  dataType?: string,
) => {
  // Reset status filter
  client.writeData({ data: { statusFilter: 'all' } });
  // Reset dateFilter
  client.writeData({
    data: {
      dateFilter: {
        startDate: ROLES_FILTER_DATES.includes(role) ? TODAY : null,
        endDate: ROLES_FILTER_DATES.includes(role) ? END_DAY_OF_CURRENT_MONTH : null,
        __typename: 'DateFilter',
      },
    },
  });
};

const OtherContent: FC<OtherContentProps> = ({ dataType }) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [expanded, setExpanded] = useState<{ [key: string]: boolean }>({
    status: true,
    periode: true,
  });
  const currentUser: ME_me = getUser();
  const role = currentUser && currentUser.role && currentUser.role.code;

  const toggleExpanded = (key: string) => {
    setExpanded(prevState => ({
      ...prevState,
      [key]: !prevState[key],
    }));
  };

  const elements: Element[] = [
    {
      id: 'status',
      title: 'Status',
      content: <StatusFilter />,
    },
    {
      id: 'periode',
      title: 'Période de recherche',
      content: <PeriodeFilter dataType={dataType} />,
    },
  ];

  // Set statusFilter and dateFilter to default if dataType change
  useEffect(() => {
    initializeStatusAndPeriodeFilters(client, role || '', dataType);
  }, [dataType]);

  return (
    <Box marginLeft="8px" padding="0px">
      {elements.map((item, index) => {
        return (
          <div className={classes.itemContainer} key={`list_other_content_item_${index}`}>
            <div className={classes.itemTitleContainer} onClick={() => toggleExpanded(item.id)}>
              <Typography className={classes.title}> {item.title} </Typography>
              <IconButton size="small" className={classes.expandBtnContainer}>
                {expanded[item.id] ? <ExpandLess /> : <ExpandMore />}
              </IconButton>
            </div>
            {expanded[item.id] && (
              <div className={classes.itemContentContainer}>{item.content}</div>
            )}
          </div>
        );
      })}
    </Box>
  );
};

export default OtherContent;
