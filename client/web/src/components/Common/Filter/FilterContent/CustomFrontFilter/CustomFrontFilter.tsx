import React, { useEffect, FC, useState } from 'react';
import moment, { Moment } from 'moment';
import { DateRangePicker } from '../../../DateRangePicker';
import useStyles from './style';
import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { GET_FILTER_DATE } from '../../../../../graphql/Filter/local';
import { END_DAY_OF_CURRENT_MONTH, TODAY } from '../../../../../Constant/date';
import { OtherContentProps } from '../OtherContent/OtherContent';
import { ME_me } from '../../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../../services/LocalStorage';
import { SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT } from '../../../../../Constant/roles';
import { DateFilterIterface } from '../PeriodeFilter/PeriodeFilter';
import {
  Box,
  Typography,
  IconButton,
  ListItem,
  Checkbox,
  ListItemText,
  ListItemSecondaryAction,
} from '@material-ui/core';
import { title } from 'process';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import { FrontFiltersInterface, GET_LOCAL_FRONT_FILTERS } from '../../../withSearch/withSearch';

interface ListItemInterface {
  id: number;
  value: any;
  name: string;
}

interface CustomFrontFilterInterface {
  data: {
    title: string;
    typename: string;
    list: ListItemInterface[];
  };
}

const CustomFrontFilter: FC<CustomFrontFilterInterface> = ({ data }) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const currentUser: ME_me = getUser();
  const isAdmin =
    currentUser &&
    currentUser.role &&
    (currentUser.role.code === SUPER_ADMINISTRATEUR ||
      currentUser.role.code === ADMINISTRATEUR_GROUPEMENT);

  const [expandedMore, setExpandedMore] = useState<boolean>(true);
  const [list, setList] = useState<any[]>(data.list);

  // get front filters list from local state
  const frontFilters = useQuery<FrontFiltersInterface>(GET_LOCAL_FRONT_FILTERS);

  useEffect(() => {
    if (frontFilters && frontFilters.data && frontFilters.data.frontFilters) {
      switch (data.typename) {
        case 'occommande':
          if (frontFilters.data.frontFilters.occommande.length === 0) {
            setList(
              list.map((item: any) => {
                item.checked = false;
                return item;
              }),
            );
          }
          break;
        case 'seen':
          if (frontFilters.data.frontFilters.seen.length === 0) {
            setList(
              list.map((item: any) => {
                item.checked = false;
                return item;
              }),
            );
          }
          break;

        default:
          break;
      }
    }
  }, [frontFilters]);

  const updateList = (list: any) => {
    if (data.typename === 'occommande') {
      client.writeData({
        data: {
          frontFilters: {
            occommande: list,
            seen: frontFilters && frontFilters.data && frontFilters.data.frontFilters.seen,
            datefilter:
              frontFilters && frontFilters.data && frontFilters.data.frontFilters.datefilter,
            __typename: 'localFront',
          },
        },
      });
    }
    if (data.typename === 'seen') {
      client.writeData({
        data: {
          frontFilters: {
            occommande:
              frontFilters && frontFilters.data && frontFilters.data.frontFilters.occommande,
            seen: list,
            datefilter:
              frontFilters && frontFilters.data && frontFilters.data.frontFilters.datefilter,
            __typename: 'localFront',
          },
        },
      });
    }
  };
  const onGetValue = (id: any) => {
    const newList = list.map((item: any) => {
      if (item && item.id === id) {
        item.checked = !item.checked;
        return item;
      }
      return item;
    });
    setList(newList);
    updateList(list.filter(item => item.checked).map(item => item.value));
  };

  return (
    <div className={classes.root}>
      <Box marginLeft="8px" borderTop="1px solid #E3E3E3" padding="16px 0">
        <Box
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          marginBottom="8px"
          onClick={() => setExpandedMore(!expandedMore)}
        >
          <Typography className={classes.name}>{data.title}</Typography>
          <IconButton size="small" className={classes.expandBtnContainer}>
            {expandedMore ? <ExpandLess /> : <ExpandMore />}
          </IconButton>
        </Box>
        <Box className={classes.noStyle}>
          {expandedMore &&
            list.map(
              (item: any) =>
                item && (
                  <ListItem
                    role={undefined}
                    dense={true}
                    button={true}
                    onClick={() => onGetValue(item.id)}
                    key={item.id}
                  >
                    <Box className={classes.checkBoxLeftName} display="flex" alignItems="center">
                      <Checkbox
                        tabIndex={-1}
                        checked={item.checked || false}
                        disableRipple={true}
                      />
                      <ListItemText className={classes.nom} primary={item.name} />
                    </Box>
                  </ListItem>
                ),
            )}
        </Box>
      </Box>
    </div>
  );
};

export default CustomFrontFilter;
