import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
    checkBoxLeftName: {
      '& .MuiCheckbox-root': {
        padding: 0,
        color: '#000000',
        marginRight: 8,
      },
      '&$checked': {
        color: '#000000',
      },
    },

    nbrProduits: {
      background: '#F3F3F3',
      fontSize: '0.75rem',
      padding: '2px 8px',
    },
    name: {
      fontFamily: 'Roboto',
      fontSize: '14px',
      color: '#1D1D1D',
      marginLeft: '16px',
    },
    noStyle: {
      listStyleType: 'none',
    },
    nom: {
      fontFamily: 'Roboto',
      fontSize: '14px',
      color: '#1D1D1D',
    },
    voirButton: {
      background: '#002444',
      textTransform: 'none',
      color: '#FFFFFF',
      height: '24px',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
    },
    searchBox: {
      maxHeight: '200px',
      overflowY: 'scroll',
      marginTop: '16px',
    },
    expandBtnContainer: {
      marginRight: 10,
    },
  }),
);

export default useStyles;
