import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    filterContainerRoot: {
      position: 'sticky',
      top: 0,
    },
    title: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: '18px',
      display: 'flex',
      justifyContent: 'center',
      color: theme.palette.secondary.main,
    },
    ocHeight: {
      height: 'calc(100vh - 282px)',
    },
  }),
);

export default useStyles;
