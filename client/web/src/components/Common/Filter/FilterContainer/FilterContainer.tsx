import React, { FC } from 'react';
import { Box } from '@material-ui/core';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import FilterContent from '../FilterContent';
import SortContent from '../SortContent';
import SearchContent from '../SearchContent';
import useStyles from './styles';
import SideMenuButton from '../../SideMenuButton';

interface FilterContainerProps {
  title?: {
    icon?: any;
    label: string;
  };
  sortParam?: any;
  filterParam?: any;
  placeholder?: string;
  button?: ButtonInterface;
  localisation?: string;
  sortCanal?: boolean;
}

interface ButtonInterface {
  url: string;
  text: string;
  color: string;
  icon: string;
  authorized?: string[];
  userIsAuthorized?: boolean;
}

const FilterContainer: FC<FilterContainerProps & RouteComponentProps<any, any, any>> = ({
  title,
  sortParam,
  filterParam,
  placeholder,
  button,
  sortCanal,
  history,
}) => {
  const classes = useStyles({});
  const pathname = history.location.pathname;

  return (
    <div className={classes.filterContainerRoot}>
      <Box paddingTop="30px">
        {title && (
          <Box className={classes.title}>
            {title.icon}{' '}
            <Box marginLeft="4px" textAlign="center">
              {title.label}
            </Box>
          </Box>
        )}
        {placeholder && <SearchContent placeholder={placeholder} />}
        {button && <SideMenuButton button={button} />}
        {sortParam && <SortContent param={sortParam} />}
      </Box>
      <Box
        className={pathname.startsWith('/operations-commerciales') ? classes.ocHeight : ''}
        overflow="auto"
        height={
          placeholder && button
            ? 'calc(100vh - 355.1px)'
            : placeholder && !button
            ? 'calc(100vh - 282px)'
            : !placeholder && button
            ? 'calc(100vh - 285.5px)'
            : 'calc(100vh - 229.5px)'
        }
      >
        {filterParam && <FilterContent param={filterParam} sortCanal={sortCanal} />}
      </Box>
    </div>
  );
};

export default withRouter(FilterContainer);
