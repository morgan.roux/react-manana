import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    operationResumeRoot: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      height: 50,
      background: '#F5F6FA 0% 0% no-repeat padding-box',
      borderRight: 4,
      padding: '0px 15px',
      '& > p:not(:nth-last-child(1))': {
        marginRight: 15,
      },
    },
    operationResumeItem: {
      fontSize: 12,
      fontWeight: 'normal',
      '& > span': {
        fontWeight: '600',
        fontSize: 14,
      },
    },
  }),
);

export default useStyles;
