import React, { FC } from 'react';
import useStyles from './styles';
import { Typography } from '@material-ui/core';
import moment from 'moment';

interface OperationResumeProps {
  data: any;
}

const OperationResume: FC<OperationResumeProps> = ({ data }) => {
  const classes = useStyles({});
  const { libelle, dateDebut, dateFin, commandeCanal } = data;
  const formattedData = [
    { label: 'Libelle', value: libelle },
    { label: 'Canal', value: (commandeCanal && commandeCanal.libelle) || '' },
    { label: 'Date de début', value: moment(dateDebut).format('DD/MM/YYYY') },
    { label: 'Date de fin', value: moment(dateFin).format('DD/MM/YYYY') },
  ];
  return (
    <div className={classes.operationResumeRoot}>
      <Typography style={{ fontWeight: 'bold' }}>Opération :</Typography>
      {formattedData.map((item, index) => (
        <Typography key={`base_info_resume_item_${index}`} className={classes.operationResumeItem}>
          {`${item.label} : `}
          <span>{item.value}</span>
        </Typography>
      ))}
    </div>
  );
};

export default OperationResume;
