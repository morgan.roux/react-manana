import React, { FC, useState } from 'react';
import useStyles from './styles';
import { TextField, FormControl } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
interface AutoCompletionProps {
  label: string;
  list: any;
  optionType: string;
  name: string;
  handleCurrent: (current: any) => void;
}
const AutoCompletion: FC<AutoCompletionProps> = ({
  handleCurrent,
  optionType,
  list,
  label,
  name,
}) => {
  const classes = useStyles({});

  const [current, setCurrent] = useState<any>();
  return (
    <FormControl>
      <Autocomplete
        className={classes.autocomplete}
        options={list ? list : []}
        disableClearable={true}
        autoSelect={true}
        getOptionLabel={(option: any) => option[optionType]}
        // tslint:disable-next-line: jsx-no-lambda
        renderInput={params => (
          <TextField
            label={label}
            {...params}
            value={current ? current[name] : ''}
            fullWidth={true}
            variant="outlined"
          />
        )}
        // tslint:disable-next-line: jsx-no-lambda
        onChange={(e: any, value: any) => {
          if (e && e.type === 'click' && value && value.id) {
            setCurrent(value);
            handleCurrent(value);
          }
        }}
        // tslint:disable-next-line: jsx-no-lambda
        onInputChange={(e: any, value) => {
          if (e && e.type === 'change') {
            setCurrent({
              [name]: value,
            });
            handleCurrent(null);
          }
        }}
        inputValue={current ? (current[name] ? current[name] : '') : ''}
        // tslint:disable-next-line: jsx-no-lambda
        onFocus={() => {
          setCurrent({
            [name]: '',
          });
          handleCurrent(null);
        }}
      />
    </FormControl>
  );
};

export default AutoCompletion;
