import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    autocomplete: {
      '& .MuiAutocomplete-inputRoot input': {
        padding: '2.5px 14px !important',
      },
    },
  }),
);

export default useStyles;
