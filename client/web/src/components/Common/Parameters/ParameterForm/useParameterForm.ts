import { useState, ChangeEvent, FormEvent, ReactNode } from 'react';
import { ParameterStateInterface } from '../Parameters';
import { useParameters } from '../../../../utils/getValueParameter';
import { useApolloClient } from '@apollo/react-hooks';
import { saveSelectedThemeToApolloCache } from '../../../Main/Content/Theme/ThemePage';

const useParameterForm = (callback: () => void, defaultState?: any) => {
  const [values, setValues] = useState<ParameterStateInterface>(defaultState);

  // Get parameters from Cache
  const parametersList = useParameters();
  const client = useApolloClient();

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    if (e) {
      e.preventDefault();
      e.stopPropagation();
    }
    callback();
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { name, value } = e.target;
    if (e.preventDefault && e.stopPropagation) {
      e.preventDefault();
      e.stopPropagation();
    }
    setValues((prevState: any) => {
      prevState[name] = value;
      return prevState;
    });
  };

  const handleSwitch = (e: React.ChangeEvent<HTMLInputElement>, checked: boolean) => {
    const { name, value } = e.target;
    if (e) {
      e.preventDefault();
      e.stopPropagation();
    }
    setValues((prevState: any) => {
      prevState[name] = checked;
      return prevState;
    });
  };

  const handleChangeSelect = (e: ChangeEvent<any>, child: ReactNode) => {
    const { name, value } = e.target;
    if (e) {
      e.preventDefault();
      e.stopPropagation();
    }
    if (name) {
      if (name.toLowerCase() === 'theme' && value) {
        saveSelectedThemeToApolloCache(parametersList, value, client);
      }
      setValues((prevState: any) => ({ ...prevState, [name]: value }));
    }
  };

  return {
    handleChange,
    handleSwitch,
    handleChangeSelect,
    handleSubmit,
    values,
    setValues,
  };
};

export default useParameterForm;
