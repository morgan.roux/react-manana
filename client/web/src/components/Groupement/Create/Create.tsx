import React, { FC, useContext } from 'react';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import SinglePage from '../../Common/SinglePage';
import { DO_CREATE_GROUPEMENT } from '../../../graphql/Groupement/mutation';
import {
  CREATE_GROUPEMENT,
  CREATE_GROUPEMENTVariables,
} from '../../../graphql/Groupement/types/CREATE_GROUPEMENT';
import { setGroupement, getGroupement, isAuthenticated } from '../../../services/LocalStorage';
import GroupementInterface from '../../../Interface/GroupementInterface';
import { RouteComponentProps } from 'react-router-dom';
import { useMutation } from '@apollo/react-hooks';
import Form from '../Form';
import { Loader } from '../../Dashboard/Content/Loader';
import { SEARCH as SEARCH_QUERY } from '../../../graphql/search/query';
import { SEARCH, SEARCHVariables } from '../../../graphql/search/types/SEARCH';
import { ContentContext, ContentStateInterface } from '../../../AppContext';

const styles = () =>
  createStyles({
    padding: {
      paddingTop: 30,
    },
  });

const Create: FC<RouteComponentProps<any, any, any> & WithStyles> = ({ history, classes }) => {
  const {
    content: { type, page, rowsPerPage, query, sortBy, filterBy },
  } = useContext<ContentStateInterface>(ContentContext);

  const queryVariables: SEARCHVariables = {
    type: [type],
    skip: page * rowsPerPage,
    take: rowsPerPage,
    query,
    sortBy,
    filterBy,
    idPharmacie: '',
  };

  const [createGroupement, { loading }] = useMutation<
    CREATE_GROUPEMENT,
    CREATE_GROUPEMENTVariables
  >(DO_CREATE_GROUPEMENT, {
    update: (cache, { data }) => {
      if (data && data.createGroupement) {
        const req = cache.readQuery<SEARCH, SEARCHVariables>({
          query: SEARCH_QUERY,
          variables: queryVariables,
        });
        if (req && req.search && req.search.data) {
          cache.writeQuery({
            query: SEARCH_QUERY,
            data: {
              search: {
                ...req.search,
                ...{ data: [...req.search.data, ...[data.createGroupement]] },
              },
            },
            variables: queryVariables,
          });
        }
      }
    },
    onCompleted: data => {
      if (data && data.createGroupement) {
        setGroupement(data.createGroupement);
        history.push('/');
      }
    },
  });

  const submitCreate = (data: GroupementInterface) => {
    createGroupement({ variables: { ...data } });
  };

  if (loading) return <Loader />;

  return (
    <div className={classes.padding}>
      <SinglePage title="Créer un nouveau groupement" width="unset">
        <Form action={submitCreate} />
      </SinglePage>
    </div>
  );
};

export default withStyles(styles, { withTheme: true })(Create);
