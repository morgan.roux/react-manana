import React, { FC } from 'react';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import SinglePage from '../../Common/SinglePage';
import Form from '../Form';
import { RouteComponentProps } from 'react-router-dom';
import { DO_UPDATE_GROUPEMENT } from '../../../graphql/Groupement/mutation';
import {
  UPDATE_GROUPEMENT,
  UPDATE_GROUPEMENTVariables,
} from '../../../graphql/Groupement/types/UPDATE_GROUPEMENT';
import { useMutation } from '@apollo/react-hooks';
import { Loader } from '../../Dashboard/Content/Loader';
import GroupementInterface from '../../../Interface/GroupementInterface';

interface UpdateProps {
  location: { state: { id: string } };
}

const styles = () => createStyles({});

const Update: FC<RouteComponentProps<any, any, any> & WithStyles & UpdateProps> = ({
  location,
  history,
}) => {
  console.log(location);

  const [updateGroupement, { loading }] = useMutation<
    UPDATE_GROUPEMENT,
    UPDATE_GROUPEMENTVariables
  >(DO_UPDATE_GROUPEMENT, {
    onCompleted: data => {
      if (data && data.updateGroupement) {
        history.push('/');
      }
    },
  });

  const submitUpdate = (data: GroupementInterface) => {
    if (location && location.state && location.state.id) {
      updateGroupement({ variables: { ...data, id: location.state.id } });
    }
  };

  if (loading) return <Loader />;

  return (
    <SinglePage
      title={`Modification du groupement ${(location && location.state && location.state.nom) ||
        ''}`}
      width="unset"
    >
      <Form defaultData={(location && location.state) || undefined} action={submitUpdate} />
    </SinglePage>
  );
};

export default withStyles(styles, { withTheme: true })(Update);
