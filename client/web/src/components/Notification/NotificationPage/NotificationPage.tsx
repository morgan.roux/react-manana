import React, { FC, Fragment, useState, useEffect } from 'react';
import {
  NOTIFICATIONS,
  NOTIFICATIONSVariables,
  NOTIFICATIONS_notifications,
} from '../../../graphql/Notification/types/NOTIFICATIONS';
import { Waypoint } from 'react-waypoint';
import Box from '@material-ui/core/Box';
import { useStyles } from '../styles';
import Typography from '@material-ui/core/Typography';
import { Loader, LoaderSmall } from '../../Dashboard/Content/Loader';
import { useQuery, useApolloClient } from '@apollo/react-hooks';
import { GET_NOTIFICATIONS } from '../../../graphql/Notification/query';
import ListItem from './ListItem';
import {
  List,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  OutlinedInput,
  Hidden,
} from '@material-ui/core';
import moment, { Moment } from 'moment';
import { DateRangePicker } from '../../Common/DateRangePicker';
import { NotificationOrderByInput } from './../../../types/graphql-global-types';
import { Notifications } from '@material-ui/icons';
import CustomSelect from '../../Common/CustomSelect';

interface DateFilterState {
  dateFilterStart: any;
  dateFilterEnd: any;
}

interface OrderByState {
  label: string;
  value: NotificationOrderByInput;
}

const DATETIME_FORMAT = 'YYYY-MM-DDTLTS';

const initalDateFilter: DateFilterState = {
  dateFilterStart: moment().subtract(7, 'days'),
  dateFilterEnd: moment(),
};

const orderByItems: OrderByState[] = [
  { label: 'Date', value: NotificationOrderByInput.dateCreation_DESC },
  { label: 'Type', value: NotificationOrderByInput.type_ASC },
  { label: 'Non Lu', value: NotificationOrderByInput.seen_ASC },
  { label: 'Lu', value: NotificationOrderByInput.seen_DESC },
];

const reformatedDateTime = (dateTime: Moment, initTime: string) => {
  if (dateTime) {
    return moment(
      moment(dateTime)
        .toISOString()
        .split('T')
        .shift() +
        ' ' +
        initTime,
    ).format(DATETIME_FORMAT);
  }
};

const NotificationPage: FC = () => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [orderBy, setOrderBy] = useState<OrderByState>(orderByItems[0]);
  const [dateFilter, setDateFilter] = useState<DateFilterState>(initalDateFilter);
  const defaultNotifications: NOTIFICATIONS_notifications = {
    data: [],
    total: 0,
    notSeen: 0,
    __typename: 'NotificationResult',
  };
  const [notifications, setNotifications] = useState<NOTIFICATIONS_notifications>(
    defaultNotifications,
  );

  const { loading, data, fetchMore } = useQuery<NOTIFICATIONS, NOTIFICATIONSVariables>(
    GET_NOTIFICATIONS,
    {
      fetchPolicy: 'cache-and-network',
      variables:
        window.innerWidth > 600
          ? {
              first: 10,
              skip: 0,
              orderBy: orderBy.value as NotificationOrderByInput,
              dateFilterStart: reformatedDateTime(dateFilter.dateFilterStart, '00:00:00'),
              dateFilterEnd: reformatedDateTime(dateFilter.dateFilterEnd, '23:59:00'),
            }
          : {
              skip: 0,
              orderBy: orderBy.value as NotificationOrderByInput,
            },
    },
  );

  /**
   * Set notifications
   */
  useEffect(() => {
    if (data && data.notifications) {
      setNotifications(data.notifications);
    }
  }, [data]);

  useEffect(() => {
    if (dateFilter.dateFilterStart && dateFilter.dateFilterEnd) {
      fetchMore({
        variables:
          window.innerWidth > 600
            ? {
                first: 10,
                skip: 0,
                orderBy: orderBy.value as NotificationOrderByInput,
                dateFilterStart: reformatedDateTime(dateFilter.dateFilterStart, '00:00:00'),
                dateFilterEnd: reformatedDateTime(dateFilter.dateFilterEnd, '23:59:00'),
              }
            : {
                skip: 0,
                orderBy: orderBy.value as NotificationOrderByInput,
              },
        updateQuery: prev => {
          return prev;
        },
      });
    }
  }, [orderBy, dateFilter]);

  const getDates = (dateStart: Moment | null, dateEnd: Moment | null) => {
    setDateFilter({
      dateFilterStart: dateStart,
      dateFilterEnd: dateEnd,
    });
  };

  const onHandleSelect = (event: any) => {
    setOrderBy({
      label: orderByItems.filter(item => item.value === event.target.value)[0].label,
      value: event.target.value,
    });
  };

  const isOutsideRange = (day: Moment) =>
    moment()
      .add(1, 'days')
      .diff(day) < 0;

  const fetchMoreNotifications = () => {
    fetchMore({
      variables: {
        first: 10,
        skip:
          data && data.notifications && data.notifications.data
            ? data.notifications.data.length
            : 0,
        orderBy: orderBy.value as NotificationOrderByInput,
        dateFilterStart: reformatedDateTime(dateFilter.dateFilterStart, '00:00:00'),
        dateFilterEnd: reformatedDateTime(dateFilter.dateFilterEnd, '23:59:00'),
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev;
        return {
          notifications: {
            __typename: 'NotificationResult',
            data:
              prev &&
              prev.notifications &&
              prev.notifications.data &&
              fetchMoreResult.notifications &&
              fetchMoreResult.notifications.data
                ? [...prev.notifications.data, ...fetchMoreResult.notifications.data]
                : [],
            total: data && data.notifications ? data.notifications.total : 0,
            notSeen: data && data.notifications ? data.notifications.notSeen : 0,
          },
        };
      },
    });
  };

  if (loading && !data) return <Loader />;

  return (
    <div className={classes.fullPageContainer}>
      <Hidden mdDown={true}>
        <div className={classes.notifLeftSidebar}>
          <Typography className={classes.TitlePage}>
            <Notifications />
            Notifications
          </Typography>
          <div className={classes.filterSection}>
            <div className={classes.filterByInput}>
              <DateRangePicker
                startDate={dateFilter.dateFilterStart}
                startDateId="startDateUniqueId"
                endDate={dateFilter.dateFilterEnd}
                endDateId="endDateUniqueId"
                getDates={getDates}
                label="Filtrer par période"
                variant="outlined"
                isOutsideRange={isOutsideRange}
                showDefaultInputIcon={false}
              />
            </div>
            <CustomSelect
              label="Trier par"
              list={orderByItems}
              listId="value"
              index="label"
              value={orderBy.value}
              onChange={onHandleSelect}
            />
          </div>
        </div>
      </Hidden>

      <div className={classes.notifMainContent}>
        {notifications && notifications.data && notifications.data.length > 0 ? (
          <>
            <List component="nav" aria-label="listes notifications">
              {notifications &&
                notifications.data.map((notification, index) => {
                  return (
                    notification && (
                      <Fragment key={`notif_${index}`}>
                        <ListItem notification={notification} />
                        {notifications &&
                          notifications.data &&
                          index === notifications.data.length - 1 &&
                          notifications.data.length !== notifications.total && (
                            <Waypoint onEnter={fetchMoreNotifications} />
                          )}
                      </Fragment>
                    )
                  );
                })}
            </List>
            <div style={{ display: 'flex', margin: '15px 0px' }}>
              {loading && !data && <LoaderSmall />}
            </div>
          </>
        ) : (
          <div className={classes.emptyNotification}>Aucune notification</div>
        )}
      </div>
    </div>
  );
};

export default NotificationPage;
