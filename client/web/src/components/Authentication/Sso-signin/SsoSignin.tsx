import { FC } from 'react';
import { getAccessToken, getGroupement } from '../../../services/LocalStorage';
import { useLocation } from 'react-router';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { APP_SSO_URL } from '../../../config';

const SsoSignIn: FC<object & RouteComponentProps> = ({ history }) => {
  const groupement = getGroupement();
  const location = useLocation();
  const user = getAccessToken();

  if (!user || !groupement) {
    history.push(`/signin${location.search}`);
    return null;
  }

  const url = `${APP_SSO_URL}/open-helloid/${(groupement && groupement.id) || ''}/${user}${
    location.search
  }`;
  window.location.href = url;
  return null;
};
export default withRouter(SsoSignIn);
