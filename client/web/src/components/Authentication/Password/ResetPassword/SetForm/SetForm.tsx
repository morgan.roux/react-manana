import React, { useState, ChangeEvent, FC } from 'react';
import { Redirect, RouteComponentProps, withRouter } from 'react-router-dom';
import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { withStyles, createStyles } from '@material-ui/core/styles';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import CustomButton from '../../../../Common/CustomButton';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import { USER_RESET_PASSWORD } from '../../../../../graphql/Authentication/mutation';
import {
  RESET_PASSWORD,
  RESET_PASSWORDVariables,
} from '../../../../../graphql/Authentication/types/RESET_PASSWORD';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { clearLocalStorage } from '../../../../../services/LocalStorage';
import { isValidPassword } from '../../../../../utils/Validator';
import { FormHelperText } from '@material-ui/core';
import BackLink from './../../BackLink'

const styles = () =>
  createStyles({
    msgError: {
      lineHeight: '1.5em',
      letterSpacing: '0.095em',
    },
  });

interface SET_FORM_PROPS {
  classes: {
    msgError: string;
  };
  token: string;
  saveBtn?: string;
  type?: string;
  message?: string;
}

const snackMessage = ({ type, message, isOpen }: SnackVariableInterface) => {
  return {
    type,
    message,
    isOpen,
  };
};

const SetForm: FC<SET_FORM_PROPS & RouteComponentProps<any, any, any>> = ({
  saveBtn,
  token,
  message,
  classes,
  history,
  type
}) => {
  const [password, setPassword] = useState<string>('');
  const [confirmPassword, setConfirmPassword] = useState<string>('');
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const successSnackBar: SnackVariableInterface = snackMessage({
    type: 'SUCCESS',
    message,
    isOpen: true,
  });
  const errorSnackBar: SnackVariableInterface = snackMessage({
    type: 'ERROR',
    message: 'TOKEN INVALID',
    isOpen: true,
  });
  const [showConfirmPassword, setShowConfirmPassword] = useState<boolean>(false);
  const [resetPassword, { data }] = useMutation<RESET_PASSWORD, RESET_PASSWORDVariables>(
    USER_RESET_PASSWORD,
    {
      onCompleted: data => {
        clearLocalStorage();
        resetAllValue();
        displaySnackBar(client, successSnackBar);
        history.push('/');
      },
      onError: error => {
        displaySnackBar(client, errorSnackBar);
      },
    },
  );

  const client = useApolloClient();

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    name === 'password' ? setPassword(value) : setConfirmPassword(value);
  };

  const resetAllValue = () => {
    setPassword('');
    setConfirmPassword('');
    setShowConfirmPassword(false);
    setShowPassword(false);
  };

  // function reset password
  const submitSetPassword = async () => {
    resetPassword({ variables: { password, token } });
  };

  const tooglePasswordVisibility = () => {
    showPassword ? setShowPassword(false) : setShowPassword(true);
  };

  const toogleConfirmPasswordVisibility = () => {
    showConfirmPassword ? setShowConfirmPassword(false) : setShowConfirmPassword(true);
  };

  const isDisabled = () => {
    if (!password || !confirmPassword) return true;
    if (password && !isValidPassword(password)) return true;
    const status = password === confirmPassword ? false : true;
    return status;
  };

  const error = password && password !== confirmPassword ? true : false;

  return (
    <>
      <div style={{ marginBottom: 35 }}>
        <CustomFormTextField
          name="password"
          type={showPassword ? 'text' : 'password'}
          label="Mot de passe"
          value={password}
          onChange={handleChange}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={tooglePasswordVisibility}>
                  {showPassword ? <VisibilityIcon /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        {password && !isValidPassword(password) && (
          <FormHelperText error={true} className={classes.msgError}>
            Le mot de passe doit contenir au moins 8 caractères de types différents (majuscules,
            minuscules, chiffres, caractères spéciaux)
          </FormHelperText>
        )}
      </div>
      <div style={{ marginBottom: 45 }}>
        <CustomFormTextField
          name="confirmPassword"
          type={showConfirmPassword ? 'text' : 'password'}
          label=" Confirmer le mot de passe"
          value={confirmPassword}
          onChange={handleChange}
          error={error}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={toogleConfirmPasswordVisibility}>
                  {showConfirmPassword ? <VisibilityIcon /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        {password && confirmPassword && password !== confirmPassword && (
          <FormHelperText error={true} className={classes.msgError}>
            Les deux mots de passe ne sont pas identiques
          </FormHelperText>
        )}
      </div>
      <BackLink />
      <CustomButton
        size="large"
        color="secondary"
        onClick={submitSetPassword}
        fullWidth={true}
        disabled={isDisabled()}
      >
        {saveBtn}
      </CustomButton>
    </>
  );
};

export default withStyles(styles, { withTheme: true })(withRouter(SetForm));

SetForm.defaultProps = {
  saveBtn: 'REINITIALISER',
  type: 'reset',
  message: 'Votre mot de passe a été réinitialisé avec succès',
};
