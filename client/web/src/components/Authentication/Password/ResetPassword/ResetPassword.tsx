import React, { FC } from 'react';
import { withStyles, createStyles } from '@material-ui/core/styles';
import SinglePage from '../../../Common/SinglePage';
import { SetForm } from './SetForm';

const styles = () => createStyles({});

interface PROPS {
  match: object;
}

const ResetPassword: FC<object> = (props: any) => {
  const { match } = props;
  return (
    <SinglePage title="Nouveau mot de passe">
      <SetForm token={match.params.token} />
    </SinglePage>
  );
};

export default withStyles(styles, { withTheme: true })(ResetPassword);
