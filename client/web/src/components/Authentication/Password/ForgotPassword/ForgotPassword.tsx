import React, { FC } from 'react';
import SinglePage from '../../../Common/SinglePage';
import useStyles from './styles';
import ForgotForm from './ForgotForm';

const ForgotPassword: FC = () => {
  const classes = useStyles({});

  return (
    <SinglePage title="Pas de souci, cela peut arriver à tout le monde !" width="100%">
      <ForgotForm />
    </SinglePage>
  );
};

export default ForgotPassword;
