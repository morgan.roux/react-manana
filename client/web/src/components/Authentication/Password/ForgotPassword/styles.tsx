import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() =>
  createStyles({
    form: {
      display: 'flex',
      flexDirection: 'column',
      width: 390,
      '@media (max-width: 767px)': {
        width: '100%',
      },
    },
    formControl: {
      marginBottom: '30px!important',
      '& fieldset': {
        border: 0,
      },
    },
    contentForm: {
      display: 'flex',
      justifyContent: 'center',
    },
    message: {
      textAlign: 'left',
      letterSpacing: 0,
      color: '#1D1D1D',
      opacity: 1,
      fontSize: 14,
      marginBottom: 25,
    },
    btnSend: {
      fontSize: '13px',
      textTransform: 'uppercase',
      minHeight: 45,
      minWidth: 175,
      width: '100%',
      fontWeight: 400,
      background:
        'transparent linear-gradient(239deg, #E34741 0%, #E34168 100%) 0% 0% no-repeat padding-box',
      boxShadow: '0px 3px 6px #00000029',
      borderRadius: 3,
      opacity: 1,
      color: '#fff',
    },
  }),
);

export default useStyles;
