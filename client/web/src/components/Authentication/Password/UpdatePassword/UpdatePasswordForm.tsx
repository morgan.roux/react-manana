import React, { useState, ChangeEvent, FC } from 'react';
import { Redirect, withRouter, RouteComponentProps } from 'react-router-dom';
import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { FormHelperText } from '@material-ui/core';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import CustomButton from '../../../Common/CustomButton';
import { CustomFormTextField } from '../../../Common/CustomTextField';
import { USER_UPDATE_PASSWORD } from '../../../../graphql/Authentication/mutation';
import {
  UPDATE_PASSWORD,
  UPDATE_PASSWORDVariables,
} from '../../../../graphql/Authentication/types/UPDATE_PASSWORD';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { isValidPassword } from '../../../../utils/Validator';
import { setUser, getUser } from '../../../../services/LocalStorage';
import { SUPER_ADMINISTRATEUR } from '../../../../Constant/roles';
import BackLink from '../BackLink';

interface UpdatePasswordState {
  password: string;
  confirmPassword: string;
  showPassword: boolean;
}

const initialState: UpdatePasswordState = {
  password: '',
  confirmPassword: '',
  showPassword: false,
};

const UpdatePasswordForm: FC<RouteComponentProps<any, any, any>> = props => {
  const client = useApolloClient();

  const [{ password, confirmPassword, showPassword }, setState] = useState<UpdatePasswordState>(
    initialState,
  );

  const [updatePassword, { loading, data }] = useMutation<
    UPDATE_PASSWORD,
    UPDATE_PASSWORDVariables
  >(USER_UPDATE_PASSWORD, {
    update: (_, { data }) => {
      if (data && data.updatePassword) {
        const userFromLocal = getUser();
        userFromLocal.lastPasswordChangedDate = Date.now();
        userFromLocal.status = 'ACTIVATED';
        setUser(userFromLocal);
        props.history.push('/');
      }
    },
    onCompleted: data => {
      const snackBarData: SnackVariableInterface = {
        type: 'SUCCESS',
        message: 'Votre mot de passe a été mis à jour avec succès',
        isOpen: true,
      };
      resetForm();
      displaySnackBar(client, snackBarData);
    },
    onError: error => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: error.message,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const handleChange = (e: ChangeEvent<HTMLInputElement>): void => {
    const { name, value } = e.target;
    setState(prevState => ({ ...prevState, [name]: value }));
  };

  const resetForm = (): void => {
    setState(initialState);
  };

  const handleSubmit = (): void => {
    updatePassword({
      variables: { newPassword: password },
    });
  };

  const tooglePasswordVisibility = (): void => {
    setState(prevState => ({ ...prevState, showPassword: !prevState.showPassword }));
  };

  const isDisabled = (): boolean => {
    if (!isValidPassword(password) || !(password === confirmPassword)) return true;
    return false;
  };

  if (data && data.updatePassword) {
    const currentUser = getUser();
    if (
      currentUser &&
      currentUser.role &&
      currentUser.role.code &&
      currentUser.role.code === SUPER_ADMINISTRATEUR
    ) {
      return <Redirect to="/groupement/" />;
    }
    currentUser.status = 'ACTIVATED';
    setUser(currentUser);
    return <Redirect to="/" />;
  }

  return (
    <>
      <div style={{ marginBottom: 35 }}>
        <CustomFormTextField
          name="password"
          type={showPassword ? 'text' : 'password'}
          label="Nouveau mot de passe"
          value={password}
          onChange={handleChange}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={tooglePasswordVisibility}>
                  {showPassword ? <VisibilityIcon /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        {password && !isValidPassword(password) && (
          <FormHelperText error={true} style={{ lineHeight: '1.5em', letterSpacing: '0.095em' }}>
            Le mot de passe doit contenir au moins 8 caractères de types différents (majuscules,
            minuscules, chiffres, caractères spéciaux)
          </FormHelperText>
        )}
      </div>

      <div style={{ marginBottom: 45 }}>
        <CustomFormTextField
          name="confirmPassword"
          type={showPassword ? 'text' : 'password'}
          label="Confirmer le nouveau mot de passe"
          value={confirmPassword}
          onChange={handleChange}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={tooglePasswordVisibility}>
                  {showPassword ? <VisibilityIcon /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        {password && confirmPassword && password !== confirmPassword && (
          <FormHelperText error={true} style={{ lineHeight: '1.5em', letterSpacing: '0.095em' }}>
            Les deux mots de passe ne sont pas identiques
          </FormHelperText>
        )}
      </div>

      <BackLink />
      <CustomButton
        size="large"
        color="secondary"
        onClick={handleSubmit}
        fullWidth={true}
        disabled={isDisabled() || loading}
      >
        Mettre à jour
      </CustomButton>
    </>
  );
};

export default withRouter(UpdatePasswordForm);
