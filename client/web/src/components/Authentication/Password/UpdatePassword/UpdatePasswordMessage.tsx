import React from 'react';
import useStyles from './style';

const UpdatePasswordMessage = (props: any) => {
  const { user } = props;
  const classes = useStyles({});
  const msg =
    'Pour assurer la securité de votre compte, vous devez mettre à jour votre mot de passe.';
  return user ? (
    user.lastPasswordChangedDate === null ? (
      <div className={classes.message}>
        Votre mot de passe est encore un mot de passe par défaut. {msg}
      </div>
    ) : (
      <div className={classes.message}>
        Votre mot de passe n'a pas été changé depuis longtemps. {msg}
      </div>
    )
  ) : null;
};

export default UpdatePasswordMessage;
