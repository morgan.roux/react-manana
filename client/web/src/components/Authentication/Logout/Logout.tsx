import React, { useEffect, useState } from 'react';
import { clearLocalStorage } from '../../../services/LocalStorage';
import { Redirect } from 'react-router-dom';

const Logout = () => {
  const [disconnected, setDisconnected] = useState(false);
  useEffect(() => {
    clearLocalStorage();
    setDisconnected(true);
  }, []);

  if (disconnected) {
    return <Redirect to="/" />;
  }

  return <div>Déconnexion...</div>;
};
export default Logout;
