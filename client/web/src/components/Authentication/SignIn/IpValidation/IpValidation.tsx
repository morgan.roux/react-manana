import React, { useEffect } from 'react';
import { ConnexionStatut } from '../../../../types/graphql-global-types';
import { useQuery } from '@apollo/react-hooks';
import { GET_IP_VALIDATION } from '../../../../graphql/Authentication/query';
import { IP_VALIDATION as IP_VALIDATION_INTERFACE } from '../../../../graphql/Authentication/types/IP_VALIDATION';
import { setAccessToken, getAccessToken, isAuthenticated } from '../../../../services/LocalStorage';
import { RouteComponentProps, Redirect, withRouter } from 'react-router-dom';

interface IpValidationProps {
  match: {
    params: { token: string; status: ConnexionStatut };
  };
}

const IpValidation = (props: IpValidationProps & RouteComponentProps) => {
  const {
    match: {
      params: { token, status },
    },
    history,
  } = props;

  const { loading, error, data } = useQuery<IP_VALIDATION_INTERFACE>(GET_IP_VALIDATION, {
    variables: {
      token,
      status,
    },

    fetchPolicy: 'cache-and-network',
  });

  useEffect(() => {
    if (!loading && !error) {
      if (
        status === 'AUTHORIZED' &&
        data &&
        data.validateUserIp &&
        data.validateUserIp.accessToken
      ) {
        setAccessToken(data.validateUserIp.accessToken);
        history.push('/');
      } else history.push('/signin');
    }
  }, [status, loading, data]);

  if (getAccessToken() && isAuthenticated()) {
    return <Redirect to="/" />;
  }

  return <></>;
};

export default withRouter(IpValidation);
