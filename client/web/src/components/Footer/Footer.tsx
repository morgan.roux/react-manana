import React, { FC } from 'react';
import { useStyles } from './styles';
import Copyright from '../Copyright';

const Footer: FC = () => {
  const classes = useStyles({});
  return (
    <footer className={classes.root}>
      <Copyright />
    </footer>
  );
};

export default Footer;
