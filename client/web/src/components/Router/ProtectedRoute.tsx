import React, { FC } from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import Unauthorized from '../Unauthorized';

export interface ProtectedRouteProps extends RouteProps {
  isAuthorized: boolean;
  redirectPath?: string;
}

const ProtectedRoute: FC<ProtectedRouteProps> = props => {
  const defaultRedirectPath = '/unauthorized';
  const { isAuthorized, redirectPath } = props;

  if (!isAuthorized) {
    const renderComponent = () => (
      <Redirect to={{ pathname: redirectPath ? redirectPath : defaultRedirectPath }} />
    );
    return <Route {...props} component={renderComponent} render={props => <Unauthorized />} />;
  } else {
    return <Route {...props} />;
  }
};

export default ProtectedRoute;
