# Use Node 12
FROM node:10-alpine

ENV NODE_OPTIONS "--max-old-space-size=2048"

WORKDIR /app

# Copy package.json for caching node_modules
COPY package.json /app/

# Copy yarn.lock for packages consistency
COPY yarn.lock /app/

RUN yarn

COPY . /app/

EXPOSE 3000

CMD ["yarn", "start"]
