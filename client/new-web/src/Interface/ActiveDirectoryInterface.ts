import { LdapType } from '../types/graphql-global-types'
export default interface IActiveDirectory {
    id: string;
    idGroupement: string;
    url: string;
    user: string;
    password: string;
    base: string;
    ldapType: LdapType;
  }
  