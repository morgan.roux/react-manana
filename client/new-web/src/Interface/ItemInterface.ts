export interface Item {
  id: string;
  code: string;
  name: string;
  dateCreation: Date;
  dateDerniereModification: Date;
}
