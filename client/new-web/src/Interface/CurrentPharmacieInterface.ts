import { PharmacieMinimInfo } from '../graphql/Pharmacie/types/PharmacieMinimInfo';

export default interface ICurrentPharmacieInterface {
  pharmacie: PharmacieMinimInfo;
}
