import { Panier } from '../graphql/Panier/types/Panier';
export default interface ICurrentPanierInterface {
  panier: Panier | null;
}
