export default interface IFtp {
    id: string;
    idGroupement: string;
    host: string;
    port: number;
    user: string;
    password: string;
  }
  