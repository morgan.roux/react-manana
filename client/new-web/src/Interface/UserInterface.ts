import IGroupement from './GroupementInterface';

export default interface User {
  type: String;
  id: String;
  email: String;
  status: String;
  emailConfirmed: Boolean;
  password: String;
  passwordSecretAnswer: String;
  phoneNumber: String;
  phoneNumberConfirmed: Boolean;
  twoFactorEnabled: Boolean;
  lockoutEndDateUtc: Date;
  lockoutEnabled: Boolean;
  accessFailedCount: Number;
  userName: String;
  lastLoginDate: Date;
  lastPasswordChangedDate: Date;
  isLockedOut: Boolean;
  isLockedOutPermanent: Boolean;
  isObligationChangePassword: Boolean;
  accessFailedCountBeforeLockoutPermanent: Number;
  dateCreation: Date;
  dateModification: Date;
  groupement: IGroupement;
  idGroupement: String;
}
