export default interface IHelloidSso {
    id: string;
    idGroupement: string;
    helloIdUrl: string;
    helloIDConsumerUrl: string;
    x509Certificate: string;
    apiKey: string;
    apiPass: string;
  }
  