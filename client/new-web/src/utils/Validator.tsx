import { MINYEAR } from '../Constant/date';
import moment from 'moment';

export const isEmailValid = (email: string): boolean => {
  if (email.length > 0) {
    const regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
    return email !== undefined && regexEmail.test(email.trim());
  }
  return true;
};

export const isValidPassword = (password: string): boolean => {
  return /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/.test(password) ? true : false;
};

const testNumber = /^[0-9]+$/;

export const isValidYears = (year: number | string): boolean => {
  const y = parseInt(year.toString(), 10);
  if (y === 0) return false;
  if (y) {
    if (!testNumber.test(y.toString())) {
      return false;
    }

    if (y.toString().length !== 4) {
      return false;
    }

    const currentYear = new Date().getFullYear();
    if (y < MINYEAR || y > currentYear) {
      return false;
    }

    return true;
  }

  return true;
};

const daysInMonth = (month, year) => {
  return moment(`${year}-${month}`, 'YYYY-MM').daysInMonth();
};

export const isValidDays = (days: number | string, month: number, year?: string) => {
  const d = parseInt(days.toString(), 10);
  if (d === 0) return false;
  if (d && month) {
    return month >= 1 && month <= 12 && d > 0 && d <= daysInMonth(month, year);
  }
  return true;
};

export const isUrlValid = (url: string): boolean => {
  const res = url.match(
    /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm,
  );
  return res === null ? false : true;
};
