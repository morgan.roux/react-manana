import SnackVariableInterface from '../Interface/SnackVariableInterface';
import { ApolloClient} from '@apollo/client';
import { GET_SNACKBAR_STATE } from '../graphql/Common/snackbar/query';

export const displaySnackBar = (client: ApolloClient<any>, data: SnackVariableInterface) => {
  client.writeQuery({
  query: GET_SNACKBAR_STATE,
    data: {
      snackBar: { ...data, __typename: 'SnackBar' },
    },
  });
};
