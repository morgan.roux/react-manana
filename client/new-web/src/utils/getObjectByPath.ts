export const getObjectByPath = (obj, path) => {
  const splitedPath = path.split('.');
  try {
    for (let i = 0, path = splitedPath, len = path.length; i < len; i++) {
      obj = obj[path[i]];
    }
    return obj;
  } catch (e) {
    return null;
  }
};
