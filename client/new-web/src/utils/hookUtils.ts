import { ChangeEvent, KeyboardEvent } from 'react';
import CallbackFunction from '../types/CallbackFunction';

export const handleChangeTextInput = (setSearch: CallbackFunction<string>) => (
  evt: ChangeEvent<HTMLInputElement>,
) => {
  setSearch(evt.target.value);
};

export const handleSetValue = <T = undefined>(setValue: (value: T) => void, value: T) => () => {
  setValue(value);
};

export const handleFormEnterKeyPress = (handleSubmit?: () => void) => (
  evt: KeyboardEvent<HTMLFormElement | HTMLDivElement>,
) => {
  if (evt.key === 'Enter' && handleSubmit) {
    evt.preventDefault();
    handleSubmit();
  }
};
