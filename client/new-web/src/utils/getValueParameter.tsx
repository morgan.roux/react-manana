import { DO_GET_PARAMETER_LOCAL } from '../graphql/Parametre/local';
import { useQuery } from '@apollo/client';
import { ParameterInfo } from '../graphql/Parametre/types/ParameterInfo';

export const useParameters = () => {
  const parameters = useQuery(DO_GET_PARAMETER_LOCAL);
  return parameters && parameters.data && parameters.data.parameters;
};

export interface ParameterValue {
  id: string;
  value: string;
}

export const useParameter = (code: string) => {
  const getparameter = useParameters();
  if (getparameter) {
    const result = getparameter.find((parameter: ParameterInfo) => parameter.code === code);
    if (result) return result;
  }
  return null;
};

export const useValueParameter = (code: string) => {
  const getparameter = useParameters();

  if (getparameter) {
    const result = getparameter.find((parameter: ParameterInfo) => parameter.code === code);
    if (result) {
      const value = result.value && result.value.value ? result.value.value : result.defaultValue;
      return value;
    }
  }

  return null;
};

export const useValueParameterAsBoolean = (code: string) => {
  try {
    return 'true' === useValueParameter(code);
  } catch (e) {
    return false;
  }
};

export const useValueParameterByView = (codeGroupement: string, codePharmacie: string, isMobile: boolean) => {
  const valueGroupement = useValueParameter(codeGroupement);
  let valuePharmacie = useValueParameter(codePharmacie);

  try {

    if (valuePharmacie === 'disabled' || valueGroupement === 'disabled') {
      return false;
    } else if (valueGroupement === 'all') {
      return !((valuePharmacie === 'mobile' && !isMobile) || (valuePharmacie === 'web' && isMobile));
    }

    return (valueGroupement === 'mobile' && isMobile) || (valueGroupement === 'web' && !isMobile)

  } catch (e) {

  }
  return false;

};

export const useValueParameterAsMoney = (code: string) => {
  try {
    const value = useValueParameter(code);
    return parseFloat(value).toFixed(2);
  } catch (e) {
    return 0;
  }
};

export const useValueParameterAsString = (code: string) => {
  try {
    return useValueParameter(code) as string;
  } catch (e) {
    return '';
  }
};

export const useValueParameterAsNumber = (code: string) => {
  try {
    return useValueParameter(code) * 1;
  } catch (e) {
    return 0;
  }
};

export const useValueParameterAsDate = (code: string) => {
  try {
    return useValueParameter(code).toDateString();
  } catch (e) {
    return null;
  }
};
