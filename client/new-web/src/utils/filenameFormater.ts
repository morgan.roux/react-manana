import moment from 'moment';

export const formatFilename = (file: File): string => {
  const cleanFilename = file.name.toLowerCase().replace(/[^a-z0-9]/g, '-');
  const extension = file.name.split('.').pop();
  const newFilename = `files/${moment().format(
    'YYYY-MM-DD-h-mm-ss',
  )}-${cleanFilename}.${extension}`;
  return newFilename.replace(/ |-|\)|\(/g, '');
};

export const formatFilenameWithoutDate = (file: File): string => {
  const cleanFilename = file.name.toLowerCase().replace(/[^a-z0-9]/g, '-');
  const extension = file.name.split('.').pop();
  const newFilename = `${cleanFilename}.${extension}`;
  return newFilename.replace(/ |-|\)|\(/g, '');
};
