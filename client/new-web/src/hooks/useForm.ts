/**
 * * IMPORTANT : ADD ANOTHERS HANDLER IF YOU NEED A SPECIFIC HANDLER FOR YOUR INPUT
 */

import { ChangeEvent, useState } from 'react';

export interface FormStateInterface {
  [key: string]: any;
}

/**
 * Hooks for form management
 *
 * @param initialState
 */
export const useForm = (initialState: FormStateInterface) => {
  const [formState, setFormState] = useState<FormStateInterface>(initialState);

  /**
   * Handler for classic input
   *
   * @param {(ChangeEvent<HTMLTextAreaElement | HTMLInputElement>)} event
   */
  const handleChangeInput = (event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormState(prev => ({ ...prev, [name]: value }));
  };

  /**
   * Handler for input date on material UI
   *
   * @param {string} name
   */
  const handleChangeDate = (name: string) => (date: Date | null) => {
    setFormState(prev => ({ ...prev, [name]: date || new Date() }));
  };

  /**
   * Handler for select input on material UI
   *
   * @param {React.ChangeEvent<{ name?: string; value: unknown }>} event
   */
  const handleChangeSelect = (event: React.ChangeEvent<{ name?: string; value: unknown }>) => {
    const name = event.target.name as keyof typeof formState;
    setFormState({
      ...formState,
      [name]: event.target.value,
    });
  };

  /**
   * Function to init form
   *
   * @param {FormStateInterface} initialState
   */
  const initFormState = (initialState: FormStateInterface) => {
    setFormState(initialState);
  };

  /**
   * Function to update a specific field
   *
   * @param {string} fieldName
   * @param {*} fieldValue
   */
  const updateField = (fieldName: string, fieldValue: any) => {
    setFormState(prev => ({ ...prev, [fieldName]: fieldValue }));
  };

  return {
    formState,
    handleChangeInput,
    handleChangeDate,
    handleChangeSelect,
    initFormState,
    updateField,
  };
};
