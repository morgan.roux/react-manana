import { isEnv } from 'apollo-utilities';

export const API_URL = process.env.REACT_APP_API_URL;
export const APP_WS_URL = process.env.REACT_APP_WS_URL;
export const APP_SSO_URL = process.env.REACT_APP_SAML_SSO_ENDPOINT;
export const IP_API_ADDRESS = process.env.REACT_APP_IP_API_ADDRESS;
export const CHROME_EXTENSION_ID =  process.env.REACT_APP_CHROME_EXTENSION_ID;
export const FIREFOX_EXTENSION_LINK =  process.env.REACT_APP_FIREFOX_EXTENSION_LINK;
export const GATEWAY_FEDERATION_URL = process.env.REACT_APP_GATEWAY_FEDERATION_URL;
export const AWS_HOST = process.env.REACT_APP_AWS_HOST || 'https://pharmacie.s3-eu-west-3.amazonaws.com';
export const CURRENT_VERSION = process.env.REACT_APP_CURRENT_VERSION || 'v1.0.0';


