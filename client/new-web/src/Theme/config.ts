import { ThemeOptions } from '@material-ui/core/styles/createMuiTheme';

const availableThemeNames = [
  'onyx',
  'raspberry',
  'caramel',
  'sky',
  'leaf',
  'magic',
  'angel',
] as const;

export type ThemeName = typeof availableThemeNames[number];
type Theme = {
  [key in ThemeName]: ThemeOptions;
};

const common = {
  black: '#1D1D1D',
  white: '#ffffff',
};

const breakpoints = {
  values: {
    xxs: 0,
    xs: 411,
    sm: 600,
    md: 960,
    lg: 1280,
    xl: 1920,
  },
};

const availableThemes: Theme = {
  onyx: {
    palette: {
      type: 'light',
      primary: {
        main: '#27272F',
      },
      secondary: {
        main: '#E34168',
      },
      text: {
        primary: '#373740',
      },
      common,
    },
    breakpoints,
  },
  raspberry: {
    palette: {
      type: 'light',
      primary: {
        main: '#9A275A',
      },
      secondary: {
        main: '#9A275A',
      },
      text: {
        primary: '#373740',
      },
      common,
    },
    breakpoints,
  },
  caramel: {
    palette: {
      type: 'light',
      primary: {
        main: '#AA5042',
      },
      secondary: {
        main: '#AA5042',
      },
      text: {
        primary: '#373740',
      },
      common,
    },
    breakpoints,
  },
  sky: {
    palette: {
      type: 'light',
      primary: {
        main: '#3171AC',
      },
      secondary: {
        main: '#3171AC',
      },
      text: {
        primary: '#373740',
      },
      common,
    },
    breakpoints,
  },
  leaf: {
    palette: {
      type: 'light',
      primary: {
        main: '#228B5D',
      },
      secondary: {
        main: '#228B5D',
      },
      text: {
        primary: '#373740',
      },
      common,
    },
    breakpoints,
  },
  magic: {
    palette: {
      type: 'light',
      primary: {
        main: '#89469F',
      },
      secondary: {
        main: '#89469F',
      },
      text: {
        primary: '#373740',
      },
      common,
    },
    breakpoints,
  },
  angel: {
    palette: {
      type: 'light',
      primary: {
        main: '#F34F9C',
      },
      secondary: {
        main: '#F34F9C',
      },
      text: {
        primary: '#373740',
      },
      common,
    },
    breakpoints,
  },
};

export { availableThemes };
