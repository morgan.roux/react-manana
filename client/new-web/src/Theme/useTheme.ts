import React from 'react';
import DispatchContext from './ThemeContext'

const useTheme = () => {
    return React.useContext(DispatchContext);
}

export default useTheme