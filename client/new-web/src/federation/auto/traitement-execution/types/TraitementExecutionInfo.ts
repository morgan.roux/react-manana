/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TraitementExecutionInfo
// ====================================================

export interface TraitementExecutionInfo_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface TraitementExecutionInfo_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface TraitementExecutionInfo_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface TraitementExecutionInfo_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface TraitementExecutionInfo {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: TraitementExecutionInfo_dernierChangementStatut | null;
  changementStatuts: TraitementExecutionInfo_changementStatuts[];
  urgence: TraitementExecutionInfo_urgence;
  importance: TraitementExecutionInfo_importance;
}
