import gql from 'graphql-tag';
import { TRAITEMENT_EXECUTION_AUTO_INFO } from '../traitement-execution/fragment';

export const PARAMETRE_TRAITEMENT_AUTO_INFO = gql`
  fragment TraitementInfo on TATraitement {
    id
    dataType
    description
    dateDebut
    isPrivate
    type {
      id
      code
      libelle
    }
    participants {
      id
      fullName
      photo {
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
    }

    importance {
      id
      ordre
      couleur
      libelle
    }

    fonction {
      id
      ordre
      libelle
    }
    tache {
      id
      ordre
      libelle
    }

    repetition
    repetitionUnite
    recurrence
    heure
    dateHeureFin
    nombreOccurencesFin
    joursSemaine
    urgence {
      id
      code
      libelle
      couleur
    }
  }
`;

export const FULL_TRAITEMENT_AUTO_INFO = gql`
  fragment TraitementInfo on TATraitement {
    id
    dataType
    description
    termine
    nombreTraitementsCloture
    nombreTraitementsEnRetard
    joursSemaine
    dateDebut
    isPrivate
    type {
      id
      code
      libelle
    }
    participants {
      id
      fullName
      photo {
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
    }

    importance {
      id
      ordre
      libelle
      couleur
    }

    fonction {
      id
      ordre
      libelle
    }
    tache {
      id
      ordre
      libelle
    }

    repetition
    repetitionUnite
    recurrence
    heure
    dateHeureFin
    nombreOccurencesFin
    urgence {
      id
      code
      libelle
      couleur
    }

    derniereExecutionCloturee {
      ...TraitementExecutionInfo
    }
    execution {
      ...TraitementExecutionInfo
    }
  }
  ${TRAITEMENT_EXECUTION_AUTO_INFO}
`;
