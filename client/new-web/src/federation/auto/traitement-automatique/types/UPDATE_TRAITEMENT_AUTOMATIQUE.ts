/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TATraitementInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_TRAITEMENT_AUTOMATIQUE
// ====================================================

export interface UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_type {
  __typename: "TATraitementType";
  id: string;
  code: string;
  libelle: string;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_participants_photo | null;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_fonction {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_tache {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_derniereExecutionCloturee_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_derniereExecutionCloturee_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_derniereExecutionCloturee_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_derniereExecutionCloturee_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_derniereExecutionCloturee {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_derniereExecutionCloturee_dernierChangementStatut | null;
  changementStatuts: UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_derniereExecutionCloturee_changementStatuts[];
  urgence: UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_derniereExecutionCloturee_urgence;
  importance: UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_derniereExecutionCloturee_importance;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_execution_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_execution_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_execution_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_execution_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_execution {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_execution_dernierChangementStatut | null;
  changementStatuts: UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_execution_changementStatuts[];
  urgence: UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_execution_urgence;
  importance: UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_execution_importance;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement {
  __typename: "TATraitement";
  id: string;
  dataType: string;
  description: string;
  termine: boolean;
  nombreTraitementsCloture: number;
  nombreTraitementsEnRetard: number;
  joursSemaine: number[] | null;
  dateDebut: any | null;
  isPrivate: boolean;
  type: UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_type;
  participants: UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_participants[];
  importance: UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_importance;
  fonction: UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_fonction | null;
  tache: UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_tache | null;
  repetition: number;
  repetitionUnite: string;
  recurrence: string;
  heure: string;
  dateHeureFin: any | null;
  nombreOccurencesFin: number | null;
  urgence: UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_urgence;
  derniereExecutionCloturee: UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_derniereExecutionCloturee | null;
  execution: UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement_execution;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUE {
  updateOneTATraitement: UPDATE_TRAITEMENT_AUTOMATIQUE_updateOneTATraitement;
}

export interface UPDATE_TRAITEMENT_AUTOMATIQUEVariables {
  input: TATraitementInput;
  id: string;
}
