/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TATraitementExecutionChangementStatutInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CHANGER_STATUT_TRAITEMENT_EXECUTION
// ====================================================

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_type {
  __typename: "TATraitementType";
  id: string;
  code: string;
  libelle: string;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_participants_photo | null;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_fonction {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_tache {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_derniereExecutionCloturee_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_derniereExecutionCloturee_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_derniereExecutionCloturee_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_derniereExecutionCloturee_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_derniereExecutionCloturee {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_derniereExecutionCloturee_dernierChangementStatut | null;
  changementStatuts: CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_derniereExecutionCloturee_changementStatuts[];
  urgence: CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_derniereExecutionCloturee_urgence;
  importance: CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_derniereExecutionCloturee_importance;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_execution_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_execution_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_execution_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_execution_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_execution {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_execution_dernierChangementStatut | null;
  changementStatuts: CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_execution_changementStatuts[];
  urgence: CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_execution_urgence;
  importance: CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_execution_importance;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution {
  __typename: "TATraitement";
  id: string;
  dataType: string;
  description: string;
  termine: boolean;
  nombreTraitementsCloture: number;
  nombreTraitementsEnRetard: number;
  joursSemaine: number[] | null;
  dateDebut: any | null;
  isPrivate: boolean;
  type: CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_type;
  participants: CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_participants[];
  importance: CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_importance;
  fonction: CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_fonction | null;
  tache: CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_tache | null;
  repetition: number;
  repetitionUnite: string;
  recurrence: string;
  heure: string;
  dateHeureFin: any | null;
  nombreOccurencesFin: number | null;
  urgence: CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_urgence;
  derniereExecutionCloturee: CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_derniereExecutionCloturee | null;
  execution: CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution_execution;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTION {
  changerStatutTATraitementExecution: CHANGER_STATUT_TRAITEMENT_EXECUTION_changerStatutTATraitementExecution;
}

export interface CHANGER_STATUT_TRAITEMENT_EXECUTIONVariables {
  input: TATraitementExecutionChangementStatutInput;
}
