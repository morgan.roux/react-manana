/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, TATraitementTypeFilter, TATraitementTypeSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_TRAITEMENT_AUTOMATIQUES_TYPES
// ====================================================

export interface GET_TRAITEMENT_AUTOMATIQUES_TYPES_tATraitementTypes_nodes {
  __typename: "TATraitementType";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_TYPES_tATraitementTypes {
  __typename: "TATraitementTypeConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_TRAITEMENT_AUTOMATIQUES_TYPES_tATraitementTypes_nodes[];
}

export interface GET_TRAITEMENT_AUTOMATIQUES_TYPES {
  tATraitementTypes: GET_TRAITEMENT_AUTOMATIQUES_TYPES_tATraitementTypes;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_TYPESVariables {
  paging?: OffsetPaging | null;
  filter?: TATraitementTypeFilter | null;
  sorting?: TATraitementTypeSort[] | null;
}
