/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TraitementInfo
// ====================================================

export interface TraitementInfo_type {
  __typename: "TATraitementType";
  id: string;
  code: string;
  libelle: string;
}

export interface TraitementInfo_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface TraitementInfo_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: TraitementInfo_participants_photo | null;
}

export interface TraitementInfo_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface TraitementInfo_fonction {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
}

export interface TraitementInfo_tache {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
}

export interface TraitementInfo_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface TraitementInfo_derniereExecutionCloturee_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface TraitementInfo_derniereExecutionCloturee_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface TraitementInfo_derniereExecutionCloturee_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface TraitementInfo_derniereExecutionCloturee_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface TraitementInfo_derniereExecutionCloturee {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: TraitementInfo_derniereExecutionCloturee_dernierChangementStatut | null;
  changementStatuts: TraitementInfo_derniereExecutionCloturee_changementStatuts[];
  urgence: TraitementInfo_derniereExecutionCloturee_urgence;
  importance: TraitementInfo_derniereExecutionCloturee_importance;
}

export interface TraitementInfo_execution_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface TraitementInfo_execution_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface TraitementInfo_execution_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface TraitementInfo_execution_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface TraitementInfo_execution {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: TraitementInfo_execution_dernierChangementStatut | null;
  changementStatuts: TraitementInfo_execution_changementStatuts[];
  urgence: TraitementInfo_execution_urgence;
  importance: TraitementInfo_execution_importance;
}

export interface TraitementInfo {
  __typename: "TATraitement";
  id: string;
  dataType: string;
  description: string;
  termine: boolean;
  nombreTraitementsCloture: number;
  nombreTraitementsEnRetard: number;
  joursSemaine: number[] | null;
  dateDebut: any | null;
  isPrivate: boolean;
  type: TraitementInfo_type;
  participants: TraitementInfo_participants[];
  importance: TraitementInfo_importance;
  fonction: TraitementInfo_fonction | null;
  tache: TraitementInfo_tache | null;
  repetition: number;
  repetitionUnite: string;
  recurrence: string;
  heure: string;
  dateHeureFin: any | null;
  nombreOccurencesFin: number | null;
  urgence: TraitementInfo_urgence;
  derniereExecutionCloturee: TraitementInfo_derniereExecutionCloturee | null;
  execution: TraitementInfo_execution;
}
