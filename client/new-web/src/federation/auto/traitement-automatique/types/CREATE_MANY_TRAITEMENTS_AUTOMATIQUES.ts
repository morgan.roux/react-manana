/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TATraitementInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES
// ====================================================

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_type {
  __typename: "TATraitementType";
  id: string;
  code: string;
  libelle: string;
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_participants_photo | null;
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_fonction {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_tache {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_derniereExecutionCloturee_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_derniereExecutionCloturee_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_derniereExecutionCloturee_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_derniereExecutionCloturee_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_derniereExecutionCloturee {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_derniereExecutionCloturee_dernierChangementStatut | null;
  changementStatuts: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_derniereExecutionCloturee_changementStatuts[];
  urgence: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_derniereExecutionCloturee_urgence;
  importance: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_derniereExecutionCloturee_importance;
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_execution_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_execution_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_execution_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_execution_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_execution {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_execution_dernierChangementStatut | null;
  changementStatuts: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_execution_changementStatuts[];
  urgence: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_execution_urgence;
  importance: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_execution_importance;
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements {
  __typename: "TATraitement";
  id: string;
  dataType: string;
  description: string;
  termine: boolean;
  nombreTraitementsCloture: number;
  nombreTraitementsEnRetard: number;
  joursSemaine: number[] | null;
  dateDebut: any | null;
  isPrivate: boolean;
  type: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_type;
  participants: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_participants[];
  importance: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_importance;
  fonction: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_fonction | null;
  tache: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_tache | null;
  repetition: number;
  repetitionUnite: string;
  recurrence: string;
  heure: string;
  dateHeureFin: any | null;
  nombreOccurencesFin: number | null;
  urgence: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_urgence;
  derniereExecutionCloturee: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_derniereExecutionCloturee | null;
  execution: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements_execution;
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUES {
  createManyTATraitements: CREATE_MANY_TRAITEMENTS_AUTOMATIQUES_createManyTATraitements[];
}

export interface CREATE_MANY_TRAITEMENTS_AUTOMATIQUESVariables {
  input: TATraitementInput[];
}
