/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_MANY_TRAITEMENTS_AUTOMATIQUES
// ====================================================

export interface DELETE_MANY_TRAITEMENTS_AUTOMATIQUES {
  deleteManyTATraitements: number;
}

export interface DELETE_MANY_TRAITEMENTS_AUTOMATIQUESVariables {
  ids: string[];
}
