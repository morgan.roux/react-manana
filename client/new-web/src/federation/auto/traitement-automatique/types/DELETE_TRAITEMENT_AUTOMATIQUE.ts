/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_TRAITEMENT_AUTOMATIQUE
// ====================================================

export interface DELETE_TRAITEMENT_AUTOMATIQUE_deleteOneTATraitement {
  __typename: "TATraitement";
  id: string;
}

export interface DELETE_TRAITEMENT_AUTOMATIQUE {
  deleteOneTATraitement: DELETE_TRAITEMENT_AUTOMATIQUE_deleteOneTATraitement;
}

export interface DELETE_TRAITEMENT_AUTOMATIQUEVariables {
  id: string;
}
