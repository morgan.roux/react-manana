import gql from 'graphql-tag';
import { FULL_COMMANDE_ORALE_INFO } from '../commande-orale/fragment';

export const CREATE_PASSATION = gql`
  mutation CREATE_PASSATION($input: COPassationInput!) {
    createOneCOPassation(input: $input) {
      ...COCommandeOraleInfo
    }
  }
  ${FULL_COMMANDE_ORALE_INFO}
`;
