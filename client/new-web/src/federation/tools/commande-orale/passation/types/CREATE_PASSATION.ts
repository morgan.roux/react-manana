/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { COPassationInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_PASSATION
// ====================================================

export interface CREATE_PASSATION_createOneCOPassation {
  __typename: "COCommandeOrale";
  id: string;
  idUser: string;
  designation: string;
  quantite: number;
  forme: string;
  dateHeure: any;
  cloturee: boolean;
  commentaire: string | null;
}

export interface CREATE_PASSATION {
  createOneCOPassation: CREATE_PASSATION_createOneCOPassation;
}

export interface CREATE_PASSATIONVariables {
  input: COPassationInput;
}
