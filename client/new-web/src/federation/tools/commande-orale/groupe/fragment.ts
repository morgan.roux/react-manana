import gql from 'graphql-tag';

export const MINIMAL_GROUPE_SOURCE_APPRO_INFO = gql`
  fragment COGroupeInfo on COGroupe {
    id
    nom
    ordre
    idPharmacie
    idGroupement
    nombreSourceAppros
  }
`;

export const FULL_GROUPE_SOURCE_APPRO_INFO = gql`
  fragment COGroupeInfo on COGroupe {
    id
    nom
    ordre
    idPharmacie
    idGroupement
    sourceAppros {
      id
      nom
      ordre
      tel
      commentaire
      idGroupeSourceAppro
      idPharmacie
      idGroupement
      attributs {
        id
        identifiant
        valeur
      }
    }
  }
`;

export const CO_GROUPE_SOURCE_APPRO_INFO = gql`
  fragment COGroupeInfo on COGroupe {
    id
    nom
    sourceAppros {
      id
      nom
      ordre
      tel
      commentaire
      idGroupement
      attributs {
        id
        identifiant
        valeur
      }
    }
  }
`;


