import gql from 'graphql-tag';
import {
  MINIMAL_GROUPE_SOURCE_APPRO_INFO,
  FULL_GROUPE_SOURCE_APPRO_INFO,
  CO_GROUPE_SOURCE_APPRO_INFO,
} from './fragment';

export const GET_GROUPES = gql`
  query GET_GROUPES($paging: OffsetPaging, $filter: COGroupeFilter, $sorting: [COGroupeSort!]) {
    cOGroupes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...COGroupeInfo
      }
    }
  }

  ${MINIMAL_GROUPE_SOURCE_APPRO_INFO}
`;

export const GET_GROUPE = gql`
  query GET_GROUPE($id: ID!) {
    cOGroupe(id: $id) {
      ...COGroupeInfo
    }
  }

  ${FULL_GROUPE_SOURCE_APPRO_INFO}
`;

export const GET_GROUPES_CO = gql`
  query GET_GROUPES_CO($paging: OffsetPaging, $filter: COGroupeFilter, $sorting: [COGroupeSort!]) {
    cOGroupes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...COGroupeInfo
      }
    }
  }

  ${CO_GROUPE_SOURCE_APPRO_INFO}
`;
