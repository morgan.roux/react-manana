/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { COGroupeInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_GROUPE
// ====================================================

export interface CREATE_GROUPE_createOneCOGroupeSourceAppro_sourceAppros_attributs {
  __typename: "COSourceApproAttribut";
  id: string;
  identifiant: string;
  valeur: string;
}

export interface CREATE_GROUPE_createOneCOGroupeSourceAppro_sourceAppros {
  __typename: "COSourceAppro";
  id: string;
  nom: string;
  ordre: number;
  tel: string;
  commentaire: string | null;
  idGroupement: string;
  attributs: CREATE_GROUPE_createOneCOGroupeSourceAppro_sourceAppros_attributs[];
}

export interface CREATE_GROUPE_createOneCOGroupeSourceAppro {
  __typename: "COGroupe";
  id: string;
  nom: string;
  sourceAppros: CREATE_GROUPE_createOneCOGroupeSourceAppro_sourceAppros[];
}

export interface CREATE_GROUPE {
  createOneCOGroupeSourceAppro: CREATE_GROUPE_createOneCOGroupeSourceAppro;
}

export interface CREATE_GROUPEVariables {
  input: COGroupeInput;
}
