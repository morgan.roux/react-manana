/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { COGroupeInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_GROUPE
// ====================================================

export interface UPDATE_GROUPE_updateOneCOGroupeSourceAppro_sourceAppros_attributs {
  __typename: "COSourceApproAttribut";
  id: string;
  identifiant: string;
  valeur: string;
}

export interface UPDATE_GROUPE_updateOneCOGroupeSourceAppro_sourceAppros {
  __typename: "COSourceAppro";
  id: string;
  nom: string;
  ordre: number;
  tel: string;
  commentaire: string | null;
  idGroupement: string;
  attributs: UPDATE_GROUPE_updateOneCOGroupeSourceAppro_sourceAppros_attributs[];
}

export interface UPDATE_GROUPE_updateOneCOGroupeSourceAppro {
  __typename: "COGroupe";
  id: string;
  nom: string;
  sourceAppros: UPDATE_GROUPE_updateOneCOGroupeSourceAppro_sourceAppros[];
}

export interface UPDATE_GROUPE {
  updateOneCOGroupeSourceAppro: UPDATE_GROUPE_updateOneCOGroupeSourceAppro;
}

export interface UPDATE_GROUPEVariables {
  id: string;
  input: COGroupeInput;
}
