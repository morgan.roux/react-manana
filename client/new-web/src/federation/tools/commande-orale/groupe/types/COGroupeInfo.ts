/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: COGroupeInfo
// ====================================================

export interface COGroupeInfo_sourceAppros_attributs {
  __typename: "COSourceApproAttribut";
  id: string;
  identifiant: string;
  valeur: string;
}

export interface COGroupeInfo_sourceAppros {
  __typename: "COSourceAppro";
  id: string;
  nom: string;
  ordre: number;
  tel: string;
  commentaire: string | null;
  idGroupement: string;
  attributs: COGroupeInfo_sourceAppros_attributs[];
}

export interface COGroupeInfo {
  __typename: "COGroupe";
  id: string;
  nom: string;
  sourceAppros: COGroupeInfo_sourceAppros[];
}
