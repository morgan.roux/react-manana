/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CLOTURER_COMMANDE
// ====================================================

export interface CLOTURER_COMMANDE_cloturerCommande {
  __typename: "COCommandeOrale";
  id: string;
  idUser: string;
  designation: string;
  quantite: number;
  forme: string;
  dateHeure: any;
  cloturee: boolean;
  commentaire: string | null;
}

export interface CLOTURER_COMMANDE {
  cloturerCommande: CLOTURER_COMMANDE_cloturerCommande;
}

export interface CLOTURER_COMMANDEVariables {
  id: string;
}
