/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, COCommandeOraleFilter, COCommandeOraleSort } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_COMMANDES_ORALES
// ====================================================

export interface GET_COMMANDES_ORALES_cOCommandeOrales_nodes {
  __typename: "COCommandeOrale";
  id: string;
  idUser: string;
  designation: string;
  quantite: number;
  forme: string;
  dateHeure: any;
  cloturee: boolean;
  commentaire: string | null;
}

export interface GET_COMMANDES_ORALES_cOCommandeOrales {
  __typename: "COCommandeOraleConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_COMMANDES_ORALES_cOCommandeOrales_nodes[];
}

export interface GET_COMMANDES_ORALES {
  cOCommandeOrales: GET_COMMANDES_ORALES_cOCommandeOrales;
}

export interface GET_COMMANDES_ORALESVariables {
  paging?: OffsetPaging | null;
  filter?: COCommandeOraleFilter | null;
  sorting?: COCommandeOraleSort[] | null;
}
