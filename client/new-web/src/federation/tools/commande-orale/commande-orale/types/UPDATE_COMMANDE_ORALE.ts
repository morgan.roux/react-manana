/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { COCommandeOraleInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_COMMANDE_ORALE
// ====================================================

export interface UPDATE_COMMANDE_ORALE_updateOneCOCommandeOrale {
  __typename: "COCommandeOrale";
  id: string;
  idUser: string;
  designation: string;
  quantite: number;
  forme: string;
  dateHeure: any;
  cloturee: boolean;
  commentaire: string | null;
}

export interface UPDATE_COMMANDE_ORALE {
  updateOneCOCommandeOrale: UPDATE_COMMANDE_ORALE_updateOneCOCommandeOrale;
}

export interface UPDATE_COMMANDE_ORALEVariables {
  id: string;
  input: COCommandeOraleInput;
}
