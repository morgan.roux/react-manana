/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { COCommandeOraleAggregateFilter } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_ROW_COMMANDES_ORALES
// ====================================================

export interface GET_ROW_COMMANDES_ORALES_cOCommandeOraleAggregate_count {
  __typename: "COCommandeOraleCountAggregate";
  id: number | null;
}

export interface GET_ROW_COMMANDES_ORALES_cOCommandeOraleAggregate {
  __typename: "COCommandeOraleAggregateResponse";
  count: GET_ROW_COMMANDES_ORALES_cOCommandeOraleAggregate_count | null;
}

export interface GET_ROW_COMMANDES_ORALES {
  cOCommandeOraleAggregate: GET_ROW_COMMANDES_ORALES_cOCommandeOraleAggregate;
}

export interface GET_ROW_COMMANDES_ORALESVariables {
  filter?: COCommandeOraleAggregateFilter | null;
}
