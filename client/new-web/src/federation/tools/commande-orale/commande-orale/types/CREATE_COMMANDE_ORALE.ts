/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { COCommandeOraleInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_COMMANDE_ORALE
// ====================================================

export interface CREATE_COMMANDE_ORALE_createOneCOCommandeOrale {
  __typename: "COCommandeOrale";
  id: string;
  idUser: string;
  designation: string;
  quantite: number;
  forme: string;
  dateHeure: any;
  cloturee: boolean;
  commentaire: string | null;
}

export interface CREATE_COMMANDE_ORALE {
  createOneCOCommandeOrale: CREATE_COMMANDE_ORALE_createOneCOCommandeOrale;
}

export interface CREATE_COMMANDE_ORALEVariables {
  input: COCommandeOraleInput;
}
