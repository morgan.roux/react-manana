import gql from 'graphql-tag';
import { SOURCE_APPRO_INFO } from './fragment';

export const CREATE_SOURCE_APPRO = gql`
  mutation CREATE_SOURCE_APPRO($input: COSourceApproInput!) {
    createOneCOSourceAppro(input: $input) {
      ...COSourceApproInfo
    }
  }
  ${SOURCE_APPRO_INFO}
`;

export const UPDATE_SOURCE_APPRO = gql`
  mutation UPDATE_SOURCE_APPRO($id: String!, $input: COSourceApproInput!) {
    updateOneCOSourceAppro(id: $id, input: $input) {
      ...COSourceApproInfo
    }
  }
  ${SOURCE_APPRO_INFO}
`;

export const DELETE_ONE_SOURCE_APPRO = gql`
  mutation DELETE_ONE_SOURCE_APPRO($input: DeleteOneInput!) {
    deleteOneCOSourceAppro(input: $input) {
      id
    }
  }
`;
