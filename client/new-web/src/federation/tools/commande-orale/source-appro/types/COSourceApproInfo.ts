/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: COSourceApproInfo
// ====================================================

export interface COSourceApproInfo_attributs {
  __typename: "COSourceApproAttribut";
  id: string;
  identifiant: string;
  valeur: string;
}

export interface COSourceApproInfo {
  __typename: "COSourceAppro";
  id: string;
  nom: string;
  ordre: number;
  tel: string;
  commentaire: string | null;
  idGroupeSourceAppro: string;
  idPharmacie: string;
  idGroupement: string;
  attributs: COSourceApproInfo_attributs[];
}
