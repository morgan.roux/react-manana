import gql from 'graphql-tag';

export const SOURCE_APPRO_INFO = gql`
  fragment COSourceApproInfo on COSourceAppro {
    id
    nom
    ordre
    tel
    commentaire
    idGroupeSourceAppro
    idPharmacie
    idGroupement
    attributs {
      id
      identifiant
      valeur
    }
  }
`;

