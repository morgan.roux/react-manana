/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { COReceptionInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_RECEPTION
// ====================================================

export interface CREATE_RECEPTION_createOneCOReception {
  __typename: "COCommandeOrale";
  id: string;
  idUser: string;
  designation: string;
  quantite: number;
  forme: string;
  dateHeure: any;
  cloturee: boolean;
  commentaire: string | null;
}

export interface CREATE_RECEPTION {
  createOneCOReception: CREATE_RECEPTION_createOneCOReception;
}

export interface CREATE_RECEPTIONVariables {
  input: COReceptionInput;
}
