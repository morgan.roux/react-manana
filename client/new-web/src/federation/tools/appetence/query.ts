import gql from 'graphql-tag';
import { FULL_APPETENCE_INFO } from './fragment';

export const GET_ROLE_APPETENCES = gql`
  query ROLE_APPETENCES($idRole: String!) {
    roleAppetences(idRole: $idRole) {
      ...AppetenceInfo
    }
  }
  ${FULL_APPETENCE_INFO}
`;

export const GET_USER_APPETENCES = gql`
  query USER_APPETENCES($idUser: String!) {
    userAppetences(idUser: $idUser) {
      ...AppetenceInfo
    }
  }
  ${FULL_APPETENCE_INFO}
`;
