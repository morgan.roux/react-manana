/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { AppetenceInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: SAVE_USER_APPETENCES
// ====================================================

export interface SAVE_USER_APPETENCES_saveUserAppetences_item {
  __typename: "Item";
  id: string;
  code: string;
  name: string;
}

export interface SAVE_USER_APPETENCES_saveUserAppetences {
  __typename: "Appetence";
  id: string;
  idItem: string;
  ordre: number;
  createdAt: any;
  updatedAt: any;
  item: SAVE_USER_APPETENCES_saveUserAppetences_item;
}

export interface SAVE_USER_APPETENCES {
  saveUserAppetences: SAVE_USER_APPETENCES_saveUserAppetences[];
}

export interface SAVE_USER_APPETENCESVariables {
  appetences?: AppetenceInput[] | null;
  idUser: string;
}
