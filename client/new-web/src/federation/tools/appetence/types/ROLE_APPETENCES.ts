/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ROLE_APPETENCES
// ====================================================

export interface ROLE_APPETENCES_roleAppetences_item {
  __typename: "Item";
  id: string;
  code: string;
  name: string;
}

export interface ROLE_APPETENCES_roleAppetences {
  __typename: "Appetence";
  id: string;
  idItem: string;
  ordre: number;
  createdAt: any;
  updatedAt: any;
  item: ROLE_APPETENCES_roleAppetences_item;
}

export interface ROLE_APPETENCES {
  roleAppetences: ROLE_APPETENCES_roleAppetences[];
}

export interface ROLE_APPETENCESVariables {
  idRole: string;
}
