import gql from 'graphql-tag';
import { FULL_ITEM_SCORING, ITEM_SCORING_COUNT } from './fragment';

export const GET_ITEM_SCORINGS = gql`
  query GET_ITEM_SCORINGS(
    $paging: OffsetPaging
    $filter: ItemScoringFilter
    $sorting: [ItemScoringSort!]
  ) {
    itemScorings(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...ItemScoring
      }
    }
  }
  ${FULL_ITEM_SCORING}
`;

export const GET_ITEM_SCORINGS_AGGREGATES = gql`
  query GET_ITEM_SCORINGS_AGGREGATES($filter: ItemScoringAggregateFilter) {
    itemScoringAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;

export const GET_ITEM_STATISTIQUES = gql`
  query ITEM_STATISTIQUES($idUser: String!) {
    itemStatistiques(idUser: $idUser) {
      item {
        id
        code
        codeItem
        name
      }
      occurence
      percentage
    }
  }
`;

export const GET_COUNT_SCORING = gql`
  query GET_COUNT_SCORING(
    $dateDebut: DateTime
    $dateFin: DateTime
    $dateEcheance: DateTime
    $idItems: [String!]
  ) {
    itemScoringsCount(
      dateDebut: $dateDebut
      dateFin: $dateFin
      dateEcheance: $dateEcheance
      idItems: $idItems
    ) {
      ...ItemScoringCount
    }
  }
  ${ITEM_SCORING_COUNT}
`;

export const GET_UPDATE_ITEM_SCORING_FOR_ITEM = gql`
  query UPDATE_ITEM_SCORING_FOR_ITEM($codeItem: String!, $idItemAssocie: String!) {
    updateItemScoringForItem(codeItem: $codeItem, idItemAssocie: $idItemAssocie)
  }
`;
export const UPDATE_ITEM_SCORING_FOR_USER = gql`
  query UPDATE_ITEM_SCORING_FOR_USER($codeItem: String!, $idUser: String!) {
    updateItemScoringForUser(codeItem: $codeItem, idUser: $idUser)
  }
`;


export const UPDATE_ITEM_SCORING_FOR_ALL_USERS = gql`
  query UPDATE_ITEM_SCORING_FOR_ALL_USERS {
    updateItemScoringForAllUsers
  }
`;

