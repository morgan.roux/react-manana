/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ItemScoring
// ====================================================

export interface ItemScoring_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface ItemScoring_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface ItemScoring_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface ItemScoring_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: ItemScoring_participants_photo | null;
}

export interface ItemScoring_item {
  __typename: "Item";
  id: string;
  code: string;
  codeItem: string;
  name: string;
}

export interface ItemScoring {
  __typename: "ItemScoring";
  id: string;
  idItem: string | null;
  idItemAssocie: string | null;
  idUser: string | null;
  description: string | null;
  dateEcheance: any | null;
  idUrgence: string | null;
  statut: string | null;
  nomItem: string | null;
  ordreStatut: number | null;
  urgence: ItemScoring_urgence;
  importance: ItemScoring_importance;
  participants: ItemScoring_participants[];
  idImportance: string | null;
  nombreJoursRetard: number;
  score: number;
  item: ItemScoring_item;
}
