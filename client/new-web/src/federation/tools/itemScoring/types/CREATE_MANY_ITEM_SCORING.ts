/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { CreateManyItemScoringsInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_MANY_ITEM_SCORING
// ====================================================

export interface CREATE_MANY_ITEM_SCORING_createManyItemScorings_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CREATE_MANY_ITEM_SCORING_createManyItemScorings_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_MANY_ITEM_SCORING_createManyItemScorings_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface CREATE_MANY_ITEM_SCORING_createManyItemScorings_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CREATE_MANY_ITEM_SCORING_createManyItemScorings_participants_photo | null;
}

export interface CREATE_MANY_ITEM_SCORING_createManyItemScorings_item {
  __typename: "Item";
  id: string;
  code: string;
  codeItem: string;
  name: string;
}

export interface CREATE_MANY_ITEM_SCORING_createManyItemScorings {
  __typename: "ItemScoring";
  id: string;
  idItem: string | null;
  idItemAssocie: string | null;
  idUser: string | null;
  description: string | null;
  dateEcheance: any | null;
  idUrgence: string | null;
  statut: string | null;
  nomItem: string | null;
  ordreStatut: number | null;
  urgence: CREATE_MANY_ITEM_SCORING_createManyItemScorings_urgence;
  importance: CREATE_MANY_ITEM_SCORING_createManyItemScorings_importance;
  participants: CREATE_MANY_ITEM_SCORING_createManyItemScorings_participants[];
  idImportance: string | null;
  nombreJoursRetard: number;
  score: number;
  item: CREATE_MANY_ITEM_SCORING_createManyItemScorings_item;
}

export interface CREATE_MANY_ITEM_SCORING {
  createManyItemScorings: CREATE_MANY_ITEM_SCORING_createManyItemScorings[];
}

export interface CREATE_MANY_ITEM_SCORINGVariables {
  input: CreateManyItemScoringsInput;
}
