/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ItemScoringPersonnalisationInfo
// ====================================================

export interface ItemScoringPersonnalisationInfo {
  __typename: "ItemScoringPersonnalisation";
  id: string;
  type: string;
  idTypeAssocie: string;
  ordre: number;
  idPharmacie: string;
}
