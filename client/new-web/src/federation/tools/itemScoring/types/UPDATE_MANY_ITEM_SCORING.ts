/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UpdateManyItemScoringsInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_MANY_ITEM_SCORING
// ====================================================

export interface UPDATE_MANY_ITEM_SCORING_updateManyItemScorings {
  __typename: "UpdateManyResponse";
  /**
   * The number of records updated.
   */
  updatedCount: number;
}

export interface UPDATE_MANY_ITEM_SCORING {
  updateManyItemScorings: UPDATE_MANY_ITEM_SCORING_updateManyItemScorings;
}

export interface UPDATE_MANY_ITEM_SCORINGVariables {
  input: UpdateManyItemScoringsInput;
}
