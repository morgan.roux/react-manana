/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: UPDATE_ITEM_SCORING_FOR_USER
// ====================================================

export interface UPDATE_ITEM_SCORING_FOR_USER {
  updateItemScoringForUser: number;
}

export interface UPDATE_ITEM_SCORING_FOR_USERVariables {
  codeItem: string;
  idUser: string;
}
