/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: DQStatut
// ====================================================

export interface DQStatut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  type: string;
}
