/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ItemScoringCount
// ====================================================

export interface ItemScoringCount_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface ItemScoringCount_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface ItemScoringCount {
  __typename: "ItemScoringCount";
  urgence: ItemScoringCount_urgence;
  importance: ItemScoringCount_importance;
  occurence: number;
}
