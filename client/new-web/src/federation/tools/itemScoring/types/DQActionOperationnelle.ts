/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: DQActionOperationnelle
// ====================================================

export interface DQActionOperationnelle_origine {
  __typename: "Origine";
  id: string;
  libelle: string;
}

export interface DQActionOperationnelle_statut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  type: string;
}

export interface DQActionOperationnelle_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface DQActionOperationnelle_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: DQActionOperationnelle_participants_photo | null;
}

export interface DQActionOperationnelle_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface DQActionOperationnelle_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface DQActionOperationnelle {
  __typename: "DQActionOperationnelle";
  id: string;
  dataType: string;
  description: string;
  origine: DQActionOperationnelle_origine | null;
  statut: DQActionOperationnelle_statut;
  participants: DQActionOperationnelle_participants[];
  urgence: DQActionOperationnelle_urgence;
  importance: DQActionOperationnelle_importance;
}
