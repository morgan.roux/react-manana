/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ItemScoringAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_ITEM_SCORINGS_AGGREGATES
// ====================================================

export interface GET_ITEM_SCORINGS_AGGREGATES_itemScoringAggregate_count {
  __typename: "ItemScoringCountAggregate";
  id: number | null;
}

export interface GET_ITEM_SCORINGS_AGGREGATES_itemScoringAggregate {
  __typename: "ItemScoringAggregateResponse";
  count: GET_ITEM_SCORINGS_AGGREGATES_itemScoringAggregate_count | null;
}

export interface GET_ITEM_SCORINGS_AGGREGATES {
  itemScoringAggregate: GET_ITEM_SCORINGS_AGGREGATES_itemScoringAggregate;
}

export interface GET_ITEM_SCORINGS_AGGREGATESVariables {
  filter?: ItemScoringAggregateFilter | null;
}
