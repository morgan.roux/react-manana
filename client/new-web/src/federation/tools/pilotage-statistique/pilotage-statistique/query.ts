import gql from 'graphql-tag';
import { PILOTAGE_STATISTIQUE_INFO } from './fragment';

export const PILOTAGE_STATISTIQUE = gql`
  query GET_PILOTAGE_STATISTIQUE(
    $input: PilotageStatistiqueInput!
  ) {
    pfPilotageStatistique(input: $input) {
      ...PilotageStatistiqueInfo
    }
  }

  ${PILOTAGE_STATISTIQUE_INFO}
`;