/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PilotageStatistiqueInfo
// ====================================================

export interface PilotageStatistiqueInfo_evolutions_importances {
  __typename: "PFPilotageImportance";
  id: string;
  ordre: number;
  libelle: string;
  pilotageCouleur: string;
  pilotageOccurences: number;
}

export interface PilotageStatistiqueInfo_evolutions {
  __typename: "PFPilotageEvolution";
  id: string;
  categorie: string;
  ordre: number;
  importances: PilotageStatistiqueInfo_evolutions_importances[];
}

export interface PilotageStatistiqueInfo_items_statuts {
  __typename: "PFPilotageStatut";
  id: string;
  code: string;
  libelle: string;
  pilotageCouleur: string;
  pilotageOccurences: number;
}

export interface PilotageStatistiqueInfo_items {
  __typename: "PFPilotageItem";
  id: string;
  code: string;
  libelle: string;
  pilotagePercentage: number;
  pilotageCouleur: string;
  statuts: PilotageStatistiqueInfo_items_statuts[];
}

export interface PilotageStatistiqueInfo {
  __typename: "PFPilotageStatistique";
  evolutions: PilotageStatistiqueInfo_evolutions[];
  items: PilotageStatistiqueInfo_items[];
}
