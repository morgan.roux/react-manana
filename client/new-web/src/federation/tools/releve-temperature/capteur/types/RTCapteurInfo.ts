/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: RTCapteurInfo
// ====================================================

export interface RTCapteurInfo_pointAcces {
  __typename: "RTPointAcces";
  id: string;
  nom: string;
  numeroSerie: string;
}

export interface RTCapteurInfo {
  __typename: "RTCapteur";
  id: string;
  nom: string;
  numeroSerie: string;
  pointAcces: RTCapteurInfo_pointAcces;
}
