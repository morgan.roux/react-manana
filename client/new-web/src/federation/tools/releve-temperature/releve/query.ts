import gql from 'graphql-tag';
import { FULL_RELEVE_INFO, FULL_STATUS} from './fragment';

export const GET_RELEVES = gql`
  query GET_RELEVES(
    $paging: OffsetPaging
    $filter: RTReleveFilter
    $sorting: [RTReleveSort!]
  ) {
    rTReleves(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...RTReleveInfo
      }
    }
  }

  ${FULL_RELEVE_INFO}
`;

export const GET_ROW_RELEVE = gql`
  query GET_ROW_RELEVE($filter: RTReleveAggregateFilter) {
    rTReleveAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;

export const GET_LIST_RELEVE_STATUS = gql`
  query GET_Status(
    $paging: OffsetPaging
    $filter: RTStatusFilter
    $sorting: [RTStatusSort!]
  ) {
    rTStatuses(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...RTStatus
      }
    }
  }
  ${FULL_STATUS}
`;

