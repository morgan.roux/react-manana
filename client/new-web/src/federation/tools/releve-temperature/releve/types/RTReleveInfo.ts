/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: RTReleveInfo
// ====================================================

export interface RTReleveInfo_releveur_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface RTReleveInfo_releveur {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: RTReleveInfo_releveur_photo | null;
}

export interface RTReleveInfo_status {
  __typename: "RTStatus";
  id: string;
  code: string;
  couleur: string;
  libelle: string;
}

export interface RTReleveInfo_frigo {
  __typename: "RTFrigo";
  id: string;
  nom: string;
  temperatureBasse: number;
  temperatureHaute: number;
  temporisation: number | null;
}

export interface RTReleveInfo {
  __typename: "RTReleve";
  id: string;
  temperature: number | null;
  type: string;
  createdAt: any;
  releveur: RTReleveInfo_releveur | null;
  status: RTReleveInfo_status;
  frigo: RTReleveInfo_frigo;
}
