/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_ONE_RELEVE
// ====================================================

export interface DELETE_ONE_RELEVE_deleteOneRTReleve {
  __typename: "RTReleveDeleteResponse";
  id: string | null;
}

export interface DELETE_ONE_RELEVE {
  deleteOneRTReleve: DELETE_ONE_RELEVE_deleteOneRTReleve;
}

export interface DELETE_ONE_RELEVEVariables {
  input: DeleteOneInput;
}
