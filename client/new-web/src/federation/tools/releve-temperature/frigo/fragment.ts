import gql from 'graphql-tag';
import { FULL_TRAITEMENT_AUTO_INFO } from '../../../auto/traitement-automatique/fragment';
import { FULL_RELEVE_INFO } from '../releve/fragment';
import { FULL_CAPTEUR_INFO } from '../capteur/fragment'

export const FULL_FRIGO_INFO = gql`
  fragment RTFrigoInfo on RTFrigo {
    id
    nom
    temperatureBasse
    temperatureHaute
    idPharmacie
    temporisation
    typeReleve
    traitements {
      ...TraitementInfo
    }
    releves {
      ...RTReleveInfo
    }
    capteurs {
      ...RTCapteurInfo
    }
  }

  ${FULL_TRAITEMENT_AUTO_INFO}
  ${FULL_RELEVE_INFO}
  ${FULL_CAPTEUR_INFO}
`;

export const MINIMAL_FRIGO_INFO = gql`
  fragment RTFrigoInfo on RTFrigo {
    id
    nom
    temperatureBasse
    temperatureHaute
    idPharmacie
    temporisation
    typeReleve
    traitements {
      ...TraitementInfo
    }
    capteurs {
      ...RTCapteurInfo
    }
    dernierReleve {
      ...RTReleveInfo
    }
  }

  ${FULL_TRAITEMENT_AUTO_INFO}
  ${FULL_RELEVE_INFO}
  ${FULL_CAPTEUR_INFO}
`;

