/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, RTFrigoFilter, RTFrigoSort } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_FRIGOS
// ====================================================

export interface GET_FRIGOS_rTFrigos_nodes_traitements_type {
  __typename: "TATraitementType";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_FRIGOS_rTFrigos_nodes_traitements_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_FRIGOS_rTFrigos_nodes_traitements_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_FRIGOS_rTFrigos_nodes_traitements_participants_photo | null;
}

export interface GET_FRIGOS_rTFrigos_nodes_traitements_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_FRIGOS_rTFrigos_nodes_traitements_fonction {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
}

export interface GET_FRIGOS_rTFrigos_nodes_traitements_tache {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
}

export interface GET_FRIGOS_rTFrigos_nodes_traitements_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_FRIGOS_rTFrigos_nodes_traitements_derniereExecutionCloturee_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface GET_FRIGOS_rTFrigos_nodes_traitements_derniereExecutionCloturee_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface GET_FRIGOS_rTFrigos_nodes_traitements_derniereExecutionCloturee_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_FRIGOS_rTFrigos_nodes_traitements_derniereExecutionCloturee_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_FRIGOS_rTFrigos_nodes_traitements_derniereExecutionCloturee {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: GET_FRIGOS_rTFrigos_nodes_traitements_derniereExecutionCloturee_dernierChangementStatut | null;
  changementStatuts: GET_FRIGOS_rTFrigos_nodes_traitements_derniereExecutionCloturee_changementStatuts[];
  urgence: GET_FRIGOS_rTFrigos_nodes_traitements_derniereExecutionCloturee_urgence;
  importance: GET_FRIGOS_rTFrigos_nodes_traitements_derniereExecutionCloturee_importance;
}

export interface GET_FRIGOS_rTFrigos_nodes_traitements_execution_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface GET_FRIGOS_rTFrigos_nodes_traitements_execution_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface GET_FRIGOS_rTFrigos_nodes_traitements_execution_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_FRIGOS_rTFrigos_nodes_traitements_execution_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_FRIGOS_rTFrigos_nodes_traitements_execution {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: GET_FRIGOS_rTFrigos_nodes_traitements_execution_dernierChangementStatut | null;
  changementStatuts: GET_FRIGOS_rTFrigos_nodes_traitements_execution_changementStatuts[];
  urgence: GET_FRIGOS_rTFrigos_nodes_traitements_execution_urgence;
  importance: GET_FRIGOS_rTFrigos_nodes_traitements_execution_importance;
}

export interface GET_FRIGOS_rTFrigos_nodes_traitements {
  __typename: "TATraitement";
  id: string;
  dataType: string;
  description: string;
  termine: boolean;
  nombreTraitementsCloture: number;
  nombreTraitementsEnRetard: number;
  joursSemaine: number[] | null;
  dateDebut: any | null;
  isPrivate: boolean;
  type: GET_FRIGOS_rTFrigos_nodes_traitements_type;
  participants: GET_FRIGOS_rTFrigos_nodes_traitements_participants[];
  importance: GET_FRIGOS_rTFrigos_nodes_traitements_importance;
  fonction: GET_FRIGOS_rTFrigos_nodes_traitements_fonction | null;
  tache: GET_FRIGOS_rTFrigos_nodes_traitements_tache | null;
  repetition: number;
  repetitionUnite: string;
  recurrence: string;
  heure: string;
  dateHeureFin: any | null;
  nombreOccurencesFin: number | null;
  urgence: GET_FRIGOS_rTFrigos_nodes_traitements_urgence;
  derniereExecutionCloturee: GET_FRIGOS_rTFrigos_nodes_traitements_derniereExecutionCloturee | null;
  execution: GET_FRIGOS_rTFrigos_nodes_traitements_execution;
}

export interface GET_FRIGOS_rTFrigos_nodes_capteurs_pointAcces {
  __typename: "RTPointAcces";
  id: string;
  nom: string;
  numeroSerie: string;
}

export interface GET_FRIGOS_rTFrigos_nodes_capteurs {
  __typename: "RTCapteur";
  id: string;
  nom: string;
  numeroSerie: string;
  pointAcces: GET_FRIGOS_rTFrigos_nodes_capteurs_pointAcces;
}

export interface GET_FRIGOS_rTFrigos_nodes_dernierReleve_releveur_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_FRIGOS_rTFrigos_nodes_dernierReleve_releveur {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_FRIGOS_rTFrigos_nodes_dernierReleve_releveur_photo | null;
}

export interface GET_FRIGOS_rTFrigos_nodes_dernierReleve_status {
  __typename: "RTStatus";
  id: string;
  code: string;
  couleur: string;
  libelle: string;
}

export interface GET_FRIGOS_rTFrigos_nodes_dernierReleve_frigo {
  __typename: "RTFrigo";
  id: string;
  nom: string;
  temperatureBasse: number;
  temperatureHaute: number;
  temporisation: number | null;
}

export interface GET_FRIGOS_rTFrigos_nodes_dernierReleve {
  __typename: "RTReleve";
  id: string;
  temperature: number | null;
  type: string;
  createdAt: any;
  releveur: GET_FRIGOS_rTFrigos_nodes_dernierReleve_releveur | null;
  status: GET_FRIGOS_rTFrigos_nodes_dernierReleve_status;
  frigo: GET_FRIGOS_rTFrigos_nodes_dernierReleve_frigo;
}

export interface GET_FRIGOS_rTFrigos_nodes {
  __typename: "RTFrigo";
  id: string;
  nom: string;
  temperatureBasse: number;
  temperatureHaute: number;
  idPharmacie: string;
  temporisation: number | null;
  typeReleve: string;
  traitements: GET_FRIGOS_rTFrigos_nodes_traitements[] | null;
  capteurs: GET_FRIGOS_rTFrigos_nodes_capteurs[] | null;
  dernierReleve: GET_FRIGOS_rTFrigos_nodes_dernierReleve | null;
}

export interface GET_FRIGOS_rTFrigos {
  __typename: "RTFrigoConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_FRIGOS_rTFrigos_nodes[];
}

export interface GET_FRIGOS {
  rTFrigos: GET_FRIGOS_rTFrigos;
}

export interface GET_FRIGOSVariables {
  paging?: OffsetPaging | null;
  filter?: RTFrigoFilter | null;
  sorting?: RTFrigoSort[] | null;
}
