/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_ONE_FRIGO
// ====================================================

export interface DELETE_ONE_FRIGO_deleteOneRTFrigo {
  __typename: "RTFrigoDeleteResponse";
  id: string | null;
}

export interface DELETE_ONE_FRIGO {
  deleteOneRTFrigo: DELETE_ONE_FRIGO_deleteOneRTFrigo;
}

export interface DELETE_ONE_FRIGOVariables {
  input: DeleteOneInput;
}
