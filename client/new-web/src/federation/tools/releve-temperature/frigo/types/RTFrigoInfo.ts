/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: RTFrigoInfo
// ====================================================

export interface RTFrigoInfo_traitements_type {
  __typename: "TATraitementType";
  id: string;
  code: string;
  libelle: string;
}

export interface RTFrigoInfo_traitements_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface RTFrigoInfo_traitements_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: RTFrigoInfo_traitements_participants_photo | null;
}

export interface RTFrigoInfo_traitements_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface RTFrigoInfo_traitements_fonction {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
}

export interface RTFrigoInfo_traitements_tache {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
}

export interface RTFrigoInfo_traitements_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface RTFrigoInfo_traitements_derniereExecutionCloturee_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface RTFrigoInfo_traitements_derniereExecutionCloturee_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface RTFrigoInfo_traitements_derniereExecutionCloturee_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface RTFrigoInfo_traitements_derniereExecutionCloturee_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface RTFrigoInfo_traitements_derniereExecutionCloturee {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: RTFrigoInfo_traitements_derniereExecutionCloturee_dernierChangementStatut | null;
  changementStatuts: RTFrigoInfo_traitements_derniereExecutionCloturee_changementStatuts[];
  urgence: RTFrigoInfo_traitements_derniereExecutionCloturee_urgence;
  importance: RTFrigoInfo_traitements_derniereExecutionCloturee_importance;
}

export interface RTFrigoInfo_traitements_execution_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface RTFrigoInfo_traitements_execution_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface RTFrigoInfo_traitements_execution_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface RTFrigoInfo_traitements_execution_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface RTFrigoInfo_traitements_execution {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: RTFrigoInfo_traitements_execution_dernierChangementStatut | null;
  changementStatuts: RTFrigoInfo_traitements_execution_changementStatuts[];
  urgence: RTFrigoInfo_traitements_execution_urgence;
  importance: RTFrigoInfo_traitements_execution_importance;
}

export interface RTFrigoInfo_traitements {
  __typename: "TATraitement";
  id: string;
  dataType: string;
  description: string;
  termine: boolean;
  nombreTraitementsCloture: number;
  nombreTraitementsEnRetard: number;
  joursSemaine: number[] | null;
  dateDebut: any | null;
  isPrivate: boolean;
  type: RTFrigoInfo_traitements_type;
  participants: RTFrigoInfo_traitements_participants[];
  importance: RTFrigoInfo_traitements_importance;
  fonction: RTFrigoInfo_traitements_fonction | null;
  tache: RTFrigoInfo_traitements_tache | null;
  repetition: number;
  repetitionUnite: string;
  recurrence: string;
  heure: string;
  dateHeureFin: any | null;
  nombreOccurencesFin: number | null;
  urgence: RTFrigoInfo_traitements_urgence;
  derniereExecutionCloturee: RTFrigoInfo_traitements_derniereExecutionCloturee | null;
  execution: RTFrigoInfo_traitements_execution;
}

export interface RTFrigoInfo_capteurs_pointAcces {
  __typename: "RTPointAcces";
  id: string;
  nom: string;
  numeroSerie: string;
}

export interface RTFrigoInfo_capteurs {
  __typename: "RTCapteur";
  id: string;
  nom: string;
  numeroSerie: string;
  pointAcces: RTFrigoInfo_capteurs_pointAcces;
}

export interface RTFrigoInfo_dernierReleve_releveur_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface RTFrigoInfo_dernierReleve_releveur {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: RTFrigoInfo_dernierReleve_releveur_photo | null;
}

export interface RTFrigoInfo_dernierReleve_status {
  __typename: "RTStatus";
  id: string;
  code: string;
  couleur: string;
  libelle: string;
}

export interface RTFrigoInfo_dernierReleve_frigo {
  __typename: "RTFrigo";
  id: string;
  nom: string;
  temperatureBasse: number;
  temperatureHaute: number;
  temporisation: number | null;
}

export interface RTFrigoInfo_dernierReleve {
  __typename: "RTReleve";
  id: string;
  temperature: number | null;
  type: string;
  createdAt: any;
  releveur: RTFrigoInfo_dernierReleve_releveur | null;
  status: RTFrigoInfo_dernierReleve_status;
  frigo: RTFrigoInfo_dernierReleve_frigo;
}

export interface RTFrigoInfo {
  __typename: "RTFrigo";
  id: string;
  nom: string;
  temperatureBasse: number;
  temperatureHaute: number;
  idPharmacie: string;
  temporisation: number | null;
  typeReleve: string;
  traitements: RTFrigoInfo_traitements[] | null;
  capteurs: RTFrigoInfo_capteurs[] | null;
  dernierReleve: RTFrigoInfo_dernierReleve | null;
}
