import gql from 'graphql-tag';

export const FULL_POINT_ACCES_INFO = gql`
  fragment RTPointAccesInfo on RTPointAcces {
    id
    nom
    numeroSerie
    capteurs{
      id
      nom
      numeroSerie
    }
  }
`;