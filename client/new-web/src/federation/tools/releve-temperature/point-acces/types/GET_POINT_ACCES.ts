/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, RTPointAccesFilter, RTPointAccesSort } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_POINT_ACCES
// ====================================================

export interface GET_POINT_ACCES_rTPointAcces_nodes_capteurs {
  __typename: "RTCapteur";
  id: string;
  nom: string;
  numeroSerie: string;
}

export interface GET_POINT_ACCES_rTPointAcces_nodes {
  __typename: "RTPointAcces";
  id: string;
  nom: string;
  numeroSerie: string;
  capteurs: GET_POINT_ACCES_rTPointAcces_nodes_capteurs[];
}

export interface GET_POINT_ACCES_rTPointAcces {
  __typename: "RTPointAccesConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_POINT_ACCES_rTPointAcces_nodes[];
}

export interface GET_POINT_ACCES {
  rTPointAcces: GET_POINT_ACCES_rTPointAcces;
}

export interface GET_POINT_ACCESVariables {
  paging?: OffsetPaging | null;
  filter?: RTPointAccesFilter | null;
  sorting?: RTPointAccesSort[] | null;
}
