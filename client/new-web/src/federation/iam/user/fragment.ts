import gql from 'graphql-tag';

export const MINIMAL_USER_INFO = gql`
  fragment UserInfo on User {
    id
    dataType
    fullName
    photo {
      id
      chemin
      type
      nomOriginal
      publicUrl
    }
    phoneNumber
  }
`;

export const FULL_USER_INFO = gql`
  fragment UserInfo on User {
    id
    dataType
    fullName
    role {
      id
      code
      nom
    }
    photo {
      id
      chemin
      type
      nomOriginal
      publicUrl
    }
    phoneNumber
  }
`;
