/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: UserInfo
// ====================================================

export interface UserInfo_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface UserInfo_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface UserInfo {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: UserInfo_role | null;
  photo: UserInfo_photo | null;
  phoneNumber: string | null;
}
