/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_ROLES
// ====================================================

export interface GET_ROLES_roles {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface GET_ROLES {
  roles: (GET_ROLES_roles | null)[] | null;
}
