/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_PRT_CONTACT
// ====================================================

export interface SEARCH_PRT_CONTACT_search_data_User {
  __typename: "User" | "TATraitement" | "SuiviAppelType" | "Laboratoire";
}

export interface SEARCH_PRT_CONTACT_search_data_PRTContact_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface SEARCH_PRT_CONTACT_search_data_PRTContact_contact {
  __typename: "ContactType";
  id: string;
  mailProf: string | null;
  mailPerso: string | null;
}

export interface SEARCH_PRT_CONTACT_search_data_PRTContact {
  __typename: "PRTContact";
  id: string;
  dataType: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  fonction: string;
  photo: SEARCH_PRT_CONTACT_search_data_PRTContact_photo | null;
  contact: SEARCH_PRT_CONTACT_search_data_PRTContact_contact | null;
}

export type SEARCH_PRT_CONTACT_search_data = SEARCH_PRT_CONTACT_search_data_User | SEARCH_PRT_CONTACT_search_data_PRTContact;

export interface SEARCH_PRT_CONTACT_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_PRT_CONTACT_search_data | null)[] | null;
}

export interface SEARCH_PRT_CONTACT {
  search: SEARCH_PRT_CONTACT_search | null;
}

export interface SEARCH_PRT_CONTACTVariables {
  index?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
  include?: (string | null)[] | null;
  exclude?: (string | null)[] | null;
}
