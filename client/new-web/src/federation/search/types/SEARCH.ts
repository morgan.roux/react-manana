/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH
// ====================================================

export interface SEARCH_search_data_Laboratoire {
  __typename: "Laboratoire" | "PRTContact";
}

export interface SEARCH_search_data_TATraitement_type {
  __typename: "TATraitementType";
  id: string;
  code: string;
  libelle: string;
}

export interface SEARCH_search_data_TATraitement_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface SEARCH_search_data_TATraitement_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: SEARCH_search_data_TATraitement_participants_photo | null;
}

export interface SEARCH_search_data_TATraitement_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface SEARCH_search_data_TATraitement_fonction {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
}

export interface SEARCH_search_data_TATraitement_tache {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
}

export interface SEARCH_search_data_TATraitement_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface SEARCH_search_data_TATraitement_derniereExecutionCloturee_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface SEARCH_search_data_TATraitement_derniereExecutionCloturee_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface SEARCH_search_data_TATraitement_derniereExecutionCloturee_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface SEARCH_search_data_TATraitement_derniereExecutionCloturee_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface SEARCH_search_data_TATraitement_derniereExecutionCloturee {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: SEARCH_search_data_TATraitement_derniereExecutionCloturee_dernierChangementStatut | null;
  changementStatuts: SEARCH_search_data_TATraitement_derniereExecutionCloturee_changementStatuts[];
  urgence: SEARCH_search_data_TATraitement_derniereExecutionCloturee_urgence;
  importance: SEARCH_search_data_TATraitement_derniereExecutionCloturee_importance;
}

export interface SEARCH_search_data_TATraitement_execution_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface SEARCH_search_data_TATraitement_execution_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface SEARCH_search_data_TATraitement_execution_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface SEARCH_search_data_TATraitement_execution_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface SEARCH_search_data_TATraitement_execution {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: SEARCH_search_data_TATraitement_execution_dernierChangementStatut | null;
  changementStatuts: SEARCH_search_data_TATraitement_execution_changementStatuts[];
  urgence: SEARCH_search_data_TATraitement_execution_urgence;
  importance: SEARCH_search_data_TATraitement_execution_importance;
}

export interface SEARCH_search_data_TATraitement {
  __typename: "TATraitement";
  id: string;
  dataType: string;
  description: string;
  termine: boolean;
  nombreTraitementsCloture: number;
  nombreTraitementsEnRetard: number;
  joursSemaine: number[] | null;
  dateDebut: any | null;
  isPrivate: boolean;
  type: SEARCH_search_data_TATraitement_type;
  participants: SEARCH_search_data_TATraitement_participants[];
  importance: SEARCH_search_data_TATraitement_importance;
  fonction: SEARCH_search_data_TATraitement_fonction | null;
  tache: SEARCH_search_data_TATraitement_tache | null;
  repetition: number;
  repetitionUnite: string;
  recurrence: string;
  heure: string;
  dateHeureFin: any | null;
  nombreOccurencesFin: number | null;
  urgence: SEARCH_search_data_TATraitement_urgence;
  derniereExecutionCloturee: SEARCH_search_data_TATraitement_derniereExecutionCloturee | null;
  execution: SEARCH_search_data_TATraitement_execution;
}

export interface SEARCH_search_data_User_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_search_data_User_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface SEARCH_search_data_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: SEARCH_search_data_User_role | null;
  photo: SEARCH_search_data_User_photo | null;
  phoneNumber: string | null;
}

export interface SEARCH_search_data_SuiviAppelType_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface SEARCH_search_data_SuiviAppelType_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface SEARCH_search_data_SuiviAppelType_interlocuteur_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface SEARCH_search_data_SuiviAppelType_interlocuteur_role {
  __typename: "Role";
  id: string;
  dataType: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_search_data_SuiviAppelType_interlocuteur {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: SEARCH_search_data_SuiviAppelType_interlocuteur_photo | null;
  phoneNumber: string | null;
  role: SEARCH_search_data_SuiviAppelType_interlocuteur_role | null;
}

export interface SEARCH_search_data_SuiviAppelType_declarant_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface SEARCH_search_data_SuiviAppelType_declarant {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: SEARCH_search_data_SuiviAppelType_declarant_photo | null;
  phoneNumber: string | null;
}

export interface SEARCH_search_data_SuiviAppelType_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface SEARCH_search_data_SuiviAppelType_concernes_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface SEARCH_search_data_SuiviAppelType_concernes {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: SEARCH_search_data_SuiviAppelType_concernes_photo | null;
  phoneNumber: string | null;
}

export interface SEARCH_search_data_SuiviAppelType_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface SEARCH_search_data_SuiviAppelType_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface SEARCH_search_data_SuiviAppelType {
  __typename: "SuiviAppelType";
  id: string;
  dataType: string;
  idUserDeclarant: string;
  idUserInterlocuteur: string;
  idFonction: string | null;
  idTache: string | null;
  idImportance: string;
  idUrgence: string;
  commentaire: string | null;
  idFichiers: string[];
  idGroupement: string;
  status: string | null;
  idPharmacie: string;
  createdAt: any;
  updatedAt: any;
  fonction: SEARCH_search_data_SuiviAppelType_fonction | null;
  tache: SEARCH_search_data_SuiviAppelType_tache | null;
  interlocuteur: SEARCH_search_data_SuiviAppelType_interlocuteur;
  declarant: SEARCH_search_data_SuiviAppelType_declarant;
  fichiers: SEARCH_search_data_SuiviAppelType_fichiers[] | null;
  concernes: SEARCH_search_data_SuiviAppelType_concernes[] | null;
  urgence: SEARCH_search_data_SuiviAppelType_urgence;
  importance: SEARCH_search_data_SuiviAppelType_importance;
}

export type SEARCH_search_data = SEARCH_search_data_Laboratoire | SEARCH_search_data_TATraitement | SEARCH_search_data_User | SEARCH_search_data_SuiviAppelType;

export interface SEARCH_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_search_data | null)[] | null;
}

export interface SEARCH {
  search: SEARCH_search | null;
}

export interface SEARCHVariables {
  index?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
  include?: (string | null)[] | null;
  exclude?: (string | null)[] | null;
}
