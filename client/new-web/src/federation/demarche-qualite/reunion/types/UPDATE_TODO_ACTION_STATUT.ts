/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UPDATE_TODO_ACTION_STATUT
// ====================================================

export interface UPDATE_TODO_ACTION_STATUT_updateTodoActionStatut {
  __typename: "TodoAction";
  id: string;
  ordre: number;
  description: string;
  status: string;
}

export interface UPDATE_TODO_ACTION_STATUT {
  updateTodoActionStatut: UPDATE_TODO_ACTION_STATUT_updateTodoActionStatut | null;
}

export interface UPDATE_TODO_ACTION_STATUTVariables {
  id: string;
  code: string;
}
