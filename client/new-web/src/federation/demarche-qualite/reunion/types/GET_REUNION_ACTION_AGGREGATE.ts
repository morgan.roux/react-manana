/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQReunionActionAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_REUNION_ACTION_AGGREGATE
// ====================================================

export interface GET_REUNION_ACTION_AGGREGATE_dQReunionActionAggregate_count {
  __typename: "DQReunionActionCountAggregate";
  id: number | null;
}

export interface GET_REUNION_ACTION_AGGREGATE_dQReunionActionAggregate {
  __typename: "DQReunionActionAggregateResponse";
  count: GET_REUNION_ACTION_AGGREGATE_dQReunionActionAggregate_count | null;
}

export interface GET_REUNION_ACTION_AGGREGATE {
  dQReunionActionAggregate: GET_REUNION_ACTION_AGGREGATE_dQReunionActionAggregate;
}

export interface GET_REUNION_ACTION_AGGREGATEVariables {
  filter?: DQReunionActionAggregateFilter | null;
}
