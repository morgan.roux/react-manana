/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQReunionAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_REUNION_AGGREGATE
// ====================================================

export interface GET_REUNION_AGGREGATE_dQReunionAggregate_count {
  __typename: "DQReunionCountAggregate";
  id: number | null;
}

export interface GET_REUNION_AGGREGATE_dQReunionAggregate {
  __typename: "DQReunionAggregateResponse";
  count: GET_REUNION_AGGREGATE_dQReunionAggregate_count | null;
}

export interface GET_REUNION_AGGREGATE {
  dQReunionAggregate: GET_REUNION_AGGREGATE_dQReunionAggregate;
}

export interface GET_REUNION_AGGREGATEVariables {
  filter?: DQReunionAggregateFilter | null;
}
