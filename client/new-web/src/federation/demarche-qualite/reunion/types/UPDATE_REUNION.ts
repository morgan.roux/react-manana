/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQReunionInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_REUNION
// ====================================================

export interface UPDATE_REUNION_updateOneDQReunion_animateur_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface UPDATE_REUNION_updateOneDQReunion_animateur {
  __typename: "User";
  id: string;
  fullName: string | null;
  email: string | null;
  role: UPDATE_REUNION_updateOneDQReunion_animateur_role | null;
}

export interface UPDATE_REUNION_updateOneDQReunion_responsable_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface UPDATE_REUNION_updateOneDQReunion_responsable {
  __typename: "User";
  id: string;
  fullName: string | null;
  email: string | null;
  role: UPDATE_REUNION_updateOneDQReunion_responsable_role | null;
}

export interface UPDATE_REUNION_updateOneDQReunion_participants_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface UPDATE_REUNION_updateOneDQReunion_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  email: string | null;
  role: UPDATE_REUNION_updateOneDQReunion_participants_role | null;
}

export interface UPDATE_REUNION_updateOneDQReunion {
  __typename: "DQReunion";
  id: string;
  description: string;
  idUserAnimateur: string;
  idResponsable: string;
  createdAt: any;
  updatedAt: any;
  nombreTotalActions: number;
  nombreTotalClotureActions: number;
  animateur: UPDATE_REUNION_updateOneDQReunion_animateur;
  responsable: UPDATE_REUNION_updateOneDQReunion_responsable;
  participants: UPDATE_REUNION_updateOneDQReunion_participants[];
}

export interface UPDATE_REUNION {
  updateOneDQReunion: UPDATE_REUNION_updateOneDQReunion;
}

export interface UPDATE_REUNIONVariables {
  input: DQReunionInput;
  id: string;
}
