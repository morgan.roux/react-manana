/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_REUNION_ACTION
// ====================================================

export interface DELETE_REUNION_ACTION_deleteOneDQReunionAction {
  __typename: "DQReunionActionDeleteResponse";
  id: string | null;
}

export interface DELETE_REUNION_ACTION {
  deleteOneDQReunionAction: DELETE_REUNION_ACTION_deleteOneDQReunionAction;
}

export interface DELETE_REUNION_ACTIONVariables {
  input: DeleteOneInput;
}
