/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_REUNION
// ====================================================

export interface DELETE_REUNION_deleteOneDQReunion {
  __typename: "DQReunionDeleteResponse";
  id: string | null;
  description: string | null;
}

export interface DELETE_REUNION {
  deleteOneDQReunion: DELETE_REUNION_deleteOneDQReunion;
}

export interface DELETE_REUNIONVariables {
  input: DeleteOneInput;
}
