/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ReunionInfo
// ====================================================

export interface ReunionInfo_animateur_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface ReunionInfo_animateur {
  __typename: "User";
  id: string;
  fullName: string | null;
  email: string | null;
  role: ReunionInfo_animateur_role | null;
}

export interface ReunionInfo_responsable_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface ReunionInfo_responsable {
  __typename: "User";
  id: string;
  fullName: string | null;
  email: string | null;
  role: ReunionInfo_responsable_role | null;
}

export interface ReunionInfo_participants_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface ReunionInfo_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  email: string | null;
  role: ReunionInfo_participants_role | null;
}

export interface ReunionInfo {
  __typename: "DQReunion";
  id: string;
  description: string;
  idUserAnimateur: string;
  idResponsable: string;
  createdAt: any;
  updatedAt: any;
  nombreTotalActions: number;
  nombreTotalClotureActions: number;
  animateur: ReunionInfo_animateur;
  responsable: ReunionInfo_responsable;
  participants: ReunionInfo_participants[];
}
