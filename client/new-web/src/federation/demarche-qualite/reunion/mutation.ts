import gql from 'graphql-tag';

import { MINIMAL_REUNION_INFO } from './fragment';

export const CREATE_REUNION = gql`
  mutation CREATE_REUNION($input: DQReunionInput!) {
    createOneDQReunion(input: $input) {
      ...ReunionInfo
    }
  }
  ${MINIMAL_REUNION_INFO}
`;

export const UPDATE_REUNION = gql`
  mutation UPDATE_REUNION($input: DQReunionInput!, $id: String!) {
    updateOneDQReunion(id: $id, input: $input) {
      ...ReunionInfo
    }
  }
  ${MINIMAL_REUNION_INFO}
`;

export const DELETE_REUNION = gql`
  mutation DELETE_REUNION($input: DeleteOneInput!) {
    deleteOneDQReunion(input: $input) {
      id
      description
    }
  }
`;

export const DELETE_MANY_REUNIONS = gql`
  mutation DELETE_MANY_REUNIONS($input: DeleteManyDQReunionsInput!) {
    deleteManyDQReunions(input: $input) {
      deletedCount
    }
  }
`;


export const DELETE_REUNION_ACTION = gql`
  mutation DELETE_REUNION_ACTION($input: DeleteOneInput!) {
    deleteOneDQReunionAction(input: $input) {
      id
    }
  }
`;

export const UPDATE_TODO_ACTION_STATUT = gql`
  mutation UPDATE_TODO_ACTION_STATUT($id: String!, $code: String!) {
    updateTodoActionStatut(id: $id, code: $code) {
      id
      ordre
      description
      status
    }
  }
`;
