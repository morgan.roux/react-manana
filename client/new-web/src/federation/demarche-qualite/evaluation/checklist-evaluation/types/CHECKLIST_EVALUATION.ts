/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CHECKLIST_EVALUATION
// ====================================================

export interface CHECKLIST_EVALUATION_dqChecklistEvaluation_cloture {
  __typename: "DQChecklistEvaluationCloture";
  id: string;
}

export interface CHECKLIST_EVALUATION_dqChecklistEvaluation {
  __typename: "DQChecklistEvaluation";
  id: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
  cloture: CHECKLIST_EVALUATION_dqChecklistEvaluation_cloture | null;
}

export interface CHECKLIST_EVALUATION {
  dqChecklistEvaluation: CHECKLIST_EVALUATION_dqChecklistEvaluation | null;
}

export interface CHECKLIST_EVALUATIONVariables {
  id: string;
}
