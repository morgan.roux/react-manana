/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQChecklistEvaluationInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_CHECKLIST_EVALUATION
// ====================================================

export interface UPDATE_CHECKLIST_EVALUATION_updateDQChecklistEvaluation_cloture {
  __typename: "DQChecklistEvaluationCloture";
  id: string;
}

export interface UPDATE_CHECKLIST_EVALUATION_updateDQChecklistEvaluation {
  __typename: "DQChecklistEvaluation";
  id: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
  cloture: UPDATE_CHECKLIST_EVALUATION_updateDQChecklistEvaluation_cloture | null;
}

export interface UPDATE_CHECKLIST_EVALUATION {
  updateDQChecklistEvaluation: UPDATE_CHECKLIST_EVALUATION_updateDQChecklistEvaluation | null;
}

export interface UPDATE_CHECKLIST_EVALUATIONVariables {
  id: string;
  checklistEvaluationInput: DQChecklistEvaluationInput;
}
