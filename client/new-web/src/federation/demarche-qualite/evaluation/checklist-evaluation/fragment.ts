import gql from 'graphql-tag';
import { MINIMAL_CHECKLIST_INFO } from '../../checklist/fragment';

export const FULL_EVALUATION_INFO = gql`
  fragment ChecklistEvaluationInfo on DQChecklistEvaluation {
    id
    ordre
    libelle
    createdAt
    updatedAt
    cloture{
      id
      commentaire
      fichiers{
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
      createdAt
      updatedAt
    }
    updatedBy{
      id
      fullName
    }
    checklist {
      ...ChecklistInfo
    }
    sections {
      id
      libelle
      ordre
      items {
        id
        ordre
        libelle
        idChecklist
        idChecklistEvaluationSection
        idChecklistSectionItem
        verifie
      }
    }
  }
  ${MINIMAL_CHECKLIST_INFO}
`;

export const MINIMAL_EVALUATION_INFO = gql`
  fragment ChecklistEvaluationInfo on DQChecklistEvaluation {
    id
    ordre
    libelle
    createdAt
    updatedAt
    cloture{
      id
    }
  }
`;
