/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: AfficheInfo
// ====================================================

export interface AfficheInfo {
  __typename: "DQAffiche";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}
