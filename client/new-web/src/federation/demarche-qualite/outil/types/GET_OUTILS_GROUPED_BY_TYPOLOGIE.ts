/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_OUTILS_GROUPED_BY_TYPOLOGIE
// ====================================================

export interface GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie_affiches {
  __typename: "DQAffiche";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie_enregistrements {
  __typename: "DQEnregistrement";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie_memos {
  __typename: "DQMemo";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie_procedures {
  __typename: "DQProcedure";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie_documents {
  __typename: "DQDocument";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie_checklists_sections_items {
  __typename: "DQChecklistSectionItem";
  id: string;
  ordre: number;
  libelle: string;
  nombreEvaluations: number;
}

export interface GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie_checklists_sections {
  __typename: "DQChecklistSection";
  id: string;
  ordre: number;
  libelle: string;
  items: (GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie_checklists_sections_items | null)[] | null;
}

export interface GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie_checklists {
  __typename: "DQChecklist";
  id: string;
  ordre: number;
  libelle: string;
  nombreEvaluations: number;
  createdAt: any | null;
  updatedAt: any | null;
  sections: (GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie_checklists_sections | null)[] | null;
}

export interface GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie {
  __typename: "DQOutilGroupedByTypologie";
  affiches: (GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie_affiches | null)[] | null;
  enregistrements: (GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie_enregistrements | null)[] | null;
  memos: (GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie_memos | null)[] | null;
  procedures: (GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie_procedures | null)[] | null;
  documents: (GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie_documents | null)[] | null;
  checklists: (GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie_checklists | null)[] | null;
}

export interface GET_OUTILS_GROUPED_BY_TYPOLOGIE {
  dqOutilsGroupedByTypologie: GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie | null;
}
