/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQOutilInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_OUTIL
// ====================================================

export interface UPDATE_OUTIL_updateDQOutil_DQAffiche {
  __typename: "DQAffiche";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface UPDATE_OUTIL_updateDQOutil_DQDocument {
  __typename: "DQDocument";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface UPDATE_OUTIL_updateDQOutil_DQEnregistrement {
  __typename: "DQEnregistrement";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface UPDATE_OUTIL_updateDQOutil_DQMemo {
  __typename: "DQMemo";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface UPDATE_OUTIL_updateDQOutil_DQProcedure {
  __typename: "DQProcedure";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export type UPDATE_OUTIL_updateDQOutil = UPDATE_OUTIL_updateDQOutil_DQAffiche | UPDATE_OUTIL_updateDQOutil_DQDocument | UPDATE_OUTIL_updateDQOutil_DQEnregistrement | UPDATE_OUTIL_updateDQOutil_DQMemo | UPDATE_OUTIL_updateDQOutil_DQProcedure;

export interface UPDATE_OUTIL {
  updateDQOutil: UPDATE_OUTIL_updateDQOutil | null;
}

export interface UPDATE_OUTILVariables {
  id: string;
  typologie: string;
  outilInput: DQOutilInput;
}
