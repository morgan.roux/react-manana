/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_OUTIL
// ====================================================

export interface DELETE_OUTIL_deleteDQOutil_DQAffiche {
  __typename: "DQAffiche";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface DELETE_OUTIL_deleteDQOutil_DQDocument {
  __typename: "DQDocument";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface DELETE_OUTIL_deleteDQOutil_DQEnregistrement {
  __typename: "DQEnregistrement";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface DELETE_OUTIL_deleteDQOutil_DQMemo {
  __typename: "DQMemo";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface DELETE_OUTIL_deleteDQOutil_DQProcedure {
  __typename: "DQProcedure";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export type DELETE_OUTIL_deleteDQOutil = DELETE_OUTIL_deleteDQOutil_DQAffiche | DELETE_OUTIL_deleteDQOutil_DQDocument | DELETE_OUTIL_deleteDQOutil_DQEnregistrement | DELETE_OUTIL_deleteDQOutil_DQMemo | DELETE_OUTIL_deleteDQOutil_DQProcedure;

export interface DELETE_OUTIL {
  deleteDQOutil: DELETE_OUTIL_deleteDQOutil | null;
}

export interface DELETE_OUTILVariables {
  id: string;
  typologie: string;
}
