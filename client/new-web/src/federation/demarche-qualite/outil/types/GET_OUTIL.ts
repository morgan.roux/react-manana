/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_OUTIL
// ====================================================

export interface GET_OUTIL_dqOutil_DQAffiche {
  __typename: "DQAffiche";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface GET_OUTIL_dqOutil_DQDocument {
  __typename: "DQDocument";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface GET_OUTIL_dqOutil_DQEnregistrement {
  __typename: "DQEnregistrement";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface GET_OUTIL_dqOutil_DQMemo {
  __typename: "DQMemo";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface GET_OUTIL_dqOutil_DQProcedure {
  __typename: "DQProcedure";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export type GET_OUTIL_dqOutil = GET_OUTIL_dqOutil_DQAffiche | GET_OUTIL_dqOutil_DQDocument | GET_OUTIL_dqOutil_DQEnregistrement | GET_OUTIL_dqOutil_DQMemo | GET_OUTIL_dqOutil_DQProcedure;

export interface GET_OUTIL {
  dqOutil: GET_OUTIL_dqOutil | null;
}

export interface GET_OUTILVariables {
  id: string;
  typologie: string;
}
