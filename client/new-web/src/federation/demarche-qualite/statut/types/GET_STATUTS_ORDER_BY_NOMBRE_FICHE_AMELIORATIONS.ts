/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_STATUTS_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS
// ====================================================

export interface GET_STATUTS_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS_dQStatutsOrderByNombreFicheAmeliorations {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  type: string;
  code: string;
  nombreActionOperationnelles: number;
}

export interface GET_STATUTS_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS {
  dQStatutsOrderByNombreFicheAmeliorations: GET_STATUTS_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS_dQStatutsOrderByNombreFicheAmeliorations[];
}

export interface GET_STATUTS_ORDER_BY_NOMBRE_FICHE_AMELIORATIONSVariables {
  searchText?: string | null;
}
