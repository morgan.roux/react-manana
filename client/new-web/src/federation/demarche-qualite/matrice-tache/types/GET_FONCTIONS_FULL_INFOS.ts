/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, DQMTFonctionFilter, DQMTFonctionSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_FONCTIONS_FULL_INFOS
// ====================================================

export interface GET_FONCTIONS_FULL_INFOS_dQMTFonctions_nodes_taches {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
  idProjet: string | null;
}

export interface GET_FONCTIONS_FULL_INFOS_dQMTFonctions_nodes {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
  nombreTaches: number;
  idProjet: string | null;
  taches: GET_FONCTIONS_FULL_INFOS_dQMTFonctions_nodes_taches[];
}

export interface GET_FONCTIONS_FULL_INFOS_dQMTFonctions {
  __typename: "DQMTFonctionConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_FONCTIONS_FULL_INFOS_dQMTFonctions_nodes[];
}

export interface GET_FONCTIONS_FULL_INFOS {
  dQMTFonctions: GET_FONCTIONS_FULL_INFOS_dQMTFonctions;
}

export interface GET_FONCTIONS_FULL_INFOSVariables {
  paging?: OffsetPaging | null;
  filter?: DQMTFonctionFilter | null;
  sorting?: DQMTFonctionSort[] | null;
  idPharmacie: string;
}
