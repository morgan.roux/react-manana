/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: TOGGLE_ONE_FONCTION_ACTIVATION
// ====================================================

export interface TOGGLE_ONE_FONCTION_ACTIVATION_toggleOneDQMtFonctionActivation {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
  nombreTaches: number;
  active: boolean;
}

export interface TOGGLE_ONE_FONCTION_ACTIVATION {
  toggleOneDQMtFonctionActivation: TOGGLE_ONE_FONCTION_ACTIVATION_toggleOneDQMtFonctionActivation;
}

export interface TOGGLE_ONE_FONCTION_ACTIVATIONVariables {
  idPharmacie: string;
  idFonction: string;
}
