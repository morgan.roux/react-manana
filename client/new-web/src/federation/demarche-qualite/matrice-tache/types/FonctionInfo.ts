/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: FonctionInfo
// ====================================================

export interface FonctionInfo_taches {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
}

export interface FonctionInfo {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
  nombreTaches: number;
  taches: FonctionInfo_taches[];
}
