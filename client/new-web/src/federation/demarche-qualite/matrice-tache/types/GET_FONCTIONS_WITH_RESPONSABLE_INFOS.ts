/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, DQMTFonctionFilter, DQMTFonctionSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_FONCTIONS_WITH_RESPONSABLE_INFOS
// ====================================================

export interface GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_collaborateurResponsables_type {
  __typename: "DQMTResponsableType";
  id: string;
  code: string;
  ordre: number;
  couleur: string;
  libelle: string;
  max: number | null;
}

export interface GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_collaborateurResponsables_responsables {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_collaborateurResponsables {
  __typename: "DQMTTacheResponsableCollaborateur";
  idFonction: string;
  idTache: string | null;
  type: GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_collaborateurResponsables_type;
  responsables: GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_collaborateurResponsables_responsables[];
}

export interface GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches_collaborateurResponsables_type {
  __typename: "DQMTResponsableType";
  id: string;
  code: string;
  ordre: number;
  couleur: string;
  libelle: string;
  max: number | null;
}

export interface GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches_collaborateurResponsables_responsables {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches_collaborateurResponsables {
  __typename: "DQMTTacheResponsableCollaborateur";
  idFonction: string;
  idTache: string | null;
  type: GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches_collaborateurResponsables_type;
  responsables: GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches_collaborateurResponsables_responsables[];
}

export interface GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
  idProjet: string | null;
  active: boolean;
  collaborateurResponsables: GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches_collaborateurResponsables[];
}

export interface GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
  nombreTaches: number;
  idProjet: string | null;
  active: boolean;
  collaborateurResponsables: GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_collaborateurResponsables[];
  taches: GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches[];
}

export interface GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions {
  __typename: "DQMTFonctionConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes[];
}

export interface GET_FONCTIONS_WITH_RESPONSABLE_INFOS {
  dQMTFonctions: GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions;
}

export interface GET_FONCTIONS_WITH_RESPONSABLE_INFOSVariables {
  paging?: OffsetPaging | null;
  filter?: DQMTFonctionFilter | null;
  sorting?: DQMTFonctionSort[] | null;
  idPharmacie: string;
}
