/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_FONCTION
// ====================================================

export interface GET_FONCTION_dQMTFonction_taches {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
  active: boolean;
}

export interface GET_FONCTION_dQMTFonction {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
  nombreTaches: number;
  active: boolean;
  taches: GET_FONCTION_dQMTFonction_taches[];
}

export interface GET_FONCTION {
  dQMTFonction: GET_FONCTION_dQMTFonction | null;
}

export interface GET_FONCTIONVariables {
  id: string;
  idPharmacie: string;
}
