/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_TACHE
// ====================================================

export interface DELETE_TACHE_deleteOneDQMTTache {
  __typename: "DQMTTacheDeleteResponse";
  id: string | null;
}

export interface DELETE_TACHE {
  deleteOneDQMTTache: DELETE_TACHE_deleteOneDQMTTache;
}

export interface DELETE_TACHEVariables {
  input: DeleteOneInput;
}
