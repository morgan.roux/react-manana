/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TacheInfo
// ====================================================

export interface TacheInfo {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
}
