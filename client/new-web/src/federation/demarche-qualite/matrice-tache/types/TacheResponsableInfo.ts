/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TacheResponsableInfo
// ====================================================

export interface TacheResponsableInfo {
  __typename: "DQMTTacheResponsable";
  id: string;
  idType: string;
  idUser: string;
  idTache: string | null;
  idFonction: string;
  idPharmacie: string;
  idGroupement: string;
}
