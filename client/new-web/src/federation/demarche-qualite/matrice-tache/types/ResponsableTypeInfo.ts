/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ResponsableTypeInfo
// ====================================================

export interface ResponsableTypeInfo {
  __typename: "DQMTResponsableType";
  id: string;
  code: string;
  ordre: number;
  couleur: string;
  libelle: string;
}
