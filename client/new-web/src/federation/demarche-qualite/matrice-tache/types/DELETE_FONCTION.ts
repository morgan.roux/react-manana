/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_FONCTION
// ====================================================

export interface DELETE_FONCTION_deleteOneDQMTFonction {
  __typename: "DQMTFonctionDeleteResponse";
  id: string | null;
}

export interface DELETE_FONCTION {
  deleteOneDQMTFonction: DELETE_FONCTION_deleteOneDQMTFonction;
}

export interface DELETE_FONCTIONVariables {
  input: DeleteOneInput;
}
