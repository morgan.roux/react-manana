/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, DQFicheTypeFilter, DQFicheTypeSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: FICHE_TYPES
// ====================================================

export interface FICHE_TYPES_dQFicheTypes_nodes {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface FICHE_TYPES_dQFicheTypes {
  __typename: "DQFicheTypeConnection";
  /**
   * Array of nodes.
   */
  nodes: FICHE_TYPES_dQFicheTypes_nodes[];
}

export interface FICHE_TYPES {
  dQFicheTypes: FICHE_TYPES_dQFicheTypes;
}

export interface FICHE_TYPESVariables {
  paging?: OffsetPaging | null;
  filter?: DQFicheTypeFilter | null;
  sorting?: DQFicheTypeSort[] | null;
}
