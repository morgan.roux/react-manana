/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQSolutionInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_SOLUTION
// ====================================================

export interface CREATE_SOLUTION_createOneDQSolution_user_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_SOLUTION_createOneDQSolution_user {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CREATE_SOLUTION_createOneDQSolution_user_photo | null;
}

export interface CREATE_SOLUTION_createOneDQSolution {
  __typename: "DQSolution";
  id: string;
  description: string;
  user: CREATE_SOLUTION_createOneDQSolution_user;
  createdAt: any;
}

export interface CREATE_SOLUTION {
  createOneDQSolution: CREATE_SOLUTION_createOneDQSolution;
}

export interface CREATE_SOLUTIONVariables {
  idFicheIncident: string;
  solution: DQSolutionInput;
}
