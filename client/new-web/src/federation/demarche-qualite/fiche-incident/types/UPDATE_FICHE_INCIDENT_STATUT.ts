/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UPDATE_FICHE_INCIDENT_STATUT
// ====================================================

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_declarant_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_declarant {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_declarant_photo | null;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_statut {
  __typename: "DQStatut";
  id: string;
  code: string;
  libelle: string;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_origineAssocie_User_photo | null;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_origineAssocie = UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_origineAssocie_User | UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_origineAssocie_Laboratoire | UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_origineAssocie_PrestataireService;

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_origine {
  __typename: "Origine";
  id: string;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_participants_photo | null;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_participantsAInformer_photo | null;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut {
  __typename: "DQFicheIncident";
  id: string;
  description: string;
  idFicheAmelioration: string | null;
  idTicketReclamation: string | null;
  idSolution: string | null;
  idUserDeclarant: string;
  idType: string | null;
  idCause: string | null;
  dateIncident: any | null;
  dateEcheance: any | null;
  idOrigine: string;
  idOrigineAssocie: string | null;
  createdAt: any;
  updatedAt: any;
  isPrivate: boolean;
  declarant: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_declarant;
  statut: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_statut;
  origineAssocie: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_origineAssocie | null;
  origine: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_origine;
  urgence: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_urgence;
  importance: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_importance;
  fonction: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_fonction | null;
  tache: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_tache | null;
  fonctionAInformer: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_fonctionAInformer | null;
  tacheAInformer: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_tacheAInformer | null;
  type: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_type | null;
  cause: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_cause | null;
  participants: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_participants[];
  participantsAInformer: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_participantsAInformer[];
  reunion: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut_reunion | null;
}

export interface UPDATE_FICHE_INCIDENT_STATUT {
  updateFicheIncidentStatut: UPDATE_FICHE_INCIDENT_STATUT_updateFicheIncidentStatut;
}

export interface UPDATE_FICHE_INCIDENT_STATUTVariables {
  id: string;
  code: string;
}
