/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteManyDQFicheIncidentsInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_MANY_FICHE_INCIDENTS
// ====================================================

export interface DELETE_MANY_FICHE_INCIDENTS_deleteManyDQFicheIncidents {
  __typename: "DeleteManyResponse";
  /**
   * The number of records deleted.
   */
  deletedCount: number;
}

export interface DELETE_MANY_FICHE_INCIDENTS {
  deleteManyDQFicheIncidents: DELETE_MANY_FICHE_INCIDENTS_deleteManyDQFicheIncidents;
}

export interface DELETE_MANY_FICHE_INCIDENTSVariables {
  input: DeleteManyDQFicheIncidentsInput;
}
