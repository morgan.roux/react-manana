/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: FicheIncidentInfo
// ====================================================

export interface FicheIncidentInfo_declarant_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface FicheIncidentInfo_declarant {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: FicheIncidentInfo_declarant_photo | null;
}

export interface FicheIncidentInfo_statut {
  __typename: "DQStatut";
  id: string;
  code: string;
  libelle: string;
}

export interface FicheIncidentInfo_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface FicheIncidentInfo_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: FicheIncidentInfo_origineAssocie_User_photo | null;
}

export interface FicheIncidentInfo_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface FicheIncidentInfo_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type FicheIncidentInfo_origineAssocie = FicheIncidentInfo_origineAssocie_User | FicheIncidentInfo_origineAssocie_Laboratoire | FicheIncidentInfo_origineAssocie_PrestataireService;

export interface FicheIncidentInfo_origine {
  __typename: "Origine";
  id: string;
}

export interface FicheIncidentInfo_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface FicheIncidentInfo_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface FicheIncidentInfo_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface FicheIncidentInfo_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface FicheIncidentInfo_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface FicheIncidentInfo_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface FicheIncidentInfo_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface FicheIncidentInfo_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface FicheIncidentInfo_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface FicheIncidentInfo_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: FicheIncidentInfo_participants_photo | null;
}

export interface FicheIncidentInfo_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface FicheIncidentInfo_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: FicheIncidentInfo_participantsAInformer_photo | null;
}

export interface FicheIncidentInfo_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface FicheIncidentInfo {
  __typename: "DQFicheIncident";
  id: string;
  description: string;
  idFicheAmelioration: string | null;
  idTicketReclamation: string | null;
  idSolution: string | null;
  idUserDeclarant: string;
  idType: string | null;
  idCause: string | null;
  dateIncident: any | null;
  dateEcheance: any | null;
  idOrigine: string;
  idOrigineAssocie: string | null;
  createdAt: any;
  updatedAt: any;
  isPrivate: boolean;
  declarant: FicheIncidentInfo_declarant;
  statut: FicheIncidentInfo_statut;
  origineAssocie: FicheIncidentInfo_origineAssocie | null;
  origine: FicheIncidentInfo_origine;
  urgence: FicheIncidentInfo_urgence;
  importance: FicheIncidentInfo_importance;
  fonction: FicheIncidentInfo_fonction | null;
  tache: FicheIncidentInfo_tache | null;
  fonctionAInformer: FicheIncidentInfo_fonctionAInformer | null;
  tacheAInformer: FicheIncidentInfo_tacheAInformer | null;
  type: FicheIncidentInfo_type | null;
  cause: FicheIncidentInfo_cause | null;
  participants: FicheIncidentInfo_participants[];
  participantsAInformer: FicheIncidentInfo_participantsAInformer[];
  reunion: FicheIncidentInfo_reunion | null;
}
