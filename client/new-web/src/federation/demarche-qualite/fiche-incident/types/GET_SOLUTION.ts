/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_SOLUTION
// ====================================================

export interface GET_SOLUTION_dQSolution_user_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_SOLUTION_dQSolution_user {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_SOLUTION_dQSolution_user_photo | null;
}

export interface GET_SOLUTION_dQSolution {
  __typename: "DQSolution";
  id: string;
  description: string;
  user: GET_SOLUTION_dQSolution_user;
  createdAt: any;
}

export interface GET_SOLUTION {
  dQSolution: GET_SOLUTION_dQSolution | null;
}

export interface GET_SOLUTIONVariables {
  id: string;
}
