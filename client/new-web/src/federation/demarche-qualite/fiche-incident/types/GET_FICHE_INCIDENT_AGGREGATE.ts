/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQFicheIncidentAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_FICHE_INCIDENT_AGGREGATE
// ====================================================

export interface GET_FICHE_INCIDENT_AGGREGATE_dQFicheIncidentAggregate_count {
  __typename: "DQFicheIncidentCountAggregate";
  id: number | null;
}

export interface GET_FICHE_INCIDENT_AGGREGATE_dQFicheIncidentAggregate {
  __typename: "DQFicheIncidentAggregateResponse";
  count: GET_FICHE_INCIDENT_AGGREGATE_dQFicheIncidentAggregate_count | null;
}

export interface GET_FICHE_INCIDENT_AGGREGATE {
  dQFicheIncidentAggregate: GET_FICHE_INCIDENT_AGGREGATE_dQFicheIncidentAggregate;
}

export interface GET_FICHE_INCIDENT_AGGREGATEVariables {
  filter?: DQFicheIncidentAggregateFilter | null;
}
