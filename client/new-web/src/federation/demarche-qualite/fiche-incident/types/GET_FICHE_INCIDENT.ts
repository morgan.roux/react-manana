/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_FICHE_INCIDENT
// ====================================================

export interface GET_FICHE_INCIDENT_dQFicheIncident_declarant_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_declarant {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_FICHE_INCIDENT_dQFicheIncident_declarant_photo | null;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_statut {
  __typename: "DQStatut";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: GET_FICHE_INCIDENT_dQFicheIncident_origineAssocie_User_photo | null;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type GET_FICHE_INCIDENT_dQFicheIncident_origineAssocie = GET_FICHE_INCIDENT_dQFicheIncident_origineAssocie_User | GET_FICHE_INCIDENT_dQFicheIncident_origineAssocie_Laboratoire | GET_FICHE_INCIDENT_dQFicheIncident_origineAssocie_PrestataireService;

export interface GET_FICHE_INCIDENT_dQFicheIncident_origine {
  __typename: "Origine";
  id: string;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_FICHE_INCIDENT_dQFicheIncident_participants_photo | null;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_FICHE_INCIDENT_dQFicheIncident_participantsAInformer_photo | null;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface GET_FICHE_INCIDENT_dQFicheIncident {
  __typename: "DQFicheIncident";
  id: string;
  description: string;
  idFicheAmelioration: string | null;
  idTicketReclamation: string | null;
  idSolution: string | null;
  idUserDeclarant: string;
  idType: string | null;
  idCause: string | null;
  dateIncident: any | null;
  dateEcheance: any | null;
  idOrigine: string;
  idOrigineAssocie: string | null;
  createdAt: any;
  updatedAt: any;
  isPrivate: boolean;
  declarant: GET_FICHE_INCIDENT_dQFicheIncident_declarant;
  statut: GET_FICHE_INCIDENT_dQFicheIncident_statut;
  origineAssocie: GET_FICHE_INCIDENT_dQFicheIncident_origineAssocie | null;
  origine: GET_FICHE_INCIDENT_dQFicheIncident_origine;
  urgence: GET_FICHE_INCIDENT_dQFicheIncident_urgence;
  importance: GET_FICHE_INCIDENT_dQFicheIncident_importance;
  fonction: GET_FICHE_INCIDENT_dQFicheIncident_fonction | null;
  tache: GET_FICHE_INCIDENT_dQFicheIncident_tache | null;
  fonctionAInformer: GET_FICHE_INCIDENT_dQFicheIncident_fonctionAInformer | null;
  tacheAInformer: GET_FICHE_INCIDENT_dQFicheIncident_tacheAInformer | null;
  type: GET_FICHE_INCIDENT_dQFicheIncident_type | null;
  cause: GET_FICHE_INCIDENT_dQFicheIncident_cause | null;
  participants: GET_FICHE_INCIDENT_dQFicheIncident_participants[];
  participantsAInformer: GET_FICHE_INCIDENT_dQFicheIncident_participantsAInformer[];
  reunion: GET_FICHE_INCIDENT_dQFicheIncident_reunion | null;
}

export interface GET_FICHE_INCIDENT {
  dQFicheIncident: GET_FICHE_INCIDENT_dQFicheIncident | null;
}

export interface GET_FICHE_INCIDENTVariables {
  id: string;
}
