import gql from 'graphql-tag';

export const PERCEPTION_INFO = gql`
  fragment PerceptionInfo on DQPerception {
    id
    libelle
  }
`;

export const PERCEPTION_WITH_NOMBRE_FICHE_AMELIORATIONS_INFO = gql`
  fragment PerceptionInfo on DQPerception {
    id
    libelle
    nombreFicheAmeliorations
  }
`;
