/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQFicheAmeliorationAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_FICHE_AMELIORATION_AGGREGATE
// ====================================================

export interface GET_FICHE_AMELIORATION_AGGREGATE_dQFicheAmeliorationAggregate_count {
  __typename: "DQFicheAmeliorationCountAggregate";
  id: number | null;
}

export interface GET_FICHE_AMELIORATION_AGGREGATE_dQFicheAmeliorationAggregate {
  __typename: "DQFicheAmeliorationAggregateResponse";
  count: GET_FICHE_AMELIORATION_AGGREGATE_dQFicheAmeliorationAggregate_count | null;
}

export interface GET_FICHE_AMELIORATION_AGGREGATE {
  dQFicheAmeliorationAggregate: GET_FICHE_AMELIORATION_AGGREGATE_dQFicheAmeliorationAggregate;
}

export interface GET_FICHE_AMELIORATION_AGGREGATEVariables {
  filter?: DQFicheAmeliorationAggregateFilter | null;
}
