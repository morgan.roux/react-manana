/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: FicheAmeliorationInfo
// ====================================================

export interface FicheAmeliorationInfo_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface FicheAmeliorationInfo_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: FicheAmeliorationInfo_origineAssocie_User_photo | null;
}

export interface FicheAmeliorationInfo_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface FicheAmeliorationInfo_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type FicheAmeliorationInfo_origineAssocie = FicheAmeliorationInfo_origineAssocie_User | FicheAmeliorationInfo_origineAssocie_Laboratoire | FicheAmeliorationInfo_origineAssocie_PrestataireService;

export interface FicheAmeliorationInfo_statut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface FicheAmeliorationInfo_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface FicheAmeliorationInfo_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface FicheAmeliorationInfo_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface FicheAmeliorationInfo_auteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface FicheAmeliorationInfo_origine {
  __typename: "Origine";
  id: string;
}

export interface FicheAmeliorationInfo_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface FicheAmeliorationInfo_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface FicheAmeliorationInfo_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface FicheAmeliorationInfo_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface FicheAmeliorationInfo_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface FicheAmeliorationInfo_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface FicheAmeliorationInfo_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface FicheAmeliorationInfo_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: FicheAmeliorationInfo_participants_photo | null;
}

export interface FicheAmeliorationInfo_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface FicheAmeliorationInfo_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: FicheAmeliorationInfo_participantsAInformer_photo | null;
}

export interface FicheAmeliorationInfo_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface FicheAmeliorationInfo {
  __typename: "DQFicheAmelioration";
  id: string;
  description: string;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  dateAmelioration: any | null;
  dateEcheance: any | null;
  idType: string | null;
  idCause: string | null;
  idAction: string | null;
  createdAt: any;
  updatedAt: any;
  origineAssocie: FicheAmeliorationInfo_origineAssocie | null;
  statut: FicheAmeliorationInfo_statut;
  cause: FicheAmeliorationInfo_cause | null;
  type: FicheAmeliorationInfo_type | null;
  fichiers: FicheAmeliorationInfo_fichiers[];
  auteur: FicheAmeliorationInfo_auteur;
  origine: FicheAmeliorationInfo_origine | null;
  urgence: FicheAmeliorationInfo_urgence;
  importance: FicheAmeliorationInfo_importance;
  fonction: FicheAmeliorationInfo_fonction | null;
  tache: FicheAmeliorationInfo_tache | null;
  fonctionAInformer: FicheAmeliorationInfo_fonctionAInformer | null;
  tacheAInformer: FicheAmeliorationInfo_tacheAInformer | null;
  participants: FicheAmeliorationInfo_participants[];
  participantsAInformer: FicheAmeliorationInfo_participantsAInformer[];
  reunion: FicheAmeliorationInfo_reunion | null;
}
