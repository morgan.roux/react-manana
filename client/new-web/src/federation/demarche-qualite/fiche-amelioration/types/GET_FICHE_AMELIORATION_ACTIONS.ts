/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, DQFicheAmeliorationActionFilter, DQFicheAmeliorationActionSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_FICHE_AMELIORATION_ACTIONS
// ====================================================

export interface GET_FICHE_AMELIORATION_ACTIONS_dQFicheAmeliorationActions_nodes_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface GET_FICHE_AMELIORATION_ACTIONS_dQFicheAmeliorationActions_nodes {
  __typename: "DQFicheAmeliorationAction";
  id: string;
  description: string;
  dateHeureMiseEnplace: any;
  createdAt: any;
  updatedAt: any;
  fichiers: GET_FICHE_AMELIORATION_ACTIONS_dQFicheAmeliorationActions_nodes_fichiers[];
}

export interface GET_FICHE_AMELIORATION_ACTIONS_dQFicheAmeliorationActions {
  __typename: "DQFicheAmeliorationActionConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_FICHE_AMELIORATION_ACTIONS_dQFicheAmeliorationActions_nodes[];
}

export interface GET_FICHE_AMELIORATION_ACTIONS {
  dQFicheAmeliorationActions: GET_FICHE_AMELIORATION_ACTIONS_dQFicheAmeliorationActions;
}

export interface GET_FICHE_AMELIORATION_ACTIONSVariables {
  paging?: OffsetPaging | null;
  filter?: DQFicheAmeliorationActionFilter | null;
  sorting?: DQFicheAmeliorationActionSort[] | null;
}
