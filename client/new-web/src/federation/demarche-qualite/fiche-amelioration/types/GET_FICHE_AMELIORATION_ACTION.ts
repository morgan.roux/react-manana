/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_FICHE_AMELIORATION_ACTION
// ====================================================

export interface GET_FICHE_AMELIORATION_ACTION_dQFicheAmeliorationAction_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface GET_FICHE_AMELIORATION_ACTION_dQFicheAmeliorationAction {
  __typename: "DQFicheAmeliorationAction";
  id: string;
  description: string;
  dateHeureMiseEnplace: any;
  createdAt: any;
  updatedAt: any;
  fichiers: GET_FICHE_AMELIORATION_ACTION_dQFicheAmeliorationAction_fichiers[];
}

export interface GET_FICHE_AMELIORATION_ACTION {
  dQFicheAmeliorationAction: GET_FICHE_AMELIORATION_ACTION_dQFicheAmeliorationAction | null;
}

export interface GET_FICHE_AMELIORATION_ACTIONVariables {
  id: string;
}
