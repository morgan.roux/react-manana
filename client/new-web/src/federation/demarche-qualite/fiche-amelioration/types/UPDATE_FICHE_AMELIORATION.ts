/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQFicheAmeliorationInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_FICHE_AMELIORATION
// ====================================================

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_origineAssocie_User_photo | null;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_origineAssocie = UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_origineAssocie_User | UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_origineAssocie_Laboratoire | UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_origineAssocie_PrestataireService;

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_statut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_auteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_origine {
  __typename: "Origine";
  id: string;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_participants_photo | null;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_participantsAInformer_photo | null;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration {
  __typename: "DQFicheAmelioration";
  id: string;
  description: string;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  dateAmelioration: any | null;
  dateEcheance: any | null;
  idType: string | null;
  idCause: string | null;
  idAction: string | null;
  createdAt: any;
  updatedAt: any;
  origineAssocie: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_origineAssocie | null;
  statut: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_statut;
  cause: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_cause | null;
  type: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_type | null;
  fichiers: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_fichiers[];
  auteur: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_auteur;
  origine: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_origine | null;
  urgence: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_urgence;
  importance: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_importance;
  fonction: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_fonction | null;
  tache: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_tache | null;
  fonctionAInformer: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_fonctionAInformer | null;
  tacheAInformer: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_tacheAInformer | null;
  participants: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_participants[];
  participantsAInformer: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_participantsAInformer[];
  reunion: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration_reunion | null;
}

export interface UPDATE_FICHE_AMELIORATION {
  updateOneDQFicheAmelioration: UPDATE_FICHE_AMELIORATION_updateOneDQFicheAmelioration;
}

export interface UPDATE_FICHE_AMELIORATIONVariables {
  id: string;
  update: DQFicheAmeliorationInput;
}
