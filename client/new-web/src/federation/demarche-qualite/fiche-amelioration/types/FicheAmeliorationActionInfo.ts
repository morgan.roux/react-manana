/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: FicheAmeliorationActionInfo
// ====================================================

export interface FicheAmeliorationActionInfo_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface FicheAmeliorationActionInfo {
  __typename: "DQFicheAmeliorationAction";
  id: string;
  description: string;
  dateHeureMiseEnplace: any;
  createdAt: any;
  updatedAt: any;
  fichiers: FicheAmeliorationActionInfo_fichiers[];
}
