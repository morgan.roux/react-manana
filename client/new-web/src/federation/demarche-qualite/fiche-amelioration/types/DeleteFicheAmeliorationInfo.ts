/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: DeleteFicheAmeliorationInfo
// ====================================================

export interface DeleteFicheAmeliorationInfo {
  __typename: "DQFicheAmeliorationDeleteResponse";
  id: string | null;
  description: string | null;
}
