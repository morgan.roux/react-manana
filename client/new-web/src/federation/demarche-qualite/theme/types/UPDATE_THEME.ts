/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQThemeInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_THEME
// ====================================================

export interface UPDATE_THEME_updateDQTheme {
  __typename: "DQTheme";
  id: string;
  ordre: number | null;
  nom: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  nombreSousThemes: number;
}

export interface UPDATE_THEME {
  updateDQTheme: UPDATE_THEME_updateDQTheme | null;
}

export interface UPDATE_THEMEVariables {
  id: string;
  themeInput: DQThemeInput;
}
