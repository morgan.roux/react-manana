/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQSousThemeInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_SOUS_THEME
// ====================================================

export interface UPDATE_SOUS_THEME_updateDQSousTheme_exigences {
  __typename: "DQExigence";
  id: string;
  ordre: number | null;
  finalite: string | null;
  titre: string | null;
  nombreOutils: number;
}

export interface UPDATE_SOUS_THEME_updateDQSousTheme {
  __typename: "DQSousTheme";
  id: string;
  ordre: number | null;
  nom: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  nombreExigences: number;
  exigences: (UPDATE_SOUS_THEME_updateDQSousTheme_exigences | null)[] | null;
}

export interface UPDATE_SOUS_THEME {
  updateDQSousTheme: UPDATE_SOUS_THEME_updateDQSousTheme | null;
}

export interface UPDATE_SOUS_THEMEVariables {
  id: string;
  sousThemeInput: DQSousThemeInput;
}
