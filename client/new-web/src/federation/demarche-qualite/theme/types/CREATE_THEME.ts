/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQThemeInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_THEME
// ====================================================

export interface CREATE_THEME_createDQTheme {
  __typename: "DQTheme";
  id: string;
  ordre: number | null;
  nom: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  nombreSousThemes: number;
}

export interface CREATE_THEME {
  createDQTheme: CREATE_THEME_createDQTheme | null;
}

export interface CREATE_THEMEVariables {
  themeInput: DQThemeInput;
}
