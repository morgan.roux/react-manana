/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_EXIGENCE
// ====================================================

export interface DELETE_EXIGENCE_deleteDQExigence {
  __typename: "DQExigence";
  id: string;
  ordre: number | null;
  finalite: string | null;
  titre: string | null;
}

export interface DELETE_EXIGENCE {
  deleteDQExigence: DELETE_EXIGENCE_deleteDQExigence | null;
}

export interface DELETE_EXIGENCEVariables {
  id: string;
}
