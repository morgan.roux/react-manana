/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_THEME
// ====================================================

export interface DELETE_THEME_deleteDQTheme {
  __typename: "DQTheme";
  id: string;
  ordre: number | null;
  nom: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  nombreSousThemes: number;
}

export interface DELETE_THEME {
  deleteDQTheme: DELETE_THEME_deleteDQTheme | null;
}

export interface DELETE_THEMEVariables {
  id: string;
}
