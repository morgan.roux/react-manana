/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_MINIMAL_SOUS_THEME
// ====================================================

export interface GET_MINIMAL_SOUS_THEME_dqSousTheme_exigences {
  __typename: "DQExigence";
  id: string;
  ordre: number | null;
  finalite: string | null;
  titre: string | null;
  nombreOutils: number;
}

export interface GET_MINIMAL_SOUS_THEME_dqSousTheme {
  __typename: "DQSousTheme";
  id: string;
  ordre: number | null;
  nom: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  nombreExigences: number;
  exigences: (GET_MINIMAL_SOUS_THEME_dqSousTheme_exigences | null)[] | null;
}

export interface GET_MINIMAL_SOUS_THEME {
  dqSousTheme: GET_MINIMAL_SOUS_THEME_dqSousTheme | null;
}

export interface GET_MINIMAL_SOUS_THEMEVariables {
  id: string;
}
