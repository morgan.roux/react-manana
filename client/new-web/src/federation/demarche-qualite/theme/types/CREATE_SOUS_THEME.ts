/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQSousThemeInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_SOUS_THEME
// ====================================================

export interface CREATE_SOUS_THEME_createDQSousTheme_exigences {
  __typename: "DQExigence";
  id: string;
  ordre: number | null;
  finalite: string | null;
  titre: string | null;
  nombreOutils: number;
}

export interface CREATE_SOUS_THEME_createDQSousTheme {
  __typename: "DQSousTheme";
  id: string;
  ordre: number | null;
  nom: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  nombreExigences: number;
  exigences: (CREATE_SOUS_THEME_createDQSousTheme_exigences | null)[] | null;
}

export interface CREATE_SOUS_THEME {
  createDQSousTheme: CREATE_SOUS_THEME_createDQSousTheme | null;
}

export interface CREATE_SOUS_THEMEVariables {
  idTheme: string;
  sousThemeInput: DQSousThemeInput;
}
