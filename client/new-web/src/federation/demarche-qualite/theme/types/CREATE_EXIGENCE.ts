/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQExigenceInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_EXIGENCE
// ====================================================

export interface CREATE_EXIGENCE_createDQExigence {
  __typename: "DQExigence";
  id: string;
  ordre: number | null;
  finalite: string | null;
  titre: string | null;
}

export interface CREATE_EXIGENCE {
  createDQExigence: CREATE_EXIGENCE_createDQExigence | null;
}

export interface CREATE_EXIGENCEVariables {
  idSousTheme: string;
  exigenceInput?: DQExigenceInput | null;
}
