/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_FULL_THEMES
// ====================================================

export interface GET_FULL_THEMES_dqThemes {
  __typename: "DQTheme";
  id: string;
  ordre: number | null;
  nom: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  nombreSousThemes: number;
}

export interface GET_FULL_THEMES {
  dqThemes: (GET_FULL_THEMES_dqThemes | null)[] | null;
}
