/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_THEMES
// ====================================================

export interface GET_THEMES_dqThemes_sousThemes_exigences {
  __typename: "DQExigence";
  id: string;
  ordre: number | null;
  finalite: string | null;
  titre: string | null;
  nombreOutils: number;
}

export interface GET_THEMES_dqThemes_sousThemes {
  __typename: "DQSousTheme";
  id: string;
  ordre: number | null;
  nom: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  nombreExigences: number;
  exigences: (GET_THEMES_dqThemes_sousThemes_exigences | null)[] | null;
}

export interface GET_THEMES_dqThemes {
  __typename: "DQTheme";
  id: string;
  ordre: number | null;
  nom: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  nombreSousThemes: number;
  sousThemes: (GET_THEMES_dqThemes_sousThemes | null)[] | null;
}

export interface GET_THEMES {
  dqThemes: (GET_THEMES_dqThemes | null)[] | null;
}
