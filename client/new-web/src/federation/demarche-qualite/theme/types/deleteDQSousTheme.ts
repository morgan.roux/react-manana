/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: deleteDQSousTheme
// ====================================================

export interface deleteDQSousTheme_deleteDQSousTheme_exigences {
  __typename: "DQExigence";
  id: string;
  ordre: number | null;
  finalite: string | null;
  titre: string | null;
  nombreOutils: number;
}

export interface deleteDQSousTheme_deleteDQSousTheme {
  __typename: "DQSousTheme";
  id: string;
  ordre: number | null;
  nom: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  nombreExigences: number;
  exigences: (deleteDQSousTheme_deleteDQSousTheme_exigences | null)[] | null;
}

export interface deleteDQSousTheme {
  deleteDQSousTheme: deleteDQSousTheme_deleteDQSousTheme | null;
}

export interface deleteDQSousThemeVariables {
  id: string;
}
