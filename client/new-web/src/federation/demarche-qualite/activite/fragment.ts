import gql from 'graphql-tag';

export const ACTIVITE_INFO = gql`
  fragment ActiviteInfo on DQActivite {
    id
    libelle
  }
`;

export const ACTIVITE_WITH_NOMBRE_FICHE_AMELIORATIONS_INFO = gql`
  fragment ActiviteInfo on DQActivite {
    id
    libelle
    nombreFicheAmeliorations
  }
`;
