/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, DQActiviteFilter, DQActiviteSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_ACTIVITES
// ====================================================

export interface GET_ACTIVITES_dQActivites_nodes {
  __typename: "DQActivite";
  id: string;
  libelle: string;
  nombreFicheAmeliorations: number;
}

export interface GET_ACTIVITES_dQActivites {
  __typename: "DQActiviteConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_ACTIVITES_dQActivites_nodes[];
}

export interface GET_ACTIVITES {
  dQActivites: GET_ACTIVITES_dQActivites;
}

export interface GET_ACTIVITESVariables {
  paging?: OffsetPaging | null;
  filter?: DQActiviteFilter | null;
  sorting?: DQActiviteSort[] | null;
}
