import gql from 'graphql-tag';
import { ACTION_OPERATIONNELLE } from './fragment';

export const CREATE_ACTION_OPERATIONNELLE = gql`
    mutation CREATE_ACTION_OPERATIONNELLE($input:DQActionOperationnelleInput!){
        createOneDQActionOperationnelle(input:$input){
            ...ActionOperationnelle
        }
    }

    ${ACTION_OPERATIONNELLE}
`;

export const UPDATE_ACTION_OPERATIONNELLE = gql`
    mutation UPDATE_ACTION_OPERATIONNELLE($id:String!, $update:DQActionOperationnelleInput!){
        updateOneDQActionOperationnelle(id:$id, update:$update){
            ...ActionOperationnelle
        }
    }

    ${ACTION_OPERATIONNELLE}
`;

export const DELETE_ACTION_OPERATIONNELLE = gql`
    mutation DELETE_ACTION_OPERATIONNELLE($input:DeleteOneInput!){
        deleteOneDQActionOperationnelle(input:$input){
            id
        }
    }
`;

export const UPDATE_ACTION_OPERATIONNELLE_STATUT = gql`
  mutation UPDATE_ACTION_OPERATIONNELLE_STATUT($id: String!, $code: String!) {
    updateActionOperationnelleStatut(id: $id, code: $code) {
      ...ActionOperationnelle
    }
  }
  ${ACTION_OPERATIONNELLE}
`;