/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQActionOperationnelleInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_ACTION_OPERATIONNELLE
// ====================================================

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_statut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_origineAssocie_User_photo | null;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_origineAssocie = UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_origineAssocie_User | UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_origineAssocie_Laboratoire | UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_origineAssocie_PrestataireService;

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_fichiers {
  __typename: "Fichier";
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_suiviPar {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_auteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_origine {
  __typename: "Origine";
  id: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_participants_photo | null;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_participantsAInformer_photo | null;
}

export interface UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle {
  __typename: "DQActionOperationnelle";
  id: string;
  description: string;
  statut: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_statut;
  idUserAuteur: string;
  idUserSuivi: string;
  idOrigine: string;
  idOrigineAssocie: string | null;
  dateAction: any | null;
  dateEcheance: any | null;
  idType: string | null;
  idCause: string | null;
  origineAssocie: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_origineAssocie | null;
  fichiers: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_fichiers[];
  suiviPar: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_suiviPar;
  auteur: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_auteur;
  origine: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_origine | null;
  type: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_type | null;
  cause: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_cause | null;
  urgence: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_urgence;
  importance: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_importance;
  fonction: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_fonction | null;
  tache: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_tache | null;
  fonctionAInformer: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_fonctionAInformer | null;
  tacheAInformer: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_tacheAInformer | null;
  reunion: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_reunion | null;
  participants: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_participants[];
  participantsAInformer: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle_participantsAInformer[];
}

export interface UPDATE_ACTION_OPERATIONNELLE {
  updateOneDQActionOperationnelle: UPDATE_ACTION_OPERATIONNELLE_updateOneDQActionOperationnelle;
}

export interface UPDATE_ACTION_OPERATIONNELLEVariables {
  id: string;
  update: DQActionOperationnelleInput;
}
