/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, DQActionOperationnelleFilter, DQActionOperationnelleSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_ACTIONS_OPERATIONNELLES
// ====================================================

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_statut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_origineAssocie_User_photo | null;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_origineAssocie = GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_origineAssocie_User | GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_origineAssocie_Laboratoire | GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_origineAssocie_PrestataireService;

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_fichiers {
  __typename: "Fichier";
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_suiviPar {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_auteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_origine {
  __typename: "Origine";
  id: string;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_participants_photo | null;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_participantsAInformer_photo | null;
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes {
  __typename: "DQActionOperationnelle";
  id: string;
  description: string;
  statut: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_statut;
  idUserAuteur: string;
  idUserSuivi: string;
  idOrigine: string;
  idOrigineAssocie: string | null;
  dateAction: any | null;
  dateEcheance: any | null;
  idType: string | null;
  idCause: string | null;
  origineAssocie: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_origineAssocie | null;
  fichiers: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_fichiers[];
  suiviPar: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_suiviPar;
  auteur: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_auteur;
  origine: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_origine | null;
  type: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_type | null;
  cause: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_cause | null;
  urgence: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_urgence;
  importance: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_importance;
  fonction: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_fonction | null;
  tache: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_tache | null;
  fonctionAInformer: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_fonctionAInformer | null;
  tacheAInformer: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_tacheAInformer | null;
  reunion: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_reunion | null;
  participants: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_participants[];
  participantsAInformer: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes_participantsAInformer[];
}

export interface GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles {
  __typename: "DQActionOperationnelleConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles_nodes[];
}

export interface GET_ACTIONS_OPERATIONNELLES {
  dQActionOperationnelles: GET_ACTIONS_OPERATIONNELLES_dQActionOperationnelles;
}

export interface GET_ACTIONS_OPERATIONNELLESVariables {
  paging?: OffsetPaging | null;
  filter?: DQActionOperationnelleFilter | null;
  sorting?: DQActionOperationnelleSort[] | null;
}
