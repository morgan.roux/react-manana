/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_ACTION_OPERATIONNELLE
// ====================================================

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_statut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_origineAssocie_User_photo | null;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_origineAssocie = GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_origineAssocie_User | GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_origineAssocie_Laboratoire | GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_origineAssocie_PrestataireService;

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_fichiers {
  __typename: "Fichier";
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_suiviPar {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_auteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_origine {
  __typename: "Origine";
  id: string;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_participants_photo | null;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_participantsAInformer_photo | null;
}

export interface GET_ACTION_OPERATIONNELLE_dQActionOperationnelle {
  __typename: "DQActionOperationnelle";
  id: string;
  description: string;
  statut: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_statut;
  idUserAuteur: string;
  idUserSuivi: string;
  idOrigine: string;
  idOrigineAssocie: string | null;
  dateAction: any | null;
  dateEcheance: any | null;
  idType: string | null;
  idCause: string | null;
  origineAssocie: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_origineAssocie | null;
  fichiers: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_fichiers[];
  suiviPar: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_suiviPar;
  auteur: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_auteur;
  origine: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_origine | null;
  type: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_type | null;
  cause: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_cause | null;
  urgence: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_urgence;
  importance: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_importance;
  fonction: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_fonction | null;
  tache: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_tache | null;
  fonctionAInformer: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_fonctionAInformer | null;
  tacheAInformer: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_tacheAInformer | null;
  reunion: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_reunion | null;
  participants: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_participants[];
  participantsAInformer: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle_participantsAInformer[];
}

export interface GET_ACTION_OPERATIONNELLE {
  dQActionOperationnelle: GET_ACTION_OPERATIONNELLE_dQActionOperationnelle | null;
}

export interface GET_ACTION_OPERATIONNELLEVariables {
  id: string;
}
