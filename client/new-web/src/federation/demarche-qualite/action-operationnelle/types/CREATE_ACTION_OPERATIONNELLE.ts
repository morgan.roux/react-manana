/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQActionOperationnelleInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_ACTION_OPERATIONNELLE
// ====================================================

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_statut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_origineAssocie_User_photo | null;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_origineAssocie = CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_origineAssocie_User | CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_origineAssocie_Laboratoire | CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_origineAssocie_PrestataireService;

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_fichiers {
  __typename: "Fichier";
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_suiviPar {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_auteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_origine {
  __typename: "Origine";
  id: string;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_participants_photo | null;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_participantsAInformer_photo | null;
}

export interface CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle {
  __typename: "DQActionOperationnelle";
  id: string;
  description: string;
  statut: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_statut;
  idUserAuteur: string;
  idUserSuivi: string;
  idOrigine: string;
  idOrigineAssocie: string | null;
  dateAction: any | null;
  dateEcheance: any | null;
  idType: string | null;
  idCause: string | null;
  origineAssocie: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_origineAssocie | null;
  fichiers: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_fichiers[];
  suiviPar: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_suiviPar;
  auteur: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_auteur;
  origine: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_origine | null;
  type: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_type | null;
  cause: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_cause | null;
  urgence: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_urgence;
  importance: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_importance;
  fonction: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_fonction | null;
  tache: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_tache | null;
  fonctionAInformer: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_fonctionAInformer | null;
  tacheAInformer: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_tacheAInformer | null;
  reunion: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_reunion | null;
  participants: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_participants[];
  participantsAInformer: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle_participantsAInformer[];
}

export interface CREATE_ACTION_OPERATIONNELLE {
  createOneDQActionOperationnelle: CREATE_ACTION_OPERATIONNELLE_createOneDQActionOperationnelle;
}

export interface CREATE_ACTION_OPERATIONNELLEVariables {
  input: DQActionOperationnelleInput;
}
