/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ActionOperationnelle
// ====================================================

export interface ActionOperationnelle_statut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface ActionOperationnelle_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface ActionOperationnelle_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: ActionOperationnelle_origineAssocie_User_photo | null;
}

export interface ActionOperationnelle_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface ActionOperationnelle_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type ActionOperationnelle_origineAssocie = ActionOperationnelle_origineAssocie_User | ActionOperationnelle_origineAssocie_Laboratoire | ActionOperationnelle_origineAssocie_PrestataireService;

export interface ActionOperationnelle_fichiers {
  __typename: "Fichier";
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface ActionOperationnelle_suiviPar {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface ActionOperationnelle_auteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface ActionOperationnelle_origine {
  __typename: "Origine";
  id: string;
}

export interface ActionOperationnelle_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface ActionOperationnelle_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface ActionOperationnelle_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface ActionOperationnelle_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface ActionOperationnelle_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface ActionOperationnelle_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface ActionOperationnelle_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface ActionOperationnelle_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface ActionOperationnelle_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface ActionOperationnelle_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface ActionOperationnelle_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: ActionOperationnelle_participants_photo | null;
}

export interface ActionOperationnelle_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface ActionOperationnelle_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: ActionOperationnelle_participantsAInformer_photo | null;
}

export interface ActionOperationnelle {
  __typename: "DQActionOperationnelle";
  id: string;
  description: string;
  statut: ActionOperationnelle_statut;
  idUserAuteur: string;
  idUserSuivi: string;
  idOrigine: string;
  idOrigineAssocie: string | null;
  dateAction: any | null;
  dateEcheance: any | null;
  idType: string | null;
  idCause: string | null;
  origineAssocie: ActionOperationnelle_origineAssocie | null;
  fichiers: ActionOperationnelle_fichiers[];
  suiviPar: ActionOperationnelle_suiviPar;
  auteur: ActionOperationnelle_auteur;
  origine: ActionOperationnelle_origine | null;
  type: ActionOperationnelle_type | null;
  cause: ActionOperationnelle_cause | null;
  urgence: ActionOperationnelle_urgence;
  importance: ActionOperationnelle_importance;
  fonction: ActionOperationnelle_fonction | null;
  tache: ActionOperationnelle_tache | null;
  fonctionAInformer: ActionOperationnelle_fonctionAInformer | null;
  tacheAInformer: ActionOperationnelle_tacheAInformer | null;
  reunion: ActionOperationnelle_reunion | null;
  participants: ActionOperationnelle_participants[];
  participantsAInformer: ActionOperationnelle_participantsAInformer[];
}
