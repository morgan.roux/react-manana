/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_ENCOURS_ACTION_OPERATIONNELLES
// ====================================================

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_statut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_origineAssocie_User_photo | null;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_origineAssocie = GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_origineAssocie_User | GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_origineAssocie_Laboratoire | GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_origineAssocie_PrestataireService;

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_fichiers {
  __typename: "Fichier";
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_suiviPar {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_auteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_origine {
  __typename: "Origine";
  id: string;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_participants_photo | null;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_participantsAInformer_photo | null;
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles {
  __typename: "DQActionOperationnelle";
  id: string;
  description: string;
  statut: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_statut;
  idUserAuteur: string;
  idUserSuivi: string;
  idOrigine: string;
  idOrigineAssocie: string | null;
  dateAction: any | null;
  dateEcheance: any | null;
  idType: string | null;
  idCause: string | null;
  origineAssocie: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_origineAssocie | null;
  fichiers: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_fichiers[];
  suiviPar: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_suiviPar;
  auteur: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_auteur;
  origine: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_origine | null;
  type: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_type | null;
  cause: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_cause | null;
  urgence: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_urgence;
  importance: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_importance;
  fonction: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_fonction | null;
  tache: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_tache | null;
  fonctionAInformer: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_fonctionAInformer | null;
  tacheAInformer: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_tacheAInformer | null;
  reunion: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_reunion | null;
  participants: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_participants[];
  participantsAInformer: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles_participantsAInformer[];
}

export interface GET_ENCOURS_ACTION_OPERATIONNELLES {
  dQEncoursActionOperationnelles: GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles[];
}
