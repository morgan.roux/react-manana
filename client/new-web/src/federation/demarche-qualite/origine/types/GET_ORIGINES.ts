/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, OrigineFilter, OrigineSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_ORIGINES
// ====================================================

export interface GET_ORIGINES_origines_nodes {
  __typename: "Origine";
  id: string;
  libelle: string;
  code: string;
  nombreFicheIncidents: number;
}

export interface GET_ORIGINES_origines {
  __typename: "OrigineConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_ORIGINES_origines_nodes[];
}

export interface GET_ORIGINES {
  origines: GET_ORIGINES_origines;
}

export interface GET_ORIGINESVariables {
  paging?: OffsetPaging | null;
  filter?: OrigineFilter | null;
  sorting?: OrigineSort[] | null;
}
