/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQChecklistInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_CHECKLIST
// ====================================================

export interface UPDATE_CHECKLIST_updateDQChecklist_sections_items {
  __typename: "DQChecklistSectionItem";
  id: string;
  ordre: number;
  libelle: string;
  nombreEvaluations: number;
}

export interface UPDATE_CHECKLIST_updateDQChecklist_sections {
  __typename: "DQChecklistSection";
  id: string;
  ordre: number;
  libelle: string;
  items: (UPDATE_CHECKLIST_updateDQChecklist_sections_items | null)[] | null;
}

export interface UPDATE_CHECKLIST_updateDQChecklist {
  __typename: "DQChecklist";
  id: string;
  ordre: number;
  libelle: string;
  nombreEvaluations: number;
  createdAt: any | null;
  updatedAt: any | null;
  sections: (UPDATE_CHECKLIST_updateDQChecklist_sections | null)[] | null;
}

export interface UPDATE_CHECKLIST {
  updateDQChecklist: UPDATE_CHECKLIST_updateDQChecklist | null;
}

export interface UPDATE_CHECKLISTVariables {
  id: string;
  checklistInput: DQChecklistInput;
}
