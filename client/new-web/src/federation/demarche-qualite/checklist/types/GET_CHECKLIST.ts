/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_CHECKLIST
// ====================================================

export interface GET_CHECKLIST_dqChecklist_sections_items {
  __typename: "DQChecklistSectionItem";
  id: string;
  ordre: number;
  libelle: string;
  nombreEvaluations: number;
}

export interface GET_CHECKLIST_dqChecklist_sections {
  __typename: "DQChecklistSection";
  id: string;
  ordre: number;
  libelle: string;
  items: (GET_CHECKLIST_dqChecklist_sections_items | null)[] | null;
}

export interface GET_CHECKLIST_dqChecklist {
  __typename: "DQChecklist";
  id: string;
  ordre: number;
  libelle: string;
  nombreEvaluations: number;
  createdAt: any | null;
  updatedAt: any | null;
  sections: (GET_CHECKLIST_dqChecklist_sections | null)[] | null;
}

export interface GET_CHECKLIST {
  dqChecklist: GET_CHECKLIST_dqChecklist | null;
}

export interface GET_CHECKLISTVariables {
  id: string;
}
