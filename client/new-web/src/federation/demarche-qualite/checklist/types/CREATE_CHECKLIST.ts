/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQChecklistInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_CHECKLIST
// ====================================================

export interface CREATE_CHECKLIST_createDQChecklist_sections_items {
  __typename: "DQChecklistSectionItem";
  id: string;
  ordre: number;
  libelle: string;
  nombreEvaluations: number;
}

export interface CREATE_CHECKLIST_createDQChecklist_sections {
  __typename: "DQChecklistSection";
  id: string;
  ordre: number;
  libelle: string;
  items: (CREATE_CHECKLIST_createDQChecklist_sections_items | null)[] | null;
}

export interface CREATE_CHECKLIST_createDQChecklist {
  __typename: "DQChecklist";
  id: string;
  ordre: number;
  libelle: string;
  nombreEvaluations: number;
  createdAt: any | null;
  updatedAt: any | null;
  sections: (CREATE_CHECKLIST_createDQChecklist_sections | null)[] | null;
}

export interface CREATE_CHECKLIST {
  createDQChecklist: CREATE_CHECKLIST_createDQChecklist | null;
}

export interface CREATE_CHECKLISTVariables {
  checklistInput: DQChecklistInput;
}
