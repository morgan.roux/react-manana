import gql from 'graphql-tag';

export const MINIMAL_CHECKLIST_INFO = gql`
  fragment ChecklistInfo on DQChecklist {
    id
    code
    ordre
    libelle
    nombreEvaluations
    createdAt
    updatedAt
  }
`;

export const FULL_CHECKLIST_INFO = gql`
  fragment ChecklistInfo on DQChecklist {
    id
    ordre
    libelle
    nombreEvaluations
    createdAt
    updatedAt
    sections {
      id
      ordre
      libelle
      items {
        id
        ordre
        libelle
        nombreEvaluations
      }
    }
  }
`;
