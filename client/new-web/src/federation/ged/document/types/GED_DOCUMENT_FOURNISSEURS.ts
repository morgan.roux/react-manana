/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GedDocumentFournisseurInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GED_DOCUMENT_FOURNISSEURS
// ====================================================

export interface GED_DOCUMENT_FOURNISSEURS_gedDocumentFournisseurs_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  nom: string;
  dataType: string;
}

export interface GED_DOCUMENT_FOURNISSEURS_gedDocumentFournisseurs_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
  dataType: string;
}

export type GED_DOCUMENT_FOURNISSEURS_gedDocumentFournisseurs = GED_DOCUMENT_FOURNISSEURS_gedDocumentFournisseurs_Laboratoire | GED_DOCUMENT_FOURNISSEURS_gedDocumentFournisseurs_PrestataireService;

export interface GED_DOCUMENT_FOURNISSEURS {
  gedDocumentFournisseurs: GED_DOCUMENT_FOURNISSEURS_gedDocumentFournisseurs[] | null;
}

export interface GED_DOCUMENT_FOURNISSEURSVariables {
  input: GedDocumentFournisseurInput;
}
