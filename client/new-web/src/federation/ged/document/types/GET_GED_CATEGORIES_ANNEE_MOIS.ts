/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GedCategorieAnneeMoisInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_GED_CATEGORIES_ANNEE_MOIS
// ====================================================

export interface GET_GED_CATEGORIES_ANNEE_MOIS_gedCategoriesAnneeMois {
  __typename: "GedPaireAnneeMois";
  annee: number;
  mois: number;
}

export interface GET_GED_CATEGORIES_ANNEE_MOIS {
  gedCategoriesAnneeMois: GET_GED_CATEGORIES_ANNEE_MOIS_gedCategoriesAnneeMois[];
}

export interface GET_GED_CATEGORIES_ANNEE_MOISVariables {
  input: GedCategorieAnneeMoisInput;
}
