/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: DocumentCategorie
// ====================================================

export interface DocumentCategorie_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface DocumentCategorie_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export interface DocumentCategorie_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
}

export type DocumentCategorie_origineAssocie = DocumentCategorie_origineAssocie_Laboratoire | DocumentCategorie_origineAssocie_PrestataireService | DocumentCategorie_origineAssocie_User;

export interface DocumentCategorie_typeAvoirAssociations {
  __typename: "GedDocumentAvoirAssociation";
  idDocument: string;
  type: string;
  correspondant: string;
}

export interface DocumentCategorie_commande {
  __typename: "PRTCommande";
  id: string;
  nbrRef: number | null;
}

export interface DocumentCategorie_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface DocumentCategorie_sousCategorie {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface DocumentCategorie_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface DocumentCategorie_verificateur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface DocumentCategorie_redacteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface DocumentCategorie_categorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface DocumentCategorie_dernierChangementStatut_createdBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface DocumentCategorie_dernierChangementStatut_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: DocumentCategorie_dernierChangementStatut_createdBy_prestataireService | null;
}

export interface DocumentCategorie_dernierChangementStatut_updatedBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface DocumentCategorie_dernierChangementStatut_updatedBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: DocumentCategorie_dernierChangementStatut_updatedBy_prestataireService | null;
}

export interface DocumentCategorie_dernierChangementStatut {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: DocumentCategorie_dernierChangementStatut_createdBy | null;
  updatedBy: DocumentCategorie_dernierChangementStatut_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface DocumentCategorie_changementStatuts_createdBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface DocumentCategorie_changementStatuts_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: DocumentCategorie_changementStatuts_createdBy_prestataireService | null;
}

export interface DocumentCategorie_changementStatuts_updatedBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface DocumentCategorie_changementStatuts_updatedBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: DocumentCategorie_changementStatuts_updatedBy_prestataireService | null;
}

export interface DocumentCategorie_changementStatuts {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: DocumentCategorie_changementStatuts_createdBy | null;
  updatedBy: DocumentCategorie_changementStatuts_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface DocumentCategorie_documentARemplacer_typeAvoirAssociations {
  __typename: "GedDocumentAvoirAssociation";
  idDocument: string;
  type: string;
  correspondant: string;
}

export interface DocumentCategorie_documentARemplacer_commande {
  __typename: "PRTCommande";
  id: string;
  nbrRef: number | null;
}

export interface DocumentCategorie_documentARemplacer_createdBy {
  __typename: "User";
  id: string;
}

export interface DocumentCategorie_documentARemplacer_sousCategorie {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface DocumentCategorie_documentARemplacer_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface DocumentCategorie_documentARemplacer_verificateur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface DocumentCategorie_documentARemplacer_redacteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface DocumentCategorie_documentARemplacer_categorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
}

export interface DocumentCategorie_documentARemplacer_dernierChangementStatut_createdBy {
  __typename: "User";
  id: string;
}

export interface DocumentCategorie_documentARemplacer_dernierChangementStatut_updatedBy {
  __typename: "User";
  id: string;
}

export interface DocumentCategorie_documentARemplacer_dernierChangementStatut {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: DocumentCategorie_documentARemplacer_dernierChangementStatut_createdBy | null;
  updatedBy: DocumentCategorie_documentARemplacer_dernierChangementStatut_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface DocumentCategorie_documentARemplacer {
  __typename: "GedDocument";
  id: string;
  description: string;
  nomenclature: string | null;
  numeroVersion: string | null;
  motCle1: string | null;
  motCle2: string | null;
  motCle3: string | null;
  dateHeureParution: any | null;
  dateHeureDebutValidite: any | null;
  dateHeureFinValidite: any | null;
  idDocumentARemplacer: string | null;
  idUserRedacteur: string | null;
  idUserVerificateur: string | null;
  idFichier: string;
  idSousCategorie: string | null;
  idPharmacie: string;
  idGroupement: string;
  createdAt: any;
  updatedAt: any;
  nombreConsultations: number;
  nombreTelechargements: number;
  nombreCommentaires: number;
  nombreReactions: number;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  factureTotalHt: number | null;
  factureTotalTtc: number | null;
  factureTva: number | null;
  factureDate: any | null;
  factureDateReglement: any | null;
  idReglementMode: string | null;
  numeroFacture: string | null;
  isGenererCommande: boolean | null;
  numeroCommande: number | null;
  typeAvoirAssociations: DocumentCategorie_documentARemplacer_typeAvoirAssociations[] | null;
  commande: DocumentCategorie_documentARemplacer_commande | null;
  idCommandes: string[] | null;
  type: string | null;
  createdBy: DocumentCategorie_documentARemplacer_createdBy;
  sousCategorie: DocumentCategorie_documentARemplacer_sousCategorie | null;
  fichier: DocumentCategorie_documentARemplacer_fichier;
  verificateur: DocumentCategorie_documentARemplacer_verificateur | null;
  redacteur: DocumentCategorie_documentARemplacer_redacteur | null;
  categorie: DocumentCategorie_documentARemplacer_categorie | null;
  dernierChangementStatut: DocumentCategorie_documentARemplacer_dernierChangementStatut | null;
}

export interface DocumentCategorie {
  __typename: "GedDocument";
  id: string;
  type: string | null;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  origineAssocie: DocumentCategorie_origineAssocie | null;
  factureTotalHt: number | null;
  factureTotalTtc: number | null;
  factureTva: number | null;
  factureDate: any | null;
  factureDateReglement: any | null;
  idReglementMode: string | null;
  numeroFacture: string | null;
  isGenererCommande: boolean | null;
  numeroCommande: number | null;
  typeAvoirAssociations: DocumentCategorie_typeAvoirAssociations[] | null;
  commande: DocumentCategorie_commande | null;
  idCommandes: string[] | null;
  description: string;
  nomenclature: string | null;
  numeroVersion: string | null;
  motCle1: string | null;
  motCle2: string | null;
  motCle3: string | null;
  dateHeureParution: any | null;
  dateHeureDebutValidite: any | null;
  dateHeureFinValidite: any | null;
  idDocumentARemplacer: string | null;
  idUserRedacteur: string | null;
  idUserVerificateur: string | null;
  idFichier: string;
  idSousCategorie: string | null;
  idPharmacie: string;
  idGroupement: string;
  favoris: boolean;
  createdAt: any;
  updatedAt: any;
  isOcr: boolean | null;
  createdBy: DocumentCategorie_createdBy;
  nombreConsultations: number;
  nombreTelechargements: number;
  nombreCommentaires: number;
  nombreReactions: number;
  sousCategorie: DocumentCategorie_sousCategorie | null;
  fichier: DocumentCategorie_fichier;
  verificateur: DocumentCategorie_verificateur | null;
  redacteur: DocumentCategorie_redacteur | null;
  categorie: DocumentCategorie_categorie | null;
  dernierChangementStatut: DocumentCategorie_dernierChangementStatut | null;
  changementStatuts: DocumentCategorie_changementStatuts[] | null;
  documentARemplacer: DocumentCategorie_documentARemplacer | null;
}
