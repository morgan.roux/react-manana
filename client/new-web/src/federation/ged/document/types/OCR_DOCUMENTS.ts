/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: OCR_DOCUMENTS
// ====================================================

export interface OCR_DOCUMENTS_oCRDocuments_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface OCR_DOCUMENTS_oCRDocuments_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export interface OCR_DOCUMENTS_oCRDocuments_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
}

export type OCR_DOCUMENTS_oCRDocuments_origineAssocie = OCR_DOCUMENTS_oCRDocuments_origineAssocie_Laboratoire | OCR_DOCUMENTS_oCRDocuments_origineAssocie_PrestataireService | OCR_DOCUMENTS_oCRDocuments_origineAssocie_User;

export interface OCR_DOCUMENTS_oCRDocuments_typeAvoirAssociations {
  __typename: "GedDocumentAvoirAssociation";
  idDocument: string;
  type: string;
  correspondant: string;
}

export interface OCR_DOCUMENTS_oCRDocuments_commande {
  __typename: "PRTCommande";
  id: string;
  nbrRef: number | null;
}

export interface OCR_DOCUMENTS_oCRDocuments_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface OCR_DOCUMENTS_oCRDocuments_sousCategorie {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface OCR_DOCUMENTS_oCRDocuments_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface OCR_DOCUMENTS_oCRDocuments_verificateur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface OCR_DOCUMENTS_oCRDocuments_redacteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface OCR_DOCUMENTS_oCRDocuments_categorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface OCR_DOCUMENTS_oCRDocuments_dernierChangementStatut_createdBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface OCR_DOCUMENTS_oCRDocuments_dernierChangementStatut_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: OCR_DOCUMENTS_oCRDocuments_dernierChangementStatut_createdBy_prestataireService | null;
}

export interface OCR_DOCUMENTS_oCRDocuments_dernierChangementStatut_updatedBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface OCR_DOCUMENTS_oCRDocuments_dernierChangementStatut_updatedBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: OCR_DOCUMENTS_oCRDocuments_dernierChangementStatut_updatedBy_prestataireService | null;
}

export interface OCR_DOCUMENTS_oCRDocuments_dernierChangementStatut {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: OCR_DOCUMENTS_oCRDocuments_dernierChangementStatut_createdBy | null;
  updatedBy: OCR_DOCUMENTS_oCRDocuments_dernierChangementStatut_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface OCR_DOCUMENTS_oCRDocuments_changementStatuts_createdBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface OCR_DOCUMENTS_oCRDocuments_changementStatuts_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: OCR_DOCUMENTS_oCRDocuments_changementStatuts_createdBy_prestataireService | null;
}

export interface OCR_DOCUMENTS_oCRDocuments_changementStatuts_updatedBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface OCR_DOCUMENTS_oCRDocuments_changementStatuts_updatedBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: OCR_DOCUMENTS_oCRDocuments_changementStatuts_updatedBy_prestataireService | null;
}

export interface OCR_DOCUMENTS_oCRDocuments_changementStatuts {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: OCR_DOCUMENTS_oCRDocuments_changementStatuts_createdBy | null;
  updatedBy: OCR_DOCUMENTS_oCRDocuments_changementStatuts_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface OCR_DOCUMENTS_oCRDocuments_documentARemplacer_typeAvoirAssociations {
  __typename: "GedDocumentAvoirAssociation";
  idDocument: string;
  type: string;
  correspondant: string;
}

export interface OCR_DOCUMENTS_oCRDocuments_documentARemplacer_commande {
  __typename: "PRTCommande";
  id: string;
  nbrRef: number | null;
}

export interface OCR_DOCUMENTS_oCRDocuments_documentARemplacer_createdBy {
  __typename: "User";
  id: string;
}

export interface OCR_DOCUMENTS_oCRDocuments_documentARemplacer_sousCategorie {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface OCR_DOCUMENTS_oCRDocuments_documentARemplacer_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface OCR_DOCUMENTS_oCRDocuments_documentARemplacer_verificateur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface OCR_DOCUMENTS_oCRDocuments_documentARemplacer_redacteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface OCR_DOCUMENTS_oCRDocuments_documentARemplacer_categorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
}

export interface OCR_DOCUMENTS_oCRDocuments_documentARemplacer_dernierChangementStatut_createdBy {
  __typename: "User";
  id: string;
}

export interface OCR_DOCUMENTS_oCRDocuments_documentARemplacer_dernierChangementStatut_updatedBy {
  __typename: "User";
  id: string;
}

export interface OCR_DOCUMENTS_oCRDocuments_documentARemplacer_dernierChangementStatut {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: OCR_DOCUMENTS_oCRDocuments_documentARemplacer_dernierChangementStatut_createdBy | null;
  updatedBy: OCR_DOCUMENTS_oCRDocuments_documentARemplacer_dernierChangementStatut_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface OCR_DOCUMENTS_oCRDocuments_documentARemplacer {
  __typename: "GedDocument";
  id: string;
  description: string;
  nomenclature: string | null;
  numeroVersion: string | null;
  motCle1: string | null;
  motCle2: string | null;
  motCle3: string | null;
  dateHeureParution: any | null;
  dateHeureDebutValidite: any | null;
  dateHeureFinValidite: any | null;
  idDocumentARemplacer: string | null;
  idUserRedacteur: string | null;
  idUserVerificateur: string | null;
  idFichier: string;
  idSousCategorie: string | null;
  idPharmacie: string;
  idGroupement: string;
  createdAt: any;
  updatedAt: any;
  nombreConsultations: number;
  nombreTelechargements: number;
  nombreCommentaires: number;
  nombreReactions: number;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  factureTotalHt: number | null;
  factureTotalTtc: number | null;
  factureTva: number | null;
  factureDate: any | null;
  factureDateReglement: any | null;
  idReglementMode: string | null;
  numeroFacture: string | null;
  isGenererCommande: boolean | null;
  numeroCommande: number | null;
  typeAvoirAssociations: OCR_DOCUMENTS_oCRDocuments_documentARemplacer_typeAvoirAssociations[] | null;
  commande: OCR_DOCUMENTS_oCRDocuments_documentARemplacer_commande | null;
  idCommandes: string[] | null;
  type: string | null;
  createdBy: OCR_DOCUMENTS_oCRDocuments_documentARemplacer_createdBy;
  sousCategorie: OCR_DOCUMENTS_oCRDocuments_documentARemplacer_sousCategorie | null;
  fichier: OCR_DOCUMENTS_oCRDocuments_documentARemplacer_fichier;
  verificateur: OCR_DOCUMENTS_oCRDocuments_documentARemplacer_verificateur | null;
  redacteur: OCR_DOCUMENTS_oCRDocuments_documentARemplacer_redacteur | null;
  categorie: OCR_DOCUMENTS_oCRDocuments_documentARemplacer_categorie | null;
  dernierChangementStatut: OCR_DOCUMENTS_oCRDocuments_documentARemplacer_dernierChangementStatut | null;
}

export interface OCR_DOCUMENTS_oCRDocuments {
  __typename: "GedDocument";
  id: string;
  type: string | null;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  origineAssocie: OCR_DOCUMENTS_oCRDocuments_origineAssocie | null;
  factureTotalHt: number | null;
  factureTotalTtc: number | null;
  factureTva: number | null;
  factureDate: any | null;
  factureDateReglement: any | null;
  idReglementMode: string | null;
  numeroFacture: string | null;
  isGenererCommande: boolean | null;
  numeroCommande: number | null;
  typeAvoirAssociations: OCR_DOCUMENTS_oCRDocuments_typeAvoirAssociations[] | null;
  commande: OCR_DOCUMENTS_oCRDocuments_commande | null;
  idCommandes: string[] | null;
  description: string;
  nomenclature: string | null;
  numeroVersion: string | null;
  motCle1: string | null;
  motCle2: string | null;
  motCle3: string | null;
  dateHeureParution: any | null;
  dateHeureDebutValidite: any | null;
  dateHeureFinValidite: any | null;
  idDocumentARemplacer: string | null;
  idUserRedacteur: string | null;
  idUserVerificateur: string | null;
  idFichier: string;
  idSousCategorie: string | null;
  idPharmacie: string;
  idGroupement: string;
  favoris: boolean;
  createdAt: any;
  updatedAt: any;
  isOcr: boolean | null;
  createdBy: OCR_DOCUMENTS_oCRDocuments_createdBy;
  nombreConsultations: number;
  nombreTelechargements: number;
  nombreCommentaires: number;
  nombreReactions: number;
  sousCategorie: OCR_DOCUMENTS_oCRDocuments_sousCategorie | null;
  fichier: OCR_DOCUMENTS_oCRDocuments_fichier;
  verificateur: OCR_DOCUMENTS_oCRDocuments_verificateur | null;
  redacteur: OCR_DOCUMENTS_oCRDocuments_redacteur | null;
  categorie: OCR_DOCUMENTS_oCRDocuments_categorie | null;
  dernierChangementStatut: OCR_DOCUMENTS_oCRDocuments_dernierChangementStatut | null;
  changementStatuts: OCR_DOCUMENTS_oCRDocuments_changementStatuts[] | null;
  documentARemplacer: OCR_DOCUMENTS_oCRDocuments_documentARemplacer | null;
}

export interface OCR_DOCUMENTS {
  oCRDocuments: OCR_DOCUMENTS_oCRDocuments[] | null;
}

export interface OCR_DOCUMENTSVariables {
  idDocument?: string | null;
}
