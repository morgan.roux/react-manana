/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GedDocumentInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_DOCUMENT_CATEGORIE
// ====================================================

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
}

export type CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_origineAssocie = CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_origineAssocie_Laboratoire | CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_origineAssocie_PrestataireService | CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_origineAssocie_User;

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_typeAvoirAssociations {
  __typename: "GedDocumentAvoirAssociation";
  idDocument: string;
  type: string;
  correspondant: string;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_commande {
  __typename: "PRTCommande";
  id: string;
  nbrRef: number | null;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_sousCategorie {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_verificateur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_redacteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_categorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_dernierChangementStatut_createdBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_dernierChangementStatut_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_dernierChangementStatut_createdBy_prestataireService | null;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_dernierChangementStatut_updatedBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_dernierChangementStatut_updatedBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_dernierChangementStatut_updatedBy_prestataireService | null;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_dernierChangementStatut {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_dernierChangementStatut_createdBy | null;
  updatedBy: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_dernierChangementStatut_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_changementStatuts_createdBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_changementStatuts_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_changementStatuts_createdBy_prestataireService | null;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_changementStatuts_updatedBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_changementStatuts_updatedBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_changementStatuts_updatedBy_prestataireService | null;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_changementStatuts {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_changementStatuts_createdBy | null;
  updatedBy: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_changementStatuts_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_typeAvoirAssociations {
  __typename: "GedDocumentAvoirAssociation";
  idDocument: string;
  type: string;
  correspondant: string;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_commande {
  __typename: "PRTCommande";
  id: string;
  nbrRef: number | null;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_createdBy {
  __typename: "User";
  id: string;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_sousCategorie {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_verificateur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_redacteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_categorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_dernierChangementStatut_createdBy {
  __typename: "User";
  id: string;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_dernierChangementStatut_updatedBy {
  __typename: "User";
  id: string;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_dernierChangementStatut {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_dernierChangementStatut_createdBy | null;
  updatedBy: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_dernierChangementStatut_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer {
  __typename: "GedDocument";
  id: string;
  description: string;
  nomenclature: string | null;
  numeroVersion: string | null;
  motCle1: string | null;
  motCle2: string | null;
  motCle3: string | null;
  dateHeureParution: any | null;
  dateHeureDebutValidite: any | null;
  dateHeureFinValidite: any | null;
  idDocumentARemplacer: string | null;
  idUserRedacteur: string | null;
  idUserVerificateur: string | null;
  idFichier: string;
  idSousCategorie: string | null;
  idPharmacie: string;
  idGroupement: string;
  createdAt: any;
  updatedAt: any;
  nombreConsultations: number;
  nombreTelechargements: number;
  nombreCommentaires: number;
  nombreReactions: number;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  factureTotalHt: number | null;
  factureTotalTtc: number | null;
  factureTva: number | null;
  factureDate: any | null;
  factureDateReglement: any | null;
  idReglementMode: string | null;
  numeroFacture: string | null;
  isGenererCommande: boolean | null;
  numeroCommande: number | null;
  typeAvoirAssociations: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_typeAvoirAssociations[] | null;
  commande: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_commande | null;
  idCommandes: string[] | null;
  type: string | null;
  createdBy: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_createdBy;
  sousCategorie: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_sousCategorie | null;
  fichier: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_fichier;
  verificateur: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_verificateur | null;
  redacteur: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_redacteur | null;
  categorie: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_categorie | null;
  dernierChangementStatut: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer_dernierChangementStatut | null;
}

export interface CREATE_DOCUMENT_CATEGORIE_createOneGedDocument {
  __typename: "GedDocument";
  id: string;
  type: string | null;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  origineAssocie: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_origineAssocie | null;
  factureTotalHt: number | null;
  factureTotalTtc: number | null;
  factureTva: number | null;
  factureDate: any | null;
  factureDateReglement: any | null;
  idReglementMode: string | null;
  numeroFacture: string | null;
  isGenererCommande: boolean | null;
  numeroCommande: number | null;
  typeAvoirAssociations: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_typeAvoirAssociations[] | null;
  commande: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_commande | null;
  idCommandes: string[] | null;
  description: string;
  nomenclature: string | null;
  numeroVersion: string | null;
  motCle1: string | null;
  motCle2: string | null;
  motCle3: string | null;
  dateHeureParution: any | null;
  dateHeureDebutValidite: any | null;
  dateHeureFinValidite: any | null;
  idDocumentARemplacer: string | null;
  idUserRedacteur: string | null;
  idUserVerificateur: string | null;
  idFichier: string;
  idSousCategorie: string | null;
  idPharmacie: string;
  idGroupement: string;
  favoris: boolean;
  createdAt: any;
  updatedAt: any;
  isOcr: boolean | null;
  createdBy: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_createdBy;
  nombreConsultations: number;
  nombreTelechargements: number;
  nombreCommentaires: number;
  nombreReactions: number;
  sousCategorie: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_sousCategorie | null;
  fichier: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_fichier;
  verificateur: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_verificateur | null;
  redacteur: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_redacteur | null;
  categorie: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_categorie | null;
  dernierChangementStatut: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_dernierChangementStatut | null;
  changementStatuts: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_changementStatuts[] | null;
  documentARemplacer: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument_documentARemplacer | null;
}

export interface CREATE_DOCUMENT_CATEGORIE {
  createOneGedDocument: CREATE_DOCUMENT_CATEGORIE_createOneGedDocument;
}

export interface CREATE_DOCUMENT_CATEGORIEVariables {
  input: GedDocumentInput;
}
