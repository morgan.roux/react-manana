/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_DOCUMENT_CATEGORIE
// ====================================================

export interface DELETE_DOCUMENT_CATEGORIE_deleteOneGedDocument {
  __typename: "GedDocumentDeleteResponse";
  id: string | null;
}

export interface DELETE_DOCUMENT_CATEGORIE {
  deleteOneGedDocument: DELETE_DOCUMENT_CATEGORIE_deleteOneGedDocument;
}

export interface DELETE_DOCUMENT_CATEGORIEVariables {
  input: DeleteOneInput;
}
