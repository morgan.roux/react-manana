/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SearchDocumentResult
// ====================================================

export interface SearchDocumentResult_data_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface SearchDocumentResult_data_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export interface SearchDocumentResult_data_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
}

export type SearchDocumentResult_data_origineAssocie = SearchDocumentResult_data_origineAssocie_Laboratoire | SearchDocumentResult_data_origineAssocie_PrestataireService | SearchDocumentResult_data_origineAssocie_User;

export interface SearchDocumentResult_data_typeAvoirAssociations {
  __typename: "GedDocumentAvoirAssociation";
  idDocument: string;
  type: string;
  correspondant: string;
}

export interface SearchDocumentResult_data_commande {
  __typename: "PRTCommande";
  id: string;
  nbrRef: number | null;
}

export interface SearchDocumentResult_data_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface SearchDocumentResult_data_sousCategorie {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface SearchDocumentResult_data_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface SearchDocumentResult_data_verificateur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface SearchDocumentResult_data_redacteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface SearchDocumentResult_data_categorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface SearchDocumentResult_data_dernierChangementStatut_createdBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface SearchDocumentResult_data_dernierChangementStatut_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: SearchDocumentResult_data_dernierChangementStatut_createdBy_prestataireService | null;
}

export interface SearchDocumentResult_data_dernierChangementStatut_updatedBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface SearchDocumentResult_data_dernierChangementStatut_updatedBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: SearchDocumentResult_data_dernierChangementStatut_updatedBy_prestataireService | null;
}

export interface SearchDocumentResult_data_dernierChangementStatut {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: SearchDocumentResult_data_dernierChangementStatut_createdBy | null;
  updatedBy: SearchDocumentResult_data_dernierChangementStatut_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface SearchDocumentResult_data_changementStatuts_createdBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface SearchDocumentResult_data_changementStatuts_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: SearchDocumentResult_data_changementStatuts_createdBy_prestataireService | null;
}

export interface SearchDocumentResult_data_changementStatuts_updatedBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface SearchDocumentResult_data_changementStatuts_updatedBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: SearchDocumentResult_data_changementStatuts_updatedBy_prestataireService | null;
}

export interface SearchDocumentResult_data_changementStatuts {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: SearchDocumentResult_data_changementStatuts_createdBy | null;
  updatedBy: SearchDocumentResult_data_changementStatuts_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface SearchDocumentResult_data_documentARemplacer_typeAvoirAssociations {
  __typename: "GedDocumentAvoirAssociation";
  idDocument: string;
  type: string;
  correspondant: string;
}

export interface SearchDocumentResult_data_documentARemplacer_commande {
  __typename: "PRTCommande";
  id: string;
  nbrRef: number | null;
}

export interface SearchDocumentResult_data_documentARemplacer_createdBy {
  __typename: "User";
  id: string;
}

export interface SearchDocumentResult_data_documentARemplacer_sousCategorie {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface SearchDocumentResult_data_documentARemplacer_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface SearchDocumentResult_data_documentARemplacer_verificateur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface SearchDocumentResult_data_documentARemplacer_redacteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface SearchDocumentResult_data_documentARemplacer_categorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
}

export interface SearchDocumentResult_data_documentARemplacer_dernierChangementStatut_createdBy {
  __typename: "User";
  id: string;
}

export interface SearchDocumentResult_data_documentARemplacer_dernierChangementStatut_updatedBy {
  __typename: "User";
  id: string;
}

export interface SearchDocumentResult_data_documentARemplacer_dernierChangementStatut {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: SearchDocumentResult_data_documentARemplacer_dernierChangementStatut_createdBy | null;
  updatedBy: SearchDocumentResult_data_documentARemplacer_dernierChangementStatut_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface SearchDocumentResult_data_documentARemplacer {
  __typename: "GedDocument";
  id: string;
  description: string;
  nomenclature: string | null;
  numeroVersion: string | null;
  motCle1: string | null;
  motCle2: string | null;
  motCle3: string | null;
  dateHeureParution: any | null;
  dateHeureDebutValidite: any | null;
  dateHeureFinValidite: any | null;
  idDocumentARemplacer: string | null;
  idUserRedacteur: string | null;
  idUserVerificateur: string | null;
  idFichier: string;
  idSousCategorie: string | null;
  idPharmacie: string;
  idGroupement: string;
  createdAt: any;
  updatedAt: any;
  nombreConsultations: number;
  nombreTelechargements: number;
  nombreCommentaires: number;
  nombreReactions: number;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  factureTotalHt: number | null;
  factureTotalTtc: number | null;
  factureTva: number | null;
  factureDate: any | null;
  factureDateReglement: any | null;
  idReglementMode: string | null;
  numeroFacture: string | null;
  isGenererCommande: boolean | null;
  numeroCommande: number | null;
  typeAvoirAssociations: SearchDocumentResult_data_documentARemplacer_typeAvoirAssociations[] | null;
  commande: SearchDocumentResult_data_documentARemplacer_commande | null;
  idCommandes: string[] | null;
  type: string | null;
  createdBy: SearchDocumentResult_data_documentARemplacer_createdBy;
  sousCategorie: SearchDocumentResult_data_documentARemplacer_sousCategorie | null;
  fichier: SearchDocumentResult_data_documentARemplacer_fichier;
  verificateur: SearchDocumentResult_data_documentARemplacer_verificateur | null;
  redacteur: SearchDocumentResult_data_documentARemplacer_redacteur | null;
  categorie: SearchDocumentResult_data_documentARemplacer_categorie | null;
  dernierChangementStatut: SearchDocumentResult_data_documentARemplacer_dernierChangementStatut | null;
}

export interface SearchDocumentResult_data {
  __typename: "GedDocument";
  id: string;
  type: string | null;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  origineAssocie: SearchDocumentResult_data_origineAssocie | null;
  factureTotalHt: number | null;
  factureTotalTtc: number | null;
  factureTva: number | null;
  factureDate: any | null;
  factureDateReglement: any | null;
  idReglementMode: string | null;
  numeroFacture: string | null;
  isGenererCommande: boolean | null;
  numeroCommande: number | null;
  typeAvoirAssociations: SearchDocumentResult_data_typeAvoirAssociations[] | null;
  commande: SearchDocumentResult_data_commande | null;
  idCommandes: string[] | null;
  description: string;
  nomenclature: string | null;
  numeroVersion: string | null;
  motCle1: string | null;
  motCle2: string | null;
  motCle3: string | null;
  dateHeureParution: any | null;
  dateHeureDebutValidite: any | null;
  dateHeureFinValidite: any | null;
  idDocumentARemplacer: string | null;
  idUserRedacteur: string | null;
  idUserVerificateur: string | null;
  idFichier: string;
  idSousCategorie: string | null;
  idPharmacie: string;
  idGroupement: string;
  favoris: boolean;
  createdAt: any;
  updatedAt: any;
  isOcr: boolean | null;
  createdBy: SearchDocumentResult_data_createdBy;
  nombreConsultations: number;
  nombreTelechargements: number;
  nombreCommentaires: number;
  nombreReactions: number;
  sousCategorie: SearchDocumentResult_data_sousCategorie | null;
  fichier: SearchDocumentResult_data_fichier;
  verificateur: SearchDocumentResult_data_verificateur | null;
  redacteur: SearchDocumentResult_data_redacteur | null;
  categorie: SearchDocumentResult_data_categorie | null;
  dernierChangementStatut: SearchDocumentResult_data_dernierChangementStatut | null;
  changementStatuts: SearchDocumentResult_data_changementStatuts[] | null;
  documentARemplacer: SearchDocumentResult_data_documentARemplacer | null;
}

export interface SearchDocumentResult {
  __typename: "GedSearchDocumentResult";
  total: number;
  data: SearchDocumentResult_data[];
}
