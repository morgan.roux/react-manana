/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GedSearchDocumentInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: SEARCH_DOCUMENT
// ====================================================

export interface SEARCH_DOCUMENT_searchGedDocuments_data_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
}

export type SEARCH_DOCUMENT_searchGedDocuments_data_origineAssocie = SEARCH_DOCUMENT_searchGedDocuments_data_origineAssocie_Laboratoire | SEARCH_DOCUMENT_searchGedDocuments_data_origineAssocie_PrestataireService | SEARCH_DOCUMENT_searchGedDocuments_data_origineAssocie_User;

export interface SEARCH_DOCUMENT_searchGedDocuments_data_typeAvoirAssociations {
  __typename: "GedDocumentAvoirAssociation";
  idDocument: string;
  type: string;
  correspondant: string;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_commande {
  __typename: "PRTCommande";
  id: string;
  nbrRef: number | null;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_sousCategorie {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_verificateur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_redacteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_categorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_dernierChangementStatut_createdBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_dernierChangementStatut_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: SEARCH_DOCUMENT_searchGedDocuments_data_dernierChangementStatut_createdBy_prestataireService | null;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_dernierChangementStatut_updatedBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_dernierChangementStatut_updatedBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: SEARCH_DOCUMENT_searchGedDocuments_data_dernierChangementStatut_updatedBy_prestataireService | null;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_dernierChangementStatut {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: SEARCH_DOCUMENT_searchGedDocuments_data_dernierChangementStatut_createdBy | null;
  updatedBy: SEARCH_DOCUMENT_searchGedDocuments_data_dernierChangementStatut_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_changementStatuts_createdBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_changementStatuts_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: SEARCH_DOCUMENT_searchGedDocuments_data_changementStatuts_createdBy_prestataireService | null;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_changementStatuts_updatedBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_changementStatuts_updatedBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: SEARCH_DOCUMENT_searchGedDocuments_data_changementStatuts_updatedBy_prestataireService | null;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_changementStatuts {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: SEARCH_DOCUMENT_searchGedDocuments_data_changementStatuts_createdBy | null;
  updatedBy: SEARCH_DOCUMENT_searchGedDocuments_data_changementStatuts_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_typeAvoirAssociations {
  __typename: "GedDocumentAvoirAssociation";
  idDocument: string;
  type: string;
  correspondant: string;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_commande {
  __typename: "PRTCommande";
  id: string;
  nbrRef: number | null;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_createdBy {
  __typename: "User";
  id: string;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_sousCategorie {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_verificateur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_redacteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_categorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_dernierChangementStatut_createdBy {
  __typename: "User";
  id: string;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_dernierChangementStatut_updatedBy {
  __typename: "User";
  id: string;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_dernierChangementStatut {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_dernierChangementStatut_createdBy | null;
  updatedBy: SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_dernierChangementStatut_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer {
  __typename: "GedDocument";
  id: string;
  description: string;
  nomenclature: string | null;
  numeroVersion: string | null;
  motCle1: string | null;
  motCle2: string | null;
  motCle3: string | null;
  dateHeureParution: any | null;
  dateHeureDebutValidite: any | null;
  dateHeureFinValidite: any | null;
  idDocumentARemplacer: string | null;
  idUserRedacteur: string | null;
  idUserVerificateur: string | null;
  idFichier: string;
  idSousCategorie: string | null;
  idPharmacie: string;
  idGroupement: string;
  createdAt: any;
  updatedAt: any;
  nombreConsultations: number;
  nombreTelechargements: number;
  nombreCommentaires: number;
  nombreReactions: number;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  factureTotalHt: number | null;
  factureTotalTtc: number | null;
  factureTva: number | null;
  factureDate: any | null;
  factureDateReglement: any | null;
  idReglementMode: string | null;
  numeroFacture: string | null;
  isGenererCommande: boolean | null;
  numeroCommande: number | null;
  typeAvoirAssociations: SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_typeAvoirAssociations[] | null;
  commande: SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_commande | null;
  idCommandes: string[] | null;
  type: string | null;
  createdBy: SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_createdBy;
  sousCategorie: SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_sousCategorie | null;
  fichier: SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_fichier;
  verificateur: SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_verificateur | null;
  redacteur: SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_redacteur | null;
  categorie: SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_categorie | null;
  dernierChangementStatut: SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer_dernierChangementStatut | null;
}

export interface SEARCH_DOCUMENT_searchGedDocuments_data {
  __typename: "GedDocument";
  id: string;
  type: string | null;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  origineAssocie: SEARCH_DOCUMENT_searchGedDocuments_data_origineAssocie | null;
  factureTotalHt: number | null;
  factureTotalTtc: number | null;
  factureTva: number | null;
  factureDate: any | null;
  factureDateReglement: any | null;
  idReglementMode: string | null;
  numeroFacture: string | null;
  isGenererCommande: boolean | null;
  numeroCommande: number | null;
  typeAvoirAssociations: SEARCH_DOCUMENT_searchGedDocuments_data_typeAvoirAssociations[] | null;
  commande: SEARCH_DOCUMENT_searchGedDocuments_data_commande | null;
  idCommandes: string[] | null;
  description: string;
  nomenclature: string | null;
  numeroVersion: string | null;
  motCle1: string | null;
  motCle2: string | null;
  motCle3: string | null;
  dateHeureParution: any | null;
  dateHeureDebutValidite: any | null;
  dateHeureFinValidite: any | null;
  idDocumentARemplacer: string | null;
  idUserRedacteur: string | null;
  idUserVerificateur: string | null;
  idFichier: string;
  idSousCategorie: string | null;
  idPharmacie: string;
  idGroupement: string;
  favoris: boolean;
  createdAt: any;
  updatedAt: any;
  isOcr: boolean | null;
  createdBy: SEARCH_DOCUMENT_searchGedDocuments_data_createdBy;
  nombreConsultations: number;
  nombreTelechargements: number;
  nombreCommentaires: number;
  nombreReactions: number;
  sousCategorie: SEARCH_DOCUMENT_searchGedDocuments_data_sousCategorie | null;
  fichier: SEARCH_DOCUMENT_searchGedDocuments_data_fichier;
  verificateur: SEARCH_DOCUMENT_searchGedDocuments_data_verificateur | null;
  redacteur: SEARCH_DOCUMENT_searchGedDocuments_data_redacteur | null;
  categorie: SEARCH_DOCUMENT_searchGedDocuments_data_categorie | null;
  dernierChangementStatut: SEARCH_DOCUMENT_searchGedDocuments_data_dernierChangementStatut | null;
  changementStatuts: SEARCH_DOCUMENT_searchGedDocuments_data_changementStatuts[] | null;
  documentARemplacer: SEARCH_DOCUMENT_searchGedDocuments_data_documentARemplacer | null;
}

export interface SEARCH_DOCUMENT_searchGedDocuments {
  __typename: "GedSearchDocumentResult";
  total: number;
  data: SEARCH_DOCUMENT_searchGedDocuments_data[];
}

export interface SEARCH_DOCUMENT {
  searchGedDocuments: SEARCH_DOCUMENT_searchGedDocuments;
}

export interface SEARCH_DOCUMENTVariables {
  input: GedSearchDocumentInput;
}
