/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: OCR_PHARMACIE_EMAIL_DATA
// ====================================================

export interface OCR_PHARMACIE_EMAIL_DATA {
  oCRPharmacieEmailData: number;
}
