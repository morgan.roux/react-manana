/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GedSousCategorieInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_SOUS_CATEGORIE
// ====================================================

export interface UPDATE_SOUS_CATEGORIE_updateOneGedSousCategorie {
  __typename: "GedSousCategorie";
  id: string;
}

export interface UPDATE_SOUS_CATEGORIE {
  updateOneGedSousCategorie: UPDATE_SOUS_CATEGORIE_updateOneGedSousCategorie;
}

export interface UPDATE_SOUS_CATEGORIEVariables {
  id: string;
  input: GedSousCategorieInput;
}
