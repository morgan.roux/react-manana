/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_SOUS_CATEGORIE
// ====================================================

export interface DELETE_SOUS_CATEGORIE_deleteOneGedSousCategorie {
  __typename: "GedSousCategorieDeleteResponse";
  id: string | null;
}

export interface DELETE_SOUS_CATEGORIE {
  deleteOneGedSousCategorie: DELETE_SOUS_CATEGORIE_deleteOneGedSousCategorie;
}

export interface DELETE_SOUS_CATEGORIEVariables {
  input: DeleteOneInput;
}
