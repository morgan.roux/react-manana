/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_CATEGORIE
// ====================================================

export interface DELETE_CATEGORIE_deleteOneGedCategorie {
  __typename: "GedCategorieDeleteResponse";
  id: string | null;
}

export interface DELETE_CATEGORIE {
  deleteOneGedCategorie: DELETE_CATEGORIE_deleteOneGedCategorie;
}

export interface DELETE_CATEGORIEVariables {
  input: DeleteOneInput;
}
