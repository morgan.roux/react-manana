/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_CATEGORIE
// ====================================================

export interface GET_CATEGORIE_gedCategorie_mesSousCategories_partenaireValidateur {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface GET_CATEGORIE_gedCategorie_mesSousCategories_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_CATEGORIE_gedCategorie_mesSousCategories {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
  nombreDocuments: number;
  partenaireValidateur: GET_CATEGORIE_gedCategorie_mesSousCategories_partenaireValidateur | null;
  participants: GET_CATEGORIE_gedCategorie_mesSousCategories_participants[];
}

export interface GET_CATEGORIE_gedCategorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
  base: boolean;
  idGroupement: string;
  idPharmacie: string | null;
  createdAt: any;
  updatedAt: any;
  type: string | null;
  mesSousCategories: GET_CATEGORIE_gedCategorie_mesSousCategories[];
}

export interface GET_CATEGORIE {
  gedCategorie: GET_CATEGORIE_gedCategorie | null;
}

export interface GET_CATEGORIEVariables {
  id: string;
}
