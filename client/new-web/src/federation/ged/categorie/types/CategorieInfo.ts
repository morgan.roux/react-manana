/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: CategorieInfo
// ====================================================

export interface CategorieInfo_mesSousCategories_partenaireValidateur {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface CategorieInfo_mesSousCategories_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface CategorieInfo_mesSousCategories {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
  nombreDocuments: number;
  partenaireValidateur: CategorieInfo_mesSousCategories_partenaireValidateur | null;
  participants: CategorieInfo_mesSousCategories_participants[];
}

export interface CategorieInfo {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
  base: boolean;
  idGroupement: string;
  idPharmacie: string | null;
  createdAt: any;
  updatedAt: any;
  type: string | null;
  mesSousCategories: CategorieInfo_mesSousCategories[];
}
