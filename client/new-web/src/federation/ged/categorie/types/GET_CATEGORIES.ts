/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, GedCategorieFilter, GedCategorieSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_CATEGORIES
// ====================================================

export interface GET_CATEGORIES_gedCategories_nodes_mesSousCategories_partenaireValidateur {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface GET_CATEGORIES_gedCategories_nodes_mesSousCategories_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_CATEGORIES_gedCategories_nodes_mesSousCategories {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
  nombreDocuments: number;
  partenaireValidateur: GET_CATEGORIES_gedCategories_nodes_mesSousCategories_partenaireValidateur | null;
  participants: GET_CATEGORIES_gedCategories_nodes_mesSousCategories_participants[];
}

export interface GET_CATEGORIES_gedCategories_nodes {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
  base: boolean;
  idGroupement: string;
  idPharmacie: string | null;
  createdAt: any;
  updatedAt: any;
  type: string | null;
  mesSousCategories: GET_CATEGORIES_gedCategories_nodes_mesSousCategories[];
}

export interface GET_CATEGORIES_gedCategories {
  __typename: "GedCategorieConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_CATEGORIES_gedCategories_nodes[];
}

export interface GET_CATEGORIES {
  gedCategories: GET_CATEGORIES_gedCategories;
}

export interface GET_CATEGORIESVariables {
  paging?: OffsetPaging | null;
  filter?: GedCategorieFilter | null;
  sorting?: GedCategorieSort[] | null;
}
