import gql from 'graphql-tag';
import { MINIMAL_CATEGORIE_INFO, SOUS_CATEGORIE_INFO } from './fragment';

export const CREATE_CATEGORIE = gql`
  mutation CREATE_CATEGORIE($categoryInput: GedCategorieInput!) {
    createOneGedCategorie(input: $categoryInput) {
      ...CategorieInfo
    }
  }

  ${MINIMAL_CATEGORIE_INFO}
`;

export const UPDATE_CATEGORIE = gql`
  mutation UPDATE_CATEGORIE($categoryInput: GedCategorieInput!, $id: String!) {
    updateOneGedCategorie(input: $categoryInput, id: $id) {
      ...CategorieInfo
    }
  }

  ${MINIMAL_CATEGORIE_INFO}
`;

export const DELETE_CATEGORIE = gql`
  mutation DELETE_CATEGORIE($input: DeleteOneInput!) {
    deleteOneGedCategorie(input: $input) {
      id
    }
  }
`;

export const CREATE_SOUS_CATEGORIE = gql`
  mutation CREATE_SOUS_CATEGORIE($input: GedSousCategorieInput!) {
    createOneGedSousCategorie(input: $input) {
      ...SousCategorieInfo
    }
  }
  ${SOUS_CATEGORIE_INFO}
`;

export const UPDATE_SOUS_CATEGORIE = gql`
  mutation UPDATE_SOUS_CATEGORIE($id: String!, $input: GedSousCategorieInput!) {
    updateOneGedSousCategorie(id: $id, input: $input) {
      ...SousCategorieInfo
    }
  }
  ${SOUS_CATEGORIE_INFO}
`;

export const DELETE_SOUS_CATEGORIE = gql`
  mutation DELETE_SOUS_CATEGORIE($input: DeleteOneInput!) {
    deleteOneGedSousCategorie(input: $input) {
      id
    }
  }
`;
