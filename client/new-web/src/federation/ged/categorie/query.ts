import gql from 'graphql-tag';
import { MINIMAL_CATEGORIE_INFO, FULL_CATEGORIE_INFO, FULL_MY_CATEGORIE_INFO } from './fragment';

export const GET_CATEGORIES = gql`
    query GET_CATEGORIES($paging: OffsetPaging, $filter: GedCategorieFilter, $sorting: [GedCategorieSort!] ){
        gedCategories(paging: $paging, filter: $filter, sorting: $sorting){
            nodes{
            ...CategorieInfo
            }
        }
    }

    ${FULL_CATEGORIE_INFO}
`;


export const GET_MES_CATEGORIES = gql`
    query GET_MES_CATEGORIES{
        gedMesCategories{
            ...CategorieInfo
        }
    }

    ${FULL_MY_CATEGORIE_INFO}
`;

export const GET_CATEGORIE = gql`
    query GET_CATEGORIE($id : ID!){
        gedCategorie(id: $id){
            ...CategorieInfo
        }
    }

    ${FULL_CATEGORIE_INFO}
`;
