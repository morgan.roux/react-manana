/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_OPTION_PARTAGE
// ====================================================

export interface DELETE_OPTION_PARTAGE_deleteOneOptionPartage {
  __typename: "OptionPartageDeleteResponse";
  id: string | null;
}

export interface DELETE_OPTION_PARTAGE {
  deleteOneOptionPartage: DELETE_OPTION_PARTAGE_deleteOneOptionPartage;
}

export interface DELETE_OPTION_PARTAGEVariables {
  input: DeleteOneInput;
}
