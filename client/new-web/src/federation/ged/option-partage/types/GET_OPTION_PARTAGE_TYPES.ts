/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, OptionPartageTypeFilter, OptionPartageTypeSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_OPTION_PARTAGE_TYPES
// ====================================================

export interface GET_OPTION_PARTAGE_TYPES_optionPartageTypes_nodes {
  __typename: "OptionPartageType";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_OPTION_PARTAGE_TYPES_optionPartageTypes {
  __typename: "OptionPartageTypeConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_OPTION_PARTAGE_TYPES_optionPartageTypes_nodes[];
}

export interface GET_OPTION_PARTAGE_TYPES {
  optionPartageTypes: GET_OPTION_PARTAGE_TYPES_optionPartageTypes;
}

export interface GET_OPTION_PARTAGE_TYPESVariables {
  paging?: OffsetPaging | null;
  filter?: OptionPartageTypeFilter | null;
  sorting?: OptionPartageTypeSort[] | null;
}
