/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OptionPartageInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_OPTION_PARTAGE
// ====================================================

export interface UPDATE_OPTION_PARTAGE_updateOneOptionPartage_type {
  __typename: "OptionPartageType";
  id: string;
  code: string;
  libelle: string;
}

export interface UPDATE_OPTION_PARTAGE_updateOneOptionPartage {
  __typename: "OptionPartage";
  id: string;
  idParametre: string;
  idPharmacie: string;
  idType: string;
  type: UPDATE_OPTION_PARTAGE_updateOneOptionPartage_type | null;
}

export interface UPDATE_OPTION_PARTAGE {
  updateOneOptionPartage: UPDATE_OPTION_PARTAGE_updateOneOptionPartage;
}

export interface UPDATE_OPTION_PARTAGEVariables {
  id: string;
  input: OptionPartageInput;
}
