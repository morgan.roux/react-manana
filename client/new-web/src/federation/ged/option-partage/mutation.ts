import gql from 'graphql-tag';
import { FULL_OPTION_PARTAGE_INFO } from './fragment';

export const CREATE_OPTION_PARTAGE = gql`
  mutation CREATE_OPTION_PARTAGE($input: OptionPartageInput!) {
    createOneOptionPartage(input: $input) {
      ...OptionPartageInfo
    }
  }

  ${FULL_OPTION_PARTAGE_INFO}
`;

export const UPDATE_OPTION_PARTAGE = gql`
  mutation UPDATE_OPTION_PARTAGE($id: String!, $input: OptionPartageInput!) {
    updateOneOptionPartage(id: $id, input: $input) {
      ...OptionPartageInfo
    }
  }

  ${FULL_OPTION_PARTAGE_INFO}
`;

export const DELETE_OPTION_PARTAGE= gql`
  mutation DELETE_OPTION_PARTAGE($input: DeleteOneInput!) {
    deleteOneOptionPartage(input: $input) {
      id
    }
  }
`;