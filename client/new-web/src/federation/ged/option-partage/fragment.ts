import gql from 'graphql-tag';

export const FULL_OPTION_PARTAGE_INFO = gql`
  fragment OptionPartageInfo on OptionPartage {
    id
    idParametre
    idPharmacie
    idType
    type {
      id
      code
      libelle
    }
  }
`;

export const OPTION_PARTAGE_TYPE = gql`
  fragment OptionPartageTypeInfo on OptionPartageType {
    id
    code
    libelle
  }
`;