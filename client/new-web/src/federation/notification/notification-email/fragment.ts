import gql from 'graphql-tag';

export const SEND_EMAIL_RESULT_INFO = gql`
  fragment SendEmailResultInfo on SendEmailResult {
    code
    message
  }
`;
