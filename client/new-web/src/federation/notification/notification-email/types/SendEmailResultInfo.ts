/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SendEmailResultInfo
// ====================================================

export interface SendEmailResultInfo {
  __typename: "SendEmailResult";
  code: string | null;
  message: string | null;
}
