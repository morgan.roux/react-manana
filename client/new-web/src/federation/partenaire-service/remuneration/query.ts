import gql from 'graphql-tag';
import {
  PRESTATION_TYPE_INFO,
  REGLEMENT_MODE_INFO,
  REMUNERATION_ANNEE_INFO,
  REMUNERATION_INFO,
  REMUNERATION_REGLEMENT_INFO,
} from './fragment';

export const GET_PRT_REMUNERATIONS = gql`
  query GET_PRT_REMUNEARTIONS(
    $paging: OffsetPaging
    $filter: PRTRemunerationFilter
    $sorting: [PRTRemunerationSort!]
  ) {
    pRTRemunerations(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...PRTRemunerationInfo
      }
    }
  }
  ${REMUNERATION_INFO}
`;

export const GET_PRT_REMUNERATIONS_AGGREGATES = gql`
  query GET_PRT_REMUNERATIONS_AGGREGATES($filter: PRTRemunerationAggregateFilter) {
    pRTRemunerationAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;

export const GET_PRT_REMUNERATIONS_REGLEMENTS = gql`
  query GET_PRT_REMUNERATIONS_REGLEMENT(
    $paging: OffsetPaging
    $filter: PRTRemunerationReglementFilter
    $sorting: [PRTRemunerationReglementSort!]
  ) {
    pRTRemunerationReglements(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...PRTRemunerationReglementInfo
      }
    }
  }
  ${REMUNERATION_REGLEMENT_INFO}
`;

export const GET_PRT_REMUNERATION_REGLEMENTS_AGGREGATES = gql`
  query GET_PRT_REMUNERATION_REGLEMENTS_AGGREGATES(
    $filter: PRTRemunerationReglementAggregateFilter
  ) {
    pRTRemunerationReglementAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;

export const GET_PRT_REGLEMENT_MODE = gql`
  query GET_PRT_REGLEMENT_MODE(
    $paging: OffsetPaging
    $filter: PRTReglementModeFilter
    $sorting: [PRTReglementModeSort!]
  ) {
    pRTReglementModes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...PRTReglementModeInfo
      }
    }
  }
  ${REGLEMENT_MODE_INFO}
`;

export const GET_PRESTATION_TYPE = gql`
  query GET_PRESTATION_TYPE(
    $paging: OffsetPaging
    $filter: PrestationTypeFilter
    $sorting: [PrestationTypeSort!]
  ) {
    prestationTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...PrestationTypeInfo
      }
    }
  }
  ${PRESTATION_TYPE_INFO}
`;

export const GET_REMUNERATION_ANNEE = gql`
  query GET_REMUNERATION_ANNEE($input: PRTRemunerationAnneeInput!) {
    remunerationAnnee(input: $input) {
      ...PRTRemunerationAnneeInfo
    }
  }
  ${REMUNERATION_ANNEE_INFO}
`;

export const GET_REMUNERATION_PREVUE_EXIST = gql`
  query GET_REMUNERATION_PREVUE_EXIST($input: PRTRemunerationPrevueExistInput!) {
    remunerationPrevueExist(input: $input) {
      ...PRTRemunerationInfo
    }
  }
  ${REMUNERATION_INFO}
`;
