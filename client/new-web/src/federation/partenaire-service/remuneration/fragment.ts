import gql from 'graphql-tag';
import { REMUNERATION_SUIVI_OPERATIONNEL_INFO } from '../pilotage/fragment';

export const PRESTATION_TYPE_INFO = gql`
  fragment PrestationTypeInfo on PrestationType {
    id
    code
    libelle
    couleur
  }
`;

export const REMUNERATION_INFO = gql`
  fragment PRTRemunerationInfo on PRTRemuneration {
    id
    dateEcheance
    montantPrevu
    montantRealise
    idPartenaireTypeAssocie
    partenaireType
    prestationType {
      ...PrestationTypeInfo
    }
  }
  ${PRESTATION_TYPE_INFO}
`;

export const REGLEMENT_MODE_INFO = gql`
  fragment PRTReglementModeInfo on PRTReglementMode {
    id
    code
    libelle
  }
`;

export const REMUNERATION_REGLEMENT_INFO = gql`
  fragment PRTRemunerationReglementInfo on PRTRemunerationReglement {
    id
    idPrestationType
    idModeReglement
    idRemuneration
    montant
    description
    dateReglement
    reglementMode {
      ...PRTReglementModeInfo
    }
    remuneration {
      ...PRTRemunerationInfo
    }
    prestationType {
      ...PrestationTypeInfo
    }
    fichiers {
      id
      nomOriginal
      publicUrl
    }
  }
  ${REMUNERATION_INFO}
  ${REGLEMENT_MODE_INFO}
  ${PRESTATION_TYPE_INFO}
`;

export const REMUNERATION_ANNEE_INFO = gql`
  fragment PRTRemunerationAnneeInfo on PRTRemunerationAnnee {
    nomPartenaire
    composition
    totalPrevues
    totalRealises
    pourcentageRealises
    dataAnnee {
      indexMois
      data {
        ...RemunerationSuiviOperationnelInfo
      }
    }
  }
  ${REMUNERATION_SUIVI_OPERATIONNEL_INFO}
`;
