/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_PRT_REMUNERATION_REGLEMENT
// ====================================================

export interface DELETE_PRT_REMUNERATION_REGLEMENT_deleteOnePRTRemunerationReglement {
  __typename: "PRTRemunerationReglement";
  id: string;
}

export interface DELETE_PRT_REMUNERATION_REGLEMENT {
  deleteOnePRTRemunerationReglement: DELETE_PRT_REMUNERATION_REGLEMENT_deleteOnePRTRemunerationReglement;
}

export interface DELETE_PRT_REMUNERATION_REGLEMENTVariables {
  id: string;
}
