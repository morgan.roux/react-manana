/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PRTRemunerationAnneeInfo
// ====================================================

export interface PRTRemunerationAnneeInfo_dataAnnee_data_types_prestation {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface PRTRemunerationAnneeInfo_dataAnnee_data_types {
  __typename: "PRTRemunerationSuiviOperationnelMontant";
  prestation: PRTRemunerationAnneeInfo_dataAnnee_data_types_prestation;
  totalMontantPrevuPrestation: number;
  totalMontantReglementPrestation: number;
  pourcentagePrestation: number;
  idPartenaireTypeAssocie: string;
  nomPartenaire: string;
}

export interface PRTRemunerationAnneeInfo_dataAnnee_data {
  __typename: "PRTRemunerationSuiviOperationnel";
  types: PRTRemunerationAnneeInfo_dataAnnee_data_types[];
  totalMontantPrevues: number;
  totalMontantReglements: number;
  pourcentageReelReglement: number;
}

export interface PRTRemunerationAnneeInfo_dataAnnee {
  __typename: "PRTRemunerationAnneeValue";
  indexMois: number;
  data: PRTRemunerationAnneeInfo_dataAnnee_data;
}

export interface PRTRemunerationAnneeInfo {
  __typename: "PRTRemunerationAnnee";
  nomPartenaire: string;
  composition: string;
  totalPrevues: number;
  totalRealises: number;
  pourcentageRealises: number;
  dataAnnee: PRTRemunerationAnneeInfo_dataAnnee[];
}
