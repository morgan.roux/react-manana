/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTRemunerationReglementInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_PRT_REMUNERATION_REGLEMENT
// ====================================================

export interface CREATE_PRT_REMUNERATION_REGLEMENT_createOnePRTRemunerationReglement_reglementMode {
  __typename: "PRTReglementMode";
  id: string;
  code: string;
  libelle: string;
}

export interface CREATE_PRT_REMUNERATION_REGLEMENT_createOnePRTRemunerationReglement_remuneration_prestationType {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface CREATE_PRT_REMUNERATION_REGLEMENT_createOnePRTRemunerationReglement_remuneration {
  __typename: "PRTRemuneration";
  id: string;
  dateEcheance: any;
  montantPrevu: number;
  montantRealise: number;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  prestationType: CREATE_PRT_REMUNERATION_REGLEMENT_createOnePRTRemunerationReglement_remuneration_prestationType;
}

export interface CREATE_PRT_REMUNERATION_REGLEMENT_createOnePRTRemunerationReglement_prestationType {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface CREATE_PRT_REMUNERATION_REGLEMENT_createOnePRTRemunerationReglement_fichiers {
  __typename: "Fichier";
  id: string;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface CREATE_PRT_REMUNERATION_REGLEMENT_createOnePRTRemunerationReglement {
  __typename: "PRTRemunerationReglement";
  id: string;
  idPrestationType: string;
  idModeReglement: string;
  idRemuneration: string;
  montant: number;
  description: string | null;
  dateReglement: any;
  reglementMode: CREATE_PRT_REMUNERATION_REGLEMENT_createOnePRTRemunerationReglement_reglementMode;
  remuneration: CREATE_PRT_REMUNERATION_REGLEMENT_createOnePRTRemunerationReglement_remuneration;
  prestationType: CREATE_PRT_REMUNERATION_REGLEMENT_createOnePRTRemunerationReglement_prestationType;
  fichiers: CREATE_PRT_REMUNERATION_REGLEMENT_createOnePRTRemunerationReglement_fichiers[] | null;
}

export interface CREATE_PRT_REMUNERATION_REGLEMENT {
  createOnePRTRemunerationReglement: CREATE_PRT_REMUNERATION_REGLEMENT_createOnePRTRemunerationReglement;
}

export interface CREATE_PRT_REMUNERATION_REGLEMENTVariables {
  input: PRTRemunerationReglementInput;
}
