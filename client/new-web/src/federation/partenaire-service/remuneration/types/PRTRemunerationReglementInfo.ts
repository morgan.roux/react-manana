/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PRTRemunerationReglementInfo
// ====================================================

export interface PRTRemunerationReglementInfo_reglementMode {
  __typename: "PRTReglementMode";
  id: string;
  code: string;
  libelle: string;
}

export interface PRTRemunerationReglementInfo_remuneration_prestationType {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface PRTRemunerationReglementInfo_remuneration {
  __typename: "PRTRemuneration";
  id: string;
  dateEcheance: any;
  montantPrevu: number;
  montantRealise: number;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  prestationType: PRTRemunerationReglementInfo_remuneration_prestationType;
}

export interface PRTRemunerationReglementInfo_prestationType {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface PRTRemunerationReglementInfo_fichiers {
  __typename: "Fichier";
  id: string;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface PRTRemunerationReglementInfo {
  __typename: "PRTRemunerationReglement";
  id: string;
  idPrestationType: string;
  idModeReglement: string;
  idRemuneration: string;
  montant: number;
  description: string | null;
  dateReglement: any;
  reglementMode: PRTRemunerationReglementInfo_reglementMode;
  remuneration: PRTRemunerationReglementInfo_remuneration;
  prestationType: PRTRemunerationReglementInfo_prestationType;
  fichiers: PRTRemunerationReglementInfo_fichiers[] | null;
}
