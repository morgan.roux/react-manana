/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTRemunerationInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_PRT_REMUNERATION
// ====================================================

export interface CREATE_PRT_REMUNERATION_createOnePRTRemuneration_prestationType {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface CREATE_PRT_REMUNERATION_createOnePRTRemuneration {
  __typename: "PRTRemuneration";
  id: string;
  dateEcheance: any;
  montantPrevu: number;
  montantRealise: number;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  prestationType: CREATE_PRT_REMUNERATION_createOnePRTRemuneration_prestationType;
}

export interface CREATE_PRT_REMUNERATION {
  createOnePRTRemuneration: CREATE_PRT_REMUNERATION_createOnePRTRemuneration | null;
}

export interface CREATE_PRT_REMUNERATIONVariables {
  input: PRTRemunerationInput;
}
