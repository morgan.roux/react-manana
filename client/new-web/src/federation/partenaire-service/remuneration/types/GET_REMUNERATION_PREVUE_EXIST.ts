/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTRemunerationPrevueExistInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_REMUNERATION_PREVUE_EXIST
// ====================================================

export interface GET_REMUNERATION_PREVUE_EXIST_remunerationPrevueExist_prestationType {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface GET_REMUNERATION_PREVUE_EXIST_remunerationPrevueExist {
  __typename: "PRTRemuneration";
  id: string;
  dateEcheance: any;
  montantPrevu: number;
  montantRealise: number;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  prestationType: GET_REMUNERATION_PREVUE_EXIST_remunerationPrevueExist_prestationType;
}

export interface GET_REMUNERATION_PREVUE_EXIST {
  remunerationPrevueExist: GET_REMUNERATION_PREVUE_EXIST_remunerationPrevueExist | null;
}

export interface GET_REMUNERATION_PREVUE_EXISTVariables {
  input: PRTRemunerationPrevueExistInput;
}
