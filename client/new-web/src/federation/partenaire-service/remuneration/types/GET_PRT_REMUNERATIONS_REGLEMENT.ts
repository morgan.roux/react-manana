/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTRemunerationReglementFilter, PRTRemunerationReglementSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_PRT_REMUNERATIONS_REGLEMENT
// ====================================================

export interface GET_PRT_REMUNERATIONS_REGLEMENT_pRTRemunerationReglements_nodes_reglementMode {
  __typename: "PRTReglementMode";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_PRT_REMUNERATIONS_REGLEMENT_pRTRemunerationReglements_nodes_remuneration_prestationType {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface GET_PRT_REMUNERATIONS_REGLEMENT_pRTRemunerationReglements_nodes_remuneration {
  __typename: "PRTRemuneration";
  id: string;
  dateEcheance: any;
  montantPrevu: number;
  montantRealise: number;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  prestationType: GET_PRT_REMUNERATIONS_REGLEMENT_pRTRemunerationReglements_nodes_remuneration_prestationType;
}

export interface GET_PRT_REMUNERATIONS_REGLEMENT_pRTRemunerationReglements_nodes_prestationType {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface GET_PRT_REMUNERATIONS_REGLEMENT_pRTRemunerationReglements_nodes_fichiers {
  __typename: "Fichier";
  id: string;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface GET_PRT_REMUNERATIONS_REGLEMENT_pRTRemunerationReglements_nodes {
  __typename: "PRTRemunerationReglement";
  id: string;
  idPrestationType: string;
  idModeReglement: string;
  idRemuneration: string;
  montant: number;
  description: string | null;
  dateReglement: any;
  reglementMode: GET_PRT_REMUNERATIONS_REGLEMENT_pRTRemunerationReglements_nodes_reglementMode;
  remuneration: GET_PRT_REMUNERATIONS_REGLEMENT_pRTRemunerationReglements_nodes_remuneration;
  prestationType: GET_PRT_REMUNERATIONS_REGLEMENT_pRTRemunerationReglements_nodes_prestationType;
  fichiers: GET_PRT_REMUNERATIONS_REGLEMENT_pRTRemunerationReglements_nodes_fichiers[] | null;
}

export interface GET_PRT_REMUNERATIONS_REGLEMENT_pRTRemunerationReglements {
  __typename: "PRTRemunerationReglementConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_PRT_REMUNERATIONS_REGLEMENT_pRTRemunerationReglements_nodes[];
}

export interface GET_PRT_REMUNERATIONS_REGLEMENT {
  pRTRemunerationReglements: GET_PRT_REMUNERATIONS_REGLEMENT_pRTRemunerationReglements;
}

export interface GET_PRT_REMUNERATIONS_REGLEMENTVariables {
  paging?: OffsetPaging | null;
  filter?: PRTRemunerationReglementFilter | null;
  sorting?: PRTRemunerationReglementSort[] | null;
}
