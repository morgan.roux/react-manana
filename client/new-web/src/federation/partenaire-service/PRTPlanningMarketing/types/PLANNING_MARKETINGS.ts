/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTPlanningMarketingInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: PLANNING_MARKETINGS
// ====================================================

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_partenaireTypeAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  nom: string;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_partenaireTypeAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export type PLANNING_MARKETINGS_pRTPlanningMarketing_partenaireTypeAssocie = PLANNING_MARKETINGS_pRTPlanningMarketing_partenaireTypeAssocie_Laboratoire | PLANNING_MARKETINGS_pRTPlanningMarketing_partenaireTypeAssocie_PrestataireService;

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_type {
  __typename: "PRTPlanMarketingType";
  id: string;
  libelle: string;
  code: string;
  couleur: string | null;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_statut {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_miseAvants {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_actions_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_actions_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_actions {
  __typename: "TodoAction";
  id: string;
  ordre: number;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idActionParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_actions_importance | null;
  urgence: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_actions_urgence | null;
  nbComment: number | null;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings {
  __typename: "PRTPlanMarketing";
  id: string;
  titre: string;
  description: string | null;
  dateDebut: any;
  dateFin: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idGroupement: string;
  idPharmacie: string;
  type: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_type;
  statut: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_statut;
  fichiers: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_fichiers[] | null;
  miseAvants: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_miseAvants[] | null;
  actions: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_actions[] | null;
  produits: string[] | null;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing {
  __typename: "PRTPlanningMarketing";
  id: string;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  partenaireTypeAssocie: PLANNING_MARKETINGS_pRTPlanningMarketing_partenaireTypeAssocie | null;
  planMarketings: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings[] | null;
}

export interface PLANNING_MARKETINGS {
  pRTPlanningMarketing: PLANNING_MARKETINGS_pRTPlanningMarketing[];
}

export interface PLANNING_MARKETINGSVariables {
  input: PRTPlanningMarketingInput;
}
