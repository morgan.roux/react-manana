/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PrtPlanningMarketingInfo
// ====================================================

export interface PrtPlanningMarketingInfo_partenaireTypeAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  nom: string;
}

export interface PrtPlanningMarketingInfo_partenaireTypeAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export type PrtPlanningMarketingInfo_partenaireTypeAssocie = PrtPlanningMarketingInfo_partenaireTypeAssocie_Laboratoire | PrtPlanningMarketingInfo_partenaireTypeAssocie_PrestataireService;

export interface PrtPlanningMarketingInfo_planMarketings_type {
  __typename: "PRTPlanMarketingType";
  id: string;
  libelle: string;
  code: string;
  couleur: string | null;
}

export interface PrtPlanningMarketingInfo_planMarketings_statut {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface PrtPlanningMarketingInfo_planMarketings_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface PrtPlanningMarketingInfo_planMarketings_miseAvants {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
}

export interface PrtPlanningMarketingInfo_planMarketings_actions_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface PrtPlanningMarketingInfo_planMarketings_actions_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface PrtPlanningMarketingInfo_planMarketings_actions {
  __typename: "TodoAction";
  id: string;
  ordre: number;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idActionParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: PrtPlanningMarketingInfo_planMarketings_actions_importance | null;
  urgence: PrtPlanningMarketingInfo_planMarketings_actions_urgence | null;
  nbComment: number | null;
}

export interface PrtPlanningMarketingInfo_planMarketings {
  __typename: "PRTPlanMarketing";
  id: string;
  titre: string;
  description: string | null;
  dateDebut: any;
  dateFin: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idGroupement: string;
  idPharmacie: string;
  type: PrtPlanningMarketingInfo_planMarketings_type;
  statut: PrtPlanningMarketingInfo_planMarketings_statut;
  fichiers: PrtPlanningMarketingInfo_planMarketings_fichiers[] | null;
  miseAvants: PrtPlanningMarketingInfo_planMarketings_miseAvants[] | null;
  actions: PrtPlanningMarketingInfo_planMarketings_actions[] | null;
  produits: string[] | null;
}

export interface PrtPlanningMarketingInfo {
  __typename: "PRTPlanningMarketing";
  id: string;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  partenaireTypeAssocie: PrtPlanningMarketingInfo_partenaireTypeAssocie | null;
  planMarketings: PrtPlanningMarketingInfo_planMarketings[] | null;
}
