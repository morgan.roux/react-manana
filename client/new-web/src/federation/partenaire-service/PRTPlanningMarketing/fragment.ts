import gql from 'graphql-tag';
import { FULL_PLAN_MARKETING_INFO } from '../planMarketing/fragment';

export const PRT_PLANNING_MARKETING_INFO = gql`
  fragment PrtPlanningMarketingInfo on PRTPlanningMarketing {
    id
    partenaireType
    idPartenaireTypeAssocie
    partenaireTypeAssocie {
      ... on Laboratoire {
        id
        nom
      }
      ... on PrestataireService {
        id
        nom
      }
    }
    planMarketings {
      ...PRTPlanMarketingInfo
    }
  }
  ${FULL_PLAN_MARKETING_INFO}
`;
