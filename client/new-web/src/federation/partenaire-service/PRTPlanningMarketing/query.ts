import gql from 'graphql-tag';
import { PRT_PLANNING_MARKETING_INFO } from './fragment';

export const GET_PLANNING_MARKETINGS = gql`
  query PLANNING_MARKETINGS($input: PRTPlanningMarketingInput!) {
    pRTPlanningMarketing(input: $input) {
      ...PrtPlanningMarketingInfo
    }
  }

  ${PRT_PLANNING_MARKETING_INFO}
`;
