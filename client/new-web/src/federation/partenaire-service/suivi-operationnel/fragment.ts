import gql from 'graphql-tag';
import { FULL_USER_INFO } from '../../iam/user/fragment';
import { IMPORTANCE } from '../../tools/itemScoring/fragment';
import { PRT_CONTACT_FULL_INFO } from '../contact/fragment';

export const FULL_SUIVI_OPERATIONNEL_INFO = gql`
  fragment PRTSuiviOperationnelInfo on PRTSuiviOperationnel {
    id
    titre
    description
    dateHeure
    createdBy
    partenaireType
    idTypeAssocie
    idGroupement
    montant
    idImportance
    idTache
    idFonction
    contacts {
      ...PRTContactFullInfo
    }
    importance {
      ...Importance
    }
    idPharmacie
    participants {
      ...UserInfo
    }
    type {
      id
      libelle
      code
    }
    statut {
      id
      libelle
      code
    }
    fichiers {
      id
      chemin
      nomOriginal
      type
      publicUrl
    }
    nombreCommentaires
    nombreReaction
  }
  ${FULL_USER_INFO}
  ${IMPORTANCE}
  ${PRT_CONTACT_FULL_INFO}
`;

export const SUIVI_OPERATIONNEL_TYPE_INFO = gql`
  fragment PRTSuiviOperationnelTypeInfo on PRTSuiviOperationnelType {
    id
    code
    libelle
  }
`;

export const SUIVI_OPERATIONNEL_STATUT_INFO = gql`
  fragment PRTSuiviOperationnelStatutInfo on PRTSuiviOperationnelStatut {
    id
    code
    libelle
  }
`;
