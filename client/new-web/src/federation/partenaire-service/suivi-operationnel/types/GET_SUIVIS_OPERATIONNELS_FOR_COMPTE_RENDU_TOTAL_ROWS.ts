/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_TOTAL_ROWS
// ====================================================

export interface GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_TOTAL_ROWS {
  getSuiviOperationnelForCompteRenduRowsTotal: number;
}

export interface GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_TOTAL_ROWSVariables {
  idCompteRendu?: string | null;
  searchText?: string | null;
  idTypeAssocie: string;
}
