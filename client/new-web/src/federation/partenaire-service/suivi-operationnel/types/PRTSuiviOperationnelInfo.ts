/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PRTSuiviOperationnelInfo
// ====================================================

export interface PRTSuiviOperationnelInfo_contacts_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface PRTSuiviOperationnelInfo_contacts_contact {
  __typename: "ContactType";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
  whatsappMobProf: string | null;
  whatsappMobPerso: string | null;
  compteSkypeProf: string | null;
  compteSkypePerso: string | null;
  urlLinkedInProf: string | null;
  urlLinkedInPerso: string | null;
  urlTwitterProf: string | null;
  urlTwitterPerso: string | null;
  urlFacebookProf: string | null;
  urlFacebookPerso: string | null;
  codeMaj: string | null;
  idUserCreation: string | null;
  idUserModification: string | null;
}

export interface PRTSuiviOperationnelInfo_contacts {
  __typename: "PRTContact";
  id: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  fonction: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  photo: PRTSuiviOperationnelInfo_contacts_photo | null;
  contact: PRTSuiviOperationnelInfo_contacts_contact | null;
}

export interface PRTSuiviOperationnelInfo_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface PRTSuiviOperationnelInfo_participants_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PRTSuiviOperationnelInfo_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface PRTSuiviOperationnelInfo_participants {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: PRTSuiviOperationnelInfo_participants_role | null;
  photo: PRTSuiviOperationnelInfo_participants_photo | null;
  phoneNumber: string | null;
}

export interface PRTSuiviOperationnelInfo_type {
  __typename: "PRTSuiviOperationnelType";
  id: string;
  libelle: string;
  code: string;
}

export interface PRTSuiviOperationnelInfo_statut {
  __typename: "PRTSuiviOperationnelStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface PRTSuiviOperationnelInfo_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface PRTSuiviOperationnelInfo {
  __typename: "PRTSuiviOperationnel";
  id: string;
  titre: string;
  description: string | null;
  dateHeure: any;
  createdBy: string;
  partenaireType: string;
  idTypeAssocie: string;
  idGroupement: string;
  montant: number;
  idImportance: string | null;
  idTache: string | null;
  idFonction: string | null;
  contacts: PRTSuiviOperationnelInfo_contacts[] | null;
  importance: PRTSuiviOperationnelInfo_importance | null;
  idPharmacie: string;
  participants: PRTSuiviOperationnelInfo_participants[];
  type: PRTSuiviOperationnelInfo_type;
  statut: PRTSuiviOperationnelInfo_statut;
  fichiers: PRTSuiviOperationnelInfo_fichiers[] | null;
  nombreCommentaires: number | null;
  nombreReaction: number | null;
}
