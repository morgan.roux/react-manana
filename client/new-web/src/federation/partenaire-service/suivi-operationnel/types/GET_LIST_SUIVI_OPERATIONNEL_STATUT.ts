/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTSuiviOperationnelStatutFilter, PRTSuiviOperationnelStatutSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_LIST_SUIVI_OPERATIONNEL_STATUT
// ====================================================

export interface GET_LIST_SUIVI_OPERATIONNEL_STATUT_pRTSuiviOperationnelStatuts_nodes {
  __typename: "PRTSuiviOperationnelStatut";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_LIST_SUIVI_OPERATIONNEL_STATUT_pRTSuiviOperationnelStatuts {
  __typename: "PRTSuiviOperationnelStatutConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_LIST_SUIVI_OPERATIONNEL_STATUT_pRTSuiviOperationnelStatuts_nodes[];
}

export interface GET_LIST_SUIVI_OPERATIONNEL_STATUT {
  pRTSuiviOperationnelStatuts: GET_LIST_SUIVI_OPERATIONNEL_STATUT_pRTSuiviOperationnelStatuts;
}

export interface GET_LIST_SUIVI_OPERATIONNEL_STATUTVariables {
  paging?: OffsetPaging | null;
  filter?: PRTSuiviOperationnelStatutFilter | null;
  sorting?: PRTSuiviOperationnelStatutSort[] | null;
}
