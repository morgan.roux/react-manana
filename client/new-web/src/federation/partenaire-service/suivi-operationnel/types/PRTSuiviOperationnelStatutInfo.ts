/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PRTSuiviOperationnelStatutInfo
// ====================================================

export interface PRTSuiviOperationnelStatutInfo {
  __typename: "PRTSuiviOperationnelStatut";
  id: string;
  code: string;
  libelle: string;
}
