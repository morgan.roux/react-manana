/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTSuiviOperationnelAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_SUIVI_OPERATIONNEL_AGGREGATES
// ====================================================

export interface GET_SUIVI_OPERATIONNEL_AGGREGATES_pRTSuiviOperationnelAggregate_count {
  __typename: "PRTSuiviOperationnelCountAggregate";
  id: number | null;
}

export interface GET_SUIVI_OPERATIONNEL_AGGREGATES_pRTSuiviOperationnelAggregate {
  __typename: "PRTSuiviOperationnelAggregateResponse";
  count: GET_SUIVI_OPERATIONNEL_AGGREGATES_pRTSuiviOperationnelAggregate_count | null;
}

export interface GET_SUIVI_OPERATIONNEL_AGGREGATES {
  pRTSuiviOperationnelAggregate: GET_SUIVI_OPERATIONNEL_AGGREGATES_pRTSuiviOperationnelAggregate;
}

export interface GET_SUIVI_OPERATIONNEL_AGGREGATESVariables {
  filter?: PRTSuiviOperationnelAggregateFilter | null;
}
