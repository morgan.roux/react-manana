/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTSuiviOperationnelInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_SUIVI_OPERATIONNEL
// ====================================================

export interface UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_contacts_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_contacts_contact {
  __typename: "ContactType";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
  whatsappMobProf: string | null;
  whatsappMobPerso: string | null;
  compteSkypeProf: string | null;
  compteSkypePerso: string | null;
  urlLinkedInProf: string | null;
  urlLinkedInPerso: string | null;
  urlTwitterProf: string | null;
  urlTwitterPerso: string | null;
  urlFacebookProf: string | null;
  urlFacebookPerso: string | null;
  codeMaj: string | null;
  idUserCreation: string | null;
  idUserModification: string | null;
}

export interface UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_contacts {
  __typename: "PRTContact";
  id: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  fonction: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  photo: UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_contacts_photo | null;
  contact: UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_contacts_contact | null;
}

export interface UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_participants_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_participants {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_participants_role | null;
  photo: UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_participants_photo | null;
  phoneNumber: string | null;
}

export interface UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_type {
  __typename: "PRTSuiviOperationnelType";
  id: string;
  libelle: string;
  code: string;
}

export interface UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_statut {
  __typename: "PRTSuiviOperationnelStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel {
  __typename: "PRTSuiviOperationnel";
  id: string;
  titre: string;
  description: string | null;
  dateHeure: any;
  createdBy: string;
  partenaireType: string;
  idTypeAssocie: string;
  idGroupement: string;
  montant: number;
  idImportance: string | null;
  idTache: string | null;
  idFonction: string | null;
  contacts: UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_contacts[] | null;
  importance: UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_importance | null;
  idPharmacie: string;
  participants: UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_participants[];
  type: UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_type;
  statut: UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_statut;
  fichiers: UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel_fichiers[] | null;
  nombreCommentaires: number | null;
  nombreReaction: number | null;
}

export interface UPDATE_SUIVI_OPERATIONNEL {
  updateOnePRTSuiviOperationnel: UPDATE_SUIVI_OPERATIONNEL_updateOnePRTSuiviOperationnel;
}

export interface UPDATE_SUIVI_OPERATIONNELVariables {
  id: string;
  input: PRTSuiviOperationnelInput;
}
