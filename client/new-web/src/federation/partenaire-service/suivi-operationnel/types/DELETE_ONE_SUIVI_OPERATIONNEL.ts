/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_ONE_SUIVI_OPERATIONNEL
// ====================================================

export interface DELETE_ONE_SUIVI_OPERATIONNEL_deleteOnePRTSuiviOperationnel {
  __typename: "PRTSuiviOperationnelDeleteResponse";
  id: string | null;
}

export interface DELETE_ONE_SUIVI_OPERATIONNEL {
  deleteOnePRTSuiviOperationnel: DELETE_ONE_SUIVI_OPERATIONNEL_deleteOnePRTSuiviOperationnel;
}

export interface DELETE_ONE_SUIVI_OPERATIONNELVariables {
  input: DeleteOneInput;
}
