import gql from 'graphql-tag';
import { RENDEZ_VOUS_INFO } from './fragment';

export const CREATE_RENDEZ_VOUS = gql`
    mutation CREATE_RENDEZ_VOUS($input: PRTRendezVousInput!) {
        createOnePRTRendezVous(input: $input) {
            ...PRTRendezVousInfo
        }
    }
    ${ RENDEZ_VOUS_INFO }
`;

export const UPDATE_RENDEZ_VOUS = gql`
    mutation UPDATE_RENDEZ_VOUS($id: String!, $input: PRTRendezVousInput!) {
        updateOnePRTRendezVous(id: $id, input: $input) {
            ...PRTRendezVousInfo
        }
    }
    ${ RENDEZ_VOUS_INFO }
`;

export const DELETE_RENDEZ_VOUS = gql`
    mutation DELETE_RENDEZ_VOUS($id: String!) {
        deleteOnePRTRendezVous(id: $id) {
            id
        }
    }
`;