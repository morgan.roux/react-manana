/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_RENDEZ_VOUS
// ====================================================

export interface DELETE_RENDEZ_VOUS_deleteOnePRTRendezVous {
  __typename: "PRTRendezVous";
  id: string;
}

export interface DELETE_RENDEZ_VOUS {
  deleteOnePRTRendezVous: DELETE_RENDEZ_VOUS_deleteOnePRTRendezVous;
}

export interface DELETE_RENDEZ_VOUSVariables {
  id: string;
}
