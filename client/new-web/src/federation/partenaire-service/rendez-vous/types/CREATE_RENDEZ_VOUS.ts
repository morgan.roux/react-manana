/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTRendezVousInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_RENDEZ_VOUS
// ====================================================

export interface CREATE_RENDEZ_VOUS_createOnePRTRendezVous_invites_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface CREATE_RENDEZ_VOUS_createOnePRTRendezVous_invites_contact {
  __typename: "ContactType";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
  whatsappMobProf: string | null;
  whatsappMobPerso: string | null;
  compteSkypeProf: string | null;
  compteSkypePerso: string | null;
  urlLinkedInProf: string | null;
  urlLinkedInPerso: string | null;
  urlTwitterProf: string | null;
  urlTwitterPerso: string | null;
  urlFacebookProf: string | null;
  urlFacebookPerso: string | null;
  codeMaj: string | null;
  idUserCreation: string | null;
  idUserModification: string | null;
}

export interface CREATE_RENDEZ_VOUS_createOnePRTRendezVous_invites {
  __typename: "PRTContact";
  id: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  fonction: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  photo: CREATE_RENDEZ_VOUS_createOnePRTRendezVous_invites_photo | null;
  contact: CREATE_RENDEZ_VOUS_createOnePRTRendezVous_invites_contact | null;
}

export interface CREATE_RENDEZ_VOUS_createOnePRTRendezVous_participants_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_RENDEZ_VOUS_createOnePRTRendezVous_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface CREATE_RENDEZ_VOUS_createOnePRTRendezVous_participants {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: CREATE_RENDEZ_VOUS_createOnePRTRendezVous_participants_role | null;
  photo: CREATE_RENDEZ_VOUS_createOnePRTRendezVous_participants_photo | null;
  phoneNumber: string | null;
}

export interface CREATE_RENDEZ_VOUS_createOnePRTRendezVous_subject {
  __typename: "PRTRendezVousSujetVisite";
  id: string;
  libelle: string;
  code: string;
}

export interface CREATE_RENDEZ_VOUS_createOnePRTRendezVous {
  __typename: "PRTRendezVous";
  id: string;
  idSubject: string | null;
  ordreJour: string | null;
  heureDebut: string;
  heureFin: string | null;
  dateRendezVous: any;
  idPartenaireTypeAssocie: string;
  invites: CREATE_RENDEZ_VOUS_createOnePRTRendezVous_invites[];
  participants: CREATE_RENDEZ_VOUS_createOnePRTRendezVous_participants[];
  statut: string;
  partenaireType: string;
  subject: CREATE_RENDEZ_VOUS_createOnePRTRendezVous_subject | null;
  note: string | null;
}

export interface CREATE_RENDEZ_VOUS {
  createOnePRTRendezVous: CREATE_RENDEZ_VOUS_createOnePRTRendezVous;
}

export interface CREATE_RENDEZ_VOUSVariables {
  input: PRTRendezVousInput;
}
