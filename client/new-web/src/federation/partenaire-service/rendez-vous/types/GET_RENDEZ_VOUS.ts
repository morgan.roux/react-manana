/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTRendezVousFilter, PRTRendezVousSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_RENDEZ_VOUS
// ====================================================

export interface GET_RENDEZ_VOUS_pRTRendezVous_nodes_invites_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface GET_RENDEZ_VOUS_pRTRendezVous_nodes_invites_contact {
  __typename: "ContactType";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
  whatsappMobProf: string | null;
  whatsappMobPerso: string | null;
  compteSkypeProf: string | null;
  compteSkypePerso: string | null;
  urlLinkedInProf: string | null;
  urlLinkedInPerso: string | null;
  urlTwitterProf: string | null;
  urlTwitterPerso: string | null;
  urlFacebookProf: string | null;
  urlFacebookPerso: string | null;
  codeMaj: string | null;
  idUserCreation: string | null;
  idUserModification: string | null;
}

export interface GET_RENDEZ_VOUS_pRTRendezVous_nodes_invites {
  __typename: "PRTContact";
  id: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  fonction: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  photo: GET_RENDEZ_VOUS_pRTRendezVous_nodes_invites_photo | null;
  contact: GET_RENDEZ_VOUS_pRTRendezVous_nodes_invites_contact | null;
}

export interface GET_RENDEZ_VOUS_pRTRendezVous_nodes_participants_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface GET_RENDEZ_VOUS_pRTRendezVous_nodes_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface GET_RENDEZ_VOUS_pRTRendezVous_nodes_participants {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: GET_RENDEZ_VOUS_pRTRendezVous_nodes_participants_role | null;
  photo: GET_RENDEZ_VOUS_pRTRendezVous_nodes_participants_photo | null;
  phoneNumber: string | null;
}

export interface GET_RENDEZ_VOUS_pRTRendezVous_nodes_subject {
  __typename: "PRTRendezVousSujetVisite";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_RENDEZ_VOUS_pRTRendezVous_nodes {
  __typename: "PRTRendezVous";
  id: string;
  idSubject: string | null;
  ordreJour: string | null;
  heureDebut: string;
  heureFin: string | null;
  dateRendezVous: any;
  idPartenaireTypeAssocie: string;
  invites: GET_RENDEZ_VOUS_pRTRendezVous_nodes_invites[];
  participants: GET_RENDEZ_VOUS_pRTRendezVous_nodes_participants[];
  statut: string;
  partenaireType: string;
  subject: GET_RENDEZ_VOUS_pRTRendezVous_nodes_subject | null;
  note: string | null;
}

export interface GET_RENDEZ_VOUS_pRTRendezVous {
  __typename: "PRTRendezVousConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_RENDEZ_VOUS_pRTRendezVous_nodes[];
}

export interface GET_RENDEZ_VOUS {
  pRTRendezVous: GET_RENDEZ_VOUS_pRTRendezVous;
}

export interface GET_RENDEZ_VOUSVariables {
  paging?: OffsetPaging | null;
  filter?: PRTRendezVousFilter | null;
  sorting?: PRTRendezVousSort[] | null;
}
