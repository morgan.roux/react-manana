import gql from 'graphql-tag';

export const GET_LABORATOIRES = gql`
  query GET_LABORATOIRES(
    $paging: OffsetPaging
    $filter: LaboratoireFilter
    $sorting: [LaboratoireSort!]
    $idPharmacie: String!
  ) {
    laboratoires(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      id
      nom
      partenariat(idPharmacie: $idPharmacie) {
        id
        dateDebut
        dateFin
        idPharmacie
        type {
          id
          libelle
          code
        }
        statut {
          id
          libelle
          code
        }
      }
      }
    }
  }
`;

export const GET_LABORATOIRE = gql`
  query GET_LABORATOIRE($id: ID!, $idPharmacie: String!) {
    laboratoire(id: $id) {
      id
      nom
      partenariat(idPharmacie: $idPharmacie) {
        id
        dateDebut
        dateFin
        idPharmacie
        type {
          id
          libelle
          code
        }
        statut {
          id
          libelle
          code
        }
      }
      laboratoireSuite {
        id
        adresse
        codePostal
        telephone
        ville
        email
      }
    }
  }
`;
