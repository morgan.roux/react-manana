/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: LaboratoireInfo
// ====================================================

export interface LaboratoireInfo_laboratoireRattachement {
  __typename: "Laboratoire";
  id: string;
  nom: string;
}

export interface LaboratoireInfo_laboratoireRattaches {
  __typename: "Laboratoire";
  id: string;
  nom: string;
}

export interface LaboratoireInfo_laboratoireSuite {
  __typename: "LaboratoireSuite";
  id: string;
  adresse: string;
  codePostal: string | null;
  telephone: string | null;
  ville: string | null;
  email: string | null;
}

export interface LaboratoireInfo {
  __typename: "Laboratoire";
  id: string;
  nom: string;
  idLaboSuite: string | null;
  sortie: number;
  laboratoireRattachement: LaboratoireInfo_laboratoireRattachement | null;
  laboratoireRattaches: LaboratoireInfo_laboratoireRattaches[] | null;
  laboratoireSuite: LaboratoireInfo_laboratoireSuite | null;
  pharmacieInfos: any;
  createdAt: any;
  updatedAt: any;
}
