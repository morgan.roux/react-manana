/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, LaboratoireFilter, LaboratoireSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_LABORATOIRES
// ====================================================

export interface GET_LABORATOIRES_laboratoires_nodes_partenariat_type {
  __typename: "PRTPartenariatType";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_LABORATOIRES_laboratoires_nodes_partenariat_statut {
  __typename: "PRTPartenariatStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_LABORATOIRES_laboratoires_nodes_partenariat {
  __typename: "PRTPartenariat";
  id: string;
  dateDebut: any;
  dateFin: any | null;
  idPharmacie: string;
  type: GET_LABORATOIRES_laboratoires_nodes_partenariat_type;
  statut: GET_LABORATOIRES_laboratoires_nodes_partenariat_statut;
}

export interface GET_LABORATOIRES_laboratoires_nodes {
  __typename: "Laboratoire";
  id: string;
  nom: string;
  partenariat: GET_LABORATOIRES_laboratoires_nodes_partenariat | null;
}

export interface GET_LABORATOIRES_laboratoires {
  __typename: "LaboratoireConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_LABORATOIRES_laboratoires_nodes[];
}

export interface GET_LABORATOIRES {
  laboratoires: GET_LABORATOIRES_laboratoires;
}

export interface GET_LABORATOIRESVariables {
  paging?: OffsetPaging | null;
  filter?: LaboratoireFilter | null;
  sorting?: LaboratoireSort[] | null;
  idPharmacie: string;
}
