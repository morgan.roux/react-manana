/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_LABORATOIRE
// ====================================================

export interface GET_LABORATOIRE_laboratoire_partenariat_type {
  __typename: "PRTPartenariatType";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_LABORATOIRE_laboratoire_partenariat_statut {
  __typename: "PRTPartenariatStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_LABORATOIRE_laboratoire_partenariat {
  __typename: "PRTPartenariat";
  id: string;
  dateDebut: any;
  dateFin: any | null;
  idPharmacie: string;
  type: GET_LABORATOIRE_laboratoire_partenariat_type;
  statut: GET_LABORATOIRE_laboratoire_partenariat_statut;
}

export interface GET_LABORATOIRE_laboratoire_laboratoireSuite {
  __typename: "LaboratoireSuite";
  id: string;
  adresse: string;
  codePostal: string | null;
  telephone: string | null;
  ville: string | null;
  email: string | null;
}

export interface GET_LABORATOIRE_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nom: string;
  partenariat: GET_LABORATOIRE_laboratoire_partenariat | null;
  laboratoireSuite: GET_LABORATOIRE_laboratoire_laboratoireSuite | null;
}

export interface GET_LABORATOIRE {
  laboratoire: GET_LABORATOIRE_laboratoire | null;
}

export interface GET_LABORATOIREVariables {
  id: string;
  idPharmacie: string;
}
