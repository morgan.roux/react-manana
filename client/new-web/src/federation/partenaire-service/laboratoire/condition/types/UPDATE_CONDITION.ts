/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTConditionCommercialeInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_CONDITION
// ====================================================

export interface UPDATE_CONDITION_updateOnePRTConditionCommerciale_type {
  __typename: "PRTConditionCommercialeType";
  id: string;
  libelle: string;
  code: string;
}

export interface UPDATE_CONDITION_updateOnePRTConditionCommerciale_statut {
  __typename: "PRTConditionCommercialeStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface UPDATE_CONDITION_updateOnePRTConditionCommerciale_canal {
  __typename: "Canal";
  id: string;
  libelle: string;
  code: string;
}

export interface UPDATE_CONDITION_updateOnePRTConditionCommerciale_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_CONDITION_updateOnePRTConditionCommerciale {
  __typename: "PRTConditionCommerciale";
  id: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  idCanal: string | null;
  idType: string;
  idStatut: string;
  titre: string;
  dateDebut: any;
  dateFin: any;
  idGroupement: string;
  idPharmacie: string;
  description: string | null;
  type: UPDATE_CONDITION_updateOnePRTConditionCommerciale_type;
  statut: UPDATE_CONDITION_updateOnePRTConditionCommerciale_statut;
  canal: UPDATE_CONDITION_updateOnePRTConditionCommerciale_canal | null;
  fichiers: UPDATE_CONDITION_updateOnePRTConditionCommerciale_fichiers[] | null;
}

export interface UPDATE_CONDITION {
  updateOnePRTConditionCommerciale: UPDATE_CONDITION_updateOnePRTConditionCommerciale;
}

export interface UPDATE_CONDITIONVariables {
  input: PRTConditionCommercialeInput;
  id: string;
}
