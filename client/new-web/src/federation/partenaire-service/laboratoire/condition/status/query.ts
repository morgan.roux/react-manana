import gql from 'graphql-tag';
import { STATUS_CONDITION_INFO } from './fragment';

export const GET_STATUS_CONDITION = gql`
  query GET_STATUS_CONDITION(
    $paging: OffsetPaging
    $filter: PRTConditionCommercialeStatutFilter
    $sorting: [PRTConditionCommercialeStatutSort!]
  ) {
    pRTConditionCommercialeStatuts(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...StatusConditionInfo
      }
    }
  }

  ${STATUS_CONDITION_INFO}
`;
