import gql from 'graphql-tag';

export const CANAL_INFO = gql`
  fragment CanalInfo on Canal {
    id
    libelle
    code
  }
`;
