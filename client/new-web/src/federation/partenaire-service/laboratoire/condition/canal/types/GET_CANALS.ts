/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, CanalFilter, CanalSort } from "./../../../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_CANALS
// ====================================================

export interface GET_CANALS_canals_nodes {
  __typename: "Canal";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_CANALS_canals {
  __typename: "CanalConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_CANALS_canals_nodes[];
}

export interface GET_CANALS {
  canals: GET_CANALS_canals;
}

export interface GET_CANALSVariables {
  paging?: OffsetPaging | null;
  filter?: CanalFilter | null;
  sorting?: CanalSort[] | null;
}
