import gql from 'graphql-tag';
import { CANAL_INFO } from './fragment';

export const GET_CANALS = gql`
  query GET_CANALS($paging: OffsetPaging, $filter: CanalFilter, $sorting: [CanalSort!]) {
    canals(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...CanalInfo
      }
    }
  }

  ${CANAL_INFO}
`;
