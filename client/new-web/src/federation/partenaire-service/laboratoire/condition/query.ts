import gql from 'graphql-tag';
import { CONDITION_INFO } from './fragment';

export const GET_CONDITIONS = gql`
  query GET_CONDITIONS(
    $paging: OffsetPaging
    $filter: PRTConditionCommercialeFilter
    $sorting: [PRTConditionCommercialeSort!]
  ) {
    pRTConditionCommerciales(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...PRTConditionInfo
      }
    }
  }

  ${CONDITION_INFO}
`;

export const GET_ROW_CONDITIONS = gql`
  query GET_ROW_CONDITIONS($filter: PRTConditionCommercialeAggregateFilter) {
    pRTConditionCommercialeAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;
