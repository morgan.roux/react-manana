/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ConditionTypeInfo
// ====================================================

export interface ConditionTypeInfo {
  __typename: "PRTConditionCommercialeType";
  id: string;
  libelle: string;
  code: string;
}
