import gql from 'graphql-tag';


export const FULL_PARTENARIAT_STATUT_INFO = gql`
  fragment PartenariatStatutInfo on PRTPartenariatStatut {
    id
    code
    libelle
  }
`;


export const FULL_PARTENARIAT_TYPE_INFO = gql`
  fragment PartenariatTypeInfo on PRTPartenariatType {
    id
    code
    libelle
  }
`;



export const FULL_PARTENARIAT_INFO = gql`
  fragment PartenariatInfo on PRTPartenariat {
    id
    dateDebut
    dateFin
    type{
      ...PartenariatTypeInfo
    }
    statut{
      ...PartenariatStatutInfo
    }
  }
  ${FULL_PARTENARIAT_STATUT_INFO}
  ${FULL_PARTENARIAT_TYPE_INFO}
`;
