/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PartenariatStatutInfo
// ====================================================

export interface PartenariatStatutInfo {
  __typename: "PRTPartenariatStatut";
  id: string;
  code: string;
  libelle: string;
}
