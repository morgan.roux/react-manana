/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTPartenariatTypeFilter, PRTPartenariatTypeSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_LIST_PARTENARIAT_TYPES
// ====================================================

export interface GET_LIST_PARTENARIAT_TYPES_pRTPartenariatTypes_nodes {
  __typename: "PRTPartenariatType";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_LIST_PARTENARIAT_TYPES_pRTPartenariatTypes {
  __typename: "PRTPartenariatTypeConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_LIST_PARTENARIAT_TYPES_pRTPartenariatTypes_nodes[];
}

export interface GET_LIST_PARTENARIAT_TYPES {
  pRTPartenariatTypes: GET_LIST_PARTENARIAT_TYPES_pRTPartenariatTypes;
}

export interface GET_LIST_PARTENARIAT_TYPESVariables {
  paging?: OffsetPaging | null;
  filter?: PRTPartenariatTypeFilter | null;
  sorting?: PRTPartenariatTypeSort[] | null;
}
