/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PartenariatInfo
// ====================================================

export interface PartenariatInfo_type {
  __typename: "PRTPartenariatType";
  id: string;
  code: string;
  libelle: string;
}

export interface PartenariatInfo_statut {
  __typename: "PRTPartenariatStatut";
  id: string;
  code: string;
  libelle: string;
}

export interface PartenariatInfo {
  __typename: "PRTPartenariat";
  id: string;
  dateDebut: any;
  dateFin: any | null;
  type: PartenariatInfo_type;
  statut: PartenariatInfo_statut;
}
