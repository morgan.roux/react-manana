/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTPartenariatInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_PARTENARIAT
// ====================================================

export interface CREATE_PARTENARIAT_createOnePRTPartenariat_type {
  __typename: "PRTPartenariatType";
  id: string;
  code: string;
  libelle: string;
}

export interface CREATE_PARTENARIAT_createOnePRTPartenariat_statut {
  __typename: "PRTPartenariatStatut";
  id: string;
  code: string;
  libelle: string;
}

export interface CREATE_PARTENARIAT_createOnePRTPartenariat {
  __typename: "PRTPartenariat";
  id: string;
  dateDebut: any;
  dateFin: any | null;
  type: CREATE_PARTENARIAT_createOnePRTPartenariat_type;
  statut: CREATE_PARTENARIAT_createOnePRTPartenariat_statut;
}

export interface CREATE_PARTENARIAT {
  createOnePRTPartenariat: CREATE_PARTENARIAT_createOnePRTPartenariat;
}

export interface CREATE_PARTENARIATVariables {
  input: PRTPartenariatInput;
}
