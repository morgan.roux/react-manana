/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTPartenariatInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_PARTENARIAT
// ====================================================

export interface UPDATE_PARTENARIAT_updateOnePRTPartenariat_type {
  __typename: "PRTPartenariatType";
  id: string;
  code: string;
  libelle: string;
}

export interface UPDATE_PARTENARIAT_updateOnePRTPartenariat_statut {
  __typename: "PRTPartenariatStatut";
  id: string;
  code: string;
  libelle: string;
}

export interface UPDATE_PARTENARIAT_updateOnePRTPartenariat {
  __typename: "PRTPartenariat";
  id: string;
  dateDebut: any;
  dateFin: any | null;
  type: UPDATE_PARTENARIAT_updateOnePRTPartenariat_type;
  statut: UPDATE_PARTENARIAT_updateOnePRTPartenariat_statut;
}

export interface UPDATE_PARTENARIAT {
  updateOnePRTPartenariat: UPDATE_PARTENARIAT_updateOnePRTPartenariat;
}

export interface UPDATE_PARTENARIATVariables {
  update: PRTPartenariatInput;
  id: string;
}
