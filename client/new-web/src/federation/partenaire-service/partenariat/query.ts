import gql from 'graphql-tag';
import { FULL_PARTENARIAT_STATUT_INFO, FULL_PARTENARIAT_TYPE_INFO } from './fragment'

export const GET_LIST_PARTENARIAT_STATUTS = gql`
  query GET_LIST_PARTENARIAT_STATUTS(
    $paging: OffsetPaging
    $filter: PRTPartenariatStatutFilter
    $sorting: [PRTPartenariatStatutSort!]
  ) {
    pRTPartenariatStatuts(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...PartenariatStatutInfo
      }
    }
  }
${FULL_PARTENARIAT_STATUT_INFO}
`

export const GET_LIST_PARTENARIAT_TYPES = gql`
  query GET_LIST_PARTENARIAT_TYPES(
    $paging: OffsetPaging
    $filter: PRTPartenariatTypeFilter
    $sorting: [PRTPartenariatTypeSort!]
  ) {
    pRTPartenariatTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...PartenariatTypeInfo
      }
    }
  }
${FULL_PARTENARIAT_TYPE_INFO}
`
