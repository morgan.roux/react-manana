import gql from 'graphql-tag';
import { FULL_USER_INFO } from '../../iam/user/fragment';

export const FULL_MODELE_OPERATION_MARKETING_INFO = gql`
  fragment ModeleOperationMarketingInfo on PRTPlanMarketingTypeAction {
    id
    jourLancement
    description
    idPlanMarketingType
    idFonction
    idTache
    idImportance
    idGroupement
    idPharmacie
    importance {
      id
      ordre
      libelle
      couleur
    }
    participants {
      ...UserInfo
    }
  }
  ${FULL_USER_INFO}
`;
