import gql from 'graphql-tag';
import { FULL_MODELE_OPERATION_MARKETING_INFO } from './fragment';

export const CREATE_MODELE_OPERATION_MARKETING = gql`
  mutation CREATE_MODELE_OPERATION_MARKETING($input: PRTPlanMarketingTypeActionInput!) {
    createOnePRTPlanMarketingTypeAction(input: $input) {
      ...ModeleOperationMarketingInfo
    }
  }
  ${FULL_MODELE_OPERATION_MARKETING_INFO}
`;

export const UPDATE_MODELE_OPERATION_MARKETING = gql`
  mutation UPDATE_MODELE_OPERATION_MARKETING(
    $input: PRTPlanMarketingTypeActionInput!
    $id: String!
  ) {
    updateOnePRTPlanMarketingTypeAction(input: $input, id: $id) {
      ...ModeleOperationMarketingInfo
    }
  }
  ${FULL_MODELE_OPERATION_MARKETING_INFO}
`;

export const DELETE_MODELE_OPERATION_MARKETING = gql`
  mutation DELETE_MODELE_OPERATION_MARKETING($id: String!) {
    deleteOnePRTPlanMarketingTypeAction(id: $id) {
      ...ModeleOperationMarketingInfo
    }
  }
  ${FULL_MODELE_OPERATION_MARKETING_INFO}
`;
