/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ModeleOperationMarketingInfo
// ====================================================

export interface ModeleOperationMarketingInfo_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface ModeleOperationMarketingInfo_participants_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface ModeleOperationMarketingInfo_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface ModeleOperationMarketingInfo_participants {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: ModeleOperationMarketingInfo_participants_role | null;
  photo: ModeleOperationMarketingInfo_participants_photo | null;
  phoneNumber: string | null;
}

export interface ModeleOperationMarketingInfo {
  __typename: "PRTPlanMarketingTypeAction";
  id: string;
  jourLancement: number;
  description: string;
  idPlanMarketingType: string;
  idFonction: string | null;
  idTache: string | null;
  idImportance: string;
  idGroupement: string;
  idPharmacie: string;
  importance: ModeleOperationMarketingInfo_importance;
  participants: ModeleOperationMarketingInfo_participants[];
}
