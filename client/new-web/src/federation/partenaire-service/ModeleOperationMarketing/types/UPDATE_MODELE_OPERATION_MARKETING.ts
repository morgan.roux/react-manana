/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTPlanMarketingTypeActionInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_MODELE_OPERATION_MARKETING
// ====================================================

export interface UPDATE_MODELE_OPERATION_MARKETING_updateOnePRTPlanMarketingTypeAction_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface UPDATE_MODELE_OPERATION_MARKETING_updateOnePRTPlanMarketingTypeAction_participants_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface UPDATE_MODELE_OPERATION_MARKETING_updateOnePRTPlanMarketingTypeAction_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface UPDATE_MODELE_OPERATION_MARKETING_updateOnePRTPlanMarketingTypeAction_participants {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: UPDATE_MODELE_OPERATION_MARKETING_updateOnePRTPlanMarketingTypeAction_participants_role | null;
  photo: UPDATE_MODELE_OPERATION_MARKETING_updateOnePRTPlanMarketingTypeAction_participants_photo | null;
  phoneNumber: string | null;
}

export interface UPDATE_MODELE_OPERATION_MARKETING_updateOnePRTPlanMarketingTypeAction {
  __typename: "PRTPlanMarketingTypeAction";
  id: string;
  jourLancement: number;
  description: string;
  idPlanMarketingType: string;
  idFonction: string | null;
  idTache: string | null;
  idImportance: string;
  idGroupement: string;
  idPharmacie: string;
  importance: UPDATE_MODELE_OPERATION_MARKETING_updateOnePRTPlanMarketingTypeAction_importance;
  participants: UPDATE_MODELE_OPERATION_MARKETING_updateOnePRTPlanMarketingTypeAction_participants[];
}

export interface UPDATE_MODELE_OPERATION_MARKETING {
  updateOnePRTPlanMarketingTypeAction: UPDATE_MODELE_OPERATION_MARKETING_updateOnePRTPlanMarketingTypeAction;
}

export interface UPDATE_MODELE_OPERATION_MARKETINGVariables {
  input: PRTPlanMarketingTypeActionInput;
  id: string;
}
