/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_MODELE_OPERATION_MARKETING
// ====================================================

export interface DELETE_MODELE_OPERATION_MARKETING_deleteOnePRTPlanMarketingTypeAction_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface DELETE_MODELE_OPERATION_MARKETING_deleteOnePRTPlanMarketingTypeAction_participants_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface DELETE_MODELE_OPERATION_MARKETING_deleteOnePRTPlanMarketingTypeAction_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface DELETE_MODELE_OPERATION_MARKETING_deleteOnePRTPlanMarketingTypeAction_participants {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: DELETE_MODELE_OPERATION_MARKETING_deleteOnePRTPlanMarketingTypeAction_participants_role | null;
  photo: DELETE_MODELE_OPERATION_MARKETING_deleteOnePRTPlanMarketingTypeAction_participants_photo | null;
  phoneNumber: string | null;
}

export interface DELETE_MODELE_OPERATION_MARKETING_deleteOnePRTPlanMarketingTypeAction {
  __typename: "PRTPlanMarketingTypeAction";
  id: string;
  jourLancement: number;
  description: string;
  idPlanMarketingType: string;
  idFonction: string | null;
  idTache: string | null;
  idImportance: string;
  idGroupement: string;
  idPharmacie: string;
  importance: DELETE_MODELE_OPERATION_MARKETING_deleteOnePRTPlanMarketingTypeAction_importance;
  participants: DELETE_MODELE_OPERATION_MARKETING_deleteOnePRTPlanMarketingTypeAction_participants[];
}

export interface DELETE_MODELE_OPERATION_MARKETING {
  deleteOnePRTPlanMarketingTypeAction: DELETE_MODELE_OPERATION_MARKETING_deleteOnePRTPlanMarketingTypeAction;
}

export interface DELETE_MODELE_OPERATION_MARKETINGVariables {
  id: string;
}
