/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTPlanMarketingTypeActionFilter, OffsetPaging, PRTPlanMarketingTypeActionSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_MODELE_OPERATION_MARKETINGS
// ====================================================

export interface GET_MODELE_OPERATION_MARKETINGS_pRTPlanMarketingTypeActions_nodes_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_MODELE_OPERATION_MARKETINGS_pRTPlanMarketingTypeActions_nodes_participants_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface GET_MODELE_OPERATION_MARKETINGS_pRTPlanMarketingTypeActions_nodes_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface GET_MODELE_OPERATION_MARKETINGS_pRTPlanMarketingTypeActions_nodes_participants {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: GET_MODELE_OPERATION_MARKETINGS_pRTPlanMarketingTypeActions_nodes_participants_role | null;
  photo: GET_MODELE_OPERATION_MARKETINGS_pRTPlanMarketingTypeActions_nodes_participants_photo | null;
  phoneNumber: string | null;
}

export interface GET_MODELE_OPERATION_MARKETINGS_pRTPlanMarketingTypeActions_nodes {
  __typename: "PRTPlanMarketingTypeAction";
  id: string;
  jourLancement: number;
  description: string;
  idPlanMarketingType: string;
  idFonction: string | null;
  idTache: string | null;
  idImportance: string;
  idGroupement: string;
  idPharmacie: string;
  importance: GET_MODELE_OPERATION_MARKETINGS_pRTPlanMarketingTypeActions_nodes_importance;
  participants: GET_MODELE_OPERATION_MARKETINGS_pRTPlanMarketingTypeActions_nodes_participants[];
}

export interface GET_MODELE_OPERATION_MARKETINGS_pRTPlanMarketingTypeActions {
  __typename: "PRTPlanMarketingTypeActionConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_MODELE_OPERATION_MARKETINGS_pRTPlanMarketingTypeActions_nodes[];
}

export interface GET_MODELE_OPERATION_MARKETINGS {
  pRTPlanMarketingTypeActions: GET_MODELE_OPERATION_MARKETINGS_pRTPlanMarketingTypeActions;
}

export interface GET_MODELE_OPERATION_MARKETINGSVariables {
  filter?: PRTPlanMarketingTypeActionFilter | null;
  paging?: OffsetPaging | null;
  sorting?: PRTPlanMarketingTypeActionSort[] | null;
}
