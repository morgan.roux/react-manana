/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_PRT_COMMANDE
// ====================================================

export interface DELETE_PRT_COMMANDE_deleteOnePRTCommande {
  __typename: "PRTCommandeDeleteResponse";
  id: string | null;
}

export interface DELETE_PRT_COMMANDE {
  deleteOnePRTCommande: DELETE_PRT_COMMANDE_deleteOnePRTCommande;
}

export interface DELETE_PRT_COMMANDEVariables {
  input: DeleteOneInput;
}
