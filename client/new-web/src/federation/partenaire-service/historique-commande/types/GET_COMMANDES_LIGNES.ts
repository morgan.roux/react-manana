/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_COMMANDES_LIGNES
// ====================================================

export interface GET_COMMANDES_LIGNES_getCommandeLignesByIdCommande_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
}

export interface GET_COMMANDES_LIGNES_getCommandeLignesByIdCommande {
  __typename: "PRTCommandeLigne";
  id: string;
  optimiser: number | null;
  quantiteCommandee: number | null;
  quantiteLivree: number | null;
  uniteGratuite: number | null;
  prixBaseHT: number | null;
  remiseLigne: number | null;
  remiseGamme: number | null;
  prixNetUnitaireHT: number | null;
  prixTotalHT: number | null;
  idStatutLigne: string | null;
  idCommande: string;
  idProduitCanal: string | null;
  createdAt: any;
  updatedAt: any;
  produit: GET_COMMANDES_LIGNES_getCommandeLignesByIdCommande_produit | null;
}

export interface GET_COMMANDES_LIGNES {
  getCommandeLignesByIdCommande: GET_COMMANDES_LIGNES_getCommandeLignesByIdCommande[] | null;
}

export interface GET_COMMANDES_LIGNESVariables {
  idCommande: string;
}
