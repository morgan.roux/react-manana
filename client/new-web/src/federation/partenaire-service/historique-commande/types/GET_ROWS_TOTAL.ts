/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTCommandeAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_ROWS_TOTAL
// ====================================================

export interface GET_ROWS_TOTAL_pRTCommandeAggregate_count {
  __typename: "PRTCommandeCountAggregate";
  id: number | null;
}

export interface GET_ROWS_TOTAL_pRTCommandeAggregate {
  __typename: "PRTCommandeAggregateResponse";
  count: GET_ROWS_TOTAL_pRTCommandeAggregate_count | null;
}

export interface GET_ROWS_TOTAL {
  pRTCommandeAggregate: GET_ROWS_TOTAL_pRTCommandeAggregate;
}

export interface GET_ROWS_TOTALVariables {
  filter?: PRTCommandeAggregateFilter | null;
}
