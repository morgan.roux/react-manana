/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: COMMANDE_FOR_DOCUMENT_TOTAL_ROWS
// ====================================================

export interface COMMANDE_FOR_DOCUMENT_TOTAL_ROWS {
  getCommandeForDocumentRowsTotal: number;
}

export interface COMMANDE_FOR_DOCUMENT_TOTAL_ROWSVariables {
  idDocument: string;
  searchText?: string | null;
  idTypeAssocie: string;
}
