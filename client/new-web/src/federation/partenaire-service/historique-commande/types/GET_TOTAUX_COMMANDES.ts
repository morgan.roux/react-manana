/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_TOTAUX_COMMANDES
// ====================================================

export interface GET_TOTAUX_COMMANDES_getTotauxCommandes {
  __typename: "PRTTotalCommande";
  totalHT: string | null;
  totalTTC: string | null;
  totalTVA: string | null;
}

export interface GET_TOTAUX_COMMANDES {
  getTotauxCommandes: GET_TOTAUX_COMMANDES_getTotauxCommandes | null;
}

export interface GET_TOTAUX_COMMANDESVariables {
  idPharmacie: string;
  idLaboratoire: string;
}
