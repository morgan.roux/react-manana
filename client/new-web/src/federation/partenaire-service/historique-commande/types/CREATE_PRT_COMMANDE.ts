/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { CreateOnePRTCommandeInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_PRT_COMMANDE
// ====================================================

export interface CREATE_PRT_COMMANDE_createOnePRTCommande {
  __typename: "PRTCommande";
  id: string;
  dateCommande: any | null;
  nbrRef: number | null;
  quantite: number | null;
  uniteGratuite: number | null;
  prixBaseTotalHT: number | null;
  valeurRemiseTotal: number | null;
  prixNetTotalHT: number | null;
  fraisPort: number | null;
  ValeurFrancoPort: number | null;
  remiseGlobale: number | null;
  commentaireInterne: string | null;
  commentaireExterne: string | null;
  pathFile: string | null;
  idOperation: string | null;
  idUser: string | null;
  idCommandeType: string | null;
  idPharmacie: string | null;
  idPublicite: string | null;
  statut: string | null;
  idCanal: string | null;
  idGroupement: string | null;
  prixNetTotalTTC: number | null;
  totalTVA: number | null;
  source: string | null;
}

export interface CREATE_PRT_COMMANDE {
  createOnePRTCommande: CREATE_PRT_COMMANDE_createOnePRTCommande;
}

export interface CREATE_PRT_COMMANDEVariables {
  input: CreateOnePRTCommandeInput;
}
