/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: CommandeLigneInfo
// ====================================================

export interface CommandeLigneInfo_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
}

export interface CommandeLigneInfo {
  __typename: "PRTCommandeLigne";
  id: string;
  optimiser: number | null;
  quantiteCommandee: number | null;
  quantiteLivree: number | null;
  uniteGratuite: number | null;
  prixBaseHT: number | null;
  remiseLigne: number | null;
  remiseGamme: number | null;
  prixNetUnitaireHT: number | null;
  prixTotalHT: number | null;
  idStatutLigne: string | null;
  idCommande: string;
  idProduitCanal: string | null;
  createdAt: any;
  updatedAt: any;
  produit: CommandeLigneInfo_produit | null;
}
