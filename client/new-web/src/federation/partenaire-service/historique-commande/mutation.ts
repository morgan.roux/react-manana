import gql from 'graphql-tag';
import { HISTORIQUE_COMMANDE_INFO } from './fragment';

export const UPDATE_JOINDRE_COMMANDE_FACTURE = gql`
  mutation JOINDRE_COMMANDE_FACTURE($input: PRTCommandeDocumentInput!) {
    joindreCommandeFacture(input: $input) 
  }
`;

export const CHANGE_PRT_COMMANDE_STATUT = gql`
  mutation CHANGE_PRT_COMMANDE_STATUT($id: String!, $statut: String!) {
    changePRTCommandeStatut(id: $id, statut: $statut) {
      ...HistoriqueCommandeInfo
    }
  }
  ${HISTORIQUE_COMMANDE_INFO}
`;

export const CREATE_PRT_COMMANDE = gql`
  mutation CREATE_PRT_COMMANDE($input: CreateOnePRTCommandeInput!) {
    createOnePRTCommande(input: $input) {
      ...HistoriqueCommandeInfo
    }
  }
  ${HISTORIQUE_COMMANDE_INFO}
`;

export const UPDATE_PRT_COMMANDE = gql`
  mutation UPDATE_PRT_COMMANDE($input: UpdateOnePRTCommandeInput!) {
    updateOnePRTCommande(input: $input) {
      ...HistoriqueCommandeInfo
    }
  }
  ${HISTORIQUE_COMMANDE_INFO}
`;

export const DELETE_PRT_COMMANDE = gql`
  mutation DELETE_PRT_COMMANDE($input: DeleteOneInput!) {
    deleteOnePRTCommande(input: $input) {
      id
    }
  }
`;
