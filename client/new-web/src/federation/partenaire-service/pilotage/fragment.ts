import gql from 'graphql-tag';
import { PRESTATION_TYPE_INFO } from '../remuneration/fragment';

export const REMUNERATION_MENSUELLE_INFO = gql`
  fragment RemunerationMensuelleInfo on PRTRemunerationMensuelle {
    data {
      prestation {
        id
        code
        libelle
        couleur
      }
      totalRemunerationPrevu
      totalRemunerationReglement
      differenceTotalRemuneration
      month
    }
  }
`;

export const REMUNERATION_SUIVI_OPERATIONNEL_INFO = gql`
  fragment RemunerationSuiviOperationnelInfo on PRTRemunerationSuiviOperationnel {
    types {
      prestation {
        id
        code
        libelle
        couleur
      }
      totalMontantPrevuPrestation
      totalMontantReglementPrestation
      pourcentagePrestation
      idPartenaireTypeAssocie
      nomPartenaire
    }
    totalMontantPrevues
    totalMontantReglements
    pourcentageReelReglement
  }
`;
