/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTRemunerationEvolutionInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_REMUNERATION_SUIVI_OPERATIONNEL
// ====================================================

export interface GET_REMUNERATION_SUIVI_OPERATIONNEL_remunerationSuiviOperationnel_types_prestation {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface GET_REMUNERATION_SUIVI_OPERATIONNEL_remunerationSuiviOperationnel_types {
  __typename: "PRTRemunerationSuiviOperationnelMontant";
  prestation: GET_REMUNERATION_SUIVI_OPERATIONNEL_remunerationSuiviOperationnel_types_prestation;
  totalMontantPrevuPrestation: number;
  totalMontantReglementPrestation: number;
  pourcentagePrestation: number;
  idPartenaireTypeAssocie: string;
  nomPartenaire: string;
}

export interface GET_REMUNERATION_SUIVI_OPERATIONNEL_remunerationSuiviOperationnel {
  __typename: "PRTRemunerationSuiviOperationnel";
  types: GET_REMUNERATION_SUIVI_OPERATIONNEL_remunerationSuiviOperationnel_types[];
  totalMontantPrevues: number;
  totalMontantReglements: number;
  pourcentageReelReglement: number;
}

export interface GET_REMUNERATION_SUIVI_OPERATIONNEL {
  remunerationSuiviOperationnel: GET_REMUNERATION_SUIVI_OPERATIONNEL_remunerationSuiviOperationnel;
}

export interface GET_REMUNERATION_SUIVI_OPERATIONNELVariables {
  input: PRTRemunerationEvolutionInput;
}
