/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: RemunerationSuiviOperationnelInfo
// ====================================================

export interface RemunerationSuiviOperationnelInfo_types_prestation {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface RemunerationSuiviOperationnelInfo_types {
  __typename: "PRTRemunerationSuiviOperationnelMontant";
  prestation: RemunerationSuiviOperationnelInfo_types_prestation;
  totalMontantPrevuPrestation: number;
  totalMontantReglementPrestation: number;
  pourcentagePrestation: number;
  idPartenaireTypeAssocie: string;
  nomPartenaire: string;
}

export interface RemunerationSuiviOperationnelInfo {
  __typename: "PRTRemunerationSuiviOperationnel";
  types: RemunerationSuiviOperationnelInfo_types[];
  totalMontantPrevues: number;
  totalMontantReglements: number;
  pourcentageReelReglement: number;
}
