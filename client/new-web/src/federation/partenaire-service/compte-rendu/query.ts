import gql from 'graphql-tag';
import { CONDITION_INFO } from '../laboratoire/condition/fragment';
import { FULL_PLAN_MARKETING_INFO } from '../planMarketing/fragment';
import { FULL_COMPTE_RENDU_INFO } from './fragment';

export const GET_PRT_COMPTE_RENDUS = gql`
  query PRT_COMPTE_RENDUS(
    $paging: OffsetPaging
    $filter: PRTCompteRenduFilter
    $sorting: [PRTCompteRenduSort!]
  ) {
    pRTCompteRendus(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...PRTCompteRenduInfo
      }
    }
  }

  ${FULL_COMPTE_RENDU_INFO}
`;

export const GET_PRT_COMPTE_RENDU_AGGREGATES = gql`
  query PRT_COMPTE_RENDU_AGGREGATES($filter: PRTCompteRenduAggregateFilter) {
    pRTCompteRenduAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;

export const GET_PRT_COMPTE_RENDU_CONDITION_COMMERICALE_AGGREGATE = gql`
  query PRT_COMPTE_RENDU_CONDITION_COMMERCIALE_AGGREGATE(
    $filter: PRTCompteRenduConditionCommercialeAggregateFilter
  ) {
    pRTCompteRenduConditionCommercialeAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;

export const GET_PRT_COMPTE_RENDU_PLAN_MARKETING_AGGREGATE = gql`
  query PRT_COMPTE_RENDU_PLAN_MARKETING_AGGREGATE(
    $filter: PRTCompteRenduPlanMarketingAggregateFilter
  ) {
    pRTCompteRenduPlanMarketingAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;

export const GET_PRT_PLAN_MARKETING_NON_RATTACHE = gql`
  query PRT_PLAN_MARKETING_NON_RATTACHE($input: PRTCompteRenduPlanMarketingInput!) {
    compteRenduPlanMarketing(input: $input) {
      ...PRTPlanMarketingInfo
    }
  }
  ${FULL_PLAN_MARKETING_INFO}
`;

export const GET_PRT_CONDITION_COMMERCIALE_NON_RATTACHE = gql`
  query PRT_CONDITION_COMMERCIALE_NON_RATTACHE($input: PRTCompteRenduConditionCommercialeInput!) {
    compteRenduConditionCommerciale(input: $input) {
      ...PRTConditionInfo
    }
  }
  ${CONDITION_INFO}
`;
