/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTCompteRenduConditionCommercialeInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: PRT_CONDITION_COMMERCIALE_NON_RATTACHE
// ====================================================

export interface PRT_CONDITION_COMMERCIALE_NON_RATTACHE_compteRenduConditionCommerciale_type {
  __typename: "PRTConditionCommercialeType";
  id: string;
  libelle: string;
  code: string;
}

export interface PRT_CONDITION_COMMERCIALE_NON_RATTACHE_compteRenduConditionCommerciale_statut {
  __typename: "PRTConditionCommercialeStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface PRT_CONDITION_COMMERCIALE_NON_RATTACHE_compteRenduConditionCommerciale_canal {
  __typename: "Canal";
  id: string;
  libelle: string;
  code: string;
}

export interface PRT_CONDITION_COMMERCIALE_NON_RATTACHE_compteRenduConditionCommerciale_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface PRT_CONDITION_COMMERCIALE_NON_RATTACHE_compteRenduConditionCommerciale {
  __typename: "PRTConditionCommerciale";
  id: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  idCanal: string | null;
  idType: string;
  idStatut: string;
  titre: string;
  dateDebut: any;
  dateFin: any;
  idGroupement: string;
  idPharmacie: string;
  description: string | null;
  type: PRT_CONDITION_COMMERCIALE_NON_RATTACHE_compteRenduConditionCommerciale_type;
  statut: PRT_CONDITION_COMMERCIALE_NON_RATTACHE_compteRenduConditionCommerciale_statut;
  canal: PRT_CONDITION_COMMERCIALE_NON_RATTACHE_compteRenduConditionCommerciale_canal | null;
  fichiers: PRT_CONDITION_COMMERCIALE_NON_RATTACHE_compteRenduConditionCommerciale_fichiers[] | null;
}

export interface PRT_CONDITION_COMMERCIALE_NON_RATTACHE {
  compteRenduConditionCommerciale: PRT_CONDITION_COMMERCIALE_NON_RATTACHE_compteRenduConditionCommerciale[] | null;
}

export interface PRT_CONDITION_COMMERCIALE_NON_RATTACHEVariables {
  input: PRTCompteRenduConditionCommercialeInput;
}
