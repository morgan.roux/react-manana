/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_ONE_PRT_COMPTE_RENDU
// ====================================================

export interface DELETE_ONE_PRT_COMPTE_RENDU_deleteOnePRTCompteRendu {
  __typename: "PRTCompteRendu";
  id: string;
}

export interface DELETE_ONE_PRT_COMPTE_RENDU {
  deleteOnePRTCompteRendu: DELETE_ONE_PRT_COMPTE_RENDU_deleteOnePRTCompteRendu;
}

export interface DELETE_ONE_PRT_COMPTE_RENDUVariables {
  id: string;
}
