/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTCompteRenduPlanMarketingInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: PRT_PLAN_MARKETING_NON_RATTACHE
// ====================================================

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_type {
  __typename: "PRTPlanMarketingType";
  id: string;
  libelle: string;
  code: string;
  couleur: string | null;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_statut {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_miseAvants {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_actions_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_actions_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_actions {
  __typename: "TodoAction";
  id: string;
  ordre: number;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idActionParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_actions_importance | null;
  urgence: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_actions_urgence | null;
  nbComment: number | null;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing {
  __typename: "PRTPlanMarketing";
  id: string;
  titre: string;
  description: string | null;
  dateDebut: any;
  dateFin: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idGroupement: string;
  idPharmacie: string;
  type: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_type;
  statut: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_statut;
  fichiers: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_fichiers[] | null;
  miseAvants: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_miseAvants[] | null;
  actions: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_actions[] | null;
  produits: string[] | null;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE {
  compteRenduPlanMarketing: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing[] | null;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHEVariables {
  input: PRTCompteRenduPlanMarketingInput;
}
