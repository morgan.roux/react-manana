/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTCompteRenduAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: PRT_COMPTE_RENDU_AGGREGATES
// ====================================================

export interface PRT_COMPTE_RENDU_AGGREGATES_pRTCompteRenduAggregate_count {
  __typename: "PRTCompteRenduCountAggregate";
  id: number | null;
}

export interface PRT_COMPTE_RENDU_AGGREGATES_pRTCompteRenduAggregate {
  __typename: "PRTCompteRenduAggregateResponse";
  count: PRT_COMPTE_RENDU_AGGREGATES_pRTCompteRenduAggregate_count | null;
}

export interface PRT_COMPTE_RENDU_AGGREGATES {
  pRTCompteRenduAggregate: PRT_COMPTE_RENDU_AGGREGATES_pRTCompteRenduAggregate;
}

export interface PRT_COMPTE_RENDU_AGGREGATESVariables {
  filter?: PRTCompteRenduAggregateFilter | null;
}
