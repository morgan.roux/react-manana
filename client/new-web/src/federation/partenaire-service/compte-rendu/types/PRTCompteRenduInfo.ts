/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PRTCompteRenduInfo
// ====================================================

export interface PRTCompteRenduInfo_collaborateurs_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PRTCompteRenduInfo_collaborateurs_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface PRTCompteRenduInfo_collaborateurs {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: PRTCompteRenduInfo_collaborateurs_role | null;
  photo: PRTCompteRenduInfo_collaborateurs_photo | null;
  phoneNumber: string | null;
}

export interface PRTCompteRenduInfo_responsables_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface PRTCompteRenduInfo_responsables_contact {
  __typename: "ContactType";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
  whatsappMobProf: string | null;
  whatsappMobPerso: string | null;
  compteSkypeProf: string | null;
  compteSkypePerso: string | null;
  urlLinkedInProf: string | null;
  urlLinkedInPerso: string | null;
  urlTwitterProf: string | null;
  urlTwitterPerso: string | null;
  urlFacebookProf: string | null;
  urlFacebookPerso: string | null;
  codeMaj: string | null;
  idUserCreation: string | null;
  idUserModification: string | null;
}

export interface PRTCompteRenduInfo_responsables {
  __typename: "PRTContact";
  id: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  fonction: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  photo: PRTCompteRenduInfo_responsables_photo | null;
  contact: PRTCompteRenduInfo_responsables_contact | null;
}

export interface PRTCompteRenduInfo_suiviOperationnels_contacts_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface PRTCompteRenduInfo_suiviOperationnels_contacts_contact {
  __typename: "ContactType";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
  whatsappMobProf: string | null;
  whatsappMobPerso: string | null;
  compteSkypeProf: string | null;
  compteSkypePerso: string | null;
  urlLinkedInProf: string | null;
  urlLinkedInPerso: string | null;
  urlTwitterProf: string | null;
  urlTwitterPerso: string | null;
  urlFacebookProf: string | null;
  urlFacebookPerso: string | null;
  codeMaj: string | null;
  idUserCreation: string | null;
  idUserModification: string | null;
}

export interface PRTCompteRenduInfo_suiviOperationnels_contacts {
  __typename: "PRTContact";
  id: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  fonction: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  photo: PRTCompteRenduInfo_suiviOperationnels_contacts_photo | null;
  contact: PRTCompteRenduInfo_suiviOperationnels_contacts_contact | null;
}

export interface PRTCompteRenduInfo_suiviOperationnels_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface PRTCompteRenduInfo_suiviOperationnels_participants_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PRTCompteRenduInfo_suiviOperationnels_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface PRTCompteRenduInfo_suiviOperationnels_participants {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: PRTCompteRenduInfo_suiviOperationnels_participants_role | null;
  photo: PRTCompteRenduInfo_suiviOperationnels_participants_photo | null;
  phoneNumber: string | null;
}

export interface PRTCompteRenduInfo_suiviOperationnels_type {
  __typename: "PRTSuiviOperationnelType";
  id: string;
  libelle: string;
  code: string;
}

export interface PRTCompteRenduInfo_suiviOperationnels_statut {
  __typename: "PRTSuiviOperationnelStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface PRTCompteRenduInfo_suiviOperationnels_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface PRTCompteRenduInfo_suiviOperationnels {
  __typename: "PRTSuiviOperationnel";
  id: string;
  titre: string;
  description: string | null;
  dateHeure: any;
  createdBy: string;
  partenaireType: string;
  idTypeAssocie: string;
  idGroupement: string;
  montant: number;
  idImportance: string | null;
  idTache: string | null;
  idFonction: string | null;
  contacts: PRTCompteRenduInfo_suiviOperationnels_contacts[] | null;
  importance: PRTCompteRenduInfo_suiviOperationnels_importance | null;
  idPharmacie: string;
  participants: PRTCompteRenduInfo_suiviOperationnels_participants[];
  type: PRTCompteRenduInfo_suiviOperationnels_type;
  statut: PRTCompteRenduInfo_suiviOperationnels_statut;
  fichiers: PRTCompteRenduInfo_suiviOperationnels_fichiers[] | null;
  nombreCommentaires: number | null;
  nombreReaction: number | null;
}

export interface PRTCompteRenduInfo_planMarketings_type {
  __typename: "PRTPlanMarketingType";
  id: string;
  libelle: string;
  code: string;
  couleur: string | null;
}

export interface PRTCompteRenduInfo_planMarketings_statut {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface PRTCompteRenduInfo_planMarketings_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface PRTCompteRenduInfo_planMarketings_miseAvants {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
}

export interface PRTCompteRenduInfo_planMarketings_actions_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface PRTCompteRenduInfo_planMarketings_actions_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface PRTCompteRenduInfo_planMarketings_actions {
  __typename: "TodoAction";
  id: string;
  ordre: number;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idActionParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: PRTCompteRenduInfo_planMarketings_actions_importance | null;
  urgence: PRTCompteRenduInfo_planMarketings_actions_urgence | null;
  nbComment: number | null;
}

export interface PRTCompteRenduInfo_planMarketings {
  __typename: "PRTPlanMarketing";
  id: string;
  titre: string;
  description: string | null;
  dateDebut: any;
  dateFin: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idGroupement: string;
  idPharmacie: string;
  type: PRTCompteRenduInfo_planMarketings_type;
  statut: PRTCompteRenduInfo_planMarketings_statut;
  fichiers: PRTCompteRenduInfo_planMarketings_fichiers[] | null;
  miseAvants: PRTCompteRenduInfo_planMarketings_miseAvants[] | null;
  actions: PRTCompteRenduInfo_planMarketings_actions[] | null;
  produits: string[] | null;
}

export interface PRTCompteRenduInfo_conditionsCommerciales_type {
  __typename: "PRTConditionCommercialeType";
  id: string;
  libelle: string;
  code: string;
}

export interface PRTCompteRenduInfo_conditionsCommerciales_statut {
  __typename: "PRTConditionCommercialeStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface PRTCompteRenduInfo_conditionsCommerciales_canal {
  __typename: "Canal";
  id: string;
  libelle: string;
  code: string;
}

export interface PRTCompteRenduInfo_conditionsCommerciales_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface PRTCompteRenduInfo_conditionsCommerciales {
  __typename: "PRTConditionCommerciale";
  id: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  idCanal: string | null;
  idType: string;
  idStatut: string;
  titre: string;
  dateDebut: any;
  dateFin: any;
  idGroupement: string;
  idPharmacie: string;
  description: string | null;
  type: PRTCompteRenduInfo_conditionsCommerciales_type;
  statut: PRTCompteRenduInfo_conditionsCommerciales_statut;
  canal: PRTCompteRenduInfo_conditionsCommerciales_canal | null;
  fichiers: PRTCompteRenduInfo_conditionsCommerciales_fichiers[] | null;
}

export interface PRTCompteRenduInfo {
  __typename: "PRTCompteRendu";
  id: string;
  titre: string;
  remiseEchantillon: boolean;
  gestionPerime: string | null;
  rapportVisite: string | null;
  conclusion: string | null;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  createdAt: any;
  collaborateurs: PRTCompteRenduInfo_collaborateurs[] | null;
  responsables: PRTCompteRenduInfo_responsables[] | null;
  suiviOperationnels: PRTCompteRenduInfo_suiviOperationnels[] | null;
  planMarketings: PRTCompteRenduInfo_planMarketings[] | null;
  conditionsCommerciales: PRTCompteRenduInfo_conditionsCommerciales[] | null;
}
