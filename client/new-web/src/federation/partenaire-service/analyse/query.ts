import gql from 'graphql-tag';
import { FULL_ANALYSE_INFO, FULL_ANALYSE_TYPE_INFO} from './fragment';

export const GET_ANALYSES = gql`
  query GET_ANALYSES(
    $paging: OffsetPaging
    $filter: PRTAnalyseFilter
    $sorting: [PRTAnalyseSort!]
  ) {
    pRTAnalyses(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...PRTAnalyseInfo
      }
    }
  }

  ${FULL_ANALYSE_INFO}
`;

export const GET_ANALYSE_AGGREGATES = gql`
  query GET_ANALYSE_AGGREGATES($filter: PRTAnalyseAggregateFilter) {
    pRTAnalyseAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;

export const GET_LIST_ANALYSE_TYPES = gql`
  query GET_LIST_ANALYSE_TYPES(
    $paging: OffsetPaging
    $filter: PRTAnalyseTypeFilter
    $sorting: [PRTAnalyseTypeSort!]
  ) {
    pRTAnalyseTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...PRTAnalyseTypeInfo
      }
    }
  }
${FULL_ANALYSE_TYPE_INFO}
`

