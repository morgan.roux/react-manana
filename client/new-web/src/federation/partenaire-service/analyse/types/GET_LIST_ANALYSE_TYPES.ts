/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTAnalyseTypeFilter, PRTAnalyseTypeSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_LIST_ANALYSE_TYPES
// ====================================================

export interface GET_LIST_ANALYSE_TYPES_pRTAnalyseTypes_nodes {
  __typename: "PRTAnalyseType";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_LIST_ANALYSE_TYPES_pRTAnalyseTypes {
  __typename: "PRTAnalyseTypeConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_LIST_ANALYSE_TYPES_pRTAnalyseTypes_nodes[];
}

export interface GET_LIST_ANALYSE_TYPES {
  pRTAnalyseTypes: GET_LIST_ANALYSE_TYPES_pRTAnalyseTypes;
}

export interface GET_LIST_ANALYSE_TYPESVariables {
  paging?: OffsetPaging | null;
  filter?: PRTAnalyseTypeFilter | null;
  sorting?: PRTAnalyseTypeSort[] | null;
}
