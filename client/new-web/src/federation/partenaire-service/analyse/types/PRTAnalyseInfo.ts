/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PRTAnalyseInfo
// ====================================================

export interface PRTAnalyseInfo_fichiers {
  __typename: "Fichier";
  id: string;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface PRTAnalyseInfo_partenaireTypeAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  nom: string;
}

export interface PRTAnalyseInfo_partenaireTypeAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export type PRTAnalyseInfo_partenaireTypeAssocie = PRTAnalyseInfo_partenaireTypeAssocie_Laboratoire | PRTAnalyseInfo_partenaireTypeAssocie_PrestataireService;

export interface PRTAnalyseInfo_type {
  __typename: "PRTAnalyseType";
  id: string;
  libelle: string;
  code: string;
}

export interface PRTAnalyseInfo {
  __typename: "PRTAnalyse";
  id: string;
  titre: string;
  dateDebut: any;
  dateFin: any;
  dateChargement: any;
  partenaireType: string;
  fichiers: PRTAnalyseInfo_fichiers[] | null;
  partenaireTypeAssocie: PRTAnalyseInfo_partenaireTypeAssocie | null;
  type: PRTAnalyseInfo_type;
}
