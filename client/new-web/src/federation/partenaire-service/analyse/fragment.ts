import gql from 'graphql-tag';

export const FULL_ANALYSE_INFO = gql`
  fragment PRTAnalyseInfo on PRTAnalyse {
    id
    titre
    dateDebut
    dateFin
    dateChargement
    partenaireType
    fichiers{
      id
      nomOriginal
      publicUrl
    }
    partenaireTypeAssocie {
      ... on Laboratoire {
        id
        nom
      }
      ... on PrestataireService {
        id
        nom
      }
    }
    type {
      id
      libelle
      code
    }
  }
`;

export const FULL_ANALYSE_TYPE_INFO = gql`
  fragment PRTAnalyseTypeInfo on PRTAnalyseType {
    id
    code
    libelle
  }
`;
