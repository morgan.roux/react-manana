import gql from 'graphql-tag';
import { FULL_ANALYSE_INFO } from './fragment';

export const CREATE_ANALYSE = gql`
  mutation CREATE_ANALYSE($input: PRTAnalyseInput!) {
    createOnePRTAnalyse(input: $input) {
      ...PRTAnalyseInfo
    }
  }
  ${FULL_ANALYSE_INFO}
`;

export const UPDATE_ANALYSE = gql`
  mutation UPDATE_ANALYSE($id: String!, $input: PRTAnalyseInput!) {
    updateOnePRTAnalyse(id: $id, input: $input) {
      ...PRTAnalyseInfo
    }
  }
  ${FULL_ANALYSE_INFO}
`;

export const DELETE_ONE_ANALYSE = gql`
  mutation DELETE_ONE_ANALYSE($input: DeleteOneInput!) {
    deleteOnePRTAnalyse(input: $input) {
      id
    }
  }
`;
