/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: MiseAvantInfo
// ====================================================

export interface MiseAvantInfo {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
  idPharmacie: string | null;
}
