/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_MISE_AVANT
// ====================================================

export interface DELETE_MISE_AVANT_deleteOnePRTMiseAvant {
  __typename: "PRTMiseAvantDeleteResponse";
  id: string | null;
}

export interface DELETE_MISE_AVANT {
  deleteOnePRTMiseAvant: DELETE_MISE_AVANT_deleteOnePRTMiseAvant;
}

export interface DELETE_MISE_AVANTVariables {
  input: DeleteOneInput;
}
