/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTMiseAvantAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_MISE_AVANTS_AGGREGATES
// ====================================================

export interface GET_MISE_AVANTS_AGGREGATES_pRTMiseAvantAggregate_count {
  __typename: "PRTMiseAvantCountAggregate";
  id: number | null;
}

export interface GET_MISE_AVANTS_AGGREGATES_pRTMiseAvantAggregate {
  __typename: "PRTMiseAvantAggregateResponse";
  count: GET_MISE_AVANTS_AGGREGATES_pRTMiseAvantAggregate_count | null;
}

export interface GET_MISE_AVANTS_AGGREGATES {
  pRTMiseAvantAggregate: GET_MISE_AVANTS_AGGREGATES_pRTMiseAvantAggregate;
}

export interface GET_MISE_AVANTS_AGGREGATESVariables {
  filter: PRTMiseAvantAggregateFilter;
}
