import gql from 'graphql-tag';
import { FULL_MISE_AVANT_INFO} from './fragment';

export const GET_MISE_AVANTS = gql`
query GET_MISE_AVANTS($filter:PRTMiseAvantFilter  , $paging:OffsetPaging , $sorting:[PRTMiseAvantSort!] ){
pRTMiseAvants(paging: $paging, filter: $filter, sorting: $sorting){
    nodes{
    ...MiseAvantInfo
    }
}
}
${FULL_MISE_AVANT_INFO}
`;


export const GET_MISE_AVANTS_AGGREGATES = gql`
query GET_MISE_AVANTS_AGGREGATES($filter: PRTMiseAvantAggregateFilter!){
    pRTMiseAvantAggregate(filter: $filter){
        count{
            id
        }
    }
}
`;