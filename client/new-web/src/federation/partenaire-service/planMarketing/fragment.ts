import gql from 'graphql-tag';
import { FULL_ACTION } from '../../basis/todo/action/fragment';

export const FULL_PLAN_MARKETING_INFO = gql`
  fragment PRTPlanMarketingInfo on PRTPlanMarketing {
    id
    titre
    description
    dateDebut
    dateFin
    partenaireType
    idPartenaireTypeAssocie
    idGroupement
    idPharmacie
    type {
      id
      libelle
      code
      couleur
    }
    statut {
      id
      libelle
      code
    }
    fichiers {
      id
      chemin
      nomOriginal
      type
      publicUrl
    }
    miseAvants {
      id
      code
      libelle
    }
    actions {
      ...ActionInfo
    }
    produits
  }
  ${FULL_ACTION}
`;

export const PLAN_MARKETING_TYPE_INFO = gql`
  fragment PRTPlanMarketingTypeInfo on PRTPlanMarketingType {
    id
    code
    libelle
    couleur
    idPharmacie
  }
`;

export const PLAN_MARKETING_STATUT_INFO = gql`
  fragment PRTPlanMarketingStatutInfo on PRTPlanMarketingStatut {
    id
    code
    libelle
  }
`;

export const MISE_AVANT_INFO = gql`
  fragment PRTMiseAvantInfo on PRTMiseAvant {
    id
    code
    libelle
    couleur
  }
`;
