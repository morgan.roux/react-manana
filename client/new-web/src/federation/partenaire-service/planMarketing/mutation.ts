import gql from 'graphql-tag';
import { FULL_PLAN_MARKETING_INFO, PLAN_MARKETING_TYPE_INFO } from './fragment';

export const CREATE_PLAN_MARKETING = gql`
  mutation CREATE_PLAN_MARKETING($input: PRTPlanMarketingInput!) {
    createOnePRTPlanMarketing(input: $input) {
      ...PRTPlanMarketingInfo
    }
  }
  ${FULL_PLAN_MARKETING_INFO}
`;

export const UPDATE_PLAN_MARKETING = gql`
  mutation UPDATE_PLAN_MARKETING($id: String!, $input: PRTPlanMarketingInput!) {
    updateOnePRTPlanMarketing(id: $id, input: $input) {
      ...PRTPlanMarketingInfo
    }
  }
  ${FULL_PLAN_MARKETING_INFO}
`;

export const DELETE_ONE_PLAN_MARKETING = gql`
  mutation DELETE_ONE_PLAN_MARKETING($input: PRTPlanMarketingDeleteOneInput!) {
    deleteOnePRTPlanMarketing(input: $input) {
      id
    }
  }
`;

export const CHANGE_PLAN_MARKETING_STATUS = gql`
  mutation CHANGE_PLAN_MARKETING_STATUS($id: String!, $idStatut: String!) {
    changePRTPlanMarketingStatut(id: $id, idStatut: $idStatut) {
      ...PRTPlanMarketingInfo
    }
  }
  ${FULL_PLAN_MARKETING_INFO}
`;

export const CREATE_PLAN_MARKETING_TYPE = gql `
mutation CREATE_PLAN_MARKETING_TYPE($input: CreateOnePRTPlanMarketingTypeInput!){
  createOnePRTPlanMarketingType(input: $input){
    ...PRTPlanMarketingTypeInfo
  }
}${PLAN_MARKETING_TYPE_INFO}
`;

export const DELETE_ONE_PLAN_MARKETING_TYPE = gql`
mutation DELETE_ONE_PLAN_MARKETING_TYPE($input: DeleteOneInput!){
  deleteOnePRTPlanMarketingType(input: $input){
    id
  }
}
`;

export const UPDATE_ONE_PLAN_MARKETIN_TYPE = gql`
mutation UPDATE_ONE_PLAN_MARKETIN_TYPE($input: UpdateOnePRTPlanMarketingTypeInput!){
  updateOnePRTPlanMarketingType(input: $input){
    ...PRTPlanMarketingTypeInfo
  }
}${PLAN_MARKETING_TYPE_INFO}
`;
