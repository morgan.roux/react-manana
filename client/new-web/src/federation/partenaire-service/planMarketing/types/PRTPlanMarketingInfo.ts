/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PRTPlanMarketingInfo
// ====================================================

export interface PRTPlanMarketingInfo_type {
  __typename: "PRTPlanMarketingType";
  id: string;
  libelle: string;
  code: string;
  couleur: string | null;
}

export interface PRTPlanMarketingInfo_statut {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface PRTPlanMarketingInfo_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface PRTPlanMarketingInfo_miseAvants {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
}

export interface PRTPlanMarketingInfo_actions_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface PRTPlanMarketingInfo_actions_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface PRTPlanMarketingInfo_actions {
  __typename: "TodoAction";
  id: string;
  ordre: number;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idActionParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: PRTPlanMarketingInfo_actions_importance | null;
  urgence: PRTPlanMarketingInfo_actions_urgence | null;
  nbComment: number | null;
}

export interface PRTPlanMarketingInfo {
  __typename: "PRTPlanMarketing";
  id: string;
  titre: string;
  description: string | null;
  dateDebut: any;
  dateFin: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idGroupement: string;
  idPharmacie: string;
  type: PRTPlanMarketingInfo_type;
  statut: PRTPlanMarketingInfo_statut;
  fichiers: PRTPlanMarketingInfo_fichiers[] | null;
  miseAvants: PRTPlanMarketingInfo_miseAvants[] | null;
  actions: PRTPlanMarketingInfo_actions[] | null;
  produits: string[] | null;
}
