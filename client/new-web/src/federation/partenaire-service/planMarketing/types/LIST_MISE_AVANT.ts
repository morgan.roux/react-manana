/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTMiseAvantFilter, PRTMiseAvantSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: LIST_MISE_AVANT
// ====================================================

export interface LIST_MISE_AVANT_pRTMiseAvants_nodes {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface LIST_MISE_AVANT_pRTMiseAvants {
  __typename: "PRTMiseAvantConnection";
  /**
   * Array of nodes.
   */
  nodes: LIST_MISE_AVANT_pRTMiseAvants_nodes[];
}

export interface LIST_MISE_AVANT {
  pRTMiseAvants: LIST_MISE_AVANT_pRTMiseAvants;
}

export interface LIST_MISE_AVANTVariables {
  paging?: OffsetPaging | null;
  filter?: PRTMiseAvantFilter | null;
  sorting?: PRTMiseAvantSort[] | null;
}
