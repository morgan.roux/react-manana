/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CHANGE_PLAN_MARKETING_STATUS
// ====================================================

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_type {
  __typename: "PRTPlanMarketingType";
  id: string;
  libelle: string;
  code: string;
  couleur: string | null;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_statut {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_miseAvants {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_actions_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_actions_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_actions {
  __typename: "TodoAction";
  id: string;
  ordre: number;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idActionParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_actions_importance | null;
  urgence: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_actions_urgence | null;
  nbComment: number | null;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut {
  __typename: "PRTPlanMarketing";
  id: string;
  titre: string;
  description: string | null;
  dateDebut: any;
  dateFin: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idGroupement: string;
  idPharmacie: string;
  type: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_type;
  statut: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_statut;
  fichiers: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_fichiers[] | null;
  miseAvants: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_miseAvants[] | null;
  actions: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_actions[] | null;
  produits: string[] | null;
}

export interface CHANGE_PLAN_MARKETING_STATUS {
  changePRTPlanMarketingStatut: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut;
}

export interface CHANGE_PLAN_MARKETING_STATUSVariables {
  id: string;
  idStatut: string;
}
