/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTPlanMarketingAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_PLAN_MARKETING_AGGREGATES
// ====================================================

export interface GET_PLAN_MARKETING_AGGREGATES_pRTPlanMarketingAggregate_count {
  __typename: "PRTPlanMarketingCountAggregate";
  id: number | null;
}

export interface GET_PLAN_MARKETING_AGGREGATES_pRTPlanMarketingAggregate {
  __typename: "PRTPlanMarketingAggregateResponse";
  count: GET_PLAN_MARKETING_AGGREGATES_pRTPlanMarketingAggregate_count | null;
}

export interface GET_PLAN_MARKETING_AGGREGATES {
  pRTPlanMarketingAggregate: GET_PLAN_MARKETING_AGGREGATES_pRTPlanMarketingAggregate;
}

export interface GET_PLAN_MARKETING_AGGREGATESVariables {
  filter?: PRTPlanMarketingAggregateFilter | null;
}
