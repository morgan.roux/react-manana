/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTPlanMarketingInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_PLAN_MARKETING
// ====================================================

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_type {
  __typename: "PRTPlanMarketingType";
  id: string;
  libelle: string;
  code: string;
  couleur: string | null;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_statut {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_miseAvants {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_actions_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_actions_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_actions {
  __typename: "TodoAction";
  id: string;
  ordre: number;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idActionParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_actions_importance | null;
  urgence: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_actions_urgence | null;
  nbComment: number | null;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing {
  __typename: "PRTPlanMarketing";
  id: string;
  titre: string;
  description: string | null;
  dateDebut: any;
  dateFin: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idGroupement: string;
  idPharmacie: string;
  type: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_type;
  statut: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_statut;
  fichiers: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_fichiers[] | null;
  miseAvants: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_miseAvants[] | null;
  actions: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_actions[] | null;
  produits: string[] | null;
}

export interface CREATE_PLAN_MARKETING {
  createOnePRTPlanMarketing: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing;
}

export interface CREATE_PLAN_MARKETINGVariables {
  input: PRTPlanMarketingInput;
}
