/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTPlanMarketingStatutFilter, PRTPlanMarketingStatutSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_LIST_PLAN_MARKETING_STATUT
// ====================================================

export interface GET_LIST_PLAN_MARKETING_STATUT_pRTPlanMarketingStatuts_nodes {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_LIST_PLAN_MARKETING_STATUT_pRTPlanMarketingStatuts {
  __typename: "PRTPlanMarketingStatutConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_LIST_PLAN_MARKETING_STATUT_pRTPlanMarketingStatuts_nodes[];
}

export interface GET_LIST_PLAN_MARKETING_STATUT {
  pRTPlanMarketingStatuts: GET_LIST_PLAN_MARKETING_STATUT_pRTPlanMarketingStatuts;
}

export interface GET_LIST_PLAN_MARKETING_STATUTVariables {
  paging?: OffsetPaging | null;
  filter?: PRTPlanMarketingStatutFilter | null;
  sorting?: PRTPlanMarketingStatutSort[] | null;
}
