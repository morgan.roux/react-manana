/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_ONE_PLAN_MARKETING_TYPE
// ====================================================

export interface DELETE_ONE_PLAN_MARKETING_TYPE_deleteOnePRTPlanMarketingType {
  __typename: "PRTPlanMarketingTypeDeleteResponse";
  id: string | null;
}

export interface DELETE_ONE_PLAN_MARKETING_TYPE {
  deleteOnePRTPlanMarketingType: DELETE_ONE_PLAN_MARKETING_TYPE_deleteOnePRTPlanMarketingType;
}

export interface DELETE_ONE_PLAN_MARKETING_TYPEVariables {
  input: DeleteOneInput;
}
