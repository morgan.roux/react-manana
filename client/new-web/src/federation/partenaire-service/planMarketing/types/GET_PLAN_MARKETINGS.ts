/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTPlanMarketingFilter, PRTPlanMarketingSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_PLAN_MARKETINGS
// ====================================================

export interface GET_PLAN_MARKETINGS_pRTPlanMarketings_nodes_type {
  __typename: "PRTPlanMarketingType";
  id: string;
  libelle: string;
  code: string;
  couleur: string | null;
}

export interface GET_PLAN_MARKETINGS_pRTPlanMarketings_nodes_statut {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_PLAN_MARKETINGS_pRTPlanMarketings_nodes_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_PLAN_MARKETINGS_pRTPlanMarketings_nodes_miseAvants {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_PLAN_MARKETINGS_pRTPlanMarketings_nodes_actions_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_PLAN_MARKETINGS_pRTPlanMarketings_nodes_actions_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_PLAN_MARKETINGS_pRTPlanMarketings_nodes_actions {
  __typename: "TodoAction";
  id: string;
  ordre: number;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idActionParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: GET_PLAN_MARKETINGS_pRTPlanMarketings_nodes_actions_importance | null;
  urgence: GET_PLAN_MARKETINGS_pRTPlanMarketings_nodes_actions_urgence | null;
  nbComment: number | null;
}

export interface GET_PLAN_MARKETINGS_pRTPlanMarketings_nodes {
  __typename: "PRTPlanMarketing";
  id: string;
  titre: string;
  description: string | null;
  dateDebut: any;
  dateFin: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idGroupement: string;
  idPharmacie: string;
  type: GET_PLAN_MARKETINGS_pRTPlanMarketings_nodes_type;
  statut: GET_PLAN_MARKETINGS_pRTPlanMarketings_nodes_statut;
  fichiers: GET_PLAN_MARKETINGS_pRTPlanMarketings_nodes_fichiers[] | null;
  miseAvants: GET_PLAN_MARKETINGS_pRTPlanMarketings_nodes_miseAvants[] | null;
  actions: GET_PLAN_MARKETINGS_pRTPlanMarketings_nodes_actions[] | null;
  produits: string[] | null;
}

export interface GET_PLAN_MARKETINGS_pRTPlanMarketings {
  __typename: "PRTPlanMarketingConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_PLAN_MARKETINGS_pRTPlanMarketings_nodes[];
}

export interface GET_PLAN_MARKETINGS {
  pRTPlanMarketings: GET_PLAN_MARKETINGS_pRTPlanMarketings;
}

export interface GET_PLAN_MARKETINGSVariables {
  paging?: OffsetPaging | null;
  filter?: PRTPlanMarketingFilter | null;
  sorting?: PRTPlanMarketingSort[] | null;
}
