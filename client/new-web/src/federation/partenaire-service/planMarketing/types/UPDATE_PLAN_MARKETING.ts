/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTPlanMarketingInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_PLAN_MARKETING
// ====================================================

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_type {
  __typename: "PRTPlanMarketingType";
  id: string;
  libelle: string;
  code: string;
  couleur: string | null;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_statut {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_miseAvants {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_actions_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_actions_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_actions {
  __typename: "TodoAction";
  id: string;
  ordre: number;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idActionParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_actions_importance | null;
  urgence: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_actions_urgence | null;
  nbComment: number | null;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing {
  __typename: "PRTPlanMarketing";
  id: string;
  titre: string;
  description: string | null;
  dateDebut: any;
  dateFin: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idGroupement: string;
  idPharmacie: string;
  type: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_type;
  statut: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_statut;
  fichiers: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_fichiers[] | null;
  miseAvants: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_miseAvants[] | null;
  actions: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_actions[] | null;
  produits: string[] | null;
}

export interface UPDATE_PLAN_MARKETING {
  updateOnePRTPlanMarketing: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing;
}

export interface UPDATE_PLAN_MARKETINGVariables {
  id: string;
  input: PRTPlanMarketingInput;
}
