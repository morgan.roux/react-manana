/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTPlanMarketingDeleteOneInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_ONE_PLAN_MARKETING
// ====================================================

export interface DELETE_ONE_PLAN_MARKETING_deleteOnePRTPlanMarketing {
  __typename: "PRTPlanMarketing";
  id: string;
}

export interface DELETE_ONE_PLAN_MARKETING {
  deleteOnePRTPlanMarketing: DELETE_ONE_PLAN_MARKETING_deleteOnePRTPlanMarketing;
}

export interface DELETE_ONE_PLAN_MARKETINGVariables {
  input: PRTPlanMarketingDeleteOneInput;
}
