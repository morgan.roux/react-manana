/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTContactFilter, PRTContactSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_PRT_CONTACT
// ====================================================

export interface GET_PRT_CONTACT_pRTContacts_nodes_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface GET_PRT_CONTACT_pRTContacts_nodes_contact {
  __typename: "ContactType";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
  whatsappMobProf: string | null;
  whatsappMobPerso: string | null;
  compteSkypeProf: string | null;
  compteSkypePerso: string | null;
  urlLinkedInProf: string | null;
  urlLinkedInPerso: string | null;
  urlTwitterProf: string | null;
  urlTwitterPerso: string | null;
  urlFacebookProf: string | null;
  urlFacebookPerso: string | null;
  codeMaj: string | null;
  idUserCreation: string | null;
  idUserModification: string | null;
}

export interface GET_PRT_CONTACT_pRTContacts_nodes {
  __typename: "PRTContact";
  id: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  fonction: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  photo: GET_PRT_CONTACT_pRTContacts_nodes_photo | null;
  contact: GET_PRT_CONTACT_pRTContacts_nodes_contact | null;
}

export interface GET_PRT_CONTACT_pRTContacts {
  __typename: "PRTContactConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_PRT_CONTACT_pRTContacts_nodes[];
}

export interface GET_PRT_CONTACT {
  pRTContacts: GET_PRT_CONTACT_pRTContacts;
}

export interface GET_PRT_CONTACTVariables {
  paging?: OffsetPaging | null;
  filter?: PRTContactFilter | null;
  sorting?: PRTContactSort[] | null;
}
