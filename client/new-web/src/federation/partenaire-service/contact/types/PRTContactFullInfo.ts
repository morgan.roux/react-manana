/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PRTContactFullInfo
// ====================================================

export interface PRTContactFullInfo_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface PRTContactFullInfo_contact {
  __typename: "ContactType";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
  whatsappMobProf: string | null;
  whatsappMobPerso: string | null;
  compteSkypeProf: string | null;
  compteSkypePerso: string | null;
  urlLinkedInProf: string | null;
  urlLinkedInPerso: string | null;
  urlTwitterProf: string | null;
  urlTwitterPerso: string | null;
  urlFacebookProf: string | null;
  urlFacebookPerso: string | null;
  codeMaj: string | null;
  idUserCreation: string | null;
  idUserModification: string | null;
}

export interface PRTContactFullInfo {
  __typename: "PRTContact";
  id: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  fonction: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  photo: PRTContactFullInfo_photo | null;
  contact: PRTContactFullInfo_contact | null;
}
