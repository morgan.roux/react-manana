import gql from 'graphql-tag';

export const CONTACT_INFO = gql `
fragment ContactInfo on ContactType {
    id
    cp
    ville
    pays
    adresse1
    adresse2
    faxProf
    faxPerso
    telProf
    telMobProf
    telPerso
    telMobPerso
    mailProf
    mailPerso
    siteProf
    sitePerso
    whatsappMobProf
    whatsappMobPerso
    compteSkypeProf
    compteSkypePerso
    urlLinkedInProf
    urlLinkedInPerso
    urlTwitterProf
    urlTwitterPerso
    urlFacebookProf
    urlFacebookPerso
    codeMaj
    idUserCreation
    idUserModification
  }
`;

export const PRT_CONTACT_FULL_INFO = gql`
    fragment PRTContactFullInfo on PRTContact {
        id
        civilite
        nom
        prenom
        fonction
        idPartenaireTypeAssocie
        partenaireType
        photo {
            id
            publicUrl
            chemin
            nomOriginal
            type
        }
        contact {
            ...ContactInfo
        }
    }
    ${CONTACT_INFO}
`;


export const PRT_CONTACT_SEARCH_INFO = gql`
    fragment PRTContactInfo on PRTContact {
        id
        dataType
        civilite
        nom
        prenom
        fonction
        photo {
            id
            publicUrl
            chemin
            nomOriginal
            type
        }
        contact {
            id
            mailProf
            mailPerso
        }
    }
`;