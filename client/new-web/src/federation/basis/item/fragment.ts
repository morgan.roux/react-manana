import gql from 'graphql-tag';

export const FULL_ITEM_INFO = gql`
  fragment ItemInfo on Item {
    id
    code
    codeItem
    name
    createdAt
    updatedAt
  }
`;

export const ITEM_INFO = gql`
  fragment ItemInfo on Item {
    id
    code
    name
  }
`;
