import gql from 'graphql-tag';
import { FULL_ITEM_INFO, ITEM_INFO } from './fragment';

export const GET_ITEMS = gql`
  query ITEMS(
    $paging: OffsetPaging
    $filter: ItemFilter
    $sorting: [ItemSort!]
    $idPharmacie: String
  ) {
    items(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      id
      code
      codeItem
      name
      createdAt
      updatedAt
      active(idPharmacie: $idPharmacie)
      }
    }
  }
`;

export const GET_ITEMS_FETCHED = gql`
  query ITEMS_FETCHED {
    itemsFetched {
      ...ItemInfo
    }
  }
  ${ITEM_INFO}
`;

export const GET_ITEM_AGGREGATE = gql`
  query ITEM_AGGREGATE($filter: ItemAggregateFilter) {
    itemAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;
