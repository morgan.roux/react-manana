/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: clientInfo
// ====================================================

export interface clientInfo_user_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface clientInfo_user {
  __typename: "User";
  id: string;
  fullName: string | null;
  email: string | null;
  role: clientInfo_user_role | null;
  phoneNumber: string | null;
}

export interface clientInfo_contact {
  __typename: "ContactType";
  id: string;
  adresse1: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
}

export interface clientInfo {
  __typename: "ClientType";
  id: string;
  idUser: string;
  idContact: string;
  idGroupement: string;
  idPharmacie: string;
  createdAt: any;
  updatedAt: any;
  user: clientInfo_user;
  contact: clientInfo_contact;
}
