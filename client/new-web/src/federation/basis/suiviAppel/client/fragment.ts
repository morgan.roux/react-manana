import gql from 'graphql-tag';

export const CLIENT_INFO = gql`
  fragment clientInfo on ClientType {
    id
    idUser
    idContact
    idGroupement
    idPharmacie
    createdAt
    updatedAt
    user {
      id
      fullName
      email
      role {
        id
        nom
      }
      phoneNumber
    }
    contact {
      id
      adresse1
      cp
      ville
      pays
    }
  }
`;
