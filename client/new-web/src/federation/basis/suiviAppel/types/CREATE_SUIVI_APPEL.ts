/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { SuiviAppelInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_SUIVI_APPEL
// ====================================================

export interface CREATE_SUIVI_APPEL_createOneSuiviAppel_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface CREATE_SUIVI_APPEL_createOneSuiviAppel_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface CREATE_SUIVI_APPEL_createOneSuiviAppel_interlocuteur_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_SUIVI_APPEL_createOneSuiviAppel_interlocuteur_role {
  __typename: "Role";
  id: string;
  dataType: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_SUIVI_APPEL_createOneSuiviAppel_interlocuteur {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CREATE_SUIVI_APPEL_createOneSuiviAppel_interlocuteur_photo | null;
  phoneNumber: string | null;
  role: CREATE_SUIVI_APPEL_createOneSuiviAppel_interlocuteur_role | null;
}

export interface CREATE_SUIVI_APPEL_createOneSuiviAppel_declarant_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_SUIVI_APPEL_createOneSuiviAppel_declarant {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CREATE_SUIVI_APPEL_createOneSuiviAppel_declarant_photo | null;
  phoneNumber: string | null;
}

export interface CREATE_SUIVI_APPEL_createOneSuiviAppel_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_SUIVI_APPEL_createOneSuiviAppel_concernes_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_SUIVI_APPEL_createOneSuiviAppel_concernes {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CREATE_SUIVI_APPEL_createOneSuiviAppel_concernes_photo | null;
  phoneNumber: string | null;
}

export interface CREATE_SUIVI_APPEL_createOneSuiviAppel_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CREATE_SUIVI_APPEL_createOneSuiviAppel_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_SUIVI_APPEL_createOneSuiviAppel {
  __typename: "SuiviAppelType";
  id: string;
  dataType: string;
  idUserDeclarant: string;
  idUserInterlocuteur: string;
  idFonction: string | null;
  idTache: string | null;
  idImportance: string;
  idUrgence: string;
  commentaire: string | null;
  idFichiers: string[];
  idGroupement: string;
  status: string | null;
  idPharmacie: string;
  createdAt: any;
  updatedAt: any;
  fonction: CREATE_SUIVI_APPEL_createOneSuiviAppel_fonction | null;
  tache: CREATE_SUIVI_APPEL_createOneSuiviAppel_tache | null;
  interlocuteur: CREATE_SUIVI_APPEL_createOneSuiviAppel_interlocuteur;
  declarant: CREATE_SUIVI_APPEL_createOneSuiviAppel_declarant;
  fichiers: CREATE_SUIVI_APPEL_createOneSuiviAppel_fichiers[] | null;
  concernes: CREATE_SUIVI_APPEL_createOneSuiviAppel_concernes[] | null;
  urgence: CREATE_SUIVI_APPEL_createOneSuiviAppel_urgence;
  importance: CREATE_SUIVI_APPEL_createOneSuiviAppel_importance;
}

export interface CREATE_SUIVI_APPEL {
  createOneSuiviAppel: CREATE_SUIVI_APPEL_createOneSuiviAppel;
}

export interface CREATE_SUIVI_APPELVariables {
  input: SuiviAppelInput;
}
