/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: suiviAppelInfo
// ====================================================

export interface suiviAppelInfo_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface suiviAppelInfo_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface suiviAppelInfo_interlocuteur_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface suiviAppelInfo_interlocuteur_role {
  __typename: "Role";
  id: string;
  dataType: string;
  code: string | null;
  nom: string | null;
}

export interface suiviAppelInfo_interlocuteur {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: suiviAppelInfo_interlocuteur_photo | null;
  phoneNumber: string | null;
  role: suiviAppelInfo_interlocuteur_role | null;
}

export interface suiviAppelInfo_declarant_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface suiviAppelInfo_declarant {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: suiviAppelInfo_declarant_photo | null;
  phoneNumber: string | null;
}

export interface suiviAppelInfo_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface suiviAppelInfo_concernes_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface suiviAppelInfo_concernes {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: suiviAppelInfo_concernes_photo | null;
  phoneNumber: string | null;
}

export interface suiviAppelInfo_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface suiviAppelInfo_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface suiviAppelInfo {
  __typename: "SuiviAppelType";
  id: string;
  dataType: string;
  idUserDeclarant: string;
  idUserInterlocuteur: string;
  idFonction: string | null;
  idTache: string | null;
  idImportance: string;
  idUrgence: string;
  commentaire: string | null;
  idFichiers: string[];
  idGroupement: string;
  status: string | null;
  idPharmacie: string;
  createdAt: any;
  updatedAt: any;
  fonction: suiviAppelInfo_fonction | null;
  tache: suiviAppelInfo_tache | null;
  interlocuteur: suiviAppelInfo_interlocuteur;
  declarant: suiviAppelInfo_declarant;
  fichiers: suiviAppelInfo_fichiers[] | null;
  concernes: suiviAppelInfo_concernes[] | null;
  urgence: suiviAppelInfo_urgence;
  importance: suiviAppelInfo_importance;
}
