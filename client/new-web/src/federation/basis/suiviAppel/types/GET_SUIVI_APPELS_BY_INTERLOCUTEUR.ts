/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_SUIVI_APPELS_BY_INTERLOCUTEUR
// ====================================================

export interface GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_interlocuteur_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_interlocuteur_role {
  __typename: "Role";
  id: string;
  dataType: string;
  code: string | null;
  nom: string | null;
}

export interface GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_interlocuteur {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_interlocuteur_photo | null;
  phoneNumber: string | null;
  role: GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_interlocuteur_role | null;
}

export interface GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_declarant_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_declarant {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_declarant_photo | null;
  phoneNumber: string | null;
}

export interface GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_concernes_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_concernes {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_concernes_photo | null;
  phoneNumber: string | null;
}

export interface GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur {
  __typename: "SuiviAppelType";
  id: string;
  dataType: string;
  idUserDeclarant: string;
  idUserInterlocuteur: string;
  idFonction: string | null;
  idTache: string | null;
  idImportance: string;
  idUrgence: string;
  commentaire: string | null;
  idFichiers: string[];
  idGroupement: string;
  status: string | null;
  idPharmacie: string;
  createdAt: any;
  updatedAt: any;
  fonction: GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_fonction | null;
  tache: GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_tache | null;
  interlocuteur: GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_interlocuteur;
  declarant: GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_declarant;
  fichiers: GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_fichiers[] | null;
  concernes: GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_concernes[] | null;
  urgence: GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_urgence;
  importance: GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur_importance;
}

export interface GET_SUIVI_APPELS_BY_INTERLOCUTEUR {
  getSuivisAppelsByIdInterlocuteur: GET_SUIVI_APPELS_BY_INTERLOCUTEUR_getSuivisAppelsByIdInterlocuteur[];
}

export interface GET_SUIVI_APPELS_BY_INTERLOCUTEURVariables {
  idUserInterlocuteur: string;
}
