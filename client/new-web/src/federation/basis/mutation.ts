import gql from 'graphql-tag';
import { ACTIVITE_INFO } from './fragment';

export const CREATE_ACTIVITE = gql`
    mutation CREATE_ACTIVITE($input:ActiviteInput!){
        createOneActivite(input:$input){
            ...userActiviteInfo
        }
    }

    ${ACTIVITE_INFO}
`;