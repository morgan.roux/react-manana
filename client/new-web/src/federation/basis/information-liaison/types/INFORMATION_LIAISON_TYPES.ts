/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, InformationLiaisonTypeFilter, InformationLiaisonTypeSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: INFORMATION_LIAISON_TYPES
// ====================================================

export interface INFORMATION_LIAISON_TYPES_informationLiaisonTypes_nodes {
  __typename: "InformationLiaisonType";
  id: string;
  code: string;
  libelle: string;
}

export interface INFORMATION_LIAISON_TYPES_informationLiaisonTypes {
  __typename: "InformationLiaisonTypeConnection";
  /**
   * Array of nodes.
   */
  nodes: INFORMATION_LIAISON_TYPES_informationLiaisonTypes_nodes[];
}

export interface INFORMATION_LIAISON_TYPES {
  informationLiaisonTypes: INFORMATION_LIAISON_TYPES_informationLiaisonTypes;
}

export interface INFORMATION_LIAISON_TYPESVariables {
  paging?: OffsetPaging | null;
  filter?: InformationLiaisonTypeFilter | null;
  sorting?: InformationLiaisonTypeSort[] | null;
}
