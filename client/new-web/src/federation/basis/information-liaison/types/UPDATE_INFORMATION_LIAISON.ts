/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UPDATE_INFORMATION_LIAISON
// ====================================================

export interface UPDATE_INFORMATION_LIAISON_updateInformationLiaisonStatut {
  __typename: "InformationLiaison";
  id: string;
  description: string | null;
  idTache: string | null;
  idFonction: string | null;
  idImportance: string | null;
  idUrgence: string | null;
  statut: string | null;
}

export interface UPDATE_INFORMATION_LIAISON {
  updateInformationLiaisonStatut: UPDATE_INFORMATION_LIAISON_updateInformationLiaisonStatut | null;
}

export interface UPDATE_INFORMATION_LIAISONVariables {
  code: string;
  id: string;
}
