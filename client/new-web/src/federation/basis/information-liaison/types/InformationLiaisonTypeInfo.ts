/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: InformationLiaisonTypeInfo
// ====================================================

export interface InformationLiaisonTypeInfo {
  __typename: "InformationLiaisonType";
  id: string;
  code: string;
  libelle: string;
}
