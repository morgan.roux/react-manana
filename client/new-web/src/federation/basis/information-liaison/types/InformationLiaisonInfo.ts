/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: InformationLiaisonInfo
// ====================================================

export interface InformationLiaisonInfo {
  __typename: "InformationLiaison";
  id: string;
  description: string | null;
  idTache: string | null;
  idFonction: string | null;
  idImportance: string | null;
  idUrgence: string | null;
  statut: string | null;
}
