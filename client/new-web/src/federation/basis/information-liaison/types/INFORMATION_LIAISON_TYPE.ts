/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: INFORMATION_LIAISON_TYPE
// ====================================================

export interface INFORMATION_LIAISON_TYPE_informationLiaisonType {
  __typename: "InformationLiaisonType";
  id: string;
  code: string;
  libelle: string;
}

export interface INFORMATION_LIAISON_TYPE {
  informationLiaisonType: INFORMATION_LIAISON_TYPE_informationLiaisonType | null;
}

export interface INFORMATION_LIAISON_TYPEVariables {
  id: string;
}
