/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: TACHE
// ====================================================

export interface TACHE_dQMTTache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface TACHE {
  dQMTTache: TACHE_dQMTTache | null;
}

export interface TACHEVariables {
  id: string;
}
