/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: UrgenceInfo
// ====================================================

export interface UrgenceInfo {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}
