/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: userActiviteInfo
// ====================================================

export interface userActiviteInfo_user {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface userActiviteInfo_type {
  __typename: "ActiviteType";
  id: string;
  code: string;
  nom: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface userActiviteInfo {
  __typename: "Activite";
  id: string;
  idActiviteType: string;
  idItem: string;
  idItemAssocie: string;
  idUser: string | null;
  log: string | null;
  createdAt: any;
  updatedAt: any;
  user: userActiviteInfo_user;
  type: userActiviteInfo_type;
}
