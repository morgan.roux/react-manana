/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, OrigineFilter, OrigineSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: ORIGINES
// ====================================================

export interface ORIGINES_origines_nodes {
  __typename: "Origine";
  id: string;
  code: string;
  libelle: string;
  createdAt: any;
  updatedAt: any;
}

export interface ORIGINES_origines {
  __typename: "OrigineConnection";
  /**
   * Array of nodes.
   */
  nodes: ORIGINES_origines_nodes[];
}

export interface ORIGINES {
  origines: ORIGINES_origines;
}

export interface ORIGINESVariables {
  paging?: OffsetPaging | null;
  filter?: OrigineFilter | null;
  sorting?: OrigineSort[] | null;
}
