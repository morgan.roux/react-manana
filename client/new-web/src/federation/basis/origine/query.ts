import gql from 'graphql-tag';
import { ORIGINE_INFO } from './fragment';

export const ORIGINES = gql `
    query ORIGINES($paging: OffsetPaging, $filter: OrigineFilter, $sorting: [OrigineSort!]) {
        origines(paging: $paging, filter: $filter, sorting: $sorting) {
            nodes{
            ...origineInfo
            }
        }
    }
    ${ORIGINE_INFO}
`;