/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, TodoActionFilter, TodoActionSort } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: ACTIONS
// ====================================================

export interface ACTIONS_todoActions_nodes_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface ACTIONS_todoActions_nodes_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface ACTIONS_todoActions_nodes {
  __typename: "TodoAction";
  id: string;
  ordre: number;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idActionParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: ACTIONS_todoActions_nodes_importance | null;
  urgence: ACTIONS_todoActions_nodes_urgence | null;
  nbComment: number | null;
}

export interface ACTIONS_todoActions {
  __typename: "TodoActionConnection";
  /**
   * Array of nodes.
   */
  nodes: ACTIONS_todoActions_nodes[];
}

export interface ACTIONS {
  todoActions: ACTIONS_todoActions;
}

export interface ACTIONSVariables {
  paging?: OffsetPaging | null;
  filter?: TodoActionFilter | null;
  sorting?: TodoActionSort[] | null;
}
