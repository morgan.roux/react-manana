/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ActionInfo
// ====================================================

export interface ActionInfo_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface ActionInfo_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface ActionInfo {
  __typename: "TodoAction";
  id: string;
  ordre: number;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idActionParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: ActionInfo_importance | null;
  urgence: ActionInfo_urgence | null;
  nbComment: number | null;
}
