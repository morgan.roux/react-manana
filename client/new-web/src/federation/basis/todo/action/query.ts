import gql from 'graphql-tag';
import { FULL_ACTION } from './fragment';

export const GET_ACTIONS = gql`
  query ACTIONS($paging: OffsetPaging, $filter: TodoActionFilter, $sorting: [TodoActionSort!]) {
    todoActions(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...ActionInfo
      }
    }
  }

  ${FULL_ACTION}
`;
