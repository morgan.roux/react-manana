import gql from 'graphql-tag';
import { IMPORTANCE_INFO, URGENCE_INFO } from '../../fragment';

export const FULL_ACTION = gql`
  fragment ActionInfo on TodoAction {
    id
    ordre
    description
    dateDebut
    dateFin
    idActionParent
    idItemAssocie
    idImportance
    status
    importance {
      ...ImportanceInfo
    }
    urgence {
      ...UrgenceInfo
    }
    nbComment
  }
  ${IMPORTANCE_INFO}
  ${URGENCE_INFO}
`;
