import gql from 'graphql-tag';

export const ACTIVITE_INFO = gql`
  fragment userActiviteInfo on Activite {
    id
    idActiviteType
    idItem
    idItemAssocie
    idUser
    log
    createdAt
    updatedAt
    user {
      id
      fullName
    }
    type {
      id
      code
      nom
      createdAt
      updatedAt
    }
  }
`;

export const URGENCE_INFO = gql`
  fragment UrgenceInfo on Urgence {
    id
    code
    libelle
    couleur
  }
`;

export const IMPORTANCE_INFO = gql`
  fragment ImportanceInfo on Importance {
    id
    ordre
    libelle
    couleur
  }
`;
