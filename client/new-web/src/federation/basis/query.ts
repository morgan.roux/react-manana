import gql from 'graphql-tag';
import { URGENCE_INFO, IMPORTANCE_INFO } from './fragment';

export const GET_URGENCES = gql`
  query GET_URGENCES($paging: OffsetPaging, $filter: UrgenceFilter, $sorting: [UrgenceSort!]) {
    urgences(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...UrgenceInfo
      }
    }
  }

  ${URGENCE_INFO}
`;

export const GET_IMPORTANCES = gql`
  query IMPORTANCES($paging: OffsetPaging, $filter: ImportanceFilter, $sorting: [ImportanceSort!]) {
    importances(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...ImportanceInfo
      }
    }
  }

  ${IMPORTANCE_INFO}
`;

export const GET_TACHE = gql`
  query TACHE($id: ID!) {
    dQMTTache(id: $id) {
      id
      libelle
    }
  }
`;

export const GET_FONCTION = gql`
  query FONCTION($id: ID!) {
    dQMTFonction(id: $id) {
      id
      libelle
    }
  }
`;
