import { HelloActionTypes, HelloState, SET_MSG } from '../types/hello';

const initialState: HelloState = {
  msg: 'Hello World !',
};

const hello = (state: HelloState = initialState, action: HelloActionTypes): HelloState => {
  switch (action.type) {
    case SET_MSG:
      return { ...state, msg: action.payload };
    default:
      return { ...state };
  }
};

export default hello;
