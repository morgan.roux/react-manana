import { HelloState } from '../types/hello';

export const helloSelector = ({ hello }: { hello: HelloState }) => hello;
