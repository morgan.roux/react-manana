import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      textAlign: 'center',
      backgroundColor: '#2f4858',
      color: '#fff',
      padding: '15px 0',
    },
  }),
);
