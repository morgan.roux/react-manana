import { useLazyQuery, useMutation } from '@apollo/client';
import React, { ChangeEvent, FC, MouseEvent, useEffect, useRef, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { Column } from '../../../Common/newCustomContent/interfaces';
import { useStyles } from './styles';
import { Table } from './Table';
import { columnsFilter, columnsFilterWithContact, getUserColumns } from '../columns';
import {
  SEARCH_MIN_USERS,
  SEARCH_MIN_USERSVariables,
} from '../../../../graphql/User/types/SEARCH_MIN_USERS';
import { DO_SEARCH_MIN_USERS } from '../../../../graphql/User/query';
import { SearchInput } from '../../../Dashboard/Content/SearchInput';
import { debounce } from 'lodash';
import { Box, CssBaseline, Hidden } from '@material-ui/core';
import CustomSelect from '../../../Common/CustomSelect';
import { ModalFormAddClient, OutputDataClient } from '../ModalFormAddClient';
import { CREATE_CLIENT } from '../../../../federation/basis/suiviAppel/client/mutation';
import {
  CREATE_CLIENT as CREATE_CLIENT_TYPE,
  CREATE_CLIENTVariables,
} from '../../../../federation/basis/suiviAppel/client/types/CREATE_CLIENT';
import { FEDERATION_CLIENT } from '../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { CustomButton } from '@app/ui-kit';
import { Add } from '@material-ui/icons';

interface UsersProp {
  query: any;
  textSearch: string;
  setTextSearch: (value: string) => void;
  setUserIdsSelected?: (value: any) => void;
  userIdsSelected?: string[];
  setUsersSelected?: (value: any) => void;
  usersSelected?: any[];
  allUsers?: any[];
  handleChecked: (code: string) => void;
  modalAddClient?: {
    open: boolean;
    setOpen: () => void;
  };
  onAddClient?: () => void;
}

export interface SkipAndTake {
  skip: number;
  take: number;
}

export interface SortBy {
  column: string;
  order: 'asc' | 'desc' | undefined;
}

const Users: React.FC<RouteComponentProps & UsersProp> = ({
  query,
  setTextSearch,
  textSearch,
  setUserIdsSelected,
  userIdsSelected,
  setUsersSelected,
  usersSelected,
  allUsers,
  handleChecked,
  modalAddClient,
  onAddClient,
}) => {
  const classes = useStyles({});

  const columns: Column[] = getUserColumns();
  const [debouncedText, setDebouncedText] = useState<string>('');
  const [skipAndTake, setSkipAndTake] = useState<SkipAndTake>({
    skip: 0,
    take: 10,
  });
  const [sortBy, setSortBy] = useState<SortBy>({
    column: 'userName',
    order: 'asc',
  });

  const [filterColumns, setFilterColumns] = React.useState<any[]>(
    columnsFilterWithContact() as any,
  );
  const [activeFilter, setActiveFilter] = useState<any>('ALL');
  const [showAddClientBtn, setShowAddClientBtn] = useState<boolean>(false);

  const onChangeSearchText = (value: string) => {
    setDebouncedText(value);
  };

  const [searchUser, results] = useLazyQuery<SEARCH_MIN_USERS, SEARCH_MIN_USERSVariables>(
    DO_SEARCH_MIN_USERS,
    {
      fetchPolicy: 'cache-and-network',
    },
  );

  const [createClient, creationClient] = useMutation<CREATE_CLIENT_TYPE, CREATE_CLIENTVariables>(
    CREATE_CLIENT,
    {
      client: FEDERATION_CLIENT,
      onCompleted: () => {
        results.refetch();
      },
    },
  );

  useEffect(() => {
    searchUser({
      variables: {
        query: {
          ...query,
          sort: sortBy ? [{ [sortBy.column]: { order: sortBy.order } }] : [],
        },
        skip: skipAndTake.skip,
        take: skipAndTake.take,
      },
    });
  }, [query, skipAndTake, sortBy]);

  const debouncedSearchText = useRef(
    debounce((value: string) => {
      setTextSearch(value);
    }, 1500),
  );

  useEffect(() => {
    setDebouncedText(textSearch ? textSearch : '');
  }, [textSearch]);

  useEffect(() => {
    debouncedSearchText.current(debouncedText);
  }, [debouncedText]);

  const handleClick = (rowId: string, row: any) => (event: MouseEvent<unknown>) => {
    event.preventDefault();
    event.stopPropagation();
    if (setUserIdsSelected) {
      setUserIdsSelected(prev =>
        prev.includes(rowId) ? [...prev.filter(value => value !== rowId)] : [...prev, rowId],
      );
    }
    if (setUsersSelected) {
      setUsersSelected(prev =>
        prev.filter(user => user && row && user.id === row.id).length > 0
          ? [...prev.filter(value => value && row && value.id !== row.id)]
          : [...prev, row],
      );
    }
  };

  const updateSkipAndTake = (skip: number, take: number) => {
    setSkipAndTake({
      skip,
      take,
    });
  };

  const handleFilterChange = (event: any) => {
    const { value } = event.target;
    handleChecked(value);
    if (value === 'CLT') {
      setShowAddClientBtn(true);
    } else {
      setShowAddClientBtn(false);
    }
    setActiveFilter(value);
  };

  const handleSaveClient = (data: OutputDataClient) => {
    console.log(data);
    createClient({
      variables: {
        input: {
          ville: data.ville,
          nom: data.nom,
          prenom: data.prenom,
          cp: `${data.cp}`,
          email: data.email,
          pays: data.pays,
          tel: data.tel,
          adresse: data.adresse,
        },
      },
    });
  };

  return (
    <Box className={classes.container}>
      <Box className={classes.searchInputBox}>
        <SearchInput
          value={debouncedText}
          searchInFullWidth={'xl'}
          noPrefixIcon={true}
          onChange={onChangeSearchText}
          placeholder={'Entrer vos critères de recherche'}
          outlined={true}
        />
      </Box>
      <CssBaseline />
      <Hidden mdUp={true} implementation="css">
        <Box className={classes.searchInputBox}>
          <CustomSelect
            noMinheight
            label="Type de contact"
            list={filterColumns}
            listId="code"
            index="name"
            name="code"
            value={activeFilter}
            onChange={handleFilterChange}
            required={false}
          />
        </Box>
        <Box display={showAddClientBtn ? 'block' : 'none'} width="100%" marginBottom={2}>
          <CustomButton onClick={onAddClient} startIcon={<Add />} color="secondary">
            AJOUTER UN NOUVEAU CLIENT
          </CustomButton>
        </Box>
      </Hidden>

      <Table
        columns={columns}
        handleClick={handleClick}
        updateSkipAndTake={updateSkipAndTake}
        data={
          results && results.data && results.data.search && results.data.search.data
            ? results.data.search.data
            : []
        }
        total={
          results && results.data && results.data.search && results.data.search.total
            ? results.data.search.total
            : 0
        }
        userIdsSelected={userIdsSelected}
        skipAndTake={skipAndTake}
        sortBy={sortBy}
        setSortBy={setSortBy}
        allUsers={allUsers || []}
        setUserIdsSelected={setUserIdsSelected}
        setUsersSelected={setUsersSelected}
        usersSelected={usersSelected}
      />
      <ModalFormAddClient
        onSave={handleSaveClient}
        open={modalAddClient?.open || false}
        setOpen={modalAddClient?.setOpen as () => void}
      />
    </Box>
  );
};

export default withRouter(Users);
