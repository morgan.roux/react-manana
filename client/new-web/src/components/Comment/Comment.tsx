import { useApolloClient, useMutation, useQuery } from '@apollo/client';
import { AxiosResponse } from 'axios';
import { uniqBy } from 'lodash';
import React, { FC, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { ContentContext, ContentStateInterface } from '../../AppContext';
import { DO_CREATE_COMMENT, GET_COMMENTS } from '../../graphql/Comment';
import { COMMENTS, COMMENTSVariables } from '../../graphql/Comment/types/COMMENTS';
import {
  CREATE_COMMENT,
  CREATE_COMMENTVariables,
} from '../../graphql/Comment/types/CREATE_COMMENT';
import { GET_CURRENT_PHARMACIE } from '../../graphql/Pharmacie/local';
import { MY_PHARMACIE_me_pharmacie } from '../../graphql/Pharmacie/types/MY_PHARMACIE';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { SEARCH as SEARCH_QUERY } from '../../graphql/search/query';
import ICurrentPharmacieInterface from '../../Interface/CurrentPharmacieInterface';
import { AppAuthorization } from '../../services/authorization';
import { getGroupement, getUser, setPharmacie } from '../../services/LocalStorage';
import { uploadToS3 } from '../../services/S3';
import { FichierInput, TypeFichier } from '../../types/graphql-global-types';
import { formatFilenameWithoutDate } from '../../utils/filenameFormater';
import { displaySnackBar } from '../../utils/snackBarUtils';
import CommentForm from './CommentForm';
import CommentInterface from './Interface/CommentInterface';

interface CommentProps {
  codeItem: string;
  idSource: string;
  withAttachement?: boolean;
  rows?: number;
  forwardedInputRef?: any;
  refetch?: () => void;
  dispatchSubmitCb?: (dispatcherSubmit: () => void) => void;
}

const Comment: FC<CommentProps & RouteComponentProps> = ({
  codeItem,
  idSource,
  location: { pathname },
  forwardedInputRef,
  refetch,
  withAttachement = false,
  rows = 4,
  dispatchSubmitCb,
}) => {
  // take currentPharmacie
  const [currentPharmacie, setCurrentPharmacie] = useState<MY_PHARMACIE_me_pharmacie | null>(null);
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const user = getUser();
  const auth = new AppAuthorization(user);

  const client = useApolloClient();

  const [selectedFiles, setSelectedFiles] = React.useState<File[]>([]);

  const [mutationSucces, setMutationSucces] = useState<boolean>(false);

  const grp = getGroupement();
  const idGrp = (grp && grp.id) || '';

  let uploadResult: AxiosResponse<any> | null = null;
  let files: FichierInput[] = [];

  const [loading, setLoading] = useState(false);

  const [commentData, setCommentData] = useState<CommentInterface | null>(null);

  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  useEffect(() => {
    if (myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie) {
      setCurrentPharmacie(myPharmacie.data.pharmacie);
      setPharmacie(myPharmacie.data.pharmacie);
    }
  }, [myPharmacie]);

  const [doCreateComment, { loading: cLoading }] = useMutation<
    CREATE_COMMENT,
    CREATE_COMMENTVariables
  >(DO_CREATE_COMMENT, {
    onCompleted: data => {
      if (data && data.createComment) {
        setSelectedFiles([]);
        setLoading(false);
        setMutationSucces(true);
        if (refetch) refetch();
      }
    },
    update: (cache, { data, errors }) => {
      console.log('errors :>> ', errors);

      if (data && data.createComment) {
        /**
         * Update search query
         */
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables,
          });

          if (req && req.search && req.search.data) {
            cache.writeQuery({
              query: SEARCH_QUERY,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: req.search.data.map((item: any) => {
                      if (item && data && data.createComment && item.id === idSource) {
                        item = {
                          ...item,
                          comments: {
                            total: item.comments.total + 1,
                            __typename: 'CommentResult',
                          },
                        };
                        return item;
                      } else {
                        return item;
                      }
                    }),
                  },
                },
              },
              variables,
            });
          }
        }

        /**
         * Update get comment query
         */
        const query = cache.readQuery<COMMENTS, COMMENTSVariables>({
          query: GET_COMMENTS,
          variables: {
            codeItem,
            idItemAssocie: idSource,
            take: 10,
            skip: 0,
          },
        });
        if (query && query.comments) {
          cache.writeQuery({
            query: GET_COMMENTS,
            variables: {
              codeItem,
              idItemAssocie: idSource,
              take: 10,
              skip: 0,
            },
            data: {
              ...query,
              comments: {
                ...query.comments,
                total: query.comments.total + 1,
                data: [...(query.comments.data as any), data.createComment],
              },
            },
          });
        }
      }
    },
    onError: error => {
      setLoading(false);
      console.log('error :>> ', error);
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const [doPresignedUrl, { loading: presignedLoading }] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (data && data.createPutPresignedUrls && data.createPutPresignedUrls.length > 0) {
        Promise.all(
          data.createPutPresignedUrls.map(async (presigned, index) => {
            if (presigned && selectedFiles.length > 0) {
              await Promise.all(
                selectedFiles.map(async file => {
                  if (presigned.filePath.includes(formatFilenameWithoutDate(file))) {
                    const type: TypeFichier = file.type.includes('pdf')
                      ? TypeFichier.PDF
                      : file.type.includes('xlsx')
                      ? TypeFichier.EXCEL
                      : TypeFichier.PHOTO;
                    const newFile: FichierInput = {
                      type,
                      nomOriginal: file.name,
                      chemin: presigned.filePath,
                    };

                    files = uniqBy([...files, newFile], 'chemin');

                    await uploadToS3(selectedFiles[index], presigned.presignedUrl)
                      .then(result => {
                        if (result && result.status === 200) {
                          uploadResult = result;
                        }
                      })
                      .catch(error => {
                        setLoading(false);
                        console.log('error :>> ', error);
                        displaySnackBar(client, {
                          type: 'ERROR',
                          message: "Erreur lors de l'envoye de(s) fichier(s)",
                          isOpen: true,
                        });
                      });
                  }
                }),
              );
            }
          }),
        ).then(async () => {
          if (uploadResult && uploadResult.status === 200) {
            // Save comment
            if (files && files.length > 0 && commentData) {
              doCreateComment({
                variables: {
                  input: { ...commentData, codeItem, idItemAssocie: idSource, fichiers: files },
                },
              });
            }
          }
        });
      }
    },
    onError: error => {
      console.log('error :>> ', error);
      setLoading(false);
      displaySnackBar(client, {
        type: 'ERROR',
        message: "Erreur lors de l'envoye de(s) fichier(s)",
        isOpen: true,
      });
    },
  });

  const submitCreate = (data: CommentInterface) => {
    setCommentData(data);
    if (selectedFiles.length > 0) {
      setLoading(true);
      const filePaths: string[] = [];
      selectedFiles.map((file: File) => {
        filePaths.push(`comments/${idGrp}/${formatFilenameWithoutDate(file)}`);
      });
      doPresignedUrl({ variables: { filePaths } });
    } else {
      doCreateComment({ variables: { input: { ...data, codeItem, idItemAssocie: idSource } } });
    }
  };

  const isOnActu = pathname.includes('/actualite');
  const isOnOC = pathname.includes('/operation');

  const showCommentForm = (): boolean => {
    if (isOnActu) return auth.isAuthorizedToCommentActu();
    if (isOnOC) return auth.isAuthorizedToCommentOC();
    return true;
  };

  return showCommentForm() ?  (
    <CommentForm
      ref={forwardedInputRef}
      submit={submitCreate}
      loading={loading || cLoading || presignedLoading}
      withAttachement={withAttachement}
      rows={rows}
      selectedFiles={selectedFiles}
      setSelectedFiles={setSelectedFiles}
      mutationSucces={mutationSucces}
      dispatchSubmitCb={dispatchSubmitCb}
    />
  ) : null;
};

export default withRouter(Comment);
