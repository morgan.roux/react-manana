import { FichierInput } from '../../../types/graphql-global-types';

export default interface CommentInterface {
  content: string;
  fichiers?: FichierInput[];
}
