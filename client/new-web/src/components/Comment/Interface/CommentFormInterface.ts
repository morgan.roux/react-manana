import { Dispatch, SetStateAction } from 'react';
import CommentInterface from './CommentInterface';

export default interface CommentFormInterface {
  forwardedInputRef?: any;
  defaultState?: CommentInterface;
  loading?: boolean;
  withAttachement?: boolean;
  rows?: number;
  selectedFiles?: File[];
  setSelectedFiles?: Dispatch<SetStateAction<File[]>>;
  mutationSucces?: boolean;
  submit: (data: CommentInterface) => void;
  dispatchSubmitCb?: (dispatcherSubmit: () => void) => void
}
