import React, { FC } from 'react';
import { List, Link } from '@material-ui/core';
import CommentItem from '../CommentItem';
import { CommentResultInfo } from '../../../graphql/Comment/types/CommentResultInfo';
import { LoaderSmall } from '../../Dashboard/Content/Loader';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

interface CommentListProps {
  comments: CommentResultInfo | any;
  fetchMoreComments?: () => void;
  loading?: boolean;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    linkAndLoadingContainer: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    link: {
      color: '#000',
    },
    loader: {
      textAlign: 'right',
    },
  }),
);

const CommentList: FC<CommentListProps> = ({ comments, fetchMoreComments, loading }) => {
  const classes = useStyles({});

  return (
    <>
      <div className={classes.linkAndLoadingContainer}>
        {fetchMoreComments && comments && comments.data && comments.data.length < comments.total && (
          <Link component="button" className={classes.link} onClick={fetchMoreComments}>
            Afficher plus de commentaires
          </Link>
        )}
        <div className={classes.loader}>{loading && <LoaderSmall />}</div>
      </div>
      <List>
        {comments &&
          comments.data &&
          comments.data.map((comment: any) => <CommentItem key={comment.id} comment={comment} />)}
      </List>
    </>
  );
};

export default CommentList;
