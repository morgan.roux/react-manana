import {
  Avatar,
  Box,
  IconButton,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from '@material-ui/core';
import { AccountCircle, Visibility } from '@material-ui/icons';
import classnames from 'classnames';
import moment from 'moment';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { CommentResultInfo_data } from '../../../graphql/Comment/types/CommentResultInfo';
import { stringToAvatar } from '../../../utils/Helpers';
import { AWS_HOST } from '../../../utils/s3urls';
import useCommonStyles from '../../Main/Content/Messagerie/SideBar/FormMessage/styles';
import { useStyles } from './styles';

interface CommentItemProps {
  comment: CommentResultInfo_data | null;
  isVisibile?: boolean;
  onClickVisibility?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const CommentItem: FC<CommentItemProps & RouteComponentProps> = ({
  comment,
  isVisibile,
  onClickVisibility,
}) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const userPhotoUrl =
    comment &&
    comment.user &&
    comment.user.userPhoto &&
    comment.user.userPhoto.fichier &&
    comment.user.userPhoto.fichier.publicUrl;

  const userRole = (comment && comment.user && comment.user.role && comment.user.role.nom) || '';
  const userPharmacie =
    (comment && comment.user && comment.user.pharmacie && comment.user.pharmacie.nom) || '';

  const userName = comment && comment.user && comment.user.userName;

  // Subtract 30s for different on server and client
  const dateCreation =
    comment &&
    moment(comment.dateCreation)
      .subtract(30, 'seconds')
      .fromNow();

  const fichiers = (comment && comment.fichiers) || [];

  const openFile = (url: string) => () => {
    window.open(url, '_blank');
  };

  return (
    <ListItem className={classes.contentListItem}>
      <ListItemAvatar className={classes.listItemAvatar}>
        {userPhotoUrl ? (
          <img src={userPhotoUrl} alt="User Avatar" className={classes.bigAvatar} />
        ) : userName ? (
          <Avatar className={classes.bigAvatar}>{stringToAvatar(userName)}</Avatar>
        ) : (
          <AccountCircle />
        )}
      </ListItemAvatar>
      <ListItemText className={classes.contentMessage}>
        <Box display="flex" justifyContent="space-between">
          <Typography className={classes.userName}>
            {comment && comment.user ? comment.user.userName : ''}
          </Typography>
          <Box className={classes.commentDate}>
            <Typography variant="caption">{dateCreation ? dateCreation : ''} </Typography>
            {isVisibile && (
              // tslint:disable-next-line: jsx-no-lambda
              <IconButton onClick={onClickVisibility}>
                <Visibility />
              </IconButton>
            )}
          </Box>
        </Box>
        <Box className={classes.commentHeader}>
          <Typography className={classes.fonctionName}>{userRole}</Typography>
          {userPharmacie && (
            <Box marginTop="7px" display="flex" alignItems="center">
              <Typography className={classes.labelPharmacie}>Pharmacie :</Typography>
              <Typography className={classes.namePharmacie}>{userPharmacie}</Typography>
            </Box>
          )}
        </Box>
        <Typography variant="body2" className={classes.commentText}>
          {comment ? `" ${comment.content} "` : ''}
        </Typography>

        {fichiers && fichiers.length > 0 && (
          <Box className={commonClasses.filesContainer} marginLeft="0px !important">
            {fichiers.map((file, index) => (
              <Box
                key={`${file.nomOriginal}_${index}`}
                className={classnames(commonClasses.fileItem, classes.fileContainer)}
                onClick={openFile(`${AWS_HOST}/${file.chemin}`)}
              >
                <Box className={commonClasses.filenameContainer} title={file.nomOriginal}>
                  <Typography>{file.nomOriginal}</Typography>
                </Box>
              </Box>
            ))}
          </Box>
        )}
      </ListItemText>
    </ListItem>
  );
};

export default withRouter(CommentItem);
