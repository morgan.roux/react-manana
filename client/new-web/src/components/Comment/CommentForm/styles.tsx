import { makeStyles, Theme, createStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => {
  const border = `0.6px solid ${theme.palette.common.black}`;

  return createStyles({
    formContainer: {
      display: 'flex',
      flexDirection: 'column',
      borderRadius: 4,
      border,
      [theme.breakpoints.down('md')]: {
        margin: 8,
      },
    },
    textFieldNoBorder: {
      '& textarea': {
        border: 'none !important',
        [theme.breakpoints.down('md')]: {
          height: '100%',
        },
      },
      [theme.breakpoints.down('md')]: {
        height: '60px',
      },
    },
    textFieldWeb: {
      '& textarea': {
        padding: 20,
        borderRadius: 4,
        border,
      },
      '& > .MuiInput-underline:before': {
        borderBottom: 0,
      },
    },
    textFiledMobile: {
      '& textarea': {
        height: 20,
        padding: 10,
        borderRadius: 4,
        background: theme.palette.grey[200],
      },
    },
    inputFileContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      borderRadius: 4,
      border,
      borderBottom: 0,
      borderLeft: 0,
      borderRight: 0,
      '& button': {
        textTransform: 'none',
        minWidth: 'fit-content',
      },
    },
    file: {
      margin: '5px 0px 5px 5px',
    },
  });
});

export default useStyles;
