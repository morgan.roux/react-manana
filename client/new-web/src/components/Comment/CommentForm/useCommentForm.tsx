import { useState, FormEvent, ChangeEvent, MouseEvent } from 'react';
import CommentInterface from '../Interface/CommentInterface';

const useCommentForm = (
  callback: (data: CommentInterface) => void,
  defaultState?: CommentInterface,
) => {
  const initialState: CommentInterface = defaultState || { content: '' };
  const [state, setState] = useState<CommentInterface>(initialState);

  const handleSubmit = async (
    e: any //FormEvent<HTMLFormElement> & MouseEvent<HTMLAnchorElement & HTMLButtonElement>,
  ) => {
    if (e) e.preventDefault();
    await Promise.resolve(callback(state));
    setState(prevState => ({ ...prevState, content: '', fichiers: [] }));
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setState(prevState => ({ ...prevState, [name]: value }));
  };

  return { handleChange, handleSubmit, state, setState };
};

export default useCommentForm;
