import React, { FC, CSSProperties } from 'react';
import { Alert } from '@material-ui/lab';
interface CustomAlertDonneesMedicalesProps {
    message?: string;
    style?: CSSProperties
}

const CustomAlertDonneesMedicales: FC<CustomAlertDonneesMedicalesProps> = ({ style, message = "Veuilliez à ne pas échanger des données médicales des patients via ce canal de communication!" }) => {
    return (
        <Alert style={style} severity="warning">{message}</Alert>
    )
};
// à ne pas échanger des données médicales des patients
export default CustomAlertDonneesMedicales;
