import React, { FC, useState, useMemo } from 'react';
import useStyles from './styles';
import { Checkbox, CircularProgress, Box, Typography, Avatar } from '@material-ui/core';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { stringToAvatar } from '../../../utils/Helpers';
interface CustomAvatarProps {
  name: string;
  url?: string;
  width?: string | undefined;
}

const CustomAvatar: FC<CustomAvatarProps & RouteComponentProps> = ({ name, url, width }) => {
  const classes = useStyles({});
  return url ? (
    <Avatar src={url} className={classes.avatar} />
  ) : (
    <Avatar className={classes.avatar} sizes={width}>
      {name && stringToAvatar(name)}
    </Avatar>
  );
};

export default withRouter(CustomAvatar);
