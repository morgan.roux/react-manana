import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, Theme } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import classnames from 'classnames';

const style = (theme: Theme) => ({
  inputPassword: {
    '& input': {
      color: theme.palette.common.white,
    },
    '& label': {
      color: theme.palette.common.white,
    },
    '& label.Mui-focused': {
      color: theme.palette.primary.main,
    },
    '& input:invalid + fieldset': {
      // borderColor: theme.palette.common.white,
    },
    '& .Mui-focused .MuiIconButton-label svg': {
      color: `${theme.palette.primary.main} !important`,
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: theme.palette.common.white,
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: theme.palette.common.white,
      },
      '&:hover fieldset': {
        borderColor: theme.palette.primary.main,
      },
      '&.Mui-focused fieldset': {
        borderColor: theme.palette.primary.main,
      },
      '&:hover .MuiOutlinedInput-notchedOutline': {
        borderColor: theme.palette.primary.main,
      },
    },
    marginBottom: 12,
  },
});

const CustomTextField = ({ ...props }) => {
  const { classes, name, label, variant, fullWidth, className, ...inputProps } = props;

  return (
    <TextField
      {...props}
      color="secondary"
      InputLabelProps={{
        shrink: true,
      }}
      {...inputProps}
      className={className ? classnames(classes.inputPassword, className) : classes.inputPassword}
    />
  );
};

export default withStyles(style)(CustomTextField);

CustomTextField.propTypes = {
  classes: PropTypes.object.isRequired,
  label: PropTypes.string,
  name: PropTypes.string,
  variant: PropTypes.string,
  fullWidth: PropTypes.bool,
  inputProps: PropTypes.object,
};

CustomTextField.defaultProps = {
  label: '',
  name: '',
  variant: 'outlined',
  fullWidth: true,
  inputProps: {},
};
