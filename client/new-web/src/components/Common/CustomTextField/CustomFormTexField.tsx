import React, { FC } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import TextField, { TextFieldProps } from '@material-ui/core/TextField';
import classnames from 'classnames';

interface CustomFormTextFieldProps {
  shrink?: boolean;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    inputText: {
      '& input': {
        color: theme.palette.common.black,
      },
      '& label': {
        // color: theme.palette.common.black,
        '& .MuiFormLabel-asterisk': {
          color: 'red',
        },
      },
      '& label.Mui-focused, &:hover label': {
        color: theme.palette.primary.main,
      },
      '& input:invalid + fieldset': {
        // borderColor: 'red',
      },
      '& .Mui-focused .MuiIconButton-label svg': {
        color: `${theme.palette.primary.main} !important`,
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: theme.palette.common.white,
      },
      '& .MuiOutlinedInput-root': {
        '&:hover fieldset': {
          // borderColor: theme.palette.primary.main,
        },
        '&.Mui-focused fieldset': {
          borderColor: theme.palette.primary.main,
        },
      },
      marginBottom: 12,
    },
  }),
);

const CustomFormTextField: FC<TextFieldProps & CustomFormTextFieldProps> = ({
  shrink,
  inputRef,
  className,
  InputProps,
  inputProps,
  rows,
  type,
  ...props
}) => {
  const classes = useStyles({});
  return (
    <TextField
      {...props}
      InputLabelProps={{
        shrink,
      }}
      inputProps={inputProps}
      className={classnames(className, classes.inputText)}
      InputProps={InputProps}
      type={type}
      inputRef={inputRef}
      rows={rows}
    />
  );
};

CustomFormTextField.defaultProps = {
  label: '',
  name: undefined,
  color: 'primary',
  variant: 'outlined',
  fullWidth: true,
  inputProps: {},
  shrink: true,
};

export default CustomFormTextField;
