import { useLazyQuery } from '@apollo/client';
import { Box, CircularProgress, IconButton, TextField, Typography } from '@material-ui/core';
import { Group, PersonAdd } from '@material-ui/icons';
import React, { ChangeEvent, FC, useEffect } from 'react';
import { GET_COLLABORATEURS } from '../../../federation/iam/user/query';
import {
  GET_COLLABORATEURS as GET_COLLABORATEURS_TYPE,
  GET_COLLABORATEURSVariables,
} from '../../../federation/iam/user/types/GET_COLLABORATEURS';
import { getPharmacie } from '../../../services/LocalStorage';
import { useParameter } from '../../../utils/getValueParameter';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import AvatarInput, { AvatarInputProps } from '../AvatarInput';
import CustomAutocomplete from '../CustomAutocomplete';
import CustomAvatar from '../CustomAvatar';
import CustomSelectUser, { UsersModalProps } from '../CustomSelectUser';
import { CustomFormTextField } from '../CustomTextField';
import { useStyles } from './styles';
export interface UserAutocompleteProps {
  label?: string;
  required?: boolean;
  selected: any;
  onAutocompleteChange: (inputValue: any) => void;
}
interface UserInputProps extends UsersModalProps {
  className?: string;
  label?: string;
  disabled?: boolean;
  codeParameter?: string;
  inputProps?: Partial<AvatarInputProps>;
  mode?: 'modal' | 'autocomplete';
  autoCompleteProps?: UserAutocompleteProps;
  onValidate?: (users: any[]) => void;
}
const UserInput: FC<UserInputProps> = ({
  mode,
  className,
  selected,
  openModal,
  disabled,
  setOpenModal,
  onValidate,
  label,
  isCheckedTeam,
  codeParameter,
  inputProps = {},
  autoCompleteProps,
  ...rest
}) => {
  const optionPartage = useParameter(codeParameter || '')?.value?.value;
  const allTeamOption = { id: 'all', fullName: "Toute l'équipe" };
  const pharmacie = getPharmacie();
  const classes = useStyles();

  const [getCollaborateurs, gettingCollaborateurs] = useLazyQuery<
    GET_COLLABORATEURS_TYPE,
    GET_COLLABORATEURSVariables
  >(GET_COLLABORATEURS, { client: FEDERATION_CLIENT });

  const collaborateurs = gettingCollaborateurs.data?.collaborateurs;

  useEffect(() => {
    if (mode === 'autocomplete' && !gettingCollaborateurs.called && pharmacie) {
      getCollaborateurs({ variables: { idPharmacie: pharmacie.id } });
    }
  }, [pharmacie]);

  return (
    <Box className={className}>
      {(!mode || mode === 'modal') && (
        <>
          <AvatarInput
            isAllTeamSelected={isCheckedTeam}
            label={label || 'Collaborateur'}
            list={selected || []}
            icon={
              disabled ? (
                undefined
              ) : (
                <IconButton onClick={() => setOpenModal(true)}>
                  <PersonAdd />
                </IconButton>
              )
            }
            {...inputProps}
          />
          <CustomSelectUser
            {...rest}
            onValidate={onValidate}
            selected={selected}
            isCheckedTeam={isCheckedTeam}
            openModal={openModal}
            setOpenModal={setOpenModal}
            optionPartage={codeParameter ? optionPartage : 'MON_EQUIPE'}
          />
        </>
      )}
      {autoCompleteProps && (
        <CustomAutocomplete
          id="idUserAutocomplete"
          loading={gettingCollaborateurs.loading}
          options={collaborateurs ? [allTeamOption, ...collaborateurs] : []}
          optionLabelKey="fullName"
          defaultValue={allTeamOption}
          value={autoCompleteProps.selected}
          onAutocompleteChange={autoCompleteProps.onAutocompleteChange}
          label={autoCompleteProps.label || ''}
          required={autoCompleteProps.required || false}
          renderInput={params => (
            <CustomFormTextField
              {...params}
              label={label}
              InputProps={{
                ...params.InputProps,
                endAdornment: <React.Fragment>{params.InputProps.endAdornment}</React.Fragment>,
              }}
            />
          )}
          renderOption={option =>
            option.id !== 'all' ? (
              <Box className={classes.autoCompleteInput}>
                <CustomAvatar
                  key={option.id}
                  url={option.photo?.publicUrl}
                  name={option.userName || option.fullName || ''}
                />
                <Typography>{option.userName || option.fullName || ''}</Typography>
              </Box>
            ) : (
              <Box className={classes.allTeamInput}>
                <Group />
                <Typography>{option.userName || option.fullName || ''}</Typography>
              </Box>
            )
          }
        />
      )}
    </Box>
  );
};

export default UserInput;
