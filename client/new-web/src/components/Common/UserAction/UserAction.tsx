import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import { Box, Typography } from '@material-ui/core';
import ShareIcon from '@material-ui/icons/Share';
import LikeIcon from '@material-ui/icons/ThumbUp';
import classnames from 'classnames';
import React, { FC, MouseEvent, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { LoaderSmall } from '@app/ui-kit'
import { ContentContext, ContentStateInterface } from '../../../AppContext';
import { GET_SEARCH_CUSTUM_PARTAGE } from '../../../graphql/Partage/query';
import { DO_CREATE_USER_SMYLEY } from '../../../graphql/Smyley';
import {
  CREATE_USER_SMYLEY,
  CREATE_USER_SMYLEYVariables,
} from '../../../graphql/Smyley/types/CREATE_USER_SMYLEY';
import { SMYLEYS_smyleys } from '../../../graphql/Smyley/types/SMYLEYS';
import { GET_USER_SMYLEYS } from '../../../graphql/Smyley';
import {
  USER_SMYLEYS as USER_SMYLEYS_TYPE,
  USER_SMYLEYSVariables,
} from '../../../graphql/Smyley/types/USER_SMYLEYS';

import { USER_SMYLEYS_userSmyleys } from '../../../graphql/Smyley/types/USER_SMYLEYS';
import SnackVariableInterface from '../../../Interface/SnackVariableInterface';
import { AppAuthorization } from '../../../services/authorization';
import { getUser } from '../../../services/LocalStorage';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import Partage from '../../Main/Content/Friends/Modals/PartageFriends';
import useStyles from './styles';
import UserActionInfo from './UserActionInfo';
import { ListPerson } from './../ListPerson'
import { CustomModal } from '../CustomModal';
import CustomAvatar from '../CustomAvatar'
import { AWS_HOST } from '../../../config'


interface UserActionInfoProps {
  codeItem: string;
  idSource: string;
  nbSmyley?: number;
  nbComment?: number;
  nbShare?: number;
  idArticle?: string;
  userSmyleys?: USER_SMYLEYS_userSmyleys;
  presignedUrls?: any;
  smyleys?: any;
  actualite?: any;
  refetchSmyleys?: () => void;
  hidePartage?: boolean;
  hideRecommandation?: boolean;
}
const UserAction: FC<UserActionInfoProps & RouteComponentProps> = ({
  codeItem,
  idSource,
  nbComment,
  nbShare,
  nbSmyley,
  idArticle,
  history,
  location: { pathname },
  userSmyleys,
  presignedUrls,
  smyleys,
  actualite,
  refetchSmyleys,
  hidePartage,
  hideRecommandation,
}) => {
  const classes = useStyles({});
  const [smylesPresigned, setSmylesPresigned] = useState<SMYLEYS_smyleys[]>([]);
  const [showEmojisContainer, setShowEmojisContainer] = useState<boolean>(false);
  const [openReactionsModal, setOpenReactionsModal] = useState<boolean>(false)

  const [currentUserSmyleys, setcurrentUserSmyleys] = useState<
    USER_SMYLEYS_userSmyleys | undefined
  >(userSmyleys);

  const [loadUserSmyleys, loadingUserSmyleys] = useLazyQuery<USER_SMYLEYS_TYPE, USER_SMYLEYSVariables>(
    GET_USER_SMYLEYS,
    {
      fetchPolicy: 'cache-and-network'
    }
  );

  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const currentUser = getUser();
  const currentUserSmyley =
    currentUserSmyleys &&
    currentUserSmyleys.data &&
    currentUserSmyleys.data.find(
      userSmyley => userSmyley && userSmyley.user && userSmyley.user.id === currentUser.id,
    );

  const auth = new AppAuthorization(currentUser);

  const client = useApolloClient();

  const { data, loading, refetch: refetchPartages } = useQuery<any, any>(
    GET_SEARCH_CUSTUM_PARTAGE,
    {
      fetchPolicy: 'cache-and-network',
      variables: {
        type: ['partage'],
        query: {
          query: {
            bool: {
              must: [{ term: { isRemoved: false } }, { term: { idItemAssocie: idSource } }],
            },
          },
        },
        take: 100,
        skip: 0,
      },
      onError: errors => {
        errors.graphQLErrors.map(err => {
          const snackBarData: SnackVariableInterface = {
            type: 'ERROR',
            message: err.message,
            isOpen: true,
          };
          displaySnackBar(client, snackBarData);
        });
      },
      onCompleted: data => {
        console.log('data', data);
      },
    },
  );

  const isOnActu = pathname.includes('/actualite');
  const isOnOC = pathname.includes('/operations');
  const isOnProduct = pathname.includes('/catalogue-produits');

  const focusToFormInput = () => {
    const input = document.getElementById('comment-input');
    if (input) input.focus();
  };

  const goToDetails = () => {
    if (idArticle && pathname !== `/catalogue-produits/card/${idArticle}`) {
      history.push(`/catalogue-produits/card/${idArticle}`);
    }
    focusToFormInput();
  };

  const handleOverLike = (event: React.MouseEvent<any>) => {
    setShowEmojisContainer(true);
  };

  const handleCloseSmyleysContainer = () => {
    setShowEmojisContainer(false);
  };

  // Set presigned smyleys
  useEffect(() => {
    const newSmyleys: SMYLEYS_smyleys[] = [];
    if (presignedUrls) {
      presignedUrls.map((file: any) => {
        if (smyleys) {
          smyleys.map((smyley: any) => {
            if (file && smyley) {
              if (file.filePath === smyley.photo) {
                const newFile: SMYLEYS_smyleys = {
                  ...smyley,
                  photo: file.presignedUrl,
                };
                newSmyleys.push(newFile);
              }
            }
          });
        }
      });
    }
    setSmylesPresigned(newSmyleys);
  }, [presignedUrls]);

  useEffect(() => {
    setcurrentUserSmyleys(userSmyleys);
  }, [userSmyleys]);
  const handleOpenModalPartage = () => {
    setopenmodalpartage(!openmodalpartage);
  };
  console.log('partage', data?.search?.data);

  const numberRecommandation = () => {
    // const numbre;
    // data?.search?.data?.filter(item => item&&item.typePartage==="PArtage").length
  };

  const [doCreateUserSmyley] = useMutation<CREATE_USER_SMYLEY, CREATE_USER_SMYLEYVariables>(
    DO_CREATE_USER_SMYLEY,
    {
      update: (cache, { data }) => {
        if (data && data.createUserSmyley) {
          const isRemoved = data.createUserSmyley.isRemoved;
          const uniqueUser =
            currentUserSmyleys &&
            currentUserSmyleys.data &&
            currentUserSmyleys.data
              .map((smyley: any) => smyley && smyley.user && smyley.user.id)
              .includes(
                data.createUserSmyley &&
                data.createUserSmyley.user &&
                data.createUserSmyley.user.id,
              );
          if (variables && operationName) {
            const req: any = cache.readQuery({
              query: operationName,
              variables,
            });
            if (req && req.search && req.search.data) {
              cache.writeQuery({
                query: operationName,
                data: {
                  search: {
                    ...req.search,
                    ...{
                      data: req.search.data.map((item: any) => {
                        if (item && data && data.createUserSmyley && item.id === idSource) {
                          item.userSmyleys = {
                            total: isRemoved
                              ? currentUserSmyleys && currentUserSmyleys.total - 1
                              : uniqueUser
                                ? currentUserSmyleys && currentUserSmyleys.total
                                : currentUserSmyleys && currentUserSmyleys.total + 1,
                            data: isRemoved
                              ? currentUserSmyleys &&
                              currentUserSmyleys.data &&
                              currentUserSmyleys.data.filter(
                                item =>
                                  item &&
                                  item.id &&
                                  data.createUserSmyley &&
                                  data.createUserSmyley.id &&
                                  item.id !== data.createUserSmyley.id,
                              )
                              : [
                                ...((currentUserSmyleys &&
                                  currentUserSmyleys.data &&
                                  currentUserSmyleys.data.filter(
                                    item =>
                                      item &&
                                      item.user &&
                                      data.createUserSmyley &&
                                      data.createUserSmyley.user &&
                                      data.createUserSmyley.user.id &&
                                      item.user.id !== data.createUserSmyley.user.id,
                                  )) ||
                                  []),
                                data.createUserSmyley,
                              ],
                            __typename: 'UserSmyleyResult',
                          };
                          return item;
                        } else {
                          return item;
                        }
                      }),
                    },
                  },
                },
                variables,
              });
            }
          }
        }
      },
      onCompleted: data => {
        if (data && data.createUserSmyley) {
          const isRemoved = data.createUserSmyley.isRemoved;
          const uniqueUser =
            currentUserSmyleys &&
            currentUserSmyleys.data &&
            currentUserSmyleys.data
              .map((smyley: any) => smyley && smyley.user && smyley.user.id)
              .includes(
                data.createUserSmyley &&
                data.createUserSmyley.user &&
                data.createUserSmyley.user.id,
              );

          setcurrentUserSmyleys({
            total: isRemoved
              ? currentUserSmyleys && currentUserSmyleys.total - 1
              : uniqueUser
                ? currentUserSmyleys && currentUserSmyleys.total
                : currentUserSmyleys && currentUserSmyleys.total + 1,
            data: isRemoved
              ? currentUserSmyleys &&
              currentUserSmyleys.data &&
              currentUserSmyleys.data.filter(
                item =>
                  item &&
                  item.id &&
                  data.createUserSmyley &&
                  data.createUserSmyley.id &&
                  item.id !== data.createUserSmyley.id,
              )
              : [
                ...((currentUserSmyleys &&
                  currentUserSmyleys.data &&
                  currentUserSmyleys.data.filter(
                    item =>
                      item &&
                      item.user &&
                      data.createUserSmyley &&
                      data.createUserSmyley.user &&
                      data.createUserSmyley.user.id &&
                      item.user.id !== data.createUserSmyley.user.id,
                  )) ||
                  []),
                data.createUserSmyley,
              ],
          } as USER_SMYLEYS_userSmyleys);
          if (refetchSmyleys) refetchSmyleys();
        }
      },
    },
  );

  const handleClickSmyley = (smyley: SMYLEYS_smyleys) => (e: MouseEvent<any>) => {
    e.preventDefault();
    e.stopPropagation();
    handleCloseSmyleysContainer();
    if (smyley) {
      doCreateUserSmyley({
        variables: { codeItem, idSource, idSmyley: smyley.id },
      });
    }
  };

  const getSmyleyUrl = (id: string) => {
    const smyley = smylesPresigned.find(smyley => smyley.id === id);

    return smyley && smyley.photo;
  };
  const [openmodalpartage, setopenmodalpartage] = useState<boolean>(false);

  const like: SMYLEYS_smyleys | undefined =
    smylesPresigned && smylesPresigned.find(smyley => smyley && smyley.nom === "J'aime");

  const showBtnLike = (): boolean => {
    if (isOnActu) return auth.isAuthorizedToLikeActu();
    if (isOnOC) return auth.isAuthorizedToLikeOC();
    if (isOnProduct) return auth.isAuthorizedToLikeProduct();
    return true;
  };

  const handleCountClick = () => {
    loadUserSmyleys({
      variables: {
        idSource,
        codeItem
      }
    })

    setOpenReactionsModal(true)
  }

  const nbrecommandation = data?.search?.data?.filter(
    item => item && item.typePartage === 'RECOMMANDATION',
  ).length;


  const usersReactions = (loadingUserSmyleys.data?.userSmyleys?.data || [])

  return (
    <Box onMouseLeave={handleCloseSmyleysContainer}>
      {/* <hr className={classes.hr} /> */}
      <Partage
        openModal={openmodalpartage}
        handleClose={handleOpenModalPartage}
        idActualite={idSource}
        codeItem={codeItem}
        actualite={actualite}
        refetch={refetchPartages}
      />
      <Box className={classnames(classes.contentRS, classes.RobotoRegular, classes.medium)}>
        {currentUserSmyleys && currentUserSmyleys.total > 0 ? (
          <UserActionInfo
            nbComment={nbComment}
            nbSmyley={currentUserSmyleys.total}
            userSmyleys={currentUserSmyleys}
            onCountClick={handleCountClick}
          />
        ) : (
          <Box className={classes.smyleyContainer}>
            {smylesPresigned &&
              smylesPresigned.map(smyley => {
                return (
                  smyley &&
                  smyley.nom === "J'aime" && (
                    <img key={smyley.id} src={smyley.photo} alt={smyley.nom} />
                  )
                );
              })}
            <Box className={classes.nbSmyley}>
              {(currentUserSmyleys && currentUserSmyleys.total) || 0}
            </Box>
          </Box>
        )}

        {/*!hideRecommandation && (
          <Box className={classes.contentPartageRecommandation}>
            <Typography className={classes.small}>
              {data?.search?.total <= 1
                ? data?.search?.total + '  partage'
                : data?.search?.total + '  partages'}
            </Typography>
            <Typography className={classes.small}>
              {nbrecommandation <= 1
                ? nbrecommandation + '  recommandation'
                : nbrecommandation + '  recommandations'}
            </Typography>
          </Box>
              )*/}
      </Box>
      {showBtnLike() && (
        <Box className={classes.contentRS}>
          <Box
            className={classes.panierListeSocialItem}
            onClick={handleClickSmyley(like as any)}
            onMouseOver={handleOverLike}
          >
            {currentUserSmyley ? (
              <Box display="flex">
                {currentUserSmyley && currentUserSmyley.smyley && currentUserSmyley.smyley.id && (
                  <img src={getSmyleyUrl(currentUserSmyley.smyley.id)} />
                )}
                <Box className={classes.small} marginLeft="8px">
                  {currentUserSmyley && currentUserSmyley.smyley && currentUserSmyley.smyley.nom}
                </Box>
              </Box>
            ) : (
              <Box display="flex">
                <LikeIcon className={classes.panierListeSocialIcon} />
                <Box className={classes.small} marginLeft="8px">
                  J'aime
                </Box>
              </Box>
            )}

            {showEmojisContainer && (
              <Box className={classes.emojiContainer}>
                {smylesPresigned &&
                  smylesPresigned.map(smyley => {
                    return (
                      smyley && (
                        <Box key={smyley.id} onClick={handleClickSmyley(smyley)}>
                          <img src={smyley.photo} alt={smyley.nom} />
                        </Box>
                      )
                    );
                  })}
              </Box>

            )}
          </Box>
          {/*!hidePartage && (
            <Box className={classes.panierListeSocialItem}>
              <ShareIcon className={classes.panierListeSocialIcon} />
              <Box className={classes.small} marginLeft="8px" onClick={handleOpenModalPartage}>
                Partager
              </Box>
            </Box>
          )*/}
        </Box>
      )}
      <CustomModal
        open={openReactionsModal}
        setOpen={setOpenReactionsModal}
        title="Réactions"
        withBtnsActions={true}
        withCancelButton={false}
        actionButtonTitle="Fermer"
        closeIcon={true}
        headerWithBgColor={true}
        maxWidth="sm"
        fullWidth={true}
        // tslint:disable-next-line: jsx-no-lambda
        // tslint:disable-next-line: jsx-no-lambda
        onClick={event => {
          event.preventDefault();
          event.stopPropagation();
        }}

        onClickConfirm={() => setOpenReactionsModal(false)}
      >
        <Box width="100%" >
          <ListPerson actionsRenderer={(user) => {
            const userSmyley = usersReactions.find(us => us?.user?.id && us.user.id === user.id)
            return (<Box>
              <img src={`${AWS_HOST}/${userSmyley?.smyley?.photo}`} />
            </Box>)
          }} onNext={() => console.log('next')} listes={usersReactions.map((us) => ({ ...(us?.user || {}), nom: us?.user?.userName, src: us?.user?.userPhoto?.fichier?.publicUrl }))} loading={loadingUserSmyleys.loading} />
        </Box>
      </CustomModal>
    </Box>
  );
};

export default withRouter(UserAction);
