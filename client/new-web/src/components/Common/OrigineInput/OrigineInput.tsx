import { useQuery } from '@apollo/client';
import { Box } from '@material-ui/core';
import React, { FC, useState, useEffect } from 'react';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import CustomSelect from '../CustomSelect';
import { CustomTextField } from '@app/ui-kit';
import PartenaireInput from '../PartenaireInput';
import UserInput from '../UserInput';
import useOrigines from './../../hooks/basis/useOrigines'
import useStyles from './style';

interface OrigineInputProps {
  origine?: any; // id or origine instance
  origineAssocie: any; // id or origineAssocie
  onChangeOrigine: (origine: any) => void;
  onChangeOrigineAssocie: (origineAssocie: any) => void;
  readonly?: boolean;
}
const OrigineInput: FC<OrigineInputProps> = ({
  origineAssocie,
  origine,
  onChangeOrigine,
  onChangeOrigineAssocie,
  readonly,
}) => {
  const classes = useStyles();

  const [openSelectUserModal, setOpenSelectUserModal] = useState<boolean>(false);
  const [currentOrigine, setCurrentOrigine] = useState<any>()
  const origines = useOrigines()


  useEffect(() => {
    setCurrentOrigine(origine
      ? typeof origine === 'string'
        ? (origines.data?.origines.nodes || []).find(o => o.id === origine)
        : origine
      : undefined)
  }, [origine, origines.loading])


  const isInterne = currentOrigine && currentOrigine.code === 'INTERNE';
  const isLaboratoire = currentOrigine && currentOrigine.code === 'LABORATOIRE';
  const isPrestataire = currentOrigine && currentOrigine.code === 'PRESTATAIRE_SERVICE';

  const handleChangeOrigine = (event: any) => {
    const foundOrigine = (origines.data?.origines.nodes || []).find(o => o.id === event.target.value);
    onChangeOrigineAssocie(isInterne ? [] : '');
    onChangeOrigine(foundOrigine);
  };

  /*  if (origines.loading) {
      return <SmallLoading />;
    }*/
  return (
    <Box display="flex" flexDirection="row" justifyContent="space-between" className={classes.root}>
      <CustomSelect
        loading={origines.loading}
        error={origines.error}
        className={classes.field}
        label="Origine"
        required={true}
        list={origines.data?.origines.nodes || []}
        name="origine"
        onChange={handleChangeOrigine}
        listId="id"
        index="libelle"
        value={currentOrigine ? currentOrigine.id : ''}
        placeholder="Choisir l'origine"
        withPlaceholder={true}
        inputProps={{
          readOnly: readonly,
        }}
      />
      {isInterne ? (
        <UserInput
          inputProps={{
            classes: {
              root: classes.userInputRoot,
              input: classes.userInputInput,
            },
          }}
          className={classes.userInput}
          label="Correspondant"
          title="Choix de correspondant"
          singleSelect={true}
          withAssignTeam={false}
          selected={origineAssocie}
          setSelected={users => onChangeOrigineAssocie(users)}
          openModal={openSelectUserModal}
          setOpenModal={setOpenSelectUserModal}
        />
      ) : isLaboratoire ? (
        <PartenaireInput
          key="laboratoire"
          index="laboratoire"
          label="Correspondant"
          value={origineAssocie}
          onChange={laboratoire => onChangeOrigineAssocie(laboratoire)}
        />
      ) : isPrestataire ? (
        <PartenaireInput
          key="partenaire"
          index="partenaire"
          label="Correspondant"
          value={origineAssocie}
          onChange={laboratoire => onChangeOrigineAssocie(laboratoire)}
        />
      ) : (
        <CustomTextField
          label="Correspondant"
          variant="outlined"
          disabled={true}
          onChange={() => console.log('Do nothing')}
          value={''}
          InputLabelProps={{
            shrink: true,
          }}
        />
      )}
    </Box>
  );
};

export default OrigineInput;
