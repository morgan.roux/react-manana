import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    btn: {
      fontWeight: 'bold',
      margin:5,
      height:50
    },
    closeButton: {
      color: 'inherit',
    },
    button: {
      width:'100%',
      display:'flex',
      justifyContent:'flex-end'
    },
  }),
);

export default useStyles;
