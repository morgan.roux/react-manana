import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      padding: 8,
      fontFamily: 'Roboto',
      background: theme.palette.grey[200],
      flexDirection: 'row',
      alignItems: 'center',
      '& div': {
        marginRight: 40,
      },
      marginBottom: 4,
    },
    label: {
      fontSize: 14,
      fontWeight: 'bold',
    },
    value: {
      fontSize: 14,
      fontWeight: 'normal',
    },
  }),
);
