import { Box } from '@material-ui/core';
import React, { FC, memo } from 'react';
import { useStyles } from './styles';

export interface InformationContainerProps {
  object: any;
}

export const InformationContainer: FC<InformationContainerProps> = memo(({ object }) => {
  const classes = useStyles();
  return (
    <Box className={classes.root}>
      {object &&
        Object.entries(object).map(([label, value]) => (
          <Box key={label}>
            <span className={classes.label}>{label}&nbsp;:&nbsp;&nbsp;</span>
            <span className={classes.value}>{value as any}</span>
          </Box>
        ))}
    </Box>
  );
});
