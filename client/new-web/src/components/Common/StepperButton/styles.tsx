import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: '25px',
    },
  }),
);

export default useStyles;
