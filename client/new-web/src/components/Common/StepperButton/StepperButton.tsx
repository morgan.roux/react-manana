import React, { FC, Fragment, useState } from 'react';
import { RouteComponentProps, withRouter, Redirect } from 'react-router-dom';
import useStyles from './styles';
import { Box, Button } from '@material-ui/core';

interface StepperButtonProps {
  steps: string[];
  activeStep: number;
  setStep: (step: number) => void;
}
const StepperButton: FC<StepperButtonProps & RouteComponentProps<any, any, any>> = ({
  steps,
  activeStep,
  setStep,
}) => {
  const classes = useStyles({});

  const handlePrevious = () => {
    if (activeStep > 0) {
      setStep(activeStep - 1);
    }
  };
  const handleNext = () => {
    if (activeStep < steps.length - 1) {
      setStep(activeStep + 1);
    }
  };
  return (
    <Box display="flex" flexDirection="row">
      {activeStep > 0 && <Button onClick={handlePrevious}>Retour</Button>}
      {activeStep < steps.length - 1 && <Button onClick={handleNext}>Suivant</Button>}
    </Box>
  );
};

export default withRouter(StepperButton);
