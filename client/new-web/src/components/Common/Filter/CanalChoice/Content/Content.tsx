import React, { FC } from 'react';
import useStyles from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Box, ListItem, ListItemText, Radio } from '@material-ui/core';
interface ContentProps {
  libelle: string;
  code: string;
  active: boolean;
  selectActive: (name: string) => void;
}
const Content: FC<ContentProps & RouteComponentProps<any, any, any>> = ({
  libelle,
  code,
  active,
  selectActive,
}) => {
  const classes = useStyles({});

  const handleClick = () => {
    selectActive(code);
  };

  return (
    <Box className={classes.noStyle}>
      <ListItem role={undefined} dense={true} button={true} onClick={handleClick}>
        <Box display="flex" alignItems="center">
          <Radio checked={active ? active : false} tabIndex={-1} disableRipple={true} />
          <ListItemText primary={libelle} />
        </Box>
      </ListItem>
    </Box>
  );
};
export default withRouter(Content);
