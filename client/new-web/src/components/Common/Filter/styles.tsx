import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: '18px',
      color: '#F11957',
    },
  }),
);

export default useStyles;
