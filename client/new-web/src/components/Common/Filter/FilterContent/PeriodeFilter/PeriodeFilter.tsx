import React, { useEffect, FC } from 'react';
import moment, { Moment } from 'moment';
import { DateRangePicker } from '../../../DateRangePicker';
import useStyles from './style';
import { useApolloClient, useQuery } from '@apollo/client';
import { GET_FILTER_DATE } from '../../../../../graphql/Filter/local';
import { END_DAY_OF_CURRENT_MONTH, TODAY } from '../../../../../Constant/date';
import { OtherContentProps, ROLES_FILTER_DATES } from '../OtherContent/OtherContent';
import { ME_me } from '../../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../../services/LocalStorage';
import { FrontFiltersInterface, GET_LOCAL_FRONT_FILTERS } from '../../../withSearch/withSearch';
import { ADMINISTRATEUR_GROUPEMENT, SUPER_ADMINISTRATEUR } from '../../../../../Constant/roles';

export interface DateFilterIterface {
  startDate: Moment | null;
  endDate: Moment | null;
  __typename: string;
}

const PeriodeFilter: FC<OtherContentProps> = ({
  dataType,
  titleLegend,
  startDatePlaceholderText,
  endDatePlaceholderText,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const currentUser: ME_me = getUser();
  const role = currentUser && currentUser.role && currentUser.role.code;
  const isAdmin =
    currentUser &&
    currentUser.role &&
    (currentUser.role.code === SUPER_ADMINISTRATEUR ||
      currentUser.role.code === ADMINISTRATEUR_GROUPEMENT);

  const getDateFilter = useQuery<{ dateFilter: DateFilterIterface }>(GET_FILTER_DATE);
  const dateFilter = getDateFilter.data && getDateFilter.data.dateFilter;
  const frontFilters = useQuery<FrontFiltersInterface>(GET_LOCAL_FRONT_FILTERS);

  const getDates = (startDate: Moment | null, endDate: Moment | null) => {
    client.writeQuery({
      query: GET_FILTER_DATE,
      data: {
        dateFilter: {
          startDate: (startDate && startDate.toString()) || null,
          endDate: (endDate && endDate.toString()) || null,
        },
      },
    });

    client.writeQuery({
      query: GET_LOCAL_FRONT_FILTERS,
      data: {
        frontFilters: {
          occommande:
            (frontFilters && frontFilters.data && frontFilters.data.frontFilters.occommande) || [],
          seen: (frontFilters && frontFilters.data && frontFilters.data.frontFilters.seen) || null,
          datefilter: {
            dateDebut: (startDate && startDate.toString()) || null,
            dateFin: (endDate && endDate.toString()) || null,
          },
        },
      },
    });
  };

  // Init dateFilter if it's undefined
  useEffect(() => {
    if (!dateFilter) {
      client.writeQuery({
        query: GET_FILTER_DATE,
        data: {
          dateFilter: {
            startDate: role && ROLES_FILTER_DATES.includes(role) ? TODAY : null,
            endDate: role && ROLES_FILTER_DATES.includes(role) ? END_DAY_OF_CURRENT_MONTH : null,
          },
        },
      });
    }
  }, []);

  const [focusedInput, onFocusChange] = React.useState<'startDate' | 'endDate' | null>(null);

  const isOutsideRange = (day: any) => {
    if (!isAdmin) {
      if (focusedInput === 'startDate') {
        return moment().diff(day) < 0;
      }
      if (focusedInput === 'endDate') {
        return (
          moment()
            .subtract(1, 'days')
            .diff(day) > 0
        );
      }
    }
    return false;
  };

  return (
    <div className={classes.root}>
      <DateRangePicker
        startDate={(dateFilter && dateFilter.startDate && moment(dateFilter.startDate)) || null}
        startDateId="startDateUniqueId"
        endDate={(dateFilter && dateFilter.endDate && moment(dateFilter.endDate)) || null}
        endDateId="endDateUniqueId"
        getDates={getDates}
        label={titleLegend || 'Filtrer par période'}
        variant="outlined"
        numberOfMonths={4}
        startDatePlaceholderText={startDatePlaceholderText}
        endDatePlaceholderText={endDatePlaceholderText}
        withPortal={true}
        showDefaultInputIcon={false}
        focusedInput={focusedInput}
        onFocusChange={onFocusChange}
        isOutsideRange={isOutsideRange}
        showClearDates={true}
      />
    </div>
  );
};

export default PeriodeFilter;
