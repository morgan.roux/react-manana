import React, { FC, useState, useEffect } from 'react';
import useStyles from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import {
  Box,
  Button,
  Typography,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Checkbox,
  TextField,
  IconButton,
  Tooltip,
} from '@material-ui/core';
// import classnames from 'classnames';
import { SEARCH } from '../../../../../graphql/search/query';
import { useQuery } from '@apollo/client';
import {
  SEARCH as SEARCH_Interface,
  SEARCHVariables,
} from '../../../../../graphql/search/types/SEARCH';
import { makeElementsUnique } from '../../../../../utils/Helpers';
import { DATA } from '../../../../../Interface/Filter';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import { MY_PHARMACIE_me_pharmacie } from '../../../../../graphql/Pharmacie/types/MY_PHARMACIE';
import ICurrentPharmacieInterface from '../../../../../Interface/CurrentPharmacieInterface';
import { GET_CURRENT_PHARMACIE } from '../../../../../graphql/Pharmacie/local';
import BlockIcon from '@material-ui/icons/Block';
import { getGroupement, getUser } from '../../../../../services/LocalStorage';
import { ME_me } from '../../../../../graphql/Authentication/types/ME';
import { actualiteFilter } from '../../../../../services/actualiteFilter';
import { FiltersInterface, GET_LOCAL_FILTERS } from '../../../withSearch/withSearch';
interface ContentProps {
  title: string;
  type: string[];
  nom: string;
  skip: number;
  take: number;
  order: string;
  sortBy: string;
  typename: string;
  searchPlaceholder?: string;
  countKey?: string;
  updateList: (list: any, typename: string) => void;
  history: {
    location: { state: any };
  };
}
const Content: FC<ContentProps & RouteComponentProps> = ({
  title,
  type,
  nom,
  skip,
  take,
  order,
  sortBy,
  typename,
  countKey,
  updateList,
  searchPlaceholder,
  history: {
    location: { state },
  },
}) => {
  const classes = useStyles({});
  const remiseResults = [
    { id: 0, nom: 'Remise par palier', nbArticle: 0, checked: false, __typename: 'remise' },
    { id: 1, nom: 'Remise panachée', nbArticle: 0, checked: false, __typename: 'remise' },
  ];
  const todotypeResults = [
    { id: 0, nom: 'Personnel', nbArticle: 0, checked: false, __typename: 'todotype' },
    { id: 1, nom: 'Groupement', nbArticle: 0, checked: false, __typename: 'todotype' },
    { id: 2, nom: 'Pharmacie', nbArticle: 0, checked: false, __typename: 'todotype' },
  ];
  const [list, setList] = useState<DATA[]>(
    typename === 'remise' ? remiseResults : typename === 'todotype' ? todotypeResults : [],
  );
  const [currentSkip, setCurrentSkip] = useState<number>(skip);
  const [expandedMore, setExpandedMore] = useState<boolean>(true);
  const [searchResult, setSearchResult] = useState<DATA[]>([]);
  const [searchText, setSearchText] = useState<string>('');
  const [searchChecked, setSearchChecked] = useState<any[]>([]);
  const [showSearchChecked, setShowSearchChecked] = useState<boolean>(false);
  const [skipQuery, setSkipQuery] = useState<boolean>(
    typename === 'remise' || typename === 'occommande' || typename === 'todotype' ? true : false,
  );
  const [currentPharmacie, setCurrentPharmacie] = useState<MY_PHARMACIE_me_pharmacie | null>();
  // get pharmacie
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

  const groupement = getGroupement();
  const currentUser: ME_me = getUser();

  const pathname = window.location.hash;
  const skipSearchQuery: boolean =
    pathname === '#/create/actualite' || pathname.includes('#/edit/actualite/') ? true : false;

  const filters = useQuery<FiltersInterface>(GET_LOCAL_FILTERS);

  useEffect(() => {
    if (filters && filters.data && filters.data.filters) {
      switch (typename) {
        case 'famille':
          if (filters.data.filters.idFamilles.length === 0) {
            setSearchChecked([]);
            setSearchText('');
            setList(
              list.map((item: any) => {
                item.checked = false;
                return item;
              }),
            );
          }
          break;
        case 'laboratoire':
          if (filters.data.filters.idLaboratoires.length === 0) {
            setSearchChecked([]);
            setSearchText('');
            setList(
              list.map((item: any) => {
                item.checked = false;
                return item;
              }),
            );
          }
          break;
        case 'gamme':
          if (filters.data.filters.idGammesCommercials.length === 0) {
            setSearchChecked([]);
            setSearchText('');
            setList(
              list.map((item: any) => {
                item.checked = false;
                return item;
              }),
            );
          }
          break;
        case 'commandecanal':
          if (
            filters.data.filters.idCommandeCanal &&
            filters.data.filters.idCommandeCanal.length === 0
          ) {
            setSearchChecked([]);
            setSearchText('');
            setList(
              list.map((item: any) => {
                item.checked = false;
                return item;
              }),
            );
          }
          break;
        case 'commandecanals':
          if (
            filters.data.filters.idCommandeCanals &&
            filters.data.filters.idCommandeCanals.length === 0
          ) {
            setSearchChecked([]);
            setSearchText('');
            setList(
              list.map((item: any) => {
                item.checked = false;
                return item;
              }),
            );
          }
          break;
        case 'remise':
          if (filters.data.filters.idRemise.length === 0) {
            setSearchChecked([]);
            setSearchText('');
            setList(
              list.map((item: any) => {
                item.checked = false;
                return item;
              }),
            );
          }
          break;
        case 'occommande':
          if (filters.data.filters.idOcCommande.length === 0) {
            setSearchChecked([]);
            setSearchText('');
            setList(
              list.map((item: any) => {
                item.checked = false;
                return item;
              }),
            );
          }
          break;
        case 'commandetype':
          if (filters.data.filters.idCommandeTypes.length === 0) {
            setSearchChecked([]);
            setSearchText('');
            setList(
              list.map((item: any) => {
                item.checked = false;
                return item;
              }),
            );
          }
          break;
        case 'todotype':
          if (filters.data.filters.idTodoType.length === 0) {
            setSearchChecked([]);
            setSearchText('');
            setList(
              list.map((item: any) => {
                item.checked = false;
                return item;
              }),
            );
          }
          break;

        default:
          break;
      }
    }
  }, [filters]);

  useEffect(() => {
    if (myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie) {
      setCurrentPharmacie(myPharmacie.data.pharmacie);
    }
  }, [myPharmacie]);

  const withoutQueryTypename = ['actualiteorigine'];

  const existLaboState = typename === 'laboratoire' && state && state.idLaboratoire ? true : false;
  const queryTerm = existLaboState
    ? {
      query: {
        bool: { must: { term: { _id: state.idLaboratoire } } },
      },
    }
    : {};
  const results = useQuery<SEARCH_Interface, SEARCHVariables>(SEARCH, {
    variables: withoutQueryTypename.includes(typename)
      ? {
        type,
        skip,
        take,
        idPharmacie: currentPharmacie && currentPharmacie.id,
        searchActualiteParams: JSON.stringify(
          actualiteFilter(currentUser, groupement && groupement.id),
        ),
      }
      : {
        type,
        skip,
        take,
        query: {
          ...queryTerm,
          sort: [{ [sortBy]: order }],
        },
        //sortBy: [{ [sortBy]: { order } }],
        idPharmacie: currentPharmacie && currentPharmacie.id,
      },
    skip:
      !currentPharmacie || (currentPharmacie && !currentPharmacie.id)
        ? true
        : skipQuery
          ? skipQuery
          : skipSearchQuery
            ? skipSearchQuery
            : false,
  });

  useEffect(() => {
    if (
      !results.loading &&
      results.data &&
      results.data.search &&
      results.data.search.data &&
      results.data.search.data.length > 0
    ) {
      // Sort actualiteorigine by countActualites (desc)
      if (typename === 'actualiteorigine') {
        results.data.search.data.sort((a: any, b: any) => b.countActualites - a.countActualites);
      }

      const nextSkip = currentSkip + 3;
      setCurrentSkip(nextSkip);
      const query = makeElementsUnique(results.data.search.data, 'id').slice(0, nextSkip);
      setList(
        query
          .map((item: any) => {
            if (item && item[nom] !== undefined && (item[sortBy] !== undefined || countKey)) {
              return {
                id: item.id, // parseInt(item.id, 10),
                nom: item[nom],
                code: item.codeFamille, // Only for famille
                nbArticle: countKey ? parseInt(item[countKey], 10) : parseInt(item[sortBy], 10),
                __typename: typename,
                checked:
                  typename === 'laboratoire' &&
                    state &&
                    state.idLaboratoire &&
                    state.idLaboratoire === item.id
                    ? true
                    : false,
              };
            }
          })
          .sort((a: any, b: any) => b.nbArticle - a.nbArticle),
      );
      updateList(list, typename);
    }
  }, [results]);

  const fetchMore = () => {
    if (results && results.data && results.data.search && results.data.search.data) {
      const nextSkip = currentSkip + 3;
      setCurrentSkip(nextSkip);
      const query = makeElementsUnique(results.data.search.data, 'id').slice(currentSkip, nextSkip);
      const newList = list.concat(
        query.map((item: any) => {
          if (item && item[nom] !== undefined && (item[sortBy] !== undefined || countKey)) {
            return {
              id: item.id, // parseInt(item.id, 10),
              nom: item[nom],
              code: item.codeFamille,
              nbArticle: countKey ? parseInt(item[countKey], 10) : parseInt(item[sortBy], 10),
              __typename: typename,
              checked: searchChecked
                .map((checkedItem: any) => checkedItem && checkedItem.id)
                .includes(item.id)
                ? true
                : false,
            };
          }
        }),
      );
      setList(newList);
    }
  };

  const onGetValue = (id: any, typename: string) => {
    const newList = list.map((item: any) => {
      if (item && item.id === id) {
        if (item.checked) {
          setSearchChecked(searchChecked.filter((item: any) => item.id !== id));
        }
        item.checked = !item.checked;
        return item;
      }
      return item;
    });
    setList(newList);
    updateList(
      Array.from(new Set([...list, ...searchChecked].map(first => first.id))).map(id => {
        return [...list, ...searchChecked].find(item => item.id === id);
      }),
      typename,
    );
  };

  const onSearchGetValue = (id: any, typename: string) => {
    const newList = searchResult.map((item: any) => {
      if (item && item.id === id) {
        if (!item.checked) {
          setSearchChecked(searchChecked.concat([item]));
          const newList = list.map((listItem: any) => {
            if (listItem && listItem.id === id) {
              listItem.checked = true;
              return listItem;
            } else return listItem;
          });
          setList(newList);
        } else {
          setSearchChecked(searchChecked.filter((item: any) => item.id !== id));
          const newList = list.map((listItem: any) => {
            if (listItem && listItem.id === id) {
              listItem.checked = false;
              return listItem;
            } else return listItem;
          });
          setList(newList);
        }
        item.checked = !item.checked;
        return item;
      }
      return item;
    });
    setSearchResult(newList);
    updateList(
      Array.from(new Set([...list, ...newList, ...searchChecked].map(first => first.id))).map(
        id => {
          return [...list, ...newList, ...searchChecked].find(item => item.id === id);
        },
      ),
      typename,
    );
  };

  const onSearchCheckedGetValue = (id: any, typename: string) => {
    const newList = searchChecked.map((item: any) => {
      if (item && item.id === id) {
        if (!item.checked) {
          setSearchChecked(searchChecked.concat([item]));
        } else {
          setSearchChecked(searchChecked.filter((item: any) => item.id !== id));
        }
        item.checked = !item.checked;
        return item;
      }
      return item;
    });
    // setSearchResult(newList);
    updateList(newList, typename);
  };

  const handleMore = () => {
    fetchMore();
  };

  const onSearchChange = (event: any) => {
    const searchText = event.target.value as string;
    const data = results && results.data && results.data.search && results.data.search.data;
    setSearchText(searchText);
    if (!searchText || searchText === '') {
      setSearchResult([]);
    } else {
      if (data) {
        const regEx = new RegExp('^' + searchText, 'i');
        const searchResultArray: any[] = [];
        data.map((item: any) => {
          const text = item && (item[nom] as string);

          const matched = text.match(regEx);
          if (matched) {
            searchResultArray.push(item);
          }
        });
        const query = makeElementsUnique(searchResultArray, 'id').slice(
          0,
          searchResultArray.length,
        );
        const newList = query.map((item: any) => {
          if (item && item[nom] !== undefined && (item[sortBy] !== undefined || countKey)) {
            return {
              id: item.id, // parseInt(item.id, 10),
              nom: item[nom],
              code: item.codeFamille,
              nbArticle: countKey ? parseInt(item[countKey], 10) : parseInt(item[sortBy], 10),
              __typename: typename,
              checked: searchChecked
                .map((checkedItem: any) => checkedItem && checkedItem.id)
                .includes(item.id)
                ? true
                : false,
            };
          }
        });
        setSearchResult(newList);
      }
    }
  };

  return (
    <Box marginLeft="8px" borderTop="1px solid #E3E3E3" padding="16px 0">
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        marginBottom="8px"
        onClick={() => setExpandedMore(!expandedMore)}
      >
        <Typography className={classes.name}>{title}</Typography>
        <IconButton size="small" className={classes.expandBtnContainer}>
          {expandedMore ? <ExpandLess /> : <ExpandMore />}
        </IconButton>
      </Box>
      <Box className={classes.noStyle}>
        {results &&
          results.data &&
          results.data.search &&
          results.data.search.data &&
          results.data.search.data.length > 20 && (
            <Box
              display="flex"
              flexDirection="row"
              paddingRight="16px"
              paddingLeft="16px"
              alignItems="center"
            >
              <TextField
                variant="outlined"
                placeholder={searchPlaceholder && searchPlaceholder}
                value={searchText}
                onChange={onSearchChange}
              />

              <IconButton>
                {showSearchChecked ? (
                  <Tooltip title="Afficher les éléments de recherches sélectionnées">
                    <VisibilityOffIcon onClick={() => setShowSearchChecked(!showSearchChecked)} />
                  </Tooltip>
                ) : (
                  <Tooltip title="Cacher les éléments de recherches sélectionnées">
                    <VisibilityIcon onClick={() => setShowSearchChecked(!showSearchChecked)} />
                  </Tooltip>
                )}
              </IconButton>
            </Box>
          )}
        {searchResult && searchResult.length > 0 && searchText && searchText.length > 0 ? (
          <Box className={classes.searchBox}>
            {searchResult.map(
              (item: any) =>
                item && (
                  <ListItem
                    role={undefined}
                    dense={true}
                    button={true}
                    onClick={() => onSearchGetValue(item.id, item.__typename)}
                    key={item.id}
                  >
                    <Box className={classes.checkBoxLeftName} display="flex" alignItems="center">
                      <Checkbox
                        tabIndex={-1}
                        checked={item.checked}
                        disableRipple={true}
                        disabled={existLaboState}
                      />
                      <ListItemText className={classes.nom} primary={`${item.nom}`} />
                    </Box>

                    {typename !== 'remise' && typename !== 'occommande' && (
                      <ListItemSecondaryAction>
                        <Typography className={classes.nbrProduits}>{item.nbArticle}</Typography>
                      </ListItemSecondaryAction>
                    )}
                  </ListItem>
                ),
            )}
          </Box>
        ) : searchText && searchText.length > 0 && searchResult.length === 0 ? (
          <Box justifyContent="center" display="flex" flexDirection="row" alignItems="center">
            <BlockIcon />
            Aucun résultat
          </Box>
        ) : showSearchChecked ? (
          searchChecked.map(
            (item: any) =>
              item && (
                <ListItem
                  role={undefined}
                  dense={true}
                  button={true}
                  onClick={() => onSearchCheckedGetValue(item.id, item.__typename)}
                  key={item.id}
                >
                  <Box className={classes.checkBoxLeftName} display="flex" alignItems="center">
                    <Checkbox
                      tabIndex={-1}
                      checked={item.checked}
                      disableRipple={true}
                      disabled={existLaboState}
                    />
                    <ListItemText className={classes.nom} primary={`${item.nom}`} />
                  </Box>

                  {typename !== 'remise' && typename !== 'occommande' && (
                    <ListItemSecondaryAction>
                      <Typography className={classes.nbrProduits}>{item.nbArticle}</Typography>
                    </ListItemSecondaryAction>
                  )}
                </ListItem>
              ),
          )
        ) : (
          expandedMore &&
          list.map(
            (item: any) =>
              item && (
                <ListItem
                  role={undefined}
                  dense={true}
                  button={true}
                  onClick={() => onGetValue(item.id, item.__typename)}
                  key={item.id}
                  disabled={existLaboState}
                >
                  <Box className={classes.checkBoxLeftName} display="flex" alignItems="center">
                    <Checkbox
                      tabIndex={-1}
                      checked={item.checked}
                      disableRipple={true}
                      disabled={existLaboState}
                    />
                    <ListItemText className={classes.nom} primary={`${item.nom}`} />
                  </Box>

                  {typename !== 'remise' && typename !== 'todotype' && (
                    <ListItemSecondaryAction>
                      <Typography className={classes.nbrProduits}>{item.nbArticle}</Typography>
                    </ListItemSecondaryAction>
                  )}
                </ListItem>
              ),
          )
        )}
      </Box>
      {expandedMore &&
        searchResult &&
        searchResult.length === 0 &&
        !showSearchChecked &&
        searchText.length === 0 &&
        results.data &&
        results.data.search &&
        results.data.search.data &&
        list.length < results.data.search.data.length && (
          <Box display="flex" justifyContent="center">
            <Button onClick={handleMore} className={classes.voirButton}>
              Voir plus
            </Button>
          </Box>
        )}
    </Box>
  );
};
export default withRouter(Content);
