import { ApolloClient, useApolloClient } from '@apollo/client';
import { Box, IconButton, Typography } from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import React, { FC, ReactNode, useEffect, useState } from 'react';
import { END_DAY_OF_CURRENT_MONTH, TODAY } from '../../../../../Constant/date';
import { TITULAIRE_PHARMACIE } from '../../../../../Constant/roles';
import { ME_me } from '../../../../../graphql/Authentication/types/ME';
import { GET_FILTER_DATE, GET_STATUS_FILTER } from '../../../../../graphql/Filter/local';
import { getUser } from '../../../../../services/LocalStorage';
import PeriodeFilter from '../PeriodeFilter/PeriodeFilter';
import StatusFilter from '../StatusFilter';
import useStyles from './style';

export const ROLES_FILTER_DATES = [TITULAIRE_PHARMACIE];

interface Element {
  id: string;
  title: string;
  content: ReactNode;
}

export interface OtherContentProps {
  dataType: string;
  titleLegend?: string;
  startDatePlaceholderText?: string;
  endDatePlaceholderText?: string;
}

export const initializeStatusAndPeriodeFilters = (
  client: ApolloClient<object>,
  role: string,
  dataType?: string,
) => {
  // Reset status filter

  client.writeQuery({ query: GET_STATUS_FILTER, data: { statusFilter: 'all' } });
  // Reset dateFilter
  client.writeQuery({
    query: GET_FILTER_DATE,
    data: {
      dateFilter: {
        startDate: ROLES_FILTER_DATES.includes(role) ? TODAY : null,
        endDate: ROLES_FILTER_DATES.includes(role) ? END_DAY_OF_CURRENT_MONTH : null,
      },
    },
  });
};

const OtherContent: FC<OtherContentProps> = ({ dataType }) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [expanded, setExpanded] = useState<{ [key: string]: boolean }>({
    status: true,
    periode: true,
  });
  const currentUser: ME_me = getUser();
  const role = currentUser && currentUser.role && currentUser.role.code;

  const toggleExpanded = (key: string) => {
    setExpanded(prevState => ({
      ...prevState,
      [key]: !prevState[key],
    }));
  };

  const elements: Element[] = [
    {
      id: 'status',
      title: 'Status',
      content: <StatusFilter />,
    },
    {
      id: 'periode',
      title: 'Période de recherche',
      content: <PeriodeFilter dataType={dataType} />,
    },
  ];

  // Set statusFilter and dateFilter to default if dataType change
  useEffect(() => {
    initializeStatusAndPeriodeFilters(client, role || '', dataType);
  }, [dataType]);

  return (
    <Box marginLeft="8px" padding="0px">
      {elements.map((item, index) => {
        return (
          <div className={classes.itemContainer} key={`list_other_content_item_${index}`}>
            <div className={classes.itemTitleContainer} onClick={() => toggleExpanded(item.id)}>
              <Typography className={classes.title}> {item.title} </Typography>
              <IconButton size="small" className={classes.expandBtnContainer}>
                {expanded[item.id] ? <ExpandLess /> : <ExpandMore />}
              </IconButton>
            </div>
            {expanded[item.id] && (
              <div className={classes.itemContentContainer}>{item.content}</div>
            )}
          </div>
        );
      })}
    </Box>
  );
};

export default OtherContent;
