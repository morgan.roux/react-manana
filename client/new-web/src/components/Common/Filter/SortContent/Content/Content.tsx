import React, { FC } from 'react';
import useStyles from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Box, ListItem, ListItemText, Radio } from '@material-ui/core';
interface ContentProps {
  label: string;
  name: string;
  direction: string;
  active?: boolean;
  selectActive: (name: string) => void;
}
const Content: FC<ContentProps & RouteComponentProps<any, any, any>> = ({
  label,
  name,
  active,
  selectActive,
}) => {
  const classes = useStyles({});

  const handleClick = () => {
    selectActive(name);
  };

  return (
    <Box className={classes.noStyle}>
      <ListItem role={undefined} dense={true} button={true} onClick={handleClick}>
        <Box display="flex" alignItems="center">
          <Radio checked={active ? active : false} tabIndex={-1} disableRipple={true} />
          <ListItemText primary={label} />
        </Box>
      </ListItem>
    </Box>
  );
};
export default withRouter(Content);
