import { makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => ({
  customTabsRoot: {
    maxWidth: 1920,
    margin: '0 auto',
    width: '100%',
    '@global': {
      '*::-webkit-scrollbar': {
        height: '0px !important',
      },
    },
    flexGrow: 1,
    '& .MuiTabs-indicator': {
      height: 3,
    },
    '& .MuiTabs-flexContainer': {
      '& > button': {
        textTransform: 'none',
        maxWidth: 'fit-content',
        width: '100%',
        minWidth: 140,
        height: 50,
        // border: '1px solid #9E9E9E',
        // borderRadius: 5,
        // marginTop: 10,
        '@media (max-width: 1200px)': {
          width: 'auto',
        },
      },
      '& > button:not(:nth-last-child(1))': {
        marginRight: 10,
      },
      '& > button.Mui-selected, & > button:hover': {
        color: theme.palette.secondary.main,
        // backgroundColor: theme.palette.secondary.main,
        // border: 'none',
        // opacity: 1,
      },
      '& > button.Mui-selected:hover': {
        // backgroundColor: darken(theme.palette.secondary.main, 0.1),
      },
    },
  },
  customTabsAppBar: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    boxShadow: '0px 2px 4px 0px rgba(0,0,0,0.25) !important',
    '& .MuiTabs-fixed': {
      overflow: 'scroll !important',
    },
    '& .MuiIconButton-root': {
      margin: 'auto 10px',
    },
  },
  countTabsItem: {
    display: 'flex',
    alignItems: 'center',
    '& .MuiBadge-badge': {
      '&.MuiBadge-anchorOriginTopRightRectangle': {
        '&:not(:empty)': {
          backgroundColor: '#E0E0E0',
          marginLeft: 6,
          position: 'relative',
          top: 'inherit',
          right: 'inherit',
          transform: 'none',
          borderRadius: 2,
          color: '#212121',
        },
      },
    },
  },
}));

export default useStyles;
