import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    customModalRoot: {
      color: theme.palette.common.black,
      '& * ': {
        letterSpacing: 0,
      },
      [theme.breakpoints.up('md')]: {
        minWidth: 800,
      },
    },
    centerBtnsDialogActions: {
      '& .MuiDialogActions-root': {
        justifyContent: 'center',
      },
    },
    customModalTitle: {
      fontSize: 18,
      fontWeight: 'bold',
    },
    customFullScreenModalTitle: {
      fontSize: 25,
    },
    customModalTitleWithBgColor: {
      background: theme.palette.primary.main,
      '& * ': {
        color: theme.palette.common.white,
      },
      '& > h2': {
        display: 'flex',
        justifyContent: 'space-between',
      },
    },
    btn: {
      fontWeight: 'bold',
    },
    closeButton: {
      color: 'inherit',
    },
    appBar: {
      position: 'sticky',
      top: 0,
    },
    toolbar: {
      justifyContent: 'space-between',
    },
    button: {
      justifyContent:'flex-end'
    },
  }),
);

export default useStyles;
