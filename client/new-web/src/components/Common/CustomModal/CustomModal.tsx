import {
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  DialogProps,
  DialogTitle,
  IconButton,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import classnames from 'classnames';
import React, { FC, MouseEvent, ReactNode } from 'react';
import CustomButton from '../CustomButton';
import useStyles from './styles';

export interface CustomModalInterface {
  title?: string;
  open: boolean;
  children: ReactNode;
  actionButton?: string;
  disabledButton?: boolean;
  closeIcon?: boolean;
  centerBtns?: boolean;
  withBtnsActions?: boolean;
  headerWithBgColor?: boolean;
  withHeader?: boolean;
  setOpen: (open: boolean) => void;
  onClickConfirm?: (event: MouseEvent<any>) => void;
  withCancelButton?: boolean;
  withConfirmButton?: boolean;
  actionButtonTitle?: string;
  cancelButtonTitle?: string;
  customHandleClose?: () => void;
  fullScreen?: boolean;
  noDialogContent?: boolean;
}

const CustomModal: FC<CustomModalInterface & DialogProps> = ({
  title,
  actionButton,
  open,
  setOpen,
  children,
  onClickConfirm,
  disabledButton,
  closeIcon,
  centerBtns,
  withBtnsActions = true,
  headerWithBgColor = false,
  className,
  maxWidth = 'xl',
  fullWidth,
  withCancelButton,
  actionButtonTitle = 'SAUVEGARDER',
  cancelButtonTitle = 'Annuler',
  withConfirmButton = true,
  customHandleClose,
  fullScreen,
  noDialogContent,
  ...restProps
}) => {
  const classes = useStyles({});

  const handleClose = () => {
    if (customHandleClose) {
      customHandleClose();
    } else {
      setOpen(false);
    }
  };

  return (
    <Dialog
      onClick={event => event.stopPropagation()}
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      className={
        centerBtns
          ? classnames(classes.customModalRoot, classes.centerBtnsDialogActions, className)
          : classnames(classes.customModalRoot, className)
      }
      maxWidth={maxWidth}
      fullWidth={fullWidth !== undefined ? fullWidth : false}
      fullScreen={fullScreen !== undefined ? fullScreen : false}
      {...restProps}
    >
      {title && (
        <DialogTitle
          id="alert-dialog-title"
          className={
            headerWithBgColor
              ? classnames(classes.customModalTitle, classes.customModalTitleWithBgColor)
              : classes.customModalTitle
          }
        >
          {title}
          {closeIcon && (
            <IconButton
              size="small"
              aria-label="close"
              className={classes.closeButton}
              // tslint:disable-next-line: jsx-no-lambda
              onClick={event => {
                event.preventDefault();
                event.stopPropagation();
                handleClose();
              }}
            >
              <CloseIcon />
            </IconButton>
          )}
        </DialogTitle>
      )}

      {noDialogContent ? (
        <Box display="flex">{children}</Box>
      ) : (
          <DialogContent>
            <Box display="flex">{children}</Box>
          </DialogContent>
        )}

      {withBtnsActions && (
        <DialogActions className={classes.button}>
          {(withCancelButton || withCancelButton === undefined) && (
            <CustomButton
              className={classes.btn}
              // tslint:disable-next-line: jsx-no-lambda
              onClick={event => {
                event.preventDefault();
                event.stopPropagation();
                handleClose();
              }}
            >
              {cancelButtonTitle}
            </CustomButton>
          )}
          {withConfirmButton && <CustomButton
            color="secondary"
            className={classes.btn}
            // tslint:disable-next-line: jsx-no-lambda
            onClick={event => {
              event.preventDefault();
              event.stopPropagation();
              if (onClickConfirm) onClickConfirm(event);
            }}
            disabled={disabledButton && disabledButton === true ? disabledButton : false}
          >
            {actionButton || actionButtonTitle}
          </CustomButton>}
        </DialogActions>
      )}
    </Dialog>
  );
};

export default CustomModal;
