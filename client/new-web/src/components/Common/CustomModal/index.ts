import CustomModal from './CustomModal';
import CustomFullScreenModal from './CustomFullScreenModal';

export { CustomModal, CustomFullScreenModal };
