import React, { FC, Fragment, useState, useEffect } from 'react';
import { RouteComponentProps, withRouter, Redirect } from 'react-router-dom';
import useStyles from './styles';
import { Box, Typography, Checkbox, Tooltip } from '@material-ui/core';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CloseCircleIcon from '@material-ui/icons/Cancel';
import classnames from 'classnames';
interface InputProps {
  label: string;
  value?: string;
  required?: boolean;
  checked?: boolean;
  errorMessage?: string;
}
const TextInput: FC<InputProps & RouteComponentProps<any, any, any>> = ({
  label,
  value,
  required,
  checked,
  errorMessage,
}) => {
  const classes = useStyles({});
  return (
    <Box display="flex" flexDirection="row" height="32px" alignItems="center">
      <Typography className={classnames(classes.MontserratBold)}>{label}:</Typography>
      <Typography className={classnames(classes.MontserratRegular)}>{value}</Typography>
      {checked === false || checked === true ? (
        <Checkbox checked={checked} />
      ) : (required && value === undefined) || (required && value && value.length === 0) ? (
        <Tooltip title={errorMessage || ''} aria-label="errorMessage">
          <CloseCircleIcon className={classes.pink} />
        </Tooltip>
      ) : (
        <CheckCircleIcon className={classes.green} />
      )}
    </Box>
  );
};

export default withRouter(TextInput);
