import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    small: {
      // fontSize: theme.typography.small.fontSize,
    },
    MontserratBold: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
    },
    colorDefault: {
      // color: theme.palette.text.primary,
    },
    medium: {
      // fontSize: theme.typography.medium.fontSize,
    },
    pink: {
      // color: theme.palette.text.primary,
      cursor: 'pointer',
    },
    green: {
      // color: theme.palette.text.secondary,
      cursor: 'pointer',
    },
    underlined: {
      textDecoration: 'underline',
    },
    MontserratRegular: {
      fontFamily: 'Montserrat',
      fontWeight: 400,
    },
    gray: {
      // color: theme.palette.secondary.contrastText,
    },
    listBox: {
      display: 'flex',
      flexDirection: 'column',
      overflowY: 'scroll',
      maxHeight: '80px',
    },
  }),
);

export default useStyles;
