import React, { FC, Fragment, useState, useEffect } from 'react';
import { RouteComponentProps, withRouter, Redirect } from 'react-router-dom';
import useStyles from './styles';
import { Box, Typography, Tooltip } from '@material-ui/core';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CloseCircleIcon from '@material-ui/icons/Cancel';
import classnames from 'classnames';
interface ListProps {
  label: string;
  value: any[];
  errorMessage?: string;
  required?: boolean;
}
const TextInput: FC<ListProps & RouteComponentProps<any, any, any>> = ({
  label,
  value,
  errorMessage,
  required,
}) => {
  const classes = useStyles({});
  return (
    <Box display="flex" flexDirection="row">
      <Typography className={classnames(classes.MontserratBold)}>{label}:</Typography>
      {value && value.length > 0 && (
        <Box className={classes.listBox}>
          {value && value.map((item, index) => <Typography key={index}>{item.name}</Typography>)}
        </Box>
      )}

      {(required && value && value.length === 0) || (required && !value) ? (
        <Tooltip title={errorMessage || ''} aria-label="errorMessage">
          <CloseCircleIcon className={classes.pink} />
        </Tooltip>
      ) : (
        <CheckCircleIcon className={classes.green} />
      )}
    </Box>
  );
};

export default withRouter(TextInput);
