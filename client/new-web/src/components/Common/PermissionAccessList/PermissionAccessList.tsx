import React, {
  FC,
  Dispatch,
  SetStateAction,
  MouseEvent,
  useState,
  useEffect,
  ChangeEvent,
  useRef,
  useMemo,
} from 'react';
import useStyles from './styles';
import { Typography, LinearProgress } from '@material-ui/core';
import { CustomCheckbox } from '../CustomCheckbox';
import { useApolloClient, useLazyQuery } from '@apollo/client';
import { DO_SEARCH_PERMISSIONS_ACCESS } from '../../../graphql/PermissionAccess';
import {
  SEARCH_PERMISSIONS_ACCESS,
  SEARCH_PERMISSIONS_ACCESSVariables,
} from '../../../graphql/PermissionAccess/types/SEARCH_PERMISSIONS_ACCESS';
import CustomTableWithSearchPagination from '../CustomTableWithSearch/CustomTableWithSearchPagination';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import { CustomFormTextField } from '../CustomTextField';
import { debounce } from 'lodash';
import { getGroupement } from '../../../services/LocalStorage';
import CustomButton from '../CustomButton';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import Backdrop from '../Backdrop';

interface PermissionAccessList {
  selected: any[];
  setSelected: Dispatch<SetStateAction<any[]>>;
  withTitle?: boolean;
  withSubTitle?: boolean;
  withInputSearch?: boolean;
  rolesCodes?: string[];
  withFilterByGroupement?: boolean;
  withAddBtn?: boolean;
}

const PermissionAccessList: FC<PermissionAccessList & RouteComponentProps> = ({
  selected,
  setSelected,
  withTitle,
  withSubTitle,
  withInputSearch,
  rolesCodes,
  withFilterByGroupement,
  withAddBtn,
  history: { push },
  location: { pathname },
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const groupement = getGroupement();
  const idGroupement = (groupement && groupement.id) || '';

  const selectedCodes = (selected && selected.map((item: any) => item && item.code)) || [];
  const isSelected = (code: string | number) => selectedCodes.indexOf(code) !== -1;

  const [permissionAccessList, setPermissionAccessList] = useState<any[]>([]);
  const [total, setTotal] = useState<number>(0);

  const type = ['traitement'];
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(10);
  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(10);

  const rolesFilter = rolesCodes ? [{ terms: { 'roles.code': rolesCodes } }] : [];
  const groupementFilter = withFilterByGroupement
    ? [{ terms: { idGroupements: [idGroupement] } }]
    : [];
  const defaultFilterBy = [...rolesFilter, ...groupementFilter];
  const [filterBy, setFilterBy] = useState<any[]>(defaultFilterBy);

  const defaultSortBy = [{ nom: { order: 'asc' } }];
  const [sortBy, setSortBy] = useState<any[]>(defaultSortBy);

  const [searchText, setSearchText] = useState<string>('');

  const handleCheck = (row: any, rowCode: string) => (event: MouseEvent<unknown>) => {
    event.preventDefault();
    event.stopPropagation();

    const selectedIndex = selectedCodes.indexOf(rowCode);
    let newSelected: string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, row);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const handleChngeSearchText = (e: ChangeEvent<HTMLInputElement>) => {
    if (e && e.target) {
      setSearchText(e.target.value);
    }
  };

  const goToRoles = () => push('/db/roles');

  const variables: SEARCH_PERMISSIONS_ACCESSVariables | undefined = {
    type,
    skip,
    take,
    sortBy,
    filterBy,
  };

  /**
   * Get permission access list
   */
  const [searchPermissionAccess, { data, loading }] = useLazyQuery<
    SEARCH_PERMISSIONS_ACCESS,
    SEARCH_PERMISSIONS_ACCESSVariables
  >(DO_SEARCH_PERMISSIONS_ACCESS, {
    fetchPolicy: 'cache-and-network',
    variables,
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  /**
   * Get all list
   */
  const [searchAllPermissionAccess, { data: allData, loading: allLoading }] = useLazyQuery<
    SEARCH_PERMISSIONS_ACCESS,
    SEARCH_PERMISSIONS_ACCESSVariables
  >(DO_SEARCH_PERMISSIONS_ACCESS, {
    // fetchPolicy: 'cache-and-network',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  /**
   * Execute query
   */
  useEffect(() => {
    searchPermissionAccess();
  }, []);

  useMemo(() => {
    searchPermissionAccess({
      variables: {
        ...variables,
        filterBy: searchText ? [...filterBy, { wildcard: { nom: `*${searchText}*` } }] : filterBy,
      },
    });
  }, [take, skip]);

  /**
   * Debounce search
   */
  const debouncedSearch = useRef(
    debounce((text: string) => {
      searchPermissionAccess({
        variables: {
          ...variables,
          filterBy: text ? [...filterBy, { wildcard: { nom: `*${text}*` } }] : filterBy,
        },
      });
    }, 1000),
  );

  useMemo(() => {
    debouncedSearch.current(searchText);
  }, [searchText]);

  /**
   * Set permissionAccessList and total
   */
  useEffect(() => {
    if (data && data.search && data.search.data) {
      setPermissionAccessList(data.search.data);
      setTotal(data.search.total);
    }
  }, [data]);

  /**
   * Take all list on create
   */
  useMemo(() => {
    if (total !== 0 && pathname.includes('/user/create/')) {
      searchAllPermissionAccess({ variables: { ...variables, take: total } });
    }
  }, [total, pathname]);

  /**
   * Set default selected on create user
   */
  useMemo(() => {
    if (
      allData &&
      allData.search &&
      allData.search.data &&
      allData.search.data.length > 0 &&
      selected.length === 0 &&
      pathname.includes('/user/create/')
    ) {
      setSelected(allData.search.data);
    }
  }, [allData, pathname]);

  return (
    <div className={classes.infoPersoContainer}>
      {allLoading && <Backdrop />}
      {withTitle && <Typography className={classes.inputTitle}>Droits d'accès</Typography>}
      {withSubTitle && (
        <Typography className={classes.permissionAccessSubTite}>
          Choisissez les accès auxquels l'utilisateur aura droit
        </Typography>
      )}
      {withInputSearch && (
        <CustomFormTextField
          placeholder="Rechercher"
          onChange={handleChngeSearchText}
          value={searchText}
        />
      )}
      <LinearProgress
        color="secondary"
        style={{
          width: '100%',
          height: 3,
          marginBottom: 20,
          visibility: loading ? 'visible' : 'hidden',
        }}
      />
      {permissionAccessList.length === 0 && !loading ? (
        <div className={classes.noDataContainer}>
          <span>Aucun droit d'accès trouvé</span>
          {withAddBtn && (
            <CustomButton variant="outlined" color="secondary" onClick={goToRoles}>
              Ajouter des droits
            </CustomButton>
          )}
        </div>
      ) : (
        <>
          <div className={classes.inputsContainer}>
            <div className={classes.permissionAccessFormRow}>
              {permissionAccessList.map((i: any, index) => {
                const isItemSelected = isSelected(i.code);
                const labelId = `permission-access-list-checkbox-${index}`;
                return (
                  <CustomCheckbox
                    key={`permission_access_${index}`}
                    label={i.nom}
                    checked={isItemSelected}
                    inputProps={{ 'aria-labelledby': labelId }}
                    onClick={handleCheck(i, i.code)}
                    color="secondary"
                  />
                );
              })}
            </div>
          </div>
          <div className={classes.paginationContainer}>
            <CustomTableWithSearchPagination
              total={total}
              page={page}
              rowsPerPage={rowsPerPage}
              take={take}
              skip={skip}
              setTake={setTake}
              setSkip={setSkip}
              setPage={setPage}
              setRowsPerPage={setRowsPerPage}
            />
          </div>
        </>
      )}
    </div>
  );
};

PermissionAccessList.defaultProps = {
  withTitle: true,
  withSubTitle: true,
  withInputSearch: true,
  withFilterByGroupement: true,
  withAddBtn: true,
};

export default withRouter(PermissionAccessList);
