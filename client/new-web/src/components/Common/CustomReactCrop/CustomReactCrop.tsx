import React, { FC, useCallback, useRef } from 'react';
import ReactCrop, { Crop } from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import useStyles from './style';

interface CustomReactCropProps {
  src: string;
  crop: Crop;
  previewUrl: string;
  savedCrop: boolean;
  setCrop: (crop: Crop) => void;
  setPreviewUrl: (url: string) => void;
  setCropedFiles?: (files: File[]) => void;
}

const CustomReactCrop: FC<CustomReactCropProps> = ({
  src,
  crop,
  setCropedFiles,
  setCrop,
  previewUrl,
  setPreviewUrl,
  savedCrop,
}) => {
  const classes = useStyles({});
  const userImageRef = useRef(null);

  const onImageLoaded = useCallback(img => {
    userImageRef.current = img;
  }, []);

  const handleChangeReactCrop = (newCrop: ReactCrop.Crop) => {
    if (crop) {
      if (
        crop.x !== newCrop.x ||
        crop.y !== newCrop.y ||
        crop.width !== newCrop.width ||
        crop.height !== newCrop.height
      ) {
        setCrop(newCrop);
      }
    }
  };

  const makeClientCrop = async (newCrop: ReactCrop.Crop) => {
    if (savedCrop) {
      if (userImageRef.current && newCrop.width && newCrop.height) {
        createCropPreview(
          userImageRef.current,
          newCrop,
          `userAvatarCroped_${new Date().getTime()}.png`,
        );
      }
    }
  };

  const createCropPreview = async (image, crop, fileName) => {
    const canvas = document.createElement('canvas', {});
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx: CanvasRenderingContext2D | null = canvas.getContext('2d');

    if (ctx) {
      ctx.drawImage(
        image,
        crop.x * scaleX,
        crop.y * scaleY,
        crop.width * scaleX,
        crop.height * scaleY,
        0,
        0,
        crop.width,
        crop.height,
      );
    }

    return new Promise((resolve, reject) => {
      if (canvas) {
        canvas.toBlob((blob: any) => {
          if (!blob) {
            reject(new Error('Canvas is empty'));
            return;
          }
          if (blob) {
            blob.name = fileName;
            blob.crossOrigin = 'anonymous';
            window.URL.revokeObjectURL(previewUrl);
            setPreviewUrl(window.URL.createObjectURL(blob));
            if (setCropedFiles) setCropedFiles([blob]);
          }
        }, 'image/png');
      }
    });
  };

  return (
    <ReactCrop
      className={classes.reactCrop}
      src={src}
      crop={crop}
      onImageLoaded={onImageLoaded}
      onChange={newCrop => handleChangeReactCrop(newCrop)}
      onComplete={makeClientCrop}
      crossorigin="anonymous"
    />
  );
};

export default CustomReactCrop;
