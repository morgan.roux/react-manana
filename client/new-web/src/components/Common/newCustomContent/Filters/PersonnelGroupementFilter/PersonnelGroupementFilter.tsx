import React, { FC, useEffect } from 'react';
import useStyles from './styles';
import { useValueParameterAsBoolean } from '../../../../../utils/getValueParameter';
import {
  PHARMA_FILTER_PHARMACIE_NAME_CODE,
  PHARMA_FILTER_TITULAIRE_CODE,
  PHARMA_FILTER_SORTIE_CODE,
  PHARMA_FILTER_CODE_ENSEIGNE_CODE,
  PHARMA_FILTER_DEPARTEMENT_CODE,
  PHARMA_FILTER_STATUS_CODE,
  PHARMA_FILTER_CODE_IVRY_LAB_CODE,
  PHARMA_FILTER_TELEPHONE_CODE,
  PHARMA_FILTER_CIP_CODE,
  PHARMA_FILTER_VILLE_CODE,
  PHARMA_FILTER_CODE_POSTAL_CODE,
  PHARMA_FILTER_ADRESSE_CODE,
  PHARMA_FILTER_PRESIDENT_REGION_CODE,
  PHARMA_FILTER_CONTRAT_CODE,
  PHARMA_FILTER_SIRET_CODE,
  PHARMA_FILTER_REGION_CODE,
  PHARMA_FILTER_TRANCHE_CA_CODE,
  PHARMA_FILTER_COMMERCIALE_CODE,
  SEARCH_FIELDS_OPTIONS,
} from '../../../CustomPharmacie/constants';
import { FieldsOptions } from '../../../CustomContent/interfaces';
import SearchFilter from '../../SearchFilter';
import { useQuery } from '@apollo/client';
import { MINIM_SERVICES } from '../../../../../graphql/Service/types/MINIM_SERVICES';
import { GET_MINIM_SERVICES } from '../../../../../graphql/Service';
import {
  SEARCH_ROLES,
  SEARCH_ROLESVariables,
} from '../../../../../graphql/Role/types/SEARCH_ROLES';
import { DO_SEARCH_ROLES } from '../../../../../graphql/Role';
import { CircularProgress, Typography } from '@material-ui/core';
import CustomSelect from '../../../CustomSelect';
import CustomButton from '../../../CustomButton';
interface PersonnelGroupementFilterProps {
  fieldsState: any;
  handleFieldChange: (event: any) => void;
  handleRunSearch: (query: any) => void;
  handleResetFields: () => void;
}

const PersonnelGroupementFilter: FC<PersonnelGroupementFilterProps> = ({
  fieldsState,
  handleFieldChange,
  handleRunSearch,
  handleResetFields,
}) => {
  const classes = useStyles({});
  const { service, role, status, sortie } = fieldsState;

  const STATUT_LIST = [
    { code: 'ALL', value: 'Tous les statuts' },
    { code: 'ACTIVATED', value: 'Activé' },
    { code: 'BLOCKED', value: 'Désactivé' },
    { code: 'ACTIVATION_REQUIRED', value: "Demande d'activation" },
    { code: 'RESETED', value: 'Mot de passe réinitialisé' },
  ];

  const SORTIE_LIST = [
    { code: 'ALL', value: 'Tous' },
    { code: '0', value: 'Non' },
    { code: '1', value: 'Oui' },
  ];

  /**
   * Get services list
   */
  const { data: servicesData, loading: servicesLoading } = useQuery<MINIM_SERVICES>(
    GET_MINIM_SERVICES,
  );
  const SERVICE_DATA_LIST: any[] = (servicesData && servicesData.services) || [];
  const SERVICE_LIST = [
    ...[{ id: '-1', code: 'ALL', nom: 'Tous les services' }],
    ...SERVICE_DATA_LIST,
  ];

  /**
   * Get roles list
   */
  const { data: rolesData, loading: rolesLoading } = useQuery<SEARCH_ROLES, SEARCH_ROLESVariables>(
    DO_SEARCH_ROLES,
    {
      variables: {
        type: ['role'],
        filterBy: [{ term: { typeRole: 'GROUPEMENT' } }],
        sortBy: [{ nom: { order: 'asc' } }],
      },
    },
  );
  const ROLE_DATA_LIST: any[] = (rolesData && rolesData.search && rolesData.search.data) || [];
  const ROLE_LIST = [...[{ id: '-1', code: 'ALL', nom: 'Tous les rôles' }], ...ROLE_DATA_LIST];

  const handleSearchClick = () => {
    let must: any[] = [];
    if (fieldsState) {
      if (fieldsState.service && fieldsState.service !== 'ALL') {
        must.push({ term: { 'service.code': fieldsState.service } });
      }
      if (fieldsState.role && fieldsState.role !== 'ALL') {
        must.push({ term: { 'role.code': fieldsState.role } });
      }
      if (fieldsState.status && fieldsState.status !== 'ALL') {
        must.push({ term: { 'user.status': fieldsState.status } });
      }
      if (fieldsState.sortie && fieldsState.sortie !== 'ALL') {
        must.push({ term: { sortie: fieldsState.sortie } });
      }
    }
    handleRunSearch({ must });
  };

  return (
    <div className={classes.personnelGroupementFilterRoot}>
      {(servicesLoading || rolesLoading) && (
        <CircularProgress
          color="secondary"
          style={{ width: 20, height: 20, top: 18, position: 'absolute' }}
        />
      )}
      <Typography className={classes.personnelGroupementFilterTitle}>
        Saisissez les filtres qui vous conviennent
      </Typography>
      <div className={classes.personnelGroupementFilterFormContainer}>
        <CustomSelect
          label="Services"
          list={SERVICE_LIST}
          listId="code"
          index="nom"
          name="service"
          value={service || null}
          onChange={handleFieldChange}
        />
        <CustomSelect
          label="Rôle"
          list={ROLE_LIST}
          listId="code"
          index="nom"
          name="role"
          value={role || null}
          onChange={handleFieldChange}
        />
        <CustomSelect
          label="Statut"
          list={STATUT_LIST}
          listId="code"
          index="value"
          name="status"
          value={status || null}
          onChange={handleFieldChange}
        />
        <CustomSelect
          label="Sortie"
          list={SORTIE_LIST}
          listId="code"
          index="value"
          name="sortie"
          value={sortie || null}
          onChange={handleFieldChange}
        />
        <div className={classes.personnelGroupementFilterBtnsContainer}>
          <CustomButton color="default" onClick={handleResetFields}>
            Réinitialiser
          </CustomButton>
          <CustomButton color="secondary" onClick={handleSearchClick}>
            Appliquer
          </CustomButton>
        </div>
      </div>
    </div>
  );
};

export default PersonnelGroupementFilter;
