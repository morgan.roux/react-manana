import React, { FC } from 'react';
import useStyles from './styles';
import { SEARCH_FIELDS_OPTIONS } from './constants';
import { FieldsOptions } from '../../../CustomContent/interfaces';
import SearchFilter from '../../SearchFilter';
interface LaboratoireFilterProps {
  fieldsState: any;
  handleFieldChange: (event: any) => void;
  handleRunSearch: (query: any) => void;
  handleResetFields: () => void;
}

const LaboratoireFilter: FC<LaboratoireFilterProps> = ({
  fieldsState,
  handleFieldChange,
  handleRunSearch,
  handleResetFields,
}) => {
  const classes = useStyles({});

  const activeSearcFields: FieldsOptions[] = SEARCH_FIELDS_OPTIONS;

  return (
    <SearchFilter
      fieldsState={fieldsState}
      handleFieldChange={handleFieldChange}
      searchInputs={activeSearcFields}
      labelWidth={55}
      handleRunSearch={handleRunSearch}
      handleResetFields={handleResetFields}
    />
  );
};

export default LaboratoireFilter;
