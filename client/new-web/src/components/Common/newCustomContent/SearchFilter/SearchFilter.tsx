import React, { FC, useContext } from 'react';
import useStyles from './styles';
import { FieldsOptions, Option } from '../interfaces';
import {
  FormControl,
  TextField,
  InputLabel,
  Select,
  OutlinedInput,
  MenuItem,
  Button,
  Typography,
  Box,
  Divider,
} from '@material-ui/core';
import { orderBy } from 'lodash';
import { ContentStateInterface, ContentContext } from '../../../../AppContext';

interface SearchFilterProps {
  searchInputs: FieldsOptions[] | undefined;
  handleResetFields?: () => void;
  handleFieldChange?: (event: any) => void;
  disableResetButton?: boolean;
  labelWidth: number;
  fieldsState?: any;
  searchInputsLabel?: string;
  handleRunSearch?: (query: any) => void;
}

const SearchFilter: FC<SearchFilterProps> = ({
  searchInputs,
  handleResetFields,
  handleFieldChange,
  disableResetButton,
  labelWidth,
  fieldsState,
  searchInputsLabel,
  handleRunSearch,
}) => {
  const classes = useStyles({});
  const inputLabel = React.useRef<HTMLLabelElement>(null);

  const handleRunSearchClick = () => {
    if (searchInputs) {
      const query = generateQuery(fieldsState, searchInputs);
      if (handleRunSearch) handleRunSearch(query);
    }
  };
  const generateQuery = (fieldsState: any, searchInputs: FieldsOptions[]): any => {
    const must: any[] = [];

    console.log('fieldsState : ', fieldsState);

    const newFieldsState = removeAllOptionInSelect(fieldsState, searchInputs);
    console.log('newFieldsState : ', searchInputs);
    if (searchInputs) {
      searchInputs.forEach((item: FieldsOptions) => {
        if (
          newFieldsState[item.name] !== null &&
          newFieldsState[item.name] !== undefined &&
          newFieldsState[item.name] !== ''
        ) {
          if (
            item.type === 'Select' ||
            (item.type === 'Search' && item.filterType && item.filterType === 'Match')
          ) {
            must.push({
              match: {
                [item.name]: {
                  query: newFieldsState[item.name],
                },
              },
            });
          } else if (item.type === 'Search') {
            let q = `${newFieldsState[item.name]}`
              .trim()
              // .replace(/[&\/\\#,+()$~%.'":\-?<>{}]/g, ' ')
              .split(' ')
              .filter(str => str.trim().length > 0)
              .join('\\ ');

            q = item.filterType
              ? item.filterType === 'StartsWith'
                ? `${q}*`
                : item.filterType === 'EndsWith'
                ? `*${q}`
                : `*${q}*`
              : `*${q}*`;

            must.push({
              query_string: {
                query: q,
                fields: item.extraNames ? [item.name, ...item.extraNames] : [item.name],
              },
            });
          }
        }
      });
    }

    return {
      must,
    };
  };

  const removeAllOptionInSelect = (fieldsState: any, searchInputs: FieldsOptions[]): any => {
    const newStateKeys = Object.keys(fieldsState).filter(name => {
      const selectElement = searchInputs.find(
        (input: FieldsOptions) => input.name === name && input.type === 'Select',
      );

      if (!selectElement) return true;

      const fieldValue = fieldsState[name];
      return !(selectElement.options || []).some(
        (option: Option) => option.value === fieldValue && option.all,
      );
    });

    const newState: any = {};
    newStateKeys.forEach(key => {
      newState[key] = fieldsState[key];
    });
    return newState;
  };

  if (!searchInputs || searchInputs.length === 0) {
    return null;
  }

  return (
    <div className={classes.root}>
      {searchInputs && searchInputs.length > 0 && (
        <Box display="flex" flexDirection="column">
          <Typography className={classes.title}>
            Saisissez les filtres qui vous conviennent
          </Typography>
          <div className={classes.form} onSubmit={handleRunSearchClick}>
            {orderBy(searchInputs, ['code'], ['asc']).map(
              (inputItem: FieldsOptions, index: number) => {
                if (inputItem.type === 'Search') {
                  return (
                    <FormControl variant="outlined" key={index} className={classes.formControl}>
                      <TextField
                        name={inputItem.name}
                        label={inputItem.label}
                        value={fieldsState[inputItem.name] || ''}
                        placeholder={inputItem.placeholder || 'Commençant par..'}
                        onChange={handleFieldChange}
                        InputProps={{
                          classes: {
                            root: classes.cssOutlinedInput,
                            notchedOutline: classes.notchedOutline,
                          },
                        }}
                        InputLabelProps={inputItem.inputLabelProps || { shrink: true }}
                        variant={'outlined'}
                      />
                    </FormControl>
                  );
                } else if (inputItem.type === 'Select') {
                  return (
                    <FormControl variant="outlined" key={index} className={classes.formControl}>
                      <InputLabel
                        ref={inputLabel}
                        htmlFor={inputItem.id ? `${inputItem.id}` : `input-select-${index}`}
                        className={classes.selectLabel}
                      >
                        {inputItem.label}
                      </InputLabel>
                      <Select
                        name={inputItem.name}
                        value={
                          (inputItem.dataType && inputItem.dataType === 'boolean'
                            ? fieldsState[inputItem.name] !== undefined
                              ? fieldsState[inputItem.name] === true
                                ? 1
                                : 0
                              : -1
                            : fieldsState[inputItem.name]) || 0
                        }
                        onChange={handleFieldChange}
                        input={
                          <OutlinedInput
                            type="number"
                            classes={{
                              notchedOutline: classes.notchedOutline,
                              root: classes.cssOutlinedInput,
                              input: classes.cssOutlinedSelect,
                            }}
                            labelWidth={labelWidth}
                            id={inputItem.id ? `${inputItem.id}` : `input-select-${index}`}
                          />
                        }
                      >
                        {inputItem.options &&
                          (inputItem.options || []).map((menuItem: Option, index) => (
                            <MenuItem value={menuItem.value} key={index}>
                              {menuItem.label}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                  );
                }
              },
            )}
          </div>
          <div className={classes.btnContainer}>
            <Button
              onClick={handleResetFields}
              type="button"
              variant="contained"
              color="default"
              className={classes.resetButton}
              disabled={disableResetButton ? true : false}
            >
              Réinitialiser
            </Button>
            <Button
              type="submit"
              variant="contained"
              color="secondary"
              className={classes.searchButton}
              onClick={handleRunSearchClick}
            >
              Rechercher
            </Button>
          </div>
        </Box>
      )}
    </div>
  );
};

export default SearchFilter;
