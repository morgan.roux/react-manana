import { useApolloClient } from '@apollo/client';
import { Avatar, Box, Checkbox, TablePagination } from '@material-ui/core';
import CircleChecked from '@material-ui/icons/CheckCircleOutline';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import React, { Dispatch, FC, SetStateAction } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { getUser } from '../../../../services/LocalStorage';
import TablePaginationActions from '../../newTable/TablePaginationActions';
import { useStyles } from './styles';
import { LoaderSmall } from '@app/ui-kit';
import classNames from 'classnames';
import { GET_SKIP_AND_TAKE } from '../../newWithSearch/withSearch';

export type Category = 'CONTACT' | 'USER';

interface CategoryItemExtractor {
  extractPhotoPublicUrl: (info: any) => string | undefined;
  extractFullName: (info: any) => string | undefined;
  extractFonction: (info: any) => string | null;
  extractContact: (info: any) => string | null;
}
type CategoryExtractor = {
  [key in Category]: CategoryItemExtractor;
};

const extractors: CategoryExtractor = {
  USER: {
    extractPhotoPublicUrl: (participant: any) =>
      (participant.userPhoto?.fichier || participant.photo)?.publicUrl,
    extractFullName: (participant: any) => participant.userName,
    extractFonction: (participant: any) => participant.role?.nom,
    extractContact: (participant: any) => participant.email,
  },
  CONTACT: {
    extractPhotoPublicUrl: (participant: any) => participant.photo?.publicUrl,
    extractFullName: (participant: any) => `${participant.prenom} ${participant.nom}`,
    extractFonction: (participant: any) => participant.fonction,
    extractContact: (participant: any) =>
      participant.contact?.mailProf || participant.contact?.mailPerso,
  },
};

interface ListUserProps {
  category?: Category;
  listResult?: {
    loading?: boolean;
    data?: {
      search?: {
        data?: any[];
        total?: number;
      };
    };
    variables?: {
      take?: number;
      skip?: number;
    };
  };
  idTask: any;
  task: any;
  selected?: any[];
  setSelected?: Dispatch<SetStateAction<any[]>>;
  handleSelectOne?: (user: any, event: any) => void;
  handleToogleCheckUser: (user: any, event: any) => void;
}

const ListUser: FC<ListUserProps & RouteComponentProps> = ({
  category = 'USER',
  listResult,
  selected,
  setSelected,
  handleSelectOne,
  handleToogleCheckUser,
}) => {
  const classes = useStyles({});
  const user = getUser();
  const client = useApolloClient();

  const participants = listResult?.data?.search?.data || [];

  const allParticipants =
    category === 'USER'
      ? participants.filter(participant => participant && participant.id !== user.id)
      : participants;

  const ROWS_PER_PAGE_OPTIONS = [12];
  const take = listResult?.variables?.take || 12;
  const skip = listResult?.variables?.skip || 0;

  const updateSkipAndTake = (skip: number, take: number) => {
    client.writeQuery({
      query: GET_SKIP_AND_TAKE,
      data: {
        skipAndTake: {
          skip,
          take,
        },
      },
    });
  };

  const handleSelect = (participant:any,event:any) => {
    if (selected && setSelected && handleSelectOne) {
      handleSelectOne(participant, event);
    } else {
      handleToogleCheckUser(participant, event);
    }

  }

  const extract = extractors[category];

  return (
    <Box className={classes.rootListParticipant}>
      {listResult?.loading && <LoaderSmall />}

      {allParticipants.map(participant => {
         const isParticipantSelected =  (selected || []).some(({ id }) => id === participant.id);
        return <Box className={ classNames(classes.contentRootListParticipant,{[classes.selected]:isParticipantSelected} )}
        onClick={(event)=>handleSelect(participant,event)}
        >
          <Box display="flex" alignItems="center">
            <Box>
              <Avatar
                src={extract.extractPhotoPublicUrl(participant)}
                alt={extract.extractFullName(participant)}
                className={classes.avatar}
              />
            </Box>
            <Box className={classes.description}>
              <Box className={classes.title}>{extract.extractFullName(participant)}</Box>
              <Box className={classes.content}>{extract.extractFonction(participant)}</Box>
              <Box className={classes.content}>{extract.extractContact(participant)}</Box>
            </Box>
          </Box>
          <Box marginLeft="auto">
            <Checkbox
              icon={<CircleUnchecked fontSize="large" />}
              checkedIcon={<CircleChecked fontSize="large" />}
              // tslint:disable-next-line: jsx-no-lambda
              onClick={(event)=>handleSelect(participant,event)}
              checked={isParticipantSelected}
            />
          </Box>
          <Box className={classes.icon} />
        </Box>
      } )}
      {allParticipants && allParticipants.length > 0 && (
        <TablePagination
          className={classes.tablePagination}
          rowsPerPageOptions={ROWS_PER_PAGE_OPTIONS}
          // tslint:disable-next-line: jsx-no-lambda
          labelDisplayedRows={({ from, to, count }) =>
            `${from}-${to === -1 ? count : to} sur ${count !== -1 ? count : to}`
          }
          labelRowsPerPage="Nombre par page"
          component="div"
          count={listResult?.data?.search?.total || 0}
          rowsPerPage={take}
          page={skip / take}
          backIconButtonProps={{
            'aria-label': 'Page Précédente',
          }}
          nextIconButtonProps={{
            'aria-label': 'Page suivante',
          }}
          // tslint:disable-next-line: jsx-no-lambda
          onChangePage={(_, newPage) => updateSkipAndTake(newPage * take, take)}
          // tslint:disable-next-line: jsx-no-lambda
          onChangeRowsPerPage={e => updateSkipAndTake(skip, parseInt(e.target.value, 10))}
          ActionsComponent={TablePaginationActions}
        />
      )}
    </Box>
  );
};

export default withRouter(ListUser);
