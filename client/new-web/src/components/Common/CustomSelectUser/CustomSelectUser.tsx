import { useApolloClient, useQuery } from '@apollo/client';
import { Avatar, Box, Checkbox, Fab, Grid } from '@material-ui/core';
import AssignmentLateIcon from '@material-ui/icons/AssignmentLate';
import CircleChecked from '@material-ui/icons/CheckCircleOutline';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import _ from 'lodash';
import React, {
  Dispatch,
  FC,
  Fragment,
  ReactNode,
  SetStateAction,
  useEffect,
  useState,
} from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import { DO_GET_SEARCH_USER_IDS, DO_SEARCH_USERS } from '../../../graphql/User/query';
import {
  GET_SEARCH_USER_IDS,
  GET_SEARCH_USER_IDSVariables,
} from '../../../graphql/User/types/GET_SEARCH_USER_IDS';
import { SEARCH_PRT_CONTACT } from '../../../federation/search/query';
import { getPharmacie, getUser } from '../../../services/LocalStorage';
import { isMobile } from '../../../utils/Helpers';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import { CustomModal } from '../CustomModal';
import SearchInput from '../newCustomContent/SearchInput';
import WithSearch from '../newWithSearch/withSearch';
import CustomSelectTask from '../../Main/Content/TodoNew/Common/Task/CustomSelectTask';
import { CountTodosProps } from '../../Main/Content/TodoNew/TodoNew';
import ListUser, { Category } from './ListUser';
import { useStyles } from './style';
import { UserSelect } from '../../User/UserSelect';

export interface UsersModalProps {
  useInCall?: boolean;
  category?: Category;
  // Pour les contacts
  partenaireType?: string;
  idPartenaireTypeAssocie?: string;
  onValidate?: (users: any[]) => void;
  page?: boolean;
  openModal: boolean;
  setOpenModal: Dispatch<SetStateAction<boolean>>;
  withNotAssigned?: boolean;
  withAssignTeam?: boolean;
  assignTeamText?: string;
  selected?: any[];
  idParticipants?: string[];
  setSelected?: Dispatch<SetStateAction<any[]>>;
  singleSelect?: boolean;
  searchPlaceholder?: string;
  title?: string;
  isCheckedTeam?: boolean;
  setIsCheckedTeam?: Dispatch<SetStateAction<boolean>>;
  moreContent?: ReactNode;
  optionPartage?: string;
}

const CustomSelectUser: FC<UsersModalProps & RouteComponentProps & CountTodosProps> = ({
  openModal,
  useInCall,
  page,
  category = 'USER',
  idPartenaireTypeAssocie,
  partenaireType,
  setOpenModal,
  withNotAssigned = true,
  withAssignTeam = true,
  idParticipants,
  selected,
  setSelected,
  singleSelect,
  title = 'Assignation de participants',
  searchPlaceholder = 'Rechercher un participant...',
  assignTeamText = 'Assignée à toute l’équipe',
  isCheckedTeam,
  setIsCheckedTeam,
  moreContent,
  optionPartage,
  onValidate,
}) => {
  const [currentSelected, setCurrentSelected] = React.useState<any[]>([]);
  const currentPharmacie = getPharmacie();
  const optionTout = 'TOUT_LE_MONDE';

  useEffect(() => {
    setCurrentSelected(selected || []);
  }, [selected]);

  const user = getUser();
  const currentUserId = (user && user.id) || '';

  // const userPhotoUrl = (user.userPhoto?.fichier || user.photo)?.publicUrl

  const projectList = [
    { label: 'Tous les types', value: 'Tous' },
    { label: 'Pharmacie', value: 'pharmacie' },
    { label: 'Présidents de régions', value: 'userPartenaire' },
    { label: 'Partenaires de services', value: 'userLaboratoire' },
  ];

  const [open, setOpen] = useState<boolean>(false);
  const [userIds, setUserIds] = useState<any[]>([]);
  const [filtersModal, setFiltersModal] = useState<string[]>(['ALL']);

  useEffect(() => {
    setOpen(openModal);
  }, [openModal]);

  const classes = useStyles({});

  const handleToogleCheckUser = (user: any, event: any) => {
    event.stopPropagation();
    setCurrentSelected(prev => {
      if (prev.some(({ id }) => id === user.id)) {
        const next = prev.filter(currentUser => currentUser.id !== user.id);
        return next;
      } else {
        const next = [...prev, user];
        return next;
      }
    });
  };

  /*  const initialFormValue: DATAFORM = {
      searchValue: '',
      sortBy: 'Tous',
  }; */

  //const [dataParticipantList, setDataParticipantList] = useState(initialFormValue);

  const handleChange = event => {
    if (event) {
      event.preventDefault();
      event.stopPropagation();
      // TODO : Make it operational

      //  const { name, value } = event.target;
      // setDataParticipantList({ ...dataParticipantList, [name]: value });
    }
  };

  let pharmacie = '';
  let service = '';
  if (user.userPersonnel) {
    service =
      (user.userPersonnel && user.userPersonnel.service && user.userPersonnel.service.id) || '';
  } else if (user.userTitulaire || user.userPpersonnel) {
    pharmacie = (user.pharmacie && user.pharmacie.id) || '';
  }

  const optionalMust =
    category === 'CONTACT'
      ? [
        {
          term: {
            idPharmacie: currentPharmacie.id,
          },
        },
        {
          term: {
            idPartenaireTypeAssocie: idPartenaireTypeAssocie,
          },
        },
        {
          term: {
            partenaireType: partenaireType,
          },
        },
      ]
      : idParticipants && idParticipants.length
        ? [
          {
            terms: {
              _id: idParticipants,
            },
          },
        ]
        : user.userPersonnel
          ? [
            {
              term: {
                'userPersonnel.service.id': service,
              },
            },
            {
              term: {
                'userPersonnel.service.id': service,
              },
            },
          ]
          : user.userTitulaire || user.userPpersonnel
            ? [
              {
                term: {
                  'pharmacie.id': pharmacie,
                },
              },
              {
                term: {
                  'pharmacie.id': pharmacie,
                },
              },
            ]
            : [];

  const search = useQuery<GET_SEARCH_USER_IDS, GET_SEARCH_USER_IDSVariables>(
    DO_GET_SEARCH_USER_IDS,
    {
      variables: {
        query: {
          query: {
            bool: {
              must: optionalMust,
            },
          },
        },
      },
      skip: category !== 'USER' || !optionalMust.length,
      onError: error => {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );

  const ids: any[] = search?.data?.search?.data
    ? search.data.search.data.map((user: any) => user && user.id)
    : [];

  const allUsers: any[] = search?.data?.search?.data || [];

  // assignation
  const client = useApolloClient();

  const handleSelectAll = (event: any) => {
    event.preventDefault();
    event.stopPropagation();
    //  if (selected && setSelected) {
    if (currentSelected.length < allUsers.length - 1) {
      // !NOUVEAU : NOTION TOUTE L'EQUIPE
      setCurrentSelected(allUsers.filter(i => i.id !== currentUserId));
      if (setIsCheckedTeam) setIsCheckedTeam(true);
    } else {
      setCurrentSelected([]);
      if (setIsCheckedTeam) setIsCheckedTeam(false);
    }
    // }
  };

  const handleSelectOne = (user: any, event: any) => {
    event.preventDefault();
    event.stopPropagation();
    // if (selected && setSelected) {
    if (singleSelect) {
      setCurrentSelected([user]);
    } else {
      const selectedIds = currentSelected.map(i => i.id);
      if (selectedIds.includes(user.id)) {
        const newSelected = currentSelected.filter(u => u.id !== user.id);
        setCurrentSelected(newSelected);
      } else {
        const newSelected = [...currentSelected, user];
        setCurrentSelected(newSelected);
      }
    }
    //  }

    if (setIsCheckedTeam) setIsCheckedTeam(false);
  };

  const handleValidate = () => {
    if (setSelected) {
      setSelected(currentSelected);
      onValidate && onValidate(currentSelected);
    }
    setOpenModal(false);
  };

  const handleCustomClose = () => {
    setCurrentSelected(selected || []);
    setOpenModal(false);
  };

  const content = (
    <>
      <Box
        className={classes.tableContainer}
        // tslint:disable-next-line: jsx-no-lambda
        onClick={event => {
          event.preventDefault();
          event.stopPropagation();
        }}
      >
        <Box className={classes.section}>
          <Grid container={true} direction={isMobile() ? 'column' : 'row'} spacing={3}>
            <Grid item={true} xs={12} lg={8}>
              <SearchInput searchPlaceholder={searchPlaceholder} />
            </Grid>
            {category === 'USER' && !useInCall && (
              <Grid item={true} xs={12} lg={4}>
                <CustomSelectTask
                  label="Type de contact"
                  list={projectList}
                  name="sortBy"
                  className={classes.espaceHaut}
                  onChange={handleChange}
                  disabled={true}
                  value={'pharmacie'}
                />
              </Grid>
            )}
          </Grid>
        </Box>
        {/*!useInCall ? (
          <>
            {withAddParticipant && (
              <Box className={classes.section}>
                <Grid container={true} alignItems="center">
                  <Grid item={true}>
                    <Tooltip
                      title="Ajouter des personnes"
                      aria-label="add"
                      className={classes.iconCustom}
                    // onClick={openParticipantModal}
                    >
                      <Fab>
                        <GroupAddIcon />
                      </Fab>
                    </Tooltip>
                    {<AjoutParticipant
                      openModalParticipant={openModalParticipantAdd}
                      setOpenParticipant={setOpenModalParticipantAdd}
                      userIds={participantsIDs}
                      setUserIds={setParticipantIds}
                      idProjectTask={idProject}
                    />}
                  </Grid>
                  <Grid item={true} className={classes.label}>
                    <strong className={classes.label}> Ajouter des participants </strong>
                  </Grid>
                </Grid>
              </Box>
                    )*/}
        {withNotAssigned && (
          <Box className={classes.section}>
            <Grid container={true} alignItems="center" justify="space-between">
              <Grid item={true}>
                <Fab>
                  <AssignmentLateIcon />
                </Fab>
                <strong className={classes.label}> Non-assignée </strong>
              </Grid>
              <Grid item={true} className={classes.label}>
                <Checkbox
                  icon={<CircleUnchecked fontSize="large" />}
                  checkedIcon={<CircleChecked fontSize="large" />}
                  // tslint:disable-next-line: jsx-no-lambda
                  onChange={() => {
                    setCurrentSelected([]);
                  }}
                  checked={currentSelected.length === 0}
                />
              </Grid>
            </Grid>
          </Box>
        )}
        {withAssignTeam && optionalMust.length ? (
          <Fragment>
            {
              <Box className={classes.section}>
                <Grid container={true} alignItems="center">
                  <Grid item={true}>
                    <Checkbox
                      icon={<CircleUnchecked fontSize="large" />}
                      checkedIcon={<CircleChecked fontSize="large" />}
                      // tslint:disable-next-line: jsx-no-lambda
                      onClick={event => {
                        handleSelectAll(event);
                      }}
                      checked={
                        ids &&
                          ids.length &&
                          _.difference(
                            ids,
                            currentSelected.map(({ id }) => id),
                          ).length === 0
                          ? true
                          : isCheckedTeam
                            ? true
                            : currentSelected &&
                              currentSelected.length > 0 &&
                              allUsers.length === currentSelected.length
                              ? true
                              : false
                      }
                    />
                  </Grid>
                  <Grid item={true} className={classes.label}>
                    <strong className={classes.label}>{assignTeamText}</strong>
                  </Grid>
                </Grid>
              </Box>
            }
          </Fragment>
        ) : null}
        {moreContent && <Box className={classes.section}>{moreContent}</Box>}
        {user && (!idParticipants || idParticipants.includes(user.id)) && category === 'USER' && (
          <Box className={classes.section} style={{cursor:'pointer',background: user?.id && currentSelected.some(({ id }) => id === user.id)? '#F8F8F8':undefined   }}>
            <Grid
              container={true}
              direction="row"
              alignItems="center"
              justify="space-between"
              wrap="nowrap"
              onClick={event => {
                if (singleSelect) {
                  handleSelectOne(user, event);
                } else {
                  handleToogleCheckUser(user, event);
                }
              }}
            >
              <Grid container={true} direction="row" wrap="nowrap">
                <Avatar
                  alt="user"
                  src={(user.userPhoto?.fichier || user.photo)?.publicUrl}
                  className={classes.large}
                />
                <Grid item={true} className={classes.description}>
                  <Grid item={true} className={classes.title}>
                    Moi
                  </Grid>
                  <Grid item={true} className={classes.content}>
                    {user && user.role && user.role.nom}
                  </Grid>
                  <Grid item={true} className={classes.content}>
                    {user && user.email}
                  </Grid>
                </Grid>
              </Grid>

              <Grid item={true} className={classes.description}>
                <Checkbox
                  icon={<CircleUnchecked fontSize="large" />}
                  checkedIcon={<CircleChecked fontSize="large" />}
                  // tslint:disable-next-line: jsx-no-lambda
                  onClick={event => {
                    if (singleSelect) {
                      handleSelectOne(user, event);
                    } else {
                      handleToogleCheckUser(user, event);
                    }
                  }}
                  checked={user?.id && currentSelected.some(({ id }) => id === user.id)}
                />
              </Grid>
            </Grid>
          </Box>
        )}

        <Box className={classes.section}>
          <WithSearch
            type={category === 'USER' ? 'user' : 'prtcontact'}
            WrappedComponent={ListUser}
            useFederation={category === 'CONTACT'}
            searchQuery={category === 'USER' ? DO_SEARCH_USERS : SEARCH_PRT_CONTACT}
            optionalMust={optionalMust}
            props={
              !useInCall
                ? {
                  category,
                  selected: currentSelected,
                  setSelected: setCurrentSelected,
                  handleToogleCheckUser,
                  handleSelectOne,
                  paginationCentered: true,
                }
                : {}
            }
          />
        </Box>
      </Box>
    </>
  );

  const contentToutLeMonde = (
    <Box display="flex" flexDirection="column" width="100%" style={{ padding: '16px 0' }}>
      <UserSelect
        userIdsSelected={userIds}
        setUserIdsSelected={setUserIds}
        usersSelected={currentSelected}
        setUsersSelected={setCurrentSelected}
        filtersSelected={filtersModal}
        setFiltersSelected={setFiltersModal}
        disableAllFilter={false}
        clientContact
      />
    </Box>
  );

  return (
    <>
      {page ? (
        content
      ) : (
        <CustomModal
          open={open}
          setOpen={setOpenModal}
          title={title}
          withBtnsActions={true}
          withCancelButton={true}
          actionButtonTitle="Valider"
          closeIcon={true}
          headerWithBgColor={true}
          maxWidth="md"
          fullWidth={true}
          className={classes.usersModalRoot}
          // tslint:disable-next-line: jsx-no-lambda
          // tslint:disable-next-line: jsx-no-lambda
          onClick={event => {
            event.preventDefault();
            event.stopPropagation();
          }}
          disableBackdropClick={true}
          onClickConfirm={handleValidate}
          customHandleClose={handleCustomClose}
        >
          {optionPartage === optionTout ? contentToutLeMonde : content}
        </CustomModal>
      )}
    </>
  );
};

export default withRouter(CustomSelectUser);
