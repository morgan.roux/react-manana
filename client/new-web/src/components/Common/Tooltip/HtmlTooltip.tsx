import { Theme, Tooltip } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';

export const HtmlTooltip = withStyles((theme: Theme) => ({
  tooltip: {
    maxWidth: 250,
    fontSize: theme.typography.pxToRem(12),
  },
}))(Tooltip);
