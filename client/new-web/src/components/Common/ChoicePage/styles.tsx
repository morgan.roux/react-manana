import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      maxWidth: '1100px',
      margin: '0 auto',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      paddingTop: 130,

      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
      '@media (max-width: 768px)': {
        overflowY: 'auto',
        paddingTop: 86,
      },
    },
    paper: {
      flex: '1 1 300px',
      margin: '0px 16px 32px',
      maxWidth: 255,

      borderRadius: 12,
    },
    title: {
      color: '#616161',
      fontWeight: 'bold',
      fontSize: 25,
      alignContent: 'center',
      justifyContent: 'center',
    },
    subTitle: {
      marginTop: 15,
      color: '#9E9E9E',
      fontWeight: 'normal',
      fontSize: 14,
    },
    choiceContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginTop: 50,
      width: '100%',
      padding: '0 24px',
      '@media (max-width: 768px)': {
        justifyContent: 'center',
        flexWrap: 'wrap',
      },
    },
    choice: {
      width: '100%',
      height: '100%',
      padding: 16,
      textAlign: 'center',
      '&:hover': {
        backgroundColor: '#f7f7f8',
      },

      '@media (max-width: 1300px)': {
        width: '31%',
        padding: '0 20px',
      },
      '@media (max-width: 1100px)': {
        '& img': {
          maxWidth: 188,
          maxHeight: 184,
          minHeight: 184,
        },
        '& p': {
          fontSize: 16,
        },
      },
      '@media (max-width: 768px)': {
        margin: '12px 10',
        width: '32%',
        minWidth: 195,
        '& img': {
          maxWidth: 144,
          maxHeight: 140,
          minHeight: 140,
        },
        '& p': {
          fontSize: 14,
        },
      },
    },
    choiceImg: {
      maxWidth: '100%',
      maxHeight: 160,
      objectFit: 'contain',
    },
    choiceText: {
      marginTop: 16,
      fontSize: '1rem',
      textAlign: 'center',
      letterSpacing: 0,
      fontWeight: 400,
      lineHeight: 1.5,
      '& span': {
        color: theme.palette.secondary.main,
        fontWeight: 'bold',
      },
    },
    centerTextVertival: {},
    header: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      padding: '0px 25px',
      height: 60,
      position: 'relative',
      borderBottom: '1px solid #E0E0E0',
      '& .MuiIconButton-root': {
        position: 'absolute',
        color: '#27272F',
      },
    },
    headerTitle: {
      fontSize: 20,
      fontWeight: 'bold',
      textAlign: 'center',
      width: '100%',
    },
  }),
);

export default useStyles;
