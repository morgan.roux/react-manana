import React, { FC, Fragment, useState, useEffect } from 'react';
import { RouteComponentProps, withRouter, Redirect } from 'react-router-dom';
import CustomResearch from '../../Common/CustomResearch';
import { Box } from '@material-ui/core';

interface MenuComponentProps {
  label?: string;
  button?: JSX.Element;
  placeholder?: string;
}

const ResearchNavBar: FC<MenuComponentProps & RouteComponentProps> = ({
  label,
  placeholder,
  button,
}) => {
  return (
    <Box display="flex" flexDirection="row">
      {label}
      <CustomResearch placeholder={'Recherche'} />
      {button}
    </Box>
  );
};

export default withRouter(ResearchNavBar);
