import { useQuery } from '@apollo/client';
import React, { FC } from 'react';
import { GET_URGENCES } from '../../../graphql/Urgence/query';
import { URGENCES } from '../../../graphql/Urgence/types/URGENCES';
import CustomSelect from '../CustomSelect';

interface UrgenceInputProps {
  name?: string;
  value: any;
  onChange: (importance: any) => void;
  useCode?: boolean; // Use code as value
  noMinHeight?: boolean;
  disabled?: boolean;
}
const UrgenceInput: FC<UrgenceInputProps> = ({
  name,
  onChange,
  value,
  useCode = false,
  noMinHeight,
  disabled
}) => {
  const urgenceQuery = useQuery<URGENCES>(GET_URGENCES);
  const urgenceList = (urgenceQuery?.data?.urgences || []).map(urgence => ({
    ...urgence,
    color: urgence?.couleur,
    libelle: `${urgence?.code} : ${urgence?.libelle}`,
  }));

  const findUrgence = (code: string) => urgenceList?.find((urgence: any) =>
    value && useCode ? urgence?.code === code : urgence?.id === code,
  );

  const handleChange = (event: any) => {
    onChange(findUrgence(event.target.value))
  }

  const urgence: any = findUrgence(value)


  return (
    <CustomSelect
      noMinheight={noMinHeight}
      label="Urgence"
      list={urgenceList}
      name={name || 'idUrgence'}
      onChange={handleChange}
      listId={useCode ? 'code' : 'id'}
      index="libelle"
      value={value}
      required={true}
      color={urgence?.color}
      colorIndex="color"
      disabled={disabled}
    />
  );
};

export default UrgenceInput;
