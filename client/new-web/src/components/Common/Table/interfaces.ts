import { Column } from '../../Dashboard/Content/Interface';

export type Order = 'asc' | 'desc';

export interface TableToolbarProps {
  numSelected: number;
  handleSelection?: (newSelection: any[]) => void;
  handleUncheckAll?: (event: any) => void;
  setIsSelected?: (value: boolean) => void;
  isSelected?: boolean;
  total?: number;
  count: number;
  showResetFilters?: boolean;
}

export interface TableHeadProps {
  numSelected: number;
  onSelectAllClick?: () => void;
  onUncheckAll?: (event: any) => void;
  order: Order;
  orderBy: string;
  rowCount: number;
  isSelectable: boolean;
  columns: Column[] | undefined;
  columnSorted: string;
  toggleSorting: (columnName: string) => void;
  onChangePage: (newPage: number) => void;
  withAvatar?: boolean;
  data: any[];
  selected: any[] | undefined;
  total?: number;
}

export interface ItemSelected {
  [key: string]: any;
}

export interface TableProps {
  data: any[];
  isSelectable: boolean;
  selected?: ItemSelected[];
  handleSelection?: (newSelection: any[]) => void;
  columns: Column[] | undefined;
  columnSorted: string;
  toggleSorting: (columnName: string) => void;
  order: Order;
  page: number;
  onChangePage: (newPage: number) => void;
  rowsPerPage: number;
  rowsPerPageOptions: number[];
  onChangeRowsPerPage: (newRowPerPage: number) => void;
  count: number;
  densePadding: boolean;
  withAvatar?: boolean;
  avatarObjectName?: string;
  handleCheckItem?: (value: any) => void;
  handleCheckAll?: () => void;
  handleUncheckAll?: (event: any) => void;
  paginationCentered?: boolean;
  total?: number;
  hideRightPagination?: boolean;
  filteredItems?: any[];
  localDataCount?: number;
  localData?: any[];
  showResetFilters?: boolean;
  onClickRow?: (row: any) => void;
  activeRow?: any;
  isDeletable?: boolean;
  type?: string;
  variables?: any;
}
