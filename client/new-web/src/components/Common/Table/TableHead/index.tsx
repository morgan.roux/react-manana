import React, { FC, useEffect, useState } from 'react';
import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import TableHead from '@material-ui/core/TableHead';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Checkbox from '@material-ui/core/Checkbox';
import { TableHeadProps } from '../interfaces';
import { useApolloClient } from '@apollo/client';
import { Theme, lighten } from '@material-ui/core/styles';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
  root: {
    background: lighten(theme.palette.primary.main, 0.1),
    '& .MuiTableSortLabel-root.MuiTableSortLabel-active, & .MuiTableSortLabel-icon': {
      color: `${theme.palette.common.white} !important`,
    },
    '& .MuiTableCell-root': {
      padding: '0px 16px',
    },
    '& .MuiTableCell-head': {
      lineHeight: '1rem',
      height: 40,
    },
  },
  label: {
    color: theme.palette.common.white,
    fontSize: 12,
    '&:hover': {
      color: theme.palette.common.white,
      opacity: '0.6',
    },
  },
});

const EnhancedTableHead: FC<TableHeadProps & WithStyles> = ({
  classes,
  selected,
  data,
  onSelectAllClick,
  onUncheckAll,
  order,
  orderBy,
  // numSelected,
  // rowCount,
  isSelectable,
  columns,
  columnSorted,
  toggleSorting,
  onChangePage,
}) => {
  const client = useApolloClient();
  const handleNewSorting = (newColumn: any) => {
    toggleSorting(newColumn);
    // TODO: Migration
    /*(client as any).writeData({
      data: {
        sort: {
          sortItem: {
            label: newColumn,
            name: newColumn,
            direction: order === 'desc' ? 'asc' : 'desc',
            active: true,
            __typename: 'sortItem',
          },
          __typename: 'sort',
        },
      },
    });*/
    // Return to the first page when new sorting rule occurs
    onChangePage(0);
  };

  const [checkedAll, setCheckedAll] = useState<boolean>(false);

  useEffect(() => {
    if (data && selected) {
      const listIds = data.map((item: any) => item && item.id);
      const selectedIds = selected.map((item: any) => item && item.id);
      if (listIds.every((item: any) => selectedIds.includes(item))) {
        setCheckedAll(true);
      } else {
        setCheckedAll(false);
      }
    }
  }, [data, selected]);

  const handleChange = (event: any, checked: boolean) => {
    if (checkedAll && onUncheckAll) onUncheckAll(event);
    else if (!checkedAll && onSelectAllClick) onSelectAllClick();
  };

  return (
    <TableHead className={classes.root}>
      <TableRow>
        {isSelectable ? (
          <TableCell style={{ padding: '0px 4px' }}>
            <Checkbox
              // indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={checkedAll}
              onChange={handleChange}
              inputProps={{ 'aria-label': 'Select all desserts' }}
              style={{ color: '#ffffff' }}
            />
          </TableCell>
        ) : null}

        {columns
          ? (columns || []).map((column, index) => (
            <TableCell
              key={`column-${index}`}
              align={column.centered ? 'center' : 'left'}
              sortDirection={orderBy === column.name ? order : false}
            >
              <TableSortLabel
                active={orderBy === column.name ? (column.name === '' ? false : true) : false}
                hideSortIcon={column.name === '' ? true : false}
                direction={columnSorted === column.name ? order : undefined}
                onClick={() => (column.name ? handleNewSorting(column.name) : null)}
                className={classes.label}
              >
                {column.label}
              </TableSortLabel>
            </TableCell>
          ))
          : null}
      </TableRow>
    </TableHead>
  );
};

export default withStyles(styles)(EnhancedTableHead);
