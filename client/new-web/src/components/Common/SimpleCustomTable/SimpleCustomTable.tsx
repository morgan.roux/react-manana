import React, { FC, useState, ChangeEvent, MouseEvent } from 'react';
import {
  TableHead,
  TableRow,
  TableCell,
  Checkbox,
  TableSortLabel,
  TableContainer,
  Table,
  TableBody,
  Toolbar,
  Typography,
  Tooltip,
  IconButton,
} from '@material-ui/core';
import { Delete, FilterList } from '@material-ui/icons';
import classnames from 'classnames';
import useStyles from './styles';
import { getObjectByPath } from '../../../utils/getObjectByPath';
import SimpleCustomTablePagination, {
  SimpleCustomTablePaginationProps,
} from './SimpleCustomTablePagination';

function descendingComparator<T>(a: T, b: T, orderBy: string) {
  const orderByArray = orderBy.split('.');
  if (orderByArray.length > 1) {
    if (getObjectByPath(b, orderBy) < getObjectByPath(a, orderBy)) {
      return -1;
    }
    if (getObjectByPath(b, orderBy) > getObjectByPath(a, orderBy)) {
      return 1;
    }
  } else {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
  }
  return 0;
}

type Order = 'asc' | 'desc';

function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: string,
): (a: { [key in Key]: number | string }, b: { [key in Key]: number | string }) => number {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

export interface SimpleCustomTableColumn {
  disablePadding: boolean;
  key: string;
  label: string;
  numeric: boolean;
  renderer?: (row: any) => any;
}

interface SimpleCustomTableProps {
  columns: SimpleCustomTableColumn[];
  data: any[];
  showToolbar: boolean;
  selectable: boolean;
  defaultOrderByKey?: string;
  onClickRow?: (row: any) => void;
}

const SimpleCustomTable: FC<SimpleCustomTableProps & SimpleCustomTablePaginationProps> = ({
  columns,
  data,
  showToolbar,
  selectable,
  defaultOrderByKey,
  onClickRow,
  page,
  setPage,
  rowsPerPage,
  setRowsPerPage,
  total,
}) => {
  const classes = useStyles({});
  const [order, setOrder] = useState<Order>('asc');
  const defaultOrderBy = columns[0].key;
  const [orderBy, setOrderBy] = useState<string>(defaultOrderByKey || defaultOrderBy);
  const [selected, setSelected] = useState<any[]>([]);
  const selectedIds = selected.map(item => item && item.id);
  const [dense, setDense] = useState(false);
  const [clickedRow, setClickedRow] = useState<any>(null);
  const clickedRowId = clickedRow && clickedRow.id;

  const numSelected = selected.length;

  const handleSort = (property: any) => (event: MouseEvent<unknown>) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      // const newSelecteds = data.map((item, index) => (item && item.id ? item.id : index));
      // setSelected(newSelecteds);
      if (data && data.length > 0) {
        setSelected(data);
      }
      return;
    }
    setSelected([]);
  };

  const handleCheck = (row: any, rowId: string) => (event: MouseEvent<unknown>) => {
    event.preventDefault();
    event.stopPropagation();
    const selectedIndex = selectedIds.indexOf(rowId);
    let newSelected: string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, row);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const handleClickRow = (row: any, rowId: number | string) => (event: MouseEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    if (row && onClickRow) {
      onClickRow(row);
      if (rowId !== clickedRowId) {
        setClickedRow(row);
      }
    }
  };

  const handleChangeDense = (event: ChangeEvent<HTMLInputElement>) => {
    setDense(event.target.checked);
  };

  const isSelected = (rowId: string | number) => selectedIds.indexOf(rowId) !== -1;
  const isClicked = (rowId: string | number) => clickedRowId === rowId;

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, total - page * rowsPerPage);

  return (
    <div className={classes.customTableroot}>
      {showToolbar && (
        <Toolbar className={classes.toolbar}>
          {numSelected > 0 && (
            <Typography color="inherit" variant="subtitle1" component="div">
              {`${numSelected}/${total}`} Sélectionné(s)
            </Typography>
          )}
          {numSelected > 0 ? (
            <Tooltip title="Delete">
              <IconButton aria-label="delete">
                <Delete />
              </IconButton>
            </Tooltip>
          ) : (
            <Tooltip title="Filter list">
              <IconButton aria-label="filter list">
                <FilterList />
              </IconButton>
            </Tooltip>
          )}
        </Toolbar>
      )}
      <TableContainer className={classes.customTableContainer}>
        <Table
          className={classes.table}
          aria-labelledby="tableTitle"
          size={dense ? 'small' : 'medium'}
          aria-label="sticky custom table"
          stickyHeader={true}
        >
          <TableHead>
            <TableRow>
              {selectable && (
                <TableCell padding="checkbox">
                  <Checkbox
                    indeterminate={numSelected > 0 && numSelected < total}
                    checked={total > 0 && numSelected === total}
                    onChange={handleSelectAllClick}
                    inputProps={{ 'aria-label': 'select all desserts' }}
                  />
                </TableCell>
              )}
              {columns.map((column: SimpleCustomTableColumn) => {
                return (
                  <TableCell
                    key={column.key}
                    align={column.numeric ? 'left' : 'left'}
                    padding={column.disablePadding && selectable ? 'none' : 'default'}
                    sortDirection={orderBy === column.key ? order : false}
                  >
                    <TableSortLabel
                      active={orderBy === column.key}
                      direction={orderBy === column.key ? order : 'asc'}
                      onClick={handleSort(column.key)}
                    >
                      {column.label}
                      {orderBy === column.key ? (
                        <span className={classes.visuallyHidden}>
                          {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                        </span>
                      ) : null}
                    </TableSortLabel>
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>

          <TableBody>
            {stableSort(data, getComparator(order, orderBy))
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row: any, index: number) => {
                const isItemSelected = isSelected(row.id);
                const labelId = `custom-table-checkbox-${index}`;
                const isItemClicked = isClicked(row.id);
                return (
                  <TableRow
                    hover={true}
                    role="checkbox"
                    aria-checked={isItemSelected}
                    tabIndex={-1}
                    key={row.id || index}
                    selected={isItemSelected}
                    onClick={handleClickRow(row, row.id)}
                    style={{ cursor: onClickRow ? 'pointer' : 'default' }}
                    className={
                      isItemClicked ? classnames(classes.cursorPointer, classes.activeRow) : ''
                    }
                  >
                    {selectable && (
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isItemSelected}
                          inputProps={{ 'aria-labelledby': labelId }}
                          onClick={handleCheck(row, row.id)}
                          color="primary"
                        />
                      </TableCell>
                    )}
                    {columns.map((column: SimpleCustomTableColumn, columnIndex: number) => {
                      return (
                        <TableCell key={`row-${index}-${columnIndex}`} align="left">
                          {column.renderer
                            ? column.renderer(row)
                            : row[column.key] !== undefined && row[column.key] !== null
                            ? row[column.key]
                            : '-'}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            {emptyRows > 0 && (
              <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <SimpleCustomTablePagination
        total={total}
        page={page}
        setPage={setPage}
        rowsPerPage={rowsPerPage}
        setRowsPerPage={setRowsPerPage}
      />
    </div>
  );
};

export default SimpleCustomTable;
