import { ApolloProvider,ApolloClient } from '@apollo/client';
import React from 'react';
import { GRAPHQL_CLIENT } from '../../../apolloClient';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';



export const withClient = (WrappedComponent: any, client: ApolloClient<any>) => {
  return class extends React.Component {
    render = () => {
      return (
        <ApolloProvider client={client}>
            <WrappedComponent {...this.props} />
         </ApolloProvider>
      );
    };
  };
};

export const withFederationClient = (WrappedComponent: any) => withClient(WrappedComponent,FEDERATION_CLIENT)
export const withGraphqlClient = (WrappedComponent: any) => withClient(WrappedComponent,GRAPHQL_CLIENT)