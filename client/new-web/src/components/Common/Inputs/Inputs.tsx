import React, { FC, ChangeEvent, Fragment } from 'react';
import { CustomCheckbox } from '../CustomCheckbox';
import { CustomDatePicker } from '../CustomDateTimePicker';
import CustomSelect from '../CustomSelect';
import { CustomFormTextField } from '../CustomTextField';
import CustomTextarea from '../CustomTextarea';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { AutocompleteInput } from '../CustomAutocomplete';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    checkboxInputContainer: {
      '& .MuiFormControlLabel-label': {
        // color: theme.palette.secondary.main,
        // textDecoration: 'underline',
        fontSize: 14,
        letterSpacing: 0,
      },
      '& .MuiSvgIcon-root': {
        width: 20,
        height: 20,
      },
      '& .MuiCheckbox-root': {
        // color: theme.palette.common.black,
      },
      '& .Mui-checked': {
        // color: theme.palette.secondary.main,
      },
    },
  }),
);

export interface InputInterface {
  name: string;
  value: any;
  autocompleteValue?: any;
  label?: string;
  type?: 'text' | 'date' | 'select' | 'checkbox' | 'textArea' | 'autocomplete';
  disabled?: boolean;
  textAreaRow?: number;
  placeholder?: string;
  inputType?: string;
  selectOptions?: any[];
  selectOptionIdKey?: string;
  selectOptionValueKey?: string;
  required?: boolean;
}

interface InputsProps {
  inputs: InputInterface[];
  handleChange: (e: ChangeEvent<any>) => void;
  handleChangeDate: (name: string) => (date: any) => void;
  handleChangeAutoComplete: (fakeName: string, name: string) => (e: any, newValue: any) => void;
  handleChangeInputAutoComplete: (
    fakeName: string,
    name: string,
  ) => (e: ChangeEvent<any>, newInputValue: string) => void;
}

const Inputs: FC<InputsProps> = ({
  inputs,
  handleChange,
  handleChangeDate,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
}) => {
  const classes = useStyles({});

  if (inputs && inputs.length > 0) {
    return (
      <Fragment>
        {inputs.map((input, index) =>
          input.type === 'checkbox' ? (
            <div className={classes.checkboxInputContainer} key={`input_CustomCheckbox_${index}`}>
              <CustomCheckbox
                label={input.label}
                name={input.name}
                value={input.value}
                checked={input.value}
                onChange={handleChange}
                color="secondary"
                disabled={input.disabled}
              />
            </div>
          ) : input.type === 'date' ? (
            <CustomDatePicker
              key={`input_CustomDatePicker_${index}`}
              label={input.label}
              name={input.name}
              value={input.value}
              placeholder="jj/mm/aaaa"
              onChange={handleChangeDate(input.name)}
              disabled={input.disabled}
              InputLabelProps={{ shrink: true }}
            />
          ) : input.type === 'select' ? (
            <CustomSelect
              key={`input_CustomSelect_${index}`}
              listId={input.selectOptionIdKey || 'id'}
              index={input.selectOptionValueKey || 'value'}
              list={input.selectOptions || []}
              name={input.name}
              label={input.label}
              value={input.value}
              onChange={handleChange}
              disabled={input.disabled}
            />
          ) : input.type === 'autocomplete' ? (
            <AutocompleteInput
              key={`input_AutocompleteInput_${index}`}
              options={input.selectOptions || []}
              optionLabelKey={input.selectOptionValueKey || 'value'}
              value={input.autocompleteValue || ''}
              inputValue={
                input.autocompleteValue
                  ? input.autocompleteValue[input.selectOptionValueKey || 'value']
                  : input.value
              }
              name={input.name}
              onChangeAutocomplete={handleChangeAutoComplete(
                `autocompleteInput${input.name}`,
                input.name,
              )}
              onInputChange={handleChangeInputAutoComplete(
                `autocompleteInput${input.name}`,
                input.name,
              )}
              label={input.label}
              required={false}
              disableClearable={false}
              disabled={input.disabled}
            />
          ) : input.type === 'textArea' ? (
            <CustomTextarea
              key={`input_CustomTextarea_${index}`}
              label={input.label}
              name={input.name}
              value={input.value}
              onChangeTextarea={handleChange}
              rows={input.textAreaRow || 3}
              disabled={input.disabled}
            />
          ) : (
            <CustomFormTextField
              key={`input_CustomFormTextField_${index}`}
              label={input.label}
              name={input.name}
              value={input.value}
              disabled={input.disabled}
              type={input.inputType || 'text'}
              onChange={handleChange}
            />
          ),
        )}
      </Fragment>
    );
  }

  return null;
};

export default Inputs;
