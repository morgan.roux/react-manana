import CustomAutocomplete from './CustomAutocomplete';
import AutocompleteInput from './AutocompleteInput';

export { AutocompleteInput };

export default CustomAutocomplete;
