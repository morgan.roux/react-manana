import React, { FC, ReactNode, ChangeEvent } from 'react';
import { Autocomplete } from '@material-ui/lab';
import useStyles from './styles';
import { LoaderSmall } from '../../Dashboard/Content/Loader';
import { TextField } from '@material-ui/core';
import { TextFieldProps } from '@material-ui/core/TextField';
import classnames from 'classnames';

interface CustomAutocompleteProps {
  options: any[];
  optionLabelKey: string;
  value: any;
  inputValue: any;
  id?: string;
  loading?: boolean;
  loadingText?: ReactNode;
  noOptionsText?: string;
  disableClearable?: boolean;
  autoSelect?: boolean;
  shrink?: boolean;
  disabled?: boolean;
  onChangeAutocomplete: (event: any, newValue: string | null) => void;
  onInputChange: (event: ChangeEvent<any>, newInputValue: string) => void;
}

const AutocompleteInput: FC<CustomAutocompleteProps & TextFieldProps> = ({
  options,
  optionLabelKey,
  value,
  inputValue,
  loading,
  loadingText,
  noOptionsText,
  disableClearable,
  autoSelect,
  shrink,
  className,
  disabled,
  onChangeAutocomplete,
  onInputChange,
  id,
  ...props
}) => {
  const classes = useStyles({});

  return (
    <Autocomplete
    //  classes={classes}
      value={value}
      onChange={onChangeAutocomplete}
      inputValue={inputValue}
      onInputChange={onInputChange}
      id={id || 'controllable-states-demo'}
      options={options ? options : []}
      getOptionLabel={(option: any) => option[`${optionLabelKey}`]}
      renderInput={params => <TextField {...params} {...props} InputLabelProps={{ shrink }} />}
      loading={loading}
      loadingText={loadingText}
      noOptionsText={noOptionsText}
      disableClearable={disableClearable}
      disabled={disabled}
      autoSelect={autoSelect}
      className={classnames(classes.autocomplete, className)}
    />
  );
};

AutocompleteInput.defaultProps = {
  loadingText: <LoaderSmall />,
  noOptionsText: 'Aucun résultat',
  disableClearable: true,
  autoSelect: true,
  label: 'Default Label',
  variant: 'outlined',
  fullWidth: true,
  margin: 'dense',
  required: true,
  shrink: true,
};

export default AutocompleteInput;
