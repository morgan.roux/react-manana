import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
const drawerWidth = 327;
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: { width: '100%', display: 'flex', flexDirection: 'row' },
    sidebar: {
      overflowY: 'auto',
      padding: '16px 24px 24px',
      overflowX: 'hidden',
      height: 'calc(100vh - 86px)',
      width: 326,
      [theme.breakpoints.down('sm')]: {
        width: '100%',
      },
    },
    sidebarTitleContainer: {
      display: 'flex',
      alignItems: 'center',
    },
    sidebarTitle: {
      fontSize: 20,
      fontWeight: 'bold',
      fontFamily: 'Montserrat',
      color: theme.palette.secondary.main,
    },
    main: {
      width: '100%',
      height: 'calc(100vh - 86px)',
      overflow: 'auto',
    },
    appBar: {
      display: 'block',
      marginTop: 86,
      zIndex: 1,
      // [theme.breakpoints.up('lg')]: {
      //   width: `calc(100% - ${drawerWidth}px)`,
      //   marginLeft: drawerWidth,
      //   display: 'none',
      // },
    },
    toolbarTitle: {
      color: theme.palette.secondary.main,
      fontSize: 20,
      marginLeft: 16,
      fontWeight: 'bold',
      [theme.breakpoints.down('md')]: {
        color: theme.palette.common.white,
        fontSize: 30,
        width: '100%',
        textAlign: 'center',
      },
    },

    menuButton: {
      marginRight: theme.spacing(2),
      position: 'absolute',
      [theme.breakpoints.up('lg')]: {
        display: 'none',
      },
    },
  }),
);

export default useStyles;
