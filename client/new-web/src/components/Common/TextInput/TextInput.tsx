import React, { FC, Fragment, useState, useEffect } from 'react';
import { RouteComponentProps, withRouter, Redirect } from 'react-router-dom';
import useStyles from './styles';
import { Box, TextField } from '@material-ui/core';

interface TextInputProps {
  label: string;
  placeholder: string;
  validationType: string[];
  isSubmitted: boolean;
}
const TextInput: FC<TextInputProps & RouteComponentProps<any, any, any>> = ({
  label,
  placeholder,
  validationType,
  isSubmitted,
}) => {
  const [value, setValue] = useState<string>('');
  const [errorMessage, setErrorMessage] = useState<string>('');
  const handleChange = (e: any) => {
    setValue(e);
  };

  useEffect(() => {
    if (isSubmitted) {
      setErrorMessage('');
      if (validationType.includes('required') && value === '') {
        setErrorMessage(`${label} est requis`);
      }
    }
  }, [isSubmitted]);
  return (
    <Box>
      <TextField
        variant="outlined"
        value={value}
        placeholder={placeholder}
        label={label}
        onChange={e => handleChange(e.target.value)}
      />
      {<span>{errorMessage}</span>}
    </Box>
  );
};

export default withRouter(TextInput);
