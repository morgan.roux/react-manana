import React, { ChangeEvent, FC, useEffect, useRef, useState } from 'react';
import { useApolloClient, useLazyQuery } from '@apollo/client';
import { debounce } from 'lodash';

import {
  SEARCH_PARAMETRE_LABORATOIRES as SEARCH_PARAMETRE_LABORATOIRES_TYPE,
  SEARCH_PARAMETRE_LABORATOIRESVariables,
} from '../../../federation/search/types/SEARCH_PARAMETRE_LABORATOIRES';
import { SEARCH_PARAMETRE_LABORATOIRES } from '../../../federation/search/query';
import { GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE } from '../../../graphql/PartenairesServices/query';
import {
  SEARCH_CUSTOM_CONTENT_PARTENAIRE,
  SEARCH_CUSTOM_CONTENT_PARTENAIREVariables,
} from '../../../graphql/PartenairesServices/types/SEARCH_CUSTOM_CONTENT_PARTENAIRE';

import SnackVariableInterface from '../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import CustomAutocomplete from '../CustomAutocomplete';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import useStyles from './style';
import classnames from 'classnames';

const isNumber = (value: string) => {
  try {
    return !isNaN(parseFloat(value));
  } catch (error) {
    return false;
  }
};

export interface PartenaireInputProps {
  className?: string;
  index: 'partenaire' | 'laboratoire';
  label?: string;
  value?: any;
  defaultValue?: any;
  onChange: (value: any) => void;
}


const PartenaireInput: FC<PartenaireInputProps> = ({
  className,
  index,
  value,
  defaultValue,
  label,
  onChange,
}) => {
  const classes = useStyles();

  const client = useApolloClient();
  const [searchText, setSearchText] = useState<string>('');
  const [initialized, setInitialized] = useState<boolean>(false)

  // Get laboratoires list
  const [searchLaboratoires, searchLaboratoiresResult] = useLazyQuery<
    SEARCH_PARAMETRE_LABORATOIRES_TYPE,
    SEARCH_PARAMETRE_LABORATOIRESVariables
  >(SEARCH_PARAMETRE_LABORATOIRES, {
    fetchPolicy: 'cache-and-network',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: 'Une erreur est survenue lors du chargement des laboratoires',
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },

    client: FEDERATION_CLIENT,
  });

  const [searchPartenaires, searchPartenairesResult] = useLazyQuery<
    SEARCH_CUSTOM_CONTENT_PARTENAIRE,
    SEARCH_CUSTOM_CONTENT_PARTENAIREVariables
  >(GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE, {
    fetchPolicy: 'cache-and-network',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: 'Une erreur est survenue lors du chargement des prestataires de service',
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },

    onCompleted: (data) => {
      if (!initialized && defaultValue && data?.search?.data) {
        setInitialized(true)


        const defaultValuePartenaire: any = data.search.data.find(({ id }: any) => defaultValue == id)

        if (defaultValuePartenaire) {
          onChange(defaultValuePartenaire)
         // setSearchText(defaultValuePartenaire.nom)
        }


      }

    },
  });

  const debouncedSearch = useRef(
    debounce((searchText?: string) => {
      const must:any[] = [] 
      if(searchText){
        let formattedSearchText = searchText.replace(/[&\/\\#,+()$~%.'":\-?<>{}]/g, ' ');
        if (isNumber(formattedSearchText)) {
          formattedSearchText = `"${formattedSearchText}"`
        }
        must.push({
          query_string: {
            query: formattedSearchText.startsWith('*') || formattedSearchText.endsWith('*') ? formattedSearchText : `*${formattedSearchText}*`,
          }
        })
      }
      if(typeof value==="string" && value){// id
        must.push({
            term: {
              _id: value,
            },
        })
      }

      const query = must.length===0? undefined:
      must.length===1 ?
      {
        query: {
          bool:   {
            must: must[0],
          },
        
      }
    }: {

        query: {
          bool: {
            should: must
          }
        }
      }

      console.log('*****************************************query',query,searchText)

      switch (index) {
        case 'laboratoire':
          searchLaboratoires({
            variables: {
              index: ['laboratoires'],
              query,
              sortBy: [{ nom: { order: 'asc' } }],
              skip: 0,
              take: 20,
            },
          });
          break;
        case 'partenaire': {
          searchPartenaires({
            variables: {
              type: ['partenaire'],
              query,
              skip: 0,
              take: 20,
            },
          });
        }
      }
    }, 1000),
  );

  useEffect(() => {
    debouncedSearch.current();
  }, []);


  useEffect(() => {
    if (value) {
      setSearchText(value.nom);
    }
  }, [value]);
  // Debounced Search Laboratoire
  useEffect(() => {
    debouncedSearch.current(searchText);
  }, [searchText]);

  const options =
    index === 'laboratoire'
      ? searchLaboratoiresResult.data?.search?.data
      : searchPartenairesResult.data?.search?.data;
  const loading =
    index === 'laboratoire' ? searchLaboratoiresResult.loading : searchPartenairesResult.loading;
  const error =
    index === 'laboratoire' ? searchLaboratoiresResult.error : searchPartenairesResult.error;


    console.log('**************************Default value : completed', value)

  return (
    <CustomAutocomplete
      classes={
        {
          root: classnames(classes.root, className),
        } as any
      }
      loading={loading}
      options={options || []}
      optionLabelKey="nom"
      value={value}
      onAutocompleteChange={onChange}
      search={setSearchText}
      label={label}
      required={true}
      disabled={false}
      placeholder={index === "laboratoire" ? "Choisir le laboratoire" : "Choisir le prestataire de service"}
    />
  );
};

export default PartenaireInput;
