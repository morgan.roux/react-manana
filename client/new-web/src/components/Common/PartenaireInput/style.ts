import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
  root: {
    width: '100%',
    marginTop: 'unset',
    '& > div': {
      height: '50px !important',
    }
  }
}));
