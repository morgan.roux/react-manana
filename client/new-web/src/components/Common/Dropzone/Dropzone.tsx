import React, { FC, useState, useEffect } from 'react';
import { useDropzone } from 'react-dropzone';
import useStyles from './styles';
import FileIcon from '@material-ui/icons/InsertDriveFile';
import AlarmIcon from '@material-ui/icons/Alarm';
import {
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  Link,
  Typography,
  Box,
} from '@material-ui/core';
import moment from 'moment';
import { useMutation } from '@apollo/client';
import CloseIcon from '@material-ui/icons/Close';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { uploadToS3 } from '../../../services/S3';
import { AxiosResponse } from 'axios';
import classnames from 'classnames';
import { last } from 'lodash';
import CustomButton from '../CustomButton';
import { v4 as uuidv4 } from 'uuid';

export interface DropzoneProps {
  idSource?: string;
  startUpload?: boolean;
  contentText?: string;
  acceptFiles?: string;
  multiple?: boolean;
  disabled?: boolean;
  setStartUpload?: (state: boolean) => void;
  setFileUploadResult?: (
    result: AxiosResponse<any | null>,
  ) => void | React.Dispatch<React.SetStateAction<AxiosResponse<any> | null>>;
  setSelectedFiles?: (files: File[]) => void;
  selectedFiles?: File[];
  withImagePreview?: boolean;
  uploadDirectory?: string;
  identifierPrefix?: string;
  setFilesOut?: React.Dispatch<React.SetStateAction<File[]>>;
  fileAlreadySetUrl?: string;
  setFileAlreadySet?: React.Dispatch<React.SetStateAction<string | undefined>>;
  customLabel?: string;
  where?: string;
  onClickDelete?: () => void;
  onClickResize?: () => void;
  previewUrl?: string;
  withFileIcon?: boolean;
  withImagePreviewCustomized?: boolean;
}

export const formatFilename = (
  file: File,
  directory: string = 'files',
  identifierPrefix: string = moment().format('YYYYMMDDhmmss'),
): string => {
  const cleanFilename = file.name.toLowerCase().replace(/[^a-z0-9]/g, '');
  const extension = file.name.split('.').pop();
  const newFilename = `${directory ? `${directory}/` : ``
    }${identifierPrefix}${cleanFilename}.${extension}`;
  return newFilename;
};

const Dropzone: FC<DropzoneProps> = ({
  startUpload,
  setStartUpload,
  setFileUploadResult,
  contentText,
  acceptFiles,
  setSelectedFiles,
  selectedFiles,
  multiple,
  withImagePreview,
  uploadDirectory,
  identifierPrefix,
  setFilesOut,
  fileAlreadySetUrl,
  setFileAlreadySet,
  customLabel,
  disabled,
  where,
  onClickDelete,
  onClickResize,
  previewUrl,
  withFileIcon,
  withImagePreviewCustomized,
}) => {
  const classes = useStyles({});
  const [files, setFiles] = useState<File[]>([]);
  const [oneFileError, setOneFileError] = useState<boolean>(false);

  const inUserSettings = where === 'inUserSettings';
  const isInPubForm = where === 'inPubForm' || where === 'inProduitForm';

  const onDrop = (acceptedFiles: File[]) => {
    setFiles([...files, ...acceptedFiles]);
    if (setFilesOut) setFilesOut([...files, ...acceptedFiles]);
    if (setSelectedFiles && files) {
      setSelectedFiles([...files, ...acceptedFiles]);
    }
    if (setFileAlreadySet) setFileAlreadySet(undefined);
  };

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    acceptedFiles,
    open,
    isDragAccept,
    isDragReject,
  } = useDropzone({
    disabled,
    noClick: true,
    noKeyboard: true,
    multiple: multiple !== undefined ? multiple : false,
    onDrop,
    accept:
      acceptFiles ||
      'image/*, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf, application/vnd.ms-powerpoint, application/vnd.openxmlformats-officedocument.presentationml.presentation, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });

  const removeFile = (file: File) => () => {
    const newFiles = [...files];
    newFiles.splice(newFiles.indexOf(file), 1);
    acceptedFiles.splice(acceptedFiles.indexOf(file), 1);
    setFiles(newFiles);
    if (setFilesOut) setFilesOut(newFiles);
    if (setSelectedFiles) {
      setSelectedFiles(newFiles);
    }
  };

  const removeAllFile = () => {
    const newFiles = [];
    setFiles(newFiles);
    if (setFilesOut) setFilesOut(newFiles);
    if (setSelectedFiles) {
      setSelectedFiles(newFiles);
    }
    if (fileAlreadySetUrl && setFileAlreadySet) {
      setFileAlreadySet(undefined);
    }
  };

  useEffect(() => {
    if (selectedFiles) {
      setFiles(selectedFiles);
      if (setFilesOut) setFilesOut(selectedFiles);
    }
  }, [selectedFiles]);

  const FILE_EXIST: boolean =
    fileAlreadySetUrl || (selectedFiles && selectedFiles.length > 0) ? true : false;
  const CUSTOMIZED_FILE_CONTENAIRE: boolean =
    (inUserSettings || withImagePreviewCustomized) && FILE_EXIST ? true : false;

  const filesList = files.map(file => {
    return (
      <ListItem
        key={file.name}
        className={
          CUSTOMIZED_FILE_CONTENAIRE
            ? classnames(classes.fileContainer, classes.fileContainerCustomized)
            : classes.fileContainer
        }
      >
        {withImagePreview && (
          <img
            className={
              withImagePreviewCustomized ? classes.previewImageCustomized : classes.previewImage
            }
            // src={previewUrl ? previewUrl : URL.createObjectURL(file)}
            src={previewUrl && previewUrl}
            alt=""
          />
        )}

        {!withImagePreviewCustomized && (
          <Box className={classes.contentListFile}>
            <ListItemText>{file.name}</ListItemText>
            <ListItemSecondaryAction className={classes.closeIcon}>
              <IconButton
                edge="end"
                aria-label="delete"
                size="small"
                className={classes.btnIcon}
                onClick={removeFile(file)}
              >
                <CloseIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </Box>
        )}
      </ListItem>
    );
  });

  const initFileAlreadySet = () => {
    if (setFileAlreadySet) setFileAlreadySet(undefined);
  };

  const ImagePreview = () => {
    return (
      <ListItem
        key={uuidv4()}
        className={
          CUSTOMIZED_FILE_CONTENAIRE
            ? classnames(classes.fileContainer, classes.fileContainerCustomized)
            : classes.fileContainer
        }
      >
        {withImagePreview && (
          <img
            className={
              withImagePreviewCustomized ? classes.previewImageCustomized : classes.previewImage
            }
            src={previewUrl ? previewUrl : fileAlreadySetUrl ? fileAlreadySetUrl : ''}
            alt=""
          />
        )}
        {!withImagePreviewCustomized && (
          <ListItemSecondaryAction className={classes.closeIcon}>
            <IconButton
              edge="end"
              aria-label="delete"
              size="small"
              className={classes.btnIcon}
              onClick={initFileAlreadySet}
            >
              <CloseIcon />
            </IconButton>
          </ListItemSecondaryAction>
        )}
      </ListItem>
    );
  };

  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: data => {
      // On Complete presigned url, Upload to S3
      acceptedFiles.map(file => {
        if (data && data.createPutPresignedUrls) {
          data.createPutPresignedUrls.map(item => {
            if (item && item.filePath === formatFilename(file, uploadDirectory, identifierPrefix)) {
              uploadToS3(file, item.presignedUrl)
                .then(result => {
                  if (result && result.status === 200) {
                    if (setStartUpload && setFileUploadResult) {
                      setStartUpload(false);
                      setFileUploadResult(result);
                    }
                  }
                })
                .catch(error => {
                  console.log(error);
                });
            }
          });
        }
      });
    },
  });

  // If startUpload true, start upload
  useEffect(() => {
    if (startUpload) {
      const filePathsTab: string[] = [];
      acceptedFiles.map((file: File) => {
        filePathsTab.push(formatFilename(file, uploadDirectory, identifierPrefix));
      });
      // Create presigned url
      doCreatePutPresignedUrl({ variables: { filePaths: filePathsTab } });
    }
  }, [startUpload]);

  const handleClickDelete = () => {
    removeAllFile();
    if (onClickDelete) {
      onClickDelete();
    }
  };

  const NoFileInDropzone = () => {
    return (
      <>
        {withFileIcon && <FileIcon />}
        <span>
          {contentText
            ? contentText
            : customLabel
              ? customLabel
              : 'Glissez-déposer des fichiers ici'}
        </span>
        <span>ou</span>
        <Link component="button" variant="body2" onClick={open} className={classes.btnLink}>
          Parcourir
        </Link>
      </>
    );
  };

  const UserSettingsBtns = () => {
    return (
      <>
        <div className={classes.btnsActionsContainer}>
          {/* <CustomButton
            color="default"
            className={classnames(classes.btn, classes.btnDelete)}
            onClick={handleClickDelete}
          >
            Supprimer
          </CustomButton> */}
          <div className={classes.btnsEditContainer}>
            <CustomButton color="secondary" className={classes.btn} onClick={open}>
              Modifier
            </CustomButton>
            <CustomButton color="secondary" className={classes.btn} onClick={onClickResize}>
              Redimensionner
            </CustomButton>
          </div>
        </div>
      </>
    );
  };

  const PubFormBtns = () => {
    return (
      <>
        <div className={classes.btnsEditContainer}>
          <CustomButton
            color="default"
            className={classnames(classes.btn, classes.btnDelete)}
            onClick={handleClickDelete}
          >
            Supprimer
          </CustomButton>
          <CustomButton color="secondary" className={classes.btn} onClick={open}>
            Modifier
          </CustomButton>
        </div>
      </>
    );
  };

  return (
    <div
      className={
        disabled ? classnames(classes.container, classes.containerDisabled) : classes.container
      }
    >
      <div {...getRootProps()} className={classes.dropzone}>
        <input {...getInputProps()} />
        {isDragAccept && <p>Déposez les fichiers ici ...</p>}
        {isDragReject && <p>Certains fichiers seront rejetés</p>}
        {!isDragActive && (
          <>
            {inUserSettings && FILE_EXIST && (
              <Typography className={classes.previewText}>
                Aperçu de la photo redimensionnée
              </Typography>
            )}
            <div
              className={
                (inUserSettings || withImagePreviewCustomized) && FILE_EXIST
                  ? classnames(classes.layout, classes.layoutFlexRow)
                  : classes.layout
              }
            >
              {!multiple ? fileAlreadySetUrl ? <ImagePreview /> : last(filesList) : null}
              {(!FILE_EXIST || (!inUserSettings && !withImagePreviewCustomized)) && (
                <NoFileInDropzone />
              )}
              {inUserSettings && FILE_EXIST && <UserSettingsBtns />}
              {isInPubForm && FILE_EXIST && <PubFormBtns />}
            </div>
          </>
        )}
      </div>
      {oneFileError && (
        <Box textAlign="center">
          <Typography className={classes.btnLink}>
            <AlarmIcon /> Accepter un seul fichier
          </Typography>
        </Box>
      )}
      {multiple ? (
        <aside className={classes.filesList}>
          <List dense={true}>{filesList}</List>
        </aside>
      ) : null}
    </div>
  );
};

Dropzone.defaultProps = {
  withFileIcon: true,
};

export default Dropzone;
