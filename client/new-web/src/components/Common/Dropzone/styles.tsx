import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      width: '100%',
    },
    containerDisabled: {
      color: 'grey !important',
      pointerEvents: 'none',
      '& *': {
        color: 'grey !important',
        pointerEvents: 'none',
      },
    },
    dropzone: {
      width: 'auto',
      padding: '24px',
      border: '1px dashed #585858',
      borderRadius: 5,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      marginBottom: 24,
    },
    layout: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      '& li': {
        listStyle: 'none',
      },
    },
    layoutFlexRow: {
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'flex-end',
      '& li div': {
        margin: 0,
      },
    },
    filesList: {
      width: 'auto',
      '& ul': {
        display: 'flex',
        flexWrap: 'wrap',
        '& li': {
          width: 'auto',
          margin: '0px 5px',
          backgroundColor: '#fafafa',
        },
      },
    },
    contentListFile: {
      border: '1px solid #9E9E9E',
      display: 'flex',
      padding: theme.spacing(0.5),
      alignItems: 'start',
      marginLeft: theme.spacing(0.75),
      marginBottom: theme.spacing(0.75),
    },
    btnIcon: {
      color: '#fff',
      backgroundColor: '#000',
      '& span': {
        width: 16,
        height: 16,
      },
    },
    btnLink: {
      color: theme.palette.secondary.main,
    },
    fileContainer: {
      display: 'flex',
      flexDirection: 'column',
      padding: 0,
      marginBottom: 20,
    },
    fileContainerCustomized: {
      width: '100%',
      alignItems: 'flex-start',
      marginBottom: 0,
    },
    previewImageCustomized: {
      maxWidth: '100%',
      width: 400,
      borderRadius: 5,
    },
    previewImage: {
      maxWidth: 160,
    },

    closeIcon: {
      position: 'relative',
      right: 'inherit',
      margin: theme.spacing(0, 1),
    },
    btnsActionsContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      width: '100%',
    },
    btnsEditContainer: {
      display: 'flex',
      width: '100%',
      justifyContent: 'flex-end',
      '& button:nth-child(1)': {
        marginRight: 15,
      },
    },
    btn: {
      boxShadow: '0px 3px 2px #14141429',
      borderRadius: 3,
      fontWeight: 'bold',
      letterSpacing: 0,
    },
    btnDelete: {
      background: '#F5F6FA 0% 0% no-repeat padding-box',
    },
    previewText: {
      marginBottom: 15,
      fontSize: 20,
      fontWeight: 'bold',
      fontFamily: 'Montserrat',
    },
  }),
);

export default useStyles;
