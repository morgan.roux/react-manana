import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
const drawerWidth = 336;
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    drawerPaper: {
      width: drawerWidth,
      top: 86,
      boxShadow: 'none!important',
      [theme.breakpoints.down('sm')]: {
        width: '100%',
        top: 0,
        height: '100%',
      },
    },
  }),
);
export default useStyles;
