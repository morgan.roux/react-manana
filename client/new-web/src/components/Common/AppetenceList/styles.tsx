import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    infoPersoContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'center',
      marginBottom: 25,
      position: 'relative',
    },
    inputsContainer: {
      border: '1px solid #DCDCDC',
      borderRadius: 12,
      width: '100%',
      '& > div:not(:nth-last-child(1))': {
        borderBottom: '1px solid #DCDCDC',
      },
    },
    inputTitle: {
      fontWeight: 'bold',
      fontSize: 18,
      color: theme.palette.secondary.main,
      marginBottom: 15,
    },
    permissionAccessSubTite: {
      fontSize: 16,
      fontWeight: 'normal',
      marginBottom: 15,
    },
    appetenceFormRow: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      padding: '0px',
      '& .MuiFormControl-root': {
        width: 70,
      },
      '& > .MuiBox-root': {
        margin: '0px !important',
        padding: '10px 16px',
        '& .MuiFormControlLabel-label': {
          fontSize: 15,
          marginLeft: 25,
        },
        '& svg': {
          width: 20,
          height: 20,
        },
      },
      '& > .MuiBox-root:not(:nth-last-child(1))': {
        borderBottom: '1px solid #DCDCDC',
      },
      '& *': {
        fontWeight: '600',
        fontFamily: 'Montserrat',
      },
    },
    paginationContainer: {
      width: '100%',
      marginTop: 20,
      '& .MuiTablePagination-toolbar': {
        padding: 0,
      },
      '& .MuiTablePagination-spacer': {
        display: 'none',
      },
    },
    noDataContainer: {
      width: '100%',
      fontWeight: 'bold',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      '& > span': {
        marginBottom: 10,
      },
      '& > button': {
        textTransform: 'none',
      },
    },
  }),
);

export default useStyles;
