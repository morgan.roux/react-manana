import { AppBar, Box, CssBaseline, Hidden, IconButton, Typography } from '@material-ui/core';
import { ArrowBack, Close, Menu } from '@material-ui/icons';
import React, { FC, ReactNode } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { isSm, isMd } from '../../../utils/Helpers';
import { useStyles } from './styles';


interface MobileTopBarProps {
  title?: string;
  withBackBtn?: boolean;
  withCloseBtn?: boolean;
  optionBtn?: ReactNode[];
  closeUrl?: string | null;
  onClickBack?: () => void;
  backUrl?: string;
  handleDrawerToggle?: () => void;
  withTopMargin?: boolean;
  fullScreen?: boolean;
  onClose?: () => void;
  breakpoint?: 'sm' | 'md'
}

const MobileTopBar: FC<MobileTopBarProps & RouteComponentProps> = ({
  history,
  title,
  withBackBtn,
  withCloseBtn,
  optionBtn,
  closeUrl,
  onClickBack,
  handleDrawerToggle,
  withTopMargin,
  fullScreen,
  backUrl,
  onClose,
  breakpoint = 'md'
}) => {
  const classes = useStyles({});

  const handleGoBack = () => {
    if (onClickBack) {
      onClickBack();
    }
    else if(backUrl){
      history.push(backUrl)
    }
    else {
      history.goBack();
    }
  };

  const handleClose = () => {
    if (onClose) {
      onClose();
    } else {
      history.push(closeUrl || '/');
    }
  };


  const showMenuButton = isMd()
  const hiddenOptionButtonProps = breakpoint === 'sm' ? { mdUp: true } : { smUp: true }
  return (
    <AppBar
      position={fullScreen ? 'relative' : 'fixed'}
      className={classes.root}
      style={{ marginTop: withTopMargin ? '86px' : '0px', height: fullScreen ? '86px' : '50px' }}
    >
      <CssBaseline />
      <Box display="flex" alignItems="center">
        {showMenuButton ? (
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
          >
            <Menu />
          </IconButton>
        ) : (
          withBackBtn && (
            <IconButton color="inherit" onClick={handleGoBack}>
              <ArrowBack />
            </IconButton>
          )
        )}

        <Typography>{title}</Typography>
      </Box>
      {withCloseBtn && (
        <IconButton color="inherit" onClick={handleClose}>
          <Close />
        </IconButton>
      )}
      {optionBtn && (
        <Hidden {...hiddenOptionButtonProps} implementation="css">
          <Box>{optionBtn.map(btn => btn)}</Box>
        </Hidden>
      )}
    </AppBar>
  );
};

export default withRouter(MobileTopBar);
