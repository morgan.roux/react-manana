import React, { FC, ChangeEvent } from 'react';
import useStyles from './styles';
import { useQuery } from '@apollo/client';
import { MINIM_SERVICES } from '../../../graphql/Service/types/MINIM_SERVICES';
import { GET_MINIM_SERVICES } from '../../../graphql/Service';
import { SEARCH_ROLES, SEARCH_ROLESVariables } from '../../../graphql/Role/types/SEARCH_ROLES';
import { DO_SEARCH_ROLES } from '../../../graphql/Role';
import { CircularProgress, Typography } from '@material-ui/core';
import CustomSelect from '../CustomSelect';
import CustomButton from '../CustomButton';
import { GET_DEPARTEMENTS } from '../../../graphql/Departement/query';
import { DEPARTEMENTS } from '../../../graphql/Departement/types/DEPARTEMENTS';
import CustomAutocomplete from '../CustomAutocomplete';
import { GET_PERSONNEL_FONTIONS } from '../../../graphql/PersonnelFonction/query';
import { PERSONNEL_FONCTIONS } from '../../../graphql/PersonnelFonction/types/PERSONNEL_FONCTIONS';
import { CustomModal } from '../CustomModal';
import { CustomFormTextField } from '../CustomTextField';
import { GET_SEARCH_AFFECTATION_PERSONNEL_LIST } from '../../../graphql/PersonnelAffectation/query';
import {
  SEARCH_AFFECTATION_PERSONNEL_LIST,
  SEARCH_AFFECTATION_PERSONNEL_LISTVariables,
} from '../../../graphql/PersonnelAffectation/types/SEARCH_AFFECTATION_PERSONNEL_LIST';
import { last, split, head, values } from 'lodash';
import { CustomDatePicker } from '../CustomDateTimePicker';
import { RouteComponentProps, withRouter } from 'react-router';
import {
  PARTENAIRE_SERVICE_URL,
  PERSONNEL_GROUPEMENT_URL,
  PRESIDENT_REGION_URL,
} from '../../../Constant/url';
import { GET_PRESIDENT_FONCTIONS } from '../../../graphql/PpersonnelFonction/query';
import { GET_SEARCH_AFFECTATION_TITULAIRE_LIST } from '../../../graphql/TitulaireAffectation/query';
import { GET_SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST } from '../../../graphql/PartenaireRepresentant/query';
import { AttributionDepartementIcon } from '../../Dashboard/TableActionColumn';

interface AffectationDialogProps {
  open: boolean;
  setOpen: (value: boolean) => void;
  setPersonnelToAffect?: (value: any) => void;
  personnelToAffect: any;
  affectPersonnel: any;
}

const AffectationDialog: FC<AffectationDialogProps & RouteComponentProps> = ({
  open,
  setOpen,
  setPersonnelToAffect,
  personnelToAffect,
  affectPersonnel,
  location: { pathname },
}) => {
  const classes = useStyles({});
  const {
    id,
    action,
    personnel,
    titulaire,
    partenaireRepresentant,
    personnelFonction,
    partenaireRepresentantDemandeAffectation,
    personnelDemandeAffectation,
    titulaireFonction,
    titulaireDemandeAffectation,
    departement,
    dateDebut,
    dateFin,
  } = personnelToAffect;

  const isOnPersonnel = pathname.includes(PERSONNEL_GROUPEMENT_URL);
  const isOnPartenaireRepresentant = pathname.includes(PARTENAIRE_SERVICE_URL);
  const isOnAffectation = pathname.includes('/affectation');

  const confirmDisabled =
    !departement || (departement && !departement.id) || !dateDebut || !dateFin || isOnPersonnel
      ? !personnelFonction || (personnelFonction && !personnelFonction.id)
      : isOnPartenaireRepresentant
        ? false
        : !titulaireFonction || (titulaireFonction && !titulaireFonction.id);

  const remplacantDisabled =
    !action || action.type === 'DEPARTEMENT' || action.type === 'REMPLACEMENT_ARRET_SANS';
  const title = "Nouvelle action d'affectation territoriale";
  const { data: departementsData, loading: departementsLoading } = useQuery<DEPARTEMENTS>(
    GET_DEPARTEMENTS,
  );

  const DEPARTEMENT_LIST = (departementsData && departementsData.departements) || [];

  const { data: fonctionList, loading: fonctionsLoading } = useQuery(
    isOnPersonnel ? GET_PERSONNEL_FONTIONS : GET_PRESIDENT_FONCTIONS,
  );

  const FONCTION_LIST = isOnPartenaireRepresentant
    ? (partenaireRepresentant && partenaireRepresentant.fonction) || ''
    : isOnPersonnel
      ? (fonctionList && fonctionList.personnelFonctions && fonctionList.personnelFonctions.data) ||
      []
      : (fonctionList && fonctionList.titulaireFonctions && fonctionList.titulaireFonctions.data) ||
      [];

  const { data: affectationListData, loading: affectationListLoading } = useQuery(
    isOnPersonnel
      ? GET_SEARCH_AFFECTATION_PERSONNEL_LIST
      : isOnPartenaireRepresentant
        ? GET_SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST
        : GET_SEARCH_AFFECTATION_TITULAIRE_LIST,
    {
      variables: isOnPersonnel
        ? { type: ['personnel'] }
        : isOnPartenaireRepresentant
          ? { type: ['partenairerepresentant'] }
          : { type: ['titulaire'] },
    },
  );

  console.log('values : ', personnelToAffect);

  const LIST =
    (affectationListData && affectationListData.search && affectationListData.search.data) || [];

  const handleApplyClick = () => {
    const inputs = {
      id,
      typeAffectation: action && action.type,
      fonction: partenaireRepresentant && partenaireRepresentant.fonction,
      idFonction:
        (personnelFonction && personnelFonction.id) || (titulaireFonction && titulaireFonction.id),
      idsDepartements: [departement && departement.id],
      idAffecte:
        (partenaireRepresentant && partenaireRepresentant.id) ||
        (personnel && personnel.id) ||
        (titulaire && titulaire.id),
      idRemplacent:
        action && action.type === 'REMPLACEMENT_ARRET_SANS'
          ? null
          : isOnPartenaireRepresentant
            ? partenaireRepresentantDemandeAffectation &&
            partenaireRepresentantDemandeAffectation.remplacent &&
            partenaireRepresentantDemandeAffectation.remplacent.id
            : isOnPersonnel
              ? personnelDemandeAffectation &&
              personnelDemandeAffectation.personnelRemplacent &&
              personnelDemandeAffectation.personnelRemplacent.id
              : titulaireDemandeAffectation &&
              titulaireDemandeAffectation.titulaireRemplacent &&
              titulaireDemandeAffectation.titulaireRemplacent.id,
      dateDebut,
      dateFin,
    };
    affectPersonnel({ variables: isOnPartenaireRepresentant ? { input: inputs } : inputs });
    setOpen(false);
  };

  const handleResetFields = () => {
    if (setPersonnelToAffect) setPersonnelToAffect({ action });
  };

  const otherHandleChange = (name: string, value: any, checked: boolean) => {
    const nameArray = split(name, '.');
    const nameKey: any = head(nameArray);
    const exactName: any = last(nameArray);
    if (name.includes('checkbox') && setPersonnelToAffect) {
      if (nameKey === exactName && exactName !== 'cap') {
        setPersonnelToAffect(prevState => ({
          ...prevState,
          [exactName]: checked,
        }));
      } else {
        setPersonnelToAffect(prevState => ({
          ...prevState,
          [nameKey]: { ...prevState[nameKey], [exactName]: checked },
        }));
      }
    } else {
      if (setPersonnelToAffect)
        setPersonnelToAffect(prevState => ({
          ...prevState,
          [nameKey]: { ...prevState[nameKey], [exactName]: value },
        }));
    }
  };

  const handleChangeDate = (name: string) => (date: any) => {
    if (date && name && setPersonnelToAffect) {
      setPersonnelToAffect(prevState => ({ ...prevState, [name]: date }));
    }
  };

  const handleChange = (e: ChangeEvent<any>) => {
    const { name, value, checked, type } = e.target;
    if (name.includes('.')) {
      otherHandleChange(name, value, checked);
      return;
    }
    if (e && e.target) {
      const { name, value } = e.target;
      if (name && value && setPersonnelToAffect)
        setPersonnelToAffect(prevState => ({ ...prevState, [name]: value }));
    }
  };

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title={title}
      withBtnsActions={false}
      closeIcon={true}
      headerWithBgColor={true}
      maxWidth="xl"
    >
      <div className={classes.personnelGroupementFilterRoot}>
        {(departementsLoading || fonctionsLoading || affectationListLoading) && (
          <CircularProgress
            color="secondary"
            style={{ width: 20, height: 20, top: 18, position: 'absolute' }}
          />
        )}
        <div className={classes.personnelGroupementFilterFormContainer}>
          <CustomFormTextField
            label="Type d'action"
            value={action && action.label}
            disabled={true}
          />
          {isOnPartenaireRepresentant ? (
            <CustomFormTextField
              label="Quelle fonction ?"
              value={partenaireRepresentant && partenaireRepresentant.fonction}
              onChange={handleChange}
              name="partenaireRepresentant.fonction"
            />
          ) : (
            <CustomAutocomplete
              id="fonctionId"
              options={FONCTION_LIST}
              optionLabelKey="nom"
              value={isOnPersonnel ? personnelFonction : titulaireFonction}
              onAutocompleteChange={value => {
                if (setPersonnelToAffect)
                  setPersonnelToAffect(prevState => ({
                    ...prevState,
                    [isOnPersonnel ? 'personnelFonction' : 'titulaireFonction']: value,
                  }));
              }}
              label="Quelle fonction ?"
              required={false}
            />
          )}
          <CustomAutocomplete
            id="departementId"
            options={DEPARTEMENT_LIST}
            optionLabelKey="nom"
            value={AttributionDepartementIcon}
            onAutocompleteChange={value => {
              if (setPersonnelToAffect)
                setPersonnelToAffect(prevState => ({
                  ...prevState,
                  ['departement']: value,
                }));
            }}
            label="Quelle département ?"
            required={false}
          />
          {isOnPartenaireRepresentant && !isOnAffectation ? (
            <CustomFormTextField
              value={`${partenaireRepresentant &&
                partenaireRepresentant.nom} ${partenaireRepresentant &&
                partenaireRepresentant.prenom}`}
            />
          ) : (
            <CustomAutocomplete
              id="remplaçantId"
              options={LIST}
              optionLabelKey={isOnPartenaireRepresentant ? 'nom' : 'fullName'}
              value={
                isOnPartenaireRepresentant
                  ? partenaireRepresentantDemandeAffectation?.remplacent
                  : isOnPersonnel
                    ? personnelDemandeAffectation?.personnelRemplacent
                    : titulaireDemandeAffectation?.titulaireRemplacent
              }
              onAutocompleteChange={value =>
                otherHandleChange(
                  isOnPersonnel
                    ? 'personnelDemandeAffectation.personnelRemplacent'
                    : isOnPartenaireRepresentant
                      ? 'partenaireRepresentantDemandeAffectation.remplacent'
                      : 'titulaireDemandeAffectation.titulaireRemplacent',
                  value,
                  false,
                )
              }
              label="Nom du remplaçant"
              required={false}
              disabled={remplacantDisabled}
            />
          )}

          <CustomDatePicker
            label="Date début"
            placeholder="Date début"
            onChange={handleChangeDate('dateDebut')}
            name="dateDebut"
            value={dateDebut || null}
            className={classes.dateInput}
            disablePast={false}
          />
          <CustomDatePicker
            label="Date fin"
            placeholder="Date fin"
            onChange={handleChangeDate('dateFin')}
            name="dateFin"
            value={dateFin || null}
            className={classes.dateInput}
            disablePast={false}
            minDate={dateDebut}
          />
          <div className={classes.personnelGroupementFilterBtnsContainer}>
            <CustomButton color="default" onClick={handleResetFields}>
              Réinitialiser
            </CustomButton>
            <CustomButton color="secondary" onClick={handleApplyClick} disabled={confirmDisabled}>
              Appliquer
            </CustomButton>
          </div>
        </div>
      </div>
    </CustomModal>
  );
};

export default withRouter(AffectationDialog);
