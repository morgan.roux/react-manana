import React, { FC, useState, useEffect } from 'react';
import { Box } from '@material-ui/core';
import { RouteComponentProps, withRouter, Redirect } from 'react-router-dom';
import withSearch from '../withSearch';
import CreateOC from '../../Main/Content/OperationCommerciale/CreateOC';
import CreateActualite from '../../Main/Content/Actualite/Create';
import { useLazyQuery, useQuery } from '@apollo/client';
import { SEARCH as SEARCH_Interface, SEARCHVariables } from '../../../graphql/search/types/SEARCH';
import { SEARCH, GET_CUSTOM_CONTENT_SEARCH } from '../../../graphql/search/query';
import ICurrentPharmacieInterface from '../../../Interface/CurrentPharmacieInterface';
import { GET_CURRENT_PHARMACIE } from '../../../graphql/Pharmacie/local';
import { GET_OPERATION_SEARCH } from '../../../graphql/OperationCommerciale/query';
import { getGroupement } from '../../../services/LocalStorage';
interface EditComponentProps {
  match: {
    params: { id: string | undefined; type: string | undefined };
  };
}
const EditComponent: FC<EditComponentProps & RouteComponentProps<any, any, any>> = ({
  match,
  history,
}) => {
  const type = match && match.params && match.params.type;
  const id = match && match.params && match.params.id;
  const groupement = getGroupement();
  const [currentPharmacie, setCurrentPharmacie] = useState<any>();
  const [searchType, setSearchType] = useState<string>('');
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

  const [getResult, listResult] = useLazyQuery<SEARCH_Interface, SEARCHVariables>(
    type && type === 'operations-commerciales' ? GET_OPERATION_SEARCH : SEARCH,
    {
      variables: {
        type: [searchType],
        query: {
          query: { term: { _id: id && id } },
        },
        take: 1,
        idPharmacie: currentPharmacie && currentPharmacie.id,
        idGroupement: groupement && groupement.id,
        // idOperation: id && id,
      },
      fetchPolicy: 'cache-and-network',
    },
  );

  useEffect(() => {
    if (myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie && id) {
      switch (type) {
        case 'operations-commerciales':
          setSearchType('operation');
      }
      setCurrentPharmacie(myPharmacie.data.pharmacie);
      getResult();
    }
  }, [myPharmacie]);

  switch (type) {
    case 'operations-commerciales': {
      const operation =
        listResult &&
        listResult.data &&
        listResult.data.search &&
        listResult.data.search.data &&
        listResult.data.search.data[0];
      return <CreateOC title={"Modification d'une opération commerciale"} operation={operation} />;
    }

    case 'actualite':
      return <Box>{withSearch(CreateActualite, 'actualite')}</Box>;

    default:
      return <Redirect to="/" />;
  }
};

export default withRouter(EditComponent);
