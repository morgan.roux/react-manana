import React, { FC, useState } from 'react';
import { DateRangePicker as AirbnbDateRangePicker } from 'react-dates';
import { Moment } from 'moment';
import useStyles from './styles';
import { FormControl, InputLabel } from '@material-ui/core';

interface DateRangePickerProps {
  startDate: Moment | null;
  endDate: Moment | null;
  startDateId?: string;
  endDateId?: string;
  startDatePlaceholderText?: string;
  endDatePlaceholderText?: string;
  numberOfMonths?: number;
  withPortal?: boolean;
  showDefaultInputIcon?: boolean;
  focusedInput?: 'startDate' | 'endDate' | null;
  getDates: (startDate: Moment | null, endDate: Moment | null) => void;
  isOutsideRange?: (day?: any) => boolean;
  onFocusChange?: (arg: 'startDate' | 'endDate' | null) => void;
  label?: string;
  shrink?: boolean;
  required?: boolean;
  variant?: 'standard' | 'outlined' | 'filled' | undefined;
  showClearDates?: boolean;
}

const DateRangePicker: FC<DateRangePickerProps> = ({
  startDate,
  endDate,
  startDateId,
  endDateId,
  getDates,
  startDatePlaceholderText,
  endDatePlaceholderText,
  isOutsideRange,
  numberOfMonths,
  withPortal,
  showDefaultInputIcon,
  focusedInput,
  onFocusChange,
  label,
  shrink,
  required,
  variant,
  showClearDates,
}) => {
  const classes = useStyles({});
  const [defaultFocusedInput, setdefaultFocusedInput] = React.useState<
    'startDate' | 'endDate' | null
  >(null);

  // const [deb, setDeb] = useState<Moment | null>(null);
  // const [fin, setFin] = useState<Moment | null>(null);

  // const handleChange = (arg: {startDate: Moment | null, endDate: Moment | null}) => {
  //   setDeb(startDate)
  //   setFin(endDate)
  // }

  return (
    <FormControl className={classes.root} required={required} variant={variant}>
      <InputLabel shrink={shrink} className={classes.label}>
        {label}
      </InputLabel>
      <AirbnbDateRangePicker
        startDate={startDate}
        startDateId={startDateId || 'startDateUniqueId'}
        endDate={endDate}
        endDateId={endDateId || 'endDateUniqueId'}
        //onDatesChange={handleChange}
        onDatesChange={({ startDate, endDate }) => getDates(startDate, endDate)}
        focusedInput={focusedInput !== undefined ? focusedInput : defaultFocusedInput}
        onFocusChange={focusedInput =>
          onFocusChange !== undefined
            ? onFocusChange(focusedInput)
            : setdefaultFocusedInput(focusedInput)
        }
        showDefaultInputIcon={showDefaultInputIcon !== undefined ? showDefaultInputIcon : true}
        inputIconPosition="after"
        startDatePlaceholderText={startDatePlaceholderText || 'Date de début'}
        endDatePlaceholderText={endDatePlaceholderText || 'Date de fin'}
        hideKeyboardShortcutsPanel={true}
        isOutsideRange={isOutsideRange}
        numberOfMonths={numberOfMonths}
        withPortal={withPortal}
        showClearDates={showClearDates ? true : false}
      />
    </FormControl>
  );
};

export default DateRangePicker;
