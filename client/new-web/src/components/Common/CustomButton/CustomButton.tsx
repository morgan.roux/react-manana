import React, { FC } from 'react';
import Button, { ButtonProps } from '@material-ui/core/Button';
import useStyles from './styles';
import classnames from 'classnames';

const CustomButton: FC<ButtonProps> = (props: ButtonProps) => {
  const { className, children } = props;
  const classes = useStyles({});

  return (
    <Button
      onClick={event => {
        event.preventDefault();
        event.stopPropagation();
      }}
      {...props}
      className={
        className ? classnames(classes.customButtonRoot, className) : classes.customButtonRoot
      }
    >
      {children}
    </Button>
  );
};

CustomButton.defaultProps = {
  color: 'default',
  size: 'medium',
  variant: 'contained',
  disabled: false,
};

export default CustomButton;
