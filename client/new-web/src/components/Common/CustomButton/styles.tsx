import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    customButtonRoot: {
      fontSize: 14,
      letterSpacing: 0,
    },
  }),
);

export default useStyles;
