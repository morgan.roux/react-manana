import React, { FC, useEffect } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import useStyles from './styles';
import { Box } from '@material-ui/core';
import UserAction from '../../../../UserAction';
import CommentList from '../../../../../Comment/CommentList';
import { Comment } from '../../../../../Comment';
import { useQuery, useMutation } from '@apollo/client';
import { COMMENTS, COMMENTSVariables } from '../../../../../../graphql/Comment/types/COMMENTS';
import { GET_COMMENTS } from '../../../../../../graphql/Comment';
import { GET_SMYLEYS } from '../../../../../../graphql/Smyley';
import { getGroupement } from '../../../../../../services/LocalStorage';
import { SMYLEYS, SMYLEYSVariables } from '../../../../../../graphql/Smyley/types/SMYLEYS';
import {
  CREATE_GET_PRESIGNED_URL,
  CREATE_GET_PRESIGNED_URLVariables,
} from '../../../../../../graphql/S3/types/CREATE_GET_PRESIGNED_URL';
import { DO_CREATE_GET_PRESIGNED_URL } from '../../../../../../graphql/S3';
interface RightContentCommentProps {
  item: any;
  codeItem: string;
  listResult: any;
}

const RightContentComment: FC<RightContentCommentProps & RouteComponentProps<any, any, any>> = ({
  item,
  codeItem,
  listResult,
}) => {
  const classes = useStyles({});
  const loading = false;

  const groupement = getGroupement();
  // Get smyleys by groupement
  const getSmyleys = useQuery<SMYLEYS, SMYLEYSVariables>(GET_SMYLEYS, {
    variables: {
      idGroupement: groupement && groupement.id,
    },
  });

  const [doCreateGetPresignedUrls, doCreateGetPresignedUrlsResult] = useMutation<
    CREATE_GET_PRESIGNED_URL,
    CREATE_GET_PRESIGNED_URLVariables
  >(DO_CREATE_GET_PRESIGNED_URL);

  const smyleys = getSmyleys && getSmyleys.data && getSmyleys.data.smyleys;
  const presignedUrls =
    doCreateGetPresignedUrlsResult &&
    doCreateGetPresignedUrlsResult.data &&
    doCreateGetPresignedUrlsResult.data.createGetPresignedUrls;

  useEffect(() => {
    const filePaths: string[] = [];
    if (smyleys) {
      smyleys.map(smyley => {
        if (smyley) filePaths.push(smyley.photo);
      });
      if (filePaths.length > 0) {
        // create presigned
        doCreateGetPresignedUrls({ variables: { filePaths } });
      }
    }
  }, [getSmyleys.data]);

  const getComments = useQuery<COMMENTS, COMMENTSVariables>(GET_COMMENTS, {
    variables: {
      codeItem,
      idItemAssocie: (item && item.id) || '',
      take: 10,
      skip: 0,
    },
    fetchPolicy: 'cache-and-network',
  });
  const nbComment =
    (getComments &&
      getComments.data &&
      getComments.data.comments &&
      getComments.data.comments.total) ||
    0;

  const nbSmyley = (item && item.userSmyleys && item.userSmyleys.total) || 0;

  const fetchMoreComments = () => {
    getComments.fetchMore({
      variables: {
        codeItem,
        idItemAssocie: (item && item.id) || '',
        take: 10,
        skip:
          (getComments &&
            getComments.data &&
            getComments.data.comments &&
            getComments.data.comments.data &&
            getComments.data.comments.data.length) ||
          0,
      },
      updateQuery: (prev: any, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev;

        return {
          ...prev,
          comments: {
            ...prev.comments,
            data: [
              ...((prev.comments && prev.comments.data) || []),
              ...((fetchMoreResult && fetchMoreResult.comments && fetchMoreResult.comments.data) ||
                []),
            ],
          },
        } as any;
      },
    });
  };

  return (
    <Box>
      {/* User Actions  */}

      <Box margin="0px 60px">
        <hr className={classes.hrHeight} />
        {item && item.id && (
          <UserAction
            codeItem={codeItem}
            idSource={item && item.id}
            nbSmyley={nbSmyley}
            nbComment={nbComment || 0}
            nbShare={0}
            userSmyleys={(item && item.userSmyleys && item.userSmyleys) || undefined}
            presignedUrls={presignedUrls}
            smyleys={smyleys}
          />
        )}
      </Box>
      {/* Comments */}
      <Box margin="20px 60px">
        {getComments && getComments.data && getComments.data.comments && (
          <CommentList
            comments={getComments.data.comments}
            fetchMoreComments={fetchMoreComments}
            loading={loading}
          />
        )}
        {item && item.id && <Comment codeItem={codeItem} idSource={item.id} />}
      </Box>
    </Box>
  );
};

export default withRouter(RightContentComment);
