import React, { FC } from 'react';
import { useStyles } from './styles';
import { Box } from '@material-ui/core';
interface SeenIconProps {}

const SeenIcon: FC<SeenIconProps> = ({}) => {
  const classes = useStyles({});
  return <Box className={classes.root}></Box>;
};

export default SeenIcon;
