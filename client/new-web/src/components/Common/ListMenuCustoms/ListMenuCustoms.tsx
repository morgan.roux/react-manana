import React, { FC, MouseEvent } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';

interface ListMenuCustomsProps {
  label: string;
  onClick?: () => void;
  icon?: JSX.Element;
}

const ListMenuCustoms: FC<ListMenuCustomsProps & RouteComponentProps> = ({
  label,
  onClick,
  icon,
}) => {
  const handleClick = (event: MouseEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    if (onClick) onClick();
  };
  return (
    <List component="nav" onClick={handleClick}>
      <ListItem button={true}>
        <ListItemIcon>{icon ? icon : <div></div>}</ListItemIcon>
        <ListItemText primary={label} />
      </ListItem>
    </List>
  );
};

export default withRouter(ListMenuCustoms);
