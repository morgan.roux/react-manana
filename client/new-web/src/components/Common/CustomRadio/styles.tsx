import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
    noLabel: {
      '& fieldset > legend > span': {
        display: 'none',
      },
    },
    formControl: {
      width: '100%',
      marginBottom: 16,
      '& .MuiSelect-selectMenu': {
        minHeight: '30px !important',
        display: 'flex',
        alignItems: 'center',
      },
      '& label': {
        '& .MuiFormLabel-asterisk': {
          color: 'red',
        },
      },
      '&:hover label, & label.Mui-focused': {
        color: theme.palette.primary.main,
      },
      '&.Mui-focused fieldset': {
        border: `2px solid ${theme.palette.primary.main} !important`,
      },
      '& input:invalid + fieldset': {
        borderColor: 'red',
      },
      '& .Mui-focused .MuiIconButton-label svg': {
        color: `${theme.palette.primary.main} !important`,
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: theme.palette.common.white,
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          // borderColor: theme.palette.common.black,
        },
        '&:hover fieldset': {
          borderColor: theme.palette.primary.main,
        },
        '&.Mui-focused fieldset': {
          borderColor: theme.palette.primary.main,
        },
      },
      transition:
        'padding-left 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms,border-color 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms,border-width 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms',
    },
    label: {
      background: theme.palette.common.white,
      // color: theme.palette.common.black,
      padding: '0 4px',
    },
    expandBtnContainer: {
      marginRight: 0,
    },
    nbrProduits: {
      padding: '2px 6px',
      fontSize: 10,
      background: '#E0E0E0',
      borderRadius: 2,
      height: 20,
      minWidth: 20,
      textAlign: 'center',
      marginRight: 10,
    },
    fontSize14: {
      fontSize: '0.875rem',
    },
    noStyle: {
      listStyleType: 'none',
      '& .MuiListItem-root': {
        padding: '4px 0',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
      },
      '& .MuiRadio-root': {
        padding: '0 8px 0 0!important',
      },
    },
    checkBoxLeftName: {
      '& .MuiCheckbox-root': {
        padding: 0,
        color: '#000000',
        marginRight: 8,
      },
      '&$checked': {
        color: '#000000',
      },
    },
  }),
);

export default useStyles;
