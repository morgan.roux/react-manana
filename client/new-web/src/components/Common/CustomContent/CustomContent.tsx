import React, { FC, useContext, useEffect, useState, useRef, useMemo } from 'react';
import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import { Theme } from '@material-ui/core/styles';
import { useLazyQuery, useQuery, useApolloClient } from '@apollo/client';
import { GET_CUSTOM_CONTENT_SEARCH, SEARCH } from '../../../graphql/search/query';
import Table from '../Table';
import { SearchInput } from '../../Dashboard/Content/SearchInput';
import { Loader } from '../../Dashboard/Content/Loader';
import { FieldsOptions, Option, Column } from './interfaces';
import { ContentContext, ContentStateInterface } from '../../../AppContext';
import { Typography, IconButton, Box, lighten } from '@material-ui/core';
import { getGroupement, getUser } from '../../../services/LocalStorage';
import {
  SortInterface,
  FiltersInterface,
  SearchInterface,
  GET_LOCAL_SORT,
  GET_LOCAL_FILTERS,
  GET_LOCAL_SEARCH,
  resetSearchFilters,
  FrontFiltersInterface,
  GET_LOCAL_FRONT_FILTERS,
  CodeCanalActiveInterface,
  GET_CODE_CANA_ACTIVE,
} from '../withSearch/withSearch';
import ICurrentPharmacieInterface from '../../../Interface/CurrentPharmacieInterface';
import { GET_CURRENT_PHARMACIE } from '../../../graphql/Pharmacie/local';
import { debounce } from 'lodash';
import {
  PARAMETER_BY_CODE,
  PARAMETER_BY_CODEVariables,
} from '../../../graphql/Parametre/types/PARAMETER_BY_CODE';
import { GET_PARAMETER_BY_CODE } from '../../../graphql/Parametre/query';
import { CODE_LIGNE_TABLEAU } from '../../../Constant/parameter';
import SearchFilter from './SearchFilter';
import { Tune } from '@material-ui/icons';
import moment from 'moment';
import { ME_me } from '../../../graphql/Authentication/types/ME';
import Backdrop from '../Backdrop';
import { SEARCH as SEARCH_INTERFACE, SEARCHVariables } from '../../../graphql/search/types/SEARCH';
import {
  CUSTOM_CONTENT_SEARCHVariables,
  CUSTOM_CONTENT_SEARCH,
} from '../../../graphql/search/types/CUSTOM_CONTENT_SEARCH';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
  root: {
    width: '100%',
    '& a': {
      cursor: 'pointer',
    },
  },
  searchBar: {
    display: 'flex',
    height: 70,
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '0px 24px 0 24px',
    background: lighten(theme.palette.primary.main, 0.1),
    '@media (max-width: 692px)': {
      flexWrap: 'wrap',
      justifyContent: 'center',
    },
  },
  search: {
    display: 'flex',
    alignItems: 'center',
    '@media (max-width: 580px)': {
      marginBottom: 15,
    },
  },
  filterButton: {
    marginLeft: theme.spacing(3),
    color: theme.palette.common.white,
  },

  filterButtonLabel: {
    fontFamily: 'Roboto',
    fontSize: 14,
    color: theme.palette.common.white,
  },

  alignCenter: {
    textAlign: 'center',
    margin: '40px 0px',
    fontSize: 20,
    fontWeight: 'bold',
  },
  formControl: {
    width: 240,
    marginRight: theme.spacing(6),
    marginBottom: theme.spacing(3),
  },
  cssOutlinedInput: {
    '&$cssFocused $notchedOutline': {
      borderColor: `${theme.palette.primary.main} !important`,
    },
  },
  notchedOutline: {
    borderWidth: '1px',
    // borderColor: `${theme.palette.primary.dark} !important`,
  },
  searchButton: {
    width: 240,
    height: 51,
    marginRight: theme.spacing(6),
  },
  resetButton: {
    width: 240,
    height: 51,
  },
});

interface ContentProps {
  type: string;
  enableGlobalSearch: boolean;
  searchPlaceholder: string;
  filterBy?: any;
  sortBy?: any;
  columns?: Column[];
  buttons?: any;
  searchInputs?: FieldsOptions[];
  searchInputsLabel?: string;
  isSelectable?: boolean;
  datas?: any[];
  checkedItems?: any[];
  total?: number;
  checkedAll?: boolean;
  onItemsChecked?: (selection: any[]) => void;
  children?: any;
  densePadding?: boolean;
  rowsPerPageOptions?: number[];
  disableResetButton?: boolean;
  handleCheckItem?: (value: any) => void;
  handleCheckAll?: () => void;
  handleUncheckAll?: (event: any) => void;
  inSelectContainer?: boolean;
  searchInFullWidth?: boolean;
  noPrefixIcon?: boolean;
  paginationCentered?: boolean;
  optionalMust?: any;
  defaultSortName?: string;
  fieldsOptions?: { include?: string[]; exclude: string[] };
  setMinimalQuery?: (query: any) => void;
  useLocalData?: boolean;
  showResetFilters?: boolean;
  idCommandeCanal?: string;
  hideSearchBar?: boolean;
  hideSearchInput?: boolean;
  onClickRow?: (row: any) => void;
  activeRow?: any;
  isDeletable?: boolean;
  deletedIds?: string[];
}

const ROWS_PER_PAGE_OPTIONS = [12, 25, 50, 100, 250, 500, 1000, 2000];

const removeAllOptionInSelect = (fieldsState: any, searchInputs: FieldsOptions[]): any => {
  const newStateKeys = Object.keys(fieldsState).filter(name => {
    const selectElement = searchInputs.find(
      (input: FieldsOptions) => input.name === name && input.type === 'Select',
    );

    if (!selectElement) return true;

    const fieldValue = fieldsState[name];
    return !(selectElement.options || []).some(
      (option: Option) => option.value === fieldValue && option.all,
    );
  });

  const newState: any = {};
  newStateKeys.forEach(key => {
    newState[key] = fieldsState[key];
  });
  return newState;
};

// Generate elastic search query from fields value
const generateQuery = (fieldsState: any, searchInputs: FieldsOptions[]): any => {
  const must: any[] = [];
  const newFieldsState = removeAllOptionInSelect(fieldsState, searchInputs);

  if (searchInputs) {
    searchInputs.forEach((item: FieldsOptions) => {
      if (
        newFieldsState[item.name] !== null &&
        newFieldsState[item.name] !== undefined &&
        newFieldsState[item.name] !== ''
      ) {
        if (
          item.type === 'Select' ||
          (item.type === 'Search' && item.filterType && item.filterType === 'Match')
        ) {
          must.push({
            match: {
              [item.name]: {
                query: newFieldsState[item.name],
              },
            },
          });
        } else if (item.type === 'Search') {
          let q = `${newFieldsState[item.name]}`
            .trim()
            // .replace(/[&\/\\#,+()$~%.'":\-?<>{}]/g, ' ')
            .split(' ')
            .filter(str => str.trim().length > 0)
            .join('\\ ');

          q = item.filterType
            ? item.filterType === 'StartsWith'
              ? `${q}*`
              : item.filterType === 'EndsWith'
              ? `*${q}`
              : `*${q}*`
            : `*${q}*`;

          must.push({
            query_string: {
              query: q,
              fields: item.extraNames ? [item.name, ...item.extraNames] : [item.name],
            },
          });
        }
      }
    });
  }

  return {
    query: {
      bool: { must },
    },
  };
};

// Remove occurrence by key in array
const getUniqueListBy = (data: any[], key: string) =>
  Array.from(data.reduce((m, t) => m.set(t[key], t), new Map()).values());

const Content: FC<ContentProps & WithStyles> = ({
  classes,
  type,
  columns,
  enableGlobalSearch,
  searchPlaceholder,
  filterBy,
  sortBy,
  buttons,
  searchInputs,
  isSelectable,
  datas,
  checkedItems,
  onItemsChecked,
  children,
  densePadding,
  total,
  checkedAll,
  rowsPerPageOptions,
  disableResetButton,
  handleCheckItem,
  handleCheckAll,
  handleUncheckAll,
  inSelectContainer,
  searchInFullWidth,
  noPrefixIcon,
  paginationCentered,
  optionalMust,
  defaultSortName,
  fieldsOptions,
  setMinimalQuery,
  useLocalData,
  showResetFilters,
  idCommandeCanal,
  searchInputsLabel,
  hideSearchBar,
  onClickRow,
  hideSearchInput,
  activeRow,
  isDeletable,
  deletedIds,
}) => {
  const {
    content: { query: searchText },
    setContent,
  } = useContext<ContentStateInterface>(ContentContext);
  const [rowsPerPage, setRowsPerPage] = useState(ROWS_PER_PAGE_OPTIONS[0]);
  const [page, setPage] = useState(0);
  const [count, setCount] = useState(total || 0);
  const [order, setOrder] = useState<'desc' | 'asc'>('desc');
  const [columnSorted, setColumnSorted] = useState<string>('');
  const [sorted, setSorted] = useState<any>();
  const [fieldsState, setFieldsState] = useState<any>({});
  const [query, setQuery] = useState<any>({});
  const [selected, setSelected] = useState<any[]>([]);
  const inputLabel = React.useRef<HTMLLabelElement>(null);
  const [labelWidth, setLabelWidth] = React.useState(0);
  const [totalLine, setTotalLine] = useState<number>(0);
  const [filteredItems, setFilteredItems] = useState<any[]>([]);
  const [localData, setLocalData] = useState<any[]>([]);
  const [showSearchFilters, setShowSearchFilters] = useState<boolean>(hideSearchBar ? true : false);
  const [idCanal, setIdCanal] = useState<string>();
  const groupement = getGroupement();
  // const searchQuery = SEARCH; // inSelectContainer ? GET_MINIMAL_SEARCH : SEARCH;
  const frontFilters = useQuery<FrontFiltersInterface>(GET_LOCAL_FRONT_FILTERS);
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const currentUser: ME_me = getUser();
  const myParameter = useQuery<PARAMETER_BY_CODE, PARAMETER_BY_CODEVariables>(
    GET_PARAMETER_BY_CODE,
    {
      variables: { code: CODE_LIGNE_TABLEAU },
    },
  );
  const [debouncedText, setDebouncedText] = useState<string>('');
  const client = useApolloClient();
  const currentPharmacie = myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie;
  const pathname = window.location.hash;
  const isInGestionProduits = pathname.includes('#/gestion-produits');
  const isInCatalogueProduits = pathname.includes('#/catalogue-produits');
  const isInOperationCommande = pathname.includes('#/operation-produits');
  const isInGestionProduit = window.location.hash.startsWith('#/gestion-produits')
    ? true
    : false;
  const removableTypes = ['operation'];

  const [search, { loading, error, data, variables }] = useLazyQuery<
    CUSTOM_CONTENT_SEARCH,
    CUSTOM_CONTENT_SEARCHVariables
  >(GET_CUSTOM_CONTENT_SEARCH, {
    fetchPolicy:
      type === 'commande' || removableTypes.includes(type) ? 'cache-and-network' : 'cache-first',
    variables: {
      type: [type],
      skip: page * rowsPerPage,
      take: rowsPerPage,
      query: !inSelectContainer ? query : enableGlobalSearch ? searchText : query,
      sortBy: sorted ? [sorted] : sortBy,
      //filterBy,
      userId: currentUser && currentUser.id,
      idPharmacie: currentPharmacie && currentPharmacie.id,
      ...(fieldsOptions || {}),
      idGroupement: groupement && groupement.id ? groupement.id : '',
    },
  });

  useMemo(() => {
    if (data && data.search && data.search.data) {
      const searchData = data.search.data;
      setLocalData(prevState => getUniqueListBy([...prevState, ...searchData], 'id'));
    }
  }, [data]);

  useEffect(() => {
    // search();

    if (inputLabel !== null && inputLabel.current! !== null) {
      setLabelWidth(inputLabel.current!.offsetWidth);
    }
    // component will unmount
    return () => {
      resetSearchFilters(client);
    };
  }, []);

  useEffect(() => {
    if (!useLocalData) {
      if (selected && localData) {
        const result: any[] = selected.map(item => {
          const res = localData
            .map((dataItem: any) => {
              if (dataItem && dataItem.id === item.id && item.quantite) {
                const x = { ...dataItem, quantite: item.quantite };
                return x;
              } else if (dataItem && dataItem.id === item.id && !item.quantite) return dataItem;
            })
            .filter(x => x !== undefined);
          if (res && res.length > 0) return res[0];
        });

        const validResult: any[] = result.filter(item => item !== undefined && item !== null);
        if (validResult.length > 0) {
          setFilteredItems(validResult);
        } else {
          setFilteredItems(selected);
        }
      }
    } else {
      setFilteredItems(selected);
    }
  }, [selected]);

  if (totalLine === 0 && data && data.search && data.search.total) {
    setTotalLine(data.search.total);
  }

  /**********************Filter query **************** */

  const codeCanalActive = useQuery<CodeCanalActiveInterface>(GET_CODE_CANA_ACTIVE);

  const canals = useQuery<SEARCH_INTERFACE, SEARCHVariables>(SEARCH, {
    variables: {
      type: ['commandecanal'],
    },
  });

  useEffect(() => {
    console.log('codeCanalActive >==========================', codeCanalActive);
    const codeActive =
      codeCanalActive && codeCanalActive.data && codeCanalActive.data.codeCanalActive
        ? codeCanalActive.data.codeCanalActive
        : 'PFL';
    if (
      canals &&
      canals.data &&
      canals.data.search &&
      canals.data.search.data &&
      canals.data.search.data.length
    ) {
      const currentCanals: any[] = canals.data.search.data.filter(
        (item: any) => item.code === codeActive,
      );
      console.log('canals ==========================>', canals.data.search.data);
      console.log('currentCanals ==========================>', currentCanals);
      if (currentCanals && currentCanals.length) setIdCanal(currentCanals[0].id);
    }
  }, [canals, codeCanalActive]);

  // get sort item from local state
  const sortItem = useQuery<SortInterface>(GET_LOCAL_SORT);

  // get filters id list from local orderorder
  const filters = useQuery<FiltersInterface>(GET_LOCAL_FILTERS);

  // get search text from local state
  const localSearchText = useQuery<SearchInterface>(GET_LOCAL_SEARCH);

  // update the search query filter parameters
  const updateQueryFilter = (
    must: any,
    order: string,
    sortName: string,
    text: string,
    should?: any,
  ) => {
    // Add search text to must if exist

    const produitTermActive = isInGestionProduit
      ? [
          {
            term: {
              isRemoved: false,
            },
          },
          {
            term: {
              'commandeCanal.id': idCanal,
            },
          },
        ]
      : [
          {
            term: {
              isActive: true,
            },
          },
          {
            term: {
              isRemoved: false,
            },
          },
          {
            term: {
              'commandeCanal.id': idCanal,
            },
          },
        ];

    if (text && text.length > 0) {
      const textSearch = text.replace(/\s+/g, '\\ ').replace('/', '\\/');
      must.push({
        query_string: {
          query:
            textSearch.startsWith('*') || textSearch.endsWith('*') ? textSearch : `*${textSearch}*`,
          analyze_wildcard: true,
        },
      });
    }

    if (removableTypes.includes(type)) {
      must.push({ term: { isRemoved: false } });
    }

    if (optionalMust) {
      if (optionalMust && optionalMust.must) {
        must = must.concat(optionalMust.must);
      }
    }

    if (filterBy) {
      must = must.concat(filterBy);
    }

    if (type === 'produitcanal') {
      must = must.concat(...produitTermActive);
    }

    let sortQuery = sortName ? [{ [sortName]: { order } }] : [];
    if (type === 'actualite' || type === 'operation') {
      sortQuery = [{ niveauPriorite: { order: 'desc' } }, ...sortQuery];
    }

    if (must.length > 0 && sortQuery.length > 0) {
      const queryFilter = {
        query: {
          bool: {
            must: [...must],
            should: (optionalMust && optionalMust.should) || [],
            minimum_should_match: optionalMust && optionalMust.minimum_should_match,
          },
        },
        sort: sortQuery,
      };

      if (idCanal) setQuery(queryFilter);
      if (setMinimalQuery) setMinimalQuery(queryFilter);
      // search();
    } else if (sortQuery.length > 0) {
      const queryFilter = {
        sort: sortQuery,
      };

      if (idCanal) setQuery(queryFilter);
      if (setMinimalQuery && type !== 'pharmacie') setMinimalQuery(queryFilter);
      // search();
    } else {
      const queryFilter = {
        query: {
          bool: {
            must: [...must],
          },
        },
      };
      if (idCanal) setQuery(queryFilter);
      if (setMinimalQuery) setMinimalQuery(queryFilter);
      // search();
    }
    if (!datas && idCanal) search();
  };
  console.log('filters : ', filters);
  useEffect(() => {
    // Reinitialiser la valeur de query si le type change
    if (type !== '') {
      setContent(prevState => ({ ...prevState, query: '' }));
    }

    // Reset sortItem and filters
    client.writeQuery({ query: GET_LOCAL_SORT, data: { sort: { sortItem: null } } });
    resetSearchFilters(client);
  }, [type]);

  useEffect(() => {
    // Set order from sortItem
    const order =
      sortItem && sortItem.data && sortItem.data.sort && sortItem.data.sort.sortItem
        ? sortItem.data.sort.sortItem.direction
        : 'asc';
    setOrder(order as any);

    // set sortName from sortItem
    let sortName =
      defaultSortName ||
      (sortItem &&
        sortItem.data &&
        sortItem.data.sort &&
        sortItem.data.sort.sortItem &&
        sortItem.data.sort.sortItem.name) ||
      '';
    if (sortName.length > 0) setColumnSorted(sortName);

    const text =
      localSearchText && localSearchText.data && localSearchText.data.searchText
        ? localSearchText.data.searchText.text
        : '';
    setDebouncedText(text);

    if (type === 'commande') sortName = '';

    if (filters && filters.data && filters.data.filters && !inSelectContainer) {
      const must: any = [];
      const should: any = [];

      if (type === 'produitcanal') {
        if (filters.data.filters.idFamilles.length > 0) {
          const familles: any = filters.data.filters.idFamilles.map(codeFamille => ({
            prefix: {
              'famille.codeFamille': codeFamille,
            },
          }));

          must.push({ bool: { should: familles } });
        }
        if (filters.data.filters.idLaboratoires.length > 0) {
          must.push({
            terms: {
              'laboratoire.id': filters.data.filters.idLaboratoires,
            },
          });
        }
        if (filters.data.filters.idGammesCommercials.length > 0) {
          must.push({
            terms: {
              'sousGammeCommercial.gammeCommercial.idGamme':
                filters.data.filters.idGammesCommercials,
            },
          });
        }
        if (
          filters.data.filters.idCommandeCanal &&
          filters.data.filters.idCommandeCanal.length > 0
        ) {
          must.push({
            term: {
              'commandeCanal.id': filters.data.filters.idCommandeCanal,
            },
          });
        }
        if (idCommandeCanal) {
          must.push({
            terms: {
              'commandeCanal.id': [idCommandeCanal],
            },
          });
        }
        if (filters.data.filters.idRemise) {
          if (filters.data.filters.idRemise.includes(0)) {
            must.push({
              terms: {
                'pharmacieRemisePaliers.id': [
                  myPharmacie &&
                    myPharmacie.data &&
                    myPharmacie.data.pharmacie &&
                    myPharmacie.data.pharmacie.id,
                ],
              },
            });
          }
          if (filters.data.filters.idRemise.includes(1)) {
            must.push({
              terms: {
                'pharmacieRemisePanachees.id': [
                  myPharmacie &&
                    myPharmacie.data &&
                    myPharmacie.data.pharmacie &&
                    myPharmacie.data.pharmacie.id,
                ],
              },
            });
          }
        }
      }

      if (type === 'operation') {
        if (
          filters.data.filters.idCommandeCanal &&
          filters.data.filters.idCommandeCanal.length > 0
        ) {
          must.push({
            terms: {
              'commandeCanal.id': filters.data.filters.idCommandeCanal,
            },
          });
        }
        if (filters.data.filters.idCommandeTypes.length > 0) {
          must.push({
            terms: {
              'commandeType.id': filters.data.filters.idCommandeTypes,
            },
          });
        }
      }

      if (type === 'actualite') {
        if (filters.data.filters.idActualiteOrigine.length > 0) {
          must.push({
            terms: {
              'origine.id': filters.data.filters.idActualiteOrigine,
            },
          });
        }
      }

      if (type === 'laboratoire') {
        if (filters.data.filters.sortie && filters.data.filters.sortie.length > 0) {
          must.push({
            terms: {
              sortie: filters.data.filters.sortie,
            },
          });
        }
      }

      updateQueryFilter(must, order, sortName, text, should);
      // getResult();
    } else if (filters && !filters.data && inSelectContainer) {
      updateQueryFilter([], order, sortName, text);
      // getResult();
    }
  }, [filters, searchText, localSearchText, type, sortItem]);

  /***************Fin Filter query***************** */

  useEffect(() => {
    if (checkedItems) {
      setSelected(checkedItems);
    }
  }, [checkedItems]);

  useEffect(() => {
    if (searchInputs) {
      (searchInputs || []).map((item: FieldsOptions) => {
        setFieldsState((prevState: any) => ({
          ...prevState,
          [item.name]:
            item.dataType && item.dataType === 'boolean'
              ? item.value !== -1
                ? item.value === 1
                  ? true
                  : false
                : undefined
              : item.value,
        }));
      });
    }
  }, [searchInputs]);

  useEffect(() => {
    if (!loading && data && data.search && data.search.total) {
      setCount(data.search.total);
    } else if (datas && datas.length > 0) {
      setCount(datas.length);
    }
  }, [loading, data, datas]);

  useEffect(() => {
    if (page !== 0) setContent(prevState => ({ ...prevState, page: 0 }));
    // search();
  }, [page, rowsPerPage]);

  const handleSelection = (newSelection: any[]) => {
    setSelected(newSelection);
    if (onItemsChecked) onItemsChecked(newSelection);
  };

  const handleFieldChange = (event: any): void => {
    setFieldsState({
      ...fieldsState,
      [event.target.name]:
        event.target.name === 'actived' || event.target.name === 'pharmacies.actived'
          ? event.target.value !== -1
            ? event.target.value === 1
              ? true
              : false
            : undefined
          : event.target.value,
    });
  };

  const handleRunSearch = (event: any): void => {
    if (event) event.preventDefault();
    setPage(0);
    if (searchInputs) {
      const query = generateQuery(fieldsState, searchInputs);
      setQuery(query);
      if (setMinimalQuery) setMinimalQuery(query);
    }
    search();
  };

  const initState = (searchInputs: FieldsOptions[]) => setFieldsState(getInitValues(searchInputs));

  const getInitValues = (searchInputs: FieldsOptions[]) => {
    return (searchInputs || []).reduce((initialValues, item: FieldsOptions) => {
      return {
        ...initialValues,
        [item.name]:
          item.dataType && item.dataType === 'boolean'
            ? item.value !== -1
              ? item.value === 1
                ? true
                : false
              : undefined
            : item.value,
      };
    }, fieldsState);
  };

  const handleResetFields = () => {
    if (searchInputs) {
      initState(searchInputs);
      setContent(prevState => ({
        ...prevState,
        page: 0,
      }));
      const query = generateQuery(getInitValues(searchInputs), searchInputs);
      setQuery(query);
      search();
      if (setMinimalQuery) setMinimalQuery(query);
    }
  };

  const toggleSort = async (sortedBy: string) => {
    const ordered = order === 'desc' ? 'asc' : 'desc';
    setSorted({ [sortedBy]: { order: ordered } });
    setOrder(ordered);
  };

  const debouncedSearchText = useRef(
    debounce((value: string) => {
      client.writeQuery({
        query: GET_LOCAL_SEARCH,
        data: { searchText: { text: value || '' } },
      });
      setContent(prevState => ({
        ...prevState,
        page: 0,
        query: value,
      }));
    }, 1500),
  );

  useEffect(() => {
    debouncedSearchText.current(debouncedText);
  }, [debouncedText]);

  /**
   * Launch search
   */
  useEffect(() => {
    /*
    if (sorted) {
      setColumnSorted(Object.keys(sorted)[0]);
    } else if (sortBy) {
      setColumnSorted(Object.keys(sortBy[0])[0]);
    }
    */

    setContent(prevState => ({
      ...prevState,
      type,
      columns,
      searchPlaceholder,
      filterBy,
      sortBy: sorted ? [sorted] : sortBy,
      buttons,
    }));
    if (!datas && rowsPerPageFromParam) {
      search();
    }
  }, [
    type,
    columns,
    searchPlaceholder,
    filterBy,
    sortBy,
    sorted,
    buttons,
    datas,
    query,
    page,
    rowsPerPage,
  ]);

  const onChangeSearchText = (value: string) => {
    setDebouncedText(value);
  };

  // const formatNewList = (newList: any[]) => {
  //   return { data: newList };
  // };

  const checkFrontFilters = () => {
    const filters = frontFilters && frontFilters.data && frontFilters.data.frontFilters;

    let newList = (data && data.search && data.search.data) || [];
    if (filters) {
      // filters for operation commercial commandePassee (boolean)
      if (filters.occommande && filters.occommande.length > 0) {
        if (filters.occommande.length < 2) {
          filters.occommande.map(
            filterItem =>
              (newList = newList.filter((listItem: any) => listItem.commandePassee === filterItem)),
          );
          return newList;
        } else {
          return (data && data.search && data.search.data) || [];
        }
      }
      if (filters.seen) {
        if (filters.seen === 'all') {
          return (data && data.search && data.search.data) || [];
        } else if (filters.seen === 'read') {
          newList = newList.filter((listItem: any) => listItem.seen === true);
        } else {
          newList = newList.filter((listItem: any) => listItem.seen === false);
        }

        return newList;
      }
      if (filters.datefilter) {
        if (filters.datefilter.dateDebut) {
          newList = newList.filter(
            (listItem: any) =>
              listItem &&
              listItem.dateDebut &&
              moment(listItem.dateDebut).format() >= moment(filters.datefilter.dateDebut).format(),
          );
        }
        if (filters.datefilter.dateFin) {
          newList = newList.filter(
            (listItem: any) =>
              listItem &&
              listItem.dateFin &&
              moment(listItem.dateFin).format() <= moment(filters.datefilter.dateFin).format(),
          );
        }
        return newList;
      }
    }
    return (data && data.search && data.search.data) || [];
  };

  const filteredData = checkFrontFilters();

  const rowsPerPageFromParam =
    myParameter &&
    myParameter.data &&
    myParameter.data.parameterByCode &&
    myParameter.data.parameterByCode.value &&
    myParameter.data.parameterByCode.value.value;

  useMemo(() => {
    if (rowsPerPageFromParam) {
      const newRowPerPage = type === 'produitcanal' ? 12 : parseInt(rowsPerPageFromParam, 10);
      if (rowsPerPage !== newRowPerPage) {
        setRowsPerPage(newRowPerPage);
      }
    }
  }, [rowsPerPageFromParam]);

  const toggleFilter = () => {
    handleResetFields();
    onChangeSearchText('');
    setShowSearchFilters(!showSearchFilters);
  };

  const [finalData, setfinalData] = useState<any[]>([]);
  /**
   * Set final data
   */
  useEffect(() => {
    if (datas && datas.length > 0) {
      setfinalData(datas);
    } else if (data && data.search && data.search.data && data.search.data.length > 0) {
      setfinalData(data.search.data);
    } else if (filteredData && filteredData.length > 0) {
      setfinalData(filteredData);
    } else if (finalData.length !== 0) {
      setfinalData([]);
    }
  }, [data, datas, filteredData]);

  useEffect(() => {
    if (variables && deletedIds && deletedIds.length > 0) {
      const req = client.readQuery<SEARCH_INTERFACE, SEARCHVariables>({
        query: GET_CUSTOM_CONTENT_SEARCH,
        variables,
      });
      if (req && req.search && req.search.data) {
        client.writeQuery({
          query: GET_CUSTOM_CONTENT_SEARCH,
          data: {
            search: {
              ...req.search,
              ...{
                data: req.search.data.filter(
                  (a: any) => !deletedIds.some(b => a && b && a.id === b),
                ),
              },
            },
          },
          variables,
        });
      }
    }
  }, [variables, deletedIds]);

  console.log('data : ', data);

  return (
    <div className={classes.root}>
      {!hideSearchBar && (
        <div
          className={classes.searchBar}
          style={searchInFullWidth ? { justifyContent: 'center' } : {}}
        >
          <div className={classes.search} style={searchInFullWidth ? { width: '70%' } : {}}>
            <SearchInput
              value={debouncedText}
              searchInFullWidth={searchInFullWidth}
              noPrefixIcon={noPrefixIcon}
              onChange={onChangeSearchText}
              placeholder={searchPlaceholder}
              disabled={showSearchFilters}
            />
            {searchInputs && searchInputs.length > 0 && (
              <>
                <IconButton className={classes.filterButton} onClick={toggleFilter}>
                  <Tune />
                </IconButton>
                <Typography className={classes.filterButtonLabel}>Filtres</Typography>
              </>
            )}
          </div>

          {buttons}
        </div>
      )}
      {showSearchFilters && (
        <SearchFilter
          fieldsState={fieldsState}
          searchInputsLabel={searchInputsLabel}
          searchInputs={searchInputs}
          handleFieldChange={handleFieldChange}
          handleResetFields={handleResetFields}
          handleRunSearch={handleRunSearch}
          labelWidth={labelWidth}
          disableResetButton={disableResetButton}
        />
      )}
      {!isInGestionProduits &&
        !isInCatalogueProduits &&
        !isInOperationCommande &&
        hideSearchBar &&
        !hideSearchInput && (
          <Box display="flex" justifyContent="center">
            <SearchInput
              value={debouncedText}
              searchInFullWidth={searchInFullWidth}
              noPrefixIcon={noPrefixIcon}
              onChange={onChangeSearchText}
              placeholder={searchPlaceholder}
              dark={true}
            />
          </Box>
        )}

      {children && children}

      {!data || loading ? (
        <Loader />
      ) : error ? (
        <div className={classes.alignCenter}>Erreur lors de chargement de la page</div>
      ) : finalData.length > 0 ||
        (data && data.search && data.search.data && data.search.data.length > 0) ||
        (datas && datas.length > 0) ? (
        <>
          {finalData.length > 0 && loading && <Backdrop />}
          <Table
            isSelectable={isSelectable || false}
            page={page}
            rowsPerPage={rowsPerPage}
            rowsPerPageOptions={rowsPerPageOptions || ROWS_PER_PAGE_OPTIONS}
            onChangePage={(page: number) => setPage(page)}
            onChangeRowsPerPage={(rowsPerPage: number) => setRowsPerPage(rowsPerPage)}
            columns={columns}
            data={finalData}
            count={count}
            toggleSorting={toggleSort}
            order={order}
            columnSorted={columnSorted}
            densePadding={densePadding || false}
            selected={filteredItems}
            handleSelection={handleSelection}
            handleCheckItem={handleCheckItem}
            handleCheckAll={handleCheckAll}
            handleUncheckAll={handleUncheckAll}
            paginationCentered={paginationCentered || false}
            total={totalLine}
            showResetFilters={showResetFilters}
            onClickRow={onClickRow}
            activeRow={activeRow}
            type={type}
            isDeletable={isDeletable}
            variables={variables}
          />
          {type === 'ppersonnel' && (
            <Typography
              variant="subtitle2"
              style={{ textAlign: 'right', marginRight: 130 }}
            >{`Nombre total des collaborateurs : ${data &&
              data.search &&
              data.search.total}`}</Typography>
          )}
        </>
      ) : (
        <div className={classes.alignCenter}>Aucun résultat correspondant</div>
      )}
    </div>
  );
};

export default withStyles(styles)(Content);
