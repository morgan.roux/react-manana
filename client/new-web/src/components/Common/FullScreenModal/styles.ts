import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const drawerWidth = 275;
const appBarHeight = 86;
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
    appBar: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      background: theme.palette.primary.main,
      zIndex: theme.zIndex.drawer + 1,
    },
    groupementAndCloseButtonContainer: {
      width: 'auto',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'center',
      '& IconButton': {
        maxHeight: 50,
      },
    },
    whiteBackground: {
      background: '#FFFFFF',
    },
    fullWidth: {
      [theme.breakpoints.up('md')]: {
        width: '100%',
      },
    },
    flexToolbar: {
      display: 'flex',
      alignItems: 'center',
      minHeight: 86,
      justifyContent: 'space-between',
    },
    roleTitle: {
      display: 'flex',
      color: '#FFFFFF',
      paddingLeft: '8px',
      marginRight: '16px',
      '@media (max-width: 600px)': {
        display: 'none',
      },
    },
    nameGroup: {
      fontSize: '14px',
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      marginLeft: ' 8px',
      marginTop: '4px',
    },
    content: {
      marginTop: '100px',
    },
  }),
);
export default useStyles;
