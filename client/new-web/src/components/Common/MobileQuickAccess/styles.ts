import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      height: 70,
      borderTop: '1px solid #E1E1E1',
      top: 'auto',
      bottom: 0,
      display: 'flex',
      flexDirection: 'row',
      padding: '0px 16px',
      justifyContent: 'space-between',
      alignItems: 'center',
      background: theme.palette.common.white,
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
    menuItemRoot: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      color: '#27272FAB',
      fontFamily: 'Roboto',
      '& > .MuiTypography-root': {
        fontSize: '0.75em',
      },
    },
    active: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      color: theme.palette.secondary.main,
      fontFamily: 'Roboto',
      '& > .MuiTypography-root': {
        fontSize: '0.75em',
      },
    },
  }),
);
