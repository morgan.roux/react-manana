import React, { ReactNode, FC } from 'react';
import { Typography } from '@material-ui/core/';
import { withStyles } from '@material-ui/core/styles';
import styles from './styles';
import logo from '../../../assets/img/gcr_pharma.svg';

interface IPROPS {
  children?: ReactNode;
  title?: string;
  width?: string;
  classes: {
    container?: string;
    content?: string;
    top?: string;
    topTitle?: string;
    title?: string;
    img?: string;
  };
}

const SinglePage: FC<IPROPS> = props => {
  const { classes, children, title, width } = props;
  return (
    <div className={classes.container}>
      <div className={classes.content} style={{ width }}>
        <div className={classes.top}>
          <img src={logo} className={classes.img} alt="logo gcr_pharma" />
          <Typography variant="h6" gutterBottom={true} className={classes.title}>
            {title}
          </Typography>
        </div>
        {children}
      </div>
    </div>
  );
};

export default withStyles(styles, { withTheme: true })(SinglePage);

SinglePage.defaultProps = {
  title: '',
  width: '400px',
};
