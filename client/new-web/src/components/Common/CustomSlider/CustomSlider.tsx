import React, { FC } from 'react';
import Slider, { Settings } from 'react-slick';
import useStyles from './styles';
import ImgSlider1 from '../../../assets/img/p1.jpg';
import { NavigateBefore, NavigateNext } from '@material-ui/icons';
import classnames from 'classnames';
import { Box } from '@material-ui/core';

export interface SliderData {
  id: string | number;
  image: string;
  url: string;
  item?: any;
}

interface CustomSliderProps {
  data?: SliderData[];
  onClickImage?: (url: string, id: string | number) => void;
}

const CustomSlider: FC<Settings & CustomSliderProps> = ({
  dots,
  infinite,
  autoplay,
  arrows,
  speed,
  rows,
  slidesPerRow,
  lazyLoad,
  prevArrow,
  nextArrow,
  data,
  onClickImage,
}) => {
  const classes = useStyles({});

  const arrowStyles = { display: 'bloc', width: 25, height: 25 };

  const MUIPrevArrow = props => {
    const { className, style, onClick } = props;
    return (
      <NavigateBefore
        className={className}
        onClick={onClick}
        style={{ ...style, ...arrowStyles }}
      />
    );
  };

  const MUINextArrow = props => {
    const { className, style, onClick } = props;
    return (
      <NavigateNext className={className} onClick={onClick} style={{ ...style, ...arrowStyles }} />
    );
  };

  const settings: Settings = {
    dots: true,
    infinite: true,
    autoplay: false,
    arrows: true,
    speed: 1000,
    rows: 2,
    slidesPerRow: 5,
    lazyLoad: 'progressive',
    prevArrow: <MUIPrevArrow />,
    nextArrow: <MUINextArrow />,
    responsive: [
      {
        breakpoint: 1600,
        settings: {
          rows: 2,
          slidesPerRow: 4,
        },
      },
      {
        breakpoint: 1200,
        settings: {
          rows: 2,
          slidesPerRow: 3,
        },
      },
      {
        breakpoint: 900,
        settings: {
          rows: 2,
          slidesPerRow: 2,
        },
      },
      {
        breakpoint: 600,
        settings: {
          rows: 1,
          slidesPerRow: 1,
        },
      },
    ],
  };

  /*const defaultData: SliderData[] = [
    { id: 1, image: ImgSlider1, url: '' },
    { id: 2, image: ImgSlider1, url: '' },
    { id: 3, image: ImgSlider1, url: '' },
    { id: 4, image: ImgSlider1, url: '' },
    { id: 5, image: ImgSlider1, url: '' },
    { id: 6, image: ImgSlider1, url: '' },
    { id: 7, image: ImgSlider1, url: '' },
    { id: 8, image: ImgSlider1, url: '' },
    { id: 9, image: ImgSlider1, url: '' },
    { id: 10, image: ImgSlider1, url: '' },
    { id: 11, image: ImgSlider1, url: '' },
    { id: 12, image: ImgSlider1, url: '' },
  ];

  const sliderData = data && data.length > 0 ? data : defaultData;*/

  return (
    <div
      className={
        onClickImage
          ? classnames(classes.customSliderRoot, classes.imgClickable)
          : classes.customSliderRoot
      }
    >
      <Slider {...settings}>
        {/*sliderData.map(
          item =>
            item && (
              <Box className={classes.itemPromotion}>
                <img
                  src={item.image}
                  alt={item.url}
                  key={`slider_image_${item.id}`}
                  onClick={() => onClickImage && onClickImage(item.url, item.id)}
                />
              </Box>
            ),
            )*/}
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
        <Box className={classes.itemPromotion}>
          <Box className={classes.contentItemImg}>
            <img src={ImgSlider1} alt="" />
          </Box>
        </Box>
      </Slider>
    </div>
  );
};

export default CustomSlider;
