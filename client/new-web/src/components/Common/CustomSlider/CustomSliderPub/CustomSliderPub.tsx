import React, { FC } from 'react';
import Slider, { Settings } from 'react-slick';
import useStyles from './styles';
import ImgSlider1 from '../../../../assets/img/p1.jpg';
import { NavigateBefore, NavigateNext } from '@material-ui/icons';
import classnames from 'classnames';
import { Box } from '@material-ui/core';
import { useValueParameter } from '../../../../utils/getValueParameter';
import { CODE_PUB_DELAIS } from '../../../../Constant/parameter';
import { useMutation } from '@apollo/client';
import { DO_INCREMENT_PUBLICITE_VIEW_COUNT } from '../../../../graphql/SuiviPublicitaire/mutation';
import {
  incrementPubliciteViewCount,
  incrementPubliciteViewCountVariables,
} from '../../../../graphql/SuiviPublicitaire/types/incrementPubliciteViewCount';

export interface SliderData {
  id: string | number;
  image: string;
  url: string;
}

interface CustomSliderPubProps {
  data?: SliderData[];
  onClickImage?: (url: string, imageItem: any) => void;
}

const CustomSliderPub: FC<Settings & CustomSliderPubProps> = ({ data, onClickImage }) => {
  const classes = useStyles({});

  const arrowStyles = { display: 'bloc', width: 25, height: 25 };

  const MUIPrevArrow = props => {
    const { className, style, onClick } = props;
    return (
      <NavigateBefore
        className={className}
        onClick={onClick}
        style={{ ...style, ...arrowStyles }}
      />
    );
  };

  const MUINextArrow = props => {
    const { className, style, onClick } = props;
    return (
      <NavigateNext className={className} onClick={onClick} style={{ ...style, ...arrowStyles }} />
    );
  };

  const pubSpeed = useValueParameter(CODE_PUB_DELAIS);

  const [doIncrementPubliciteViewCount, doIncrementPubliciteViewCountResult] = useMutation<
    incrementPubliciteViewCount,
    incrementPubliciteViewCountVariables
  >(DO_INCREMENT_PUBLICITE_VIEW_COUNT);

  const settings: Settings = {
    dots: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: pubSpeed ? parseInt(pubSpeed, 10) * 1000 : 5000,
    arrows: true,
    speed: 2000,
    slidesToScroll: 1,
    slidesToShow: 1,
    lazyLoad: 'progressive',
    prevArrow: <MUIPrevArrow />,
    nextArrow: <MUINextArrow />,
    beforeChange: currentSlide => {
      if (data && data.length) {
        const currentPub: any = data[currentSlide];
        if (currentPub && currentPub.id) {
          doIncrementPubliciteViewCount({ variables: { idPublicite: currentPub.id } });
        }
      }
    },
  };

  const defaultData: SliderData[] = [
    { id: 1, image: ImgSlider1, url: '' },
    { id: 2, image: ImgSlider1, url: '' },
    { id: 3, image: ImgSlider1, url: '' },
    { id: 4, image: ImgSlider1, url: '' },
    { id: 5, image: ImgSlider1, url: '' },
    { id: 6, image: ImgSlider1, url: '' },
    { id: 7, image: ImgSlider1, url: '' },
    { id: 8, image: ImgSlider1, url: '' },
    { id: 9, image: ImgSlider1, url: '' },
    { id: 10, image: ImgSlider1, url: '' },
    { id: 11, image: ImgSlider1, url: '' },
    { id: 12, image: ImgSlider1, url: '' },
  ];

  const sliderData = data && data.length > 0 ? data : defaultData;

  return (
    <div
      className={
        onClickImage
          ? classnames(classes.customSliderRoot, classes.imgClickable)
          : classes.customSliderRoot
      }
    >
      <Slider {...settings}>
        {sliderData.map(
          item =>
            item && (
              <Box className={classes.itemPromotion} key={`slider_image_${item.id}`}>
                <img
                  src={item.image}
                  alt={item.url}
                  onClick={() => onClickImage && onClickImage(item.url, item)}
                />
              </Box>
            ),
        )}
      </Slider>
    </div>
  );
};

export default CustomSliderPub;
