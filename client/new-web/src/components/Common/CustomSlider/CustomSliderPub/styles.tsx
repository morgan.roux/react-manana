import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    customSliderRoot: {
      // minHeight: 280,
      // height: 450,
      // padding: '50px 100px',
      '& .slick-prev': {
        left: '0!important',
      },
      '& .slick-next': {
        right: '0!important',
      },
      '& .slick-prev:before, & .slick-next:before': {
        color: theme.palette.common.black,
      },
      '& .slick-prev, & .slick-next': {
        color: theme.palette.common.black,
        borderRadius: '50%',
      },
      '& .slick-prev:hover, & .slick-next:hover': {
        // background: '#F5F6FA 0% 0% no-repeat padding-box',
        /*background: theme.palette.common.black,*/
        color: theme.palette.common.white,
        opacity: 0.65,
      },
      '& .slick-initialized .slick-slide': {
        display: 'flex !important',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
      },
      '& .slick-slide div': {
        width: '100%',
        height: 310,
        /*borderRadius: 6,*/
        overflow: 'hidden',
      },
      '& .slick-dots li button:before': {
        content: '""',
      },
      '& .slick-dots li': {
        background: '#E0E0E0 0% 0% no-repeat padding-box',
        width: 6,
        height: 6,
      },
      '& li.slick-active, & .slick-dots li:hover': {
        background: '#616161 0% 0% no-repeat padding-box',
        width: 10,
        height: 10,
      },
      '& ul.slick-dots': {
        bottom: -25,
        display: 'flex !important',
        alignItems: 'center',
        justifyContent: 'center',
      },
      '& img': {
        objectFit: 'contain',
        width: '100%',
        height: '100%',
        '@media (min-width: 1600px)': {
          objectFit: 'fill',
        },
      },
    },
    imgClickable: {
      '& img ': {
        cursor: 'pointer !important',
      },
    },
    itemPromotion: {},
  }),
);

export default useStyles;
