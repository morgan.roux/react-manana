import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    pink: {
      // background: 'transparent linear-gradient(231deg, #E34741 0%, #E34168 100%) 0% 0% no-repeat padding-box',
      background: theme.palette.secondary.main,
      textTransform: 'uppercase',
      fontFamily: 'Roboto',
      fontSize: '14px',
      fontWeight: 'bold',
      paddingLeft: '16px',
      paddingRight: '16px',
      boxShadow: '0px 2px 2px #00000040',
      borderRadius: '3px',
      color: theme.palette.common.white,
    },
    icon: {
      marginRight: '8px',
    },
  }),
);

export default useStyles;
