import React, { FC, useState, useEffect } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Box, Typography, IconButton, Tooltip } from '@material-ui/core';
import ReplayIcon from '@material-ui/icons/Replay';
import useStyles from './styles';
import { useApolloClient, useQuery } from '@apollo/client';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import SortIcon from '@material-ui/icons/Sort';
import SortInterface from '../../../../Interface/SortInterface';
import GET_LOCAL_SORT from '../../../../localStates/SortLocal';
import { resetSearchFilters } from '../../withSearch/withSearch';
import { ME_me } from '../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../services/LocalStorage';
import { SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT } from '../../../../Constant/roles';
import { MoreVert } from '@material-ui/icons';

interface CenteredSortProps { }
const CenteredSort: FC<CenteredSortProps & RouteComponentProps<any, any, any>> = ({ history }) => {
  const client = useApolloClient();
  const classes = useStyles({});

  const [order, setOrder] = useState<'asc' | 'desc'>('asc');

  const handleResetFilter = () => { };

  const handleChangeDirection = () => {
    setOrder(prevState => (prevState === 'asc' ? 'desc' : 'asc'));
  };

  return (
    <Box display="flex" justifyContent="space-between" alignItems="center">
      <IconButton size="small" onClick={handleResetFilter}>
        <Tooltip title="Réinitialiser les filtres">
          <ReplayIcon />
        </Tooltip>
      </IconButton>
      <IconButton size="small" onClick={handleChangeDirection}>
        {order === 'asc' ? <SortIcon /> : <SortIcon className={classes.desc} />}
      </IconButton>
      <IconButton size="small">
        <MoreVert />
      </IconButton>
    </Box>
  );
};
export default withRouter(CenteredSort);
