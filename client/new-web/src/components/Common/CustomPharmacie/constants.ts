import { FieldsOptions } from '../CustomContent/interfaces';

export const PHARMA_FILTER_PHARMACIE_NAME_CODE: string = '0029';
export const PHARMA_FILTER_TITULAIRE_CODE: string = '0030';
export const PHARMA_FILTER_CIP_CODE: string = '0031';
export const PHARMA_FILTER_COMMERCIALE_CODE: string = '0033';
export const PHARMA_FILTER_SORTIE_CODE: string = '0034';
export const PHARMA_FILTER_TELEPHONE_CODE: string = '0035';
export const PHARMA_FILTER_ADRESSE_CODE: string = '0036';
export const PHARMA_FILTER_CODE_POSTAL_CODE: string = '0037';
export const PHARMA_FILTER_VILLE_CODE: string = '0038';
export const PHARMA_FILTER_DEPARTEMENT_CODE: string = '0039';
export const PHARMA_FILTER_REGION_CODE: string = '0040';
export const PHARMA_FILTER_PRESIDENT_REGION_CODE: string = '0041';
export const PHARMA_FILTER_SIRET_CODE: string = '0042';
export const PHARMA_FILTER_CONTRAT_CODE: string = '0043';
export const PHARMA_FILTER_TRANCHE_CA_CODE: string = '0044';
export const PHARMA_FILTER_CODE_ENSEIGNE_CODE: string = '0045';
export const PHARMA_FILTER_STATUS_CODE: string = ''; // Code de donnée non encore prêt
export const PHARMA_FILTER_CODE_IVRY_LAB_CODE: string = ''; // Code de donnée non encore prêt

const placeholder = 'Tapez ici';

/**
 * Search fields
 *
 */
export const SEARCH_FIELDS_OPTIONS: FieldsOptions[] = [
  {
    name: 'nom',
    label: 'Nom Pharmacie',
    value: '',
    type: 'Search',
    placeholder,
    ordre: 1,
  },
  {
    name: 'titulaires',
    label: 'Titulaire',
    value: '',
    type: 'Search',
    placeholder,
    // extraNames: ['titulaires.nom', 'titulaires.prenom'],
    extraNames: ['titulaires.fullName'],
    ordre: 2,
  },
  {
    name: 'cip',
    label: 'CIP',
    value: '',
    type: 'Search',
    filterType: 'StartsWith',
    placeholder,
    ordre: 3,
  },
  {
    name: 'cp',
    label: 'Code Postal',
    value: '',
    type: 'Search',
    filterType: 'StartsWith',
    placeholder,
    ordre: 4,
  },
  {
    name: 'ville',
    label: 'Ville',
    value: '',
    type: 'Search',
    placeholder,
    ordre: 5,
  },

  {
    name: 'tele1',
    label: 'Téléphone',
    value: '',
    type: 'Search',
    placeholder,
    filterType: 'StartsWith',
    extraNames: ['tele2'],
    ordre: 6,
  },
  {
    name: 'adresse1',
    label: 'Adresse',
    value: '',
    type: 'Search',
    placeholder,
    extraNames: ['adresse2'],
    ordre: 7,
  },
  {
    name: 'presidentRegion',
    label: 'Président de région',
    value: '',
    type: 'Search',
    extraNames: ['presidentRegion.titulaire.nom', 'presidentRegion.titulaire.prenom'],
    placeholder,
    ordre: 8,
  },
  {
    name: 'commerciale',
    label: 'Commercial',
    value: '',
    type: 'Search',
    placeholder,
    ordre: 9,
  },
  {
    name: 'departement',
    label: 'Département',
    value: '',
    type: 'Search',
    placeholder,
    extraNames: ['departement.nom', 'departement.code'],
    ordre: 10,
  },
  {
    name: 'contrat',
    label: 'Contrat',
    value: '',
    type: 'Search',
    placeholder,
    ordre: 11,
  },
  {
    name: 'tranchCA',
    label: 'Tranche CA',
    value: '',
    type: 'Search',
    placeholder,
    ordre: 12,
  },

  {
    name: 'sortie',
    label: 'Sortie',
    value: -1,
    type: 'Select',
    placeholder: 'Sortie',
    id: 'statut-select',
    options: [
      { label: 'Tous', value: -1, all: true },
      { label: 'Non', value: 0 },
      { label: 'Oui', value: 1 },
    ],
    ordre: 13,
  },
  {
    name: 'region',
    label: 'Région',
    value: '',
    type: 'Search',
    extraNames: ['departement.region.nom'],
    placeholder,
    ordre: 14,
  },
  {
    name: 'numIvrylab',
    label: 'Code plateforme',
    value: '',
    type: 'Search',
    filterType: 'StartsWith',
    placeholder,
    ordre: 15,
  },
  {
    name: 'codeEnseigne',
    label: 'Code Enseigne',
    value: '',
    type: 'Search',
    placeholder,
    ordre: 16,
  },
  {
    name: 'siret',
    label: 'Siret',
    value: '',
    type: 'Search',
    placeholder,
    ordre: 17,
  },
  {
    name: 'actived',
    label: 'Statut',
    value: -1,
    type: 'Select',
    placeholder: 'Statut',
    id: 'statut-select',
    dataType: 'boolean',
    options: [
      { label: 'Tous', value: -1, all: true },
      { label: 'Inactive', value: 0 },
      { label: 'Activée', value: 1 },
    ],
    ordre: 18,
  },
];
