import React, { FC, useState, useEffect } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Link, useTheme, Tooltip, Fade, IconButton, Box } from '@material-ui/core';
import CustomContent from '../CustomContent';
import {
  SEARCH_FIELDS_OPTIONS,
  PHARMA_FILTER_PHARMACIE_NAME_CODE,
  PHARMA_FILTER_TITULAIRE_CODE,
  PHARMA_FILTER_SORTIE_CODE,
  PHARMA_FILTER_CODE_ENSEIGNE_CODE,
  PHARMA_FILTER_DEPARTEMENT_CODE,
  PHARMA_FILTER_STATUS_CODE,
  PHARMA_FILTER_CODE_IVRY_LAB_CODE,
  PHARMA_FILTER_TELEPHONE_CODE,
  PHARMA_FILTER_CIP_CODE,
  PHARMA_FILTER_VILLE_CODE,
  PHARMA_FILTER_CODE_POSTAL_CODE,
  PHARMA_FILTER_ADRESSE_CODE,
  PHARMA_FILTER_PRESIDENT_REGION_CODE,
  PHARMA_FILTER_CONTRAT_CODE,
  PHARMA_FILTER_SIRET_CODE,
  PHARMA_FILTER_REGION_CODE,
  PHARMA_FILTER_TRANCHE_CA_CODE,
  PHARMA_FILTER_COMMERCIALE_CODE,
} from './constants';
// import { useStyles } from './styles';
import { FieldsOptions, Column } from '../CustomContent/interfaces';
import { useValueParameterAsBoolean } from '../../../utils/getValueParameter';
import { getGroupement } from '../../../services/LocalStorage';
import FicheTitulaire from '../../Dashboard/TitulairesPharmacies/FicheTitulaire/FicheTitulaire';
import FichePharmacie from '../../Dashboard/Pharmacies/FichePharmacie/FichePharmacie';
import { Visibility } from '@material-ui/icons';
import { CustomFullScreenModal } from '../CustomModal';
import { TITULAIRE_PHARMACIE_URL, PHARMACIE_URL } from '../../../Constant/url';

interface CustomPharmacieProps {
  checkedItems?: any[];
  selectable?: boolean;
  enableGlobalSearch?: boolean;
  children?: any;
  columns?: Column[];
  searchInputs?: FieldsOptions[];
  checkedAll?: boolean;
  takeSelection?: (checkedItems: any[]) => void;
  filterBy?: any;
  handleCheckItem?: (value: any) => void;
  handleCheckAll?: () => void;
  handleUncheckAll?: (event: any) => void;
  inSelectContainer?: boolean;
  paginationCentered?: boolean;
  viewInModal?: boolean;
  setQuery?: (query: any) => void;
  hideSearchBar?: boolean;
  onClickRow?: (row: any) => void;
}

type FicheType = 'PHARMACIE' | 'TITULAIRE';

let historyGlobal: any = null;

const filterSearchFields = (objectState: any, searchFields: FieldsOptions[]): FieldsOptions[] => {
  const activeStateKeys = Object.keys(objectState).filter(key => objectState[key] === true);
  const activeSearchFields = searchFields.filter((field: FieldsOptions) =>
    activeStateKeys.includes(`${field.name}State`),
  );
  return activeSearchFields;
};

const CustomPharmacie: FC<CustomPharmacieProps & RouteComponentProps> = ({
  checkedItems,
  selectable,
  enableGlobalSearch,
  children,
  columns,
  searchInputs,
  checkedAll,
  takeSelection,
  history,
  filterBy,
  handleCheckItem,
  handleCheckAll,
  handleUncheckAll,
  inSelectContainer,
  paginationCentered,
  viewInModal,
  setQuery,
  hideSearchBar,
  onClickRow,
}) => {
  historyGlobal = history;
  // const classes = useStyles({});
  const [selectedItems, setSelectedItems] = useState<any[]>([]);
  const [openFullScreenModal, setOpenFullScreenModal] = useState(false);
  const [currentId, setCurrentId] = useState<string>('');
  const [ficheType, setFicheType] = useState<FicheType>();
  const theme = useTheme();

  const COLUMNS: Column[] = [
    {
      name: 'cip',
      label: 'CIP',
      renderer: (value: any) => {
        return value.cip ? value.cip : '-';
      },
    },
    {
      name: 'nom',
      label: 'Nom',
      renderer: (value: any) => {
        return value.nom ? value.nom : '-';
      },
    },
    {
      name: 'titulaire.fullName',
      label: 'Titulaire(s)',
      renderer: (value: any) => {
        const handleClick = (titulaireId: string) => () => {
          if (historyGlobal && !viewInModal) {
            historyGlobal.push(`/db/${TITULAIRE_PHARMACIE_URL}/fiche/${titulaireId}`);
          }
          if (viewInModal) handleOpenDetail(titulaireId, 'TITULAIRE');
        };

        return (
          value.titulaires &&
          value.titulaires.map((titulaire: any, key: number) =>
            titulaire ? (
              <>
                <Link
                  component="button"
                  variant="body2"
                  onClick={handleClick(titulaire.id)}
                  style={{ color: theme.palette.secondary.main, textAlign: 'left' }}
                >
                  {titulaire && `${titulaire.fullName}`}
                </Link>
                <br />
              </>
            ) : (
              '-'
            ),
          )
        );
      },
    },
    {
      name: 'adresse1',
      label: 'Adresse',
      renderer: (value: any) => {
        return value.adresse1 ? value.adresse1 : '-';
      },
    },
    {
      name: 'cp',
      label: 'Code postal',
      renderer: (value: any) => {
        return value.cp ? value.cp : '-';
      },
    },
    {
      name: 'departement.nom',
      label: 'Département',
      renderer: (value: any) => {
        return value.departement && value.departement.nom ? value.departement.nom : '-';
      },
    },
    {
      name: 'ville',
      label: 'Ville',
      renderer: (value: any) => {
        return value.ville ? value.ville : '-';
      },
    },
    {
      name: 'tele1',
      label: 'Téléphone',
      renderer: (value: any) => {
        return value.tele1 ? value.tele1 : '-';
      },
    },
    {
      name: 'numIvrylab',
      label: 'Code plateforme',
      renderer: (value: any) => {
        return value.numIvrylab ? value.numIvrylab : '-';
      },
    },
    {
      name: 'contrat',
      label: 'Contrat',
      renderer: (value: any) => {
        return value.contrat ? value.contrat : '-';
      },
    },
    {
      name: 'sortie',
      label: 'Sortie',
      renderer: (value: any) => {
        return value.sortie ? (value.sortie === 1 ? 'Oui' : 'Non') : '-';
      },
    },
    {
      name: 'actived',
      label: 'Statut',
      renderer: (value: any) => {
        return value && value.actived !== undefined && value.actived !== null
          ? value.actived
            ? 'Activée'
            : 'Inactive'
          : '-';
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        const handleClick = () => {
          if (value.id) {
            if (historyGlobal && !viewInModal) {
              historyGlobal.push(`/db/${PHARMACIE_URL}/fiche/${value.id}`);
            }
            if (viewInModal) handleOpenDetail(value.id, 'PHARMACIE');
          }
        };

        return (
          <>
            <Tooltip
              TransitionComponent={Fade}
              TransitionProps={{ timeout: 600 }}
              title="Voir détails"
            >
              <IconButton onClick={handleClick}>
                <Visibility />
              </IconButton>
            </Tooltip>
          </>
        );
      },
    },
  ];

  const groupement = getGroupement();
  useEffect(() => {
    if (checkedItems) setSelectedItems(checkedItems);
  }, [checkedItems]);

  const takeCheckedItems = (selection: any[]) => {
    console.log('CHECKED ------> ', selection);
  };

  const handleOpenDetail = (id: string, ficheType: FicheType) => {
    setCurrentId(id);
    setFicheType(ficheType);
    setOpenFullScreenModal(true);
  };

  /**
   * /!\ IMPORTANT
   * The key on this 'objectState' should match exactly the value of 'name' key
   * in object inside 'searchFieds' array following a suffix 'State'.
   *
   * - searchFields :
   * [
   *  { name: 'nom', ... },
   *  { name: 'cp', ... },
   *  { name: 'adresse1', ... },
   * ]
   *
   * - objectState :
   * {
   *  nomState: useValueParameterAsBoolean(pharmacieNameCode),
   *  cpState: useValueParameterAsBoolean(codePostalCode),
   *  adresse1State: useValueParameterAsBoolean(adresseCode),
   * }
   */
  const objectState = {
    nomState: useValueParameterAsBoolean(PHARMA_FILTER_PHARMACIE_NAME_CODE),
    titulairesState: useValueParameterAsBoolean(PHARMA_FILTER_TITULAIRE_CODE),
    sortieState: useValueParameterAsBoolean(PHARMA_FILTER_SORTIE_CODE),
    codeEnseigneState: useValueParameterAsBoolean(PHARMA_FILTER_CODE_ENSEIGNE_CODE),
    departementState: useValueParameterAsBoolean(PHARMA_FILTER_DEPARTEMENT_CODE),
    activedState: useValueParameterAsBoolean(PHARMA_FILTER_STATUS_CODE),
    numIvrylabState: useValueParameterAsBoolean(PHARMA_FILTER_CODE_IVRY_LAB_CODE),
    tele1State: useValueParameterAsBoolean(PHARMA_FILTER_TELEPHONE_CODE),
    cipState: useValueParameterAsBoolean(PHARMA_FILTER_CIP_CODE),
    villeState: useValueParameterAsBoolean(PHARMA_FILTER_VILLE_CODE),
    cpState: useValueParameterAsBoolean(PHARMA_FILTER_CODE_POSTAL_CODE),
    adresse1State: useValueParameterAsBoolean(PHARMA_FILTER_ADRESSE_CODE),
    presidentRegionState: useValueParameterAsBoolean(PHARMA_FILTER_PRESIDENT_REGION_CODE),
    contratState: useValueParameterAsBoolean(PHARMA_FILTER_CONTRAT_CODE),
    siretState: useValueParameterAsBoolean(PHARMA_FILTER_SIRET_CODE),
    regionState: useValueParameterAsBoolean(PHARMA_FILTER_REGION_CODE),
    trancheCAState: useValueParameterAsBoolean(PHARMA_FILTER_TRANCHE_CA_CODE),
    commercialeState: useValueParameterAsBoolean(PHARMA_FILTER_COMMERCIALE_CODE),
  };

  const activeSearcFields: FieldsOptions[] = filterSearchFields(objectState, SEARCH_FIELDS_OPTIONS);

  return (
    <Box display="flex" maxWidth="100%">
      <CustomContent
        searchPlaceholder="Rechercher une pharmacie"
        searchInputsLabel="Filtre de recherche de pharmacie"
        type="pharmacie"
        columns={columns || COLUMNS}
        filterBy={filterBy || [{ term: { idGroupement: groupement && groupement.id } }]}
        sortBy={[{ nom: { order: 'desc' } }]}
        searchInputs={activeSearcFields}
        enableGlobalSearch={enableGlobalSearch ? enableGlobalSearch : true}
        isSelectable={selectable || false}
        checkedItems={selectedItems}
        onItemsChecked={takeSelection || takeCheckedItems}
        handleCheckItem={handleCheckItem}
        handleCheckAll={handleCheckAll}
        handleUncheckAll={handleUncheckAll}
        inSelectContainer={false}
        paginationCentered={paginationCentered || false}
        setMinimalQuery={setQuery}
        useLocalData={true}
        hideSearchBar={hideSearchBar}
        hideSearchInput={true}
        onClickRow={onClickRow}
      >
        {children}
      </CustomContent>
      <CustomFullScreenModal
        open={openFullScreenModal}
        setOpen={setOpenFullScreenModal}
        title={ficheType === 'TITULAIRE' ? 'Fiche titulaire' : 'Fiche pharmacie'}
        withBtnsActions={false}
      >
        {ficheType === 'TITULAIRE' ? (
          <FicheTitulaire id={currentId} hideFooterActions={true} />
        ) : (
          <FichePharmacie id={currentId} />
        )}
      </CustomFullScreenModal>
    </Box>
  );
};

export default withRouter(CustomPharmacie);
