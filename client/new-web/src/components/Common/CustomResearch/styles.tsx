import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControlRoot: {
      width: '100%',
    },
  }),
);

export default useStyles;
