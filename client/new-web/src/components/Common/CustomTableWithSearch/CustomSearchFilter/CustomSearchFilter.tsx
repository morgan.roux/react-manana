import React, { ChangeEvent, Dispatch, FC } from 'react';
import useStyles from './styles';
import CustomButton from '../../CustomButton';
import { orderBy } from 'lodash';
import { CustomFormTextField } from '../../CustomTextField';
import CustomSelect from '../../CustomSelect';

export interface SelectOption {
  label: string;
  value: any;
  all?: boolean;
}

export interface FilterInput {
  name: string;
  label: string;
  value: any;
  ordre: number;
  placeholder?: string;
  inputLabelProps?: object;
  extraNames?: string[];
  variant?: string;
  options?: SelectOption[];
  id?: string;
  dataType?: string;
  type: InputType;
  filterType?: FilterType;
}

export interface CustomSearchFilterProps {
  filtersState: any;
  setFiltersState: Dispatch<any>;
  filterInputs: FilterInput[];
  onClickFilter: () => void;
  onClickReset: () => void;
}

type InputType = 'Search' | 'Select';

type FilterType = 'Contains' | 'StartsWith' | 'EndsWith' | 'Match';

const placeholder = 'Tapez ici';

const CustomSearchFilter: FC<CustomSearchFilterProps> = ({
  filtersState,
  setFiltersState,
  filterInputs,
  onClickFilter,
  onClickReset,
}) => {
  const classes = useStyles({});

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      setFiltersState(prevState => ({ ...prevState, [name]: value }));
    }
  };

  console.log('filtersState **::>> ', filtersState);

  return (
    <div className={classes.customSearchFilterRoot}>
      {filterInputs.length > 0 && (
        <>
          <div className={classes.inputsContainer}>
            {orderBy(filterInputs, ['code'], ['asc']).map(
              (inputItem: FilterInput, index: number) => {
                if (inputItem.type === 'Search') {
                  return (
                    <CustomFormTextField
                      key={`custom_form_text_${index}`}
                      label={inputItem.label}
                      name={inputItem.name}
                      value={filtersState[inputItem.name] || ''}
                      onChange={handleChange}
                      placeholder={inputItem.placeholder || placeholder}
                    />
                  );
                } else if (inputItem.type === 'Select') {
                  return (
                    <CustomSelect
                      key={`custom_select_text_${index}`}
                      label={inputItem.label}
                      list={inputItem.options || []}
                      listId="value"
                      index="label"
                      name={inputItem.name}
                      value={filtersState[inputItem.name] || ''}
                      onChange={handleChange}
                    />
                  );
                }
              },
            )}
          </div>
          <div className={classes.btnsContainer}>
            <CustomButton color="secondary" size="medium" onClick={onClickFilter}>
              Filtrer
            </CustomButton>
            <CustomButton color="default" size="medium" onClick={onClickReset}>
              Réinitialiser
            </CustomButton>
          </div>
        </>
      )}
    </div>
  );
};

export default CustomSearchFilter;
