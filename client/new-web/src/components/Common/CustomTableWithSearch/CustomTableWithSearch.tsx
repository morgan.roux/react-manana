import { useApolloClient, useLazyQuery, useQuery } from '@apollo/client';
import {
  Checkbox,
  InputAdornment,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableSortLabel,
  Toolbar,
  Typography,
} from '@material-ui/core';
import { Delete, Refresh, Search, Tune } from '@material-ui/icons';
import classnames from 'classnames';
import { debounce, differenceBy, flatten, uniq, uniqBy } from 'lodash';
import React, {
  ChangeEvent,
  FC,
  MouseEvent,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { v4 as uuidv4 } from 'uuid';
import { LABO_FILTER_INPUTS } from '../../../Constant/laboratoire';
import { ME_me } from '../../../graphql/Authentication/types/ME';
import { GET_CURRENT_PHARMACIE } from '../../../graphql/Pharmacie/local';
import { GET_CUSTOM_CONTENT_SEARCH } from '../../../graphql/search/query';
import {
  CUSTOM_CONTENT_SEARCH,
  CUSTOM_CONTENT_SEARCHVariables,
} from '../../../graphql/search/types/CUSTOM_CONTENT_SEARCH';
import ICurrentPharmacieInterface from '../../../Interface/CurrentPharmacieInterface';
import { getGroupement, getUser } from '../../../services/LocalStorage';
import { getObjectByPath } from '../../../utils/getObjectByPath';
import { roundNumber } from '../../../utils/Helpers';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import { Loader } from '../../Dashboard/Content/Loader';
import Backdrop from '../Backdrop';
import CustomButton from '../CustomButton';
import { CustomFormTextField } from '../CustomTextField';
import NoItemContentImage from '../NoItemContentImage';
import {
  FiltersInterface,
  GET_LOCAL_FILTERS,
  GET_LOCAL_SEARCH,
  GET_LOCAL_SORT,
  resetSearchFilters,
  SearchInterface,
  SortInterface,
} from '../withSearch/withSearch';
import { ROWS_PER_PAGE_OPTIONS } from './constant';
import CustomSearchFilter, {
  FilterInput,
  SelectOption,
} from './CustomSearchFilter/CustomSearchFilter';
import CustomTableWithSearchPagination from './CustomTableWithSearchPagination';
import { CustomTableWithSearchColumn, CustomTableWithSearchProps } from './interface';
import useStyles from './styles';

function descendingComparator<T>(a: T, b: T, orderBy: string) {
  const orderByArray = orderBy ? orderBy.split('.') : [];
  if (orderByArray.length > 1) {
    if (getObjectByPath(b, orderBy) < getObjectByPath(a, orderBy)) {
      return -1;
    }
    if (getObjectByPath(b, orderBy) > getObjectByPath(a, orderBy)) {
      return 1;
    }
  } else {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
  }
  return 0;
}

type Order = 'asc' | 'desc';

function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: string,
): (a: { [key in Key]: number | string }, b: { [key in Key]: number | string }) => number {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

const CustomTableWithSearch: FC<CustomTableWithSearchProps> = ({
  searchType,
  columns,
  showToolbar,
  isSelectable,
  defaultOrderByKey,
  onClickRow,
  selected,
  setSelected,
  optionalMust,
  idCommandeCanal,
  withSelectionFilter,
  selectionFilterValue,
  withInputSearch,
  withEmptyContent,
  emptyTitle,
  emptySubTitle,
  emptyImgSrc,
  // dataFromProps,
  setDataFromProps,
  withCustomSearchFilter,
  formattedLabo,
  filterByFromPros,
  withFilterBtn,
  onClickBtnFilter,
  fetchPolicy,
  addedGroup,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();

  let defaultFilterBy: any[] =
    searchType === 'marche' || searchType === 'promotion'
      ? [{ term: { isRemoved: false } }]
      : searchType === 'produitcanal'
      ? [{ term: { isRemoved: false } }, { term: { isActive: true } }]
      : [];
  defaultFilterBy = filterByFromPros ? [...filterByFromPros, ...defaultFilterBy] : defaultFilterBy;
  const [filterBy, setFilterBy] = useState<any[]>(defaultFilterBy);

  const selectedIds = (selected && selected.map(item => item && item.id)) || [];
  const [dense, setDense] = useState(false);
  const [clickedRow, setClickedRow] = useState<any>(null);
  const clickedRowId = clickedRow && clickedRow.id;

  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(ROWS_PER_PAGE_OPTIONS[1]);
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(10);

  const [query, setQuery] = useState<any>({});

  const [total, setTotal] = useState<number>(0);
  const [data, setData] = useState<any[]>([]);

  const [debouncedText, setDebouncedText] = useState<string>('');

  const [filtersState, setFiltersState] = useState<any>({});

  const numSelected = selected ? (selected.length > total ? total : selected.length) : 0;

  const isIndeterminate: boolean = numSelected > 0 && numSelected < total;

  const defaultOrderBy = columns[0].key;

  const groupFriendTypeCriteria = filterByFromPros
    ? filterByFromPros.find(elt => elt.typeGroupeAmis !== undefined)
    : null;
  const groupFriendStatusCriteria = filterByFromPros
    ? filterByFromPros.find(elt => elt.status !== undefined)
    : null;

  const FILTER_EXIST: boolean = filterBy.length > 0 || (query && Object.entries(query).length > 0);

  const { data: sortFromFilter } = useQuery<SortInterface>(GET_LOCAL_SORT);
  // Set order from sortFromFilter
  const orderFromFilter =
    sortFromFilter &&
    sortFromFilter.sort &&
    sortFromFilter.sort.sortItem &&
    sortFromFilter.sort.sortItem.direction;

  let sortName =
    sortFromFilter &&
    sortFromFilter.sort &&
    sortFromFilter.sort.sortItem &&
    (sortFromFilter.sort.sortItem.name as any);

  const [order, setOrder] = useState<Order>((orderFromFilter as any) || 'asc');
  const [orderBy, setOrderBy] = useState<string>(sortName || defaultOrderByKey || defaultOrderBy);

  // const defaultSortBy = [{ [orderBy]: { order } }];
  const [sortBy, setSortBy] = useState<any[]>([]);

  // Set order from left filter
  useMemo(() => {
    // Get sort item from filter
    setOrder(orderFromFilter as any);
    // Set sortName from sortFromFilter
    setOrderBy(sortName);
    // setSortBy([{ [sortName]: { order: orderFromFilter } }]);
  }, [orderFromFilter, sortName]);

  // Get filters from filter
  const { data: filtersFromFilter } = useQuery<FiltersInterface>(GET_LOCAL_FILTERS);

  // Get search text from filter
  const { data: searchTextFromFilter } = useQuery<SearchInterface>(GET_LOCAL_SEARCH);

  /**
   * Set filterByFromPros
   */
  useMemo(() => {
    if (filterByFromPros) {
      const newFilterBy = [...filterByFromPros, ...defaultFilterBy];
      setFilterBy(uniq(newFilterBy));
    }
  }, [filterByFromPros]);

  /**
   * Get groupement
   */
  const groupement = getGroupement();
  const idGroupement = (groupement && groupement.id) || null;

  /**
   * Get user
   */
  const user: ME_me = getUser();
  const userId = (user && user.id) || null;

  /**
   * Get current pharmacie
   */
  const { data: dataPharma } = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const idPharmacie = (dataPharma && dataPharma.pharmacie && dataPharma.pharmacie.id) || null;

  /**
   * SEARCH QUERY
   */
  const [search, { loading, error, data: searchData, refetch }] = useLazyQuery<
    CUSTOM_CONTENT_SEARCH,
    CUSTOM_CONTENT_SEARCHVariables
  >(GET_CUSTOM_CONTENT_SEARCH, {
    fetchPolicy: fetchPolicy || 'no-cache',
    variables: {
      type: [searchType],
      skip,
      take,
      query,
      sortBy,
      filterBy,
      userId,
      idPharmacie,
      idGroupement,
    },
    onError: errors => {
      // displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  const formatMust = (mustArray: any[]): any[] => {
    return mustArray.reduce((prev, current) => {
      if (prev && current) {
        const x = prev.find(
          (item: any) =>
            item &&
            item.terms &&
            item.terms._id &&
            current.terms &&
            current.terms._id &&
            item.terms._id.map((i: any) => current.terms._id.some((z: any) => i === z)),
        );
        if (!x) {
          return prev.concat([current]);
        } else {
          return prev;
        }
      }
    }, []);
  };

  const removeAllOptionInSelect = (filtersState: any, searchInputs: FilterInput[]): any => {
    const newStateKeys = Object.keys(filtersState).filter(name => {
      const selectElement = searchInputs.find(
        (input: FilterInput) => input.name === name && input.type === 'Select',
      );

      if (!selectElement) return true;

      const fieldValue = filtersState[name];
      return !(selectElement.options || []).some(
        (option: SelectOption) => option.value === fieldValue && option.all,
      );
    });

    const newState: any = {};
    newStateKeys.forEach(key => {
      newState[key] = filtersState[key];
    });
    return newState;
  };

  // Generate elastic search query from fields value
  const generateQuery = (filtersState: any, searchInputs: FilterInput[]): any => {
    const must: any[] = [];
    const mustNot: any[] = [];
    const newFieldsState = removeAllOptionInSelect(filtersState, searchInputs);

    if (searchInputs) {
      searchInputs.forEach((item: FilterInput) => {
        if (
          newFieldsState[item.name] !== null &&
          newFieldsState[item.name] !== undefined &&
          newFieldsState[item.name] !== ''
        ) {
          if (
            item.type === 'Select' ||
            (item.type === 'Search' && item.filterType && item.filterType === 'Match')
          ) {
            /*
            must.push({
              match: {
                [item.name]: {
                  query: newFieldsState[item.name],
                },
              },
            });
            */
          } else if (item.type === 'Search') {
            let q = `${newFieldsState[item.name]}`
              .trim()
              .split(' ')
              .filter(str => str.trim().length > 0)
              .join('\\ ');

            q = item.filterType
              ? item.filterType === 'StartsWith'
                ? `${q}*`
                : item.filterType === 'EndsWith'
                ? `*${q}`
                : `*${q}*`
              : `*${q}*`;

            must.push({
              query_string: {
                query: q,
                fields: item.extraNames ? [item.name, ...item.extraNames] : [item.name],
              },
            });
          }

          // Filtre laboratoire par obligatoire
          if (searchType === 'laboratoire' && formattedLabo && formattedLabo.length > 0) {
            if (filtersState.obligatoire) {
              const trueLabs = formattedLabo.filter(item => item.obligatoire === true);

              const prevMust: any[] =
                query && query.query && query.query.bool && query.query.bool.must;
              const prevMustNot: any[] =
                query && query.query && query.query.bool && query.query.bool.must_not;

              if (trueLabs.length > 0) {
                const trueLabsIds = trueLabs.map(item => item.idLaboratoire);
                const newTerms = { terms: { _id: trueLabsIds } };

                switch (filtersState.obligatoire) {
                  case 'true':
                    if (prevMust && prevMust.length > 0) {
                      const newIds: any[] = [];
                      prevMust.map(item => {
                        if (item && item.terms && item.terms._id && item.terms._id.length > 0) {
                          newIds.concat(item.terms._id);
                        }
                      });
                      must.push({ terms: { _id: uniq(newIds) } });
                    } else {
                      must.push(newTerms);
                    }
                    break;
                  case 'false':
                    if (prevMustNot && prevMustNot.length > 0) {
                      const newIds: any[] = [];
                      prevMustNot.map(item => {
                        if (item && item.terms && item.terms._id && item.terms._id.length > 0) {
                          newIds.concat(item.terms._id);
                        }
                      });
                      must.push({ terms: { _id: uniq(newIds) } });
                    } else {
                      mustNot.push(newTerms);
                    }
                    break;
                  default:
                    break;
                }
              } else {
                displaySnackBar(client, {
                  type: 'INFO',
                  isOpen: true,
                  message: 'Aucun laboratoire obligatoire',
                });
              }
            }
          }
        }
      });
    }

    const finalMust = formatMust(must);
    const finalMustNot = formatMust(mustNot);

    return {
      query: {
        bool: { must: finalMust, must_not: finalMustNot },
      },
    };
  };

  /**
   * Set data & total
   */
  useEffect(() => {
    if (searchData && searchData.search && searchData.search.data) {
      setData(searchData.search.data);
      setTotal(searchData.search.total);
      if (setDataFromProps) setDataFromProps(searchData.search.data);
    }
  }, [searchData]);

  /**
   * Execute search on component mounted
   */
  useEffect(() => {
    search();
  }, []);

  /**
   * Execute search if variables change
   */
  useMemo(() => {
    search();
  }, [searchType, skip, take, query, sortBy, filterBy, userId, idPharmacie, idGroupement]);

  const debouncedSearchText = useRef(
    debounce((value: string) => {
      client.writeQuery({
        query: GET_LOCAL_SEARCH,
        data: { searchText: { text: value || '' } },
      });
    }, 1500),
  );

  const onChangeSearchText = useCallback((e: ChangeEvent<any>) => {
    setDebouncedText(e.target.value);
  }, []);

  useEffect(() => {
    debouncedSearchText.current(debouncedText);
  }, [debouncedText]);

  //Criteria with addedNumber
  useEffect(() => {
    if (addedGroup && refetch) {
      refetch();
    }
  }, [addedGroup]);

  /**
   * Update query if left filters & sort change
   */
  useEffect(() => {
    const text =
      (searchTextFromFilter &&
        searchTextFromFilter.searchText &&
        searchTextFromFilter.searchText.text) ||
      '';
    setDebouncedText(text);

    if (searchType === 'commande') sortName = '';

    if (filtersFromFilter && filtersFromFilter.filters) {
      const must: any = [];
      const should: any = [];

      if (searchType === 'produitcanal') {
        if (filtersFromFilter.filters.idFamilles.length > 0) {
          const familles: any = filtersFromFilter.filters.idFamilles.map(codeFamille => ({
            prefix: {
              'famille.codeFamille': codeFamille,
            },
          }));

          must.push({ bool: { should: familles } });
        }
        if (filtersFromFilter.filters.idLaboratoires.length > 0) {
          must.push({
            terms: {
              'laboratoire.id': filtersFromFilter.filters.idLaboratoires,
            },
          });
        }
        if (filtersFromFilter.filters.idGammesCommercials.length > 0) {
          must.push({
            terms: {
              'sousGammeCommercial.gammeCommercial.idGammeCommercial':
                filtersFromFilter.filters.idGammesCommercials,
            },
          });
        }
        if (
          filtersFromFilter.filters.idCommandeCanal &&
          filtersFromFilter.filters.idCommandeCanal.length > 0
        ) {
          must.push({
            terms: {
              'commandeCanal.id': filtersFromFilter.filters.idCommandeCanal,
            },
          });
        }
        if (idCommandeCanal) {
          must.push({
            terms: {
              'commandeCanal.id': [idCommandeCanal],
            },
          });
        }
        if (filtersFromFilter.filters.idRemise) {
          if (filtersFromFilter.filters.idRemise.includes(0)) {
            must.push({
              terms: {
                'pharmacieRemisePaliers.id': [idPharmacie],
              },
            });
          }
          if (filtersFromFilter.filters.idRemise.includes(1)) {
            must.push({
              terms: {
                'pharmacieRemisePanachees.id': [idPharmacie],
              },
            });
          }
        }
      }

      if (searchType === 'operation') {
        if (
          filtersFromFilter.filters.idCommandeCanal &&
          filtersFromFilter.filters.idCommandeCanal.length > 0
        ) {
          must.push({
            terms: {
              'commandeCanal.id': filtersFromFilter.filters.idCommandeCanal,
            },
          });
        }
        if (filtersFromFilter.filters.idCommandeTypes.length > 0) {
          must.push({
            terms: {
              'commandeType.id': filtersFromFilter.filters.idCommandeTypes,
            },
          });
        }
      }

      if (searchType === 'actualite') {
        if (filtersFromFilter.filters.idActualiteOrigine.length > 0) {
          must.push({
            terms: {
              'origine.id': filtersFromFilter.filters.idActualiteOrigine,
            },
          });
        }
      }

      if (searchType === 'laboratoire') {
        if (filtersFromFilter.filters.sortie && filtersFromFilter.filters.sortie.length > 0) {
          must.push({
            terms: {
              sortie: filtersFromFilter.filters.sortie,
            },
          });
        }
      }

      updateQuery(must, order, sortName, text, should);
    } else if (!filtersFromFilter) {
      updateQuery([], order, sortName, text);
    }
  }, [searchType, searchTextFromFilter, filtersFromFilter]);

  const setQueryWithCallback = useCallback((query: any) => {
    setQuery(query);
  }, []);

  /**
   * Update query
   */
  const updateQuery = useCallback(
    (must: any, order: string, sortName: string, text: string, should?: any) => {
      // add search text to must if exist
      if (text && text.length > 0) {
        const textSearch = text.replace(/\s+/g, '\\ ').replace('/', '\\/');
        must.push({
          query_string: {
            query:
              textSearch.startsWith('*') || textSearch.endsWith('*')
                ? textSearch
                : `*${textSearch}*`,
            analyze_wildcard: true,
          },
        });
      }

      if (optionalMust) {
        if (optionalMust && optionalMust.must) {
          must = must.concat(optionalMust.must);
        }
      }

      let sortQuery = sortName ? [{ [sortName]: { order } }] : [];
      if (searchType === 'actualite' || searchType === 'operation') {
        sortQuery = [{ niveauPriorite: { order: 'desc' } }, ...sortQuery];
      }

      if (must.length > 0 && sortQuery.length > 0) {
        const queryFilter = {
          query: {
            bool: {
              must: formatMust([...must]),
              should: (optionalMust && optionalMust.should) || [],
              minimum_should_match: optionalMust && optionalMust.minimum_should_match,
            },
          },
          sort: sortQuery,
        };

        setQueryWithCallback(queryFilter);
      } else if (sortQuery.length > 0) {
        const queryFilter = { sort: sortQuery };
        setQueryWithCallback(queryFilter);
      } else if (must.length > 0) {
        const queryFilter = {
          query: {
            bool: {
              must: formatMust([...must]),
            },
          },
        };
        setQueryWithCallback(queryFilter);
      } else {
        setQueryWithCallback({});
      }
    },
    [],
  );

  /**
   * Reset filters if searchType change
   */
  useMemo(() => {
    if (query !== '') setQueryWithCallback('');
    if (sortFromFilter && sortFromFilter.sort && sortFromFilter.sort.sortItem) {
      client.writeQuery({ query: GET_LOCAL_SORT, data: { sort: { sortItem: null } } });
    }
    resetSearchFilters(client);
  }, [searchType]);

  /**
   * Filter data if is withSelectionFilter
   */
  const queryBySelection = (selectionValue: string): any => {
    if (selectionValue && searchData) {
      switch (selectionValue) {
        case 'all':
          setSkip(0);
          return '';
        case 'selected':
          if (selected && selected.length > 0) {
            const newTerms = { terms: { _id: selectedIds } };
            const prevMust = query && query.query && query.query.bool && query.query.bool.must;
            const newMust =
              prevMust && prevMust.length > 0 ? [...prevMust, ...[newTerms]] : [newTerms];
            const newQuery = { query: { bool: { must: newMust } } };
            setSkip(0);
            return newQuery;
          } else {
            setSkip(0);
            displaySnackBar(client, {
              type: 'INFO',
              message: 'Aucun élément sélectionné',
              isOpen: true,
            });
            return '';
          }
        case 'notselected':
          if (selected && selected.length > 0) {
            const newTerms = { terms: { _id: selectedIds } };
            const prevMustNot =
              query && query.query && query.query.bool && query.query.bool.must_not;
            const newMustNot =
              prevMustNot && prevMustNot.length > 0 ? [...prevMustNot, ...[newTerms]] : [newTerms];

            const newQuery = { query: { bool: { must_not: newMustNot } } };
            setSkip(0);
            return newQuery;
          } else {
            setSkip(0);
            return '';
          }
        default:
          setSkip(0);
          return '';
      }
    }
  };

  /**
   * Filter by selection (withSelectionFilter)
   */
  useMemo(() => {
    if (withSelectionFilter && selectionFilterValue) {
      const q = queryBySelection(selectionFilterValue);
      setQueryWithCallback(q);
    }
  }, [withSelectionFilter, selectionFilterValue]);

  const handleSort = (property: any) => (event: MouseEvent<unknown>) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
    updateQuery([], isAsc ? 'desc' : 'asc', property, '');
    // setSortBy([{ [property]: { order: isAsc ? 'desc' : 'asc' } }]);
  };

  const updateSelected = useCallback((selectedData: any[]) => {
    if (setSelected) {
      setSelected(selectedData);
    }
  }, []);

  const handleSelectAllClick = (event: ChangeEvent<HTMLInputElement>) => {
    if (setSelected && selected) {
      if (data && data.length > 0) {
        if (isIndeterminate) {
          const diff = differenceBy(data, selected, 'id');
          if (diff && diff.length > 0) {
            updateSelected(uniqBy([...selected, ...diff], 'id'));
          } else {
            const newSelected = selected.filter(item => data.every(d => d.id !== item.id));
            updateSelected(uniqBy(newSelected, 'id'));
          }
          return;
        }

        if (!isIndeterminate && selected.length === 0) {
          updateSelected(uniqBy(data, 'id'));
          return;
        }
      }
      updateSelected([]);
    }
  };

  const handleCheck = (row: any, rowId: string) => (event: MouseEvent<unknown>) => {
    event.preventDefault();
    event.stopPropagation();
    if (selected && setSelected) {
      const selectedIndex = selectedIds.indexOf(rowId);
      let newSelected: string[] = [];

      if (selectedIndex === -1) {
        newSelected = newSelected.concat(selected, row);
      } else if (selectedIndex === 0) {
        newSelected = newSelected.concat(selected.slice(1));
      } else if (selectedIndex === selected.length - 1) {
        newSelected = newSelected.concat(selected.slice(0, -1));
      } else if (selectedIndex > 0) {
        newSelected = newSelected.concat(
          selected.slice(0, selectedIndex),
          selected.slice(selectedIndex + 1),
        );
      }

      // setSelected(newSelected);
      updateSelected(newSelected);
    }
  };

  // console.log('selected :>> ', selected);

  const handleClickRow = (row: any, rowId: number | string) => (event: MouseEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    if (row && onClickRow) {
      onClickRow(row);
      if (rowId !== clickedRowId) {
        setClickedRow(row);
      }
    }
  };

  const handleChangeDense = (event: ChangeEvent<HTMLInputElement>) => {
    setDense(event.target.checked);
  };

  const clearSelection = useCallback(() => {
    if (setSelected) setSelected([]);
  }, []);

  const isSelected = (rowId: string | number) => selectedIds.indexOf(rowId) !== -1;
  const isClicked = (rowId: string | number) => clickedRowId === rowId;

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, total - page * rowsPerPage);

  const isLoading = (): boolean => {
    if (loading && data.length === 0) {
      return true;
    }
    return false;
  };

  const filterInputs: FilterInput[] = searchType === 'laboratoire' ? LABO_FILTER_INPUTS : [];

  const handleClickFilter = () => {
    const query = generateQuery(filtersState, filterInputs);
    const queryMust: any[] = query && query.query && query.query.bool && query.query.bool.must;
    if (filtersState && filtersState.selection) {
      const q = queryBySelection(filtersState.selection);
      const qMust: any[] = q && q.query && q.query.bool && q.query.bool.must;
      if (qMust && qMust.length > 0) {
        const qMustKeys = flatten(qMust.map(item => Object.keys(item)));
        if (qMustKeys.includes('query_string')) {
          setQueryWithCallback(q);
        } else if (queryMust && queryMust.length > 0) {
          const newMust = [...qMust, ...queryMust];
          const newQuery = {
            ...q,
            query: { ...q.query, bool: { ...q.query.bool, must: formatMust(newMust) } },
          };
          setQueryWithCallback(newQuery);
        } else {
          setQueryWithCallback(q);
        }
        return;
      }
    }
    setQueryWithCallback(query);
  };

  const handleResetFilter = () => {
    setFiltersState({});
    setQueryWithCallback({});
  };

  const onClickRefresh = () => {
    search();
  };

  if (loading && data.length === 0 && searchData && searchData.search && !searchData.search.data) {
    return <Loader />;
  }

  if (error) {
    // return <div>Error</div>;
  }

  return (
    <div className={classes.customTableroot}>
      {isLoading() && <Backdrop />}
      {data.length === 0 &&
      withEmptyContent &&
      !FILTER_EXIST &&
      !loading &&
      searchData &&
      searchData.search &&
      !searchData.search.data ? (
        <div className={classes.emptyContainer}>
          <NoItemContentImage
            title={emptyTitle || 'Aucun élement'}
            subtitle={emptySubTitle || "Vous n'avez pas encore d'élément dans cette rubrique."}
            src={emptyImgSrc}
          />
        </div>
      ) : (
        <>
          {withCustomSearchFilter && (
            <CustomSearchFilter
              filtersState={filtersState}
              setFiltersState={setFiltersState}
              filterInputs={filterInputs}
              onClickFilter={handleClickFilter}
              onClickReset={handleResetFilter}
            />
          )}
          {withInputSearch && (
            <div
              className={
                showToolbar
                  ? classes.searchInputContainer
                  : classnames(classes.searchInputContainer, classes.searchInputContainerWithMargin)
              }
            >
              <CustomFormTextField
                placeholder="Rechercher"
                value={debouncedText}
                onChange={onChangeSearchText}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <Search color="disabled" />
                    </InputAdornment>
                  ),
                }}
              />
            </div>
          )}
          {showToolbar && (
            <Toolbar className={classes.toolbar}>
              <div className={classes.toolbarInfo}>
                {isSelectable && (
                  <Typography color="inherit" variant="subtitle1">
                    {`${numSelected}/${total}`} Sélectionné(s)
                  </Typography>
                )}
                {FILTER_EXIST && (
                  <Typography color="inherit" variant="subtitle1">
                    {total} résultat(s) trouvé(s)
                  </Typography>
                )}
              </div>

              <div>
                {/* <Tooltip
                  title="Vider la sélection"
                  // style={{ display: numSelected > 0 ? 'block' : 'none' }}
                >
                  <IconButton aria-label="delete" onClick={clearSelection}>
                    <Delete />
                  </IconButton>
                </Tooltip> */}
                {numSelected > 0 && (
                  <CustomButton
                    variant="text"
                    color="inherit"
                    size="medium"
                    startIcon={<Delete />}
                    className={classes.btn}
                    onClick={clearSelection}
                  >
                    Vider la sélection
                  </CustomButton>
                )}
                {withFilterBtn && (
                  <CustomButton
                    variant="text"
                    color="inherit"
                    size="medium"
                    startIcon={<Tune />}
                    className={classes.btn}
                    onClick={onClickBtnFilter}
                  >
                    Filtres
                  </CustomButton>
                )}
                <CustomButton
                  variant="text"
                  color="inherit"
                  size="medium"
                  startIcon={<Refresh />}
                  className={classes.btn}
                  onClick={onClickRefresh}
                >
                  Réactualiser
                </CustomButton>
              </div>
            </Toolbar>
          )}
          <TableContainer className={classes.customTableContainer}>
            <Table
              className={classes.table}
              aria-labelledby="tableTitle"
              size={dense ? 'small' : 'medium'}
              aria-label="sticky custom table"
              stickyHeader={true}
            >
              <TableHead>
                <TableRow>
                  {isSelectable && (
                    <TableCell padding="checkbox">
                      <Checkbox
                        indeterminate={isIndeterminate}
                        checked={total > 0 && numSelected === total}
                        onChange={handleSelectAllClick}
                        inputProps={{ 'aria-label': 'select all desserts' }}
                      />
                    </TableCell>
                  )}
                  {columns.map((column: CustomTableWithSearchColumn, index: number) => {
                    return (
                      <TableCell
                        key={`${column.key}_${index}`}
                        align={column.numeric ? 'left' : 'left'}
                        padding={column.disablePadding && isSelectable ? 'none' : 'default'}
                        sortDirection={orderBy === column.key ? order : false}
                        className={classes.tableHeadCell}
                      >
                        {column.sortable !== false ? (
                          <TableSortLabel
                            active={orderBy === column.key}
                            direction={orderBy === column.key ? order : 'asc'}
                            onClick={handleSort(column.key)}
                          >
                            {column.label}
                            {orderBy === column.key ? (
                              <span className={classes.visuallyHidden}>
                                {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                              </span>
                            ) : null}
                          </TableSortLabel>
                        ) : (
                          column.label
                        )}
                      </TableCell>
                    );
                  })}
                </TableRow>
              </TableHead>

              <TableBody>
                {stableSort(data, getComparator(order, orderBy)).map((row: any, index: number) => {
                  const isItemSelected = isSelected(row.id);
                  const labelId = `custom-table-checkbox-${index}`;
                  const isItemClicked = isClicked(row.id);
                  return (
                    <TableRow
                      hover={true}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={`${uuidv4()}_${index}`}
                      selected={isItemSelected}
                      onClick={handleClickRow(row, row.id)}
                      style={{ cursor: onClickRow ? 'pointer' : 'default' }}
                      className={
                        isItemClicked ? classnames(classes.cursorPointer, classes.activeRow) : ''
                      }
                    >
                      {isSelectable && (
                        <TableCell padding="checkbox">
                          <Checkbox
                            checked={isItemSelected}
                            inputProps={{ 'aria-labelledby': labelId }}
                            onClick={handleCheck(row, row.id)}
                            color="primary"
                          />
                        </TableCell>
                      )}
                      {columns.map((column: CustomTableWithSearchColumn, columnIndex: number) => {
                        return (
                          <TableCell key={`row-${index}-${columnIndex}`} align="left">
                            {column.renderer
                              ? column.renderer(row)
                              : row[column.key] !== undefined && row[column.key] !== null
                              ? column.numeric
                                ? roundNumber(row[column.key])
                                : row[column.key]
                              : '-'}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </TableContainer>
          <CustomTableWithSearchPagination
            total={total}
            page={page}
            rowsPerPage={rowsPerPage}
            take={take}
            skip={skip}
            setTake={setTake}
            setSkip={setSkip}
            setPage={setPage}
            setRowsPerPage={setRowsPerPage}
          />
        </>
      )}
    </div>
  );
};

export default CustomTableWithSearch;
