import { useQuery } from '@apollo/client';
import gql from 'graphql-tag';
import { useContext } from 'react';
import { ContentContext, ContentStateInterface } from '../../../AppContext';
import { GET_CURRENT_PHARMACIE } from '../../../graphql/Pharmacie/local';
import ICurrentPharmacieInterface from '../../../Interface/CurrentPharmacieInterface';
import { getGroupement } from '../../../services/LocalStorage';
import {
  GET_LOCAL_SEARCH,
  GET_LOCAL_SORT,
  SearchInterface,
  SortInterface,
} from '../withSearch/withSearch';

export interface FiltersInterface {
  filters: {
    idCheckeds: string[];
    idLaboratoires: number[];
    idFamilles: number[];
    idGammesCommercials: number[];
    idCommandeCanal: string;
    idCommandeCanals: string[];
    idCommandeTypes: number[];
    idActualiteOrigine: string[];
    idRemise: number[];
    idOcCommande: number[];
    idSeen: number[];
    idTodoType: number[];
    sortie: number[];
    laboType: number[];
    reaction: number[];
  };
}

export interface IdCheckedsInterface {
  idCheckeds: string[];
}

export const GET_ID_CHECKEDS = gql`
  {
    idCheckeds @client
  }
`;

export const GET_LOCAL_FILTERS = gql`
  {
    filters @client {
      idCheckeds
      idLaboratoires
      idFamilles
      idGammesCommercials
      idCommandeCanal
      idCommandeCanals
      idCommandeTypes
      idActualiteOrigine
      idRemise
      idOcCommande
      idSeen
      idTodoType
      sortie
      laboType
      reaction
    }
  }
`;

export const BuildQuery = props => {
  const {
    optionalMust,
    optionalMustNot,
    optionalShould,
    minimumShouldMatch,
    type,
    sortByProps,
    fields,
    defaultSort,
  } = props;

  const groupement = getGroupement();
  // get sort item from local state
  const sortItem = useQuery<SortInterface>(GET_LOCAL_SORT);

  const idCheckedsQuery = useQuery<IdCheckedsInterface>(GET_ID_CHECKEDS);

  const idCheckeds =
    (idCheckedsQuery && idCheckedsQuery.data && idCheckedsQuery.data.idCheckeds) || [];

  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

  const currentPharmacie = myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie;

  // get filters id list from local state
  const filters = useQuery<FiltersInterface>(GET_LOCAL_FILTERS);

  // get search text from local state
  const searchText = useQuery<SearchInterface>(GET_LOCAL_SEARCH);

  const {
    content: { contextQuery },
    // setContent,
  } = useContext<ContentStateInterface>(ContentContext);

  // set order from sortItem
  const order =
    sortItem && sortItem.data && sortItem.data.sort && sortItem.data.sort.sortItem
      ? sortItem.data.sort.sortItem.direction
      : 'asc';

  // set sortName from sortItem
  const sortName =
    (sortItem &&
      sortItem.data &&
      sortItem.data.sort &&
      sortItem.data.sort.sortItem &&
      sortItem.data.sort.sortItem.name) ||
    '';

  const sortQuery = sortByProps
    ? sortByProps
    : sortName
      ? [{ [sortName]: { order } }]
      : defaultSort || [];

  let must: any = [];
  let mustNot: any[] = [];

  if (contextQuery && contextQuery.must) {
    must = must.concat(contextQuery.must);
  }

  if (optionalMust) {
    must = must.concat(optionalMust);
  }

  if (optionalMustNot) {
    mustNot = mustNot.concat(optionalMustNot);
  }

  if (
    searchText &&
    searchText.data &&
    searchText.data.searchText &&
    searchText.data.searchText.text &&
    searchText.data.searchText.text.length
  ) {
    const textSearch = searchText.data.searchText.text.replace(/\s+/g, '\\ ').replace('/', '\\/');
    must.push({
      query_string: {
        query:
          textSearch.startsWith('*') || textSearch.endsWith('*') ? textSearch : `*${textSearch}*`,
        fields: fields && fields.length ? fields : [],
        analyze_wildcard: true,
      },
    });
  }

  if (idCheckeds.length > 0) {
    must.push({ terms: { _id: idCheckeds } });
  } else if (filters && filters.data && filters.data.filters) {
    const filter = filters.data.filters;

    if (filter.idCommandeCanal && type && type === 'produitcanal') {
      must.push({
        term: {
          'commandeCanal.code': filter.idCommandeCanal,
        },
      });
    } else if (!filter.idCommandeCanal && type && type === 'produitcanal') {
      must.push({
        term: {
          'commandeCanal.code': 'PFL',
        },
      });
    }
    if (filter.idCommandeCanals && filter.idCommandeCanals.length && type && type === 'produit') {
      filter.idCommandeCanals.map(id => {
        must.push({
          nested: {
            path: 'canauxArticle',
            query: {
              bool: {
                must: [
                  {
                    match: {
                      'canauxArticle.commandeCanal.id': id,
                    },
                  },
                  {
                    match: {
                      'canauxArticle.isRemoved': false,
                    },
                  },
                ],
              },
            },
          },
        });
      });
    }
    if (filter.idLaboratoires && filter.idLaboratoires.length) {
      must.push(
        type === 'produitcanal'
          ? { terms: { 'produit.produitTechReg.laboExploitant.id': filter.idLaboratoires } }
          : { terms: { 'produitTechReg.laboExploitant.id': filter.idLaboratoires } },
      );
    }
    if (filter.idGammesCommercials && filter.idGammesCommercials.length) {
      must.push({
        terms: { 'sousGammeCommercial.gammeCommercial.id': filter.idGammesCommercials },
      });
    }
    if (filter.idFamilles && filter.idFamilles.length) {
      const familles = filter.idFamilles.map(codeFamille =>
        type === 'produitcanal'
          ? {
            prefix: {
              'produit.famille.codeFamille': codeFamille,
            },
          }
          : {
            prefix: {
              'famille.codeFamille': codeFamille,
            },
          },
      );
      must.push({ bool: { should: familles } });
    }
    if (filter.idRemise) {
      if (filter.idRemise.includes(0)) {
        must.push({
          terms: {
            'pharmacieRemisePaliers.id': [currentPharmacie && currentPharmacie.id],
          },
        });
      }
      if (filter.idRemise.includes(1)) {
        must.push({
          terms: {
            'pharmacieRemisePanachees.id': [currentPharmacie && currentPharmacie.id],
          },
        });
      }
    }
    if (filter.idCommandeTypes && filter.idCommandeTypes.length) {
      must.push({
        terms: {
          'commandeType.id': filter.idCommandeTypes,
        },
      });
    }
    if (filter.idOcCommande) {
      if (filter.idOcCommande.includes(0)) {
        must.push({
          term: {
            commandePassee: true,
          },
        });
      }
      if (filter.idOcCommande.includes(1)) {
        must.push({
          term: {
            commandePassee: false,
          },
        });
      }
    }
    if (filter.sortie) {
      if (filter.sortie.length === 1) {
        if (filter.sortie.includes(0)) {
          must.push({
            term: {
              sortie: 0,
            },
          });
        }
        if (filter.sortie.includes(1)) {
          must.push({
            term: {
              sortie: 1,
            },
          });
        }
      }
    }

    if (filter.laboType && type === 'laboratoire') {
      if (filter.laboType.length === 1) {
        if (filter.laboType.includes(0)) {
          must.push({
            exists: {
              field: 'laboratoirePartenaire',
            },
          });
        }
        if (filter.laboType.includes(1)) {
          mustNot.push({
            exists: {
              field: 'laboratoirePartenaire',
            },
          });
        }
      }
    }

    if (filter.reaction && type === 'produitcanal' && groupement) {
      if (filter.reaction.length === 1) {
        if (filter.reaction.includes(0)) {
          must.push({
            term: {
              'groupementComments.id': groupement && groupement.id,
            },
          });
        }
        if (filter.reaction.includes(1)) {
          must.push({
            term: {
              'groupementSmyleys.id': groupement && groupement.id,
            },
          });
        }
      } else {
        if (filter.reaction.includes(0) && filter.reaction.includes(1)) {
          must.push(
            {
              term: {
                'groupementComments.id': groupement && groupement.id,
              },
            },
            {
              term: {
                'groupementSmyleys.id': groupement && groupement.id,
              },
            },
          );
        }
      }
    }
  }

  const query = sortQuery
    ? {
      query: {
        bool: {
          must,
          must_not: mustNot,
          should: optionalShould,
          minimum_should_match: minimumShouldMatch,
        },
      },

      sort: sortQuery,
    }
    : {
      query: {
        bool: {
          must,
          must_not: mustNot,
          should: optionalShould,
          minimum_should_match: minimumShouldMatch,
        },
      },
    };

  return filters && filters.data && sortItem && sortItem.data && currentPharmacie
    ? query
    : optionalMust
      ? query
      : null;
};
