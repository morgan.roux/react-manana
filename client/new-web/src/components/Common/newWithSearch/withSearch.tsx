import { useQuery } from '@apollo/client';
import { Box } from '@material-ui/core';
import gql from 'graphql-tag';
import React, { FC, useContext, useEffect, useState } from 'react';
import { ContentContext, ContentStateInterface } from '../../../AppContext';
import { CODE_LIGNE_TABLEAU } from '../../../Constant/parameter';
import { GET_PARAMETER_BY_CODE } from '../../../graphql/Parametre/query';
import {
  PARAMETER_BY_CODE,
  PARAMETER_BY_CODEVariables,
} from '../../../graphql/Parametre/types/PARAMETER_BY_CODE';
import { GET_CURRENT_PHARMACIE } from '../../../graphql/Pharmacie/local';
import ICurrentPharmacieInterface from '../../../Interface/CurrentPharmacieInterface';
import { getGroupement } from '../../../services/LocalStorage';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import Filter from './Filter';
import { BuildQuery } from './queryBuilder';


export const GET_VIEW_MODE = gql`
  {
    viewMode @client
  }
`;

export const GET_SKIP_AND_TAKE = gql`
  {
    skipAndTake @client {
      skip
      take
    }
  }
`;

export const NULL_QUERY = gql`
  {
    nullQuery @client
  }
`;

interface WithSearchProps {
  // TODO : Solution provisoire
  useFederation?: boolean;

  WrappedComponent?: any;
  children?: any;
  columns?: any;
  type: string;
  take?: number;
  skip?: number;
  filterProps?: any;
  searchQuery?: any;
  customContentQuery?: any;
  isSelectable?: boolean;
  viewMode?: string;
  optionalMust?: any;
  optionalMustNot?: any[];
  optionalShould?: any[];
  sortByProps?: any[]; // type => [{ [sortName]: { order } }]
  defaultSort?: any[];
  minimumShouldMatch?: number;
  optionalVariables?: any;
  tableMode?: boolean;
  noFilter?: boolean;
  props?: any;
  fetchPolicy?: any;
  unableRefetch?: boolean;
  skipQueryValue?: boolean;
  skipSearch?: boolean;
  fields?: string[];
}

const WithSearch: FC<WithSearchProps> = ({
  type,
  take,
  skip,
  filterProps,
  WrappedComponent,
  searchQuery,
  optionalMust,
  optionalMustNot,
  optionalShould,
  minimumShouldMatch,
  optionalVariables,
  tableMode,
  noFilter,
  skipQueryValue,
  props,
  fetchPolicy,
  unableRefetch,
  skipSearch,
  sortByProps,
  fields,
  defaultSort,
  useFederation = false
}) => {
  const [currentTake, setTake] = useState<number>(take || 12);
  const [currentSkip, setSkip] = useState<number>(skip || 0);

  const skipAndTakeQuery = useQuery(GET_SKIP_AND_TAKE);

  const skipAndTakeResult =
    skipAndTakeQuery && skipAndTakeQuery.data && skipAndTakeQuery.data.skipAndTake;

  const query = noFilter
    ? {}
    : BuildQuery({
      optionalMust,
      optionalMustNot,
      optionalShould,
      minimumShouldMatch,
      type,
      sortByProps,
      fields,
      defaultSort
    });

  const myParameter = useQuery<PARAMETER_BY_CODE, PARAMETER_BY_CODEVariables>(
    GET_PARAMETER_BY_CODE,
    {
      variables: { code: CODE_LIGNE_TABLEAU },
    },
  );

  useEffect(() => {
    if (skipAndTakeResult) {
      setSkip(skipAndTakeResult.skip);
      setTake(skipAndTakeResult.take);
    }
  }, [skipAndTakeResult]);

  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

  const currentPharmacie = myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie;

  const groupement = getGroupement();
  const groupementId = (groupement && groupement.id) || '';

  const skipQuery = (!skipQueryValue && (!query || !myParameter.data)) || skipSearch ? true : false;


  const commonVariables = {
    query,
    skip: currentSkip,
    take: currentTake,
  }

  const searchResult = useQuery<any, any>(searchQuery, {
    variables: useFederation ? {
      index: [type],
      ...commonVariables
    } : {
      type: [type],
      idPharmacie: currentPharmacie && currentPharmacie.id,
      ...commonVariables,
      ...optionalVariables,
      idGroupement: groupementId,
    },
    skip: skipQuery,
    fetchPolicy: fetchPolicy || 'cache-first',
    client: useFederation ? FEDERATION_CLIENT : undefined
  });

  const {
    content: { page, rowsPerPage, searchPlaceholder, contextQuery },
    setContent,
  } = useContext<ContentStateInterface>(ContentContext);

  useEffect(() => {
    if (searchResult && searchResult.data && searchResult.variables) {
      if (!unableRefetch) searchResult.refetch();
      setContent({
        page,
        type,
        searchPlaceholder,
        rowsPerPage,
        variables: searchResult.variables,
        operationName: searchQuery,
        refetch: searchResult.refetch,
        contextQuery,
      });
    }
  }, [
    contextQuery,
    page,
    rowsPerPage,
    searchPlaceholder,
    searchQuery,
    searchResult,
    setContent,
    type,
    unableRefetch,
  ]);

  return (
    <Box display="flex">
      {filterProps && <Filter filterProps={filterProps} />}
      <WrappedComponent
        listResult={searchResult}
        currentTake={currentTake}
        currentSkip={currentSkip}
        {...props}
      />
    </Box>
  );
};
export default WithSearch;
