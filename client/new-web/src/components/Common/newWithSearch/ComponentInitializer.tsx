import { useQuery } from '@apollo/client';
import { Box } from '@material-ui/core';
import { Info, ShoppingBasket, StoreMallDirectory } from '@material-ui/icons';
import moment from 'moment';
import React, { useState } from 'react';
import { GET_CHECKEDS_ACTUALITE } from '../../../graphql/Actualite/local';
import { GET_SEARCH_CUSTOM_CONTENT_ACTUALITE } from '../../../graphql/Actualite/query';
import { GET_CHECKEDS_COMMANDE } from '../../../graphql/Commande/local';
import { GET_SEARCH_CUSTOM_CONTENT_COMMANDE } from '../../../graphql/Commande/query';
import { GET_SEARCH_CUSTOM_CONTENT_GROUPE_CLIENT } from '../../../graphql/GroupeClient';
import { GET_CODEITEM_PREFIX } from '../../../graphql/Item/local';
import { GET_CHECKEDS_LABORATOIRE } from '../../../graphql/Laboratoire/local';
import {
  DO_SEARCH_LABORATOIRE,
  GET_SEARCH_CUSTOM_CONTENT_LABORATOIRE,
  GET_SEARCH_LABORATOIRE_PARTENAIRE,
} from '../../../graphql/Laboratoire/query';
import { GET_CHECKEDS_OPERATION } from '../../../graphql/OperationCommerciale/local';
import { GET_SEARCH_CUSTOM_CONTENT_OPERATION } from '../../../graphql/OperationCommerciale/query';
import { GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE } from '../../../graphql/PartenairesServices/query';
import { GET_SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT } from '../../../graphql/Personnel/query';
import { GET_SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION } from '../../../graphql/PersonnelAffectation';
import { GET_CHECKEDS_PHARMACIE, GET_CURRENT_PHARMACIE } from '../../../graphql/Pharmacie/local';
import { GET_SEARCH_CUSTOM_CONTENT_PHARMACIE } from '../../../graphql/Pharmacie/query';
import { GET_SEARCH_CUSTOM_CONTENT_PPHARMACIE } from '../../../graphql/Ppersonnel/query';
import { GET_SEARCH_GESTION_PRODUIT } from '../../../graphql/Produit/query';
import { GET_CHECKEDS_PRODUIT } from '../../../graphql/ProduitCanal/local';
import {
  GET_SEARCH_CUSTOM_CONTENT_PRODUIT,
  GET_SEARCH_PRODUIT_CANAL,
} from '../../../graphql/ProduitCanal/query';
import { GET_CHECKEDS_PROJECT } from '../../../graphql/Project/local';
import { GET_SEARCH_CUSTOM_CONTENT_PROJECT } from '../../../graphql/Project/query';
import { GET_SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIRE } from '../../../graphql/SuiviPublicitaire/query';
import { GET_SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE } from '../../../graphql/Titulaire/query';
import { GET_SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION } from '../../../graphql/TitulaireAffectation/query';
import { getGroupement, getUser } from '../../../services/LocalStorage';
import GroupeClient from '../../Dashboard/GroupeClient';
import { PartenairesServices } from '../../Dashboard/PartenairesServices';
import NewPersonnelGroupement from '../../Dashboard/PersonnelGroupement/NewPersonnelGroupement';
import PersonnelPharmacies from '../../Dashboard/PersonnelPharmacies/PersonnelPharmacies';
import FichePharmacie from '../../Dashboard/Pharmacies/FichePharmacie';
import Pharmacies from '../../Dashboard/Pharmacies/Pharmacie';
import PresidentsRegions from '../../Dashboard/PresidentsRegions/NewPersonnelGroupement';
import DetailsTitulaire from '../../Dashboard/TitulairesPharmacies/FicheTitulaire';
import NewTitulaireTable from '../../Dashboard/TitulairesPharmacies/NewTitulaire';
import { columns as ProduitColumns } from '../../Main/Content/OperationCommerciale/CreateOC/Produits/Produits';
import MainPartenaireLaboratoire from '../../Main/Content/PartenairesLaboratoires';
import Laboratoire from '../../Dashboard/Laboratoire';
import ActuColumns from '../../Main/Content/Pilotage/Cible/Columns/Actu';
import CommandeColumns from '../../Main/Content/Pilotage/Cible/Columns/Commande';
import LaboColumns from '../../Main/Content/Pilotage/Cible/Columns/Labo';
import OcColumns from '../../Main/Content/Pilotage/Cible/Columns/Oc';
import ProjectColumns from '../../Main/Content/Pilotage/Cible/Columns/Project';
import SuiviPublicitaireColumns from '../../Main/Content/Pilotage/Cible/Columns/SuiviPublicitaire';
import { pharmacieColumns } from '../../Main/Content/Pilotage/Cible/Steps/Pharmacie/Pharmacie';
import Produit from '../../Main/Content/Produits';
import { CustomFullScreenModal } from '../CustomModal';
import CustomContent from '../newCustomContent';
import {
  actualiteFilterParam,
  laboratoireFilterParam,
  operationFilterParam,
  produitCanalFilterParam,
  produitFilterParam,
  projectFilterParam,
} from './FilterParams';
import {
  actualiteSortParam,
  groupeClientSortParam,
  laboratoireSortParam,
  operationSortParam,
  produitCanalSortParam,
  produitSortParam,
  projectSortParam,
} from './SortParams';
import WithSearch from './withSearch';
import ICurrentPharmacieInterface from '../../../Interface/CurrentPharmacieInterface';
import { ME_me } from '../../../graphql/Authentication/types/ME';
import { ADMINISTRATEUR_GROUPEMENT, SUPER_ADMINISTRATEUR } from '../../../Constant/roles';
import { SEARCH_PARAMETRE_LABORATOIRES } from './../../../federation/search/query'

export const ProduitCanalFilterProps = {
  key: 'produitcanal',
  title: { icon: <StoreMallDirectory />, label: 'Catalogue des produits' },
  sortParams: produitCanalSortParam,
  filterParams: produitCanalFilterParam,
  placeholder: 'Rechercher un produit',
  sortCanal: true,
};

export const ProduitFilterProps = {
  key: 'produit',
  title: { icon: <StoreMallDirectory />, label: 'Gestion des produits' },
  sortParams: produitSortParam,
  filterParams: produitFilterParam,
  placeholder: 'Rechercher un produit',
  sortCanal: false,
};

export const CreateOcProduitFilterProps = {
  key: 'produitcanal',
  title: { label: "Création d'une opération commerciale" },
  sortParams: produitCanalSortParam,
  filterParams: produitCanalFilterParam,
  sortCanal: false,
};

export const LaboratoirePartenaireFilterProps = {
  key: 'laboratoirepartenaire',
  title: { label: 'Laboratoires' },
  sortParams: laboratoireSortParam,
  filterParams: laboratoireFilterParam,
  placeholder: 'Rechercher un laboratoire',
  sortCanal: false,
};

export const OperationFilterProps = {
  key: 'operation',
  title: { icon: <ShoppingBasket />, label: 'Opérations commerciales' },
  sortParams: operationSortParam,
  filterParams: operationFilterParam,
  placeholder: 'Rechercher une opération commerciale',
  sortCanal: false,
};

export const ActualiteFilterProps = {
  key: 'actualite',
  title: { icon: <Info />, label: 'Actualité' },
  sortParams: actualiteSortParam,
  filterParams: actualiteFilterParam,
  placeholder: 'Rechercher une actualité',
  sortCanal: false,
};

export const LaboratoireFilterProps = {
  key: 'laboratoire',
  title: { label: 'Laboratoire' },
  sortParams: laboratoireSortParam,
  filterParams: laboratoireFilterParam,
  placeholder: 'Rechercher un laboratoire',
  sortCanal: false,
};

export const ProjectFilterProps = {
  key: 'project',
  title: { label: 'Todo' },
  sortParams: projectSortParam,
  filterParams: projectFilterParam,
  placeholder: 'Rechercher un projet',
  sortCanal: false,
};

export const GestionClientFilterProps = {
  key: 'gestionclient',
  title: null,
  sortParams: groupeClientSortParam,
  filterParams: [],
  placeholder: 'Rechercher un projet',
  sortCanal: false,
};

export const CatalogueProduit = props => {
  const view = props && props.match && props.match.params && props.match.params.view;
  return (
    <WithSearch
      type="produitcanal"
      filterProps={ProduitCanalFilterProps}
      WrappedComponent={Produit}
      searchQuery={GET_SEARCH_PRODUIT_CANAL}
      optionalMust={[{ term: { isRemoved: false } }, { term: { isActive: true } }]}
      tableMode={view === 'list' ? true : false}
    />
  );
};

export const LaboratoirePartenaire = () => {
  return (
    <WithSearch
      type="laboratoire"
      WrappedComponent={MainPartenaireLaboratoire}
      filterProps={LaboratoirePartenaireFilterProps}
      searchQuery={GET_SEARCH_LABORATOIRE_PARTENAIRE}
      optionalMust={[]}
      fields={['nomLabo']}
    />
  );
};

export const GestionProduit = props => {
  const view = props && props.match && props.match.params && props.match.params.view;
  return (
    <WithSearch
      type="produit"
      filterProps={ProduitFilterProps}
      WrappedComponent={Produit}
      searchQuery={GET_SEARCH_GESTION_PRODUIT}
      optionalMust={[]}
    />
  );
};

export const CatalogueOperationProduit = props => {
  const idOperation = props && props.match && props.match.params && props.match.params.idOperation;
  const view = props && props.match && props.match.params && props.match.params.view;
  const idRemise = props && props.match && props.match.params && props.match.params.idRemise;
  return (
    <WithSearch
      type="produitcanal"
      WrappedComponent={Produit}
      filterProps={ProduitCanalFilterProps}
      searchQuery={
        view && view === 'list' ? GET_SEARCH_CUSTOM_CONTENT_PRODUIT : GET_SEARCH_PRODUIT_CANAL
      }
      optionalVariables={{ idRemiseOperation: idRemise }}
      optionalMust={[
        { term: { isRemoved: false } },
        { term: { isActive: true } },
        {
          terms: {
            'operations.id': [idOperation],
          },
        },
      ]}
    />
  );
};

export const CatalogueLaboratoireProduit = props => {
  const idLaboratoire = props?.match?.params?.idLaboratoire;
  const view = props?.match?.params?.view;
  return (
    <WithSearch
      type="produitcanal"
      WrappedComponent={Produit}
      searchQuery={
        view && view === 'list' ? GET_SEARCH_CUSTOM_CONTENT_PRODUIT : GET_SEARCH_PRODUIT_CANAL
      }
      optionalMust={[
        { term: { isRemoved: false } },
        { term: { isActive: true } },
        {
          terms: {
            'laboratoires.id': [idLaboratoire],
          },
        },
      ]}
    />
  );
};

export const CreateOperationProduit = () => {
  const props = {
    checkedItemsQuery: {
      name: 'checkedsProduit',
      type: 'produitcanal',
      query: GET_CHECKEDS_PRODUIT,
    },
    isSelectable: true,
    paginationCentered: true,
    columns: ProduitColumns,
  };
  return (
    <WithSearch
      type="produitcanal"
      props={props}
      WrappedComponent={CustomContent}
      searchQuery={GET_SEARCH_CUSTOM_CONTENT_PRODUIT}
      optionalMust={[{ term: { isRemoved: false } }, { term: { isActive: true } }]}
    />
  );
};

export const OperationCibleTable = props => {
  const groupement = getGroupement();
  const codeItemPrefixQuery = useQuery(GET_CODEITEM_PREFIX);
  const codeItemPrefix =
    codeItemPrefixQuery && codeItemPrefixQuery.data && codeItemPrefixQuery.data.codeItemPrefix;
  const childrenProps: any = {
    checkedItemsQuery: {
      name: 'checkedsOperation',
      type: 'operation',
      query: GET_CHECKEDS_OPERATION,
    },
    isSelectable: true,
    paginationCentered: true,
    columns: OcColumns,
  };
  const must = codeItemPrefix
    ? [
      { term: { isRemoved: false } },
      { term: { idGroupement: groupement && groupement.id } },
      codeItemPrefix && {
        prefix: { 'item.codeItem': codeItemPrefix },
      },
    ]
    : [{ term: { isRemoved: false } }, { term: { idGroupement: groupement && groupement.id } }];
  return (
    <WithSearch
      type={'operation'}
      props={childrenProps}
      WrappedComponent={CustomContent}
      searchQuery={GET_SEARCH_CUSTOM_CONTENT_OPERATION}
      optionalMust={must}
    />
  );
};

export const ActualiteCibleTable = props => {
  const groupement = getGroupement();
  const childrenProps: any = {
    checkedItemsQuery: {
      name: 'checkedsActualite',
      type: 'actualite',
      query: GET_CHECKEDS_ACTUALITE,
    },
    isSelectable: true,
    paginationCentered: true,
    columns: ActuColumns,
  };
  return (
    <WithSearch
      type={'actualite'}
      props={childrenProps}
      WrappedComponent={CustomContent}
      searchQuery={GET_SEARCH_CUSTOM_CONTENT_ACTUALITE}
      optionalMust={[
        { term: { isRemoved: false } },
        { term: { idGroupement: groupement && groupement.id } },
      ]}
    />
  );
};

export const LaboratoireCibleTable = props => {
  const childrenProps: any = {
    checkedItemsQuery: {
      name: 'checkedsLaboratoire',
      type: 'laboratoire',
      query: GET_CHECKEDS_LABORATOIRE,
    },
    isSelectable: true,
    paginationCentered: true,
    columns: LaboColumns,
  };
  return (
    <WithSearch
      type={'laboratoire'}
      props={childrenProps}
      WrappedComponent={CustomContent}
      searchQuery={GET_SEARCH_CUSTOM_CONTENT_LABORATOIRE}
    />
  );
};

export const CommandeCibleTable = props => {
  const childrenProps: any = {
    checkedItemsQuery: {
      name: 'checkedsCommande',
      type: 'commande',
      query: GET_CHECKEDS_COMMANDE,
    },
    isSelectable: true,
    paginationCentered: true,
    columns: CommandeColumns,
  };
  return (
    <WithSearch
      type={'commande'}
      props={childrenProps}
      WrappedComponent={CustomContent}
      searchQuery={GET_SEARCH_CUSTOM_CONTENT_COMMANDE}
      noFilter={true}
    />
  );
};

export const ProjectCibleTable = props => {
  const childrenProps: any = {
    checkedItemsQuery: {
      name: 'checkedsProject',
      type: 'project',
      query: GET_CHECKEDS_PROJECT,
    },
    isSelectable: true,
    paginationCentered: true,
    columns: ProjectColumns,
  };
  return (
    <WithSearch
      type={'project'}
      props={childrenProps}
      WrappedComponent={CustomContent}
      searchQuery={GET_SEARCH_CUSTOM_CONTENT_PROJECT}
    />
  );
};

export const PharmacieTable = props => {
  const [openModalTitulaire, setOpenModalTitulaire] = useState<boolean>(false);
  const [titulaireId, setTitulaireId] = useState<string>('');

  const [openModalPharmacie, setOpenModalPharmacie] = useState<boolean>(false);
  const [pharmacieId, setPharmacieId] = useState<string>('');

  const handleOpenTitulaireDetails = (id: string) => {
    setTitulaireId(id);
    setOpenModalTitulaire(true);
  };

  const handleOpenPharmacieDetails = (id: string) => {
    setPharmacieId(id);
    setOpenModalPharmacie(true);
  };

  const closeModalPharma = () => {
    setOpenModalPharmacie(false);
    // setPharmacieId('');
  };

  const { push, location } = props;

  const pathname = (location && location.pathname) || '';

  const isOnPilotage: boolean = pathname.includes('pilotage');

  const viewOnModal: boolean = isOnPilotage ? true : false;

  const childrenProps: any = {
    checkedItemsQuery: {
      name: 'checkedsPharmacie',
      type: 'pharmacie',
      query: GET_CHECKEDS_PHARMACIE,
    },
    isSelectable: true,
    paginationCentered: true,
    columns: pharmacieColumns(
      push,
      viewOnModal,
      handleOpenTitulaireDetails,
      handleOpenPharmacieDetails,
    ),
  };

  const groupement = getGroupement();

  const must = [
    { term: { idGroupement: groupement && groupement.id } },
    /* { term: { 'titulaires.sortie': 0 } }, */
  ];

  return (
    <Box width="100%">
      <WithSearch
        type={'pharmacie'}
        props={childrenProps}
        WrappedComponent={CustomContent}
        searchQuery={GET_SEARCH_CUSTOM_CONTENT_PHARMACIE}
        optionalMust={must}
      />
      <CustomFullScreenModal
        title="Fiche Titulaire"
        open={openModalTitulaire}
        setOpen={setOpenModalTitulaire}
        closeIcon={true}
        withBtnsActions={false}
        headerWithBgColor={true}
        fullScreen={true}
      >
        <DetailsTitulaire id={titulaireId} hideFooterActions={true} />
      </CustomFullScreenModal>
      <CustomFullScreenModal
        title="Détails de pharmacie"
        open={openModalPharmacie}
        setOpen={setOpenModalPharmacie}
        withBtnsActions={false}
        fullScreen={true}
        withHeader={false}
      >
        <FichePharmacie id={pharmacieId} inModalView={true} handleClickBack={closeModalPharma} />
      </CustomFullScreenModal>
    </Box>
  );
};

export const PharmacieDashboard = props => {
  const groupement = getGroupement();
  const must = [
    { term: { idGroupement: groupement && groupement.id } },
    { term: { sortie: 0 } },
    /* { term: { 'titulaires.sortie': 0 } }, */
  ];
  return (
    <WithSearch
      type={'pharmacie'}
      WrappedComponent={Pharmacies}
      searchQuery={GET_SEARCH_CUSTOM_CONTENT_PHARMACIE}
      optionalMust={must}
      {...{ props }}
    />
  );
};

export const TitulaireDashboard = props => {
  const titulaireUrl = '/db/titulaires-pharmacies';
  const titulairePharmacieUrl = '/db/titulaires-pharmacies/create';
  const onTitulaire =
    props &&
    props.location &&
    props.location.pathname &&
    props.location.pathname.startsWith(titulaireUrl) &&
    !props.location.pathname.startsWith(titulairePharmacieUrl);

  const currentUser: ME_me = getUser();

  const isAdmin =
    currentUser &&
    currentUser.role &&
    currentUser.role.code !== SUPER_ADMINISTRATEUR &&
    currentUser.role.code !== ADMINISTRATEUR_GROUPEMENT;

  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const myPharmacieId =
    myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie && myPharmacie.data.pharmacie.id;

  const groupement = getGroupement();
  const must = !isAdmin
    ? [{ term: { idGroupement: groupement && groupement.id } }, { term: { sortie: 0 } }]
    : [
      { term: { idGroupement: groupement && groupement.id } },
      { term: { sortie: 0 } },
      { term: { 'pharmacies.id': myPharmacieId } },
    ];
  return (
    <WithSearch
      type={'titulaire'}
      WrappedComponent={NewTitulaireTable}
      searchQuery={GET_SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE}
      {...{ props }}
      optionalMust={must}
      skipSearch={onTitulaire ? false : true}
    />
  );
};

export const PersonnelGroupementDashboard = props => {
  const isOnAffectation =
    props &&
    props.location &&
    props.location.pathname &&
    props.location.pathname.includes('/affectation');
  const groupement = getGroupement();
  const must = isOnAffectation ? [] : [{ term: { idGroupement: groupement && groupement.id } }];
  return (
    <WithSearch
      type={isOnAffectation ? 'personnelaffectation' : 'personnel'}
      WrappedComponent={NewPersonnelGroupement}
      searchQuery={
        isOnAffectation
          ? GET_SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION
          : GET_SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT
      }
      {...{ props }}
      optionalMust={must}
    />
  );
};

export const PersonnelPharmacieDashboard = () => {
  const groupement = getGroupement();

  const currentUser = getUser();

  const must: any[] = [
    { term: { idGroupement: groupement && groupement.id } },
    { term: { sortie: 0 } },
  ];

  if (
    currentUser &&
    currentUser.userTitulaire &&
    currentUser.userTitulaire.titulaire &&
    currentUser.userTitulaire.titulaire.pharmacies &&
    currentUser.userTitulaire.titulaire.pharmacies.length
  ) {
    currentUser.userTitulaire.titulaire.pharmacies.map(pharma =>
      must.push({
        term: { 'pharmacie.id': pharma.id },
      }),
    );
  }
  return (
    <WithSearch
      type={'ppersonnel'}
      WrappedComponent={PersonnelPharmacies}
      searchQuery={GET_SEARCH_CUSTOM_CONTENT_PPHARMACIE}
      optionalMust={must}
      defaultSort={[{ fullName: { order: 'asc' } }]}
    />
  );
};

export const PresidentRegionDashboard = props => {
  const isOnAffectation =
    props &&
    props.location &&
    props.location.pathname &&
    props.location.pathname.includes('/affectation');
  const now = moment().format();
  const groupement = getGroupement();
  const must = isOnAffectation
    ? []
    : [
      {
        range: {
          dateDebut: {
            lte: now,
          },
        },
      },
      {
        range: {
          dateFin: {
            gte: now,
          },
        },
      },
      { term: { idGroupement: groupement && groupement.id } },
    ];
  return (
    <WithSearch
      type={isOnAffectation ? 'titulaireaffectation' : 'titulaire'}
      WrappedComponent={PresidentsRegions}
      searchQuery={
        isOnAffectation
          ? GET_SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION
          : GET_SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE
      }
      optionalMust={must}
      {...{ props }}
    />
  );
};

export const LaboratoiresDashboard = () => {
  return (
    <WithSearch
      type={'laboratoires'}
      WrappedComponent={Laboratoire}
      useFederation={true}
      searchQuery={SEARCH_PARAMETRE_LABORATOIRES}
      optionalMust={[]}
      optionalShould={[
        { term: { sortie: 0 } },
        {
          bool: {
            must_not: [
              {
                exists: {
                  field: 'sortie',
                },
              },
            ],
          },
        },
      ]}
    />
  );
};

export const PartenaireServiceDashboard = () => {
  const groupement = getGroupement();
  const must = [
    { term: { idGroupement: groupement && groupement.id } },
    { term: { isRemoved: false } },
  ];
  return (
    <WithSearch
      type={'partenaire'}
      WrappedComponent={PartenairesServices}
      searchQuery={GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE}
      optionalMust={must}
    />
  );
};

export const GroupeClientDashboard = () => {
  // const groupement = getGroupement();
  const must = [{ term: { sortie: 0 } }];
  return (
    <WithSearch
      type="groupeclient"
      WrappedComponent={GroupeClient}
      searchQuery={GET_SEARCH_CUSTOM_CONTENT_GROUPE_CLIENT}
      optionalMust={must}
    // noFilter={true}
    />
  );
};

export const PilotageSuiviPublicitaire = () => {
  const childrenProps: any = {
    isSelectable: false,
    paginationCentered: true,
    columns: SuiviPublicitaireColumns,
  };
  return (
    <WithSearch
      type="suivipublicitaire"
      WrappedComponent={CustomContent}
      props={childrenProps}
      searchQuery={GET_SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIRE}
      optionalMust={[]}
      fetchPolicy="cache-and-network"
      unableRefetch={true}
    />
  );
};

export const CommandeTable = history => {
  const { push } = history;
  const groupement = getGroupement();
  const must = [{ term: { idGroupement: groupement && groupement.id } }];
  const childrenProps: any = {
    isSelectable: false,
    paginationCentered: true,
    columns: CommandeColumns(push),
  };
  return (
    <WithSearch
      type="commande"
      WrappedComponent={CustomContent}
      props={childrenProps}
      searchQuery={GET_SEARCH_CUSTOM_CONTENT_COMMANDE}
      optionalMust={must}
    // fetchPolicy="cache-and-network"
    />
  );
};
