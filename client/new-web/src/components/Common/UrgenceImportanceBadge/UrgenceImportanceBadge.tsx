import { useQuery } from '@apollo/client';
import { Box } from '@material-ui/core';
import React, { FC } from 'react';
import { GET_URGENCES } from '../../../graphql/Urgence/query';
import { URGENCES } from '../../../graphql/Urgence/types/URGENCES';
import CustomSelect from '../CustomSelect';
import { useStyles } from './styles';
import { LoaderSmall } from '@app/ui-kit';

const COLOR_ORDRE_MAP = {
  '1A': { color: '#E34168', order: 1 },
  '1B': { color: '#F46036', order: 2 },
  '1C': { color: '#FBB104', order: 3 },
  '2A': { color: '#00A745', order: 4 },
  '2B': { color: '#CB48B7', order: 5 },
  '2C': { color: '#63B8DD', order: 6 },
};

interface UrgenceImportanceBadgeProps {
  importanceOrdre: number;
  urgenceCode: string;
}
const UrgenceImportanceBadge: FC<UrgenceImportanceBadgeProps> = ({
  importanceOrdre,
  urgenceCode,
}) => {
  const classes = useStyles();
  const urgenceQuery = useQuery<URGENCES>(GET_URGENCES);
  const urgenceList = urgenceQuery?.data?.urgences || [];
  const urgence: any = urgenceList?.find((urgence: any) => urgence?.code === urgenceCode);
  console.log('+++ item : ', importanceOrdre, urgenceCode);

  const colorOrdre = COLOR_ORDRE_MAP[`${importanceOrdre}${urgenceCode}`];
  return (
    <Box
      display="flex"
      flexDirection="column"
      className={classes.root}
      style={{ backgroundColor: colorOrdre.color }}
    >
      {urgenceQuery.loading ? (
        <LoaderSmall />
      ) : (
        <>
          <Box>
            {`Importance`}
          </Box>
          <Box>
            {`${importanceOrdre === 1 ? 'Haute' : 'Moyenne'} ${urgence?.code === 'C' ? 'du' : 'de la'} ${urgence?.libelle?.toLowerCase()}`}
          </Box>
        </>
      )}
    </Box>
  );
};

export default UrgenceImportanceBadge;
