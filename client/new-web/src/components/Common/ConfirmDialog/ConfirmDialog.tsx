import React, { FC, ReactNode } from 'react';
import {
  Dialog,
  DialogContent,
  DialogActions,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core';
import styles from './styles';
import CustomButton from '../CustomButton';

interface ConfirmDialogProps {
  message: string | ReactNode;
  open: boolean;
  loading?: boolean;
  labelAgree?: string;
  labelDesagree?: string;
  handleClose: any;
  handleValidate: any;
  title?: string;
}

const ConfirmDialog: FC<ConfirmDialogProps> = ({
  open,
  message,
  loading,
  labelAgree,
  labelDesagree,
  handleClose,
  handleValidate,
  title,
}) => {
  const classes = styles();
  return (
    <Dialog open={open} onClose={handleClose} className={classes.root}>
      <DialogTitle className={classes.title} id="alert-dialog-title">
        {title || 'Confirmation'}
      </DialogTitle>
      <DialogContent className={classes.content}>
        <DialogContentText id="alert-dialog-description">{message}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <CustomButton onClick={handleClose} color="default" className={classes.btn}>
          {labelDesagree || 'Annuler'}
        </CustomButton>
        <CustomButton
          disabled={loading}
          onClick={handleValidate}
          color="secondary"
          className={classes.btn}
        >
          {labelAgree || 'Confirmer'}
        </CustomButton>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmDialog;
