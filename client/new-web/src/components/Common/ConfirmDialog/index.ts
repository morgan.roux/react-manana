import ConfirmDialog from './ConfirmDialog';
import ConfirmDeleteDialog from './ConfirmDeleteDialog';

export { ConfirmDeleteDialog };

export default ConfirmDialog;
