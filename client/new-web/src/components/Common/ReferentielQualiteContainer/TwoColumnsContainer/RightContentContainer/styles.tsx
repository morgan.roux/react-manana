import { Theme, lighten } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      height: '100%',
      width: '100%',
      fontFamily: 'Montserrat',
      backgroundColor: theme.palette.background.paper,
      '& .MuiPaper-root': {
        boxShadow: 'inherit!important',
      },
      '& hr': {
        margin: theme.spacing(0, 1),
        height: 1,
        backgroundColor: '#B1B1B1',
      },
    },

    /* FONT STYLE */
    MontserratRegular: {
      fontFamily: 'Montserrat',
      fontWeight: 400,
    },

    MontserratBold: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
    },
    RobotoRegular: {
      fontFamily: 'Roboto',
      fontWeight: 'normal',
    },
    headerResume: {
      backgroundColor: '#002F58',
      padding: '24px 44px',
      width: '100%',
    },
    titleHeader: {
      fontSize: '1.25rem',
      color: '#ffffff',
      marginBottom: 10,
    },
    paraphe: {
      fontSize: '0.75rem',
      color: '#B1B1B1',
      display: 'flex',
      alignContent: 'center',
      marginRight: 4,
      '& svg': {
        width: '0.75em',
        height: '0.75em',
        marginRight: 6,
      },
    },
    colorPink: {
      color: '#F11957',
    },
    fixedAppBarTab: {
      '& .MuiPaper-elevation4': {
        position: 'fixed',
      },
      /*'& div[role="tabpanel"]': {
        paddingTop: 44,
      },*/
      '& .content-tab-pannel ': {
        paddingTop: 44,
        '& div#nav-tabpanel-0': {
          height: 'calc(100vh - 303px)',
          overflowY: 'auto',
        },
        '& div#nav-tabpanel-1': {
          height: 'calc(100vh - 347px)',
          overflowY: 'auto',
        },
      },
    },
    contentTab: {
      backgroundColor: '#002F58',
      padding: '0 0px 0 46px',
      '& .MuiTabs-fixed': {
        backgroundColor: '#002F58',
      },
      '& .MuiButtonBase-root': {
        flexGrow: 'inherit',
        flexBasis: 'inherit',
        minWidth: 220,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        minHeight: 48,
      },
      '& .MuiTab-textColorInherit': {
        opacity: 1,
      },
      '& .MuiTab-wrapper': {
        display: 'flex',
        flexDirection: 'row',
        color: '#ffffff',
        fontSize: '0.75rem',
        textTransform: 'initial',
        '& .MuiSvgIcon-root': {
          marginRight: 8,
        },
      },
      '& .Mui-selected': {
        background: '#ffffff',
        '& .MuiTab-wrapper': {
          color: '#002444',
          fontSize: '0.75rem',
          textTransform: 'initial',
        },
      },
      '& .MuiTabs-indicator': {
        display: 'none',
      },
    },
    contenuTab: {},
    seeContentPdf: {
      height: 'calc(100vh - 351px)',
    },
    libelle: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 22,
      marginBottom: 24,
    },
    btnPasserCommande: {
      textTransform: 'none',
      marginRight: 8,
    },
    description: {
      fontFamily: 'Roboto',
      fontSize: 16,
      textAlign: 'justify',
    },
    buttonContainer: {
      display: 'flex',
      flexDirection: 'row',
      marginBottom: 25,
    },
  }),
);

export default useStyles;
