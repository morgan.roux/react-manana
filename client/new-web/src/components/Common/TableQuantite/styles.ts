import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    ruptureText: {
      fontSize: 14,
      fontWeight: 'bold',
      fontFamily: 'Roboto',
      textAlign: 'center',
    },
    panierListeRemiseQuantite: {
      border: '1px solid',
      borderColor: '#EFEFEF',
      display: 'flex',
      flexDirection: 'row',
      borderRadius: '4px',
      height: 30,
      backgroundColor: theme.palette.common.white,
    },
    quantiteButton: {
      // color: theme.palette.green.main,
    },
    quantiteTextField: {
      borderLeft: '1px solid',
      borderLeftColor: '#EFEFEF',
      borderRight: '1px solid',
      borderRightColor: '#EFEFEF',
      width: '48px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& > div > input': {
        textAlign: 'center',
        fontSize: '0.875rem',
        fontFamily: 'Roboto',
        fontWeight: 'bold',
      },
    },
    quantiteSaveButton: {
      borderLeft: '1px solid',
      borderLeftColor: '#EFEFEF',
    },
    underline: {
      '&&&:before': {
        borderBottom: 'none',
      },
      '&&:after': {
        borderBottom: 'none',
      },
    },
    inputFont: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
    },
    blueButton: {
      color: '#004354',
    },
  }),
);
