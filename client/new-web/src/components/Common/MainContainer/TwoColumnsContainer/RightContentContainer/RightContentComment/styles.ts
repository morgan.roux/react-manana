import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    hrHeight: {
      height: '1px !important',
      background: '#E0E0E0',
    },
  }),
);

export default useStyles;
