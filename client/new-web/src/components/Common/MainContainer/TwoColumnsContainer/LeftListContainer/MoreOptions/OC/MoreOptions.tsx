import React, { FC, Fragment, useState, MouseEvent } from 'react';
// import { useStyles } from './styles';
import MoreOptionsComponent from '../../../../../MoreOptions';
import { MoreOptionsItem } from '../../../../../MoreOptions/MoreOptions';
import { ConfirmDeleteDialog } from '../../../../../ConfirmDialog';
import { useMutation } from '@apollo/client';
import {
  deleteOperation,
  deleteOperationVariables,
} from '../../../../../../../graphql/OperationCommerciale/types/deleteOperation';
import {
  DO_DELETE_OPERATION,
  DO_MARK_OPERATION_AS_NOT_SEEN,
} from '../../../../../../../graphql/OperationCommerciale/mutation';
import {
  SEARCH as SEARCH_Interface,
  SEARCHVariables,
} from '../../../../../../../graphql/search/types/SEARCH';
import { SEARCH as SEARCH_QUERY } from '../../../../../../../graphql/search/query';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { stopEvent } from '../../../../../../../services/DOMEvent';
import {
  markAsNotSeenOperation,
  markAsNotSeenOperationVariables,
} from '../../../../../../../graphql/OperationCommerciale/types/markAsNotSeenOperation';
import { ME_me } from '../../../../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../../../../services/LocalStorage';
import Backdrop from '../../../../../Backdrop';
import { AppAuthorization } from '../../../../../../../services/authorization';
interface MoreOptionsProps {
  url?: string;
  active?: boolean;
  currentItem?: any;
  setCurrentItem?: (id: string | null) => void;
  listResult?: any;
}

const MoreOptions: FC<MoreOptionsProps & RouteComponentProps> = ({
  history,
  url,
  active,
  currentItem,
  listResult,
  setCurrentItem,
}) => {
  // const classes = useStyles({});
  const [openConfirmDialog, setOpenConfirmDialog] = useState(false);
  const [openBackdrop, setOpenBackdrop] = useState(false);
  const filterVariables = {
    skip: 0,
    take: 1000,
    query: {
      query: {
        bool: { must: { range: { countOperationsCommercials: { gte: 0 } } } },
      },
    },
    sortBy: [{ countOperationsCommercials: { order: 'desc' } }],
  };

  const user: ME_me = getUser();

  const idPharmacieUser =
    (user &&
      user.userPpersonnel &&
      user.userPpersonnel.pharmacie &&
      user.userPpersonnel.pharmacie.id) ||
    (user &&
      user.userTitulaire &&
      user.userTitulaire.titulaire &&
      user.userTitulaire.titulaire.pharmacieUser &&
      user.userTitulaire.titulaire.pharmacieUser.id);

  const auth = new AppAuthorization(user);

  const [doMarkAsNotSeen, doMarkAsNotSeenResult] = useMutation<
    markAsNotSeenOperation,
    markAsNotSeenOperationVariables
  >(DO_MARK_OPERATION_AS_NOT_SEEN, {
    variables: {
      id: currentItem && currentItem.id,
      idPharmacieUser: idPharmacieUser ? idPharmacieUser : null,
      userId: user && user.id,
    },
    update: (cache, { data }) => {
      if (data && data.markAsNotSeenOperation && data.markAsNotSeenOperation.id) {
        const req = cache.readQuery<SEARCH_Interface, SEARCHVariables>({
          query: SEARCH_QUERY,
          variables: listResult && listResult.variables,
        });
        if (req && req.search && req.search.data) {
          if (active && setCurrentItem) {
            window.history.pushState(null, '', `#${url}`);
            setCurrentItem(null);
          }
          cache.writeQuery({
            query: SEARCH_QUERY,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.map((item: any) => {
                    if (
                      item &&
                      data.markAsNotSeenOperation &&
                      item.id === data.markAsNotSeenOperation.id
                    ) {
                      item.seen = data.markAsNotSeenOperation.seen;
                      item.nombrePharmaciesConsultes =
                        data.markAsNotSeenOperation.nombrePharmaciesConsultes;
                      return item;
                    }
                  }),
                },
              },
            },
            variables: listResult && listResult.variables,
          });
        }
      }
    },
  });

  const [doDeleteOperation, doDeleteOperationResult] = useMutation<
    deleteOperation,
    deleteOperationVariables
  >(DO_DELETE_OPERATION, {
    variables: { id: currentItem && currentItem.id },
    update: (cache, { data }) => {
      if (data && data.deleteOperation && listResult && listResult.variables) {
        const req = cache.readQuery<SEARCH_Interface, SEARCHVariables>({
          query: SEARCH_QUERY,
          variables: listResult.variables,
        });

        if (
          req &&
          req.search &&
          req.search.data &&
          data.deleteOperation &&
          data.deleteOperation.id
        ) {
          if (active && setCurrentItem) {
            window.history.pushState(null, '', `#${url}`);
            setCurrentItem(null);
          }
          cache.writeQuery({
            query: SEARCH_QUERY,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.filter(
                    (item: any) => currentItem && item.id !== currentItem.id,
                  ),
                },
              },
            },
            variables: listResult.variables,
          });

          // update commandecanal filter cache

          const canalCommande = cache.readQuery<SEARCH_Interface, SEARCHVariables>({
            query: SEARCH_QUERY,
            variables: { type: ['commandecanal'], ...filterVariables },
          });
          if (canalCommande && canalCommande.search && canalCommande.search.data) {
            cache.writeQuery({
              query: SEARCH_QUERY,
              data: {
                search: {
                  ...canalCommande.search,
                  ...{
                    data: canalCommande.search.data.map((item: any) => {
                      if (
                        item &&
                        currentItem &&
                        currentItem.commandeCanal &&
                        item.id === currentItem.commandeCanal.id
                      ) {
                        item.countOperationsCommercials = item.countOperationsCommercials - 1;
                        return item;
                      } else return item;
                    }),
                  },
                },
              },
              variables: { type: ['commandecanal'], ...filterVariables },
            });
          }

          // update commandetype filter cache

          const commandeType = cache.readQuery<SEARCH_Interface, SEARCHVariables>({
            query: SEARCH_QUERY,
            variables: { type: ['commandetype'], ...filterVariables },
          });
          if (commandeType && commandeType.search && commandeType.search.data) {
            cache.writeQuery({
              query: SEARCH_QUERY,
              data: {
                search: {
                  ...commandeType.search,
                  ...{
                    data: commandeType.search.data.map((item: any) => {
                      if (
                        item &&
                        currentItem &&
                        currentItem.commandeType &&
                        item.id === currentItem.commandeType.id
                      ) {
                        item.countOperationsCommercials = item.countOperationsCommercials - 1;
                        return item;
                      } else return item;
                    }),
                  },
                },
              },
              variables: { type: ['commandetype'], ...filterVariables },
            });
          }
        }
      }
    },
    onCompleted: () => {
      // Close Backdrop
      setOpenBackdrop(false);
      setOpenConfirmDialog(false);
    },
  });
  const openDialog = (event: MouseEvent<any>) => {
    stopEvent(event);
    setOpenConfirmDialog(true);
  };

  const deleteOC = () => {
    doDeleteOperation();
  };

  const editOperation = () => {
    history.push(`/edit${url}/${currentItem.id}`);
  };

  const setAsNotRead = () => {
    if (active && setCurrentItem) {
      setCurrentItem(null);
    }
    doMarkAsNotSeen();
  };

  const defaultMoreOptionsItems: MoreOptionsItem[] = [
    { title: 'Supprimer', onClick: openDialog, disabled: !auth.isAuthorizedToDeleteOC() },
    { title: 'Modifier', onClick: editOperation, disabled: !auth.isAuthorizedToEditOC() },
  ];
  const moreOptionsItems: MoreOptionsItem[] =
    currentItem && currentItem.seen
      ? [
        ...defaultMoreOptionsItems,
        {
          title: 'Marquer comme non lue',
          onClick: setAsNotRead,
          disabled: !auth.isAuthorizedToMarkSeenOrNotSeenOC(),
        },
      ]
      : defaultMoreOptionsItems;

  if (doDeleteOperationResult.loading || doMarkAsNotSeenResult.loading) {
    return <Backdrop />;
  }
  return (
    <Fragment>
      <MoreOptionsComponent items={moreOptionsItems} active={active} />
      <ConfirmDeleteDialog
        title={`Suppression de l'opération ${currentItem.libelle}`}
        content="Etes-vous sûr de vouloir supprimer ?"
        open={openConfirmDialog}
        setOpen={setOpenConfirmDialog}
        onClickConfirm={deleteOC}
      />
    </Fragment>
  );
};

export default withRouter(MoreOptions);
