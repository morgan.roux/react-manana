import { makeStyles, Theme, createStyles, lighten } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      padding: theme.spacing(3, 2),
      border: '3px solid #fff',
      borderRadius: 6,
    },
    header: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'flex-end',
      '& .MuiTypography-root': {
        fontSize: 14,
        color: theme.palette.secondary.main,
        fontWeight: 'bold',
      },
    },
    active: {
      borderWidth: 3,
      borderRadius: 6,
      borderStyle: 'solid',
      borderColor: theme.palette.primary.main,
      color: theme.palette.common.black,
    },
    seen: {
      background: theme.palette.common.white,
      color: theme.palette.common.black,
    },
    notSeen: {
      background: lighten(theme.palette.secondary.main, 0.3),
      color: theme.palette.common.black,
    },
    SubHeader: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingBottom: theme.spacing(3),
      '& .MuiTypography-root.title': {
        fontSize: 16,
        color: '#212121',
        fontWeight: 'bold',
        paddingRight: theme.spacing(1),
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        '-webkit-box-orient': 'vertical',
        '-webkit-line-clamp': 1,
        maxWidth: 176,
      },
      '& .MuiTypography-root': {
        fontSize: 12,
        color: '#9E9E9E',
        fontWeight: 400,
      },
    },

    SubHeaderActive: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingBottom: theme.spacing(3),
      '& .MuiTypography-root.title': {
        fontSize: 16,
        color: theme.palette.common.white,
        fontWeight: 'bold',
        paddingRight: theme.spacing(1),
      },
      '& .MuiTypography-root': {
        fontSize: 12,
        color: '#9E9E9E',
        fontWeight: 400,
      },
    },

    infoListContainer: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'flex-end',
      '& .MuiTypography-root.key': {
        color: '#616161',
        display: 'flex',
        alignItems: 'center',
        fontSize: 12,
        paddingRight: theme.spacing(1),
        fontWeight: 400,
      },
      '& .MuiTypography-root': {
        fontSize: 14,
        color: '#616161',
        fontWeight: 'bold',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        '-webkit-box-orient': 'vertical',
        '-webkit-line-clamp': 1,
        maxWidth: 96,
      },
    },
    dateTimeContainer: {
      marginTop: 8,
      fontFamily: 'Roboto',
      fontSize: 12,
      display: 'flex',
    },
    dateTimeDefaultFont: {
      marginLeft: 4,
      fontFamily: 'Roboto',
      fontSize: 12,
      color: '#616161',
    },
    dateTimeDefaultFontActive: {
      marginLeft: 4,
      fontFamily: 'Roboto',
      fontSize: 12,
      color: '#616161',
    },
    dateTimeBoldFont: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 14,
    },
    iconListLabel: {
      marginLeft: 8,
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 14,
      color: '#616161',
    },
    iconListLabelActive: {
      marginLeft: 8,
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 14,
      color: '#616161',
    },
  }),
);
