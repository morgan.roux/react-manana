import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
const drawerWidth = 328;
export const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: 'flex',
      scrollbarColor: theme.palette.secondary.main,
      width: '100%',
    },
    drawer: {
      position: 'sticky',
      top: 0,
      width: drawerWidth,
      flexShrink: 0,
      height: 'calc(100vh - 86px)',
      borderRight: '1px solid #E5E5E5',
    },
    leftListContainer: {
      minWidth: 326,
      borderRight: '1px solid #E5E5E5',
    },
    rightContainer: {
      width: '100%',
      height: 'calc(100vh - 86px)',
      overflowY: 'auto',
    },
    leftList: {
      width: '100%',
      // borderRight: '2px solid #E5E5E5',
      height: 'calc(100vh - 163px)',
      overflowY: 'auto',
    },
    appBar: {
      display: 'block',
      marginTop: 86,
      zIndex: 1,
      [theme.breakpoints.up('lg')]: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        display: 'none',
      },
    },
    toolbarTitle: {
      color: theme.palette.common.white,
      fontSize: 30,
      textAlign: 'center',
      width: '100%',
      fontWeight: 'bold',
    },
    menuButton: {
      marginRight: theme.spacing(2),
      position: 'absolute',
      [theme.breakpoints.up('lg')]: {
        display: 'none',
      },
    },
  }),
);
