import * as React from 'react';
import { Typography } from '@material-ui/core';
import useStyles from '../../Dashboard/TitulairesPharmacies/stepper/Step1/styles';

interface IInfoPersoFormProps {
  title: string;
}

const FormContainer: React.FC<IInfoPersoFormProps> = props => {
  const { title, children } = props;
  const classes = useStyles({});
  return (
    <div className={classes.infoPersoContainer}>
      <Typography className={classes.inputTitle}>{title}</Typography>
      <div className={classes.inputsContainer}>{children}</div>
    </div>
  );
};

export default FormContainer;
