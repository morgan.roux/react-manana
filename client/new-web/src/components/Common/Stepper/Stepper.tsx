import React, {
  FC,
  useEffect,
  useState,
  Dispatch,
  SetStateAction,
  ReactNode,
  Fragment,
} from 'react';
import useStyles from './styles';
import {
  Stepper as MUIStepper,
  StepLabel,
  Step as MUIStep,
  Typography,
  Box,
  IconButton,
} from '@material-ui/core';
import classnames from 'classnames';
import CustomButton from '../CustomButton';
import { useQuery, useApolloClient } from '@apollo/client';
import { GET_CLICKED_CIBLE, GET_SHOW_SUB_SIDEBAR } from '../../../graphql/Actualite/local';
import { ArrowBack } from '@material-ui/icons';
import { resetSearchFilters } from '../withSearch/withSearch';

export interface Step {
  title: string;
  content: JSX.Element;
  pageTitle?: string;
}

export interface StepperProps {
  title: string;
  steps: Step[];
  disableNextBtn: boolean;
  disablePrevBtn?: boolean;
  fullWidth?: boolean;
  finished?: boolean;
  noFinalStepNext?: boolean;
  resume?: JSX.Element;
  loading?: boolean;
  where?: string;
  finalStep?: any;
  history?: any;
  backBtnText?: string;
  activeStepFromProps?: number;
  setActiveStepFromProps?: Dispatch<SetStateAction<number | undefined>>;
  children?: ReactNode;
  onConfirm?: () => void;
  backToHome?: () => void;
  onClickNext?: () => void;
  onClickPrev?: () => void;
  unResetFilter?: boolean;
}

const Stepper: FC<StepperProps> = ({
  steps,
  backToHome,
  disableNextBtn,
  disablePrevBtn,
  fullWidth,
  finished,
  onConfirm,
  noFinalStepNext,
  resume,
  loading,
  where,
  title,
  finalStep,
  history,
  backBtnText,
  onClickNext,
  onClickPrev,
  // activeStepFromProps,
  setActiveStepFromProps,
  children,
  unResetFilter,
}) => {
  const classes = useStyles({});
  const [activeStep, setActiveStep] = useState(0);
  const client = useApolloClient();

  const getShowSubSidebar = useQuery<{ showSubSidebar: boolean }>(GET_SHOW_SUB_SIDEBAR);
  const showSubSidebar =
    getShowSubSidebar && getShowSubSidebar.data && getShowSubSidebar.data.showSubSidebar;

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
    if (setActiveStepFromProps) setActiveStepFromProps(activeStep + 1);
    if (onClickNext) onClickNext();
    if (!unResetFilter) resetSearchFilters(client);
  };

  const handleBack = () => {
    if (activeStep > 0) {
      setActiveStep(prevActiveStep => prevActiveStep - 1);
      if (setActiveStepFromProps) setActiveStepFromProps(activeStep - 1);
      if (onClickPrev) onClickPrev();
      if (!unResetFilter) resetSearchFilters(client);
    }
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const hideSubSidebar = () => {
    client.writeQuery({ query: GET_SHOW_SUB_SIDEBAR, data: { showSubSidebar: false } });
    client.writeQuery({ query: GET_CLICKED_CIBLE, data: { clickedCible: undefined } });
  };

  const fromPath =
    history && history.location && history.location.state && history.location.state.from;

  useEffect(() => {
    if (fromPath) {
      handleReset();
    }
  }, [fromPath]);

  return (
    <div className={fullWidth ? classnames(classes.root, classes.fullWidth) : classes.root}>
      <Box className={classes.stepperHeader}>
        <Box display="flex" alignItems="center">
          {backBtnText ? (
            <CustomButton
              variant="text"
              color="inherit"
              startIcon={<ArrowBack />}
              onClick={backToHome ? backToHome : showSubSidebar ? hideSubSidebar : handleBack}
              style={{ textTransform: 'none' }}
            >
              {backBtnText}
            </CustomButton>
          ) : (
            <IconButton
              color="inherit"
              onClick={backToHome ? backToHome : showSubSidebar ? hideSubSidebar : handleBack}
            >
              <ArrowBack />
            </IconButton>
          )}
        </Box>
        <Typography className={classes.title}>{title}</Typography>
        {noFinalStepNext && steps.length - 1 === activeStep ? null : finalStep &&
          activeStep === steps.length - 1 ? (
          <Box className={classes.btnActionsContainer}>
            <CustomButton
              disabled={(activeStep === 0 && !backToHome) || disablePrevBtn}
              onClick={
                activeStep === 0 && backToHome
                  ? backToHome
                  : showSubSidebar
                  ? hideSubSidebar
                  : handleBack
              }
            >
              Retour
            </CustomButton>
            <CustomButton
              color="secondary"
              onClick={finalStep && finalStep.action}
              disabled={(finalStep && finalStep.loading) || disableNextBtn}
            >
              {finalStep && finalStep.buttonLabel}
            </CustomButton>
          </Box>
        ) : activeStep === 0 ? (
          <CustomButton color="secondary" onClick={handleNext} disabled={disableNextBtn}>
            {finished ? 'Confirmer mes sélections' : 'Suivant'}
          </CustomButton>
        ) : activeStep === steps.length - 1 ? (
          <CustomButton
            disabled={(activeStep === 0 && !backToHome) || disablePrevBtn}
            onClick={
              activeStep === 0 && backToHome
                ? backToHome
                : showSubSidebar
                ? hideSubSidebar
                : handleBack
            }
          >
            Retour
          </CustomButton>
        ) : (
          <Box className={classes.btnActionsContainer}>
            <CustomButton
              disabled={(activeStep === 0 && !backToHome) || disablePrevBtn}
              onClick={
                activeStep === 0 && backToHome
                  ? backToHome
                  : showSubSidebar
                  ? hideSubSidebar
                  : handleBack
              }
            >
              Retour
            </CustomButton>
            <CustomButton color="secondary" onClick={handleNext} disabled={disableNextBtn}>
              {finished ? 'Confirmer mes sélections' : 'Suivant'}
            </CustomButton>
          </Box>
        )}
      </Box>
      <MUIStepper activeStep={activeStep} alternativeLabel={true} className={classes.stepperRoot}>
        {steps.map(step => (
          <MUIStep key={step.title}>
            <StepLabel>{step.title}</StepLabel>
          </MUIStep>
        ))}
      </MUIStepper>
      {children}
      <Box width="100%" className={classes.stepperContainer}>
        <div className={classes.content}>
          {activeStep === steps.length ? (
            <Fragment>
              {resume && resume}
              <div className={classes.btnActionsContainer}>
                <CustomButton
                  fullWidth={true}
                  onClick={where === 'inCreateActualite' ? handleBack : handleReset}
                >
                  Retour
                </CustomButton>
                <CustomButton
                  fullWidth={true}
                  color="secondary"
                  onClick={onConfirm ? onConfirm : () => {}}
                  disabled={loading ? true : false}
                >
                  {loading ? 'Chargement ...' : 'Confirmer'}
                </CustomButton>
              </div>
            </Fragment>
          ) : (
            <div className={classes.instructions}>
              {steps && steps.length > 0 && steps[activeStep].content}
            </div>
          )}
        </div>
      </Box>
    </div>
  );
};

// activeStep === steps.length - 1

export default Stepper;
