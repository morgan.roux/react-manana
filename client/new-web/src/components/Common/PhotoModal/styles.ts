import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      height: 'auto',
      width: 'auto',
    },
    flexToolbar: {
      display: 'flex',
      alignItems: 'center',
      minHeight: 60,
      justifyContent: 'space-between',
      background: theme.palette.primary.main,
      color: theme.palette.common.white,
    },
    closeIcon: {
      marginLeft: theme.spacing(3),
    },
  }),
);
export default useStyles;
