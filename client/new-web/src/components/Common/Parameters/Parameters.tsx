import React, { useState, ReactNode, FC, useEffect, useCallback } from 'react';
import useStyles from './style';
import EditIcon from '@material-ui/icons/Edit';
import {
  Button,
  InputAdornment,
  IconButton,
  TextField,
  Switch,
  FormControl,
  Select,
  MenuItem,
  Collapse,
} from '@material-ui/core';
import classnames from 'classnames';
import SearchIcon from '@material-ui/icons/Search';
import { ParametersList } from './ParametersList';
import ParameterListItem from './ParametersList/ParameterListItem';
import useParameterForm from './ParameterForm/useParameterForm';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import { useMutation, useApolloClient, useLazyQuery } from '@apollo/client';
import { capitalize } from '../../../utils/Helpers';
import { DO_UPDATE_PARAMETER_VALUES } from '../../../graphql/Parametre/mutation';
import {
  UPDATE_PARAMETER_VALUES,
  UPDATE_PARAMETER_VALUESVariables,
} from '../../../graphql/Parametre/types/UPDATE_PARAMETER_VALUES';
import { ParameterInfo } from '../../../graphql/Parametre/types/ParameterInfo';
import { useParameters } from '../../../utils/getValueParameter';
import { ParamCategory, TypeFichier } from '../../../types/graphql-global-types';
import UserSettingsForm from '../../Dashboard/UserSettings/UserSettingsForm';
import {
  IDENTIFIER_PREFIX,
  DIRECTORY,
  ImageTypes,
} from '../../Dashboard/UserSettings/UserSettings';
import { ME, ME_me } from '../../../graphql/Authentication/types/ME';
import { GET_ME } from '../../../graphql/Authentication/query';
import { GROUPEMENT_AVATARS_groupementAvatars as AvatarType } from '../../../graphql/Avatars/types/GROUPEMENT_AVATARS';
import { AWS_HOST } from '../../../utils/s3urls';
import {
  UPDATE_USER_PHOTO,
  UPDATE_USER_PHOTOVariables,
} from '../../../graphql/User/types/UPDATE_USER_PHOTO';
import { DO_UPDATE_USER_PHOTO, DO_DELETE_USER_PHOTO } from '../../../graphql/User/mutation';
import { formatFilename } from '../Dropzone/Dropzone';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../graphql/S3';
import { uploadToS3 } from '../../../services/S3';
import SnackVariableInterface from '../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import { setUser, getUser } from '../../../services/LocalStorage';
import Backdrop from '../Backdrop';
import {
  CODE_LIGNE_TABLEAU,
  CODE_THEME,
  THEMES_LIST,
  CODE_NOTIF_TIMERS,
  CODE_NOTIF_TAGS,
  NOTIF_TAGS,
  NOTIF_TIMERS,
} from '../../../Constant/parameter';
import { result, find, last } from 'lodash';
import { ADMINISTRATEUR_GROUPEMENT, SUPER_ADMINISTRATEUR } from '../../../Constant/roles';
import CustomButton from '../CustomButton';
import {
  DELETE_USER_PHOTO,
  DELETE_USER_PHOTOVariables,
} from '../../../graphql/User/types/DELETE_USER_PHOTO';
import { ConfirmDeleteDialog } from '../ConfirmDialog';
import { CustomModal } from '../CustomModal';
import { CustomReactEasyCrop } from '../CustomReactEasyCrop';
import { AppAuthorization } from '../../../services/authorization';
import CustomSelect from '../CustomSelect';
import PartenaireInput from '../PartenaireInput';
import { DO_GET_PARAMETER_LOCAL } from '../../../graphql/Parametre/local';

interface ParameterGroupInterface {
  children: ReactNode;
  title: string;
}

interface ParameterValueInterface {
  id: string;
  value: string;
}

interface ParametersProps {
  withSearchInput?: boolean;
  baseUrl: string;
  where: string;
}

export interface ParameterStateInterface {
  [key: string]: any;
}

export const formatString = (str: string) => {
  return str
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .replace(/[^a-zA-Z]/g, '');
};

const sortByCodes = (a: any, b: any) => {
  if (a.code < b.code) return -1;
  if (a.code > b.code) return 1;

  return 0

}

const Parameters: FC<RouteComponentProps & ParametersProps> = ({
  history,
  location: { pathname },
  withSearchInput,
  baseUrl,
  where,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [isEditable, setIsEditable] = useState<boolean>(false);
  const [showOthersParameters, setShowOthersParameters] = useState<boolean>(false);
  const [defaultState, setDefaultState] = useState<ParameterStateInterface>({});
  const [loading, setLoading] = useState<boolean>(false);
  const [confirmDelete, setConfirmDelete] = useState<boolean>(false);
  const [openResize, setOpenResize] = useState<boolean>(false);

  const [getMe, { data: meData, loading: meLoading }] = useLazyQuery<ME>(GET_ME, {
    fetchPolicy: 'no-cache',
  });
  const [me, setMe] = useState<any>(null);
  const [avatarChemin, setAvatarChemin] = useState<string>('');

  const currentUser: ME_me = getUser();
  const [files, setFiles] = useState<File[]>([]);
  const [croppedFiles, setCroppedFiles] = useState<File[]>([]);
  const [imageType, setImageType] = useState<ImageTypes>(ImageTypes.SELECT);
  const [savedCroppedImage, setSavedCroppedImage] = useState<boolean>(false);

  const [selectedAvatar, setSelectedAvatar] = useState<AvatarType | undefined>(undefined);

  const [fileAlreadySet, setFileAlreadySet] = useState<string | undefined>(undefined);

  const auth = new AppAuthorization(currentUser);

  const isTitulaire = !!currentUser.userTitulaire;
  const isOnOptionsGroupement: boolean = pathname === '/db/options-groupement';

  const disabledEditBtn = (): boolean => {
    if (isOnOptionsGroupement && !auth.isAuthorizedToEditGroupementOptions()) {
      return true;
    }
    return false;
  };

  /**
   * Update fileAlreadySet and imageType on load page
   */
  useEffect(() => {
    if (avatarChemin && avatarChemin !== fileAlreadySet && files.length === 0) {
      setFileAlreadySet(`${AWS_HOST}/${avatarChemin}`);
    }

    if (avatarChemin && imageType !== ImageTypes.IMPORT) {
      setImageType(ImageTypes.IMPORT);
    }
  }, [avatarChemin, fileAlreadySet, files]);

  // const avatarsQuery = useQuery<GROUPEMENT_AVATARS>(GET_GROUPEMENT_AVATARS);
  // const avatars = avatarsQuery.data;
  // const avatarsLoading = avatarsQuery.loading;

  const userImage = files && files.length > 0 && last(files);
  // const userImageSrc = fileAlreadySet || (userImage && URL.createObjectURL(userImage));

  const [userPhotoSrc, setUserPhotoSrc] = useState<any>(null);

  const [previewUrl, setPreviewUrl] = useState<any>('');

  const userImageToS3 = croppedFiles && croppedFiles.length > 0 ? last(croppedFiles) : userImage;

  // Get parameters from Cache
  let parametersList = useParameters();

  if (where === 'inUserSettings') {
    parametersList =
      parametersList &&
      parametersList.filter(
        (parameter: ParameterInfo) =>
          parameter && parameter.category && parameter.category === ParamCategory.USER,
      );
  }

  if (where === 'inDashboardPharmacie') {
    parametersList =
      parametersList &&
      parametersList.filter(
        (parameter: ParameterInfo) =>
          parameter && parameter.category && parameter.category === ParamCategory.PHARMACIE,
      );
  }

  if (where === 'inDashboard') {
    parametersList =
      parametersList &&
      parametersList.filter(
        (parameter: ParameterInfo) =>
          parameter &&
          parameter.category &&
          (parameter.category === ParamCategory.GROUPEMENT ||
            parameter.category === ParamCategory.GROUPEMENT_SUPERADMIN),
      );
  }

  /**
   * Execute getMe
   */
  useEffect(() => {
    if (where === 'inUserSettings') {
      getMe();
    }
  }, [where]);

  const isSuperAdmin =
    currentUser && currentUser.role && currentUser.role.code === SUPER_ADMINISTRATEUR;

  const isAdminGrp =
    currentUser && currentUser.role && currentUser.role.code === ADMINISTRATEUR_GROUPEMENT;

  let parametersWithGroup =
    (currentUser &&
      ((currentUser.userTitulaire && currentUser.userTitulaire.id) ||
        (currentUser.userPpersonnel && currentUser.userPpersonnel.id))) ||
    (where !== 'inUserSettings' && (isSuperAdmin || isAdminGrp))
      ? parametersList &&
        parametersList.filter(
          (parameter: any) => parameter && parameter.parameterGroupe.id !== null,
        )
      : parametersList &&
        parametersList.filter(
          (parameter: any) =>
            parameter &&
            parameter.parameterGroupe.id !== null &&
            parameter.parameterGroupe.code &&
            parameter.parameterGroupe.code !== 'MESSAGERIE',
        );

  if (!isSuperAdmin) {
    parametersWithGroup =
      parametersWithGroup &&
      parametersWithGroup.filter(
        (parameter: any) =>
          parameter && parameter.parameterGroupe && parameter.parameterGroupe.code !== 'SSO',
      );
  }

  const parametersWithoutGroup =
    parametersList &&
    parametersList.filter((parameter: any) => parameter && parameter.parameterGroupe.id === null);

  const filteredParameterGroup =
    parametersWithGroup &&
    parametersWithGroup.reduce(
      (prev: Array<ParameterInfo | null> | null | undefined, current: ParameterInfo | null) => {
        if (prev && current) {
          const x = prev.find(
            (item: ParameterInfo | null) =>
              item &&
              item.parameterGroupe &&
              current.parameterGroupe &&
              item.parameterGroupe.id === current.parameterGroupe.id,
          );
          if (!x) {
            return prev.concat([current]);
          } else {
            return prev;
          }
        }
      },
      [],
    );

  const [doUpdateParameterValues, doUpdateParameterValuesResult] = useMutation<
    UPDATE_PARAMETER_VALUES,
    UPDATE_PARAMETER_VALUESVariables
  >(DO_UPDATE_PARAMETER_VALUES, {
    onCompleted: ({ updateParameterValues }) => {
      if (updateParameterValues && updateParameterValues.length > 0) {
        updateDefaultState(updateParameterValues);
        client.writeQuery({
          query: DO_GET_PARAMETER_LOCAL,
          data: { parameters: updateParameterValues },
        });
        redirectAfterSave();
      }
    },
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const [doCreatePutPresignedUrl, doCreatePutPresignedUrlResult] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (
        data &&
        data.createPutPresignedUrls &&
        data.createPutPresignedUrls.length > 0 &&
        data.createPutPresignedUrls[0] &&
        userImageToS3
      ) {
        const photo = data.createPutPresignedUrls[0];
        if (photo && photo.filePath && photo.presignedUrl) {
          setLoading(true);
          await uploadToS3(userImageToS3, photo.presignedUrl)
            .then(result => {
              if (result && result.status === 200) {
                updateUserPhoto({
                  variables: {
                    userPhoto: {
                      chemin: photo.filePath,
                      nomOriginal: userImageToS3.name,
                      type: TypeFichier.PHOTO,
                    },
                  },
                });
              }
            })
            .catch(error => {
              console.error(error);
              setLoading(false);
              const snackBarData: SnackVariableInterface = {
                type: 'ERROR',
                message:
                  "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
                isOpen: true,
              };
              displaySnackBar(client, snackBarData);
            });
        }
      }
    },
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const [updateUserPhoto, updateUserPhotoResult] = useMutation<
    UPDATE_USER_PHOTO,
    UPDATE_USER_PHOTOVariables
  >(DO_UPDATE_USER_PHOTO, {
    update: (cache, { data }) => {
      if (data && data.updateUserPhoto) {
        const req = cache.readQuery<ME>({ query: GET_ME });
        if (req && req.me) {
          cache.writeQuery({
            query: GET_ME,
            data: {
              me: { ...req.me, userPhoto: updateUserPhoto },
            },
          });
        }
      }
    },
    onCompleted: data => {
      if (
        data &&
        data.updateUserPhoto &&
        data.updateUserPhoto.fichier &&
        data.updateUserPhoto.fichier.chemin
      ) {
        getMe();
        setLoading(false);
        if (files && files.length > 0) {
          saveNewParams();
        } else {
          redirectAfterSave();
        }
      }
    },
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  /**
   * Update user
   */
  useEffect(() => {
    if (meData && meData.me) {
      setMe(meData.me);
      setUser(meData.me);
    }

    if (
      meData &&
      meData.me &&
      meData.me.userPhoto &&
      meData.me.userPhoto.fichier &&
      meData.me.userPhoto.fichier.chemin
    ) {
      setAvatarChemin(meData.me.userPhoto.fichier.chemin);
    }
  }, [meData]);

  const redirectAfterSave = () => {
    toggleIsEditable();
    history.push(pathname.replace('/edit', ''));
  };

  const saveNewParams = () => {
    // Save parameters
    const newParameters: ParameterValueInterface[] = [];

    if (parametersList && parametersList.length > 0) {
      parametersList.map((parameter: ParameterInfo) => {
        if (parameter && parameter.name) {
          const pName = formatString(parameter.name);
          let val = values[pName];

          if (val === true || val === false) val = val.toString();
          const newValue = {
            id: parameter.id,
            value: val,
          };

          newParameters.push(newValue);
        }
      });
    }
    // Do mutation
    doUpdateParameterValues({
      variables: { parameters: newParameters },
    });
  };

  const saveUserPhoto = () => {
    // Update userPhoto
    if (userImageToS3 && imageType === ImageTypes.IMPORT) {
      const filePath = formatFilename(userImageToS3, DIRECTORY, IDENTIFIER_PREFIX);
      doCreatePutPresignedUrl({ variables: { filePaths: [filePath] } });
    }

    if (
      imageType === ImageTypes.SELECT &&
      selectedAvatar &&
      selectedAvatar.fichier &&
      selectedAvatar.fichier.chemin
    ) {
      updateUserPhoto({
        variables: {
          userPhoto: {
            chemin: selectedAvatar.fichier.chemin,
            nomOriginal: 'Avatar Prédéfini',
            type: TypeFichier.PHOTO,
          },
        },
      });
    }
  };

  const submit = () => {
    setLoading(true);
    if (files && files.length === 0) {
      saveNewParams();
    }
    saveUserPhoto();
  };

  const { values, handleChangeSelect, handleChange, handleSwitch } = useParameterForm(
    submit,
    defaultState,
  );

  // const toggleIsEditable = () => {
  //   setIsEditable(!isEditable);
  // };

  const toggleIsEditable = useCallback(() => {
    setIsEditable(!isEditable);
  }, []);

  const toggleOthersParameters = () => {
    setShowOthersParameters(!showOthersParameters);
  };

  const edit = () => {
    toggleIsEditable();
    history.push({ pathname: `${baseUrl}/edit` });
  };

  const cancel = () => {
    toggleIsEditable();
    history.push({ pathname: baseUrl });
  };

  const updateDefaultState = (parameters: Array<ParameterInfo | null> | null | undefined) => {
    if (parameters && parameters.length > 0) {
      parameters.map((param: ParameterInfo | null) => {
        if (param && param.name) {
          const name = formatString(param.name);
          const value = (param.value && param.value.value) || param.defaultValue;
          setDefaultState((prevState: any) => {
            prevState[name] = value;
            return prevState;
          });
        }
      });
    }
  };

  useEffect(() => {
    if (pathname === `${baseUrl}/edit`) toggleIsEditable();
  }, [pathname]);

  const ParameterGroup: FC<ParameterGroupInterface> = ({ children, title }) => {
    return (
      <div className={classes.parameterGroup}>
        <div className={classes.parameterGroupTitle}>{title}</div>
        <div className={classes.parameterGroupContent}>{children}</div>
      </div>
    );
  };

  const renderParameterValue = (
    parameterValue: any,
    parameterType: string | null,
    parameterCode?: string,
    options?: any,
  ) => {
    let value = parameterValue;
    if (typeof value === 'boolean') value = value.toString();
    if (value === 'true') value = 'Activé';
    if (value === 'false') value = 'Désactivé';
    if (parameterType && parameterType === 'MONEY') value = `${value} £`;

    switch (parameterCode) {
      case CODE_THEME:
        value = result(find(THEMES_LIST, ['value', value]), 'libelle');
        break;
      case CODE_NOTIF_TAGS:
        value = result(find(NOTIF_TAGS, ['value', value]), 'libelle');
        break;
      case CODE_NOTIF_TIMERS:
        value = result(find(NOTIF_TIMERS, ['value', value]), 'libelle');
        break;
      default:
        break;
    }
    if (options) {
      return getSelectLabel(parameterValue, options);
    }
    return value || '-';
  };

  const nom: string =
    (me && me.userPartenaire && me.userPartenaire.nom) ||
    (me && me.userLaboratoire && me.userLaboratoire.nomLabo) ||
    (me && me.userPersonnel && me.userPersonnel.nom) ||
    (me && me.userPpersonnel && me.userPpersonnel.nom) ||
    (me && me.userTitulaire && me.userTitulaire.titulaire && me.userTitulaire.titulaire.nom) ||
    (me && me.userName) ||
    'Non renseigné';
  const prenom: string =
    (me && me.userPersonnel && me.userPersonnel.prenom) ||
    (me && me.userPpersonnel && me.userPpersonnel.prenom) ||
    (me && me.userTitulaire && me.userTitulaire.titulaire && me.userTitulaire.titulaire.prenom) ||
    'Non renseigné';
  const anneeNaissance = (me && me.anneeNaissance) || 'Non renseign';

  const userInfos: any[] = [
    { label: 'Nom', value: nom },
    { label: 'Prénom', value: prenom },
    { label: 'Année de naissance', value: anneeNaissance },
    { label: 'Sexe', value: 'Non renseigné' },
    { label: 'Téléphone', value: (me && me.phoneNumber) || 'Non renseigné' },
    { label: 'Email', value: (me && me.email) || 'Non renseigné' },
  ];

  const [doDeleteUserPhoto] = useMutation<DELETE_USER_PHOTO, DELETE_USER_PHOTOVariables>(
    DO_DELETE_USER_PHOTO,
    {
      update: (cache, { data }) => {
        if (data && data.deleteUserPhoto) {
          const req = cache.readQuery<ME>({ query: GET_ME });
          if (req && req.me) {
            cache.writeQuery({
              query: GET_ME,
              data: {
                me: { ...req.me, userPhoto: null },
              },
            });
          }
        }
      },
      onCompleted: data => {
        if (data && data.deleteUserPhoto) {
          getMe();
          setFileAlreadySet(undefined);
          const snackBarData: SnackVariableInterface = {
            type: 'SUCCESS',
            message: 'Votre avatar a été supprimeé avec succès',
            isOpen: true,
          };
          displaySnackBar(client, snackBarData);
        }
      },
      onError: errors => {
        errors.graphQLErrors.map(error => {
          if (error.message) {
            const snackBarData: SnackVariableInterface = {
              type: 'ERROR',
              message: error.message,
              isOpen: true,
            };
            displaySnackBar(client, snackBarData);
          }
        });
      },
    },
  );

  const openConfirmDelete = () => {
    setConfirmDelete(true);
  };

  const handleDeleteUserPhoto = () => {
    setConfirmDelete(false);
    if (avatarChemin) {
      doDeleteUserPhoto({ variables: { chemin: avatarChemin } });
    }
  };

  const handleClickResize = useCallback(() => {
    setOpenResize(true);
    // Reset crop
    setSavedCroppedImage(false);
    setPreviewUrl(null);
  }, []);

  /**
   * Reset preview
   */
  useEffect(() => {
    if (files && files.length > 0) {
      setPreviewUrl(null);
      // Open Resize
      handleClickResize();
    }
  }, [files]);

  const saveResizedImage = () => {
    setOpenResize(false);
    setSavedCroppedImage(true);
  };

  /**
   * Set set userPhotoSrc
   */
  useEffect(() => {
    const image = files && files.length > 0 && last(files);
    const photo = previewUrl || (image && URL.createObjectURL(image));
    if (photo) {
      setUserPhotoSrc(photo);
    }
  }, [previewUrl, files]);

  const getSelectLabel = (value: string, options: any) => {
    const option = options.find(optionItem => optionItem.value === value);
    return option && option.label;
  };

  return (
    <>
      <div className={classes.parameterRoot}>
        {meLoading && <Backdrop />}
        {(loading === true ||
          doUpdateParameterValuesResult.loading ||
          updateUserPhotoResult.loading ||
          doCreatePutPresignedUrlResult.loading) && <Backdrop value="Enregistrement en cours..." />}
        <div className={classes.head}>
          <div className={classes.headText}>Configuration de votre compte</div>
          <div className={classes.headerActions}>
            {isEditable ? (
              <div className={classes.headerActionsBtnEdit}>
                <CustomButton
                  color="default"
                  onClick={cancel}
                  className={classnames(classes.btn, classes.headerActionsBtnCancel)}
                >
                  ANNULER
                </CustomButton>
                <CustomButton
                  color="secondary"
                  onClick={submit}
                  className={classnames(classes.btn)}
                >
                  SAUVEGARDER
                </CustomButton>
              </div>
            ) : (
              <div className={classes.headerActionsBtnEdit}>
                <CustomButton color="secondary" disabled={disabledEditBtn()} onClick={edit}>
                  <EditIcon /> Modifier
                </CustomButton>
              </div>
            )}
          </div>
        </div>
        <div className={classes.containerParameter}>
          <div className={classes.parameterMainContent}>
            {withSearchInput && (
              <div className={classes.searchContainer}>
                <TextField
                  className={classes.textField}
                  variant="outlined"
                  placeholder="Rechercher"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton>
                          <SearchIcon />
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </div>
            )}

            {/* User setting */}
            {where === 'inUserSettings' && (
              <div className={classes.parameterContainer}>
                <UserSettingsForm
                  setFilesOut={setFiles}
                  withImagePreview={true}
                  withImagePreviewCustomized={true}
                  fileAlreadySetUrl={fileAlreadySet || userPhotoSrc}
                  setFileAlreadySet={setFileAlreadySet}
                  submit={submit}
                  imageType={imageType}
                  setImageType={setImageType}
                  // avatars={avatars && avatars.groupementAvatars ? avatars.groupementAvatars : []}
                  // avatarsLoading={avatarsLoading}
                  avatarSelected={selectedAvatar}
                  setAvatarSelected={setSelectedAvatar}
                  selectedFiles={files}
                  setSelectedFiles={setFiles}
                  onClickDelete={openConfirmDelete}
                  onClickResize={handleClickResize}
                  previewUrl={previewUrl}
                />

                <ParameterGroup title="Vos informations">
                  <ParametersList>
                    {userInfos.map(info => {
                      const labelId = `checkbox-list-label-${info.label}`;
                      return (
                        <ParameterListItem key={info.label} text={capitalize(info.label)}>
                          <div className={classes.parameterValue}>{info.value}</div>
                        </ParameterListItem>
                      );
                    })}
                  </ParametersList>
                </ParameterGroup>
              </div>
            )}

            <div className={classes.parameterContainer}>
              {filteredParameterGroup &&
                filteredParameterGroup.map((group: any) => {
                  if (group && group.parameterGroupe && group.parameterGroupe.nom) {
                    return (
                      <ParameterGroup
                        key={group.parameterGroupe.id}
                        title={group.parameterGroupe.nom}
                      >
                        <ParametersList>
                          {parametersWithGroup &&
                            parametersWithGroup.sort(sortByCodes).map((parameter: any) => {
                              if (
                                parameter &&
                                parameter.parameterGroupe &&
                                group.parameterGroupe &&
                                parameter.parameterGroupe.code === group.parameterGroupe.code
                              ) {
                                const paramName = formatString(parameter.name || '');
                                const valueFromState =
                                  values && values[`${formatString(paramName)}`];
                                let paramValue: string | boolean | null | undefined =
                                  valueFromState ||
                                  (parameter.value && parameter.value.value) ||
                                  parameter.defaultValue;
                                if (paramValue === 'true') paramValue = true;
                                if (paramValue === 'false') paramValue = false;
                                return (
                                  <ParameterListItem
                                    key={parameter.id}
                                    text={capitalize(parameter.name || '')}
                                  >
                                    {isEditable ? (
                                      parameter.code === CODE_NOTIF_TAGS ? (
                                        <FormControl
                                          variant="outlined"
                                          className={classes.textField}
                                        >
                                          <Select
                                            name={paramName}
                                            value={paramValue}
                                            onChange={handleChangeSelect}
                                            MenuProps={{
                                              getContentAnchorEl: null,
                                              anchorOrigin: {
                                                vertical: 'bottom',
                                                horizontal: 'left',
                                              },
                                            }}
                                          >
                                            {NOTIF_TAGS.map(item => (
                                              <MenuItem
                                                key={`notif_tags_item_${item.value}`}
                                                value={item.value}
                                              >
                                                {item.libelle}
                                              </MenuItem>
                                            ))}
                                          </Select>
                                        </FormControl>
                                      ) : parameter.code === CODE_NOTIF_TIMERS ? (
                                        <FormControl
                                          variant="outlined"
                                          className={classes.textField}
                                        >
                                          <Select
                                            name={paramName}
                                            value={paramValue}
                                            onChange={handleChangeSelect}
                                            MenuProps={{
                                              getContentAnchorEl: null,
                                              anchorOrigin: {
                                                vertical: 'bottom',
                                                horizontal: 'left',
                                              },
                                            }}
                                          >
                                            {NOTIF_TIMERS.map(item => (
                                              <MenuItem
                                                key={`notif_timers_item_${item.value}`}
                                                value={item.value}
                                              >
                                                {item.libelle}
                                              </MenuItem>
                                            ))}
                                          </Select>
                                        </FormControl>
                                      ) : parameter.code === CODE_LIGNE_TABLEAU ? (
                                        <FormControl
                                          variant="outlined"
                                          className={classes.textField}
                                        >
                                          <Select
                                            name={paramName}
                                            value={paramValue}
                                            onChange={handleChangeSelect}
                                            MenuProps={{
                                              getContentAnchorEl: null,
                                              anchorOrigin: {
                                                vertical: 'bottom',
                                                horizontal: 'left',
                                              },
                                            }}
                                          >
                                            <MenuItem value="12">12</MenuItem>
                                            <MenuItem value="25">25</MenuItem>
                                            <MenuItem value="50">50</MenuItem>
                                            <MenuItem value="100">100</MenuItem>
                                            <MenuItem value="250">250</MenuItem>
                                            <MenuItem value="500">500</MenuItem>
                                            <MenuItem value="1000">1000</MenuItem>
                                            <MenuItem value="2000">2000</MenuItem>
                                          </Select>
                                        </FormControl>
                                      ) : parameter.code === CODE_THEME ? (
                                        <FormControl
                                          variant="outlined"
                                          className={classes.textField}
                                        >
                                          <Select
                                            name={paramName}
                                            value={paramValue}
                                            onChange={handleChangeSelect}
                                            MenuProps={{
                                              getContentAnchorEl: null,
                                              anchorOrigin: {
                                                vertical: 'bottom',
                                                horizontal: 'left',
                                              },
                                            }}
                                          >
                                            {THEMES_LIST.map(item => (
                                              <MenuItem
                                                key={`theme_item_${item.value}`}
                                                value={item.value}
                                              >
                                                {item.libelle}
                                              </MenuItem>
                                            ))}
                                          </Select>
                                        </FormControl>
                                      ) : parameter.parameterType &&
                                        parameter.parameterType.code === 'BOOLEAN' ? (
                                        <Switch
                                          className={classes.switch}
                                          onChange={handleSwitch}
                                          name={paramName}
                                          defaultChecked={
                                            typeof paramValue === 'boolean' ? paramValue : undefined
                                          }
                                          // checked={typeof paramValue === 'boolean' ? paramValue : undefined}
                                        />
                                      ) : parameter.parameterType.code === 'TIME' ? (
                                        <TextField
                                          variant="outlined"
                                          placeholder=""
                                          type="time"
                                          name={paramName}
                                          defaultValue={paramValue}
                                          onChange={handleChange}
                                          className={classes.textField}
                                          InputLabelProps={{
                                            shrink: true,
                                          }}
                                          inputProps={{
                                            step: 300, // 5 min
                                          }}
                                        />
                                      ) : parameter.parameterType.code === 'SELECT' ? (
                                        <FormControl
                                          variant="outlined"
                                          className={classes.textField}
                                        >
                                          <Select
                                            name={paramName}
                                            value={paramValue}
                                            onChange={handleChangeSelect}
                                            MenuProps={{
                                              getContentAnchorEl: null,
                                              anchorOrigin: {
                                                vertical: 'bottom',
                                                horizontal: 'left',
                                              },
                                            }}
                                          >
                                            {parameter &&
                                              parameter.options &&
                                              parameter.options.map(option => (
                                                <MenuItem
                                                  key={`option-select-${option.value}`}
                                                  value={option.value}
                                                >
                                                  {option.label}
                                                </MenuItem>
                                              ))}
                                          </Select>
                                        </FormControl>
                                      ) :
                                        parameter.parameterType.code === 'PRESTATAIRE_SERVICE' || parameter.parameterType.code === 'LABORATOIRE' ? (
                                          <FormControl
                                            variant="outlined"
                                            className={classes.textField}
                                          >
                                            <PartenaireInput index={parameter.parameterType.code === 'PRESTATAIRE_SERVICE' ? "partenaire" : 'laboratoire'} defaultValue={paramValue} onChange={(newPartenaire) => handleChange({
                                              target: {
                                                name: paramName,
                                                value: newPartenaire?.id
                                              }
                                            } as any)} />
                                          </FormControl>

                                        ) : (
                                          <TextField
                                            variant="outlined"
                                            placeholder=""
                                            name={paramName}
                                            defaultValue={paramValue}
                                            onChange={handleChange}
                                            className={classes.textField}
                                          />
                                        )
                                    ) : (
                                      <div className={classes.parameterValue}>
                                        {renderParameterValue(
                                          paramValue,
                                          parameter &&
                                            parameter.parameterType &&
                                            parameter.parameterType.code,
                                          parameter.code,
                                          parameter.options,
                                        )}
                                      </div>
                                    )}
                                  </ParameterListItem>
                                );
                              }
                            })}
                        </ParametersList>
                      </ParameterGroup>
                    );
                  }
                })}

              {/* Autre parametre */}
              {parametersWithoutGroup && parametersWithoutGroup.length > 0 && (
                <div className={classes.othersParametersContainer}>
                  <Button className={classes.othersParametersBtn} onClick={toggleOthersParameters}>
                    Autres paramètres
                    {showOthersParameters ||
                    !(filteredParameterGroup && filteredParameterGroup.length) ? (
                      <ArrowDropUpIcon />
                    ) : (
                      <ArrowDropDownIcon />
                    )}
                  </Button>
                  <Collapse
                    in={
                      showOthersParameters ||
                      !(filteredParameterGroup && filteredParameterGroup.length)
                    }
                    timeout="auto"
                    unmountOnExit={true}
                  >
                    <div>
                      <ParametersList>
                        {parametersWithoutGroup &&
                          parametersWithoutGroup.map((parameter: any) => {
                            if (parameter) {
                              const paramName = formatString(parameter.name || '');
                              const valueFromState = values && values[`${formatString(paramName)}`];
                              let paramValue: string | boolean | null | undefined =
                                valueFromState ||
                                (parameter.value && parameter.value.value) ||
                                parameter.defaultValue;
                              if (paramValue === 'true') paramValue = true;
                              if (paramValue === 'false') paramValue = false;
                              return (
                                <ParameterListItem
                                  key={parameter.id}
                                  text={capitalize(parameter.name || '')}
                                >
                                  {isEditable ? (
                                    parameter.parameterType &&
                                    parameter.parameterType.code === 'BOOLEAN' ? (
                                      <Switch
                                        className={classes.switch}
                                        onChange={handleSwitch}
                                        name={paramName}
                                        defaultChecked={
                                          typeof paramValue === 'boolean' ? paramValue : undefined
                                        }
                                        // checked={typeof paramValue === 'boolean' ? paramValue : undefined}
                                      />
                                    ) : parameter.parameterType.code === 'TIME' ? (
                                      <TextField
                                        variant="outlined"
                                        placeholder=""
                                        type="time"
                                        name={paramName}
                                        defaultValue={paramValue}
                                        onChange={handleChange}
                                        className={classes.textField}
                                        InputLabelProps={{
                                          shrink: true,
                                        }}
                                        inputProps={{
                                          step: 300, // 5 min
                                        }}
                                      />
                                    ) : (
                                      <TextField
                                        variant="outlined"
                                        placeholder=""
                                        name={paramName}
                                        defaultValue={paramValue}
                                        onChange={handleChange}
                                        className={classes.textField}
                                      />
                                    )
                                  ) : (
                                    <div className={classes.parameterValue}>
                                      {renderParameterValue(
                                        paramValue,
                                        parameter &&
                                          parameter.parameterType &&
                                          parameter.parameterType.code,
                                      )}
                                    </div>
                                  )}
                                </ParameterListItem>
                              );
                            }
                          })}
                      </ParametersList>
                    </div>
                  </Collapse>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>

      <ConfirmDeleteDialog
        open={confirmDelete}
        setOpen={setConfirmDelete}
        title="Suppression avatar"
        content="Vous êtes sûre de vouloir supprimer votre avatar ?"
        onClickConfirm={handleDeleteUserPhoto}
      />
      <CustomModal
        open={openResize}
        setOpen={setOpenResize}
        title="Redimensionnement"
        onClickConfirm={saveResizedImage}
        actionButton="Enregistrer"
        closeIcon={true}
        centerBtns={true}
      >
        {userPhotoSrc ? (
          <CustomReactEasyCrop
            src={userPhotoSrc}
            withZoom={true}
            croppedImage={previewUrl}
            setCroppedImage={setPreviewUrl}
            setCropedFiles={setCroppedFiles}
            savedCroppedImage={savedCroppedImage}
          />
        ) : (
          <div>Aucune Image</div>
        )}
      </CustomModal>
    </>
  );
};

Parameters.defaultProps = {
  withSearchInput: true,
  baseUrl: '/db/options-groupement',
  where: 'inDashboard',
};

export default withRouter(Parameters);
