import React, { FC, useState, useEffect, useMemo, useContext } from 'react';
import ListIcon from '@material-ui/icons/List';
import ListAltIcon from '@material-ui/icons/ListAlt';
import classnames from 'classnames';
import { useToolbarStyles } from './styles';
import { Typography, Box } from '@material-ui/core';
import ReplayIcon from '@material-ui/icons/Replay';
import { TableToolbarProps } from '../interfaces';
import { useApolloClient, useLazyQuery } from '@apollo/client';
import { GET_LOCAL_SORT, resetSearchFilters } from '../../withSearch/withSearch';
import { SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT } from '../../../../Constant/roles';
import { ME_me } from '../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../services/LocalStorage';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { Refresh, Delete, Tune } from '@material-ui/icons';
import CustomButton from '../../CustomButton';
import { CustomCheckbox } from '../../CustomCheckbox';
import { GET_MINIMAL_SEARCH_ID } from '../../../../graphql/MinimalSearch/query';
import Backdrop from '../../Backdrop';
import {
  MINIMAL_SEARCH_IDVariables,
  MINIMAL_SEARCH_ID,
} from '../../../../graphql/MinimalSearch/types/MINIMAL_SEARCH_ID';
import { CustomModal } from '../../CustomModal';
import { ContentContext, ContentStateInterface } from '../../../../AppContext';
import PharmacieFilter from '../../newCustomContent/Filters/PharmacieFilter';
import PersonnelGroupementFilter from '../../../Common/newCustomContent/Filters/PersonnelGroupementFilter';
import TitulaireFilter from '../../newCustomContent/Filters/TitulaireFilter';
import PersonnelAffectationFilter from '../../newCustomContent/Filters/PersonnelAffectationFilter';
import LaboratoireFilter from '../../newCustomContent/Filters/LaboratoireFilter';
import { GET_SKIP_AND_TAKE } from '../../newWithSearch/withSearch';
import { GET_ID_CHECKEDS } from '../../newWithSearch/queryBuilder';

const EnhancedTableToolbar: FC<TableToolbarProps & RouteComponentProps> = ({
  checkedItems,
  listResult,
  clearSelection,
  setShowCheckeds,
  allTotal,
  showCheckeds,
  checkedItemsQuery,
  selected,
  setSelected,
  showCheckedElementOrResult = true,
  showCheckedAll = true,
  showResetFilters = true,
  showRefreshTable = true,
  location: { pathname },
}) => {
  const classes = useToolbarStyles();
  const client = useApolloClient();
  const currentUser: ME_me = getUser();
  const isAdmin =
    currentUser &&
    currentUser.role &&
    (currentUser.role.code === SUPER_ADMINISTRATEUR ||
      currentUser.role.code === ADMINISTRATEUR_GROUPEMENT);

  const dashboardPharmacieUrl = '/db/pharmacies';
  const operationPharmacieUrl = '/pharmacie';
  const personnelGroupementUrl = '/db/personnel-groupement/list';
  const personnelAffectationUrl = '/db/personnel-groupement/affectation';
  const titulaireUrl = '/db/titulaires-pharmacies';
  const titulairePharmacieUrl = '/db/titulaires-pharmacies/create';
  const gestionClientUrl = '/db/groupes-clients';
  const laboratoireUrl = '/laboratoire';
  const onPharmacie =
    pathname.startsWith(dashboardPharmacieUrl) ||
    pathname.includes(operationPharmacieUrl) ||
    pathname.startsWith(`${gestionClientUrl}/create`) ||
    pathname.startsWith(`${gestionClientUrl}/edit`);

  const onPersonnelGroupement = pathname.startsWith(personnelGroupementUrl);
  const onPersonnelAffectation = pathname.startsWith(personnelAffectationUrl);
  const onTitulaire =
    pathname.startsWith(titulaireUrl) && !pathname.startsWith(titulairePharmacieUrl);
  const onPharmacieTitulaire = pathname.startsWith(titulairePharmacieUrl);
  const onLaboratoire = pathname.includes(laboratoireUrl);
  const type = listResult && listResult.variables && listResult.variables.type;

  const [checkedAll, setCheckedAll] = useState<boolean>(
    (checkedItems && allTotal > 0 && checkedItems.length === allTotal) || false,
  );

  const [openFilter, setOpenFilter] = useState<boolean>(false);

  const [minimalData, setMinimalData] = useState<any[]>([]);

  const [getAllItems, { data: allData, loading }] = useLazyQuery<
    MINIMAL_SEARCH_ID,
    MINIMAL_SEARCH_IDVariables
  >(GET_MINIMAL_SEARCH_ID, {
    variables: { type, query: listResult && listResult.variables && listResult.variables.query },
    onCompleted: data => {
      const results = data && data.search && data.search.data;
      setMinimalData(results || []);
      if (selected && setSelected) {
        const newSelected = results ? results : [];
        setSelected(newSelected);
        //setCheckedAll(true);
        return;
      }

      if (results && checkedItemsQuery) {
        // TODO: Migration
        /* (client as any).writeData({
           data: {
             [checkedItemsQuery.name]: results,
           },
         });*/
        //setCheckedAll(true);
      }
    },
    fetchPolicy: selected ? 'network-only' : 'cache-first',
  });

  useEffect(() => {
    if (checkedItems && checkedItems.length > 0 && checkedItems.length === minimalData.length) {
      setCheckedAll(true);
    } else {
      setCheckedAll(false);
    }
  }, [checkedItems]);

  useEffect(() => {
    if (selected) {
      client.writeQuery({
        query: GET_ID_CHECKEDS,
        data: {
          idCheckeds: showCheckeds ? selected && selected.map(item => item && item.id) : [],
        },
      });
      return;
    }
    client.writeQuery({
      query: GET_ID_CHECKEDS,
      data: {
        idCheckeds: showCheckeds ? checkedItems && checkedItems.map(item => item && item.id) : [],
      },
    });
  }, [showCheckeds]);

  const total =
    (listResult && listResult.data && listResult.data.search && listResult.data.search.total) || 0;

  const isActualite: boolean =
    window.location.hash === '#/' || window.location.hash.startsWith('#/actualite');
  const isOperation: boolean = window.location.hash.startsWith('#/operations-commerciales');
  const dataType: string = isActualite ? 'actualite' : isOperation ? 'operation' : '';

  const handleResetFields = () => {
    setFieldsState({});
    setContent({
      page,
      type,
      searchPlaceholder,
      rowsPerPage,
      contextQuery: null,
    });
  };

  const handleResetFilter = () => {
    setFieldsState({});
    setContent({
      page,
      type,
      searchPlaceholder,
      rowsPerPage,
      contextQuery: null,
    });
    resetSearchFilters(client, isAdmin, dataType);
  };

  const handleCheckAll = () => {
    setCheckedAll(prev => !prev);
    if (type && !checkedAll) {
      getAllItems();
    }
    if (selected && setSelected) {
      if (selected.length === allTotal) {
        setSelected([]);
      }
      console.log('minimalData :>> ', minimalData);
      return;
    } else {
      if (checkedItemsQuery) {
        const results = allData && allData.search && allData.search.data;
        // TODO: Migration
        /*(client as any).writeData({
          data: {
            [checkedItemsQuery.name]: checkedAll ? null : results,
          },
        });*/
      }
    }
  };

  const handleOpenFilter = () => {
    setOpenFilter(true);
  };

  const handleShowCheckeds = () => {
    client.writeQuery({
      query: GET_SKIP_AND_TAKE,
      data: {
        skipAndTake: {
          skip: 0,
          take: 12,
        },
      },
    });
    setShowCheckeds(!showCheckeds);
  };

  const onClickRefresh = () => {
    if (listResult && listResult.refetch) {
      listResult.refetch();
    }
  };

  const {
    content: { page, rowsPerPage, searchPlaceholder },
    setContent,
  } = useContext<ContentStateInterface>(ContentContext);

  const [fieldsState, setFieldsState] = useState<any>({});

  const handleFieldChange = (event: any): void => {
    setFieldsState({
      ...fieldsState,
      [event.target.name]:
        event.target.name === 'actived' || event.target.name === 'pharmacies.actived'
          ? event.target.value !== -1
            ? event.target.value === 1
              ? true
              : false
            : undefined
          : event.target.value,
    });
  };

  const handleRunSearch = (query: any) => {
    setOpenFilter(false);
    setContent({
      page,
      type,
      searchPlaceholder,
      rowsPerPage,
      contextQuery: query,
    });
  };

  const filter =
    onPharmacie || onPharmacieTitulaire ? (
      <PharmacieFilter
        fieldsState={fieldsState}
        handleFieldChange={handleFieldChange}
        handleRunSearch={handleRunSearch}
        handleResetFields={handleResetFields}
      />
    ) : onPersonnelGroupement ? (
      <PersonnelGroupementFilter
        fieldsState={fieldsState}
        handleFieldChange={handleFieldChange}
        handleRunSearch={handleRunSearch}
        handleResetFields={handleResetFields}
      />
    ) : onTitulaire ? (
      <TitulaireFilter
        fieldsState={fieldsState}
        handleFieldChange={handleFieldChange}
        handleRunSearch={handleRunSearch}
        handleResetFields={handleResetFields}
      />
    ) : onPersonnelAffectation ? (
      <PersonnelAffectationFilter
        setFieldsState={setFieldsState}
        fieldsState={fieldsState}
        handleFieldChange={handleFieldChange}
        handleRunSearch={handleRunSearch}
        handleResetFields={handleResetFields}
      />
    ) : onLaboratoire ? (
      <LaboratoireFilter
        fieldsState={fieldsState}
        handleFieldChange={handleFieldChange}
        handleRunSearch={handleRunSearch}
        handleResetFields={handleResetFields}
      />
    ) : null;

  const numSelected = (): number => {
    // if (selected && setSelected) {
    //   return selected.length > total ? total : selected.length;
    // }
    return checkedItems.length;
  };

  return (
    <div className={classnames(classes.root)}>
      {loading && <Backdrop />}
      {/* {filter} */}
      <div className={classes.title}>
        <Typography color="inherit" variant="subtitle1">
          {`${numSelected()}/${allTotal}`} {numSelected() > 1 ? 'sélectionnés' : 'sélectionné'}{' '}
          {`(${total})`}
        </Typography>
      </div>
      <div>
        <div className={classes.buttons}>
          {showCheckedAll && (
            <CustomCheckbox
              variant="text"
              color="inherit"
              size="medium"
              name="checkedAll"
              label={`Tout sélectionner (${allTotal})`}
              value={checkedAll}
              checked={checkedAll}
              onChange={handleCheckAll}
            />
          )}
          {filter && (
            <CustomButton
              variant="text"
              color="inherit"
              size="medium"
              startIcon={<Tune />}
              className={classes.btn}
              onClick={handleOpenFilter}
            >
              Filtres
            </CustomButton>
          )}
          {showResetFilters && (
            <CustomButton
              variant="text"
              color="inherit"
              size="medium"
              startIcon={<ReplayIcon />}
              className={classes.btn}
              onClick={handleResetFilter}
            >
              Réinitialiser les filtres
            </CustomButton>
          )}
          {checkedItems && numSelected() > 0 && (
            <>
              {showCheckedElementOrResult && (
                <CustomButton
                  variant="text"
                  color="inherit"
                  size="medium"
                  startIcon={showCheckeds ? <ListIcon /> : <ListAltIcon />}
                  className={classes.btn}
                  onClick={handleShowCheckeds}
                >
                  {showCheckeds ? 'Afficher les résultats' : 'Afficher les éléments selectionnés'}
                </CustomButton>
              )}

              <CustomButton
                variant="text"
                color="inherit"
                size="medium"
                startIcon={<Delete />}
                className={classes.btn}
                onClick={clearSelection}
              >
                Vider la sélection
              </CustomButton>
            </>
          )}
          {showRefreshTable && (
            <CustomButton
              variant="text"
              color="inherit"
              size="medium"
              startIcon={<Refresh />}
              className={classes.btn}
              onClick={onClickRefresh}
            >
              Réactualiser
            </CustomButton>
          )}
        </div>
      </div>
      <CustomModal
        title="Filtres de recherche"
        children={filter}
        open={openFilter}
        setOpen={setOpenFilter}
        withBtnsActions={false}
        headerWithBgColor={true}
        closeIcon={true}
      />
    </div>
  );
};

export default withRouter(EnhancedTableToolbar);
