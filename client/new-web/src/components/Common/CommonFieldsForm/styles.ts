import { createStyles, Theme } from '@material-ui/core/styles';
import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    usersModalRoot: {
      width: '100%',
      '& .MuiButton-label': {
        zIndex: 'auto',
      },

      '& .MuiDialogActions-root': {
        display: 'flex',
        justifyContent: 'center',
        padding: '0 0 22px',
        '& .MuiButtonBase-root': {
          padding: '11px 34px!important',
        },
        [theme.breakpoints.down('md')]: {
          width: '100vw',
          padding: 8,
          justifyContent: 'flex-end',
        },
      },
      [theme.breakpoints.down('md')]: {
        '& .MuiDialog-scrollPaper': {
          justifyContent: 'left',
        },
        '& .MuiDialog-paperFullWidth': {
          width: '100%',
          maxWidth: '100%',
          minHeight: '100%',
        },
        '& .MuiDialog-paper': {
          margin: 0,
        },
        '& .MuiDialogContent-root': {
          padding: 0,
        },
        '& .MuiDialogTitle-root': {
          background: theme.palette.secondary.main,
        },
      },
      '& .MuiTab-wrapper': {
        flexDirection: 'row',
        textTransform: 'none',
        '& > .MuiSvgIcon-root': {
          marginRight: 8,
        },
      },
      '& .MuiTab-labelIcon': {
        minHeight: 'auto',
      },
    },
    tableContainer: {
      marginTop: 16,
      [theme.breakpoints.down('md')]: {
        padding: 15,
      },
    },
    espaceHaut: {
      margin: 0,
    },
    espacedroite: {
      marginRight: '12px',
    },
    section: {
      '& .ql-container.ql-snow': {
        minHeight: 97,
      },
      '& .quill.customized-title': {
        marginBottom: 33,
      },
      '& .MuiMenuItem-root': {
        minHeight: '40px!important',
      },

      '&.MuiListItem-root.Mui-selected': {
        minHeight: '40px !important',
        '&:hover': {
          minHeight: '40px !important',
        },
      },
      [theme.breakpoints.down('md')]: {
        margin: 0,
        width: '100%',
      },
    },
    sectionDatePicker: {
      margin: '15px 15px 32px 15px ',
      [theme.breakpoints.down('md')]: {
        margin: '0px 0px 16px 0px',
      },
    },
    inputText: {
      width: '100%',
      marginBottom: 12,
      '& .MuiOutlinedInput-root': {
        '&.Mui-focused': {
          '& .MuiOutlinedInput-notchedOutline': {
            borderColor: 'rgba(0, 0, 0, 0.23)!important',
          },
        },
      },
      '& .MuiSelect-selectMenu': {
        fontSize: '0.875rem',
        [theme.breakpoints.down('md')]: {
          whiteSpace: 'normal',
        },
      },
      '& .MuiOutlinedInput-input': { fontSize: '0.875rem' },
      '& .MuiFormLabel-root': {
        color: '#878787',
        fontSize: '0.875rem',
      },
    },

    contentDatePicker: {
      '& .MuiGrid-root': {
        position: 'relative',
      },
      '& .MuiInputLabel-formControl': {
        position: 'initial',
        transform: 'translate(0, 14px) scale(1)',
      },
      '& .MuiTextField-root': {
        width: '100%',
      },
      '& .MuiIconButton-root': {
        padding: 4,
        marginBottom: 10,
      },
      '& .MuiFormLabel-root': {
        color: '#B1B1B1',
      },

      '& .MuiFormControl-root': {
        padding: '7px 14px !important',
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.23)',
        marginTop: 0,
      },
      '& .MuiInput-underline:before': {
        display: 'none',
      },
      '& .MuiInputLabel-shrink': {
        transform: 'translate(0, -13.5px) scale(0.75)',
        background: '#ffffff',
        width: 152,
        paddingLeft: 6,
      },
      '& .MuiInputBase-input': {
        padding: '0 0 10px',
      },
    },
    dateSelectNone: {
      position: 'absolute',
      width: '95%',
      height: '100%',
      left: 0,
      zIndex: 2,
    },
    row: {
      display: 'flex',
      flexDirection: 'row',
      '& > *:nth-child(1)': {
        marginRight: 5,
      },
      '& > *:nth-child(2)': {
        marginLeft: 5,
      },
    },
    tabs: {
      background: theme.palette.common.white,
      color: theme.palette.common.black,
    },
    tabPaper: {
      '& .MuiPaper-elevation4': {
        boxShadow: '0px 1px 0px 0px rgba(0,0,0,0.14)',
      },
    },
    collaborateurInput: {
      marginBottom: 16,
      borderRadius: 5,
      border: '1px solid #c4c4c4',
      [theme.breakpoints.up('sm')]: {
        marginTop: 16,
      },
      '& legend': {
        marginLeft: 12,
      },
    },
    collaborateurBox: {
      display: 'flex',
      justifyContent: 'space-between',
    },
  }),
);
