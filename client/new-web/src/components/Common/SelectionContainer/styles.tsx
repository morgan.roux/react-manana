import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    tableRoot: {
      height: 'auto',
      maxHeight: 'calc(100vh - 300px)',
      overflowY: 'auto',
      minWidth: '65%',
    },
    rootSelectionContainer: {
      '& .MuiCheckbox-root': {
        paddingTop: '0px !important',
        paddingBottom: '0px !important',
        color: '#000',
      },
      '& .MuiCheckbox-colorSecondary.Mui-checked': {
        color: '#000',
      },
    },
  }),
);

export default useStyles;
