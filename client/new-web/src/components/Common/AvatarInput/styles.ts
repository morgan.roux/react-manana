import { createStyles, Theme, withStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    collaborateurInput: {
      minHeight: 56,
      marginBottom: 24,
      borderRadius: 5,
      border: '1px solid #c4c4c4',
      [theme.breakpoints.up('sm')]: {
        marginTop: 24,
      },
      '& legend': {
        marginLeft: 12,
      },
    },
    collaborateurBox: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    collaborateurPlaceholder: {
      marginLeft: 10,
      color: '#aaa',
      fontSize: 12
    }

  }),
);

export default useStyles;
