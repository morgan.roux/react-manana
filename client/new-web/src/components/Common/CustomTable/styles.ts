import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    tableRoot: {
      height: 'auto',
      maxHeight: 'calc(100vh - 300px)',
      overflowY: 'auto',
      minWidth: '65%',
    },
    rootSelectionContainer: {
      '& .MuiCheckbox-root': {
        paddingTop: '0px !important',
        paddingBottom: '0px !important',
        color: '#000',
      },
      '& .MuiCheckbox-colorSecondary.Mui-checked': {
        color: '#000',
      },
    },
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
  }),
);

export default useStyles;
