import LinearProgress from '@material-ui/core/LinearProgress';
import Modal from '@material-ui/core/Modal';
import { withStyles } from '@material-ui/core/styles';
import React, { Component } from 'react';
import loadingPhoto from './../../../assets/img/gcr_pharma.svg';
import { LoaderSmall } from '../../Dashboard/Content/Loader';
import { CircularProgress } from '@material-ui/core';

const styles = (theme: any) => ({
  col: {
    display: 'flex' as 'flex',
    flex: '0 1 auto',
    flexDirection: 'column' as 'column',
    left: 0,
    msFlex: '0 1 auto',
    position: 'absolute' as 'absolute',
    right: 0,
    top: '35%',
    webkitFlex: '0 1 auto',
  },
  loadingPwg: {
    margin: '0 auto 16px',
    width: '11rem',
    zIndex: 999,
  },
  loadingProgress: {
    margin: '8px auto',
    width: '15rem',
    zIndex: 999,
  },

  loadingContent: {
    height: '100vh',
    width: '100vw',
    background: '#FFF',
    display: 'flex' as 'flex',
    flexDirection: 'row' as 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

interface LoadingProps {
  classes: any;
}

class SmallLoading extends Component<LoadingProps, {}> {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.loadingContent}>
        <CircularProgress size={50} disableShrink={true} color="primary" />
      </div>
    );
  }
}

export default withStyles(styles)(SmallLoading);
