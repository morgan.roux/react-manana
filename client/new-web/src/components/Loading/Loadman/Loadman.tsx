import Loadable from 'react-loadable';
import MainLoading from './../MainLoading';

import WaitPromise from './WaitPromise';

export default (loader: any, loading?: any) =>
  Loadable({
    loader: () => WaitPromise(loader, 1000),
    loading: loading === null ? () => null : loading || MainLoading,
  });
