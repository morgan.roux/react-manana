import React, { FC } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import CustomButton from '../Common/CustomButton';
import { withRouter, RouteComponentProps } from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    unauthorizedRoot: {
      width: '100%',
      height: '100%',
      minHeight: 'calc(100vh - 86px)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      '& > button': {
        textTransform: 'none',
        minWidth: 300,
        minHeight: 50,
      },
      '& *': {
        fontFamily: 'Montserrat',
      },
    },
    unauthorizedTitle: {
      fontSize: 55,
    },

    unauthorizedSubTitle: {
      fontSize: 30,
    },
  }),
);

const Unauthorized: FC<RouteComponentProps> = ({ history: { push } }) => {
  const classes = useStyles({});
  const goToHome = () => push('/');
  return (
    <div className={classes.unauthorizedRoot}>
      <h1 className={classes.unauthorizedTitle}>401 - Unauthorized</h1>
      <CustomButton size="large" color="secondary" onClick={goToHome}>
        Retourner à la page d'accueil
      </CustomButton>
    </div>
  );
};

export default withRouter(Unauthorized);
