import React, { FC } from "react";
import withSearch from "../Common/withSearch";
import { actualiteFilter } from "../../services/actualiteFilter";
import { getAccessToken, getGroupement, getUser } from "../../services/LocalStorage";
import { Actualite } from '../Main/Content/Actualite';
import createClient from "../../apolloClient";
import { ApolloProvider } from "@apollo/client";
import { Box } from "@material-ui/core";
import { ThemeProvider } from "../../Theme";
import './../../App.css'



const ActualiteAdapter:FC<{}> = ({}) => {
    const user = getUser()
    const groupement = getGroupement()
    const token = getAccessToken() || '';
    const client = createClient(token);

    
    const actualiteOptionalSearchMust = actualiteFilter(user, groupement && groupement.id);

    return (<ApolloProvider client={client}>
      <ThemeProvider>
      
      <Box>
    {withSearch(
        Actualite,
        'actualite',
        null,
        actualiteOptionalSearchMust,
        undefined,
        {
          exclude: [
            'item',
            'laboratoire',
            'servicesCible',
            'pharmaciesCible',
            'pharmaciesRolesCible',
            'presidentsCibles',
            'partenairesCible',
            'laboratoiresCible',
          ],
        },
      )}
      </Box>
      </ThemeProvider>
      </ApolloProvider>)

} 

export default ActualiteAdapter