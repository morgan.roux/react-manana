import React from 'react';
import { useStyles } from './styles';
import { AppBar as AppBarMaterialUI, Toolbar } from '@material-ui/core';
import { HeaderMenu } from './Menu';
import { SearchInput } from './Search';
import { Brand } from './Brand';

const AppBar = () => {
  const classes = useStyles({});

  return (
    <AppBarMaterialUI position="static">
      <Toolbar>
        <Brand />
        <SearchInput />
        <div className={classes.grow} />
        <HeaderMenu />
      </Toolbar>
    </AppBarMaterialUI>
  );
};

export default AppBar;
