import React, { FC } from 'react';
import { useStyles } from './style';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';

export interface SearchInputProps {
  name?: string;
  value?: any;
  onChange?: (event: React.ChangeEvent<any>) => void;
}

const SearchInput: FC<SearchInputProps> = ({ name, value, onChange }) => {
  const classes = useStyles({});

  return (
    <div className={classes.search}>
      <InputBase
        placeholder="Que recherchez-vous ?"
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput,
        }}
        inputProps={{ 'aria-label': 'search' }}
        name={name}
        value={value}
        onChange={onChange ? onChange : undefined}
      />
      <div className={classes.searchIcon}>
        <SearchIcon />
      </div>
    </div>
  );
};

export default SearchInput;
