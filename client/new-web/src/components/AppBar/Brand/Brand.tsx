import React, { FC } from 'react';
import logo from '../../../assets/img/gcr_pharma.svg';
import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import { Theme } from '@material-ui/core/styles';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
  root: {
    flexGrow: 1,
  },
  logo: {
    height: 50,
  },
});

const Brand: FC<WithStyles> = ({ classes }) => {
  return (
    <div className={classes.root}>
      <img className={classes.logo} src={logo} />
    </div>
  );
};

export default withStyles(styles)(Brand);
