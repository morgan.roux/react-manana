import { useQuery } from '@apollo/react-hooks';
import { QueryResult } from 'react-apollo';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import { ORIGINES } from '../../../federation/basis/origine/query';
import {
    ORIGINES as ORIGINES_TYPE,
    ORIGINESVariables,
} from '../../../federation/basis/origine/types/ORIGINES';

const useOrigines = (
): QueryResult<ORIGINES_TYPE, ORIGINESVariables> => {
    return useQuery<ORIGINES_TYPE, ORIGINESVariables>(ORIGINES, {
        client: FEDERATION_CLIENT
    });
    ;
};

export default useOrigines;
