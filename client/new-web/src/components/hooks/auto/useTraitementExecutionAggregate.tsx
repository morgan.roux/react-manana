import { useQuery } from '@apollo/client';
import { QueryResult } from 'react-apollo';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import { GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE } from './../../../federation/auto/traitement-execution/query'
import { GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE as GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE_TYPE, GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATEVariables } from './../../../federation/auto/traitement-execution/types/GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE'

const useTraitementExecutionAggregate = (
): QueryResult<GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE_TYPE, GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATEVariables> => {

    return useQuery<GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE_TYPE, GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATEVariables>(GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE, {
        client: FEDERATION_CLIENT
    });
    ;
};

export default useTraitementExecutionAggregate;
