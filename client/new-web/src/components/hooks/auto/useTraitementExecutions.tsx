import { useQuery } from '@apollo/client';
import { QueryResult } from 'react-apollo';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import { GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS } from '../../../federation/auto/traitement-execution/query'
import { GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS as GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_TYPE, GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONSVariables } from '../../../federation/auto/traitement-execution/types/GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS'

const useTraitementExecutions = (
): QueryResult<GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_TYPE, GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONSVariables> => {

    return useQuery<GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_TYPE, GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONSVariables>(GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS, {
        client: FEDERATION_CLIENT
    });
    ;
};

export default useTraitementExecutions;
