import { MutationTuple, useMutation } from '@apollo/client';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import { UPDATE_ONE_ITEM_SCORING_PERSONNALISATION } from '../../../federation/tools/itemScoring/mutation'
import { UPDATE_ONE_ITEM_SCORING_PERSONNALISATION as UPDATE_ONE_ITEM_SCORING_PERSONNALISATION_TYPE, UPDATE_ONE_ITEM_SCORING_PERSONNALISATIONVariables } from '../../../federation/tools/itemScoring/types/UPDATE_ONE_ITEM_SCORING_PERSONNALISATION'

const useUpdateOneItemScoringPersonnalisation = (
): MutationTuple<UPDATE_ONE_ITEM_SCORING_PERSONNALISATION_TYPE, UPDATE_ONE_ITEM_SCORING_PERSONNALISATIONVariables> => {

    return useMutation<UPDATE_ONE_ITEM_SCORING_PERSONNALISATION_TYPE, UPDATE_ONE_ITEM_SCORING_PERSONNALISATIONVariables>(UPDATE_ONE_ITEM_SCORING_PERSONNALISATION, {
        client: FEDERATION_CLIENT
    });
    ;
};

export default useUpdateOneItemScoringPersonnalisation;
