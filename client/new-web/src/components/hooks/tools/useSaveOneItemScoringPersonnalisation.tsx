import { ItemScoringPersonnalisationInput } from '../../../types/federation-global-types';
import { ItemScoringPersonnalisationInfo } from '../../../federation/tools/itemScoring/types/ItemScoringPersonnalisationInfo';
import useCreateOneItemScoringPersonnalisation from './useCreateOneItemScoringPersonnalisation'
import useUpdateOneItemScoringPersonnalisation from './useUpdateOneItemScoringPersonnalisation'
import { useState } from 'react';


const useSaveOneItemScoringPersonnalisation = (
): [(input: ItemScoringPersonnalisationInput, id?: string) => Promise<any>, { loading?: boolean, error?: Error, data?: ItemScoringPersonnalisationInfo }] => {

    const [mode, setMode] = useState<'creation' | 'modification'>('creation')
    const [create, creation] = useCreateOneItemScoringPersonnalisation()
    const [update, modification] = useUpdateOneItemScoringPersonnalisation()


    const upsert = (input: ItemScoringPersonnalisationInput, id?: string) => {
        if (id) {
            setMode('modification')
            return update({
                variables: {
                    id,
                    input: {
                        type: input.type,
                        idTypeAssocie: input.idTypeAssocie,
                        ordre: input.ordre
                    }

                }
            })
        }
        else {
            setMode('creation')
            return create({
                variables: {
                    input: {
                        type: input.type,
                        idTypeAssocie: input.idTypeAssocie,
                        ordre: input.ordre
                    }
                }
            })

        }


    }



    return [upsert, {
        loading: mode === 'creation' ? creation.loading : modification.loading,
        error: mode === 'creation' ? creation.error : modification.error,
        data: mode === 'creation' ? creation.data?.createOneItemScoringPersonnalisation : modification.data?.updateOneItemScoringPersonnalisation
    }]

};

export default useSaveOneItemScoringPersonnalisation;
