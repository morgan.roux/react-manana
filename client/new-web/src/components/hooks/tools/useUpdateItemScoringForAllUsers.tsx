import { QueryTuple, useLazyQuery } from '@apollo/client';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import { UPDATE_ITEM_SCORING_FOR_ALL_USERS as UPDATE_ITEM_SCORING_FOR_ALL_USERS_TYPE } from '../../../federation/tools/itemScoring/types/UPDATE_ITEM_SCORING_FOR_ALL_USERS';
import { UPDATE_ITEM_SCORING_FOR_ALL_USERS } from '../../../federation/tools/itemScoring/query';

const useUpdateItemScoringForAllUsers = (
): QueryTuple<UPDATE_ITEM_SCORING_FOR_ALL_USERS_TYPE, any> => {

    return useLazyQuery<UPDATE_ITEM_SCORING_FOR_ALL_USERS_TYPE>(
        UPDATE_ITEM_SCORING_FOR_ALL_USERS,
        { client: FEDERATION_CLIENT },
    );

    ;
};

export default useUpdateItemScoringForAllUsers;
