import { useValueParameterAsNumber } from "../../../../../utils/getValueParameter";


export const useNiveauMatriceFonctions = (ignorePharmacie?: boolean) => {
    const groupementNiveauMatriceFonctions = useValueParameterAsNumber('0830')
    const pharmacieNiveauMatriceFonctions = useValueParameterAsNumber('0730')

    return ignorePharmacie ? groupementNiveauMatriceFonctions : pharmacieNiveauMatriceFonctions || groupementNiveauMatriceFonctions
};