import { uniqWith } from 'lodash';
import {
  GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes,
  GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches_collaborateurResponsables_responsables,
} from './../../../../../federation/demarche-qualite/matrice-tache/types/GET_FONCTIONS_WITH_RESPONSABLE_INFOS';

interface KeyValue {
  [key: string]: any;
}

export const extractResponsablesFromFonctions = (
  fonctions: GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes[],
): GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches_collaborateurResponsables_responsables[] => {
  return uniqWith(
    fonctions.reduce((responsables, currentFonction) => {
      return [
        ...responsables,
        ...currentFonction.taches.reduce((responsablesTache, currentTache) => {
          return [
            ...responsablesTache,
            ...currentTache.collaborateurResponsables.reduce(
              (collaborateurs, currentResponsable) => {
                return [...collaborateurs, ...currentResponsable.responsables] as any;
              },
              [],
            ),
          ] as any;
        }, []),
      ];
    }, []),
    (a: any, b: any) => a.id === b.id,
  );
};

export const formatFonctions = (
  fonctions: GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes[],
): KeyValue[] => {
  // const responsables = extractResponsablesFromFonctions(fonctions);

  return fonctions
    .filter((fonction:any)=>fonction.active)
    .sort((a, b) => a.ordre - b.ordre)
    .reduce((result, currentFonction) => {
      return [
        ...result,
        ...currentFonction.taches
          .filter((tache:any)=>tache.active)
          .sort((a, b) => a.ordre - b.ordre)
          .reduce((resultTaches, currentTache, index) => {
            return [
              ...resultTaches,

              {
                fonction: currentFonction.libelle,
                ordreFonction: currentFonction.ordre,
                index,
                nombreTaches:currentFonction.taches.length,
                tache: currentTache.libelle,
                ordreTache: currentTache.ordre,
                ...currentTache.collaborateurResponsables.reduce(
                  (responsableCodes, currentResponsables) => {
                    return {
                      ...responsableCodes,
                      ...currentResponsables.responsables.reduce(
                        (listCollaborateurs, currentCollaborateur) => {
                          return {
                            ...listCollaborateurs,
                            [currentCollaborateur.id]: currentResponsables.type.code,
                          };
                        },
                        {},
                      ),
                    };
                  },
                  {},
                ),
              },
            ] as any;
          }, []),
      ];
    }, []);
};
