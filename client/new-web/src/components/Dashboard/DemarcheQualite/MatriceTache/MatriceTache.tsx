import React, { FC, useState, useEffect, ChangeEvent } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import useStyles from './style';
import { Box, Divider, IconButton } from '@material-ui/core';
import { CardTheme, CardThemeList } from '@app/ui-kit';
import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import { getUser, getPharmacie } from '../../../../services/LocalStorage';
import { ME_me } from '../../../../graphql/Authentication/types/ME';
import { TITULAIRE_PHARMACIE } from '../../../../Constant/roles';

import {
  GET_FONCTIONS as GET_FONCTIONS_TYPE,
  GET_FONCTIONSVariables,
} from '../../../../federation/demarche-qualite/matrice-tache/types/GET_FONCTIONS';
import {
  GET_FONCTION as GET_FONCTION_TYPE,
  GET_FONCTIONVariables,
} from '../../../../federation/demarche-qualite/matrice-tache/types/GET_FONCTION';
import {
  DELETE_FONCTION as DELETE_FONCTION_TYPE,
  DELETE_FONCTIONVariables,
} from '../../../../federation/demarche-qualite/matrice-tache/types/DELETE_FONCTION';
import {
  CREATE_FONCTION as CREATE_FONCTION_TYPE,
  CREATE_FONCTIONVariables,
} from '../../../../federation/demarche-qualite/matrice-tache/types/CREATE_FONCTION';
import {
  UPDATE_FONCTION as UPDATE_FONCTION_TYPE,
  UPDATE_FONCTIONVariables,
} from '../../../../federation/demarche-qualite/matrice-tache/types/UPDATE_FONCTION';
import {
  TOGGLE_ONE_FONCTION_ACTIVATION as TOGGLE_ONE_FONCTION_ACTIVATION_TYPE,
  TOGGLE_ONE_FONCTION_ACTIVATIONVariables,
} from '../../../../federation/demarche-qualite/matrice-tache/types/TOGGLE_ONE_FONCTION_ACTIVATION';
import {
  PERSONNALISER_FONCTION as PERSONNALISER_FONCTION_TYPE,
  PERSONNALISER_FONCTIONVariables,
} from '../../../../federation/demarche-qualite/matrice-tache/types/PERSONNALISER_FONCTION';
import {
  GET_FONCTION,
  GET_FONCTIONS,
} from '../../../../federation/demarche-qualite/matrice-tache/query';
import { Add, Delete, Edit } from '@material-ui/icons';
import { TwoColumnsParameters, Backdrop, ConfirmDeleteDialog, ErrorPage } from '@app/ui-kit';

import {
  CREATE_FONCTION,
  DELETE_FONCTION,
  UPDATE_FONCTION,
  TOGGLE_ONE_FONCTION_ACTIVATION,
  PERSONNALISER_FONCTION,
} from '../../../../federation/demarche-qualite/matrice-tache/mutation';
import { FEDERATION_CLIENT } from './../apolloClientFederation';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import NoItemContentImage from '../../../Common/NoItemContentImage';
import MatriceForm from './MatriceTacheForm/MatriceTacheForm';
import MatriceTacheFonctionDetails from './MatriceTacheFonctionDetails/MatriceTacheFonctionDetails';
import { AppAuthorization } from '../../../../services/authorization';
import { useNiveauMatriceFonctions } from './hooks';

const MatriceTache: FC<RouteComponentProps> = ({
  history: { push },
  location: { pathname },
  match: { params },
}) => {
  const classes = useStyles({});

  const user: ME_me = getUser();
  const pharmacie = getPharmacie();
  const isTitulairePharmacie = user?.role?.code === TITULAIRE_PHARMACIE;
  const auth = new AppAuthorization(user)

  const [show, setShow] = useState<boolean>(false);
  const [showDeleteButton, setShowDeleteButton] = useState<boolean>(false);
  const [fonctionToDeleteId, setFonctionToDeleteId] = useState<string>('');
  const [mode, setMode] = useState<'creation' | 'modification'>('creation');
  const [ordre, setOrdre] = useState<number | undefined>();
  const [libelle, setLibelle] = useState<string>('');
  const [currentFonctionId, setCurrentFonctionId] = useState<string>('');
  const niveauMatriceFonctions = useNiveauMatriceFonctions(auth.isSupAdminOrIsGpmAdmin)


  const client = useApolloClient();
  const selectedFonctionId = (params as any).id;

  const { loading, error, data, refetch } = useQuery<GET_FONCTIONS_TYPE, GET_FONCTIONSVariables>(
    GET_FONCTIONS,
    {
      client: FEDERATION_CLIENT,
      fetchPolicy: 'cache-and-network',
      variables: {
        idPharmacie: pharmacie.id,
        paging: {
          offset: 0,
          limit: 50,
        },
      },
    },
  );

  const [loadFonction, loadingFonction] = useLazyQuery<GET_FONCTION_TYPE, GET_FONCTIONVariables>(
    GET_FONCTION,
    {
      client: FEDERATION_CLIENT,
      fetchPolicy: 'cache-and-network', // TODO : Use cache
    },
  );

  const [addFonction, creationFonction] = useMutation<
    CREATE_FONCTION_TYPE,
    CREATE_FONCTIONVariables
  >(CREATE_FONCTION, {
    onError: () => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: `Une s'est produite lors de l'ajout de la nouvelle fonction de Base`,
        isOpen: true,
      });
    },
    onCompleted: () => {
      setShow(false);
      reInit();
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: 'La fonction de Base a été ajoutée avec succès !',
        isOpen: true,
      });

      refetch();
    },
    update: (cache, creationResult) => {
      if (creationResult?.data?.createOneDQMTFonction) {
        const previousList = cache.readQuery<GET_FONCTIONS_TYPE, GET_FONCTIONSVariables>({
          query: GET_FONCTIONS,
        })?.dQMTFonctions;

        cache.writeQuery<GET_FONCTIONS_TYPE, GET_FONCTIONSVariables>({
          query: GET_FONCTIONS,
          data: {
            dQMTFonctions: {...(previousList || {}), nodes: [creationResult.data.createOneDQMTFonction, ...(previousList?.nodes || [])] } as any,
          },
        });
      }
    },
    client: FEDERATION_CLIENT,
  });

  const [updateFonction, modificationFonction] = useMutation<
    UPDATE_FONCTION_TYPE,
    UPDATE_FONCTIONVariables
  >(UPDATE_FONCTION, {
    onError: () => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: `Une s'est produite lors de la mise à jour de la fonction de Base`,
        isOpen: true,
      });
    },
    onCompleted: () => {
      setShow(false);
      reInit();
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: 'La fonction de Base a été modifiée avec succès !',
        isOpen: true,
      });
      refetch();
    },
    client: FEDERATION_CLIENT,
  });

  const [personaliserFonction, personnalisationFonction] = useMutation<
    PERSONNALISER_FONCTION_TYPE,
    PERSONNALISER_FONCTIONVariables
  >(PERSONNALISER_FONCTION, {
    onError: () => {
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: `Une erreur s'est produite lors de la modification de la fonction.`,
        isOpen: true,
      });
    },
    onCompleted: () => {
      setShow(false);
      reInit();
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: 'La fonction a été modifiée avec succès !',
        isOpen: true,
      });
      refetch();
    },
    client: FEDERATION_CLIENT,
  });

  const [deleteFonction] = useMutation<DELETE_FONCTION_TYPE, DELETE_FONCTIONVariables>(
    DELETE_FONCTION,
    {
      onError: () => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: `Une s'est produite lors de la suppression de la fonction de Base`,
          isOpen: true,
        });
      },
      onCompleted: (deleteResult) => {
        setShowDeleteButton(false);
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'La fonction de Base a été supprimée avec succès !',
          isOpen: true,
        });

        refetch();
        if (selectedFonctionId && selectedFonctionId === deleteResult.deleteOneDQMTFonction?.id) {
          push('/db/matrice_taches/');
        }
      },
      update: (cache, deletionResult) => {
        if (deletionResult.data?.deleteOneDQMTFonction?.id) {
          const previousList = cache.readQuery<GET_FONCTIONS_TYPE, GET_FONCTIONSVariables>({
            query: GET_FONCTIONS,
          })?.dQMTFonctions;

          const deleteId = deletionResult.data.deleteOneDQMTFonction.id;

          cache.writeQuery<GET_FONCTIONS_TYPE>({
            query: GET_FONCTIONS,
            data: {
              dQMTFonctions: {...(previousList || {}), nodes: (previousList?.nodes || []).filter((item) => item?.id !== deleteId)} as any,
            },
          });

          if (deleteId === currentFonctionId) {
            push('db/demarche_qualite');
          }
        }
      },
      client: FEDERATION_CLIENT,
    },
  );

  const [toggleFonctionActivation, loadingFonctionActivation] = useMutation<
    TOGGLE_ONE_FONCTION_ACTIVATION_TYPE,
    TOGGLE_ONE_FONCTION_ACTIVATIONVariables
  >(TOGGLE_ONE_FONCTION_ACTIVATION, {
    onError: () => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: `Une s'est produite lors de la mise à jour de la fonction de Base`,
        isOpen: true,
      });
    },
    onCompleted: () => {
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: 'La fonction de Base a été modifiée avec succès !',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const handleIdChange = (item: any) => {
    loadFonction({
      variables: {
        idPharmacie: pharmacie.id,
        id: item.id,
      },
    });
  };

  useEffect(() => {
    if (loadingFonction.called && loadingFonction.data?.dQMTFonction?.id) {
      push({
        pathname: `/db/matrice_taches/${loadingFonction.data.dQMTFonction.id}`,
        state: { fonction: loadingFonction.data?.dQMTFonction },
      });
    }
  }, [loadingFonction.data]);

  const handleClick = (id: string): void => {
    setFonctionToDeleteId(id);
    setShowDeleteButton(!showDeleteButton);
  };

  const reInit = () => {
    setOrdre(undefined);
    setLibelle('');
  };

  const handleChangeShow = (mode: 'creation' | 'modification', element: any = null): void => {
    setShow(!show);
    setMode(mode);
    if (element) {
      setCurrentFonctionId(element.id);
      setOrdre(element.ordre);
      setLibelle(element.libelle);
    }
  };

  const handleSubmit = (): void => {
    if (ordre) {
      if (mode === 'creation') {
        addFonction({
          variables: {
            idPharmacie: pharmacie.id,
            input: {
              dQMTFonction: {
                ordre,
                libelle,
              },
            },
          },
        });
      } else {
        if (isTitulairePharmacie) {
          personaliserFonction({
            variables: {
              idFonction: currentFonctionId,
              idPharmacie: pharmacie.id,
              input: {
                ordre,
                libelle,
              },
            },
          });
        } else {
          updateFonction({
            variables: {
              idPharmacie: pharmacie.id,
              input: {
                id: currentFonctionId,
                update: {
                  ordre,
                  libelle,
                },
              },
            },
          });
        }
      }
    }
  };

  const handleDelete = (): void => {
    deleteFonction({
      variables: {
        input: {
          id: fonctionToDeleteId,
        },
      },
    });
  };

  const handleToggleFonctionActivation = ({ id }): void => {
    toggleFonctionActivation({
      variables: {
        idFonction: id,
        idPharmacie: pharmacie.id,
      },
    });
  };

  if (loading || loadingFonction.loading)
    return <Backdrop value="Récuperation de données..." open={true} />;

  if (error) {
    return <ErrorPage />;
  }



  const fonctionHeader = (
    <div className={classes.headerContent}>
      <div>Liste des fonctions de Base</div>
      {!isTitulairePharmacie && (
        <IconButton onClick={handleChangeShow.bind(null, 'creation')}>
          <Add color="primary" />
        </IconButton>
      )}
    </div>
  )

  const listFonctions = (
    data?.dQMTFonctions.nodes.length || 0 > 0 ? (
      <Box>
        {
          niveauMatriceFonctions === 1 && (fonctionHeader)
        }
        <CardThemeList>
          {data?.dQMTFonctions.nodes
            .sort((a, b) => (a?.ordre || 0) - (b?.ordre || 0))
            .map((item: any, index: number) => {
              const result: boolean =
                (selectedFonctionId && item && selectedFonctionId === item.id) === true;

              return (
                <CardTheme
                  applyMinHeight={false}
                  key={item.id}
                  listItemFields={{
                    url: '/db/matrice_taches/',
                  }}
                  title={`${item.ordre}-${item.libelle}${item.active ? '' : '(désactivée)'}`}
                  setCurrentId={setCurrentFonctionId}
                  littleComment={niveauMatriceFonctions === 1 ? ' ' : `Nbre de fonctions : ${item.nombreTaches}`}
                  active={result}
                  onClick={handleIdChange.bind(null, item)}
                  moreOptions={
                    isTitulairePharmacie
                      ? [
                        {
                          menuItemLabel: {
                            title: item.active ? 'Désactiver' : 'Activer',
                            icon: <Edit />,
                          },
                          onClick: (event) => handleToggleFonctionActivation(item),
                        },

                        {
                          menuItemLabel: {
                            title: 'Modifier',
                            icon: <Edit />,
                          },
                          onClick: handleChangeShow.bind(null, 'modification', item),
                        },
                      ]
                      : [
                        {
                          menuItemLabel: {
                            title: 'Modifier',
                            icon: <Edit />,
                          },
                          onClick: handleChangeShow.bind(null, 'modification', item),
                        },
                        {
                          menuItemLabel: {
                            title: 'Supprimer',
                            icon: <Delete />,
                          },
                          onClick: handleClick.bind(null, item.id),
                        },
                      ]
                  }
                />
              );
            })}
        </CardThemeList>
        <ConfirmDeleteDialog
          key="delete-fonction"
          open={showDeleteButton}
          setOpen={setShowDeleteButton}
          onClickConfirm={handleDelete}
        />
      </Box>
    ) : (
      <NoItemContentImage
        title="Aucune fonction de Base à afficher"
        subtitle="Ajouter-en une en cliquant sur le bouton d'en haut"
      />
    )
  )

  return (<>
    <MatriceForm
      saving={
        mode === 'modification'
          ? isTitulairePharmacie
            ? personnalisationFonction.loading
            : modificationFonction.loading
          : creationFonction.loading
      }
      title={
        mode == 'creation' ? 'Ajout de fonction de Base' : 'Modification de fonction de Base'
      }
      show={show}
      setShow={setShow}
      onSubmit={handleSubmit}
      ordre={ordre}
      libelle={libelle}
      setLibelle={setLibelle}
      setOrdre={setOrdre}
    />
    {niveauMatriceFonctions === 1 ? listFonctions : (
      <TwoColumnsParameters
        leftHeaderContent={
          fonctionHeader
        }
        rightContent={<MatriceTacheFonctionDetails />}
      >
        {listFonctions}
      </TwoColumnsParameters>
    )}
  </>);
};

export default withRouter(MatriceTache);
