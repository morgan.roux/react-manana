import { Box, Divider } from '@material-ui/core';
import React, { FC, useState, ChangeEvent } from 'react';
import useStyles from './style';
import { CustomButton } from '@app/ui-kit';
import AddIcon from '@material-ui/icons/Add';
import { RouteComponentProps, withRouter, useLocation } from 'react-router';
import MatriceTacheForm from '../MatriceTacheForm/MatriceTacheForm';
import { useApolloClient, useMutation } from '@apollo/client';
import { getUser, getPharmacie } from '../../../../../services/LocalStorage';
import { ME_me } from '../../../../../graphql/Authentication/types/ME';
import { TITULAIRE_PHARMACIE } from '../../../../../Constant/roles';
import {
  CREATE_TACHEVariables,
  CREATE_TACHE as CREATE_TACHE_TYPE,
} from '../../../../../federation/demarche-qualite/matrice-tache/types/CREATE_TACHE';
import {
  GET_FONCTION as GET_FONCTION_TYPE,
  GET_FONCTIONVariables,
} from '../../../../../federation/demarche-qualite/matrice-tache/types/GET_FONCTION';
import { GET_FONCTION } from '../../../../../federation/demarche-qualite/matrice-tache/query';
import { CREATE_TACHE } from '../../../../../federation/demarche-qualite/matrice-tache/mutation';

import { FEDERATION_CLIENT } from '../../apolloClientFederation';
import TacheComponent from './Tache/Tache';
import NoItemContentImage from '../../../../Common/NoItemContentImage';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';

const DetailsTheme: FC<RouteComponentProps> = ({ history }) => {
  const classes = useStyles({});
  const location: any = useLocation();
  const state = location.state;
  const item = state && state.fonction;

  const user: ME_me = getUser();
  const pharmacie = getPharmacie();
  const isTitulairePharmacie = user?.role?.code === TITULAIRE_PHARMACIE;

  const [show, setShow] = useState<boolean>(false);
  const [ordre, setOrdre] = useState<number | undefined>();
  const [libelle, setLibelle] = useState<string>('');

  const reInit = () => {
    setOrdre(undefined);
    setLibelle('');
  };

  const client = useApolloClient();

  const [addTache, creationTache] = useMutation<CREATE_TACHE_TYPE, CREATE_TACHEVariables>(
    CREATE_TACHE,
    {
      onError: () => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: `Une s'est produite lors de l'ajout de la nouvelle fonction`,
          isOpen: true,
        });
      },
      onCompleted: () => {
        setShow(false);
        reInit();
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'La fonction a été ajoutée avec succès !',
          isOpen: true,
        });
      },
      update: (cache, creationResult) => {
        if (creationResult.data?.createOneDQMTTache) {
          const previousFonction = cache.readQuery<GET_FONCTION_TYPE, GET_FONCTIONVariables>({
            query: GET_FONCTION,
            variables: {
              idPharmacie: pharmacie.id,
              id: item.id,
            },
          })?.dQMTFonction;

          const updatedFonction = {
            ...previousFonction,
            nombreTaches: ((previousFonction as any)?.nombreTaches || 0) + 1,
            taches: [
              creationResult.data.createOneDQMTTache,
              ...((previousFonction as any)?.taches || []),
            ],
          };

          cache.writeQuery<GET_FONCTION_TYPE, GET_FONCTIONVariables>({
            query: GET_FONCTION,
            variables: {
              idPharmacie: pharmacie.id,
              id: item.id,
            },
            data: {
              dQMTFonction: updatedFonction,
            } as any,
          });
        }
      },
      client: FEDERATION_CLIENT,
    },
  );

  const toggleShow = () => {
    setShow(!show);
  };

  const handleSave = (): void => {
    if (ordre) {
      addTache({
        variables: {
          input: {
            dQMTTache: {
              idFonction: item.id as any,
              ordre,
              libelle,
            },
          },
        },
      });
    }
  };

  return (
    <>
      {state ? (
        <Box className={classes.root}>
          <Box className={classes.header}>
            <div>
              <span className={classes.spanBold}>{`${item.ordre}-${item.libelle}`}</span>
            </div>
            <div>
              {!isTitulairePharmacie && (
                <CustomButton
                  startIcon={<AddIcon />}
                  children="Ajouter une fonction"
                  color="secondary"
                  className={classes.btn}
                  onClick={toggleShow}
                />
              )}
              <MatriceTacheForm
                saving={creationTache.loading}
                title={'Ajout de fonction'}
                show={show}
                setShow={setShow}
                onSubmit={handleSave}
                ordre={ordre}
                libelle={libelle}
                setLibelle={setLibelle}
                setOrdre={setOrdre}
              />
            </div>
          </Box>
          <Divider />
          <TacheComponent fonctionId={item.id} />
        </Box>
      ) : (
        <NoItemContentImage
          title="Aucune fonction de Base selectionnée"
          subtitle="Choisissez un en cliquant sur la partie gauche pour voir le contenu dans cette partie"
        />
      )}
    </>
  );
};

export default withRouter(DetailsTheme);
