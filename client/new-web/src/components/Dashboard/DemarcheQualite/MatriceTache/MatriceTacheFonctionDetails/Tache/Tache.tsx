import React, { ChangeEvent, FC, useState, MouseEvent } from 'react';
import { useApolloClient, useQuery, useMutation } from '@apollo/client';
import { Box } from '@material-ui/core';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { getUser, getPharmacie } from '../../../../../../services/LocalStorage';
import { ME_me } from '../../../../../../graphql/Authentication/types/ME';
import { TITULAIRE_PHARMACIE } from '../../../../../../Constant/roles';
import useStyles from './style';
import {
  Theme,
  ConfirmDeleteDialog,
  LoaderSmall,
  ErrorPage,
  CardThemeList,
  CardTheme,
} from '@app/ui-kit';
import {
  GET_FONCTIONVariables,
  GET_FONCTION as GET_FONCTION_TYPE,
} from '../../../../../../federation/demarche-qualite/matrice-tache/types/GET_FONCTION';
import {
  UPDATE_TACHE as UPDATE_TACHE_TYPE,
  UPDATE_TACHEVariables,
} from '../../../../../../federation/demarche-qualite/matrice-tache/types/UPDATE_TACHE';
import {
  DELETE_TACHE as DELETE_TACHE_TYPE,
  DELETE_TACHEVariables,
} from '../../../../../../federation/demarche-qualite/matrice-tache/types/DELETE_TACHE';
import {
  TOGGLE_ONE_TACHE_ACTIVATION as TOGGLE_ONE_TACHE_ACTIVATION_TYPE,
  TOGGLE_ONE_TACHE_ACTIVATIONVariables,
} from '../../../../../../federation/demarche-qualite/matrice-tache/types/TOGGLE_ONE_TACHE_ACTIVATION';
import {
  PERSONNALISER_TACHE as PERSONNALISER_TACHE_TYPE,
  PERSONNALISER_TACHEVariables,
} from '../../../../../../federation/demarche-qualite/matrice-tache/types/PERSONNALISER_TACHE';
import { GET_FONCTION } from '../../../../../../federation/demarche-qualite/matrice-tache/query';
import {
  UPDATE_TACHE,
  DELETE_TACHE,
  TOGGLE_ONE_TACHE_ACTIVATION,
  PERSONNALISER_TACHE,
} from '../../../../../../federation/demarche-qualite/matrice-tache/mutation';

import { FEDERATION_CLIENT } from '../../../apolloClientFederation';
import { Delete, Edit } from '@material-ui/icons';
import MatriceTacheForm from '../../MatriceTacheForm/MatriceTacheForm';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';

interface TacheComponentProps {
  fonctionId: string;
}

const TacheComponent: FC<TacheComponentProps & RouteComponentProps> = ({ fonctionId, history }) => {
  const classes = useStyles({});

  const user: ME_me = getUser();
  const pharmacie = getPharmacie();
  const isTitulairePharmacie = user?.role?.code === TITULAIRE_PHARMACIE;

  const [show, setShow] = useState<boolean>(false);
  const [showDeleteButton, setShowDeleteButton] = useState<boolean>(false);
  const [toDeleteTache, setToDeleteTache] = useState<any>();
  const [toEditTache, setToEditTache] = useState<any>();
  const [ordre, setOrdre] = useState<number | undefined>();
  const [libelle, setLibelle] = useState<string>('');

  const reInit = () => {
    setOrdre(undefined);
    setLibelle('');
  };

  const client = useApolloClient();

  const loadingFonction = useQuery<GET_FONCTION_TYPE, GET_FONCTIONVariables>(GET_FONCTION, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'network-only',
    variables: {
      idPharmacie: pharmacie.id,
      id: fonctionId,
    },
  });

  const [deleteTache, deletionTache] = useMutation<DELETE_TACHE_TYPE, DELETE_TACHEVariables>(
    DELETE_TACHE,
    {
      onError: () => {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: `Une erreur s'est produite lors de la suppression de la fonction.`,
          isOpen: true,
        });
      },

      onCompleted: () => {
        setShowDeleteButton(false);
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'La fonction a été supprimée avec succès !',
          isOpen: true,
        });
      },
      update: (cache, deletionResult) => {
        if (deletionResult.data?.deleteOneDQMTTache?.id) {
          const previousFonction = cache.readQuery<GET_FONCTION_TYPE, GET_FONCTIONVariables>({
            query: GET_FONCTION,
            variables: {
              idPharmacie: pharmacie.id,
              id: fonctionId,
            },
          })?.dQMTFonction;

          const deleteId = deletionResult.data.deleteOneDQMTTache.id;

          const deletedTache = {
            ...previousFonction,
            nombreTaches: ((previousFonction as any)?.nombreTaches || 0) - 1,
            taches: (loadingFonction.data?.dQMTFonction?.taches || []).filter(
              (item) => item.id !== deleteId,
            ),
          };

          cache.writeQuery<GET_FONCTION_TYPE>({
            query: GET_FONCTION,
            data: {
              dQMTFonction: deletedTache,
            } as any,
          });
        }
      },

      client: FEDERATION_CLIENT,
    },
  );

  const [updateTache, modificationTache] = useMutation<UPDATE_TACHE_TYPE, UPDATE_TACHEVariables>(
    UPDATE_TACHE,
    {
      onError: () => {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: `Une erreur s'est produite lors de la modification de la fonction.`,
          isOpen: true,
        });
      },
      onCompleted: () => {
        setShow(false);
        reInit();
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'La fonction a été modifiée avec succès !',
          isOpen: true,
        });
      },
      client: FEDERATION_CLIENT,
    },
  );

  const [personaliserTache, personnalisationTache] = useMutation<
    PERSONNALISER_TACHE_TYPE,
    PERSONNALISER_TACHEVariables
  >(PERSONNALISER_TACHE, {
    onError: () => {
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: `Une erreur s'est produite lors de la modification de la fonction.`,
        isOpen: true,
      });
    },
    onCompleted: () => {
      setShow(false);
      reInit();
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: 'La fonction a été modifiée avec succès !',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [toggleTacheActivation, loadingTacheActivation] = useMutation<
    TOGGLE_ONE_TACHE_ACTIVATION_TYPE,
    TOGGLE_ONE_TACHE_ACTIVATIONVariables
  >(TOGGLE_ONE_TACHE_ACTIVATION, {
    onError: () => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: `Une s'est produite lors de la mise à jour de la fonction`,
        isOpen: true,
      });
    },
    onCompleted: () => {
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: 'La fonction a été modifiée avec succès !',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const handleDelete = (element: any): void => {
    setToDeleteTache(element);
    setShowDeleteButton(!showDeleteButton);
  };

  const handleUpdate = (tache: any) => {
    setToEditTache(tache);
    setOrdre(tache.ordre);
    setLibelle(tache.libelle);
    setShow(!show);
  };

  const doUpdate = (): void => {
    if (ordre) {
      if (isTitulairePharmacie) {
        personaliserTache({
          variables: {
            idPharmacie: pharmacie.id,
            idTache: toEditTache.id,
            input: {
              idFonction: fonctionId,
              ordre,
              libelle,
            },
          },
        });
      } else {
        updateTache({
          variables: {
            input: {
              id: toEditTache.id,
              update: {
                idFonction: fonctionId,
                ordre,
                libelle,
              },
            },
          },
        });
      }
    }
  };

  const doDelete = (): void => {
    if (toDeleteTache?.id) {
      deleteTache({
        variables: {
          input: {
            id: toDeleteTache.id,
          },
        },
      });
    }
  };

  const handleToggleTacheActivation = ({ id }): void => {
    toggleTacheActivation({
      variables: {
        idTache: id,
        idPharmacie: pharmacie.id,
      },
    });
  };

  return (
    <Box className={classes.content}>
      {loadingFonction.loading ? (
        <LoaderSmall />
      ) : loadingFonction.error ? (
        <ErrorPage />
      ) : (
        <CardThemeList>
          {(loadingFonction.data?.dQMTFonction?.taches || [])
            .sort((a, b) => a.ordre - b.ordre)
            .map((currentTache: any) => (
              <CardTheme
                applyMinHeight={false}
                key={currentTache.id}
                title={`${currentTache.ordre}-${currentTache.libelle}${currentTache.active && loadingFonction.data?.dQMTFonction?.active ? '' : '(désactivée)'
                  }`}
                littleComment=" "
                moreOptions={
                  isTitulairePharmacie
                    ? [
                      {
                        menuItemLabel: {
                          title: currentTache.active ? 'Désactiver' : 'Activer',
                          icon: <Edit />,
                        },
                        onClick: () => handleToggleTacheActivation(currentTache),
                      },

                      {
                        menuItemLabel: {
                          title: 'Modifier fonction',
                          icon: <Edit />,
                        },
                        onClick: handleUpdate.bind(null, currentTache),
                      },
                    ]
                    : [
                      {
                        menuItemLabel: {
                          title: 'Modifier fonction',
                          icon: <Edit />,
                        },
                        onClick: handleUpdate.bind(null, currentTache),
                      },
                      {
                        menuItemLabel: {
                          title: 'Supprimer fonction',
                          icon: <Delete />,
                        },
                        onClick: handleDelete.bind(null, currentTache),
                      },
                    ]
                }
              />
            ))}
        </CardThemeList>
      )}
      <MatriceTacheForm
        title={'Modification de fonction'}
        saving={isTitulairePharmacie ? personnalisationTache.loading : modificationTache.loading}
        show={show}
        setShow={setShow}
        ordre={ordre}
        libelle={libelle}
        setLibelle={setLibelle}
        setOrdre={setOrdre}
        onSubmit={doUpdate}
      />
      <ConfirmDeleteDialog
        open={showDeleteButton}
        setOpen={setShowDeleteButton}
        onClickConfirm={doDelete}
        isLoading={deletionTache.loading}
      />
    </Box>
  );
};

export default withRouter(TacheComponent);
