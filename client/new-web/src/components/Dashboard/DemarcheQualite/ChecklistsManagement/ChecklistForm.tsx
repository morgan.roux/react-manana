import React, { FC, useEffect, useState } from 'react';
import { Button } from '@material-ui/core';
import Image from './../../../../assets/img/mutation-success.png';
import { useLazyQuery, useMutation, useApolloClient } from '@apollo/client';
import { ChecklistFormPage, Checklist } from '@app/ui-kit';
import NotificationPanel from '../../../Common/NoItemContentImage'; // TODO : Use ui-kit

import { withRouter, RouteComponentProps } from 'react-router-dom';
import {
  GET_CHECKLIST as GET_CHECKLIST_TYPE,
  GET_CHECKLISTVariables,
} from './../../../../federation/demarche-qualite/checklist/types/GET_CHECKLIST';
import {
  CREATE_CHECKLIST as CREATE_CHECKLIST_TYPE,
  CREATE_CHECKLISTVariables,
} from './../../../../federation/demarche-qualite/checklist/types/CREATE_CHECKLIST';
import {
  UPDATE_CHECKLIST as UPDATE_CHECKLIST_TYPE,
  UPDATE_CHECKLISTVariables,
} from './../../../../federation/demarche-qualite/checklist/types/UPDATE_CHECKLIST';

import {
  GET_CHECKLISTS as GET_CHECKLISTS_TYPE,
  GET_CHECKLISTSVariables,
} from './../../../../federation/demarche-qualite/checklist/types/GET_CHECKLISTS';

import {
  GET_CHECKLIST,
  GET_CHECKLISTS,
} from './../../../../federation/demarche-qualite/checklist/query';
import {
  CREATE_CHECKLIST,
  UPDATE_CHECKLIST,
} from './../../../../federation/demarche-qualite/checklist/mutation';
import { FEDERATION_CLIENT } from './../apolloClientFederation';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import { AppAuthorization } from '../../../../services/authorization';
import { getUser, getPharmacie } from '../../../../services/LocalStorage';

const ChecklistForm: FC<RouteComponentProps> = ({ match: { params }, history }) => {
  const client = useApolloClient();

  const user = getUser();
  const auth = new AppAuthorization(user);
  const idPharmacie = getPharmacie().id;
  const variables = !auth.isSupAdminOrIsGpmAdmin
    ? { idPharmacie, onlyChecklistsPharmacie: true }
    : undefined;

  const checklistId = (params as any).id;
  const isNewChecklist = 'new' === checklistId;
  const [checklist, setCheckList] = useState<Checklist>();
  const [displayNotificationPanel, setDisplayNotificationPanel] = useState<boolean>(false);

  const [loadChecklist, { called, loading, data }] = useLazyQuery<
    GET_CHECKLIST_TYPE,
    GET_CHECKLISTVariables
  >(GET_CHECKLIST, {
    variables: {
      id: checklistId,
    },
    client: FEDERATION_CLIENT,
    fetchPolicy: 'network-only',
  });

  const [createChecklist, creationChecklist] = useMutation<
    CREATE_CHECKLIST_TYPE,
    CREATE_CHECKLISTVariables
  >(CREATE_CHECKLIST, {
    onError: () => {
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: isNewChecklist
          ? `Une erreur s'est produite lors de l'ajout de checklist.`
          : `Une erreur s'est produite lors de modification de checklist.`,
        isOpen: true,
      });
    },
    onCompleted: () => {
      setDisplayNotificationPanel(true);
    },
    update: (cache, creationResult) => {
      if (creationResult.data && creationResult.data.createDQChecklist) {
        const previousList = cache.readQuery<GET_CHECKLISTS_TYPE, GET_CHECKLISTSVariables>({
          query: GET_CHECKLISTS,
          variables,
        })?.dqChecklists;

        cache.writeQuery<GET_CHECKLISTS_TYPE, GET_CHECKLISTSVariables>({
          query: GET_CHECKLISTS,
          variables,
          data: {
            dqChecklists: [creationResult.data.createDQChecklist, ...(previousList || [])],
          },
        });
      }
    },
    client: FEDERATION_CLIENT,
  });

  const [updateChecklist, modificationChecklist] = useMutation<
    UPDATE_CHECKLIST_TYPE,
    UPDATE_CHECKLISTVariables
  >(UPDATE_CHECKLIST, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      setDisplayNotificationPanel(true);
    },
  });

  useEffect(() => {
    if (!isNewChecklist) {
      if (!called) {
        loadChecklist();
      } else if (data?.dqChecklist) {
        setCheckList(data.dqChecklist as any); // FIXME : Fix graphql schema
      }
    }
  }, [checklistId, loading]);

  const handleSave = (checklistToSave: Checklist) => {
    if (isNewChecklist) {
      createChecklist({
        variables: {
          checklistInput: {
            ...(checklistToSave as any),
            idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? idPharmacie : undefined,
          },
        },
      });
    } else if (checklistToSave.id) {
      updateChecklist({
        variables: {
          id: checklistToSave.id,
          checklistInput: {
            ...(checklistToSave as any),
            id: undefined,
          },
        },
      });
    }
  };

  const handleGoBack = () => {
    history.push('/db/check-list-qualite');
  };

  return displayNotificationPanel ? (
    <NotificationPanel
      src={Image}
      title={isNewChecklist ? `Ajout de check-list réussi` : `Modification de check-list réussie`}
      subtitle={
        isNewChecklist
          ? `Le check-list que vous venez de créer est maintenant dans la liste`
          : `Le check-list que vous venez de modifier est à jour.`
      }
    >
      <Button onClick={handleGoBack}>Retour</Button>
    </NotificationPanel>
  ) : (
    <ChecklistFormPage
      key={Math.random()}
      loading={loading}
      saving={creationChecklist.loading || modificationChecklist.loading}
      onRequestGoBack={handleGoBack}
      onRequestSave={handleSave}
      checklist={!isNewChecklist ? checklist : undefined}
      mode={isNewChecklist ? 'creation' : 'modification'}
    />
  );
};

export default withRouter(ChecklistForm);
