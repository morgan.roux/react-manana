import React, { FC } from 'react';
import { ApolloProvider } from 'react-apollo';
import { FEDERATION_CLIENT } from '../apolloClientFederation';
import ChecklistManagement from './ChecklistManagement';

// FIXME : Temporary solution
const WithApolloFederationClient: FC<{}> = ({}) => (
  <ApolloProvider client={FEDERATION_CLIENT}>
    <ChecklistManagement />
  </ApolloProvider>
);

export default WithApolloFederationClient;
