import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { Button } from '@material-ui/core';
import { useLazyQuery, useMutation, useQuery, useApolloClient } from '@apollo/client';
import { ExigenceFormPage, RequestSavingExigence, IExigence } from '@app/ui-kit';
import NotificationPanel from '../../../../../Common/NoItemContentImage'; // TODO : Use ui-kit
import Image from './../../../../../../assets/img/mutation-success.png';

import {
  GET_THEME as GET_THEME_TYPE,
  GET_THEMEVariables,
} from './../../../../../../federation/demarche-qualite/theme/types/GET_THEME';
import {
  GET_MINIMAL_SOUS_THEME as GET_MINIMAL_SOUS_THEME_TYPE,
  GET_MINIMAL_SOUS_THEMEVariables,
} from './../../../../../../federation/demarche-qualite/theme/types/GET_MINIMAL_SOUS_THEME';

import {
  GET_EXIGENCE as GET_EXIGENCE_TYPE,
  GET_EXIGENCEVariables,
} from './../../../../../../federation/demarche-qualite/theme/types/GET_EXIGENCE';

import {
  GET_THEME,
  GET_MINIMAL_SOUS_THEME,
  GET_EXIGENCE,
} from './../../../../../../federation/demarche-qualite/theme/query';

import {
  CREATE_EXIGENCE,
  UPDATE_EXIGENCE,
} from './../../../../../../federation/demarche-qualite/theme/mutation';

import {
  CREATE_EXIGENCE as CREATE_EXIGENCE_TYPE,
  CREATE_EXIGENCEVariables,
} from './../../../../../../federation/demarche-qualite/theme/types/CREATE_EXIGENCE';

import {
  UPDATE_EXIGENCE as UPDATE_EXIGENCE_TYPE,
  UPDATE_EXIGENCEVariables,
} from './../../../../../../federation/demarche-qualite/theme/types/UPDATE_EXIGENCE';

import {
  GET_OUTILS_GROUPED_BY_TYPOLOGIE as GET_OUTILS_GROUPED_BY_TYPOLOGIE_TYPE,
  GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie,
} from './../../../../../../federation/demarche-qualite/outil/types/GET_OUTILS_GROUPED_BY_TYPOLOGIE';

import { GET_OUTILS_GROUPED_BY_TYPOLOGIE } from './../../../../../../federation/demarche-qualite/outil/query';
import { FEDERATION_CLIENT } from './../../../apolloClientFederation';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';

const ExigenceForm: FC<RouteComponentProps> = ({ match: { params }, history }) => {
  const client = useApolloClient();

  const exigenceId = (params as any).id;
  const themeId = (params as any).idTheme;
  const sousThemeId = (params as any).idSousTheme;
  const isNewExigence = 'new' === exigenceId;

  console.log('****************************Params', exigenceId, themeId, sousThemeId)
  const [exigence, setExigence] = useState<IExigence>();
  const [displayNotificationPanel, setDisplayNotificationPanel] = useState<boolean>(false);

  const loadTheme = useQuery<GET_THEME_TYPE, GET_THEMEVariables>(GET_THEME, {
    variables: {
      id: themeId,
    },
    client: FEDERATION_CLIENT,
  });

  const loadSousTheme = useQuery<GET_MINIMAL_SOUS_THEME_TYPE, GET_MINIMAL_SOUS_THEMEVariables>(
    GET_MINIMAL_SOUS_THEME,
    {
      variables: {
        id: sousThemeId,
      },
      client: FEDERATION_CLIENT,
    },
  );

  const loadOutils = useQuery<
    GET_OUTILS_GROUPED_BY_TYPOLOGIE_TYPE,
    GET_OUTILS_GROUPED_BY_TYPOLOGIE_dqOutilsGroupedByTypologie
  >(GET_OUTILS_GROUPED_BY_TYPOLOGIE, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'network-only',
  });

  const [loadExigence, { called, loading, data, error }] = useLazyQuery<
    GET_EXIGENCE_TYPE,
    GET_EXIGENCEVariables
  >(GET_EXIGENCE, {
    variables: {
      id: exigenceId,
    },
    client: FEDERATION_CLIENT,
    fetchPolicy: 'network-only',
  });

  const [createExigence, creationExigence] = useMutation<
    CREATE_EXIGENCE_TYPE,
    CREATE_EXIGENCEVariables
  >(CREATE_EXIGENCE, {
    onError: () => {
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: isNewExigence
          ? `Une erreur s'est produite lors de l'ajout d'exigence.`
          : `Une erreur s'est produite lors de modification d'exigence.`,
        isOpen: true,
      });
    },
    onCompleted: () => {
      setDisplayNotificationPanel(true);
    },
    update: (cache, creationResult) => {
      if (creationResult.data && creationResult.data.createDQExigence) {
        const previousTheme = cache.readQuery<GET_THEME_TYPE, GET_THEMEVariables>({
          query: GET_THEME,
          variables: {
            id: themeId,
          },
        })?.dqTheme;

        const updatedTheme = {
          ...previousTheme,
          sousThemes: ((previousTheme as any).sousThemes || []).map((sousTheme: any) => {
            if (sousTheme.id === sousThemeId) {
              return {
                ...sousTheme,
                nombreExigences: (sousTheme.nombreExigences || 0) + 1,
                exigences: [creationResult.data?.createDQExigence, ...(sousTheme.exigences || [])],
              };
            }
            return sousTheme;
          }),
        };

        cache.writeQuery<GET_THEME_TYPE, GET_THEMEVariables>({
          query: GET_THEME,
          variables: {
            id: themeId,
          },
          data: {
            dqTheme: updatedTheme as any,
          },
        });
      }
    },
    client: FEDERATION_CLIENT,
  });

  const [updateExigence, modificationExigence] = useMutation<
    UPDATE_EXIGENCE_TYPE,
    UPDATE_EXIGENCEVariables
  >(UPDATE_EXIGENCE, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      setDisplayNotificationPanel(true);
    },
  });

  useEffect(() => {
    if (!isNewExigence) {
      if (!called) {
        loadExigence();
      } else if (data?.dqExigence) {
        setExigence(data.dqExigence as any); // FIXME : Fix graphql schema
      }
    }
  }, [exigenceId, loading]);

  const handleSave = (exigenceToSave: RequestSavingExigence) => {
    if (isNewExigence) {
      createExigence({
        variables: {
          idSousTheme: sousThemeId,
          exigenceInput: exigenceToSave,
        },
      });
    } else if (exigenceToSave.id) {
      updateExigence({
        variables: {
          id: exigenceToSave.id,
          exigenceInput: {
            ...(exigenceToSave as any),
            id: undefined,
          },
        },
      });
    }
  };

  const handleGoBack = () => {
    history.push(`/db/demarche_qualite/${themeId}`);
  };

  const isLoading =
    loadTheme.loading || loadSousTheme.loading || loadOutils.loading || (!isNewExigence && loading);
  const queryError =
    loadTheme.error ||
    loadSousTheme.error ||
    loadOutils.error ||
    (!isNewExigence ? error : undefined);

  const isSaving = isNewExigence ? creationExigence.loading : modificationExigence.loading;
  return displayNotificationPanel ? (
    <NotificationPanel
      src={Image}
      title={isNewExigence ? `Ajout d'exigence réussi` : `Modification d'exigence réussie`}
      subtitle={
        isNewExigence
          ? `L'exigence que vous venez de créer est maintenant dans la liste`
          : `L'exigence que vous venez de modifier est à jour.`
      }
    >
      <Button onClick={handleGoBack}>Retour</Button>
    </NotificationPanel>
  ) : (
    <ExigenceFormPage
      key={Math.random()}
      mode={isNewExigence ? 'creation' : 'modification'}
      loading={isLoading}
      saving={isSaving}
      error={queryError}
      theme={loadTheme.data?.dqTheme as any}
      sousTheme={loadSousTheme.data?.dqSousTheme as any}
      exigence={isNewExigence ? undefined : (exigence as any)}
      outils={loadOutils.data?.dqOutilsGroupedByTypologie as any}
      onRequestSave={handleSave}
      onRequestGoBack={handleGoBack}
    />
  );
};

export default withRouter(ExigenceForm);
