import { Box, Divider } from '@material-ui/core';
import React, { FC, useState, ChangeEvent } from 'react';
import useStyles from './style';
import { CustomButton } from '@app/ui-kit';
import AddIcon from '@material-ui/icons/Add';
import { RouteComponentProps, withRouter, useLocation } from 'react-router';
import ThemeAdd from '../ThemeAdd/ThemeAdd';
import { useApolloClient, useMutation } from '@apollo/client';
import { CREATE_SOUS_THEME } from '../../../../federation/demarche-qualite/theme/mutation';
import {
  CREATE_SOUS_THEMEVariables,
  CREATE_SOUS_THEME as CREATE_SOUS_THEME_TYPE,
} from '../../../../federation/demarche-qualite/theme/types/CREATE_SOUS_THEME';
import { FEDERATION_CLIENT } from '../apolloClientFederation';
import SousThemeComponent from './SousTheme/SousTheme';
import NoItemContentImage from '../../../Common/NoItemContentImage';
import { GET_THEME } from '../../../../federation/demarche-qualite/theme/query';
import {
  GET_THEME as GET_THEME_TYPE,
  GET_THEMEVariables,
} from '../../../../federation/demarche-qualite/theme/types/GET_THEME';
import { displaySnackBar } from '../../../../utils/snackBarUtils';

const DetailsTheme: FC<RouteComponentProps> = ({ history }) => {
  const classes = useStyles({});
  const location: any = useLocation();
  const state = location.state;
  const item = state && state.theme; // Type thème avec sous-thèmes

  const [show, setShow] = useState<boolean>(false);
  const [numeroOrdre, setNumeroOrdre] = useState<number | undefined>();
  const [nom, setNom] = useState<string>('');
  const reInit = () => {
    setNumeroOrdre(undefined);
    setNom('');
  };

  const client = useApolloClient();

  const handleNumeroOrdreChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setNumeroOrdre(parseInt((event.target as HTMLInputElement).value, 10));
  };

  const handleNomChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setNom((event.target as HTMLInputElement).value);
  };

  const [addSousTheme, creationSousTheme] = useMutation<
    CREATE_SOUS_THEME_TYPE,
    CREATE_SOUS_THEMEVariables
  >(CREATE_SOUS_THEME, {
    onError: () => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: `Une s'est produite lors de l'ajout du nouveau sous-thème`,
        isOpen: true,
      });
    },
    onCompleted: () => {
      setShow(false);
      reInit();
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: 'Le sous-thème a été ajouté avec succès !',
        isOpen: true,
      });
    },
    update: (cache, creationResult) => {
      if (creationResult.data && creationResult.data.createDQSousTheme) {
        const previousTheme = cache.readQuery<GET_THEME_TYPE, GET_THEMEVariables>({
          query: GET_THEME,
          variables: {
            id: item.id,
          },
        })?.dqTheme;

        const updatedTheme = {
          ...previousTheme,
          nombreSousThemes: ((previousTheme as any)?.sousThemes?.length || 0) + 1,
          sousThemes: [
            creationResult.data.createDQSousTheme,
            ...((previousTheme as any)?.sousThemes || []),
          ],
        };

        cache.writeQuery<GET_THEME_TYPE, GET_THEMEVariables>({
          query: GET_THEME,
          variables: {
            id: item.id,
          },
          data: {
            dqTheme: updatedTheme,
          } as any,
        });
      }
    },
    client: FEDERATION_CLIENT,
  });

  const handleChangeShow = () => {
    setShow(!show);
  };

  const handleSubmit = (): void => {
    addSousTheme({
      variables: {
        idTheme: item.id,
        sousThemeInput: {
          ordre: numeroOrdre,
          nom: nom,
        },
      },
    });
  };

  return (
    <>
      {state ? (
        <Box className={classes.root}>
          <Box className={classes.header}>
            <div>
              <span className={classes.spanBold}>{`${item.ordre}-${item.nom}`}</span>
            </div>
            <div>
              <CustomButton
                startIcon={<AddIcon />}
                children="Ajouter un sous-thème"
                color="secondary"
                className={classes.btn}
                onClick={handleChangeShow}
              />
              <ThemeAdd
                title={'Ajout de sous-thème'}
                saving={creationSousTheme.loading}
                show={show}
                setShow={setShow}
                onSubmit={handleSubmit}
                value1={numeroOrdre}
                value2={nom}
                setValue1={handleNumeroOrdreChange}
                setValue2={handleNomChange}
              />
            </div>
          </Box>
          <Divider />
          {item.sousThemes && item.sousThemes.length > 0 && (
            <SousThemeComponent sousThemes={item.sousThemes} themeId={item.id} />
          )}
        </Box>
      ) : (
        <NoItemContentImage
          title="Aucun thème selectionné"
          subtitle="Choisissez un en cliquant sur la partie gauche pour voir le contenu dans cette partie"
        />
      )}
    </>
  );
};

export default withRouter(DetailsTheme);
