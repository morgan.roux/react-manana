import React, { FC } from 'react';
import { SearchInput } from '../../Content/SearchInput';
import { Box } from '@material-ui/core';
import { useStyles } from '../style';
import CustomSelect from '../../../Common/CustomSelect';
import { ElementConfigItem } from './../services/basis-service';

interface SubHeaderFormProps {
  elementConfigs: ElementConfigItem[];
  selectedConfig: ElementConfigItem;
  search: string;
  hideSearchField?: boolean;
  setSearch(search: string): void;
  setModelName(modelName: string): void;
}

const SubHeader: FC<SubHeaderFormProps> = (props) => {
  const classes = useStyles({});
  const {
    search,
    setSearch,
    elementConfigs,
    selectedConfig,
    setModelName,
    hideSearchField,
  } = props;

  return (
    <Box className={classes.subHeader}>
      <CustomSelect
        label=""
        list={elementConfigs}
        listId="modelName"
        index="name"
        name="elementType"
        value={selectedConfig.modelName}
        onChange={(event) => setModelName(event.target.value)}
        required={false}
        disabled={false}
        style={{ backgroundColor: 'white', marginTop: 17 }}
      />
      {!hideSearchField && (
        <SearchInput
          value={search}
          onChange={(value) => setSearch(value)}
          placeholder={'Rechercher'}
        />
      )}
    </Box>
  );
};

export default SubHeader;
