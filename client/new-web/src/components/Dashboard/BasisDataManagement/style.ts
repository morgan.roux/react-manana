import { makeStyles, Theme, createStyles, lighten } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    tableContainer: {
      width: '95%',
      marginTop: 15,
    },
    container: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    table: {
      borderRadius: 0,
    },
    order: {
      color: theme.palette.common.white,
    },
    subHeader: {
      display: 'flex',
      height: 70,
      alignItems: 'center',
      justifyContent: 'space-between',
      padding: '0px 24px 0 24px',
      background: lighten(theme.palette.primary.main, 0.1),
      '@media (max-width: 692px)': {
        flexWrap: 'wrap',
        justifyContent: 'center',
      },
    },
    pagination: { alignSelf: 'flex-end', marginRight: 25 },
  }),
);
