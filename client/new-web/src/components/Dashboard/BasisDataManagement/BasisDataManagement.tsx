import React, { FC, useState, useEffect } from 'react';
import { useApolloClient } from '@apollo/client';

import { withRouter, RouteComponentProps } from 'react-router-dom';
import TableWithSearch from './TableWithSearch';
import TableWithFetchAll from './TableWithFetchAllQuery';
import TableWithFetchOffsetLimit from './TableWithFetchOffsetLimit';
import UpsertForm from './UpsertForm';
import { Box } from '@material-ui/core';
import SubHeader from './SubHeader';
import FormHeader from './FormHeader';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import {
  ElementConfigItem,
  getElementConfigs,
  createElement,
  updateElement,
  deleteElement,
} from './services/basis-service';

const BasisDataManagement: FC<RouteComponentProps> = ({
  location: { pathname },
  match: { params },
  history,
}) => {
  const client = useApolloClient();

  const elementId = (params as any).id;
  const elementModelName = (params as any).modelName;

  const onForm = !!elementId;
  const onCreateNewElement = elementId === 'new';

  const [search, setSearch] = useState<string>('');
  const [elementConfigs, setElementConfigs] = useState<ElementConfigItem[]>();
  const [saving, setSaving] = useState<boolean>(false);
  const [values, setValues] = useState({}); // We can use useReducer

  const selectedElementConfig =
    elementConfigs && elementConfigs.find((config) => config.modelName === elementModelName);
  const useFetchAllQuery = !!(selectedElementConfig && selectedElementConfig.type === 'FETCH_ALL');
  const useSearch = !!(selectedElementConfig && selectedElementConfig.type === 'SEARCH');
  const useFetchLimitOffset = !!(
    selectedElementConfig && selectedElementConfig.type === 'FETCH_OFFSET_LIMIT'
  );

  useEffect(() => {
    getElementConfigs().then((loadedConfigs) => {
      setElementConfigs(loadedConfigs);
      if (!elementModelName && loadedConfigs?.length > 0) {
        setModelName(loadedConfigs[0].modelName);
      }
    });
  }, []);

  const onElementToEditLoaded = (elementToEdit: any) => {
    const defaultValues = Object.keys(
      (selectedElementConfig || {}).fieldLabelsMapping || {},
    ).reduce((acc, currentFieldName) => {
      return {
        ...acc,
        [currentFieldName]: elementToEdit[currentFieldName],
      };
    }, {});

    setValues(defaultValues);
  };

  const goBack = () => {
    history.push(`/db/basis/${elementModelName}`);
  };

  const setModelName = (newModelName: string): void => {
    history.push(`/db/basis/${newModelName}`);
  };

  const handleSaveBtnClicked = () => {
    (onCreateNewElement
      ? createElement(elementModelName, values)
      : updateElement(elementId, elementModelName, values)
    )
      .then(() => {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'Enregistré avec succès',
          isOpen: true,
        });

        setSaving(false);
        goBack();
      })
      .catch((error) => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: "Une erreur s'est produite lors de l'enregistrement",
          isOpen: true,
        });
        setSaving(false);
      });
  };

  const handleDeleteBtnClicked = (id: string, modelName: string) => {
    deleteElement(id, modelName)
      .then(() => {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'Supprimé avec succès',
          isOpen: true,
        });

        goBack();

        // TODO : Update cache
        window.location.reload();
      })
      .catch((error) => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: "Une erreur s'est produite lors de suppression de l'enregistrement",
          isOpen: true,
        });
      });
  };

  return (
    <Box>
      {typeof elementConfigs === 'undefined' ? (
        'Chargement'
      ) : elementConfigs.length === 0 || !selectedElementConfig ? (
        'Non configuré'
      ) : (
        <>
          {onForm ? (
            <>
              <FormHeader
                saving={saving}
                title={onCreateNewElement ? 'Création' : 'Modification'}
                onSaveBtnClicked={handleSaveBtnClicked}
                onBackBtnClicked={goBack}
              />
              <UpsertForm
                elementConfig={selectedElementConfig}
                values={values}
                onChangeValues={setValues}
              />
            </>
          ) : (
            <SubHeader
              {...{
                elementConfigs,
                search,
                setSearch,
                selectedConfig: selectedElementConfig,
                setModelName,
              }}
              hideSearchField={useFetchAllQuery || useFetchLimitOffset}
            />
          )}

          <TableWithSearch
            search={search}
            elementConfig={selectedElementConfig}
            elementId={elementId}
            onCreateNewElement={onCreateNewElement}
            onForm={onForm}
            display={!onForm && useSearch}
            onElementToEditLoaded={onElementToEditLoaded}
            onDeleteBtnClicked={handleDeleteBtnClicked}
          />
          <TableWithFetchOffsetLimit
            elementConfig={selectedElementConfig}
            elementId={elementId}
            onCreateNewElement={onCreateNewElement}
            display={!onForm && useFetchLimitOffset}
            onElementToEditLoaded={onElementToEditLoaded}
            onDeleteBtnClicked={handleDeleteBtnClicked}
          />

          <TableWithFetchAll
            elementConfig={selectedElementConfig}
            elementId={elementId}
            onCreateNewElement={onCreateNewElement}
            onForm={onForm}
            display={!onForm && useFetchAllQuery}
            onElementToEditLoaded={onElementToEditLoaded}
            onDeleteBtnClicked={handleDeleteBtnClicked}
          />
        </>
      )}
    </Box>
  );
};

export default withRouter(BasisDataManagement);
