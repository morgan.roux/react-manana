import React, {
    FC
} from 'react';
import { CustomFormTextField } from '../../../Common/CustomTextField';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { ElementConfigItem } from './../services/basis-service'



interface UpsertFromProps {
    elementConfig: ElementConfigItem,
    values: any;
    onChangeValues: (values: any) => void
}


const UpsertForm: FC<UpsertFromProps & RouteComponentProps> = ({
    values,
    elementConfig,
    onChangeValues,
    match:{params}
}) => {
    

    const handleChange = (event) => {
        onChangeValues({
            ...(values || {}),
            [event.target.name]: event.target.value
        })
    }

    return (
        <div style={{padding:20}}>
            {
                Object.keys(elementConfig.fieldLabelsMapping)
                    .map(fieldName => {
                        return (
                            <CustomFormTextField
                                key={fieldName}
                                name={fieldName}
                                label={elementConfig.fieldLabelsMapping[fieldName]}
                                value={values && values[fieldName]}
                                onChange={handleChange}
                                type="text"
                            />
                        )
                    })
            }


        </div>
    );
};

export default withRouter(UpsertForm)
