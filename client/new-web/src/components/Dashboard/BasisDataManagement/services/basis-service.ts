import axios from 'axios';
import { GATEWAY_FEDERATION_URL } from './../../../../config';
import { getGroupement, getAccessToken, getPharmacie } from './../../../../services/LocalStorage';

// TODO : Use shared types
export interface ElementConfigItem {
  id: string;
  type: 'SEARCH' | 'FETCH_ALL' | 'FETCH_OFFSET_LIMIT';
  name: String;
  modelName: string;
  config: any;
  fieldLabelsMapping: any;
}

export interface Element {
  id: string;

  modelName: string;

  body: any;
}

export const getHeaders = (): any => {
  const groupement = getGroupement();
  const pharmacie = getPharmacie();
  const headerToken = getAccessToken();

  return {
    authorization: headerToken ? `Bearer ${headerToken}` : '',
    groupement: groupement ? groupement.id : '',
    pharmacie: pharmacie ? pharmacie.id : '',
  };
};

const fetchGraphQl = async ({ variables, query }: any): Promise<any> => {
  return axios({
    headers: getHeaders(),
    url: `${GATEWAY_FEDERATION_URL}/graphql`,
    method: 'post',
    data: {
      variables,
      query,
    },
  }).then(({ data }) => {
    return data.data;
  });
};

export const getElementConfigs = async (): Promise<ElementConfigItem[]> => {
  return fetchGraphQl({
    query: `
        query elementConfigs {
            elementConfigs {
              id
              type
              name
              modelName
              config
              fieldLabelsMapping
            }
          }
        `,
  }).then((data) => data.elementConfigs);
};

export const createElement = async (modelName: string, body: any): Promise<Element> => {
  return fetchGraphQl({
    variables: {
      modelName,
      body,
    },
    query: `
        mutation createElement ($modelName:String!,$body:JSON!){
            createElement(modelName:$modelName , body: $body) {
            id
            modelName
            body
          }
        }`,
  }).then((data) => data.createElement);
};

export const updateElement = async (id: string, modelName: string, body: any): Promise<Element> => {
  return fetchGraphQl({
    variables: {
      id,
      modelName,
      body,
    },
    query: `
        mutation updateElement ($id:ID!, $modelName:String!,$body:JSON!){
            updateElement(id:$id, modelName:$modelName , body: $body) {
            id
            modelName
            body
          }
        }`,
  }).then((data) => data.updateElement);
};

export const deleteElement = async (id: string, modelName: string): Promise<Element> => {
  return fetchGraphQl({
    variables: {
      id,
      modelName,
    },
    query: `
        mutation deleteElement ($id:ID!, $modelName:String!){
            deleteElement(id:$id, modelName:$modelName) {
            id
            modelName
            body
          }
        }`,
  }).then((data) => data.deleteElement);
};

export const searchElements = async (
  elementConfig: ElementConfigItem,
  offset: number,
  limit: number,
): Promise<{ nodes: any[]; totalCount: number }> => {
  if (!elementConfig.config || !elementConfig.config.query) {
    return { totalCount: 0, nodes: [] };
  }

  return fetchGraphQl({
    variables: {
      paging: {
        offset,
        limit,
      },
    },
    query: `
        query ${elementConfig.config.query} ($paging:OffsetPaging) {
            ${elementConfig.config.query} (paging:$paging) {
              totalCount
              nodes { id ${Object.keys(elementConfig.fieldLabelsMapping).join(' ')} }
            }
          }
        `,
  }).then((data) => data[elementConfig.config.query]);
};

export const getOneElement = async (
  id: any,
  elementConfig: ElementConfigItem,
): Promise<any | undefined> => {
  if (!elementConfig.config || !elementConfig.config.oneQuery || !id) {
    return undefined;
  }

  return fetchGraphQl({
    variables: {
      id,
    },
    query: `
        query ${elementConfig.config.oneQuery} ($id: ID!) {
            ${elementConfig.config.oneQuery} (id:$id) {
               id ${Object.keys(elementConfig.fieldLabelsMapping).join(' ')}
            }
          }
        `,
  }).then((data) => data[elementConfig.config.oneQuery]);
};

export const countElements = async (elementConfig: ElementConfigItem): Promise<number> => {
  if (!elementConfig.config || !elementConfig.config.aggregateQuery) {
    return 0;
  }

  return fetchGraphQl({
    query: `
        query ${elementConfig.config.aggregateQuery} {
            ${elementConfig.config.aggregateQuery}{
              count {
                id
              }
            }
          }
        `,
  }).then((data) => data[elementConfig.config.aggregateQuery].count.id);
};
