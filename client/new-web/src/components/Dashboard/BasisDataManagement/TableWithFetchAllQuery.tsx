import React, { FC, useState, useEffect } from 'react';

import TableDataManagement from './TableDataManagement';
import { ElementConfigItem } from './services/basis-service'
import { fetchAllElements } from './services/graphql-service'


interface BasisDataManagementWithFetchAllProps {
  elementConfig: ElementConfigItem
  elementId: any;
  onCreateNewElement: boolean;
  onForm: boolean;
  display: boolean;
  onElementToEditLoaded: (elementToEdit: any) => void
  onDeleteBtnClicked: (id: string, type: string) => void
}


const BasisDataManagementWithFetchAll: FC<BasisDataManagementWithFetchAllProps> = ({
  elementConfig,
  elementId,
  onCreateNewElement,
  onForm,
  display,
  onElementToEditLoaded,
  onDeleteBtnClicked

}) => {


  const [data, setData] = useState<any[]>([])

  useEffect(() => {

    if (!onCreateNewElement && elementConfig && elementConfig.type==='FETCH_ALL') {
      fetchAllElements(elementConfig)
        .then((data: any[]) => {

          if (!onForm && display) {
            setData(data)
          }
          else {
            const elementToEdit = data.find(item => item.id === elementId)
            if (elementToEdit) {
              onElementToEditLoaded(elementToEdit)
            }
          }


        })
    }


  }, [elementId])



  return display ? (
    <TableDataManagement elementConfig={elementConfig} data={data} onDeleteBtnCliked={onDeleteBtnClicked} />

  ) : null;
};

export default BasisDataManagementWithFetchAll;
