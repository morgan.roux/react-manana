import { useApolloClient, useLazyQuery, useMutation } from '@apollo/react-hooks';
import { OperationMarketingPage } from '@app/ui-kit';
import React, { FC, useEffect, useState } from 'react';
import {
  CREATE_MISE_AVANT,
  DELETE_MISE_AVANT,
  UPDATE_MISE_AVANT,
} from '../../../federation/partenaire-service/OperationMarketing/mutation';
import { GET_MISE_AVANTS } from '../../../federation/partenaire-service/OperationMarketing/query';
import {
  CREATE_MISE_AVANT as CREATE_MISE_AVANT_TYPE,
  CREATE_MISE_AVANTVariables,
} from '../../../federation/partenaire-service/OperationMarketing/types/CREATE_MISE_AVANT';
import {
  DELETE_MISE_AVANT as DELETE_MISE_AVANT_TYPE,
  DELETE_MISE_AVANTVariables,
} from '../../../federation/partenaire-service/OperationMarketing/types/DELETE_MISE_AVANT';
import {
  GET_MISE_AVANTS as GET_MISE_AVANTS_TYPE,
  GET_MISE_AVANTSVariables,
} from '../../../federation/partenaire-service/OperationMarketing/types/GET_MISE_AVANTS';
import {
  UPDATE_MISE_AVANT as UPDATE_MISE_AVANT_TYPE,
  UPDATE_MISE_AVANTVariables,
} from '../../../federation/partenaire-service/OperationMarketing/types/UPDATE_MISE_AVANT';
import { getPharmacie } from '../../../services/LocalStorage';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import { FEDERATION_CLIENT } from '../DemarcheQualite/apolloClientFederation';
// import {commonFieldComponent} from './../../Common/CommonFieldsForm';

const OperationMarketing: FC = () => {
  const [miseEnAvantToEdit, setMiseEnAvantToEdit] = useState<any>();
  const client = useApolloClient();
  const pharmacie = getPharmacie();

  const [saving, setSaving] = useState<boolean>(false);

  const [loadMiseAvant, loadingMiseAvant] = useLazyQuery<
    GET_MISE_AVANTS_TYPE,
    GET_MISE_AVANTSVariables
  >(GET_MISE_AVANTS, { client: FEDERATION_CLIENT });

  const [createMiseAvant, creatingMiseAvant] = useMutation<
    CREATE_MISE_AVANT_TYPE,
    CREATE_MISE_AVANTVariables
  >(CREATE_MISE_AVANT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La Mise en avant a été créee avec success',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingMiseAvant?.refetch();
      setSaving(false);
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs sont survenues pendant la création de la Mise en avant',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [updateMiseAvant, updatingMiseAvant] = useMutation<
    UPDATE_MISE_AVANT_TYPE,
    UPDATE_MISE_AVANTVariables
  >(UPDATE_MISE_AVANT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La Mise en avant a été modifiée avec succes',
        type: 'SUCCESS',
        isOpen: true,
      });
      data.refetch();
      setSaving(false);
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs sont survenues pendant la modification de la Mise en avant',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [deleteMiseAvant, deletingMiseAvant] = useMutation<
    DELETE_MISE_AVANT_TYPE,
    DELETE_MISE_AVANTVariables
  >(DELETE_MISE_AVANT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La Mise en avant a été supprimé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingMiseAvant.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression de la Mise en avant',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const data = (loadingMiseAvant.data?.pRTMiseAvants.nodes as any) || [];

  useEffect(() => {
    if (data) {
      loadMiseAvant({
        variables: {
          filter: {
            idPharmacie: {
              eq: pharmacie.id,
            },
          },
        },
      });
    }
  }, [data]);

  const handleRequestSave = (data: any) => {
    if (!data.id) {
      createMiseAvant({
        variables: {
          input: {
            pRTMiseAvant: {
              libelle: data.libelle,
              code: data.code,
              couleur: data.couleur,
            },
          },
        },
      });
      return;
    }
    if (data.id) {
      updateMiseAvant({
        variables: {
          update: {
            id: data.id,
            update: {
              code: data.code,
              libelle: data.libelle,
              couleur: data.couleur,
            },
          },
        },
      });
      return;
    }
  };

  const handleRequestEdit = (data: any) => {
    setMiseEnAvantToEdit(data);
  };

  const handleRequestDelete = (data: any) => {
    if (data.id) {
      deleteMiseAvant({
        variables: {
          input: {
            id: data.id,
          },
        },
      });
    }
  };
  return (
    <>
      <OperationMarketingPage
        loading={loadingMiseAvant.loading}
        saving={deletingMiseAvant.loading || creatingMiseAvant.loading || updatingMiseAvant.loading}
        rowsTotal={data.length}
        data={data}
        miseEnAvantToEdit={miseEnAvantToEdit}
        onRequestSave={handleRequestSave}
        onRequestEdit={handleRequestEdit}
        onRequestDelete={handleRequestDelete}
      />
    </>
  );
};

export default OperationMarketing;
