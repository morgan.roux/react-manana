import { Theme, createStyles, makeStyles, lighten } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
    },
    searchBar: {
      top: 0,
      width: '100%',
      opacity: 1,
      zIndex: 10,
      position: 'sticky',
      display: 'flex',
      height: 70,
      alignItems: 'center',
      justifyContent: 'space-between',
      padding: '0px 24px 0 24px',
      background: lighten(theme.palette.primary.main, 0.1),
      '@media (max-width: 692px)': {
        flexWrap: 'wrap',
        justifyContent: 'center',
      },
      marginBottom: theme.spacing(6),
    },
    search: {
      display: 'flex',
      alignItems: 'center',
      '@media (max-width: 580px)': {
        marginBottom: 15,
      },
    },
    image: {
      cursor: 'zoom-in',
      maxHeight: 60,
    },
    searchBarTitle: {
      color: theme.palette.common.white,
      fontSize: 20,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
    },
    deleteButton: {
      background: '#E0E0E0',
      color: theme.palette.common.black,
      marginRight: theme.spacing(3),
    },
  }),
);

export default useStyles;
