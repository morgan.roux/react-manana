import React, { FC, useState } from 'react';
import useStyles from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Edit, Delete } from '@material-ui/icons';
import { IconButton, Tooltip } from '@material-ui/core';
import CreationProduit from './CreationProduit';

interface GestionProduitsProps {}

const GestionProduits: FC<GestionProduitsProps & RouteComponentProps> = ({
  // history,
  location: { pathname },
}) => {
  const classes = useStyles({});
  const [allValues, setAllValues] = useState<any>({ item: { codeItem: 'K' } });
  const [openPhoto, setOpenPhoto] = useState<boolean>(false);
  const [currentPhoto, setCurrentPhoto] = useState<any>();
  const isOnCreate = pathname && pathname.includes('/create');

  const zoomImage = (photo: string) => {
    setCurrentPhoto(photo);
    setOpenPhoto(true);
  };

  /* const imageColumn = [
    {
      name: '',
      label: 'Photo',
      renderer: (value: any) => {
        return value &&
          value.articlePhoto &&
          value.articlePhoto.fichier &&
          value.articlePhoto.fichier.publicUrl ? (
          <img
            className={classes.image}
            onClick={() => zoomImage(value.articlePhoto.fichier)}
            src={value.articlePhoto.fichier}
          />
        ) : (
          <img className={classes.image} src={''} />
        );
      },
    },
  ];

  const controlColumns = [
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <Tooltip title="Modifier">
            <IconButton>
              <Edit />
            </IconButton>
          </Tooltip>
        );
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <Tooltip title="Supprimer">
            <IconButton>
              <Delete />
            </IconButton>
          </Tooltip>
        );
      },
    },
  ]; */

  /* if (isOnCreate) {
    return <CreationProduit />;
  } else */

  return null;
};

export default withRouter(GestionProduits);
