import { useQuery } from '@apollo/client';
import { Tab, Tabs } from '@material-ui/core';
import React, { FC, useEffect } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { SEARCH } from '../../../../../../graphql/search/query';
import { ProduitCanalInput } from '../../../../../../types/graphql-global-types';
import Backdrop from '../../../../../Common/Backdrop';
import { CanalArticleInterface } from '../../Interface/CanalArticleInterface';
import Form from './Form';
import useStyles from './styles';

interface CanauxCommandesProps {
  setNextBtnDisabled: (disables: boolean) => void;
  allValues: any;
  setAllValues: (values: any) => void;
  isEdit?: boolean;
  createUpdateProduit: () => void;
}

interface CustomTabPanelInterface {
  index: number;
  currentTab: number;
}

const CanauxCommandes: FC<CanauxCommandesProps & RouteComponentProps> = ({
  history,
  setNextBtnDisabled,
  allValues,
  setAllValues,
  isEdit,
  createUpdateProduit,
}) => {
  const classes = useStyles({});
  const [tab, setTab] = React.useState(0);

  const commandeCanalResult = useQuery(SEARCH, { variables: { type: ['commandecanal'] } });
  console.log('allValues', allValues);

  useEffect(() => {
    if (
      commandeCanalResult &&
      commandeCanalResult.data &&
      commandeCanalResult.data.search &&
      commandeCanalResult.data.search.data
    ) {
      const produitCanaux: ProduitCanalInput[] = (allValues && allValues.produitCanaux) || [];

      commandeCanalResult.data.search.data.map((commandeCanal: any) => {
        if (
          !produitCanaux.some(
            canalArticle => canalArticle && canalArticle.codeCommandeCanal === commandeCanal.code,
          )
        ) {
          console.log('Canal empty ===> ', commandeCanal.code);

          produitCanaux.push({
            codeCommandeCanal: commandeCanal.code,
            isRemoved: true,
            qteMin: 0,
          });
        }
      });

      if (!allValues || (allValues && !allValues.activeCommandeCanal))
        setAllValues(prevState => ({
          ...prevState,
          produitCanaux,
          activeCommandeCanal: commandeCanalResult.data.search.data[0],
        }));

      const actives =
        allValues && allValues.activeCommandeCanal
          ? produitCanaux
          : produitCanaux.filter(produitCanal => produitCanal.isRemoved === false);

      if (actives.length) {
        const canals =
          (allValues &&
            allValues.activeCommandeCanal &&
            commandeCanalResult.data.search.data.filter(
              commandeCanal => commandeCanal.code === allValues.activeCommandeCanal.code,
            )) ||
          (commandeCanalResult &&
            commandeCanalResult.data &&
            commandeCanalResult.data.search &&
            commandeCanalResult.data.search.data &&
            commandeCanalResult.data.search.data.filter(
              commandeCanal => commandeCanal.code === actives[0].codeCommandeCanal,
            ));

        if (canals && canals.length) {
          const index =
            commandeCanalResult &&
            commandeCanalResult.data &&
            commandeCanalResult.data.search &&
            commandeCanalResult.data.search.data &&
            commandeCanalResult.data.search.data.indexOf(canals[0]);
          setAllValues(prevState => ({
            ...prevState,
            activeCommandeCanal: canals[0],
          }));

          setTab(index);
        }
      } else {
        setNextBtnDisabled(true);
      }
    }
  }, [commandeCanalResult]);

  useEffect(() => {
    if (
      commandeCanalResult &&
      commandeCanalResult.data &&
      commandeCanalResult.data.search &&
      commandeCanalResult.data.search.data &&
      allValues &&
      allValues.produitCanaux &&
      !allValues.produitCanaux.length
    ) {
      const produitCanaux: ProduitCanalInput[] = (allValues && allValues.produitCanaux) || [];

      commandeCanalResult.data.search.data.map((commandeCanal: any) => {
        if (
          !produitCanaux.some(
            canalArticle => canalArticle && canalArticle.codeCommandeCanal === commandeCanal.code,
          )
        ) {
          console.log('Canal empty ===> ', commandeCanal.code);

          produitCanaux.push({
            codeCommandeCanal: commandeCanal.code,
            isRemoved: true,
            qteMin: 0,
          });
        }
      });
    }
  }, [commandeCanalResult, allValues]);

  useEffect(() => {
    const produitCanaux: ProduitCanalInput[] = (allValues && allValues.produitCanaux) || [];
    const actives = produitCanaux.filter(produitCanal => produitCanal.isRemoved === false);
    if (actives.length) {
      setNextBtnDisabled(false);
    }
  }, [allValues]);

  const handleTabsChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setTab(newValue);
  };

  const handleCanalCommandeChange = (commandeCanal: any) => {
    setAllValues(prevState => ({
      ...prevState,
      activeCommandeCanal: commandeCanal,
    }));
  };

  const currentCanalArticle: CanalArticleInterface & ProduitCanalInput =
    allValues &&
    allValues.produitCanaux &&
    allValues.produitCanaux.find(
      canalArticle =>
        allValues &&
        allValues.activeCommandeCanal &&
        allValues.activeCommandeCanal.code === canalArticle.codeCommandeCanal,
    );

  return (
    <div className={classes.root}>
      {commandeCanalResult.loading && <Backdrop />}
      <div className={classes.container}>
        <Tabs
          value={tab}
          onChange={handleTabsChange}
          centered={true}
          classes={{ root: classes.tabs }}
        >
          {commandeCanalResult &&
            commandeCanalResult.data &&
            commandeCanalResult.data.search &&
            commandeCanalResult.data.search.data &&
            commandeCanalResult.data.search.data.map((commandeCanal: any) => (
              <Tab
                key={`commandeCanal-${commandeCanal.id}`}
                classes={{ selected: classes.selectedTab, root: classes.tab }}
                label={commandeCanal.libelle}
                onClick={() => handleCanalCommandeChange(commandeCanal)}
              />
            ))}
        </Tabs>

        <Form
          tab={tab}
          setNextBtnDisabled={setNextBtnDisabled}
          allValues={allValues}
          setAllValues={setAllValues}
          canalArticle={currentCanalArticle}
          disabled={!isEdit}
          createUpdateProduit={createUpdateProduit}
        />
      </div>
    </div>
  );
};

export default withRouter(CanauxCommandes);
