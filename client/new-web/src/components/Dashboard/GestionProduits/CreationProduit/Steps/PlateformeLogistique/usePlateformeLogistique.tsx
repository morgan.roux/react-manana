import { useState, ChangeEvent, FormEvent } from 'react';
import { PlateformeLogistiqueInterface } from '../../Interface/PlateformeLogistiqueInterface';
const usePlateformeLogistique = (defaultState?: PlateformeLogistiqueInterface) => {
  const initialState: PlateformeLogistiqueInterface = defaultState || {
    qteStock: 0,
    stv: 0,
    unitePetitCond: 0,
    pxAchatArticle: 0,
    pxVenteArticle: 0,
    datePeremption: null,
    dateCreation: null,
    dateModification: null,
    gammeCommercial: null,
    sousGammeCommercial: null,
    idCanalSousGamme: '',
    codeSousGammeCatalogue: '',
    gammeCatalogue: null,
    sousGammeCatalogue: null,
  };

  const [values, setValues] = useState<PlateformeLogistiqueInterface>(initialState);

  const numberFields = ['qteStock', 'stv', 'unitePetitCond', 'pxAchatArticle', 'pxVenteArticle'];
  const checkboxFields = ['generiqArticle', 'carteFidelite'];

  const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement | any>) => {
    if (e && e.target) {
      const { name, value, checked } = e.target;
      setValues(prevState => ({
        ...prevState,
        ...defaultState,
        [name]: typeof value === 'string' ? value.trimLeft() : value,
      }));
      if (checkboxFields.includes(name)) {
        setValues(prevState => ({ ...prevState, ...defaultState, [name]: checked }));
      }
      if (numberFields.includes(name)) {
        const parsed = parseInt(value, 10);
        setValues(prevState => ({
          ...prevState,
          ...defaultState,
          [name]: isNaN(parsed) ? 0 : parsed,
        }));
      }
    }
  };

  const handleChangeDate = (name: string) => (date: any) => {
    if (date && name) {
      setValues(prevState => ({ ...prevState, [name]: date }));
    }
  };

  const handleChangeAutocomplete = (
    e: ChangeEvent<any>,
    inputValue: any,
    name: string,
    input: string,
    typename: string,
    variable?: string,
    key?: string,
    isNumber?: boolean,
  ) => {
    if (e && e.type === 'click' && inputValue && inputValue.id) {
      setValues(prevState => ({ ...prevState, ...defaultState, [name]: inputValue }));
      if (variable)
        if (isNumber) {
          setValues(prevState => ({
            ...prevState,
            ...defaultState,
            [variable]: parseInt(inputValue[key || variable]),
          }));
        } else {
          setValues(prevState => ({
            ...prevState,
            ...defaultState,
            [variable]: inputValue[key || variable],
          }));
        }
    }
    if (e && e.type === 'change' && inputValue !== undefined && typeof inputValue === 'string') {
      setValues(prevState => ({
        ...prevState,
        ...defaultState,
        [name]: { id: '', [input]: inputValue, __typename: typename },
      }));
    }
  };

  return {
    handleChange,
    handleChangeAutocomplete,
    handleChangeDate,
    values,
    setValues,
  };
};

export default usePlateformeLogistique;
