import { Theme, createStyles, makeStyles, lighten } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'center',
    },
    container: {
      width: 815,
      maxWidth: 815,
    },
    inlineContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    subTitle: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 18,
      color: theme.palette.secondary.main,
    },
  }),
);

export default useStyles;
