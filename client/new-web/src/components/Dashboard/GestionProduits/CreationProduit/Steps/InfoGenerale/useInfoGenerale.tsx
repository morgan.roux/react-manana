import { useState, ChangeEvent, FormEvent, useEffect } from 'react';
import { InfoGeneraleInterface } from '../../Interface/InfoGeneraleInterface';

const useInfoGenerale = (defaultState?: InfoGeneraleInterface) => {
  const initialState: InfoGeneraleInterface = defaultState || {
    libelle: '',
    codeReference: '',
    typeCodeReferent: 0,
    isGenerique: 0,
    codeTva: 0,
    codeLibelleStockage: 0,
    acte: null,
    codeActe: 0,
    idLaboDistributaire: '',
    idLaboExploitant: '',
    idLaboTitulaire: '',
    laboDistributaire: null,
    laboExploitant: null,
    laboTitulaire: null,
    selectedFiles: [],
    fichierImage: null,
    pxAchat: 0,
    pxVente: 0,
    dateSuppression: null,
    codeFamille: '',
    codeTauxss: 0,
    idCategorie: 0,
    codeListe: 0,
  };

  const [values, setValues] = useState<InfoGeneraleInterface>(initialState);

  if (defaultState && defaultState.idProduit && !values.idProduit) {
    setValues(defaultState);
  }

  const floatFields = ['pxAchat', 'pxVente'];
  const numberFields = ['hospitalArticle'];
  const checkboxFields = ['isGenerique', 'carteFidelite'];

  const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement | any>) => {
    if (e && e.target) {
      const { name, value, checked } = e.target;
      setValues(prevState => ({
        ...prevState,
        ...defaultState,
        [name]: typeof value === 'string' ? value.trimLeft() : value,
      }));
      if (checkboxFields.includes(name)) {
        const value = values[name] === 0 ? 1 : 0;
        setValues(prevState => ({ ...prevState, ...defaultState, [name]: value }));
      }
      if (numberFields.includes(name)) {
        const parsed = parseInt(value);
        setValues(prevState => ({
          ...prevState,
          ...defaultState,
          [name]: isNaN(parsed) ? 0 : parsed,
        }));
      }
      if (floatFields.includes(name)) {
        const formated = value && value.replace(',', '.');
        const parsed = formated && !formated.endsWith('.') ? parseFloat(formated) : formated;
        setValues(prevState => ({
          ...prevState,
          ...defaultState,
          [name]: isNaN(formated) ? 0 : parsed,
        }));
      }
    }
  };

  const handleFileSelection = (value: any) => {
    console.log('value file ! ', value);
    const uniqueFile: any = value && value.length && value[value.length - 1];
    setValues(prevState => ({
      ...prevState,
      selectedFiles: [uniqueFile],
      fichierImage: uniqueFile,
    }));
  };

  const handleChangeAutocomplete = (
    e: ChangeEvent<any>,
    inputValue: any,
    name: string,
    input: string,
    typename: string,
    variable?: string,
    key?: string,
  ) => {
    if (e && e.type === 'click' && inputValue && inputValue.id) {
      if (variable) {
        setValues(prevState => ({
          ...prevState,
          ...defaultState,
          [name]: inputValue,
          [variable]: inputValue[key || variable],
        }));
      } else {
        setValues(prevState => ({ ...prevState, ...defaultState, [name]: inputValue }));
      }
    }
    if (e && e.type === 'change' && inputValue !== undefined && typeof inputValue === 'string') {
      setValues(prevState => ({
        ...prevState,
        ...defaultState,
        [name]: { id: '', [input]: inputValue, __typename: typename },
      }));
    }
  };

  const handleFileDeletion = () => {
    setValues(prevState => ({ ...prevState, selectedFiles: [], fichierImage: null }));
  };

  const handleChangeDate = (name: string) => (date: any) => {
    if (date && name) {
      setValues(prevState => ({ ...prevState, [name]: date }));
    }
  };

  return {
    handleChange,
    handleChangeAutocomplete,
    handleFileSelection,
    handleFileDeletion,
    handleChangeDate,
    values,
    setValues,
  };
};

export default useInfoGenerale;
