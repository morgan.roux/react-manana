import { useState, ChangeEvent, FormEvent, useEffect } from 'react';
import { CanalArticleInterface } from '../../../Interface/CanalArticleInterface';
import { ProduitCanalInput } from '../../../../../../../types/graphql-global-types';
const useForm = (defaultState?: ProduitCanalInput & CanalArticleInterface, allValues?: any) => {
  const initialState: ProduitCanalInput & CanalArticleInterface = defaultState || {
    qteStock: 0,
    qteMin: 0,
    stv: 0,
    unitePetitCond: 0,
    prixFab: 0,
    prixPhv: 0,
    datePeremption: null,
    dateCreation: null,
    dateModification: null,
    dateRetrait: null,
    idCanalSousGamme: null,
    codeSousGammeCatalogue: 0,
    codeCommandeCanal: null,
    isRemoved: true,
    laboratoire: null,
    gammeCommercial: null,
    sousGammeCommercial: null,
    gammeCatalogue: null,
    sousGammeCatalogue: null,
  };

  const [values, setValues] = useState<ProduitCanalInput & CanalArticleInterface>(initialState);
  //console.log('values : ', values);

  // if (defaultState && defaultState !== values) {
  //   setValues(defaultState);
  // }

  const numberFields = ['qteStock', 'stv', 'unitePetitCond'];
  const checkboxFields = ['isRemoved'];
  const floatFields = ['prixFab', 'prixPhv'];

  const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement | any>) => {
    if (e && e.target) {
      if (defaultState && defaultState !== values) {
        setValues(defaultState);
      }
      const { name, value, checked } = e.target;
      setValues(prevState => ({
        ...prevState,
        ...defaultState,
        [name]: typeof value === 'string' ? value.trimLeft() : value,
      }));
      if (checkboxFields.includes(name)) {
        const value = defaultState && defaultState[name] === false ? true : false;
        setValues(prevState => ({ ...prevState, ...defaultState, [name]: value }));
      }
      if (numberFields.includes(name)) {
        const parsed = parseInt(value, 10);
        setValues(prevState => ({
          ...prevState,
          ...defaultState,
          [name]: isNaN(parsed) ? 0 : parsed,
        }));
      }
      if (floatFields.includes(name)) {
        const formated = value && value.replace(',', '.');
        const parsed = formated && !formated.endsWith('.') ? parseFloat(formated) : formated;
        setValues(prevState => ({
          ...prevState,
          ...defaultState,
          [name]: isNaN(formated) ? 0 : parsed,
        }));
      }
    }
  };

  const activeCanal = (value: boolean) => {
    setValues(prevState => ({
      ...prevState,
      ...defaultState,
      isRemoved: value,
    }));
  };

  const handleFileSelection = (value: any) => {
    setValues(prevState => ({ ...prevState, selectedFiles: value, fichierImage: value }));
  };

  const handleChangeAutocomplete = (
    e: ChangeEvent<any>,
    inputValue: any,
    name: string,
    input: string,
    typename: string,
    variable?: string,
    key?: string,
    isNumber?: boolean,
  ) => {
    if (e && e.type === 'click' && inputValue && inputValue.id && defaultState) {
      if (variable) {
        if (isNumber) {
          setValues({
            ...defaultState,
            [name]: inputValue,
            [variable]: parseInt(inputValue[key || variable]),
          });
        } else {
          setValues({
            ...defaultState,
            [name]: inputValue,
            [variable]: inputValue[key || variable],
          });
        }
      } else {
        setValues({ ...defaultState, [name]: inputValue });
      }
    }
    if (e && e.type === 'change' && inputValue !== undefined && typeof inputValue === 'string') {
      setValues(prevState => ({
        ...prevState,
        ...defaultState,
        [name]: { id: '', [input]: inputValue, __typename: typename },
      }));
    }
  };

  const handleFileDeletion = () => {
    setValues(prevState => ({ ...prevState, selectedFiles: [], fichierImage: null }));
  };

  const handleChangeDate = (name: string) => (date: any) => {
    if (date && name) {
      setValues(prevState => ({ ...prevState, ...defaultState, [name]: date }));
    }
  };

  return {
    handleChange,
    handleChangeAutocomplete,
    handleFileSelection,
    handleFileDeletion,
    handleChangeDate,
    values,
    setValues,
    activeCanal,
  };
};

export default useForm;
