import { FichierInput } from '../../../../../types/graphql-global-types';

export interface InfoGeneraleInterface {
  idProduit?: string;
  libelle: string;
  dateSuppression: any;
  isGenerique: number;
  selectedFiles: File[];
  fichierImage: FichierInput | null;
  idCategorie: number;
  typeCodeReferent: number;
  codeReference: string;
  codeFamille: string;
  acte: any;
  codeActe: number;
  laboTitulaire: any;
  laboExploitant: any;
  laboDistributaire: any;
  idLaboTitulaire: string;
  idLaboExploitant: string;
  idLaboDistributaire: string;
  pxVente: number;
  pxAchat: number;
  codeTauxss: number;
  codeLibelleStockage: number;
  codeTva: number;
  codeListe: number;
}
