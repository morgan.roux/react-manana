import React, { FC, useState, useEffect, Fragment } from 'react';
import useStyles from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import Stepper from './stepProduitt';
import InfoGenerale from './Steps/InfoGenerale';
import { useMutation, useApolloClient, useQuery } from '@apollo/client';
import {
  createUpdateArticle,
  createUpdateArticleVariables,
} from '../../../../graphql/Produit/types/createUpdateArticle';
import { CREATE_UPDATE_ARTICLE } from '../../../../graphql/Produit/mutation';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../graphql/S3';
import { TypeFichier, ProduitCanalInput } from '../../../../types/graphql-global-types';
import { formatFilename } from '../../../../utils/filenameFormater';
import { uploadToS3 } from '../../../../services/S3';
import { Loader } from '../../Content/Loader';
import CanauxCommandes from './Steps/CanauxCommandes';
import { CanalArticleInterface } from './Interface/CanalArticleInterface';
import { GET_ARTICLE } from '../../../../graphql/Produit/query';
import { PRODUIT, PRODUITVariables } from '../../../../graphql/Produit/types/PRODUIT';
import Backdrop from '../../../Common/Backdrop';
import ConfirmDialog from '../../../Common/ConfirmDialog';

interface CreationProduitProps {
  match: { params: { id: string } };
  openCloseModal: (isOpen: boolean) => void;
  setIsAfterCreateUpdate?: (isOpen: boolean) => void;
  idProduit?: string;
}

const CreationProduit: FC<CreationProduitProps & RouteComponentProps> = ({
  history,
  match,
  openCloseModal,
  setIsAfterCreateUpdate,
  idProduit,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [nextBtnDisabled, setNextBtnDisabled] = useState<boolean>(true);

  const [loading, setLoading] = useState<boolean>(false);
  const [openModal, setOpenaModal] = useState<boolean>(false);
  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [isCreate, setIsCreate] = useState<boolean>(idProduit ? false : true);
  const [currentStep, setCurrentStep] = useState<number>(0);
  const [resetChange, setResetChange] = useState<boolean>(false);
  const [isFinale, setIsFinale] = useState<boolean>(false);

  /* const idProduit = match && match.params && match.params.id; */

  const title = idProduit ? 'Modification de produit' : 'Ajout de produit';
  const [allValues, setAllValues] = useState<any>(
    idProduit
      ? null
      : {
        isGenerique: 0,
        unitePetitCond: 0,
        pxVente: 0,
        pxAchat: 0,
        codeFamille: '',
        codeTauxss: 0,
        dateSuppression: null,
        fichierImage: null,
        selectedFiles: [],
      },
  );

  const articleResult = useQuery<PRODUIT, PRODUITVariables>(GET_ARTICLE, {
    variables: {
      id: idProduit || '',
    },
    skip: !idProduit ? true : false,
    fetchPolicy: 'cache-and-network',
  });

  useEffect(() => {
    if (articleResult && articleResult.data && articleResult.data.produit) {
      const article = articleResult.data.produit;
      if (article) {
        setAllValues(prev => ({ resetChange, ...prev, ...initValues(article) }));
      }
    }
  }, [articleResult, resetChange]);

  useEffect(() => {
    if (resetChange) {
      if (!currentStep) articleResult.refetch();
      setResetChange(false);
    }
  }, [resetChange]);

  const initValues = (article: any) => {
    return {
      idProduit: article.id,
      dateSuppression: article.supprimer,
      pxVente: article.produitTechReg && article.produitTechReg.prixVenteTTC,
      pxAchat: article.produitTechReg && article.produitTechReg.prixAchatHT,
      libelle: article.libelle,
      codeReference: article.produitCode && article.produitCode.code,
      typeCodeReferent: article.produitCode && article.produitCode.typeCode,
      codeTva:
        article.produitTechReg && article.produitTechReg.tva && article.produitTechReg.tva.codeTva,
      codeLibelleStockage:
        article.produitTechReg &&
        article.produitTechReg.libelleStockage &&
        article.produitTechReg.libelleStockage.codeInfo,
      acte: article.produitTechReg && article.produitTechReg.acte,
      codeActe:
        article.produitTechReg &&
        article.produitTechReg.acte &&
        article.produitTechReg.acte.codeActe,
      codeListe:
        article.produitTechReg &&
        article.produitTechReg.liste &&
        article.produitTechReg.liste.codeListe,
      codeTauxss:
        article.produitTechReg &&
        article.produitTechReg.tauxss &&
        article.produitTechReg.tauxss.codeTaux,
      idCategorie: article.categorie && article.categorie.resipIdCategorie,
      laboTitulaire: article.produitTechReg && article.produitTechReg.laboTitulaire,
      laboExploitant: article.produitTechReg && article.produitTechReg.laboExploitant,
      laboDistributaire: article.produitTechReg && article.produitTechReg.laboDistirbuteur,
      idLaboTitulaire:
        article.produitTechReg &&
        article.produitTechReg.laboTitulaire &&
        article.produitTechReg.laboTitulaire.id,
      idLaboExploitant:
        article.produitTechReg &&
        article.produitTechReg.laboExploitant &&
        article.produitTechReg.laboExploitant.id,
      idLaboDistributaire:
        article.produitTechReg &&
        article.produitTechReg.laboDistirbuteur &&
        article.produitTechReg.laboDistirbuteur.id,
      famille: article.famille,
      codeFamille: article.famille && article.famille.codeFamille,
      fichierImage:
        (article.produitPhoto &&
          article.produitPhoto.fichier && {
          type: article.produitPhoto.fichier.type,
          nomOriginal: article.produitPhoto.fichier.nomOriginal,
          chemin: article.produitPhoto.fichier.chemin,
        }) ||
        null,
      selectedFiles:
        (article.produitPhoto &&
          article.produitPhoto.fichier && [
            {
              type: 'image/png',
              name: article.produitPhoto.fichier.nomOriginal,
              publicUrl: article.produitPhoto.fichier.publicUrl,
            },
          ]) ||
        [],
      produitCanaux:
        article.canauxArticle &&
        article.canauxArticle.map(canalArticle => ({
          ...canalArticle,
          codeCommandeCanal:
            canalArticle && canalArticle.commandeCanal && canalArticle.commandeCanal.code,
          gammeCommercial:
            canalArticle &&
            canalArticle.sousGammeCommercial &&
            canalArticle.sousGammeCommercial.gammeCommercial,
          idCanalSousGamme:
            canalArticle && canalArticle.sousGammeCommercial && canalArticle.sousGammeCommercial.id,
          gammeCatalogue:
            canalArticle &&
            canalArticle.sousGammeCatalogue &&
            canalArticle.sousGammeCatalogue.gammeCatalogue,
          codeSousGammeCatalogue:
            canalArticle &&
            canalArticle.sousGammeCatalogue &&
            canalArticle.sousGammeCatalogue.codeSousGamme,
        })),
    };
  };

  const getVarialbesValues = (allValues: any) => {
    let variables = allValues;
    delete variables.resetChange;
    delete variables.acte;
    delete variables.famille;
    delete variables.gammeCatalogue;
    delete variables.gammeCommercial;
    delete variables.laboratoire;
    delete variables.selectedFiles;
    delete variables.sousGammeCatalogue;
    delete variables.sousGammeCommercial;
    delete variables.activeCommandeCanal;
    delete variables.laboDistributaire;
    delete variables.laboExploitant;
    delete variables.laboTitulaire;
    const produitCanaux =
      variables &&
      variables.produitCanaux &&
      variables.produitCanaux.map((canalArticle: ProduitCanalInput & CanalArticleInterface) => {
        const variables: any = canalArticle;
        delete variables.laboratoire;
        delete variables.gammeCommercial;
        delete variables.sousGammeCommercial;
        delete variables.gammeCatalogue;
        delete variables.sousGammeCatalogue;
        delete variables.commandeCanal;
        delete variables.__typename;
        return variables;
      });

    variables = { ...variables, produitCanaux };
    return variables as createUpdateArticleVariables;
  };

  const [doCreateUpdateProduit, doCreateUpdateProduitResult] = useMutation<
    createUpdateArticle,
    createUpdateArticleVariables
  >(CREATE_UPDATE_ARTICLE, {
    onCompleted: data => {
      if (data && data.createUpdateProduit) {
        setLoading(false);
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Produit ${allValues && allValues.idProduit ? 'modifié' : 'créée'} avec succès`,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);

        // articleResult.refetch();
        if (isCreate) {
          openCloseModal(false);
        }

        setIsFinale(true);
        if (isEdit) setIsEdit(false);
        if (setIsAfterCreateUpdate) setIsAfterCreateUpdate(true);
      }
    },
    onError: errors => {
      setLoading(false);
      displaySnackBar(client, {
        isOpen: true,
        type: 'ERROR',
        message: errors.message,
      });
    },
  });

  const sendFileToAWS = async (item: any, fileObject: any, typeFichier: TypeFichier) => {
    if (fileObject && fileObject.file && item && item.presignedUrl) {
      return uploadToS3(fileObject.file, item.presignedUrl).then(_ => ({
        chemin: item && item.filePath,
        nomOriginal: fileObject.file.name,
        type: typeFichier,
      }));
    }
  };

  const [doCreatePutPresignedUrl, doCreatePutPresignedUrlResult] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (data && data.createPutPresignedUrls && data.createPutPresignedUrls.length > 0) {
        sendFileToAWS(
          data.createPutPresignedUrls[0],
          { file: allValues.fichierImage },
          TypeFichier.PHOTO,
        ).then(async fichierImage => {
          console.log('fichier ==>', fichierImage);
          const variables = getVarialbesValues({ ...allValues, fichierImage: fichierImage });
          return doCreateUpdateProduit({ variables });
        });
      }
    },
    onError: error => {
      setLoading(false);
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const createUpdateProduit = () => {
    setLoading(true);
    const fichierImage = allValues && allValues.selectedFiles && allValues.selectedFiles[0];
    if (fichierImage && !fichierImage.publicUrl) {
      const filePath = formatFilename(fichierImage);
      doCreatePutPresignedUrl({ variables: { filePaths: [filePath] } });
    } else {
      const variables = getVarialbesValues({ ...allValues });
      doCreateUpdateProduit({ variables });
    }
  };

  const steps = [
    {
      title: 'Données économiques',
      content: (
        <InfoGenerale
          setNextBtnDisabled={setNextBtnDisabled}
          allValues={allValues}
          setAllValues={setAllValues}
          isEdit={isCreate ? true : isEdit}
          resetChange={resetChange}
          setResetChange={setResetChange}
        />
      ),
    },
    {
      title: 'Canaux de commandes',
      content: (
        <CanauxCommandes
          setNextBtnDisabled={setNextBtnDisabled}
          allValues={allValues}
          setAllValues={setAllValues}
          isEdit={isCreate ? true : isEdit}
          createUpdateProduit={createUpdateProduit}
        />
      ),
    },
  ];

  const finalStep = {
    buttonLabel: idProduit ? 'MODIFIER PRODUIT' : 'AJOUTER PRODUIT',
    action: createUpdateProduit,
  };

  const backToHome = () => {
    openCloseModal(false);
  };

  const handleOpenCloseModal = (open: boolean) => () => {
    setOpenaModal(open);
  };

  const setActiveEdit = (edit: boolean) => () => {
    setIsEdit(edit);
  };

  if (articleResult && articleResult.loading) {
    return <Loader />;
  }

  const isLoading = (): boolean => {
    if (
      (doCreatePutPresignedUrlResult && doCreatePutPresignedUrlResult.loading) ||
      (doCreateUpdateProduitResult && doCreateUpdateProduitResult.loading) ||
      loading
    ) {
      return true;
    }
    return false;
  };

  return (
    <Fragment>
      {isLoading() && (
        <Backdrop value={`${idProduit ? 'Modification' : 'Ajout'} de l'article en cours ...`} />
      )}
      <Stepper
        title={title}
        steps={steps}
        backToHome={!isEdit ? () => openCloseModal(false) : handleOpenCloseModal(true)}
        disableNextBtn={nextBtnDisabled}
        fullWidth={false}
        finalStep={finalStep}
        isEdit={isEdit}
        setEdit={setActiveEdit}
        isCreate={isCreate}
        currentStep={currentStep}
        setCurrentStep={setCurrentStep}
        setResetChange={setResetChange}
        isFinale={isFinale}
        setIsFinale={setIsFinale}
        setAllValues={setAllValues}
      />
      <ConfirmDialog
        open={openModal}
        message={'Voulez-vous retourner à la liste des produits ?'}
        handleClose={handleOpenCloseModal(false)}
        handleValidate={backToHome}
      />
    </Fragment>
  );
};

export default withRouter(CreationProduit);
