import React, { useEffect, useState, useContext } from 'react';
import CustomContent from '../../Common/newCustomContent';
import SubToolbar from '../../Common/newCustomContent/SubToolbar';
import useStyles from './styles';
import {
  useCheckedGroupeClient,
  useGroupeClientColumns,
  useButtonHeadAction,
  useCreateGroupeClient,
  useGetGroupeClient,
} from './utils';
import CustomButton from '../../Common/CustomButton';
import { Add } from '@material-ui/icons';
import { RouteComponentProps, withRouter } from 'react-router';
import NoItemContentImage from '../../Common/NoItemContentImage';
import mutationSuccessImg from '../../../assets/img/mutation-success.png';
import { ConfirmDeleteDialog } from '../../Common/ConfirmDialog';
import SearchInput from '../../Common/newCustomContent/SearchInput';
import { GROUPES_CLIENTS_URL } from '../../../Constant/url';
import GroupeClientForm from './GroupeClientForm';
import useGroupeClientForm from './GroupeClientForm/usePharmacieForm';
import { useCheckedsPharmacie } from '../TitulairesPharmacies/utils';
import Backdrop from '../../Common/Backdrop';
import { useApolloClient } from '@apollo/client';
import { ContentStateInterface, ContentContext } from '../../../AppContext';

interface GroupeClientProps {
  listResult: any;
}

const GroupeClient: React.FC<GroupeClientProps & RouteComponentProps> = ({
  listResult,
  history: { push },
  location: { pathname },
  match: { params },
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  const [
    columns,
    deleteRow,
    onClickConfirmDelete,
    deleteLoading,
    selected,
    setSelected,
  ] = useGroupeClientColumns(setOpenDeleteDialog);

  const checkedsGroupeClient = useCheckedGroupeClient();

  const { handleChange, values, setValues, handleChangeDate } = useGroupeClientForm();

  const checkedsPharmacie = useCheckedsPharmacie();

  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const [operationNameState, setOperationName] = useState<any>();
  const [variablesState, setVariables] = useState<any>();

  useEffect(() => {
    if (operationName && !operationNameState) {
      setOperationName(operationName);
      setVariables(variables);
    }
  }, [operationName]);

  const [createUpdateGroupeClient, mutationSuccess, mutationLoading] = useCreateGroupeClient(
    values,
    operationNameState,
    variablesState,
  );

  const [goToAddGroupeClient, goBack] = useButtonHeadAction(push);
  const [getGroupeClient, groupeClient] = useGetGroupeClient();

  const isOnList = pathname === `/db/${GROUPES_CLIENTS_URL}`;
  const isOnCreate = pathname === `/db/${GROUPES_CLIENTS_URL}/create`;
  const isOnEdit: boolean = pathname.startsWith(`/db/${GROUPES_CLIENTS_URL}/edit`);

  const mutationSuccessTitle = `${isOnCreate ? 'Création' : 'Modification'
    } d'un groupe de client réussie`;

  const mutationSuccessSubTitle = `Le groupe de client que vous venez de ${isOnCreate ? 'créer' : 'modifier'
    } est maintenant dans la liste.`;

  const loadingMsg = `${isOnCreate ? 'Création' : isOnEdit ? 'Modification' : 'Chargement'
    } en cours...`;

  const { groupeClientId } = params as any;
  const EDIT_URL = `/db/${GROUPES_CLIENTS_URL}/edit/${groupeClientId}`;

  const goToList = () => push(`/db/${GROUPES_CLIENTS_URL}`);

  /**
   * Set checked pharmacie to valuses
   */
  useEffect(() => {
    if (checkedsPharmacie) {
      const idPharmacies = checkedsPharmacie.map(i => i.id);
      setValues(prevState => ({ ...prevState, idPharmacies }));
    }
  }, [checkedsPharmacie]);

  /**
   * Get groupe client on edit
   */
  useEffect(() => {
    if (pathname === EDIT_URL && groupeClientId) {
      getGroupeClient({ variables: { id: groupeClientId } });
    }
  }, [pathname, groupeClientId]);

  /**
   * Set values on edit
   */
  useEffect(() => {
    if (groupeClient) {
      const idPharmacies =
        (groupeClient.pharmacies && (groupeClient.pharmacies.map(i => i && i.id) as string[])) ||
        [];
      setValues({
        id: groupeClient.id,
        nom: groupeClient.nomGroupe || '',
        dateValiditeDebut: groupeClient.dateValiditeDebut,
        dateValiditeFin: groupeClient.dateValiditeFin,
        idPharmacies,
      });

        // TODO: Migration
      /*(client as any).writeData({
        data: { checkedsPharmacie: idPharmacies.map(id => ({ id, __typename: 'Pharmacie' })) },
      });*/
    }
  }, [groupeClient]);

  const subToolbarTitle = isOnCreate
    ? `Ajout de groupe de client`
    : isOnEdit
      ? `Modification de groupe de client`
      : 'Liste des groupes de client';

  const onClickDefaultBtn = () => {
    if (!isOnList) {
      goBack();
      return;
    }
    setOpenDeleteDialog(true);
  };

  const onClickSecondaryBtn = () => {
    if (!isOnList) {
      createUpdateGroupeClient();
      return;
    }
    goToAddGroupeClient();
  };

  const SubToolbarChildren = () => {
    return (
      <div className={classes.childrenRoot}>
        {((checkedsGroupeClient && checkedsGroupeClient.length > 0) ||
          selected.length > 0 ||
          !isOnList) && (
            <CustomButton color="default" onClick={onClickDefaultBtn}>
              {!isOnList ? 'Annuler' : 'Supprimer la sélection'}
            </CustomButton>
          )}
        <CustomButton
          color="secondary"
          startIcon={!isOnList ? null : <Add />}
          onClick={onClickSecondaryBtn}
        >
          {isOnCreate ? 'Enregistrer' : isOnEdit ? 'Modifier' : 'Ajouter un groupe de client'}
        </CustomButton>
      </div>
    );
  };

  const listGroupeClient = (
    <CustomContent
      selected={selected}
      setSelected={setSelected}
      // checkedItemsQuery={{
      //   name: 'checkedsGroupeClient',
      //   type: 'groupeclient',
      //   query: GET_CHECKEDS_GROUPE_CLIENT,
      // }}
      {...{ listResult, columns }}
      isSelectable={true}
    />
  );

  const MutationSuccessComponent = () => {
    return (
      <div className={classes.mutationSuccessContainer}>
        <NoItemContentImage
          src={mutationSuccessImg}
          title={mutationSuccessTitle}
          subtitle={mutationSuccessSubTitle}
        >
          <CustomButton color="default" onClick={goBack}>
            Retour à la liste
          </CustomButton>
        </NoItemContentImage>
      </div>
    );
  };

  const DeleteDialogContent = () => {
    if (deleteRow) {
      return (
        <span>
          Êtes-vous sur de vouloir supprimer
          <span style={{ fontWeight: 'bold' }}> {deleteRow.nomGroupe || ''} </span>?
        </span>
      );
    }
    return <span>Êtes-vous sur de vouloir supprimer ces groupes de client ?</span>;
  };

  const finalStepBtn = {
    buttonLabel: isOnEdit ? 'Modifier' : 'Ajouter',
    action: () => {
      onClickSecondaryBtn();
    },
  };

  return (
    <div className={classes.container}>
      {(mutationLoading || deleteLoading) && (
        <Backdrop value={deleteLoading ? 'Suppression en cours...' : loadingMsg} />
      )}
      {isOnList && (
        <>
          <SubToolbar
            title={subToolbarTitle}
            dark={!isOnList}
            withBackBtn={!isOnList}
            onClickBack={goToList}
          >
            <SubToolbarChildren />
          </SubToolbar>
          <div className={classes.searchInputBox}>
            <SearchInput searchPlaceholder="Rechercher un groupe de client" />
          </div>
        </>
      )}
      {mutationSuccess ? (
        <MutationSuccessComponent />
      ) : !isOnList ? (
        <GroupeClientForm
          title={subToolbarTitle}
          values={values}
          handleChangeInput={handleChange}
          handleChangeDate={handleChangeDate}
          isOnCreate={isOnCreate}
          isOnEdit={isOnEdit}
          finalStepBtn={finalStepBtn}
        />
      ) : (
        <>{listGroupeClient}</>
      )}
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
    </div>
  );
};

export default withRouter(GroupeClient);
