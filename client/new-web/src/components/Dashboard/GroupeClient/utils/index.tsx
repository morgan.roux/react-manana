import React, { useState, useContext, SetStateAction, Dispatch, useMemo } from 'react';
import {
  useQuery,
  useApolloClient,
  QueryLazyOptions,
  useMutation,
  useLazyQuery,
} from '@apollo/client';
import TableActionColumn from '../../TableActionColumn';
import { Column } from '../../../Common/newCustomContent/interfaces';
import { ContentStateInterface, ContentContext } from '../../../../AppContext';
import { GROUPES_CLIENTS_URL } from '../../../../Constant/url';
import {
  GET_CHECKEDS_GROUPE_CLIENT,
  DO_CREATE_UPDATE_GROUPE_CLIENT,
  GET_GROUPE_CLIENT,
} from '../../../../graphql/GroupeClient';
import moment from 'moment';
import { differenceBy } from 'lodash';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import {
  CREATE_UPDATE_GROUPE_CLIENT,
  CREATE_UPDATE_GROUPE_CLIENTVariables,
} from '../../../../graphql/GroupeClient/types/CREATE_UPDATE_GROUPE_CLIENT';
import {
  GROUPE_CLIENT,
  GROUPE_CLIENTVariables,
  GROUPE_CLIENT_groupeClient,
} from '../../../../graphql/GroupeClient/types/GROUPE_CLIENT';
import { DO_DELETE_SOFT_GROUPES_CLIENTS } from '../../../../graphql/GroupeClient/mutation';
import {
  DELETE_SOFT_GROUPES_CLIENTS,
  DELETE_SOFT_GROUPES_CLIENTSVariables,
} from '../../../../graphql/GroupeClient/types/DELETE_SOFT_GROUPES_CLIENTS';

/**
 *  Create groupe client hooks
 */
export const useCreateGroupeClient = (
  values: CREATE_UPDATE_GROUPE_CLIENTVariables,
  operationName: any,
  variables: any,
): [() => void, boolean, boolean] => {
  const client = useApolloClient();
  const [mutationSuccess, setMutationSuccess] = useState<boolean>(false);

  const [createUpdateGroupeClient, { loading: mutationLoading }] = useMutation<
    CREATE_UPDATE_GROUPE_CLIENT,
    CREATE_UPDATE_GROUPE_CLIENTVariables
  >(DO_CREATE_UPDATE_GROUPE_CLIENT, {
    variables: { ...values },
    update: (cache, { data }) => {
      if (data && data.createUpdateGroupeClient && values && !values.id) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables,
          });
          if (req && req.search && req.search.data) {
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    total: req.search.total + 1,
                    data: [...req.search.data, data.createUpdateGroupeClient],
                  },
                },
              },
              variables,
            });
          }
        }
      }
    },
    onCompleted: data => {
      if (data && data.createUpdateGroupeClient) {
        setMutationSuccess(true);
          // TODO: Migration

        /*(client as any).writeData({
          data: { checkedsPharmacie: null },
        });*/
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
      // displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
    },
  });

  return [createUpdateGroupeClient, mutationSuccess, mutationLoading];
};

/**
 *  Delete groupe client hooks
 */
export const useDeleteGroupeClient = (
  selected: any[],
  setSelected: Dispatch<SetStateAction<any[]>>,
  deleteRow: any,
  setDeleteRow: Dispatch<SetStateAction<any>>,
): [() => void, boolean] => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  // const checkeds = useCheckedGroupeClient();
  const client = useApolloClient();

  const selectedIds = (selected.length > 0 && selected.map(i => i.id)) || [];

  const [deleteGroupesClients, { loading, data: deleteData }] = useMutation<
    DELETE_SOFT_GROUPES_CLIENTS,
    DELETE_SOFT_GROUPES_CLIENTSVariables
  >(DO_DELETE_SOFT_GROUPES_CLIENTS, {
    variables: { ids: selectedIds },
    update: (cache, { data }) => {
      if (data && data.deleteSoftGroupeClients) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables,
          });
          if (req && req.search && req.search.data) {
            const source = req.search.data;
            const result = data.deleteSoftGroupeClients;
            const dif = differenceBy(source, result, 'id');
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: dif,
                  },
                },
              },
              variables,
            });
              // TODO: Migration

            /*(client as any).writeData({
              data: { checkedsGroupeClient: differenceBy(selected, result, 'id') },
            });*/
          }
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
    onCompleted: data => {
      if (data && data.deleteSoftGroupeClients) {
        displaySnackBar(client, { isOpen: true, type: 'SUCCESS', message: 'Suppression réussie' });
        setSelected([]);
        setDeleteRow(null);
      }
    },
  });

  return [deleteGroupesClients, loading];
};

/**
 *  list checked groupe client hooks
 */
export const useCheckedGroupeClient = () => {
  const checkedsGroupeClient = useQuery(GET_CHECKEDS_GROUPE_CLIENT);
  return (
    checkedsGroupeClient &&
    checkedsGroupeClient.data &&
    checkedsGroupeClient.data.checkedsGroupeClient
  );
};

/**
 *  Subtoolbar button action hooks
 */
export const useButtonHeadAction = (push: any) => {
  const goToAddGroupeClient = () => push(`/db/${GROUPES_CLIENTS_URL}/create`);
  const goBack = () => push(`/db/${GROUPES_CLIENTS_URL}`);

  return [goToAddGroupeClient, goBack];
};

/**
 *  get groupe client hooks
 */
export const useGetGroupeClient = (): [
  (options?: QueryLazyOptions<GROUPE_CLIENTVariables> | undefined) => void,
  GROUPE_CLIENT_groupeClient | null | undefined,
] => {
  const client = useApolloClient();
  const [getGroupeClient, { data, loading: groupeClientLoading }] = useLazyQuery<
    GROUPE_CLIENT,
    GROUPE_CLIENTVariables
  >(GET_GROUPE_CLIENT, {
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        });
      });
    },
  });

  const groupeClient = data && data.groupeClient;
  return [getGroupeClient, groupeClient];
};

export const useGroupeClientColumns = (
  setOpenDeleteDialog: Dispatch<SetStateAction<boolean>>,
): [Column[], any, () => void, boolean, any[], Dispatch<SetStateAction<any[]>>] => {
  const [selected, setSelected] = useState<any[]>([]);
  const [deleteRow, setDeleteRow] = useState<any>(null);

  const [deleteGroupeClients, deleteLoading] = useDeleteGroupeClient(
    deleteRow ? [deleteRow] : selected,
    setSelected,
    deleteRow,
    setDeleteRow,
  );

  // const checkedsGroupeClient = useCheckedGroupeClient();
  // const client = useApolloClient();

  /**
   * Set null deleteRow if selected length > 0
   */
  useMemo(() => {
    if (selected && selected.length > 0) {
      if (deleteRow !== null) setDeleteRow(null);
    }
  }, [selected]);

  const onClickDelete = (row: any) => {
    setDeleteRow(row);
  };

  const onClickConfirmDelete = () => {
    setOpenDeleteDialog(false);
    if (selected.length > 0) {
      deleteGroupeClients();
    }

    if (deleteRow && deleteRow.id) {
      deleteGroupeClients();
    }
  };

  const columns = [
    {
      label: 'Code',
      name: 'codeGroupe',
      renderer: (row: any) => {
        return row.codeGroupe ? row.codeGroupe : '-';
      },
    },
    {
      label: 'Nom du groupe',
      name: 'nomGroupe',
      renderer: (row: any) => {
        return row.nomGroupe ? row.nomGroupe : '-';
      },
    },
    {
      label: 'Date de début de validité',
      name: 'dateValiditeDebut',
      renderer: (row: any) => {
        return row.dateValiditeDebut ? moment(row.dateValiditeDebut).format('DD/MM/YYYY') : '-';
      },
    },
    {
      label: 'Date de fin de validité',
      name: 'dateValiditeFin',
      renderer: (row: any) => {
        return row.dateValiditeFin ? moment(row.dateValiditeFin).format('DD/MM/YYYY') : '-';
      },
    },
    {
      label: 'Nombre de Pharmacie',
      name: '',
      renderer: (row: any) => {
        return row.pharmacies ? row.pharmacies.length : '-';
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: any) => {
        return (
          <TableActionColumn
            row={row}
            baseUrl={GROUPES_CLIENTS_URL}
            setOpenDeleteDialog={setOpenDeleteDialog}
            handleClickDelete={onClickDelete}
          />
        );
      },
    },
  ];

  return [columns, deleteRow, onClickConfirmDelete, deleteLoading, selected, setSelected];
};
