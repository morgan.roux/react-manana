import React, { FC, Fragment } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { Typography } from '@material-ui/core';
import CustomSelect from '../../../../Common/CustomSelect';
import { CustomDatePicker } from '../../../../Common/CustomDateTimePicker';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import { GroupeClientFormProps, FormInputInterface, InputInterface } from '../GroupeClientForm';
import useCommonStyles from '../../../commonStyles';
import useStyles from '../styles';
import classnames from 'classnames';

const GroupeClientFormStepOne: FC<GroupeClientFormProps & RouteComponentProps> = ({
  values,
  handleChangeInput,
  handleChangeDate,
  // isOnCreate,
  // isOnEdit,
  // title,
}) => {
  const commonClasses = useCommonStyles();
  const classes = useStyles({});
  const { nom, dateValiditeDebut, dateValiditeFin } = values;

  const FORM_INPUTS: FormInputInterface[] = [
    {
      title: 'Informations',
      inputs: [
        {
          label: 'Nom',
          placeholder: 'Nom du groupe',
          type: 'text',
          name: 'nom',
          value: nom,
          inputType: 'text',
          selectOptions: [],
        },
        {
          label: 'Date de début de validité',
          placeholder: '00/00/0000',
          type: 'date',
          name: 'dateValiditeDebut',
          value: dateValiditeDebut,
          inputType: undefined,
        },
        {
          label: 'Date de fin de validité',
          placeholder: '00/00/0000',
          type: 'date',
          name: 'dateValiditeFin',
          value: dateValiditeFin,
          inputType: undefined,
        },
      ],
    },
  ];

  return (
    <div className={classnames(commonClasses.formRoot, classes.groupeClientFormRoot)}>
      <div className={commonClasses.formContainer}>
        {FORM_INPUTS.map((item: FormInputInterface, index: number) => {
          return (
            <Fragment key={`groupe_client_form_item_${index}`}>
              <Typography className={commonClasses.inputTitle}>{item.title}</Typography>
              <div className={commonClasses.inputsContainer}>
                {item.inputs.map((input: InputInterface, inputIndex: number) => {
                  return (
                    <div
                      className={commonClasses.formRow}
                      key={`pharmacie_form_input_${inputIndex}`}
                    >
                      <Typography className={commonClasses.inputLabel}>
                        {input.label} <span>*</span>
                      </Typography>
                      {input.type === 'select' ? (
                        <CustomSelect
                          label=""
                          list={input.selectOptions || []}
                          listId="id"
                          index="value"
                          name={input.name}
                          value={input.value}
                          onChange={handleChangeInput}
                          shrink={false}
                          placeholder={input.placeholder}
                          withPlaceholder={true}
                        />
                      ) : input.type === 'date' ? (
                        <CustomDatePicker
                          placeholder={input.placeholder}
                          onChange={handleChangeDate(input.name)}
                          name={input.name}
                          value={input.value}
                          minDate={input.name === 'dateValiditeFin' ? dateValiditeDebut : undefined}
                        />
                      ) : (
                        <CustomFormTextField
                          name={input.name}
                          value={input.value}
                          onChange={handleChangeInput}
                          placeholder={input.placeholder}
                          type={input.inputType}
                        />
                      )}
                    </div>
                  );
                })}
              </div>
            </Fragment>
          );
        })}
      </div>
    </div>
  );
};

export default withRouter(GroupeClientFormStepOne);
