import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { GroupeClientFormProps } from '../GroupeClientForm';
import { PharmacieTable } from '../../../../Common/newWithSearch/ComponentInitializer';
import useCommonStyles from '../../../commonStyles';
import useStyles from '../styles';
import classnames from 'classnames';

const GroupeClientFormStepTwo: FC<GroupeClientFormProps & RouteComponentProps> = (
  {
    // values,
    // handleChangeInput,
    // handleChangeDate,
    // isOnCreate,
    // isOnEdit,
    // title,
  },
) => {
  const commonClasses = useCommonStyles();
  const classes = useStyles({});

  return (
    <div className={classnames(commonClasses.formRoot, classes.groupeClientFormRoot)}>
      <PharmacieTable />
    </div>
  );
};

export default withRouter(GroupeClientFormStepTwo);
