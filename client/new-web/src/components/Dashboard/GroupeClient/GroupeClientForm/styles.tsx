import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    groupeClientFormRoot: {
      width: '100%',
      marginTop: '-30px !important',
    },
  }),
);

export default useStyles;
