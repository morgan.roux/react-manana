import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: '100%',
      padding: '16px 0',
    },
    interlocuteurContainer: {
      width: '100%',
      marginTop: 16,
      marginBottom: 24,
    },
    interlocuteurInputContainer: {},
    interlocuteurHeader: {
      width: '100%',
    },
    interlocuteurLabel: {
      width: '100%',
      display: 'flex',
      justifyContent: 'space-between',
      marginBottom: 8,
    },
    actionRechercheAvancee: {
      color: theme.palette.error.dark,
      cursor: 'pointer',
      '&:hover': {
        color: theme.palette.error.light,
      },
    },
    msgInterlocuteur: {
      color: theme.palette.grey[600],
      marginBottom: 16,
    },
    collaborateurField: {
      '& .MuiMenuItem-root': {
        minHeight: '40px!important',
      },

      '&.MuiListItem-root.Mui-selected': {
        minHeight: '40px !important',
        '&:hover': {
          minHeight: '40px !important',
        },
      },
      [theme.breakpoints.down('md')]: {
        margin: 0,
        width: '100%',
      },
    },
    collaborateurInput: {
      marginTop: -6,
      height: 57,
      borderRadius: 5,
      border: `1px solid ${theme.palette.grey[500]}`,
      color: theme.palette.grey[700],
      display: 'flex',
      width: '100%',
      '& legend': {
        marginLeft: 12,
      },
    },
    collaborateurBox: {
      display: 'flex',
      justifyContent: 'space-between',
      width: '100%',
    },
    collaborateurItem: {
      display: 'flex',
      flexDirection: 'row',
      paddingLeft: 8,
      alignItems: 'center',
    },
    textFieldContainer: {
      width: '100%',
      minHeight: 150,
    },
    fonctionContainer: {
      margin: '12px 0 0 0',
    },
    actionModal: {
      width: '100%',
      marginTop: -40,
      display: 'flex',
      justifyContent: 'flex-end',
    },
    actionModalContact: {
      width: '100%',
      display: 'flex',
      justifyContent: 'flex-end',
      marginTop: 16,
    },
  }),
);

export default useStyles;
