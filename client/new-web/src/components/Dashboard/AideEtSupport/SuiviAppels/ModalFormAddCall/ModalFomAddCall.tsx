import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import {
  CustomButton,
  CustomEditorText,
  CustomFormTextField,
  CustomModal,
  Dropzone,
} from '@app/ui-kit';
import { Avatar, Box, Grid, IconButton } from '@material-ui/core';
import useStyles from './styles';
import { PersonAdd } from '@material-ui/icons';
import AssignTaskUserModal from '../../../../Common/CustomSelectUser';
import { isMobile, stringToAvatar } from '../../../../../utils/Helpers';
import { uniqBy } from 'lodash';
import { Alert, AlertTitle, AvatarGroup } from '@material-ui/lab';
import ProjectInput from '../../../../Common/ProjectInput';
import UrgenceInput from '../../../../Common/UrgenceInput';
import ImportanceInput from '../../../../Common/ImportanceInput';
import { UserSelect } from '../../../../User/UserSelect';
import CustomAutocomplete from '../../../../Common/CustomAutocomplete';
import { GET_CUSTOM_CONTENT_SEARCH } from '../../../../../graphql/search/query';
import {
  CUSTOM_CONTENT_SEARCH,
  CUSTOM_CONTENT_SEARCHVariables,
} from '../../../../../graphql/search/types/CUSTOM_CONTENT_SEARCH';
import { useLazyQuery } from '@apollo/client';
import { ConsoleLine } from 'mdi-material-ui';
export interface Data {
  id?: string;
  idImportance?: string;
  idUrgence?: string;
  commentaire?: string;
  concernesIds?: string[];
  idTache?: string | null;
  idFonction?: string | null;
  idUserDeclarant?: string;
  idUserInterlocuteur?: string;
  fichiers?: File[] | null;
  concernes?: any[];
  interlocuteur?: any;
}

export interface ModalFormAddCallProps {
  mode: 'ajout' | 'modification';
  onOpen: () => void;
  open: boolean;
  onSave: (data: Data) => void;
  value?: Data;
}

export const ModalFormAddCall: FC<ModalFormAddCallProps> = ({
  mode,
  onOpen,
  open,
  onSave,
  value,
}) => {
  const styles = useStyles();
  const [assignedUsers, setAssingedUsers] = useState<any[]>([]);
  const [assignParticipant, setAssignParticipant] = useState(false);
  const [fichiersJoints, setFichiersJoints] = useState<File[]>([]);
  const [input, setInput] = useState<Data | undefined>();
  const [tache, setTache] = useState<any>();
  const [openContact, setOpenContact] = useState<boolean>(false);
  const [userIds, setUserIds] = useState<any[]>([]);
  const [filtersModal, setFiltersModal] = useState<string[]>(['ALL']);
  const [usersSelected, setUsersSelected] = useState<any[]>([]);
  const [idInterlocuteur, setIdInterlocuteur] = useState<string | undefined>();
  const [optionsAutoComplete, setOptionsAutoComplte] = useState<any[]>([]);
  const [valueAuto, setValueAuto] = useState<any>();
  const [fonction, setFonction] = useState<any>({
    idTache: '',
    idFonction: '',
  });
  const [errorForm, setErrorForm] = useState<boolean>(false);
  const [valueTel, setValueTel] = useState<string>('');

  const [search, searchResult] = useLazyQuery<
    CUSTOM_CONTENT_SEARCH,
    CUSTOM_CONTENT_SEARCHVariables
  >(GET_CUSTOM_CONTENT_SEARCH, {
    onCompleted: data => {
      setOptionsAutoComplte(data.search?.data || []);
    },
  });

  const handleOpenContact = () => {
    setOpenContact(prev => !prev);
  };

  const handleSelectedCollaborateurs = (selected: any) => {
    setAssingedUsers(selected);
    setTache(undefined);
    setFonction({ idTache: '', idFonction: '' });
  };

  const onAutocompleteChange = (inputValue: any) => {
    setValueAuto(inputValue);
    setIdInterlocuteur(inputValue.id);
    setValueTel(
      inputValue.contact
        ? inputValue.contact.telPerso
        : inputValue.phoneNumber
          ? inputValue.phoneNumber
          : 'N/A',
    );
  };

  const handleChangeImportance = (event: any) => {
    setInput({ ...input, idImportance: event.target.value });
  };

  const handleChangeUrgence = (event: any) => {
    setInput({ ...input, idUrgence: event.target.value });
  };

  const handleCommentChange = (value: string) => {
    setInput({ ...input, commentaire: value });
  };

  const handleTacheChange = (value: any) => {
    console.log(value);
    setAssingedUsers([]);
    setFonction({
      idTache: value.idTache,
      idFonction: value.idFonction,
    });
    setTache(value);
    setInput({ ...input, concernesIds: undefined });
  };

  const handleSubmit = () => {
    if (
      input &&
      input.idImportance &&
      idInterlocuteur &&
      input.idUrgence &&
      (assignedUsers.length > 0 || (fonction.idTache && fonction.idFonction))
    ) {
      onSave({
        id: input.id,
        idImportance: input.idImportance,
        idUrgence: input.idUrgence,
        idUserInterlocuteur: idInterlocuteur,
        fichiers: fichiersJoints,
        commentaire: input.commentaire || '',
        idFonction: fonction.idFonction || '',
        idTache: fonction.idTache || '',
        concernesIds: assignedUsers.length > 0 ? assignedUsers.map(user => user.id) : undefined,
      });
    } else {
      setErrorForm(true);
    }
  };

  const handleUserSelected = () => {
    console.log(usersSelected);
    if (usersSelected.length > 0) {
      setIdInterlocuteur(usersSelected[0].id);
      setValueTel(
        (usersSelected[0].contact ? usersSelected[0].contact.telPerson : null) ||
        (usersSelected[0].phoneNumber ? usersSelected[0].phoneNumber : ''),
      );
      setValueAuto(usersSelected[0]);
      handleOpenContact();
    }
  };

  const searchInterlocuteur = (userName: string) => {
    if (userName.length > 0) {
      search({
        variables: {
          skip: 0,
          take: 30,
          query: {
            query: {
              bool: {
                must: [
                  {
                    query_string: {
                      query: `*${userName}*`,
                      analyze_wildcard: true,
                      fields: [],
                    },
                  },
                ],
              },
            },
          },
          type: ['user'],
        },
      });
    }
  };

  useEffect(() => {
    setInput(value);
    setAssingedUsers(value?.concernes || []);
    setTache({ idTache: value?.idTache });
    setFonction({
      idTache: value?.idTache,
      idFonction: value?.idFonction,
    });
    setUserIds([value?.idUserInterlocuteur]);
    setOptionsAutoComplte(
      value ? [{ id: value?.idUserInterlocuteur, userName: value?.interlocuteur.fullName }] : [],
    );
    setValueAuto(
      value
        ? { id: value?.idUserInterlocuteur, userName: value?.interlocuteur.fullName }
        : undefined,
    );
    setIdInterlocuteur(value?.idUserInterlocuteur);
  }, [value]);

  return (
    <CustomModal
      title={mode == 'ajout' ? "Ajout d'un appel reçus" : "Modification d'un appel"}
      open={open}
      setOpen={onOpen}
      withBtnsActions={false}
      closeIcon
      headerWithBgColor
      maxWidth="lg"
      fullScreen={isMobile() ? true : false}
    >
      <div style={{ maxWidth: isMobile() ? '100%' : 600 }} className={styles.root}>
        {errorForm && (
          <Alert severity="error">
            <AlertTitle>Erreur</AlertTitle>
            <p>Veuillez bien remplir le formulaire!</p>
          </Alert>
        )}
        <div className={styles.interlocuteurContainer}>
          <div className={styles.interlocuteurHeader}>
            <div className={styles.interlocuteurLabel}>
              <h3>Interlocuteur</h3>
              <div onClick={handleOpenContact} className={styles.actionRechercheAvancee}>
                Recherche avancée
              </div>
            </div>
            <p className={styles.msgInterlocuteur}>
              Séléctionner l'interlocuteur, par son nom ou par son numéro téléphone
            </p>
          </div>
          <Grid className={styles.interlocuteurInputContainer} container spacing={3}>
            <Grid item xs={12} md={6} lg={6}>
              <CustomAutocomplete
                id="interlocuteurId"
                value={valueAuto}
                style={{ marginTop: 0 }}
                options={optionsAutoComplete}
                required={false}
                optionLabelKey="userName"
                label="Nom interlocuteur"
                onAutocompleteChange={onAutocompleteChange}
                search={searchInterlocuteur}
              />
            </Grid>
            <Grid item xs={12} md={6} lg={6}>
              <CustomFormTextField
                label={'Téléphone interlocuteur'}
                placeholder={'Téléphone interlocuteur'}
                required
                value={valueTel}
              />
            </Grid>
          </Grid>
        </div>
        <hr />
        <Grid style={{ marginTop: 24 }} container spacing={3}>
          <Grid item xs={12} sm={12}>
            <ProjectInput
              onlyMatriceFonctions={true}
              value={tache?.idTache}
              onChange={handleTacheChange}
            />
          </Grid>
          <Grid item xs={12} md={6} lg={6}>
            <UrgenceInput noMinHeight value={input?.idUrgence} onChange={handleChangeUrgence} />
          </Grid>
          <Grid item xs={12} md={6} lg={6}>
            <ImportanceInput
              NoMarginBottom
              value={input?.idImportance}
              onChange={handleChangeImportance}
            />
          </Grid>
          <Grid item xs={12} sm={12}>
            <fieldset className={styles.collaborateurInput}>
              <legend>&nbsp;Collaborateur&nbsp;</legend>
              <div className={styles.collaborateurBox} id="collaborateur-input">
                <div className={styles.collaborateurItem}>
                  <AvatarGroup max={assignedUsers.length}>
                    {uniqBy(assignedUsers, 'id').map((u: any) => {
                      const photo = (u.userPhoto?.fichier || u.photo)?.publicUrl;
                      return photo ? (
                        <Avatar alt="avatarCollaborateur" src={photo} />
                      ) : (
                        <Avatar style={{ width: 30, height: 30, fontSize: 12 }} key={u.id}>
                          {stringToAvatar(u.fullName || '')}
                        </Avatar>
                      );
                    })}
                  </AvatarGroup>
                </div>
                <IconButton onClick={() => setAssignParticipant(true)}>
                  <PersonAdd />
                </IconButton>
              </div>
              <AssignTaskUserModal
                openModal={assignParticipant}
                setOpenModal={setAssignParticipant}
                selected={assignedUsers}
                setSelected={handleSelectedCollaborateurs}
              />
            </fieldset>
          </Grid>
          <Grid item xs={12} sm={12}>
            <CustomEditorText
              style={{ marginBottom: 24 }}
              value={input?.commentaire || ''}
              placeholder="Ajouter commentaire"
              onChange={(e: string) => handleCommentChange(e)}
            />
          </Grid>
        </Grid>
        <Dropzone
          contentText="Glissez et déposez vos documents"
          selectedFiles={fichiersJoints}
          setSelectedFiles={setFichiersJoints}
          multiple
        />
        <div className={styles.actionModal}>
          <CustomButton onClick={onOpen}>Annuler</CustomButton>&nbsp;&nbsp;&nbsp;&nbsp;
          <CustomButton onClick={handleSubmit} color="secondary">
            {mode === 'ajout' ? 'AJOUTER' : 'SAUVEGARDER'}
          </CustomButton>
        </div>
        <CustomModal
          open={openContact}
          setOpen={handleOpenContact}
          title={'Contact'}
          closeIcon={true}
          headerWithBgColor={true}
          maxWidth="lg"
          fullWidth={false}
          fullScreen={isMobile() ? true : false}
          withBtnsActions={false}
        >
          <Box display="flex" flexDirection="column" width="100%" style={{ padding: '16px 0' }}>
            <UserSelect
              userIdsSelected={userIds}
              setUserIdsSelected={setUserIds}
              usersSelected={usersSelected}
              setUsersSelected={setUsersSelected}
              filtersSelected={filtersModal}
              setFiltersSelected={setFiltersModal}
              disableAllFilter={false}
              clientContact
            />
            <div className={styles.actionModalContact}>
              <CustomButton color="secondary" onClick={handleUserSelected}>
                AJOUTER
              </CustomButton>
            </div>
          </Box>
        </CustomModal>
      </div>
    </CustomModal>
  );
};
