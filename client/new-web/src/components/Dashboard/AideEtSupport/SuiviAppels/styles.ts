import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    bannerMobile: {
      width: '100%',
      height: 72,
      background: theme.palette.primary.light,
      display: 'flex',
      justifyContent: 'space-between',
      color: theme.palette.common.white,
      padding: '0 16px',
      alignItems: 'center',
    },
    goBack: {
      fontSize: 25,
    },
    title: {
      marginTop: -3,
      fontSize: 'large',
      marginLeft: -32,
    },
    rootWeb: {
      width: '100%',
    },
    rootMobile: {
      width: '100%',
      padding: '0 16px',
    },
    drawerStyles: {
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
    },
    searchInputContainer: {
      width: '100%',
      height: 'auto',
      border: `1px solid ${theme.palette.primary.dark}`,
      margin: '16px 0',
      borderRadius: 4,
    },
    detailContainer: {
      height: 500,
      width: '100%',
      padding: 16,
      '& h2': {
        fontSize: 18,
        fontWeight: 'normal',
        marginBottom: 24,
      },
    },
    drawer: {
      borderRadius: 10,
    },
    closeDrawer: {
      margin: '16px auto',
      width: 70,
      height: 5,
      borderRadius: 5,
      background: theme.palette.primary.light,
    },
    detail: {
      width: '100vh',
      height: 350,
      background: '#F8F8F8',
      boxShadow: '1px 1px 5px rgba(0,0,0, 0.2)',
      borderRadius: 5,
      display: 'flex',
      alignItems: 'center',
    },
    swiperWrapper: {
      padding: 8,
    },
    detailContent: {
      width: '100%',
      height: '90%',
      display: 'flex',
      alignContent: 'space-between',
      flexWrap: 'wrap',
    },
    detailItem: {
      padding: '0 12px',
      display: 'flex',
      width: '100%',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    key: {
      color: 'gray',
    },
    banner: {
      width: '100%',
      height: 70,
      background: theme.palette.primary.light,
      color: '#ffffff',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    headerTable: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginBottom: 24,
    },
    actionHeaderTable: {
      display: 'flex',
      flexWrap: 'wrap',
      width: 500,
      justifyContent: 'space-between',
    },
    actionHeaderTableSearch: {
      border: `1px solid ${theme.palette.primary.dark}`,
      borderRadius: 4,
      height: 36,
      width: 280,
    },
    actionHeaderTableAdd: {},
    tableContainer: {
      padding: '0 24px',
      marginTop: 24,
    },
    titleTab: {
      display: 'flex',
      flexDirection: 'row',
    },
    avatar: {
      height: 30,
      width: 30,
      fontSize: 'small',
    },
    page: {
      width: '100%',
      textAlign: 'center',
      marginLeft: 8,
      marginTop: 16,
      fontSize: 15,
    },
  }),
);

export default useStyles;
