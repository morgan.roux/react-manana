import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import {
  Box,
  Button,
  debounce,
  Divider,
  FormControlLabel,
  IconButton,
  Radio,
  Typography,
} from '@material-ui/core';
import { AddCircle, Close, PictureAsPdf } from '@material-ui/icons';
import { AxiosResponse } from 'axios';
import classnames from 'classnames';
import { uniqBy } from 'lodash';
import React, {
  ChangeEvent,
  Dispatch,
  FC,
  MouseEvent,
  SetStateAction,
  useRef,
  useState,
} from 'react';
import { useDropzone } from 'react-dropzone';
import { ME_me } from '../../../../../graphql/Authentication/types/ME';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { SEARCH as SEARCH_QUERY } from '../../../../../graphql/search/query';
import { SEARCH, SEARCHVariables } from '../../../../../graphql/search/types/SEARCH';
import { GET_SMYLEYS_WITH_MIN_INFO } from '../../../../../graphql/Smyley';
import {
  SMYLEYS_WITH_MIN_INFO,
  SMYLEYS_WITH_MIN_INFOVariables,
  SMYLEYS_WITH_MIN_INFO_smyleys,
} from '../../../../../graphql/Smyley/types/SMYLEYS_WITH_MIN_INFO';
import { DO_CREATE_UPDATE_TICKET } from '../../../../../graphql/Ticket';
import {
  CREATE_UPDATE_TICKET,
  CREATE_UPDATE_TICKETVariables,
} from '../../../../../graphql/Ticket/types/CREATE_UPDATE_TICKET';
import { GET_TICKET_MOTIFS } from '../../../../../graphql/TicketMotif';
import { TICKET_MOTIFS } from '../../../../../graphql/TicketMotif/types/TICKET_MOTIFS';
import { GET_TICKET_ORIGINES } from '../../../../../graphql/TicketOrigine';
import { TICKET_ORIGINES } from '../../../../../graphql/TicketOrigine/types/TICKET_ORIGINES';
import { DO_SEARCH_USERS } from '../../../../../graphql/User';
import {
  SEARCH_USERS,
  SEARCH_USERSVariables,
} from '../../../../../graphql/User/types/SEARCH_USERS';
import { getGroupement, getUser } from '../../../../../services/LocalStorage';
import { uploadToS3 } from '../../../../../services/S3';
import {
  FichierInput,
  TicketType,
  TicketVisibilite,
} from '../../../../../types/graphql-global-types';
import { formatFilenameWithoutDate } from '../../../../../utils/filenameFormater';
import { formatBytes } from '../../../../../utils/Helpers';
import { AWS_HOST } from '../../../../../utils/s3urls';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import Backdrop from '../../../../Common/Backdrop';
import CustomButton from '../../../../Common/CustomButton';
import { CustomModal } from '../../../../Common/CustomModal';
import CustomSelect from '../../../../Common/CustomSelect';
import CustomTextarea from '../../../../Common/CustomTextarea';
import Autocomplete from '../../../../Main/Content/Messagerie/SideBar/FormMessage/Autocomplete';
import useCommonStyles from '../../../../Main/Content/Messagerie/SideBar/FormMessage/styles';
import { TypeFichier } from '../../../../Main/Content/OperationCommerciale/CreateOC/Resume/useResume';
import useStyles from './styles';
import useTicketForm from './useTicketForm';
import { searchUsersFilterBy } from './utils';

interface TicketFormProps {
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
  refetch: any;
}

const TicketForm: FC<TicketFormProps> = ({ open: openModal, setOpen, refetch }) => {
  return <></>;
  /* 
  const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const client = useApolloClient();
  const grp = getGroupement();
  const idGrp = (grp && grp.id) || '';

  const currentUser: ME_me = getUser();
  const currentUserId = (currentUser && currentUser.id) || '';

  const [loading, setLoading] = useState(false);

  let uploadResult: AxiosResponse<any> | null = null;
  let files: FichierInput[] = [];

  const termGroupement = [{ term: { idGroupement: idGrp } }];

  const { values, setValues, handleChange, handleChangeAutocomplete } = useTicketForm();

  const { cibleDestination, selectedFiles, userDestination, destination, ...othersValues } = values;
  const {
    cible,
    commentaire,
    dateHeureSaisie,
    fichiers,
    idMotif,
    idOrigine,
    idSmyley,
    typeTicket,
    visibilite,
  } = othersValues;

  const { idLaboratoireCible, idPharmacieCible, idPrestataireServiceCible, idServiceCible } = cible;

  let searchType = 'service';
  let optionLabel = 'nom';
  let cibleDestinationLabel = 'Service';
  // Set search type
  switch (destination) {
    case 'groupement':
      searchType = 'service';
      cibleDestinationLabel = 'Service';
      break;
    case 'president':
      searchType = 'pharmacie';
      cibleDestinationLabel = 'Pharmacie';
      break;
    case 'laboratoire':
      searchType = 'laboratoire';
      optionLabel = 'nomLabo';
      cibleDestinationLabel = 'Laboratoire';
      break;
    case 'partenaire':
      searchType = 'partenaire';
      cibleDestinationLabel = 'Partenaire';
      break;
    default:
      searchType = 'service';
      optionLabel = 'nom';
      cibleDestinationLabel = 'Service';
      break;
  }

  const [searchVar, setSearchVar] = useState<SEARCHVariables>({
    type: [searchType],
    take: 5,
  });

  const [searchUsersVar, setSearchUsersVar] = useState<SEARCH_USERSVariables>({
    take: 5,
    query: {
      query: {
        bool: {
          must: [...termGroupement],
        },
      },
    },
  });

  const onDrop = (acceptedFiles: File[]) => {
    setValues(prevState => ({
      ...prevState,
      selectedFiles: [...selectedFiles, ...acceptedFiles],
    }));
  };

  const { getRootProps, getInputProps, open, acceptedFiles } = useDropzone({
    // Disable click and keydown behavior
    noClick: true,
    noKeyboard: true,
    multiple: true,
    onDrop,
  });

  const removeFile = (file: File) => () => {
    const newFiles = [...selectedFiles];
    newFiles.splice(newFiles.indexOf(file), 1);
    acceptedFiles.splice(acceptedFiles.indexOf(file), 1);
    setValues(prevState => ({
      ...prevState,
      selectedFiles: newFiles,
    }));
  };

  const typeTicketList = [
    { id: TicketType.RECLAMATION, value: 'Réclamation' },
    { id: TicketType.APPEL, value: 'Appel' },
  ];

  const destinationList = [
    { id: 'groupement', value: 'Le groupement' },
    { id: 'president', value: 'Président de Région' },
    { id: 'laboratoire', value: 'Laboratoire Partenaire' },
    { id: 'partenaire', value: 'Partenaire de Service' },
  ];

  // Get cibles (Service, president, labo, or partenaire)
  const [search, { data: searchData, loading: searchLoading }] = useLazyQuery<
    SEARCH,
    SEARCHVariables
  >(SEARCH_QUERY, {
    // fetchPolicy: 'cache-and-network',
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const cibleDestList = (searchData && searchData.search && searchData.search.data) || [];

  // Get users
  const [searchUsers, { data: userData, loading: userLoading }] = useLazyQuery<
    SEARCH_USERS,
    SEARCH_USERSVariables
  >(DO_SEARCH_USERS, {
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const users = (userData && userData.search && userData.search.data) || [];

  const onOpenUserAutocomplete = () => {
    searchUsers({ variables: searchUsersVar });
  };

  const onOpenCibleDestAutocomplete = () => {
    search({ variables: searchVar });
  };

  const debouncedSearch = useRef(
    debounce(
      (
        text: string,
        name: string,
        searchType: string,
        searchVariables: SEARCHVariables,
        wildcardField: string,
        idLaboratoireCible: string | null | undefined,
        idPharmacieCible: string | null | undefined,
        idPrestataireServiceCible: string | null | undefined,
        idServiceCible: string | null | undefined,
        destination: string,
      ) => {
        if (name === 'userDestination') {
          const newFilterBy = searchUsersFilterBy(
            destination,
            idLaboratoireCible,
            idPharmacieCible,
            idPrestataireServiceCible,
            idServiceCible,
          );
          searchUsers({
            variables: {
              ...searchUsersVar,
              filterBy: [...newFilterBy, { wildcard: { userName: `*${text}*` } }],
              take: null,
            },
          });
        }

        if (name !== 'userDestination') {
          const fB =
            searchType === 'pharmacie' || searchType === 'partenaire' ? [...termGroupement] : [];
          search({
            variables: {
              ...searchVariables,
              type: [searchType],
              filterBy: [...fB, { wildcard: { [wildcardField]: `*${text}*` } }],
              take: null,
            },
          });
        }
      },
      1000,
    ),
  );

  const onChangeUserTxt = (name: string) => (e: ChangeEvent<any>, value: string) => {
    debouncedSearch.current(
      value,
      name,
      searchType,
      searchVar,
      optionLabel,
      idLaboratoireCible,
      idPharmacieCible,
      idPrestataireServiceCible,
      idServiceCible,
      destination,
    );
  };

  // Get motif list
  const { data: motifData } = useQuery<TICKET_MOTIFS>(GET_TICKET_MOTIFS, {
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const motifList = (motifData && motifData.ticketMotifs) || [];

  // Get origine list
  const { data: origineData } = useQuery<TICKET_ORIGINES>(GET_TICKET_ORIGINES, {
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const origineList = (origineData && origineData.ticketOrigines) || [];

  // Get smyleys list
  const { data: smyleyData } = useQuery<SMYLEYS_WITH_MIN_INFO, SMYLEYS_WITH_MIN_INFOVariables>(
    GET_SMYLEYS_WITH_MIN_INFO,
    {
      variables: { idGroupement: idGrp },
      onError: error => {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );

  // TODO : Form smylesy
  const smyleyList = (smyleyData && smyleyData.smyleys) || [];
  const like = smyleyList.find(i => i && i.nom === "J'aime");
  const [currentSmyley, setCurrentSmyley] = useState<
    SMYLEYS_WITH_MIN_INFO_smyleys | null | undefined
  >(null);

  const [opacity, setOpacity] = useState<number>(0);

  // Set search variables
  React.useEffect(() => {
    switch (destination) {
      case 'groupement':
        setSearchVar(prevState => ({ ...prevState, type: ['service'], filterBy: [] }));
        break;
      case 'president':
        setSearchVar(prevState => ({
          ...prevState,
          type: ['pharmacie'],
          filterBy: [...termGroupement, { term: { sortie: 0 } }],
        }));
        break;
      case 'laboratoire':
        setSearchVar(prevState => ({ ...prevState, type: ['laboratoire'], filterBy: [] }));
        break;
      case 'partenaire':
        setSearchVar(prevState => ({
          ...prevState,
          type: ['partenaire'],
          filterBy: [...termGroupement],
        }));
        break;
      default:
        setSearchVar(prevState => ({ ...prevState, type: ['service'], filterBy: [] }));
        break;
    }

    // Init cibleDestination
    setValues(prevState => ({ ...prevState, cibleDestination: '' }));
  }, [destination]);

  // Set search user variables
  React.useEffect(() => {
    const newFilterBy = searchUsersFilterBy(
      destination,
      idLaboratoireCible,
      idPharmacieCible,
      idPrestataireServiceCible,
      idServiceCible,
    );
    setSearchUsersVar(prevState => ({ ...prevState, filterBy: newFilterBy }));
    // Init userDestination
    setValues(prevState => ({ ...prevState, userDestination: '' }));
  }, [
    destination,
    idLaboratoireCible,
    idPharmacieCible,
    idPrestataireServiceCible,
    idServiceCible,
  ]);

  const [doCreateUpdateTicket, { loading: ticketLoading }] = useMutation<
    CREATE_UPDATE_TICKET,
    CREATE_UPDATE_TICKETVariables
  >(DO_CREATE_UPDATE_TICKET, {
    onCompleted: data => {
      if (data && data.createUpdateTicket) {
        setLoading(false);
        setOpen(false);
        if (refetch) refetch();
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Ticket ajouté avec succès',
        });
      }
    },
    onError: error => {
      setLoading(false);
      console.log('error :>> ', error);
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const [doPresignedUrl, { loading: presignedLoading }] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (data && data.createPutPresignedUrls && data.createPutPresignedUrls.length > 0) {
        Promise.all(
          data.createPutPresignedUrls.map(async (presigned, index) => {
            if (presigned && selectedFiles.length > 0) {
              await Promise.all(
                selectedFiles.map(async file => {
                  if (presigned.filePath.includes(formatFilenameWithoutDate(file))) {
                    const type: TypeFichier = file.type.includes('pdf')
                      ? TypeFichier.PDF
                      : file.type.includes('xlsx')
                      ? TypeFichier.EXCEL
                      : TypeFichier.PHOTO;
                    const newFile: FichierInput = {
                      type,
                      nomOriginal: file.name,
                      chemin: presigned.filePath,
                    };
                    setValues(prevState => {
                      const fichiers = prevState.fichiers
                        ? [...prevState.fichiers, newFile]
                        : [newFile];
                      files = uniqBy(fichiers, 'chemin');
                      const nextState = { ...prevState, fichiers: files };
                      return nextState;
                    });

                    await uploadToS3(selectedFiles[index], presigned.presignedUrl)
                      .then(result => {
                        if (result && result.status === 200) {
                          uploadResult = result;
                        }
                      })
                      .catch(error => {
                        setLoading(false);
                        console.log('error :>> ', error);
                        displaySnackBar(client, {
                          type: 'ERROR',
                          message: "Erreur lors de l'envoye de(s) fichier(s)",
                          isOpen: true,
                        });
                      });
                  }
                }),
              );
            }
          }),
        ).then(async () => {
          if (uploadResult && uploadResult.status === 200) {
            // Add ticket
            await doCreateUpdateTicket({
              variables: {
                input: {
                  ...othersValues,
                  idCurrentUser: currentUserId,
                  fichiers: files,
                  idSmyley: (currentSmyley && currentSmyley.id) || '',
                  cible: { ...othersValues.cible, idCurrentUser: currentUserId },
                },
              },
            });
          }
        });
      }
    },
    onError: error => {
      console.log('error :>> ', error);
      setLoading(false);
      displaySnackBar(client, {
        type: 'ERROR',
        message: "Erreur lors de l'envoye de(s) fichier(s)",
        isOpen: true,
      });
    },
  });

  const onClickCancel = () => {
    setOpen(false);
  };

  const onClickAdd = () => {
    setLoading(true);
    if (!disabledBtnAdd()) {
      if (selectedFiles.length > 0) {
        const filePaths: string[] = [];
        selectedFiles.map((file: File) => {
          filePaths.push(`tickets/${idGrp}/${formatFilenameWithoutDate(file)}`);
        });
        doPresignedUrl({ variables: { filePaths } });
      } else {
        // Add ticket
        doCreateUpdateTicket({
          variables: {
            input: {
              ...othersValues,
              idCurrentUser: currentUserId,
              idSmyley: (currentSmyley && currentSmyley.id) || '',
              cible: { ...othersValues.cible, idCurrentUser: currentUserId },
            },
          },
        });
      }
    }
  };

  const disabledBtnAdd = (): boolean => {
    if (idMotif && typeTicket && idOrigine && destination) {
      return false;
    }
    return true;
  };

  const onClickSmyley = (smyley: SMYLEYS_WITH_MIN_INFO_smyleys) => (event: MouseEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    setCurrentSmyley(smyley);
    setValues(prevState => ({ ...prevState, idSmyley: (smyley && smyley.id) || '' }));
  };

  const showSmyleys = () => {
    setOpacity(1);
  };

  const hideSmyleys = () => {
    setOpacity(0);
  };

  return (
    <CustomModal
      open={openModal}
      setOpen={setOpen}
      title="Ajout d'un Ticket"
      withBtnsActions={false}
      closeIcon={true}
      headerWithBgColor={true}
      maxWidth="md"
      fullWidth={true}
      className={classes.modal}
    >
      {(loading || presignedLoading || ticketLoading) && <Backdrop value="Ajout en cours..." />}
      <div className={classes.ticketFormRoot}>
        <div className={classes.formRow}>
          <CustomSelect
            listId="id"
            index="nom"
            list={motifList}
            name="idMotif"
            label="Motif"
            value={idMotif}
            onChange={handleChange}
            required={true}
          />
        </div>
        <div className={classnames(classes.formRow, classes.inputContainer)}>
          <CustomSelect
            listId="id"
            index="value"
            list={typeTicketList}
            name="typeTicket"
            label="Type de ticket"
            value={typeTicket}
            onChange={handleChange}
            required={true}
          />
          <CustomSelect
            listId="id"
            index="nom"
            list={origineList}
            name="idOrigine"
            label="Origine"
            value={idOrigine}
            onChange={handleChange}
            required={true}
          />
        </div>
        <div className={classnames(classes.formRow, classes.inputContainer)}>
          <CustomSelect
            listId="id"
            index="value"
            list={destinationList}
            name="destination"
            label="Destination"
            value={destination}
            onChange={handleChange}
            required={true}
          />
          <div className={classnames(classes.autocomplete)}>
            <Autocomplete
              variant="outlined"
              label={cibleDestinationLabel}
              value={cibleDestination}
              name="cibleDestination"
              handleChange={handleChangeAutocomplete}
              // tslint:disable-next-line: jsx-no-lambda
              handleInputChange={() => onChangeUserTxt('cibleDestination')}
              options={cibleDestList}
              optionLabel={optionLabel}
              handleOpenAutocomplete={onOpenCibleDestAutocomplete}
              loading={searchLoading}
              disabled={!destination}
            />
          </div>
        </div>
        <div className={classnames(classes.formRow, classes.autocomplete)}>
          <Autocomplete
            variant="outlined"
            label="Associé à"
            value={userDestination}
            name="userDestination"
            handleChange={handleChangeAutocomplete}
            // tslint:disable-next-line: jsx-no-lambda
            handleInputChange={() => onChangeUserTxt('userDestination')}
            options={users}
            optionLabel="userName"
            handleOpenAutocomplete={onOpenUserAutocomplete}
            loading={userLoading}
          />
        </div>
        <div className={classes.formRow}>
          <CustomTextarea
            label="Commentaire"
            name="commentaire"
            value={commentaire}
            onChangeTextarea={handleChange}
            rows={3}
          />
        </div>
        <div className={classes.formRow}>
          <FormControlLabel
            control={
              <Radio
                onChange={handleChange}
                checked={visibilite === TicketVisibilite.PRIVE}
                value={TicketVisibilite.PRIVE}
                name="visibilite"
              />
            }
            label="Privé"
          />
          <FormControlLabel
            control={
              <Radio
                onChange={handleChange}
                checked={visibilite === TicketVisibilite.OUVERT}
                value={TicketVisibilite.OUVERT}
                name="visibilite"
              />
            }
            label="Ouvert"
          />
        </div>
        <div className={classes.formRow}>
          <div className={classes.emojisContainer} onMouseLeave={hideSmyleys}>
            <img
              className={classes.activeEmoji}
              onClick={onClickSmyley(like as any)}
              src={`${AWS_HOST}/${(currentSmyley && currentSmyley.photo) ||
                (like && like.photo) ||
                ''}`}
              alt={(currentSmyley && currentSmyley.nom) || (like && like.nom) || ''}
              onMouseOver={showSmyleys}
            />
            <div className={classes.emojisList} style={{ opacity }}>
              {smyleyList.map(s => (
                <div className={classes.imgContainer}>
                  <img
                    src={`${AWS_HOST}/${(s && s.photo) || ''}`}
                    alt={(s && s.nom) || ''}
                    onClick={onClickSmyley(s as any)}
                    title={(s && s.nom) || ''}
                  />
                </div>
              ))}
            </div>
          </div>
        </div>
        <Divider orientation="horizontal" />
        <div {...getRootProps({ className: classes.dropzone })}>
          <input {...getInputProps()} />
          <Box display="flex" width="100%">
            <Typography style={{ minWidth: 'fit-content' }}>Pièces Jointes :</Typography>
          </Box>
          <div className={classes.btnAndFilesContainer}>
            <Button variant="contained" endIcon={<AddCircle />} color="default" onClick={open}>
              Sélect. fichiers
            </Button>
            <Box className={commonClasses.filesContainer}>
              {selectedFiles.map((file, index) => (
                <Box key={`${file.name}_${index}`} className={commonClasses.fileItem}>
                  <PictureAsPdf />
                  <Box className={commonClasses.filenameContainer}>
                    <Typography>{file.name}</Typography>
                    {file.size && <Typography>{formatBytes(file.size, 1)}</Typography>}
                  </Box>
                  <IconButton size="small" onClick={removeFile(file)}>
                    <Close />
                  </IconButton>
                </Box>
              ))}
            </Box>
          </div>
        </div>
        <Divider orientation="horizontal" />
        <div className={classes.btnsContainer}>
          <CustomButton color="default" onClick={onClickCancel}>
            Annuler
          </CustomButton>
          <CustomButton color="secondary" onClick={onClickAdd} disabled={disabledBtnAdd()}>
            Ajouter
          </CustomButton>
        </div>
      </div>
    </CustomModal>
  ); */
};

export default TicketForm;
