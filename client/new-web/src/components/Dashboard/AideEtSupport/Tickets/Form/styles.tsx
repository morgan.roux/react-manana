import { makeStyles, Theme, createStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    modal: {
      '& h2': {
        fontSize: 30,
      },
      '& .MuiDialog-paperWidthMd': {
        maxWidth: 1024,
      },
    },
    ticketFormRoot: {
      width: '100%',
      maxWidth: 445,
      display: 'flex',
      flexDirection: 'column',
      margin: '35px auto 79px',
    },
    dropzone: {
      display: 'flex',
      alignItems: 'start',
      flexDirection: 'column',
      margin: '15px 0px',
      // marginTop: 5,
      // borderBottom: '1px solid rgba(0, 0, 0, 0.42)',
      // paddingBottom: 10,
      '@media (max-width: 1318px)': {
        flexWrap: 'wrap',
        '& > button': {
          margin: '5px 0px',
        },
      },
    },
    btnAndFilesContainer: {
      width: '100%',
      display: 'flex',
      alignItems: 'start',
      '& > button': {
        textTransform: 'none',
        boxShadow: 'none',
        margin: '0px auto',
      },
      '& > button:hover': {
        boxShadow: 'none',
      },
      '& > button, & > p': {
        minWidth: 'fit-content',
      },
      '& > div > div': {
        marginLeft: 'auto',
      },
    },
    formRow: {
      display: 'flex',
      flexDirection: 'row',
      marginBottom: 37,
      '& > div': {
        marginBottom: 0,
        width: '100%',
      },
    },
    inputContainer: {
      '& > div:nth-last-child(1)': {
        marginLeft: 17,
      },
    },
    autocomplete: {
      '& .MuiAutocomplete-input': {
        padding: '0px 4px !important',
      },
    },
    btnsContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginTop: 39.5,
      '& *': {
        fontFamily: 'Montserrat',
        fontWeight: 'bold',
        letterSpacing: 0,
      },
      '& > button': {
        height: 50,
        width: 200,
        fontSize: 16,
      },
    },
    emojisContainer: {
      display: 'flex',
      alignItems: 'center',
      '& img': {
        cursor: 'pointer',
        border: '1px solid transparent',
      },
    },
    emojisList: {
      maxWidth: 'fit-content',
      marginLeft: 10,
      boxShadow: '0px 2px 12px #00000029',
      borderRadius: 4,
      display: 'flex',
      padding: 8,
      transition: 'opacity 0.3s ease-in-out',
      '& > img': {
        width: 20,
        height: 20,
      },
    },
    imgContainer: {
      display: 'flex',
      padding: 5,
    },
    activeEmoji: {
      width: 30,
      height: 30,
    },
    customAutomcomplete: {
      width: '100%',
      '& .MuiFormLabel-asterisk': {
        color: 'red',
      },
      '& .MuiInputBase-root': {
        padding: 0,
      },
    },
  }),
);

export default useStyles;
