import { head, last, split } from 'lodash';
import React, { ChangeEvent, useState } from 'react';
import {
  FichierInput,
  TicketType,
  TicketVisibilite,
  TicketAppel,
  StatusTicket,
} from '../../../../../types/graphql-global-types';

export interface TicketInterface {
  id: string | null;
  origine: string;
  idOrganisation: string;
  nomInterlocuteur: string;
  telephoneInterlocuteur: string;
  dateHeureSaisie: any;
  commentaire: string;
  idSmyley: string | null;
  visibilite: TicketVisibilite;
  typeTicket: TicketType;
  typeAppel: TicketAppel | null;
  declarant: string;
  idMotif: string;
  priority: number;
  status: StatusTicket;
  fichiers: FichierInput[] | null;
  idsUserConcernees: string[];
  selectedFiles: File[];
}

export const initialState: TicketInterface = {
  id: null,
  origine: '',
  idOrganisation: '',
  nomInterlocuteur: '',
  telephoneInterlocuteur: '',
  typeAppel: null,
  dateHeureSaisie: null,
  commentaire: '',
  idSmyley: null,
  visibilite: TicketVisibilite.OUVERT,
  typeTicket: '' as any,
  declarant: '',
  idMotif: '',
  fichiers: null,
  priority: 1,
  idsUserConcernees: [],
  status: StatusTicket.NOUVELLE,
  selectedFiles: [],
};

const useTicketForm = (
  defaultState?: TicketInterface,
  callback?: (data: TicketInterface) => void,
) => {
  const initValues: TicketInterface = defaultState || initialState;
  const [values, setValues] = useState<TicketInterface>(initValues);

  const otherHandleChange = (name: string, value: any) => {
    if (name.includes('.')) {
      const nameArray = split(name, '.');
      const nameKey: any = head(nameArray);
      const exactName: any = last(nameArray);
      setValues(prevState => ({
        ...prevState,
        [nameKey]: { ...prevState[nameKey], [exactName]: value },
      }));
    }
  };

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      if (name.includes('.')) {
        otherHandleChange(name, value);
      } else {
        setValues(prevState => ({ ...prevState, [name]: value }));
      }
    }
  };

  const handleChangeAutocomplete = (name: string) => (e: React.ChangeEvent<any>, value: any) => {
    if (name.includes('.')) {
      otherHandleChange(name, value);
    } else {
      setValues(prevState => ({ ...prevState, [name]: value }));
    }
  };

  const handleChangeDate = (name: string) => (date: any) => {
    if (date && name) {
      if (name.includes('.')) {
        otherHandleChange(name, date);
      } else {
        setValues(prevState => ({ ...prevState, [name]: date }));
      }
    }
  };

  /*  // Set  cible destination
  React.useEffect(() => {
    const id = (cibleDestination && cibleDestination.id) || '';
    if (destination) {
      switch (destination) {
        case 'groupement':
          setValues(prevState => ({
            ...prevState,
            cible: { ...prevState.cible, idServiceCible: id },
          }));
          break;
        case 'president':
          setValues(prevState => ({
            ...prevState,
            cible: { ...prevState.cible, idPharmacieCible: id },
          }));
          break;
        case 'laboratoire':
          setValues(prevState => ({
            ...prevState,
            cible: { ...prevState.cible, idLaboratoireCible: id },
          }));
          break;
        case 'partenaire':
          setValues(prevState => ({
            ...prevState,
            cible: { ...prevState.cible, idPrestataireServiceCible: id },
          }));
          break;
        default:
          break;
      }
    }
  }, [destination, cibleDestination]); */

  return {
    handleChange,
    handleChangeDate,
    handleChangeAutocomplete,
    values,
    setValues,
  };
};

export default useTicketForm;
