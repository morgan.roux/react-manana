import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    globalTabsContainer: {
      marginTop: 15,
      padding: 6,
      display: 'flex',
      justifyContent: 'center',

      '& .MuiTab-wrapper': {
        fontFamily: 'Montserrat',
        fontWeight: 600,
        fontSize: 14,
        textTransform: 'none',
      },
      '& .MuiTabs-indicator': {
        height: 3,
      },
      '& .MuiPaper-root': {
        boxShadow: '0px 4px 4px 0px rgba(0,0,0,0.25) !important',
      },
    },
    title: {
      margin: 12,
      font: 'normal normal 600 24px/29px Montserrat',
    },
    partenaireContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      marginTop: 15,
      '& > p:nth-child(1)': {
        fontFamily: 'Montserrat',
        fontWeight: 600,
        fontSize: 24,
      },
      '& > p:nth-child(2)': {
        fontFamily: 'Montserrat',
        fontWeight: 600,
        fontSize: 14,
      },
    },
    searchInputBox: {
      padding: '20px 0px 0px 20px',
      display: 'flex',
    },
    contentContainer: {
      maxWidth: 1568,
      margin: '25px auto',
      '& .MuiTab-wrapper': {
        fontFamily: 'Montserrat',
        fontWeight: 600,
        fontSize: 14,
        textTransform: 'none',
      },
      '& .MuiTabs-indicator': {
        height: 3,
      },
      '& .MuiPaper-root': {
        //boxShadow: '0px 4px 4px 0px rgba(0,0,0,0.25) !important',
      },
    },
  }),
);

export default useStyles;
