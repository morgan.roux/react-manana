import { IconButton } from '@material-ui/core';
import { Visibility } from '@material-ui/icons';
import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { DO_SEARCH_USERS } from '../../../../../graphql/User';
import { getGroupement } from '../../../../../services/LocalStorage';
import CustomContent from '../../../../Common/newCustomContent';
import SearchInput from '../../../../Common/newCustomContent/SearchInput';
import WithSearch from '../../../../Common/newWithSearch/withSearch';
import { Column } from '../../../Content/Interface';
import useStyles from '../styles';

interface IcollaborateurProps {
  queryShould?: any;
}
const Collaborateur: React.FC<RouteComponentProps & IcollaborateurProps> = props => {
  const groupement = getGroupement();
  const classes = useStyles({});

  const {
    queryShould,
    history: { push },
    location: { pathname },
  } = props;

  const isOnAppel: boolean = pathname.includes('appel');
  const isOnReclamation: boolean = pathname.includes('reclamation');

  const viewDetail = (row: any) => () => {
    isOnAppel
      ? push(`/db/aide-support/appel/collaborateur/${row.id}`)
      : push(`/db/aide-support/reclamation/collaborateur/${row.id}`);
  };
  const suiviReclamationColumns: Column[] = [
    {
      name: 'nom',
      label: 'Personne',
      renderer: (value: any) => {
        return value.userName ? value.userName : '-';
      },
    },
    {
      name: 'role.nom',
      label: 'Rôle',
      renderer: (value: any) => {
        return value.role && value.role ? value.role.nom : '-';
      },
    },
    {
      name: 'cp',
      label: 'Code postal',
      renderer: (value: any) => {
        return value.userPersonnel && value.userPersonnel.contact
          ? value.userPersonnel.contact.cp
          : value.userPartenaire && value.userPartenaire.contact
          ? value.userPartenaire.contact.cp
          : '-';
      },
    },
    {
      name: 'ville',
      label: 'Ville',
      renderer: (value: any) => {
        return value.ville ? value.ville : '-';
      },
    },
    {
      name: 'departement.nom',
      label: 'Nom du département',
      renderer: (value: any) => {
        return value.departement && value.departement.nom ? value.departement.nom : '-';
      },
    },
    isOnAppel
      ? {
          name: 'nbTicket',
          label: 'Nombre de tickets',
          renderer: (value: any) => {
            // const nbTicket = value.nbTicket || '0';
            const nbTicket = value.nbAppel;
            return nbTicket;
          },
        }
      : {
          name: 'nbTicket',
          label: 'Nombre de tickets',
          renderer: (value: any) => {
            // const nbTicket = value.nbTicket || '0';
            const nbTicket = value.nbReclamation;
            return nbTicket;
          },
        },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <IconButton onClick={viewDetail(value)}>
            <Visibility />
          </IconButton>
        );
      },
    },
  ];

  const must = [
    {
      bool: {
        should: queryShould,
        minimum_should_match: 1,
      },
    },
  ];

  const childrenProps: any = {
    isSelectable: false,
    paginationCentered: true,
    columns: suiviReclamationColumns,
  };

  return (
    <>
      <div className={classes.searchInputBox}>
        <SearchInput searchPlaceholder="Rechercher" />
      </div>
      <WithSearch
        type={'user'}
        props={childrenProps}
        WrappedComponent={CustomContent}
        searchQuery={DO_SEARCH_USERS}
        optionalMust={must}
      />
    </>
  );
};

export default withRouter(Collaborateur);
