import React, { FC } from 'react';
import { Button, Menu, MenuItem } from '@material-ui/core';
import { useMutation, useApolloClient } from '@apollo/client';
import Tooltip from '@material-ui/core/Tooltip';
import Fade from '@material-ui/core/Fade';
import useStyles from './styles';
import { LoaderSmall } from '../../../Content/Loader';
import {
  UPDATE_TICKET_STATUT,
  UPDATE_TICKET_STATUTVariables,
} from '../../../../../graphql/Ticket/types/UPDATE_TICKET_STATUT';
import { DO_UPDATE_TICKET_STATUT } from '../../../../../graphql/Ticket/mutation';
import { StatusTicket } from '../../../../../types/graphql-global-types';
interface StatusProps {
  id: string;
  currentStatus: any;
}

const Status: FC<StatusProps> = ({ id, currentStatus }) => {
  const classes = useStyles({});

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const client = useApolloClient();
  const [status, setStatus] = React.useState<any>(currentStatus);

  const listStatus = [
    {
      id: 'NOUVELLE',
      nom: 'Nouvelle',
    },
    {
      id: 'EN_COURS',
      nom: 'En cours',
    },
    {
      id: 'EN_CHARGE',
      nom: 'En charge',
    },
    {
      id: 'CLOTURE',
      nom: 'Cloturé',
    },
  ];

  let bntText = '-';
  if (status) {
    switch (status) {
      case 'NOUVELLE':
        bntText = 'Nouvelle';
        break;
      case 'EN_COURS':
        bntText = 'En cours';
        break;
      case 'EN_CHARGE':
        bntText = 'En charge';
        break;
      case 'CLOTURE':
        bntText = `Cloturé`;
        break;
      default:
        break;
    }
  }

  const [doUpdateTicketStatut, { loading }] = useMutation<
    UPDATE_TICKET_STATUT,
    UPDATE_TICKET_STATUTVariables
  >(DO_UPDATE_TICKET_STATUT);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const updateStatus = (id: string, status: StatusTicket) => {
    handleClose();
    setStatus(status);
    doUpdateTicketStatut({
      variables: { id, status },
    });
  };

  if (loading) {
    return (
      <div className={classes.loaderContainer}>
        <LoaderSmall />
      </div>
    );
  }

  return (
    <>
      <Tooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Status">
        <Button
          className={classes.btn}
          variant="text"
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
        >
          {bntText}
        </Button>
      </Tooltip>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        getContentAnchorEl={null}
      >
        {listStatus &&
          listStatus.map(item => (
            <MenuItem onClick={() => updateStatus(id, item.id as any)}>{item.nom}</MenuItem>
          ))}
      </Menu>
    </>
  );
};

export default Status;
