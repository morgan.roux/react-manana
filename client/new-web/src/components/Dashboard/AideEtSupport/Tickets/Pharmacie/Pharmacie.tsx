import { IconButton } from '@material-ui/core';
import { Visibility } from '@material-ui/icons';
import React, { Fragment } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { GET_CHECKEDS_PHARMACIE } from '../../../../../graphql/Pharmacie/local';
import { GET_SEARCH_CUSTOM_CONTENT_PHARMACIE } from '../../../../../graphql/Pharmacie/query';
import { getGroupement } from '../../../../../services/LocalStorage';
import CustomContent from '../../../../Common/newCustomContent';
import SearchInput from '../../../../Common/newCustomContent/SearchInput';
import WithSearch from '../../../../Common/newWithSearch/withSearch';
import { Column } from '../../../Content/Interface';
import useStyles from '../styles';
interface IPharmacieProps {
  queryShould?: any;
}
const Pharmacie: React.FC<RouteComponentProps & IPharmacieProps> = props => {
  const groupement = getGroupement();
  const classes = useStyles({});
  const {
    queryShould,
    history: { push },
    location: { pathname },
  } = props;
  const isOnAppel: boolean = pathname.includes('appel');
  const viewDetail = (row: any) => () => {
    isOnAppel
      ? push(`/db/aide-support/appel/pharmacie/${row.id}`)
      : push(`/db/aide-support/reclamation/pharmacie/${row.id}`);
  };

  const suiviReclamationColumns: Column[] = [
    {
      name: 'cip',
      label: 'CIP',
      renderer: (value: any) => {
        return value.cip ? value.cip : '-';
      },
    },
    {
      name: 'nom',
      label: 'Pharmacie',
      renderer: (value: any) => {
        return value.nom ? value.nom : '-';
      },
    },
    {
      name: 'cp',
      label: 'Code postal',
      renderer: (value: any) => {
        return value.cp ? value.cp : '-';
      },
    },
    {
      name: 'ville',
      label: 'Ville',
      renderer: (value: any) => {
        return value.ville ? value.ville : '-';
      },
    },
    {
      name: 'departement.nom',
      label: 'Nom du département',
      renderer: (value: any) => {
        return value.departement && value.departement.nom ? value.departement.nom : '-';
      },
    },
    {
      name: 'nbTicket',
      label: 'Nombre de tickets',
      renderer: (value: any) => {
        const nbTicket = value.nbReclamation + value.nbAppel;
        // const nbTicket = value.nbTicket || '0';
        return nbTicket;
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <IconButton onClick={viewDetail(value)}>
            <Visibility />
          </IconButton>
        );
      },
    },
  ];

  const must = [
    { term: { idGroupement: groupement && groupement.id } },
    { term: { sortie: 0 } },
    {
      bool: {
        should: queryShould,
        minimum_should_match: 1,
      },
    },
  ];
  console.log('QueryShould >>>>>>>>>>>>>>>>', queryShould);
  const childrenProps: any = {
    checkedItemsQuery: {
      name: 'checkedsPharmacie',
      type: 'pharmacie',
      query: GET_CHECKEDS_PHARMACIE,
    },
    isSelectable: false,
    paginationCentered: true,
    columns: suiviReclamationColumns,
  };

  return (
    <Fragment>
      <div className={classes.searchInputBox}>
        <SearchInput searchPlaceholder="Rechercher" />
      </div>
      <WithSearch
        type={'pharmacie'}
        props={childrenProps}
        WrappedComponent={CustomContent}
        searchQuery={GET_SEARCH_CUSTOM_CONTENT_PHARMACIE}
        optionalMust={must}
      />
    </Fragment>
  );
};

export default withRouter(Pharmacie);
