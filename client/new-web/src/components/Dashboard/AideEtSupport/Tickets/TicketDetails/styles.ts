import { Theme, createStyles, makeStyles, darken } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    rootContainer: {
      display: 'flex',
      width: '100%',
      flexWrap: 'wrap',
      maxWidth: 1238,
      marginTop: 40,
      marginLeft: 142,
      justifyContent: 'space-between',
    },
    infoContainer: {
      width: '100%',
      maxWidth: 534,
      minWidth: 534,
      padding: '20px 25px',
      marginBottom: 32,
    },
    suiviContainer: {
      width: '100%',
      maxWidth: 613,
    },
    infoTitle: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 16,
      marginBottom: 23,
    },
    filesContainer: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      marginLeft: '10px',
      width: '100%',
      '@media (max-width: 1318px)': {
        margin: '5px 0px 0px',
      },
    },
    infoRowContainer: {
      display: 'flex',
      marginBottom: 10,
    },
    infoRowLabel: {
      fontFamily: 'Roboto',
      fontSize: 14,
      color: '#878787',
      minWidth: 150,
    },
    infoRowValue: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 14,
    },
    infoCommentsContainer: {
      border: '1px solid #E0E0E0',
      borderRadius: 31,
      padding: 16,
      fontWeight: 'bold',
      fontFamily: 'Roboto',
      fontSize: 14,
      color: '#878787',
      minWidth: 150,
      marginTop: 16,
      position: 'relative',
      marginBottom: 36,
    },
    smyleyContainer: {
      width: 45,
      height: 45,
      borderRadius: '50%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      right: 24,
    },
    nomFile: {
      margin: 0,
    },
    fileItem: {
      display: 'flex',
      alignItems: 'start',
      border: '1px solid #9E9E9E',
      padding: 4,
      marginLeft: 6,
      marginBottom: 6,
      '& .MuiTypography-root': {
        fontSize: '0.75rem',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        '-webkit-line-clamp': 1,
        '-webkit-box-orient': 'vertical',
        whiteSpace: 'nowrap',
        maxWidth: 110,
        width: '100%',
      },
    },
    filenameContainer: {
      display: 'flex',
      flexDirection: 'column',
    },
    editIconContainer: {
      display: 'flex',
      justifyContent: 'flex-end',
    },
    editIcon: {
      background: theme.palette.secondary.main,
      color: theme.palette.common.white,
      '&:hover': {
        background: darken(theme.palette.secondary.main, 0.1),
      },
    },
    suiviTitle: {
      fontFamily: 'Roboto',
      fontSize: 20,
      fontWeight: 'bold',
    },
    exportBtn: {
      width: 300,
      height: 50,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 14,
      color: '#5D5D5D',
      marginTop: 25,
      marginBottom: 35,
      marginLeft: 10,
    },
    transformButton: {
      width: 300,
      height: 50,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 14,
      marginTop: 25,
      marginBottom: 35,
    },
    reclamationRoot: {
      padding: '17px 25px',
      marginBottom: 22,
      '& textarea[disabled]': {
        background: '#F2F2F2',
      },
    },
    suiviDescription: {
      fontFamily: 'Montserrat',
      fontSize: 14,
      color: '#878787',
      padding: 15,
      marginTop: 27,
    },
    modalRoot: {
      display: 'flex',
      flexDirection: 'column',
      maxWidth: 467,
      margin: 100,
    },
    autocomplete: {
      marginBottom: 16,
    },
    rowContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    statusSelect: {
      width: 160,
    },
    dateTimeContainer: {
      marginBottom: 20,
      display: 'flex',
      '& > .MuiFormControl-root:nth-child(1)': {
        marginRight: '10px',
      },
    },
    modalButtonContainer: {
      marginTop: 110,
      display: 'flex',
      justifyContent: 'space-between',
      '& > button': {
        width: 200,
      },
    },
  }),
);

export default useStyles;
