import React from 'react';

export interface AideInterface {
  id?: string;
  title: string;
  description: string;
}

export const initialState: AideInterface = {
  id: '',
  title: '',
  description: '',
};

const useAideForm = (defaultState?: AideInterface, callback?: (data: AideInterface) => void) => {
  const initValues: AideInterface = defaultState || initialState;
  const [values, setValues] = React.useState<AideInterface>(initValues);

  const handleChange = (e: React.ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      setValues(prevState => ({ ...prevState, [name]: value }));
    }
  };

  return {
    handleChange,
    values,
    setValues,
  };
};

export default useAideForm;
