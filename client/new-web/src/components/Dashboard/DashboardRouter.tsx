import React from 'react';
import { Route, Switch } from 'react-router-dom';
import withUser from '../../components/Common/withUser';
import {
  ADMINISTRATEUR_GROUPEMENT,
  PRESIDENT_REGION,
  SUPER_ADMINISTRATEUR,
  TITULAIRE_PHARMACIE,
} from '../../Constant/roles';
import {
  AIDE_SUPPORT_URL,
  COLORS_URL,
  GROUPES_CLIENTS_URL,
  GROUPE_AMIS,
  PARTAGE_IDEE_URL,
  PARTENAIRE_LABORATOIRE_URL,
  LABORATOIRE_URL,
  PARTENAIRE_SERVICE_URL,
  PERSONNEL_GROUPEMENT_URL,
  PERSONNEL_PHARMACIE_URL,
  PHARMACIE_URL,
  PRESIDENT_REGION_URL,
  TITULAIRE_PHARMACIE_URL,
  TRAITEMENT_AUTOMATIQUE_URL,
  PARAMETRE_FRIGO_URL,
  DASHBOARD_ITEM_SCORING_URL,
  MODELE_OPERATION_MARKETING_URL,
  OPERATION_MARKETING_URL,
} from '../../Constant/url';
import { AppAuthorization } from '../../services/authorization';
import {
  GroupeClientDashboard,
  LaboratoiresDashboard,
  PartenaireServiceDashboard,
  PersonnelGroupementDashboard,
  PersonnelPharmacieDashboard,
  PharmacieDashboard,
  PresidentRegionDashboard,
  TitulaireDashboard,
} from '../Common/newWithSearch/ComponentInitializer';
import { ThemePage } from '../Main/Content/Theme';
import { ProtectedRoute } from '../Router';
import AideEtSupport from './AideEtSupport';
import Aide from './AideEtSupport/Aide';
import Tickets from './AideEtSupport/Tickets';
import Avatar from './Avatar';
import BasisDataManagement from './BasisDataManagement';
import { ColorManagementDashboard } from './ColorManagement';
import ChecklistManagement, { ChecklistForm } from './DemarcheQualite/ChecklistsManagement';
import DemarcheQualite from './DemarcheQualite/DemarcheQualite';
import MatriceTache from './DemarcheQualite/MatriceTache/MatriceTache';
import MatriceTacheDetails from './DemarcheQualite/MatriceTache/MatriceTacheFonctionDetails/MatriceTacheFonctionDetails';
import { ExigenceForm } from './DemarcheQualite/Theme/SousTheme/ExigenceForm';
import DetailsTheme from './DemarcheQualite/Theme/Theme';
import EspaceDocumentaire from './EspaceDocumentaire';
import EspacePublicitaire from './EspacePublicitaire';
import { FormUser } from './FormUser';
import GroupDetail from './GroupDetail';
import GroupDetailDashboard from './GroupDetail/GroupDetailDashboard';
import { Groupement } from './Groupement';
import GroupeFriends from './GroupFriends/GroupeFriends';
import TraitementAutomatique from './TraitementAutomatique/TraitementAutomatique';
import ParametreFrigo from './ParametreFrigo/ParametreFrigo';
import { IdeeBonnePratiqueDashboard } from './IdeeBonnePratique/';
import IdeeBonnePratiqueDetails from './IdeeBonnePratique/IdeeBonnePratiqueDetails';
import OptionsGroupement from './OptionsGroupement';
import ParametreSso from './ParametreSso';
import { PartenairesServices } from './PartenairesServices';
import FichePrestataire from './PartenairesServices/FichePrestataire';
import FichePrestataireContact from './PartenairesServices/FichePrestataire/Contact';
import { CreatePersonnelPharmacie } from './PersonnelPharmacies/Create';
import FichePharmacie from './Pharmacies/FichePharmacie';
import Roles from './Roles';
import FicheTitulaire from './TitulairesPharmacies/FicheTitulaire';
import UserSettings from './UserSettings';
import Utilitaire from './Utilitaire/Utilitaire';
import DashboardItemScoring from './DashboardItemScoring';

import Rgpd from './Rgpd/Rgpd';

import Categorie from './Categorie/Categorie';
import GestionEspaceDocumentaire from './EspaceDocumentaire/GestionEspaceDocumentaire';
import OptionsPharmacie from './OptionsPharmacie';
import { CreateUpdateLabo } from './Laboratoire/CreateUpdate';
import FicheLaboratoire from './Laboratoire/Fiche';
import { CommandeOrale } from './CommandeOrale';
import { SuiviAppels } from './AideEtSupport/SuiviAppels';
import { CarnetContact } from './AideEtSupport/CarnetContact';
import ModeleOperationMarketing from './ModeleOperationMarketing/ModeleOperationMarketing';
import OperationMarketing from './OperationMarketing/OperationMarketing';
// Removed on route after client feedback
// <Route
// path = "/db/groupements"
// exact = { true}
// component = { withUser(Groupements, { permitRoles: [SUPER_ADMINISTRATEUR] })}
// {...props }
// />

// Removed on route after client feedback
// <Route
// path = "/db/groupements"
// exact = { true}
// component = { withUser(Groupements, { permitRoles: [SUPER_ADMINISTRATEUR] })}
// {...props }
// />

const DashboardRouter = (props: any = {}) => {
  const { user } = props;
  const auth = new AppAuthorization(user);

  const USER_ROUTES = [
    {
      url: PRESIDENT_REGION_URL,
      addUserAuth: auth.isAuthorizedToAddUserPresidentRegion(),
      editUserAuth: auth.isAuthorizedToEditUserPresidentRegion(),
    },
    {
      url: PERSONNEL_GROUPEMENT_URL,
      addUserAuth: true,
      editUserAuth: true,
    },
    {
      url: PARTENAIRE_LABORATOIRE_URL,
      addUserAuth: auth.isAuthorizedToAddUserLaboratoirePartenaire(),
      editUserAuth: auth.isAuthorizedToEditUserLaboratoirePartenaire(),
    },
    {
      url: PERSONNEL_PHARMACIE_URL,
      addUserAuth: auth.isAuthorizedToEditUserPersonnelPharmacie(),
      editUserAuth: auth.isAuthorizedToEditUserPersonnelPharmacie(),
    },
    {
      url: PARTENAIRE_SERVICE_URL,
      addUserAuth: auth.isAuthorizedToAddUserPartenaireService(),
      editUserAuth: auth.isAuthorizedToEditUserPartenaireService(),
    },
    {
      url: TITULAIRE_PHARMACIE_URL,
      addUserAuth: auth.isAuthorizedToEditUserTitulairePharmacie(),
      editUserAuth: auth.isAuthorizedToEditUserTitulairePharmacie(),
    },
  ];

  return (
    <Switch>
      {/* Option groupement list */}
      <ProtectedRoute
        path={['/db/options-groupement']}
        exact={true}
        strict={true}
        component={withUser(OptionsGroupement)}
        {...props}
        isAuthorized={auth.isAuthorizedToViewGroupementOptions()}
      />
      {/* Edit option groupement */}
      <ProtectedRoute
        path={['/db/options-groupement/edit']}
        exact={true}
        strict={true}
        component={withUser(OptionsGroupement)}
        {...props}
        isAuthorized={auth.isAuthorizedToEditGroupementOptions()}
      />
      {/* User settings */}
      <Route
        path={['/db/user-settings', '/db/user-settings/edit']}
        exact={true}
        strict={true}
        component={withUser(UserSettings)}
        {...props}
      />
      {/* Pharmacie settings */}
      <Route
        path={['/db/options-pharmacie', '/db/options-pharmacie/edit']}
        exact={true}
        strict={true}
        component={withUser(OptionsPharmacie)}
        {...props}
      />
      {/* Theme */}
      <ProtectedRoute
        path="/db/theme"
        exact={true}
        strict={true}
        component={withUser(ThemePage)}
        {...props}
        isAuthorized={auth.isAuthorizedToChangeTheme()}
      />
      {/* Pub list */}
      <ProtectedRoute
        path={['/db/espace-publicitaire']}
        exact={true}
        strict={true}
        component={withUser(EspacePublicitaire)}
        {...props}
        isAuthorized={auth.isAuthorizedToViewPubList()}
      />
      {/* Add pub */}
      <ProtectedRoute
        path={['/db/espace-publicitaire/create']}
        exact={true}
        strict={true}
        component={withUser(EspacePublicitaire)}
        {...props}
        isAuthorized={auth.isAuthorizedToAddPub()}
      />
      {/* Edit pub */}
      <ProtectedRoute
        path={['/db/espace-publicitaire/edit/:pubId']}
        exact={true}
        strict={true}
        component={withUser(EspacePublicitaire)}
        {...props}
        isAuthorized={auth.isAuthorizedToEditPub()}
      />
      {/* Avatars */}
      <Route
        path={['/db/avatar', '/db/avatar/create', '/db/avatar/edit/:avatarId']}
        exact={true}
        strict={true}
        component={withUser(Avatar)}
        {...props}
      />

      {/* Todo colors */}
      <Route
        path={[`/db/${COLORS_URL}`, `/db/${COLORS_URL}/create`, `/db/${COLORS_URL}/edit/:colorId`]}
        exact={true}
        strict={true}
        component={withUser(ColorManagementDashboard)}
        {...props}
      />
      {/* Partage idee et bonne pratique */}
      <Route
        path={[
          `/db/${PARTAGE_IDEE_URL}`,
          `/db/${PARTAGE_IDEE_URL}/create`,
          `/db/${PARTAGE_IDEE_URL}/edit/:ideeId`,
        ]}
        exact={true}
        strict={true}
        component={withUser(IdeeBonnePratiqueDashboard)}
        {...props}
      />
      <Route
        path={`/db/${PARTAGE_IDEE_URL}/fiche/:ideeId`}
        exact={true}
        strict={true}
        component={withUser(IdeeBonnePratiqueDetails)}
        {...props}
      />

      {/* Personnel groupement */}
      <Route
        path={[
          `/db/${PERSONNEL_GROUPEMENT_URL}`,
          `/db/${PERSONNEL_GROUPEMENT_URL}/affectation`,
          `/db/${PERSONNEL_GROUPEMENT_URL}/list`,
          `/db/${PERSONNEL_GROUPEMENT_URL}/create`,
          `/db/${PERSONNEL_GROUPEMENT_URL}/edit/:personnelId`,
        ]}
        exact={true}
        component={withUser(PersonnelGroupementDashboard, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT],
        })}
        {...props}
      />

      {/* Debut Route Demarche qualite */}
      <Route
        path={['/db/demarche_qualite/exigence/:idTheme/:idSousTheme/:id']}
        exact={true}
        component={ExigenceForm}
        {...props}
        isAuthorized={auth.isAuthorizedToReIndex()}
      />

      <Route path="/db/categorie" exact={true} component={Categorie} user={user} />

      <ProtectedRoute
        path={['/db/demarche_qualite', '/db/demarche_qualite/:id']}
        exact={true}
        component={DemarcheQualite}
        {...props}
        isAuthorized={auth.isAuthorizedToReIndex()}
      />

      <Route path="/db/demarche_qualite" exact={true} component={DemarcheQualite} {...props} />

      <Route
        path="/db/demarche_qualite/:idTheme"
        exact={true}
        component={DetailsTheme}
        {...props}
      />

      <Route path="/db/matrice_taches" exact={true} component={MatriceTache} {...props} />
      <Route path="/db/matrice_taches/:id" exact={true} component={MatriceTache} {...props} />

      {/*Fin Route demarche qualite*/}

      {/* Commande orale */}
      <Route path="/db/commande-orale" exact={true} component={CommandeOrale} {...props} />

      {/* Groupement */}
      <Route
        path="/db/groupement"
        exact={true}
        component={withUser(Groupement, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT],
        })}
        {...props}
      />
      {/* President region list */}
      <Route
        path={[
          `/db/${PRESIDENT_REGION_URL}`,
          `/db/${PRESIDENT_REGION_URL}/list`,
          `/db/${PRESIDENT_REGION_URL}/affectation`,
        ]}
        exact={true}
        component={withUser(PresidentRegionDashboard, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT],
        })}
        {...props}
        isAuthorized={auth.isAuthorizedToViewPresidentRegionList()}
      />
      {/* Titiulaire pharmacie list */}
      <ProtectedRoute
        path={[
          `/db/${TITULAIRE_PHARMACIE_URL}`,
          `/db/${TITULAIRE_PHARMACIE_URL}/create`,
          `/db/${TITULAIRE_PHARMACIE_URL}/edit/:titulaireId`,
        ]}
        exact={true}
        component={withUser(TitulaireDashboard, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT, TITULAIRE_PHARMACIE],
        })}
        {...props}
        isAuthorized={auth.isAuthorizedToViewTitulairePharmacieList()}
      />
      {/* Personnel pharmacie list */}
      <ProtectedRoute
        path={`/db/${PERSONNEL_PHARMACIE_URL}`}
        exact={true}
        component={withUser(PersonnelPharmacieDashboard, {
          permitRoles: [
            SUPER_ADMINISTRATEUR,
            ADMINISTRATEUR_GROUPEMENT,
            TITULAIRE_PHARMACIE,
            PRESIDENT_REGION,
          ],
        })}
        {...props}
        isAuthorized={auth.isAuthorizedToViewPersonnelPharmacieList()}
      />
      {/* Liste laboratoire */}
      <ProtectedRoute
        path={[`/db/${LABORATOIRE_URL}`]}
        exact={true}
        component={withUser(LaboratoiresDashboard, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT],
        })}
        {...props}
        isAuthorized={auth.isAuthorizedToViewLaboratoirePartenaireList()}
      />
      <ProtectedRoute
        path={[`/db/${LABORATOIRE_URL}/create`, `/db/${LABORATOIRE_URL}/edit/:laboratoireId`]}
        exact={true}
        component={withUser(CreateUpdateLabo, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT],
        })}
        {...props}
        isAuthorized={auth.isAuthorizedToViewLaboratoirePartenaireList()}
      />
      {/* View details laboratoire */}
      <Route
        path={`/db/${LABORATOIRE_URL}/fiche/:laboratoireId`}
        exact={true}
        component={withUser(FicheLaboratoire, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT],
        })}
        {...props}
      />
      {/* Partenaire labo list */}
      {/* <ProtectedRoute
        path={[
          `/db/${PARTENAIRE_LABORATOIRE_URL}`,
          `/db/${PARTENAIRE_LABORATOIRE_URL}/create`,
          `/db/${PARTENAIRE_LABORATOIRE_URL}/edit/:laboratoireId`,
        ]}
        exact={true}
        component={withUser(PartenaireLaboratoireDashboard, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT],
        })}
        {...props}
        isAuthorized={auth.isAuthorizedToViewLaboratoirePartenaireList()}
      />
      <Route
        path={[
          `/db/${PARTENAIRE_LABORATOIRE_URL}/fiche/:idPartenaire/contact/list`,
          `/db/${PARTENAIRE_LABORATOIRE_URL}/fiche/:idPartenaire/contact/details/:id`,
          `/db/${PARTENAIRE_LABORATOIRE_URL}/fiche/:idPartenaire/ressource/list`,
          `/db/${PARTENAIRE_LABORATOIRE_URL}/fiche/:idPartenaire/ressource/create`,
          `/db/${PARTENAIRE_LABORATOIRE_URL}/fiche/:idPartenaire/ressource/edit/:id`,
        ]}
        exact={true}
        component={withUser(FicheLaboratoirePrestataire, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT, TITULAIRE_PHARMACIE],
        })}
      />
      <Route
        path={[
          `/db/${PARTENAIRE_LABORATOIRE_URL}/fiche/:idPartenaire/contact/edit/:id`,
          `/db/${PARTENAIRE_LABORATOIRE_URL}/fiche/:idPartenaire/contact/create`,
        ]}
        exact={true}
        component={withUser(FicheLaboPartenaireContact, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT, TITULAIRE_PHARMACIE],
        })}
      /> */}

      {/* Partenaire service list */}
      <ProtectedRoute
        path={`/db/${PARTENAIRE_SERVICE_URL}`}
        exact={true}
        component={withUser(PartenaireServiceDashboard, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT, TITULAIRE_PHARMACIE],
        })}
        {...props}
        isAuthorized={auth.isAuthorizedToViewPartenaireServiceList()}
      />
      {/* Create partenaire servce */}
      <Route
        path={[
          `/db/${PARTENAIRE_SERVICE_URL}/create`,
          `/db/${PARTENAIRE_SERVICE_URL}/edit/:idPartenaire`,
        ]}
        exact={true}
        component={withUser(PartenairesServices, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT, TITULAIRE_PHARMACIE],
        })}
      />
      <Route
        path={[
          `/db/${PARTENAIRE_SERVICE_URL}/fiche/:idPartenaire/contact/list`,
          `/db/${PARTENAIRE_SERVICE_URL}/fiche/:idPartenaire/contact/details/:id`,
          `/db/${PARTENAIRE_SERVICE_URL}/fiche/:idPartenaire/contact/affectation`,
          `/db/${PARTENAIRE_SERVICE_URL}/fiche/:idPartenaire/ressource/list`,
          `/db/${PARTENAIRE_SERVICE_URL}/fiche/:idPartenaire/ressource/create`,
          `/db/${PARTENAIRE_SERVICE_URL}/fiche/:idPartenaire/ressource/edit/:id`,
        ]}
        exact={true}
        component={withUser(FichePrestataire, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT, TITULAIRE_PHARMACIE],
        })}
      />
      {/* Create and edit partenaire contact */}
      <Route
        path={[
          `/db/${PARTENAIRE_SERVICE_URL}/fiche/:idPartenaire/contact/edit/:id`,
          `/db/${PARTENAIRE_SERVICE_URL}/fiche/:idPartenaire/contact/create`,
        ]}
        exact={true}
        component={withUser(FichePrestataireContact, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT, TITULAIRE_PHARMACIE],
        })}
      />

      {/* Create personnel pharmacie */}
      <Route
        path={[
          `/db/${PERSONNEL_PHARMACIE_URL}/create`,
          `/db/${PERSONNEL_PHARMACIE_URL}/edit/:pPersonnelId`,
        ]}
        exact={true}
        component={withUser(CreatePersonnelPharmacie, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT, TITULAIRE_PHARMACIE],
        })}
      />
      {/* List pharmacies */}
      <ProtectedRoute
        path={`/db/${PHARMACIE_URL}`}
        exact={true}
        component={withUser(PharmacieDashboard, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT],
        })}
        {...props}
        isAuthorized={auth.isAuthorizedToViewPharmacieList()}
      />
      {/* Create Pharmacie */}
      <Route
        path={[`/db/${PHARMACIE_URL}/create`, `/db/${PHARMACIE_URL}/edit/:pharmaId`]}
        exact={true}
        component={withUser(PharmacieDashboard, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT],
        })}
        {...props}
        // isAuthorized={auth.isAuthorizedToViewPharmacieList()}
      />
      {/* Utilitaire */}
      <ProtectedRoute
        path={[
          '/db/utilitaire',
          '/db/utilitaire/indexation',
          '/db/utilitaire/maj-active-directory',
          '/db/utilitaire/maj-helloid',
        ]}
        exact={true}
        component={Utilitaire}
        {...props}
        isAuthorized={auth.isAuthorizedToReIndex()}
      />

      <ProtectedRoute
        path={['/db/demarche_qualite', '/db/demarche_qualite/:id']}
        exact={true}
        component={Utilitaire}
        {...props}
        isAuthorized={auth.isAuthorizedToReIndex()}
      />

      <ProtectedRoute
        path={['/db/check-list-qualite/:id']}
        exact={true}
        component={ChecklistForm}
        {...props}
        isAuthorized={auth.isAuthorizedToManageChecklist()}
      />

      <ProtectedRoute
        path={['/db/check-list-qualite']}
        exact={true}
        component={ChecklistManagement}
        {...props}
        isAuthorized={auth.isAuthorizedToManageChecklist()}
      />

      <Route
        path={[
          '/db/sso-parameter',
          '/db/sso-parameter/ftp',
          '/db/sso-parameter/active-directory',
          '/db/sso-parameter/helloid-parameter',
          '/db/sso-parameter/manage-applications',
          '/db/sso-parameter/manage-applications/applications',
          '/db/sso-parameter/manage-applications/helloid-applications-role',
          '/db/sso-parameter/manage-applications/helloid-group',
          '/db/sso-parameter/manage-applications/applications-role',
          '/db/sso-parameter/manage-applications/external-mapping',
        ]}
        exact={true}
        component={ParametreSso}
        {...props}
      />
      {/* View details pharmacie */}
      <Route
        path={`/db/${PHARMACIE_URL}/fiche/:idPharmacie`}
        exact={true}
        component={withUser(FichePharmacie, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT],
        })}
        {...props}
      />
      {/* View details titulaire pharmacie */}
      <Route
        path={`/db/${TITULAIRE_PHARMACIE_URL}/fiche/:idTitulaire`}
        exact={true}
        component={withUser(FicheTitulaire, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT],
        })}
        {...props}
      />
      {/*
        <Route
          path="/db/historique-titulaire/:idUser"
          exact={true}
          component={HistoriquePersonnel}
          {...props}
        />
      */}
      {/* Roles */}
      <Route
        path={['/db/roles', '/db/roles/accessPermision/:code', '/db/roles/appetence/:idRole']}
        exact={true}
        strict={true}
        component={withUser(Roles)}
        {...props}
      />
      <Route path={`/db/${AIDE_SUPPORT_URL}`} exact={true} component={AideEtSupport} {...user} />
      <Route
        path={[`/db/${AIDE_SUPPORT_URL}/:ticketType/:type/:idType?/:list?/:idTicket?`]}
        exact={true}
        component={Tickets}
        {...props}
      />
      <Route
        path={[`/db/${AIDE_SUPPORT_URL}/suivi-appel`]}
        exact={true}
        component={SuiviAppels}
        {...props}
      />
      <Route
        path={[`/db/${AIDE_SUPPORT_URL}/carnet-contact`]}
        exact={true}
        component={CarnetContact}
        {...props}
      />
      <Route
        path={[`/db/${AIDE_SUPPORT_URL}/aides`, `/db/aide/${AIDE_SUPPORT_URL}/aide/create`]}
        exact={true}
        component={Aide}
        {...user}
      />

      {/* Data management */}
      <Route
        path={['/db/basis', '/db/basis/:modelName', '/db/basis/:modelName/:id']}
        exact={true}
        strict={true}
        component={withUser(BasisDataManagement)}
        {...props}
      />

      {/* Groupes clients */}
      <Route
        path={[
          `/db/${GROUPES_CLIENTS_URL}`,
          `/db/${GROUPES_CLIENTS_URL}/create`,
          `/db/${GROUPES_CLIENTS_URL}/edit/:groupeClientId`,
        ]}
        exact={true}
        component={withUser(GroupeClientDashboard, {
          permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT],
        })}
        {...props}
        // isAuthorized={auth.isAuthorizedToViewPharmacieList()}
      />
      {/* User routes (create) */}
      {USER_ROUTES.map((item, index) => (
        <ProtectedRoute
          key={`create_user_routes_${index}`}
          path={`/db/${item.url}/user/create/:id`}
          exact={true}
          component={withUser(FormUser, {
            permitRoles: [
              SUPER_ADMINISTRATEUR,
              ADMINISTRATEUR_GROUPEMENT,
              TITULAIRE_PHARMACIE,
              PRESIDENT_REGION,
            ],
          })}
          isAuthorized={item.addUserAuth}
        />
      ))}
      {/* User routes (edit) */}
      {USER_ROUTES.map((item, index) => (
        <ProtectedRoute
          key={`edit_user_routes_${index}`}
          path={`/db/${item.url}/user/edit/:id/:userId`}
          exact={true}
          component={withUser(FormUser, {
            permitRoles: [
              SUPER_ADMINISTRATEUR,
              ADMINISTRATEUR_GROUPEMENT,
              TITULAIRE_PHARMACIE,
              PRESIDENT_REGION,
            ],
          })}
          isAuthorized={item.editUserAuth}
        />
      ))}
      <Route
        path="/db/espace-documentaire"
        exact={true}
        component={GestionEspaceDocumentaire}
        {...props}
      />
      <Route path={`/db/${GROUPE_AMIS}`} exact={true} component={GroupeFriends} {...props} />
      {/* RGPD*/}
      <Route
        path={`/db/${TRAITEMENT_AUTOMATIQUE_URL}`}
        exact={true}
        component={TraitementAutomatique}
        {...props}
      />
      <Route
        path={`/db/${PARAMETRE_FRIGO_URL}`}
        exact={true}
        component={ParametreFrigo}
        {...props}
      />

      <Route
        path={`/db/${DASHBOARD_ITEM_SCORING_URL}`}
        exact={true}
        component={DashboardItemScoring}
        {...props}
      />

      <Route
        path={[
          '/db/rgpd',
          '/db/rgpd/accueil',
          '/db/rgpd/autorisation',
          '/db/rgpd/partenaires',
          '/db/rgpd/politique-de-confidentialite',
          '/db/rgpd/conditions-d-utilisations',
          '/db/rgpd/informations-sur-les-cookies',
        ]}
        exact={true}
        component={Rgpd}
        {...props}
      />
      <Route
        path={[`/db/${GROUPE_AMIS}/:idgroup`, `/db/${GROUPE_AMIS}/:idgroup/membre`]}
        exact={true}
        component={GroupDetailDashboard}
        {...props}
      />
      <Route
        path={[`/db/${MODELE_OPERATION_MARKETING_URL}/:idModele?`]}
        exact={true}
        component={ModeleOperationMarketing}
        {...props}
      />
      <Route
        path={[`/db/${OPERATION_MARKETING_URL}/`]}
        exact={true}
        component={OperationMarketing}
        {...props}
      />
    </Switch>
  );
};

export default DashboardRouter;
