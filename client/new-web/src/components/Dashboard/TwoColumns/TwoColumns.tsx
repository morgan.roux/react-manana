import React, { FC } from 'react';
import useStyles from './style';
import { Box, Divider, Grid } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

interface ITwoColumns {
  rightContent: any;
}

const TwoColumns: FC<ITwoColumns> = ({ children, rightContent }) => {
  const classes = useStyles({});

  return (
    <Grid container={true} className={classes.root}>
      <Grid item={true} xs={3} className={classes.leftOperation}>
        <Box className={classes.leftOperationHeader}>
          <span className={classes.leftOperationHeaderTitle}>Liste des thèmes</span>
          <div className={classes.addIconContent}>
            <AddIcon />
          </div>
          <Divider />
        </Box>
        <Box className={classes.listItemContent}>{children}</Box>
      </Grid>
      <Grid item={true} xs={9}>
        {rightContent}
      </Grid>
    </Grid>
  );
};

export default TwoColumns;
