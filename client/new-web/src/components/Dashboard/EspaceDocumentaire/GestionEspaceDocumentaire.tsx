import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import React, { FC } from 'react';
import { IFolder } from './EspaceDocumentaire/CreateFolder/CreateFolder';
import EspaceDocumentaireProvider from './EspaceDocumentaireProvider';
import { FEDERATION_CLIENT } from '../DemarcheQualite/apolloClientFederation';
import {
  GET_CATEGORIES as GET_CATEGORIES_TYPE,
  GET_CATEGORIESVariables,
} from '../../../federation/ged/categorie/types/GET_CATEGORIES';
import { GET_CATEGORIES } from '../../../federation/ged/categorie/query';
import {
  GET_DOCUMENTS_CATEGORIE as GET_DOCUMENTS_CATEGORIE_TYPE,
  GET_DOCUMENTS_CATEGORIEVariables,
} from '../../../federation/ged/document/types/GET_DOCUMENTS_CATEGORIE';
import { GET_DOCUMENTS_CATEGORIE } from '../../../federation/ged/document/query';
import { DocumentCategorie } from '../../Main/Content/EspaceDocumentaire/EspaceDocumentaire';
import { UPDATE_DOCUMENT_STATUS } from '../../../federation/ged/document/mutation';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import {
  UPDATE_DOCUMENT_STATUS as UPDATE_DOCUMENT_STATUS_TYPE,
  UPDATE_DOCUMENT_STATUSVariables,
} from '../../../federation/ged/document/types/UPDATE_DOCUMENT_STATUS';
import { getPharmacie } from '../../../services/LocalStorage';

const GestionEspaceDocumentaire: FC = () => {
  const client = useApolloClient();
  const pharmacie = getPharmacie();

  const { loading: documentsLoading, error: documentsError, data: documents } = useQuery<
    GET_DOCUMENTS_CATEGORIE_TYPE,
    GET_DOCUMENTS_CATEGORIEVariables
  >(GET_DOCUMENTS_CATEGORIE, {
    client: FEDERATION_CLIENT,
    variables: {
      filter: {
        idPharmacie: {
          eq: pharmacie.id,
        },
      },
    },
  });

  const [
    getCategories,
    { loading: categoriesLoading, error: categoriesError, data: categories },
  ] = useLazyQuery<GET_CATEGORIES_TYPE, GET_CATEGORIESVariables>(GET_CATEGORIES, {
    client: FEDERATION_CLIENT,
  });

  const [updateDocumentStatut] = useMutation<
    UPDATE_DOCUMENT_STATUS_TYPE,
    UPDATE_DOCUMENT_STATUSVariables
  >(UPDATE_DOCUMENT_STATUS, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le statut du document a été changé avec succès!',
        isOpen: true,
        type: 'SUCCESS',
      });
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant le changement du statut du document!',
        isOpen: true,
        type: 'ERROR',
      });
    },
    client: FEDERATION_CLIENT,
  });

  const handleCreateNewFolder = (folder: IFolder): void => { };

  const handleGetCategories = (): void => {
    getCategories();
  };

  const handleApplyReplacement = (
    statut: string,
    document: DocumentCategorie,
    commentaire: string,
  ): void => {
    updateDocumentStatut({
      variables: {
        input: {
          idDocument: document?.id as any,
          commentaire: commentaire,
          status: statut === 'approuver' ? 'APPROUVE' : 'REFUSE',
        },
      },
    });
  };

  return (
    <EspaceDocumentaireProvider
      categories={{
        loading: categoriesLoading,
        error: categoriesError as any,
        data: categories?.gedCategories.nodes as any,
      }}
      documents={{
        loading: documentsLoading,
        error: documentsError as any,
        data: documents?.gedDocuments.nodes as any,
      }}
      onRequestApplyReplacement={handleApplyReplacement}
      onRequestGetCategories={handleGetCategories}
      onRequestCreateNewFolder={handleCreateNewFolder}
    />
  );
};

export default GestionEspaceDocumentaire;
