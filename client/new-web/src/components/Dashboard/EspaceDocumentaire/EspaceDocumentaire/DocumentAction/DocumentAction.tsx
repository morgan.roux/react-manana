import { Box, FormControlLabel, Radio, RadioGroup, TextField, Typography } from '@material-ui/core';
import React, { FC , useState , ChangeEvent , useEffect } from 'react';

interface DocumentActionProps{
  withStatut? : boolean
  statut? : 'approuver' | 'rejeter'
  setStatut? : ( statut : 'approuver' | 'rejeter' ) => void
  commentaire : string
  setCommentaire : ( commentaire : string ) => void
}

const DocumentAction: FC<DocumentActionProps> = ({withStatut,commentaire,setCommentaire,statut,setStatut}) => {

  const handleChangeValue = ( event : ChangeEvent<HTMLInputElement> ) : void => {
    const { value } = event.target as HTMLInputElement;
    setStatut && setStatut(value as any);
  };

  const handleCommentaireChange = ( event : ChangeEvent<HTMLInputElement> ) : void => {
    setCommentaire((event.target as HTMLInputElement).value);
  };

  return (
    <Box>
      {
        withStatut && (
          <RadioGroup row value="approuver" onChange={handleChangeValue} style={{ marginBottom: 12 }}>
            <FormControlLabel
              value='approuver'
              control={<Radio checked={ statut === 'approuver' } />}
              label={<Typography variant="subtitle2">Approuver</Typography>}
            />
            <FormControlLabel
              value='rejeter'
              control={<Radio checked={ statut === 'rejeter' } />}
              label={<Typography variant="subtitle2">Rejeter</Typography>}
            />
          </RadioGroup>
        )
      }
      <TextField
        label="Commentaire"
        variant="outlined"
        rows={5}
        fullWidth
        multiline
        InputLabelProps={{ shrink: true }}
        disabled={!withStatut}
        value={commentaire}
        onChange={handleCommentaireChange}
      />
    </Box>
  );
};

export default DocumentAction;
