import { Box, Button, Divider, Typography } from '@material-ui/core';
import React, { FC , useState } from 'react';
import InfoDocument from '../InfoDocument';
import style from './style';
import ReactIframe from 'react-iframe';
import classnames from 'classnames';
import { DocumentCategorie } from '../../../../Main/Content/EspaceDocumentaire/EspaceDocumentaire';

interface IValidateDocument {
  type: 'ADD' | 'REPLACE';
  document : DocumentCategorie
  onRequestApplyReplacement : ( statut : string , document : DocumentCategorie , commentaire : string ) => void
}

const ValidateDocument: FC<IValidateDocument> = ({ type , document , onRequestApplyReplacement }) => {
  const classes = style();

  const [ statut , setStatut ] = useState<'approuver' | 'rejeter'>('approuver');
  const [ commentaire , setCommentaire ] = useState<string>('');

  const handleApply = () : void => {
    onRequestApplyReplacement(statut,document,commentaire);
  };

  return (
    <Box className={classnames(classes.root, { [classes.replace]: type === 'REPLACE' })}>
      <Box className={classes.validate}>
        <ReactIframe
          id={document?.id}
          key={document?.id}
          className={classes.view}
          width="100%"
          url={document?.fichier?.publicUrl as any}
        />
        <InfoDocument
          flex={ type === 'REPLACE' }
          document={document}
          withStatut={ type === 'ADD' ? true : false }
          statut={ type === 'ADD' ? statut : undefined }
          setStatut={ type === 'ADD' ? setStatut as any : undefined }
          commentaire={ type === 'ADD' ? commentaire : '' }
          setCommentaire={ type === 'ADD' ? setCommentaire as any : () => {} }
        />
      </Box>

      {type === 'REPLACE' && (
        <Box position="relative" component="p" my={2} py={2}>
          <Divider />
          <Typography className={classes.divider} component="span">
            À remplacer par
          </Typography>
        </Box>
      )}

      {type === 'REPLACE' && (
        <Box className={classes.validate}>
          <ReactIframe
            id={document?.documentARemplacer?.id}
            className={classes.view}
            width="100%"
            url={document?.documentARemplacer?.fichier?.publicUrl as any}
          />
          <InfoDocument 
            flex={type === 'REPLACE'}
            document={document?.documentARemplacer as any}
            withStatut={true}
            statut={statut}
            setStatut={setStatut}
            commentaire={commentaire}
            setCommentaire={setCommentaire}
          />
        </Box>
      )}

      <Button 
        variant="contained" 
        color="secondary"
        onClick={handleApply}
        className={classes.actionBtn}
        disabled={ commentaire === '' }
      >
        Appliquer
      </Button>
    </Box>
  );
};

export default ValidateDocument;
