import { CustomSelect, ErrorPage, Loader } from '@app/ui-kit';
import {
  Box,
  Button,
  DialogActions,
  DialogContent,
  MenuItem,
  TextField,
  Typography,
} from '@material-ui/core';
import React, { FC , useState , ChangeEvent } from 'react';
import styles from './style';

export interface IFolder{
  name : string
  idCategorie : string
  idSousCategorie : string
}
interface CreateFolderProps{
  categories : {
    loading : boolean
    error : Error
    data : any []
  }
  onRequestCreateNewFolder : ( folder : IFolder ) => void
}

const CreateFolder: FC<CreateFolderProps> = ({
  categories,
  onRequestCreateNewFolder
}) => {
  const classes = styles();

  const [ newFolderName , setNewFolderName ] = useState<string>('');
  const [ idCategorie , setIdCategorie ] = useState<string>('');
  const [ idSousCategorie , setIdSousCategorie ] = useState<string>('');
  const [ currentSousCategories , setCurrentSousCategories ] = useState<any [] | undefined>(undefined);

  const handleNewFolderNameChange = ( event : ChangeEvent<HTMLInputElement> ) : void => {
    setNewFolderName((event.target as HTMLInputElement).value);
  };

  const handleIdCategorieChange = ( event : ChangeEvent<HTMLSelectElement> ) : void => {
    const { value } = event.target as HTMLSelectElement;
    setIdCategorie(value);
    setCurrentSousCategories(categories.data.filter((element) => element.id === value )[0].sousCategories);
  };

  const handleIdSousCategorieChange = ( event : ChangeEvent<HTMLSelectElement> ) : void => {
    setIdSousCategorie((event.target as HTMLSelectElement).value);
  };

  const isActiveCreateButton = () : boolean => {
    return newFolderName === '' || idCategorie === '' || idSousCategorie === '';
  };

  const reInit = () : void => {
    setNewFolderName('');
    setIdCategorie('');
    setIdSousCategorie('');
  };

  const handleCreateClick = () : void => {
    const folder : IFolder = {
      name : newFolderName,
      idCategorie : idCategorie,
      idSousCategorie : idSousCategorie
    };

    onRequestCreateNewFolder(folder);
    reInit();
  };

  return (
    <>
      {
        categories?.loading 
        ? 
        (
          <Loader />
        ) 
        : 
        (
          !categories?.error && categories?.data 
          ? 
          (
            <>
              <DialogContent className={classes.create}>
                <TextField
                  variant="outlined"
                  label="Nom du Nouveau dossier"
                  InputLabelProps={{ shrink: true }}
                  fullWidth
                  value={newFolderName}
                  onChange={handleNewFolderNameChange}
                />
        
                <Typography variant="body2" style={{ marginBottom: 12 }}>
                  Créer le dossier sous
                </Typography>
                <CustomSelect 
                  label="Catégorie"
                  value={idCategorie}
                  list={categories.data}
                  listId='id'
                  index="libelle"
                  onChange={handleIdCategorieChange}
                />
        
                <CustomSelect 
                  label="Sous-Catégorie"
                  value={idSousCategorie}
                  list={currentSousCategories}
                  listId='id'
                  index="libelle"
                  onChange={handleIdSousCategorieChange}
                />
              </DialogContent>
              <DialogActions className={classes.actions}>
                <Button 
                  variant="contained" 
                  color="secondary"
                  onClick={handleCreateClick}
                  disabled={isActiveCreateButton()} 
                >
                  Créer
                </Button>
              </DialogActions>
            </>
          ) 
          : 
          (
            <ErrorPage />
          )
        )
      }
    </>
  );
};

export default CreateFolder;
