import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
  container: {
    height: '100%',
    overflowY: 'hidden',
  },
  appBar: {
    color: 'initial',
    backgroundColor: theme.palette.common.white,
    borderBottom: '1px solid #E0E0E0',
  },
  content: {
    height: 'calc(100% - 65px)',
    padding: theme.spacing(4),
    overflowY: 'hidden',
  },
  search: {
    width: 450,
  },
  filter: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(2, 0),
    '&>button': {
      textTransform: 'initial',
    },
    '&>button:first-of-type': {
      marginLeft: 'auto',
      marginRight: theme.spacing(2),
    },
  },
  table: {
    height: 'calc(100% - 108px)',
    overflowY: 'auto',
    '& table': {
      width: 'calc(100% - 4px)',
    },
  },
}));
