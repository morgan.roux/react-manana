import {
  Dialog,
  DialogProps,
  DialogTitle,
  IconButton,
  makeStyles,
  Typography,
} from '@material-ui/core';
import { ArrowBack, Close } from '@material-ui/icons';
import React, { FC } from 'react';
import classnames from 'classnames';

const styles = makeStyles(theme => ({
  bgTitle: {
    position: 'relative',
    color: theme.palette.common.white,
    backgroundColor: theme.palette.primary.main,
    '& h2': {
      fontSize: 28,
    },
  },
  btnStyle: {
    position: 'absolute',
    top: theme.spacing(2),
    right: theme.spacing(2),
  },
  flex: {
    display: 'flex',
    alignItems: 'center',
    '& h6': {
      flex: 1,
      fontSize: 20,
    },
  },
}));

const ModalDocument: FC<DialogProps & {
  heading?: boolean;
  label?: string;
  handleClose?(): void;
}> = ({ label, children, handleClose, maxWidth, heading = true, fullScreen, ...props }) => {
  const classes = styles();
  return (
    <Dialog {...props} fullScreen={fullScreen}>
      {heading && (
        <DialogTitle
          className={classnames(classes.bgTitle, { [classes.flex]: fullScreen })}
          disableTypography
        >
          {fullScreen && (
            <IconButton size="small" onClick={handleClose}>
              <ArrowBack htmlColor="#FFF" />
            </IconButton>
          )}
          <Typography
            variant={fullScreen ? 'subtitle1' : 'h2'}
            align={fullScreen ? 'center' : 'left'}
          >
            {label}
          </Typography>
          {!fullScreen && (
            <IconButton size="small" className={classes.btnStyle} onClick={handleClose}>
              <Close htmlColor="#FFF" />
            </IconButton>
          )}
        </DialogTitle>
      )}
      {children}
    </Dialog>
  );
};

export default ModalDocument;
