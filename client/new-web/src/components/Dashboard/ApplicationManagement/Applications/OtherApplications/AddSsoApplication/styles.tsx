import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => createStyles({
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    display: "inline-block"
  },
  icon: {
    width: 45,
    height: 45
  },
  dialog: {
    width: 100,
    height: 100
  },
  applicationName: {
    fontSize: 13
  },
  checkbox: {
    display: 'inline-block'
  },
  titleApps: {
    display: "inline-block",
  },
  dialogSize: {
    width: 600
  }
}));

export default useStyles;
