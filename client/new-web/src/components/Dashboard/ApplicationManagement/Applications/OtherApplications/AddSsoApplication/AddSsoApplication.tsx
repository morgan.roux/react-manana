import React, { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper, { PaperProps } from '@material-ui/core/Paper';
import Draggable from 'react-draggable';
import useStyles from './styles';
import { useMutation, useApolloClient } from '@apollo/client';
import Form from './AddApplicationForm';
import { ISsoApplicationInput } from '../../../../../../Interface/SsoApplicationInputInterface';
import SnackVariableInterface from '../../../../../../Interface/SnackVariableInterface';
import {
  DO_CREATE_SSO_APPLICATION,
  DO_UPDATE_SSO_APPLICATION,
} from '../../../../../../graphql/Application/mutation';
import {
  CREATE_SSO_APPLICATION,
  CREATE_SSO_APPLICATIONVariables,
} from '../../../../../../graphql/Application/types/CREATE_SSO_APPLICATION';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import {
  SsoType,
  UpdateMappingVariableInput,
  TypeFichier,
  Sendingtype,
} from '../../../../../../types/graphql-global-types';
import { getGroupement } from '../../../../../../services/LocalStorage';
import {
  UPDATE_SSO_APPLICATION,
  UPDATE_SSO_APPLICATIONVariables,
} from '../../../../../../graphql/Application/types/UPDATE_SSO_APPLICATION';
import { AxiosResponse } from 'axios';
import moment from 'moment';
import { formatFilename } from '../../../../../Common/Dropzone/Dropzone';
import { AWS_HOST } from '../../../../../../utils/s3urls';
import { GET_APPLICATIONS_GROUP } from '../../../../../../graphql/Application/query';
import {
  APPLICATIONSGROUP,
  APPLICATIONSGROUPVariables,
} from '../../../../../../graphql/Application/types/APPLICATIONSGROUP';
import {
  MANY_CRYPTO_MD5,
  MANY_CRYPTO_MD5Variables,
} from '../../../../../../graphql/CryptoMd5/types/MANY_CRYPTO_MD5';
import { GET_MANY_CRYPTO_MD5 } from '../../../../../../graphql/CryptoMd5/query';
import Backdrop from '../../../../../Common/Backdrop';
import { useValueParameterAsBoolean } from '../../../../../../utils/getValueParameter';

export interface DialogProps {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  setStartLoadingAdd: React.Dispatch<React.SetStateAction<boolean>>;
  setStartLoadingUpdate: React.Dispatch<React.SetStateAction<boolean>>;
  defaultData?: ISsoApplicationInput;
  isAdd: boolean;
}
export enum DialogAction {
  close,
  addApplication,
  updateApplication,
}

export interface DataInterface {
  dialogAction: DialogAction;
  data?: ISsoApplicationInput;
}
export const DIRECTORY = 'applications/icon';
export const IDENTIFIER_PREFIX = moment().format('YYYYMMDDhmmss');

function PaperComponent(props: PaperProps) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

export default function DraggableDialog(props: DialogProps) {
  const { open, setOpen, defaultData, isAdd, setStartLoadingAdd, setStartLoadingUpdate } = props;
  const classes = useStyles({});
  const client = useApolloClient();
  const groupement = getGroupement();
  const [startUpload, setStartUpload] = useState<boolean>(false);
  const [fileUploadResult, setFileUploadResult] = useState<AxiosResponse<any> | null>(null);
  const [files, setFiles] = useState<File[]>([]);
  const [fileAlreadySet, setFileAlreadySet] = useState<string | undefined>();
  const [datas, setDatas] = useState<DataInterface>();
  const activeDirectoryParam = useValueParameterAsBoolean('0049');
  const ftpParam = useValueParameterAsBoolean('0050');

  const [createSsoApplication] = useMutation<
    CREATE_SSO_APPLICATION,
    CREATE_SSO_APPLICATIONVariables
  >(DO_CREATE_SSO_APPLICATION, {
    update: (cache, { data }) => {
      const created = data && data.createSsoApplication;
      const applicationsOnCache = cache.readQuery<APPLICATIONSGROUP, APPLICATIONSGROUPVariables>({
        query: GET_APPLICATIONS_GROUP,
        variables: {
          idgroupement: (groupement && groupement.id) || '',
        },
      });
      const applicationsGroup = applicationsOnCache && applicationsOnCache.applicationsGroup;
      applicationsGroup &&
        applicationsGroup.push({
          id: (created && created.id) || '',
          groupement: {
            id: (groupement && groupement.id) || '',
            nom: (groupement && groupement.nom) || '',
            __typename: 'Groupement',
          },
          applications: created ? created : null,
          __typename: 'ApplicationsGroup',
        });

      cache.writeQuery({
        query: GET_APPLICATIONS_GROUP,
        data: { applicationsGroup },
        variables: {
          idgroupement: (groupement && groupement.id) || '',
        },
      });

      if (
        created &&
        created.ssoType === SsoType.CRYPTAGE_MD5 &&
        created.cryptoMd5Detail && created.cryptoMd5Detail.haveExternalUserMapping
      ) {
        try {
          const md5OnCache = cache.readQuery<MANY_CRYPTO_MD5, MANY_CRYPTO_MD5Variables>({
            query: GET_MANY_CRYPTO_MD5,
            variables: {
              idgroupement: (groupement && groupement.id) || '',
            },
          });

          console.log('md5OnCache ', md5OnCache);

          const cryptoMd5s = md5OnCache && md5OnCache.cryptoMd5s;
          if (cryptoMd5s && created && created.cryptoMd5Detail) {
            cryptoMd5s.push({
              SsoApplication: {
                __typename: 'SsoApplication',
                id: created.id || '',
                nom: created.nom || '',
                url: created.url || '',
              },
              Sendingtype: created.cryptoMd5Detail.Sendingtype || Sendingtype.GET,
              beginGet: created.cryptoMd5Detail.beginGet || 0,
              endGet: created.cryptoMd5Detail.endGet || 0,
              haveExternalUserMapping: created.cryptoMd5Detail.haveExternalUserMapping || false,
              externalUserMappings: null,
              hexadecimal: created.cryptoMd5Detail.hexadecimal,
              requestUrl: created.cryptoMd5Detail.requestUrl,
              id: created.id,
              __typename: 'CryptoMd5',
            });

            cache.writeQuery({
              query: GET_MANY_CRYPTO_MD5,
              data: { cryptoMd5s },
              variables: {
                idgroupement: (groupement && groupement.id) || '',
              },
            });
          }
        } catch (error) {
          console.warn('no cache for this query');
        }
      }
      setStartLoadingAdd(false);
    },
    onCompleted: async data => {
      if (data && data.createSsoApplication) {
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Création de l'application avec succès`,
          isOpen: true,
        };

        displaySnackBar(client, snackBarData);
        // setOpen(false);
        // window.location.reload();
        setDatas(undefined);
        setFiles([]);
      }
    },
    onError: err => {
      console.log('err ', err);
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `${err.message}`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
      setOpen(false);
      setStartUpload(false);
      setStartLoadingAdd(false);
    },
  });

  const [updateSsoApplication, { loading: loadingUpdate }] = useMutation<
    UPDATE_SSO_APPLICATION,
    UPDATE_SSO_APPLICATIONVariables
  >(DO_UPDATE_SSO_APPLICATION, {
    onCompleted: async data => {
      if (data && data.updateSsoApplication) {
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Modification de l'application avec succès`,
          isOpen: true,
        };

        displaySnackBar(client, snackBarData);
        // setOpen(false);
        setStartUpload(false);
        setDatas(undefined);
        // window.location.reload();
        setStartLoadingUpdate(false);
        setFiles([]);
      }
    },
    onError: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur lors de la modification de l'application`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
      setOpen(false);
      setStartUpload(false);
      setStartLoadingUpdate(false);
    },
  });

  const handleClose = (data: DataInterface) => {
    if (
      data &&
      (data.dialogAction === DialogAction.addApplication ||
        data.dialogAction === DialogAction.updateApplication)
    ) {
      setStartUpload(true);
      setDatas(data);
      setOpen(false);
    } else {
      setDatas(undefined);
      setOpen(false);
    }
  };

  useEffect(() => {
    if (defaultData && defaultData.icon && defaultData.icon.chemin) {
      setFileAlreadySet(`${AWS_HOST}/${defaultData.icon.chemin}`);
    } else {
      setFileAlreadySet(undefined);
    }
  }, [defaultData]);

  const updateData = (datas: DataInterface, icon: any) => {
    if (datas && datas.data) {
      setStartLoadingUpdate(true);
      if (datas.data.mappings && datas.data.mappings.length !== 0) {
        updateSsoApplication({
          variables: {
            ...datas.data,
            mappings: {
              ...(datas.data.mappings as UpdateMappingVariableInput[]),
            },
            id: datas.data.id || '',
            url: datas.data.url || '',
            ssoType: datas.data.ssoType || SsoType.SAML,
            idGroupement: (groupement && groupement.id) || '',
            icon,
          },
        });
      } else {
        updateSsoApplication({
          variables: {
            ...datas.data,
            mappings: undefined,
            id: datas.data.id || '',
            url: datas.data.url || '',
            ssoType: datas.data.ssoType || SsoType.SAML,
            idGroupement: (groupement && groupement.id) || '',
            icon,
          },
        });
      }
    }
  };

  const creatingData = (datas: DataInterface, icon: any) => {
    if (datas && datas.data) {
      setStartLoadingAdd(true);
      if (datas.data.ssoType === SsoType.OTHER_CRYPTO && !activeDirectoryParam) {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: `Vous devez d'abord activer et configurer l'active directory`,
          isOpen: true,
        };

        displaySnackBar(client, snackBarData);
        setStartLoadingAdd(false);
        return;
      }
      if (datas.data.ssoType === SsoType.TOKEN_FTP_AUTHENTIFICATION && !ftpParam) {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: `Vous devez d'abord activer et configurer l'FTP`,
          isOpen: true,
        };

        displaySnackBar(client, snackBarData);
        setStartLoadingAdd(false);
        return;
      }
      createSsoApplication({
        variables: {
          ...datas.data,
          url: datas.data.url || '',
          ssoType: datas.data.ssoType || SsoType.SAML,
          idGroupement: (groupement && groupement.id) || '',
          icon,
        },
      });
    }
  };

  useEffect(() => {
    // create
    if (datas && datas.dialogAction && datas.dialogAction === DialogAction.addApplication) {
      if (
        fileAlreadySet === undefined &&
        files.length > 0 &&
        fileUploadResult &&
        fileUploadResult.status === 200 &&
        fileUploadResult.statusText === 'OK' &&
        datas
      ) {
        const icon = {
          chemin: formatFilename(files[0], DIRECTORY, IDENTIFIER_PREFIX),
          nomOriginal: files[0].name,
          type: TypeFichier.PHOTO,
        };
        creatingData(datas, icon);
      } else if (files.length === 0 && fileAlreadySet && datas) {
        creatingData(datas, undefined);
      } else if (files.length === 0 && fileAlreadySet === undefined && datas) {
        creatingData(datas, null);
      }
    }

    // update
    if (datas && datas.dialogAction && datas.dialogAction === DialogAction.updateApplication) {
      if (
        fileAlreadySet === undefined &&
        files.length > 0 &&
        fileUploadResult &&
        fileUploadResult.status === 200 &&
        fileUploadResult.statusText === 'OK' &&
        datas
      ) {
        console.log('files ', files);
        const icon = {
          chemin: formatFilename(files[0], DIRECTORY, IDENTIFIER_PREFIX),
          nomOriginal: files[0].name,
          type: TypeFichier.PHOTO,
        };
        console.log('icon ', icon);
        updateData(datas, icon);
      } else if (files.length === 0 && fileAlreadySet && datas) {
        console.log('noicon ');
        updateData(datas, undefined);
      } else if (files.length === 0 && fileAlreadySet === undefined && datas) {
        console.log('null icon ');
        updateData(datas, null);
      }
    }
  }, [files, fileUploadResult, datas]);

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        PaperComponent={PaperComponent}
        aria-labelledby="draggable-dialog-title"
        maxWidth="md"
      >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          {`${isAdd ? 'Ajouter' : 'Modifier'} une application`}
        </DialogTitle>
        <DialogContent dividers={true} className={classes.dialogSize}>
          <Form
            isAdd={isAdd}
            action={handleClose}
            defaultData={defaultData}
            fileIdentifierPrefix={IDENTIFIER_PREFIX}
            setFilesOut={setFiles}
            setFileUploadResult={setFileUploadResult}
            startUpload={startUpload}
            setStartUpload={setStartUpload}
            withImagePreview={true}
            uploadDirectory={DIRECTORY}
            fileAleadySetUrl={fileAlreadySet}
            setFileAlreadySet={setFileAlreadySet}
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={handleClose.bind(null, { dialogAction: DialogAction.close })}
            variant="contained"
          >
            Annuler
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
