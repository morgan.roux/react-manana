import React, { FC, useState } from 'react';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { CustomFormTextField } from '../../../../../../../Common/CustomTextField';
import { TokenFtpInput } from '../../../../../../../../types/graphql-global-types';

export interface TokenFtpFormProps {
  defaultData: TokenFtpInput | undefined;
  handleChange: (param: any) => void;
}

const styles = () =>
  createStyles({
    textareaStyle: {
      marginBottom: 25,
      overflowY: "auto"
    },
    marginBottom: {
      marginBottom: 25,
    },
    marginRight: {
      marginRight: 10,
    },
    titleForm: {
      color: '#1D1D1D',
      marginTop: 0,
    },
    flex: {
      display: 'flex',
      flexDirection: 'row',
    },
    w100: {
      width: '100%',
    },
  });

const TokenFtpForm: FC<RouteComponentProps<any, any, any> & WithStyles & TokenFtpFormProps> = ({ classes, handleChange, defaultData }) => {
  const requestUrl = defaultData && defaultData.requestUrl;
  const ftpFilePath = defaultData && defaultData.ftpFilePath;
  const ftpFileName = defaultData && defaultData.ftpFileName;

  return (
    <>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="toknFtp.requestUrl"
            label="Url de requête"
            onChange={handleChange}
            value={requestUrl}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="toknFtp.ftpFilePath"
            label="Chemin du dossier du fichier"
            onChange={handleChange}
            value={ftpFilePath}
            required={false}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="toknFtp.ftpFileName"
            label="Nom du fichier"
            onChange={handleChange}
            value={ftpFileName}
            required={true}
          />
        </div>
    </>
  );
};

export default withStyles(styles, { withTheme: true })(withRouter(TokenFtpForm));
