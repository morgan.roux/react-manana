import React, { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper, { PaperProps } from '@material-ui/core/Paper';
import Draggable from 'react-draggable';
import useStyles from './styles';
import { useQuery, useMutation, useApolloClient } from '@apollo/client';
import Form from './AddApplicationForm';
import { getGroupement } from '../../../../../services/LocalStorage';
import { ExternalMappingInput } from '../../../../../types/graphql-global-types';
import {
  USERS_GROUPEMENT,
  USERS_GROUPEMENTVariables,
  USERS_GROUPEMENT_usersGroupement,
} from '../../../../../graphql/User/types/USERS_GROUPEMENT';
import { GET_USERS_GROUPEMENT } from '../../../../../graphql/User/query';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { Loader } from '../../../Content/Loader';
import {
  CREATE_EXTERNAL_MAPPING,
  CREATE_EXTERNAL_MAPPINGVariables,
} from '../../../../../graphql/ExternalMapping/types/CREATE_EXTERNAL_MAPPING';
import {
  DO_CREATE_EXTERNAL_MAPPING,
  DO_UPDATE_EXTERNAL_MAPPING,
} from '../../../../../graphql/ExternalMapping/mutation';
import {
  MANY_CRYPTO_MD5_cryptoMd5s_externalUserMappings,
  MANY_CRYPTO_MD5_cryptoMd5s,
  MANY_CRYPTO_MD5,
  MANY_CRYPTO_MD5Variables,
} from '../../../../../graphql/CryptoMd5/types/MANY_CRYPTO_MD5';
import {
  UPDATE_EXTERNAL_MAPPING,
  UPDATE_EXTERNAL_MAPPINGVariables,
} from '../../../../../graphql/ExternalMapping/types/UPDATE_EXTERNAL_MAPPING';
import { GET_MANY_CRYPTO_MD5 } from '../../../../../graphql/CryptoMd5/query';

export interface DialogProps {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  defaultData?: ExternalMappingInput;
  isAdd: boolean;
  allMappingList?: Array<MANY_CRYPTO_MD5_cryptoMd5s_externalUserMappings | null>;
  md5Id: string;
  title: String;
  isNew: boolean;
  appToShow?: MANY_CRYPTO_MD5_cryptoMd5s[];
  setAddLoading: React.Dispatch<React.SetStateAction<boolean>>;
}
export enum DialogAction {
  close,
  addMapping,
  updateMapping,
  addNewMapping,
}

export interface DataInterface {
  dialogAction: DialogAction;
  data?: ExternalMappingInput;
}
export const DIRECTORY = 'applications/icon';

function PaperComponent(props: PaperProps) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

export default function DraggableDialog(props: DialogProps) {
  const {
    open,
    setOpen,
    defaultData,
    isAdd,
    allMappingList,
    md5Id,
    title,
    isNew,
    appToShow,
    setAddLoading,
  } = props;
  const classes = useStyles({});
  const client = useApolloClient();
  const groupement = getGroupement();
  const [userList, setUserList] = useState<USERS_GROUPEMENT_usersGroupement[]>([]);

  const { data: usersGroupement, loading: usersGroupementLoading } = useQuery<
    USERS_GROUPEMENT,
    USERS_GROUPEMENTVariables
  >(GET_USERS_GROUPEMENT, {
    // fetchPolicy: "cache-and-network",
    variables: {
      idGroupement: (groupement && groupement.id) || '',
    },
    onError: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur lors de la récupération des utilisateurs, veuillez recharger la page!!`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const [addUserMapping] = useMutation<CREATE_EXTERNAL_MAPPING, CREATE_EXTERNAL_MAPPINGVariables>(
    DO_CREATE_EXTERNAL_MAPPING,
    {
      onCompleted: async data => {
        if (data && data.createExternalMapping) {
          const snackBarData: SnackVariableInterface = {
            type: 'SUCCESS',
            message: `Mapping ajoutée!`,
            isOpen: true,
          };

          displaySnackBar(client, snackBarData);
          setAddLoading(false);
          // setOpen(false);
          // window.location.reload();
        }
      },
      onError: errors => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: `Erreur lors de l'ajout de mapping!`,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
        setAddLoading(false);
      },
    },
  );

  const [editUserMapping] = useMutation<UPDATE_EXTERNAL_MAPPING, UPDATE_EXTERNAL_MAPPINGVariables>(
    DO_UPDATE_EXTERNAL_MAPPING,
    {
      onCompleted: async data => {
        if (data && data.updateExternalMapping) {
          const snackBarData: SnackVariableInterface = {
            type: 'SUCCESS',
            message: `Modification du mapping avec succès!`,
            isOpen: true,
          };

          displaySnackBar(client, snackBarData);
          setAddLoading(false);
          // setOpen(false);
          // window.location.reload();
        }
      },
      onError: errors => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: `Erreur lors de la modification du mapping!`,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
        setAddLoading(false);
      },
    },
  );

  const handleClose = (incomingData: DataInterface) => {
    const { dialogAction, data } = incomingData;

    if (dialogAction === DialogAction.addMapping || dialogAction === DialogAction.addNewMapping) {
      data &&
        addUserMapping({
          variables: {
            extenalMappingInput: {
              ...data,
              md5Id: dialogAction === DialogAction.addMapping ? md5Id : (data && data.md5Id) || '',
            },
          },
          update: (cache, { data: dataCreated }) => {
            const added = dataCreated && dataCreated.createExternalMapping;

            if (added) {
              const md5OnCache = cache.readQuery<MANY_CRYPTO_MD5, MANY_CRYPTO_MD5Variables>({
                query: GET_MANY_CRYPTO_MD5,
                variables: {
                  idgroupement: (groupement && groupement.id) || '',
                },
              });

              if (md5OnCache && md5OnCache.cryptoMd5s) {
                const found = md5OnCache.cryptoMd5s.find(
                  md5 =>
                    (md5 && md5.id) ===
                    (dialogAction === DialogAction.addMapping ? md5Id : (data && data.md5Id) || ''),
                );
                if (found && found.externalUserMappings) {
                  found.externalUserMappings.push({ ...added });
                }
                cache.writeQuery({
                  query: GET_MANY_CRYPTO_MD5,
                  data: { cryptoMd5s: md5OnCache.cryptoMd5s },
                  variables: {
                    idgroupement: (groupement && groupement.id) || '',
                  },
                });
              }
            }
          },
        });
      setAddLoading(true);
      setOpen(false);
    } else if (dialogAction === DialogAction.updateMapping) {
      data &&
        editUserMapping({
          variables: {
            id: (data as any).id,
            extenalMappingInput: {
              userID: data.userID,
              idClient: data.idClient,
              idExternaluser: data.idExternaluser,
              password: data.idExternaluser,
              md5Id,
            },
          },
        });
      setAddLoading(true);
      setOpen(false);
    } else setOpen(false);
  };

  useEffect(() => {
    const users: USERS_GROUPEMENT_usersGroupement[] = [];

    usersGroupement &&
      usersGroupement.usersGroupement &&
      usersGroupement.usersGroupement.forEach(user => {
        const found =
          allMappingList &&
          allMappingList.find(
            userMapping =>
              (userMapping && userMapping.user && userMapping.user.id) === (user && user.id),
          );
        if (!found && user) users.push(user);
      });
    setUserList(users);
  }, [usersGroupement, allMappingList]);

  if (usersGroupementLoading) return <Loader />;

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        PaperComponent={PaperComponent}
        aria-labelledby="draggable-dialog-title"
        maxWidth="md"
      >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          {title}
        </DialogTitle>
        <DialogContent dividers={true} className={classes.dialogSize}>
          <Form
            isAdd={isAdd}
            action={handleClose}
            defaultData={defaultData}
            userList={userList}
            isNew={isNew}
            appToShow={appToShow}
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={e => {
              handleClose({ dialogAction: DialogAction.close });
            }}
            variant="contained"
          >
            Annuler
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
