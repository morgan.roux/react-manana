import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    marginTop: {
      marginTop: 50,
    },
    root: {
      flexWrap: 'nowrap',
      flexGrow: 1,
      padding: 24,
      borderRadius: 4,
      border: '1px solid rgba(0, 0, 0, 0.12)',
      display: 'table',
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    buttonAdd: {
      display: 'inline-block',
      float: 'right',
    },
    titleApps: {
      display: 'inline-block',
    },
    buttonBadge: {
      fontSize: 14,
      color: '#fff',
      background: 'linear-gradient(90deg, rgba(227,65,104,1) 14%, rgba(241,25,87,1) 100%)',
      borderRadius: 15,
    },
    cursorStyle: {
      cursor: 'pointer',
    },
  }),
);

export default useStyles;
