import React, { useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper, { PaperProps } from '@material-ui/core/Paper';
import Draggable from 'react-draggable';
import { Grid } from '@material-ui/core';
import ApplicationDialogList from '../../HelloidApplicationsRole/ApplicationDialogList';
import { useStyles } from './styles';

export interface DialogProps {
  open: boolean;
  title: string;
  applicationList?: any[];
  roleId: string;
  handleClose: (params: any) => void;
  codeRole: string;
}
export enum DialogAction {
  close,
  addToRole,
}

function PaperComponent(props: PaperProps) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

export default function DraggableDialog(props: DialogProps) {
  const { open, title, applicationList, codeRole, roleId, handleClose } = props;
  const [checkboxState, setCheckboxState] = React.useState<any[]>([]);
  const classes = useStyles({});

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCheckboxState({ ...checkboxState, [event.target.name]: event.target.checked });
  };

  const canSave = () => {
    for (const key in checkboxState) {
      if (checkboxState[key] == true) return true;
    }
    // console.log(checked);
    return false;
  };

  useEffect(() => {
    setCheckboxState([]);
  }, [open]);

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        PaperComponent={PaperComponent}
        aria-labelledby="draggable-dialog-title"
        maxWidth="md"
      >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          Ajouter d'applications pour {title}
        </DialogTitle>
        <DialogContent dividers={true} className={classes.dialogContent}>
          <form onSubmit={e => handleClose(DialogAction.addToRole)}>
            <Grid container={true} spacing={2}>
              <ApplicationDialogList
                applicationList={applicationList}
                handleChangeCheck={handleChange}
              />
            </Grid>
          </form>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={e => {
              handleClose(DialogAction.close);
            }}
            variant="contained"
          >
            Annuler
          </Button>
          <Button
            autoFocus={true}
            onClick={e => {
              handleClose({
                roleId,
                codeRole,
                checkboxState,
                dialogAction: DialogAction.addToRole,
              });
            }}
            variant="contained"
            color="primary"
            disabled={!canSave()}
          >
            Ajouter
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
