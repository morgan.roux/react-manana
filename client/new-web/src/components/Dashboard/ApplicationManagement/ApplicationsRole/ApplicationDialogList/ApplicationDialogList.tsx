import React from 'react';
import { Grid, Checkbox } from '@material-ui/core';
import WebAssetIcon from '@material-ui/icons/WebAsset';
import useStyles from './styles';
import { APPLICATIONSGROUP_applicationsGroup } from '../../../../../graphql/Application/types/APPLICATIONSGROUP';
import { AWS_HOST } from '../../../../../utils/s3urls';

export interface DialogProps {
  applicationList?: (APPLICATIONSGROUP_applicationsGroup | null)[];
  handleChangeCheck: (param: any) => void;
}

export default function ApplicationDialogList(props: DialogProps) {
  const { applicationList, handleChangeCheck } = props;
  const classes = useStyles({});
  return (
    <>
      {!applicationList ||
        (applicationList &&
          applicationList.length === 0 &&
          "Il n'y a pas d'autres applications à ajouter")}
      {applicationList &&
        applicationList.map(application => {
          return (
            application &&
            application.applications && (
              <Grid item xs={4} sm={4} lg={4} md={4} key={application.applications.id}>
                <div className={classes.paper}>
                  <Checkbox
                    className={classes.checkbox}
                    name={application.applications.id}
                    onChange={handleChangeCheck}
                  />
                  <span>
                    {application.applications.icon && application.applications.icon.chemin && (
                      <img
                        src={`${AWS_HOST}/${application.applications.icon.chemin}`}
                        className={classes.icon}
                      />
                    )}
                    {(!application.applications.icon || !application.applications.icon.chemin) && (
                      <WebAssetIcon className={classes.icon} />
                    )}
                    <p className={classes.applicationName}>{application.applications.nom}</p>
                  </span>
                </div>
              </Grid>
            )
          );
        })}
    </>
  );
}
