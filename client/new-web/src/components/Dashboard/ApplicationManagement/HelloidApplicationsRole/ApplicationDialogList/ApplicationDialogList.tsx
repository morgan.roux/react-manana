import React from 'react';
import { Grid, Checkbox } from '@material-ui/core';
import WebAssetIcon from '@material-ui/icons/WebAsset';
import useStyles from './styles';
import { APPLICATIONSGROUP_applicationsGroup } from '../../../../../graphql/Application/types/APPLICATIONSGROUP';
import { ALL_HELLOID_APPLICATION_helloidApplications } from '../../../../../graphql/HelloIdApplication/types/ALL_HELLOID_APPLICATION';

export interface DialogProps {
  applicationList?: Array<ALL_HELLOID_APPLICATION_helloidApplications | null>;
  handleChangeCheck: (param: any) => void;
}

export default function ApplicationDialogList(props: DialogProps) {
  const { applicationList, handleChangeCheck } = props;
  const classes = useStyles({});
  return (
    <>
      {!applicationList ||
        (applicationList &&
          applicationList.length === 0 &&
          "Il n'y a pas d'autres applications à ajouter")}
      {applicationList &&
        applicationList.map(application => {
          return (
            application && (
              <Grid item={true} xs={4} sm={4} lg={4} md={4} key={application.applicationGUID}>
                <div className={classes.paper}>
                  <Checkbox
                    className={classes.checkbox}
                    name={application.applicationGUID}
                    onChange={handleChangeCheck}
                  />
                  <span>
                    {application.iconlink && (
                      <img src={application.iconlink} className={classes.icon} />
                    )}
                    {!application.iconlink && <WebAssetIcon className={classes.icon} />}
                    <p className={classes.applicationName}>{application.name}</p>
                  </span>
                </div>
              </Grid>
            )
          );
        })}
    </>
  );
}
