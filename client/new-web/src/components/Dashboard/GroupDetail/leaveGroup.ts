import { useApolloClient, useMutation } from '@apollo/client';
import { DO_LEAVE_GROUP } from '../../../graphql/GroupDetail/mutation';
import { LEAVE_GROUP, LEAVE_GROUPVariables } from '../../../graphql/GroupDetail/types/LEAVE_GROUP';
import SnackVariableInterface from '../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../utils/snackBarUtils';

export const useLeaveGroup = () => {
  const client = useApolloClient();
  const [doLeaveGroup, { loading }] = useMutation<LEAVE_GROUP, LEAVE_GROUPVariables>(
    DO_LEAVE_GROUP,
    {
      onCompleted: data => {
        if (data && data.leaveGroupeAmis) {
          displaySnackBar(client, {
            isOpen: true,
            type: 'SUCCESS',
            message: ' Groupe Supprimeé avec succès',
          });
        }
        // if (listResult && listResult.refetch) {
        //   listResult.refetch();
        // }
      },
      onError: err => {
        if (err.networkError?.message === 'Network request failed') {
          const snackBarData: SnackVariableInterface = {
            type: 'ERROR',
            message: 'Problème de connexion...',
            isOpen: true,
          };
          displaySnackBar(client, snackBarData);
        } else {
          const snackBarData: SnackVariableInterface = {
            type: 'ERROR',
            message: err.message.split(':')[1],
            isOpen: true,
          };
          displaySnackBar(client, snackBarData);
        }
      },
    },
  );
  return {
    doLeaveGroup,
    loading,
  };
};
