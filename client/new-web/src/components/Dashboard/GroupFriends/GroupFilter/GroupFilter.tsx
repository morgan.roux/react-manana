import { Typography } from '@material-ui/core';
import React, { ChangeEvent, Dispatch, FC, SetStateAction } from 'react';
import CustomButton from '../../../Common/CustomButton';
import CustomSelect from '../../../Common/CustomSelect';
import useStyles from './styles';
import { GroupFilterInterface } from './useGroupFilterForm';

interface GroupFilterProps {
  state: GroupFilterInterface;
  setState: Dispatch<SetStateAction<GroupFilterInterface>>;
  handleChangeInput: (e: ChangeEvent<any>) => void;
  onClickApplyBtn: () => void;
  onClickInitBtn: () => void;
}

const GroupFilter: FC<GroupFilterProps> = ({
  state,
  handleChangeInput,
  onClickApplyBtn,
  onClickInitBtn,
}) => {
  const classes = useStyles({});
  const { status, typeGroupeAmis } = state;

  const STATUS_LIST = [
    { id: 'ALL', value: 'Tous les statuts' },
    { id: 'ACTIVER', value: 'Activé' },
    { id: 'BLOQUER', value: 'Bloqué' },
  ];

  const TYPE_LIST = [
    { id: 'ALL', value: 'Tous les types' },
    { id: 'PHARMACIE', value: 'PHARMACIE' },
    { id: 'DEPARTEMENT', value: 'DEPARTEMENT' },
    { id: 'REGION', value: 'REGION' },
    { id: 'PERSONNALISE', value: 'PERSONNALISE' },
  ];

  return (
    <div className={classes.avatarFilterRoot}>
      <Typography className={classes.avatarFilterTitle}>
        Saisissez les filtres qui vous conviennent
      </Typography>
      <div className={classes.avatarFilterFormContainer}>
        <CustomSelect
          label="Type"
          list={TYPE_LIST}
          listId="id"
          index="value"
          name="typeGroupeAmis"
          value={typeGroupeAmis}
          onChange={handleChangeInput}
          placeholder="Type du groupe"
          withPlaceholder={true}
        />
        <CustomSelect
          label="Status"
          list={STATUS_LIST}
          listId="id"
          index="value"
          name="status"
          value={status}
          onChange={handleChangeInput}
          placeholder="Statuts du groupe"
          withPlaceholder={true}
        />
        <div className={classes.avatarFilterBtnsContainer}>
          <CustomButton color="default" onClick={onClickInitBtn}>
            Réinitialiser
          </CustomButton>
          <CustomButton color="secondary" onClick={onClickApplyBtn}>
            Appliquer
          </CustomButton>
        </div>
      </div>
    </div>
  );
};

export default GroupFilter;
