import { ChangeEvent, useState } from 'react';

export interface GroupFilterInterface {
  typeGroupeAmis: string;
  status: string;
}

export const initialState: GroupFilterInterface = {
  typeGroupeAmis: 'ALL',
  status: 'ALL',
};

const useGroupFilterForm = (defaultState?: GroupFilterInterface) => {
  const initValues: GroupFilterInterface = defaultState || initialState;
  const [values, setValues] = useState<GroupFilterInterface>(initValues);

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      setValues(prevState => ({ ...prevState, [name]: value }));
    }
  };

  return {
    handleChange,
    values,
    setValues,
  };
};

export default useGroupFilterForm;
