import { Box } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import React, { useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import CustomButton from '../../Common/CustomButton';
import SubToolbar from '../../Common/newCustomContent/SubToolbar';
import ModalDocument from '../EspaceDocumentaire/EspaceDocumentaire/ModalDocument';
import CreateGroupe from './CreateGroupe';
import GroupList from './GroupList';
import useStyles from './styles';

const GroupFriends: React.FC<RouteComponentProps> = ({
  history: { push },
  location: { pathname },
}) => {
  const [selected, setSelected] = useState<any[]>([]);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [deleteRow, setDeleteRow] = useState<any>(null);
  const [isCreate, setIsCreate] = useState(false);
  const [addedGroup, setAddedGroup] = useState(0);
  const classes = useStyles({});
  const handleIsCreate = () => {
    setIsCreate(true);
  };
  const handleCloseCreate = () => {
    setIsCreate(false);
  };
  const SubToolbarChildren = () => {
    return (
      <Box className={classes.childrenRoot}>
        <CustomButton
          color="secondary"
          startIcon={<Add />}
          onClick={handleIsCreate}
          disabled={false}
        >
          {'Nouveau Groupe'}
        </CustomButton>
      </Box>
    );
  };

  return (
    <Box className={classes.container}>
      <SubToolbar
        title={`Liste de groupe d'amis`}
        dark={false}
        withBackBtn={false}
        onClickBack={() => {}}
      >
        <SubToolbarChildren />
      </SubToolbar>

      <Box className={classes.marginTop}>
        <GroupList
          selected={selected}
          setSelected={setSelected}
          deleteRow={deleteRow}
          setDeleteRow={setDeleteRow}
          openDeleteDialog={openDeleteDialog}
          setOpenDeleteDialog={setOpenDeleteDialog}
          addedGroup={addedGroup}
        />
      </Box>

      <ModalDocument
        label="Nouveau groupe"
        open={isCreate}
        onClose={handleCloseCreate}
        handleClose={handleCloseCreate}
        maxWidth="sm"
        fullWidth={true}
      >
        <CreateGroupe
          addedGroup={addedGroup}
          setAddedGroup={setAddedGroup}
          handleCloseCreate={handleCloseCreate}
        />
      </ModalDocument>
    </Box>
  );
};

export default withRouter(GroupFriends);
