import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    searchInputBox: {
      padding: '20px 0px 0px 20px',
      display: 'flex',
    },
    container: {
      width: '100%',
    },
    childrenRoot: {
      display: 'flex',
      '& > button:not(:nth-last-child(1))': {
        marginRight: 15,
      },
    },
    marginTop: {
      marginTop: 20,
      marginBottom: 20,
    },
  }),
);

export default useStyles;
