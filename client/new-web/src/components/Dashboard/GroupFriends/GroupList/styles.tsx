import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    avatarListRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      position: 'relative',
      height: 'auto',
      padding: '0px 30px',
      '& .MuiTableHead-root th': {
        minWidth: 'auto',
        zIndex: 0,
      },

      '& *': {
        fontFamily: 'Roboto',
      },
    },
  }),
);

export default useStyles;
