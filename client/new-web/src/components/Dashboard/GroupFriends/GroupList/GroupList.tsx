import { useApolloClient, useMutation } from '@apollo/client';
import { Box, Fade, IconButton, Tooltip } from '@material-ui/core';
import { Edit, EnhancedEncryption, NoEncryption, Visibility } from '@material-ui/icons';
import moment from 'moment';
import React, { Dispatch, FC, SetStateAction, useCallback, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import EmptyImgSrc from '../../../../assets/img/img_no_list_item2.png';
import { GROUPE_AMIS } from '../../../../Constant/url';
import { DO_UPDATE_STATUS_GROUP } from '../../../../graphql/GroupeAmis/mutation';
import {
  UPDATE_STATUS_GROUPE,
  UPDATE_STATUS_GROUPEVariables,
} from '../../../../graphql/GroupeAmis/types/UPDATE_STATUS_GROUPE';
import { GroupeAmisStatus } from '../../../../types/graphql-global-types';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop';
import { ConfirmDeleteDialog } from '../../../Common/ConfirmDialog';
import { CustomModal } from '../../../Common/CustomModal';
import CustomTableWithSearch from '../../../Common/CustomTableWithSearch';
import { CustomTableWithSearchColumn } from '../../../Common/CustomTableWithSearch/interface';
import ModalDocument from '../../EspaceDocumentaire/EspaceDocumentaire/ModalDocument';
import { GROUPE_AMIS_ES_TYPE } from '../constant';
import EditGroupe from '../EditGroupe';
import GroupFilter from '../GroupFilter';
import useGroupFilterForm from '../GroupFilter/useGroupFilterForm';
import useStyles from './styles';

interface GroupListProps {
  selected: any[];
  deleteRow: any;
  openDeleteDialog: boolean;
  setSelected: Dispatch<SetStateAction<any[]>>;
  setDeleteRow: Dispatch<SetStateAction<any>>;
  setOpenDeleteDialog: Dispatch<SetStateAction<boolean>>;
  addedGroup: number;
}

const GroupList: FC<GroupListProps & RouteComponentProps> = ({
  selected,
  setSelected,
  setDeleteRow,
  deleteRow,
  openDeleteDialog,
  setOpenDeleteDialog,
  addedGroup,
  history: { push },
}) => {
  const classes = useStyles({});
  const client = useApolloClient();

  const [filterBy, setFilterBy] = useState<any[]>([]);
  const [openFilterModal, setOpenFilterModal] = useState<boolean>(false);
  const [rowToEdit, setRowToEdit] = useState(null);
  const { values, setValues, handleChange } = useGroupFilterForm();
  const { status, typeGroupeAmis } = values;

  const [data, setData] = useState<any[]>([]);

  const emptyTitle = 'Aucun Groupe';
  const emptySubTitle = "Vous n'avez pas encore de groupe ";

  const handleCloseCreate = () => {
    setRowToEdit(null);
  };

  const [updateStatus, { loading: loadingStatus, data: dataStatus }] = useMutation<
    UPDATE_STATUS_GROUPE,
    UPDATE_STATUS_GROUPEVariables
  >(DO_UPDATE_STATUS_GROUP, {
    onCompleted: data => {
      if (data && data.updateGroupeAmisStatus) {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'Status mis à jour avec succès',
          isOpen: true,
        });
      }
    },
    onError: errors => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: 'Erreur lors de la mis à jour de status',
        isOpen: true,
      });
    },
  });

  const handleActiveGroupe = (id: string) => {
    updateStatus({ variables: { id, status: 'ACTIVER' as GroupeAmisStatus } });
  };

  const handleBlockGroupe = (id: string) => {
    updateStatus({ variables: { id, status: 'BLOQUER' as GroupeAmisStatus } });
  };

  const handleEditGroup = (row: any) => {
    setRowToEdit(row);
  };

  const goToSee = (id: string) => {
    push(`/db/${GROUPE_AMIS}/${id}`);
  };

  const ACTIONS_COLUMNS: CustomTableWithSearchColumn[] = [
    {
      key: '',
      label: '',
      numeric: false,
      disablePadding: false,
      sortable: false,
      renderer: (row: any) => {
        return (
          <>
            <Tooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Voir">
              <IconButton
                color="inherit"
                size="small"
                style={{ zIndex: 1 }}
                onClick={() => {
                  goToSee(row.id);
                }}
              >
                <Visibility />
              </IconButton>
            </Tooltip>
            <Tooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Modifier">
              <IconButton
                style={{ zIndex: 1 }}
                color="secondary"
                size="small"
                onClick={() => {
                  handleEditGroup(row);
                }}
              >
                <Edit />
              </IconButton>
            </Tooltip>
            {row.groupStatus === 'ACTIVER' ? (
              <Tooltip
                TransitionComponent={Fade}
                TransitionProps={{ timeout: 600 }}
                title="Bloquer"
              >
                <IconButton
                  style={{ zIndex: 1 }}
                  size="small"
                  color="secondary"
                  onClick={() => {
                    handleBlockGroupe(row.id);
                  }}
                >
                  <NoEncryption />
                </IconButton>
              </Tooltip>
            ) : (
              <Tooltip
                TransitionComponent={Fade}
                TransitionProps={{ timeout: 600 }}
                title="Activer"
              >
                <IconButton
                  style={{ zIndex: 1 }}
                  size="small"
                  color="secondary"
                  onClick={() => {
                    handleActiveGroupe(row.id);
                  }}
                >
                  <EnhancedEncryption />
                </IconButton>
              </Tooltip>
            )}
          </>
        );
      },
    },
  ];

  const DATES_COLUMNS: CustomTableWithSearchColumn[] = [
    {
      key: 'dateCreation',
      label: 'Date de création',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        return row.dateCreation ? moment(row.dateCreation).format('DD/MM/YYYY') : '-';
      },
    },
  ];

  const GROUP_COLUMNS: CustomTableWithSearchColumn[] = [
    {
      key: 'nom',
      label: "Groupe d'amis",
      numeric: false,
      disablePadding: false,
      sortable: true,
    },
    {
      key: 'nbPharmacie',
      label: 'Nombre de pharmacies',
      numeric: true,
      disablePadding: false,
    },
    {
      key: 'nbMember',
      label: 'Nombre de membres',
      numeric: true,
      disablePadding: false,
    },
    {
      key: 'nbPartage',
      label: 'Nombre de partages',
      numeric: true,
      disablePadding: false,
      renderer: (row: any) => {
        console.log('row', row);

        return row.nbPartage || 0;
      },
    },
    {
      key: 'nbRecommandation',
      label: 'Nombre de recommandations',
      numeric: true,
      disablePadding: false,
    },
  ];

  const COLUMNS: CustomTableWithSearchColumn[] = [
    ...GROUP_COLUMNS,
    ...DATES_COLUMNS,
    ...ACTIONS_COLUMNS,
  ];

  const onClickBtnFilter = () => {
    setOpenFilterModal(true);
  };

  const onClickApplyBtn = () => {
    setOpenFilterModal(false);
    // Set filterBy
    const newFilterBy: any[] = [];

    if (typeGroupeAmis && typeGroupeAmis !== 'ALL') {
      const newTerm = { term: { typeGroupeAmis } };
      newFilterBy.push(newTerm);
    }

    if (status && status !== 'ALL') {
      const newTerm = { term: { status } };
      newFilterBy.push(newTerm);
    }
    setFilterBy(newFilterBy);
  };

  const onClickInitBtn = useCallback(() => {
    setOpenFilterModal(false);
    setValues(prevState => ({ ...prevState, status: 'ALL', typeGroupeAmis: 'ALL' }));
    setFilterBy([]);
  }, []);

  const DeleteDialogContent = () => {
    if (deleteRow && deleteRow.description) {
      return (
        <span>
          Êtes-vous sur de vouloir supprimer le groupe
          <span style={{ fontWeight: 'bold' }}> {deleteRow.description}</span> ?
        </span>
      );
    }
    return <span>Êtes-vous sur de vouloir supprimer ces groupes ?</span>;
  };

  return (
    <Box className={classes.avatarListRoot}>
      {loadingStatus && <Backdrop value="Suppression en cours..." />}
      <CustomTableWithSearch
        searchType={GROUPE_AMIS_ES_TYPE}
        isSelectable={false}
        selected={selected}
        setSelected={setSelected}
        showToolbar={true}
        columns={COLUMNS}
        withEmptyContent={true}
        emptyTitle={emptyTitle}
        emptySubTitle={emptySubTitle}
        emptyImgSrc={EmptyImgSrc}
        withInputSearch={true}
        withFilterBtn={true}
        onClickBtnFilter={onClickBtnFilter}
        filterByFromPros={filterBy}
        dataFromProps={data}
        setDataFromProps={setData}
        fetchPolicy={'network-only'}
        addedGroup={addedGroup}
      />
      <CustomModal
        title="Filtres de recherche"
        open={openFilterModal}
        setOpen={setOpenFilterModal}
        closeIcon={true}
        headerWithBgColor={true}
        withBtnsActions={false}
        maxWidth="xl"
      >
        <GroupFilter
          state={values}
          setState={setValues}
          handleChangeInput={handleChange}
          onClickApplyBtn={onClickApplyBtn}
          onClickInitBtn={onClickInitBtn}
        />
      </CustomModal>
      <ModalDocument
        label="Modifier Groupe"
        open={rowToEdit !== null}
        onClose={handleCloseCreate}
        handleClose={handleCloseCreate}
        maxWidth="sm"
        fullWidth={true}
      >
        <EditGroupe row={rowToEdit} setRow={setRowToEdit} />
      </ModalDocument>
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={() => { }}
      />
    </Box>
  );
};

export default withRouter(GroupList);
