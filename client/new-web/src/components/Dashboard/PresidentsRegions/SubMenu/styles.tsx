import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    addButton: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginRight: 15,
        marginTop: 12,
    },
    searchInputBox: {
        padding: '20px 0px 0px 20px',
        display: 'flex',
      },
    deleteButton: {
      marginRight: 12,
    }
  }),
);

export default useStyles;
