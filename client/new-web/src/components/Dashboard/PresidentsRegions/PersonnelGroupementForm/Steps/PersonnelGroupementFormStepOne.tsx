import React, { FC, useState, useEffect, useCallback, ChangeEvent } from 'react';
import useStyles from '../styles';
import {
  Typography,
  RadioGroup,
  FormControlLabel,
  Radio,
  CircularProgress,
  LinearProgress,
} from '@material-ui/core';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import CustomSelect from '../../../../Common/CustomSelect';
import Dropzone from '../../../../Common/Dropzone';
import { PersonnelGroupementFormProps } from '../PersonnelGroupementForm';
import { AWS_HOST } from '../../../../../utils/s3urls';
import { last } from 'lodash';
import { CustomModal } from '../../../../Common/CustomModal';
import { CustomReactEasyCrop } from '../../../../Common/CustomReactEasyCrop';
import { Sexe } from '../../../../../types/graphql-global-types';
import { useLazyQuery } from '@apollo/client';
import { DO_SEARCH_AVATARS } from '../../../../../graphql/Avatar/query';
import {
  SEARCH_AVATARSVariables,
  SEARCH_AVATARS,
} from '../../../../../graphql/Avatar/types/SEARCH_AVATARS';
import { getGroupement } from '../../../../../services/LocalStorage';

const PersonnelGroupementFormStepOne: FC<PersonnelGroupementFormProps> = ({
  title,
  selectedFiles,
  setSelectedFiles,
  values,
  handleChangeInput,
  imageUrl,
  setImageUrl,
  // croppedFiles,
  setCroppedFiles,
  avatarImageSrc,
}) => {
  const classes = useStyles({});
  const groupement = getGroupement();
  const idGroupement = groupement && groupement.id;

  const [openResize, setOpenResize] = useState<boolean>(false);
  const [croppedImgUrl, setCroppedImgUrl] = useState<any>('');
  const [savedCroppedImage, setSavedCroppedImage] = useState<boolean>(false);

  const [imgType, setImgType] = useState<'AVATAR' | 'IMPORT'>('AVATAR');

  const [avatarFilters, setAvatarFilters] = useState({ searchKeyWord: '', searchSexe: 'ALL' });
  const { searchKeyWord, searchSexe } = avatarFilters;

  const [take, setTake] = useState<number | null>(10);
  const defaultFilterBy = [{ term: { idGroupement } }];
  const [filterBy, setFilterBy] = useState<any[]>(defaultFilterBy);

  const [avatarList, setAvatarList] = useState<any[]>([]);
  const [selectedAvatar, setSelectedAvatar] = useState<any>(null);

  const AVATAR_SEXE_LIST = [
    { id: 'ALL', value: 'Tous' },
    { id: Sexe.M, value: 'Homme' },
    { id: Sexe.F, value: 'Femme' },
  ];

  const image: any = null;

  const handleChangeRadioGroup = (event: React.ChangeEvent<HTMLInputElement>) => {
    setImgType((event.target as HTMLInputElement).value as any);
  };

  const deleteAvatar = () => {
    console.log(' Delete Avatar Photo :>> ');
  };

  const handleClickResize = useCallback(() => {
    setOpenResize(true);
    // Reset cropped image
    setSavedCroppedImage(false);
    setCroppedImgUrl(null);
  }, []);

  const handleChangeFilter = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      setAvatarFilters(prevState => ({ ...prevState, [name]: value }));
      setTake(null);
      if (name === 'searchKeyWord') {
        const filter = value.length > 0 ? [{ wildcard: { description: `*${value}*` } }] : [];
        setFilterBy([...defaultFilterBy, ...filter]);
      }
      if (name === 'searchSexe') {
        if (value === 'M' || value === 'F') {
          const filter = [{ term: { codeSexe: value } }];
          setFilterBy([...defaultFilterBy, ...filter]);
        } else if (value === 'ALL') {
          const filter = [{ terms: { codeSexe: ['M', 'F'] } }];
          setFilterBy([...defaultFilterBy, ...filter]);
        }
      }
    }
  };

  const handleClickAvatar = (avatar: any) => () => {
    setSelectedAvatar(avatar);
  };

  /**
   * Get avatars list
   */
  const [
    searchAvatars,
    { data: avatarData, loading: avatarLoading, error: avatarError },
  ] = useLazyQuery<SEARCH_AVATARS, SEARCH_AVATARSVariables>(DO_SEARCH_AVATARS);

  /**
   * Set avatarList
   */
  useEffect(() => {
    if (avatarData && avatarData.search && avatarData.search.data) {
      setAvatarList(avatarData.search.data);
    }
  }, [avatarData]);

  console.log('avatarData **::>> ', avatarData);

  /**
   * Reset preview
   */
  useEffect(() => {
    if (selectedFiles && selectedFiles.length > 0) {
      // Reset cropped image
      setCroppedImgUrl(null);
      // Open Resize
      handleClickResize();
    }
  }, [selectedFiles]);

  const saveResizedImage = () => {
    setOpenResize(false);
    setSavedCroppedImage(true);
  };

  /**
   * Set image url
   */
  useEffect(() => {
    if (image && image.chemin) {
      setImageUrl(`${AWS_HOST}/${image.chemin}`);
    }

    if (selectedFiles && selectedFiles.length > 0) {
      const file = last(selectedFiles);
      if (file) {
        const url = URL.createObjectURL(file);
        setImageUrl(url);
      }
    }
  }, [image, selectedFiles]);

  /**
   * Execute search avatars
   */
  useEffect(() => {
    if (imgType === 'AVATAR') {
      searchAvatars({
        variables: {
          type: ['avatar'],
          skip: 0,
          take,
          query: '',
          sortBy: [{ description: { order: 'desc' } }],
          filterBy,
        },
      });
    }
  }, [imgType, take, filterBy]);

  return (
    <div className={classes.personnelGroupementFormRoot}>
      <div className={classes.imgFormContainer}>
        <Typography className={classes.inputTitle}>Votre profil</Typography>
        <RadioGroup
          row={true}
          aria-label="imgType"
          name="imgType"
          value={imgType}
          onChange={handleChangeRadioGroup}
          className={classes.radioGroup}
        >
          <FormControlLabel
            value="AVATAR"
            control={<Radio color="primary" />}
            label="Sélectionner un avatar"
          />
          <Typography>ou</Typography>
          <FormControlLabel
            value="IMPORT"
            control={<Radio color="primary" />}
            label="Télécharger une photo"
          />
        </RadioGroup>
        {imgType === 'AVATAR' ? (
          <div className={classes.avatarListContainer}>
            <div className={classes.avatarListFilterContainer}>
              <CustomFormTextField
                name="searchKeyWord"
                value={searchKeyWord}
                onChange={handleChangeFilter}
                placeholder="Mots clés"
              />
              <CustomSelect
                label="Sexe"
                list={AVATAR_SEXE_LIST}
                listId="id"
                index="value"
                name="searchSexe"
                value={searchSexe}
                onChange={handleChangeFilter}
                shrink={true}
              />
            </div>
            <div className={classes.avatarListImgContainer}>
              {avatarLoading && (
                <LinearProgress
                  color="secondary"
                  style={{ width: '100%', top: -15, position: 'absolute', height: 2 }}
                />
              )}
              {!avatarLoading && avatarList.length > 0 && (
                <>
                  {avatarList.map((item: any, index: number) => {
                    return (
                      <div key={`avatar_list_${index}`} className={classes.avatarContainer}>
                        <img
                          src={(item.fichier && item.fichier.publicUrl) || ''}
                          onClick={handleClickAvatar(item)}
                          className={
                            selectedAvatar && selectedAvatar.id === item.id
                              ? classes.selectedAvatar
                              : ''
                          }
                        />
                        <span>{item.description}</span>
                      </div>
                    );
                  })}
                </>
              )}
              {!avatarLoading && !avatarError && avatarList.length === 0 && (
                <span className={classes.textMsg}>Aucun avatar trouvé 😃</span>
              )}
              {avatarError && <span className={classes.textMsg}>Error 😩</span>}
            </div>
          </div>
        ) : (
          <Dropzone
            contentText="Glissez et déposez ici votre photo"
            selectedFiles={selectedFiles}
            setSelectedFiles={setSelectedFiles}
            multiple={false}
            acceptFiles="image/*"
            withFileIcon={false}
            where="inUserSettings"
            withImagePreview={true}
            withImagePreviewCustomized={true}
            onClickDelete={deleteAvatar}
            fileAlreadySetUrl={croppedImgUrl || (imageUrl as any)}
            onClickResize={handleClickResize}
          />
        )}
      </div>
      <div className={classes.infoPersoContainer}>
        <Typography className={classes.inputTitle}>Informations personnelles</Typography>
        <div className={classes.inputsContainer}>
          <div className={classes.formRow}>
            <Typography>Civilité</Typography>
            <CustomSelect
              label=""
              // list={AVATAR_SEXE_LIST}
              listId="id"
              index="value"
              name="civilite"
              // value={civilite}
              onChange={handleChangeInput}
              shrink={false}
              placeholder="Choisissez une civilité"
              withPlaceholder={true}
            />
          </div>
          <div className={classes.formRow}>
            <Typography>Prénom</Typography>
            <CustomFormTextField
              name="prenom"
              // value={prenom}
              onChange={handleChangeInput}
              placeholder="Prénom de la personne"
            />
          </div>
          <div className={classes.formRow}>
            <Typography>Nom</Typography>
            <CustomFormTextField
              name="nom"
              // value={nom}
              onChange={handleChangeInput}
              placeholder="Nom de la personne"
            />
          </div>

          <div className={classes.formRow}>
            <Typography>Sexe</Typography>
            <CustomSelect
              label=""
              // list={AVATAR_SEXE_LIST}
              listId="id"
              index="value"
              name="sexe"
              // value={sexe}
              onChange={handleChangeInput}
              shrink={false}
              placeholder="Sexe de la personne"
              withPlaceholder={true}
            />
          </div>
          <div className={classes.formRow}>
            <Typography>Email</Typography>
            <CustomFormTextField
              name="email"
              // value={email}
              onChange={handleChangeInput}
              placeholder="Son email"
            />
          </div>
          <div className={classes.formRow}>
            <Typography>Téléphone</Typography>
            <CustomFormTextField
              name="tel"
              // value={tel}
              onChange={handleChangeInput}
              placeholder="Son numéro"
            />
          </div>
        </div>
      </div>
      <div className={classes.infoPersoContainer}>
        <Typography className={classes.inputTitle}>Responsabilité</Typography>
        <div className={classes.inputsContainer}>
          <div className={classes.formRow}>
            <Typography>Rôle</Typography>
            <CustomSelect
              label=""
              // list={AVATAR_SEXE_LIST}
              listId="id"
              index="value"
              name="role"
              // value={role}
              onChange={handleChangeInput}
              shrink={false}
              placeholder="Rôle de la personne"
              withPlaceholder={true}
            />
          </div>
        </div>
      </div>

      <CustomModal
        open={openResize}
        setOpen={setOpenResize}
        title="Redimensionnement"
        onClickConfirm={saveResizedImage}
        actionButton="Enregistrer"
        closeIcon={true}
        centerBtns={true}
      >
        {avatarImageSrc ? (
          <CustomReactEasyCrop
            src={avatarImageSrc}
            withZoom={true}
            croppedImage={croppedImgUrl}
            setCroppedImage={setCroppedImgUrl}
            setCropedFiles={setCroppedFiles}
            savedCroppedImage={savedCroppedImage}
          />
        ) : (
          <div>Aucune Image</div>
        )}
      </CustomModal>
    </div>
  );
};

export default PersonnelGroupementFormStepOne;
