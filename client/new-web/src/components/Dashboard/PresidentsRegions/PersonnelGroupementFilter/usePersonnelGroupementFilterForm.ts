import { useState, ChangeEvent } from 'react';

export interface PersonnelFilterInterface {
  service: string;
  role: string;
  status: string;
  sortie: string;
}

export const initialState: PersonnelFilterInterface = {
  service: 'ALL',
  role: 'ALL',
  status: 'ALL',
  sortie: '0',
};

const usePersonnelGroupementFilterForm = (defaultState?: PersonnelFilterInterface) => {
  const initValues: PersonnelFilterInterface = defaultState || initialState;
  const [values, setValues] = useState<PersonnelFilterInterface>(initValues);

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      setValues(prevState => ({ ...prevState, [name]: value }));
    }
  };

  return {
    handleChange,
    values,
    setValues,
  };
};

export default usePersonnelGroupementFilterForm;
