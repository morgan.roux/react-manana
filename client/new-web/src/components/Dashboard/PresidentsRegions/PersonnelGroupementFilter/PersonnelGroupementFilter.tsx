import React, { Dispatch, SetStateAction, ChangeEvent, FC } from 'react';
import { Typography, CircularProgress } from '@material-ui/core';
import useStyles from './styles';
import CustomSelect from '../../../Common/CustomSelect';
import CustomButton from '../../../Common/CustomButton';
import { PersonnelFilterInterface } from './usePersonnelGroupementFilterForm';
import { useQuery } from '@apollo/client';
import { GET_MINIM_SERVICES } from '../../../../graphql/Service';
import { MINIM_SERVICES } from '../../../../graphql/Service/types/MINIM_SERVICES';
import { SEARCH_ROLES, SEARCH_ROLESVariables } from '../../../../graphql/Role/types/SEARCH_ROLES';
import { DO_SEARCH_ROLES } from '../../../../graphql/Role';

interface PersonnelGroupementFilterProps {
  state: PersonnelFilterInterface;
  setState: Dispatch<SetStateAction<PersonnelFilterInterface>>;
  handleChangeInput: (e: ChangeEvent<any>) => void;
  onClickApplyBtn: () => void;
  onClickInitBtn: () => void;
}

const PersonnelGroupementFilter: FC<PersonnelGroupementFilterProps> = ({
  state,
  handleChangeInput,
  onClickApplyBtn,
  onClickInitBtn,
}) => {
  const classes = useStyles({});
  const { service, role, status, sortie } = state;

  const STATUT_LIST = [
    { code: 'ALL', value: 'Tous les statuts' },
    { code: 'ACTIVATED', value: 'Activé' },
    { code: 'BLOCKED', value: 'Désactivé' },
    { code: 'ACTIVATION_REQUIRED', value: "Demande d'activation" },
    { code: 'RESETED', value: 'Mot de passe réinitialisé' },
  ];

  const SORTIE_LIST = [
    { code: 'ALL', value: 'Tous' },
    { code: '0', value: 'Non' },
    { code: '1', value: 'Oui' },
  ];

  /**
   * Get services list
   */
  const { data: servicesData, loading: servicesLoading } = useQuery<MINIM_SERVICES>(
    GET_MINIM_SERVICES,
  );
  const SERVICE_DATA_LIST: any[] = (servicesData && servicesData.services) || [];
  const SERVICE_LIST = [
    ...[{ id: '-1', code: 'ALL', nom: 'Tous les services' }],
    ...SERVICE_DATA_LIST,
  ];

  /**
   * Get roles list
   */
  const { data: rolesData, loading: rolesLoading } = useQuery<SEARCH_ROLES, SEARCH_ROLESVariables>(
    DO_SEARCH_ROLES,
    {
      variables: {
        type: ['role'],
        filterBy: [{ term: { typeRole: 'GROUPEMENT' } }],
        sortBy: [{ nom: { order: 'asc' } }],
      },
    },
  );
  const ROLE_DATA_LIST: any[] = (rolesData && rolesData.search && rolesData.search.data) || [];
  const ROLE_LIST = [...[{ id: '-1', code: 'ALL', nom: 'Tous les rôles' }], ...ROLE_DATA_LIST];

  console.log('rolesData :>> ', rolesData);

  return (
    <div className={classes.personnelGroupementFilterRoot}>
      {(servicesLoading || rolesLoading) && (
        <CircularProgress
          color="secondary"
          style={{ width: 20, height: 20, top: 18, position: 'absolute' }}
        />
      )}
      <Typography className={classes.personnelGroupementFilterTitle}>
        Saisissez les filtres qui vous conviennent
      </Typography>
      <div className={classes.personnelGroupementFilterFormContainer}>
        <CustomSelect
          label="Services"
          list={SERVICE_LIST}
          listId="code"
          index="nom"
          name="service"
          value={service}
          onChange={handleChangeInput}
        />
        <CustomSelect
          label="Rôle"
          list={ROLE_LIST}
          listId="code"
          index="nom"
          name="role"
          value={role}
          onChange={handleChangeInput}
        />
        <CustomSelect
          label="Statut"
          list={STATUT_LIST}
          listId="code"
          index="value"
          name="status"
          value={status}
          onChange={handleChangeInput}
        />
        <CustomSelect
          label="Sortie"
          list={SORTIE_LIST}
          listId="code"
          index="value"
          name="sortie"
          value={sortie}
          onChange={handleChangeInput}
        />
        <div className={classes.personnelGroupementFilterBtnsContainer}>
          <CustomButton color="default" onClick={onClickInitBtn}>
            Réinitialiser
          </CustomButton>
          <CustomButton color="secondary" onClick={onClickApplyBtn}>
            Appliquer
          </CustomButton>
        </div>
      </div>
    </div>
  );
};

export default PersonnelGroupementFilter;
