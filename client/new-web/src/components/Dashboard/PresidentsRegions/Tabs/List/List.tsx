import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { PRESIDENT_REGION_URL } from '../../../../../Constant/url';
import CustomContent from '../../../../Common/newCustomContent';
import SubMenu from '../../SubMenu';
import { useButtonHeadAction, useCreatePersonnel } from '../../utils/utils';
interface ListAffectationProps {
  listResult?: any;
  columns?: any;
  pathname: any;
  values: any;
  sexe: any;
  idGroupement: string;
  civilite: string | null | undefined;
  nom: string | null | undefined;
  codeService: string;
  personnelOnClickConfirmDelete: any;
  personnelDeleteRow: any;
  openDeleteDialog: any;
  setOpenDeleteDialog: any;
}

const List: React.FC<ListAffectationProps & RouteComponentProps> = ({
  listResult,
  columns,
  pathname,
  values,
  sexe,
  idGroupement,
  civilite,
  nom,
  codeService,
  history: { push },
  personnelOnClickConfirmDelete,
  personnelDeleteRow,
  openDeleteDialog,
  setOpenDeleteDialog,
}) => {
  const isOnList =
    pathname === `/db/${PRESIDENT_REGION_URL}` ||
    `/db/${PRESIDENT_REGION_URL}/affectation` ||
    `/db/${PRESIDENT_REGION_URL}/list`;
  const isOnCreate = pathname === `/db/${PRESIDENT_REGION_URL}/create`;
  const isOnEdit: boolean = pathname.startsWith(`/db/${PRESIDENT_REGION_URL}/edit`);

  const [createPersonnel, mutationSuccess] = useCreatePersonnel({
    variables: { ...values, sexe: sexe as any, idGroupement },
  });
  const disabledSaveBtn = (): boolean => {
    if (!isOnList && (!civilite || !nom || !codeService)) {
      return true;
    }
    return false;
  };
  const [goToAddPersonnel, goBack] = useButtonHeadAction(push);
  return (
    <>
      <SubMenu
        isOnList={isOnList}
        isOnCreate={isOnCreate}
        isOnEdit={isOnEdit}
        createPersonnel={createPersonnel}
        disabledSaveBtn={disabledSaveBtn}
        goToAddPersonnel={goToAddPersonnel}
        goBack={goBack}
        personnelOnClickConfirmDelete={personnelOnClickConfirmDelete}
        personnelDeleteRow={personnelDeleteRow}
        openDeleteDialog={openDeleteDialog}
        setOpenDeleteDialog={setOpenDeleteDialog}
      />

      <CustomContent {...{ listResult, columns }} />
    </>
  );
};

export default withRouter(List);
