import { useApolloClient, useMutation } from '@apollo/client';
import { differenceBy } from 'lodash';
import { Dispatch, SetStateAction, useContext } from 'react';
import { ContentContext, ContentStateInterface } from '../../../../AppContext';
import { DO_DELETE_SOFT_PPERSONNELS } from '../../../../graphql/Ppersonnel';
import {
  DELETE_SOFT_PPERSONNELS,
  DELETE_SOFT_PPERSONNELSVariables,
} from '../../../../graphql/Ppersonnel/types/DELETE_SOFT_PPERSONNELS';
import { displaySnackBar } from '../../../../utils/snackBarUtils';

/**
 *  Delete personnel pharmacie hooks
 */
const useDeletePersonnelPharmacie = (
  selected: any[],
  setSelected: Dispatch<SetStateAction<any[]>>,
  deleteRow: any,
  setDeleteRow: Dispatch<SetStateAction<any>>,
) => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const selectedIds = (selected.length > 0 && selected.map(i => i.id)) || [];
  const client = useApolloClient();

  const [deletePpersonnels, { data, loading }] = useMutation<
    DELETE_SOFT_PPERSONNELS,
    DELETE_SOFT_PPERSONNELSVariables
  >(DO_DELETE_SOFT_PPERSONNELS, {
    variables: { ids: selectedIds },
    update: (cache, { data }) => {
      if (data && data.deleteSoftPpersonnels) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables,
          });
          if (req && req.search && req.search.data) {
            const source = req.search.data;
            const result = data.deleteSoftPpersonnels;
            const diff = differenceBy(source, result, 'id');
            cache.writeQuery({
              query: operationName,
              data: { search: { ...req.search, ...{ data: diff } } },
              variables,
            });
              // TODO: Migration
            /*(client as any).writeData({
              data: { checkedsPpersonnel: differenceBy(selected, result, 'id') },
            });*/
          }
        }
      }
    },
    onCompleted: data => {
      if (data && data.deleteSoftPpersonnels) {
        if (selected.length > 0) setSelected([]);
        if (deleteRow) setDeleteRow(null);
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'Personnel(s) supprimé(s) avec succès',
          isOpen: true,
        });
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  return { deletePpersonnels, data, loading };
};

export default useDeletePersonnelPharmacie;
