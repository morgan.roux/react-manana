import { QueryLazyOptions, useApolloClient, useQuery } from '@apollo/client';
import { useContext, useState } from 'react';
import { ContentContext, ContentStateInterface } from '../../../../AppContext';
import { PHARMACIE_URL } from '../../../../Constant/url';
import {
  PERSONNELVariables,
  PERSONNEL_personnel,
} from '../../../../graphql/Personnel/types/PERSONNEL';
import { GET_CHECKEDS_PHARMACIE } from '../../../../graphql/Pharmacie/local';

/**
 *
 *  Create personnel hooks
 *
 */
export const useCreatePharmacie = (mutationVariables: any): [(variables: any) => any, boolean] => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const client = useApolloClient();

  const [mutationSuccess, setMutationSuccess] = useState<boolean>(false);

  // const [createUpdatePersonnel, { loading: mutationLoading }] = useMutation<
  //   CREATE_PERSONNEL,
  //   CREATE_PERSONNELVariables
  // >(DO_CREATE_PERSONNEL, {
  //   update: (cache, { data }) => {
  //     console.log(mutationVariables);
  //     if (
  //       data &&
  //       data.createUpdatePersonnel &&
  //       mutationVariables &&
  //       mutationVariables.variables &&
  //       !mutationVariables.variables.id
  //     ) {
  //       if (variables && operationName) {
  //         const req: any = cache.readQuery({
  //           query: operationName,
  //           variables: variables,
  //         });
  //         if (req && req.search && req.search.data) {
  //           cache.writeQuery({
  //             query: operationName,
  //             data: {
  //               search: {
  //                 ...req.search,
  //                 ...{
  //                   data: [...req.search.data, data.createUpdatePersonnel],
  //                 },
  //               },
  //             },

  //             variables: variables,
  //           });
  //         }
  //       }
  //     }
  //   },
  //   onCompleted: data => {
  //     if (data && data.createUpdatePersonnel) {
  //       setMutationSuccess(true);
  //     }
  //   },
  //   onError: errors => {
  //     errors.graphQLErrors.map(err => {
  //       displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
  //     });
  //     displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
  //   },
  // });

  const createUpdatePharmacie = () => { };

  return [createUpdatePharmacie, mutationSuccess];
};

/**
 *
 *  Delete personnel hooks
 *
 */

export const useDeletePharmacie = () => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);
  const checkeds = useCheckedPharmacie();

  const client = useApolloClient();

  // const [deletePharmacies, { loading, data: deleteData }] = useMutation<
  //   DELETE_PERSONNELS,
  //   DELETE_PERSONNELSVariables
  // >(DO_DELETE_PERSONNELS, {
  //   update: (cache, { data }) => {
  //     if (data && data.deletePersonnels) {
  //       if (variables && operationName) {
  //         const req: any = cache.readQuery({
  //           query: operationName,
  //           variables: variables,
  //         });
  //         if (req && req.search && req.search.data) {
  //           const source = req.search.data;
  //           const result = data.deletePersonnels;
  //           const dif = differenceBy(source, result, 'id');
  //           cache.writeQuery({
  //             query: operationName,
  //             data: {
  //               search: {
  //                 ...req.search,
  //                 ...{
  //                   data: dif,
  //                 },
  //               },
  //             },
  //             variables: variables,
  //           });
  //           (client as any).writeData({
  //             data: { checkedsPersonnelGroupement: differenceBy(checkeds, result, 'id') },
  //           });
  //         }
  //       }
  //     }
  //   },
  //   onError: errors => {
  //     errors.graphQLErrors.map(err => {
  //       displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
  //     });
  //   },
  // });

  const deletePharmacies = () => { };
  return deletePharmacies;
};

/**
 *
 *  list checked personnel groupement hooks
 *
 */

export const useCheckedPharmacie = () => {
  const checkedsPharmacie = useQuery(GET_CHECKEDS_PHARMACIE);
  return checkedsPharmacie && checkedsPharmacie.data && checkedsPharmacie.data.checkedsPharmacie;
};

/**
 *
 *  subtoolbar button action hooks
 *
 */

export const useButtonHeadAction = (push: any) => {
  const goToAddPharmacie = () => push(`/db/${PHARMACIE_URL}/create`);
  const goBack = () => push(`/db/${PHARMACIE_URL}`);

  return [goToAddPharmacie, goBack];
};

/**
 *
 *  get personnel hooks
 *
 */

export const useGetPharmacie = (): [
  (options?: QueryLazyOptions<PERSONNELVariables> | undefined) => void,
  PERSONNEL_personnel | null | undefined,
] => {
  const client = useApolloClient();

  // const [getPersonnel, { data, loading: personnelLoading }] = useLazyQuery<
  //   PERSONNEL,
  //   PERSONNELVariables
  // >(GET_PERSONNEL, {
  //   onError: errors => {
  //     errors.graphQLErrors.map(err => {
  //       displaySnackBar(client, {
  //         type: 'ERROR',
  //         message: err.message,
  //         isOpen: true,
  //       });
  //     });
  //   },
  // });

  // const personnel = data && data.personnel;

  const getPharmacie = () => { };
  const pharmacie = null;
  return [getPharmacie, pharmacie];
};
