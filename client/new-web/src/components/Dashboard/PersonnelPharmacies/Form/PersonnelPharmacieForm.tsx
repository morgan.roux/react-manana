import { useApolloClient, useLazyQuery, useQuery } from '@apollo/client';
import classnames from 'classnames';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { PERSONNEL_PHARMACIE_URL } from '../../../../Constant/url';
import { CIVILITE_LIST } from '../../../../Constant/user';
import { GET_MY_PHARMACIE, GET_PHARMACIES } from '../../../../graphql/Pharmacie/query';
import { MY_PHARMACIE } from '../../../../graphql/Pharmacie/types/MY_PHARMACIE';
import { PHARMACIES, PHARMACIES_pharmacies } from '../../../../graphql/Pharmacie/types/PHARMACIES';
import { GET_PERSONNEL_PHARMACY } from '../../../../graphql/Ppersonnel';
import {
  PERSONNEL_PHARMACY,
  PERSONNEL_PHARMACYVariables,
} from '../../../../graphql/Ppersonnel/types/PERSONNEL_PHARMACY';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import { isEmailValid } from '../../../../utils/Validator';
import Backdrop from '../../../Common/Backdrop';
import CustomAutocomplete from '../../../Common/CustomAutocomplete';
import CustomButton from '../../../Common/CustomButton';
import { CustomCheckbox } from '../../../Common/CustomCheckbox';
import CustomSelect from '../../../Common/CustomSelect';
import { CustomFormTextField } from '../../../Common/CustomTextField';
import PersonnelPharmacieFormInterface from '../Interface/PersonnelPharmacieFormInterface';
import useStyles from './styles';
import usePersonnelPharmacieForm from './usePersonnelPharmacieForm';

const PersonnelPharmacieForm: FC<RouteComponentProps & PersonnelPharmacieFormInterface> = ({
  history,
  defaultState,
  submit,
  loading,
  location: { pathname },
  match: { params },

  // startUpload,
  // setStartUpload,
  // setFileUploadResult,
  // withImagePreview,
  // fileIdentifierPrefix,
  // setFilesOut,
  // uploadDirectory,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();

  const { pPersonnelId } = params as any;
  const EDIT_URL = `/db/${PERSONNEL_PHARMACIE_URL}/edit/${pPersonnelId}`;

  const {
    values: {
      pharmacie,
      idPharmacie,
      nom,
      prenom,
      civilite,
      telBureau,
      telMobile,
      mail,
      estAmbassadrice,
      commentaire,
    },
    handleChange,
    handleChangeAutocomplete,
    handleSubmit,
    setValues,
  } = usePersonnelPharmacieForm(submit, defaultState);

  const getPharmacies = useQuery<PHARMACIES>(GET_PHARMACIES, {
    fetchPolicy: 'network-only',
  });

  const [disabledAutocomplete, setDisabledAutocomplete] = useState<boolean>(false);

  const getMyPharmacie = useQuery<MY_PHARMACIE>(GET_MY_PHARMACIE);

  const myPharmacie =
    (getMyPharmacie &&
      getMyPharmacie.data &&
      getMyPharmacie.data.me &&
      getMyPharmacie.data.me.pharmacie) ||
    null;

  let pharmacieOptions: Array<PHARMACIES_pharmacies | null> | null = [];
  if (getPharmacies.data && getPharmacies.data.pharmacies) {
    pharmacieOptions = getPharmacies.data.pharmacies;
  }

  // Check my pharmacy and update pharmacy options
  useEffect(() => {
    if (myPharmacie && pharmacieOptions) {
      // If myPharmacy find it on  pharmacieOptions
      const pharma = pharmacieOptions.find(p => p && p.id === myPharmacie.id);
      if (pharma && myPharmacie.id === pharma.id) {
        setDisabledAutocomplete(true);
        setValues(prevState => ({ ...prevState, pharmacie: pharma as any }));
        setValues(prevState => ({ ...prevState, idPharmacie: pharma.id }));
      }
    }
  }, [getMyPharmacie, pharmacieOptions]);

  const isDisabled = () => {
    if (!idPharmacie || !nom) return true;
    if (mail && !isEmailValid(mail)) return true;
    return false;
  };

  const goToBack = () => {
    history.goBack();
  };

  /**
   * Update
   */
  const [getPpersonnel, { loading: pLoading }] = useLazyQuery<
    PERSONNEL_PHARMACY,
    PERSONNEL_PHARMACYVariables
  >(GET_PERSONNEL_PHARMACY, {
    onCompleted: data => {
      if (data && data.ppersonnel) {
        const pp = data.ppersonnel;
        const ph = pp.pharmacie;
        setValues(prevState => ({
          ...prevState,
          id: pp.id,
          estAmbassadrice: pp.estAmbassadrice || false,
          civilite: pp.civilite || '',
          commentaire: pp.commentaire || '',
          nom: pp.nom || '',
          prenom: pp.prenom || '',
          pharmacie: (ph as any) || undefined,
          idPharmacie: (ph && ph.id) || '',
          sortie: pp.sortie === 1 ? true : false,
          mail: (pp.contact && pp.contact.mailProf) || '',
          telBureau: (pp.contact && pp.contact.telProf) || '',
          telMobile: (pp.contact && pp.contact.telMobProf) || '',
        }));
      }
    },
    onError: error => {
      console.log('error :>> ', error);
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  useEffect(() => {
    if (pPersonnelId && pathname === EDIT_URL) {
      getPpersonnel({ variables: { id: pPersonnelId } });
    }
  }, [pathname, pPersonnelId]);

  /**
   * End update
   */

  return (
    <div className={classes.form}>
      {((getPharmacies.loading && !getPharmacies.data) || pLoading) && <Backdrop />}
      {/* <Dropzone
        customLabel={'Glissez-déposez votre photo ici'}
        setStartUpload={setStartUpload}
        startUpload={startUpload}
        setFileUploadResult={setFileUploadResult}
        withImagePreview={withImagePreview}
        uploadDirectory={uploadDirectory}
        identifierPrefix={fileIdentifierPrefix}
        setFilesOut={setFilesOut}
      /> */}
      <div className={classes.marginBottom}>
        <CustomAutocomplete
          id="idPharmacie"
          options={myPharmacie ? [myPharmacie] : pharmacieOptions}
          value={pharmacie}
          disabled={disabledAutocomplete}
          onAutocompleteChange={handleChangeAutocomplete}
          optionLabelKey="nom"
          label="Pharmacie"
        />
      </div>

      <div className={classnames(classes.marginBottom, classes.flex)}>
        <div className={classnames(classes.marginRight, classes.w100)}>
          <CustomFormTextField
            name="prenom"
            label="Prénom"
            value={prenom}
            onChange={handleChange}
          />
        </div>
        <div className={classes.w100}>
          <CustomFormTextField
            name="nom"
            label="Nom"
            value={nom}
            onChange={handleChange}
            required={true}
          />
        </div>
      </div>
      <div className={classes.marginBottom}>
        <CustomSelect
          label="Civilité"
          list={CIVILITE_LIST}
          listId="id"
          index="value"
          name="civilite"
          value={civilite}
          onChange={handleChange}
        />
      </div>
      <div className={classnames(classes.marginBottom, classes.flex)}>
        <div className={classnames(classes.marginRight, classes.w100)}>
          <CustomFormTextField
            name="telBureau"
            label="Téléphone Bureau"
            value={telBureau}
            onChange={handleChange}
          />
        </div>
        <div className={classes.w100}>
          <CustomFormTextField
            name="telMobile"
            label="Téléphone Mobile"
            value={telMobile}
            onChange={handleChange}
          />
        </div>
      </div>

      <div className={classes.marginBottom}>
        <CustomFormTextField
          name="mail"
          label="Email Messagerie"
          value={mail}
          onChange={handleChange}
          error={!isEmailValid(mail)}
        />
      </div>

      <div className={classes.marginBottom}>
        <CustomFormTextField
          name="commentaire"
          label="Commentaire"
          value={commentaire}
          onChange={handleChange}
        />
      </div>

      <div className={classes.marginBottom}>
        <CustomCheckbox
          name="estAmbassadrice"
          label="Ambassadeur"
          value={estAmbassadrice}
          checked={estAmbassadrice}
          onChange={handleChange}
        />
      </div>

      <div className={classnames(classes.marginBottom, classes.flex)}>
        <div className={classnames(classes.marginRight, classes.w100)}>
          <CustomButton size="large" fullWidth={true} onClick={goToBack}>
            RETOUR
          </CustomButton>
        </div>
        <div className={classes.w100}>
          <CustomButton
            size="large"
            color="secondary"
            fullWidth={true}
            disabled={isDisabled() || loading}
            onClick={handleSubmit}
          >
            {loading ? 'Chargement...' : 'ENREGISTRER'}
          </CustomButton>
        </div>
      </div>
    </div>
  );
};

export default withRouter(PersonnelPharmacieForm);
