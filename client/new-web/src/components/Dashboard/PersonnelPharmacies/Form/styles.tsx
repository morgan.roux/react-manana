import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    form: {
      maxWidth: 502,
      '@media (max-width: 1024px)': {
        margin: '0px 20px',
      },
    },
    marginBottom: {
      marginBottom: 25,
    },
    marginRight: {
      marginRight: 10,
    },
    titleForm: {
      color: '#1D1D1D',
      marginTop: 0,
    },
    flex: {
      display: 'flex',
      flexDirection: 'row',
    },
    w100: {
      width: '100%',
    },
    checkboxs: {
      '@media (max-width: 1024px)': {
        justifyContent: 'center',
      },
      '& div': {
        '@media (max-width: 1024px)': {
          width: 'auto',
        },
      },
    },
    autocomplete: {
      width: '100%',
      '& .MuiAutocomplete-inputRoot input': {
        padding: '2.5px 14px !important',
      },
      '& input': {
        color: '#000000',
      },
      '& label': {
        color: '#000000',
      },
      '&:hover label, &.Mui-focused label': {
        color: '#B3D00A',
      },
      '&.Mui-focused fieldset': {
        border: '2px solid #B3D00A !important',
      },
      '& input:invalid + fieldset': {
        borderColor: 'red',
      },
      '& .Mui-focused .MuiIconButton-label svg': {
        color: '#B3D00A !important',
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: '#ffffff',
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: '#000000',
        },
        '&:hover fieldset': {
          borderColor: '#B3D00A',
        },
        '&.Mui-focused fieldset': {
          borderColor: '#B3D00A',
        },
      },
    },
  }),
);

export default useStyles;
