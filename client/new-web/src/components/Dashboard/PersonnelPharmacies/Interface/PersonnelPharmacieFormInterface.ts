import PersonnelPharmacieInterface from './PersonnelPharmacieInterface';
import { AxiosResponse } from 'axios';
import React from 'react';

export default interface PersonnelPharmacieFormInterface {
  defaultState?: PersonnelPharmacieInterface;
  loading: boolean;
  startUpload: boolean;
  setStartUpload: (state: boolean) => void;
  setFileUploadResult: React.Dispatch<React.SetStateAction<AxiosResponse<any> | null>>;
  withImagePreview?: boolean;
  fileIdentifierPrefix?: string;
  setFilesOut?: React.Dispatch<React.SetStateAction<File[]>>;
  uploadDirectory?: string;
  submit(data: PersonnelPharmacieInterface): void;
}
