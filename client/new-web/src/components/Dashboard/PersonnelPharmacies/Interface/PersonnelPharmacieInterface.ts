import PharmacieInterface from './PharmacieInterface';
export default interface PersonnelPharmacieInterface {
  id: string | null;
  pharmacie?: PharmacieInterface;
  idPharmacie: string;
  nom: string;
  prenom: string;
  civilite: string;
  telBureau: string;
  telMobile: string;
  mail: string;
  estAmbassadrice: boolean;
  sortie: boolean;
  commentaire: string;
}
