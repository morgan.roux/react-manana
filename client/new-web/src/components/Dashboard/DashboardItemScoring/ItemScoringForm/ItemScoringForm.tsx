import React, { FC, useState, ChangeEvent, MouseEvent } from 'react';
import { CustomModal, CustomFormTextField } from '@app/ui-kit';
import useStyles from './style';
import { Box } from '@material-ui/core';

interface ItemScoringFormProps {
  open: boolean;
  saving: boolean;
  libelle: string;
  ordre: number | undefined;
  setOrdre: (ordre: number) => void;
  setOpen: (open: boolean) => void;
  onSubmit?: (event: MouseEvent) => void;
}

const ItemScoringForm: FC<ItemScoringFormProps> = ({
  open,
  saving,
  ordre,
  libelle,
  setOrdre,
  setOpen,
  onSubmit,
}) => {
  const classes = useStyles({});

  return (
    <CustomModal
      title={libelle}
      open={open}
      setOpen={setOpen}
      headerWithBgColor={true}
      fullWidth={true}
      maxWidth="sm"
      closeIcon={true}
      withBtnsActions={true}
      disabledButton={!ordre || saving}
      onClickConfirm={onSubmit}
      centerBtns={true}
    >
      <Box className={classes.formsContent}>
        <CustomFormTextField
          type="number"
          placeholder="Score"
          value={ordre}
          onChange={event => setOrdre(event.target.value ? parseInt(event.target.value, 10) : 0)}
          label="Score"
        />
      </Box>
    </CustomModal>
  );
};

export default ItemScoringForm;
