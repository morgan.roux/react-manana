import React, { FC } from 'react';
import { avatarStyle } from '../../../../commonStyles/avatar';
import classnames from 'classnames';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    selected: {
      border: `2px solid ${theme.palette.secondary.main}`,
    },
    empty: {
      border: '1px solid #E0E0E0',
    },
    margin: {
      margin: 8,
    },
  }),
);

interface AvatarProps {
  src: string;
  alt?: string;
  selected?: boolean;
  className?: any;
  empty?: boolean;
}

const Avatar: FC<AvatarProps> = ({ src, alt, selected, className, empty }) => {
  const avatarStyles = avatarStyle();
  const classes = useStyles({});

  return (
    <div
      className={classnames(
        className,
        classes.margin,
        avatarStyles.avatarContainer,
        selected ? classes.selected : null,
        selected !== undefined && !selected && empty ? classes.empty : null,
      )}
    >
      {empty ? (
        <span>X</span>
      ) : (
        <img src={src} alt={alt ? alt : ''} className={avatarStyles.avatarImage} />
      )}
    </div>
  );
};

export default Avatar;
