import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    avatarFormRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      marginTop: 50,
      '& *': {
        fontFamily: 'Montserrat',
        letterSpacing: 0,
      },
    },
    inputTitle: {
      fontWeight: 'bold',
      fontSize: 18,
      color: theme.palette.secondary.main,
      marginBottom: 15,
    },
    imgFormContainer: {
      width: 800,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      '& > div > div': {
        height: 300,
        border: '1px dashed #DCDCDC',
        alignItems: 'flex-start',
        '& img': {
          width: 225,
          height: 225,
          borderRadius: '50%',
        },
      },
    },
    descFormContainer: {
      width: 800,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'center',
    },
    descInputsContainer: {
      border: '1px solid #DCDCDC',
      borderRadius: 12,
      width: '100%',
      '& > div:nth-child(1)': {
        borderBottom: '1px solid #DCDCDC',
      },
      '& > div:nth-child(2)': {
        '& fieldset > legend > span': {
          display: 'none !important',
        },
      },
    },
    formRow: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      flexDirection: 'row',
      padding: '15px 25px',
      '& > div': {
        maxWidth: 300,
        marginBottom: 0,
        '& input, & .MuiSelect-root': {
          fontWeight: '600',
          fontSize: 16,
          letterSpacing: 0.28,
        },
      },
    },
    formRowTitle: {
      fontSize: 14,
      fontWeight: 'normal',
    },
  }),
);

export default useStyles;
