import React, { FC, useState } from 'react';
import { SourceApprovisionnement } from '@app/ui-kit';
import {
  CREATE_GROUPE,
  UPDATE_GROUPE,
  DELETE_ONE_GROUPE,
} from '../../../federation/tools/commande-orale/groupe/mutation';
import {
  UPDATE_GROUPE as UPDATE_GROUPE_TYPE,
  UPDATE_GROUPEVariables,
} from '../../../federation/tools/commande-orale/groupe/types/UPDATE_GROUPE';
import { GET_GROUPE, GET_GROUPES } from '../../../federation/tools/commande-orale/groupe/query';
import { useLazyQuery, useMutation, useQuery } from '@apollo/client';
import {
  CREATE_GROUPE as CREATE_GROUPE_TYPE,
  CREATE_GROUPEVariables,
} from '../../../federation/tools/commande-orale/groupe/types/CREATE_GROUPE';
import {
  GET_GROUPE as GET_GROUPE_TYPE,
  GET_GROUPEVariables,
} from '../../../federation/tools/commande-orale/groupe/types/GET_GROUPE';
import {
  GET_GROUPES as GET_GROUPES_TYPE,
  GET_GROUPESVariables,
} from '../../../federation/tools/commande-orale/groupe/types/GET_GROUPES';
import {
  DELETE_ONE_GROUPE as DELETE_ONE_GROUPE_TYPE,
  DELETE_ONE_GROUPEVariables,
} from '../../../federation/tools/commande-orale/groupe/types/DELETE_ONE_GROUPE';

import {
  DELETE_ONE_SOURCE_APPRO,
  UPDATE_SOURCE_APPRO,
  CREATE_SOURCE_APPRO,
} from '../../../federation/tools/commande-orale/source-appro/mutation';
import {
  CREATE_SOURCE_APPRO as CREATE_SOURCE_APPRO_TYPE,
  CREATE_SOURCE_APPROVariables,
} from '../../../federation/tools/commande-orale/source-appro/types/CREATE_SOURCE_APPRO';
import {
  UPDATE_SOURCE_APPRO as UPDATE_SOURCE_APPRO_TYPE,
  UPDATE_SOURCE_APPROVariables,
} from '../../../federation/tools/commande-orale/source-appro/types/UPDATE_SOURCE_APPRO';
import {
  DELETE_ONE_SOURCE_APPRO as DELETE_ONE_SOURCE_APPRO_TYPE,
  DELETE_ONE_SOURCE_APPROVariables,
} from '../../../federation/tools/commande-orale/source-appro/types/DELETE_ONE_SOURCE_APPRO';

import { FEDERATION_CLIENT } from '../DemarcheQualite/apolloClientFederation';
import { getPharmacie } from '../../../services/LocalStorage';

const CommandeOrale: FC = () => {
  const [idGroupeCurrent, setIdGroupeCurrent] = useState<string | undefined>(undefined);

  const [createGroupe, creationGroupe] = useMutation<CREATE_GROUPE_TYPE, CREATE_GROUPEVariables>(
    CREATE_GROUPE,
    {
      client: FEDERATION_CLIENT,
      update: (cache, result) => {
        if (result.data?.createOneCOGroupeSourceAppro) {
          const prevList = cache.readQuery<GET_GROUPES_TYPE, GET_GROUPESVariables>({
            query: GET_GROUPES,
            variables: {
              filter: {
                idPharmacie: {
                  eq: getPharmacie().id,
                },
              },
            },
          })?.cOGroupes;

          cache.writeQuery<GET_GROUPES_TYPE, GET_GROUPESVariables>({
            query: GET_GROUPES,
            variables: {
              filter: {
                idPharmacie: {
                  eq: getPharmacie().id,
                },
              },
            },
            data: {
              cOGroupes: {
                ...(prevList || {}),
                nodes:  [...(prevList?.nodes || []), result.data.createOneCOGroupeSourceAppro] as any,
              } as any
              
              
            },
          });
        }
      },
    },
  );

  const [updateGroupe, updatetingGroupe] = useMutation<UPDATE_GROUPE_TYPE, UPDATE_GROUPEVariables>(
    UPDATE_GROUPE,
    {
      client: FEDERATION_CLIENT,
    },
  );

  const loadGroupes = useQuery<GET_GROUPES_TYPE, GET_GROUPESVariables>(GET_GROUPES, {
    client: FEDERATION_CLIENT,
    variables: {
      filter: {
        idPharmacie: {
          eq: getPharmacie().id,
        },
      },
    },
  });

  const [loadGroupe, loadingGroupe] = useLazyQuery<GET_GROUPE_TYPE, GET_GROUPEVariables>(
    GET_GROUPE,
    {
      client: FEDERATION_CLIENT,
    },
  );

  const [deleteOneGroupe, deletetingOneGroupe] = useMutation<
    DELETE_ONE_GROUPE_TYPE,
    DELETE_ONE_GROUPEVariables
  >(DELETE_ONE_GROUPE, {
    client: FEDERATION_CLIENT,
    update: (cache, result) => {
      if (result.data?.deleteOneCOGroupe) {
        const prevList = cache.readQuery<GET_GROUPES_TYPE, GET_GROUPESVariables>({
          query: GET_GROUPES,
          variables: {
            filter: {
              idPharmacie: {
                eq: getPharmacie().id,
              },
            },
          },
        })?.cOGroupes;

        const id = result.data.deleteOneCOGroupe.id;

        cache.writeQuery<GET_GROUPES_TYPE, GET_GROUPESVariables>({
          query: GET_GROUPES,
          variables: {
            filter: {
              idPharmacie: {
                eq: getPharmacie().id,
              },
            },
          },
          data: {
            cOGroupes: { ...prevList,  nodes: (prevList?.nodes || []).filter(item => item.id !== id)} as any,
          },
        });
      }
    },
  });

  const [createSourceAppro, creationSourceAppro] = useMutation<
    CREATE_SOURCE_APPRO_TYPE,
    CREATE_SOURCE_APPROVariables
  >(CREATE_SOURCE_APPRO, {
    client: FEDERATION_CLIENT,
    update: (cache, result) => {
      if (result.data?.createOneCOSourceAppro) {
        const prevList = cache.readQuery<GET_GROUPE_TYPE, GET_GROUPEVariables>({
          query: GET_GROUPE,
          variables: {
            id: result.data.createOneCOSourceAppro.idGroupeSourceAppro,
          },
        })?.cOGroupe;

        cache.writeQuery<GET_GROUPE_TYPE, GET_GROUPEVariables>({
          query: GET_GROUPE,
          variables: {
            id: result.data.createOneCOSourceAppro.idGroupeSourceAppro,
          },
          data: {
            cOGroupe: {
              ...prevList,
              sourceAppros: [
                ...(prevList?.sourceAppros || []),
                result.data.createOneCOSourceAppro,
              ] as any,
            } as any,
          },
        });

        const prevGroupesList = cache.readQuery<GET_GROUPES_TYPE, GET_GROUPESVariables>({
          query: GET_GROUPES,
          variables: {
            filter: {
              idPharmacie: {
                eq: getPharmacie().id,
              },
            },
          },
        })?.cOGroupes;

        cache.writeQuery<GET_GROUPES_TYPE, GET_GROUPESVariables>({
          query: GET_GROUPES,
          variables: {
            filter: {
              idPharmacie: {
                eq: getPharmacie().id,
              },
            },
          },
          data: {
            cOGroupes: { 
              ...prevGroupesList,
              nodes: (prevGroupesList?.nodes || []).map(item =>
              item.id === result.data?.createOneCOSourceAppro.idGroupeSourceAppro
                ? { ...item, nombreSourceAppros: (item as any).nombreSourceAppros + 1 }
                : item,
            )} as any,
          },
        });
      }
    },
  });

  const [updateSourceAppro, updatingSourceAppro] = useMutation<
    UPDATE_SOURCE_APPRO_TYPE,
    UPDATE_SOURCE_APPROVariables
  >(UPDATE_SOURCE_APPRO, {
    client: FEDERATION_CLIENT,
  });

  const [deleteOneSourceAppro, deletingOneSourceAppro] = useMutation<
    DELETE_ONE_SOURCE_APPRO_TYPE,
    DELETE_ONE_SOURCE_APPROVariables
  >(DELETE_ONE_SOURCE_APPRO, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      loadingGroupe.refetch();
    },
  });

  const handleRequestShowDetails = (id: string) => {
    loadGroupe({
      variables: {
        id,
      },
    });
    setIdGroupeCurrent(id);
  };

  const handleRequestSaveGroupeSourceAppro = (data: any) => {
    if (data.id) {
      updateGroupe({
        variables: {
          id: data.id,
          input: {
            nom: data.nom,
            ordre: parseInt(data.ordre, 10),
          },
        },
      });
    } else {
      createGroupe({
        variables: {
          input: {
            nom: data.nom,
            ordre: parseInt(data.ordre, 10),
          },
        },
      });
    }
  };

  const handeleRequestDeleteGroupeSourceAppro = (id: string) => {
    deleteOneGroupe({
      variables: {
        input: {
          id,
        },
      },
    });
  };

  const handleRequestSaveSourceAppro = (data: any) => {
    if (data.id) {
      updateSourceAppro({
        variables: {
          id: data.id,
          input: {
            idGroupeSourceAppro: data.idGroupeSourceAppro,
            nom: data.nom,
            commentaire: data.commentaire,
            tel: data.tel,
            attributs: data.attributs,
            ordre: parseInt(data.ordre, 10),
          },
        },
      });
    } else {
      createSourceAppro({
        variables: {
          input: {
            nom: data.nom,
            idGroupeSourceAppro: data.idGroupeSourceAppro,
            tel: data.tel,
            commentaire: data.commentaire,
            attributs: data.attributs,
            ordre: parseInt(data.ordre, 10),
          },
        },
      });
    }
  };
  const handleRequestDeleteSourceAppro = (id: string) => {
    deleteOneSourceAppro({
      variables: {
        input: {
          id,
        },
      },
    });
  };

  return (
    <SourceApprovisionnement
      currendIdGroupeSourceAppro={idGroupeCurrent}
      sourceAppros={{
        data: loadingGroupe.data?.cOGroupe?.sourceAppros as any,
        loading: loadingGroupe.loading,
        error: loadingGroupe.error,
      }}
      groupeSourceAppros={{
        data: loadGroupes.data?.cOGroupes?.nodes.sort((a: any, b: any) => a.ordre - b.ordre) as any,
        loading: loadGroupes.loading,
        error: loadGroupes.error,
      }}
      onRequestSaveGroupeSourceAppro={handleRequestSaveGroupeSourceAppro}
      onRequestDeleteGroupeSourceAppro={handeleRequestDeleteGroupeSourceAppro}
      onRequestDeleteSourceAppro={handleRequestDeleteSourceAppro}
      onRequestSaveSourceAppro={handleRequestSaveSourceAppro}
      onRequestShowDetails={handleRequestShowDetails}
    />
  );
};

export default CommandeOrale;
