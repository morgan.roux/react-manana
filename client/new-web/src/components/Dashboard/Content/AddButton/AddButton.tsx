import React, { FC } from 'react';
import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import { Theme } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
  root: {
    marginLeft: 10,
    height: 40,
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 14,
    // background: ' linear-gradient(to left, rgba(218,224,33,1) 0%, rgba(139,198,63,1) 70%);',
    '& span': {
      lineHeight: 'initial',
    },
  },
  icon: {
    marginRight: 10,
  },
});

interface AddButtonProps {
  label: string;
  onClick: () => void;
}

const AddButton: FC<AddButtonProps & WithStyles> = ({ onClick, label, classes }: any) => {
  return (
    <Button variant="contained" color="secondary" onClick={onClick} className={classes.root}>
      <AddIcon className={classes.icon} />
      {label}
    </Button>
  );
};

export default withStyles(styles)(AddButton);
