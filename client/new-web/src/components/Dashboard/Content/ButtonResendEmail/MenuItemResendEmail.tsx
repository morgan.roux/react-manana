import React, { FC } from 'react';
import { MenuItem, ListItemIcon, Typography, CircularProgress } from '@material-ui/core';
import { useMutation, useApolloClient } from '@apollo/client';
import {
  RESEND_USER_EMAIL,
  RESEND_USER_EMAILVariables,
} from '../../../../graphql/User/types/RESEND_USER_EMAIL';
import { DO_RESEND_USER_EMAIL } from '../../../../graphql/User/mutation';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import { Send } from '@material-ui/icons';

interface ResendEmailProps {
  email: string;
  login: string;
  disabled: boolean;
}

const MenuItemResendEmail: FC<ResendEmailProps> = ({ email, login, disabled }) => {
  const client = useApolloClient();

  const [doResendMail, { loading }] = useMutation<RESEND_USER_EMAIL, RESEND_USER_EMAILVariables>(
    DO_RESEND_USER_EMAIL,
    {
      onError: error => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: error.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      },
      onCompleted: _ => {
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Mail de définition de mot de passe re-envoyé avec succès`,
          isOpen: true,
        };

        displaySnackBar(client, snackBarData);
      },
    },
  );

  const resendMail = () => {
    if (email && login) {
      doResendMail({ variables: { email, login } });
    }
  };

  return (
    <MenuItem onClick={resendMail} disabled={disabled}>
      <ListItemIcon>{loading ? <CircularProgress size="20px" /> : <Send />}</ListItemIcon>
      <Typography variant="inherit">Ré-envoyer l'email d'activation</Typography>
    </MenuItem>
  );
};

export default MenuItemResendEmail;
