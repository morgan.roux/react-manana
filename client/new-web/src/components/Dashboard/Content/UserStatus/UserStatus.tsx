import React, { FC } from 'react';
import { Button, Menu, MenuItem } from '@material-ui/core';
import { useMutation, useApolloClient } from '@apollo/client';
import Tooltip from '@material-ui/core/Tooltip';
import Fade from '@material-ui/core/Fade';
import {
  UPDATE_USER_STATUS,
  UPDATE_USER_STATUSVariables,
} from '../../../../graphql/User/types/UPDATE_USER_STATUS';
import { DO_UPDATE_USER_STATUS } from '../../../../graphql/User/mutation';
import { LoaderSmall } from '../../Content/Loader';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import useStyles from './styles';
import { AppAuthorization } from '../../../../services/authorization';
import {
  TITULAIRE_PHARMACIE_URL,
  PRESIDENT_REGION_URL,
  PARTENAIRE_LABORATOIRE_URL,
  // PERSONNEL_GROUPEMENT_URL,
  PERSONNEL_PHARMACIE_URL,
  PARTENAIRE_SERVICE_URL,
} from '../../../../Constant/url';
import { getUser } from '../../../../services/LocalStorage';
import { useValueParameterAsNumber } from '../../../../utils/getValueParameter';
import moment from 'moment';
interface UserStatusProps {
  user: any;
  pathname: string;
}

const UserStatus: FC<UserStatusProps> = ({ user, pathname }) => {
  const classes = useStyles({});
  const currentUser = getUser();

  const auth = new AppAuthorization(currentUser);

  const isTitulaire: boolean = pathname.includes(TITULAIRE_PHARMACIE_URL);
  const isPresident: boolean = pathname.includes(PRESIDENT_REGION_URL);
  const isPartenaireLaboratoire: boolean = pathname.includes(PARTENAIRE_LABORATOIRE_URL);
  // const isPersonnelGroupement: boolean = pathname.includes( PERSONNEL_GROUPEMENT_URL)
  const isPersonnelPharamcie: boolean = pathname.includes(PERSONNEL_PHARMACIE_URL);
  const isPartenaireService: boolean = pathname.includes(PARTENAIRE_SERVICE_URL);

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const client = useApolloClient();
  const inactivationAccountParameter = useValueParameterAsNumber('0321') || 60
  let bntText = '-';
  if (user && user.status) {




    switch (user.status) {
      case 'ACTIVATED':
        bntText = 'Activé';
        if (user.lastLoginDate) {
          const now = moment();
          const lastLoginDate = moment(user.lastLoginDate);
          if (now.diff(lastLoginDate, 'days') > inactivationAccountParameter) {
            bntText = 'Bloqué';
          }
        }

        break;
      case 'ACTIVATION_REQUIRED':
        bntText = "Demande d'activation";
        break;
      case 'BLOCKED':
        bntText = 'Bloqué';
        break;
      case 'RESETED':
        bntText = `Mot de passe réinitialisé`;
        break;
      default:
        break;
    }
  }

  const [doUpdateUserStatus, { loading }] = useMutation<
    UPDATE_USER_STATUS,
    UPDATE_USER_STATUSVariables
  >(DO_UPDATE_USER_STATUS, {
    onError: error => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: error.message,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const updateStatus = (id: string, status: string) => {
    handleClose();
    doUpdateUserStatus({
      variables: { id, status },
    });
  };

  const disabledBtn = (): boolean => {
    if (isPartenaireService && !auth.isAuthorizedToViewDetailsPartenaireService()) {
      return true;
    }

    if (isTitulaire && !auth.isAuthorizedToViewDetailsTitulairePharmacie()) {
      return true;
    }

    if (isPresident && !auth.isAuthorizedToViewDetailsPresidentRegion()) {
      return true;
    }

    if (isPartenaireLaboratoire && !auth.isAuthorizedToViewDetailsLaboratoirePartenaire()) {
      return true;
    }

    if (isPersonnelPharamcie && !auth.isAuthorizedToViewDetailsPersonnelPharmacie()) {
      return true;
    }

    /* if (user.status === 'ACTIVATION_REQUIRED' || user.status === 'RESETED') {
       return true;
     }*/
    return false;
  };

  if (loading) {
    return (
      <div className={classes.loaderContainer}>
        <LoaderSmall />
      </div>
    );
  }



  return user && user.status ? (
    <>
      <Tooltip
        TransitionComponent={Fade}
        TransitionProps={{ timeout: 600 }}
        title="Activé ou desactivé le compte utilisateur"
      >
        <Button
          className={classes.btn}
          variant="text"
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
          disabled={disabledBtn()}
        >
          {bntText}
        </Button>
      </Tooltip>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        getContentAnchorEl={null}
      >
        <MenuItem onClick={() => updateStatus(user.id, 'ACTIVATED')}>Activer</MenuItem>
        <MenuItem onClick={() => updateStatus(user.id, 'BLOCKED')}>Bloquer</MenuItem>
      </Menu>
    </>
  ) : (
    <>-</>
  );
};

export default UserStatus;
