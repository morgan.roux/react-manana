import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    whiteBackground: {
      height: '100%',
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      borderRadius: 4,
      borderColor: theme.palette.common.white,
      borderWidth: 1,
      borderStyle: 'solid',
      background: theme.palette.common.white,
    },
    darkBackground: {
      height: 40,
      // width: '50%',
      display: 'flex',
      alignItems: 'center',
      borderRadius: 4,
      borderColor: theme.palette.common.black,
      borderWidth: 1,
      borderStyle: 'solid',
      background: theme.palette.common.white,
    },
    fullWidth: {
      width: '40vw',
    },
    outlined: {
      height: 50,
      // width: '40vw',
      display: 'flex',
      alignItems: 'center',
      borderRadius: 4,
      borderColor: theme.palette.common.black,
      borderWidth: 1,
      borderStyle: 'solid',
      background: theme.palette.common.white,
    },
    input: {
      marginLeft: 8,
      flex: 1,
      width: '100%',
    },
    iconButton: {
      padding: 10,
      color: theme.palette.text.primary,
    },
    divider: {
      width: 1,
      height: 28,
      margin: 4,
    },
    peopleIcon: {
      margin: '0 12px',
    },
  }),
);
