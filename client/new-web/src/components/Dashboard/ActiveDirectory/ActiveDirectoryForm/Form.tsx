import React, { FC, useState } from 'react';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { CustomFormTextField } from '../../../Common/CustomTextField';
import CustomButton from '../../../Common/CustomButton';
import IActiveDirectory from '../../../../Interface/ActiveDirectoryInterface';
import { useForm } from '../CustomHooks';
import CustomSelect from '../../../Common/CustomSelect';
import { LdapType } from '../../../../types/graphql-global-types';

interface FormProps {
  defaultData?: IActiveDirectory;
  action(data: IActiveDirectory): void;
}

const styles = () =>
  createStyles({
    textareaStyle: {
      marginBottom: 25,
      overflowY: 'auto',
    },
    marginBottom: {
      marginBottom: 25,
    },
    marginRight: {
      marginRight: 10,
    },
    titleForm: {
      color: '#1D1D1D',
      marginTop: 0,
    },
    flex: {
      display: 'flex',
      flexDirection: 'row',
    },
    w100: {
      width: '100%',
    },
  });

const Form: FC<RouteComponentProps<any, any, any> & WithStyles & FormProps> = ({
  classes,
  defaultData,
  action,
}) => {
  const list = [
    { name: 'Active Directory Windows', value: LdapType.WINDOWS_LDAP },
    { name: 'OPENLDAP ', value: LdapType.OPENLDAP },
  ];

  const { values, handleChange, handleSubmit } = useForm(action, defaultData);
  const { url, user, password, base, ldapType } = values;

  const canSubmit = (): boolean => {
    return url && user && password && base && ldapType ? true : false;
  };
  return (
    <>
      <h3 className={classes.titleForm}>ACTIVE DIRECTORY</h3>
      <form onSubmit={handleSubmit}>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="url"
            label="Lien LDAP"
            onChange={handleChange}
            value={url}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="user"
            label="Utilisateur administrateur"
            onChange={handleChange}
            value={user}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="password"
            label="Mot de passe"
            onChange={handleChange}
            value={password}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="base"
            label="Base"
            onChange={handleChange}
            value={base}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomSelect
            label="Type de LDAP"
            list={list}
            index="name"
            listId="value"
            name="ldapType"
            value={ldapType}
            onChange={handleChange}
            disabled={false}
          />
        </div>
        <div className={`${classes.marginBottom} ${classes.flex}`}>
          <div className={classes.w100}>
            <CustomButton
              size="large"
              color="primary"
              fullWidth={true}
              disabled={!canSubmit()}
              onClick={handleSubmit}
            >
              ENREGISTRER
            </CustomButton>
          </div>
        </div>
      </form>
    </>
  );
};

export default withStyles(styles, { withTheme: true })(withRouter(Form));
