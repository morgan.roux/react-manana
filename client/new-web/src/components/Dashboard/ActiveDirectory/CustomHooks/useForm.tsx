import { useState, ChangeEvent } from 'react';
import IActiveDirectory from '../../../../Interface/ActiveDirectoryInterface';
import { LdapType } from '../../../../types/graphql-global-types';


const useForm = (
  callback: (data: IActiveDirectory) => void,
  defaultState?: IActiveDirectory,
) => {
  const initialState: IActiveDirectory = defaultState || {
    id: "",
    idGroupement : "",
    url : "",
    user : "",
    password : "",
    base : "",
    ldapType : LdapType.WINDOWS_LDAP,
  };

  const [values, setValues] = useState<IActiveDirectory>(initialState);

  const handleSubmit = (e: any) => {
    if (e) e.preventDefault();
    callback(values);
  };

  const handleChange = (e: ChangeEvent<any>) => {
    const { name, value } = e.target;
    e.persist();
    setValues(prevState => ({ ...prevState, [name]: value }));
  };

  return {
    handleChange,
    handleSubmit,
    values,
  };
};

export default useForm;
