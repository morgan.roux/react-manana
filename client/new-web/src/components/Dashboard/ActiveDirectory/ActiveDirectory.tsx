import React, { FC, useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { Grid, Box } from '@material-ui/core';
import useStyles from './styles';
import Form from './ActiveDirectoryForm';
import { getGroupement } from '../../../services/LocalStorage';
import { useQuery, useMutation, useApolloClient } from '@apollo/client';
import IActiveDirectory from '../../../Interface/ActiveDirectoryInterface';
import {
  ACTIVE_DIRECTORY_GROUPEMENT,
  ACTIVE_DIRECTORY_GROUPEMENTVariables,
} from '../../../graphql/ActiveDirectory/types/ACTIVE_DIRECTORY_GROUPEMENT';
import { GET_ACTIVE_DIRECTORY_GROUPEMENT } from '../../../graphql/ActiveDirectory/query';
import {
  CREATE_ACTIVE_DIRECTORY,
  CREATE_ACTIVE_DIRECTORYVariables,
} from '../../../graphql/ActiveDirectory/types/CREATE_ACTIVE_DIRECTORY';
import {
  DO_CREATE_ACTIVE_DIRECTORY,
  DO_UPDATE_ACTIVE_DIRECTORY,
} from '../../../graphql/ActiveDirectory/mutation';
import SnackVariableInterface from '../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import { LdapType } from '../../../types/graphql-global-types';
import {
  UPDATE_ACTIVE_DIRECTORY,
  UPDATE_ACTIVE_DIRECTORYVariables,
} from '../../../graphql/ActiveDirectory/types/UPDATE_ACTIVE_DIRECTORY';
import { Loader } from '../Content/Loader';

const ActiveDirectory: FC = () => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [myActiveDirectory, setmyActiveDirectory] = useState<IActiveDirectory>();
  const [update, setUpdate] = useState(true);

  const groupement = getGroupement();

  const { data, loading } = useQuery<
    ACTIVE_DIRECTORY_GROUPEMENT,
    ACTIVE_DIRECTORY_GROUPEMENTVariables
  >(GET_ACTIVE_DIRECTORY_GROUPEMENT, {
    fetchPolicy: 'cache-and-network',
    variables: {
      idgroupement: (groupement && groupement.id) || '',
    },
  });

  const [createActiveDirectory] = useMutation<
    CREATE_ACTIVE_DIRECTORY,
    CREATE_ACTIVE_DIRECTORYVariables
  >(DO_CREATE_ACTIVE_DIRECTORY, {
    onCompleted: async data => {
      if (data && data.createActiveDirectoryCredential) {
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Création d'Active directory avec succès`,
          isOpen: true,
        };

        displaySnackBar(client, snackBarData);
        const { createActiveDirectoryCredential } = data;
        setmyActiveDirectory({
          id: createActiveDirectoryCredential.id,
          url: createActiveDirectoryCredential.url ? createActiveDirectoryCredential.url : '',
          user: createActiveDirectoryCredential.user ? createActiveDirectoryCredential.user : '',
          password: createActiveDirectoryCredential.password
            ? createActiveDirectoryCredential.password
            : '',
          base: createActiveDirectoryCredential.base ? createActiveDirectoryCredential.base : '',
          idGroupement: groupement && groupement.id,
          ldapType: createActiveDirectoryCredential.ldapType
            ? createActiveDirectoryCredential.ldapType
            : LdapType.WINDOWS_LDAP,
        });
        window.location.reload();
      }
    },
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur lors de la création de l'Active directory`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const [updateActiveDirectory] = useMutation<
    UPDATE_ACTIVE_DIRECTORY,
    UPDATE_ACTIVE_DIRECTORYVariables
  >(DO_UPDATE_ACTIVE_DIRECTORY, {
    onCompleted: data => {
      if (data && data.updateActiveDirectoryCredential) {
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Modification de l'Active directory avec succès`,
          isOpen: true,
        };

        displaySnackBar(client, snackBarData);
        window.location.reload();
      }
    },
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur lors de la mis à jour de l'active directory`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const submitUpdate = (incommingDatas: IActiveDirectory) => {
    if (update) updateActiveDirectory({ variables: { ...incommingDatas } });
    else {
      createActiveDirectory({
        variables: { ...incommingDatas, idGroupement: groupement && groupement.id },
      });
      setUpdate(true);
    }
  };

  useEffect(() => {
    if (data && data.activedirectoryGroupement) {
      setUpdate(true);
      const myActiveDirectory = data.activedirectoryGroupement;
      setmyActiveDirectory({
        id: myActiveDirectory.id,
        url: myActiveDirectory.url ? myActiveDirectory.url : '',
        user: myActiveDirectory.user ? myActiveDirectory.user : '',
        password: myActiveDirectory.password ? myActiveDirectory.password : '',
        base: myActiveDirectory.base ? myActiveDirectory.base : '',
        idGroupement: groupement && groupement.id,
        ldapType: myActiveDirectory.ldapType ? myActiveDirectory.ldapType : LdapType.WINDOWS_LDAP,
      });
    } else {
      setUpdate(false);
    }
  }, [data]);

  if (loading) return <Loader />;

  return (
    <Box display="flex" justifyContent="center" className={classes.contentBottom}>
      {myActiveDirectory && (
        <Grid item={true} xs={6} className={classes.spacingLeft}>
          <Form defaultData={myActiveDirectory} action={submitUpdate} />
        </Grid>
      )}
      {!myActiveDirectory && (
        <Grid item={true} xs={6} className={classes.spacingLeft}>
          <Form defaultData={undefined} action={submitUpdate} />
        </Grid>
      )}
    </Box>
  );
};
export default withRouter(ActiveDirectory);
