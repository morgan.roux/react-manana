import { Add } from '@material-ui/icons';
import { Box } from '@material-ui/core';
import React, { Fragment, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { ContentContext, ContentStateInterface } from '../../../AppContext';
import { COLORS_URL } from '../../../Constant/url';
import Backdrop from '../../Common/Backdrop';
import { ConfirmDeleteDialog } from '../../Common/ConfirmDialog';
import CustomButton from '../../Common/CustomButton';
import CustomContent from '../../Common/newCustomContent';
import SearchInput from '../../Common/newCustomContent/SearchInput';
import SubToolbar from '../../Common/newCustomContent/SubToolbar';
import ColorForm from './ColorForm';
import useColorForm from './ColorForm/useColorForm';
import { useColorColumns } from './Hooks/useColorColumns';
import { useCreateUpdateColor } from './Hooks/useCreateUpdateColor';
import useStyles from './styles';

interface ColorManagementProps {
  listResult: any;
}

const ColorManagement: React.FC<ColorManagementProps & RouteComponentProps> = ({
  listResult,
  history: { push },
  location: { pathname },
}) => {
  const classes = useStyles({});
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  const {
    columns,
    deleteRow,
    onClickConfirmDelete,
    loading: deleteLoading,
    selected,
    setSelected,
  } = useColorColumns(setOpenDeleteDialog);

  const { handleChange, handleChangeColor, values, setValues } = useColorForm();

  const { code, libelle } = values;

  const ADD_URL = `/db/${COLORS_URL}/create`;

  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const [operationNameState, setOperationName] = useState<any>();
  const [variablesState, setVariables] = useState<any>();

  const { createUpdateColor, loading: mutationLoading, data: mutationData } = useCreateUpdateColor(
    { input: values },
    operationNameState,
    variablesState,
  );

  const isOnList = pathname === `/db/${COLORS_URL}`;
  const isOnCreate = pathname === `/db/${COLORS_URL}/create`;
  const isOnEdit: boolean = pathname.startsWith(`/db/${COLORS_URL}/edit`);

  const loadingMsg = `${
    isOnCreate ? 'Création' : isOnEdit ? 'Modification' : 'Chargement'
  } en cours...`;

  const goToList = () => push(`/db/${COLORS_URL}`);

  const subToolbarTitle = isOnCreate
    ? `Ajout de couleur todo`
    : isOnEdit
    ? `Modification de couleur todo`
    : 'Liste des couleurs de todo';

  const onClickDefaultBtn = () => {
    if (!isOnList) {
      goToList();
      return;
    }
    setOpenDeleteDialog(true);
  };

  const goToAddColor = () => push(ADD_URL);

  const onClickSecondaryBtn = () => {
    if (!isOnList) {
      createUpdateColor();
      return;
    }
    goToAddColor();
  };

  const disableSaveButton = (): boolean => {
    if (!code || !libelle) return true;
    return false;
  };

  // Hooks
  useEffect(() => {
    if (operationName && !operationNameState) {
      setOperationName(operationName);
      setVariables(variables);
    }
  }, [operationName]);

  // Return to list after mutation
  useEffect(() => {
    if (mutationData && mutationData.createUpdateCouleur) {
      goToList();
    }
  }, [mutationData]);

  const SubToolbarChildren = () => {
    return (
      <Box className={classes.childrenRoot}>
        {selected.length > 0 && isOnList && (
          <CustomButton color="default" onClick={onClickDefaultBtn}>
            {!isOnList ? 'Annuler' : 'Supprimer la sélection'}
          </CustomButton>
        )}
        <CustomButton
          color="secondary"
          startIcon={!isOnList ? null : <Add />}
          onClick={onClickSecondaryBtn}
          disabled={!isOnList ? disableSaveButton() : false}
        >
          {isOnCreate ? 'Ajouter' : isOnEdit ? 'Modifier' : 'Ajouter une couleur'}
        </CustomButton>
      </Box>
    );
  };

  const colorList = (
    <CustomContent
      selected={selected}
      setSelected={setSelected}
      {...{ listResult, columns }}
      isSelectable={true}
    />
  );

  const DeleteDialogContent = () => {
    if (deleteRow) {
      return (
        <span>
          Êtes-vous sur de vouloir supprimer
          <span style={{ fontWeight: 'bold' }}> {deleteRow.nomGroupe || ''} </span>?
        </span>
      );
    }
    return <span>Êtes-vous sur de vouloir supprimer ces couleurs ?</span>;
  };

  return (
    <Box className={classes.container}>
      {(mutationLoading || deleteLoading) && (
        <Backdrop value={deleteLoading ? 'Suppression en cours...' : loadingMsg} />
      )}
      <SubToolbar
        title={subToolbarTitle}
        dark={!isOnList}
        withBackBtn={!isOnList}
        onClickBack={goToList}
      >
        <SubToolbarChildren />
      </SubToolbar>
      <Box>
        {isOnList && (
          <Fragment>
            <Box className={classes.searchInputBox}>
              <SearchInput searchPlaceholder="Rechercher une couleur" />
            </Box>
          </Fragment>
        )}

        {!isOnList ? (
          <ColorForm
            title={subToolbarTitle}
            values={values}
            handleChangeInput={handleChange}
            handleChangeColor={handleChangeColor}
            isOnCreate={isOnCreate}
            isOnEdit={isOnEdit}
            setValues={setValues}
          />
        ) : (
          colorList
        )}
      </Box>
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
    </Box>
  );
};

export default withRouter(ColorManagement);
