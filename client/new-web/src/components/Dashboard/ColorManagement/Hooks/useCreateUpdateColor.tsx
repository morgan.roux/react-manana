import { useApolloClient, useMutation } from '@apollo/client';
import { useState } from 'react';
import { DO_CREATE_UPDATE_COLOR } from '../../../../graphql/Color';
import {
  CREATE_UPDATE_COLOR,
  CREATE_UPDATE_COLORVariables,
} from '../../../../graphql/Color/types/CREATE_UPDATE_COLOR';
import { displaySnackBar } from '../../../../utils/snackBarUtils';

export const useCreateUpdateColor = (
  values: CREATE_UPDATE_COLORVariables,
  operationName: any,
  variables: any,
) => {
  const client = useApolloClient();
  const [mutationSuccess, setMutationSuccess] = useState<boolean>(false);

  const [createUpdateColor, { loading, data }] = useMutation<
    CREATE_UPDATE_COLOR,
    CREATE_UPDATE_COLORVariables
  >(DO_CREATE_UPDATE_COLOR, {
    variables: { ...values },
    update: (cache, { data }) => {
      if (data && data.createUpdateCouleur && values && !values.input.id) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables,
          });
          if (req && req.search && req.search.data) {
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    total: req.search.total + 1,
                    data: [...req.search.data, data.createUpdateCouleur],
                  },
                },
              },
              variables,
            });
          }
        }
      }
    },
    onCompleted: data => {
      if (data && data.createUpdateCouleur) {
        setMutationSuccess(true);
        // (client as any).writeData({
        //   data: { checkedsPharmacie: null },
        // });
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  return { createUpdateColor, mutationSuccess, loading, data };
};
