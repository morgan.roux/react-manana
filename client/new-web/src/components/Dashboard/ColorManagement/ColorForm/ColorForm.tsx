import { useApolloClient, useLazyQuery } from '@apollo/client';
import React, { ChangeEvent, Dispatch, FC, SetStateAction, useEffect } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { COLORS_URL } from '../../../../Constant/url';
import { GET_COLOR } from '../../../../graphql/Color';
import { COLOR, COLORVariables } from '../../../../graphql/Color/types/COLOR';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import { ColorFormInterface } from './useColorForm';
import useStyles from './styles';
import useCommonStyles from '../../commonStyles';
import classnames from 'classnames';
import { Typography } from '@material-ui/core';
import { CustomFormTextField } from '../../../Common/CustomTextField';
import { ChromePicker } from 'react-color';
import Backdrop from '../../../Common/Backdrop';

export interface ColorFormProps {
  title: string;
  isOnCreate: boolean;
  isOnEdit: boolean;
  values: ColorFormInterface;
  setValues: Dispatch<SetStateAction<ColorFormInterface>>;
  handleChangeInput: (e: ChangeEvent<any>) => void;
  handleChangeColor: (color: any) => void;
}

const ColorForm: FC<ColorFormProps & RouteComponentProps> = ({
  values,
  setValues,
  handleChangeInput,
  handleChangeColor,
  isOnEdit,
  history: { push },
  match: { params },
  location: { pathname },
}) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();
  const client = useApolloClient();
  const { code, libelle } = values;

  const { colorId } = params as any;
  const EDIT_URL = `/db/${COLORS_URL}/edit/${colorId}`;

  const [getColor, { data, loading }] = useLazyQuery<COLOR, COLORVariables>(GET_COLOR, {
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        });
      });
    },
  });

  /**
   * Get groupe client on edit
   */
  useEffect(() => {
    if (pathname === EDIT_URL && colorId && isOnEdit) {
      getColor({ variables: { id: colorId } });
    }
  }, [pathname, colorId, isOnEdit, EDIT_URL]);

  /**
   * Set values on edit
   */
  useEffect(() => {
    if (data && data.couleur) {
      const c = data.couleur;
      setValues(prevState => ({ ...prevState, id: c.id, code: c.code, libelle: c.libelle }));
    }
  }, [data]);

  return (
    <div className={classnames(commonClasses.formRoot, classes.colorFormRoot)}>
      {loading && <Backdrop />}
      <div className={commonClasses.formContainer}>
        <Typography className={commonClasses.inputTitle}>Définition de la couleur</Typography>
        <div className={commonClasses.inputsContainer}>
          <div className={commonClasses.formRow}>
            <Typography className={commonClasses.inputLabel}>
              Nom <span>*</span>
            </Typography>
            <CustomFormTextField
              name="libelle"
              value={libelle}
              onChange={handleChangeInput}
              placeholder="Nom de la couleur..."
            />
          </div>
          <div className={classnames(commonClasses.formRow, classes.colorPickerContainer)}>
            <ChromePicker
              className={classes.colorPicker}
              color={code}
              onChangeComplete={handleChangeColor}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(ColorForm);
