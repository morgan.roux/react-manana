import React, { FC } from 'react';
import { useStyles } from './styles';
import AppBar from '../AppBar';
import Sidebar from '../Sidebar';
import Footer from '../Footer';

import { Grid } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';

const Dashboard: FC = () => {
  const classes = useStyles({});

  const handleChangePage = (event: unknown, newPage: number) => {
    // setPage(newPage);
    console.log('Ok');
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    // setRowsPerPage(parseInt(event.target.value, 10));
    // setPage(0);
    console.log('Ok');
  };

  return (
    <Grid container>
      <Grid item sm={12}>
        <AppBar />
      </Grid>
      <Grid item sm={12} className={classes.root}>
        <Grid item sm={2}>
          <Sidebar />
        </Grid>
        <Grid item sm={10}>
          <Grid item sm={12} className={classes.alignRight}>
            <Button variant="contained">Ajouter un utilisateur</Button>
          </Grid>
          <Grid item sm={12} className={classes.search}>
            <TextField
              label="Rechercher un utilisateur"
              type="text"
              variant="outlined"
              margin="none"
              fullWidth
            />
            <Button size="large" variant="contained">
              Rechercher
            </Button>
          </Grid>
          <Grid item sm={12} className={classes.table}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Email</TableCell>
                  <TableCell>Nom</TableCell>
                  <TableCell>Rôles</TableCell>
                  <TableCell>Actions possibles</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell>thibaut@gmail.com</TableCell>
                  <TableCell>Thibaut Rennée</TableCell>
                  <TableCell>Titulaire pharmacie</TableCell>
                  <TableCell>
                    <Button size="small" variant="contained">
                      Bloquer
                    </Button>
                    <Button size="small" variant="contained">
                      Modifier
                    </Button>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>thibaut@gmail.com</TableCell>
                  <TableCell>Thibaut Rennée</TableCell>
                  <TableCell>Titulaire pharmacie</TableCell>
                  <TableCell>
                    <Button size="small" variant="contained">
                      Bloquer
                    </Button>
                    <Button size="small" variant="contained">
                      Modifier
                    </Button>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25]}
              component="div"
              count={100}
              rowsPerPage={10}
              page={9}
              backIconButtonProps={{
                'aria-label': 'previous page',
              }}
              nextIconButtonProps={{
                'aria-label': 'next page',
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </Grid>
        </Grid>
      </Grid>
      <Grid item sm={12}>
        <Footer />
      </Grid>
    </Grid>
  );
};

export default Dashboard;
