import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import { ModeleOperationMarketingPage } from '@app/ui-kit';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import {
  CREATE_MODELE_OPERATION_MARKETING,
  DELETE_MODELE_OPERATION_MARKETING,
  UPDATE_MODELE_OPERATION_MARKETING,
} from '../../../federation/partenaire-service/ModeleOperationMarketing/mutation';
import { GET_MODELE_OPERATION_MARKETINGS } from '../../../federation/partenaire-service/ModeleOperationMarketing/query';
import {
  CREATE_MODELE_OPERATION_MARKETING as CREATE_MODELE_OPERATION_MARKETING_TYPE,
  CREATE_MODELE_OPERATION_MARKETINGVariables,
} from '../../../federation/partenaire-service/ModeleOperationMarketing/types/CREATE_MODELE_OPERATION_MARKETING';
import {
  DELETE_MODELE_OPERATION_MARKETING as DELETE_MODELE_OPERATION_MARKETING_TYPE,
  DELETE_MODELE_OPERATION_MARKETINGVariables,
} from '../../../federation/partenaire-service/ModeleOperationMarketing/types/DELETE_MODELE_OPERATION_MARKETING';
import {
  GET_MODELE_OPERATION_MARKETINGS as GET_MODELE_OPERATION_MARKETINGS_ALL,
  GET_MODELE_OPERATION_MARKETINGSVariables,
} from '../../../federation/partenaire-service/ModeleOperationMarketing/types/GET_MODELE_OPERATION_MARKETINGS';
import {
  UPDATE_MODELE_OPERATION_MARKETING as UPDATE_MODELE_OPERATION_MARKETING_TYPE,
  UPDATE_MODELE_OPERATION_MARKETINGVariables,
} from '../../../federation/partenaire-service/ModeleOperationMarketing/types/UPDATE_MODELE_OPERATION_MARKETING';
import {
  CREATE_PLAN_MARKETING_TYPE,
  DELETE_ONE_PLAN_MARKETING_TYPE,
  UPDATE_ONE_PLAN_MARKETIN_TYPE,
} from '../../../federation/partenaire-service/planMarketing/mutation';
import { GET_LIST_PLAN_MARKETING_TYPES } from '../../../federation/partenaire-service/planMarketing/query';
import {
  CREATE_PLAN_MARKETING_TYPE as CREATE_PLAN_MARKETING_TYPES,
  CREATE_PLAN_MARKETING_TYPEVariables,
} from '../../../federation/partenaire-service/planMarketing/types/CREATE_PLAN_MARKETING_TYPE';
import {
  DELETE_ONE_PLAN_MARKETING_TYPE as DELETE_ONE_PLAN_MARKETING_TYPES,
  DELETE_ONE_PLAN_MARKETING_TYPEVariables,
} from '../../../federation/partenaire-service/planMarketing/types/DELETE_ONE_PLAN_MARKETING_TYPE';
// import {commonFieldComponent} from './../../Common/CommonFieldsForm';
import {
  GET_LIST_PLAN_MARKETING_TYPES as GET_LIST_PLAN_MARKETING_TYPES_TYPE,
  GET_LIST_PLAN_MARKETING_TYPESVariables,
} from '../../../federation/partenaire-service/planMarketing/types/GET_LIST_PLAN_MARKETING_TYPES';
import {
  UPDATE_ONE_PLAN_MARKETIN_TYPE as UPDATE_ONE_PLAN_MARKETIN_TYPES,
  UPDATE_ONE_PLAN_MARKETIN_TYPEVariables,
} from '../../../federation/partenaire-service/planMarketing/types/UPDATE_ONE_PLAN_MARKETIN_TYPE';
import { getPharmacie } from '../../../services/LocalStorage';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import CommonFieldsForm from '../../Common/CommonFieldsForm';
import { FEDERATION_CLIENT } from '../DemarcheQualite/apolloClientFederation';
import noContentActionImageSrc from '../../../assets/img/image_default.png';

interface ModeleOperationMarketingProps {
  match: {
    params: {
      idModele: string | undefined;
    };
  };
}
const ModeleOperationMarketing: FC<ModeleOperationMarketingProps & RouteComponentProps> = ({
  match,
}) => {
  const [taskToEdit, setTaskToEdit] = useState<any>(undefined);
  const [saving, setSaving] = useState<boolean>(false);
  // const [typeToEdit, setTypeToEdit] = useState<any>(undefined);
  const [modeleSelected, setModeleSelected] = useState<any>();
  const [userParticipants, setUserParticipants] = useState<any[]>([]);
  const [tache, setTache] = useState<any>();
  const [importance, setImportance] = useState<any>();
  const [morePlanMarketingType, setMorePlanMarketingType] = useState<number>(5);
  const client = useApolloClient();
  const pharmacie = getPharmacie();

  // To-Do
  const [loadOperationMarketingActions, loadingOperationMarketingActions] = useLazyQuery<
    GET_MODELE_OPERATION_MARKETINGS_ALL,
    GET_MODELE_OPERATION_MARKETINGSVariables
  >(GET_MODELE_OPERATION_MARKETINGS, { client: FEDERATION_CLIENT });

  const [createOperationMarketingAction, creatingOperationMarketingAction] = useMutation<
    CREATE_MODELE_OPERATION_MARKETING_TYPE,
    CREATE_MODELE_OPERATION_MARKETINGVariables
  >(CREATE_MODELE_OPERATION_MARKETING, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La To-Do a été créee avec success',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingOperationMarketingActions.refetch();
      setSaving(false);
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs sont survenues pendant la création de la To-Do',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [updateOperationMarketingAction, updatingOperationMarketingAction] = useMutation<
    UPDATE_MODELE_OPERATION_MARKETING_TYPE,
    UPDATE_MODELE_OPERATION_MARKETINGVariables
  >(UPDATE_MODELE_OPERATION_MARKETING, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La To-Do a été modifiéé avec success',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingOperationMarketingActions.refetch();
      setSaving(false);
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs sont survenues pendant la modification de la to-Do',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [deleteOperationmarketingAction, deletingOperationMarketingAction] = useMutation<
    DELETE_MODELE_OPERATION_MARKETING_TYPE,
    DELETE_MODELE_OPERATION_MARKETINGVariables
  >(DELETE_MODELE_OPERATION_MARKETING, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La To-Do a été supprimé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingOperationMarketingActions.refetch();
      setSaving(false);
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression de la To-Do',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  // Modele
  const planMarketingTypes = useQuery<
    GET_LIST_PLAN_MARKETING_TYPES_TYPE,
    GET_LIST_PLAN_MARKETING_TYPESVariables
  >(GET_LIST_PLAN_MARKETING_TYPES, {
    variables: {
      filter: {
        or: [
          {
            idPharmacie: {
              eq: pharmacie.id,
            },
          },
          {
            idPharmacie: {
              is: null,
            },
          },
        ],
      },
      paging: {
        offset: 0,
        limit: 100,
      },
    },
    client: FEDERATION_CLIENT,
  });

  const [createModele, creatingModele] = useMutation<
    CREATE_PLAN_MARKETING_TYPES,
    CREATE_PLAN_MARKETING_TYPEVariables
  >(CREATE_PLAN_MARKETING_TYPE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La modèle a été créee avec success',
        type: 'SUCCESS',
        isOpen: true,
      });
      planMarketingTypes.refetch();
      setSaving(false);
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs sont survenues pendant la création de la modèle',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [updateModele, updatingModele] = useMutation<
    UPDATE_ONE_PLAN_MARKETIN_TYPES,
    UPDATE_ONE_PLAN_MARKETIN_TYPEVariables
  >(UPDATE_ONE_PLAN_MARKETIN_TYPE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La modèle a été modifiéé avec success',
        type: 'SUCCESS',
        isOpen: true,
      });
      planMarketingTypes.refetch();
      setSaving(false);
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs sont survenues pendant la modification de la modèle',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [deleteModele, deletingModele] = useMutation<
    DELETE_ONE_PLAN_MARKETING_TYPES,
    DELETE_ONE_PLAN_MARKETING_TYPEVariables
  >(DELETE_ONE_PLAN_MARKETING_TYPE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La modèle a été supprimé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      setSaving(false);
      planMarketingTypes.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression de la modèle',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  useEffect(() => {
    if (match.params.idModele) {
      handleClickPlanMarketingType(match.params.idModele);
    }
  }, [planMarketingTypes.data?.pRTPlanMarketingTypes]);

  const handeleRequestSave = (data: any) => {
    if (!data.id) {
      setSaving(true);
      createOperationMarketingAction({
        variables: {
          input: {
            jourLancement: data.jourLancement,
            idParticipants: data.idParticipants,
            description: data.description,
            idPlanMarketingType: modeleSelected.id,
            idFonction: tache?.idFonction,
            idTache: tache?.idTache,
            idImportance: data.idImportance,
          },
        },
      });
      return;
    }
    if (data.id) {
      setSaving(true);
      updateOperationMarketingAction({
        variables: {
          id: data.id,
          input: {
            jourLancement: data.jourLancement,
            idParticipants: data.idParticipants,
            description: data.description,
            idPlanMarketingType: modeleSelected.id,
            idFonction: data.idfonction,
            idTache: data.idTache,
            idImportance: data.idImportance,
          },
        },
      });
    }
  };

  const handleRequestSaveModele = (data: any) => {
    if (!data.id) {
      setSaving(true);
      createModele({
        variables: {
          input: {
            pRTPlanMarketingType: {
              libelle: data.libelle,
              code: data.code,
              couleur: data.couleur,
            },
          },
        },
      });
      return;
    }
    if (data.id) {
      setSaving(true);
      updateModele({
        variables: {
          input: {
            id: data.id,
            update: {
              libelle: data.libelle,
              code: data.code,
              couleur: data.couleur,
            },
          },
        },
      });
    }
  };

  const handleClickPlanMarketingType = (id: string) => {
    window.history.pushState(null, '', `/#/db/modele-operation-marketing/${id}`);
    const planMarketingType = (planMarketingTypes.data?.pRTPlanMarketingTypes.nodes || []).find(
      pRTPlanMarketingType => pRTPlanMarketingType.id === id,
    );
    if (planMarketingType) setModeleSelected(planMarketingType);
    loadOperationMarketingActions({
      variables: {
        filter: {
          idPharmacie: {
            eq: pharmacie.id,
          },
          idPlanMarketingType: {
            eq: id,
          },
        },
      },
    });
  };

  const handleRequestDeleteModele = (data: any) => {
    setSaving(true);
    deleteModele({
      variables: {
        input: {
          id: data.id,
        },
      },
    });
  };

  const handleRequestDeleteTask = (data: any) => {
    setSaving(true);
    deleteOperationmarketingAction({
      variables: {
        id: data.id,
      },
    });
  };

  const handleRequestGetModele = (data: any) => {
    setModeleSelected(data);
  };

  const handleRequestTaskToEdit = (task: any) => {
    console.log('++++++++++++++++++++++++ ', task);
    setUserParticipants(task.participants || []);
    setImportance(task.importance);
    setTache({ id: task.idTask, idFonction: task.idFonction, idTask: task.idTask });
  };

  const handleFetchMoreType = (count: number) => {
    console.log('-------------------COUNT-----------------: ', count);
    setMorePlanMarketingType(count);
    // planMarketingTypes.fetchMore({
    //   variables: {
    //     paging: {
    //       offset: planMarketingTypes.data?.pRTPlanMarketingTypes.nodes.length || 0 + count,
    //     },
    //   },
    //   updateQuery: (prev, { fetchMoreResult }) => {
    //     if (
    //       prev &&
    //       prev.pRTPlanMarketingTypes &&
    //       prev.pRTPlanMarketingTypes.nodes &&
    //       fetchMoreResult &&
    //       fetchMoreResult.pRTPlanMarketingTypes.nodes
    //     ) {
    //       const { nodes: currentData } = prev.pRTPlanMarketingTypes;
    //       return {
    //         ...prev,
    //         nodes: [...currentData, ...fetchMoreResult.pRTPlanMarketingTypes.nodes],
    //       };
    //     }
    //     return prev;
    //   },
    // });
  };

  //   const tasks = {
  // filter: operationMarketingActions.filter,
  // paging: operationMarketingActions.paging,
  //   }

  return (
    <>
      <ModeleOperationMarketingPage
        noContentActionImageSrc={noContentActionImageSrc}
        onRequestSave={handeleRequestSave}
        taskToEdit={taskToEdit}
        tasks={{
          data: loadingOperationMarketingActions.data?.pRTPlanMarketingTypeActions.nodes as any,
          loading: loadingOperationMarketingActions.loading,
        }}
        saving={saving}
        commonFieldsIds={{
          idImportance: importance?.id,
          idTache: tache?.idTache,
          idFonction: tache?.idFonction,
          idParticipants: userParticipants.map(user => user.id) as any,
        }}
        commonFieldComponent={
          <CommonFieldsForm
            selectedUsers={userParticipants}
            urgence={null}
            urgenceProps={{
              useCode: false,
            }}
            usersModalProps={{
              withNotAssigned: false,
              searchPlaceholder: 'Rechercher...',
              withAssignTeam: false,
              singleSelect: false,
              title: 'Choix de collaborateurs',
            }}
            selectUsersFieldLabel="Collaborateurs"
            projet={tache}
            onChangeUsersSelection={setUserParticipants}
            onChangeProjet={projet => {
              setTache(projet);
            }}
            importance={importance}
            onChangeImportance={importance => setImportance(importance)}
          />
        }
        types={{
          data: planMarketingTypes.data?.pRTPlanMarketingTypes.nodes as any,
          loading: planMarketingTypes.loading,
        }}
        typeToEdit={modeleSelected}
        onRequestSaveModele={handleRequestSaveModele}
        onRequestDeleteModele={handleRequestDeleteModele}
        onRequestGetType={handleRequestGetModele}
        onRequestGetTypeId={handleClickPlanMarketingType}
        onRequestDeleteTask={handleRequestDeleteTask}
        onRequestTaskToEdit={handleRequestTaskToEdit}
        onFetchMoreType={handleFetchMoreType}
      />
    </>
  );
};

export default withRouter(ModeleOperationMarketing);
