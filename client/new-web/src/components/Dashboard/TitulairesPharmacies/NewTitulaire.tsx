import { useApolloClient } from '@apollo/client';
import { Box } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import React, { useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { ContentContext, ContentStateInterface } from '../../../AppContext';
import mutationSuccessImg from '../../../assets/img/mutation-success.png';
import { TITULAIRE_PHARMACIE_URL } from '../../../Constant/url';
import { GET_CHECKEDS_TITULAIRE_PHARMACIE } from '../../../graphql/Titulaire/local';
import CustomButton from '../../Common/CustomButton';
import CustomContent from '../../Common/newCustomContent';
import SearchInput from '../../Common/newCustomContent/SearchInput';
import SubToolbar from '../../Common/newCustomContent/SubToolbar';
import { PharmacieTable } from '../../Common/newWithSearch/ComponentInitializer';
import NoItemContentImage from '../../Common/NoItemContentImage';
import Stepper, { Step } from '../../Common/Stepper/Stepper';
import { useStyles } from '../styles';
import { Step1, Step2 } from './stepper';
import useStylesTitulaire from './styles';
import {
  useCheckedsPharmacie,
  useCheckedsTitulaire,
  useTitulaireColumns,
  useTitulaireForm,
} from './utils';
import { useCreateUpdateTitulaire } from './utils/useCreateUpdateTitulaire';
import { useGetTitulaire } from './utils/useGetTitulaire';

interface INewTitulaireProps {
  listResult: any;
}

const NewTitulaireTable: React.FC<INewTitulaireProps & RouteComponentProps> = props => {
  const CREATE_TITULAIRE_LINK = `/db/${TITULAIRE_PHARMACIE_URL}/create`;
  const EDIT_TITULAIRE_LINK = `/db/${TITULAIRE_PHARMACIE_URL}/edit`;

  const {
    listResult,
    history,
    location: { pathname },
    match: { params },
  } = props;

  const client = useApolloClient();

  const classes = useStyles({});
  const titClasses = useStylesTitulaire();

  const [columns, ConfirmDeleteDialog, handleDelete] = useTitulaireColumns(history);

  const checkedsTitulaire = useCheckedsTitulaire();

  const { values, handleChange, setValues } = useTitulaireForm();

  const checkedsPharmacie = useCheckedsPharmacie();

  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const [operationNameState, setOperationName] = useState<any>();
  const [variablesState, setVariables] = useState<any>();

  useEffect(() => {
    if (operationName && !operationNameState) {
      setOperationName(operationName);
      setVariables(variables);
    }
  }, [operationName]);

  const [createUpdateTitulaire, mutationSuccess, loading] = useCreateUpdateTitulaire(
    values,
    operationNameState,
    variablesState,
  );

  const [getTitulaire] = useGetTitulaire(setValues);

  const isOnCreate = pathname === CREATE_TITULAIRE_LINK ? true : false;

  const isOnEdit = pathname.startsWith(EDIT_TITULAIRE_LINK) ? true : false;

  const [disableNextButton, setDisableNextButton] = useState<boolean>(false);

  const [activeStep, setActiveStep] = useState<number | undefined>(0);

  const mutationSuccessTitle = `${isOnCreate ? 'Ajout' : 'Modification'} d'un titulaire réussi`;

  const mutationSuccessSubTitle = `Le titulaire que vous venez ${isOnCreate ? 'de créer' : 'de modifier'
    } est maintenant\ndans la liste.`;

  useEffect(() => {
    return () => {
        // TODO: Migration
      /*
      (client as any).writeData({
        data: {
          checkedsTitulairePharmacie: null,
          skipAndTake: {
            skip: 0,
            take: 12,
            __typename: 'SkipAndTake',
          },
        },
      });*/
    };
  }, []);

  useEffect(() => {
    if (checkedsPharmacie) {
      const idPharmacies = checkedsPharmacie.map(checkedPharmacie => checkedPharmacie.id);
      setValues(prevState => ({ ...prevState, idPharmacies }));
    }
  }, [checkedsPharmacie]);

  useEffect(() => {
    if (activeStep === 0) setDisableNextButton(values.nom === '' || values.prenom === '');
    if (activeStep === 1) setDisableNextButton(false);
    if (activeStep === 2) {
      setDisableNextButton(
        !checkedsPharmacie || (checkedsPharmacie && checkedsPharmacie.length === 0),
      );
    }
  }, [activeStep, values, checkedsPharmacie]);

  useEffect(() => {
    const { titulaireId } = params as any;
    if (titulaireId) {
      getTitulaire({ variables: { id: titulaireId } });
    }
  }, [params]);

  const gotToAddTitulaire = () => history.push(CREATE_TITULAIRE_LINK);

  const goToHome = () => history.push(`/db/${TITULAIRE_PHARMACIE_URL}`);

  const children = (
    <div className={classes.childrenRoot}>
      {checkedsTitulaire && checkedsTitulaire.length > 0 && (
        <CustomButton color="default" onClick={() => handleDelete()}>
          Supprimer la sélection
        </CustomButton>
      )}
      <CustomButton color="secondary" startIcon={<Add />} onClick={gotToAddTitulaire}>
        Ajouter un titulaire
      </CustomButton>
    </div>
  );

  const listTitulaire = (
    <div className={titClasses.listTitulaireRoot}>
      <div className={classes.searchInputBox}>
        <SearchInput searchPlaceholder="Rechercher un titulaire" />
      </div>
      <CustomContent
        checkedItemsQuery={{
          name: 'checkedsTitulairePharmacie',
          type: 'titulaire',
          query: GET_CHECKEDS_TITULAIRE_PHARMACIE,
        }}
        {...{ listResult, columns }}
        isSelectable={true}
      />
    </div>
  );

  const finalStep = {
    buttonLabel: isOnEdit ? 'Modifier' : 'Ajouter',
    loading,
    action: () => {
      createUpdateTitulaire();
    },
  };

  const actionSuccess = (
    <div className={classes.mutationSuccessContainer}>
      <NoItemContentImage
        src={mutationSuccessImg}
        title={mutationSuccessTitle}
        subtitle={mutationSuccessSubTitle}
      >
        <CustomButton color="default" onClick={goToHome}>
          Retour à la liste
        </CustomButton>
      </NoItemContentImage>
    </div>
  );

  return (
    <div className={classes.container}>
      {!(isOnCreate || isOnEdit) && (
        <SubToolbar {...{ children }} title="Liste des titulaires de pharmacie" />
      )}
      {mutationSuccess ? (
        actionSuccess
      ) : isOnCreate || isOnEdit ? (
        <Stepper
          title={isOnCreate ? 'Ajout nouveau titulaire' : "Modification d'un titulaire"}
          steps={[
            {
              title: 'Informations personnelles',
              content: <Step1 {...{ handleChange, values }} />,
            },
            {
              title: 'Contact',
              content: <Step2 {...{ handleChange, values }} />,
            },
            {
              title: 'Choix des pharmacies',
              content: (
                <Box width="100%">
                  <PharmacieTable />
                </Box>
              ),
            },
          ]}
          backToHome={goToHome}
          disableNextBtn={disableNextButton}
          fullWidth={false}
          finalStep={finalStep}
          activeStepFromProps={activeStep}
          setActiveStepFromProps={setActiveStep}
        />
      ) : (
        listTitulaire
      )}
      {ConfirmDeleteDialog}
    </div>
  );
};

export default withRouter(NewTitulaireTable);
