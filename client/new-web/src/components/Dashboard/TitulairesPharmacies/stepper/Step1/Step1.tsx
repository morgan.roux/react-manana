import React from 'react';
import { CIVILITE_LIST } from '../../../../../Constant/user';
import { createUpdateTitulaireVariables } from '../../../../../graphql/Titulaire/types/createUpdateTitulaire';
import CustomSelect from '../../../../Common/CustomSelect';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import FormContainer from '../../../../Common/FormContainer';
import InputRow from '../../../../Common/InputRow';
import useStyles from './styles';

interface ITitulaireFormProps {
  handleChange: (e: React.ChangeEvent<any>) => void;
  values: createUpdateTitulaireVariables;
}

const Step1: React.FC<ITitulaireFormProps> = props => {
  const { handleChange, values } = props;

  const classes = useStyles({});

  const { civilite, nom, prenom } = values;

  const infoPersoComponent = (
    <>
      <InputRow title="Civilité">
        <CustomSelect
          label=""
          list={CIVILITE_LIST}
          listId="id"
          index="value"
          name="civilite"
          value={civilite}
          onChange={handleChange}
          shrink={false}
          placeholder="Choisissez une civilité"
          withPlaceholder={true}
        />
      </InputRow>
      <InputRow title="Prénom" required={true}>
        <CustomFormTextField
          name="prenom"
          value={prenom}
          onChange={handleChange}
          placeholder="Prénom de la personne"
        />
      </InputRow>
      <InputRow title="Nom" required={true}>
        <CustomFormTextField
          name="nom"
          value={nom}
          onChange={handleChange}
          placeholder="Nom de la personne"
        />
      </InputRow>
    </>
  );

  return (
    <div className={classes.personnelGroupementFormRoot}>
      <FormContainer title="Informations personnelles">{infoPersoComponent}</FormContainer>
    </div>
  );
};

export default Step1;
