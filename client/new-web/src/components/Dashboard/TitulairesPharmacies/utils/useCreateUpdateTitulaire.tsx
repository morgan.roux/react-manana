import { useMutation, useApolloClient } from '@apollo/client';
import {
  createUpdateTitulaire,
  createUpdateTitulaireVariables,
} from '../../../../graphql/Titulaire/types/createUpdateTitulaire';
import { DO_CREATE_UPDATE_TITULAIRE } from '../../../../graphql/Titulaire/mutation';
import { useState, useContext } from 'react';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import { ContentStateInterface, ContentContext } from '../../../../AppContext';

export const useCreateUpdateTitulaire = (
  values: createUpdateTitulaireVariables,
  operationName: any,
  variables: any,
): [() => void, boolean, boolean] => {
  const client = useApolloClient();

  const [mutationSuccess, setMutationSuccess] = useState<boolean>(false);
  const [createUpdateTitulaire, { loading }] = useMutation<
    createUpdateTitulaire,
    createUpdateTitulaireVariables
  >(DO_CREATE_UPDATE_TITULAIRE, {
    variables: { ...values },
    onCompleted: data => {
      if (data && data.createUpdateTitulaire) {
        setMutationSuccess(true);
          // TODO: Migration

        /*(client as any).writeData({
          data: { checkedsPharmacie: null },
        });*/
      }
    },
    update: (cache, { data }) => {
      if (data && data.createUpdateTitulaire && values && !values.id) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables: variables,
          });
          if (req && req.search && req.search.data) {
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    total: req.search.total + 1,
                    data: [...req.search.data, data.createUpdateTitulaire],
                  },
                },
              },

              variables: variables,
            });
          }
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
      displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
    },
  });

  return [createUpdateTitulaire, mutationSuccess, loading];
};
