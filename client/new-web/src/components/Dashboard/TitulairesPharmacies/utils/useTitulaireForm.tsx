import { useState, ChangeEvent } from 'react';
import { createUpdateTitulaireVariables } from '../../../../graphql/Titulaire/types/createUpdateTitulaire';

export const initialState: createUpdateTitulaireVariables = {
  id: null,
  civilite: null,
  nom: '',
  prenom: null,
  idPharmacies: [''],
  contact: {
    adresse1: null,
    adresse2: null,
    cp: null,
    faxPerso: null,
    faxProf: null,
    mailPerso: null,
    mailProf: null,
    telPerso: null,
    telProf: null,
    ville: null,
    compteSkypePerso: null,
    compteSkypeProf: null,
    pays: null,
    sitePerso: null,
    siteProf: null,
    telMobPerso: null,
    telMobProf: null,
    urlFacebookPerso: null,
    urlFacebookProf: null,
    urlLinkedinPerso: null,
    urlLinkedinProf: null,
    urlTwitterPerso: null,
    urlTwitterProf: null,
    whatsappMobPerso: null,
    whatsAppMobProf: null,
  },
};

const useTitulaireForm = (defaultState?: createUpdateTitulaireVariables) => {
  const initValues: createUpdateTitulaireVariables = defaultState || initialState;
  const [values, setValues] = useState<createUpdateTitulaireVariables>(initValues);
  const currentContact = values && (values.contact as any);
  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      //const isInContact = currentContact && currentContact[name];
      setValues(prevState => ({ ...prevState, [name]: value }));
      if (name != 'nom' && name != 'prenom' && name != 'civilite') {
        setValues(prevState => ({
          ...prevState,
          contact: { ...prevState.contact, [name]: value },
        }));
      }

      // if (isInContact) {
      //   setValues(prevState => ({
      //     ...prevState,
      //     contact: { ...prevState.contact, [name]: value },
      //   }));
      // } else {
      //   setValues(prevState => ({ ...prevState, [name]: value }));
      // }
    }
  };

  return {
    handleChange,
    values,
    setValues,
  };
};

export default useTitulaireForm;
