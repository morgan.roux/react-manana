import { useQuery } from '@apollo/client';
import { GET_CHECKEDS_TITULAIRE_PHARMACIE } from '../../../../graphql/Titulaire/local';

export const useCheckedsTitulaire = () => {
  const checkedTitulairePharmacie = useQuery(GET_CHECKEDS_TITULAIRE_PHARMACIE);
  return (
    checkedTitulairePharmacie &&
    checkedTitulairePharmacie.data &&
    checkedTitulairePharmacie.data.checkedsTitulairePharmacie
  );
};
