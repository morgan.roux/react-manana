import React, { useState } from 'react';
import { useCheckedsTitulaire, useDeleteTitulaire } from '.';
// import { useStyles } from '../../styles';
import { TITULAIRE_PHARMACIE_URL } from '../../../../Constant/url';
import { ConfirmDeleteDialog } from '../../../Common/ConfirmDialog';
import CustomAvatar from '../../../Common/CustomAvatar';
import { Column } from '../../Content/Interface';
import TableActionColumn from '../../TableActionColumn';
import moment from 'moment';

export const useTitulaireColumns = (history: any): [Column[], JSX.Element, (row?: any) => void] => {
  // const classes = useStyles({});

  const deletePersonnel = useDeleteTitulaire();

  const checkedsTitulaire = useCheckedsTitulaire();

  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  const [deleteRow, setDeleteRow] = useState<any>();

  const handleClickDelete = (row?: any) => {
    if (row) {
      setDeleteRow(row);
    }
    setOpenDeleteDialog(true);
  };

  const onClickConfirmDelete = () => {
    if (deleteRow) {
      const { id } = deleteRow;
      deletePersonnel({ variables: { ids: [id] } });
      setDeleteRow(null);
    } else if (checkedsTitulaire) {
      const ids = checkedsTitulaire.map(titulaire => titulaire.id);
      deletePersonnel({ variables: { ids } });
    }
    setOpenDeleteDialog(false);
  };

  const DeleteDialogContent = () => {
    if (deleteRow) {
      return (
        <span>
          Êtes-vous sur de vouloir supprimer
          <span style={{ fontWeight: 'bold' }}>
            {' '}
            {deleteRow.civilite || ''} {deleteRow.prenom || ''} {deleteRow.nom || ''}{' '}
          </span>
          ?
        </span>
      );
    }
    return <span>Êtes-vous sur de vouloir supprimer ces personnels ?</span>;
  };

  const confirmDeleteDialog = (
    <ConfirmDeleteDialog
      open={openDeleteDialog}
      setOpen={setOpenDeleteDialog}
      content={<DeleteDialogContent />}
      onClickConfirm={onClickConfirmDelete}
    />
  );

  const columns: Column[] = [
    {
      name: '',
      label: 'Photo',
      renderer: (value: any) => {
        const valueUser =
          value.users &&
            value.pharmacies &&
            value.pharmacies.length === 1 &&
            value.users.length === 1
            ? value.users[0]
            : null;
        return (
          <CustomAvatar
            name={value && value.fullName}
            url={
              valueUser &&
              valueUser.userPhoto &&
              valueUser.userPhoto.fichier &&
              valueUser.userPhoto.fichier.publicUrl
            }
          />
        );
      },
    },
    {
      name: 'civilite',
      label: 'Civilité',
    },
    {
      name: 'prenom',
      label: 'Prénom',
    },
    {
      name: 'nom',
      label: 'Nom',
    },
    {
      name: 'contact.adresse1',
      label: 'Adresse 1',
      renderer: (value: any) => {
        return value && value.contact && value.contact.adresse1 ? value.contact.adresse1 : '-';
      },
    },
    {
      name: 'contact.ville',
      label: 'Ville',
      renderer: (value: any) => {
        return value && value.contact && value.contact.ville ? value.contact.ville : '-';
      },
    },
    {
      name: 'contact.cp',
      label: 'CP',
      renderer: (value: any) => {
        return value && value.contact && value.contact.cp ? value.contact.cp : '-';
      },
    },
    {
      name: 'contact.telProf',
      label: 'Téléphone 1',
      renderer: (value: any) => {
        return value && value.contact && value.contact.telProf ? value.contact.telProf : '-';
      },
    },
    {
      name: 'contact.telMobProf',
      label: 'Téléphone 2',
      renderer: (value: any) => {
        return value && value.contact && value.contact.telMobProf ? value.contact.telMobProf : '-';
      },
    },
    {
      name: 'contact.mailProf',
      label: 'Email',
      renderer: (value: any) => {
        return (value && value.contact && value.contact.mailProf) || '-';
      },
    },
    {
      name: 'president.region',
      label: 'Président de region',
      renderer: (value: any) => {
        return (value && value.estPresidentRegion) || '-';
      },
    },
    {
      name: 'contact.dateCreation',
      label: 'Date de création',
      renderer: (value: any) => {
        if (value && value.dateCreation) return moment(value.dateCreation).format('L');
        return '-';
      },
    },
    {
      name: 'contact.dateModification',
      label: 'Date de modification',
      renderer: (value: any) => {
        if (value && value.dateModification) return moment(value.dateModification).format('L');
        return '-';
      },
    },

    {
      name: '',
      label: '',
      sortable: false,
      renderer: (value: any) => {
        return (
          <TableActionColumn
            row={value}
            baseUrl={TITULAIRE_PHARMACIE_URL}
            setOpenDeleteDialog={() => { }}
            withViewDetails={true}
            handleClickDelete={handleClickDelete}
          />
        );
      },
    },
  ];

  return [columns, confirmDeleteDialog, handleClickDelete];
};
