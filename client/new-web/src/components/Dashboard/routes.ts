import React from 'react';
import { Route, RouteProps, Switch } from 'react-router-dom';
import withUser from '../../components/Common/withUser';
import {
    ADMINISTRATEUR_GROUPEMENT,
    PRESIDENT_REGION,
    SUPER_ADMINISTRATEUR,
    TITULAIRE_PHARMACIE,
} from '../../Constant/roles';
import {
    AIDE_SUPPORT_URL,
    COLORS_URL,
    GROUPES_CLIENTS_URL,
    GROUPE_AMIS,
    PARTAGE_IDEE_URL,
    PARTENAIRE_LABORATOIRE_URL,
    LABORATOIRE_URL,
    PARTENAIRE_SERVICE_URL,
    PERSONNEL_GROUPEMENT_URL,
    PERSONNEL_PHARMACIE_URL,
    PHARMACIE_URL,
    PRESIDENT_REGION_URL,
    TITULAIRE_PHARMACIE_URL,
    TRAITEMENT_AUTOMATIQUE_URL,
    PARAMETRE_FRIGO_URL,
    DASHBOARD_ITEM_SCORING_URL
} from '../../Constant/url';
import { AppAuthorization } from '../../services/authorization';
import {
    GroupeClientDashboard,
    LaboratoiresDashboard,
    PartenaireServiceDashboard,
    PersonnelGroupementDashboard,
    PersonnelPharmacieDashboard,
    PharmacieDashboard,
    PresidentRegionDashboard,
    TitulaireDashboard,
} from '../Common/newWithSearch/ComponentInitializer';
import { ThemePage } from '../Main/Content/Theme';
import { ProtectedRoute } from '../Router';
import AideEtSupport from './AideEtSupport';
import Aide from './AideEtSupport/Aide';
import Tickets from './AideEtSupport/Tickets';
import Avatar from './Avatar';
import BasisDataManagement from './BasisDataManagement';
import { ColorManagementDashboard } from './ColorManagement';
import ChecklistManagement, { ChecklistForm } from './DemarcheQualite/ChecklistsManagement';
import DemarcheQualite from './DemarcheQualite/DemarcheQualite';
import MatriceTache from './DemarcheQualite/MatriceTache/MatriceTache';
import MatriceTacheDetails from './DemarcheQualite/MatriceTache/MatriceTacheFonctionDetails/MatriceTacheFonctionDetails';
import { ExigenceForm } from './DemarcheQualite/Theme/SousTheme/ExigenceForm';
import DetailsTheme from './DemarcheQualite/Theme/Theme';
import EspaceDocumentaire from './EspaceDocumentaire';
import EspacePublicitaire from './EspacePublicitaire';
import { FormUser } from './FormUser';
import GroupDetail from './GroupDetail';
import GroupDetailDashboard from './GroupDetail/GroupDetailDashboard';
import { Groupement } from './Groupement';
import GroupeFriends from './GroupFriends/GroupeFriends';
import TraitementAutomatique from './TraitementAutomatique/TraitementAutomatique';
import ParametreFrigo from './ParametreFrigo/ParametreFrigo';
import { IdeeBonnePratiqueDashboard } from './IdeeBonnePratique/';
import IdeeBonnePratiqueDetails from './IdeeBonnePratique/IdeeBonnePratiqueDetails';
import OptionsGroupement from './OptionsGroupement';
import ParametreSso from './ParametreSso';
import { PartenairesServices } from './PartenairesServices';
import FichePrestataire from './PartenairesServices/FichePrestataire';
import FichePrestataireContact from './PartenairesServices/FichePrestataire/Contact';
import { CreatePersonnelPharmacie } from './PersonnelPharmacies/Create';
import FichePharmacie from './Pharmacies/FichePharmacie';
import Roles from './Roles';
import FicheTitulaire from './TitulairesPharmacies/FicheTitulaire';
import UserSettings from './UserSettings';
import Utilitaire from './Utilitaire/Utilitaire';
import DashboardItemScoring from './DashboardItemScoring';
import Rgpd from './Rgpd/Rgpd';
import Categorie from './Categorie/Categorie';
import GestionEspaceDocumentaire from './EspaceDocumentaire/GestionEspaceDocumentaire';
import OptionsPharmacie from './OptionsPharmacie';
import { CreateUpdateLabo } from './Laboratoire/CreateUpdate';
import FicheLaboratoire from './Laboratoire/Fiche';
import { CommandeOrale } from './CommandeOrale';
import { SuiviAppels } from './AideEtSupport/SuiviAppels';
import { CarnetContact } from './AideEtSupport/CarnetContact';

import { ProtectedRouteProps } from "../Router/ProtectedRoute";


export const getRoutes = ({ user }: any): (ProtectedRouteProps | RouteProps)[] => {
    const auth = new AppAuthorization(user)
    const USER_ROUTES = [
        {
            url: PRESIDENT_REGION_URL,
            addUserAuth: auth.isAuthorizedToAddUserPresidentRegion(),
            editUserAuth: auth.isAuthorizedToEditUserPresidentRegion(),
        },
        {
            url: PERSONNEL_GROUPEMENT_URL,
            addUserAuth: true,
            editUserAuth: true,
        },
        {
            url: PARTENAIRE_LABORATOIRE_URL,
            addUserAuth: auth.isAuthorizedToAddUserLaboratoirePartenaire(),
            editUserAuth: auth.isAuthorizedToEditUserLaboratoirePartenaire(),
        },
        {
            url: PERSONNEL_PHARMACIE_URL,
            addUserAuth: auth.isAuthorizedToEditUserPersonnelPharmacie(),
            editUserAuth: auth.isAuthorizedToEditUserPersonnelPharmacie(),
        },
        {
            url: PARTENAIRE_SERVICE_URL,
            addUserAuth: auth.isAuthorizedToAddUserPartenaireService(),
            editUserAuth: auth.isAuthorizedToEditUserPartenaireService(),
        },
        {
            url: TITULAIRE_PHARMACIE_URL,
            addUserAuth: auth.isAuthorizedToEditUserTitulairePharmacie(),
            editUserAuth: auth.isAuthorizedToEditUserTitulairePharmacie(),
        },
    ];


    return [
        {
            path: ['/db/options-groupement'],
            exact: true,
            strict: true,
            component: withUser(OptionsGroupement),
            isAuthorized: auth.isAuthorizedToViewGroupementOptions()
        },
        {
            path: ['/db/options-groupement/edit'],
            exact: true,
            strict: true,
            component: withUser(OptionsGroupement),
            isAuthorized: auth.isAuthorizedToEditGroupementOptions()
        },
        {
            path: ['/db/user-settings', '/db/user-settings/edit'],
            exact: true,
            strict: true,
            component: withUser(UserSettings)
        }
        ,
        {
            path: ['/db/options-pharmacie', '/db/options-pharmacie/edit'],
            exact: true,
            strict: true,
            component: withUser(OptionsPharmacie),
        },
        {
            path: "/db/theme",
            exact: true,
            strict: true,
            component: withUser(ThemePage),
            isAuthorized: auth.isAuthorizedToChangeTheme()
        },
        {
            path: ['/db/espace-publicitaire'],
            exact: true,
            strict: true,
            component: withUser(EspacePublicitaire),
            isAuthorized: auth.isAuthorizedToViewPubList()
        },
        {
            path: ['/db/espace-publicitaire/create'],
            exact: true,
            strict: true,
            component: withUser(EspacePublicitaire),
            isAuthorized: auth.isAuthorizedToAddPub()
        }

        ,
        {
            path: ['/db/espace-publicitaire/edit/:pubId'],
            exact: true,
            strict: true,
            component: withUser(EspacePublicitaire),
            isAuthorized: auth.isAuthorizedToEditPub()
        },
        {
            path: ['/db/avatar', '/db/avatar/create', '/db/avatar/edit/:avatarId'],
            exact: true,
            strict: true,
            component: withUser(Avatar),
        },
        {
            path: [`/db/${COLORS_URL}`, `/db/${COLORS_URL}/create`, `/db/${COLORS_URL}/edit/:colorId`],
            exact: true,
            strict: true,
            component: withUser(ColorManagementDashboard),
        },
        {
            path: [
                `/db/${PARTAGE_IDEE_URL}`,
                `/db/${PARTAGE_IDEE_URL}/create`,
                `/db/${PARTAGE_IDEE_URL}/edit/:ideeId`,
            ],
            exact: true,
            strict: true,
            component: withUser(IdeeBonnePratiqueDashboard)
        }
        ,
        {
            path: [`/db/${PARTAGE_IDEE_URL}/fiche/:ideeId`],
            exact: true,
            strict: true,
            component: withUser(IdeeBonnePratiqueDetails),
        },

        /* Personnel groupement */

        {
            path: [
                `/db/${PERSONNEL_GROUPEMENT_URL}`,
                `/db/${PERSONNEL_GROUPEMENT_URL}/affectation`,
                `/db/${PERSONNEL_GROUPEMENT_URL}/list`,
                `/db/${PERSONNEL_GROUPEMENT_URL}/create`,
                `/db/${PERSONNEL_GROUPEMENT_URL}/edit/:personnelId`
            ],
            exact: true,
            component: withUser(PersonnelGroupementDashboard, {
                permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT],
            }),
        },
        {
            path: ['/db/demarche_qualite/exigence/:idTheme/:idSousTheme/:id'],
            exact: true,
            component: ExigenceForm,
        },
        {
            path: ["/db/categorie"],
            exact: true,
            component: Categorie
        },
        {
            path: ['/db/demarche_qualite', '/db/demarche_qualite/:id'],
            exact: true,
            component: DemarcheQualite,
            isAuthorized: auth.isAuthorizedToViewGroupementOptions()
        },
        {
            path: ["/db/demarche_qualite/:idTheme"],
            exact: true,
            component: DetailsTheme,
        },
        {
            path: ["/db/matrice_taches", "/db/matrice_taches/:id"],
            exact: true,
            component: MatriceTache
        },
        {
            path: ["/db/commande-orale"],
            exact: true,
            component: CommandeOrale,
        },
        {
            path: ["/db/groupement"],
            exact: true,
            component: withUser(Groupement, {
                permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT],
            }),
        },
        {
            path: [
                `/db/${PRESIDENT_REGION_URL}`,
                `/db/${PRESIDENT_REGION_URL}/list`,
                `/db/${PRESIDENT_REGION_URL}/affectation`,
            ],
            exact: true,
            component: withUser(PresidentRegionDashboard, {
                permitRoles: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT],
            }),
            isAuthorized: auth.isAuthorizedToViewPresidentRegionList()
        }
        /* Titiulaire pharmacie list */
    ]
}