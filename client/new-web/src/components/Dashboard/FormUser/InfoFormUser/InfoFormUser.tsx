import React from 'react';
import { Theme } from '@material-ui/core/styles';
import { CSSProperties } from '@material-ui/core/styles/withStyles';
import { withStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import IconCall from '@material-ui/icons/Call';
import IconEmail from '@material-ui/icons/Email';
import IconMap from '../../../../assets/img/ic_place_24px.svg';
import IconLocationCity from '@material-ui/icons/LocationCity';
import { IFragementPharmacie, IUsers } from '../TitulairePharmacies/TitulairePharmacies';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
  content: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 60,
    '@media (max-width: 599px)': {
      flexDirection: 'column',
      alignItems: 'start',
    },
  },
  icon: {
    color: '#e34168',
  },
  center: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 0,
  },
  column: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'start',
    paddingLeft: 0,
  },
  name: {
    fontSize: '0.875rem',
  },
  textMail: {
    fontSize: '0.875rem',
    color: '#989898',
  },
  contentIcon: {
    marginRight: 12,
    width: 24,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentLeftList: {
    overflowY: 'auto',
    height: 210,
    display: 'flex',
    alignItems: 'center',
  },
  listPadding: {
    width: 300,
    '@media (max-width: 356px)': {
      width: 250,
    },
  },
});

interface IProps {
  pharmacie: IFragementPharmacie;
  classes: {
    content: 'string';
    center: 'string';
    contentIcon: 'string';
    icon: 'string';
  };
}

const InfoFormUser = (props: any) => {
  const { classes, pharmacie } = props;

  return (
    <Box className={classes.content}>
      <List dense={true}>
        <ListItem className={classes.center}>
          <Box className={classes.contentIcon}>
            <IconLocationCity className={classes.icon} />
          </Box>

          <ListItemText>{pharmacie.nom || '-'}</ListItemText>
        </ListItem>
        <ListItem className={classes.center}>
          <Box className={classes.contentIcon}>
            <IconEmail className={classes.icon} />
          </Box>
          <ListItemText>{pharmacie.mail || '-'}</ListItemText>
        </ListItem>
        <ListItem className={classes.center}>
          <Box className={classes.contentIcon}>
            <img className={classes.icon} src={IconMap} alt="" />
          </Box>
          <ListItemText>{pharmacie.adresse || '-'}</ListItemText>
        </ListItem>
        <ListItem className={classes.center}>
          <Box className={classes.contentIcon}>
            <IconCall className={classes.icon} />
          </Box>
          <ListItemText>{pharmacie.tel || '-'}</ListItemText>
        </ListItem>
      </List>
      <Box className={classes.contentLeftList}>
        <List dense={true} className={classes.listPadding}>
          {pharmacie.users.length > 0 &&
            pharmacie.users[0].id &&
            pharmacie.users.map((user: IUsers) => (
              <ListItem className={classes.column}>
                <Typography className={classes.name}>{user.userName || '-'}</Typography>
                <Typography className={classes.textMail}>{user.email}</Typography>
              </ListItem>
            ))}
        </List>
      </Box>
    </Box>
  );
};

export default withStyles(styles, { withTheme: true })(InfoFormUser);
