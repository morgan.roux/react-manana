import React, { FC } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Menu, { MenuProps } from '@material-ui/core/Menu';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import HtmlTooltip from './HtmlToolTip';
import { ResponsableTypeInfo } from './../../../../../../federation/demarche-qualite/matrice-tache/types/ResponsableTypeInfo';
import {
  GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches_collaborateurResponsables,
  GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches_collaborateurResponsables_responsables,
} from './../../../../../../federation/demarche-qualite/matrice-tache/types/GET_FONCTIONS_WITH_RESPONSABLE_INFOS';
import MatriceResponsableType from './MatriceResponsableType';

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props: MenuProps) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.black,
      },
    },
  },
}))(MenuItem);

interface MatriceResponsableTypeMenuProps {
  idUser?: string;
  collaborateurResponsables: GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches_collaborateurResponsables[];
  selectedIdType?: string;
  anchorEl?: HTMLElement;
  onClickItem: (
    type: ResponsableTypeInfo | undefined,
    event: React.MouseEvent<HTMLElement>,
  ) => void;
  onClose: () => void;
}

const MatriceResponsableTypeMenu: FC<MatriceResponsableTypeMenuProps> = ({
  idUser,
  collaborateurResponsables,
  selectedIdType,
  anchorEl,
  onClickItem,
  onClose,
}) => {
  const maxResponsablesReached = (
    collaborateurResponsable: GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches_collaborateurResponsables,
  ): {
    reached: boolean;
    collaborateurResponsables: GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches_collaborateurResponsables_responsables[];
  } => {
    if (collaborateurResponsable.type.max !== null) {
      const responsables = idUser
        ? collaborateurResponsable.responsables.filter(({ id }) => id !== idUser)
        : collaborateurResponsable.responsables;

      return {
        reached: responsables.length >= collaborateurResponsable.type.max,
        collaborateurResponsables: responsables,
      };
    }

    return { reached: false, collaborateurResponsables: [] };
  };

  return (
    <div>
      <StyledMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={onClose}
      >
        <StyledMenuItem
          key="default"
          selected={!Boolean(selectedIdType)}
          onClick={(event) => onClickItem(undefined, event)}
        >
          <ListItemIcon>
            <MatriceResponsableType
              collaborateurResponsables={collaborateurResponsables}
              value={undefined}
              onClick={(event) => onClickItem(undefined, event)}
            />
          </ListItemIcon>
          <ListItemText primary="Aucun" />
        </StyledMenuItem>

        {collaborateurResponsables.map((collaborateurResponsable) => {
          const responsablesReached = maxResponsablesReached(collaborateurResponsable);
          const menuItem = (
            <StyledMenuItem
              key={collaborateurResponsable.type.id}
              selected={Boolean(
                selectedIdType && selectedIdType === collaborateurResponsable.type.id,
              )}
              onClick={(event) => !responsablesReached.reached && onClickItem(collaborateurResponsable.type, event)}
              style={responsablesReached.reached? {color:'darkgray'}: undefined }
            >
              <ListItemIcon>
                <MatriceResponsableType
                  collaborateurResponsables={collaborateurResponsables}
                  value={collaborateurResponsable.type.id}
                  onClick={(event) => onClickItem(collaborateurResponsable.type, event)}
                />
              </ListItemIcon>
              <ListItemText primary={collaborateurResponsable.type.libelle} />
            </StyledMenuItem>
          );

          return responsablesReached.reached ? (
            <HtmlTooltip
              title={
                <React.Fragment>
                  <Typography color="secondary">
                    {responsablesReached.collaborateurResponsables.length === 1
                      ? 'Responsable'
                      : 'Responsables'}
                  </Typography>
                  <List key={`${collaborateurResponsable.idTache}-${idUser}`}>
                    {responsablesReached.collaborateurResponsables.map(({ id, fullName }) => (
                      <ListItem key={id}>
                        <ListItemText>{fullName}</ListItemText>
                      </ListItem>
                    ))}
                  </List>
                </React.Fragment>
              }
            >
              {menuItem}
            </HtmlTooltip>
          ) : (
            menuItem
          );
        })}
      </StyledMenu>
    </div>
  );
};

export default MatriceResponsableTypeMenu;
