import React, { FC } from 'react';
import Parameters from '../../Common/Parameters';

const OptionsPharmacie: FC = () => {
  return (
    <Parameters
      withSearchInput={true}
      baseUrl="/db/options-pharmacie"
      where="inDashboardPharmacie"
    />
  );
};

export default OptionsPharmacie;
