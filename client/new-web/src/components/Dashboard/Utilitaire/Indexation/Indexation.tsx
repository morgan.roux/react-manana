import React, { FC, ReactNode, useState, ChangeEvent } from 'react';
import { RouteComponentProps, withRouter, Link } from 'react-router-dom';
import {
  Box,
  Typography,
  MenuItem,
  ListItemIcon,
  RadioGroup,
  FormControlLabel,
  Radio,
  CircularProgress,
  Button,
} from '@material-ui/core';
import useStyles from './styles';
import { SvgIconProps } from '@material-ui/core/SvgIcon';
import CustomSelect from '../../../Common/CustomSelect';
import { useLazyQuery, useApolloClient } from '@apollo/client';
import { DO_REFRESH_INDEXES, DO_REINDEX } from '../../../../graphql/RefreshIndexes/query';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import { REFRESH_INDEXES } from '../../../../graphql/RefreshIndexes/types/REFRESH_INDEXES';
import Backdrop from '../../../Common/Backdrop';
import { REINDEX, REINDEXVariables } from '../../../../graphql/RefreshIndexes/types/REINDEX';
import { FEDERATION_CLIENT } from '../../DemarcheQualite/apolloClientFederation';
import { GET_GET_DOCUMENT_FROM_EMAIL as GET_GET_DOCUMENT_FROM_EMAIL_TYPE } from '../../../../federation/ged/document/types/GET_GET_DOCUMENT_FROM_EMAIL';
import { UPDATE_ITEM_SCORING_FOR_ALL_USERS as UPDATE_ITEM_SCORING_FOR_ALL_USERS_TYPE } from '../../../../federation/tools/itemScoring/types/UPDATE_ITEM_SCORING_FOR_ALL_USERS';
import { GET_GET_DOCUMENT_FROM_EMAIL } from '../../../../federation/ged/document/query';
import { UPDATE_ITEM_SCORING_FOR_ALL_USERS } from '../../../../federation/tools/itemScoring/query';


const Indexation: FC = () => {
  const classes = useStyles({});
  const [open, setOpen] = React.useState(true);
  const client = useApolloClient();

  const [checkedRadio, setCheckedRadio] = useState<string>('all');
  const [routingKey, setRoutinkey] = useState<string>('');
  const [loadingMessage, setLoadingMessage] = useState<string>(
    'Indexation en cours, veuillez patienter..."',
  );

  const [getFromEmail, { loading }] = useLazyQuery<GET_GET_DOCUMENT_FROM_EMAIL_TYPE>(
    GET_GET_DOCUMENT_FROM_EMAIL,
    { client: FEDERATION_CLIENT },
  );

  const [updateItemScoring, updatingItemScoring] = useLazyQuery<UPDATE_ITEM_SCORING_FOR_ALL_USERS_TYPE>(
    UPDATE_ITEM_SCORING_FOR_ALL_USERS,
    { client: FEDERATION_CLIENT },
  );



  const [refreshIndexes, refreshIndexesResult] = useLazyQuery<REFRESH_INDEXES>(DO_REFRESH_INDEXES, {
    variables: {
      withMapping: false,
    },
    errorPolicy: 'all',
    onCompleted: data => {
      const snackBarData: SnackVariableInterface = {
        type: 'SUCCESS',
        message: 'Réindexation terminée',
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  const [reindex, reindexResult] = useLazyQuery<REINDEX, REINDEXVariables>(DO_REINDEX, {
    onCompleted: data => {
      const snackBarData: SnackVariableInterface = {
        type: 'SUCCESS',
        message: 'Réindexation terminée',
        isOpen: true,
      };
      const routingKeyFromVariables = reindexResult.variables.routingKey;
      if (routingKey === routingKeyFromVariables) displaySnackBar(client, snackBarData);
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  // if (refreshIndexesResult.loading || reindexResult.loading) {
  //   const snackBarData: SnackVariableInterface = {
  //     type: 'INFO',
  //     message: 'Refresh index en cours...',
  //     isOpen: true,
  //   };
  //   displaySnackBar(client, snackBarData);
  // }

  const handleChange = (event: any) => {
    event.preventDefault();
    event.stopPropagation();
    setCheckedRadio(event.target.value);
  };

  const onChange = (event: any) => {
    event.preventDefault();
    event.stopPropagation();
    setRoutinkey(event.target.value);
  };

  const handleSubmit = (event: ChangeEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();

    if (checkedRadio === 'all') {
      setLoadingMessage(
        'Indexation en cours, cela prendra environ 5 à 15 minutes, veuillez patienter...',
      );
      refreshIndexes();
    } else if (routingKey !== '') {
      reindex({ variables: { routingKey } });
    }
  };

  const list = [
    { name: 'Laboratoire', value: 'laboratoires' },
    { name: 'Partenaire', value: 'partenaires' },
    { name: 'Personnel du groupement', value: 'personnels' },
    { name: 'Personnel des pharmacies', value: 'ppersonnels' },
    { name: 'Titulaire', value: 'titulaires' },
    { name: 'Pharmacie', value: 'pharmacies' },
    { name: 'Produit', value: 'produits' },
    { name: 'Produit canal', value: 'produitcanals' },
    { name: 'Marché', value: 'marches' },
    { name: 'Promotion', value: 'promotions' },
    { name: 'Opération', value: 'operations' },
    { name: 'Actualité', value: 'actualites' },
    { name: 'Commande', value: 'commandes' },
  ];
  return (
    <>
      {(refreshIndexesResult.loading || reindexResult.loading) && (
        <Backdrop value={loadingMessage} />
      )}
      <div className={classes.root}>
        <RadioGroup row={true} value={checkedRadio} onChange={handleChange}>
          <FormControlLabel value="all" control={<Radio />} label="Reindexer tous" />
          <FormControlLabel
            value="one"
            control={<Radio />}
            label="Sélectionner les éléments à réindexer"
          />
        </RadioGroup>

        <div className={classes.input}>
          <CustomSelect
            label="Données à réindexer"
            list={list}
            index="name"
            listId="value"
            name=""
            value={routingKey}
            onChange={(e: any) => onChange(e)}
            disabled={checkedRadio === 'one' ? false : true}
          />
        </div>

        <Button
          className={classes.btn}
          variant="contained"
          color="secondary"
          onClick={handleSubmit}
          disabled={refreshIndexesResult.loading || reindexResult.loading ? true : false}
        >
          Réindexer
        </Button>

        <Button
          onClick={() => getFromEmail()}
          disabled={loading}
          variant="contained"
          color="secondary"
          style={{
            marginTop: 20
          }}
        >
          Facture : Scan email
        </Button>

        <Button
          onClick={() => updateItemScoring()}
          disabled={updatingItemScoring.loading}
          variant="contained"
          color="secondary"
          style={{
            marginTop: 20
          }}
        >
          Mettre à jour les scores
        </Button>
      </div>
    </>
  );
};
export default withRouter(Indexation);
