import React, { FC } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import Indexation from './Indexation';
import MajActiveDirectory from './MajActiveDirectory';
import MajHelloId from './MajHelloId';
import SendEmail from './ScanEmail'

const UtilitaireRouter: FC = () => {
    return (
        <Switch>
            <Route
                path={"/db/utilitaire/scan-email"}
                exact={true}
                render={() => <SendEmail />}
            />
            <Route
                path={["/db/utilitaire", "/db/utilitaire/indexation"]}
                exact={true}
                component={Indexation}
                render={() => <Indexation />}
            />
            <Route
                path={["/db/utilitaire", "/db/utilitaire/maj-active-directory"]}
                exact={true}
                component={MajActiveDirectory}
            />
            <Route
                path={["/db/utilitaire", "/db/utilitaire/maj-helloid"]}
                exact={true}
                component={MajHelloId}
            />
        </Switch>
    )
}

export default withRouter(UtilitaireRouter);