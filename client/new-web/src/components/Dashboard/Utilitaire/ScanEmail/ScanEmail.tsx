import React, { FC, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { Button } from '@material-ui/core';
import useStyles from './styles';
import { useLazyQuery } from '@apollo/client';
import Backdrop from '../../../Common/Backdrop';
import { FEDERATION_CLIENT } from '../../DemarcheQualite/apolloClientFederation';
import { GET_GET_DOCUMENT_FROM_EMAIL as GET_GET_DOCUMENT_FROM_EMAIL_TYPE } from '../../../../federation/ged/document/types/GET_GET_DOCUMENT_FROM_EMAIL';

import { GET_GET_DOCUMENT_FROM_EMAIL } from '../../../../federation/ged/document/query';

const ScanEmail: FC = () => {
  const classes = useStyles({});



  const [getFromEmail, { loading }] = useLazyQuery<GET_GET_DOCUMENT_FROM_EMAIL_TYPE>(
    GET_GET_DOCUMENT_FROM_EMAIL,
    { client: FEDERATION_CLIENT },
  );



  return (
    <>
      {loading && <Backdrop value="veuillez patienter..." />}
      <div className={classes.root}>
        <h3>Scan email</h3>

        <Button
          className={classes.btn}
          variant="contained"
          color="secondary"
          onClick={() => getFromEmail()}
          disabled={false}
        >
          Scan
        </Button>
      </div>
    </>
  );
};
export default withRouter(ScanEmail);
