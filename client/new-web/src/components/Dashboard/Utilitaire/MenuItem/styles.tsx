import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    icon: {
      color: 'gray',
    },
    contentBoxRow: {
      display: 'flex',
      alignItems: 'center',
      padding: theme.spacing(0.75, 0.375),
      '& > * + *': {
        marginLeft: theme.spacing(1),
      },
      '& a': {
        color: '#000000',
        textDecoration: 'none',
      },
    },
    nbrProduits: {
      background: '#F3F3F3',
      fontSize: '0.75rem',
      padding: theme.spacing(0, 1),
    },
    rightAlign: {
      textAlign: 'right',
    },
    itemLabel: {
      fontSize: 14,
      fontWeight: 500,
      padding: 0,
      margin: 0,
    },
    active: {
      backgroundColor: 'rgba(255, 255, 255, 0.05)',
      color: theme.palette.secondary.main,
    },
  }),
);

export default useStyles;
