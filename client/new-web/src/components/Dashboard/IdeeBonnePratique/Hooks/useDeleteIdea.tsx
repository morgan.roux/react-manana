import { useApolloClient, useMutation } from '@apollo/client';
import { differenceBy } from 'lodash';
import { Dispatch, SetStateAction, useContext } from 'react';
import { ContentContext, ContentStateInterface } from '../../../../AppContext';
import { DO_DELETE_IDEE_BONNE_PRATIQUE } from '../../../../graphql/IdeeBonnePratique/mutation';
import {
  DELETE_IDEE_BONNE_PRATIQUE,
  DELETE_IDEE_BONNE_PRATIQUEVariables,
} from '../../../../graphql/IdeeBonnePratique/types/DELETE_IDEE_BONNE_PRATIQUE';
import { displaySnackBar } from '../../../../utils/snackBarUtils';

export const useDeleteIdea = (
  selected: any[],
  setSelected: Dispatch<SetStateAction<any[]>>,
  deleteRow: any,
  setDeleteRow: Dispatch<SetStateAction<any>>,
) => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const client = useApolloClient();
  console.log('deleteRow :>> ', deleteRow);

  const selectedIds = (selected.length > 0 && selected.map(i => i.id)) || [];

  const [deleteIdea, { loading, data }] = useMutation<
    DELETE_IDEE_BONNE_PRATIQUE,
    DELETE_IDEE_BONNE_PRATIQUEVariables
  >(DO_DELETE_IDEE_BONNE_PRATIQUE, {
    variables: { ids: selectedIds },
    update: (cache, { data }) => {
      if (data && data.softDeleteIdeeOuBonnePratiques) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables,
          });
          if (req && req.search && req.search.data) {
            const source = req.search.data;
            const result = data.softDeleteIdeeOuBonnePratiques;
            const dif = differenceBy(source, result, 'id');
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: dif,
                  },
                },
              },
              variables,
            });
          }
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
    onCompleted: data => {
      if (data && data.softDeleteIdeeOuBonnePratiques) {
        displaySnackBar(client, { isOpen: true, type: 'SUCCESS', message: 'Suppression réussie' });
        setSelected([]);
        setDeleteRow(null);
      }
    },
  });

  return { deleteIdea, loading, data };
};
