import { useApolloClient, useMutation } from '@apollo/client';
import { useState } from 'react';
import { DO_CREATE_UPDATE_IDEE_BONNE_PRATIQUE } from '../../../../graphql/IdeeBonnePratique/mutation';
import {
  CREATE_UPDATE_IDEE_BONNE_PRATIQUE,
  CREATE_UPDATE_IDEE_BONNE_PRATIQUEVariables,
} from '../../../../graphql/IdeeBonnePratique/types/CREATE_UPDATE_IDEE_BONNE_PRATIQUE';
import { displaySnackBar } from '../../../../utils/snackBarUtils';

export const useCreateUpdateIdea = (
  values: any, /// to be changed CREATE_UPDATE_IDEE_BONNE_PRATIQUEVariables,
  operationName: any,
  variables: any,
) => {
  const client = useApolloClient();
  const [mutationSuccess, setMutationSuccess] = useState<boolean>(false);

  const [createUpdateIdea, { loading, data }] = useMutation<
    CREATE_UPDATE_IDEE_BONNE_PRATIQUE,
    CREATE_UPDATE_IDEE_BONNE_PRATIQUEVariables
  >(DO_CREATE_UPDATE_IDEE_BONNE_PRATIQUE, {
    variables: { ...values },
    update: (cache, { data }) => {
      if (data && data.createUpdateIdeeOuBonnePratique && values && !values.input.id) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables,
          });
          if (req && req.search && req.search.data) {
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    total: req.search.total + 1,
                    data: [...req.search.data, data.createUpdateIdeeOuBonnePratique],
                  },
                },
              },
              variables,
            });
          }
        }
      }
    },
    onCompleted: data => {
      if (data && data.createUpdateIdeeOuBonnePratique) {
        setMutationSuccess(true);
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  return { createUpdateIdea, mutationSuccess, loading, data };
};
