import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    personnelGroupementRoot: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      position: 'relative',
      height: 'auto',
      minHeight: 'fit-content',
    },
    detailLeftBox: {
      padding: 16,
      maxWidth: 1024,
      border: '1px solid #707070',
      borderRadius: 12,
      opacity: 1,
      width: '100%',
    },
    detailsContainer: {
      maxWidth: 1700,
      margin: theme.spacing(5, 'auto', 5),
      padding: theme.spacing(0, 3),
      [theme.breakpoints.down('sm')]: {
        flexDirection: 'column',
      },
    },
    items: {
      marginBottom: theme.spacing(2),
    },
    detailsRightBox: {
      padding: 16,
      marginLeft: theme.spacing(5),
      opacity: 1,
      width: '100%',
      maxWidth: 626,
      [theme.breakpoints.down('sm')]: {
        marginLeft: theme.spacing(0),
        maxWidth: 900,
        marginTop: theme.spacing(4),
      },
    },
    radioButton: {
      display: 'flex',
      flexDirection: 'row',
      marginBottom: 28,
      '& .MuiFormControlLabel-root': {
        marginRight: 30,
      },
      '& .MuiFormControlLabel-label': {
        fontSize: '0.875rem',
        fontWeight: 'bold',
      },
    },
    detailsTitle: {
      font: 'normal normal bold 22px/29px Roboto',
      marginBottom: theme.spacing(2),
    },
    detailsSubtitleRight: {
      fontWeight: 'bold',
      fontSize: '0.875rem',
    },
    detailsSubtitleLeft: {
      fontWeight: 'bold',
      fontSize: '1rem',
    },
    detailFontSize14: {
      fontSize: '0.875rem',
    },
    mutationSuccessContainer: {
      width: '100%',
      minHeight: 'calc(100vh - 156px)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
      '& img': {
        width: 350,
        height: 270,
      },

      '& p': {
        textAlign: 'center',
        maxWidth: 600,
      },
      '& > div > p:nth-child(2)': {
        fontSize: 30,
        fontWeight: 'bold',
      },
      '& > div > p:nth-child(3)': {
        fontSize: 16,
        fontWeight: 'normal',
      },
      '& button': {
        marginTop: 30,
      },
    },
    container: {
      width: '100%',
    },
    childrenRoot: {
      display: 'flex',
      '& > button:not(:nth-last-child(1))': {
        marginRight: 15,
      },
    },
    searchInputBox: {
      padding: '20px 0px 0px 20px',
      display: 'flex',
    },
    fixed: {
      position: 'fixed',
      top: 0,
      zIndex: 1,
    },
  }),
);

export default useStyles;
