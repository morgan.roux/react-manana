import { Add } from '@material-ui/icons';
import React, { Fragment, useContext, useEffect, useState } from 'react';
import { Box } from '@material-ui/core';
import { RouteComponentProps, withRouter } from 'react-router';
import { ContentContext, ContentStateInterface } from '../../../AppContext';
import { PARTAGE_IDEE_URL } from '../../../Constant/url';
import Backdrop from '../../Common/Backdrop';
import { ConfirmDeleteDialog } from '../../Common/ConfirmDialog';
import CustomButton from '../../Common/CustomButton';
import CustomContent from '../../Common/newCustomContent';
import SearchInput from '../../Common/newCustomContent/SearchInput';
import SubToolbar from '../../Common/newCustomContent/SubToolbar';
import { useIdeaColumns } from './Hooks/useIdeaColumns';
import useStyles from './styles';
import FormIdea from '../../Main/Content/InteligenceCollective/PartageIdeeBonnePratique/FormAjout/StepperIdea';

interface IdeeBonnePratiqueProps {
  listResult: any;
}

const IdeeBonnePratique: React.FC<IdeeBonnePratiqueProps & RouteComponentProps> = ({
  listResult,
  history: { push },
  location: { pathname },
}) => {
  const classes = useStyles({});
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  const {
    columns,
    deleteRow,
    onClickConfirmDelete,
    loading: deleteLoading,
    selected,
    setSelected,
    updateStatusLoading,
  } = useIdeaColumns(setOpenDeleteDialog);

  const ADD_URL = `/db/${PARTAGE_IDEE_URL}/create`;

  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const [operationNameState, setOperationName] = useState<any>();
  const [variablesState, setVariables] = useState<any>();
  const baseUrl: string = `/db/${PARTAGE_IDEE_URL}`;
  const isOnList = pathname === baseUrl;
  const isOnCreate = pathname === `/db/${PARTAGE_IDEE_URL}/create`;
  const isOnEdit: boolean = pathname.startsWith(`/db/${PARTAGE_IDEE_URL}/edit`);
  const isOnDetail: boolean = pathname.startsWith(`/db/${PARTAGE_IDEE_URL}/fiche`);

  // const { createUpdateIdea, loading: mutationLoading, data: mutationData } = useCreateUpdateIdea(
  //   { input: values },
  //   operationNameState,
  //   variablesState,
  // );

  const loadingMsg = `${
    isOnCreate ? 'Création' : isOnEdit ? 'Modification' : 'Chargement'
  } en cours...`;

  const goToList = () => push(`/db/${PARTAGE_IDEE_URL}`);

  const subToolbarTitle = isOnCreate
    ? `Ajout d'idée et bonne pratique`
    : isOnEdit
    ? `Modification`
    : 'Liste des idées et bonnes pratiques';

  const onClickDefaultBtn = () => {
    if (!isOnList) {
      goToList();
      return;
    }
    setOpenDeleteDialog(true);
  };

  const gotoAddIdea = () => push(ADD_URL);

  const onClickSecondaryBtn = () => {
    // if (!isOnList) {
    //   createUpdateIdea();
    //   return;
    // }
    gotoAddIdea();
  };

  const disableSaveButton = (): boolean => {
    // if (!code || !libelle) return true;
    return false;
  };

  // Hooks
  useEffect(() => {
    if (operationName && !operationNameState) {
      setOperationName(operationName);
      setVariables(variables);
    }
  }, [operationName]);

  // Return to list after mutation
  // useEffect(() => {
  //   if (mutationData && mutationData.createUpdateIdeeOuBonnePratique) {
  //     goToList();
  //   }
  // }, [mutationData]);

  const SubToolbarChildren = () => {
    return (
      <Box className={classes.childrenRoot}>
        {selected.length > 0 && isOnList && (
          <CustomButton color="default" onClick={onClickDefaultBtn}>
            {!isOnList ? 'Annuler' : 'Supprimer la sélection'}
          </CustomButton>
        )}
        <CustomButton
          color="secondary"
          startIcon={!isOnList ? null : <Add />}
          onClick={onClickSecondaryBtn}
          disabled={!isOnList ? disableSaveButton() : false}
        >
          {isOnCreate ? 'Ajouter' : isOnEdit ? 'Modifier' : 'Ajouter une idée'}
        </CustomButton>
      </Box>
    );
  };

  const ideaList = (
    <CustomContent
      selected={selected}
      setSelected={setSelected}
      {...{ listResult, columns }}
      isSelectable={true}
      showResetFilters={false}
    />
  );

  const DeleteDialogContent = () => {
    if (deleteRow) {
      return <span>Êtes-vous sur de vouloir supprimer ?</span>;
    }
    return <span>Êtes-vous sur de vouloir supprimer ces idées ?</span>;
  };

  return (
    <Box className={classes.container}>
      {(deleteLoading || updateStatusLoading) && (
        <Backdrop value={deleteLoading ? 'Suppression en cours...' : loadingMsg} />
      )}
      {isOnList && (
        <SubToolbar
          title={subToolbarTitle}
          dark={!isOnList}
          withBackBtn={!isOnList}
          onClickBack={goToList}
        >
          <SubToolbarChildren />
        </SubToolbar>
      )}
      <Box>
        {isOnList && (
          <Fragment>
            <Box className={classes.searchInputBox}>
              <SearchInput searchPlaceholder="Rechercher une idée" />
            </Box>
            {ideaList}
          </Fragment>
        )}
        {(isOnCreate || isOnEdit) && <FormIdea baseUrl={baseUrl} />}
      </Box>
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
    </Box>
  );
};

export default withRouter(IdeeBonnePratique);
