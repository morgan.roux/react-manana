import { useQuery } from '@apollo/client';
import { Box, FormControlLabel, Radio, RadioGroup, Typography } from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons';
import { last } from 'lodash';
import moment from 'moment';
import React, { useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { PARTAGE_IDEE_URL } from '../../../Constant/url';
import { GET_IDEE_BONNE_PRATIQUE } from '../../../graphql/IdeeBonnePratique';
import {
  IDEE_BONNE_PRATIQUE,
  IDEE_BONNE_PRATIQUEVariables,
} from '../../../graphql/IdeeBonnePratique/types/IDEE_BONNE_PRATIQUE';
import { UPDATE_STATUS_IDEE_BONNE_PRATIQUEVariables } from '../../../graphql/IdeeBonnePratique/types/UPDATE_STATUS_IDEE_BONNE_PRATIQUE';
import { IdeeOuBonnePratiqueStatus } from '../../../types/graphql-global-types';
import Backdrop from '../../Common/Backdrop';
import { ConfirmDeleteDialog } from '../../Common/ConfirmDialog';
import CustomButton from '../../Common/CustomButton';
import CustomTextarea from '../../Common/CustomTextarea';
import SubToolbar from '../../Common/newCustomContent/SubToolbar';
import { useDeleteIdea } from './Hooks/useDeleteIdea';
import { useUpdateStatus } from './Hooks/useUpdateStatus';
import useStyles from './styles';

interface IdeeBonnePratiqueProps {
  match: {
    params: {
      ideeId: string;
    };
  };
}

const IdeeBonnePratiqueDetails: React.FC<IdeeBonnePratiqueProps & RouteComponentProps> = props => {
  const {
    match: {
      params: { ideeId },
    },
    history: { push },
  } = props;

  const nl2br = (str: string | null): string | null => {
    return str ? str.replace(new RegExp('\n', 'g'), '<br/>') : str;
  };

  const [set, setter] = useState<any[]>([]);
  const [selected, setSelected] = useState<any[]>([]);
  const onClickBack = () => {
    push(`/db/${PARTAGE_IDEE_URL}`);
  };

  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const { deleteIdea, loading: deleteLoading } = useDeleteIdea([ideeId], setter, set, setSelected);
  const { data, loading } = useQuery<IDEE_BONNE_PRATIQUE, IDEE_BONNE_PRATIQUEVariables>(
    GET_IDEE_BONNE_PRATIQUE,
    {
      variables: {
        id: ideeId,
      },
      fetchPolicy: 'network-only',
    },
  );
  const resultData = data && data.ideeOuBonnePratique;

  const leftSections = [
    {
      title: 'Bénéficiaires clés',
      details: (resultData && resultData.beneficiaires_cles) || '-',
    },
    {
      title: 'Contexte',
      details: (resultData && resultData.contexte) || '-',
    },
    {
      title: 'Objectifs',
      details: (resultData && resultData.objectifs) || '-',
    },
    {
      title: 'Résultats',
      details: (resultData && resultData.resultats) || '-',
    },
    {
      title: 'Facteurs clés de succès',
      details: (resultData && resultData.facteursClesDeSucces) || '-',
    },
    {
      title: 'Contraintes',
      details: (resultData && resultData.contraintes) || '-',
    },
  ];

  const rightSections = [
    {
      title: 'Origine',
      item: (resultData && resultData._origine) || '-',
    },
    {
      title: 'Classification',
      item: (resultData && resultData.classification && resultData.classification.nom) || '-',
    },
    {
      title: 'Auteur',
      item: (resultData && resultData.auteur && resultData.auteur.userName) || '-',
    },
    {
      title: ' Vérificateur',
      item: '-',
    },
    {
      title: 'Date de création',
      item: moment(resultData && resultData.dateCreation).format('L'),
    },
  ];

  console.log('ID PAGE>>>>>>>>.', ideeId);

  const classes = useStyles({});

  const { updateStatus } = useUpdateStatus();
  const handleClickDelete = () => {
    setOpenDeleteDialog(!openDeleteDialog);
  };

  const onClickConfirmDelete = () => {
    deleteIdea({
      variables: {
        ids: [ideeId],
      },
    });
    onClickBack();
  };

  const initialValue: UPDATE_STATUS_IDEE_BONNE_PRATIQUEVariables = {
    id: ideeId,
    status: last(
      resultData &&
      resultData.ideeOuBonnePratiqueChangeStatus.map(change => change && change.status),
    ) as any,
    commentaire: last(
      resultData &&
      (resultData.ideeOuBonnePratiqueChangeStatus.map(
        change => change && change.commentaire,
      ) as any),
    ),
  };
  const handleClickEdit = () => { };
  const [values, setValues] = useState<UPDATE_STATUS_IDEE_BONNE_PRATIQUEVariables>(initialValue);

  const onClickApply = () => {
    updateStatus({
      variables: {
        ...values,
      },
    });
  };

  const handleChange = (event: React.ChangeEvent) => {
    const { name, value } = event.target as any;
    setValues(prevState => ({ ...prevState, [name]: value }));
    console.log('State values', values);
  };

  const DeleteDialogContent = () => {
    return <span>Êtes-vous sur de vouloir supprimer</span>;
  };
  const { commentaire, status } = values;
  return (
    <>
      {deleteLoading || (loading && <Backdrop />)}
      <div className={classes.fixed}>
        <SubToolbar
          title="Détails"
          dark={true}
          withBackBtn={true}
          onClickBack={onClickBack}
          backBtnIcon={<ArrowBack />}
        >
          <ConfirmDeleteDialog
            open={openDeleteDialog}
            setOpen={setOpenDeleteDialog}
            content={<DeleteDialogContent />}
            onClickConfirm={onClickConfirmDelete}
          />
          <CustomButton onClick={handleClickDelete}>Supprimer</CustomButton>
          <CustomButton onClick={handleClickEdit} color="secondary">
            Modifier
          </CustomButton>
        </SubToolbar>
      </div>
      <Box display="flex" alignItems="start" className={classes.detailsContainer}>
        <Box className={classes.detailLeftBox}>
          <Typography className={classes.detailsTitle}>{resultData?.title ?? '-'}</Typography>
          {leftSections.map(section => (
            <Box className={classes.items} key={section.title}>
              <Typography className={classes.detailsSubtitleLeft}>{section.title}</Typography>
              <Typography>
                <Box
                  dangerouslySetInnerHTML={
                    {
                      __html: nl2br(section.details),
                    } as any
                  }
                />
              </Typography>
            </Box>
          ))}
        </Box>
        <Box className={classes.detailsRightBox}>
          <Typography className={classes.detailsTitle}>
            Détails de l'idée ou bonne pratique
          </Typography>
          {rightSections.map(section => (
            <Box className={classes.detailsTitle} key={section.title}>
              <Typography className={classes.detailsSubtitleRight}>{section.title} : </Typography>
              <Typography className={classes.detailFontSize14}>{section.item}</Typography>
            </Box>
          ))}

          <RadioGroup
            className={classes.radioButton}
            name="status"
            value={status}
            onChange={handleChange}
          >
            <FormControlLabel
              id={IdeeOuBonnePratiqueStatus.VALIDEE}
              value={IdeeOuBonnePratiqueStatus.VALIDEE}
              control={<Radio />}
              label="Approuver"
            />
            <FormControlLabel
              id={IdeeOuBonnePratiqueStatus.REJETEE}
              value={IdeeOuBonnePratiqueStatus.REJETEE}
              control={<Radio />}
              label="Rejeter"
            />
          </RadioGroup>
          <Box mb={3}>
            <CustomTextarea
              label="commentaire"
              name="commentaire"
              value={commentaire as any}
              onChangeTextarea={handleChange}
              rows={6}
              rowsMax={9}
            />
          </Box>

          <CustomButton color="secondary" onClick={onClickApply}>
            Appliquer
          </CustomButton>
        </Box>
      </Box>
    </>
  );
};

export default withRouter(IdeeBonnePratiqueDetails);
