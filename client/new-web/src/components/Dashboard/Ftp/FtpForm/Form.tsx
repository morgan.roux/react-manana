import React, { FC, useState } from 'react';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { CustomFormTextField } from '../../../Common/CustomTextField';
import CustomButton from '../../../Common/CustomButton';
import IFtp from '../../../../Interface/FtpInterface';
import { useForm } from '../CustomHooks';

interface FormProps {
  defaultData?: IFtp;
  action(data: IFtp): void;
}

const styles = () =>
  createStyles({
    textareaStyle: {
      marginBottom: 25,
      overflowY: 'auto',
    },
    marginBottom: {
      marginBottom: 25,
    },
    marginRight: {
      marginRight: 10,
    },
    titleForm: {
      color: '#1D1D1D',
      marginTop: 0,
    },
    flex: {
      display: 'flex',
      flexDirection: 'row',
    },
    w100: {
      width: '100%',
    },
  });

const Form: FC<RouteComponentProps<any, any, any> & WithStyles & FormProps> = ({
  classes,
  defaultData,
  action,
}) => {
  const { values, handleChange, handleSubmit } = useForm(action, defaultData);
  const { host, port, user, password } = values;

  const canSubmit = (): boolean => {
    return host && port && user && password ? true : false;
  };
  return (
    <>
      <h3 className={classes.titleForm}>FTP</h3>
      <form onSubmit={handleSubmit}>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="host"
            label="Host"
            onChange={handleChange}
            value={host}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="port"
            label="Port"
            type="number"
            onChange={handleChange}
            value={port}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="user"
            label="Utilisateur"
            onChange={handleChange}
            value={user}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="password"
            label="Mot de passe"
            onChange={handleChange}
            value={password}
            required={true}
          />
        </div>
        <div className={`${classes.marginBottom} ${classes.flex}`}>
          <div className={classes.w100}>
            <CustomButton
              size="large"
              color="primary"
              fullWidth={true}
              disabled={!canSubmit()}
              onClick={handleSubmit}
            >
              ENREGISTRER
            </CustomButton>
          </div>
        </div>
      </form>
    </>
  );
};

export default withStyles(styles, { withTheme: true })(withRouter(Form));
