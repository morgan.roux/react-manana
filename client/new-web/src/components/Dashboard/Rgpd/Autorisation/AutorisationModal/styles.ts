import { createStyles, Theme } from '@material-ui/core/styles';
import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    usersModalRoot: {
      width: '100%',
      '& .MuiButton-label': {
        zIndex: 'auto',
      },
      '& .MuiDialogContent-root': {
        minHeight: 500,
      },
    },
    tableContainer: {
      width: '100%',
      marginTop: 20,
    },
    espaceHaut: {
      margin: '50px 0',
    },
    espacedroite: {
      marginRight: '12px',
    },
    section: {
      margin: 15,
    },
    inputText: {
      width: '100%',
      marginBottom: 12,
      '& .MuiOutlinedInput-root': {
        '&.Mui-focused': {
          '& .MuiOutlinedInput-notchedOutline': {
            borderColor: 'rgba(0, 0, 0, 0.23)!important',
          },
        },
      },
      '& .MuiSelect-selectMenu': {
        fontSize: '0.875rem',
      },
      '& .MuiOutlinedInput-input': { fontSize: '0.875rem' },
      '& .MuiFormLabel-root': {
        color: '#878787',
        fontSize: '0.875rem',
      },
    },
    customReactQuill: {
      width: "100%",
      outline: 'none',
      marginBottom: 18,
      marginTop: 25,
      border: '1px solid rgba(0, 0, 0, 0.23)',
      borderRadius: 4,
      '& .ql-toolbar.ql-snow': {
        border: '0px',
        borderBottom: '1px solid rgba(0, 0, 0, 0.23)',
      },
      '& .ql-container': {
        minHeight: 200,
        border: '0px',
      },
      '&:hover': {
        borderColor: theme.palette.primary.main,
      },
      '&:focus': {
        outline: `auto ${theme.palette.primary.main}`,
      },
    },
  }),
);
