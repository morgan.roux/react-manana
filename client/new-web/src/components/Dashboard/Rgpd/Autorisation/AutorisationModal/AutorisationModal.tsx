import React, { FC, useState } from 'react';
import { useStyles } from './styles';
import { CustomModal } from '../../../../Common/CustomModal';
import Box from '@material-ui/core/Box';
import ReactQuill from 'react-quill';
import { useApolloClient, useMutation } from '@apollo/client';
import { DO_CREATE_AUTHORIZATION } from '../../../../../graphql/Authorization/mutation';
import {
  CREATE_AUTHORIZATION,
  CREATE_AUTHORIZATIONVariables,
} from '../../../../../graphql/Authorization/types/CREATE_AUTHORIZATION';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { GET_AUTHORIZATION_LIST } from '../../../../../graphql/Authorization/query';
import { AUTHORIZATION_LIST } from '../../../../../graphql/Authorization/types/AUTHORIZATION_LIST';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import { GET_RGPD_AUTORISATIONS } from '../../../../../graphql/Rgpd/query';
import { RGPD_AUTORISATIONS } from '../../../../../graphql/Rgpd/types/RGPD_AUTORISATIONS';
interface AutorisationModalProps {
  open: boolean;
  setOpen: (any) => void;
  title: string;
  withButtonAction?: boolean;
  closeIcon?: boolean;
  withHeaderColor?: boolean;
  onClickConfirm?: any;
  item?: any | undefined;
  isOnCreate: boolean;
}
const AutorisationModal: FC<AutorisationModalProps> = ({
  open,
  setOpen,
  title,
  closeIcon,
  withHeaderColor,
  item,
  isOnCreate,
}) => {
  const classes = useStyles({});

  //valeur initiale popup

  const [titre, setTitre] = useState<string>(isOnCreate ? '' : item.title);
  const [description, setDescription] = useState<string>(isOnCreate ? '' : item.description);

  //form change popup
  const descriptionChange = (content: string) => {
    setDescription(content);
  };
  const formChange = event => {
    const { value, name } = event.target;

    setTitre(value);
  };

  //mutation create and update autorisation
  const client = useApolloClient();
  const [CreateUpdateAuthorization] = useMutation<
    CREATE_AUTHORIZATION,
    CREATE_AUTHORIZATIONVariables
  >(DO_CREATE_AUTHORIZATION, {
    onCompleted: () => {
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: isOnCreate
          ? 'Autorisation ajoutée avec succès'
          : 'Autorisation modifiée avec succès',
        isOpen: true,
      });
      setOpen(false);
    },
    onError: error => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: error.message,
        isOpen: true,
      });
    },
    update: (cache, { data }) => {
      if (cache && data) {
        const req: any = cache.readQuery({ query: GET_RGPD_AUTORISATIONS });
        const IDdel = data.createUpdateRgpdAutorisation.id;
        if (req && isOnCreate) {
          const result = [...req.rgpdAutorisations, ...[data.createUpdateRgpdAutorisation]];
          cache.writeQuery<RGPD_AUTORISATIONS, any>({
            query: GET_RGPD_AUTORISATIONS,
            data: {
              rgpdAutorisations: result,
            },
          });
          setTitre('');
          setDescription('');
        } else {
          const result = [...req.rgpdAutorisations];
          const resultFiltered = result.map(item => {
            if (item.id === IDdel) return data.createUpdateRgpdAutorisation;
            else return item;
          });

          cache.writeQuery<RGPD_AUTORISATIONS, any>({
            query: GET_RGPD_AUTORISATIONS,
            data: {
              rgpdAutorisations: resultFiltered,
            },
          });
        }
      }
    },
  });

  const handleCreateUpdateAuthorization = (title: string, content: string) => {
    if (title && content) {
      const RgpdCreateAutorisationInput = isOnCreate
        ? { title: title, description: content }
        : { id: item.id, title: title, description: content };
      CreateUpdateAuthorization({ variables: { input: { ...RgpdCreateAutorisationInput } } });
    }
  };

  const handleClickConfirm = event => {
    event.stopPropagation();
    if (titre !== '' && description !== '') {
      handleCreateUpdateAuthorization(titre, description);
    }
  };

  //activebutton
  const [activeButton, setActiveButton] = useState<boolean>(false);
  React.useEffect(() => {
    if (titre !== '' && description !== '') {
      setActiveButton(true);
    } else setActiveButton(false);
  }, [titre, description]);
  return (
    <>
      <CustomModal
        open={open}
        setOpen={setOpen}
        title={title}
        withBtnsActions={activeButton}
        closeIcon={closeIcon}
        headerWithBgColor={withHeaderColor}
        maxWidth="sm"
        fullWidth={true}
        className={classes.usersModalRoot}
        onClickConfirm={handleClickConfirm}
        withCancelButton={false}
        actionButtonTitle="ENREGISTRER"
      >
        <Box className={classes.tableContainer}>
          <Box className={classes.section}>
            <CustomFormTextField
              name="titre"
              label="titre"
              value={titre}
              required={true}
              onChange={formChange}
            />
          </Box>
          <Box className={classes.section}>
            <ReactQuill
              className={classes.customReactQuill}
              theme="snow"
              value={description}
              onChange={descriptionChange}
            />
          </Box>
        </Box>
      </CustomModal>
    </>
  );
};
export default AutorisationModal;
