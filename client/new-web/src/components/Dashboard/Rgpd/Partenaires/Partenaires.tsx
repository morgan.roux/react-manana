import { useApolloClient, useQuery } from '@apollo/client';
import { Box, Grid, IconButton, Paper, Typography } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { GET_RGPD_PARTENAIRES } from '../../../../graphql/Rgpd/query';
import {
  RGPD_PARTENAIRES,
  RGPD_PARTENAIRESVariables,
} from '../../../../graphql/Rgpd/types/RGPD_PARTENAIRES';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop/Backdrop';
import PaperItem from '../Common/PaperItem';
import PartenaireModal from './PartenairesModal/PartenaireModal';
import PartenaireModalModal from './PartenairesModal/PartenaireModal';
//import ActionButton from '../Section/RgpdActionButton11';
import useStyles from './styles';

interface PartenairesProps {
  listResult?: any;
}

const Partenaires: FC<PartenairesProps & RouteComponentProps> = ({ }) => {
  const classes = useStyles({});
  const client = useApolloClient();
  // query to get partenaires
  const { data: partenaireData, loading: partenaireDataLoading } = useQuery<
    RGPD_PARTENAIRES,
    RGPD_PARTENAIRESVariables
  >(GET_RGPD_PARTENAIRES, {
    onError: error => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: error.message,
        isOpen: true,
      });
    },
  });
  const [openModal, setOpenModal] = useState(false);
  const handleClick = event => {
    // openModal
    event.stopPropagation();
    setOpenModal(true);
  };

  const partenaires = partenaireData && partenaireData.rgpdPartenaires;
  return (
    <Box className={classes.root}>
      <Box display="flex" flexDirection="column" width="100%">
        <Box display="flex" alignItems="flex-end">
          <Typography className={classes.title}>Partenaires</Typography>
        </Box>
        <Typography className={classes.icon} onClick={handleClick}>
          <Add />
          Ajouter un partenaire
        </Typography>
      </Box>
      {partenaireDataLoading && <Backdrop value="Chargement en cours, veuillez patienter ..." />}
      <Box className={classes.contentPaperItem}>
        {partenaires &&
          partenaires.length > 0 &&
          partenaires.map(partenaire => (
            <PaperItem
              key={partenaire.id}
              title={partenaire.title}
              content={partenaire.description}
              item={partenaire}
              isOnAutorisation={false}
            />
          ))}
      </Box>
      <Box>
        <PartenaireModal
          setOpen={setOpenModal}
          open={openModal}
          title="Ajouter un partenaire"
          closeIcon={true}
          withHeaderColor={true}
          isOnCreate={true}
        />
      </Box>
    </Box>
  );
};

export default withRouter(Partenaires);
