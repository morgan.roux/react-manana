import React, { FC, useState } from 'react';
import { useStyles } from './styles';
import { CustomModal } from '../../../../Common/CustomModal';
import Box from '@material-ui/core/Box';
import ReactQuill from 'react-quill';
import { useApolloClient, useMutation } from '@apollo/client';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';

import { CustomFormTextField } from '../../../../Common/CustomTextField';
import { DO_CREATE_UPDATE_RGPD_PARTENAIRE } from '../../../../../graphql/Rgpd/mutation';
import {
  CREATE_UPDATE_RGPD_PARTENAIRE,
  CREATE_UPDATE_RGPD_PARTENAIREVariables,
} from '../../../../../graphql/Rgpd/types/CREATE_UPDATE_RGPD_PARTENAIRE';
import { GET_RGPD_PARTENAIRES } from '../../../../../graphql/Rgpd/query';
import {
  RGPD_PARTENAIRES,
  RGPD_PARTENAIRESVariables,
} from '../../../../../graphql/Rgpd/types/RGPD_PARTENAIRES';
interface PartenaireModalModalProps {
  open: boolean;
  setOpen: (any) => void;
  title: string;
  closeIcon?: boolean;
  withHeaderColor?: boolean;
  onClickConfirm?: any;
  item?: any | undefined;
  isOnCreate: boolean;
}

const PartenaireModal: FC<PartenaireModalModalProps> = ({
  open,
  setOpen,
  title,
  closeIcon,
  withHeaderColor,
  item,
  isOnCreate,
}) => {
  const classes = useStyles({});

  //valeur initiale popup

  const [titre, setTitre] = useState<string>(isOnCreate ? '' : item.title);
  const [description, setDescription] = useState<string>(isOnCreate ? '' : item.description);

  //form change popup

  const descriptionChange = (content: string) => {
    setDescription(content);
  };
  const formChange = event => {
    const { value, name } = event.target;
    setTitre(value);
  };

  //mutation create and update partenaires
  const client = useApolloClient();
  const [CreateUpdatePartenaire] = useMutation<
    CREATE_UPDATE_RGPD_PARTENAIRE,
    CREATE_UPDATE_RGPD_PARTENAIREVariables
  >(DO_CREATE_UPDATE_RGPD_PARTENAIRE, {
    onCompleted: () => {
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: isOnCreate ? 'Partenaire ajouté avec succès' : 'Partenaire modifié avec succès',
        isOpen: true,
      });
      setOpen(false);
    },
    onError: error => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: error.message,
        isOpen: true,
      });
    },
    update: (cache, { data }) => {
      if (cache && data) {
        const req: any = cache.readQuery({ query: GET_RGPD_PARTENAIRES });
        const IDdel = data.createUpdateRgpdPartenaire.id;
        if (req && isOnCreate) {
          const result = [...req.rgpdPartenaires, ...[data.createUpdateRgpdPartenaire]];
          cache.writeQuery<RGPD_PARTENAIRES, RGPD_PARTENAIRESVariables>({
            query: GET_RGPD_PARTENAIRES,
            data: {
              rgpdPartenaires: result,
            },
          });
          setDescription('');
          setTitre('');
        } else {
          const result = [...req.rgpdPartenaires];
          const resultFiltered = result.map(item => {
            if (item.id === IDdel) return data.createUpdateRgpdPartenaire;
            else return item;
          });

          cache.writeQuery<RGPD_PARTENAIRES, RGPD_PARTENAIRESVariables>({
            query: GET_RGPD_PARTENAIRES,
            data: {
              rgpdPartenaires: resultFiltered,
            },
          });
        }
      }
    },
  });

  const handleCreateUpdatePartenaire = (title: string, content: string) => {
    if (title && content) {
      const RgpdCreatePartenaireInput = isOnCreate
        ? { title: title, description: content }
        : { id: item.id, title: title, description: content };
      CreateUpdatePartenaire({ variables: { input: { ...RgpdCreatePartenaireInput } } });
    }
  };

  const handleClickConfirm = event => {
    event.stopPropagation();
    if (titre !== '' && description !== '') {
      handleCreateUpdatePartenaire(titre, description);
    }
  };

  //activebutton
  const [activeButton, setActiveButton] = useState<boolean>(false);
  React.useEffect(() => {
    if (titre !== '' && description !== '') {
      setActiveButton(true);
    } else setActiveButton(false);
  }, [titre, description]);
  return (
    <>
      <CustomModal
        open={open}
        setOpen={setOpen}
        title={title}
        withBtnsActions={activeButton}
        closeIcon={closeIcon}
        headerWithBgColor={withHeaderColor}
        maxWidth="sm"
        fullWidth={true}
        className={classes.usersModalRoot}
        onClickConfirm={handleClickConfirm}
        withCancelButton={false}
        actionButtonTitle="ENREGISTRER"
      >
        <Box className={classes.tableContainer}>
          <Box className={classes.section}>
            <CustomFormTextField
              name="titre"
              label="titre"
              value={titre}
              required={true}
              onChange={formChange}
            />
          </Box>
          <Box className={classes.section}>
            <ReactQuill
              className={classes.customReactQuill}
              theme="snow"
              value={description}
              onChange={descriptionChange}
            />
          </Box>
        </Box>
      </CustomModal>
    </>
  );
};
export default PartenaireModal;
