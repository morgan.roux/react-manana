import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      padding: '36px 50px',
    },
    contentPaperItem: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-between  ',
    },

    title: {
      letterSpacing: 0,
      opacity: 1,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 20,
    },
    icon: {
      display: 'flex',
      alignItems: 'center',
      fontFamily: 'Montserrat',
      fontSize: '14px',
      fontWeight: 'bold',
      color: theme.palette.secondary.main,
      marginTop: 12,
      cursor: 'pointer',
      width: 'fit-content',
    },
  }),
);

export default useStyles;
