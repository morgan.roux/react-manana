import { useApolloClient, useMutation } from '@apollo/client';
import { Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Delete, Edit, MoreHoriz } from '@material-ui/icons';
import AddIcon from '@material-ui/icons/Add';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import PlaylistAddIcon from '@material-ui/icons/PlaylistAdd';
import React, { useState } from 'react';
import Backdrop from '../../../../Common/Backdrop';
import { ConfirmDeleteDialog } from '../../../../Common/ConfirmDialog';
import { ActionButtonMenu } from '../../../../Main/Content/TodoNew/Common/ActionButton';
import { operationVariable } from '../../../../Main/Content/TodoNew/MainContent/MainContent';



// import { GET_SEARCH_ACTION } from '../../../../../../../graphql/Action';
// import { DO_DELETE_ACTIONS } from '../../../../../../../graphql/ActionNew/mutation';
// import {
//   DELETE_ACTIONS,
//   DELETE_ACTIONSVariables,
// } from '../../../../../../../graphql/ActionNew/types/DELETE_ACTIONS';
// import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';
// import Backdrop from '../../../../../../Common/Backdrop';
// import { ConfirmDeleteDialog } from '../../../../../../Common/ConfirmDialog';
// import { ActionButtonMenu } from '../../../Common/ActionButton';
// import AjoutTaches from '../../../Modals/AjoutTache';
// import { CountTodosProps } from '../../../TodoNew';

// import { operationVariable } from '../../MainContent';
interface RgpdActionButtonProps {
  rgpd?: any;
  refetch?: any;
}
const RgpdActionButton: React.FC<RgpdActionButtonProps> = props => {
  const { rgpd, refetch } = props;

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = anchorEl ? true : false;
  const [openModalRgpd, setOpenModalRgpd] = useState<boolean>(false);
  const [openDeleteDialogRgpd, setOpenDeleteDialogRgpd] = useState<boolean>(false);
  const [operationName, setOperationName] = useState<string>('');

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    if (event) {
      event.stopPropagation();
      setAnchorEl(event.currentTarget);
    }
  };

  const handlesetOpenDialogDelete = () => {
    setOpenDeleteDialogRgpd(!openDeleteDialogRgpd);
  };

  const client = useApolloClient();

  const handleClose = () => {
    setAnchorEl(null);
  };

  const { addDown, addSubs, edit, addUpper } = operationVariable;

  const handleOpenModalRgpdAction = () => {
    setOpenModalRgpd(!openModalRgpd);
    setOperationName(edit);
  };

  //   const handleOpenAddSubTask = () => {
  //     setOpenModalTache(!openModalTache);
  //     setOperationName(addSubs);
  //   };
  //   const handleOpenAddUpperTask = () => {
  //     setOpenModalTache(!openModalTache);
  //     setOperationName(addUpper);
  //   };
  //   const handleOpenAddDownTask = () => {
  //     setOpenModalTache(!openModalTache);
  //     setOperationName(addDown);
  //   };
  //   const [removeTache, { loading: loadingDelete }] = useMutation<
  //     DELETE_ACTIONS,
  //     DELETE_ACTIONSVariables
  //   >(DO_DELETE_ACTIONS, {
  //     onCompleted: data => {
  //       displaySnackBar(client, {
  //         isOpen: true,
  //         type: 'SUCCESS',
  //         message: 'Tâche supprimée avec succès',
  //       });
  //       refetch();
  //     },
  //   });

  const onClickConfirmDeleteRgpd = () => {
    // removeTache({
    //   variables: {
    //     ids: partenaire && partenaire.id,
    //   },
    // });
    setOpenDeleteDialogRgpd(!openDeleteDialogRgpd);
  };

  const rgpdlistMenuItems: ActionButtonMenu[] = [

    {
      label: 'Modifier',
      icon: <Edit />,
      onClick: () => handleOpenModalRgpdAction(),
      disabled: false,
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: handlesetOpenDialogDelete,
      disabled: false,
    },
  ];

  return (
    <>
      {/* { <Backdrop value="Suppression en cours, veuillez patienter..." />} */}
      {/* <ConfirmDeleteDialog
        open={openDeleteDialogRgpd}
        setOpen={handlesetOpenDialogDelete}
        onClickConfirm={onClickConfirmDeleteRgpd}
      /> */}
      <IconButton aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        <MoreHoriz />
      </IconButton>
      <Menu
        // tslint:disable-next-line: jsx-no-lambda
        onClick={event => {
          event.preventDefault();
          event.stopPropagation();
        }}
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        {rgpdlistMenuItems &&
          rgpdlistMenuItems.length > 0 &&
          rgpdlistMenuItems.map((i: ActionButtonMenu, index: number) => (
            <MenuItem
              // tslint:disable-next-line: jsx-no-lambda
              onClick={event => {
                event.preventDefault();
                event.stopPropagation();
                handleClose();
                i.onClick(event);
              }}
              key={`table_menu_item_${index}`}
              disabled={i.disabled}
            >
              <ListItemIcon>{i.icon}</ListItemIcon>
              <Typography variant="inherit">{i.label}</Typography>
            </MenuItem>
          ))}
      </Menu>
      {/* <AjoutTaches
        openModalTache={openModalTache}
        setOpenTache={setOpenModalTache}
        task={task}
        operationName={operationName}
        refetchCountTodos={refetchCountTodos}
        refetchTasks={refetch}
      /> */}
    </>
  );
};

export default RgpdActionButton;
