import { Box, Paper, Typography } from '@material-ui/core';

import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import AutorisationActionButton from '../../Autorisation/ActionButton/AutorisationActionButton';
import PartenaireActionButton from '../../Partenaires/ActionButton/PartenaireActionButton';

import useStyles from './styles';

interface PaperItemProps {
  listResult?: any;
  title: string;
  content: string;
  item: any;
  isOnAutorisation: boolean;
}

const PaperItem: FC<PaperItemProps & RouteComponentProps> = ({
  content,
  title,
  item,
  isOnAutorisation,
}) => {
  const classes = useStyles({});
  const [itemSelected, setItemSelected] = useState<any>(item);
  useEffect(() => {
    setItemSelected(item);
  }, [item]);
  return (
    <>
      <Paper elevation={20} className={classes.onPaper}>
        <Box display="flex" alignItems="center" mb={2.5}>
          <Typography className={classes.titlePaper}>{title}</Typography>

          <Box marginLeft="auto">
            {isOnAutorisation ? (
              <AutorisationActionButton item={itemSelected} />
            ) : (
              <PartenaireActionButton item={itemSelected} />
            )}
          </Box>
        </Box>

        <Box className={classes.contentText} dangerouslySetInnerHTML={{ __html: content }} />
      </Paper>
    </>
  );
};

export default withRouter(PaperItem);
