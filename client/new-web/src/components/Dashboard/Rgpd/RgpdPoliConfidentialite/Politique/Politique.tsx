import { useApolloClient } from '@apollo/client';
import React, { FC } from 'react';
import useStyles from './styles';
import { Box, Paper, Typography } from '@material-ui/core';
import { useGetRgpds } from '../../hooks';

interface PolitiqueProps {
  Content?: any;
}

const Politique: FC<PolitiqueProps> = ({ }) => {
  const { data: getData, loading: getLoading } = useGetRgpds();
  const exist =
    (getData && getData.rgpds && getData.rgpds[0] && getData.rgpds[0].politiqueConfidentialite) ||
    '';

  const client = useApolloClient();
  const classes = useStyles({});
  return (
    <>
      <Paper elevation={20} className={classes.onPaper}>
        <Box dangerouslySetInnerHTML={{ __html: exist }}></Box>
      </Paper>
    </>
  );
};
export default Politique;
