import { useApolloClient } from '@apollo/client';
import React, { FC, useEffect, useState } from 'react';
import useStyles from './styles';
import { Box, Typography } from '@material-ui/core';
import CustomButton from '../../../Common/CustomButton';
import SubToolbar from '../../../Common/newCustomContent/SubToolbar';
import Politique from './Politique/Politique';
import Information from './Information/Information';
import Condition from './Condition/Condition';
import CustomTabs from '../../../Common/CustomTabs';
import { TabInterface } from '../../../Common/CustomTabs/CustomTabs';
import { RouteComponentProps, withRouter } from 'react-router';

interface RgpdPoliConfidentialiteProps {
  listResult?: any;
  valueBtn?: any;
  Content?: string;
  match: {
    params: {
      tab: string | undefined;
    };
  };
}

const RgpdPoliConfidentialite: FC<RgpdPoliConfidentialiteProps & RouteComponentProps> = ({
  Content,
  match: {
    params: { tab },
  },
}) => {
  const client = useApolloClient();
  const classes = useStyles({});
  const [currentTab, setCurrentTab] = useState(tab || '');
  const [result, setResult] = useState<any>();

  const tabs: TabInterface[] = [
    {
      id: 0,
      label: 'POLITIQUE DE CONFIDENTIALITÉ',
      content: <Politique />,
      clickHandlerParams: 'politique',
    },
    {
      id: 1,
      label: `CONDITIONS D'UTILISATION`,
      content: <Condition />,
      clickHandlerParams: 'condition',
    },
    {
      id: 2,
      label: 'INFORMATIONS SUR LES COOKIES',
      content: <Information />,
      clickHandlerParams: 'information',
    },
  ];

  const handleTabClick = (param: string) => {
    window.history.pushState(null, '', `/#/rgpd-politique-de-confident/${param}`);
    setCurrentTab(param);
  };
  const tabStep = [
    { index: 0, tab: 'politique' },
    { index: 1, tab: 'information' },
    { index: 2, tab: 'condition' },
  ];

  const activeStepItem = tabStep.find(tabItem => tabItem.tab === tab);
  const activeStep = activeStepItem && activeStepItem.index;
  const [title, setTitle] = useState<string>();

  useEffect(() => {
    if (currentTab === 'politique') setTitle('Politique de confidentialité');
    else if (currentTab === 'condition') setTitle(`Conditions d'utilisation`);
    else setTitle('Informations sur les cookies');
  }, [currentTab]);

  return (
    <Box className={classes.container}>
      <SubToolbar dark={true} withBackBtn={true} title={title}></SubToolbar>
      <Box className={classes.fichePharmacieMainContent}>
        <CustomTabs
          tabs={tabs}
          hideArrow={true}
          clickHandler={handleTabClick}
          activeStep={activeStep}
        />
      </Box>
    </Box>
  );
};
export default withRouter(RgpdPoliConfidentialite);
