import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
        width: '100%',

      },
    fichePharmacieMainContent: {
      display: 'flex',
      padding: theme.spacing(5,3),
      maxWidth: 1200,
      margin:'0 auto',
      width:'100%',
      '& .MuiAppBar-root':{
        background:'inherit',
        boxShadow:'none!important',
        '& .MuiTabs-flexContainer':{
          justifyContent:'center',
          '& .MuiTab-root':{
            width: '100%',
            maxWidth: 305,
            backgroundColor:'#9E9E9E',
            '&:not(:nth-last-child(1))':{
              marginRight: 24
          },
            '&.Mui-selected':{
              backgroundColor:theme.palette.secondary.main,
              color: theme.palette.common.white
            }
          }
        }
      },
      '& .MuiOutlinedInput-input': {
        padding: '7px 14px !important',
      },
      '& .MuiOutlinedInput-adornedEnd': {
        paddingRight: 50,
      },
      '& .MuiTabs-root': {
        width: '100%',
      },
      '& #customTabsAppBarId': {
        marginBottom: 25,
      },
    },
  }),
);

export default useStyles;
