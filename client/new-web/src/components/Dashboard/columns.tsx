import { Link, useTheme } from '@material-ui/core';
import React, { Fragment, useState } from 'react';
import {
  LABORATOIRE_URL,
  PARTENAIRE_LABORATOIRE_URL,
  PARTENAIRE_SERVICE_URL,
  PERSONNEL_GROUPEMENT_URL,
  PERSONNEL_PHARMACIE_URL,
  PHARMACIE_URL,
  PRESIDENT_REGION_URL,
  TITULAIRE_PHARMACIE_URL,
} from '../../Constant/url';
import { capitalize } from '../../utils/Helpers';
import CustomAvatar from '../Common/CustomAvatar';
import UserStatus from './Content/UserStatus';
import { useStyles } from './styles';
import TableActionColumn from './TableActionColumn';

export const useGetColumns = (
  type: string,
  history: any,
  pathname: string,
  handleModal: (open: boolean, userId: string, userEmail: string, userLogin: string) => void,
  onClickHistory?: (open: boolean, userId: string) => void,
  handleClickDelete?: (item: any) => void,
  setCurrentItem?: (item: any) => void,
) => {
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const theme = useTheme();

  const classes = useStyles({});

  switch (type) {
    case 'TITULAIRE_PHARMACIE':
      /* titulaire pharmacie */
      return [
        {
          name: '',
          label: 'Photo',
          renderer: (value: any) => {
            const valueUser =
              value.users &&
                value.pharmacies &&
                value.pharmacies.length === 1 &&
                value.users.length === 1
                ? value.users[0]
                : null;
            return (
              <CustomAvatar
                name={value && value.fullName}
                url={
                  valueUser &&
                  valueUser.userPhoto &&
                  valueUser.userPhoto.fichier &&
                  valueUser.userPhoto.fichier.publicUrl
                }
              />
            );
          },
        },
        {
          name: 'fullName',
          label: 'Nom',
        },
        {
          name: 'user.anneeNaissance',
          label: 'Année de naissance',
          renderer: (value: any) => {
            const valueUser =
              value.users &&
                value.pharmacies &&
                value.pharmacies.length === 1 &&
                value.users.length === 1
                ? value.users[0]
                : null;
            return (valueUser && valueUser.anneeNaissance) || '-';
          },
        },
        {
          name: 'users.email',
          label: 'Email',
          renderer: (value: any) => {
            const valueUser =
              value.users &&
                value.pharmacies &&
                value.pharmacies.length === 1 &&
                value.users.length === 1
                ? value.users[0]
                : null;
            return (valueUser && valueUser.email) || '-';
          },
        },
        {
          name: 'cp',
          label: 'Code Postal',
          renderer: (value: any) => {
            return value.cp ? value.cp : '-';
          },
        },
        {
          name: 'ville',
          label: 'Ville',
          renderer: (value: any) => {
            return value.ville ? value.ville : '-';
          },
        },
        {
          name: 'adresse1',
          label: 'Adresse',
          renderer: (value: any) => {
            return value.adresse1 ? value.adresse1 : '-';
          },
        },
        {
          name: '',
          label: 'Statut Titulaire',
          sortable: false,
          renderer: (value: any) => {
            const valueUser =
              value.users &&
                value.pharmacies &&
                value.pharmacies.length === 1 &&
                value.users.length === 1
                ? value.users[0]
                : null;
            return valueUser ? <UserStatus user={valueUser} pathname={pathname} /> : '-';
          },
        },
        {
          name: 'pharmacies.nom',
          label: 'Pharmacie(s)',
          renderer: (value: any) => {
            const pharmacies: any[] = (value && value.pharmacies) || [];
            return (
              <div className={classes.pharmaciesLinkContainer}>
                {pharmacies.map((pharmacie: any, index: number) => {
                  if (pharmacie) {
                    const goToPharmacie = () => {
                      history.push(`/db/${PHARMACIE_URL}/fiche/${pharmacie.id}`);
                    };
                    return (
                      <Fragment key={`titulaire_pharma_${index}`}>
                        <Link color="secondary" variant="body2" onClick={goToPharmacie}>
                          {pharmacie.nom || '-'}
                        </Link>
                        <span> / </span>
                      </Fragment>
                    );
                  } else {
                    return '-';
                  }
                })}
              </div>
            );
          },
        },
        {
          name: 'pharmacies.cip',
          label: 'CIP',
          renderer: (value: any) => {
            return value.pharmacies
              ? value.pharmacies.map((pharm: any) => pharm.cip && pharm.cip).join(' / ')
              : '-';
          },
        },
        {
          name: 'pharmacies.actived',
          label: 'Statut Pharmacie',
          renderer: (value: any) => {
            return value.pharmacies
              ? value.pharmacies
                .map((pharm: any) =>
                  pharm.actived ? (pharm.actived ? 'Activée' : 'Inactive') : '-',
                )
                .join(' / ')
              : '-';
          },
        },
        {
          name: 'pharmacies.sortie',
          label: 'Sortie',
          renderer: (value: any) => {
            return value.pharmacies
              ? value.pharmacies
                .map((pharm: any) => (pharm.sortie === 0 ? 'Non' : 'Oui'))
                .join(' / ')
              : '-';
          },
        },
        {
          name: '',
          label: '',
          sortable: false,
          renderer: (value: any) => {
            return (
              <TableActionColumn
                row={value}
                baseUrl={TITULAIRE_PHARMACIE_URL}
                setOpenDeleteDialog={setOpenDeleteDialog}
                withViewDetails={true}
              // handleClickDelete={onClickDelete}
              />
            );
          },
        },
      ];
      /*  end titulaire */
      break;

    case 'PERSONNEL_GROUPEMENT':
      return [
        {
          name: '',
          label: 'Photo',
          renderer: (value: any) => {
            return (
              <CustomAvatar
                name={value && value.fullName}
                url={
                  value &&
                  value.user &&
                  value.user.userPhoto &&
                  value.user.userPhoto.fichier &&
                  value.user.userPhoto.fichier.publicUrl
                }
              />
            );
          },
        },
        {
          name: 'civilite',
          label: 'Civilité',
        },

        {
          name: 'prenom',
          label: 'Prénom',
        },
        {
          name: 'nom',
          label: 'Nom',
        },
        {
          name: 'service.nom',
          label: 'Services',
          renderer: (value: any) => {
            return (value && value.service && value.service.nom) || '-';
          },
        },
        {
          name: 'role.nom',
          label: 'Rôle',
          renderer: (value: any) => {
            return value.role && value.role.nom ? capitalize(value.role.nom) : '-';
          },
        },
        {
          name: '',
          label: 'Email',
          renderer: (value: any) => {
            return value.mailProf && value.mailProf
              ? value.mailProf
              : value.mailPerso
                ? value.mailPerso
                : '-';
          },
        },
        {
          name: '',
          label: 'Téléphone',
          renderer: (value: any) => {
            return value.telBureau && value.telBureau
              ? value.telBureau
              : value.telDomicile
                ? value.telDomicile
                : value.telMobProf
                  ? value.telMobProf
                  : value.telMobPerso
                    ? value.telMobPerso
                    : '-';
          },
        },
        {
          name: '',
          label: 'Statut',
          renderer: (value: any) => {
            return <UserStatus user={value.user} pathname={pathname} />;
          },
        },
        {
          name: '',
          label: '',
          renderer: (value: any) => {
            return (
              <TableActionColumn
                row={value}
                baseUrl={PERSONNEL_GROUPEMENT_URL}
                setOpenDeleteDialog={setOpenDeleteDialog}
              // handleClickDelete={onClickDelete}
              />
            );
          },
        },
      ];

      /*  end PERSONNEL_GROUPEMENT */
      break;
    case 'PERSONNEL_PHARMACIE':
      return [
        {
          name: '',
          label: 'Photo',
          renderer: (value: any) => {
            return (
              <CustomAvatar
                name={value && value.fullName}
                url={
                  value &&
                  value.user &&
                  value.user.userPhoto &&
                  value.user.userPhoto.fichier &&
                  value.user.userPhoto.fichier.publicUrl
                }
              />
            );
          },
        },
        { name: 'civilite', label: 'Civilité' },
        { name: 'fullName', label: 'Nom' },
        {
          name: 'user.anneeNaissance',
          label: 'Année de naissance',
          renderer: (value: any) => (value && value.user && value.user.anneeNaissance) || '-',
        },
        {
          name: 'pharmacie.nom',
          label: 'Pharmacie',
          renderer: (value: any) => {
            const goToPharmacie = () => {
              history.push(`/db/${PHARMACIE_URL}/fiche/${value.pharmacie.id}`);
            };
            return (
              (value.pharmacie && (
                <Link
                  color="secondary"
                  variant="body2"
                  onClick={goToPharmacie}
                  style={{ textAlign: 'left', cursor: 'pointer' }}
                >
                  {value.pharmacie.nom}
                </Link>
              )) ||
              '-'
            );
          },
        },
        {
          name: '',
          label: 'Titulaire',
          renderer: (value: any) => {
            const goToTitulaire = (id: string) => () => {
              history.push(`/db/${TITULAIRE_PHARMACIE_URL}/fiche/${id}`);
            };
            const titulaires: any[] = (value.pharmacie && value.pharmacie.titulaires) || [];
            return titulaires.map((t: any, index: number) => {
              const isMultiple: boolean = titulaires.length > 1 && index < titulaires.length - 1;
              if (t) {
                return (
                  <Fragment key={`titulaire_link_${index}`}>
                    <Link
                      color="secondary"
                      component="button"
                      variant="body2"
                      onClick={goToTitulaire(t.id)}
                      style={{ textAlign: 'left', cursor: 'pointer' }}
                    >
                      {t && `${t.fullName}`}
                      {isMultiple && (
                        <>
                          ,<span style={{ visibility: 'hidden' }}>&nbsp;</span>
                        </>
                      )}
                    </Link>
                  </Fragment>
                );
              } else {
                return '-';
              }
            });
          },
        },
        {
          name: 'estAmbassadrice',
          label: 'Ambassadeur',
          renderer: (value: any) => {
            return value.estAmbassadrice === true ? 'Oui' : 'Non';
          },
        },
        {
          name: '',
          label: 'Statut',
          renderer: (value: any) => {
            return <UserStatus user={value.user} pathname={pathname} />;
          },
        },
        {
          name: '',
          label: '',
          renderer: (value: any) => {
            return (
              <TableActionColumn
                row={value}
                baseUrl={PERSONNEL_PHARMACIE_URL}
                setOpenDeleteDialog={setOpenDeleteDialog}
              // handleClickDelete={onClickDelete}
              />
            );
          },
        },
      ];
      /*  end PERSONNEL_PHARMACIE */
      break;
    case 'PRESIDENT_REGION':
      return [
        { name: 'mail', label: 'Email' },
        { name: 'civilite', label: 'Civilité' },
        { name: 'fullName', label: 'Nom' },
        {
          name: 'pharmacie',
          label: 'Pharmacie(s)',
          renderer: (value: any) => {
            const pharmacies: any[] = (value && value.pharmacies) || [];
            const goToPharmacie = (id: string) => () => {
              history.push(`/db/${PHARMACIE_URL}/fiche/${id}`);
            };
            return pharmacies.map((p: any, index: number) => {
              const isMultiple: boolean = pharmacies.length > 1 && index < pharmacies.length - 1;
              if (p) {
                return (
                  <Link
                    key={`pharmacie_link_${index}`}
                    color="secondary"
                    component="button"
                    variant="body2"
                    onClick={goToPharmacie(p.id)}
                    style={{ textAlign: 'left', cursor: 'pointer' }}
                  >
                    {p.nom}
                    {isMultiple && (
                      <>
                        ,<span style={{ visibility: 'hidden' }}>&nbsp;</span>
                      </>
                    )}
                  </Link>
                );
              } else {
                return '-';
              }
            });
          },
        },
        {
          name: 'departement',
          label: 'Département',
          renderer: (value: any) => {
            return value.pharmacies
              ? value.pharmacies.map((data: any) =>
                data ? data.departement ? <span>{data.departement.nom}</span> : null : '',
              )
              : null;
          },
        },
        {
          name: 'cp',
          label: 'Code postal',
          renderer: (value: any) => {
            return value.cp ? value.cp : '-';
          },
        },
        {
          name: 'ville',
          label: 'Ville',
          renderer: (value: any) => {
            return value.ville ? value.ville : '-';
          },
        },
        {
          name: '',
          label: 'Statut',
          renderer: (value: any) => {
            return <UserStatus user={value.user} pathname={pathname} />;
          },
        },
        {
          name: '',
          label: '',
          renderer: (value: any) => {
            return (
              <TableActionColumn
                row={value}
                baseUrl={PRESIDENT_REGION_URL}
                setOpenDeleteDialog={setOpenDeleteDialog}
                withViewDetails={true}
              // handleClickDelete={onClickDelete}
              />
            );
          },
        },
      ];
      /*  end PRESIDENT_REGION */
      break;
    case 'LABORATOIRE':
      return [
        // { name: 'mail', label: 'Email' },
        { name: 'nom', label: 'Nom' },
        /*{
          name: 'role.nom',
          label: 'Rôle',
          renderer: (value: any) => {
            return value.role && value.role.nom ? capitalize(value.role.nom) : '-';
          },
        },
        {
          name: 'user.userName',
          label: 'User',
          renderer: (value: any) => {
            return value.user ? <span>{value.user.userName}</span> : '-';
          },
        },
        {
          name: '',
          label: 'Statut',
          renderer: (value: any) => {
            return <UserStatus user={value.user} pathname={pathname} />;
          },
        },*/
        {
          name: 'laboratoireSuite.email',
          label: 'Email',
          renderer: (value: any) => {
            return value?.laboratoireSuite?.email || '-';
          },
        },
        {
          name: 'laboratoireSuite.codePostal',
          label: 'Code Postal',
          renderer: (value: any) => {
            return value?.laboratoireSuite?.codePostal || '-';
          },
        },
        {
          name: 'laboratoireSuite.ville',
          label: 'Ville',
          renderer: (value: any) => {
            return value?.laboratoireSuite?.ville || '-';
          },
        },
        {
          name: 'laboratoireSuite.adresse',
          label: 'Adresse',
          renderer: (value: any) => {
            return value?.laboratoireSuite?.adresse || '-';
          },
        },
        {
          name: 'laboratoireRattachement.nom',
          label: 'Rattaché à',
          renderer: (value: any) => {
            return value?.laboratoireRattachement
              ? value.laboratoireRattachement.nom
              : '-';
          },
        },

        {
          name: 'laboratoireRattaches.nom',
          label: 'Laboratoires rattachés',
          renderer: (value: any) => {
            return value?.laboratoireRattaches?.length > 0
              ? value.laboratoireRattaches.map(({ nom }) => nom).join(', ')
              : '-';
          },
        },
        {
          name: '',
          label: '',
          renderer: (value: any) => {
            return (
              <TableActionColumn
                row={value}
                baseUrl={LABORATOIRE_URL}
                setOpenDeleteDialog={setOpenDeleteDialog}
                withViewDetails={true}
                handleClickDelete={handleClickDelete}
                setCurrentItem={setCurrentItem}
              />
            );
          },
        },
      ];
      /*  end PARTENAIRE_LABORATOIRE */
      break;
    case 'PARTENAIRE_SERVICE':
      return [
        { name: 'nom', label: 'Nom' },
        { name: 'adresse1', label: 'Adresse' },
        { name: 'cp', label: 'Code postal' },
        { name: 'ville', label: 'Ville' },
        { name: 'pays', label: 'Pays' },
        { name: 'telBureau', label: 'Bureau' },
        { name: 'telMobile', label: 'Mobile' },
        { name: 'mail', label: 'Email' },
        { name: 'site', label: 'Site' },
        { name: 'commentaire', label: 'Commentaire' },
        {
          name: 'user.nom.userName',
          label: 'User',
          renderer: (value: any) => {
            return value.user ? <span>{value.user.userName}</span> : '-';
          },
        },
        {
          name: '',
          label: 'Statut',
          renderer: (value: any) => {
            return <UserStatus user={value.user} pathname={pathname} />;
          },
        },
        {
          name: '',
          label: '',
          renderer: (value: any) => {
            return (
              <TableActionColumn
                row={value}
                baseUrl={PARTENAIRE_SERVICE_URL}
                setOpenDeleteDialog={setOpenDeleteDialog}
              // handleClickDelete={onClickDelete}
              />
            );
          },
        },
      ];
      /*  end PARTENAIRE_SERVICE */
      break;
    default:
      return [];
      break;
  }
};
