import React, { FC } from 'react';
import { Switch, Route, withRouter, RouteComponentProps } from 'react-router-dom';
import ActiveDirectory from '../ActiveDirectory';
import Ftp from '../Ftp';
import HelloIdParameter from '../HelloIdParameter';
import Applications from '../ApplicationManagement/Applications';
import HelloIdGroup from '../ApplicationManagement/HelloidGroup';
import HelloIdApplicationsRole from '../ApplicationManagement/HelloidApplicationsRole';
import ApplicationsRole from '../ApplicationManagement/ApplicationsRole';
import ExternalMapping from '../ApplicationManagement/ExternalMapping';

export interface IEnabledParameters {
  activeDirectoryParam: boolean;
  ftpParam: boolean;
  helloidParam: boolean;
  applicationsParam: boolean;
  isSuperAdmin: boolean;
}

const ParametreSsoRouter: FC<RouteComponentProps & IEnabledParameters> = ({
  activeDirectoryParam,
  ftpParam,
  helloidParam,
  applicationsParam,
  isSuperAdmin,
}) => {
  let routes: any[] = [];
  if (isSuperAdmin) {
    if (activeDirectoryParam) {
      routes = [
        ['/db/sso-parameter', '/db/sso-parameter/active-directory'],
        ['/db/sso-parameter/ftp'],
        ['/db/sso-parameter/helloid-parameter'],
        [
          '/db/sso-parameter/manage-applications',
          '/db/sso-parameter/manage-applications/applications',
        ],
      ];
    } else {
      if (ftpParam) {
        routes = [
          ['/db/sso-parameter/active-directory'],
          ['/db/sso-parameter', '/db/sso-parameter/ftp'],
          ['/db/sso-parameter/helloid-parameter'],
          [
            '/db/sso-parameter/manage-applications',
            '/db/sso-parameter/manage-applications/applications',
          ],
        ];
      } else {
        if (helloidParam) {
          routes = [
            ['/db/sso-parameter/active-directory'],
            ['/db/sso-parameter/ftp'],
            ['/db/sso-parameter', '/db/sso-parameter/helloid-parameter'],
            [
              '/db/sso-parameter/manage-applications',
              '/db/sso-parameter/manage-applications/applications',
            ],
          ];
        } else {
          routes = [
            ['/db/sso-parameter/active-directory'],
            ['/db/sso-parameter/ftp'],
            ['/db/sso-parameter/helloid-parameter'],
            [
              '/db/sso-parameter',
              '/db/sso-parameter/manage-applications',
              '/db/sso-parameter/manage-applications/applications',
            ],
          ];
        }
      }
    }
  } else {
    if (ftpParam) {
      routes = [
        ['/db/sso-parameter/active-directory'],
        ['/db/sso-parameter', '/db/sso-parameter/ftp'],
        ['/db/sso-parameter/helloid-parameter'],
        [
          '/db/sso-parameter/manage-applications',
          '/db/sso-parameter/manage-applications/applications',
        ],
      ];
    } else {
      routes = [
        ['/db/sso-parameter/active-directory'],
        ['/db/sso-parameter/ftp'],
        ['/db/sso-parameter/helloid-parameter'],
        [
          '/db/sso-parameter',
          '/db/sso-parameter/manage-applications',
          '/db/sso-parameter/manage-applications/applications',
        ],
      ];
    }
  }

  return (
    <Switch>
      <Route path={routes[0]} exact={true} component={ActiveDirectory} />
      <Route path={routes[1]} exact={true} component={Ftp} />
      <Route path={routes[2]} exact={true} component={HelloIdParameter} />
      {/* <Route
        path="/db/sso-parameter/manage-applications"
        exact={true}
        component={ApplicationManagement}
      /> */}
      <Route path={routes[3]} exact={true} component={Applications} />
      <Route
        path="/db/sso-parameter/manage-applications/helloid-group"
        exact={true}
        component={HelloIdGroup}
      />
      <Route
        path="/db/sso-parameter/manage-applications/helloid-applications-role"
        exact={true}
        component={HelloIdApplicationsRole}
      />
      <Route
        path="/db/sso-parameter/manage-applications/applications-role"
        exact={true}
        component={ApplicationsRole}
      />
      <Route
        path="/db/sso-parameter/manage-applications/external-mapping"
        exact={true}
        component={ExternalMapping}
      />
    </Switch>
  );
};

export default withRouter(ParametreSsoRouter);
