import { useApolloClient, useMutation } from '@apollo/client';
import { Box, Fade, IconButton, Tooltip, Typography } from '@material-ui/core';
import { Add, Delete, Edit, History } from '@material-ui/icons';
import moment from 'moment';
import React, { FC, MouseEvent, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { DO_DELETE_MARCHE } from '../../../../graphql/Marche';
import { GET_SEARCH_CUSTOM_CONTENT_MARCHE } from '../../../../graphql/Marche/query';
import {
  DELETE_MARCHE,
  DELETE_MARCHEVariables,
} from '../../../../graphql/Marche/types/DELETE_MARCHE';
import { DO_DELETE_PROMOTION } from '../../../../graphql/Promotion';
import { GET_SEARCH_CUSTOM_CONTENT_PROMOTION } from '../../../../graphql/Promotion/query';
import {
  DELETE_PROMOTION,
  DELETE_PROMOTIONVariables,
} from '../../../../graphql/Promotion/types/DELETE_PROMOTION';
import { AppAuthorization } from '../../../../services/authorization';
import { getUser } from '../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop';
import { ConfirmDeleteDialog } from '../../../Common/ConfirmDialog';
import CustomButton from '../../../Common/CustomButton';
import { CustomTableWithSearchColumn } from '../../../Common/CustomTableWithSearch/interface';
import Filter from '../../../Common/Filter';
import CustomContent from '../../../Common/newCustomContent/CustomContent';
import WithSearch from '../../../Common/newWithSearch/withSearch';
import {
  MARCHE_BASE_URL,
  MARCHE_CREATE_FIRST_STEP_URL,
  MARCHE_EDIT_FIRST_STEP_URL,
  PROMO_BASE_URL,
  PROMO_CREATE_FIRST_STEP_URL,
  PROMO_EDIT_FIRST_STEP_URL,
} from '../constant';
import { getColumns } from './column';
import useStyles from './styles';

const MarcheList: FC<RouteComponentProps> = ({ history: { push }, location: { pathname } }) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const user = getUser();
  const auth = new AppAuthorization(user);

  const [openDelete, setOpenDelete] = useState<boolean>(false);
  const [deleteRow, setDeleteRow] = useState<any>(null);

  const isOnMarche: boolean = pathname.startsWith(MARCHE_BASE_URL);
  const isOnPromo: boolean = pathname.startsWith(PROMO_BASE_URL);

  const title = `Liste des ${isOnMarche ? 'marchés' : 'promotions'}`;
  const btnText = `Ajouter ${isOnMarche ? 'un marché' : 'une promotion'}`;

  const searchType = isOnMarche ? 'marche' : 'promotion';

  const searchQuery = isOnMarche
    ? GET_SEARCH_CUSTOM_CONTENT_MARCHE
    : GET_SEARCH_CUSTOM_CONTENT_PROMOTION;

  // const emptyTitle = `Aucun${isOnMarche ? ' Marché' : 'e Promotion'}`;
  // const emptySubTitle = `Vous n'avez pas encore de ${
  //   isOnMarche ? 'marché' : 'promotion'
  // } dans cette rubrique.`;

  const disabledEditIconBtn = (): boolean => {
    if (isOnMarche && !auth.isAuthorizedToEditMarche()) {
      return true;
    }
    if (isOnPromo && !auth.isAuthorizedToEditPromo()) {
      return true;
    }
    return false;
  };

  const disabledDeleteIconBtn = (): boolean => {
    if (isOnMarche && !auth.isAuthorizedToDeleteMarche()) {
      return true;
    }
    if (isOnPromo && !auth.isAuthorizedToDeletePromo()) {
      return true;
    }
    return false;
  };

  const ACTIONS_COLUMNS: any[] = [
    {
      name: '',
      label: '',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        return (
          <>
            <Tooltip
              TransitionComponent={Fade}
              TransitionProps={{ timeout: 600 }}
              title="Historique"
            >
              <IconButton color="primary">
                <History />
              </IconButton>
            </Tooltip>
            <Tooltip
              TransitionComponent={Fade}
              TransitionProps={{ timeout: 600 }}
              title="Supprimer"
            >
              <IconButton
                color="primary"
                onClick={onClickDelete(row)}
                disabled={disabledDeleteIconBtn()}
              >
                <Delete />
              </IconButton>
            </Tooltip>
            <Tooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Modifier">
              <IconButton
                color="secondary"
                onClick={goToEdit(row.id)}
                disabled={disabledEditIconBtn()}
              >
                <Edit />
              </IconButton>
            </Tooltip>
          </>
        );
      },
    },
  ];

  const DATES_COLUMNS: any[] = getColumns('date');

  const MARCHE_COLUMNS: any[] = getColumns('marche');
  const PROMO_COLUMNS: any[] = getColumns('promotion');

  const COLUMNS: CustomTableWithSearchColumn[] = isOnMarche
    ? [...MARCHE_COLUMNS, ...DATES_COLUMNS, ...ACTIONS_COLUMNS]
    : [...PROMO_COLUMNS, ...DATES_COLUMNS, ...ACTIONS_COLUMNS];

  /**
   * Mutation delete Marche
   */
  const [doDeleteMarche, { loading: deleteMarcheLoading }] = useMutation<
    DELETE_MARCHE,
    DELETE_MARCHEVariables
  >(DO_DELETE_MARCHE, {
    onCompleted: data => {
      if (data && data.softDeleteMarche) {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'Marché supprimé avec succès',
          isOpen: true,
        });
        setDeleteRow(null);
      }
    },
    onError: errors => {
      displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  /**
   * Mutation delete Promotion
   */
  const [doDeletePromo, { loading: deletePromoLoading }] = useMutation<
    DELETE_PROMOTION,
    DELETE_PROMOTIONVariables
  >(DO_DELETE_PROMOTION, {
    onCompleted: data => {
      if (data && data.softDeletePromotion) {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'Promotion supprimée avec succès',
          isOpen: true,
        });
        setDeleteRow(null);
      }
    },
    onError: errors => {
      displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  const goToCreate = () => {
    if (isOnMarche) push(MARCHE_CREATE_FIRST_STEP_URL);
    if (isOnPromo) push(PROMO_CREATE_FIRST_STEP_URL);
  };

  const goToEdit = (id: string) => () => {
    if (id) {
      if (isOnMarche) push(`${MARCHE_EDIT_FIRST_STEP_URL}/${id}`);
      if (isOnPromo) push(`${PROMO_EDIT_FIRST_STEP_URL}/${id}`);
    }
  };

  const onClickDelete = (row: any) => (event: MouseEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    setOpenDelete(true);
    setDeleteRow(row);
  };

  const onConfirmDelete = () => {
    setOpenDelete(false);
    if (deleteRow && deleteRow.id) {
      if (isOnMarche) doDeleteMarche({ variables: { id: deleteRow.id } });
      if (isOnPromo) doDeletePromo({ variables: { id: deleteRow.id } });
    }
  };

  const loading = (): boolean => {
    if (deleteMarcheLoading || deletePromoLoading) {
      return true;
    }
    return false;
  };

  const disabledAddBtn = (): boolean => {
    if (isOnMarche && !auth.isAuthorizedToAddMarche()) {
      return true;
    }
    if (isOnPromo && !auth.isAuthorizedToAddPromo()) {
      return true;
    }
    return false;
  };

  const DeleteMsg = () => {
    const type = isOnMarche ? 'le marché' : 'la promotion';
    return (
      <>
        {`Êtes vous sur de vouloir supprimer ${type} `}
        <strong>{deleteRow && deleteRow.nom}</strong> ?
      </>
    );
  };

  const tableProps = {
    isSelectable: false,
    paginationCentered: true,
    columns: COLUMNS,
  };

  return (
    <div className={classes.marcheListRoot}>
      {loading() && <Backdrop value="Suppression en cours..." />}
      <div className={classes.filterContainer}>
        <Filter />
      </div>
      <div className={classes.mainListPage}>
        <div className={classes.marcheListHead}>
          <Typography className={classes.marcheListHeadTitle}>{title}</Typography>
          <CustomButton
            color="secondary"
            startIcon={<Add />}
            onClick={goToCreate}
            disabled={disabledAddBtn()}
          >
            {btnText}
          </CustomButton>
        </div>
        <Box className={classes.marcheListTable}>
          <WithSearch
            type={searchType}
            WrappedComponent={CustomContent}
            searchQuery={searchQuery}
            props={tableProps}
            optionalMust={[{ term: { isRemoved: false } }]}
          />
        </Box>

        <ConfirmDeleteDialog
          open={openDelete}
          setOpen={setOpenDelete}
          onClickConfirm={onConfirmDelete}
          content={<DeleteMsg />}
        />
      </div>
    </div>
  );
};

export default withRouter(MarcheList);
