import BaseInformations from './BaseInformations';
import PromotionArticles from './Articles';
import PromotionLaboratoires from './Laboratoires';
import ClientGroupes from './ClientGroupes';

export { BaseInformations, PromotionArticles, PromotionLaboratoires, ClientGroupes };
