import { Typography } from '@material-ui/core';
import { uniq } from 'lodash';
import React, { FC, useEffect, useState } from 'react';
import { GET_SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE } from '../../../../../../graphql/ProduitCanal/query';
import { CustomTableWithSearchColumn } from '../../../../../Common/CustomTableWithSearch/interface';
import CustomContent from '../../../../../Common/newCustomContent';
import WithSearch from '../../../../../Common/newWithSearch/withSearch';
import { MarcheStepProps, StepTableProps } from '../../../interface';
import BaseInfoResume from '../BaseInformations/BaseInfoResume';
import useStyles from '../styles';

const Articles: FC<MarcheStepProps & StepTableProps> = ({
  state,
  setState,
  selected,
  setSelected,
  canalList,
  // isOnPromo,
  // isOnMarche,
}) => {
  const classes = useStyles({});
  const selectedIds: string[] = selected.map(item => item && item.id);
  const { idsCanalArticle } = state;

  const [data, setData] = useState<any[]>([]);

  // const [selectionValue, setSelectionValue] = useState('all');

  // const canal: any = canalList && canalList.find(i => i.code === state.codeCanal);
  // const canalId: string = canal && canal.id;

  const columns: any[] = [
    {
      name: 'produit.produitCode.code',
      label: 'Code',
      numeric: true,
      disablePadding: true,
      renderer: (value: any) => {
        return (
          (value && value.produit && value.produit.produitCode && value.produit.produitCode.code) ||
          '-'
        );
      },
    },
    {
      name: 'produit.libelle',
      label: 'Libellé',
      numeric: false,
      disablePadding: false,
      renderer: (value: any) => {
        return (value && value.produit && value.produit.libelle) || '-';
      },
    },
    {
      name: 'produit.produitTechReg.laboExploitant.nomLabo',
      label: 'Laboratoire',
      numeric: false,
      disablePadding: false,
      renderer: (value: any) => {
        return (
          (value &&
            value.produit &&
            value.produit.produitTechReg &&
            value.produit.produitTechReg.laboExploitant &&
            value.produit.produitTechReg.laboExploitant.nomLabo) ||
          '-'
        );
      },
    },
    {
      name: 'produit.famille.libelleFamille',
      label: 'Famille',
      numeric: false,
      disablePadding: false,
      renderer: (value: any) => {
        return (
          (value &&
            value.produit &&
            value.produit.famille &&
            value.produit.famille.libelleFamille) ||
          '-'
        );
      },
    },
    {
      name: 'produit.produitTechReg.tva.tauxTva',
      label: 'Taux de TVA',
      numeric: false,
      disablePadding: false,
      renderer: (value: any) => {
        return (
          (value &&
            value.produit &&
            value.produit.produitTechReg &&
            value.produit.produitTechReg.tva &&
            value.produit.produitTechReg.tva.tauxTva) ||
          '-'
        );
      },
    },
    // {
    //   key: 'sousGammeCommercial.libelle',
    //   label: 'Gamme',
    //   renderer: (value: any) => {
    //     return (value.sousGammeCommercial && value.sousGammeCommercial.libelle) || '-';
    //   },
    // },
    {
      name: 'sousGammeCommercial.libelle',
      label: 'Sous Gamme',
      numeric: false,
      disablePadding: false,
      renderer: (value: any) => {
        return (value.sousGammeCommercial && value.sousGammeCommercial.libelle) || '-';
      },
    },
  ];

  /**
   * Set idsCanalArticle
   */

  const tableProps = {
    selected,
    setSelected,
    isSelectable: true,
    paginationCentered: true,
    columns,
  };

  useEffect(() => {
    if (selected.length > 0) {
      const newIdsCanalArticle = uniq([...idsCanalArticle, ...selectedIds]);
      setState(prevState => ({
        ...prevState,
        idsCanalArticle: newIdsCanalArticle,
      }));
    } else {
      setState(prevState => ({ ...prevState, idsCanalArticle: [] }));
    }
  }, [selected, data]);

  return (
    <div className={classes.formStepRoot}>
      <Typography className={classes.tableTitle}>Sélection des articles rattachés</Typography>
      <div className={classes.resumeAndSelection}>
        <BaseInfoResume data={state} />
        {/* <SelectionFilter value={selectionValue} setValue={setSelectionValue} /> */}
      </div>
      <div className={classes.tableSection}>
        <WithSearch
          type="produitcanal"
          props={tableProps}
          WrappedComponent={CustomContent}
          searchQuery={GET_SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE}
          optionalMust={[{ term: { isRemoved: false } }, { term: { isActive: true } }]}
        />
      </div>
    </div>
  );
};

export default Articles;
