import React, { FC } from 'react';
import { DataInterface } from '../../../interface';
import useStyles from './styles';
import { Typography } from '@material-ui/core';
import moment from 'moment';
import { getUser } from '../../../../../../services/LocalStorage';
import { ME_me } from '../../../../../../graphql/Authentication/types/ME';

interface BaseInfoResumeProps {
  data: DataInterface;
}

const BaseInfoResume: FC<BaseInfoResumeProps> = ({ data }) => {
  const classes = useStyles({});
  const { nom, codeCanal, dateDebut, dateFin } = data;
  const canalLibelle =
    codeCanal === 'PFL' ? 'PLATEFORME' : codeCanal === 'DIRECT' ? 'DIRECT LABO' : 'GROSSISTE';
  const user: ME_me = getUser();
  const userName = user && user.userName;
  const formattedData = [
    { label: 'Nom', value: nom },
    { label: 'Créateur', value: userName },
    { label: 'Canal', value: canalLibelle },
    { label: 'Date de début', value: moment(dateDebut).format('DD/MM/YYYY') },
    { label: 'Date de fin', value: moment(dateFin).format('DD/MM/YYYY') },
  ];
  return (
    <div className={classes.resumeRoot}>
      {formattedData.map((item, index) => (
        <Typography key={`base_info_resume_item_${index}`} className={classes.resumeItem}>
          {`${item.label} : `}
          <span>{item.value}</span>
        </Typography>
      ))}
    </div>
  );
};

export default BaseInfoResume;
