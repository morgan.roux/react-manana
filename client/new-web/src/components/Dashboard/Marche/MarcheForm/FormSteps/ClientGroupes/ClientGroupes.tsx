import { Typography } from '@material-ui/core';
import { keyBy, mapValues, uniqBy } from 'lodash';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { GET_SEARCH_CUSTOM_CONTENT_GROUPE_CLIENT } from '../../../../../../graphql/GroupeClient';
import {
  GroupeClienRemisetInput,
  RemiseDetailInput,
} from '../../../../../../types/graphql-global-types';
import CustomButton from '../../../../../Common/CustomButton';
import { CustomFullScreenModal } from '../../../../../Common/CustomModal';
import CustomContent from '../../../../../Common/newCustomContent/CustomContent';
import WithSearch from '../../../../../Common/newWithSearch/withSearch';
import { MarcheStepProps, StepTableProps } from '../../../interface';
import Palier from '../../Palier';
import { PalierState } from '../../Palier/Palier';
import BaseInfoResume from '../BaseInformations/BaseInfoResume';
import useStyles from '../styles';

const ClientGroupes: FC<MarcheStepProps & StepTableProps> = ({
  state,
  setState,
  selected,
  setSelected,
  // data,
  // setData,
}) => {
  const classes = useStyles({});

  const { groupeClients, avecPalier } = state;

  const [selectionValue, setSelectionValue] = useState('all');

  const [openPalier, setOpenPalier] = useState<boolean>(false);

  const [palierState, setPalierState] = useState<PalierState>({});
  const [codeGroupeClient, setCodeGroupeClient] = useState<number>(-1);

  const handleOpenPalier = (row: any) => () => {
    setOpenPalier(true);
    setCodeGroupeClient(row.codeGroupe);
  };

  const columns: any[] = [
    {
      name: 'codeGroupe',
      label: 'Code',
      numeric: true,
      disablePadding: true,
    },
    {
      name: 'nomGroupe',
      label: 'Nom',
      numeric: false,
      disablePadding: false,
    },
    {
      name: 'nomCommercial',
      label: 'Nom commercial',
      numeric: false,
      disablePadding: false,
    },
    {
      name: 'dateValiditeDebut',
      label: 'Date de début de validité',
      numeric: false,
      disablePadding: false,
      renderer: (value: any) => {
        return value ? moment(value).format('DD/MM/YYYY') : '-';
      },
    },
    {
      name: 'dateValiditeFin',
      label: 'Date de fin de validité',
      numeric: false,
      disablePadding: false,
      renderer: (value: any) => {
        return value ? moment(value).format('DD/MM/YYYY') : '-';
      },
    },
    {
      name: '',
      label: '',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        return (
          <CustomButton
            color="primary"
            size="small"
            onClick={handleOpenPalier(row)}
            disabled={avecPalier ? false : true}
          >
            Définir le palier
          </CustomButton>
        );
      },
    },
  ];

  /**
   * Update palier on edit
   */
  useEffect(() => {
    if (groupeClients.length > 0) {
      const paliers: any[] = [];
      groupeClients.map(g => {
        if (g && g.remiseDetails && g.remiseDetails.length > 0) {
          const palier = { code: g.codeGroupeClient, remiseDetails: g.remiseDetails };
          paliers.push(palier);
        }
      });

      if (paliers.length > 0) {
        const values = mapValues(keyBy(paliers, 'code'), p => {
          const rmDetails = p.remiseDetails.map((i: any) => ({ ...i, id: uuidv4() }));
          return { remiseDetails: rmDetails };
        });
        setPalierState(prevState => ({ ...prevState, ...values }));
      }
    }
  }, []);

  /**
   * Set groupes clients
   */
  useEffect(() => {
    const grpClients: GroupeClienRemisetInput[] = [];
    if (selected.length > 0) {
      selected.map(item => {
        let newRemiseDeltails: RemiseDetailInput[] = [];
        if (
          palierState[item.codeGroupe] &&
          palierState[item.codeGroupe].remiseDetails &&
          palierState[item.codeGroupe].remiseDetails.length > 0
        ) {
          newRemiseDeltails = palierState[item.codeGroupe].remiseDetails.map(elem => {
            return {
              quantiteMin: parseInt(elem.quantiteMin as any, 10),
              quantiteMax: parseInt(elem.quantiteMax as any, 10),
              pourcentageRemise: parseInt(elem.pourcentageRemise as any, 10),
              remiseSupplementaire: parseInt(elem.remiseSupplementaire as any, 10),
            };
          });
        }
        const groupeclientRemise: GroupeClienRemisetInput = {
          codeGroupeClient: item.codeGroupe,
          idRemise: null,
          remiseDetails: newRemiseDeltails,
        };
        grpClients.push(groupeclientRemise);
      });
      const newGroupeClients: GroupeClienRemisetInput[] = uniqBy(grpClients, 'codeGroupeClient');
      setState(prevState => ({ ...prevState, groupeClients: newGroupeClients }));
    } else {
      setState(prevState => ({ ...prevState, groupeClients: [] }));
    }
  }, [selected, palierState]);

  const tableProps = {
    selected,
    setSelected,
    isSelectable: true,
    paginationCentered: true,
    columns,
  };
  return (
    <div className={classes.formStepRoot}>
      <Typography className={classes.tableTitle}>Sélection des groupes de clients</Typography>
      <div className={classes.resumeAndSelection}>
        <BaseInfoResume data={state} />
        {/* <SelectionFilter value={selectionValue} setValue={setSelectionValue} /> */}
      </div>
      <div className={classes.tableSection}>
        <WithSearch
          type="groupeclient"
          props={tableProps}
          WrappedComponent={CustomContent}
          searchQuery={GET_SEARCH_CUSTOM_CONTENT_GROUPE_CLIENT}
          optionalMust={[
            { term: { sortie: 0 } },
            // TODO : A confirler { range: { dateValiditeDebut: { lte: moment().format() } } },
            { range: { dateValiditeFin: { gte: moment().format() } } },
          ]}
        />
      </div>
      {/* Modal palier */}
      <CustomFullScreenModal
        open={openPalier}
        setOpen={setOpenPalier}
        title="Définition du palier / marché"
        withBtnsActions={false}
      >
        <Palier
          state={palierState}
          setState={setPalierState}
          codeGroupeClient={codeGroupeClient}
          parentData={state}
        />
      </CustomFullScreenModal>
    </div>
  );
};

export default ClientGroupes;
