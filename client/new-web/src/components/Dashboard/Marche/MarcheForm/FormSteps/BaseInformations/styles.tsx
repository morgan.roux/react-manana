import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    baseInfoRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
      '@media (max-width: 825px)': {
        padding: '0px 20px',
      },
    },
    container: {
      maxWidth: 800,
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      '& > div': {
        margin: '0px 0px 25px',
      },
    },
    inputsContainer: {
      display: 'flex',
      alignItems: 'center',
      '& > div:nth-child(1)': {
        marginRight: 30,
        marginBottom: 0,
      },
    },
    checkboxContainer: {
      width: '50%',
    },
    resumeRoot: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      width: '100%',
      height: 50,
      background: '#F5F6FA 0% 0% no-repeat padding-box',
      borderRight: 4,
      padding: '0px 15px',
      '& > p:not(:nth-last-child(1))': {
        marginRight: 15,
      },
    },
    resumeItem: {
      fontSize: 12,
      fontWeight: 'normal',
      '& > span': {
        fontWeight: '600',
        fontSize: 14,
      },
    },
  }),
);

export default useStyles;
