import React, { ChangeEvent, FC, useEffect } from 'react';
import useStyles from './styles';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import { CustomDatePicker } from '../../../../../Common/CustomDateTimePicker';
import CustomSelect from '../../../../../Common/CustomSelect';
import { MarcheStepProps } from '../../../interface';
import Backdrop from '../../../../../Common/Backdrop';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { CustomCheckbox } from '../../../../../Common/CustomCheckbox';
import {
  MARCHE_TYPES,
  STATUS_LIST,
  PROMOTION_TYPES,
  PROMO_CREATE_FIRST_STEP_URL,
  MARCHE_CREATE_FIRST_STEP_URL,
  MARCHE_EDIT_FIRST_STEP_URL,
  PROMO_EDIT_FIRST_STEP_URL,
} from '../../../constant';

const BaseInformations: FC<MarcheStepProps & RouteComponentProps> = ({
  state,
  setState,
  isOnMarche,
  isOnPromo,
  isOnCreate,
  isOnEdit,
  location: { pathname },
  history: { push },
  match: { params },
  canalList,
}) => {
  const classes = useStyles({});

  const { id: dataId } = params as any;

  const namePlaceholder = isOnMarche ? 'Nom du marché' : 'Nom de la promotion';

  const {
    nom,
    codeCanal,
    dateDebut,
    dateFin,
    marcheType,
    promotionType,
    status,
    avecPalier,
  } = state;

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value, checked } = e.target;
      if (name === 'avecPalier') {
        setState(prevState => ({ ...prevState, [name]: checked }));
      } else {
        setState(prevState => ({ ...prevState, [name]: value }));
      }
    }
  };

  /**
   * Redirect to step 0
   */
  useEffect(() => {
    if (!pathname.includes('/base-infos')) {
      if (isOnCreate && isOnMarche) push(MARCHE_CREATE_FIRST_STEP_URL);
      if (isOnCreate && isOnPromo) push(PROMO_CREATE_FIRST_STEP_URL);
      if (isOnEdit && dataId) {
        if (isOnMarche) push(`${MARCHE_EDIT_FIRST_STEP_URL}/${dataId}`);
        if (isOnPromo) push(`${PROMO_EDIT_FIRST_STEP_URL}/${dataId}`);
      }
    }
  }, []);

  const handleStartDateChange = date => {
    setState(prevState => ({ ...prevState, dateDebut: date }));
  };

  const handleEndDateChange = date => {
    setState(prevState => ({ ...prevState, dateFin: date }));
  };

  // const minDateDebut = isOnCreate ? new Date() : dateDebut || new Date();
  const TYPES_LIST = isOnMarche ? MARCHE_TYPES : PROMOTION_TYPES;
  const typeValue = isOnMarche ? marcheType : promotionType;
  const typeName = isOnMarche ? 'marcheType' : 'promotionType';

  const loading = () => {
    // if (canalLoading) {
    //   return true;
    // }
    return false;
  };

  return (
    <div className={classes.baseInfoRoot}>
      {loading() && <Backdrop />}
      <div className={classes.container}>
        <CustomFormTextField
          label="Nom"
          name="nom"
          value={nom}
          onChange={handleChange}
          required={true}
          placeholder={namePlaceholder}
        />
        <div className={classes.inputsContainer}>
          <CustomDatePicker
            label="Date début"
            onChange={handleStartDateChange}
            name="dateDebut"
            value={dateDebut}
            InputLabelProps={{ shrink: true }}
            required={true}
            // minDate={minDateDebut}
          />
          <CustomDatePicker
            label="Date fin"
            onChange={handleEndDateChange}
            name="dateFin"
            value={dateFin}
            InputLabelProps={{ shrink: true }}
            required={true}
            minDate={dateDebut}
          />
        </div>
        <div className={classes.inputsContainer}>
          <CustomSelect
            label="Type"
            list={TYPES_LIST}
            listId="code"
            index="libelle"
            name={typeName}
            value={typeValue || ''}
            onChange={handleChange}
            required={true}
          />
          <CustomSelect
            label="Canal"
            list={canalList || []}
            listId="code"
            index="libelle"
            name="codeCanal"
            value={codeCanal || ''}
            onChange={handleChange}
            required={true}
          />
        </div>
        {isOnMarche && (
          <div className={classes.inputsContainer}>
            <CustomSelect
              label="Statut"
              list={STATUS_LIST}
              listId="code"
              index="libelle"
              name="status"
              value={status || ''}
              onChange={handleChange}
              required={true}
            />
            <div className={classes.checkboxContainer}>
              <CustomCheckbox
                label="Avec palier"
                name="avecPalier"
                value={avecPalier}
                checked={avecPalier}
                onChange={handleChange}
              />
            </div>
          </div>
        )}
        {isOnPromo && (
          <CustomSelect
            label="Statut"
            list={STATUS_LIST}
            listId="code"
            index="libelle"
            name="status"
            value={status || ''}
            onChange={handleChange}
            required={true}
          />
        )}
      </div>
    </div>
  );
};

export default withRouter(BaseInformations);
