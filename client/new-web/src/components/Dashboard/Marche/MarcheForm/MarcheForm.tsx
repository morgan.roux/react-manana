import React, { FC, useState, useEffect, useMemo } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import useStyles from './styles';
import Stepper, { Step } from '../../../Common/Stepper/Stepper';
import {
  BaseInformations,
  PromotionArticles,
  ClientGroupes,
  PromotionLaboratoires,
} from './FormSteps';
import { MarcheInterface, PromotionInterface } from '../interface';
import {
  MARCHE_BASE_URL,
  PROMO_BASE_URL,
  SECOND_STEP_KEY_URL_ARTI,
  SECOND_STEP_KEY_URL_LABO,
  FIRST_STEP_KEY_URL,
  THIRD_STEP_KEY_URL,
  MARCHE_CREATE_SECOND_STEP_URL_ARTI,
  MARCHE_CREATE_SECOND_STEP_URL_LABO,
  MARCHE_CREATE_THIRD_STEP_URL,
  MARCHE_CREATE_FIRST_STEP_URL,
  PROMO_CREATE_SECOND_STEP_URL_ARTI,
  PROMO_CREATE_THIRD_STEP_URL,
  PROMO_CREATE_FIRST_STEP_URL,
  MARCHE_EDIT_SECOND_STEP_URL_ARTI,
  MARCHE_EDIT_SECOND_STEP_URL_LABO,
  MARCHE_EDIT_THIRD_STEP_URL,
  PROMO_EDIT_SECOND_STEP_URL_ARTI,
  PROMO_EDIT_THIRD_STEP_URL,
  MARCHE_EDIT_FIRST_STEP_URL,
  PROMO_EDIT_FIRST_STEP_URL,
} from '../constant';
import Filter from '../../../Common/newWithSearch/Filter';
import {
  MarcheStatus,
  MarcheType,
  PromotionType,
  PromotioStatus,
  GroupeClienRemisetInput,
  RemiseDetailInput,
  MarcheLaboratoireInput,
} from '../../../../types/graphql-global-types';
import { useMutation, useApolloClient, useLazyQuery, useQuery } from '@apollo/client';
import { DO_CREATE_UPDATE_MARCHE, GET_MARCHE } from '../../../../graphql/Marche';
import {
  CREATE_UPDATE_MARCHE,
  CREATE_UPDATE_MARCHEVariables,
} from '../../../../graphql/Marche/types/CREATE_UPDATE_MARCHE';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop';
import { DO_CREATE_UPDATE_PROMOTION, GET_PROMOTION } from '../../../../graphql/Promotion';
import {
  CREATE_UPDATE_PROMOTION,
  CREATE_UPDATE_PROMOTIONVariables,
} from '../../../../graphql/Promotion/types/CREATE_UPDATE_PROMOTION';
import { MARCHE, MARCHEVariables } from '../../../../graphql/Marche/types/MARCHE';
import { PROMOTION, PROMOTIONVariables } from '../../../../graphql/Promotion/types/PROMOTION';
import { uniq, uniqBy } from 'lodash';
import { MINIM_COMMANDE_CANALS } from '../../../../graphql/CommandeCanal/types/MINIM_COMMANDE_CANALS';
import { GET_MINIM_COMMANDE_CANALS } from '../../../../graphql/CommandeCanal';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { resetSearchFilters } from '../../../Common/withSearch/withSearch';
import {
  GestionClientFilterProps,
  ProduitCanalFilterProps,
  ProduitFilterProps,
} from '../../../Common/newWithSearch/ComponentInitializer';
import ProduitCanalColumns from '../../../Main/Content/Pilotage/Cible/Columns/ProduitCanal';

const MarcheForm: FC<RouteComponentProps> = ({
  history,
  location: { pathname },
  match: { params },
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const { push } = history;

  const { id: dataId } = params as any;

  const isOnMarche: boolean = pathname.startsWith(MARCHE_BASE_URL);
  const isOnPromo: boolean = pathname.startsWith(PROMO_BASE_URL);

  const isOnCreate: boolean = pathname.includes('/create');
  const isOnEdit: boolean = pathname.includes('/edit');

  const isOnFirstStep: boolean = pathname.includes(FIRST_STEP_KEY_URL);
  const isOnSecondStep: boolean =
    pathname.includes(SECOND_STEP_KEY_URL_ARTI) || pathname.includes(SECOND_STEP_KEY_URL_LABO);
  const isOnThirdStep: boolean = pathname.includes(THIRD_STEP_KEY_URL);

  const title: string = isOnMarche
    ? isOnCreate
      ? 'Ajout de marché'
      : 'Modification de marché'
    : isOnPromo
      ? isOnCreate
        ? 'Ajout de promotion'
        : 'Modification de promotion'
      : '';

  const [selectedArticle, setSelectedArticle] = useState<any[]>([]);
  const [selectedLabo, setSelectedLabo] = useState<any[]>([]);
  const [selectedGroupClient, setSelectedGroupClient] = useState<any[]>([]);

  const [articleTableData, setArticleTableData] = useState<any[]>([]);
  const [laboTableData, setLaboTableData] = useState<any[]>([]);
  const [clientTableData, setClientTableData] = useState<any[]>([]);

  const [state, setState] = useState<MarcheInterface & PromotionInterface>({
    id: null,
    nom: '',
    dateDebut: null,
    dateFin: null,
    status: MarcheStatus.ACTIF,
    codeCanal: '',
    marcheType: MarcheType.ARTICLE,
    promotionType: PromotionType.OFFRE_DU_MOIS,
    avecPalier: isOnPromo ? true : false,
    laboratoires: [],
    idsCanalArticle: [],
    groupeClients: [],
  });

  // const checkesdProduitResult = useQuery(GET_CHECKEDS_PRODUIT);

  // const checkedsProduit =
  //   (checkesdProduitResult &&
  //     checkesdProduitResult.data &&
  //     checkesdProduitResult.data.checkedsProduit) ||
  //   [];

  // useEffect(() => {
  //   if (checkedsProduit && checkedsProduit.length) {
  //     setState(prevState => ({
  //       ...prevState,
  //       idsCanalArticle: checkedsProduit.map((produit: any) => produit && produit.id),
  //     }));
  //   }
  // }, [checkedsProduit]);

  const {
    id,
    nom,
    dateDebut,
    dateFin,
    status,
    avecPalier,
    codeCanal,
    marcheType,
    promotionType,
    laboratoires,
    idsCanalArticle,
    groupeClients,
  } = state;

  const isTypeArti = marcheType === MarcheType.ARTICLE;
  const isTypeLabo = marcheType === MarcheType.LABORATOIRE;
  const loadingMsg = isOnCreate ? 'Création en cours...' : 'Modification en cours...';

  /**
   * GET LIST OF CANAL
   */
  const { data: canalList, loading: canalLoading } = useQuery<MINIM_COMMANDE_CANALS>(
    GET_MINIM_COMMANDE_CANALS,
    {
      fetchPolicy: 'cache-first',
      onError: errors => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: errors.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      },
    },
  );

  const steps: Step[] = [
    {
      title: 'Informations de base',
      content: (
        <BaseInformations
          state={state}
          setState={setState}
          isOnMarche={isOnMarche}
          isOnPromo={isOnPromo}
          isOnCreate={isOnCreate}
          isOnEdit={isOnEdit}
          canalList={(canalList && canalList.commandeCanals) || []}
        />
      ),
    },
    {
      title: isTypeLabo
        ? 'Sélection des laboratoires rattachés'
        : 'Sélection des articles rattachés',
      content: isTypeLabo ? (
        <PromotionLaboratoires
          state={state}
          setState={setState}
          isOnMarche={isOnMarche}
          isOnPromo={isOnPromo}
          isOnCreate={isOnCreate}
          isOnEdit={isOnEdit}
          selected={selectedLabo}
          setSelected={setSelectedLabo}
          data={laboTableData}
          setData={setLaboTableData}
        />
      ) : (
        <PromotionArticles
          state={state}
          setState={setState}
          isOnMarche={isOnMarche}
          isOnPromo={isOnPromo}
          isOnCreate={isOnCreate}
          isOnEdit={isOnEdit}
          selected={selectedArticle}
          setSelected={setSelectedArticle}
          data={articleTableData}
          setData={setArticleTableData}
          canalList={(canalList && canalList.commandeCanals) || []}
        />
      ),
    },
    {
      title: 'Sélection des groupes de clients',
      content: (
        <ClientGroupes
          state={state}
          setState={setState}
          isOnMarche={isOnMarche}
          isOnPromo={isOnPromo}
          isOnCreate={isOnCreate}
          isOnEdit={isOnEdit}
          selected={selectedGroupClient}
          setSelected={setSelectedGroupClient}
          data={clientTableData}
          setData={setClientTableData}
        />
      ),
    },
  ];

  const handleGoToList = () => {
    if (isOnMarche) push('/marches');
    if (isOnPromo) push('/promotions');
  };

  const handleClickNext = () => {
    // CREATE
    if (isOnCreate) {
      // Create marche
      if (isOnMarche) {
        if (isOnFirstStep) {
          if (isTypeArti) push(MARCHE_CREATE_SECOND_STEP_URL_ARTI);
          if (isTypeLabo) push(MARCHE_CREATE_SECOND_STEP_URL_LABO);
        }
        if (isOnSecondStep) push(MARCHE_CREATE_THIRD_STEP_URL);
      }

      // Create Promotion
      if (isOnPromo) {
        if (isOnFirstStep) push(PROMO_CREATE_SECOND_STEP_URL_ARTI);
        if (isOnSecondStep) push(PROMO_CREATE_THIRD_STEP_URL);
      }
    }

    // EDIT
    if (isOnEdit && dataId) {
      // Edit marche
      if (isOnMarche) {
        if (isOnFirstStep) {
          if (isTypeArti) push(`${MARCHE_EDIT_SECOND_STEP_URL_ARTI}/${dataId}`);
          if (isTypeLabo) push(`${MARCHE_EDIT_SECOND_STEP_URL_LABO}/${dataId}`);
        }
        if (isOnSecondStep) push(`${MARCHE_EDIT_THIRD_STEP_URL}/${dataId}`);
      }

      // Edit Promotion
      if (isOnPromo) {
        if (isOnFirstStep) push(`${PROMO_EDIT_SECOND_STEP_URL_ARTI}/${dataId}`);
        if (isOnSecondStep) push(`${PROMO_EDIT_THIRD_STEP_URL}/${dataId}`);
      }
    }
  };

  const handleClickPrev = () => {
    // CREATE
    if (isOnCreate) {
      // Create marche
      if (isOnMarche) {
        if (isOnThirdStep) {
          if (isTypeArti) push(MARCHE_CREATE_SECOND_STEP_URL_ARTI);
          if (isTypeLabo) push(MARCHE_CREATE_SECOND_STEP_URL_LABO);
        }

        if (isOnSecondStep) push(MARCHE_CREATE_FIRST_STEP_URL);
      }

      // Create promo
      if (isOnPromo) {
        if (isOnThirdStep) push(PROMO_CREATE_SECOND_STEP_URL_ARTI);
        if (isOnSecondStep) push(PROMO_CREATE_FIRST_STEP_URL);
      }
    }

    // Edit
    if (isOnEdit && dataId) {
      // Edit marche
      if (isOnMarche) {
        if (isOnThirdStep) {
          if (isTypeArti) push(`${MARCHE_EDIT_SECOND_STEP_URL_ARTI}/${dataId}`);
          if (isTypeLabo) push(`${MARCHE_EDIT_SECOND_STEP_URL_LABO}/${dataId}`);
        }

        if (isOnSecondStep) push(`${MARCHE_EDIT_FIRST_STEP_URL}/${dataId}`);
      }

      // Edit promo
      if (isOnPromo) {
        if (isOnThirdStep) push(`${PROMO_EDIT_SECOND_STEP_URL_ARTI}/${dataId}`);
        if (isOnSecondStep) push(`${PROMO_EDIT_FIRST_STEP_URL}/${dataId}`);
      }
    }
  };

  const labos = marcheType === MarcheType.LABORATOIRE ? laboratoires : [];
  const clients = marcheType === MarcheType.ARTICLE ? idsCanalArticle : [];

  /**
   * Mutation create Marche
   */
  const [doCreateMarche, { loading: createMarcheLoading }] = useMutation<
    CREATE_UPDATE_MARCHE,
    CREATE_UPDATE_MARCHEVariables
  >(DO_CREATE_UPDATE_MARCHE, {
    variables: {
      id,
      nom,
      dateDebut,
      dateFin,
      marcheType,
      avecPalier,
      status: status as MarcheStatus,
      codeCanal,
      laboratoires: labos,
      idsCanalArticle: clients,
      groupeClients,
    },
    onCompleted: data => {
      if (data && data.createUpdateMarche) {
        const message = `Marché ${isOnCreate ? 'créé' : 'modifié'} avec succès`;
        displaySnackBar(client, { type: 'SUCCESS', message, isOpen: true });
        resetSearchFilters(client);
        push('/marches');
      }
    },
    onError: errors => {
      displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  /**
   * Mutation create Promotion
   */
  const [doCreatePromo, { loading: createPromoLoading }] = useMutation<
    CREATE_UPDATE_PROMOTION,
    CREATE_UPDATE_PROMOTIONVariables
  >(DO_CREATE_UPDATE_PROMOTION, {
    variables: {
      id,
      nom,
      dateDebut,
      dateFin,
      promotionType,
      status: status as PromotioStatus,
      codeCanal,
      idsCanalArticle,
      groupeClients,
    },
    onCompleted: data => {
      if (data && data.createUpdatePromotion) {
        const message = `Promotion ${isOnCreate ? 'créée' : 'modifiée'} avec succès`;
        displaySnackBar(client, { type: 'SUCCESS', message, isOpen: true });
        push('/promotions');
      }
    },
    onError: errors => {
      displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  /**
   * Get Marche for Edit
   */
  const [getMarche, { data: marcheData, loading: marcheLoading }] = useLazyQuery<
    MARCHE,
    MARCHEVariables
  >(GET_MARCHE, { fetchPolicy: 'cache-and-network' });

  /**
   * Get Promo for Edit
   */
  const [getPromo, { data: promoData, loading: promoLoading }] = useLazyQuery<
    PROMOTION,
    PROMOTIONVariables
  >(GET_PROMOTION, { fetchPolicy: 'cache-and-network' });

  /**
   * Run queries getMarche or getPromo if it's on edit
   */
  useEffect(() => {
    if (isOnEdit && dataId) {
      if (isOnMarche) getMarche({ variables: { id: dataId } });
      if (isOnPromo) getPromo({ variables: { id: dataId } });
    }
  }, []);

  /**
   * Update state it's on edit
   * @param data
   * @param type
   */
  const updateState = (data: any) => {
    const canal: string = (data.commandeCanal && data.commandeCanal.code) || '';
    const defaultStatus = isOnMarche ? MarcheStatus.ACTIF : PromotioStatus.ACTIF;
    const articles: any[] =
      (data.canalArticles && data.canalArticles.map(item => item && item.id)) || [];

    // Format labos
    const labs: MarcheLaboratoireInput[] = [];
    if (data.laboratoires && data.laboratoires.length > 0) {
      data.laboratoires.map(item => {
        if (item && item.laboratoire) {
          const newLab: MarcheLaboratoireInput = {
            idLaboratoire: item.laboratoire.id,
            obligatoire: item.obligatoire,
          };
          labs.push(newLab);
        }
      });
    }

    // Format groupe client
    const groupClients: GroupeClienRemisetInput[] = [];
    if (data.groupeClients && data.groupeClients.length > 0) {
      data.groupeClients.map((item: any) => {
        if (item) {
          const remiseDetails: RemiseDetailInput[] =
            item.remise &&
            item.remise.remiseDetails &&
            item.remise.remiseDetails.map((el: any) => {
              return {
                quantiteMin: el.quantiteMin,
                quantiteMax: el.quantiteMax,
                pourcentageRemise: el.pourcentageRemise,
                remiseSupplementaire: el.remiseSupplementaire,
              };
            });

          const groupCli: GroupeClienRemisetInput = {
            codeGroupeClient: item && item.groupeClient && item.groupeClient.codeGroupe,
            idRemise: item.remise && item.remise.id,
            remiseDetails,
          };

          groupClients.push(groupCli);
        }
      });
      //setSelectedGroupClient(groupClients);
    }

    setState(prevState => ({
      ...prevState,
      id: data.id,
      nom: data.nom || '',
      dateDebut: data.dateDebut,
      dateFin: data.dateFin,
      marcheType: data.marcheType || MarcheType.ARTICLE,
      promotionType: data.promotionType || PromotionType.OFFRE_DU_MOIS,
      codeCanal: canal,
      status: data.status || defaultStatus,
      avecPalier: isOnPromo ? true : data.avecPalier || false,
      idsCanalArticle: articles,
      laboratoires: labs,
      groupeClients: groupClients,
    }));
  };

  console.log('+++++++++++++++++++++++++', isOnPromo);

  /**
   * Set state for edit
   */
  useMemo(() => {
    if (marcheData && marcheData.marche) {
      updateState(marcheData.marche);
    }
    if (promoData && promoData.promotion) {
      updateState(promoData.promotion);
    }
  }, [marcheData, promoData]);

  /**
   * Set default selected if it's edit
   * @param defaultSelected
   * @param dataType
   */

  const updateDefaultSelect = (defaultSelected: any[]) => {
    const newSelected: any[] = [];
    if (isOnEdit) {
      // Groupe Clients
      if (defaultSelected.length > 0 && selectedGroupClient.length < defaultSelected.length) {
        defaultSelected.map(item => {
          if (item && item.groupeClient) {
            newSelected.push(item.groupeClient);
          }
        });
        setSelectedGroupClient(uniqBy(newSelected, 'codeGroupe'));
      }

      // Articles
      if (
        defaultSelected.length > 0 &&
        selectedArticle.length < defaultSelected.length
        // &&
        // !isTypeLabo &&
        // !isOnThirdStep
      ) {
        defaultSelected.map(item => {
          if (item && item.produit) newSelected.push(item);
        });
        setSelectedArticle(uniqBy(newSelected, 'id'));
      }

      // Laboratoires
      if (defaultSelected.length > 0 && selectedLabo.length < defaultSelected.length) {
        defaultSelected.map(item => {
          if (item && item.laboratoire) {
            newSelected.push(item.laboratoire);
          }
        });

        setSelectedLabo(uniqBy(newSelected, 'id'));
      }
    }
  };

  /**
   * Set default select if it's on edit
   */
  useMemo(() => {
    const marcheArticles = marcheData && marcheData.marche && marcheData.marche.canalArticles;
    const marcheLabos = marcheData && marcheData.marche && marcheData.marche.laboratoires;
    const marcheClients = marcheData && marcheData.marche && marcheData.marche.groupeClients;
    const promoArticles = promoData && promoData.promotion && promoData.promotion.canalArticles;
    const promoClients = promoData && promoData.promotion && promoData.promotion.groupeClients;

    // Articles && Labos
    if (isOnSecondStep) {
      // Arcticles
      if (isOnMarche && marcheArticles && isTypeArti) {
        updateDefaultSelect(marcheArticles);
      }
      if (isOnPromo && promoArticles) {
        updateDefaultSelect(promoArticles);
      }
      // Labos
      if (isOnMarche && isTypeLabo && marcheLabos) {
        updateDefaultSelect(marcheLabos);
      }
    }

    // Groupe client
    if (isOnThirdStep) {
      if (isOnMarche && marcheClients) updateDefaultSelect(marcheClients);
      if (isOnPromo && promoClients) updateDefaultSelect(promoClients);
    }
  }, [
    marcheData,
    promoData,
    articleTableData,
    laboTableData,
    clientTableData,
    isOnSecondStep,
    isOnThirdStep,
    isTypeLabo,
    isTypeArti,
  ]);

  const createMarche = () => {
    if (isOnMarche) doCreateMarche();
    if (isOnPromo) doCreatePromo();
  };

  const finalStepBtn = {
    buttonLabel: isOnCreate ? 'Créer' : 'Modifier',
    action: createMarche,
  };

  const disabledNextBtn = (): boolean => {
    if (isOnFirstStep && (!nom || !dateDebut || !dateFin || !marcheType || !codeCanal || !status)) {
      return true;
    }

    // Marche
    if (isOnSecondStep && isOnMarche && laboratoires.length === 0 && idsCanalArticle.length === 0) {
      return true;
    }

    // Promo
    if (isOnSecondStep && isOnPromo && idsCanalArticle.length === 0) {
      return true;
    }

    if (isOnThirdStep && groupeClients.length === 0) {
      return true;
    }
    return false;
  };

  const loading = (): boolean => {
    if (
      createMarcheLoading ||
      createPromoLoading ||
      canalLoading ||
      (isOnEdit && (marcheLoading || promoLoading))
    ) {
      return true;
    }
    return false;
  };

  return (
    <div className={classes.marcheFormRoot}>
      {loading() && <Backdrop value={loadingMsg} />}
      {!isOnFirstStep && !createMarcheLoading && (
        <div className={classes.filterContainer}>
          <Filter
            filterProps={isOnSecondStep ? ProduitCanalFilterProps : GestionClientFilterProps}
          />
        </div>
      )}
      <Stepper
        title={title}
        steps={steps}
        disableNextBtn={disabledNextBtn()}
        backToHome={handleGoToList}
        onClickNext={handleClickNext}
        onClickPrev={handleClickPrev}
        finalStep={finalStepBtn}
        backBtnText="Retour à la liste"
      />
    </div>
  );
};

export default withRouter(MarcheForm);
