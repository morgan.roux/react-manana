import React, { FC, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Column } from '../Content/Interface';
import { useGetColumns } from '../columns';
import { ResetUserPasswordModal } from '../ResetUserPasswordModal';
import CustomContent from '../../Common/CustomContent';
import HistoriquePersonnel from '../HistoriquePersonnel';
import { getGroupement } from '../../../services/LocalStorage';

const PartenairesLaboratoires: FC<RouteComponentProps> = ({ history, location: { pathname } }) => {
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [openHistory, setOpenHistory] = useState<boolean>(false);
  const [userId, setUserId] = useState<string>('');
  const [userEmail, setUserEmail] = useState<string>('');

  const groupement = getGroupement();
  const idGroupement = (groupement && groupement.id) || '';

  const defaultFilterBy = [{ term: { idGroupement } }];
  const [filterBy, setfilterBy] = useState<any[]>(defaultFilterBy);

  const handleModal = (open: boolean, userId: string, email: string) => {
    setOpenModal(open);
    setUserId(userId);
    setUserEmail(email);
  };

  const handleClickHistory = (open: boolean, userId: string) => {
    setOpenHistory(open);
    setUserId(userId);
  };

  const columns: Column[] = useGetColumns(
    'PARTENAIRE_LABORATOIRE',
    history,
    pathname,
    handleModal,
    handleClickHistory,
  );

  return (
    <>
      <CustomContent
        searchPlaceholder="Rechercher un laboratoire"
        type="laboratoire"
        columns={columns}
        sortBy={[{ nom: { order: 'desc' } }]}
        enableGlobalSearch={true}
        inSelectContainer={true}
        filterBy={filterBy}
      />

      <div>
        {/* <ResetUserPasswordModal
          open={openModal}
          setOpen={setOpenModal}
          userId={userId}
          email={userEmail}
        /> */}
      </div>
      <HistoriquePersonnel open={openHistory} setOpen={setOpenHistory} userId={userId} />
    </>
  );
};

export default withRouter(PartenairesLaboratoires);
