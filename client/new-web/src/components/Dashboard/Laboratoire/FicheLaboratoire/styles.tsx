import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      justifyContent: 'center',
      marginTop: '15px',
    },
    large: {
      width: theme.spacing(14),
      height: theme.spacing(14),
    },
    tableau: {
      display: 'flex',
      justifyContent: 'space-between',
      width: '100%',
    },
    h5: {
      textAlign: 'right',
      font: 'Medium 16px/33px Montserrat',
      letterSpacing: 0,
      opacity: 1,
      color: '#1D1D1D',
    },
  }),
);

export default useStyles;
