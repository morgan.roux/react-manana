import { useApolloClient, useMutation } from '@apollo/client';
import { Box, IconButton, Typography } from '@material-ui/core';
import React, { ChangeEvent, FC, Fragment, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import CustomContent from '../../../../Common/newCustomContent';
import SearchInput from '../../../../Common/newCustomContent/SearchInput';
import WithSearch from '../../../../Common/newWithSearch/withSearch';
import { Column } from '../../../Content/Interface';
import useStyles from './styles';
import moment from 'moment';
import { getGroupement, getUser } from '../../../../../services/LocalStorage';
import { ME_me } from '../../../../../graphql/Authentication/types/ME';
import { AWS_HOST } from '../../../../../utils/s3urls';
import Status from '../../../AideEtSupport/Tickets/TicketStatus';
import { Visibility } from '@material-ui/icons';
import { CustomModal } from '../../../../Common/CustomModal';
import TicketDetails from '../../../AideEtSupport/Tickets/TicketDetails';
import { GET_SEARCH_CUSTOM_CONTENT_TICKET } from '../../../../../graphql/Ticket';

interface ReclamationsProps {
  idLaboratoire: string | null;
}

const Reclamations: FC<ReclamationsProps & RouteComponentProps> = ({
  location: { pathname },
  history: { push },
  idLaboratoire,
}) => {
  const classes = useStyles({});

  const groupement = getGroupement();

  const currentUser: ME_me = getUser();
  const currentUserName = (currentUser && currentUser.userName) || '';
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [ticket, setTicket] = useState<any>(null);

  const getOrigine = (origine: string) => {
    switch (origine) {
      case 'INTERNE':
        return 'Interne';
      case 'PATIENT':
        return 'Patient';
      case 'GROUPE_AMIS':
        return `Groupe d'amis`;
      case 'GROUPEMENT':
        return 'Groupement';
      case 'FOURNISSEUR':
        return 'Laboratoire';
      case 'PRESTATAIRE':
        return 'Prestataire';

      default:
        return '';
    }
  };

  const getPriority = (priority: number) => {
    switch (priority) {
      case 1:
        return 'Normal';
      case 2:
        return 'Important';
      case 3:
        return `Bloquant`;

      default:
        return '';
    }
  };

  const viewDetail = (value: any) => () => {
    setTicket(value);
    setOpenModal(true);
  };

  const ticketColumns: Column[] = [
    {
      name: 'dateHeureSaisie',
      label: 'Date de saisie',
      renderer: (value: any) => {
        return value.dateHeureSaisie ? moment(value.dateHeureSaisie).format('DD/MM/YYYY') : '-';
      },
    },
    {
      name: 'dateHeureSaisie',
      label: 'Heure de saisie',
      renderer: (value: any) => {
        return value.dateHeureSaisie ? moment(value.dateHeureSaisie).format('HH:mm') : '-';
      },
    },
    {
      name: 'origineType',
      label: 'Origine',
      renderer: (value: any) => {
        return value.origineType ? getOrigine(value.origineType) : '-';
      },
    },
    {
      name: 'priority',
      label: 'Priorité',
      renderer: (value: any) => {
        return value.priority ? getPriority(value.priority) : '-';
      },
    },
    {
      name: 'declarant.userName',
      label: 'Declarant',
      renderer: (value: any) => {
        return (
          (value.declarant && value.declarant.userName
            ? value.declarant.userName === currentUserName
              ? 'Moi'
              : value.declarant.userName
            : '-') || '-'
        );
      },
    },
    {
      name: '',
      label: 'Concerné(e)',
      renderer: (value: any) => {
        const concernees =
          value.usersConcernees && value.usersConcernees.length
            ? value.usersConcernees.map(user => user.userName).join(', ')
            : '';
        return concernees;
      },
    },
    {
      name: 'smyley.nom',
      label: 'Smyley',
      renderer: (value: any) => {
        if (value.smyley && value.smyley.photo) {
          return (
            <img
              src={`${AWS_HOST}/${value.smyley.photo}`}
              alt={value.smyley.nom}
              title={value.smyley.nom}
            />
          );
        } else {
          return '-';
        }
      },
    },
    {
      name: 'statusTicket',
      label: 'Statut',
      renderer: (value: any) => {
        return <Status id={value && value.id} currentStatus={value && value.statusTicket} />;
      },
    },
    {
      name: '',
      label: '-',
      renderer: (value: any) => {
        return (
          <IconButton onClick={viewDetail(value)}>
            <Visibility />
          </IconButton>
        );
      },
    },
  ];

  const tableProps = {
    isSelectable: false,
    paginationCentered: true,
    columns: ticketColumns,
  };

  return (
    <Box className={classes.tableContainer}>
      <Box width="100%">
        <Typography className={classes.title}>Liste des ressources</Typography>
        <Box width="100%">
          <Box className={classes.searchInputBox}>
            <SearchInput searchPlaceholder="Rechercher une réclamation..." />
          </Box>
          <WithSearch
            type="ticket"
            props={tableProps}
            WrappedComponent={CustomContent}
            searchQuery={GET_SEARCH_CUSTOM_CONTENT_TICKET}
            optionalMust={[
              { term: { 'groupement.id': (groupement && groupement.id) || '' } },
              { term: { typeTicket: 'RECLAMATION' } },
              { term: { origineType: 'FOURNISSEUR' } },
              { term: { idOrganisation: idLaboratoire } },
            ]}
          />
        </Box>
        <CustomModal
          open={openModal}
          setOpen={setOpenModal}
          /*  maxWidth={'md'} */
          title={'Details Réclamation'}
          withBtnsActions={false}
          closeIcon={true}
          headerWithBgColor={true}
          fullWidth={true}
        >
          <TicketDetails ticket={ticket} isOnAppel={false} />
        </CustomModal>
      </Box>
    </Box>
  );
};
export default withRouter(Reclamations);
