import { useLazyQuery } from '@apollo/client';
import { Box, Typography } from '@material-ui/core';
import React, { FC, Fragment, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { GET_SEARCH_COMMENT } from '../../../../../graphql/Comment/query';
import {
  SEARCH_COMMENT,
  SEARCH_COMMENTVariables,
} from '../../../../../graphql/Comment/types/SEARCH_COMMENT';
import { Comment } from '../../../../Comment';
import CommentList from './List';
import useStyles from './styles';
interface ReclamationsProps {
  idLaboratoire: string | null;
}

const Comments: FC<ReclamationsProps & RouteComponentProps> = ({ idLaboratoire }) => {
  const classes = useStyles({});

  const [take, setTake] = useState<number>(5);
  const skip = 0;

  const query = {
    query: {
      bool: {
        must: [
          {
            term: { idItemAssocie: idLaboratoire },
          },
          {
            term: { 'item.code': 'LABO' },
          },
          {
            term: { isRemoved: false },
          },
        ],
      },
    },
    sort: [{ dateModification: { order: 'desc' } }],
  };

  const [getCommets, { loading, data: result, refetch }] = useLazyQuery<
    SEARCH_COMMENT,
    SEARCH_COMMENTVariables
  >(GET_SEARCH_COMMENT);

  React.useEffect(() => {
    getCommets({
      variables: {
        skip,
        take,
        query,
      },
    });
  }, []);

  console.log('8989898989', result);

  const comments =
    result && result.search && result.search.data && result.search.data.length
      ? result.search.data
      : [];

  const fetchMoreComments = () => {
    getCommets({
      variables: {
        skip,
        take: 2 * take,
        query,
      },
    });
    setTake(2 * take);
  };

  const fetchAfterCreate = () => {
    getCommets({
      variables: {
        skip,
        take: take + 1,
        query,
      },
    });
    setTake(take + 1);
  };

  return (
    <Box className={classes.tableContainer}>
      <Box width="100%">
        <Typography className={classes.title}>Liste des commentaires</Typography>
        <Box width="100%">
          <Comment codeItem="LABO" idSource={idLaboratoire || ''} refetch={fetchAfterCreate} />
          <CommentList
            total={(result && result.search && result.search.total) || 0}
            comments={comments}
            loading={loading}
            fetchMoreComments={fetchMoreComments}
          />
        </Box>
      </Box>
    </Box>
  );
};
export default withRouter(Comments);
