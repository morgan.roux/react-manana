import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    tableContainer: {
      width: '100%',
      maxWidth: '1300px',
      margin: '8px auto',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize: 18,
      fontFamily: 'Montserrat',
      letterSpacing: 0,
      color: theme.palette.secondary.main,
      opacity: 1,
    },
    sectionContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      marginBottom: 5,
      width: '18%',
      '& .MuiPaper-root, & .MuiPaper-elevation1': {
        boxShadow: 'none!important',
      },
      '& > div': {
        borderRadius: 12,
        padding: '8px 20px 20px',
        border: '1px solid #DCDCDC',
        boxShadow: 'none!important',
        width: '100%',
        minHeight: '89%',
      },
      '@media (max-width: 1300px)': {
        width: '19%',
      },
      '@media (max-width:1200px)': {
        width: '24%',
        marginBottom: 24,
      },
      '@media (max-width:1024px)': {
        width: '31%',
      },
      '@media (max-width:756px)': {
        width: '48%',
      },
      '@media (max-width:578px)': {
        width: '98%',
      },
    },

    inputTitle: {
      fontWeight: 'bold',
      fontSize: 18,
      color: theme.palette.secondary.main,
      marginBottom: 5,
    },
  }),
);

export default useStyles;
