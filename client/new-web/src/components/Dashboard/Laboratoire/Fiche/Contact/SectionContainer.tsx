import * as React from 'react';
import { Typography } from '@material-ui/core';
import useStyles from './styles';

interface IInfoPersoFormProps {
  title?: string;
  children?: any;
}

const SectionContainer: React.FC<IInfoPersoFormProps> = props => {
  const { title, children } = props;
  const classes = useStyles({});

  return (
    <div className={classes.sectionContainer}>
      <Typography className={classes.inputTitle}>{title}</Typography>
      <div>{children}</div>
    </div>
  );
};

export default SectionContainer;
