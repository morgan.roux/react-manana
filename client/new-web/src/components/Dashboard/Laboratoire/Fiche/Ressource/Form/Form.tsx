import { useQuery } from '@apollo/client';
import { FormControlLabel, Radio, RadioGroup, Typography } from '@material-ui/core/';
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { GET_ALL_ITEMS } from '../../../../../../graphql/Item';
import CustomAutocomplete from '../../../../../Common/CustomAutocomplete';
import { CustomDatePicker } from '../../../../../Common/CustomDateTimePicker';
import { CustomModal } from '../../../../../Common/CustomModal';
import CustomSelect from '../../../../../Common/CustomSelect';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import Dropzone from '../../../../../Common/Dropzone';
import FormContainer from '../../../../../Common/FormContainer';
import InputRow from '../../../../../Common/InputRow';
import useStyles from './styles';
interface FormProps {
  handleChange: (e: ChangeEvent<any>) => any;
  handleChangeDate: (date: string) => any;
  handleChangeAutocomplete: (value: any) => void;
  values: any;
  setValues: (values: any) => void;
  open: boolean;
  setOpen: (value: any) => void;
  onClickConfirm: () => void;
}

const RessourceForm: FC<FormProps & RouteComponentProps> = ({
  location: { pathname },
  handleChange,
  handleChangeDate,
  handleChangeAutocomplete,
  values,
  setValues,
  open,
  setOpen,
  onClickConfirm,
}) => {
  const classes = useStyles({});
  const [documentType, setDocumentType] = useState<'FILE' | 'URL'>('FILE');
  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);

  const handlChangeRadioGroup = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDocumentType((event.target as HTMLInputElement).value as any);
    setSelectedFiles([]);
  };

  //list fictive
  const resourceTypelist = [
    { label: 'Ressource type A', value: 'A' },
    { label: 'Ressource type B', value: 'B' },
  ];

  const itemsQuery = useQuery(GET_ALL_ITEMS);

  useEffect(() => {
    if (selectedFiles && selectedFiles.length > 0) {
      setValues(prevState => ({ ...prevState, photo: selectedFiles[0] }));
    }
  }, [selectedFiles]);

  console.log('selectedFiles : ', selectedFiles);

  const items = itemsQuery && itemsQuery.data && itemsQuery.data.items;

  const { code, libelle, item, dateChargement, typeRessource, url } = values;

  return (
    <CustomModal
      onClick={event => event.stopPropagation()}
      title={`Formulaire Ressource`}
      open={open}
      setOpen={setOpen}
      withBtnsActions={true}
      closeIcon={true}
      headerWithBgColor={true}
      fullWidth={true}
      actionButton={'Enregistrer'}
      onClickConfirm={onClickConfirm}
    >
      <div className={classes.pharmacieFormRoot}>
        <div className={classes.formContainer}>
          <FormContainer title=" ">
            <InputRow title="Type">
              <CustomSelect
                list={resourceTypelist}
                listId="value"
                index="label"
                name="typeRessource"
                value={typeRessource}
                onChange={handleChange}
              />
            </InputRow>
            <InputRow title="Item">
              <CustomAutocomplete
                id="itemId"
                options={items || []}
                optionLabelKey="name"
                value={item}
                onAutocompleteChange={handleChangeAutocomplete}
                label="Item de la ressource"
                required={true}
              />
            </InputRow>
            <InputRow title="Code">
              <CustomFormTextField
                name="code"
                value={code}
                onChange={handleChange}
                placeholder="Code de la ressource"
                type="text"
              />
            </InputRow>
            <InputRow title="Libellé">
              <CustomFormTextField
                name="libelle"
                value={libelle}
                onChange={handleChange}
                placeholder="Libellé de la ressource"
              />
            </InputRow>
            <InputRow title="Date de chargement">
              <CustomDatePicker
                placeholder=""
                name="dateChargement"
                value={dateChargement}
                onChange={handleChangeDate('dateChargement')}
              />
            </InputRow>
          </FormContainer>
          <RadioGroup
            row={true}
            aria-label="documentType"
            name="documentType"
            value={documentType}
            onChange={handlChangeRadioGroup}
            className={classes.radioGroup}
          >
            <FormControlLabel
              value="FILE"
              control={<Radio color="primary" />}
              label="Fichier de la ressource digital"
            />
            <Typography>ou</Typography>
            <FormControlLabel
              value="URL"
              control={<Radio color="primary" />}
              label="URL de la ressource digital"
            />
          </RadioGroup>
          {documentType === 'FILE' ? (
            <Dropzone
              multiple={false}
              setSelectedFiles={setSelectedFiles}
              selectedFiles={selectedFiles}
            />
          ) : (
            <CustomFormTextField
              name="url"
              value={url}
              onChange={handleChange}
              type="text"
              fullWidth={true}
            />
          )}
        </div>
      </div>
    </CustomModal>
  );
};
export default withRouter(RessourceForm);
