import { useMutation, useApolloClient } from '@apollo/client';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import {
  CREATE_UPDATE_LABORATOIRE_RESSOURCE,
  CREATE_UPDATE_LABORATOIRE_RESSOURCEVariables,
} from '../../../../../graphql/LaboratoireRessource/types/CREATE_UPDATE_LABORATOIRE_RESSOURCE';
import { DO_CREATE_UPDATE_LABORATOIRE_RESSOURCE } from '../../../../../graphql/LaboratoireRessource/mutation';

export const useCreateUpdateRessource = (
  values: CREATE_UPDATE_LABORATOIRE_RESSOURCEVariables,
  operationName: any,
  variables: any,
  isOnEdit: boolean,
): [any, boolean] => {
  const client = useApolloClient();

  const [createUpdateRessource, { loading }] = useMutation<
    CREATE_UPDATE_LABORATOIRE_RESSOURCE,
    CREATE_UPDATE_LABORATOIRE_RESSOURCEVariables
  >(DO_CREATE_UPDATE_LABORATOIRE_RESSOURCE, {
    update: (cache, { data }) => {
      if (data && data.createUpdateLaboratoireRessource && values) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables: variables,
          });
          if (req && req.search && req.search.data && !isOnEdit) {
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    total: req.search.total + 1,
                    data: [...req.search.data, data.createUpdateLaboratoireRessource],
                  },
                },
              },
              variables: variables,
            });
          }
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
      displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
    },
    onCompleted: data => {
      if (data && data.createUpdateLaboratoireRessource) {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: `Ressource ${isOnEdit ? 'modifiée' : 'crée'} avec succès`,
          isOpen: true,
        });
      }
    },
  });

  return [createUpdateRessource, loading];
};
