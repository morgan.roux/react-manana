import { useApolloClient, useMutation } from '@apollo/client';
import { Add } from '@material-ui/icons';
import React, { useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { ContentContext, ContentStateInterface } from '../../../AppContext';
import mutationSuccessImg from '../../../assets/img/mutation-success.png';
// import PharmacieForm from './PharmacieForm/PharmacieForm';
import { LABORATOIRE_URL } from '../../../Constant/url';
import { SET_SORTIE_LABORATOIRE } from '../../../graphql/Laboratoire/mutation';
import {
  setLaboratoireSortie,
  setLaboratoireSortieVariables,
} from '../../../graphql/Laboratoire/types/setLaboratoireSortie';
// import usePharmacie from './PharmacieForm/usePharmacieForm';
import { getGroupement } from '../../../services/LocalStorage';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import { ConfirmDeleteDialog } from '../../Common/ConfirmDialog';
import CustomButton from '../../Common/CustomButton';
import CustomContent from '../../Common/newCustomContent';
import SearchInput from '../../Common/newCustomContent/SearchInput';
import SubToolbar from '../../Common/newCustomContent/SubToolbar';
import NoItemContentImage from '../../Common/NoItemContentImage';
import { useGetColumns } from '../columns';
import { Column } from '../Content/Interface';
import useStyles from './styles';

interface LaboratoireProps {
  listResult: any;
}

const Laboratoire: React.FC<LaboratoireProps & RouteComponentProps> = props => {
  const {
    listResult,
    history,
    history: { push },
    location: { pathname },
    match: { params },
  } = props;
  const classes = useStyles();
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [openHistory, setOpenHistory] = useState<boolean>(false);
  const [userId, setUserId] = useState<string>('');
  const [userEmail, setUserEmail] = useState<string>('');
  const [laboToDelete, setLaboToDelete] = useState<any>(null);
  const {
    content: { variables, operationName, refetch },
  } = useContext<ContentStateInterface>(ContentContext);
  const client = useApolloClient();

  const [saveLaboratoireSortie] = useMutation<setLaboratoireSortie, setLaboratoireSortieVariables>(
    SET_SORTIE_LABORATOIRE,
    {
      onCompleted: data => {
        if (data && data.updateLaboratoireSortie) {
          if (laboToDelete) setLaboToDelete(null);
          displaySnackBar(client, {
            type: 'SUCCESS',
            message: 'Laboratoire(s) supprimé(s) avec succès',
            isOpen: true,
          });
          if (refetch) refetch();
          setOpenDeleteDialog(false);
        }
      },
      onError: errors => {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
        });
      },
    },
  );

  const handleModal = (open: boolean, userId: string, email: string) => {
    setOpenModal(open);
    setUserId(userId);
    setUserEmail(email);
  };

  const handleClickHistory = (open: boolean, userId: string) => {
    setOpenHistory(open);
    setUserId(userId);
  };

  const onClickDelete = (row: any) => {
    setOpenDeleteDialog(true);
    setLaboToDelete(row);
  };

  const onClickConfirmDelete = () => {
    if (laboToDelete) saveLaboratoireSortie({ variables: { id: laboToDelete.id, sortie: 1 } });
  };

  const columns: Column[] = useGetColumns(
    'LABORATOIRE',
    history,
    pathname,
    handleModal,
    handleClickHistory,
    onClickDelete,
  );

  /* const isOnCreate: boolean = pathname.startsWith(`/db/${LABORATOIRE_URL}/create`) ? true : false;
  const isOnEdit: boolean = pathname.startsWith(`/db/${LABORATOIRE_URL}/edit`); */

  /* const { personnelId } = params as any;
  const EDIT_URL = `/db/${LABORATOIRE_URL}/edit/${personnelId}`; */

  const listLaboratoire = <CustomContent {...{ listResult, columns }} isSelectable={false} />;

  const goToList = () => push(`/db/${LABORATOIRE_URL}`);
  const goToAdd = () => push(`/db/${LABORATOIRE_URL}/create`);

  const DeleteDialogContent = () => {
    if (laboToDelete) {
      return (
        <span>
          Êtes-vous sur de vouloir supprimer
          <span> {laboToDelete.nomLabo || ''}? </span>
        </span>
      );
    }
    return <span>Êtes-vous sur de vouloir supprimer ces laboratoires ?</span>;
  };

  return (
    <div className={classes.container}>
      <SubToolbar title={'Liste des laboratoire'} withBackBtn={false} onClickBack={goToList}>
        <CustomButton color="secondary" startIcon={<Add />} onClick={goToAdd}>
          Ajouter un laboratoire
        </CustomButton>
      </SubToolbar>
      <div className={classes.searchInputBox}>
        <SearchInput searchPlaceholder="Rechercher un laboratoire" />
      </div>
      {listLaboratoire}
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
    </div>
  );
};

export default withRouter(Laboratoire);
