import {
  Fade,
  IconButton,
  ListItemIcon,
  Menu,
  MenuItem,
  SvgIcon,
  Typography,
} from '@material-ui/core';
import { SvgIconProps } from '@material-ui/core/SvgIcon';
import {
  Delete,
  Edit,
  FormatListBulleted,
  History,
  Mail,
  MoreHoriz,
  PersonAdd,
  SyncAlt,
  Visibility,
  VpnKey,
  Launch,
  ImportExport,
} from '@material-ui/icons';
import { useMutation, useApolloClient } from '@apollo/client';
import React, { FC, MouseEvent, ReactNode, useCallback, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import {
  ADMINISTRATEUR_GROUPEMENT,
  SUPER_ADMINISTRATEUR,
  TITULAIRE_PHARMACIE,
} from '../../Constant/roles';
import {
  COLORS_URL,
  GROUPES_CLIENTS_URL,
  LABORATOIRE_URL,
  PARTAGE_IDEE_URL,
  PARTENAIRE_LABORATOIRE_URL,
  PARTENAIRE_SERVICE_URL,
  PERSONNEL_GROUPEMENT_URL,
  PERSONNEL_PHARMACIE_URL,
  PHARMACIE_URL,
  PRESIDENT_REGION_URL,
  TITULAIRE_PHARMACIE_URL,
} from '../../Constant/url';
import { AppAuthorization } from '../../services/authorization';
import { getUser } from '../../services/LocalStorage';
import UsersModal from '../Common/UsersModal';
import { MenuItemResendEmail } from './Content/ButtonResendEmail';
import HistoriquePersonnel from './HistoriquePersonnel';
import { ResetUserPasswordModal } from './ResetUserPasswordModal';
import { FEDERATION_CLIENT } from './../Dashboard/DemarcheQualite/apolloClientFederation';
import {
  GENERATE_FONCTIONS as GENERATE_FONCTIONS_TYPE,
  GENERATE_FONCTIONSVariables,
} from './../../federation/demarche-qualite/matrice-tache/types/GENERATE_FONCTIONS';
import { GENERATE_FONCTIONS } from './../../federation/demarche-qualite/matrice-tache/mutation';
import { displaySnackBar } from '../../utils/snackBarUtils';
import useExportMatriceTache from './DemarcheQualite/MatriceTache/ExportMatriceTache/useExportMatriceTache';

export interface TableActionColumnMenu {
  label: any;
  icon: ReactNode;
  disabled: boolean;
  onClick: (event: any, row: any) => void;
}
export interface TableActionColumnProps {
  row: any;
  baseUrl: string;
  setOpenDeleteDialog?: React.Dispatch<React.SetStateAction<boolean>>;
  withViewDetails?: boolean;
  showResendEmail?: boolean;
  customMenuItems?: TableActionColumnMenu[];
  handleClickDelete?: (row: any) => void;
  handleClickAffectation?: (action: any, row: any) => void;
  setCurrentItem?: (item: any) => void;
}

export const ArretSansRemplacementIcon = (props: SvgIconProps) => {
  return (
    <SvgIcon {...props} viewBox="0 0 24 24">
      <g id="delete-24px" width="24" height="24" viewBox="0 0 24 24">
        <path
          id="Tracé_2876"
          data-name="Tracé 2876"
          d="M206.063,71.652h0a.968.968,0,0,1,.978.944.289.289,0,0,0,.292.282h7.4a.288.288,0,0,0,.293-.28V62.641a.289.289,0,0,0-.292-.281h-7.4a.289.289,0,0,0-.293.281.981.981,0,0,1-1.961,0,2.216,2.216,0,0,1,2.253-2.175h7.4a2.217,2.217,0,0,1,2.245,2.17V72.6a2.214,2.214,0,0,1-2.247,2.17h-7.4a2.216,2.216,0,0,1-2.253-2.17A.968.968,0,0,1,206.063,71.652Z"
          transform="translate(-200.243 -60.467)"
          fill="#424242"
        />
        <path
          id="Tracé_2877"
          data-name="Tracé 2877"
          d="M220.179,66.837l-3.032-3.479h0a.816.816,0,0,0-.6-.291h-.013a.8.8,0,0,0-.606.288,1.106,1.106,0,0,0,0,1.419l1.64,1.854h-8.092a1.005,1.005,0,0,0,0,1.991h8.119l-1.606,1.852A1.125,1.125,0,0,0,216,71.895a.832.832,0,0,0,.594.271h.013a.8.8,0,0,0,.606-.291L220.1,68.56A1.354,1.354,0,0,0,220.179,66.837Z"
          transform="translate(-208.604 -60.467)"
          fill="#424242"
        />
      </g>
    </SvgIcon>
  );
};

export const AttributionDepartementIcon = (props: SvgIconProps) => {
  const { color } = props;
  return (
    <SvgIcon {...props} viewBox="0 0 24 24">
      <g width="18.68" height="17" viewBox="0 0 18.68 17">
        <g id="Groupe_13167" data-name="Groupe 13167" transform="translate(-177.231 -57.446)">
          <path
            id="Icon_material-person-pin-circle"
            data-name="Icon material-person-pin-circle"
            d="M183.881,57.446a6.657,6.657,0,0,0-6.65,6.65c0,4.987,6.65,10.35,6.65,10.35s6.65-5.363,6.65-10.35A6.657,6.657,0,0,0,183.881,57.446Zm0,1.9a1.9,1.9,0,1,1-1.9,1.9h0A1.908,1.908,0,0,1,183.881,59.346Zm0,9.5a4.541,4.541,0,0,1-3.8-2.043c.019-1.257,2.537-1.947,3.8-1.947s3.781.694,3.8,1.948A4.541,4.541,0,0,1,183.881,68.846Z"
            fill={color ? color : '#424242'}
          />
          <path
            id="Tracé_2878"
            data-name="Tracé 2878"
            d="M195.437,59.5h-1.581V57.92a.473.473,0,0,0-.473-.474h-.645a.474.474,0,0,0-.473.474V59.5h-1.581a.473.473,0,0,0-.473.473v.645a.473.473,0,0,0,.473.473h1.581v1.581a.473.473,0,0,0,.473.473h.645a.472.472,0,0,0,.473-.473V61.092h1.581a.474.474,0,0,0,.474-.473v-.645A.473.473,0,0,0,195.437,59.5Z"
            fill={color ? color : '#424242'}
          />
        </g>
      </g>
    </SvgIcon>
  );
};

const TableActionColumn: FC<TableActionColumnProps & RouteComponentProps> = ({
  row,
  baseUrl,
  setOpenDeleteDialog,
  handleClickDelete,
  handleClickAffectation,
  withViewDetails,
  history: { push },
  showResendEmail = true,
  customMenuItems,
  setCurrentItem,
}) => {
  const client = useApolloClient();
  const currentUser = getUser();
  const [openHistory, setOpenHistory] = useState<boolean>(false);
  const [openInitPwd, setOpenInitPwd] = useState<boolean>(false);

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const [openUsersModal, setOpenUsersModal] = useState<boolean>(false);

  const rolesPermitReset = [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT];

  const isTitulaire: boolean =
    baseUrl === TITULAIRE_PHARMACIE_URL || baseUrl.includes(TITULAIRE_PHARMACIE_URL);
  const isPresident: boolean =
    baseUrl === PRESIDENT_REGION_URL || baseUrl.includes(PRESIDENT_REGION_URL);
  const isPersonnelGroupement: boolean =
    baseUrl === PERSONNEL_GROUPEMENT_URL || baseUrl.includes(PERSONNEL_GROUPEMENT_URL);
  const isPersonnelPharamcie: boolean = baseUrl === PERSONNEL_PHARMACIE_URL;
  const isPartenaireService: boolean = baseUrl === PARTENAIRE_SERVICE_URL;
  const isLaboratoire: boolean = baseUrl === LABORATOIRE_URL;
  const isLaboratoirePartenaire: boolean = baseUrl === PARTENAIRE_LABORATOIRE_URL;

  const isPharmacie: boolean = baseUrl === PHARMACIE_URL;
  const isGroupeClient: boolean = baseUrl === GROUPES_CLIENTS_URL;
  const isPersonnelAffecation: boolean = baseUrl === `${PERSONNEL_GROUPEMENT_URL}\affectation`;
  const isPresidentAffecation: boolean = baseUrl === `${PRESIDENT_REGION_URL}\affectation`;
  const auth = new AppAuthorization(currentUser);
  const isAffectation = isPersonnelAffecation || isPresidentAffecation;
  const isColor: boolean = baseUrl.includes(COLORS_URL);
  const isPartageIdeeBonnePratique: boolean = baseUrl === `${PARTAGE_IDEE_URL}`;
  const rowUser =
    row.user ||
    (row.users && row.pharmacies && row.pharmacies.length === 1 && row.users.length === 1
      ? row.users[0]
      : null);

  // FIXME : Move in another component
  const [generateFonctions, generatingFonctions] = useMutation<
    GENERATE_FONCTIONS_TYPE,
    GENERATE_FONCTIONSVariables
  >(GENERATE_FONCTIONS, {
    client: FEDERATION_CLIENT,
  });

  const exportingMatriceTache = useExportMatriceTache();

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    event.preventDefault();
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const onClickDelete = (event: MouseEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    handleClose();
    if (setOpenDeleteDialog) setOpenDeleteDialog(true);
    if (handleClickDelete) {
      handleClickDelete(row);
    }
  };

  const goToEdit = () => {
    handleClose();
    if (setCurrentItem) setCurrentItem(row);
    if (isPresident) {
      push(`/db/${TITULAIRE_PHARMACIE_URL}/edit/${row.id}`);
    } else {
      push(`/db/${baseUrl}/edit/${row.id}`);
    }
  };

  const handleClickHistorique = () => {
    handleClose();
    setOpenHistory(true);
  };

  const goToCreateAccount = () => {
    push(`/db/${baseUrl}/user/create/${row.id}`);
  };

  const goToEditAccount = () => {
    if ((isTitulaire || isPresident) && row.pharmacies && row.pharmacies.length > 1) {
      handleClose();
      setOpenUsersModal(true);
    } else {
      if (isTitulaire || isPresident) {
        if (rowUser) {
          push(`/db/${baseUrl}/user/edit/${row.id}/${rowUser.id}`);
        }
      } else {
        push(`/db/${baseUrl}/user/edit/${row.id}/${rowUser && rowUser.id}`);
      }
    }
  };

  const handleClickInitPwd = useCallback(() => {
    handleClose();
    setOpenInitPwd(true);
  }, []);

  const handleDeployPharmacie = (event: any, row: any) => {
    event.stopPropagation();

    generateFonctions({
      variables: {
        idPharmacie: row.id,
      },
    })
      .then(() => {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: `La pharmacie a été deployée avec succès !`,
          isOpen: true,
        });
      })
      .catch(() => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: `Une erreur s'est produite lors de deploiment de la pharmacie`,
          isOpen: true,
        });
      });
  };

  const handleExportMatricePharmacie = (format: 'pdf' | 'xlsx', event: any, row: any) => {
    event.stopPropagation();
    exportingMatriceTache.launch(row, format);
  };

  const disabledDelete = (): boolean => {
    if (isPersonnelGroupement) return row.sortie === 0 ? false : true;
    return false;
  };

  const disabledInitPassword = (): boolean => {
    if (currentUser && currentUser.role && currentUser.role.code) {
      if (
        auth.isSupAdminOrIsGpmAdmin ||
        (isPersonnelPharamcie && currentUser.role.code === TITULAIRE_PHARMACIE)
      ) {
        return false;
      }
    }

    if (isPartenaireService && !auth.isAuthorizedToInitPasswordPartenaireService()) {
      return true;
    }

    if (isTitulaire && !auth.isAuthorizedToInitPasswordTitulairePharmacie()) {
      return true;
    }

    if (isPresident && !auth.isAuthorizedToInitPasswordPresidentRegion()) {
      return true;
    }

    if (isLaboratoirePartenaire && !auth.isAuthorizedToInitPasswordLaboratoirePartenaire()) {
      return true;
    }

    if (isPersonnelPharamcie && !auth.isAuthorizedToInitPasswordPersonnelPharmacie()) {
      return true;
    }

    if (!rowUser || (rowUser && !rowUser.email)) {
      return true;
    }
    if (
      currentUser &&
      currentUser.role &&
      currentUser.role.code &&
      !rolesPermitReset.includes(currentUser.role.code)
    ) {
      return true;
    }
    return false;
  };

  const disabledAddUser = (): boolean => {
    if (isPartenaireService && !auth.isAuthorizedToAddUserPartenaireService()) {
      return true;
    }

    if (isTitulaire && !auth.isAuthorizedToAddUserTitulairePharmacie()) {
      return true;
    }

    if (isPresident && !auth.isAuthorizedToAddUserPresidentRegion()) {
      return true;
    }

    if (isLaboratoirePartenaire && !auth.isAuthorizedToAddUserLaboratoirePartenaire()) {
      return true;
    }

    if (isPersonnelPharamcie && !auth.isAuthorizedToAddUserPersonnelPharmacie()) {
      return true;
    }

    if (isTitulaire || isPresident) {
      if (row.pharmacies && row.users && row.pharmacies.length === row.users.length) {
        return true;
      }
    }

    if ((!isTitulaire || !isPresident) && rowUser) return true;
    return false;
  };

  const disabledEditUser = (): boolean => {
    if (isPartenaireService && !auth.isAuthorizedToEditUserPartenaireService()) {
      return true;
    }

    if (isTitulaire && !auth.isAuthorizedToEditUserTitulairePharmacie()) {
      return true;
    }

    if (isPresident && !auth.isAuthorizedToEditUserPresidentRegion()) {
      return true;
    }

    if (isLaboratoirePartenaire && !auth.isAuthorizedToEditUserLaboratoirePartenaire()) {
      return true;
    }

    if (isPersonnelPharamcie && !auth.isAuthorizedToEditUserPersonnelPharmacie()) {
      return true;
    }

    if (isTitulaire || isPresident) {
      if (!row.users || (row.users && row.users.length <= 0)) return true;
    } else {
      if (!rowUser) return true;
    }

    return false;
  };

  const disabledViewHistory = (): boolean => {
    if (isPartenaireService && !auth.isAuthorizedToViewHistoryPartenaireService()) {
      return true;
    }

    if (isTitulaire && !auth.isAuthorizedToViewHistoryTitulairePharmacie()) {
      return true;
    }

    if (isPresident && !auth.isAuthorizedToViewHistoryPresidentRegion()) {
      return true;
    }

    if (isLaboratoirePartenaire && !auth.isAuthorizedToViewHistoryLaboratoirePartenaire()) {
      return true;
    }

    if (isPersonnelPharamcie && !auth.isAuthorizedToViewHistoryPersonnelPharmacie()) {
      return true;
    }

    if (!rowUser) return true;
    return false;
  };

  const disableDeployPharmacie = (): boolean => {
    return !auth.isSupAdminOrIsGpmAdmin;
  };

  const disableExportMatricePharmacie = (): boolean => {
    return !auth.isSupAdminOrIsGpmAdmin;
  };

  const disabledViewDetails = (): boolean => {
    if (isPartenaireService && !auth.isAuthorizedToViewDetailsPartenaireService()) {
      return true;
    }

    if (isTitulaire && !auth.isAuthorizedToViewDetailsTitulairePharmacie()) {
      return true;
    }

    if (isPresident && !auth.isAuthorizedToViewDetailsPresidentRegion()) {
      return true;
    }

    if (isLaboratoirePartenaire && !auth.isAuthorizedToViewDetailsLaboratoirePartenaire()) {
      return true;
    }

    if (isPersonnelPharamcie && !auth.isAuthorizedToViewDetailsPersonnelPharmacie()) {
      return true;
    }

    if (!row.id) return true;
    return false;
  };

  const handleClickShowDetails = () => {
    if (isPresident) {
      push(`/db/${TITULAIRE_PHARMACIE_URL}/fiche/${row.id}`);
    } else if (isPartenaireService) {
      push(`/db/${PARTENAIRE_SERVICE_URL}/fiche/${row.id}/contact/list`);
    } else if (isLaboratoirePartenaire) {
      push(`/db/${PARTENAIRE_LABORATOIRE_URL}/fiche/${row.id}/contact/list`);
    } else {
      push(`/db/${baseUrl}/fiche/${row.id}`);
    }
  };

  const handleSendMessage = () => { };

  const showDetailsMenu: TableActionColumnMenu = {
    label: isPharmacie
      ? 'Fiche Pharmacie'
      : isLaboratoire
        ? 'Fiche Laboratoire'
        : 'Voir les détails',
    icon: <Visibility />,
    onClick: handleClickShowDetails,
    disabled: disabledViewDetails(),
  };

  const editAndDeleteMenu: TableActionColumnMenu[] = [
    { label: 'Modifier', icon: <Edit />, onClick: goToEdit, disabled: false },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: onClickDelete,
      disabled: disabledDelete(),
    },
  ];

  // TODO : Add to props
  const deployPharmacie: TableActionColumnMenu = {
    label: 'Deployer Pharmacie',
    icon: <Launch />,
    onClick: handleDeployPharmacie,
    disabled: disableDeployPharmacie(),
  };

  const exportMatricePharmacie: TableActionColumnMenu[] = [
    {
      label: 'Exporter la matrice des fonctions de la Pharmacie (Excel)',
      icon: <ImportExport />,
      onClick: handleExportMatricePharmacie.bind(null, 'xlsx'),
      disabled: disableExportMatricePharmacie(),
    },
    {
      label: 'Exporter la matrice des fonctions de la Pharmacie (PDF)',
      icon: <ImportExport />,
      onClick: handleExportMatricePharmacie.bind(null, 'pdf'),
      disabled: disableExportMatricePharmacie(),
    },
  ];

  const historyMenu: TableActionColumnMenu[] = [
    {
      label: 'Historique',
      icon: <History />,
      onClick: handleClickHistorique,
      disabled: disabledViewHistory(),
    },
  ];

  const commonMenu: TableActionColumnMenu[] =
    isLaboratoirePartenaire || isLaboratoire
      ? editAndDeleteMenu
      : [...editAndDeleteMenu, ...historyMenu];

  const sendMessageMenu: TableActionColumnMenu = {
    label: 'Envoyer un message',
    icon: <Mail />,
    onClick: handleSendMessage,
    disabled: false,
  };

  const usersListMenu: TableActionColumnMenu = {
    label: 'Liste des comptes',
    icon: <FormatListBulleted />,
    onClick: goToEditAccount,
    disabled: disabledEditUser(),
  };

  const othersMenuItems: TableActionColumnMenu[] = [
    {
      label: 'Modifier compte',
      icon: <Edit />,
      onClick: goToEditAccount,
      disabled: disabledEditUser(),
    },
    {
      label: 'Historique',
      icon: <History />,
      onClick: handleClickHistorique,
      disabled: disabledViewHistory(),
    },
    {
      label: 'Réinitialiser mot de passe',
      icon: <VpnKey />,
      onClick: handleClickInitPwd,
      disabled: disabledInitPassword(),
    },
  ];

  const onClickAffectation = (event: MouseEvent<any>, action: any) => {
    event.preventDefault();
    event.stopPropagation();
    handleClose();
    if (handleClickAffectation) handleClickAffectation(action, row);
  };

  const affectationMenuItems: TableActionColumnMenu[] = [
    {
      label: 'Remplacement',
      icon: <SyncAlt />,
      onClick: e => {
        onClickAffectation(e, { type: 'REMPLACEMENT_ATTRIBUTION', label: 'Remplacement' });
      },
      disabled: false,
    },
    {
      label: 'Arrêt sans remplacement',
      icon: <ArretSansRemplacementIcon />,
      onClick: e => {
        onClickAffectation(e, {
          type: 'REMPLACEMENT_ARRET_SANS',
          label: 'Arrêt sans remplacement',
        });
      },
      disabled: false,
    },
    {
      label: 'Attribution de département',
      icon: <AttributionDepartementIcon />,
      onClick: e => {
        onClickAffectation(e, { type: 'DEPARTEMENT', label: 'Attribution de département' });
      },
      disabled: false,
    },
  ];

  const basicMenu: TableActionColumnMenu[] = [
    {
      label: 'Voir les détails',
      icon: <Visibility />,
      onClick: handleClickShowDetails,
      disabled: false,
    },
    { label: 'Modifier', icon: <Edit />, onClick: goToEdit, disabled: false },
    { label: 'Supprimer', icon: <Delete />, onClick: onClickDelete, disabled: false },
  ];

  const defaultMenuItems: TableActionColumnMenu[] = [
    { label: 'Modifier', icon: <Edit />, onClick: goToEdit, disabled: false },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: onClickDelete,
      disabled: disabledDelete(),
    },
    {
      label: 'Créer compte',
      icon: <PersonAdd />,
      onClick: goToCreateAccount,
      disabled: disabledAddUser(),
    },
    ...((isTitulaire || isPresident) && row.pharmacies && row.pharmacies.length > 1
      ? [usersListMenu]
      : othersMenuItems),
  ];

  const menuItems: TableActionColumnMenu[] = isAffectation
    ? affectationMenuItems
    : isGroupeClient || isColor
      ? editAndDeleteMenu
      : isPharmacie || isLaboratoirePartenaire
        ? [showDetailsMenu, ...commonMenu, sendMessageMenu]
        : isLaboratoire
          ? [...commonMenu]
          : withViewDetails
            ? [...defaultMenuItems, ...[showDetailsMenu]]
            : isPartageIdeeBonnePratique
              ? basicMenu
              : defaultMenuItems;

  if (isPharmacie && auth.isSupAdminOrIsGpmAdmin) {
    menuItems.push(deployPharmacie);
  }

  if (isPharmacie && auth.isSupAdminOrIsGpmAdmin) {
    exportMatricePharmacie.forEach(menu => menuItems.push(menu));
  }

  return (
    <div>
      <IconButton aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        <MoreHoriz />
      </IconButton>
      <Menu
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        {customMenuItems
          ? customMenuItems.map((i: TableActionColumnMenu, index: number) => (
            <MenuItem
              // tslint:disable-next-line: jsx-no-lambda
              onClick={event => {
                handleClose();
                i.onClick(event, row);
              }}
              key={`table_menu_item_${index}`}
              disabled={i.disabled}
            >
              <ListItemIcon>{i.icon}</ListItemIcon>
              <Typography variant="inherit">{i.label}</Typography>
            </MenuItem>
          ))
          : menuItems &&
          menuItems.length > 0 &&
          menuItems.map((i: TableActionColumnMenu, index: number) => (
            <MenuItem
              // tslint:disable-next-line: jsx-no-lambda
              onClick={event => {
                handleClose();
                i.onClick(event, row);
              }}
              key={`table_menu_item_${index}`}
              disabled={i.disabled}
            >
              <ListItemIcon>{i.icon}</ListItemIcon>
              <Typography variant="inherit">{i.label}</Typography>
            </MenuItem>
          ))}
        {showResendEmail &&
          ((!isAffectation &&
            !isGroupeClient &&
            !isColor &&
            !isPharmacie &&
            !isLaboratoirePartenaire &&
            !isLaboratoire &&
            !isTitulaire &&
            !isPresident &&
            !isPartenaireService) ||
            (row.pharmacies && row.pharmacies.length <= 1)) && (
            <MenuItemResendEmail
              email={rowUser && rowUser.email}
              login={rowUser && rowUser.login}
              disabled={
                rowUser &&
                  rowUser.email &&
                  (rowUser.status !== 'ACTIVATED' || rowUser.status !== 'ACTIVATION_REQUIRED')
                  ? false
                  : true
              }
            />
          )}
      </Menu>
      <HistoriquePersonnel
        open={openHistory}
        setOpen={setOpenHistory}
        userId={rowUser && rowUser.id}
      />
      <ResetUserPasswordModal
        open={openInitPwd}
        setOpen={setOpenInitPwd}
        userId={rowUser && rowUser.id}
        email={rowUser && rowUser.email}
        login={rowUser && rowUser.login}
      />
      <UsersModal open={openUsersModal} setOpen={setOpenUsersModal} row={row} baseUrl={baseUrl} />

      {exportingMatriceTache.component}
    </div>
  );
};

export default withRouter(TableActionColumn);
