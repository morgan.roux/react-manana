import { useApolloClient, useQuery } from '@apollo/client';
import { Box } from '@material-ui/core';
import React, { FC, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import {
  ADMINISTRATEUR_GROUPEMENT,
  AMBASSADEUR,
  SUPER_ADMINISTRATEUR,
} from '../../../Constant/roles';
import { GET_ROLES_SEARCH } from '../../../graphql/Role/query';
import {
  ROLES_SEARCH,
  ROLES_SEARCHVariables,
  ROLES_SEARCH_search_data_Role,
} from '../../../graphql/Role/types/ROLES_SEARCH';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import AccessPermision from '../AccessPermision';
import Appetence from '../Appetence';
import SubHeader from './SubHeader';
import TableRoles from './TableRoles';

const Roles: FC<RouteComponentProps> = ({ location: { pathname } }) => {
  const client = useApolloClient();

  const [order, setOrder] = useState<'asc' | 'desc'>('asc');
  const [search, setSearch] = useState<string>('');
  const [take, setTake] = useState<number>(10);
  const [skip, setSkip] = useState<number>(0);

  const { data } = useQuery<ROLES_SEARCH, ROLES_SEARCHVariables>(GET_ROLES_SEARCH, {
    variables: {
      type: ['role'],
      sortBy: [{ nom: { order } }],
      filterBy: [{ wildcard: { nom: `*${search}*` } }],
      query: {
        query: {
          bool: {
            must_not: [
              {
                terms: {
                  code: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT, AMBASSADEUR],
                },
              },
            ],
          },
        },
      },
      take,
      skip,
    },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        });
      });
    },
  });

  const roles = data && data.search && (data.search.data as ROLES_SEARCH_search_data_Role[]);
  const total = (data && data.search && data.search.total) || 0;

  return (
    <Box>
      {pathname === '/db/roles' && roles && (
        <>
          <SubHeader {...{ search, setSearch }} />
          <TableRoles roles={roles} {...{ order, setOrder, take, skip, setTake, setSkip, total }} />
        </>
      )}
      {pathname.startsWith('/db/roles/accessPermision') && <AccessPermision />}
      {pathname.startsWith('/db/roles/appetence') && <Appetence />}
    </Box>
  );
};

export default withRouter(Roles);
