import React, { FC } from 'react';
import { SearchInput } from '../../Content/SearchInput';
import { Box } from '@material-ui/core';
import { useStyles } from '../style';

interface SearchRoleFormProps {
  search: string;
  setSearch(search: string): void;
}

const SubHeader: FC<SearchRoleFormProps> = props => {
  const classes = useStyles({});
  const { search, setSearch } = props;
  return (
    <Box className={classes.subHeader}>
      <SearchInput value={search} onChange={value => setSearch(value)} placeholder={'Rechercher'} />
    </Box>
  );
};

export default SubHeader;
