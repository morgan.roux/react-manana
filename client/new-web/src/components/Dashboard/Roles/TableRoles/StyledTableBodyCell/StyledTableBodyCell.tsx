import { withStyles, TableCell } from '@material-ui/core';

const StyledTableBodyCell = withStyles(theme => ({
  body: {
    fontWeight: 'bolder',
    fontSize: 14,
  },
}))(TableCell);

export default StyledTableBodyCell;
