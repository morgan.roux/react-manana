import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    personnelGroupementRoot: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      position: 'relative',
      height: 'auto',
      minHeight: 'fit-content',
    },
    mutationSuccessContainer: {
      width: '100%',
      minHeight: 'calc(100vh - 156px)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
      '& img': {
        width: 350,
        height: 270,
      },

      '& p': {
        textAlign: 'center',
        maxWidth: 600,
      },
      '& > div > p:nth-child(2)': {
        fontSize: 30,
        fontWeight: 'bold',
      },
      '& > div > p:nth-child(3)': {
        fontSize: 16,
        fontWeight: 'normal',
      },
      '& button': {
        marginTop: 30,
      },
    },
    container: {
      width: '100%',
    },
    childrenRoot: {
      display: 'flex',
      '& > button:not(:nth-last-child(1))': {
        marginRight: 15,
      },
    },
    searchInputBox: {
      padding: '20px 0px 0px 20px',
      display: 'flex',
    },
    serviceFormcontainer: {
      padding: 33,
      display: 'flex',
      alignItems: 'flex-start',
    },
    serviceFormFirstLine: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'flex-start',
    },
    innerFormItems: {
      marginBottom: 20,
      marginRight: 20,
    },
    sectionContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      marginBottom: 5,
      width: '18%',
      '& .MuiPaper-root, & .MuiPaper-elevation1': {
        boxShadow: 'none!important',
      },
      '& > div': {
        borderRadius: 12,
        padding: '8px 20px 20px',
        border: '1px solid #DCDCDC',
        boxShadow: 'none!important',
        width: '100%',
        minHeight: '89%',
      },
      '@media (max-width: 1300px)': {
        width: '19%',
      },
      '@media (max-width:1200px)': {
        width: '24%',
        marginBottom: 24,
      },
      '@media (max-width:1024px)': {
        width: '31%',
      },
      '@media (max-width:756px)': {
        width: '48%',
      },
      '@media (max-width:578px)': {
        width: '98%',
      },
    },

    inputTitle: {
      fontWeight: 'bold',
      fontSize: 18,
      color: theme.palette.secondary.main,
      marginBottom: 5,
    },
  }),
);

export default useStyles;
