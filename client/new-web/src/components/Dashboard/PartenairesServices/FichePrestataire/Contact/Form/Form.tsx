import { RouteComponentProps, withRouter } from 'react-router';
import React, { ChangeEvent, FC, Fragment, useContext, useEffect, useState } from 'react';
import useStyles from './styles';
import Backdrop from '../../../../../Common/Backdrop';
import Stepper from '../../../../../Common/Stepper';
import { Step } from '../../../../../Common/Stepper/Stepper';
import { Typography } from '@material-ui/core';
import {
  PARTENAIRE,
  PARTENAIREVariables,
  PARTENAIRE_partenaire,
} from '../../../../../../graphql/Partenaire/types/PARTENAIRE';
import { FormStepOne, FormStepTwo } from './Steps';
import { last, split, head, isEqual } from 'lodash';
import { uploadToS3 } from '../../../../../../services/S3';
import { useApolloClient, useMutation, useQuery } from '@apollo/client';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import { formatFilenameWithoutDate } from '../../../../../../utils/filenameFormater';
import { getGroupement } from '../../../../../../services/LocalStorage';
import { FichierInput, TypeFichier } from '../../../../../../types/graphql-global-types';
import {
  CREATE_UPDATE_PARTENAIRE_REPRESENTANT,
  CREATE_UPDATE_PARTENAIRE_REPRESENTANTVariables,
} from '../../../../../../graphql/PartenaireRepresentant/types/CREATE_UPDATE_PARTENAIRE_REPRESENTANT';
import { DO_CREATE_UPDATE_PARTENAIRE_REPRESENTANT } from '../../../../../../graphql/PartenaireRepresentant/mutation';
import SnackVariableInterface from '../../../../../../Interface/SnackVariableInterface';
import { GET_PARTENAIRE } from '../../../../../../graphql/Partenaire/query';
import {
  PARTENAIRE_REPRESENTANT,
  PARTENAIRE_REPRESENTANTVariables,
} from '../../../../../../graphql/PartenaireRepresentant/types/PARTENAIRE_REPRESENTANT';
import { GET_PARTENAIRE_REPRESENTANT } from '../../../../../../graphql/PartenaireRepresentant/query';
import { type } from 'os';
import NoItemContentImage from '../../../../../Common/NoItemContentImage';
import CustomButton from '../../../../../Common/CustomButton';
import mutationSuccessImg from '../../../../../../assets/img/mutation-success.png';
import { PARTENAIRE_SERVICE_URL } from '../../../../../../Constant/url';
import { isEmailValid } from '../../../../../../utils/Validator';
import { ContentStateInterface, ContentContext } from '../../../../../../AppContext';
interface FormProps {
  match: { params: { idPartenaire: string | undefined; id: string | undefined } };
}

const Form: FC<FormProps & RouteComponentProps> = ({
  location: { pathname },
  match: {
    params: { idPartenaire, id },
  },
  history: { push },
}) => {
  const classes = useStyles({});

  const isOnCreate: boolean = pathname.includes('/create');

  const groupement = getGroupement();

  const [loading, setLoading] = useState<boolean>(false);

  const [nextBtnDisabled, setNextBtnDisabled] = useState<boolean>(false);

  const [mutationSucess, setMutationSuccess] = useState<boolean>(false);
  const initialState = {
    id: '',
    photo: null,
    civilite: '',
    nom: '',
    prenom: '',
    sexe: '',
    fonction: '',
    idPartenaire: idPartenaire || '',
    idPartenaireType: '',
    afficherComme: '',
    contact: {},
  };

  const { data: contactData, loading: contactLoading } = useQuery(GET_PARTENAIRE_REPRESENTANT, {
    variables: { id: id || '' },
    onCompleted: data => {
      if (data && data.partenaireRepresentant) {
        const representant = data.partenaireRepresentant;
        const contact = representant.contact;
        const { ['__typename']: typename, ...contactRest } = contact;
        if (representant && representant.photo) {
          setPhoto(representant.photo);
        }
        setValues({
          id: id || '',
          civilite: representant.civilite || '',
          nom: representant.nom,
          prenom: representant.prenom || '',
          sexe: representant.sexe || '',
          fonction: representant.fonction || '',
          idPartenaire: (representant.partenaire && representant.partenaire.id) || '',
          idPartenaireType: (representant.partenaireType && representant.partenaireType.id) || '',
          afficherComme: representant.afficherComme || '',
          contact: contactRest,
        });
      }
    },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const { data: partenaireData, loading: partenaireLoading } = useQuery<
    PARTENAIRE,
    PARTENAIREVariables
  >(GET_PARTENAIRE, {
    variables: { id: idPartenaire as string },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const partenaire = partenaireData && partenaireData.partenaire;
  const [values, setValues] = useState<any>(initialState);

  const [photo, setPhoto] = useState<any>(null);

  const client = useApolloClient();

  const onClickBack = () => {
    push(`/db/${PARTENAIRE_SERVICE_URL}/fiche/${idPartenaire}/contact/list`);
  };

  const saveContact = () => { };

  const otherHandleChange = (name: string, value: any, checked: boolean) => {
    const nameArray = split(name, '.');
    const nameKey: any = head(nameArray);
    const exactName: any = last(nameArray);
    if (name.includes('checkbox')) {
      if (nameKey === exactName && exactName !== 'cap') {
        setValues(prevState => ({
          ...prevState,
          [exactName]: checked,
        }));
      } else {
        setValues(prevState => ({
          ...prevState,
          [nameKey]: { ...prevState[nameKey], [exactName]: checked },
        }));
      }
    } else {
      setValues(prevState => ({
        ...prevState,
        [nameKey]: { ...prevState[nameKey], [exactName]: value },
      }));
    }
  };

  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const [
    createUpdatePartenaireRepresentant,
    { loading: createUpdatePartenaireRepresentantLoading },
  ] = useMutation<
    CREATE_UPDATE_PARTENAIRE_REPRESENTANT,
    CREATE_UPDATE_PARTENAIRE_REPRESENTANTVariables
  >(DO_CREATE_UPDATE_PARTENAIRE_REPRESENTANT, {
    update: (cache, { data }) => {
      if (data && data.createUpdatePartenaireRepresentant && values) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables: variables,
          });
          if (req && req.search && req.search.data && isOnCreate) {
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    total: req.search.total + 1,
                    data: [...req.search.data, data.createUpdatePartenaireRepresentant],
                  },
                },
              },
              variables: variables,
            });
          }
        }
      }
    },
    onCompleted: data => {
      if (data && data.createUpdatePartenaireRepresentant) {
        setLoading(false);
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Contact ${isOnCreate ? 'crée' : 'modifiée'} avec succès`,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
        setMutationSuccess(true);
      }
    },
    onError: errors => {
      setLoading(false);
      let errorMessage: string = 'Erreur du serveur';
      errors.graphQLErrors.map(error => {
        if (error.extensions) {
          switch (error.extensions.code) {
            case 'MAIL_NOT_SENT':
              errorMessage = "L'email n'est pas envoyé à l'utilisatuer";
              break;
            case 'EMAIL_ALREADY_EXIST':
              errorMessage = 'Cet email est déjà utilisé par un autre utilisateur';
              break;
          }
        }
      });

      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errorMessage,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const submit = () => {
    setLoading(true);
    if (photo && photo.size) {
      // Presigned file
      const filePathsTab: string[] = [];
      filePathsTab.push(
        `users/photos/${groupement && groupement.id}/${formatFilenameWithoutDate(photo)}`,
      );
      doCreatePutPresignedUrl({ variables: { filePaths: filePathsTab } });
      return;
    }

    let userPhoto: FichierInput | null = null;
    if (photo && photo.id) {
      userPhoto = {
        chemin: photo.chemin,
        nomOriginal: photo.nomOriginal,
        type: TypeFichier.AVATAR,
        idAvatar: photo && photo.idAvatar,
      };
    }
    createUpdatePartenaireRepresentant({
      variables: {
        input: {
          ...values,
          contact: { ...values.contact },
          photo: userPhoto,
        },
      },
    });
  };

  const finalStepBtn = {
    buttonLabel: isOnCreate ? 'Ajouter' : 'Modifier',
    action: submit,
  };

  const [doCreatePutPresignedUrl, { loading: presignedLoading }] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (
        data &&
        data.createPutPresignedUrls &&
        data.createPutPresignedUrls.length > 0 &&
        photo &&
        photo.size
      ) {
        const presignedPhoto = data.createPutPresignedUrls[0];
        if (presignedPhoto && presignedPhoto.filePath && presignedPhoto.presignedUrl) {
          setLoading(true);
          await uploadToS3(photo, presignedPhoto.presignedUrl)
            .then(result => {
              if (result && result.status === 200) {
                createUpdatePartenaireRepresentant({
                  variables: {
                    input: {
                      ...values,
                      contact: { ...values.contact },
                      photo: {
                        chemin: presignedPhoto.filePath,
                        nomOriginal: photo.name,
                        type: TypeFichier.PHOTO,
                      },
                    },
                  },
                });
              }
            })
            .catch(error => {
              console.error(error);
              setLoading(false);
              displaySnackBar(client, {
                type: 'ERROR',
                message:
                  "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
                isOpen: true,
              });
            });
        }
      }
    },
    onError: errors => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      });
    },
  });

  const handleChange = (e: ChangeEvent<any>) => {
    const { name, value, checked, type } = e.target;
    if (name.includes('.')) {
      otherHandleChange(name, value, checked);
      return;
    }
    if (e && e.target) {
      const { name, value } = e.target;
      setValues(prevState => ({ ...prevState, [name]: value }));
    }
  };

  const title = `${isOnCreate ? 'Ajout' : 'Modification'} de contact`;

  useEffect(() => {
    const { civilite, nom, sexe, contact } = values;

    if (nom === '' || sexe === '' || civilite === '') {
      setNextBtnDisabled(true);
    } else if (
      contact &&
      contact.mailProf &&
      contact.mailProf.length > 0 &&
      !isEmailValid(contact.mailProf)
    ) {
      displaySnackBar(client, {
        isOpen: true,
        type: 'ERROR',
        message: 'Adresse de messagerie invalide',
      });
      setNextBtnDisabled(true);
    } else {
      setNextBtnDisabled(false);
    }
  }, [values]);

  const steps: Step[] = [
    {
      title: 'Information',
      content: (
        <FormStepOne
          photo={photo}
          setPhoto={setPhoto}
          values={values}
          handleChange={handleChange}
        />
      ),
    },
    {
      title: 'Contact',
      content: <FormStepTwo values={values} handleChange={handleChange} />,
    },
  ];

  console.log('values ', values);

  const goToHome = () => {
    push(`/db/${PARTENAIRE_SERVICE_URL}/fiche/${idPartenaire}/contact/list`);
  };

  const mutationSuccessTitle = `${isOnCreate ? 'Création' : 'Modification'
    } de contact d'un prestataire de service`;
  const mutationSuccessSubTitle = `Le contact que vous venez de ${isOnCreate ? 'créer' : 'modifier'
    } est maintenant disponible dans la liste.`;

  if (mutationSucess) {
    return (
      <div className={classes.mutationSuccessContainer}>
        <NoItemContentImage
          src={mutationSuccessImg}
          title={mutationSuccessTitle}
          subtitle={mutationSuccessSubTitle}
        >
          <CustomButton color="default" onClick={goToHome}>
            Retour à la liste
          </CustomButton>
        </NoItemContentImage>
      </div>
    );
  }

  return (
    <Fragment>
      {loading && <Backdrop />}
      <Stepper
        title={title}
        steps={steps}
        disableNextBtn={nextBtnDisabled}
        backToHome={onClickBack}
        finalStep={finalStepBtn}
        finished={false}
      >
        {partenaire ? (
          <div className={classes.partenaireContainer}>
            <Typography className={classes.partenaireName}>
              {partenaire && partenaire.nom}
            </Typography>
            <Typography className={classes.partenaireAdresse}>
              {(partenaire.partenaireServiceSuite && partenaire.partenaireServiceSuite.adresse1) ||
                ''}
            </Typography>
          </div>
        ) : null}
      </Stepper>
    </Fragment>
  );
};
export default withRouter(Form);
