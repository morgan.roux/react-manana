import { ArrowBack, Edit } from '@material-ui/icons';
import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import CustomAvatar from '../../../../../Common/CustomAvatar';
import CustomButton from '../../../../../Common/CustomButton';
import useStyles from './styles';
import classnames from 'classnames';
import { useApolloClient, useQuery } from '@apollo/client';
import Sectioncontainer from '../../../SectionContainer';
import { Box, IconButton, Link, Typography } from '@material-ui/core';
import { GET_PARTENAIRE_REPRESENTANT } from '../../../../../../graphql/PartenaireRepresentant/query';
import {
  PARTENAIRE_REPRESENTANT,
  PARTENAIRE_REPRESENTANTVariables,
} from '../../../../../../graphql/PartenaireRepresentant/types/PARTENAIRE_REPRESENTANT';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import useCommonStyles from '../../../../commonStyles';
import { PARTENAIRE_SERVICE_URL } from '../../../../../../Constant/url';
interface FicheContactProps {
  match: { params: { idPartenaire: string | undefined; id: string | undefined } };
}

const FicheContact: React.FC<FicheContactProps & RouteComponentProps> = ({
  location: { pathname },
  match: {
    params: { id, idPartenaire },
  },
  history: { push },
}) => {
  const classes = useStyles({});
  const handleClick = () => { };
  const client = useApolloClient();
  const commonStyles = useCommonStyles();
  const { data, loading } = useQuery<PARTENAIRE_REPRESENTANT, PARTENAIRE_REPRESENTANTVariables>(
    GET_PARTENAIRE_REPRESENTANT,
    {
      variables: { id: id as string },
      onError: error => {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );
  const goToEdit = () => {
    push(`/db/${PARTENAIRE_SERVICE_URL}/fiche/${idPartenaire}/contact/edit/${id}`);
  };

  const goBack = () => {
    push(`/db/${PARTENAIRE_SERVICE_URL}/fiche/${idPartenaire}/contact/list/`);
  };
  const sections = [
    {
      title: 'Informations Personnelles',
      content: [
        {
          subtitle: 'Civilité',
          item: data && data.partenaireRepresentant ? data.partenaireRepresentant.civilite : '-',
        },

        {
          subtitle: 'Prénom',
          item: data && data.partenaireRepresentant ? data.partenaireRepresentant.prenom : '-',
        },
        {
          subtitle: 'Nom',
          item: data && data.partenaireRepresentant ? data.partenaireRepresentant.nom : '-',
        },
        {
          subtitle: 'Genre',
          item: data && data.partenaireRepresentant ? data.partenaireRepresentant.sexe : '-',
        },
      ],
    },
    {
      title: 'Coordonnées',
      content: [
        {
          subtitle: 'Adresse',
          item:
            data && data.partenaireRepresentant && data.partenaireRepresentant.contact
              ? data.partenaireRepresentant.contact.adresse1
              : '-',
        },
        {
          subtitle: 'Code postal',
          item:
            data && data.partenaireRepresentant && data.partenaireRepresentant.contact
              ? data.partenaireRepresentant.contact.cp
              : '-',
        },
        {
          subtitle: 'Ville',
          item:
            data && data.partenaireRepresentant && data.partenaireRepresentant.contact
              ? data.partenaireRepresentant.contact.ville
              : '-',
        },
      ],
    },
    {
      title: 'Internet',
      content: [
        {
          subtitle: 'Adresse de messagerie',
          item:
            data && data.partenaireRepresentant && data.partenaireRepresentant.contact
              ? data.partenaireRepresentant.contact.mailPerso
              : '-',
        },
        {
          subtitle: 'Afficher comme',
          item:
            data && data.partenaireRepresentant && data.partenaireRepresentant.contact
              ? data.partenaireRepresentant.contact.mailPerso
              : '-',
        },
        {
          subtitle: 'Page web',
          item:
            data && data.partenaireRepresentant && data.partenaireRepresentant.contact
              ? data.partenaireRepresentant.contact.siteProf
              : '-',
        },
        { subtitle: 'Adresse de messagerie instantanée', item: '' },
      ],
    },
    {
      title: 'Numéros de téléphone',
      content: [
        {
          subtitle: 'Bureau',
          item:
            data && data.partenaireRepresentant && data.partenaireRepresentant.contact
              ? data.partenaireRepresentant.contact.telProf
              : '-',
        },
        {
          subtitle: 'Domicile',
          item:
            data && data.partenaireRepresentant && data.partenaireRepresentant.contact
              ? data.partenaireRepresentant.contact.telPerso
              : '-',
        },
        {
          subtitle: 'Télécopie (bureau)',
          item:
            data && data.partenaireRepresentant && data.partenaireRepresentant.contact
              ? data.partenaireRepresentant.contact.telProf
              : '-',
        },
        {
          subtitle: 'Téléphone mobile',
          item:
            data && data.partenaireRepresentant && data.partenaireRepresentant.contact
              ? data.partenaireRepresentant.contact.telPerso
              : '-',
        },
      ],
    },
    {
      title: 'Réseaux sociaux',
      content: [
        {
          subtitle: 'LinkedIn',
          item: (
            <Link
              href={
                'https://' + (data?.partenaireRepresentant?.contact?.urlLinkedinProf ?? '-') || ''
              }
              target="_blank"
            >
              linkedin.com
            </Link>
          ),
        },
        {
          subtitle: 'Facebook',
          item: (
            <Link
              href={
                'https://' + (data?.partenaireRepresentant?.contact?.urlFacebookProf ?? '-') || ''
              }
              target="_blank"
            >
              facebook.com
            </Link>
          ),
        },
        {
          subtitle: 'WhatSapp',
          item:
            data && data.partenaireRepresentant && data.partenaireRepresentant.contact
              ? data.partenaireRepresentant.contact.whatsAppMobProf
              : '-',
        },
        {
          subtitle: 'Messenger',
          item: (
            <Link
              href={'https://' + (data?.partenaireRepresentant?.contact?.urlMessenger ?? '-') || ''}
              target="_blank"
              color="primary"
            >
              messenger.com
            </Link>
          ),
        },
        {
          subtitle: 'Youtube',
          item: (
            <Link
              href={'https://' + (data?.partenaireRepresentant?.contact?.urlYoutube ?? '-') || ''}
              target="_blank"
            >
              youtube.com
            </Link>
          ),
        },
      ],
    },
  ];

  const ImageSection = (
    <Box className={classes.contentSection}>
      <Box className={classes.nameContent}>
        <Box className={classes.sectionImage}>
          <CustomAvatar
            name={data?.partenaireRepresentant?.nom ?? 'Nom'}
            url={data?.partenaireRepresentant?.photo?.publicUrl ?? 'url'}
          />
          <Typography className={classes.titleName}>
            {data?.partenaireRepresentant?.nom ?? '-'} {data?.partenaireRepresentant?.prenom ?? '-'}
            <Typography className={classes.contentLabel}>
              <Typography className={classes.labelTitle}>Fonction :</Typography>

              <Typography className={classes.labelResult}>
                {data && data.partenaireRepresentant ? data.partenaireRepresentant.fonction : '-'}
              </Typography>
            </Typography>
          </Typography>
        </Box>
        <Box>
          <CustomButton onClick={goToEdit} color="secondary" startIcon={<Edit />}>
            Modifier
          </CustomButton>
        </Box>
      </Box>
    </Box>
  );
  return (
    <Box className={classes.ficheContactContainer}>
      <Box display="flex" alignItems="center">
        <IconButton color="inherit" onClick={goBack} style={{ marginRight: 'auto' }}>
          <ArrowBack />
        </IconButton>
        <Typography>Retour</Typography>
      </Box>
      <Box
        width="100%"
        display="flex"
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
      >
        <Typography className={classes.title}>Fiche détaillé de contact</Typography>
        <Box display="flex" alignItems="center" justifyContent="center" flexDirection="column">
          <Typography className={classes.subTitles}>
            {data &&
              data.partenaireRepresentant &&
              data.partenaireRepresentant.partenaire &&
              data.partenaireRepresentant.partenaire.partenaireServiceSuite &&
              data.partenaireRepresentant.partenaire.partenaireServiceSuite.groupement &&
              data.partenaireRepresentant.partenaire.partenaireServiceSuite.groupement.nom}
          </Typography>
          <Typography>
            {data && data.partenaireRepresentant && data.partenaireRepresentant.contact
              ? data.partenaireRepresentant.contact.adresse1
              : '-'}
          </Typography>
        </Box>
      </Box>
      <Box width="100%" padding="25px">
        <Box className={classnames(classes.contentSectionNote)}>
          {ImageSection}
          <Box className={classnames(classes.sectionImg)}>
            <Typography className={classes.inputTitle}>Note</Typography>
            <Box> Note</Box>{' '}
          </Box>
        </Box>
        <Box className={classes.sections}>
          {sections.map(section => (
            <Sectioncontainer
              title={section.title}
              children={
                <Box>
                  {section.content.map(content => (
                    <Box style={{ marginTop: 10 }}>
                      <Typography className={classes.titleSub}>{content.subtitle}</Typography>
                      <Typography className={classes.subtitle}>
                        {content.item === '' ? '-' : content.item}
                      </Typography>
                    </Box>
                  ))}
                </Box>
              }
            />
          ))}
        </Box>
      </Box>
    </Box>
  );
};
export default withRouter(FicheContact);
