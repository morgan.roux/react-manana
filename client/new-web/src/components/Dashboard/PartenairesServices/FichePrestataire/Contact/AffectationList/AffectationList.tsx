import { useApolloClient, useMutation } from '@apollo/client';
import { Box } from '@material-ui/core';
import { SyncAlt } from '@material-ui/icons';
import moment from 'moment';
import React, { FC, useContext, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { ContentStateInterface, ContentContext } from '../../../../../../AppContext';
import { PARTENAIRE_SERVICE_URL } from '../../../../../../Constant/url';
import { DO_AFFECT_PARTENAIRE_REPRESENTANT } from '../../../../../../graphql/PartenaireRepresentantAffectation/mutation';
import { GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION } from '../../../../../../graphql/PartenaireRepresentantAffectation/query';
import {
  AFFECT_PARTENAIRE_REPRESENTANT,
  AFFECT_PARTENAIRE_REPRESENTANTVariables,
} from '../../../../../../graphql/PartenaireRepresentantAffectation/types/AFFECT_PARTENAIRE_REPRESENTANT';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import AffectationDialog from '../../../../../Common/AffectationDialog';
import { ConfirmDeleteDialog } from '../../../../../Common/ConfirmDialog';
import CustomButton from '../../../../../Common/CustomButton';
import CustomContent from '../../../../../Common/newCustomContent';
import SearchInput from '../../../../../Common/newCustomContent/SearchInput';
import WithSearch from '../../../../../Common/newWithSearch/withSearch';
import { Column } from '../../../../Content/Interface';
import TableActionColumn, {
  ArretSansRemplacementIcon,
  AttributionDepartementIcon,
  TableActionColumnMenu,
} from '../../../../TableActionColumn';
import useStyles from './styles';

interface AffectationListProps {
  match: { params: { idPartenaire: string | undefined; id: string | undefined } };
}

const AffectationList: FC<AffectationListProps & RouteComponentProps> = ({
  location: { pathname },
  history: { push },
  match: {
    params: { idPartenaire, id },
  },
}) => {
  const classes = useStyles({});
  const isOnList: boolean = pathname.includes('/list');
  const [openAffectationDialog, setOpenAffectationDialog] = useState<boolean>(false);
  const [representantToAffect, setRepresentantToAffect] = useState<any>({});
  // const isOnCreate: boolean = pathname.includes('/create');
  // const currentTab = isOnList ? 0 : 1;

  const CREATE_URL = `/db/${PARTENAIRE_SERVICE_URL}/fiche/${idPartenaire}/contact/create`;

  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [deleteRow, setDeleteRow] = useState<any>(null);
  const [selected, setSelected] = useState<any[]>([]);

  const goToCreate = () => {
    push(CREATE_URL);
  };

  const goToEdit = (_event: any, row: any) => {
    if (row && row.id) {
      push(`/db/${PARTENAIRE_SERVICE_URL}/fiche/${idPartenaire}/contact/edit/${row.id}`);
    }
  };

  const goToDetails = (_event: any, row: any) => {
    if (row && row.id) {
      push(`/db/${PARTENAIRE_SERVICE_URL}/fiche/${idPartenaire}/contact/details/${row.id}`);
    }
  };

  const onClickDelete = (_event: any, row: any) => {
    setOpenDeleteDialog(true);
    setDeleteRow(row);
  };

  const onClickConfirmDelete = () => {
    setOpenDeleteDialog(false);
    if (deleteRow) {
      console.log('deleteRow :>> ', deleteRow);
    }
  };

  const onClickAffectation = (event: any, action: any) => {
    event.preventDefault();
    event.stopPropagation();
    setOpenAffectationDialog(true);
    setRepresentantToAffect({ ...action });
  };

  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const client = useApolloClient();
  const [affectPartenaireRepresentant, { loading, data: affectData }] = useMutation<
    AFFECT_PARTENAIRE_REPRESENTANT,
    AFFECT_PARTENAIRE_REPRESENTANTVariables
  >(DO_AFFECT_PARTENAIRE_REPRESENTANT, {
    update: (cache, { data }) => {
      if (data && data.createUpdatePartenaireRepresentantAffectation) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables: variables,
          });
          if (req && req.search && req.search.data) {
            // const source = req.search.data;
            const result = data.createUpdatePartenaireRepresentantAffectation;
            // const dif = differenceBy(source, result, 'id');
            // cache.writeQuery({
            //   query: operationName,
            //   data: {
            //     search: {
            //       ...req.search,
            //       ...{
            //         data: dif,
            //       },
            //     },
            //   },
            //   variables: variables,
            // });
            // (client as any).writeData({
            //   data: { checkedsPersonnelGroupement: differenceBy(checkeds, result, 'id') },
            // });
            displaySnackBar(client, {
              type: 'SUCCESS',
              message: 'Utilisateur affecté avec succès',
              isOpen: true,
            });
          }
        }
      }
    },
    onError: errors => {
      console.log('======eror =========+> ', errors);
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  const affectationDialog = (
    <AffectationDialog
      open={openAffectationDialog}
      setOpen={setOpenAffectationDialog}
      personnelToAffect={representantToAffect}
      setPersonnelToAffect={setRepresentantToAffect}
      affectPersonnel={affectPartenaireRepresentant}
    />
  );

  const menuItems: TableActionColumnMenu[] = [
    {
      label: 'Remplacement',
      icon: (
        <div className={classes.dptIconBackground}>
          <SyncAlt color="inherit" />
        </div>
      ),
      onClick: (e, row) => {
        onClickAffectation(e, {
          action: { type: 'REMPLACEMENT_ATTRIBUTION', label: 'Remplacement' },
          ...row,
        });
      },
      disabled: false,
    },
    {
      label: 'Arrêt sans remplacement',
      icon: (
        <div className={classes.dptIconBackground}>
          <ArretSansRemplacementIcon color="inherit" />
        </div>
      ),
      onClick: (e, row) => {
        onClickAffectation(e, {
          action: { type: 'REMPLACEMENT_ARRET_SANS', label: 'Arrêt sans remplacement' },
          ...row,
        });
      },
      disabled: false,
    },
    {
      label: 'Attribution de département',
      icon: (
        <div className={classes.dptIconBackground}>
          <AttributionDepartementIcon color="inherit" />
        </div>
      ),
      onClick: (e, row) => {
        onClickAffectation(e, {
          action: { type: 'DEPARTEMENT', label: 'Attribution de département' },
          ...row,
        });
      },
      disabled: false,
    },
  ];

  const columns: Column[] = [
    {
      name: 'partenaireRepresentant.fonction',
      label: 'Fonction',
      renderer: (value: any) => {
        return (
          (value && value.partenaireRepresentant && value.partenaireRepresentant.fonction) || '-'
        );
      },
    },

    {
      name: 'partenaireRepresentant.nom',
      label: 'Prénom',
      renderer: (value: any) => {
        return (value && value.partenaireRepresentant && value.partenaireRepresentant.nom) || '-';
      },
    },
    {
      name: 'departement.nom',
      label: 'Nom du département',
      renderer: (value: any) => {
        return (
          `${value && value.departement && value.departement.nom} (${value &&
          value.departement &&
          value.departement.code})` || '-'
        );
      },
    },
    {
      name: 'partenaireRepresentant.dateDebut',
      label: 'Date de début',
      renderer: (value: any) => {
        return '-';
      },
    },
    {
      name: 'partenaireRepresentant.dateFin',
      label: 'Date de fin',
      renderer: (value: any) => {
        return '-';
      },
    },
    {
      name: 'partenaireRepresentantDemandeAffectation.remplacent.nom',
      label: 'Remplacant',
      renderer: (value: any) => {
        console.log('value : ', value);
        return (
          (value &&
            value.partenaireRepresentantDemandeAffectation &&
            value.partenaireRepresentantDemandeAffectation.remplacent &&
            value.partenaireRepresentantDemandeAffectation.remplacent.nom) ||
          '-'
        );
      },
    },
    {
      name: 'dateDebut',
      label: 'Date de début',
      renderer: (value: any) => {
        return value && value.dateFin && moment(value.dateDebut).format('L');
      },
    },
    {
      name: 'dateFin',
      label: 'Date de fin',
      renderer: (value: any) => {
        return value && value.dateFin && moment(value.dateFin).format('L');
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <TableActionColumn
            row={value}
            baseUrl={`${PARTENAIRE_SERVICE_URL}/affectation`}
            handleClickAffectation={onClickAffectation}
            customMenuItems={menuItems}
            showResendEmail={false}
          />
        );
      },
    },
  ];

  const tableProps = {
    isSelectable: true,
    paginationCentered: true,
    columns,
    selected,
    setSelected,
  };

  const DeleteDialogContent = () => {
    if (deleteRow) {
      const name = `${deleteRow.prenom || ''} ${deleteRow.nom || ''}`;
      return (
        <span>
          Êtes-vous sur de vouloir supprimer
          <span style={{ fontWeight: 'bold' }}> {name} </span>?
        </span>
      );
    }
    return <span>Êtes-vous sur de vouloir supprimer ces contacts ?</span>;
  };

  return (
    <Box width="100%">
      <div className={classes.searchInputBox}>
        <SearchInput searchPlaceholder="Rechercher un contact..." />
        <div>
          {selected && selected.length > 0 && (
            <CustomButton onClick={() => setOpenAffectationDialog(true)}>
              AFFECTATION GROUPEE
            </CustomButton>
          )}
          <CustomButton color="secondary" className={classes.btnAdd} onClick={goToCreate}>
            Ajout de contact
          </CustomButton>
        </div>
      </div>
      <WithSearch
        type="partenairerepresentantaffectation"
        props={tableProps}
        WrappedComponent={CustomContent}
        searchQuery={GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION}
        optionalMust={[{ term: { isRemoved: false } }]}
      />
      {affectationDialog}
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
    </Box>
  );
};
export default withRouter(AffectationList);
