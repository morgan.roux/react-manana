import React from 'react';
import { CIVILITE_LIST } from '../../../../../Constant/user';
import { createUpdateTitulaireVariables } from '../../../../../graphql/Titulaire/types/createUpdateTitulaire';
import { CustomDatePicker } from '../../../../Common/CustomDateTimePicker';
import CustomSelect from '../../../../Common/CustomSelect';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import FormContainer from '../../../../Common/FormContainer';
import InputRow from '../../../../Common/InputRow';
import useStyles from './styles';

interface IPartennaireFormProps {
  handleChange: (e: React.ChangeEvent<any>) => void;
  handleChangeDate: (date: string) => any;
  values: any;
}

const Step1: React.FC<IPartennaireFormProps> = ({ handleChange, values, handleChangeDate }) => {
  const classes = useStyles({});

  const {
    nom,
    dateDebut,
    dateFin,
    idTypeService,
    typePartenaire,
    statutPartenaire,
    adresse1,
    codePostal,
    ville,
  } = values;
  const typePartenaireList = [
    { label: 'Non partenaire', value: 'NON_PARTENAIRE' },
    { label: 'Négociation en cours', value: 'NEGOCIATION_EN_COURS' },
    { label: 'Nouveau référencement', value: 'NOUVEAU_REFERENCEMENT' },
    { label: 'Partenariat actif', value: 'PARTENARIAT_ACTIF' },
  ];

  const statutPartenaireList = [
    { label: 'Activé', value: 'ACTIVER' },
    { label: 'Bloqué', value: 'BLOQUER' },
  ];

  const typeServiceList = [
    { label: 'service type A', value: 'A' },
    { label: 'service type B', value: 'B' },
  ];
  const infoComponent = (
    <>
      <InputRow title="Nom" required={true}>
        <CustomFormTextField
          name="nom"
          value={nom}
          onChange={handleChange}
          placeholder="Nom du prestataire de service"
          required={true}
        />
      </InputRow>
      <InputRow title="Date début de partenariat" required={true}>
        <CustomDatePicker
          placeholder=""
          name="dateDebut"
          value={dateDebut}
          onChange={handleChangeDate('dateDebut')}
        />
      </InputRow>
      <InputRow title="Date fin de partenariat" required={true}>
        <CustomDatePicker
          placeholder=""
          name="dateFin"
          value={dateFin}
          onChange={handleChangeDate('dateFin')}
        />
      </InputRow>
      {/* <InputRow title="Services">
        <CustomSelect
          label="Type de services"
          list={typeServiceList}
          listId="value"
          index="label"
          name="idTypeService"
          value={idTypeService}
          onChange={handleChange}
          shrink={false}
        />
      </InputRow> */}
      <InputRow title="Type">
        <CustomSelect
          label="Type de partenariat"
          list={typePartenaireList}
          listId="value"
          index="label"
          name="typePartenaire"
          value={typePartenaire}
          onChange={handleChange}
        />
      </InputRow>

      <InputRow title="Statut">
        <CustomSelect
          label="Statut de partenariat"
          list={statutPartenaireList}
          listId="value"
          index="label"
          name="statutPartenaire"
          value={statutPartenaire}
          onChange={handleChange}
        />
      </InputRow>
    </>
  );
  const coordonneComponent = (
    <>
      <InputRow title="Adresse">
        <CustomFormTextField
          name="adresse1"
          value={adresse1}
          onChange={handleChange}
          placeholder="Son adresse"
        />
      </InputRow>

      <InputRow title="CP">
        <CustomFormTextField
          name="codePostal"
          value={codePostal}
          onChange={handleChange}
          placeholder="Son code postal"
        />
      </InputRow>
      <InputRow title="Ville">
        <CustomFormTextField
          name="ville"
          value={ville}
          onChange={handleChange}
          placeholder="Sa ville"
        />
      </InputRow>
    </>
  );

  return (
    <div className={classes.personnelGroupementFormRoot}>
      <FormContainer title="Informations">{infoComponent}</FormContainer>
      <FormContainer title="Coordonnées">{coordonneComponent}</FormContainer>
    </div>
  );
};

export default Step1;
