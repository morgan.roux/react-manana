import { Button } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import React from 'react';
import { ServicePartenaireInput } from '../../../../../types/graphql-global-types';
import FormContainer from '../../../../Common/FormContainer';
import ServicesForm from '../../servicesForm';

interface IStep2Props {
  values: any;
  setValues: any;
  handleChange: (e: React.ChangeEvent<any>, index) => void;
}

interface IservicePartenaires {
  id: string;
  nom: string;
  dateDemarrage: Date;
  nbrQualifiers: number;
  commentaire: string;
}

const Step2: React.FC<IStep2Props> = props => {
  const { values, setValues } = props;
  const { servicePartenaires } = values;

  const getRandomInt = (min: number, max: number) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };
  const initialServices = {
    id: getRandomInt(100000, 9999999),
    nom: '',
    commentaire: '',
    dateDemarrage: null,
    nbCollaboQualifie: 0,
  };

  const pushNewService = () => {
    setValues(prevState => ({
      ...prevState,
      servicePartenaires: [
        ...prevState.servicePartenaires,
        { ...initialServices, id: getRandomInt(100000, 9999999) },
      ],
    }));
  };

  const removeService = (id: number) => {
    const newServices =
      servicePartenaires && servicePartenaires.filter(service => service && service.id !== id);
    setValues(prevState => ({ ...prevState, servicePartenaires: newServices }));
  };

  const handleChangeDate = (name: string, itemIndex: number) => (date: any) => {
    const newServices =
      servicePartenaires &&
      servicePartenaires.map((service, index) => {
        if (itemIndex === index) {
          service[name] = date;
          return service;
        } else {
          return service;
        }
      });
    setValues(prevState => ({ ...prevState, servicePartenaires: newServices }));
  };

  const handleServiceChange = (event: any, itemIndex: number) => {
    const { name, value, type } = event.target;
    const elementValue = type === 'number' ? parseInt(value, 10) : value;
    const newServices =
      servicePartenaires &&
      servicePartenaires.map((service, index) => {
        if (itemIndex === index) {
          service[name] = elementValue;

          return service;
        } else {
          return service;
        }
      });
    setValues(prevState => ({ ...prevState, servicePartenaires: newServices }));
  };

  const listServices =
    servicePartenaires &&
    servicePartenaires.map((service, index) => (
      <ServicesForm
        removeService={() => removeService(service.id)}
        values={service}
        handleChange={handleServiceChange}
        handleChangeDate={handleChangeDate}
        index={index}
      />
    ));

  return (
    <div>
      <FormContainer title="Listes des services proposés">{listServices}</FormContainer>
      <Button
        fullWidth={true}
        startIcon={<Add />}
        style={{ border: 'dashed' }}
        onClick={pushNewService}
      >
        Ajouter un service
      </Button>
    </div>
  );
};

export default Step2;
