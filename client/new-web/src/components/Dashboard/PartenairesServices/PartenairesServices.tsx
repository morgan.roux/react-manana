import { useApolloClient, useQuery } from '@apollo/client';
import { Add } from '@material-ui/icons';
import React, { ChangeEvent, Fragment, useContext, useEffect, useMemo, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { ContentContext, ContentStateInterface } from '../../../AppContext';
import mutationSuccessImg from '../../../assets/img/mutation-success.png';
import { PARTENAIRE_SERVICE_URL } from '../../../Constant/url';
import { GET_CHECKEDS_PARTENAIRE_SERVICE } from '../../../graphql/PartenairesServices/local';
import { GET_PARTENAIRE_SERVICE } from '../../../graphql/PartenairesServices/query';
import {
  PARTENAIRE_SERVICE,
  PARTENAIRE_SERVICEVariables,
} from '../../../graphql/PartenairesServices/types/PARTENAIRE_SERVICE';
import {
  PartenaireServiceInput,
  ServicePartenaireInput,
  StatutPartenaire,
  TypePartenaire,
} from '../../../types/graphql-global-types';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import Backdrop from '../../Common/Backdrop';
import CustomButton from '../../Common/CustomButton';
import CustomContent from '../../Common/newCustomContent';
import SearchInput from '../../Common/newCustomContent/SearchInput';
import SubToolbar from '../../Common/newCustomContent/SubToolbar';
import NoItemContentImage from '../../Common/NoItemContentImage';
import Stepper, { Step } from '../../Common/Stepper/Stepper';
import { Step1, Step2 } from './stepper';
import useStyles from './styles';
import { usePartenaireServiceColumns } from './utils/partenaireServiceColumns';
import { useCheckedsPartenaireService } from './utils/useCheckedsPartenaireService';
import { useCreateUpdatePartenaireService } from './utils/useCreatePartenaireService';
import { useButtonHeadAction } from './utils/utils';

interface IPartenaireServiceProps {
  listResult: any;
  match: { params: { idPartenaire: string | undefined; id: string | undefined } };
}

const PartenaireService: React.FC<IPartenaireServiceProps & RouteComponentProps> = props => {
  const {
    listResult,
    history,
    history: { push },
    location: { pathname },
    match: {
      params: { idPartenaire, id },
    },
  } = props;

  const classes = useStyles({});

  const [openModal, setOpenModal] = useState<boolean>(false);
  const [openHistory, setOpenHistory] = useState<boolean>(false);
  const [disableNextButton, setDisableNextButton] = useState<boolean>(true);
  const [userId, setUserId] = useState<string>('');
  const [activeStep, setActiveStep] = useState<undefined | number>(0);
  const [userEmail, setUserEmail] = useState<string>('');
  const checkedsPartenaireService = useCheckedsPartenaireService();
  const isOnList =
    pathname === `/db/${PARTENAIRE_SERVICE_URL}` ||
    pathname === `/db/${PARTENAIRE_SERVICE_URL}/affectation` ||
    pathname === `/db/${PARTENAIRE_SERVICE_URL}/list`;
  const isOnContactDetail: boolean = pathname.includes('/contact/details');
  const isOnCreate = pathname === `/db/${PARTENAIRE_SERVICE_URL}/create` ? true : false;
  const isOnEdit: boolean = pathname.startsWith(`/db/${PARTENAIRE_SERVICE_URL}/edit`);
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const [operationNameState, setOperationName] = useState<any>();
  const [variablesState, setVariables] = useState<any>();
  const addOrEdit = isOnEdit ? 'Valider' : 'Ajouter';

  useEffect(() => {
    if (operationName && !operationNameState) {
      setOperationName(operationName);
      setVariables(variables);
    }
  }, [operationName]);

  const handleModal = (open: boolean, userId: string, email: string) => {
    setOpenModal(open);
    setUserId(userId);
    setUserEmail(email);
  };

  const finalStep = {
    buttonLabel: addOrEdit,
    action: () => {
      createUpdatePartenaireService();
    },
  };

  const initialState: PartenaireServiceInput = {
    id: '',
    nom: '',
    dateDebut: null,
    dateFin: null,
    idTypeService: '',
    typePartenaire: TypePartenaire.NON_PARTENAIRE,
    statutPartenaire: StatutPartenaire.ACTIVER,
    adresse1: '',
    codePostal: '',
    ville: '',
    servicePartenaires: [],
  };

  const client = useApolloClient();

  const { data: partenaireData, loading: partenaireLoading } = useQuery<
    PARTENAIRE_SERVICE,
    PARTENAIRE_SERVICEVariables
  >(GET_PARTENAIRE_SERVICE, {
    variables: { id: idPartenaire as string },
    skip: isOnEdit ? false : true,
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const [values, setValues] = useState(initialState);

  const handleClickHistory = (open: boolean, userId: string) => {
    setOpenHistory(open);
    setUserId(userId);
  };

  const [
    createUpdatePartenaireService,
    mutationSuccess,
    loading,
  ] = useCreateUpdatePartenaireService(
    { input: values },
    operationNameState,
    variablesState,
    isOnEdit,
  );

  const handleChangeDate = (name: string) => (date: any) => {
    setValues(prevState => ({ ...prevState, [name]: date }));
  };

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value, required, type } = e.target;
      console.log('name', type);
      setValues(prevState => ({ ...prevState, [name]: value }));
      console.log('values', value);
    }
  };

  const { nom, dateDebut, dateFin, servicePartenaires } = values;
  console.log('dateFin', dateFin);
  console.log('dateDebut', dateFin);

  useEffect(() => {
    const emptyServiceName =
      servicePartenaires && servicePartenaires.find((service: any) => service.nom === '');
    if (emptyServiceName) {
      setDisableNextButton(true);
    } else {
      setDisableNextButton(false);
    }

    if (dateFin === null || dateDebut === null || nom === null) {
      setDisableNextButton(true);
    }

    if (dateDebut > dateFin) {
      displaySnackBar(client, {
        isOpen: true,
        type: 'ERROR',
        message:
          'La date de début de partenariat doit être antérieure à la date de fin de partenariat',
      });
      setDisableNextButton(true);
    }
  }, [activeStep, values]);

  const goToHome = () => history.push(`/db/${PARTENAIRE_SERVICE_URL}`);

  const steps: Step[] = [
    {
      title: 'Information',
      content: (
        <Step1 handleChange={handleChange} values={values} handleChangeDate={handleChangeDate} />
      ),
    },
    {
      title: 'Liste des services',
      content: <Step2 handleChange={handleChange} values={values} setValues={setValues} />,
    },
  ];

  const stepperComponent = (
    <Stepper
      title={
        isOnEdit ? "Modification d'un partenaire de service" : "Ajout d'un partenaire de service"
      }
      steps={steps}
      backToHome={goToHome}
      disableNextBtn={disableNextButton}
      fullWidth={false}
      finalStep={finalStep}
      activeStepFromProps={activeStep}
      setActiveStepFromProps={setActiveStep}
    />
  );

  // const groupement = getGroupement();
  // const idGroupement = groupement && groupement.id;

  const [goToAddPartenaire, goBack] = useButtonHeadAction(push);
  const { columns, confirmDeleteDialog, setOpenDeleteDialog } = usePartenaireServiceColumns(
    history,
  );

  const mutationSuccessTitle = `${isOnCreate ? 'Ajout' : 'Modification'
    } du partenaire de service réussi`;
  const mutationSuccessSubTitle = `Le nouveau partenaire de service que vous venez ${isOnCreate ? "d'ajouter" : 'de modifier'
    } est désormais dans la liste des partenaires de service du groupement.`;

  //  const EDIT_URL = `/db/${PARTENAIRE_SERVICE_URL}/edit/${idPartenaire}`;

  // Set values on edit
  useMemo(() => {
    if (partenaireData && partenaireData.partenaire) {
      const p = partenaireData.partenaire;
      const servicePartenaires: ServicePartenaireInput[] = [];
      if (p.services && p.services.length > 0) {
        p.services.map(i => {
          if (i) {
            const spi: ServicePartenaireInput = {
              id: i.id,
              nom: i.nom,
              dateDemarrage: i.dateDemarrage,
              nbCollaboQualifie: i.nbCollaboQualifie,
              commentaire: i.commentaire,
            };
            servicePartenaires.push(spi);
          }
        });
      }

      setValues(prevState => ({
        ...prevState,
        id: p.id,
        adresse1: (p.partenaireServiceSuite && p.partenaireServiceSuite.adresse1) || '',
        codePostal: (p.partenaireServiceSuite && p.partenaireServiceSuite.codePostal) || '',
        ville: (p.partenaireServiceSuite && p.partenaireServiceSuite.ville) || '',
        dateDebut:
          (p.partenaireServicePartenaire && p.partenaireServicePartenaire.dateDebutPartenaire) ||
          null,
        dateFin:
          (p.partenaireServicePartenaire && p.partenaireServicePartenaire.dateFinPartenaire) ||
          null,
        // TODO : From back
        idTypeService: '',
        nom: p.nom || '',
        servicePartenaires,
        statutPartenaire:
          p.partenaireServicePartenaire && p.partenaireServicePartenaire.statutPartenaire,
        typePartenaire:
          p.partenaireServicePartenaire && p.partenaireServicePartenaire.typePartenaire,
      }));
    }
  }, [partenaireData]);

  const listPartenaire = (
    <CustomContent
      checkedItemsQuery={{
        name: 'checkedsPartenaireService',
        type: 'partenaire',
        query: GET_CHECKEDS_PARTENAIRE_SERVICE,
      }}
      isSelectable={true}
      listResult={listResult}
      columns={columns}
    />
  );

  const actionSuccess = (
    <div className={classes.mutationSuccessContainer}>
      <NoItemContentImage
        src={mutationSuccessImg}
        title={mutationSuccessTitle}
        subtitle={mutationSuccessSubTitle}
      >
        <CustomButton color="default" onClick={goBack}>
          Retour à la liste
        </CustomButton>
      </NoItemContentImage>
    </div>
  );

  const goToList = () => push(`/db/${PARTENAIRE_SERVICE_URL}`);

  // const goToAdd = () => push(`/db/${PARTENAIRE_SERVICE_URL}/create`);

  return (
    <div className={classes.container}>
      {partenaireLoading && <Backdrop />}
      {!isOnCreate && !isOnEdit && (
        <Fragment>
          <SubToolbar
            title={
              isOnCreate ? `Ajout nouveau partenaire service` : 'Liste des partenaires service'
            }
            dark={isOnCreate}
            withBackBtn={false}
            onClickBack={goToList}
          >
            {checkedsPartenaireService && checkedsPartenaireService.length > 0 && (
              <CustomButton onClick={() => setOpenDeleteDialog(true)}>
                SUPPRIMER LA SELECTION
              </CustomButton>
            )}
            <CustomButton color="secondary" startIcon={<Add />} onClick={goToAddPartenaire}>
              Ajout d'un partenaire
            </CustomButton>
          </SubToolbar>
          <div className={classes.searchInputBox}>
            <SearchInput searchPlaceholder="Rechercher un partenaire service" />
          </div>
        </Fragment>
      )}
      {loading && (
        <Backdrop
          value={`${isOnCreate ? 'Ajout' : 'Modification'
            } de partenaire de service en cours, veuillez patienter...`}
        />
      )}
      {mutationSuccess ? actionSuccess : isOnCreate || isOnEdit ? stepperComponent : listPartenaire}
      {confirmDeleteDialog}
    </div>
  );
};

export default withRouter(PartenaireService);
