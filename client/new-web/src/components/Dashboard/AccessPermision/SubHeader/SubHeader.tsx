import React, { FC } from 'react';
import ArrowBackspaceIcon from '@material-ui/icons/ArrowBack';
import CustomButton from '../../../Common/CustomButton';
import { IconButton, Box } from '@material-ui/core';
import { useStyles } from '../styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';

interface SubHeaderProps {
  handleSave(): void;
  roleName: string;
}

const SubHeader: FC<RouteComponentProps & SubHeaderProps> = ({
  history: { push },
  handleSave,
  roleName,
}) => {
  const classes = useStyles({});

  return (
    <Box className={classes.infoBar}>
      <Box>
        <IconButton className={classes.backIcon} onClick={() => push('/db/roles')}>
          <ArrowBackspaceIcon />
        </IconButton>
      </Box>
      <Box className={classes.title}>{roleName}</Box>
      <Box>
        <CustomButton
          className={classes.resetButton}
          color="default"
          onClick={() => push('/db/roles')}
        >
          annuler
        </CustomButton>
        <CustomButton className={classes.saveButton} color="secondary" onClick={handleSave}>
          enregistrer
        </CustomButton>
      </Box>
    </Box>
  );
};

export default withRouter(SubHeader);
