import React, { FC } from 'react';
import {
  List,
  ListItem,
  Divider,
  ListItemIcon,
  ListItemText,
  Checkbox,
  Box,
} from '@material-ui/core';
import { useStyles } from '../styles';

export interface ITraitement {
  nom: string;
  checked: boolean;
  code: string;
}

interface AccessPermisionListProps {
  listData: ITraitement[];
  allCheckedTraitement: string[];
  setAllCheckedTraitement(allCheckedTraitement: string[]): void;
}

const AccessPermisionList: FC<AccessPermisionListProps> = ({
  listData,
  allCheckedTraitement,
  setAllCheckedTraitement,
}) => {
  const classes = useStyles({});

  const handleChange = (code: string) => {
    const alreadyChecked = Boolean(allCheckedTraitement.find(checked => checked === code));
    if (alreadyChecked) {
      const newAllCheckedTraitement = allCheckedTraitement.filter(value => value !== code);
      setAllCheckedTraitement(newAllCheckedTraitement);
    } else {
      setAllCheckedTraitement([...allCheckedTraitement, ...[code]]);
    }
  };

  return (
    <List className={classes.list}>
      {listData.map((item, index: number) => {
        const { code } = item;
        const checked = Boolean(allCheckedTraitement.find(value => value === code));
        return (
          <Box key={index}>
            <ListItem className={classes.listItem}>
              <ListItemIcon>
                <Checkbox
                  checked={checked}
                  onChange={event => code && handleChange(code)}
                  edge="start"
                />
              </ListItemIcon>
              <ListItemText primary={item.nom} />
            </ListItem>
            {index !== listData.length - 1 && <Divider />}
          </Box>
        );
      })}
    </List>
  );
};

export default AccessPermisionList;
