import { makeStyles, Theme, createStyles, lighten } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    infoBar: {
      display: 'flex',
      height: 70,
      alignItems: 'center',
      justifyContent: 'space-between',
      padding: '0px 24px 0 24px',
      background: lighten(theme.palette.primary.main, 0.1),
      '@media (max-width: 692px)': {
        flexWrap: 'wrap',
        justifyContent: 'center',
      },
    },
    title: {
      color: theme.palette.common.white,
      fontWeight: 'bold',
      fontSize: 18,
    },
    tableTitle: {
      color: theme.palette.secondary.main,
      fontWeight: 'bold',
      fontSize: 18,
      paddingBottom: 12,
    },
    content: {
      width: '50%',
      paddingTop: 15,
    },
    container: {
      display: 'flex',
      justifyContent: 'center',
    },
    list: {
      border: '1px solid #DCDCDC',
      borderRadius: 12,
      marginTop: 15,
      paddingTop: 0,
      paddingBottom: 0,
    },
    listItem: {
      paddingTop: 10,
      paddingBottom: 10,
      paddingLeft: 20,
      paddingRight: 20,
      fontWeight: 'bold',
    },
    backIcon: {
      color: theme.palette.common.white,
    },
    resetButton: {
      marginRight: 5,
    },
    saveButton: {
      marginLeft: 5,
    },
  }),
);
