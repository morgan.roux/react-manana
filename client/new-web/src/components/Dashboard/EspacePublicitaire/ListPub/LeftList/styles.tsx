import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    listPubLeftListRoot: {
      display: 'flex',
      flexDirection: 'column',
      width: 533,
      minWidth: 400,
      padding: '15px 0px',
      borderRight: `1px solid rgba(0, 0, 0, 0.23)`,
      minHeight: 'calc(100vh - 156px)',
      '@media (max-width: 1630px)': {
        width: 458,
        minWidth: 346,
      },
      '@media (max-width: 1276px)': {
        width: 430,
        minWidth: 325,
      },
    },
    listPubLeftListCardsContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      borderTop: `1px solid rgba(0, 0, 0, 0.23)`,
      marginTop: 15,
      height: '-webkit-fill-available',
      padding: '0 20px',
    },
    listPubLeftListTitle: {
      fontWeight: 'bold',
      fontSize: 20,
      marginLeft: 23,
    },
    noPubContainer: {
      height: '100%',
      padding: '0px 50px',
      '& img': {
        maxWidth: 300,
        maxHeight: 200,
      },
    },
  }),
);

export default useStyles;
