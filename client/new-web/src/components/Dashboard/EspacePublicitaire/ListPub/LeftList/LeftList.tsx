import React, { FC, Fragment } from 'react';
import useStyles from './styles';
import PubCard from '../PubCard';
import { Typography, Box } from '@material-ui/core';
import NoItemListImage from '../../../../Common/NoItemListImage';
import { Waypoint } from 'react-waypoint';
import noPubImg from '../../../../../assets/img/no-list-pub.png';

interface LeftListProps {
  list: any;
  pubTotal: number;
  queryVariables: any;
  fetchMorePub: () => void;
}

const LeftList: FC<LeftListProps> = ({ list, queryVariables, fetchMorePub, pubTotal }) => {
  const classes = useStyles({});
  return (
    <div className={classes.listPubLeftListRoot}>
      <Typography className={classes.listPubLeftListTitle}>Liste de mes publicités</Typography>
      <div className={classes.listPubLeftListCardsContainer}>
        {list && list.length > 0 ? (
          list.map((item: any, index: number) => (
            <Fragment key={`pub_list_left_card_${index}`}>
              <PubCard pub={item} queryVariables={queryVariables} />
              {list && index === list.length - 1 && list.length !== pubTotal && (
                <Waypoint onEnter={fetchMorePub} />
              )}
            </Fragment>
          ))
        ) : (
          <div className={classes.noPubContainer}>
            <NoItemListImage
              src={noPubImg}
              title="Aucune publicité"
              subtitle="Vous n'avez pas encore ajouté de publicité à votre liste."
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default LeftList;
