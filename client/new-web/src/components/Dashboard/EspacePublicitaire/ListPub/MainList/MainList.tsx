import React, { FC, useState, useEffect, useCallback, ChangeEvent } from 'react';
import useStyles from './styles';
import {
  Typography,
  IconButton,
  TablePagination,
  makeStyles,
  Theme,
  createStyles,
  useTheme,
} from '@material-ui/core';
import { PUB_EMPLACEMENT_LIST, PUB_DELAIS_LIST } from '../../../../../Constant/publicite';
import { FirstPage, ChevronLeft, ChevronRight, LastPage } from '@material-ui/icons';
import { PubliciteInfo } from '../../../../../graphql/Publicite/types/PubliciteInfo';
import PubCard from '../PubCard';
import classnames from 'classnames';
import CustomSelect from '../../../../Common/CustomSelect';
import { keyBy } from 'lodash';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { v4 as uuid } from 'uuid';
import { useMutation, useApolloClient } from '@apollo/client';
import {
  UPDATE_ORDRE_PUBLICITE,
  UPDATE_ORDRE_PUBLICITEVariables,
} from '../../../../../graphql/Publicite/types/UPDATE_ORDRE_PUBLICITE';
import { DO_UPDATE_ORDRE_PUBLICITE } from '../../../../../graphql/Publicite';
import moment from 'moment';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { CODE_PUB_DELAIS } from '../../../../../Constant/parameter';
import { useParameter } from '../../../../../utils/getValueParameter';
import { DO_UPDATE_PARAMETER_VALUES } from '../../../../../graphql/Parametre/mutation';
import {
  UPDATE_PARAMETER_VALUES,
  UPDATE_PARAMETER_VALUESVariables,
} from '../../../../../graphql/Parametre/types/UPDATE_PARAMETER_VALUES';

interface MainListProps {
  list: any[];
  queryVariables: any;
  onClickFirstPage: () => void;
  onClickLastPage: () => void;
  onClickPrevPage: () => void;
  onClickNextPage: () => void;
}

const MainList: FC<MainListProps> = ({ list, queryVariables }) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const empTotal = PUB_EMPLACEMENT_LIST.length;
  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(6);

  // Get delais from param
  const delaisFromParam = useParameter(CODE_PUB_DELAIS);
  const delaisValue =
    delaisFromParam &&
    ((delaisFromParam.value && delaisFromParam.value.value) || delaisFromParam.defaultValue);
  const delaisId = delaisFromParam && delaisFromParam.id;
  const [delais, setDelais] = useState<string>(delaisValue || '');

  const handleChangePage = (page: number) => {
    setPage(page);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<any>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeDelais = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { value } = e.target;
      setDelais(value);
      if (delaisId) {
        updateParam({ variables: { parameters: [{ id: delaisId, value: value.toString() }] } });
      }
    }
  };

  const [updateParam] = useMutation<UPDATE_PARAMETER_VALUES, UPDATE_PARAMETER_VALUESVariables>(
    DO_UPDATE_PARAMETER_VALUES,
    {
      onCompleted: ({ updateParameterValues }) => {
        if (updateParameterValues && updateParameterValues.length > 0) {
          displaySnackBar(client, {
            type: 'SUCCESS',
            message: 'Délai mis à jour avec succès',
            isOpen: true,
          });
        }
      },
      onError: errors => {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, {
            type: 'ERROR',
            message: err.message,
            isOpen: true,
          });
        });
        displaySnackBar(client, {
          type: 'ERROR',
          message: errors.message,
          isOpen: true,
        });
      },
    },
  );

  const [columns, setColumns] = useState<any>(null);

  const updateColumns = useCallback((data: any) => {
    setColumns(data);
  }, []);

  /**
   * Set initial columns
   */
  useEffect(() => {
    const formattedData: any[] = PUB_EMPLACEMENT_LIST.map((emp, index) => {
      let findeds = list.filter(pub => pub && pub.ordre === emp.id);
      if (findeds && findeds.length > 0) {
        findeds = findeds.map(item => ({ ...item, uuid: uuid() }));
      }
      // return { ...emp, uuid: uuid(), items: finded ? [{ ...finded, uuid: uuid() }] : [] };

      return { ...emp, uuid: uuid(), items: findeds ? findeds : [] };
    });
    const formattedColumns = keyBy(formattedData, 'uuid');
    updateColumns(formattedColumns);
  }, [list]);

  const [doUpdateOrdrePub] = useMutation<UPDATE_ORDRE_PUBLICITE, UPDATE_ORDRE_PUBLICITEVariables>(
    DO_UPDATE_ORDRE_PUBLICITE,
  );

  const onDragEnd = (result, columns, setColumns) => {
    if (!result.destination) return;
    const { source, destination } = result;

    if (source.droppableId !== destination.droppableId) {
      const sourceColumn = columns[source.droppableId];
      const destColumn = columns[destination.droppableId];
      const sourceItems = [...sourceColumn.items];
      const destItems = [...destColumn.items];

      const doMove = () => {
        const [removed] = sourceItems.splice(source.index, 1);
        destItems.splice(destination.index, 0, removed);
        setColumns({
          ...columns,
          [source.droppableId]: {
            ...sourceColumn,
            items: sourceItems,
          },
          [destination.droppableId]: {
            ...destColumn,
            items: destItems,
          },
        });

        // Mutation update pub ordre
        if (removed) {
          doUpdateOrdrePub({ variables: { id: removed.id, ordre: destColumn.id } });
        }
      };

      if (destItems.length > 0) {
        const moved = sourceItems[source.index];
        const blockeds = destItems.map(item =>
          moment(moved.dateDebut).isBetween(item.dateDebut, item.dateFin, 'minute', '[]'),
        );
        if (blockeds.includes(true)) {
          displaySnackBar(client, {
            type: 'INFO',
            isOpen: true,
            message:
              'La période d’affichage sur cet emplacement est déjà prise, veuillez choisir un autre emplacement ou changer la période. Merci',
          });
        } else {
          doMove();
        }
      } else {
        doMove();
      }
    } else {
      const column = columns[source.droppableId];
      const copiedItems = [...column.items];
      const [removed] = copiedItems.splice(source.index, 1);
      copiedItems.splice(destination.index, 0, removed);
      setColumns({
        ...columns,
        [source.droppableId]: {
          ...column,
          items: copiedItems,
        },
      });
    }
  };

  return (
    <div className={classes.listPubMainListRoot}>
      <div className={classes.mainListHead}>
        <Typography className={classes.mainListHeadTitle}>
          Publicités en cours d'affichage
        </Typography>
        <div className={classes.pubDelaisContainer}>
          <Typography className={classes.pubDelaisText}>
            Délai d'affichage entre chaque publicité
          </Typography>
          <div className={classes.formContainer}>
            <CustomSelect
              labelle=""
              list={PUB_DELAIS_LIST}
              listId="id"
              index="value"
              name="delais"
              value={delais}
              onChange={handleChangeDelais}
              shrink={false}
            />
          </div>
        </div>
      </div>

      <div className={classes.mainListContainer}>
        <DragDropContext onDragEnd={result => onDragEnd(result, columns, setColumns)}>
          {columns &&
            Object.entries(columns)
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map(([columnId, column]: any, index) => {
                const isEmptyColumn =
                  column && column.items && column.items.length === 0 ? true : false;
                return (
                  <div className={classes.pubEmplacementContainer} key={columnId}>
                    <Typography className={classes.emplacementTitle}>
                      {column && column.value}
                    </Typography>
                    <div
                      className={
                        isEmptyColumn
                          ? classnames(classes.pubEmplacement, classes.pubEmplacementEmpty)
                          : classes.pubEmplacement
                      }
                    >
                      <Droppable
                        droppableId={columnId}
                        key={columnId}
                      // isDropDisabled={isEmptyColumn ? false : true}
                      >
                        {(provided, snapshot) => {
                          return (
                            <div
                              {...provided.droppableProps}
                              ref={provided.innerRef}
                              className={
                                snapshot.isDraggingOver
                                  ? classnames(classes.droppable, classes.droppableIisDraggingOver)
                                  : classes.droppable
                              }
                            >
                              {isEmptyColumn && !snapshot.isDraggingOver && (
                                <Typography className={classes.pubEmplacementText}>
                                  Glissez-déposez ici
                                </Typography>
                              )}
                              {column &&
                                column.items.map((pub, index) => {
                                  const marginTopCss = index * 60;
                                  return (
                                    <Draggable key={pub.uuid} draggableId={pub.uuid} index={index}>
                                      {(provided, snapshot) => {
                                        return (
                                          <div
                                            className={classes.draggable}
                                            ref={provided.innerRef}
                                            {...provided.draggableProps}
                                            {...provided.dragHandleProps}
                                            style={{
                                              ...provided.draggableProps.style,
                                              top: marginTopCss,
                                              position: snapshot.isDragging ? 'fixed' : 'absolute',
                                            }}
                                          >
                                            <PubCard pub={pub} queryVariables={queryVariables} />
                                          </div>
                                        );
                                      }}
                                    </Draggable>
                                  );
                                })}
                              {provided.placeholder}
                            </div>
                          );
                        }}
                      </Droppable>
                    </div>
                  </div>
                );
              })}
        </DragDropContext>
      </div>

      <div className={classes.paginationContainer}>
        <TablePagination
          component="div"
          count={empTotal}
          page={page}
          onChangePage={(event, page) => handleChangePage(page)}
          rowsPerPage={rowsPerPage}
          ActionsComponent={TablePaginationActions}
          labelDisplayedRows={({ from, to, count }) =>
            `${from} - ${to === -1 ? count : to} sur ${count}`
          }
          labelRowsPerPage="Nombre par page : "
          rowsPerPageOptions={[6, { label: 'Tous', value: -1 }]}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </div>
    </div>
  );
};

interface TablePaginationActionsProps {
  count: number;
  page: number;
  rowsPerPage: number;
  onChangePage: (event: React.MouseEvent<HTMLButtonElement>, newPage: number) => void;
}

const useStyles1 = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexShrink: 0,
      marginLeft: theme.spacing(2.5),
    },
  }),
);

const TablePaginationActions = (props: TablePaginationActionsProps) => {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPage /> : <FirstPage />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <ChevronRight /> : <ChevronLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <ChevronLeft /> : <ChevronRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPage /> : <LastPage />}
      </IconButton>
    </div>
  );
};

export default MainList;
