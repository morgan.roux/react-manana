import { Theme, createStyles, makeStyles, lighten } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    listPubMainListRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: '15px 0px',
      '& *': {
        fontFamily: 'Roboto',
      },
    },
    mainListHead: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      padding: '0px 25px 8px',
    },
    mainListHeadTitle: {
      fontWeight: 'bold',
      fontSize: 25,
    },
    pubDelaisContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      '& *': {
        fontFamily: 'Montserrat !important',
      },
    },
    pubDelaisText: {
      fontWeight: 'normal',
      fontSize: 14,
      fontFamily: 'Montserrat',
      color: '#878787',
      marginRight: 10,
    },
    mainListContainer: {
      padding: '25px 10px',
      borderTop: `1px solid rgba(0, 0, 0, 0.23)`,
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
    },
    formContainer: {
      width: 100,
      '& *': {
        fontWeight: '600',
      },
      '& .MuiFormControl-root': {
        marginBottom: 0,
        '& .MuiOutlinedInput-input': {
          padding: '5px 14px !important',
        },
        '& fieldset > legend > span': {
          display: 'none !important',
        },
      },
    },
    pubEmplacementContainer: {
      display: 'flex',
      flexDirection: 'column',
      margin: 15,
      width: '30%',
      '@media (max-width: 1600px)': {
        width: '45%',
      },
      '@media (max-width: 1248px)': {
        width: '70%',
      },
    },
    pubEmplacement: {
      minWidth: 250,
      minHeight: 299,
      borderRadius: 5,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& .MuiCard-root': {
        minWidth: '250px !important',

        margin: '0px !important',
      },
    },
    pubEmplacementEmpty: {
      border: `1px dashed rgba(0, 0, 0, 0.23)`,
    },
    emplacementTitle: {
      fontWeight: 'bold',
      marginBottom: 10,
      fontSize: 16,
    },
    pubEmplacementText: {
      fontWeight: 'bold',
      fontSize: 20,
      color: '#9E9E9E',
    },
    paginationContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      margin: '30px 0px 50px',
      height: 15,
      '& button, & p': {
        color: theme.palette.common.black,
      },
    },
    paginationInfo: {
      fontSize: 12,
      marginRight: 40,
    },
    droppable: {
      position: 'relative',
      overflow: 'hidden',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      background: theme.palette.common.white,
      width: '100%',
      minHeight: 310,
      '& > div:not(:nth-child(1))': {
        // position: 'absolute',
        // top: 65,
      },
    },
    droppableIisDraggingOver: {
      // flexDirection: 'row',
      background: lighten(theme.palette.secondary.main, 0.8),
      '& > div': {
        // position: 'relative',
        top: '60px !important',
      },
    },
    draggable: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      userSelect: 'none',
      minHeight: 'fit-content',
      color: 'white',
      width: '99%',
    },
  }),
);

export default useStyles;
