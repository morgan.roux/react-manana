import { useApolloClient, useMutation, useQuery } from '@apollo/client';
import { Box } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import { AxiosResponse } from 'axios';
import moment from 'moment';
import React, { FC, Fragment, useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import mutationSuccessImg from '../../../assets/img/mutation-success.png';
import { PUB_BASE_URL } from '../../../Constant/publicite';
import { GET_CHECKEDS_ACTUALITE } from '../../../graphql/Actualite/local';
import { GET_CHECKEDS_COMMANDE } from '../../../graphql/Commande/local';
import { GET_CHECKEDS_LABORATOIRE } from '../../../graphql/Laboratoire/local';
import { GET_CHECKEDS_OPERATION } from '../../../graphql/OperationCommerciale/local';
import { GET_CHECKEDS_PRODUIT } from '../../../graphql/ProduitCanal/local';
import { GET_CHECKEDS_PROJECT } from '../../../graphql/Project/local';
import { DO_CREATE_UPDATE_PUBLICITE } from '../../../graphql/Publicite';
import {
  CREATE_UPDATE_PUBLICITE,
  CREATE_UPDATE_PUBLICITEVariables,
} from '../../../graphql/Publicite/types/CREATE_UPDATE_PUBLICITE';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import SnackVariableInterface from '../../../Interface/SnackVariableInterface';
import { AppAuthorization } from '../../../services/authorization';
import { getGroupement, getUser } from '../../../services/LocalStorage';
import { uploadToS3 } from '../../../services/S3';
import { FichierInput, OriginePublicite, TypeFichier } from '../../../types/graphql-global-types';
import { formatFilenameWithoutDate } from '../../../utils/filenameFormater';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import Backdrop from '../../Common/Backdrop';
import CustomButton from '../../Common/CustomButton';
import SearchInput from '../../Common/newCustomContent/SearchInput';
import NoItemContentImage from '../../Common/NoItemContentImage';
import ActuColumns from '../../Main/Content/Pilotage/Cible/Columns/Actu';
import LaboColumns from '../../Main/Content/Pilotage/Cible/Columns/Labo';
import OcColumns from '../../Main/Content/Pilotage/Cible/Columns/Oc';
import ProduitCanalColumns from '../../Main/Content/Pilotage/Cible/Columns/ProduitCanal';
import ProjectColumns from '../../Main/Content/Pilotage/Cible/Columns/Project';
import ItemTable from '../../Main/Content/Pilotage/Cible/Steps/ItemTable';
import FormPub, { FormValidation, StepInterface, StepOne, StepTwo } from './FormPub/FormPub';
import usePubForm from './FormPub/useFormPub';
import ListPub from './ListPub';
import useStyles from './styles';
import SubHead from './SubHead';

interface EspacePublicitaireProps {
  match: { params: { pubId: string } };
}

const EspacePublicitaire: FC<EspacePublicitaireProps & RouteComponentProps> = ({
  history: { push },
  location: { pathname },
  match: {
    params: { pubId },
  },
}) => {
  const CREATE_URL = `${PUB_BASE_URL}/create`;
  const EDIT_URL = `${PUB_BASE_URL}/edit/${pubId}`;
  const classes = useStyles({});
  const [activeStepId, setActiveStepId] = useState<number>(0);
  const { values, setValues, handleChange, handleChangeDate } = usePubForm();
  const [nextBtnDisabled, setNextBtnDisabled] = useState<boolean>(true);
  const [formIsValid, setFormIsValid] = useState<FormValidation>({
    stepOne: false,
    stepTwo: false,
  });
  const [mutationSucces, setmutationSucces] = useState<boolean>(false);
  const client = useApolloClient();
  const [selectedFiles, setSelectedFiles] = useState<File[] | undefined>([]);
  let uploadResult: AxiosResponse<any> | null = null;
  let pubImage: FichierInput | null = null;
  const [loading, setLoading] = useState<boolean>(false);
  const groupement = getGroupement();

  const user = getUser();
  const auth = new AppAuthorization(user);

  const checkedsProduitQuery = useQuery(GET_CHECKEDS_PRODUIT);
  const checkedsOperationQuery = useQuery(GET_CHECKEDS_OPERATION);
  const checkedsActualiteQuery = useQuery(GET_CHECKEDS_ACTUALITE);
  const checkedsLaboratoireQuery = useQuery(GET_CHECKEDS_LABORATOIRE);
  const checkedsCommandeQuery = useQuery(GET_CHECKEDS_COMMANDE);
  const checkedsProjectQuery = useQuery(GET_CHECKEDS_PROJECT);

  const { dateDebut, dateFin } = values;

  useEffect(() => {
    const checkedsOperation =
      checkedsOperationQuery &&
      checkedsOperationQuery.data &&
      checkedsOperationQuery.data.checkedsOperation;

    const checkedsProduit =
      checkedsProduitQuery &&
      checkedsProduitQuery.data &&
      checkedsProduitQuery.data.checkedsProduit;

    const checkedsActualite =
      checkedsActualiteQuery &&
      checkedsActualiteQuery.data &&
      checkedsActualiteQuery.data.checkedsActualite;

    const checkedsLaboratoire =
      checkedsLaboratoireQuery &&
      checkedsLaboratoireQuery.data &&
      checkedsLaboratoireQuery.data.checkedsLaboratoire;

    const checkedsCommande =
      checkedsCommandeQuery &&
      checkedsCommandeQuery.data &&
      checkedsCommandeQuery.data.checkedsCommande;

    const checkedsProject =
      checkedsProjectQuery &&
      checkedsProjectQuery.data &&
      checkedsProjectQuery.data.checkedsProject;

    if (checkedsOperation && checkedsOperation.length > 0) {
      setValues(prevState => ({
        ...prevState,
        idItemSource: checkedsOperation[0] && checkedsOperation[0].id,
        items: checkedsOperation,
      }));
    } else if (checkedsProduit && checkedsProduit.length) {
      setValues(prevState => ({
        ...prevState,
        idItemSource: checkedsProduit[0] && checkedsProduit[0].id,
        items: checkedsProduit,
      }));
    } else if (checkedsActualite && checkedsActualite.length) {
      setValues(prevState => ({
        ...prevState,
        idItemSource: checkedsActualite[0] && checkedsActualite[0].id,
        items: checkedsActualite,
      }));
    } else if (checkedsLaboratoire && checkedsLaboratoire.length) {
      setValues(prevState => ({
        ...prevState,
        idItemSource: checkedsLaboratoire[0] && checkedsLaboratoire[0].id,
        items: checkedsLaboratoire,
      }));
    } else if (checkedsCommande && checkedsCommande.length) {
      setValues(prevState => ({
        ...prevState,
        idItemSource: checkedsCommande[0] && checkedsCommande[0].id,
        items: checkedsCommande,
      }));
    } else if (checkedsProject && checkedsProject.length) {
      setValues(prevState => ({
        ...prevState,
        idItemSource: checkedsProject[0] && checkedsProject[0].id,
        items: checkedsProject,
      }));
    } else {
      setValues(prevState => ({
        ...prevState,
        idItemSource: null,
        items: [],
      }));
    }
  }, [
    checkedsOperationQuery,
    checkedsProduitQuery,
    checkedsActualiteQuery,
    checkedsCommandeQuery,
    checkedsLaboratoireQuery,
    checkedsProjectQuery,
  ]);

  const getColumns = (item: any) => {
    let column: any[] = [];
    if (item && item.codeItem) {
      switch (item.codeItem.startsWith('A')) {
        case true:
          column = OcColumns;
          break;
      }
      switch (item.codeItem.startsWith('C')) {
        case true:
          column = LaboColumns;
          break;
      }
      switch (item.codeItem.startsWith('B')) {
        case true:
          column = ActuColumns;
          break;
      }
      switch (item.codeItem.startsWith('K')) {
        case true:
          column = ProduitCanalColumns;
          break;
      }
      switch (item.codeItem.startsWith('L')) {
        case true:
          column = ProjectColumns;
          break;
      }
    }
    return column;
  };

  const columns = values && values.item ? getColumns(values.item) : OcColumns;
  const IMAGE_EXIST = values && values.image && values.image.chemin && values.image.nomOriginal;

  const externeSteps: StepInterface[] = [
    {
      id: 0,
      label: 'Détails',
      content: (
        <StepOne
          values={values}
          setValues={setValues}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          selectedFiles={selectedFiles}
          setSelectedFiles={setSelectedFiles}
        />
      ),
    },
    {
      id: 1,
      label: 'Réglages',
      content: (
        <StepTwo
          values={values}
          setValues={setValues}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
        />
      ),
    },
  ];

  const interneSteps: StepInterface[] = [
    {
      id: 0,
      label: 'Détails',
      content: (
        <StepOne
          values={values}
          setValues={setValues}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          selectedFiles={selectedFiles}
          setSelectedFiles={setSelectedFiles}
        />
      ),
    },
    {
      id: 1,
      label:
        values.item && values.item.name
          ? `Choix de la source dans ${values.item.name}`
          : `Choix de la source`,
      content: (
        <Box>
          <div className={classes.searchInputBox}>
            <SearchInput searchPlaceholder="Rechercher ..." />
          </div>
          <ItemTable
            setAllValues={setValues}
            allValues={values}
            columns={columns}
            setNextBtnDisabled={setNextBtnDisabled}
          />
        </Box>
      ),
    },
    {
      id: 2,
      label: 'Réglages',
      content: (
        <StepTwo
          values={values}
          setValues={setValues}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
        />
      ),
    },
  ];

  const steps: StepInterface[] =
    values.origine === OriginePublicite.EXTERNE ? externeSteps : interneSteps;

  const isOnList: boolean = pathname === PUB_BASE_URL;
  const isOnCreate: boolean = pathname === CREATE_URL;
  const isOnEdit: boolean = pathname === EDIT_URL;
  const isOnFirstStep: boolean = activeStepId === 0;
  const isOnLastStep: boolean = activeStepId >= steps.length - 1;
  const nextBtnLabel = isOnLastStep ? (isOnCreate ? 'Créer' : 'Modifier') : 'Suivant';

  const title = isOnList
    ? `Configuration de l'espace publicitaire`
    : `${isOnCreate ? 'Création' : 'Modification'} d'une publicité`;

  const loadingMsg = `${isOnCreate ? 'Création' : isOnEdit ? 'Modification' : 'Chargement'
    } en cours...`;

  const mutationSuccessTitle = `${isOnCreate ? 'Création' : 'Modification'
    } d'une publicité réussie`;

  const mutationSuccessSubTitle = `La publicité que vous venez de ${isOnCreate ? 'créer' : 'modifier'
    } est maintenant dans la liste.`;

  const disabledBtnNext =
    (isOnFirstStep && !formIsValid.stepOne) ||
    (isOnLastStep && !formIsValid.stepTwo) ||
    (!isOnLastStep && !isOnFirstStep && nextBtnDisabled);

  const goToCreate = () => push(CREATE_URL);

  const goToList = () => {
    setmutationSucces(false);
    push(PUB_BASE_URL);
  };

  const handleNext = () => {
    setActiveStepId(prevActiveStep => prevActiveStep + 1);
  };

  const handleBack = () => {
    if (activeStepId > 0) {
      if (activeStepId === steps.length) {
        setActiveStepId(prevActiveStep => prevActiveStep - 2);
      } else {
        setActiveStepId(prevActiveStep => prevActiveStep - 1);
      }
    } else {
      goToList();
    }
  };

  // const handleReset = () => {
  //   setActiveStepId(0);
  // };

  const { codeItem, idItemSource, items, item } = values;

  /**
   * Update items (get last element on items), codeItem
   */
  useEffect(() => {
    // if (items.length > 1) {
    //   const i = last(items);
    //   if (i && i.id !== idItemSource) {
    //     setValues(prevState => ({ ...prevState, idItemSource: i.id, items: [i] }));
    //   }
    // } else if (items.length === 0) {
    //   if (items.length !== 0) setValues(prevState => ({ ...prevState, items: [] }));
    //   if (idItemSource !== null) setValues(prevState => ({ ...prevState, idItemSource: null }));
    // }

    if (item && item.code !== codeItem) {
      setValues(prevState => ({ ...prevState, codeItem: item.code }));
    }
  }, [items, idItemSource, item, codeItem]);

  /**
   * Upload Publicite Image
   */
  const [uploadFiles, uploadFilesResult] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (data && data.createPutPresignedUrls) {
        Promise.all(
          data.createPutPresignedUrls.map(async (presignedFile, index) => {
            // Upload to s3
            if (
              presignedFile &&
              selectedFiles &&
              selectedFiles.length > 0 &&
              selectedFiles.map(file =>
                presignedFile.filePath.includes(formatFilenameWithoutDate(file)),
              )
            ) {
              // Set fichier chemin and upload to s3
              selectedFiles.map(item => {
                if (presignedFile.filePath.includes(formatFilenameWithoutDate(item))) {
                  pubImage = {
                    type: TypeFichier.PHOTO,
                    nomOriginal: item.name,
                    chemin: presignedFile.filePath,
                  };
                  setValues((prevState: any) => ({ ...prevState, image: pubImage }));
                }
              });
              await uploadToS3(selectedFiles[index], presignedFile.presignedUrl)
                .then(result => {
                  if (result && result.status === 200) {
                    uploadResult = result;
                  }
                })
                .catch(error => {
                  setLoading(false);
                  console.log(error);
                  const snackBarData: SnackVariableInterface = {
                    type: 'ERROR',
                    message:
                      "Erreur lors de l'envoye de l'image. Si vous utilisez AdBlocker, désactivez-le et réessayez.",
                    isOpen: true,
                  };
                  displaySnackBar(client, snackBarData);
                });
            }
          }),
        ).then(_ => {
          // Execute doCreateUpdatePub
          if (
            !disabledBtnNext &&
            uploadResult &&
            uploadResult.status === 200 &&
            values &&
            pubImage &&
            dateDebut &&
            dateFin
          ) {
            doCreateUpdatePub({
              variables: {
                ...values,
                dateDebut: moment(dateDebut)
                  .utc()
                  .format(),
                dateFin: moment(dateFin)
                  .utc()
                  .format(),
                image: pubImage,
              },
            });
          } else {
            setLoading(false);
          }
        });
      }
    },
    onError: errors => {
      setLoading(false);
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  /**
   * Mutation Create & Update Publicite
   */
  const [doCreateUpdatePub, doCreateUpdatePubResult] = useMutation<
    CREATE_UPDATE_PUBLICITE,
    CREATE_UPDATE_PUBLICITEVariables
  >(DO_CREATE_UPDATE_PUBLICITE, {
    onCompleted: data => {
      if (data && data.createUpdatePublicite) {
        setmutationSucces(true);
        setLoading(false);
      } else {
        displaySnackBar(client, {
          type: 'ERROR',
          message: `Erreur lors de la ${isOnCreate ? 'création' : 'modification'}`,
          isOpen: true,
        });
      }
    },
    onError: errors => {
      setLoading(false);
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  const handleCreateUpdatePublicite = () => {
    // If form is valid
    // Start loading
    setLoading(true);
    if (values && selectedFiles && selectedFiles.length > 0) {
      // Presigned file
      const filePathsTab: string[] = [];
      selectedFiles.map((file: File) => {
        filePathsTab.push(
          `publicites/${groupement && groupement.id}/${formatFilenameWithoutDate(file)}`,
        );
      });
      // Create presigned url
      uploadFiles({ variables: { filePaths: filePathsTab } });
    }

    // EDIT WITHOUT NEW IMAGE
    if (
      isOnEdit &&
      values &&
      IMAGE_EXIST &&
      selectedFiles &&
      selectedFiles.length === 0 &&
      !disabledBtnNext &&
      dateDebut &&
      dateFin
    ) {
      // Execute doCreateUpdatePub
      doCreateUpdatePub({
        variables: {
          ...values,
          dateDebut: moment(dateDebut)
            .utc()
            .format(),
          dateFin: moment(dateFin)
            .utc()
            .format(),
        },
      });
    }
  };

  const StepBtns = () => {
    return (
      <Fragment>
        <CustomButton color="default" onClick={handleBack}>
          Retour
        </CustomButton>
        <CustomButton
          color="secondary"
          onClick={isOnLastStep ? handleCreateUpdatePublicite : handleNext}
          disabled={disabledBtnNext}
        >
          {nextBtnLabel}
        </CustomButton>
      </Fragment>
    );
  };

  const AddBtn = () => {
    return (
      <CustomButton
        color="secondary"
        startIcon={<EditIcon />}
        onClick={goToCreate}
        disabled={!auth.isAuthorizedToAddPub()}
      >
        Ajouter une pub
      </CustomButton>
    );
  };

  const MutationSuccessBtn = () => {
    return (
      <CustomButton color="default" onClick={goToList}>
        Retour à la liste
      </CustomButton>
    );
  };

  const onClickBack = () => push('/db/espace-publicitaire');

  return (
    <div className={classes.espacePubRoot}>
      {(loading || uploadFilesResult.loading || doCreateUpdatePubResult.loading) && (
        <Backdrop value={loadingMsg} />
      )}

      <SubHead title={title} showBtnBack={!isOnList} onClickBack={onClickBack}>
        {mutationSucces ? (
          <MutationSuccessBtn />
        ) : (
          <Fragment>
            {(isOnCreate || isOnEdit) && <StepBtns />}
            {isOnList && <AddBtn />}
          </Fragment>
        )}
      </SubHead>

      {mutationSucces ? (
        <div className={classes.mutationSuccessContainer}>
          <NoItemContentImage
            src={mutationSuccessImg}
            title={mutationSuccessTitle}
            subtitle={mutationSuccessSubTitle}
          />
        </div>
      ) : (
        <>
          {isOnList && <ListPub />}
          {(isOnCreate || isOnEdit) && (
            <FormPub
              steps={steps}
              activeStepId={activeStepId}
              setActiveStepId={setActiveStepId}
              formIsValid={formIsValid}
              setFormIsValid={setFormIsValid}
              values={values}
              setValues={setValues}
              handleChange={handleChange}
              handleChangeDate={handleChangeDate}
              selectedFiles={selectedFiles}
            />
          )}
        </>
      )}
    </div>
  );
};

export default EspacePublicitaire;
