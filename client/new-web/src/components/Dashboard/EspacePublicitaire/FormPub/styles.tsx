import { Theme, createStyles, makeStyles, lighten } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formPubRootContainer: {
      padding: '0 16px',
      width: '100%',
      height: 'calc(100vh - 156px)',
      overflow: 'auto',
    },
    formPubRoot: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      paddingTop: 40,
      maxWidth: 720,
      margin: '0 auto',
      '& *': {
        fontFamily: 'Montserrat',
        letterSpacing: 0,
      },
      '& .MuiStepper-root': {
        padding: '0px !important',
      },
      '& .MuiPaper-root': {
        boxShadow: 'none !important',
        minWidth: 560,
      },
      '& .MuiStepIcon-root.MuiStepIcon-active, & .MuiStepIcon-root.MuiStepIcon-completed': {
        color: theme.palette.secondary.main,
        '& .MuiStepIcon-text': {
          fill: theme.palette.common.white,
        },
      },
      '& .MuiStepIcon-root': {
        color: lighten(theme.palette.common.black, 0.9),
        // marginLeft: -20,
        // zIndex: 1,

        '& .MuiStepIcon-text': {
          fill: lighten('#878787', 0.3),
        },
      },
      '& label, & input, & textarea, & .MuiSelect-select.MuiSelect-select': {
        fontWeight: '600',
      },
    },
    formPubRootNoMaxWidth: {
      maxWidth: 'none',
      '& .MuiPaper-root': {
        minWidth: 800,
      },
    },
    formContainer: {
      marginBottom: 40,
      '& > div': {
        margin: 0,
      },
    },
    formSubTitle: {
      color: theme.palette.secondary.main,
      fontSize: 18,
      fontWeight: 'bold',
      margin: '25px 0px',
    },
    formPubDropzone: {
      '& > div > div': {
        margin: 0,
      },
    },
    formPubTextArea: {
      marginTop: '25px !important',
    },
    formPubRadioGroup: {
      flexDirection: 'row',
      marginTop: '-16px !important',
      marginBottom: '16px !important',
    },
    formPubEmplacement: {
      border: `1px solid rgba(0, 0, 0, 0.23)`,
      borderRadius: 12,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      padding: '15px 25px',
      '& > p': {
        fontSize: '1rem',
        color: 'rgba(0, 0, 0, 0.54)',
        fontWeight: '600',
      },
      '& > div': {
        marginBottom: 0,
        maxWidth: 250,
        '& fieldset > legend > span': {
          display: 'none !important',
        },
      },
    },
    dateInput: {
      marginBottom: 25,
    },
    fieldset: {
      borderRadius: 5,
      border: '1px solid rgba(0, 0, 0, 0.23)',
      '&:hover': {
        border: `1px solid ${theme.palette.secondary.main}`,
        '& label': {
          color: theme.palette.primary.main,
        },
      },
      '& label': {
        fontWeight: '600',
        fontSize: 14,
        color: theme.palette.common.black,
      },
    },
  }),
);

export default useStyles;
