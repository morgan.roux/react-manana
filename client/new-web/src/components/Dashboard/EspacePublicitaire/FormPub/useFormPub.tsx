import { useState, ChangeEvent } from 'react';
import { FichierInput, TypeEspace, OriginePublicite } from '../../../../types/graphql-global-types';

export interface PubInterface {
  id: string | null;
  libelle: string | null;
  description: string | null;
  typeEspace: TypeEspace;
  origine: OriginePublicite;
  url: string | null;
  ordre: number | null;
  codeItem: string | null;
  idItemSource: string | null;
  image: FichierInput | null;
  dateDebut: any;
  dateFin: any;
  item: any;
  items: any[];
}

export const initialState: PubInterface = {
  id: null,
  libelle: null,
  description: null,
  typeEspace: TypeEspace.PUBLICITE,
  origine: OriginePublicite.EXTERNE,
  url: null,
  ordre: null,
  codeItem: null,
  idItemSource: null,
  image: null,
  dateDebut: null,
  dateFin: null,
  item: null,
  items: [],
};

const usePubForm = (defaultState?: PubInterface, callback?: (data: PubInterface) => void) => {
  const initValues: PubInterface = defaultState || initialState;
  const [values, setValues] = useState<PubInterface>(initValues);

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      setValues(prevState => ({ ...prevState, [name]: value }));
    }
  };

  const handleChangeDate = (name: string) => (date: any) => {
    if (date && name) {
      setValues(prevState => ({ ...prevState, [name]: date }));
    }
  };

  return {
    handleChange,
    handleChangeDate,
    values,
    setValues,
  };
};

export default usePubForm;
