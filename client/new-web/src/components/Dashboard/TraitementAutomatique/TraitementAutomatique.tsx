import { useMutation, useQuery, useApolloClient } from '@apollo/client';
import { TraitementAutomatiquePage } from '@app/ui-kit';
import React, { FC, useState } from 'react';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import {
  CREATE_TRAITEMENT_AUTOMATIQUE,
  UPDATE_TRAITEMENT_AUTOMATIQUE,
  DELETE_TRAITEMENT_AUTOMATIQUE,
} from '../../../federation/auto/traitement-automatique/mutation';
import {
  GET_TRAITEMENT_AUTOMATIQUES_TYPES,
  GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION,
} from '../../../federation/auto/traitement-automatique/query';
import {
  CREATE_TRAITEMENT_AUTOMATIQUE as CREATE_TRAITEMENT_AUTOMATIQUE_TYPE,
  CREATE_TRAITEMENT_AUTOMATIQUEVariables,
} from '../../../federation/auto/traitement-automatique/types/CREATE_TRAITEMENT_AUTOMATIQUE';
import {
  GET_TRAITEMENT_AUTOMATIQUES_TYPES as GET_TRAITEMENT_AUTOMATIQUES_TYPES_TYPE,
  GET_TRAITEMENT_AUTOMATIQUES_TYPESVariables,
} from '../../../federation/auto/traitement-automatique/types/GET_TRAITEMENT_AUTOMATIQUES_TYPES';
import {
  GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION as GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_TYPE,
  GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTIONVariables,
} from '../../../federation/auto/traitement-automatique/types/GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION';
import {
  UPDATE_TRAITEMENT_AUTOMATIQUE as UPDATE_TRAITEMENT_AUTOMATIQUE_TYPE,
  UPDATE_TRAITEMENT_AUTOMATIQUEVariables,
} from '../../../federation/auto/traitement-automatique/types/UPDATE_TRAITEMENT_AUTOMATIQUE';
import {
  DELETE_TRAITEMENT_AUTOMATIQUE as DELETE_TRAITEMENT_AUTOMATIQUE_TYPE,
  DELETE_TRAITEMENT_AUTOMATIQUEVariables,
} from '../../../federation/auto/traitement-automatique/types/DELETE_TRAITEMENT_AUTOMATIQUE';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import { getPharmacie } from './../../../services/LocalStorage';
import { SEARCH_search_data_TATraitement } from './../../../federation/search/types/SEARCH';
import { useSearch } from './../../Common/useSearch';
import CommonFieldsForm from '../../Common/CommonFieldsForm';

const TraitementAutomatique: FC<{}> = () => {
  const client = useApolloClient();
  const [search, searchResult] = useSearch<SEARCH_search_data_TATraitement>();
  const [saved, setSaved] = useState<boolean>(false);
  const [saving, setSaving] = useState<boolean>(false);
  const [isPrivate, setIsPrivate] = useState<boolean>(false);

  const pharmacie: any = getPharmacie();

  const idPharmacie = pharmacie.id;

  const filterTraitementAutomatique = {
    filter: {
      idPharmacie: {
        eq: idPharmacie,
      },
    },
  };

  const loadTraitementTypes = useQuery<
    GET_TRAITEMENT_AUTOMATIQUES_TYPES_TYPE,
    GET_TRAITEMENT_AUTOMATIQUES_TYPESVariables
  >(GET_TRAITEMENT_AUTOMATIQUES_TYPES, {
    client: FEDERATION_CLIENT,
  });

  const [createTraitement, creatingTraitement] = useMutation<
    CREATE_TRAITEMENT_AUTOMATIQUE_TYPE,
    CREATE_TRAITEMENT_AUTOMATIQUEVariables
  >(CREATE_TRAITEMENT_AUTOMATIQUE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le traitement récurrent a été créé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      setState([]);

      setSaved(true);
      setSaving(false);

      searchResult.refetch();
    },
    /*update: (cache, result) => {
      if (result.data?.createOneTATraitement) {
        const previousList = cache.readQuery<
          GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_TYPE,
          GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTIONVariables
        >({
          query: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION,
          variables: filterTraitementAutomatique,
        })?.tATraitements;

        cache.writeQuery<
          GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_TYPE,
          GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTIONVariables
        >({
          query: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION,
          variables: filterTraitementAutomatique,
          data: {
            tATraitements: [...(previousList || []), result.data.createOneTATraitement],
          },
        });
      }
    },*/

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la création du traitement récurrent',
        type: 'ERROR',
        isOpen: true,
      });

      setSaving(false);
    },
    client: FEDERATION_CLIENT,
  });

  const [updateTraitement, updatingTraitement] = useMutation<
    UPDATE_TRAITEMENT_AUTOMATIQUE_TYPE,
    UPDATE_TRAITEMENT_AUTOMATIQUEVariables
  >(UPDATE_TRAITEMENT_AUTOMATIQUE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le traitement récurrent a été modifié avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      setState([]);

      setSaved(true);
      setSaving(false);
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la modification du traitement récurrent',
        type: 'ERROR',
        isOpen: true,
      });

      setSaving(false);
    },
    client: FEDERATION_CLIENT,
  });

  const [deleteTraitement, deletingTraitement] = useMutation<
    DELETE_TRAITEMENT_AUTOMATIQUE_TYPE,
    DELETE_TRAITEMENT_AUTOMATIQUEVariables
  >(DELETE_TRAITEMENT_AUTOMATIQUE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le traitement récurrent a été supprimé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });

      // Temporary solution

      setTimeout(() => {
        searchResult.refetch();
      }, 2000);
    },
    /* update: (cache, result) => {
       if (result.data?.deleteOneTATraitement) {
         const previousList = cache.readQuery<
           GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_TYPE,
           GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTIONVariables
         >({
           query: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION,
           variables: filterTraitementAutomatique,
         })?.tATraitements;
 
         cache.writeQuery<
           GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_TYPE,
           GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTIONVariables
         >({
           query: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION,
           variables: filterTraitementAutomatique,
           data: {
             tATraitements:
               previousList?.filter(
                 (traitement) => traitement.id !== result.data?.deleteOneTATraitement.id,
               ) || [],
           },
         });
       }
     },*/

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression du traitement récurrent',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [assignedUsers, setAssingedUsers] = useState<any[]>([]);
  const [traitementToEdit, setTraitementToEdit] = React.useState(undefined);

  const handleUserDestinationChanges = (selected: any) => {
    setAssingedUsers(selected);
  };

  const handleRequestSave = (traitement: any) => {
    const projet = state.projet;
    setSaved(false);
    setSaving(true);
    if (traitement.id) {
      updateTraitement({
        variables: {
          id: traitement.id,
          input: {
            ...traitement,
            id: undefined,
            idUrgence: state.idUrgence,
            idImportance: state.idImportance,
            idFonction: projet?.idFonction,
            idTache: projet?.idTache,
            idEtiquette: state.idEtiquette,
            idUserParticipants: assignedUsers.map(({ id }) => id),
            isPrivate,
          },
        },
      });
    } else {
      createTraitement({
        variables: {
          input: {
            ...traitement,
            id: undefined,
            idUrgence: state.idUrgence,
            idImportance: state.idImportance,
            idFonction: projet?.idFonction,
            idTache: projet?.idTache,
            idEtiquette: state.idEtiquette,
            idUserParticipants: assignedUsers.map(({ id }) => id),
            isPrivate,
          },
        },
      });
    }
  };

  const handleDelete = (traitement: any) => {
    deleteTraitement({
      variables: {
        id: traitement.id,
      },
    });
  };

  const handleRequestEdit = (traitementToEdit: any) => {
    setTraitementToEdit(traitementToEdit);
    setAssingedUsers(traitementToEdit?.participants || []);
    setState(prevState => ({
      ...prevState,
      projet: traitementToEdit?.tache
        ? { ...traitementToEdit.tache, idTache: traitementToEdit.tache.id }
        : undefined, // Tache or projet
      idUrgence: traitementToEdit?.urgence.id,
      idImportance: traitementToEdit?.importance.id,
    }));
    setIsPrivate(traitementToEdit?.isPrivate || false);
  };

  const [state, setState] = useState<any>({});

  const handleChange = event => {
    if (event.target) {
      const { value, name } = event.target;
      setState(prevState => ({ ...prevState, [name]: value }));
    }
  };

  return (
    <TraitementAutomatiquePage
      saved={saved}
      setSaved={setSaved}
      loading={searchResult.loading}
      saving={saving}
      error={searchResult.error}
      data={(searchResult.data || []) as any}
      rowsTotal={searchResult.total || 0}
      traitementToEdit={traitementToEdit}
      idUrgence={state.idUrgence || ''}
      idImportance={state.idImportance || ''}
      collaborateurs={assignedUsers}
      idTache={state.idProject || ''}
      onRequestSearch={change => {
        const must: any[] = [{ term: { idPharmacie: idPharmacie } }];
        if (change.searchText) {
          must.push({
            query_string: {
              query: `*${change.searchText}*`,
              analyze_wildcard: true,
              fields: [],
            },
          });
        }

        if (change.statut !== 'ALL') {
          must.push({
            term: { 'execution.dernierChangementStatut.status': change.statut },
          });
        }

        search({
          variables: {
            skip: change.skip,
            take: change.take,
            query: {
              query: {
                bool: {
                  must,
                },
              },
            },
            index: ['tatraitement'],
          },
        });
      }}
      commonFieldsComponent={
        <CommonFieldsForm
          selectedUsers={assignedUsers}
          urgence={state.idUrgence}
          urgenceProps={{
            useCode: false,
          }}
          usersModalProps={{
            withNotAssigned: false,
            searchPlaceholder: 'Rechercher...',
            withAssignTeam: false,
            singleSelect: false,
          }}
          isPrivate={isPrivate}
          onChangePrivate={setIsPrivate}
          projet={state.projet}
          importance={state.idImportance ? state.idImportance : undefined}
          onChangeUsersSelection={handleUserDestinationChanges}
          onChangeUrgence={urgence =>
            handleChange({ target: { name: 'idUrgence', value: urgence.id } })
          }
          onChangeImportance={importance =>
            handleChange({ target: { name: 'idImportance', value: importance.id } })
          }
          onChangeProjet={projet => {
            handleChange({ target: { name: 'projet', value: projet } });
          }}
        />
      }
      traitementTypes={{
        loading: loadTraitementTypes.loading,
        error: loadTraitementTypes.error,
        data: loadTraitementTypes.data?.tATraitementTypes.nodes,
      }}
      onRequestSave={handleRequestSave}
      onRequestDelete={handleDelete}
      onRequestEdit={handleRequestEdit}
    />
  );
};

export default TraitementAutomatique;
