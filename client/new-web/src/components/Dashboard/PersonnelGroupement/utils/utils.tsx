import React, { useState, useContext } from 'react';
import {
  useQuery,
  useMutation,
  useApolloClient,
  useLazyQuery,
  QueryLazyOptions,
} from '@apollo/client';
import { GET_CHECKEDS_PERSONNEL_GROUPEMENT } from '../../../../graphql/Personnel/local';
import TableActionColumn from '../../TableActionColumn';
import {
  PERSONNEL_GROUPEMENT_URL,
  PHARMACIE_URL,
  PARTENAIRE_SERVICE_URL,
} from '../../../../Constant/url';
import { Column } from '../../../Common/newCustomContent/interfaces';
import {
  CREATE_PERSONNEL,
  CREATE_PERSONNELVariables,
} from '../../../../graphql/Personnel/types/CREATE_PERSONNEL';
import {
  DO_CREATE_PERSONNEL,
  GET_PERSONNEL,
  DO_DELETE_PERSONNELS,
} from '../../../../graphql/Personnel';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import {
  PERSONNEL,
  PERSONNELVariables,
  PERSONNEL_personnel,
} from '../../../../graphql/Personnel/types/PERSONNEL';
import {
  DELETE_PERSONNELS,
  DELETE_PERSONNELSVariables,
} from '../../../../graphql/Personnel/types/DELETE_PERSONNELS';
import { ContentStateInterface, ContentContext } from '../../../../AppContext';
import { differenceBy } from 'lodash';
import CustomAvatar from '../../../Common/CustomAvatar';
import UserStatus from '../../Content/UserStatus';
import moment from 'moment';
import { GET_CHECKEDS_PERSONNEL_AFFECTATION } from '../../../../graphql/PersonnelAffectation/local';
import {
  AFFECT_PERSONNEL,
  AFFECT_PERSONNELVariables,
} from '../../../../graphql/PersonnelAffectation/types/AFFECT_PERSONNEL';
import { DO_AFFECT_PERSONNEL } from '../../../../graphql/PersonnelAffectation/mutation';

/**
 *
 *  Create personnel hooks
 *
 */
export const useCreatePersonnel = (mutationVariables: any): [(variables?: any) => any, boolean] => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const client = useApolloClient();

  const [mutationSuccess, setMutationSuccess] = useState<boolean>(false);

  const [createUpdatePersonnel] = useMutation<CREATE_PERSONNEL, CREATE_PERSONNELVariables>(
    DO_CREATE_PERSONNEL,
    {
      update: (cache, { data }) => {
        console.log(mutationVariables);
        if (
          data &&
          data.createUpdatePersonnel &&
          mutationVariables &&
          mutationVariables.variables &&
          !mutationVariables.variables.id
        ) {
          if (variables && operationName) {
            const req: any = cache.readQuery({
              query: operationName,
              variables: variables,
            });
            if (req && req.search && req.search.data) {
              cache.writeQuery({
                query: operationName,
                data: {
                  search: {
                    ...req.search,
                    ...{
                      total: req.search.total + 1,
                      data: [...req.search.data, data.createUpdatePersonnel],
                    },
                  },
                },

                variables: variables,
              });
            }
          }
        }
      },
      onCompleted: data => {
        if (data && data.createUpdatePersonnel) {
          setMutationSuccess(true);
        }
      },
      onError: errors => {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
        });
        displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
      },
    },
  );

  return [createUpdatePersonnel, mutationSuccess];
};

/**
 *
 *  Delete personnel hooks
 *
 */

export const useDeletePersonnel = () => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);
  const checkeds = useCheckedPersonnelGroupement();

  const client = useApolloClient();
  const [deletePersonnels, { loading, data: deleteData }] = useMutation<
    DELETE_PERSONNELS,
    DELETE_PERSONNELSVariables
  >(DO_DELETE_PERSONNELS, {
    update: (cache, { data }) => {
      if (data && data.deletePersonnels) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables: variables,
          });
          if (req && req.search && req.search.data) {
            const source = req.search.data;
            const result = data.deletePersonnels;
            const dif = differenceBy(source, result, 'id');
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: dif,
                  },
                },
              },
              variables: variables,
            });
              // TODO: Migration

            /*(client as any).writeData({
              data: { checkedsPersonnelGroupement: differenceBy(checkeds, result, 'id') },
            });*/
            displaySnackBar(client, {
              type: 'SUCCESS',
              message:
                result && result.length > 1
                  ? 'Utilisateurs supprimés avec succès'
                  : 'Utilisateur supprimé avec succès',
              isOpen: true,
            });
          }
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });
  return deletePersonnels;
};

export const useAffectPersonnel = () => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);
  const checkeds = useCheckedPersonnelGroupement();

  const client = useApolloClient();
  const [affectPersonnel, { loading, data: affectData }] = useMutation<
    AFFECT_PERSONNEL,
    AFFECT_PERSONNELVariables
  >(DO_AFFECT_PERSONNEL, {
    update: (cache, { data }) => {
      if (data && data.createUpdatePersonnelAffectation) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables: variables,
          });
          if (req && req.search && req.search.data) {
            // const source = req.search.data;
            const result = data.createUpdatePersonnelAffectation;
            // const dif = differenceBy(source, result, 'id');
            // cache.writeQuery({
            //   query: operationName,
            //   data: {
            //     search: {
            //       ...req.search,
            //       ...{
            //         data: dif,
            //       },
            //     },
            //   },
            //   variables: variables,
            // });
            // (client as any).writeData({
            //   data: { checkedsPersonnelGroupement: differenceBy(checkeds, result, 'id') },
            // });
            displaySnackBar(client, {
              type: 'SUCCESS',
              message: 'Utilisateur affecté avec succès',
              isOpen: true,
            });
          }
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });
  return affectPersonnel;
};

/**
 *
 *  list checked personnel groupement hooks
 *
 */

export const useCheckedPersonnelGroupement = () => {
  const checkedPersonnelGroupement = useQuery(GET_CHECKEDS_PERSONNEL_GROUPEMENT);
  return (
    (checkedPersonnelGroupement &&
      checkedPersonnelGroupement.data &&
      checkedPersonnelGroupement.data.checkedsPersonnelGroupement) ||
    []
  );
};

export const useCheckedPersonnelAffectation = () => {
  const checkedPersonnelAffectation = useQuery(GET_CHECKEDS_PERSONNEL_AFFECTATION);
  return (
    (checkedPersonnelAffectation &&
      checkedPersonnelAffectation.data &&
      checkedPersonnelAffectation.data.checkedsPersonnelAffecation) ||
    []
  );
};

/**
 *
 *  subtoolbar button action hooks
 *
 */

export const useButtonHeadAction = (push: any) => {
  const goToAddPersonnel = () => push(`/db/${PERSONNEL_GROUPEMENT_URL}/create`);
  const goBack = () => push(`/db/${PERSONNEL_GROUPEMENT_URL}`);

  return [goToAddPersonnel, goBack];
};

/**
 *  get personnel hooks
 */

export const useGetPersonnel = (
  fetchPolicy?:
    | 'cache-first'
    | 'network-only'
    | 'cache-only'
    | 'no-cache'
    | 'standby'
    | 'cache-and-network',
): [
    (options?: QueryLazyOptions<PERSONNELVariables> | undefined) => void,
    PERSONNEL_personnel | null | undefined,
  ] => {
  const client = useApolloClient();
  const [getPersonnel, { data, loading: personnelLoading }] = useLazyQuery<
    PERSONNEL,
    PERSONNELVariables
  >(GET_PERSONNEL, {
    fetchPolicy: fetchPolicy || 'cache-first',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        });
      });
    },
  });
  const personnel = data && data.personnel;
  return [getPersonnel, personnel];
};

export const usePersonneGroupementColumns = (
  setOpenDeleteDialog: any,
): [Column[], any, () => void] => {
  const deletePersonnels = useDeletePersonnel();
  const [deleteRow, setDeleteRow] = useState<any>();
  const checkedsPersonnelGroupement = useCheckedPersonnelGroupement();
  const client = useApolloClient();

  const onClickDelete = (row: any) => {
    setDeleteRow(row);
  };

  const onClickConfirmDelete = () => {
    setOpenDeleteDialog(false);
    if (checkedsPersonnelGroupement && checkedsPersonnelGroupement.length > 0) {
      const ids: string[] = checkedsPersonnelGroupement.map(i => i.id);
      deletePersonnels({ variables: { ids } });
    }

    if (deleteRow && deleteRow.id) {
      deletePersonnels({ variables: { ids: [deleteRow.id] } });
    }
  };

  const personnelColumns = [
    {
      name: '',
      label: 'Photo',
      renderer: (value: any) => {
        return (
          <CustomAvatar
            name={value && value.fullName}
            url={
              value &&
              value.user &&
              value.user.userPhoto &&
              value.user.userPhoto.fichier &&
              value.user.userPhoto.fichier.publicUrl
            }
          />
        );
      },
    },
    {
      name: 'civilite',
      label: 'Civilité',
    },

    {
      name: 'prenom',
      label: 'Prénom',
    },
    {
      name: 'nom',
      label: 'Nom',
    },
    {
      name: 'service.nom',
      label: 'Services',
      renderer: (value: any) => {
        return value && value.service && value.service.nom ? value.service.nom : '-';
      },
    },
    {
      name: 'role.nom',
      label: 'Rôle',
      centered: true,
      renderer: (value: any) => {
        return value && value.role && value.role.nom ? value.role.nom : '-';
      },
    },
    {
      name: 'contact.mailProf',
      label: 'Email',
      renderer: (value: any) => {
        return value && value.contact && value.contact.mailProf ? value.contact.mailProf : '-';
      },
    },
    {
      name: 'contact.telProf',
      label: 'Téléphone',
      renderer: (value: any) => {
        return value && value.contact && value.contact.telProf ? value.contact.telProf : '-';
      },
    },
    {
      name: 'user.status',
      label: 'Statut',
      renderer: (value: any) => {
        return <UserStatus user={value.user} pathname={PARTENAIRE_SERVICE_URL} />;
      },
    },
    {
      name: 'dateCreation',
      label: 'Date de création',
      renderer: (value: any) => {
        return value && value.dateCreation ? moment(value.dateCreation).format('L') : '-';
      },
    },
    {
      name: 'dateModification',
      label: 'Date de modification',
      renderer: (value: any) => {
        return value && value.dateModification ? moment(value.dateModification).format('L') : '-';
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <TableActionColumn
            row={value}
            baseUrl={`${PERSONNEL_GROUPEMENT_URL}`}
            setOpenDeleteDialog={setOpenDeleteDialog}
            handleClickDelete={onClickDelete}
          />
        );
      },
    },
  ];

  return [personnelColumns, deleteRow, onClickConfirmDelete];
};

export const useAffectationColumns = (
  setOpenAffectationDialog: any,
): [Column[], any, (value: any) => any] => {
  const [personnelToAffect, setPersonnelToAffect] = useState<any>({});
  // const deletePersonnels = useDeletePersonnel();
  // const checkedsPersonnelAffectation = useCheckedPersonnelAffectation();

  const onClickAffectation = (action: any, row: any) => {
    setOpenAffectationDialog(true);
    setPersonnelToAffect({ ...row, action });
  };

  // const onClickConfirmDelete = () => {
  //   setOpenDeleteDialog(false);
  //   if (checkedsPersonnelGroupement && checkedsPersonnelGroupement.length > 0) {
  //     const ids: string[] = checkedsPersonnelGroupement.map(i => i.id);
  //     deletePersonnels({ variables: { ids } });
  //   }

  //   if (deleteRow && deleteRow.id) {
  //     deletePersonnels({ variables: { ids: [deleteRow.id] } });
  //   }
  // };

  const AffectationColumns = [
    {
      name: 'personnelFonction.nom',
      label: 'Fonctions',
      renderer: (value: any) => {
        return (value && value.personnelFonction && value.personnelFonction.nom) || '-';
      },
    },
    {
      name: 'departement.nom',
      label: 'Nom du département',
      renderer: (value: any) => {
        return (
          `${value && value.departement && value.departement.nom} (${value &&
          value.departement &&
          value.departement.code})` || '-'
        );
      },
    },
    {
      name: 'personnel.fullName',
      label: 'Affectation actuelle',
      renderer: (value: any) => {
        return (value && value.personnel && value.personnel.fullName) || '-';
      },
    },
    {
      name: 'personnel.dateDebut',
      label: 'Date de début',
      renderer: (value: any) => {
        return '-';
      },
    },
    {
      name: 'personnel.dateFin',
      label: 'Date de fin',
      renderer: (value: any) => {
        return '-';
      },
    },
    {
      name: 'personnelDemandeAffectation.personnelRemplacent.fullName',
      label: 'Remplacant',
      renderer: (value: any) => {
        console.log('value : ', value);
        return (
          (value &&
            value.personnelDemandeAffectation &&
            value.personnelDemandeAffectation.personnelRemplacent &&
            value.personnelDemandeAffectation.personnelRemplacent.fullName) ||
          '-'
        );
      },
    },
    {
      name: 'dateDebut',
      label: 'Date de début',
      renderer: (value: any) => {
        return value && value.dateFin && moment(value.dateDebut).format('L');
      },
    },
    {
      name: 'dateFin',
      label: 'Date de fin',
      renderer: (value: any) => {
        return value && value.dateFin && moment(value.dateFin).format('L');
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <TableActionColumn
            row={value}
            baseUrl={`${PERSONNEL_GROUPEMENT_URL}/affectation`}
            handleClickAffectation={onClickAffectation}
          />
        );
      },
    },
  ];

  return [AffectationColumns, personnelToAffect, setPersonnelToAffect];
};
