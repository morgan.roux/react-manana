import * as React from 'react';
import useStyles from '../styles';
import FormContainer from '../../../../Common/FormContainer';
import InputRow from '../../../../Common/InputRow';
import CustomSelect from '../../../../Common/CustomSelect';
import { CIVILITE_LIST } from '../../../../../Constant/user';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import { CREATE_PERSONNELVariables } from '../../../../../graphql/Personnel/types/CREATE_PERSONNEL';
import { GET_MINIM_SERVICES } from '../../../../../graphql/Service';
import { useQuery } from '@apollo/client';
import { MINIM_SERVICES } from '../../../../../graphql/Service/types/MINIM_SERVICES';

interface IStep1Props {
  values: CREATE_PERSONNELVariables;
  handleChange(e: React.ChangeEvent<any>): void;
}

const Step1: React.FC<IStep1Props> = props => {
  const { values, handleChange } = props;

  const { civilite, nom, prenom, codeService } = values;

  const classes = useStyles({});

  const { data: dataService, loading: loadingService } = useQuery<MINIM_SERVICES>(
    GET_MINIM_SERVICES,
  );

  const infoPersoComponent = (
    <>
      <InputRow title="Civilité">
        <CustomSelect
          label=""
          list={CIVILITE_LIST}
          listId="id"
          index="value"
          name="civilite"
          value={civilite}
          onChange={handleChange}
          shrink={false}
          placeholder="Choisissez une civilité"
          withPlaceholder={true}
        />
      </InputRow>

      <InputRow title="Prénom">
        <CustomFormTextField
          name="prenom"
          value={prenom}
          onChange={handleChange}
          placeholder="Prénom de la personne"
        />
      </InputRow>
      <InputRow title="Nom">
        <CustomFormTextField
          name="nom"
          value={nom}
          onChange={handleChange}
          placeholder="Nom de la personne"
        />
      </InputRow>
    </>
  );

  const responsabilityComponent = (
    <>
      <InputRow title="Services" required>
        <CustomSelect
          label=""
          list={(dataService && dataService.services) || []}
          listId="code"
          index="nom"
          name="codeService"
          value={codeService}
          onChange={handleChange}
          shrink={false}
          placeholder="Service de la personne"
          withPlaceholder={true}
        />
      </InputRow>
    </>
  );

  return (
    <div className={classes.personnelGroupementFormRoot}>
      <FormContainer title="Information personnelle">{infoPersoComponent}</FormContainer>
      <FormContainer title="Responsabilité">{responsabilityComponent}</FormContainer>
    </div>
  );
};

export default Step1;
