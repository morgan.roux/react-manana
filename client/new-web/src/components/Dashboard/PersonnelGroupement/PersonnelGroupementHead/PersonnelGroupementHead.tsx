import React, { FC, useCallback, Dispatch, SetStateAction } from 'react';
import useStyles from './styles';
import { Typography, IconButton } from '@material-ui/core';
import CustomButton from '../../../Common/CustomButton';
import { Add, ArrowBack } from '@material-ui/icons';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import classnames from 'classnames';
import { PERSONNEL_GROUPEMENT_URL } from '../../../../Constant/url';

interface PersonnelGroupementHeadProps {
  title: string;
  isOnList: boolean;
  isOnCreate: boolean;
  isOnEdit: boolean;
  selected: any[];
  deleteRow: any;
  openDeleteDialog: boolean;
  setSelected: Dispatch<SetStateAction<any[]>>;
  setDeleteRow: Dispatch<SetStateAction<any>>;
  setOpenDeleteDialog: Dispatch<SetStateAction<boolean>>;
  disabledSaveBtn: boolean;
  onClickSaveAvatar: () => void;
}

const PersonnelGroupementHead: FC<PersonnelGroupementHeadProps & RouteComponentProps> = ({
  onClickSaveAvatar,
  isOnList,
  isOnCreate,
  // isOnEdit,
  selected,
  // setSelected,
  // deleteRow,
  // setDeleteRow,
  // openDeleteDialog,
  setOpenDeleteDialog,
  history: { push },
  // location: { pathname },
  title,
  disabledSaveBtn,
}) => {
  const classes = useStyles({});
  const saveBtnText = isOnCreate ? 'Ajouter' : 'Modifier';

  const goToBack = () => push(`/db/${PERSONNEL_GROUPEMENT_URL}`);

  const goToAddPersonnel = () => push(`/db/${PERSONNEL_GROUPEMENT_URL}/create`);

  const onClickDeleteSelected = useCallback(() => setOpenDeleteDialog(true), []);

  const onClickCancel = () => {
    push(`/db/${PERSONNEL_GROUPEMENT_URL}`);
  };

  return (
    <div
      className={
        isOnList
          ? classes.personnelGroupementHeadRoot
          : classnames(
              classes.personnelGroupementHeadRoot,
              classes.personnelGroupementHeadRootWithBg,
            )
      }
    >
      {!isOnList && (
        <IconButton onClick={goToBack} color="inherit">
          <ArrowBack />
        </IconButton>
      )}
      <Typography className={classes.personnelGroupementHeadTitle}>{title}</Typography>

      <div className={classes.personnelGroupementHeadBtnsContainer}>
        {isOnList && (
          <>
            {selected.length > 0 && (
              <CustomButton color="default" onClick={onClickDeleteSelected}>
                Supprimer la sélection
              </CustomButton>
            )}
            <CustomButton color="secondary" startIcon={<Add />} onClick={goToAddPersonnel}>
              Ajouter un personnel
            </CustomButton>
          </>
        )}
        {!isOnList && (
          <>
            <CustomButton color="default" onClick={onClickCancel}>
              Annuler
            </CustomButton>
            <CustomButton color="secondary" onClick={onClickSaveAvatar} disabled={disabledSaveBtn}>
              {saveBtnText}
            </CustomButton>
          </>
        )}
      </div>
    </div>
  );
};

export default withRouter(PersonnelGroupementHead);
