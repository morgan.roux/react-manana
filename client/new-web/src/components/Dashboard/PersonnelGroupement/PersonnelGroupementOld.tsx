import React, { FC, useState } from 'react';
import { Theme, makeStyles, createStyles } from '@material-ui/core/styles';
import { Column } from './../Content/Interface';
import { getGroupement } from '../../../services/LocalStorage';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { useGetColumns } from '../columns';
import { ResetUserPasswordModal } from '../ResetUserPasswordModal';
import CustomContent from '../../Common/CustomContent';
import HistoriquePersonnel from '../HistoriquePersonnel';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    pointer: {
      cursor: 'pointer',
    },
  }),
);

const PersonnelGroupement: FC<RouteComponentProps> = ({ history, location: { pathname } }) => {
  const classes = useStyles({});

  const [openModal, setOpenModal] = useState<boolean>(false);
  const [userId, setUserId] = useState<string>('');
  const [userLogin, setUserLogin] = useState<string>('');
  const [userEmail, setUserEmail] = useState<string>('');
  const [openHistory, setOpenHistory] = useState<boolean>(false);

  const groupement = getGroupement();
  const id = groupement && groupement.id ? groupement.id : null;

  const handleModal = (open: boolean, userId: string, email: string, login: string) => {
    setOpenModal(open);
    setUserId(userId);
    setUserEmail(email);
    setUserLogin(login);
  };

  const handleClickHistory = (open: boolean, userId: string) => {
    setOpenHistory(open);
    setUserId(userId);
  };

  const columns: Column[] = useGetColumns(
    'PERSONNEL_GROUPEMENT',
    history,
    pathname,
    handleModal,
    handleClickHistory,
  );

  return (
    <>
      <CustomContent
        searchPlaceholder="Rechercher un personnel"
        type="personnel"
        columns={columns}
        filterBy={[{ term: { idGroupement: id } }]}
        sortBy={[{ nom: { order: 'desc' } }]}
        enableGlobalSearch={true}
        inSelectContainer={true}
      />

      <div>
        <ResetUserPasswordModal
          open={openModal}
          setOpen={setOpenModal}
          userId={userId}
          email={userEmail}
          login={userLogin}
        />
      </div>

      <HistoriquePersonnel open={openHistory} setOpen={setOpenHistory} userId={userId} />
    </>
  );
};

export default withRouter(PersonnelGroupement);
