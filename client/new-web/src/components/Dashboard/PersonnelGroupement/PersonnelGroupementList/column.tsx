import React from 'react';
import { CustomTableWithSearchColumn } from '../../../Common/CustomTableWithSearch/interface';
import moment from 'moment';
import { startCase, lowerCase } from 'lodash';
import UserStatus from '../../Content/UserStatus';

const usePersonnelGroupementColumn = (pathname: string) => {
  const DATES_COLUMNS: CustomTableWithSearchColumn[] = [
    {
      key: 'dateCreation',
      label: 'Date de création',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        return moment(row.dateDebut).format('DD/MM/YYYY');
      },
    },
    {
      key: 'dateModification',
      label: 'Mise à jour',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        return moment(row.dateFin).format('DD/MM/YYYY');
      },
    },
  ];

  const PERSONNEL_COLUMNS: CustomTableWithSearchColumn[] = [
    {
      key: '',
      label: 'Profil',
      numeric: false,
      disablePadding: false,
      sortable: false,
      renderer: (row: any) => {
        console.log('row :::**>> ', row);
        if (
          row &&
          row.user &&
          row.user.userPhoto &&
          row.user.userPhoto.fichier &&
          row.user.userPhoto.fichier.publicUrl
        ) {
          return (
            <img
              src={row.user.userPhoto.fichier.publicUrl}
              style={{ width: 50, height: 50, borderRadius: '50%' }}
            />
          );
        }
        return '-';
      },
    },
    {
      key: 'civilite',
      label: 'Civilité',
      numeric: false,
      disablePadding: false,
    },

    {
      key: 'prenom',
      label: 'Prénom',
      numeric: false,
      disablePadding: false,
    },
    {
      key: 'nom',
      label: 'Nom',
      numeric: false,
      disablePadding: false,
    },
    {
      key: 'service.nom',
      label: 'Services',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        return (row.service && row.service.nom) || '-';
      },
    },
    {
      key: '',
      label: 'Rôle',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        return row.role && row.role.nom ? startCase(lowerCase(row.role.nom)) : '-';
      },
    },
    /*  {
      key: 'mailProf',
      label: 'Email',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        return row.mailProf || row.mailPerso || '-';
      },
    },
    {
      key: 'telMobProf',
      label: 'Télépnone',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        return row.telMobProf || row.telBureau || row.telMobPerso || row.telDomicile || '-';
      },
    }, */
    {
      key: 'sortie',
      label: 'Sortie',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        return row.sortie === 0 ? 'Non' : row.sortie === 1 ? 'Oui' : '-';
      },
    },
    {
      key: 'user.status',
      label: 'Statut',
      numeric: true,
      disablePadding: false,
      renderer: (row: any) => {
        return <UserStatus user={row.user} pathname={pathname} />;
      },
    },
  ];

  return { DATES_COLUMNS, PERSONNEL_COLUMNS };
};

export default usePersonnelGroupementColumn;
