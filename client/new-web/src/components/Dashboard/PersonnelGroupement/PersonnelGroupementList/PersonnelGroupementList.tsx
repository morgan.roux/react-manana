import React, { FC, Dispatch, SetStateAction, useState, useCallback, useEffect } from 'react';
import useStyles from './styles';
import CustomTableWithSearch from '../../../Common/CustomTableWithSearch';
import { CustomTableWithSearchColumn } from '../../../Common/CustomTableWithSearch/interface';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import EmptyImgSrc from '../../../../assets/img/img_no_list_item2.png';
import { CustomModal } from '../../../Common/CustomModal';
import AvatarFilter from '../PersonnelGroupementFilter';
import usePersonnelGroupementFilterForm from '../PersonnelGroupementFilter/usePersonnelGroupementFilterForm';
import { getGroupement } from '../../../../services/LocalStorage';
import { useApolloClient, useMutation } from '@apollo/client';
import {
  DELETE_PERSONNELS,
  DELETE_PERSONNELSVariables,
} from '../../../../graphql/Personnel/types/DELETE_PERSONNELS';
import { DO_DELETE_PERSONNELS } from '../../../../graphql/Personnel';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop';
import { ConfirmDeleteDialog } from '../../../Common/ConfirmDialog';
import { isInvalidArray } from '../../../../utils/Helpers';
import TableActionColumn from '../../TableActionColumn';
import { PERSONNEL_GROUPEMENT_URL } from '../../../../Constant/url';
import usePersonnelGroupementColumn from './column';
import { PersonnelGroupementDashboard } from '../../../Common/newWithSearch/ComponentInitializer';

interface PersonnelGroupementListProps {
  selected: any[];
  deleteRow: any;
  openDeleteDialog: boolean;
  setSelected: Dispatch<SetStateAction<any[]>>;
  setDeleteRow: Dispatch<SetStateAction<any>>;
  setOpenDeleteDialog: Dispatch<SetStateAction<boolean>>;
}

interface ActionMenuProps {
  row: any;
}

const PersonnelGroupementList: FC<PersonnelGroupementListProps & RouteComponentProps> = ({
  selected,
  setSelected,
  setDeleteRow,
  deleteRow,
  openDeleteDialog,
  setOpenDeleteDialog,
  location: { pathname },
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const groupement = getGroupement();
  const idGroupement = (groupement && groupement.id) || '';

  const defaultFilterBy = [{ term: { idGroupement } }];
  const defaultSortiteFilter = [{ terms: { sortie: [0] } }];
  const [sortieFilter, setSortieFilter] = useState<any[]>(defaultSortiteFilter);
  const [filterBy, setfilterBy] = useState<any[]>([...defaultFilterBy, ...sortieFilter]);
  const [openFilterModal, setOpenFilterModal] = useState<boolean>(false);

  const { values, setValues, handleChange } = usePersonnelGroupementFilterForm();
  const { service, role, status, sortie } = values;

  const [data, setData] = useState<any[]>([]);

  const emptyTitle = 'Aucun Personnel';
  const emptySubTitle = "Vous n'avez pas encore de personnel dans cette rubrique.";

  const onClickDelete = (row: any) => {
    setDeleteRow(row);
    if (selected.length > 0) {
      setSelected([]);
    }
  };

  const ACTIONS_COLUMNS: CustomTableWithSearchColumn[] = [
    {
      key: '',
      label: '',
      numeric: false,
      disablePadding: false,
      sortable: false,
      renderer: (row: any) => {
        return (
          <TableActionColumn
            row={row}
            baseUrl={PERSONNEL_GROUPEMENT_URL}
            setOpenDeleteDialog={setOpenDeleteDialog}
            handleClickDelete={onClickDelete}
          />
        );
      },
    },
  ];

  const { PERSONNEL_COLUMNS, DATES_COLUMNS } = usePersonnelGroupementColumn(pathname);
  const COLUMNS: CustomTableWithSearchColumn[] = [
    ...PERSONNEL_COLUMNS,
    ...DATES_COLUMNS,
    ...ACTIONS_COLUMNS,
  ];

  const onClickBtnFilter = () => {
    setOpenFilterModal(true);
  };

  const onClickApplyBtn = () => {
    setOpenFilterModal(false);
    // Set filterBy
    const newFilterBy: any[] = [];
    if (service && service !== 'ALL') {
      const newTerm = { term: { 'service.code': service } };
      newFilterBy.push(newTerm);
    }
    if (role && role !== 'ALL') {
      const newTerm = { term: { 'role.code': role } };
      newFilterBy.push(newTerm);
    }
    if (status && status !== 'ALL') {
      const newTerm = { term: { 'user.status': status } };
      newFilterBy.push(newTerm);
    }
    if (sortie === 'ALL') {
      const newTerm = { terms: { sortie: [0, 1] } };
      newFilterBy.push(newTerm);
    } else {
      const newTerm = { terms: { sortie: [Number(sortie)] } };
      newFilterBy.push(newTerm);
    }
    setfilterBy(newFilterBy);
  };

  const onClickInitBtn = useCallback(() => {
    setOpenFilterModal(false);
    setValues(prevState => ({
      ...prevState,
      service: 'ALL',
      role: 'ALL',
      status: 'ALL',
      sortie: '0',
    }));
    setfilterBy([...defaultSortiteFilter]);
  }, []);

  /**
   * Mutation delete personnels
   */
  const [deletePersonnels, { loading, data: deleteData }] = useMutation<
    DELETE_PERSONNELS,
    DELETE_PERSONNELSVariables
  >(DO_DELETE_PERSONNELS, {
    onCompleted: data => {
      if (data && data.deletePersonnels && data.deletePersonnels.length > 0) {
        setDeleteRow(null);
        const isInvalid: boolean = isInvalidArray(data.deletePersonnels);
        if (isInvalid) {
          displaySnackBar(client, {
            type: 'ERROR',
            message: 'Erreur lors de la suppression',
            isOpen: true,
          });
        } else {
          setSelected([]);
          displaySnackBar(client, {
            type: 'SUCCESS',
            message: 'Personnel(s) supprimé(s) avec succès',
            isOpen: true,
          });
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
      // displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
    },
  });

  /**
   * Re-render after delete
   */
  useEffect(() => {
    if (deleteData && deleteData.deletePersonnels) {
      onClickInitBtn();
      // setfilterBy([]);
    }
  }, [deleteData]);

  const onClickConfirmDelete = () => {
    setOpenDeleteDialog(false);
    if (selected.length > 0) {
      const ids: string[] = selected.map(i => i.id);
      deletePersonnels({ variables: { ids } });
    }

    if (deleteRow && deleteRow.id) {
      deletePersonnels({ variables: { ids: [deleteRow.id] } });
    }
  };

  const DeleteDialogContent = () => {
    if (deleteRow) {
      return (
        <span>
          Êtes-vous sur de vouloir supprimer
          <span style={{ fontWeight: 'bold' }}>
            {' '}
            {deleteRow.civilite || ''} {deleteRow.prenom || ''} {deleteRow.nom || ''}{' '}
          </span>
          ?
        </span>
      );
    }
    return <span>Êtes-vous sur de vouloir supprimer ces personnels ?</span>;
  };

  return (
    <div className={classes.personnelGroupementListRoot}>
      {loading && <Backdrop value="Suppression en cours..." />}
      {/* <CustomTableWithSearch
        searchType="personnel"
        isSelectable={true}
        selected={selected}
        setSelected={setSelected}
        showToolbar={true}
        columns={COLUMNS}
        withEmptyContent={true}
        emptyTitle={emptyTitle}
        emptySubTitle={emptySubTitle}
        emptyImgSrc={EmptyImgSrc}
        withInputSearch={true}
        withFilterBtn={true}
        onClickBtnFilter={onClickBtnFilter}
        filterByFromPros={filterBy}
        dataFromProps={data}
        setDataFromProps={setData}
      /> */}
      <PersonnelGroupementDashboard columns={COLUMNS} />
      <CustomModal
        title="Filtres de recherche"
        open={openFilterModal}
        setOpen={setOpenFilterModal}
        closeIcon={true}
        headerWithBgColor={true}
        withBtnsActions={false}
        maxWidth="xl"
      >
        <AvatarFilter
          state={values}
          setState={setValues}
          handleChangeInput={handleChange}
          onClickApplyBtn={onClickApplyBtn}
          onClickInitBtn={onClickInitBtn}
        />
      </CustomModal>
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
    </div>
  );
};

export default withRouter(PersonnelGroupementList);
