import React, { ReactNode, useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router';
import mutationSuccessImg from '../../../assets/img/mutation-success.png';
import { PERSONNEL_GROUPEMENT_URL } from '../../../Constant/url';
import { getGroupement } from '../../../services/LocalStorage';
import CustomButton from '../../Common/CustomButton';
import CustomTabs from '../../Common/CustomTabs';
import NoItemContentImage from '../../Common/NoItemContentImage';
import Stepper from '../../Common/Stepper';
import { Step } from '../../Common/Stepper/Stepper';
import PersonnelGroupementFilter from './PersonnelGroupementFilter';
import usePersonnelGroupement from './PersonnelGroupementForm/usePersonnelGroupementForm';
import Step1 from './Stepper/Step1';
import Step2 from './Stepper/Step2';
import useStyles from './styles';
import Affectation from './Tabs/Affectation/Affectation';
import List from './Tabs/List/List';
import {
  useAffectationColumns,
  useAffectPersonnel,
  useButtonHeadAction,
  useCreatePersonnel,
  useGetPersonnel,
  usePersonneGroupementColumns,
} from './utils/utils';

interface INewPersonnelGroupementProps {
  listResult: any;
}

interface TabsInferface {
  id: number;
  label: string;
  content: ReactNode;
  clickHandlerParams?: string;
}

const NewPersonnelGroupement: React.FC<INewPersonnelGroupementProps &
  RouteComponentProps> = props => {
  const {
    listResult,
    history: { push },
    location: { pathname },
    match: { params },
  } = props;

  const classes = useStyles({});

  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [openAffectationDialog, setOpenAffectationDialog] = useState<boolean>(false);
  const [
    personnelColumns,
    personnelDeleteRow,
    personnelOnClickConfirmDelete,
  ] = usePersonneGroupementColumns(setOpenDeleteDialog);

  const [affecationColums, personnelToAffect, setPersonnelToAffect] = useAffectationColumns(
    setOpenAffectationDialog,
  );

  const affectPersonnel = useAffectPersonnel();

  const { handleChange, values, setValues } = usePersonnelGroupement();

  const groupement = getGroupement();

  const idGroupement = groupement && groupement.id;

  const { sexe, civilite, nom, codeService } = values;

  const { contact: c } = values;

  const [createPersonnel, mutationSuccess] = useCreatePersonnel({
    variables: { ...values, sexe: sexe as any, idGroupement },
  });

  const [goToAddPersonnel, goBack] = useButtonHeadAction(push);

  const [getPersonnel, personnel] = useGetPersonnel('no-cache');

  const isOnList =
    pathname === `/db/${PERSONNEL_GROUPEMENT_URL}` ||
    pathname === `/db/${PERSONNEL_GROUPEMENT_URL}/affectation` ||
    pathname === `/db/${PERSONNEL_GROUPEMENT_URL}/list`;
  const isOnAffectation = pathname === `/db/${PERSONNEL_GROUPEMENT_URL}/affectation`;

  const isOnCreate = pathname === `/db/${PERSONNEL_GROUPEMENT_URL}/create`;
  const isOnEdit: boolean = pathname.startsWith(`/db/${PERSONNEL_GROUPEMENT_URL}/edit`);

  const activeStep = isOnAffectation ? 1 : 0;
  const mutationSuccessTitle = `${isOnCreate ? 'Ajout' : 'Modification'} du personnel réussi`;

  const mutationSuccessSubTitle = `Le nouvel utilisateur que vous venez ${
    isOnCreate ? "d'ajouter" : 'de modifier'
  } est désormais dans la liste du personnel du groupement.`;

  const { personnelId } = params as any;

  const EDIT_URL = `/db/${PERSONNEL_GROUPEMENT_URL}/edit/${personnelId}`;

  // const disabledSaveBtn = (): boolean => {
  //   if (!isOnList && (!civilite || !nom || !codeService)) {
  //     return true;
  //   }
  //   return false;
  // };

  const [list, setList] = useState<any>(listResult);

  useEffect(() => {
    if (listResult) {
      setList(listResult);
    }
  }, [listResult]);

  const goToHome = () => push(`/db/${PERSONNEL_GROUPEMENT_URL}/list`);

  // const children = (
  //   <div className={classes.childrenRoot}>
  //     {((checkedsPersonnelGroupement && checkedsPersonnelGroupement.length > 0) || !isOnList) && (
  //       <CustomButton
  //         color="default"
  //         onClick={() => (!isOnList ? goBack() : setOpenDeleteDialog(true))}
  //       >
  //         {!isOnList ? 'Annuler' : 'Supprimer la sélection'}
  //       </CustomButton>
  //     )}

  //     <CustomButton
  //       color="secondary"
  //       startIcon={!isOnList ? null : <Add />}
  //       onClick={() =>
  //         !isOnList
  //           ? createPersonnel({ variables: { ...values, sexe: sexe as any, idGroupement } })
  //           : goToAddPersonnel()
  //       }
  //       disabled={disabledSaveBtn()}
  //     >
  //       {isOnCreate ? 'Ajouter' : isOnEdit ? 'Modifier' : 'Ajouter un personnel'}
  //     </CustomButton>
  //   </div>
  // );

  const [filtersState, setFiltersState] = useState<any>({});

  const filter = () => (
    <PersonnelGroupementFilter
      state={filtersState}
      setState={setFiltersState}
      onClickApplyBtn={() => {}}
      onClickInitBtn={() => {}}
      handleChangeInput={() => {}}
    />
  );
  const actionSuccess = (
    <div className={classes.mutationSuccessContainer}>
      <NoItemContentImage
        src={mutationSuccessImg}
        title={mutationSuccessTitle}
        subtitle={mutationSuccessSubTitle}
      >
        <CustomButton color="default" onClick={goBack}>
          Retour à la liste
        </CustomButton>
      </NoItemContentImage>
    </div>
  );

  const steps: Step[] = [
    {
      title: 'Informations personnelles',
      content: <Step1 {...{ handleChange, values }} />,
    },
    {
      title: 'Contact',
      content: <Step2 {...{ handleChange, values }} />,
    },
  ];
  const handleTabsClick = (params: string) => {
    push(`/db/${PERSONNEL_GROUPEMENT_URL}${params}`);
  };

  const tabs = [
    {
      id: 0,
      label: 'Liste du personnel',
      clickHandlerParams: '/list',
      content: (
        <List
          columns={personnelColumns || []}
          listResult={listResult}
          pathname={
            activeStep === 1
              ? `db/${PERSONNEL_GROUPEMENT_URL}/list`
              : `db/${PERSONNEL_GROUPEMENT_URL}/affectation`
          }
          values={values}
          sexe={sexe}
          idGroupement={idGroupement}
          civilite={civilite}
          nom={nom}
          codeService={codeService}
          personnelOnClickConfirmDelete={personnelOnClickConfirmDelete}
          personnelDeleteRow={personnelDeleteRow}
          openDeleteDialog={openDeleteDialog}
          setOpenDeleteDialog={setOpenDeleteDialog}
        />
      ),
    },
    {
      id: 1,
      label: 'Affectation territoriale',
      clickHandlerParams: '/affectation',
      content: (
        <Affectation
          pathname={
            activeStep === 0
              ? `db/${PERSONNEL_GROUPEMENT_URL}/list`
              : `db/${PERSONNEL_GROUPEMENT_URL}/affectation`
          }
          listResult={list}
          columns={affecationColums || []}
          values={values}
          sexe={sexe}
          idGroupement={idGroupement}
          civilite={civilite}
          nom={nom}
          codeService={codeService}
          openAffectationDialog={openAffectationDialog}
          setOpenAffectationDialog={setOpenAffectationDialog}
          setPersonnelToAffect={setPersonnelToAffect}
          personnelToAffect={personnelToAffect}
          affectPersonnel={affectPersonnel}
        />
      ),
    },
  ];

  const finalStep = {
    buttonLabel: isOnEdit ? 'Modifier' : 'Ajouter',

    action: () => {
      createPersonnel({
        variables: {
          ...values,
          sexe: sexe as any,
          idGroupement,
          contact: {
            adresse1: (c && c.adresse1) || null,
            adresse2: (c && c.adresse2) || null,
            compteSkypePerso: (c && c.compteSkypePerso) || null,
            compteSkypeProf: (c && c.compteSkypeProf) || null,
            cp: (c && c.cp) || null,
            faxPerso: (c && c.faxPerso) || null,
            faxProf: (c && c.faxProf) || null,
            mailPerso: (c && c.mailPerso) || null,
            mailProf: (c && c.mailProf) || null,
            pays: (c && c.pays) || null,
            sitePerso: (c && c.sitePerso) || null,
            siteProf: (c && c.siteProf) || null,
            telMobPerso: (c && c.telMobPerso) || null,
            telMobProf: (c && c.telMobProf) || null,
            telPerso: (c && c.telPerso) || null,
            telProf: (c && c.telProf) || null,
            urlFacebookPerso: (c && c.urlFacebookPerso) || null,
            urlFacebookProf: (c && c.urlFacebookProf) || null,
            urlLinkedinPerso: (c && c.urlLinkedinPerso) || null,
            urlLinkedinProf: (c && c.urlLinkedinProf) || null,
            urlTwitterPerso: (c && c.urlTwitterPerso) || null,
            urlTwitterProf: (c && c.urlTwitterProf) || null,
            ville: (c && c.ville) || null,
            whatsAppMobProf: (c && c.whatsAppMobProf) || null,
            whatsappMobPerso: (c && c.whatsappMobPerso) || null,
          },
        },
      });
    },
  };

  const stepperComponent = (
    <Stepper
      title={`${isOnEdit ? 'Modification' : 'Ajout'}  d'un Personnel du groupement`}
      steps={steps}
      backToHome={goToHome}
      disableNextBtn={!Boolean(codeService)}
      fullWidth={false}
      finalStep={finalStep}
    />
  );

  useEffect(() => {
    if (pathname === EDIT_URL && personnelId) {
      getPersonnel({ variables: { id: personnelId } });
    }
  }, [pathname, personnelId]);

  useEffect(() => {
    if (personnel) {
      setValues({
        id: personnel.id,
        codeService: (personnel.service && personnel.service.code) || '',
        respHierarch: null,
        sortie: personnel.sortie as any,
        dateSortie: personnel.dateSortie,
        civilite: personnel.civilite,
        nom: personnel.nom,
        prenom: personnel.prenom,
        sexe: personnel.sexe,
        commentaire: personnel.commentaire,
        idGroupement: personnel.idGroupement,
        contact: personnel.contact,
      });
    }
  }, [personnel]);

  const goToList = () => push(PERSONNEL_GROUPEMENT_URL);

  return (
    <div className={classes.container}>
      {mutationSuccess ? (
        actionSuccess
      ) : !isOnList ? (
        stepperComponent
      ) : (
        <CustomTabs
          tabs={tabs}
          clickHandler={handleTabsClick}
          activeStep={activeStep}
          hideArrow={true}
        />
      )}
    </div>
  );
};

export default NewPersonnelGroupement;
