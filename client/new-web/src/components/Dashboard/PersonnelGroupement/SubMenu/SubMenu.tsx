import React, { useState } from 'react';
import SearchInput from '../../../Common/newCustomContent/SearchInput/SearchInput';
import { Add } from '@material-ui/icons';
import CustomButton from '../../../Common/CustomButton';
import useStyles from './styles';
import {
  useButtonHeadAction,
  useCheckedPersonnelGroupement,
  useCheckedPersonnelAffectation,
} from '../utils/utils';
import { RouteComponentProps, withRouter } from 'react-router';
import { ConfirmDeleteDialog } from '../../../Common/ConfirmDialog';
import AffectationDialog from '../../../Common/AffectationDialog';
interface SubMenuProps {
  isOnList?: string | boolean;
  isOnEdit?: string | boolean;
  isOnCreate?: boolean;
  disabledSaveBtn: () => undefined | boolean;
  createPersonnel: (variables: any) => void;
  values?: any;
  sexe?: string;
  idGroupement?: string;
  goToAddPersonnel: any;
  goBack: () => any;
  personnelOnClickConfirmDelete?: any;
  personnelDeleteRow?: any;
  affectationOnClickConfirmDelete?: any;
  affectationDeleteRow?: any;
  personnelToAffect?: any;
  setPersonnelToAffect?: (value: any) => void;
  openDeleteDialog?: any;
  setOpenDeleteDialog?: any;
  openAffectationDialog?: any;
  setOpenAffectationDialog?: any;
  affectPersonnel?: any;
}

const SubMenu: React.FC<SubMenuProps & RouteComponentProps> = props => {
  const {
    isOnEdit,
    isOnList,
    createPersonnel,
    disabledSaveBtn,
    isOnCreate,
    values,
    sexe,
    idGroupement,
    goToAddPersonnel,
    goBack,
    personnelOnClickConfirmDelete,
    personnelDeleteRow,
    openDeleteDialog,
    setOpenDeleteDialog,
    openAffectationDialog,
    setOpenAffectationDialog,
    location: { pathname },
    personnelToAffect,
    setPersonnelToAffect,
    affectPersonnel,
  } = props;

  const classes = useStyles({});
  const checkedsPersonnelGroupement = useCheckedPersonnelGroupement();
  const checkedsPersonnelAffectation = useCheckedPersonnelAffectation();
  const isOnAffecation = pathname.includes('/affectation');
  const DeleteDialogContent = () => {
    if (personnelDeleteRow) {
      return (
        <span>
          Êtes-vous sur de vouloir supprimer
          <span style={{ fontWeight: 'bold' }}>
            {' '}
            {personnelDeleteRow.civilite || ''} {personnelDeleteRow.prenom || ''}{' '}
            {personnelDeleteRow.nom || ''}{' '}
          </span>
          ?
        </span>
      );
    }
    return <span>Êtes-vous sur de vouloir supprimer ces personnels ?</span>;
  };
  const confirmDeleteDialog = (
    <ConfirmDeleteDialog
      open={openDeleteDialog}
      setOpen={setOpenDeleteDialog}
      content={<DeleteDialogContent />}
      onClickConfirm={personnelOnClickConfirmDelete}
    />
  );

  const affectationDialog = (
    <AffectationDialog
      open={openAffectationDialog}
      setOpen={setOpenAffectationDialog}
      personnelToAffect={personnelToAffect}
      setPersonnelToAffect={setPersonnelToAffect}
      affectPersonnel={affectPersonnel}
    />
  );

  return (
    <>
      <div className={classes.addButton}>
        {((!isOnAffecation &&
          checkedsPersonnelGroupement &&
          checkedsPersonnelGroupement.length > 0) ||
          !isOnList) && (
          <CustomButton
            className={classes.deleteButton}
            color="default"
            onClick={() => (!isOnList ? goBack() : setOpenDeleteDialog(true))}
          >
            {!isOnList ? 'Annuler' : 'Supprimer la sélection'}
          </CustomButton>
        )}
        {((isOnAffecation &&
          checkedsPersonnelAffectation &&
          checkedsPersonnelAffectation.length > 0) ||
          !isOnList) && (
          <CustomButton
            className={classes.deleteButton}
            color="default"
            onClick={() => (!isOnList ? goBack() : setOpenDeleteDialog(true))}
          >
            {!isOnList ? 'Annuler' : 'Affectation Groupée'}
          </CustomButton>
        )}
        <CustomButton
          color="secondary"
          startIcon={!isOnList ? null : <Add />}
          // tslint:disable-next-line: jsx-no-lambda
          onClick={() =>
            !isOnList
              ? createPersonnel({ variables: { ...values, sexe: sexe as any, idGroupement } })
              : goToAddPersonnel()
          }
          disabled={disabledSaveBtn()}
        >
          {isOnCreate ? 'Ajouter' : isOnEdit ? 'Modifier' : 'Ajouter un personnel'}
        </CustomButton>
      </div>
      <div className={classes.searchInputBox}>
        <SearchInput searchPlaceholder="Rechercher un personnel de groupement" />
      </div>
      {confirmDeleteDialog}
      {setPersonnelToAffect && affectationDialog}
    </>
  );
};

export default withRouter(SubMenu);
