import React, { FC } from 'react';
import FichePharmacieCard from '../FichePharmacieCard';
import useStyles from './styles';
import classnames from 'classnames';
import InformationsGenerales from './InformationsGenerales';
import Contacts from './Contacts';
import Segmentation from './Segmentation';
import Adresse from './Adresse';
import { FichePharmacieChildProps } from '../FichePharmacie';
import ComplementInformations from './ComplementInformations';
import Horaires from './Horaires';
import InformationsRegion from './InformationsRegion';
import Commentaires from './Commentaires';

const Coordonnees: FC<FichePharmacieChildProps> = ({
  pharmacie,
  handleChange,
  handleChangeDate,
  values,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  isEdit,
}) => {
  const classes = useStyles({});

  const cards = [
    {
      title: 'Informations Générales',
      children: (
        <InformationsGenerales
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      title: 'Contacts',
      children: (
        <Contacts
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      title: 'Segmentation',
      children: (
        <Segmentation
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      title: 'Adresse',
      children: (
        <Adresse
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
  ];

  const cards2 = [
    {
      title: "Complément d'informations",
      children: (
        <ComplementInformations
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      title: 'Horaires',
      children: (
        <Horaires
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      title: 'Informations Région',
      children: (
        <InformationsRegion
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      title: 'Commentaires',
      children: (
        <Commentaires
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
  ];

  return (
    <div className={classes.coordonneesRoot}>
      <div
        className={classnames(
          classes.coordonneeCardsContainer,
          classes.coordonneeCardsContainerFirst,
        )}
      >
        {cards.map((card, index) => (
          <FichePharmacieCard key={`fiche_pharma_coordonnee_1_${index}`} title={card.title}>
            {card.children}
          </FichePharmacieCard>
        ))}
      </div>
      <div
        className={classnames(
          classes.coordonneeCardsContainer,
          classes.coordonneeCardsContainerSecond,
        )}
      >
        {cards2.map((card, index) => (
          <FichePharmacieCard key={`fiche_pharma_coordonnee_2_${index}`} title={card.title}>
            {card.children}
          </FichePharmacieCard>
        ))}
      </div>
    </div>
  );
};

export default Coordonnees;
