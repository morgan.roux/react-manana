import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
// import useStyles from '../styles';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import { CustomCheckbox } from '../../../../../Common/CustomCheckbox';
import { useCommonStyles } from '../../styles';
import { CustomDatePicker } from '../../../../../Common/CustomDateTimePicker';
import { noValueText } from '../../../../../../Constant/text';

const InformationsGenerales: FC<FichePharmacieChildProps> = ({
  pharmacie,
  handleChange,
  handleChangeDate,
  values,
  isEdit,
  // handleChangeAutoComplete,
  // handleChangeInputAutoComplete,
}) => {
  // const classes = useStyles({});
  const commonClasses = useCommonStyles();
  const { entreeSortie, cip, numFiness, sortie, sortieFuture } = values;
  const p = pharmacie;
  const motifEntre =
    (p &&
      p.entreeSortie &&
      p.entreeSortie.pharmacieMotifEntree &&
      p.entreeSortie.pharmacieMotifEntree.libelle) ||
    noValueText;

  return (
    <div className={commonClasses.cardContent}>
      <CustomDatePicker
        label="Date d'entrée"
        placeholder="jj/mm/aaaa"
        value={(entreeSortie && entreeSortie.dateEntree) || null}
        name="entreeSortie.dateEntree"
        onChange={handleChangeDate('entreeSortie.dateEntree')}
        InputLabelProps={{ shrink: true }}
        disabled={isEdit}
      />
      <div className={commonClasses.infoContainer}>
        <span className={commonClasses.infoTitle}>Motif d'entré</span>
        <span className={commonClasses.infoValue}>{motifEntre}</span>
      </div>
      <div className={commonClasses.infoContainer}>
        <span className={commonClasses.infoTitle}>Dernier CIP</span>
        <span className={commonClasses.infoValue}>{cip || noValueText}</span>
      </div>
      <CustomFormTextField
        label="Numéro CIP"
        value={cip || ''}
        onChange={handleChange}
        name="cip"
        type="number"
        disabled={isEdit}
      />
      <CustomFormTextField
        label="Numéro Finesse"
        value={numFiness || ''}
        name="numFiness"
        onChange={handleChange}
        type="number"
        disabled={isEdit}
      />
      <div className={commonClasses.infoContainer}>
        <CustomCheckbox
          label="Sortie Future"
          name="sortieFuture.checkbox.sortieFuture"
          value={sortieFuture}
          checked={sortieFuture}
          onChange={handleChange}
          color="secondary"
          disabled={isEdit}
        />
        <CustomCheckbox
          label="Sortie"
          name="sortie.checkbox.sortie"
          value={sortie}
          checked={sortie}
          onChange={handleChange}
          color="secondary"
          disabled={isEdit}
        />
      </div>
    </div>
  );
};

export default InformationsGenerales;
