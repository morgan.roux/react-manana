import React, { FC } from 'react';
import { PHARMACIE_pharmacie } from '../../../../../../graphql/Pharmacie/types/PHARMACIE';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps';

interface MapProps {
  pharmacie: PHARMACIE_pharmacie | null;
}

const Map: FC<MapProps> = ({ pharmacie }) => {
  const latitude = (pharmacie && pharmacie.latitude) || '';
  const longitude = (pharmacie && pharmacie.longitude) || '';

  return (
    <GoogleMap
      defaultCenter={{ lat: parseFloat(latitude), lng: parseFloat(longitude) }}
      defaultZoom={17}
    >
      <Marker position={{ lat: parseFloat(latitude), lng: parseFloat(longitude) }} />
    </GoogleMap>
  );
};

const WrappedMap = withScriptjs(withGoogleMap(Map));

export default WrappedMap;
