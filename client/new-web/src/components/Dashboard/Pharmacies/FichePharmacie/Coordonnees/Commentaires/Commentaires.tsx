import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
// import useStyles from '../styles';
import CustomTextarea from '../../../../../Common/CustomTextarea';
import { useCommonStyles } from '../../styles';

const Commentaires: FC<FichePharmacieChildProps> = ({
  handleChange,
  values,
  isEdit,
  // pharmacie,
  // handleChangeDate,
  // handleChangeAutoComplete,
  // handleChangeInputAutoComplete,
}) => {
  // const classes = useStyles({});
  const commonClasses = useCommonStyles();
  const { commentaire } = values;

  return (
    <div className={commonClasses.cardContent}>
      <CustomTextarea
        label=""
        name="commentaire"
        value={commentaire || ''}
        onChangeTextarea={handleChange}
        rows={9}
        rowsMax={9}
        disabled={isEdit}
      />
    </div>
  );
};

export default Commentaires;
