import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
// import useStyles from '../styles';
import { useCommonStyles } from '../../styles';
import { CustomDatePicker } from '../../../../../Common/CustomDateTimePicker';
import { useQuery, useApolloClient } from '@apollo/client';
import { GET_TYPOLOGIES } from '../../../../../../graphql/Typologie';
import { TYPOLOGIES } from '../../../../../../graphql/Typologie/types/TYPOLOGIES';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import Backdrop from '../../../../../Common/Backdrop';
import { TRANCHE_CA_S } from '../../../../../../graphql/TrancheCA/types/TRANCHE_CA_S';
import { GET_TRANCHE_CA_S } from '../../../../../../graphql/TrancheCA';
import { GET_QUALITES } from '../../../../../../graphql/Qualite/query';
import { QUALITES } from '../../../../../../graphql/Qualite/types/QUALITES';
import { CONTRATS } from '../../../../../../graphql/Contrat/types/CONTRATS';
import { GET_CONTRATS } from '../../../../../../graphql/Contrat';
import Inputs from '../../../../../Common/Inputs';
import { InputInterface } from '../../../../../Common/Inputs/Inputs';

const Segmentation: FC<FichePharmacieChildProps> = ({
  // pharmacie,
  handleChange,
  handleChangeDate,
  values,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  isEdit,
}) => {
  // const classes = useStyles({});
  const commonClasses = useCommonStyles();
  const client = useApolloClient();

  const { segmentation: s } = values;

  const idTypologie = (s && s.idTypologie) || '';
  const idTrancheCA = (s && s.idTrancheCA) || '';
  const idQualite = (s && s.idQualite) || '';
  const idContrat = (s && s.idContrat) || '';
  const dateSignature = (s && s.dateSignature) || null;

  /**
   * Get typologie list
   */
  const { data: typologieData, loading: typologieLoading } = useQuery<TYPOLOGIES>(GET_TYPOLOGIES, {
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  /**
   * Get trancheCA list
   */
  const { data: trancheCAData, loading: trancheCALoading } = useQuery<TRANCHE_CA_S>(
    GET_TRANCHE_CA_S,
    {
      onError: errors => {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );

  /**
   * Get qualite list
   */
  const { data: qualiteData, loading: qualiteLoading } = useQuery<QUALITES>(GET_QUALITES, {
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  /**
   * Get contrat list
   */
  const { data: contratData, loading: contratLoading } = useQuery<CONTRATS>(GET_CONTRATS, {
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const typologieList = (typologieData && typologieData.typologies) || [];
  const trancheCAList = (trancheCAData && trancheCAData.trancheCAs) || [];
  const qualiteList = (qualiteData && qualiteData.qualites) || [];
  const contratList = (contratData && contratData.contrats) || [];

  const inputs: InputInterface[] = [
    {
      name: 'segmentation.idTypologie',
      label: 'Typologie',
      value: idTypologie,
      selectOptions: typologieList,
      selectOptionValueKey: 'libelle',
      type: 'autocomplete',
      autocompleteValue: typologieList.find(i => i && i.id === idTypologie),
      disabled: isEdit,
    },
    {
      name: 'segmentation.idTrancheCA',
      label: 'Tranche de CA',
      value: idTrancheCA,
      selectOptions: trancheCAList,
      selectOptionValueKey: 'libelle',
      type: 'autocomplete',
      autocompleteValue: trancheCAList.find(i => i && i.id === idTrancheCA),
      disabled: isEdit,
    },
    {
      name: 'segmentation.idQualite',
      label: 'Démarche qualité',
      value: idQualite,
      selectOptions: qualiteList,
      selectOptionValueKey: 'libelle',
      type: 'autocomplete',
      autocompleteValue: qualiteList.find(i => i && i.id === idQualite),
      disabled: isEdit,
    },
    {
      name: 'segmentation.idContrat',
      label: 'Contrat',
      value: idContrat,
      selectOptions: contratList,
      selectOptionValueKey: 'nom',
      type: 'autocomplete',
      autocompleteValue: contratList.find(i => i && i.id === idContrat),
      disabled: isEdit,
    },
  ];

  return (
    <div className={commonClasses.cardContent}>
      {(typologieLoading || trancheCALoading || qualiteLoading || contratLoading) && <Backdrop />}
      {/* {inputs.map((input, index) => (
        <CustomSelect
          key={`fiche_pharma_segmentation_inputs_${index}`}
          listId={input.selectOptionIdKey || 'id'}
          index={input.selectOptionValueKey || 'nom'}
          list={input.selectOptions || []}
          name={input.name}
          label={input.label}
          value={input.value}
          onChange={handleChange}
        />
      ))} */}
      <Inputs
        inputs={inputs}
        handleChange={handleChange}
        handleChangeDate={handleChangeDate}
        handleChangeAutoComplete={handleChangeAutoComplete}
        handleChangeInputAutoComplete={handleChangeInputAutoComplete}
      />
      <CustomDatePicker
        label="Date de signature"
        placeholder="jj/mm/aaaa"
        value={dateSignature}
        name="segmentation.dateSignature"
        onChange={handleChangeDate('segmentation.dateSignature')}
        InputLabelProps={{ shrink: true }}
        disabled={isEdit}
      />
    </div>
  );
};

export default Segmentation;
