import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
import useStyles from '../styles';
import { useCommonStyles } from '../../styles';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import Backdrop from '../../../../../Common/Backdrop';
import WrappedMap from './Map';

const Adresse: FC<FichePharmacieChildProps> = ({
  pharmacie,
  handleChange,
  values,
  isEdit,
  // handleChangeDate,
  // handleChangeAutoComplete,
  // handleChangeInputAutoComplete,
}) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const { adresse1, adresse2, cp, ville, pays, latitude, longitude } = values;

  return (
    <div className={classes.addressCardContent}>
      <div className={commonClasses.cardContent}>
        <CustomFormTextField
          label="Adresse"
          value={adresse1 || ''}
          name="adresse1"
          onChange={handleChange}
          disabled={isEdit}
        />
        <CustomFormTextField
          label="Complément"
          value={adresse2 || ''}
          name="adresse2"
          onChange={handleChange}
          disabled={isEdit}
        />
        <div className={commonClasses.twoInputContainer}>
          <CustomFormTextField
            label="Code postal"
            value={cp || ''}
            name="cp"
            onChange={handleChange}
            type="number"
            disabled={isEdit}
          />
          <CustomFormTextField
            label="Ville"
            value={ville || ''}
            name="ville"
            onChange={handleChange}
            disabled={isEdit}
          />
        </div>
        <CustomFormTextField
          label="Pays"
          value={pays || ''}
          name="pays"
          onChange={handleChange}
          disabled={isEdit}
        />
        <div className={commonClasses.twoInputContainer}>
          <CustomFormTextField
            label="Latitude"
            value={latitude || ''}
            name="latitude"
            onChange={handleChange}
            type="number"
            disabled={isEdit}
          />
          <CustomFormTextField
            label="Longitude"
            value={longitude || ''}
            name="longitude"
            onChange={handleChange}
            type="number"
            disabled={isEdit}
          />
        </div>
      </div>
      <div className={classes.mapAdressContainer}>
        <WrappedMap
          googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&librairies=geometry,drawing,places&key=AIzaSyAC0s2SBG93g4SqI16JiNxI9rBB6Nn1cOU`}
          loadingElement={<Backdrop />}
          containerElement={<div style={{ height: '100%' }} />}
          mapElement={<div style={{ height: '100%' }} />}
          pharmacie={pharmacie}
        />
      </div>
    </div>
  );
};

export default Adresse;
