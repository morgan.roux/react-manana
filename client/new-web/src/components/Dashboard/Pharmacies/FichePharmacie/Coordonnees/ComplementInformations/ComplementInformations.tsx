import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
import useStyles from '../styles';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import { Typography } from '@material-ui/core';
import { useCommonStyles } from '../../styles';
import { FormInputInterface } from '../../../PharmacieForm/PharmacieForm';
import { useQuery, useApolloClient } from '@apollo/client';
import { GROSSISTES } from '../../../../../../graphql/Grossiste/types/GROSSISTES';
import { GET_GROSSISTES } from '../../../../../../graphql/Grossiste';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import Backdrop from '../../../../../Common/Backdrop';
import { GENERIQUEURS } from '../../../../../../graphql/Generiqueur/types/GENERIQUEURS';
import { GET_GENERIQUEURS } from '../../../../../../graphql/Generiqueur';
import { head, slice } from 'lodash';
import { AutocompleteInput } from '../../../../../Common/CustomAutocomplete';

const ComplementInformations: FC<FichePharmacieChildProps> = ({
  handleChange,
  values,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  isEdit,
  // pharmacie,
  // handleChangeDate,
}) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();
  const client = useApolloClient();

  const { uga, nbEmploye, nbAssistant, nbAutreTravailleur, idGrossistes, idGeneriqueurs } = values;

  // const p = pharmacie;

  const idGrossistes0 = (idGrossistes && head(idGrossistes)) || '';
  const idGrossistes1 = (idGrossistes && head(slice(idGrossistes, 1))) || '';

  const idGeneriqueurs0 = (idGeneriqueurs && head(idGeneriqueurs)) || '';
  const idGeneriqueurs1 = (idGeneriqueurs && head(slice(idGeneriqueurs, 1))) || '';

  const employeesInfos = [
    { name: 'nbEmploye', label: 'Employés', value: nbEmploye || 0, disabled: isEdit },
    { name: 'nbAssistant', label: 'Assistants', value: nbAssistant || 0, disabled: isEdit },
    {
      name: 'nbAutreTravailleur',
      label: 'Autres',
      value: nbAutreTravailleur || 0,
      disabled: isEdit,
    },
  ];

  /**
   * Get grossiste list
   */
  const { data: grossisteData, loading: grossisteLoading } = useQuery<GROSSISTES>(GET_GROSSISTES, {
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  /**
   * Get generiqueur list
   */
  const { data: generiqueurData, loading: generiqueurLoading } = useQuery<GENERIQUEURS>(
    GET_GENERIQUEURS,
    {
      onError: errors => {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );

  const grossisteList = (grossisteData && grossisteData.grossistes) || [];
  const generiqueurList = (generiqueurData && generiqueurData.generiqueurs) || [];

  const grossisteAndGeneriqueurInputs: FormInputInterface[] = [
    {
      title: 'Grossiste',
      inputs: [
        {
          name: 'idGrossistes_0',
          value: idGrossistes0,
          selectOptions: grossisteList,
          selectOptionValueKey: 'nom',
          autocompleteValue: grossisteList.find(i => i && i.id === idGrossistes0),
        },
        {
          name: 'idGrossistes_1',
          value: idGrossistes1,
          selectOptions: grossisteList,
          selectOptionValueKey: 'nom',
          autocompleteValue: grossisteList.find(i => i && i.id === idGrossistes1),
        },
      ],
    },
    {
      title: 'Génériqueur',
      inputs: [
        {
          name: 'idGeneriqueurs_0',
          value: idGeneriqueurs0,
          selectOptions: generiqueurList,
          selectOptionValueKey: 'nom',
          autocompleteValue: generiqueurList.find(i => i && i.id === idGeneriqueurs0),
        },
        {
          name: 'idGeneriqueurs_1',
          value: idGeneriqueurs1,
          selectOptions: generiqueurList,
          selectOptionValueKey: 'nom',
          autocompleteValue: generiqueurList.find(i => i && i.id === idGeneriqueurs1),
        },
      ],
    },
  ];

  return (
    <div className={commonClasses.cardContent}>
      {(grossisteLoading || generiqueurLoading) && <Backdrop />}
      <div className={commonClasses.infoContainer}>
        {employeesInfos.map((item, index) => (
          <Typography
            key={`employeesInfos_info_complement_${index}`}
            className={commonClasses.employeesInfos}
          >
            {item.label}
            <input
              type="number"
              name={item.name}
              value={item.value}
              onChange={handleChange}
              style={{ width: item.value === 0 ? 55 : item.value.toString().length * 14 }}
              disabled={isEdit}
            />
          </Typography>
        ))}
      </div>
      <CustomFormTextField
        label="UGA"
        value={uga || ''}
        name="uga"
        onChange={handleChange}
        disabled={isEdit}
      />
      <div className={classes.gridFormContainer}>
        {grossisteAndGeneriqueurInputs.map((item, index) => (
          <div
            className={classes.twoVerticalInputContainer}
            key={`grossisteAndGeneriqueurInputs_${index}`}
          >
            <Typography className={commonClasses.employeesInfos}>{item.title}</Typography>
            {item.inputs.map((input, inputIndex) => (
              <AutocompleteInput
                key={`custom_select_info_comple_${inputIndex}`}
                options={input.selectOptions || []}
                optionLabelKey={input.selectOptionValueKey || 'nom'}
                value={input.autocompleteValue || ''}
                inputValue={
                  input.autocompleteValue
                    ? input.autocompleteValue[input.selectOptionValueKey || 'nom']
                    : input.value
                }
                name={input.name}
                id={`AutocompleteInput_${inputIndex}`}
                onChangeAutocomplete={handleChangeAutoComplete(
                  `autocompleteInput${input.name}`,
                  input.name,
                )}
                onInputChange={handleChangeInputAutoComplete(
                  `autocompleteInput${input.name}`,
                  input.name,
                )}
                label=""
                required={false}
                disableClearable={false}
                disabled={isEdit}
              />
            ))}
          </div>
        ))}
      </div>
    </div>
  );
};

export default ComplementInformations;
