import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
import useStyles from '../styles';
import { Typography, Divider } from '@material-ui/core';
import { useCommonStyles } from '../../styles';

const Horaires: FC<FichePharmacieChildProps> = ({}) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const horaires = [
    { day: 'Lun.', matin: '8h30 - 12h30', apresMidi: '13h30 - 19h30' },
    { day: 'Mar.', matin: '8h30 - 12h30', apresMidi: '13h30 - 19h30' },
    { day: 'Mer.', matin: '8h30 - 12h30', apresMidi: '13h30 - 19h30' },
    { day: 'Jeu.', matin: '8h30 - 12h30', apresMidi: '13h30 - 19h30' },
    { day: 'Ven.', matin: '8h30 - 12h30', apresMidi: '13h30 - 19h30' },
    { day: 'Sam.', matin: '8h30 - 12h30', apresMidi: '13h30 - 18h30' },
    { day: 'Dim.', matin: 'Fermé', apresMidi: '' },
  ];

  return (
    <div className={commonClasses.cardContent}>
      {horaires.map((h, index) => (
        <div className={classes.horaireRow} key={`horaires_${index}`}>
          <Typography className={classes.horaireDay}>{h.day}</Typography>
          <Typography className={classes.horaireValue}>{h.matin}</Typography>
          <Divider orientation="vertical" />
          <Typography className={classes.horaireValue}>{h.apresMidi}</Typography>
        </div>
      ))}
    </div>
  );
};

export default Horaires;
