import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
// import useStyles from '../styles';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import { useCommonStyles } from '../../styles';
import { InputInterface } from '../../../PharmacieForm/PharmacieForm';

const Contacts: FC<FichePharmacieChildProps> = ({
  handleChange,
  values,
  isEdit,
  // handleChangeDate,
  // pharmacie,
  // handleChangeAutoComplete,
  // handleChangeInputAutoComplete,
}) => {
  // const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const { contact: c } = values;

  const numTel = (c && c.telProf) || '';
  const numTelMobile = (c && c.telMobProf) || '';
  const numFax = (c && c.faxProf) || '';
  const mail1 = (c && c.mailProf) || '';
  const mail2 = (c && c.mailPerso) || '';
  const siteWeb = (c && c.siteProf) || '';

  const inputs: InputInterface[] = [
    { label: 'Numéro Tél', value: numTel, name: 'contact.telProf', inputType: 'tel' },
    {
      label: 'Numéro Mobile',
      value: numTelMobile,
      name: 'contact.telMobProf',
      inputType: 'tel',
    },
    { label: 'Numéro Fax', value: numFax, name: 'contact.faxProf', inputType: 'number' },
    { label: 'Mail 1', value: mail1, name: 'contact.mailProf', inputType: 'text' },
    { label: 'Mail 2', value: mail2, name: 'contact.mailPerso', inputType: 'text' },
    { label: 'Site web', value: siteWeb, name: 'contact.siteProf', inputType: 'text' },
  ];

  return (
    <div className={commonClasses.cardContent}>
      {inputs.map((input, index) => (
        <CustomFormTextField
          key={`fiche_pharma_contacts_inputs_${index}`}
          label={input.label}
          value={input.value}
          name={input.name}
          onChange={handleChange}
          type={input.inputType}
          disabled={isEdit}
        />
      ))}
    </div>
  );
};

export default Contacts;
