import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const usesStyles = makeStyles((theme: Theme) =>
  createStyles({
    fichePharmacieMainContent: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      flexGrow: 1,
      padding: '0px 25px',
      '& .MuiOutlinedInput-input': {
        padding: '7px 14px !important',
      },
      '& .MuiOutlinedInput-adornedEnd': {
        paddingRight: 0,
      },

      '@media (max-width: 1024px)': {
        '& .MuiTab-root': {
          minWidth: '100px !important',
        },
      },
    },
  }),
);

const useCommonStyles = makeStyles((theme: Theme) =>
  createStyles({
    cardContent: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      '& > div.MuiTextField-root:not(:nth-last-child(1))': {
        marginBottom: 20,
      },
      '& > div.MuiAutocomplete-root:not(:nth-last-child(1))': {
        marginBottom: 16,
      },

      '& > div:nth-last-child(-n+1)': {
        marginBottom: 0,
      },
      '& .MuiFormControl-marginDense': {
        marginTop: 0,
      },
    },

    infoContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      '& .MuiFormControlLabel-label': {
        color: theme.palette.secondary.main,
        textDecoration: 'underline',
        fontWeight: 500,
        fontSize: 14,
        letterSpacing: 0,
      },
      '& .MuiSvgIcon-root': {
        width: 20,
        height: 20,
      },
      '& .MuiCheckbox-root': {
        // color: theme.palette.common.black,
      },
      '& .Mui-checked': {
        // color: theme.palette.secondary.main,
      },
    },
    infoContainerEntreSortie: {
      display: 'flex',
      flexDirection: 'row',
      margin: '20px 0',
      alignItems: 'center',
      '& .MuiFormControlLabel-root': {
        alignItems: 'end',
      },
      '& .MuiFormControlLabel-label': {
        fontWeight: 500,
        fontSize: 14,
        letterSpacing: 0,
      },
      '& .MuiSvgIcon-root': {
        width: 20,
        height: 20,
      },
      '& .MuiButtonBase-root': {
        padding: '0 9px',
      },
    },
    infoContainerEntreSortieTop: {
      display: 'flex',
      flexDirection: 'row',
      margin: '0 0 20px',
      alignItems: 'center',
      '& .MuiFormControlLabel-root': {
        alignItems: 'end',
      },
      '& .MuiFormControlLabel-label': {
        fontWeight: 500,
        fontSize: 14,
        letterSpacing: 0,
      },
      '& .MuiSvgIcon-root': {
        width: 20,
        height: 20,
      },
      '& .MuiButtonBase-root': {
        padding: '0 9px',
      },
    },
    infoTitle: {
      fontSize: 12,
      fontWeight: 'normal',
      letterSpacing: 0.4,
      color: '#9E9E9E',
    },

    satisfactionTitle: {
      fontSize: 14,
      fontWeight: 500,
      color: '#424242',
      display: 'flex',
      alignItems: 'center',
      marginBottom: 23,
    },
    infoValue: {
      fontSize: 14,
      fontFamily: 'Montserrat',
      letterSpacing: 0.24,
      textAlign: 'right',
      color: theme.palette.common.black,
    },
    twoInputContainer: {
      display: 'flex',
      flexDirection: 'row',
      marginBottom: 21,
      '& > div': {
        marginBottom: 0,
      },
      '& > div:nth-child(1)': {
        marginRight: 10,
      },
    },
    mb12: {
      marginBottom: 12,
    },
    employeesInfos: {
      fontSize: 12,
      letterSpacing: 0.4,
      marginBottom: 23,
      display: 'flex',
      alignItems: 'center',
      '& > span, & > input': {
        border: '1px solid #616161',
        borderRadius: 4,
        background: '#F2F2F2 0% 0% no-repeat padding-box',
        padding: '4px 5px',
        marginLeft: 5,
      },
      '& > input': {
        minWidth: '33px !important',
      },
    },
    inputsWithLeftLabelContainer: {
      display: 'flex',
      flexDirection: 'column',
      '& > div': {
        alignItems: 'center',
        '& > div': {
          minWidth: 'fit-content',
          '& > p': {
            marginRight: 40,
          },
        },
      },
      '& > div:not(:nth-last-child(1))': {
        marginBottom: 15,
      },
    },
    checkboxInputContainer: {
      '& .MuiFormControlLabel-label': {
        // color: theme.palette.secondary.main,
        // textDecoration: 'underline',
        fontSize: 14,
        letterSpacing: 0,
      },
      '& .MuiSvgIcon-root': {
        width: 20,
        height: 20,
      },
      '& .MuiCheckbox-root': {
        // color: theme.palette.common.black,
      },
      '& .Mui-checked': {
        // color: theme.palette.secondary.main,
      },
    },
    contentInputTextfield: {
      '& > div': {
        marginBottom: 16,
        '& .MuiCheckbox-root': {
          padding: '0 9px',
        },
      },
    },
    espacePharmacieInfoContainer: {
      '@media (max-width: 1024px)': {
        flexWrap: 'wrap',
        justifyContent: 'start',
        '& p': {
          marginRight: 16,
        },
      },
    },
  }),
);

export { useCommonStyles };

export default usesStyles;
