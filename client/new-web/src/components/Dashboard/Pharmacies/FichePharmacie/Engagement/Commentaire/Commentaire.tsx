import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
// import useStyles from '../styles';
import { useCommonStyles } from '../../styles';
import CustomTextarea from '../../../../../Common/CustomTextarea';

const Commentaire: FC<FichePharmacieChildProps> = ({ handleChange, isEdit }) => {
  // const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const commentaire = '';

  return (
    <div className={commonClasses.cardContent}>
      <CustomTextarea
        label="Commentaire"
        name="commentaire"
        value={commentaire}
        onChangeTextarea={handleChange}
        rows={4}
        disabled={isEdit}
      />
    </div>
  );
};

export default Commentaire;
