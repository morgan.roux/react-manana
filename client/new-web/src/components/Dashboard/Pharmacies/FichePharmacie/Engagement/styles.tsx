import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    engagementRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: '20px 0px',
      '& > div': {
        width: '100%',
      },
      '& .MuiInputBase-root.Mui-disabled': {
        background: '#F2F2F2',
      },
      '& textarea[disabled]': {
        background: '#F2F2F2',
      },
    },
    tablesContainer: {
      display: 'flex',
      flexDirection: 'row',
      marginBottom: 15,
      '& > div': {
        width: '50%',
        '@media (max-width: 1300px)': {
          width: '100%',
          marginBottom: 24,
        },
      },
      '& > div:not(:nth-last-child(1)) ': {
        marginRight: 15,
      },
      '& .MuiTableHead-root th': {
        padding: '2px 0 5px 16px !important',
        minWidth: 'auto',
        maxWidth: 183,
        textAlign: 'center',
      },
      '& .MuiTableHead-root *': {
        minHeight: 48,
      },
      '& .MuiTableBody-root td': {
        padding: '2px 0 5px 16px !important',
        minWidth: 'auto',
        maxWidth: 183,
      },
      '@media (max-width: 1300px)': {
        flexWrap: 'wrap',
      },
    },
    inputsContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      '& > div': {
        marginBottom: 0,
      },
    },
  }),
);

export default useStyles;
