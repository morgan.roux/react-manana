import React, { FC } from 'react'
import { Box, Button } from '@material-ui/core'
import useExportMatriceTache from './../../../DemarcheQualite/MatriceTache/ExportMatriceTache/useExportMatriceTache'

import { PictureAsPdf, Description } from '@material-ui/icons';
import useStyles from './styles'

interface MatriceTachesProps {
    pharmacie: any
}

const MatriceTaches: FC<MatriceTachesProps> = ({ pharmacie }) => {
    const exportingMatriceTache = useExportMatriceTache()
    const classes = useStyles()


    return (
        <Box className={classes.root}>
            {exportingMatriceTache.component}
            <Box>
                <Button
                    className={classes.button}
                    variant="contained"
                    color="secondary"
                    startIcon={<PictureAsPdf />}

                    onClick={() => exportingMatriceTache.launch(pharmacie, 'pdf')}
                >
                    Exporter au format pdf
      </Button>
                <Button
                    className={classes.button}
                    variant="contained"
                    color="secondary"
                    startIcon={<Description />}
                    onClick={() => exportingMatriceTache.launch(pharmacie, 'xlsx')}

                >
                    Exporter au format excel
      </Button>
            </Box>
        </Box>
    )
}

export default MatriceTaches