import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      padding: 20,
      justifyContent:'center'
    },
    button: {
      margin: theme.spacing(1),
    },
  }),
);

export default useStyles;
