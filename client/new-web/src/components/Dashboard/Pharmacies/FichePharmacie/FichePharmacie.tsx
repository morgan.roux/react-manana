import React, { FC, useEffect, useState, Dispatch, SetStateAction, ChangeEvent } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import TitrePharmacie from './TitrePharmacie';
import useStyles from './styles';
import { useQuery } from '@apollo/client';
import { GET_PHARMACIE } from '../../../../graphql/Pharmacie/query';
import CustomTabs from '../../../Common/CustomTabs';
import { TabInterface } from '../../../Common/CustomTabs/CustomTabs';
import Coordonnees from './Coordonnees';
import SubToolbar from '../../../Common/newCustomContent/SubToolbar';
import CustomButton from '../../../Common/CustomButton';
import { ArrowBack } from '@material-ui/icons';
import { Box } from '@material-ui/core';
import { PHARMACIE_URL } from '../../../../Constant/url';
import EntreeSortie from './EntreeSortie';
import Comptabilite from './Comptabilite';
import Informatique from './Informatique';
import Achat from './Achat';
import EspaceVente from './EspaceVente';
import ChiffreAffaire from './ChiffreAffaire';
import Engagement from './Engagement';
import Digital from './Digital';
import MatriceTaches from './MatriceTaches'
import OptionPartage from './OptionPartage';

import {
  PHARMACIE_pharmacie,
  PHARMACIE,
  PHARMACIEVariables,
} from '../../../../graphql/Pharmacie/types/PHARMACIE';
import Backdrop from '../../../Common/Backdrop';
import usePharmacieFormForm, { PharmacieFormInterface } from '../PharmacieForm/usePharmacieForm';
import { map } from 'lodash';
import { useCreateUpdatePharmacie } from '../utils';
import { AppAuthorization } from '../../../../services/authorization';
import { getUser } from '../../../../services/LocalStorage';

export interface FichePharmacieChildProps {
  pharmacie: PHARMACIE_pharmacie | null;
  values: PharmacieFormInterface;
  disabledSaveBtn?: boolean;
  setDisabledSaveBtn?: Dispatch<SetStateAction<boolean>>;
  handleChange: (e: React.ChangeEvent<any>) => void;
  handleChangeDate: (name: string) => (date: any) => void;
  handleChangeAutoComplete: (fakeName: string, name: string) => (e: any, newValue: any) => void;
  handleChangeInputAutoComplete: (
    fakeName: string,
    name: string,
  ) => (e: ChangeEvent<any>, newInputValue: string) => void;
  isEdit: boolean;
}

interface FichePharmacieProps {
  id?: string;
  inModalView?: boolean;
  handleClickBack?: () => void;
}

const FichePharmacie: FC<FichePharmacieProps & RouteComponentProps> = ({
  id,
  match: { params },
  history,
  inModalView,
  handleClickBack,
}) => {
  const classes = useStyles({});
  const user = getUser();
  const auth = new AppAuthorization(user);

  const { idPharmacie } = params as any;
  const [disabledSaveBtn, setDisabledSaveBtn] = useState(false);
  const [isEdit, setisEdit] = useState(true);

  const { push } = history;

  const { loading, error, data } = useQuery<PHARMACIE, PHARMACIEVariables>(GET_PHARMACIE, {
    variables: { id: id || idPharmacie },
    fetchPolicy: 'cache-and-network',
  });

  const pharmacie = (data && data.pharmacie) || null;

  const {
    handleChange,
    handleChangeDate,
    values,
    setValues,
    handleChangeAutoComplete,
    handleChangeInputAutoComplete,
  } = usePharmacieFormForm(pharmacie as any);

  const { createUpdatePharmacie, mutationLoading, mutationSuccess } = useCreateUpdatePharmacie(
    values,
  );

  const handleIsEdit = () => {
    setisEdit(prevState => !prevState);
  };

  const tabs: TabInterface[] = [
    {
      id: 0,
      label: 'Coordonnées',
      content: (
        <Coordonnees
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      id: 1,
      label: 'Entrées/Sorties',
      content: (
        <EntreeSortie
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      id: 2,
      label: 'Comptabilité',
      content: (
        <Comptabilite
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          setValues={setValues}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      id: 3,
      label: 'Informatique',
      content: (
        <Informatique
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      id: 4,
      label: 'Achat',
      content: (
        <Achat
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      id: 5,
      label: 'Espace Vente',
      content: (
        <EspaceVente
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      id: 6,
      label: "Chiffre d'affaire",
      content: (
        <ChiffreAffaire
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          disabledSaveBtn={disabledSaveBtn}
          setDisabledSaveBtn={setDisabledSaveBtn}
          setValues={setValues}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      id: 7,
      label: 'Engagement',
      content: (
        <Engagement
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      id: 8,
      label: 'Digital',
      content: (
        <Digital
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          setValues={setValues}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          setDisabledSaveBtn={setDisabledSaveBtn}
          disabledSaveBtn={disabledSaveBtn}
          isEdit={isEdit}
        />
      ),
    },

    {
      id: 9,
      label: 'Matrice des fonctions',
      content: (
        <MatriceTaches
          pharmacie={pharmacie}
        />
      ),
    },

    {
      id: 10,
      label: 'Options de Partage',
      content: (
        <OptionPartage
          pharmacie={pharmacie}
        />
      ),
    },
  ];

  const onClickBack = () => {
    if (inModalView) {
      if (handleClickBack) {
        handleClickBack();
        return;
      }
      history.goBack();
      return;
    } else {
      push(`/db/${PHARMACIE_URL}`);
    }
  };

  const onClickSave = () => {
    createUpdatePharmacie();
    handleIsEdit();
  };

  /**
   * Set default valuse on pharmacie change
   */
  useEffect(() => {
    if (pharmacie) {
      const p = pharmacie;
      // Formate mutation variables
      const idGrossistes = (p.grossistes && map(p.grossistes, 'id')) || null;
      const idGeneriqueurs = (p.generiqueurs && map(p.generiqueurs, 'id')) || null;

      const contact = p.contact;
      const entreeSortie = p.entreeSortie;
      const satisfaction = p.satisfaction;
      const segmentation = p.segmentation;
      const compta = p.compta;
      const informatique = p.informatique;
      const achat = p.achat;
      const cap = p.cap;
      const concept = p.concept;
      const chiffreAffaire = p.statCA;

      setValues({
        id: p.id,
        cip: p.cip || '',
        nom: p.nom || '',
        adresse1: p.adresse1,
        adresse2: p.adresse2,
        cp: p.cp,
        ville: p.ville,
        pays: p.pays,
        longitude: p.longitude,
        latitude: p.latitude,
        numFiness: p.numFiness,
        sortie: p.sortie,
        sortieFuture: null /** TODO p.sortieFuture */,
        nbEmploye: p.nbEmploye,
        nbAssistant: p.nbAssistant,
        nbAutreTravailleur: p.nbAutreTravailleur,
        uga: p.uga,
        commentaire: p.commentaire,
        idGrossistes,
        idGeneriqueurs,
        contact: contact
          ? {
            adresse1: contact.adresse1 || null,
            adresse2: contact.adresse2 || null,
            cp: contact.cp || null,
            ville: contact.ville || null,
            pays: contact.pays || null,
            telProf: contact.telProf || null,
            faxProf: contact.faxProf || null,
            faxPerso: contact.faxPerso || null,
            telMobProf: contact.telMobProf || null,
            telPerso: contact.telPerso || null,
            telMobPerso: contact.telMobPerso || null,
            mailProf: contact.mailProf || null,
            mailPerso: contact.mailPerso || null,
            siteProf: contact.siteProf || null,
            sitePerso: contact.sitePerso || null,
            whatsAppMobProf: contact.whatsAppMobProf || null,
            whatsappMobPerso: contact.whatsappMobPerso || null,
            compteSkypeProf: contact.compteSkypeProf || null,
            compteSkypePerso: contact.compteSkypePerso || null,
            urlLinkedinProf: contact.urlLinkedinProf || null,
            urlLinkedinPerso: contact.urlLinkedinPerso || null,
            urlTwitterProf: contact.urlTwitterProf || null,
            urlTwitterPerso: contact.urlTwitterPerso || null,
            urlFacebookProf: contact.urlFacebookProf || null,
            urlFacebookPerso: contact.urlFacebookPerso || null,
          }
          : null,
        entreeSortie: entreeSortie
          ? {
            idContrat: (entreeSortie.contrat && entreeSortie.id) || null,
            idConcurent: (entreeSortie.concurrent && entreeSortie.concurrent.id) || null,
            idConcurentAncien: (entreeSortie.concurrent && entreeSortie.concurrent.id) || null,
            dateEntree: entreeSortie.dateEntree || null,
            idMotifEntree:
              (entreeSortie.pharmacieMotifEntree && entreeSortie.pharmacieMotifEntree.id) || null,
            commentaireEntree: entreeSortie.commentaireEntree || null,
            // TODO sortieFuture
            sortieFuture: 0,
            dateSortieFuture: entreeSortie.dateSortieFuture || null,
            idMotifSortieFuture:
              (entreeSortie.pharmacieMotifSortieFuture &&
                entreeSortie.pharmacieMotifSortieFuture.id) ||
              null,
            commentaireSortieFuture: entreeSortie.commentaireSortieFuture || null,
            // TODO sortie
            sortie: 0,
            dateSortie: entreeSortie.dateSortie || null,
            idMotifSortie:
              (entreeSortie.pharmacieMotifSortie && entreeSortie.pharmacieMotifSortie.id) || null,
            commentaireSortie: entreeSortie.commentaireSortie || null,
            // TODO dateSignature
            dateSignature: null,
          }
          : null,
        satisfaction: satisfaction
          ? {
            dateSaisie: satisfaction.dateSaisie || null,
            commentaire: satisfaction.commentaire || null,
            idSmyley: (satisfaction.smyley && satisfaction.smyley.id) || null,
          }
          : null,
        segmentation: segmentation
          ? {
            idTypologie: (segmentation.typologie && segmentation.typologie.id) || null,
            idTrancheCA: (segmentation.trancheCA && segmentation.trancheCA.id) || null,
            idQualite: (segmentation.qualite && segmentation.qualite.id) || null,
            idContrat: (segmentation.contrat && segmentation.contrat.id) || null,
            dateSignature: segmentation.dateSignature || null,
          }
          : null,
        compta: compta
          ? {
            siret: compta.siret || null,
            codeERP: compta.codeERP || null,
            ape: compta.ape || null,
            tvaIntra: compta.tvaIntra || null,
            rcs: compta.rcs || null,
            contactCompta: compta.contactCompta || null,
            chequeGrp: compta.chequeGrp || null,
            valeurChequeGrp: compta.valeurChequeGrp || null,
            commentaireChequeGrp: compta.commentaireChequeGrp || null,
            droitAccesGrp: compta.droitAccesGrp || null,
            valeurChequeAccesGrp: compta.valeurChequeAccesGrp || null,
            commentaireChequeAccesGrp: compta.commentaireChequeAccesGrp || null,
            structureJuridique: compta.structureJuridique || null,
            denominationSociale: compta.denominationSociale || null,
            telCompta: compta.telCompta || null,
            modifStatut: compta.modifStatut || null,
            raisonModif: compta.raisonModif || null,
            dateModifStatut: compta.dateModifStatut || null,
            nomBanque: compta.nomBanque || null,
            banqueGuichet: compta.banqueGuichet || null,
            banqueCompte: compta.banqueCompte || null,
            banqueRib: compta.banqueRib || null,
            banqueCle: compta.banqueCle || null,
            dateMajRib: compta.dateMajRib || null,
            iban: compta.iban || null,
            swift: compta.swift || null,
            dateMajIban: compta.dateMajIban || null,
          }
          : null,
        informatique: informatique
          ? {
            idLogiciel: (informatique.logiciel && informatique.logiciel.id) || null,
            numVersion: informatique.numVersion || null,
            dateLogiciel: informatique.dateLogiciel || null,
            nbrePoste: informatique.nbrePoste || null,
            nbreComptoir: informatique.nbreComptoir || null,
            nbreBackOffice: informatique.nbreBackOffice || null,
            nbreBureau: informatique.nbreBureau || null,
            commentaire: informatique.commentaire || null,
            idAutomate1: (informatique.automate1 && informatique.automate1.id) || null,
            dateInstallation1: informatique.dateInstallation1 || null,
            idAutomate2: (informatique.automate2 && informatique.automate2.id) || null,
            dateInstallation2: informatique.dateInstallation2 || null,
            idAutomate3: (informatique.automate3 && informatique.automate3.id) || null,
            dateInstallation3: informatique.dateInstallation3 || null,
          }
          : null,
        achat: achat
          ? {
            dateDebut: achat.dateDebut || null,
            dateFin: achat.dateFin || null,
            identifiantAchatCanal: achat.identifiantAchatCanal || null,
            commentaire: achat.commentaire || null,
            idCanal: (achat.canal && achat.canal.id) || null,
          }
          : null,
        cap: cap
          ? {
            cap: cap.cap || null,
            dateDebut: cap.dateDebut || null,
            dateFin: cap.dateFin || null,
            identifiantCap: cap.identifiantCap || null,
            commentaire: cap.commentaire || null,
          }
          : null,
        concept: concept
          ? {
            idFournisseurMobilier:
              (concept.fournisseurMobilier && concept.fournisseurMobilier.id) || null,
            dateInstallConcept: concept.dateInstallConcept || null,
            avecConcept: concept.avecConcept || null,
            idConcept: (concept.concept && concept.concept.id) || null,
            avecFacade: concept.avecFacade || null,
            dateInstallFacade: concept.dateInstallFacade || null,
            idEnseigniste: (concept.enseigniste && concept.enseigniste.id) || null,
            conformiteFacade: concept.conformiteFacade || null,
            idFacade: (concept.facade && concept.facade.id) || null,
            surfaceTotale: concept.surfaceTotale || null,
            surfaceVente: concept.surfaceVente || null,
            SurfaceVitrine: concept.SurfaceVitrine || null,
            volumeLeaflet: concept.volumeLeaflet || null,
            nbreLineaire: concept.nbreLineaire || null,
            nbreVitrine: concept.nbreVitrine || null,
            otcLibAcces: concept.otcLibAcces || null,
            idLeaflet: (concept.leaflet && concept.leaflet.id) || null,
            commentaire: concept.commentaire || null,
          }
          : null,
        chiffreAffaire: chiffreAffaire
          ? {
            exercice: chiffreAffaire.exercice || null,
            dateDebut: chiffreAffaire.dateDebut || null,
            dateFin: chiffreAffaire.dateFin || null,
            caTTC: chiffreAffaire.caTTC || null,
            caHt: chiffreAffaire.caHt || null,
            caTVA1: chiffreAffaire.caTVA1 || null,
            tauxTVA1: chiffreAffaire.tauxTVA1 || null,
            caTVA2: chiffreAffaire.caTVA2 || null,
            tauxTVA2: chiffreAffaire.tauxTVA2 || null,
            caTVA3: chiffreAffaire.caTVA3 || null,
            tauxTVA3: chiffreAffaire.tauxTVA3 || null,
            caTVA4: chiffreAffaire.caTVA4 || null,
            tauxTVA4: chiffreAffaire.tauxTVA4 || null,
            caTVA5: chiffreAffaire.caTVA5 || null,
            tauxTVA5: chiffreAffaire.tauxTVA5 || null,
          }
          : null,
        digitales:
          p.digitales && p.digitales.length > 0
            ? (p.digitales.map(i => {
              if (i) {
                return {
                  id: i.id || '',
                  idServicePharmacie: (i.servicePharmacie && i.servicePharmacie.id) || '',
                  idPrestataire: (i.pharmaciePestataire && i.pharmaciePestataire.id) || '',
                  flagService: i.flagService || false,
                  dateInstallation: i.dateInstallation,
                  url: i.url || '',
                };
              }
            }) as any)
            : [],
      });
    }
  }, [pharmacie]);

  if (error) return <p>Error</p>;

  const PharmacieIntrouvable = () => {
    return (
      <Box
        display="flex"
        alignItems="center"
        justifyContent="center"
        minHeight="calc(100vh - 160px)"
        fontFamily="Montserrat"
      >
        <h1>Pharmacie introuvable</h1>
      </Box>
    );
  };

  return (
    <Box display="flex" flexDirection="column" width="100%">
      {(loading || mutationLoading) && (
        <Backdrop value={mutationLoading ? 'Modification en cours...' : ''} />
      )}
      <SubToolbar
        title="Détails de pharmacie"
        dark={true}
        withBackBtn={true}
        onClickBack={onClickBack}
        backBtnIcon={<ArrowBack />}
      >
        {(!isEdit && (
          <>
            <CustomButton onClick={handleIsEdit}>Anuler </CustomButton>
            <CustomButton
              color="secondary"
              disabled={!auth.isAuthorizedToEditPharmacie() || disabledSaveBtn}
              onClick={onClickSave}
            >
              Valider
            </CustomButton>
          </>
        )) || (
            <CustomButton
              disabled={!auth.isAuthorizedToEditPharmacie()}
              onClick={handleIsEdit}
              color="secondary"
            >
              {' '}
            Modifier{' '}
            </CustomButton>
          )}
      </SubToolbar>
      {pharmacie && (
        <div className={classes.fichePharmacieMainContent}>
          <TitrePharmacie currentPharmacie={pharmacie} />
          <CustomTabs tabs={tabs} />
        </div>
      )}
      {!pharmacie && !loading && <PharmacieIntrouvable />}
    </Box>
  );
};

export default withRouter(FichePharmacie);
