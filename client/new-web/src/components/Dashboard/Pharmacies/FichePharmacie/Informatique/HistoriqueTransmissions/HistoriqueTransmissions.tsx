import React, { FC, useState } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
// import useStyles from '../styles';
import { useCommonStyles } from '../../styles';
import CustomTable, {
  CustomTableColumn,
} from '../../../../../Main/Content/Pilotage/Dashboard/Table/CustomTable';

const HistoriqueTransmissions: FC<FichePharmacieChildProps> = ({}) => {
  // const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(5);
  const [take, setTake] = useState<number>(5);
  const [skip, setSkip] = useState<number>(0);

  const users: any[] = [
    {
      date: '05/03/2020',
      heure: '07:00',
      commentaire: 'Ceci est un très long commentaire',
      statut: 'ALLIANCE-PREMIUM',
    },
    {
      date: '05/03/2020',
      heure: '07:00',
      commentaire: 'Ceci est un très long commentaire',
      statut: 'ALLIANCE-PREMIUM',
    },
  ];

  const columns: CustomTableColumn[] = [
    {
      key: 'date',
      label: 'Date',
      disablePadding: false,
      numeric: true,
    },
    {
      key: 'heure',
      label: 'Heure',
      disablePadding: false,
      numeric: true,
    },
    {
      key: 'commentaire',
      label: 'Commentaire',
      disablePadding: false,
      numeric: true,
    },
    {
      key: 'statut',
      label: 'Statut',
      disablePadding: false,
      numeric: true,
    },
  ];

  return (
    <div className={commonClasses.cardContent}>
      <CustomTable
        columns={columns}
        showToolbar={false}
        selectable={false}
        data={users}
        rowCount={users.length}
        total={users.length}
        page={page}
        rowsPerPage={rowsPerPage}
        take={take}
        skip={skip}
        setTake={setTake}
        setSkip={setSkip}
        setPage={setPage}
        setRowsPerPage={setRowsPerPage}
      />
    </div>
  );
};

export default HistoriqueTransmissions;
