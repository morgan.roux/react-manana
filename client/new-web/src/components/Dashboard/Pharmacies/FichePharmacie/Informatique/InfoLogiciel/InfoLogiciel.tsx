import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
import useStyles from '../styles';
import { useCommonStyles } from '../../styles';
import { Typography } from '@material-ui/core';
import CustomTextarea from '../../../../../Common/CustomTextarea';
import classnames from 'classnames';
import Inputs, { InputInterface } from '../../../../../Common/Inputs/Inputs';
import { LOGICIELS_WITH_MINIM_INFO } from '../../../../../../graphql/Logiciel/types/LOGICIELS_WITH_MINIM_INFO';
import { useQuery, useApolloClient } from '@apollo/client';
import { GET_LOGICIELS_WITH_MINIM_INFO } from '../../../../../../graphql/Logiciel';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import Backdrop from '../../../../../Common/Backdrop';
import { GET_AUTOMATES_WITH_MINIM_INFO } from '../../../../../../graphql/Automate';
import { AUTOMATES_WITH_MINIM_INFO } from '../../../../../../graphql/Automate/types/AUTOMATES_WITH_MINIM_INFO';
import { AutocompleteInput } from '../../../../../Common/CustomAutocomplete';

interface InputsProps {
  inputs: any[];
}

const InfoLogiciel: FC<FichePharmacieChildProps> = ({
  handleChange,
  handleChangeDate,
  values,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  isEdit,
  // pharmacie,
}) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const client = useApolloClient();

  const { informatique: i } = values;
  // const p = pharmacie;

  const idLogiciel = (i && i.idLogiciel) || '';

  const numVersion = (i && i.numVersion) || '';
  const dateLogiciel = (i && i.dateLogiciel) || null;
  const commentaire = (i && i.commentaire) || '';

  const idAutomate1 = (i && i.idAutomate1) || '';
  const idAutomate2 = (i && i.idAutomate2) || '';
  const idAutomate3 = (i && i.idAutomate3) || '';

  const nbreComptoir = (i && i.nbreComptoir) || 0;
  const nbreBackOffice = (i && i.nbreBackOffice) || 0;
  const nbreBureau = (i && i.nbreBureau) || 0;

  /**
   * Get logiciel list
   */
  const { data: logicielData, loading: logicielLoading } = useQuery<LOGICIELS_WITH_MINIM_INFO>(
    GET_LOGICIELS_WITH_MINIM_INFO,
    {
      onError: errors => {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );

  /**
   * Get automate list
   */
  const { data: automateData, loading: automateLoading } = useQuery<AUTOMATES_WITH_MINIM_INFO>(
    GET_AUTOMATES_WITH_MINIM_INFO,
    {
      onError: errors => {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );

  const logicielList = (logicielData && logicielData.logiciels) || [];
  const l = logicielList.find(l => l && l.id === idLogiciel);
  const ssii = (l && l.ssii && l.ssii.nom) || '';

  const automateList = (automateData && automateData.automates) || [];

  const inputsOne: InputInterface[] = [
    {
      name: 'informatique.idLogiciel',
      label: 'Logiciel',
      value: idLogiciel,
      type: 'autocomplete',
      selectOptions: logicielList,
      selectOptionIdKey: 'id',
      selectOptionValueKey: 'nom',
      autocompleteValue: logicielList.find(i => i && i.id === idLogiciel),
      disabled: isEdit,
    },
    {
      name: '',
      label: 'SSII',
      value: ssii,
      type: 'text',
      disabled: true,
    },
  ];

  const inputsTwo: InputInterface[] = [
    {
      name: 'informatique.numVersion',
      label: 'Version',
      value: numVersion,
      type: 'text',
      disabled: isEdit,
    },
    {
      name: 'informatique.dateLogiciel',
      label: "Date d'installation",
      value: dateLogiciel,
      type: 'date',
      disabled: isEdit,
    },
  ];

  const inputsThree: InputInterface[] = [
    {
      name: 'informatique.idAutomate1',
      label: 'Automate / Robot',
      value: idAutomate1,
      selectOptions: automateList,
      selectOptionValueKey: 'modeleAutomate',
      autocompleteValue: automateList.find(i => i && i.id === idAutomate1),
      disabled: isEdit,
    },
    {
      name: 'informatique.idAutomate2',
      label: 'Automate / Robot',
      value: idAutomate2,
      selectOptions: automateList,
      selectOptionValueKey: 'modeleAutomate',
      autocompleteValue: automateList.find(i => i && i.id === idAutomate2),
      disabled: isEdit,
    },
    {
      name: 'informatique.idAutomate3',
      label: 'Automate / Robot',
      value: idAutomate3,
      selectOptions: automateList,
      selectOptionValueKey: 'modeleAutomate',
      autocompleteValue: automateList.find(i => i && i.id === idAutomate3),
      disabled: isEdit,
    },
  ];

  const nbPostesInfos: any[] = [
    { name: 'informatique.nbreComptoir', label: 'Comptoir', value: nbreComptoir, disabled: isEdit },
    {
      name: 'informatique.nbreBackOffice',
      label: 'Backoffice',
      value: nbreBackOffice,
      disabled: isEdit,
    },
    { name: 'informatique.nbreBureau', label: 'Burreau', value: nbreBureau, disabled: isEdit },
  ];

  return (
    <div className={commonClasses.cardContent}>
      {(logicielLoading || automateLoading) && <Backdrop />}
      <div className={classnames(classes.inputsContainer)}>
        <Inputs
          inputs={inputsOne}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
        />
      </div>
      <div className={classnames(classes.inputsContainer)}>
        <Inputs
          inputs={inputsTwo}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
        />
      </div>
      <div className={commonClasses.inputsWithLeftLabelContainer}>
        {inputsThree.map((input, index) => (
          <div
            key={`fiche_pharma_info_logiciel_tInputsWithLeftLabel_${index}`}
            className={classes.inputsContainer}
          >
            <div className={commonClasses.infoContainer}>
              <Typography className={classes.employeesInfosInformatique}>{input.label}</Typography>
            </div>
            {/* <CustomSelect
              listId={input.selectOptionIdKey || 'id'}
              index={input.selectOptionValueKey || 'value'}
              list={input.selectOptions || []}
              name={input.name}
              label=""
              value={input.value}
              onChange={handleChange}
            /> */}
            <AutocompleteInput
              options={input.selectOptions || []}
              optionLabelKey={input.selectOptionValueKey || 'value'}
              value={input.autocompleteValue || ''}
              inputValue={
                input.autocompleteValue
                  ? input.autocompleteValue[input.selectOptionValueKey || 'value']
                  : input.value
              }
              name={input.name}
              onChangeAutocomplete={handleChangeAutoComplete(
                `autocompleteInput${input.name}`,
                input.name,
              )}
              onInputChange={handleChangeInputAutoComplete(
                `autocompleteInput${input.name}`,
                input.name,
              )}
              label=""
              required={false}
              disableClearable={false}
              disabled={isEdit}
            />
          </div>
        ))}
      </div>

      <div className={classes.nbPosteContainer}>
        <Typography className={commonClasses.employeesInfos}>Nombre de Postes</Typography>
        <div className={commonClasses.infoContainer}>
          {nbPostesInfos.map((item, index) => (
            <Typography key={`nbPostesInfos_${index}`} className={commonClasses.employeesInfos}>
              {item.label}
              <input
                type="number"
                name={item.name}
                value={item.value}
                onChange={handleChange}
                style={{ width: item.value === 0 ? 55 : item.value.toString().length * 14 }}
                disabled={isEdit}
              />
            </Typography>
          ))}
        </div>
      </div>
      <CustomTextarea
        label="Commentaire"
        name="informatique.commentaire"
        value={commentaire}
        onChangeTextarea={handleChange}
        rows={3}
        disabled={isEdit}
      />
    </div>
  );
};

export default InfoLogiciel;
