import React, { ReactNode, FC } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { Typography, Paper } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    fichePharmacieCardRoot: {
      borderRadius: 5,
      padding: 15,
      boxShadow: '0px 2px 12px rgba(20, 20, 20, 0.16) !important',
      '& .MuiPaper-root, & .MuiPaper-elevation1': {
        boxShadow: '0px 2px 12px rgba(20, 20, 20, 0.16) !important',
      },
    },
    title: {
      fontSize: 12,
      fontWeight: 500,
      letterSpacing: 0.4,
      color: theme.palette.common.black,
      textAlign: 'left',
      position: 'relative',
      top: '-24px',
      marginBottom: 0,
      background: 'white',
      maxWidth: 'fit-content',
    },
  }),
);

export interface FichePharmacieCardProps {
  title?: string;
  children: ReactNode;
}

const FichePharmacieCard: FC<FichePharmacieCardProps> = ({ title, children }) => {
  const classes = useStyles({});

  return (
    <Paper className={classes.fichePharmacieCardRoot}>
      {title && (
        <Typography className={classes.title} gutterBottom={true}>
          {title}
        </Typography>
      )}
      {children}
    </Paper>
  );
};

export default FichePharmacieCard;
