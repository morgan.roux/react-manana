import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    entreeSortieRoot: {
      maxWidth: 1280,
      width: '100%',
      display: 'flex',
      margin: '0 auto',
      justifyContent: 'center',
      padding: '25px 0px',
      '& > div': {
        width: '50%',
      },
      '& > div:not(:nth-last-child(1)) ': {
        marginRight: 15,
      },
      '@media (max-width: 900px)': {
        flexWrap: 'wrap',
        '& > div': {
          width: '90%',
          margin: '24px 0',
        },
        '& > div:not(:nth-last-child(1)) ': {
          marginRight: 0,
        },
      },
      '& .MuiInputBase-root.Mui-disabled': {
        background: '#F2F2F2',
      },
      '& textarea[disabled]': {
        background: '#F2F2F2',
      },
    },
  }),
);

export default useStyles;
