import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
// import useStyles from '../styles';
// import CustomSelect from '../../../../../Common/CustomSelect';
import { useCommonStyles } from '../../styles';
import { CustomCheckbox } from '../../../../../Common/CustomCheckbox';
import { CustomDatePicker } from '../../../../../Common/CustomDateTimePicker';
import CustomTextarea from '../../../../../Common/CustomTextarea';
import { InputInterface } from '../../../PharmacieForm/PharmacieForm';
import { EntreeSortieChildProps } from '../EntreeSortie';
import { AutocompleteInput } from '../../../../../Common/CustomAutocomplete';

const SortieFuture: FC<FichePharmacieChildProps & EntreeSortieChildProps> = ({
  pharmacie,
  handleChange,
  handleChangeDate,
  values,
  motifSortieList,
  concurrentList,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  isEdit,
}) => {
  // const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const { satisfaction: s, entreeSortie: es } = values;
  const p = pharmacie;

  const sortieFuture = (es && es.sortieFuture) || 0;
  const dateSortieFuture = (es && es.dateSortieFuture) || null;
  const commentaireSortieFuture = (es && es.commentaireSortieFuture) || '';
  const sortie = (es && es.sortie) || 0;

  const idMotifSortie = (es && es.idMotifSortie) || '';
  const idMotifSortieFuture = (es && es.idMotifSortieFuture) || '';
  const idConcurent = (es && es.idConcurent) || '';
  const commentaireSortie = (es && es.commentaireSortie) || '';

  const motifSortieFutureInput: InputInterface = {
    name: 'entreeSortie.idMotifSortieFuture',
    label: 'Motif de sortie future',
    value: idMotifSortieFuture,
    selectOptions: motifSortieList,
    selectOptionValueKey: 'libelle',
    autocompleteValue: motifSortieList.find(i => i && i.id === idMotifSortieFuture),
  };

  const selectInputs: InputInterface[] = [
    {
      name: 'entreeSortie.idMotifSortie',
      label: 'Motif de sortie',
      value: idMotifSortie,
      selectOptions: motifSortieList,
      selectOptionValueKey: 'libelle',
      autocompleteValue: motifSortieList.find(i => i && i.id === idMotifSortie),
    },
    {
      name: 'entreeSortie.idConcurent',
      label: 'Groupement de sortie',
      value: idConcurent,
      selectOptions: concurrentList,
      selectOptionValueKey: 'nom',
      autocompleteValue: concurrentList.find(i => i && i.id === idConcurent),
    },
  ];

  return (
    <div className={commonClasses.cardContent}>
      <div className={commonClasses.infoContainerEntreSortieTop}>
        <CustomCheckbox
          label="Sortie Future"
          name="entreeSortie.checkbox.sortieFuture"
          value={sortieFuture}
          checked={sortieFuture}
          onChange={handleChange}
          color="secondary"
          disabled={isEdit}
        />
      </div>
      <CustomDatePicker
        label="Date de sortie Future"
        placeholder="jj/mm/aaaa"
        onChange={handleChangeDate('entreeSortie.dateSortieFuture')}
        name="entreeSortie.dateSortieFuture"
        value={dateSortieFuture}
        InputLabelProps={{ shrink: true }}
        disabled={isEdit}
      />
      {/* <CustomSelect
        listId={motifSortieFutureInput.selectOptionIdKey || 'id'}
        index={motifSortieFutureInput.selectOptionValueKey || 'nom'}
        list={motifSortieFutureInput.selectOptions}
        name={motifSortieFutureInput.name}
        label={motifSortieFutureInput.label}
        value={motifSortieFutureInput.value}
        onChange={handleChange}
      /> */}
      <AutocompleteInput
        options={motifSortieFutureInput.selectOptions || []}
        optionLabelKey={motifSortieFutureInput.selectOptionValueKey || 'nom'}
        value={motifSortieFutureInput.autocompleteValue || ''}
        inputValue={
          motifSortieFutureInput.autocompleteValue
            ? motifSortieFutureInput.autocompleteValue[
                motifSortieFutureInput.selectOptionValueKey || 'nom'
              ]
            : motifSortieFutureInput.value
        }
        name={motifSortieFutureInput.name}
        id={`AutocompleteInput_aze`}
        onChangeAutocomplete={handleChangeAutoComplete(
          `autocompleteInput${motifSortieFutureInput.name}`,
          motifSortieFutureInput.name,
        )}
        onInputChange={handleChangeInputAutoComplete(
          `autocompleteInput${motifSortieFutureInput.name}`,
          motifSortieFutureInput.name,
        )}
        label={motifSortieFutureInput.label}
        required={false}
        disableClearable={false}
        disabled={isEdit}
      />
      <CustomTextarea
        label="Commentaire de sortie future"
        name="entreeSortie.commentaireSortieFuture"
        value={commentaireSortieFuture}
        onChangeTextarea={handleChange}
        rows={3}
        disabled={isEdit}
      />
      <div className={commonClasses.infoContainerEntreSortie}>
        <CustomCheckbox
          label="Sortie"
          name="entreeSortie.checkbox.sortie"
          value={sortie}
          checked={sortie}
          onChange={handleChange}
          color="secondary"
        />
      </div>
      {selectInputs.map((input, index) => (
        // <CustomSelect
        //   key={`fiche_pharma_sortie_future_select_inputs_${index}`}
        //   listId={input.selectOptionIdKey || 'id'}
        //   index={input.selectOptionValueKey || 'libelle'}
        //   list={input.selectOptions}
        //   name={input.name}
        //   label={input.label}
        //   value={input.value}
        //   onChange={handleChange}
        // />
        <AutocompleteInput
          key={`fiche_pharma_sortie_future_select_inputs_${index}`}
          options={input.selectOptions || []}
          optionLabelKey={input.selectOptionValueKey || 'libelle'}
          value={input.autocompleteValue || ''}
          inputValue={
            input.autocompleteValue
              ? input.autocompleteValue[input.selectOptionValueKey || 'libelle']
              : input.value
          }
          name={input.name}
          onChangeAutocomplete={handleChangeAutoComplete(
            `autocompleteInput${input.name}`,
            input.name,
          )}
          onInputChange={handleChangeInputAutoComplete(
            `autocompleteInput${input.name}`,
            input.name,
          )}
          label={input.label}
          required={false}
          disableClearable={false}
          disabled={isEdit}
        />
      ))}
      <CustomTextarea
        label="Commentaire de sortie"
        name="entreeSortie.commentaireSortie"
        value={commentaireSortie}
        onChangeTextarea={handleChange}
        rows={3}
        disabled={isEdit}
      />
    </div>
  );
};

export default SortieFuture;
