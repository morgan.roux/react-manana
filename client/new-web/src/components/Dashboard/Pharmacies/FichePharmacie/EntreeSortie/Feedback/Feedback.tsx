import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
// import useStyles from '../styles';
import { useCommonStyles } from '../../styles';
import { Typography } from '@material-ui/core';
import CustomTextarea from '../../../../../Common/CustomTextarea';
import { CustomDatePicker } from '../../../../../Common/CustomDateTimePicker';
import { AWS_HOST } from '../../../../../../utils/s3urls';
import { noValueText } from '../../../../../../Constant/text';
import { EntreeSortieChildProps } from '../EntreeSortie';
import Inputs from '../../../../../Common/Inputs';
import { InputInterface } from '../../../../../Common/Inputs/Inputs';
import { isCreditCard } from 'validator';
import { isDefinitionNode } from 'graphql';

const Feedback: FC<FichePharmacieChildProps & EntreeSortieChildProps> = ({
  pharmacie,
  handleChange,
  handleChangeDate,
  values,
  concurrentList,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  isEdit,
}) => {
  // const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const { satisfaction: s, entreeSortie: es } = values;
  const p = pharmacie;

  const smyley = p && p.satisfaction && p.satisfaction.smyley && p.satisfaction.smyley.photo;
  const dateSaisie = (s && s.dateSaisie) || null;
  const satisfactionCommentaire = (s && s.commentaire) || '';
  const commentaireEntree = (es && es.commentaireEntree) || '';
  const motifEntree =
    (p &&
      p.entreeSortie &&
      p.entreeSortie.pharmacieMotifEntree &&
      p.entreeSortie.pharmacieMotifEntree.libelle) ||
    noValueText;

  // const idMotifSortieFuture = (es && es.idMotifSortieFuture) || '';
  const idConcurentAncien = (es && es.idConcurentAncien) || '';

  const dateEntree = (es && es.dateEntree) || null;
  const dateSignature = (es && es.dateSignature) || null;

  const selectInputs: InputInterface[] = [
    // {
    //   name: 'entreeSortie.idMotifSortieFuture',
    //   label: 'Motif de sortie future',
    //   value: idMotifSortieFuture,
    //   selectOptions: [],
    //   selectOptionValueKey: 'nom',
    // },
    {
      name: 'entreeSortie.idConcurentAncien',
      label: 'Ancien groupement',
      value: idConcurentAncien,
      selectOptions: concurrentList,
      selectOptionValueKey: 'nom',
      type: 'autocomplete',
      autocompleteValue: concurrentList.find(i => i && i.id === idConcurentAncien),
      disabled: isEdit,
    },
    {
      name: 'entreeSortie.commentaireEntree',
      label: 'Commentaire de sortie',
      value: commentaireEntree,
      type: 'textArea',
      textAreaRow: 3,
      disabled: isEdit,
    },
  ];

  const dateInputs = [
    {
      name: 'entreeSortie.dateEntree',
      label: "Date d'entrée",
      value: dateEntree,
      disabled: isEdit,
    },
    {
      name: 'entreeSortie.dateSignature',
      label: 'Date de signature',
      value: dateSignature,
      disabled: isEdit,
    },
  ];

  return (
    <div className={commonClasses.cardContent}>
      <div className={commonClasses.infoContainer}>
        <Typography className={commonClasses.satisfactionTitle}>
          Satisfaction
          <span style={{ marginLeft: 20 }}>
            {smyley ? (
              <img src={`${AWS_HOST}/${smyley}`} style={{ width: 20, height: 20 }} />
            ) : (
              <>Aucun</>
            )}
          </span>
        </Typography>
      </div>
      <CustomDatePicker
        label="Date de saisie"
        placeholder="jj/mm/aaaa"
        onChange={handleChangeDate('satisfaction.dateSaisie')}
        name="satisfaction.dateSaisie"
        value={dateSaisie}
        InputLabelProps={{ shrink: true }}
        disabled={isEdit}
      />
      <CustomTextarea
        label="Commentaire associé au niveau de satisfaction"
        name="satisfaction.commentaire"
        value={satisfactionCommentaire}
        onChangeTextarea={handleChange}
        rows={3}
        disabled={isEdit}
      />
      <div className={commonClasses.infoContainerEntreSortie}>
        <Typography className={commonClasses.infoTitle}>Motif d'entrée :</Typography>
        <Typography className={commonClasses.infoValue}> {motifEntree}</Typography>
      </div>
      <div className={commonClasses.twoInputContainer}>
        {dateInputs.map((input, index) => (
          <CustomDatePicker
            key={`fiche_pharma_feedback_date_inputs_${index}`}
            label={input.label}
            placeholder="jj/mm/aaaa"
            onChange={handleChangeDate(input.name)}
            name={input.name}
            value={input.value}
            InputLabelProps={{ shrink: true }}
            disabled={isEdit}
          />
        ))}
      </div>
      {/* {selectInputs.map((input, index) => (
        <CustomSelect
          key={`fiche_pharma_feedback_select_inputs_${index}`}
          listId={input.selectOptionIdKey || 'id'}
          index={input.selectOptionValueKey || 'nom'}
          list={input.selectOptions}
          name={input.name}
          label={input.label}
          value={input.value}
          onChange={handleChange}
        />
      ))} */}
      <Inputs
        inputs={selectInputs}
        handleChange={handleChange}
        handleChangeDate={handleChangeDate}
        handleChangeAutoComplete={handleChangeAutoComplete}
        handleChangeInputAutoComplete={handleChangeInputAutoComplete}
      />
    </div>
  );
};

export default Feedback;
