import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
import useStyles from '../styles';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import { useCommonStyles } from '../../styles';
import { CustomCheckbox } from '../../../../../Common/CustomCheckbox';
import { CustomDatePicker } from '../../../../../Common/CustomDateTimePicker';
import { InputInterface } from '../../../../../Common/Inputs/Inputs';

const InfoJuridique: FC<FichePharmacieChildProps> = ({
  handleChange,
  handleChangeDate,
  values,
  isEdit,
  // pharmacie,
  // handleChangeAutoComplete,
  // handleChangeInputAutoComplete,
}) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const { compta: c } = values;

  const structureJuridique = (c && c.structureJuridique) || '';
  const modifStatut = (c && c.modifStatut) || false;
  const dateModifStatut = (c && c.dateModifStatut) || null;
  const denominationSociale = (c && c.denominationSociale) || '';
  const raisonModif = (c && c.raisonModif) || '';

  const othersInputs: InputInterface[] = [
    {
      label: 'Dénomination sociale',
      name: 'compta.denominationSociale',
      value: denominationSociale,
      type: 'text',
      disabled: isEdit,
    },
    {
      label: 'Raison de la modification',
      name: 'compta.raisonModif',
      value: raisonModif,
      type: 'text',
      disabled: isEdit,
    },
  ];

  return (
    <div className={commonClasses.cardContent}>
      <div className={classes.inputsContainerInfoJuridique}>
        <CustomFormTextField
          label="Structure juridique"
          name="compta.structureJuridique"
          value={structureJuridique}
          onChange={handleChange}
          style={{ maxWidth: 443 }}
        />
        <div className={classes.modifStatusContainer}>
          <CustomCheckbox
            label="Modif. Statut"
            name="compta.checkbox.modifStatut"
            value={modifStatut}
            checked={modifStatut}
            onChange={handleChange}
            color="secondary"
            disabled={isEdit}
          />
        </div>
        <CustomDatePicker
          label="Date"
          placeholder="jj/mm/aaaa"
          onChange={handleChangeDate('compta.dateModifStatut')}
          name="compta.dateModifStatut"
          value={dateModifStatut}
          InputLabelProps={{ shrink: true }}
          style={{ maxWidth: 282 }}
          disabled={isEdit}
        />
      </div>
      {othersInputs.map((input, index) => (
        <CustomFormTextField
          key={`fiche_pharma_info_juridiq_${index}`}
          label={input.label}
          name={input.name}
          value={input.value}
          onChange={handleChange}
          disabled={isEdit}
        />
      ))}
    </div>
  );
};

export default InfoJuridique;
