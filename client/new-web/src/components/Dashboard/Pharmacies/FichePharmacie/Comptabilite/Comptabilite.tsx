import React, { FC, Dispatch, SetStateAction } from 'react';
import { FichePharmacieChildProps } from '../FichePharmacie';
import InfoBancaire from './InfoBancaire';
import InfoJuridique from './InfoJuridique';
import useStyles from './styles';
import FichePharmacieCard from '../FichePharmacieCard';
import MainInfo from './MainInfo';
import { PharmacieFormInterface } from '../../PharmacieForm/usePharmacieForm';

export interface ComptabiliteProps {
  setValues: Dispatch<SetStateAction<PharmacieFormInterface>>;
}

const Comptabilite: FC<FichePharmacieChildProps & ComptabiliteProps> = ({
  pharmacie,
  handleChange,
  handleChangeDate,
  values,
  setValues,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  isEdit,
}) => {
  const classes = useStyles({});

  const mainInfoCard = {
    title: '',
    children: (
      <MainInfo
        pharmacie={pharmacie}
        handleChange={handleChange}
        handleChangeDate={handleChangeDate}
        values={values}
        handleChangeAutoComplete={handleChangeAutoComplete}
        handleChangeInputAutoComplete={handleChangeInputAutoComplete}
        isEdit={isEdit}
      />
    ),
  };

  const cards = [
    {
      title: 'Information bancaire',
      children: (
        <InfoBancaire
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          setValues={setValues}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      title: 'Information juridique',
      children: (
        <InfoJuridique
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
  ];

  return (
    <div className={classes.comptabiliteRoot}>
      <FichePharmacieCard title={mainInfoCard.title}>{mainInfoCard.children}</FichePharmacieCard>
      <div className={classes.cardsContainers}>
        {cards.map((card, index) => (
          <FichePharmacieCard key={`fiche_pharma_entree_sortie_1_${index}`} title={card.title}>
            {card.children}
          </FichePharmacieCard>
        ))}
      </div>
    </div>
  );
};

export default Comptabilite;
