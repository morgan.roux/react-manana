import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const usesStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      marginBottom: theme.spacing(2),
      marginTop: theme.spacing(2),
      '& *': {
        fontFamily: 'Montserrat',
        letterSpacing: 0,
      },
    },
    conteneur: {
      display: 'flex',
    },
    title: {
      fontSize: 24,
      fontWeight: 700,
    },
    titulaires: {
      letterSpacing: 0.24,
      fontSize: 14,
      fontWeight: 500,
      '@media (max-width: 599px)': {
        fontSize: '0.75rem',
        flexWrap: 'wrap',
      },
    },
  }),
);

export default usesStyles;
