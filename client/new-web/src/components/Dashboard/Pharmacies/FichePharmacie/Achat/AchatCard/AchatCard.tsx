import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
import useStyles from '../styles';
import { useCommonStyles } from '../../styles';
import CustomTextarea from '../../../../../Common/CustomTextarea';
import Inputs from '../../../../../Common/Inputs';

interface AchatCardProps {
  inputs: any[];
}

const AchatCard: FC<FichePharmacieChildProps & AchatCardProps> = ({
  inputs,
  handleChange,
  handleChangeDate,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  isEdit,
  // pharmacie,
  // values,
}) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const commentInput = inputs.find(i => i.type === 'textArea');
  const filterInputs = inputs.filter(i => i.type !== 'textArea');

  return (
    <div className={commonClasses.cardContent}>
      <div className={classes.inputsContainer}>
        <Inputs
          inputs={filterInputs}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
        />
      </div>
      <div className={classes.inputsContainer}>
        {commentInput && (
          <CustomTextarea
            label={commentInput.label}
            name={commentInput.name}
            value={commentInput.value}
            onChangeTextarea={handleChange}
            rows={3}
          />
        )}
      </div>
    </div>
  );
};

export default AchatCard;
