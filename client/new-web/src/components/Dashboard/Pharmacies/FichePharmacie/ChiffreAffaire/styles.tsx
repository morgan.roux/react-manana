import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    chiffreAffaireRoot: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
      padding: '25px 0px',
      '& > div': {
        maxWidth: 613,
        width: '100%',
        padding: '33px 24px',
      },

      '& .MuiInputBase-root.Mui-disabled': {
        background: '#F2F2F2',
      },
      '& textarea[disabled]': {
        background: '#F2F2F2',
      },
    },
    inputsContainer: {
      display: 'flex',
      flexDirection: 'column',
      // justifyContent: 'space-between',
      '& > div': {
        marginBottom: 15,
      },
    },
    formRowContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',

      '& > div': {
        marginBottom: 0,
      },
      '& > div:nth-child(1)': {
        '& > p': {
          marginBottom: '0!important',
          marginRight: '0!important',
          width: 120,
        },
      },
      '@media (max-width: 442px)': {
        flexWrap: 'wrap',
      },
    },
    margeVentillation: {
      marginTop: 20,
      marginBottom: 10,
      fontSize: '0.875rem',
      fontWeight: 500,
    },
  }),
);

export default useStyles;
