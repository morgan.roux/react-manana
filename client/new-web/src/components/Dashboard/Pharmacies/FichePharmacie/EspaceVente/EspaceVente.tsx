import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../FichePharmacie';
import Facade from './Facade';
import Concept from './Concept';
import useStyles from './styles';
import FichePharmacieCard from '../FichePharmacieCard';
import Pharmacie from './Pharmacie';
import classnames from 'classnames';

const EspaceVente: FC<FichePharmacieChildProps> = ({
  pharmacie,
  handleChange,
  handleChangeDate,
  values,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  isEdit,
}) => {
  const classes = useStyles({});

  const cards = [
    {
      title: 'Concept',
      children: (
        <Concept
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
    {
      title: 'Facade',
      children: (
        <Facade
          pharmacie={pharmacie}
          handleChange={handleChange}
          handleChangeDate={handleChangeDate}
          values={values}
          handleChangeAutoComplete={handleChangeAutoComplete}
          handleChangeInputAutoComplete={handleChangeInputAutoComplete}
          isEdit={isEdit}
        />
      ),
    },
  ];

  return (
    <div className={classes.espaceVenteRoot}>
      <div className={classnames(classes.cardContainer, classes.conceptAndFacadeContainer)}>
        {cards.map((card, index) => (
          <FichePharmacieCard key={`fiche_pharma_espace_vente_card_${index}`} title={card.title}>
            {card.children}
          </FichePharmacieCard>
        ))}
      </div>
      <div className={classnames(classes.cardContainer, classes.pharmaContainer)}>
        <FichePharmacieCard title="Pharmacie">
          <Pharmacie
            pharmacie={pharmacie}
            handleChange={handleChange}
            handleChangeDate={handleChangeDate}
            values={values}
            handleChangeAutoComplete={handleChangeAutoComplete}
            handleChangeInputAutoComplete={handleChangeInputAutoComplete}
            isEdit={isEdit}
          />
        </FichePharmacieCard>
      </div>
    </div>
  );
};

export default EspaceVente;
