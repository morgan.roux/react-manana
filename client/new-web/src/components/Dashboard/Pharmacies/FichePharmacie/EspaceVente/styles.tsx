import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    espaceVenteRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      maxWidth: 1200,
      margin: '0 auto',
      padding: '25px 0px',
      '& > div:nth-child(1)': {
        marginBottom: 25,
      },
      '& .MuiInputBase-root.Mui-disabled': {
        background: '#F2F2F2',
      },
      '& textarea[disabled]': {
        background: '#F2F2F2',
      },
    },

    cardContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      width: '100%',
    },
    conceptAndFacadeContainer: {
      '& > div': {
        width: 300,
        '@media (max-width: 768px)': {
          width: '48%',
        },
        '@media (max-width: 599px)': {
          width: '98%',
          marginBottom: 24,
        },
      },
      '& > div:nth-last-child(1)': {
        marginLeft: 150,
        '@media (max-width: 768px)': {
          marginLeft: 0,
        },
      },
      '@media (max-width: 768px)': {
        justifyContent: 'space-between',
      },
      '@media (max-width: 599px)': {
        justifyContent: 'center',
        flexDirection: 'column',
      },
    },
    pharmaContainer: {
      '& > div': {
        width: '100%',
      },
    },
    inputsContainer: {
      display: 'flex',
      flexDirection: 'column',
      '& > div': {
        marginBottom: 23,
      },
    },
  }),
);

export default useStyles;
