import React, { FC } from 'react';
import { FichePharmacieChildProps } from '../../FichePharmacie';
// import useStyles from '../styles';
import { useCommonStyles } from '../../styles';
import Inputs from '../../../../../Common/Inputs';
import { InputInterface } from '../../../../../Common/Inputs/Inputs';
import { isCreditCard } from 'validator';
import classnames from 'classnames';

const Concept: FC<FichePharmacieChildProps> = ({
  // pharmacie,
  handleChange,
  handleChangeDate,
  values,
  handleChangeAutoComplete,
  handleChangeInputAutoComplete,
  isEdit,
}) => {
  // const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const { concept: c } = values;

  const avecConcept = (c && c.avecConcept) || false;
  const idFournisseurMobilier = (c && c.idFournisseurMobilier) || '';
  const dateInstallConcept = (c && c.dateInstallConcept) || null;

  // TODO
  const typeConcept = '';
  const modeleConcept = '';

  // TODO :  List Fournisseur mobilier

  const inputs: InputInterface[] = [
    {
      name: 'concept.checkbox.avecConcept',
      label: 'Concept',
      value: avecConcept,
      type: 'checkbox',
      disabled: isEdit,
    },
    {
      name: 'concept.idFournisseurMobilier',
      label: 'Fournisseur mobilier',
      value: idFournisseurMobilier,
      type: 'select',
      selectOptions: [],
      disabled: isEdit,
    },
    {
      name: 'concept.dateInstallConcept',
      label: 'Date installation',
      value: dateInstallConcept,
      type: 'date',
      disabled: isEdit,
    },
    {
      name: 'concept.typeConcept',
      label: 'Type concept',
      value: typeConcept,
      type: 'select',
      selectOptions: [],
      disabled: isEdit,
    },
    {
      name: 'concept.modeleConcept',
      label: 'Modèle concept',
      value: modeleConcept,
      type: 'text',
      disabled: isEdit,
    },
  ];

  return (
    <div className={classnames(commonClasses.cardContent, commonClasses.contentInputTextfield)}>
      <Inputs
        inputs={inputs}
        handleChange={handleChange}
        handleChangeDate={handleChangeDate}
        handleChangeAutoComplete={handleChangeAutoComplete}
        handleChangeInputAutoComplete={handleChangeInputAutoComplete}
      />
    </div>
  );
};

export default Concept;
