import { useState, ChangeEvent } from 'react';
import {
  ContactInput,
  PharmacieEntreeSortieInput,
  PharmacieSatisfactionInput,
  PharmacieSegmentationInput,
  PharmacieComptaInput,
  PharmacieInformatiqueInput,
  PharmacieAchatInput,
  PharmacieCapInput,
  PharmacieConceptInput,
  PharmacieStatCAInput,
  PharmacieDigitaleInput,
} from '../../../../types/graphql-global-types';
import { last, split, head, isEqual } from 'lodash';
import { number } from 'prop-types';

export interface PharmacieFormInterface {
  id: string | null;
  cip: string;
  nom: string;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  longitude: string | null;
  latitude: string | null;
  numFiness: string | null;
  sortie: number | null;
  sortieFuture: number | null;
  nbEmploye: number | null;
  nbAssistant: number | null;
  nbAutreTravailleur: number | null;
  uga: string | null;
  commentaire: string | null;
  idGrossistes: string[] | null;
  idGeneriqueurs: string[] | null;
  contact: ContactInput | null;
  entreeSortie: PharmacieEntreeSortieInput | null;
  satisfaction: PharmacieSatisfactionInput | null;
  segmentation: PharmacieSegmentationInput | null;
  compta: PharmacieComptaInput | null;
  informatique: PharmacieInformatiqueInput | null;
  achat: PharmacieAchatInput | null;
  cap: PharmacieCapInput | null;
  concept: PharmacieConceptInput | null;
  chiffreAffaire: PharmacieStatCAInput | null;
  digitales: PharmacieDigitaleInput[] | null;
}

export const initialState: PharmacieFormInterface = {
  id: null,
  cip: '',
  nom: '',
  adresse1: null,
  adresse2: null,
  cp: null,
  ville: null,
  pays: null,
  longitude: null,
  latitude: null,
  numFiness: null,
  sortie: null,
  sortieFuture: null,
  nbEmploye: null,
  nbAssistant: null,
  nbAutreTravailleur: null,
  uga: null,
  commentaire: null,
  idGrossistes: null,
  idGeneriqueurs: null,
  contact: null,
  entreeSortie: null,
  satisfaction: null,
  segmentation: null,
  compta: null,
  informatique: null,
  achat: null,
  cap: null,
  concept: null,
  chiffreAffaire: null,
  digitales: null,
};

const usePharmacieFormForm = (defaultState?: PharmacieFormInterface) => {
  const initValues: PharmacieFormInterface = defaultState || initialState;
  const [values, setValues] = useState<PharmacieFormInterface>(initValues);

  const otherHandleChange = (name: string, value: any, checked: boolean) => {
    const nameArray = split(name, '.');
    const nameKey: any = head(nameArray);
    const exactName: any = last(nameArray);
    if (name.includes('checkbox')) {
      if (nameKey === exactName && exactName !== 'cap') {
        setValues(prevState => ({
          ...prevState,
          [exactName]: checked,
        }));
      } else {
        setValues(prevState => ({
          ...prevState,
          [nameKey]: { ...prevState[nameKey], [exactName]: checked },
        }));
      }
    } else {
      setValues(prevState => ({
        ...prevState,
        [nameKey]: { ...prevState[nameKey], [exactName]: value },
      }));
    }
  };

  const handleChangeArray = (name: string, value: any) => {
    const exactName = head(split(name, '_'));
    const position = last(split(name, '_'));

    if (position && exactName) {
      setValues(prevState => {
        const old: any[] = prevState[exactName];
        if (old) {
          // Spice old
          old.splice(Number(position), 1, value);
          const nextState = {
            ...prevState,
            [exactName]: old.filter(i => i !== '' && i !== undefined && i !== null),
          };
          return nextState;
        } else {
          const nextState = {
            ...prevState,
            [exactName]: [value].filter(i => i !== '' && i !== undefined && i !== null),
          };
          return nextState;
        }
      });
      return;
    }
  };

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value, checked, type } = e.target;
      const isNumber = type === 'number';
      const elementValue = isNumber && parseInt(value) < 0 ? '0' : value;
      if (name.includes('.')) {
        otherHandleChange(name, elementValue, checked);
        return;
      }

      // Set idGrossistes, idGeneriqueurs, ...etc
      if (name.includes('_')) {
        handleChangeArray(name, elementValue);
        return;
      }

      setValues(prevState => ({ ...prevState, [name]: elementValue }));
    }
  };

  const handleChangeDate = (name: string) => (date: any) => {
    if (name.includes('.')) {
      otherHandleChange(name, date, false);
      return;
    }
    setValues(prevState => ({ ...prevState, [name]: date }));
  };

  const handleChangeAutoComplete = (fakeName: string, name: string) => (
    e: ChangeEvent<any>,
    newValue: any,
  ) => {
    if (e && e.target) {
      setValues(prevState => ({ ...prevState, [fakeName]: newValue }));

      if (newValue && newValue.id) {
        if (name.includes('.')) {
          otherHandleChange(name, newValue.id, false);
          return;
        }

        // Set idGrossistes, idGeneriqueurs, ...etc
        if (name.includes('_')) {
          handleChangeArray(name, newValue.id);
          return;
        }

        setValues(prevState => ({ ...prevState, [name]: newValue.id }));
      }
    }
  };

  const handleChangeInputAutoComplete = (fakeName: string, name: string) => (
    e: ChangeEvent<any>,
    newInputValue: string,
  ) => {
    if (e && e.target) {
      // setValues(prevState => ({ ...prevState, [fakeName]: newValue }));

      if (name.includes('.')) {
        otherHandleChange(name, newInputValue, false);
        return;
      }

      // Set idGrossistes, idGeneriqueurs, ...etc
      if (name.includes('_')) {
        handleChangeArray(name, newInputValue);
        return;
      }

      setValues(prevState => ({ ...prevState, [name]: newInputValue }));
    }
  };

  return {
    handleChange,
    handleChangeDate,
    handleChangeAutoComplete,
    handleChangeInputAutoComplete,
    values,
    setValues,
  };
};

export default usePharmacieFormForm;
