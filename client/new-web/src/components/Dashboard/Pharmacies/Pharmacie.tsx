import { useApolloClient, useLazyQuery } from '@apollo/client';
import { Add } from '@material-ui/icons';
import React, { useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import mutationSuccessImg from '../../../assets/img/mutation-success.png';
import { PHARMACIE_URL } from '../../../Constant/url';
import { GET_PHARMACIE } from '../../../graphql/Pharmacie';
import {
  PHARMACIE,
  PHARMACIEVariables,
  PHARMACIE_pharmacie,
} from '../../../graphql/Pharmacie/types/PHARMACIE';
import { useValueParameterAsBoolean } from '../../../utils/getValueParameter';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import Backdrop from '../../Common/Backdrop';
import { ConfirmDeleteDialog } from '../../Common/ConfirmDialog';
import CustomButton from '../../Common/CustomButton';
import { FieldsOptions } from '../../Common/CustomContent/interfaces';
import {
  PHARMA_FILTER_ADRESSE_CODE,
  PHARMA_FILTER_CIP_CODE,
  PHARMA_FILTER_CODE_ENSEIGNE_CODE,
  PHARMA_FILTER_CODE_IVRY_LAB_CODE,
  PHARMA_FILTER_CODE_POSTAL_CODE,
  PHARMA_FILTER_COMMERCIALE_CODE,
  PHARMA_FILTER_CONTRAT_CODE,
  PHARMA_FILTER_DEPARTEMENT_CODE,
  PHARMA_FILTER_PHARMACIE_NAME_CODE,
  PHARMA_FILTER_PRESIDENT_REGION_CODE,
  PHARMA_FILTER_REGION_CODE,
  PHARMA_FILTER_SIRET_CODE,
  PHARMA_FILTER_SORTIE_CODE,
  PHARMA_FILTER_STATUS_CODE,
  PHARMA_FILTER_TELEPHONE_CODE,
  PHARMA_FILTER_TITULAIRE_CODE,
  PHARMA_FILTER_TRANCHE_CA_CODE,
  PHARMA_FILTER_VILLE_CODE,
  SEARCH_FIELDS_OPTIONS,
} from '../../Common/CustomPharmacie/constants';
import CustomContent from '../../Common/newCustomContent';
import SearchInput from '../../Common/newCustomContent/SearchInput';
import SubToolbar from '../../Common/newCustomContent/SubToolbar';
import NoItemContentImage from '../../Common/NoItemContentImage';
import PharmacieForm from './PharmacieForm/PharmacieForm';
import usePharmacieFormForm from './PharmacieForm/usePharmacieForm';
import useStyles from './styles';
import {
  useCheckedPharmacie,
  useCreateUpdatePharmacie,
  useGetPharmacie,
  usePharmacieColumns,
} from './utils';
import { useButtonHeadAction } from './utils/utils';

interface IPharmacieProps {
  listResult: any;
}

const Pharmacie: React.FC<IPharmacieProps & RouteComponentProps> = ({
  listResult,
  history,
  location: { pathname },
  match: { params },
}) => {
  const classes = useStyles({});
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const { push } = history;

  const {
    columns,
    deleteRow,
    onClickConfirmDelete,
    deleteLoading,
    selected,
    setSelected,
  } = usePharmacieColumns(setOpenDeleteDialog, history, undefined, null);
  const { checkedsPharmacie } = useCheckedPharmacie();
  const { handleChange, handleChangeDate, values, setValues } = usePharmacieFormForm();

  const { pharmaId } = params as any;

  // const groupement = getGroupement();
  // const idGroupement = groupement && groupement.id;

  const objectState = {
    nomState: useValueParameterAsBoolean(PHARMA_FILTER_PHARMACIE_NAME_CODE),
    titulairesState: useValueParameterAsBoolean(PHARMA_FILTER_TITULAIRE_CODE),
    sortieState: useValueParameterAsBoolean(PHARMA_FILTER_SORTIE_CODE),
    codeEnseigneState: useValueParameterAsBoolean(PHARMA_FILTER_CODE_ENSEIGNE_CODE),
    departementState: useValueParameterAsBoolean(PHARMA_FILTER_DEPARTEMENT_CODE),
    activedState: useValueParameterAsBoolean(PHARMA_FILTER_STATUS_CODE),
    numIvrylabState: useValueParameterAsBoolean(PHARMA_FILTER_CODE_IVRY_LAB_CODE),
    tele1State: useValueParameterAsBoolean(PHARMA_FILTER_TELEPHONE_CODE),
    cipState: useValueParameterAsBoolean(PHARMA_FILTER_CIP_CODE),
    villeState: useValueParameterAsBoolean(PHARMA_FILTER_VILLE_CODE),
    cpState: useValueParameterAsBoolean(PHARMA_FILTER_CODE_POSTAL_CODE),
    adresse1State: useValueParameterAsBoolean(PHARMA_FILTER_ADRESSE_CODE),
    presidentRegionState: useValueParameterAsBoolean(PHARMA_FILTER_PRESIDENT_REGION_CODE),
    contratState: useValueParameterAsBoolean(PHARMA_FILTER_CONTRAT_CODE),
    siretState: useValueParameterAsBoolean(PHARMA_FILTER_SIRET_CODE),
    regionState: useValueParameterAsBoolean(PHARMA_FILTER_REGION_CODE),
    trancheCAState: useValueParameterAsBoolean(PHARMA_FILTER_TRANCHE_CA_CODE),
    commercialeState: useValueParameterAsBoolean(PHARMA_FILTER_COMMERCIALE_CODE),
  };

  const filterSearchFields = (objectState: any, searchFields: FieldsOptions[]): FieldsOptions[] => {
    const activeStateKeys = Object.keys(objectState).filter(key => objectState[key] === true);
    const activeSearchFields = searchFields.filter((field: FieldsOptions) =>
      activeStateKeys.includes(`${field.name}State`),
    );
    return activeSearchFields;
  };

  const activeSearcFields: FieldsOptions[] = filterSearchFields(objectState, SEARCH_FIELDS_OPTIONS);

  const { createUpdatePharmacie, mutationLoading, mutationSuccess } = useCreateUpdatePharmacie(
    values,
  );
  const { goToAddPharmacie, goBack } = useButtonHeadAction(push);

  const client = useApolloClient();

  const [getPharmacie, { data, loading }] = useLazyQuery<PHARMACIE, PHARMACIEVariables>(
    GET_PHARMACIE,
    {
      onError: errors => {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, {
            type: 'ERROR',
            message: err.message,
            isOpen: true,
          });
        });
      },
      fetchPolicy: 'network-only',
    },
  );
  const pharmacie: PHARMACIE_pharmacie | null = (data && data.pharmacie) || null;
  const [fieldsState, setFieldsState] = useState<any>({});

  const isOnList = pathname === `/db/${PHARMACIE_URL}`;
  const isOnCreate = pathname === `/db/${PHARMACIE_URL}/create`;
  const isOnEdit: boolean = pathname.startsWith(`/db/${PHARMACIE_URL}/edit`);

  const subToolbarTitle = isOnList
    ? 'Liste des pharmacies'
    : `${isOnCreate ? 'Ajout' : 'Modification'} d'une pharmacie`;

  const mutationSuccessTitle = `${isOnCreate ? 'Ajout' : 'Modification'} de la pharmacie réussi`;
  const mutationSuccessSubTitle = `La nouvelle pharmacie que vous venez ${isOnCreate ? "d'ajouter" : 'de modifier'
    } est désormais dans la liste des pharmacies du groupement.`;

  const mutationLoadingMsg = `${isOnCreate ? 'Création' : isOnEdit ? 'Modification' : 'Suppression'
    } en cours...`;

  const EDIT_URL = `/db/${PHARMACIE_URL}/edit/${pharmaId}`;

  const onClickDefaultBtn = () => {
    if (!isOnList) {
      goBack();
      return;
    }
    setOpenDeleteDialog(true);
  };

  const onClickSecondaryBtn = () => {
    if (!isOnList) {
      createUpdatePharmacie();
      return;
    }
    goToAddPharmacie();
  };

  const SubToolbarChildren = () => {
    return (
      <div className={classes.childrenRoot}>
        {((checkedsPharmacie && checkedsPharmacie.length > 0) ||
          selected.length > 0 ||
          !isOnList) && (
            <CustomButton color="default" onClick={onClickDefaultBtn}>
              {!isOnList ? 'Annuler' : 'Supprimer la sélection'}
            </CustomButton>
          )}
        <CustomButton
          color="secondary"
          startIcon={!isOnList ? null : <Add />}
          onClick={onClickSecondaryBtn}
        >
          {isOnCreate ? 'Ajouter' : isOnEdit ? 'modifier' : 'Ajouter une pharmacie'}
        </CustomButton>
      </div>
    );
  };

  const handleFieldChange = (event: any): void => {
    setFieldsState({
      ...fieldsState,
      [event.target.name]:
        event.target.name === 'actived' || event.target.name === 'pharmacies.actived'
          ? event.target.value !== -1
            ? event.target.value === 1
              ? true
              : false
            : undefined
          : event.target.value,
    });
  };

  const listPharmacie = (
    <CustomContent
      selected={selected}
      setSelected={setSelected}
      // checkedItemsQuery={{
      //   name: 'checkedsPharmacie',
      //   type: 'pharmacie',
      //   query: GET_CHECKEDS_PHARMACIE,
      // }}
      {...{ listResult, columns }}
      isSelectable={true}
    />
  );

  const ActionSuccess = () => {
    return (
      <div className={classes.mutationSuccessContainer}>
        <NoItemContentImage
          src={mutationSuccessImg}
          title={mutationSuccessTitle}
          subtitle={mutationSuccessSubTitle}
        >
          <CustomButton color="default" onClick={goBack}>
            Retour à la liste
          </CustomButton>
        </NoItemContentImage>
      </div>
    );
  };

  const DeleteDialogContent = () => {
    if (deleteRow) {
      return (
        <span>
          Êtes-vous sur de vouloir supprimer
          <span style={{ fontWeight: 'bold' }}> {deleteRow.nom || ''} </span>?
        </span>
      );
    }
    return <span>Êtes-vous sur de vouloir supprimer ces pharmacies ?</span>;
  };

  useEffect(() => {
    if (pathname === EDIT_URL && pharmaId) {
      getPharmacie({ variables: { id: pharmaId } });
    }
  }, [pathname, pharmaId]);

  /**
   * Set values on edit
   */
  useEffect(() => {
    if (pharmacie) {
      const p = pharmacie;
      console.log('*******', p);
      setValues(prevValues => ({
        ...prevValues,
        id: p.id,
        entreeSortie: {
          ...prevValues.entreeSortie,
          dateEntree: (p.entreeSortie && p.entreeSortie.dateEntree) || null,
          idMotifEntree:
            p.entreeSortie &&
            p.entreeSortie.pharmacieMotifEntree &&
            p.entreeSortie.pharmacieMotifEntree.id,
          idContrat: p.entreeSortie && p.entreeSortie.contrat && p.entreeSortie.contrat.id,
        },
        cip: p.cip || '',
        nom: p.nom || '',
        adresse1: p.adresse1 || '',
        cp: p.cp || '',
        ville: p.ville || '',
        informatique: {
          idLogiciel:
            (p.informatique && p.informatique.logiciel && p.informatique.logiciel.id) || '',
        },
      }));
    }
  }, [pharmacie]);

  if (mutationSuccess) return <ActionSuccess />;

  return (
    <div className={classes.container}>
      {(loading || mutationLoading || deleteLoading) && (
        <Backdrop
          value={
            mutationLoading ? mutationLoadingMsg : deleteLoading ? 'Suppression en cours...' : ''
          }
        />
      )}
      <SubToolbar
        title={subToolbarTitle}
        dark={!isOnList}
        withBackBtn={!isOnList}
        onClickBack={goBack}
      >
        <SubToolbarChildren />
      </SubToolbar>
      {isOnList ? (
        <>
          <div className={classes.searchInputBox}>
            <SearchInput searchPlaceholder="Rechercher une pharmacie" />
          </div>
          {listPharmacie}
        </>
      ) : (
        <PharmacieForm
          values={values}
          isOnCreate={isOnCreate}
          isOnEdit={isOnEdit}
          handleChangeInput={handleChange}
          handleChangeDate={handleChangeDate}
        />
      )}

      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
    </div>
  );
};

export default withRouter(Pharmacie);
