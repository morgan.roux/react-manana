import { useApolloClient, useLazyQuery } from '@apollo/client';
import {
  PHARMACIEVariables,
  PHARMACIE_pharmacie,
  PHARMACIE,
} from '../../../../graphql/Pharmacie/types/PHARMACIE';
import { GET_PHARMACIE } from '../../../../graphql/Pharmacie';
import { displaySnackBar } from '../../../../utils/snackBarUtils';

/**
 *  get pharmacie hooks
 */
const useGetPharmacie = (id: string) => {
  const client = useApolloClient();

  const [getPharmacie, { data, loading }] = useLazyQuery<PHARMACIE, PHARMACIEVariables>(
    GET_PHARMACIE,
    {
      variables: { id },
      onError: errors => {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, {
            type: 'ERROR',
            message: err.message,
            isOpen: true,
          });
        });
      },
    },
  );

  const pharmacie: PHARMACIE_pharmacie | null = (data && data.pharmacie) || null;

  return { getPharmacie, pharmacie, loading };
};

export default useGetPharmacie;
