import React, { FC, useState, ChangeEvent, Props } from 'react';
import Validator from 'validator';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import { CustomFormTextField } from '../../Common/CustomTextField';
import CustomButton from '../../Common/CustomButton';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { useForm } from '../CustomHooks';
import GroupementInterface from '../../../Interface/GroupementInterface';
import Dropzone from '../../Common/Dropzone';
import { AxiosResponse } from 'axios';

interface FormProps {
  defaultData?: GroupementInterface;
  startUpload?: boolean;
  setStartUpload?: (state: boolean) => void;
  setFileUploadResult?: React.Dispatch<React.SetStateAction<AxiosResponse<any> | null>>;
  withImagePreview?: boolean;
  fileIdentifierPrefix?: string;
  setFilesOut?: React.Dispatch<React.SetStateAction<File[]>>;
  uploadDirectory?: string;
  fileAleadySetUrl?: string;
  setFileAlreadySet?: React.Dispatch<React.SetStateAction<string | undefined>>;
  action(data: GroupementInterface): void;
}

const styles = () =>
  createStyles({
    marginBottom: {
      marginBottom: 25,
    },
    marginRight: {
      marginRight: 10,
    },
    titleForm: {
      color: '#1D1D1D',
      marginTop: 0,
    },
    flex: {
      display: 'flex',
      flexDirection: 'row',
    },
    w100: {
      width: '100%',
    },
  });

const Form: FC<RouteComponentProps<any, any, any> & WithStyles & FormProps> = ({
  classes,
  action,
  defaultData,
  history,
  startUpload,
  setStartUpload,
  setFileUploadResult,
  withImagePreview,
  fileIdentifierPrefix,
  setFilesOut,
  uploadDirectory,
  fileAleadySetUrl,
  setFileAlreadySet,
}) => {
  const { values, handleChange, handleSubmit } = useForm(action, defaultData);
  const {
    nom,
    nomResponsable,
    prenomResponsable,
    adresse1,
    adresse2,
    ville,
    cp,
    pays,
    telBureau,
    telMobile,
    mail,
    site,
    commentaire,
  } = values;

  const canSubmit = (): boolean => {
    return nom &&
      nomResponsable &&
      prenomResponsable &&
      adresse1 &&
      cp &&
      ville &&
      telBureau &&
      mail &&
      Validator.isEmail(mail)
      ? true
      : false;
  };

  const error = () => {
    if (mail && !Validator.isEmail(mail)) return true;
    return false;
  };

  const goBackToChoice = () => {
    history.goBack();
  };

  return (
    <>
      <h3 className={classes.titleForm}>LOGO DU GROUPEMENT</h3>
      <Dropzone
        setStartUpload={setStartUpload}
        startUpload={startUpload}
        setFileUploadResult={setFileUploadResult}
        withImagePreview={withImagePreview}
        identifierPrefix={fileIdentifierPrefix}
        setFilesOut={setFilesOut}
        uploadDirectory={uploadDirectory}
        fileAlreadySetUrl={fileAleadySetUrl}
        setFileAlreadySet={setFileAlreadySet}
      />

      <form onSubmit={handleSubmit}>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="nom"
            label="Nom du groupement"
            onChange={handleChange}
            value={nom}
            required={true}
          />
        </div>
        <h3 className={classes.titleForm}>RESPONSABLE</h3>
        <div className={`${classes.marginBottom} ${classes.flex}`}>
          <div className={`${classes.marginRight} ${classes.w100}`}>
            <CustomFormTextField
              name="nomResponsable"
              label="Nom"
              onChange={handleChange}
              value={nomResponsable}
              required={true}
            />
          </div>
          <div className={classes.w100}>
            <CustomFormTextField
              name="prenomResponsable"
              label="Prénom"
              onChange={handleChange}
              value={prenomResponsable}
              required={true}
            />
          </div>
        </div>
        <h3 className={classes.titleForm}>COORDONNÉES</h3>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="adresse1"
            label="Adresse 1"
            onChange={handleChange}
            value={adresse1}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="adresse2"
            label="Adresse 2"
            onChange={handleChange}
            value={adresse2}
          />
        </div>
        <div className={`${classes.marginBottom} ${classes.flex}`}>
          <div className={`${classes.marginRight} ${classes.w100}`}>
            <CustomFormTextField
              name="cp"
              label="Code Postal"
              onChange={handleChange}
              value={cp}
              required={true}
            />
          </div>
          <div className={`${classes.marginRight} ${classes.w100}`}>
            <CustomFormTextField
              name="ville"
              label="Ville"
              onChange={handleChange}
              value={ville}
              required={true}
            />
          </div>
          <div className={classes.w100}>
            <CustomFormTextField name="pays" label="Pays" onChange={handleChange} value={pays} />
          </div>
        </div>
        <div className={`${classes.marginBottom} ${classes.flex}`}>
          <div className={`${classes.marginRight} ${classes.w100}`}>
            <CustomFormTextField
              name="telBureau"
              label="Téléphone bureau"
              onChange={handleChange}
              value={telBureau}
              required={true}
            />
          </div>
          <div className={classes.w100}>
            <CustomFormTextField
              name="telMobile"
              label="Téléphone Mobile"
              onChange={handleChange}
              value={telMobile}
            />
          </div>
        </div>
        <div className={`${classes.marginBottom} ${classes.flex}`}>
          <div className={`${classes.marginRight} ${classes.w100}`}>
            <CustomFormTextField
              error={error()}
              name="mail"
              label="Email"
              onChange={handleChange}
              value={mail}
              required={true}
            />
          </div>
          <div className={classes.w100}>
            <CustomFormTextField
              name="site"
              label="Site web"
              onChange={handleChange}
              value={site}
            />
          </div>
        </div>
        <h3 className={classes.titleForm}>COMMENTAIRE</h3>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="commentaire"
            label="Commentaire (optionnel)"
            onChange={handleChange}
            value={commentaire}
          />
        </div>
        <div className={`${classes.marginBottom} ${classes.flex}`}>
          <div className={`${classes.marginRight} ${classes.w100}`}>
            <CustomButton size="large" fullWidth={true} onClick={goBackToChoice}>
              RETOUR
            </CustomButton>
          </div>
          <div className={classes.w100}>
            <CustomButton
              size="large"
              color="secondary"
              fullWidth={true}
              disabled={!canSubmit()}
              onClick={handleSubmit}
            >
              ENREGISTRER
            </CustomButton>
          </div>
        </div>
      </form>
    </>
  );
};

export default withStyles(styles, { withTheme: true })(withRouter(Form));
