import React, { useState, ChangeEvent, FC } from 'react';
import { useQuery, useApolloClient } from '@apollo/client';
import { Theme, makeStyles, createStyles } from '@material-ui/core/styles';
import InputAdornment from '@material-ui/core/InputAdornment';
import GroupIcon from '@material-ui/icons/Group';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import CustomSelect from '../../Common/CustomSelect/CustomSelectChoixGroupement';
import CustomButton from '../../Common/CustomButton';
import { GET_GROUPEMENTS } from '../../../graphql/Groupement/query';
import { GROUPEMENTS } from '../../../graphql/Groupement/types/GROUPEMENTS';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { setGroupement, getAccessToken } from './../../../services/LocalStorage';
import { useLocation } from 'react-router';
import { APP_SSO_URL } from '../../../config';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import Backdrop from '../../Common/Backdrop';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    content: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      marginBottom: 65,
      justifyContent: 'space-between',
      '& .MuiInputBase-adornedStart': {
        marginTop: '0!important',
        paddingLeft: 14,
      },
      '& .MuiSelect-select': {
        padding: '16px 14px',
        minHeight: 30,
      },
    },
    form: {
      width: '100%',
      '& button': {
        marginTop: 80,
      },
    },
    addGroupementBtn: {
      background: 'linear-gradient(to right, rgba(166,207,53,1) 0%, rgba(139,198,63,1) 100%)',
      height: 24,
      minHeight: 24,
      maxHeight: 24,
      width: 24,
      minWidth: 24,
      maxWidth: 24,
      marginLeft: 20,
      marginTop: 16,
      color: '#ffffff',
      '&:hover': {
        opacity: 0.8,
      },
    },
  }),
);

const Choice: FC<RouteComponentProps> = ({ history: { push } }) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [id, setId] = useState<string>('');
  const [groupements, setGroupements] = useState<Array<object | null> | null>([]);
  const disabled = groupements && groupements.length > 0 ? false : true;

  const { data, loading } = useQuery<GROUPEMENTS>(GET_GROUPEMENTS, {
    onError: () => {
      displaySnackBar(client, { type: 'ERROR', message: 'Il y a un erreur', isOpen: true });
    },
  });

  const location = useLocation();
  const token = getAccessToken();

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    if (value) {
      setId(value);
    }
  };

  const isDisabled = () => {
    if (!id) return true;
    return false;
  };

  const handleNext = () => {
    const selectedGroupement = (groupements || []).find((grp: any) => grp && id === grp.id);
    if (selectedGroupement) {
      setGroupement(selectedGroupement);
      if (location.search) {
        const url = `${APP_SSO_URL}/open-helloid/${id || ''}/${token}${location.search}`;
        window.location.href = url;
        return null;
      }
      push('/');
    }
  };

  const goToCreateGroupement = () => push(`/groupement/new`);

  if (data && data.groupements && data.groupements !== groupements) {
    setGroupements(data.groupements);
  }

  return (
    <div className={classes.content}>
      <div className={classes.form}>
        {loading && <Backdrop />}
        <CustomSelect
          list={groupements}
          listId="id"
          index="nom"
          variant="outlined"
          disabled={disabled}
          value={id}
          onClick={handleChange}
          startAdornment={
            <InputAdornment position="start">
              <GroupIcon />
            </InputAdornment>
          }
        />
        <CustomButton
          size="large"
          color="secondary"
          onClick={handleNext}
          fullWidth={true}
          disabled={isDisabled()}
        >
          SUIVANT
        </CustomButton>
      </div>
      <Fab
        size="small"
        aria-label="add"
        className={classes.addGroupementBtn}
        onClick={goToCreateGroupement}
      >
        <AddIcon />
      </Fab>
    </div>
  );
};

export default withRouter(Choice);
