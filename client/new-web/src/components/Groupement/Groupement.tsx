import React, { FC } from 'react';
import { withStyles, createStyles } from '@material-ui/core/styles';
import SinglePage from '../Common/SinglePage';
import Choice from './ChoiceGroupement';

const styles = () => createStyles({});

const Groupement: FC<any> = props => {
  const groupements: Array<string> = [];
  return (
    <SinglePage title="Choix du groupement">
      <Choice />
    </SinglePage>
  );
};

export default withStyles(styles, { withTheme: true })(Groupement);
