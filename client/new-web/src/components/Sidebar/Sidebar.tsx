import React, { FC } from 'react';
import { useStyles } from './styles';
import { Link } from 'react-router-dom';

const Sidebar: FC = () => {
  const classes = useStyles({});
  return (
    <div className={classes.root}>
      <Link to="/actualites">Actualités</Link>
      <Link to="/evenements">Evènements</Link>
      <Link to="/formations">Formations</Link>
      <Link to="/utilisateurs">Utilisateurs</Link>
      <Link to="/groupements">Groupements</Link>
    </div>
  );
};

export default Sidebar;
