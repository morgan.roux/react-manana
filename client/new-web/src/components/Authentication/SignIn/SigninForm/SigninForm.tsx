import { useApolloClient, useMutation } from '@apollo/client';
import { Button } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import React, { ChangeEvent, FC, KeyboardEvent } from 'react';
import { FormattedMessage } from 'react-intl';
import { useLocation } from 'react-router';
import { Redirect, RouteComponentProps, withRouter } from 'react-router-dom';
import { USER_SIGNIN } from '../../../../graphql/Authentication/mutation';
import { SIGNIN, SIGNINVariables } from '../../../../graphql/Authentication/types/SIGNIN';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { getAccessToken, isAuthenticated, setAccessToken } from '../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import CustomTextField from '../../../Common/CustomTextField/CustomTextField';
import useStyles from './styles';

interface SignInState {
  login: string;
  password: string;
  showPassword: boolean;
  ip?: string;
}

const initialSignInState: SignInState = {
  login: '',
  password: '',
  showPassword: false,
  ip: '',
};

const SigninForm: FC<RouteComponentProps<any>> = props => {
  const [{ login, password, showPassword, ip }, setSignInState] = React.useState<SignInState>(
    initialSignInState,
  );
  const client = useApolloClient();
  const classes = useStyles({});
  const location = useLocation();

  const [doSignIn, { loading: doSignInLoading, data: doSignInData }] = useMutation<
    SIGNIN,
    SIGNINVariables
  >(USER_SIGNIN, {
    update: async (cache, { data }) => {
      if (data && data.login && data.login.accessToken) {
        setAccessToken(data.login.accessToken)
      }
    },
    onCompleted: data => {
      //  props.history.push('/');

      if (data && data.login && data.login.accessToken) {
        setAccessToken(data.login.accessToken)
      }
      window.location.reload();
    },
    onError: errors => {
      let errorMessage: string = 'Erreur du serveur';
      errors.graphQLErrors.map(error => {
        if (error.extensions) {
          switch (error.extensions.code) {
            case 'ACCOUNT_NOT_EXIST':
              errorMessage = `Veuillez contacter l’administrateur du site : compte ou mot de passe incorrect`;
              break;
            case 'PASSWORD_INVALID':
              errorMessage = 'Mot de passe incorrect';
              break;
            case 'GROUPMENT_EXITED':
              errorMessage = 'Votre groupement est sortie, veuillez contacter votre administrateur';
              break;
            case 'PERSONNEL_EXITED':
              errorMessage =
                'Il semble que vous ne fait plus partie du personnel, veuillez contacter votre administrateur';
              break;
            case 'PPERSONNEL_EXITED':
              errorMessage =
                'Il semble que vous ne fait plus partie du personnel du pharmacie, veuillez contacter votre administrateur';
              break;
            case 'USER_BLOCKED':
              errorMessage =
                'Veuillez contacter l’administrateur du site : compte a été bloqué';
              break;
            case 'USER_BLOCKED_OLD_LAST_LOGIN':
              errorMessage =
                'Veuillez contacter l’administrateur du site : votre compte a été bloqué pour cause d’inactivité';
              break;
            case 'ACTIVATION_REQUIRED':
              errorMessage = "Votre compte n'est pas encore activé, veuillez l'activer";
              break;
            case 'USE_SEND_GRID':
              errorMessage = "Utiliser sendGrid pour l'envoi du mail";
              break;
            case 'MAIL_IP_NOT_SENT':
              errorMessage = "Erreur lors de l'envoi de mail de confirmation d'adresse IP";
              break;
            case 'MAIL_CONFIRM_IP_SENT':
              errorMessage =
                "Un mail de confirmation d'adresse IP vous a été envoyé pour pouvoir s'assurer qu'il s'agit bien de vous";
              break;
            case 'IP_BLOCKED':
              errorMessage = 'Cet appareil a été bloqué par le groupement';
              break;
            case 'IP_TO_CONFIRM':
              errorMessage = 'Cet appareil est en attente de confirmation du groupement';
              break;
            case 'EXPIRED_TOKEN':
              errorMessage = 'Votre token est expiré';
              break;

            default:
              break;
          }
        }
      });

      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errorMessage,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const handleSubmit = () => {
    doSignIn({
      variables: {
        login,
        password,
        ip,
      },
    });
  };

  /* useEffect(() => {
    fetch('https://api.ipify.org/?format=json')
      .then(results => results.json())
      .then(data => {
        setSignInState(prevState => ({ ...prevState, ip: data.ip }));
      })
      .catch(errors => {
        const errorBlock: SnackVariableInterface = {
          type: 'ERROR',
          message:
            "Une erreur est survenue, merci de recharger la page puis de contacter l'équipe technique si cela ne fonctionne pas mieux",
          isOpen: true,
        };
        displaySnackBar(client, errorBlock);
      });
  }, [ip]); */

  const canSubmit = () => true;

  if (getAccessToken() && isAuthenticated()) {
    if (location.search) {
      return <Redirect to={`/${location.search}`} />;
    }
    return <Redirect to="/" />;
  }

  const handleClickShowPassword = () => {
    setSignInState(prevState => ({ ...prevState, showPassword: !prevState.showPassword }));
  };

  const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
  };

  const handleValueChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setSignInState(prevState => ({ ...prevState, [name]: value }));
  };

  const handleKeyPress = (e: KeyboardEvent<HTMLFormElement>) => {
    if (e.key === 'Enter') {
      handleSubmit();
    }
  };

  return (
    <form className={classes.form} onKeyPress={handleKeyPress}>
      <CustomTextField
        label="Login"
        type="text"
        name="login"
        value={login}
        onChange={handleValueChange}
        margin="dense"
        variant="outlined"
        fullWidth={true}
      />

      <CustomTextField
        label={<FormattedMessage id="auth.login.labels.password" />}
        type={showPassword ? 'text' : 'password'}
        name="password"
        value={password}
        onChange={handleValueChange}
        margin="dense"
        variant="outlined"
        fullWidth={true}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <IconButton
                style={{ color: '#fff' }}
                size="small"
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
              >
                {showPassword ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          ),
        }}
      />
      <Button
        variant="contained"
        className={classes.btnLogin}
        size="small"
        onClick={handleSubmit}
        disabled={!canSubmit() || doSignInLoading}
      >
        {doSignInLoading ? (
          <FormattedMessage id="auth.login.labels.loading" />
        ) : (
          <FormattedMessage id="auth.login.labels.login" />
        )}
      </Button>
    </form>
  );
};

export default withRouter(SigninForm);
