import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import { Grid, Typography, Card, CardContent } from '@material-ui/core';
import { useStyles } from './styles';
import SigninForm from './SigninForm';
import { CustomCheckbox } from '../../Common/CustomCheckbox';

import SigninWithSocial from './SigninWithSocial';
import Copyright from '../../Copyright';

const SignIn: FC = () => {
  const classes = useStyles({});

  return (
    <Grid container={true} className={classes.root}>
      <Grid item={true} className={classes.cardContent}>
        <Card className={classes.card}>
          <CardContent className={classes.fullWidth}>
            <Typography
              variant="h3"
              gutterBottom={true}
              className={`${classes.centerText} ${classes.title}`}
            >
              Bienvenue
            </Typography>
            <p className={`${classes.centerText} ${classes.subtitle}`}>
              Sur votre portail Digital4win
            </p>
            <SigninForm />
            <div className={classes.stayLoggedInAndForgotPassword}>
              <CustomCheckbox location="inLogin" />
              <Link to="/password">Mot de passe oublié ?</Link>
            </div>
            <SigninWithSocial />
            <Copyright />
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export default SignIn;
