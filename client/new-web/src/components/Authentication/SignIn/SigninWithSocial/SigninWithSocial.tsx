import React, { FC } from 'react';
import useStyles from './styles';
import LinkedinIcon from './icons/linkedin.svg';
import FacebookIcon from './icons/facebook.svg';
import GmailIcon from './icons/gmail.svg';

const SigninWithSocial: FC = () => {
  const classes = useStyles({});
  return (
    <>
      <div className={classes.socialIconsTopText}>Ou se connecter via</div>
      <div className={classes.socialIcons}>
        <img src={LinkedinIcon} alt="Copyright Icon" />
        <img src={FacebookIcon} alt="Copyright Icon" />
        <img src={GmailIcon} alt="Copyright Icon" />
      </div>
    </>
  );
};

export default SigninWithSocial;
