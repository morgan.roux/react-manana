import React, { FC, KeyboardEvent, useState } from 'react';
import { useApolloClient, useMutation } from '@apollo/client';
import { InputAdornment, IconButton, FormControl, OutlinedInput, Button } from '@material-ui/core/';
import MailIcon from '@material-ui/icons/Mail';
import { withStyles } from '@material-ui/core/styles';
import { USER_FORGOT_PASSWORD } from '../../../../graphql/Authentication/mutation';
import { isEmailValid } from '../../../../utils/Validator';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import BackLink from './../BackLink'
import {
  FORGOT_PASSWORD,
  FORGOT_PASSWORDVariables,
} from '../../../../graphql/Authentication/types/FORGOT_PASSWORD';
import CustomButton from '../../../Common/CustomButton';
import useStyles from './styles';

const CustomOutlinedInput = withStyles({
  root: {
    background: '#F3F3F3 0% 0% no-repeat padding-box',
    borderRadius: 3,
    border: 0,
    color: '#878787',
    height: 50,
    '&.Mui-focused': {
      borderColor: 'red',
      border: '5px',
    },
  },
})(OutlinedInput);

interface IState {
  login: string;
}

const ForgotPasswordForm: FC<RouteComponentProps<any, any, any>> = ({ history }) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const initialState: IState = { login: '' };
  const [{ login }, setstate] = useState<IState>(initialState);

  const [doForgotPassword, { loading, data }] = useMutation<
    FORGOT_PASSWORD,
    FORGOT_PASSWORDVariables
  >(USER_FORGOT_PASSWORD, {
    onCompleted: ({ forgotPassword }) => {
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: 'Mail de réinitialisation de mot de passe envoyé',
        isOpen: true,
      });

      history.push('/signin');
    },
    onError: error => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: "L'email entré ne correspond à aucun compte",
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const handleSubmit = () => {
    doForgotPassword({
      variables: {
        login,
      },
    });
  };

  const canSubmit = () => true;

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setstate(prevState => ({ ...prevState, [name]: value }));
  };

  const handleKeyPress = (e: KeyboardEvent<HTMLFormElement>) => {
    if (e.key === 'Enter') {
      handleSubmit();
    }
  };

  return (
    <div className={classes.contentForm}>
      <form onKeyPress={handleKeyPress} className={classes.form}>
        <p className={classes.message}>
          Entrez votre adresse email, on vous enverra un lien pour changer votre mot de passe.
        </p>
        <FormControl variant="outlined" className={classes.formControl}>
          <CustomOutlinedInput
            placeholder="Login..."
            id="login"
            name="login"
            value={login}
            fullWidth={true}
            margin="dense"
            startAdornment={
              <InputAdornment position="start">
                <IconButton>
                  <MailIcon />
                </IconButton>
              </InputAdornment>
            }
            labelWidth={0}
            onChange={handleChange}
          />
        </FormControl>
        <BackLink />
        <CustomButton
          color="secondary"
          variant="contained"
          className={classes.btnSend}
          size="large"
          onClick={handleSubmit}
          fullWidth={true}
          disabled={!canSubmit() || loading}
        >
          {loading ? 'Chargement...' : 'Envoyer'}
        </CustomButton>
      </form>
    </div>
  );
};

export default withRouter(ForgotPasswordForm);
