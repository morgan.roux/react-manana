import React, { FC } from 'react';
import SinglePage from '../../../Common/SinglePage';
import UpdatePasswordForm from './UpdatePasswordForm';
import UpdatePasswordMessage from './UpdatePasswordMessage';

const UpdatePassword: FC = (props: any) => {
  const { user } = props;

  return (
    <SinglePage title="Renouvellement de mot de passe">
      <UpdatePasswordMessage user={user} />
      <UpdatePasswordForm />
    </SinglePage>
  );
};

export default UpdatePassword;
