import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() =>
  createStyles({
    message: {
      marginBottom: 40,
      textAlign: 'justify',
      fontSize: 14,
    },
  }),
);

export default useStyles;
