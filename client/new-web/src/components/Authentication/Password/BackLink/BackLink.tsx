import React, { FC } from 'react';
import { Link } from '@material-ui/core';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { clearLocalStorage } from '../../../../services/LocalStorage';



interface BackLinkProps {
    text?: string
}

const BackLink: FC<BackLinkProps & RouteComponentProps> = ({ history, text = "Annuler" }) => {

    const handleClick = () => {
        clearLocalStorage()
        history.push('/')
    }

    return (
        <Link style={{
            textAlign: 'right',
            cursor: 'pointer',
            margin: 10
        }} onClick={handleClick}>{text}</Link>
    );
};

export default withRouter(BackLink);

