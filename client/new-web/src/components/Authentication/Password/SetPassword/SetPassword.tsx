import React, { FC } from 'react';
import { withStyles, createStyles } from '@material-ui/core/styles';
import SinglePage from '../../../Common/SinglePage';
import { SetForm } from '../ResetPassword/SetForm';

const styles = () => createStyles({});

interface SetPasswordProps {
  match: {
    params: {
      token: string;
    };
  };
}

const SetPassword: FC<SetPasswordProps> = props => {
  const { match } = props;
  return (
    <SinglePage title="Nouveau mot de passe">
      <SetForm token={match.params.token} saveBtn="Enregistrer" type="set" message="Mot de passe enregistré avec succès" />
    </SinglePage>
  );
};

export default withStyles(styles, { withTheme: true })(SetPassword);

SetPassword.defaultProps = {
  match: {
    params: {
      token: '',
    },
  },
};
