import React, { FC, useEffect, useState } from 'react';
import { Column } from '../../../Dashboard/Content/Interface';
import { RouteComponentProps } from 'react-router-dom';
import ICurrentPharmacieInterface from '../../../../Interface/CurrentPharmacieInterface';
import moment from 'moment';
import { useQuery } from '@apollo/client';
import { GET_CURRENT_PHARMACIE } from '../../../../graphql/Pharmacie/local';
import CommandeStatus from './CommandeStatus/CommandeStatus';
import Button from '@material-ui/core/Button';
import CustomContent from '../../../Common/CustomContent/CustomContent';
import { FieldsOptions } from '../../../Common/CustomContent/interfaces';
import { getUser, getGroupement } from '../../../../services/LocalStorage';
import { AppAuthorization } from '../../../../services/authorization';
import { PharmacieMinimInfo } from '../../../../graphql/Pharmacie/types/PharmacieMinimInfo';
import SubToolbar from '../../../Common/newCustomContent/SubToolbar';
import SearchInput from '../../../Common/newCustomContent/SearchInput';
import useStyles from './styles';
import { CommandeTable } from '../../../Common/newWithSearch/ComponentInitializer';
import { Box } from '@material-ui/core';
const INPUT_SEARCH_FIELDS: FieldsOptions[] = [
  {
    name: 'owner.userName',
    label: 'Validant',
    value: '',
    type: 'Search',
    placeholder: 'Contenant...',
    ordre: 1,
  },
  {
    name: 'operation.libelle',
    label: 'Opération',
    value: '',
    type: 'Search',
    placeholder: 'Contenant...',
    ordre: 2,
  },
  {
    name: 'commandeStatut.libelle',
    label: 'Statut',
    value: '',
    type: 'Search',
    placeholder: 'Contenant...',
    ordre: 3,
  },
];

const Commandes: FC<RouteComponentProps<any, any, any>> = ({ history }) => {
  const user = getUser();
  const auth = new AppAuthorization(user);
  const groupement = getGroupement();
  const classes = useStyles({});
  const columns: Column[] = [
    {
      name: '',
      label: 'Ref. Cde',
      renderer: (value: any) => {
        return value && value.codeReference ? value.codeReference : '-';
      },
    },
    {
      name: '',
      label: 'Date',
      renderer: (value: any) => {
        return value && value.dateCreation
          ? `${moment(value.dateCreation).format('DD/MM/YYYY')} ${moment(value.dateCreation).format(
            'hh:mm',
          )}`
          : '-';
      },
    },
    {
      name: '',
      label: 'Type Cde',
      renderer: (value: any) => {
        return '-';
      },
    },
    {
      name: 'prixNetTotalHT',
      label: 'Net HT',
      renderer: (value: any) => {
        const prix = value && value.prixNetTotalHT ? value.prixNetTotalHT.toFixed(2) : '';
        return value && value.prixNetTotalHT ? `${prix}€` : '-';
      },
    },
    {
      name: '',
      label: 'Frais de Port',
      renderer: (value: any) => {
        const prix = value && value.frais_Port ? value.frais_Port.toFixed(2) : '';
        return value.frais_Port ? `${prix}€` : '-';
      },
    },
    {
      name: 'owner.userName',
      label: 'Validant',
      renderer: (value: any) => {
        return value && value.owner && value.owner.userName ? value.owner.userName : '-';
      },
    },
    {
      name: 'operation.libelle',
      label: 'OPERATION',
      renderer: (value: any) => {
        return (value && value.operation && value.operation.libelle) || '-';
      },
    },
    {
      name: 'commandeStatut.libelle',
      label: 'STATUT',
      renderer: (value: any) => {
        return value && value.commandeStatut && value.commandeStatut.libelle ? (
          <CommandeStatus
            status={value.commandeStatut.libelle ? value.commandeStatut.libelle : '-'}
            id={value.id}
          />
        ) : (
          '-'
        );
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return value ? (
          <Button
            onClick={viewCommande(value)}
            disabled={!auth.isAuthorizedToViewDetailsCommande()}
          >
            Voir
          </Button>
        ) : (
          '-'
        );
      },
    },
  ];

  const commandeTable = () => {
    return <CommandeTable />;
  };
  const viewCommande = (commande: any) => () => {
    if (CommandeStatus) history.push(`/suivi-commandes/${commande.id}`, { from: '/commandes' });
  };

  // take currentPharmacie
  const [currentPharmacie, setCurrentPharmacie] = useState<PharmacieMinimInfo | null>(null);
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

  useEffect(() => {
    if (myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie) {
      setCurrentPharmacie(myPharmacie.data.pharmacie);
    }
  }, [myPharmacie]);

  // TODO : Filter by pharmacie
  return (
    <div>
      <SubToolbar title="Suivi de commandes" dark={true} withBackBtn={false} />
      <div className={classes.searchInputBox}>
        <SearchInput searchPlaceholder="Rechercher une commande" />
      </div>
      <CommandeTable {...history} />
    </div>
  );
};

export default Commandes;
