import React, { FC, useState } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import classnames from 'classnames';
import { LoaderSmall } from '../../../../Dashboard/Content/Loader/Loader';
import { Box, Button, Menu, MenuItem } from '@material-ui/core';
import { DO_UPDATE_STATUT_COMMANDE } from '../../../../../graphql/Commande/mutation';
import {
  UPDATE_STATUT_COMMANDE,
  UPDATE_STATUT_COMMANDEVariables,
} from '../../../../../graphql/Commande/types/UPDATE_STATUT_COMMANDE';
import { useMutation } from '@apollo/client';
interface CommandeStatusProps {
  status: string;
  id: string;
}
const CommandeStatus: FC<CommandeStatusProps & RouteComponentProps<any, any, any>> = ({
  status,
  id,
}) => {
  const classes = useStyles({});
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [btnText, setBtnText] = useState<string>(status);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const [doUpdateStatutCommande, updataStatutCommandeResult] = useMutation<
    UPDATE_STATUT_COMMANDE,
    UPDATE_STATUT_COMMANDEVariables
  >(DO_UPDATE_STATUT_COMMANDE, {
    onCompleted: data => {
      handleClose();
    },
  });

  const updateStatus = (value: string) => {
    doUpdateStatutCommande({
      variables: {
        id,
        statut: value,
      },
    });
    switch (value) {
      case 'TRANSMISE':
        return setBtnText('Transmise');
      case 'ACQUITTEE':
        return setBtnText('Prise en Compte');
      case 'ATTENTE':
        return setBtnText('En Attente');
      case 'LIVRAISON':
        return setBtnText('En Livraison');
      case 'PREPARATION':
        return setBtnText('En Préparation');
      case 'CLOTUREE':
        return setBtnText('Cloturée');
      case 'ANNULEE':
        return setBtnText('Annulée');
      default:
        return setBtnText('');
    }
  };
  return (
    <Box width="100%" display="flex" justifyContent="start">
      <Button
        style={{ textTransform: 'none', marginLeft: '-8px' }}
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        {btnText}
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        getContentAnchorEl={null}
      >
        <MenuItem onClick={() => updateStatus('TRANSMISE')}>Transmise</MenuItem>
        <MenuItem onClick={() => updateStatus('ACQUITTEE')}>Prise en Compte</MenuItem>
        <MenuItem onClick={() => updateStatus('ATTENTE')}>En Attente</MenuItem>
        <MenuItem onClick={() => updateStatus('LIVRAISON')}>En Livraison</MenuItem>
        <MenuItem onClick={() => updateStatus('PREPARATION')}>En Préparation</MenuItem>
        <MenuItem onClick={() => updateStatus('CLOTUREE')}>Cloturée</MenuItem>
        <MenuItem onClick={() => updateStatus('ANNULEE')}>Annulée</MenuItem>
      </Menu>
    </Box>
  );
};

export default withRouter(CommandeStatus);
