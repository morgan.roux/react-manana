import { Theme, lighten } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      border: '1px solid #9E9E9E',
      borderRadius: 6,
      margin: '15px 75px 24px',
      padding: '12px 48px',
    },
    typeOC: {
      fontSize: '0.875rem',
      fontWeight: 'bold',
      color: theme.palette.secondary.main,
    },
    labelName: {
      color: '#616161',
      margin: '2px 0',
      '& .MuiTypography-root': {
        padding: '0 4px',
        color: '#616161',
        fontWeight: 'bold',
      },
    },
    libelle: {
      padding: '4px 0 8px',
      fontWeight: 'bold',
      fontSize: '1rem',
    },
  }),
);

export default useStyles;
