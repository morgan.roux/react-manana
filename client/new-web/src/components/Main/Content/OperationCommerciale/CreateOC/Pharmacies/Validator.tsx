import AllValuesInterface from '../Interface/AllValuesInterface';
const Validator = (values: AllValuesInterface, activeSelection: string, checkedsPharmacie: any) => {
  if (activeSelection === 'array') {
    switch (checkedsPharmacie) {
      case undefined: {
        return true;
      }
    }
    switch (checkedsPharmacie.length) {
      case 0: {
        return true;
      }
    }
    return false;
  } else if (activeSelection === 'file') {
    switch (values.selectedFiles.pharmacies.length) {
      case 0: {
        return true;
      }
    }
    return false;
  } else {
    return false;
  }
};
export default Validator;
