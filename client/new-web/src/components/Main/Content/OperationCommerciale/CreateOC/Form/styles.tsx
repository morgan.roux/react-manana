import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    form: {
      width: '100%',
      maxWidth: 720,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'start',
      flexDirection: 'column',
      margin: '0 auto',
      '& > div': {
        marginBottom: 15,
      },
      '& > div:nth-child(6)': {
        '& > div': {
          marginBottom: 0,
        },
      },
    },
    pink: {
      color: theme.palette.text.primary,
    },
    dateTextField: {
      marginLeft: 16,
    },
    fieldsetLegend: {
      marginLeft: 5,
    },
    fieldsetLabel: {
      color: theme.palette.common.black,
    },
    treeView: {
      flexGrow: 1,
      maxWidth: 400,
    },
    fieldset: {
      marginTop: 16,
      width: '100%',
      '& > fieldset': {
        border: '1px solid rgba(0, 0, 0, 0.23)',
        borderRadius: 5,
      },
      '& .MuiCheckbox-root, & .MuiCheckbox-colorSecondary.Mui-checked': {
        color: theme.palette.primary.main,
        padding: '0px 9px',
      },
      '& .MuiInputBase-root.Mui-disabled': {
        color: '#ccc',
        '&:hover fieldset': {
          border: '1px solid rgba(0, 0, 0, 0.38)',
          // borderColor: 'rgba(0, 0, 0, 0.38)',
        },
        '& .Mui-focused fieldset': {
          border: '1px solid rgba(0, 0, 0, 0.38)',
          // borderColor: 'rgba(0, 0, 0, 0.38)',
        },
      },
    },
  }),
);

export default useStyles;
