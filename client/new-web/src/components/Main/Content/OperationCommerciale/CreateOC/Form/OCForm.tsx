import React, { FC, useEffect, useState, useRef } from 'react';
import useStyles from './styles';
import CustomAutocomplete from '../../../../../Common/CustomAutocomplete';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import useOCForm from './useOCForm';
import Dropzone from '../../../../../Common/Dropzone';
import OCFOrmInterface from '../Interface/OCFormInterface';
import { LABORATOIRES } from '../../../../../../graphql/Laboratoire/types/LABORATOIRES';
import { commandeTypes } from '../../../../../../graphql/CommandeType/types/commandeTypes';
import { commandeCanals } from '../../../../../../graphql/CommandeCanal/types/commandeCanals';
import { useQuery, useLazyQuery, useApolloClient } from '@apollo/client';
import {
  GET_LABORATOIRES,
  DO_SEARCH_LABORATOIRE,
} from '../../../../../../graphql/Laboratoire/query';
import { GET_COMMANDCANALS } from '../../../../../../graphql/CommandeCanal/query';
import { GET_COMMANDETYPES } from '../../../../../../graphql/CommandeType/query';
import { CustomCheckbox } from '../../../../../Common/CustomCheckbox';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import Validator from './Validator';
import { Moment } from 'moment';
import { DateRangePicker } from '../../../../../Common/DateRangePicker';
import FormInterface from '../Interface/OCInterface';
import { Box, CircularProgress, FormLabel } from '@material-ui/core';
import CustomTextarea from '../../../../../Common/CustomTextarea';
import CustomSelect from '../../../../../Common/CustomSelect';
import {
  SEARCH_LABORATOIRE,
  SEARCH_LABORATOIREVariables,
} from '../../../../../../graphql/Laboratoire/types/SEARCH_LABORATOIRE';
import SnackVariableInterface from '../../../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import { debounce } from 'lodash';
import TodoGenerator from '../../../Actualite/TodoGenerator';
import {
  SEARCH_PROJECT,
  SEARCH_PROJECTVariables,
} from '../../../../../../graphql/Project/types/SEARCH_PROJECT';
import { DO_SEARCH_PROJECT } from '../../../../../../graphql/Project';
import CustomTree from '../../../../../Common/CustomTree';
import { FiltersInterface, GET_LOCAL_FILTERS } from '../../../../../Common/withSearch/withSearch';
const OCForm: FC<OCFOrmInterface & RouteComponentProps> = ({
  defaultState,
  setPresentationSelectedFiles,
  setNextBtnDisabled,
  history,
  setAllValues,
  allValues,
  match,
}) => {
  const client = useApolloClient();
  const classes = useStyles({});
  const {
    values,
    setValues,
    handleChange,
    handleTodoChangeCheckbox,
    handleChangeAutocomplete,
    handleChangeActionDueDate,
    handleChangeActionDescription,
    handleChangeProjectAutocomplete,
  } = useOCForm(allValues as FormInterface);
  const idOperation = match && match.params && match.params.id;
  const pathName = history.location.pathname;
  const {
    laboratoire,
    canalCommande,
    accordCommercial,
    nbMoyenLigne,
    nbPharmacieCommande,
    caMoyenPharmacie,
    actionDescription,
    actionPriorite,
    actionDueDate,
    project,
    libelle,
    description,
    dateDebut,
    dateFin,
    marchePromotion,
    idMarche,
    idPromotion,
  } = values;

  const canalList = useQuery<commandeCanals>(GET_COMMANDCANALS);

  const [commandeCanalList, setCommandeCanalList] = useState<any>();

  const [queryTextLaboratoire, setQueryTextLaboratoire] = useState<string>('');
  const [checkedItem, setCheckedItem] = useState<any>(allValues && allValues.codeItem);

  const laboSortBy = [{ nomLabo: { order: 'asc' } }];
  const laboFilterBy = [{ term: { sortie: 0 } }];

  const laboType = ['laboratoire'];
  const prioriteList = [
    { id: 1, value: 'NORMAL' },
    { id: 2, value: 'IMPORTANT' },
    { id: 3, value: 'BLOQUANT' },
  ];

  // get filters id list from local state
  const filters = useQuery<FiltersInterface>(GET_LOCAL_FILTERS);
  // Get laboratoires list
  const [searchLaboratoires, searchLaboratoiresResult] = useLazyQuery<
    SEARCH_LABORATOIRE,
    SEARCH_LABORATOIREVariables
  >(DO_SEARCH_LABORATOIRE, {
    fetchPolicy: 'cache-first',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  useEffect(() => {
    if (pathName.includes('/pharmacie') || pathName.includes('/produit')) {
      const newPathName = pathName.replace('/pharmacie', '').replace('/produit', '');
      history.push(newPathName);
    }
  });

  useEffect(() => {
    if (values) {
      setAllValues((prevState: any) => ({
        ...allValues,
        ...values,
      }));
    }
  }, [values]);

  useEffect(() => {
    const filter = filters && filters.data && filters.data.filters;
    if (canalCommande) {
      const codeCanal = canalCommande && canalCommande.code;
      // TODO: Migration
      /*(client as any).writeData({
        data: {
          filters: filter
            ? {
              ...filter,
              idCommandeCanals: codeCanal,
            }
            : {
              idCommandeCanals: codeCanal,
              __typename: 'local',
            },
        },
      });*/
      if (!idOperation) {
        // TODO: Migration
       /* (client as any).writeData({
          data: {
            codeCanalActive: codeCanal,
          },
        });*/
        // TODO: Migration
        /*(client as any).writeData({
          data: {
            checkedsProduit: null,
          },
        });*/
      }
    }
  }, [canalCommande]);

  useEffect(() => {
    if (allValues) {
      setNextBtnDisabled(Validator(allValues));
    }
  }, [allValues]);

  useEffect(() => {
    if (laboratoire && laboratoire.nomLabo) {
      setQueryTextLaboratoire(laboratoire.nomLabo);
    }
  }, [laboratoire]);
  // Debounced Search Laboratoire
  useEffect(() => {
    debouncedSearch.current(queryTextLaboratoire);
  }, [queryTextLaboratoire]);

  // Lanch search laboratoire
  useEffect(() => {
    searchLaboratoires({
      variables: {
        type: laboType,
        take: 10,
        query: queryTextLaboratoire,
        sortBy: laboSortBy,
        filterBy: laboFilterBy,
      },
    });
  }, []);

  useEffect(() => {
    if (canalList && canalList.data && canalList.data.commandeCanals) {
      setCommandeCanalList(canalList.data.commandeCanals);
    }
  }, [canalList]);

  useEffect(() => {
    setValues(prevState => ({
      ...prevState,
      ...{ codeItem: checkedItem },
    }));
  }, [checkedItem]);

  // Debounced Search Laboratoire

  const debouncedSearch = useRef(
    debounce((query: string) => {
      if (query.length > 0) {
        searchLaboratoires({
          variables: {
            type: laboType,
            query,
            sortBy: laboSortBy,
            filterBy: laboFilterBy,
            take: 10,
          },
        });
      }
    }, 1000),
  );

  const getDates = (startDate: Moment | null, endDate: Moment | null) => {
    setValues(prevState => ({
      ...prevState,
      ...{ dateDebut: startDate },
      ...{ dateFin: endDate },
    }));
  };
  const isOutsideRange = () => false;

  const handleSetSelectedItem = (value: any) => {
    let isTrue = false;
    if (
      value &&
      value.codeItem &&
      (value.code === 'MARCHE' ||
        (value.parent && value.parent.code && value.parent.code === 'OPERAPROM'))
    ) {
      isTrue = true;
    }
    setValues(prevState => ({
      ...prevState,
      isMarchePromotion: isTrue,
      marchePromotion: null,
      idMarche: '',
      idPromotion: '',
    }));
    setCheckedItem(value);
  };

  return (
    <div className={classes.form}>
      <div className={classes.fieldset}>
        <fieldset>
          <legend className={classes.fieldsetLegend}>
            <FormLabel className={classes.fieldsetLabel}>Type d'opération</FormLabel>
          </legend>
          <CustomTree
            type="item"
            nodeKey="codeItem"
            nodeLabel="name"
            nodeIndex="A"
            nodeParent="parent"
            multiple={false}
            setChecked={handleSetSelectedItem}
            checked={checkedItem}
            onlyLastChild={true}
            expendAll={true}
          />
        </fieldset>
      </div>
      <DateRangePicker
        startDate={dateDebut}
        startDateId="startDateUniqueId"
        endDate={dateFin}
        endDateId="endDateUniqueId"
        getDates={getDates}
        label="Période d’activation"
        required={true}
        variant="outlined"
        isOutsideRange={isOutsideRange}
      />
      <CustomFormTextField
        label="Libellé"
        name="libelle"
        value={libelle}
        onChange={handleChange}
        required={true}
      />
      <CustomTextarea
        label="Description Détaillée"
        name="description"
        value={description}
        onChangeTextarea={handleChange}
        rows={5}
        required={true}
      />
      <CustomFormTextField
        label="CA moyen par pharmacie"
        name="caMoyenPharmacie"
        value={caMoyenPharmacie}
        onChange={handleChange}
      />
      <CustomFormTextField
        label="Nombre total de pharmacies"
        name="nbPharmacieCommande"
        value={nbPharmacieCommande}
        onChange={handleChange}
      />
      <CustomFormTextField
        label="Nombre moyen de lignes"
        name="nbMoyenLigne"
        value={nbMoyenLigne}
        onChange={handleChange}
      />
      <CustomSelect
        label="Priorité"
        list={prioriteList}
        listId="id"
        index="value"
        name="niveauPriorite"
        value={allValues && allValues.niveauPriorite}
        onChange={handleChange}
        required={false}
      />
      <CustomAutocomplete
        id="laboratoireId"
        options={
          (searchLaboratoiresResult &&
            searchLaboratoiresResult.data &&
            searchLaboratoiresResult.data.search &&
            (searchLaboratoiresResult.data.search.data as any)) ||
          []
        }
        optionLabelKey="nomLabo"
        value={laboratoire}
        onAutocompleteChange={value => handleChangeAutocomplete(value, 'laboratoire')}
        label="Laboratoire (optionnel)"
        required={false}
      />
      <Dropzone
        setSelectedFiles={setPresentationSelectedFiles}
        contentText="Glissez et déposez le document de présentation"
        multiple={true}
        selectedFiles={allValues && allValues.selectedFiles && allValues.selectedFiles.presentation}
      />
      <CustomAutocomplete
        id="canalCommandeId"
        options={commandeCanalList ? commandeCanalList : []}
        optionLabelKey="libelle"
        value={canalCommande}
        onAutocompleteChange={value => handleChangeAutocomplete(value, 'canalCommande')}
        label="Canal de commande"
      />
      <CustomCheckbox
        name="accordCommercial"
        label="Accord Commercial"
        value={accordCommercial}
        checked={accordCommercial}
        onChange={handleChange}
      />
      <TodoGenerator
        generateAction={allValues && allValues.generateAction}
        handleChangeActionDescription={handleChangeActionDescription}
        handleChangeActionDueDate={handleChangeActionDueDate}
        actionDescription={actionDescription}
        actionDueDate={actionDueDate}
        actionPriorite={actionPriorite}
        handleChangeCheckbox={handleTodoChangeCheckbox}
        handleChange={handleChange}
        handleChangeProjectAutocomplete={handleChangeProjectAutocomplete}
        project={project}
      />
    </div>
  );
};

export default withRouter(OCForm);
