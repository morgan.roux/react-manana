import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    dialog: {
      overflowY: 'scroll',
      maxHeight: '400px',
    },
    closeButton: {
      height: '50px',
      width: '50px',
    },
  }),
);

export default useStyles;
