export default interface ResumeItem {
  label: string;
  value?: string | string[] | File | number;
  type: RESUMETYPE;
}

export enum RESUMETYPE {
  INPUT = 'INPUT',
  LIST = 'LIST',
  FILE = 'FILE',
}
