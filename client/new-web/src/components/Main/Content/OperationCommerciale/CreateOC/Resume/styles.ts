import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    btnActionsContainer: {
      marginTop: 10,
      display: 'flex',
      flexDirection: 'row',
      maxWidth: 650,
      width: '100%',
      '& button:nth-child(1)': {
        marginRight: 30,
      },
      '& button': {
        height: 50,
      },
    },
    resumeGroupContent: {
      border: '1px solid #cfcdcd',
      borderRadius: 10,
      maxWidth: '40%',
      width: '100%',
    },
    resumeList: {
      paddingTop: 0,
      paddingBottom: 0,
      '& li': {
        borderBottom: '1px solid #cfcdcd',
        padding: 10,
      },
      '& li:nth-last-child(1)': {
        borderBottom: 0,
      },
    },
    resumeListItem: {
      '& .MuiListItemText-root .MuiTypography-body1': {
        color: '#878787',
        letterSpacing: 0,
        fontSize: 14,
        fontWeight: 500,
        opacity: 1,
        fontFamily: 'Montserrat',
      },
    },
    resumeValue: {
      fontWeight: 600,
      opacity: 1,
      fontFamily: 'Montserrat',
      letterSpacing: 0.28,
      fontSize: 16,
      color: '#1D1D1D',
      maxWidth: '60%',
      textAlign: 'justify',
    },
    link: {
      color: 'rgb(227, 65, 104)',
      textDecoration: 'underlined',
    },
    subtitle: {
      color: '#1D1D1D',
      opacity: 1,
      fontSize: '20px',
      fontWeight: 'bold',
      letterSpacing: 0,
      marginBottom: '16px',
    },
    createOCButton: {
      height: '50px',
    },
  }),
);

export default useStyles;
