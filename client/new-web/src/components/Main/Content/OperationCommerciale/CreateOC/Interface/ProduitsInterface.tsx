import { AxiosResponse } from 'axios';
export default interface OCFormInterface {
  setNextBtnDisabled: (disabled: boolean) => void;
  setProduitsSelectedFiles: (files: File[]) => void;
  match: {
    params: { filter: string | undefined; type: string | undefined };
  };
  setAllValues: (values: any) => void;
  allValues: any;
}
