import { Moment } from 'moment';
export default interface FormInterface {
  libelle?: string;
  description?: string;
  dateDebut: Moment | null;
  dateFin: Moment | null;
  laboratoire?: LaboratoireInterface;
  generateAction: boolean;
  caMoyenPharmacie: number;
  nbPharmacieCommande: number;
  nbMoyenLigne: number;
  canalCommande?: any;
  canalType?: CanalTypeInterface;
  accordCommercial?: boolean;
  selectedFiles: {
    presentation: File[];
  };
  actionDescription?: string;
  actionDueDate?: Moment;
  actionPriorite?: number;
  project?: any;
  codeItem?: any;
  idMarche?: string;
  idPromotion?: string;
  marchePromotion?: any;
  isMarchePromotion?: boolean;
}
export interface LaboratoireInterface {
  id: string;
  nomLabo: string;
  __typename: string;
}

export interface CanalCommandeInterface {
  id: string;
  libelle: string;
  __typename: string;
}

export interface CanalTypeInterface {
  id: string;
  libelle: string;
  __typename: string;
}
