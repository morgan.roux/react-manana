import React, { FC, useState, useEffect } from 'react';
import useStyles from './styles';
import { Typography, Box, CircularProgress } from '@material-ui/core';
import { Step } from '../../../../Common/Stepper/Stepper';
import Stepper from '../../../../Common/Stepper';
import OCForm, { TypeOperation } from './Form';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import Resume from './Resume';
import Pharmacies from './Pharmacies';
import Produits from './Produits';
import AllValuesInterface from './Interface/AllValuesInterface';
import moment from 'moment';
import { useApolloClient } from '@apollo/client';
import Filter from '../../../../Common/newWithSearch/Filter';
import { CreateOcProduitFilterProps } from '../../../../Common/newWithSearch/ComponentInitializer';
import { useQuery } from '@apollo/client';
import { GET_CHECKEDS_PRODUIT } from '../../../../../graphql/ProduitCanal/local';
interface CreateOCProps {
  title: string;
  operation?: any;
}
const CreateOC: FC<CreateOCProps & RouteComponentProps> = ({ history, title, operation }) => {
  const classes = useStyles({});
  const pathName = history.location.pathname;
  const client = useApolloClient();
  const [nextBtnDisabled, setNextBtnDisabled] = useState<boolean>(true);

  const [allValues, setAllValues] = useState<AllValuesInterface>({
    actionDueDate: null,
    pharmaciesType: 'all',
    produitsType: 'array',
    generateAction: false,
    selectedFiles: { presentation: [], produits: [], pharmacies: [] },
    niveauPriorite: 1,
    caMoyenPharmacie: 0,
    nbPharmacieCommande: 0,
    nbMoyenLigne: 0,
    isMarchePromotion: false,
  });

  const clearCheckedsProduit = () => {
      // TODO: Migration
    /*(client as any).writeData({
      data: {
        checkedsProduit: null,
      },
    });*/
  };

  const clearCheckedsPharmacie = () => {
    // TODO: Migration
   /* (client as any).writeData({
      data: {
        checkedsPharmacie: null,
      },
    });*/
  };

  useEffect(() => {
    return () => {
      clearCheckedsProduit();
      clearCheckedsPharmacie();
    };
  }, []);

  useEffect(() => {
    if (operation) {
      if (operation && operation.operationArticles && operation.operationArticles.data) {
        console.log('operationArticles', operation.operationArticles);

        const articles = operation.operationArticles.data.map(
          (item: any) => item && item.produitCanal,
        );
        const articlesQte = operation.operationArticles.data.filter(
          (item: any) =>
            item &&
            item.quantite &&
            item.produitCanal && {
              article: item.produitCanal,
              quantite: item.quantite,
              __typename: 'PanierLigne',
            },
        );
        // TODO: Migration
        /*(client as any).writeData({
          data: {
            checkedsProduit: articles,
          },
        });*/

        // TODO: Migration
        /*(client as any).writeData({
          data: {
            localPanier: {
              panierLignes: articlesQte,
              __typename: 'PanierLignes',
            },
          },
        });*/
      }
      if (
        operation &&
        operation.operationPharmacie &&
        operation.operationPharmacie.pharmaciedetails
      ) {
        const pharmacies = operation.operationPharmacie.pharmaciedetails.map(
          (item: any) =>
            item && item.pharmacie && { id: item.pharmacie.id, __typename: 'Pharmacie' },
        );
        // TODO: Migration
        /*(client as any).writeData({
          data: {
            checkedsPharmacie: pharmacies,
          },
        });*/
      }
      setAllValues({
        idOperation: operation && operation.id,
        libelle: operation && operation.libelle,
        dateDebut: operation && operation.dateDebut && moment(operation.dateDebut),
        dateFin: operation && operation.dateFin && moment(operation.dateFin),
        description: operation && operation.description,
        niveauPriorite: operation && operation.niveauPriorite,
        laboratoire: operation && operation.laboratoire,
        caMoyenPharmacie: operation && operation.caMoyenPharmacie,
        nbPharmacieCommande: operation && operation.nbPharmacieCommande,
        nbMoyenLigne: operation && operation.nbMoyenLigne,
        codeItem: operation && operation.item,
        generateAction: operation && operation.actions ? true : false,
        idProject:
          operation &&
          operation.actions &&
          operation.actions.data &&
          operation.actions.data[0] &&
          operation.actions.data[0].project &&
          operation.actions.data[0].project.id,
        project:
          operation &&
          operation.actions &&
          operation.actions.data &&
          operation.actions.data[0] &&
          operation.actions.data[0].project,
        actionDescription:
          operation &&
          operation.actions &&
          operation.actions.data &&
          operation.actions.data[0] &&
          operation.actions.data[0].description,
        actionDueDate:
          operation &&
          operation.actions &&
          operation.actions.data &&
          operation.actions.data[0] &&
          operation.actions.data[0].dueDate,
        actionPriorite:
          operation &&
          operation.actions &&
          operation.actions.data &&
          operation.actions.data[0] &&
          operation.actions.data[0].priority,
        canalCommande: operation &&
          operation.commandeCanal && {
          code: operation.commandeCanal.code,
          libelle: operation.commandeCanal.libelle,
          __typename: '',
        },
        canalType: operation &&
          operation.commandeType && {
          code: operation.commandeType.code,
          libelle: operation.commandeType.libelle,
          __typename: '',
        },
        accordCommercial: operation && operation.accordCommercial,
        pharmaciesType:
          operation && operation.operationPharmacie && operation.operationPharmacie.globalite
            ? 'all'
            : operation && operation.operationPharmacie && operation.operationPharmacie.fichierCible
              ? 'file'
              : 'array',
        pharmacies:
          operation &&
            operation.operationPharmacie &&
            !operation.operationPharmacie.globalite &&
            operation.operationPharmacie.pharmaciedetails
            ? operation.operationPharmacie.pharmaciedetails.map(
              (item: any) =>
                item && item.pharmacie && { id: item.pharmacie.id, name: item.pharmacie.nom },
            )
            : [],
        produitsType:
          operation &&
            operation.operationArticleCible &&
            operation.operationArticleCible.fichierCible
            ? 'file'
            : 'array',

        produits:
          operation &&
            operation.operationArticleCible &&
            operation.operationArticleCible.fichierCible
            ? []
            : operation &&
            operation.operationArticles &&
            operation.operationArticles.data &&
            operation.operationArticles.data.map(
              (item: any) =>
                item &&
                item.produitCanal && {
                  ...item.produitCanal,
                  quantite: item.quantite,
                },
            ),
        selectedFiles: {
          pharmacies:
            operation && operation.operationPharmacie && operation.operationPharmacie.fichierCible
              ? [
                {
                  lastModified: Date.now(),
                  name: operation.operationPharmacie.fichierCible.nomOriginal,
                  type: 'application/vnd.ms-excel',
                  size: 1000000,
                  publicUrl: operation.operationPharmacie.fichierCible.publicUrl,
                } as any,
              ]
              : [],
          presentation:
            operation &&
            operation.fichierPresentations &&
            operation.fichierPresentations.map((fichier: any) => {
              return {
                lastModified: Date.now(),
                name: fichier && fichier.nomOriginal,
                type: 'application/pdf',
                size: 1000000,
                publicUrl: fichier && fichier.publicUrl,
              };
            }),
          produits:
            operation &&
              operation.operationArticleCible &&
              operation.operationArticleCible.fichierCible
              ? [
                {
                  lastModified: Date.now(),
                  name: operation.operationArticleCible.fichierCible.nomOriginal,
                  type: 'application/vnd.ms-excel',
                  size: 1000000,
                  publicUrl: operation.operationArticleCible.fichierCible.publicUrl,
                } as any,
              ]
              : [],
        },
        idMarche: (operation && operation.marche && operation.marche.id) || '',
        idPromotion: (operation && operation.promotion && operation.promotion.id) || '',
        marchePromotion: (operation && operation.marche) || (operation && operation.promotion),
      });
    }
  }, [operation]);

  const steps: Step[] =
    allValues && allValues.isMarchePromotion
      ? [
        {
          title: `Details d'operation`,
          content: (
            <OCForm
              setPresentationSelectedFiles={(value: File[]) =>
                setAllValues(prevState => ({
                  ...prevState,
                  ...{
                    selectedFiles: {
                      presentation: value,
                      produits: prevState.selectedFiles.produits,
                      pharmacies: prevState.selectedFiles.pharmacies,
                    },
                  },
                }))
              }
              setNextBtnDisabled={setNextBtnDisabled}
              setAllValues={setAllValues}
              allValues={allValues}
            />
          ),
        },
        {
          title: `Type d'operation`,
          content: (
            <TypeOperation
              setNextBtnDisabled={setNextBtnDisabled}
              setAllValues={setAllValues}
              allValues={allValues}
            />
          ),
        },
        {
          title: 'Choix des pharmacies',
          content: (
            <Pharmacies
              setNextBtnDisabled={setNextBtnDisabled}
              setPharmaciesSelectedFiles={(value: File[]) =>
                setAllValues(prevState => ({
                  ...prevState,
                  ...{
                    selectedFiles: {
                      presentation: prevState.selectedFiles.presentation,
                      produits: prevState.selectedFiles.produits,
                      pharmacies: value,
                    },
                  },
                }))
              }
              setAllValues={setAllValues}
              allValues={allValues}
            />
          ),
        },
        {
          title: 'Résumé',
          content: <Resume allValues={allValues} />,
        },
      ]
      : [
        {
          title: `Details d'operation`,
          content: (
            <OCForm
              setPresentationSelectedFiles={(value: File[]) =>
                setAllValues(prevState => ({
                  ...prevState,
                  ...{
                    selectedFiles: {
                      presentation: value,
                      produits: prevState.selectedFiles.produits,
                      pharmacies: prevState.selectedFiles.pharmacies,
                    },
                  },
                }))
              }
              setNextBtnDisabled={setNextBtnDisabled}
              setAllValues={setAllValues}
              allValues={allValues}
            />
          ),
        },
        {
          title: 'Choix des pharmacies',
          content: (
            <Pharmacies
              setNextBtnDisabled={setNextBtnDisabled}
              setPharmaciesSelectedFiles={(value: File[]) =>
                setAllValues(prevState => ({
                  ...prevState,
                  ...{
                    selectedFiles: {
                      presentation: prevState.selectedFiles.presentation,
                      produits: prevState.selectedFiles.produits,
                      pharmacies: value,
                    },
                  },
                }))
              }
              setAllValues={setAllValues}
              allValues={allValues}
            />
          ),
        },
        {
          title: 'Choix des produits',
          content: (
            <Produits
              setNextBtnDisabled={setNextBtnDisabled}
              setProduitsSelectedFiles={(value: File[]) =>
                setAllValues(prevState => ({
                  ...prevState,
                  ...{
                    selectedFiles: {
                      presentation: prevState.selectedFiles.presentation,
                      produits: value,
                      pharmacies: prevState.selectedFiles.pharmacies,
                    },
                  },
                }))
              }
              setAllValues={setAllValues}
              allValues={allValues}
            />
          ),
        },
        {
          title: 'Résumé',
          content: <Resume allValues={allValues} />,
        },
      ];

  const backToHome = (): void => {
    history.push('/operations-commerciales');
  };

  useEffect(() => {
    const ocCanalCode = operation && operation.commandeCanal && operation.commandeCanal.code;
    const allValuesCanalCode = allValues && allValues.canalCommande && allValues.canalCommande.code;
    if (ocCanalCode && allValuesCanalCode && ocCanalCode !== allValuesCanalCode) {
      clearCheckedsProduit();
    }
  }, [allValues]);

  console.log('***************ALL*****************', allValues);

  if (pathName.startsWith('/edit') && (!operation || !allValues.canalCommande)) {
    return (
      <Box display="flex" width="100%" height="100%" justifyContent="center" alignItems="center">
        <CircularProgress color="secondary" />
      </Box>
    );
  }

  return (
    <Box display="flex">
      {pathName.includes('/produit') && <Filter filterProps={CreateOcProduitFilterProps} />}
      <Stepper
        title={title}
        steps={steps}
        backToHome={backToHome}
        disableNextBtn={nextBtnDisabled}
        fullWidth={false}
      />
    </Box>
  );
};

export default withRouter(CreateOC);
