import React, { FC, useState, useEffect } from 'react';
import useStyles from './styles';
import classnames from 'classnames';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import {
  CircularProgress,
  Box,
  Typography,
  Button,
  List,
  ListItem,
  ListItemText,
  Link,
} from '@material-ui/core';

import CustomButton from '../../../../../Common/CustomButton';
import { RESUMETYPE } from '../Interface/ResumeInterface';

import useResume from './useResume';
import AllValuesInterface from '../Interface/AllValuesInterface';
import ListModal from './ListModal';
import Backdrop from '../../../../../Common/Backdrop';
import { useLazyQuery } from '@apollo/client';
import { GET_MARCHE_PRODUIT_CANAL } from '../../../../../../graphql/Marche/query';
import { GET_PROMOTION_PRODUIT_CANAL } from '../../../../../../graphql/Promotion/query';
import {
  MARCHE_PRODUIT_CANAL,
  MARCHE_PRODUIT_CANALVariables,
} from '../../../../../../graphql/Marche/types/MARCHE_PRODUIT_CANAL';
import {
  PROMOTION_PRODUIT_CANAL,
  PROMOTION_PRODUIT_CANALVariables,
} from '../../../../../../graphql/Promotion/types/PROMOTION_PRODUIT_CANAL';
interface ResumeProps {
  allValues: AllValuesInterface;
  setIsOperationCreated?: (value: boolean) => void;
}
const Resume: FC<ResumeProps & RouteComponentProps<any, any, any>> = ({
  history,
  allValues,
  setIsOperationCreated,
}) => {
  const classes = useStyles({});
  const pathName = history.location.pathname;
  const goToOC = () => {
    if (setIsOperationCreated) setIsOperationCreated(true);
    history.push('/operations-commerciales');
  };

  const { values, createOC, isUploading, isOperationSucceed } = useResume(allValues, goToOC);
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [modalList, setModalList] = useState<string[]>([]);
  const [modalTitle, setModalTitle] = useState<string>('');

  /**
   * Get Marche for Edit
   */
  const [getMarche, { data: marcheData }] = useLazyQuery<
    MARCHE_PRODUIT_CANAL,
    MARCHE_PRODUIT_CANALVariables
  >(GET_MARCHE_PRODUIT_CANAL, { fetchPolicy: 'cache-and-network' });

  /**
   * Get Promo for Edit
   */
  const [getPromo, { data: promoData }] = useLazyQuery<
    PROMOTION_PRODUIT_CANAL,
    PROMOTION_PRODUIT_CANALVariables
  >(GET_PROMOTION_PRODUIT_CANAL, { fetchPolicy: 'cache-and-network' });

  /**
   * Run queries getMarche or getPromo if it's on edit
   */
  useEffect(() => {
    if (allValues && (allValues.idMarche || allValues.idPromotion)) {
      if (allValues.idMarche) getMarche({ variables: { id: allValues.idMarche } });
      if (allValues.idPromotion) getPromo({ variables: { id: allValues.idPromotion } });
    }
  }, [allValues]);

  useEffect(() => {
    if (pathName.includes('/pharmacie') || pathName.includes('/produit')) {
      const newPathName = pathName.replace('/pharmacie', '').replace('/produit', '');
      history.push(newPathName);
    }
  });

  const openModal = (e: any, item: any) => {
    e.preventDefault();
    const produits =
      allValues && (allValues.idMarche || allValues.idPromotion)
        ? marcheData &&
          marcheData.marche &&
          marcheData.marche.canalArticles &&
          marcheData.marche.canalArticles.length
          ? marcheData.marche.canalArticles.map(prod => prod?.produit?.libelle).filter(item => item)
          : promoData &&
            promoData.promotion &&
            promoData.promotion.canalArticles &&
            promoData.promotion.canalArticles.length
            ? promoData.promotion.canalArticles
              .map(prod => prod?.produit?.libelle)
              .filter(item => item)
            : item.value
        : item.value;
    console.log('produits', produits);
    setModalList(produits);
    setModalTitle(`Liste des ${item.label}`);
    setIsModalOpen(!isModalOpen);
  };

  return (
    <Box
      width="100%"
      height="100%"
      justifyContent="center"
      alignItems="center"
      display="flex"
      flexDirection="column"
    >
      <Typography className={classes.subtitle}>Votre opération</Typography>
      <Box className={classes.resumeGroupContent}>
        <List className={classes.resumeList}>
          {values.map((item, index) => (
            <ListItem className={classes.resumeListItem}>
              <ListItemText primary={item.label} />
              {item.type === RESUMETYPE.INPUT || item.type === RESUMETYPE.FILE ? (
                <div className={classes.resumeValue}>{item.value}</div>
              ) : (
                <Link className={classes.link} onClick={(e: any) => openModal(e, item)}>
                  Voir la liste
                </Link>
              )}
            </ListItem>
          ))}
        </List>
      </Box>

      {isUploading && (
        <Backdrop
          value={
            allValues && allValues.idOperation
              ? "Modification de l'opération commerciale en cours..."
              : "Création de l'opération commerciale en cours..."
          }
        />
      )}

      <Box marginTop="32px">
        <CustomButton
          className={classes.createOCButton}
          onClick={createOC}
          fullWidth={true}
          color="secondary"
          disabled={isUploading || isOperationSucceed}
        >
          {allValues && allValues.idOperation ? 'Modifier ' : 'Créer '} opération commerciale
        </CustomButton>
      </Box>
      <ListModal
        isModalOpen={isModalOpen}
        list={modalList}
        title={modalTitle}
        setIsModalOpen={setIsModalOpen}
      />
    </Box>
  );
};

export default withRouter(Resume);
