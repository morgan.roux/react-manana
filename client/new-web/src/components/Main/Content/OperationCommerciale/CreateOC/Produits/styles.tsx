import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    label: {
      '& .MuiFormControlLabel-label': {
        fontSize: '12px',
      },
    },
    tableContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
  }),
);

export default useStyles;
