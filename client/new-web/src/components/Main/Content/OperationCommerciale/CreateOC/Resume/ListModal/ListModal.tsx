import React, { FC, useState, useEffect } from 'react';
import useStyles from './styles';
import classnames from 'classnames';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import {
  Box,
  Dialog,
  DialogContent,
  DialogTitle,
  List,
  ListItem,
  ListItemText,
  IconButton,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
interface ListModalProps {
  title: string;
  list: string[];
  isModalOpen: boolean;
  setIsModalOpen: (value: boolean) => void;
}
const ListModal: FC<ListModalProps & RouteComponentProps<any, any, any>> = ({
  title,
  list,
  isModalOpen,
  setIsModalOpen,
}) => {
  const classes = useStyles({});

  return (
    <Dialog open={isModalOpen}>
      <Box display="flex" flexDirection="row" justifyContent="space-between">
        <DialogTitle>{title}</DialogTitle>{' '}
        <IconButton className={classes.closeButton}>
          <CloseIcon onClick={() => setIsModalOpen(false)} />
        </IconButton>
      </Box>

      <DialogContent className={classes.dialog}>
        <List>
          {list &&
            list.map(item => (
              <ListItem>
                <ListItemText primary={item} />
              </ListItem>
            ))}
        </List>
      </DialogContent>
    </Dialog>
  );
};

export default withRouter(ListModal);
