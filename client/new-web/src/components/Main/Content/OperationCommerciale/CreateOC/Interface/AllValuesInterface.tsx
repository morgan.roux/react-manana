import { Moment } from 'moment';
export default interface AllValuesInterface {
  idOperation?: string;
  description?: string;
  niveauPriorite: number;
  generateAction: boolean;
  libelle?: string;
  dateDebut?: Moment;
  dateFin?: Moment;
  laboratoire?: LaboratoireInterface;
  canalCommande?: CanalCommandeInterface;
  canalType?: CanalTypeInterface;
  accordCommercial?: boolean;
  pharmaciesType: string;
  produitsType: string;
  selectedFiles: DropzoneInterface;
  pharmacies?: ItemInterface[];
  produits?: ItemInterface[];
  caMoyenPharmacie: number;
  nbPharmacieCommande: number;
  nbMoyenLigne: number;
  codeItem?: any;
  actionDescription?: string;
  actionDueDate?: any;
  actionPriorite?: number;
  idProject?: any;
  project?: any;
  idMarche?: string;
  idPromotion?: string;
  marchePromotion?: any;
  isMarchePromotion?: boolean;
}

export interface LaboratoireInterface {
  id: string;
  nom: string;
  __typename: string;
}

export interface CanalCommandeInterface {
  code: string;
  libelle: string;
  __typename: string;
}

export interface CanalTypeInterface {
  code: string;
  libelle: string;
  __typename: string;
}

export interface DropzoneInterface {
  pharmacies: File[];
  presentation: File[];
  produits: File[];
}

export interface ItemInterface {
  id: string;
  name: string;
  quantite?: number;
}
