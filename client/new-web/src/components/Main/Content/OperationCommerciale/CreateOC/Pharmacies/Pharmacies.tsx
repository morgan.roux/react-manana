import { useQuery } from '@apollo/client';
import { Box, RadioGroup, Typography } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { GET_CHECKEDS_PHARMACIE } from '../../../../../../graphql/Pharmacie/local';
import Dropzone from '../../../../../Common/Dropzone';
import SearchInput from '../../../../../Common/newCustomContent/SearchInput';
import { PharmacieTable } from '../../../../../Common/newWithSearch/ComponentInitializer';
import RadioButton from '../../../../../Common/SelectionContainer/RadioButton';
import AllValuesInterface from '../Interface/AllValuesInterface';
import PharmaciesInterface from '../Interface/PharmaciesInterface';
import useStyles from './styles';
import Validator from './Validator';

const Pharmacies: FC<PharmaciesInterface & RouteComponentProps> = ({
  setNextBtnDisabled,
  history,
  setPharmaciesSelectedFiles,
  setAllValues,
  allValues,
}) => {
  const classes = useStyles({});
  const types = [
    { value: 'all', label: 'Toutes les Pharmacies' },
    { value: 'array', label: 'Sélection de Pharmacies' },
    { value: 'file', label: 'Fichier de Pharmacies' },
  ];
  const pathName = history.location.pathname;
  const [activeSelection, setActiveSelection] = useState<string>(
    allValues && allValues.pharmaciesType,
  );

  const checkesdPharmacieResult = useQuery(GET_CHECKEDS_PHARMACIE);

  const checkedsPharmacie =
    (checkesdPharmacieResult &&
      checkesdPharmacieResult.data &&
      checkesdPharmacieResult.data.checkedsPharmacie) ||
    [];

  useEffect(() => {
    if (allValues) {
      setNextBtnDisabled(Validator(allValues, activeSelection, checkedsPharmacie));
    }
  }, [allValues, checkedsPharmacie]);

  useEffect(() => {
    const newPathName = pathName.replace('/pharmacie', '').replace('/produit', '');
    if (activeSelection !== 'array') {
      history.push(newPathName);
    } else {
      history.push(`${newPathName}/pharmacie`);
    }
    setAllValues((prevState: AllValuesInterface) => ({
      ...prevState,
      ...{ pharmaciesType: activeSelection },
    }));
  }, [activeSelection]);

  const handleSelection = (value: string) => {
    setActiveSelection(value);
  };

  return (
    <Box width="100%">
      <Typography className={classes.title}>Cible de pharmacies</Typography>
      <Box display="flex" flexDirection="row" justifyContent="center">
        <RadioGroup row={true}>
          {types.map(item => (
            <RadioButton
              active={activeSelection === item.value ? true : false}
              setSelection={handleSelection}
              value={item.value}
              label={item.label}
              key={item.value}
            />
          ))}
        </RadioGroup>
      </Box>
      {activeSelection === 'array' && (
        <div className={classes.tableContainer}>
          <SearchInput fullWidth={true} searchPlaceholder="Rechercher une pharmacie" />
          <Box>{PharmacieTable}</Box>
        </div>
      )}
      {activeSelection === 'file' && (
        <Dropzone
          acceptFiles={
            'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
          }
          setSelectedFiles={setPharmaciesSelectedFiles}
          selectedFiles={allValues && allValues.selectedFiles && allValues.selectedFiles.pharmacies}
          multiple={false}
        />
      )}
    </Box>
  );
};

export default withRouter(Pharmacies);
