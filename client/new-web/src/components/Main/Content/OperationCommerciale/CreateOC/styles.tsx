import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      overflowY: 'auto',
      width: '100%',
    },
    title: {
      color: '#1D1D1D',
      fontSize: 24,
      fontWeight: 'bold',
      letterSpacing: 0,
      opacity: 1,
    },
    instructions: {
      margin: '10px 0px',
    },
  }),
);

export default useStyles;
