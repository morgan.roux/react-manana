import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    tableRoot: {
      paddingTop: theme.spacing(3),
    },
    title: {
      textAlign: 'center',
    },
    tableContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
  }),
);

export default useStyles;
