import { useState, useEffect } from 'react';
import AllValuesInterface from '../Interface/AllValuesInterface';
import ResumeItem, { RESUMETYPE } from '../Interface/ResumeInterface';
import moment from 'moment';
import { uploadToS3 } from '../../../../../../services/S3';
import { useMutation, useQuery, useApolloClient } from '@apollo/client';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../../graphql/S3';
import { DO_CREATE_OPERATION } from '../../../../../../graphql/OperationCommerciale/mutation';
import {
  createUpdateOperation,
  createUpdateOperationVariables,
} from '../../../../../../graphql/OperationCommerciale/types/createUpdateOperation';
import ICurrentPharmacieInterface from '../../../../../../Interface/CurrentPharmacieInterface';
import {
  GET_CURRENT_PHARMACIE,
  GET_CHECKEDS_PHARMACIE,
} from '../../../../../../graphql/Pharmacie/local';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import SnackVariableInterface from '../../../../../../Interface/SnackVariableInterface';
import { ME_me } from '../../../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../../../services/LocalStorage';
import {
  formatFilenameWithoutDate,
  formatFilename,
} from '../../../../../../utils/filenameFormater';
import { GET_CHECKEDS_PRODUIT } from '../../../../../../graphql/ProduitCanal/local';
import { GET_LOCAL_PANIER } from '../../../../../../graphql/Panier/local';
import { GET_MINIMAL_SEARCH_LABEL } from '../../../../../../graphql/MinimalSearch/query';

export enum TypeFichier {
  AVATAR = 'AVATAR',
  EXCEL = 'EXCEL',
  LOGO = 'LOGO',
  PDF = 'PDF',
  PHOTO = 'PHOTO',
}

const useResume = (allValues: AllValuesInterface, goToOC: () => void) => {
  const client = useApolloClient();
  const checkesdProduitResult = useQuery(GET_CHECKEDS_PRODUIT);

  const checkedsProduit =
    (checkesdProduitResult &&
      checkesdProduitResult.data &&
      checkesdProduitResult.data.checkedsProduit) ||
    [];

  const produits = useQuery(GET_MINIMAL_SEARCH_LABEL, {
    variables: {
      type: ['produitcanal'],
      query: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  _id: checkedsProduit && checkedsProduit.map(produit => produit && produit.id),
                },
              },
            ],
          },
        },
      },
    },
    skip: checkedsProduit && checkedsProduit.length === 0,
  });

  const checkesdPharmacieResult = useQuery(GET_CHECKEDS_PHARMACIE);

  const checkedsPharmacie =
    (checkesdPharmacieResult &&
      checkesdPharmacieResult.data &&
      checkesdPharmacieResult.data.checkedsPharmacie) ||
    [];

  const pharmacies = useQuery(GET_MINIMAL_SEARCH_LABEL, {
    variables: {
      type: ['pharmacie'],
      query: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  _id:
                    checkedsPharmacie &&
                    checkedsPharmacie.map(pharmacie => pharmacie && pharmacie.id),
                },
              },
            ],
          },
        },
      },
    },
    skip: checkedsPharmacie && checkedsPharmacie.length === 0,
  });

  const localPanier = useQuery(GET_LOCAL_PANIER);

  const panierLignes =
    (localPanier &&
      localPanier.data &&
      localPanier.data.localPanier &&
      localPanier.data.localPanier.panierLignes) ||
    [];

  const getProduitQte = (id: any) => {
    const ligne = panierLignes.find(
      ligne => ligne && ligne.produitCanal && ligne.produitCanal.id === id,
    );
    return (ligne && ligne.quantite) || 0;
  };

  const initialVariables = {
    idOperation: allValues && allValues.idOperation,
    description: allValues && allValues.description,
    niveauPriorite: allValues && allValues.niveauPriorite,
    libelle: allValues && allValues.libelle,
    dateDebut: allValues && allValues.dateDebut && allValues.dateDebut.format(),
    dateFin: allValues && allValues.dateFin && allValues.dateFin.format(),
    idLaboratoire: allValues && allValues.laboratoire && allValues.laboratoire.id,
    globalite: allValues && allValues.pharmaciesType === 'all' ? true : false,
    typeCommande: allValues && allValues.canalType && allValues.canalType.code,
    idsPharmacie:
      allValues && allValues.pharmaciesType === 'array' && checkedsPharmacie
        ? checkedsPharmacie.map((item: any) => item && item.id)
        : [],
    articles:
      allValues && allValues.produitsType === 'array' && checkedsProduit
        ? checkedsProduit.map((item: any) => {
          return { idCanalArticle: item.id, quantite: getProduitQte(item && item.id) };
        })
        : [],
    idCanalCommande: allValues && allValues.canalCommande && allValues.canalCommande.code,
    accordCommercial: allValues && allValues.accordCommercial ? true : false,
    idProject: (allValues && allValues.generateAction && allValues.idProject) || null,
    actionPriorite: (allValues && allValues.generateAction && allValues.actionPriorite) || null,
    actionDescription:
      (allValues && allValues.generateAction && allValues.actionDescription) || null,
    actionDueDate: (allValues && allValues.generateAction && allValues.actionDueDate) || null,
    caMoyenPharmacie: allValues && allValues.caMoyenPharmacie,
    nbPharmacieCommande: allValues && allValues.nbPharmacieCommande,
    nbMoyenLigne: allValues && allValues.nbMoyenLigne,
    codeItem: allValues && allValues.codeItem && allValues.codeItem.code,
    idMarche: allValues && allValues.idMarche,
    idPromotion: allValues && allValues.idPromotion,
  };

  const [variables, setVariables] = useState<any>(initialVariables);
  const [acceptedFiles, setAcceptedFiles] = useState<any[]>([]);
  const [isUploading, setIsUploading] = useState<boolean>(false);
  const [isOperationSucceed, setIsOperationSucceed] = useState<boolean>(false);
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

  const capitalizeFirstLetter = (text: string) => {
    return text && text.length > 0 ? text[0].toUpperCase() + text.slice(1).toLowerCase() : '';
  };

  const currentUser: ME_me = getUser();
  const idPharmacieUser =
    (currentUser &&
      currentUser.userPpersonnel &&
      currentUser.userPpersonnel.pharmacie &&
      currentUser.userPpersonnel.pharmacie.id) ||
    (currentUser &&
      currentUser.userTitulaire &&
      currentUser.userTitulaire.titulaire &&
      currentUser.userTitulaire.titulaire.pharmacieUser &&
      currentUser.userTitulaire.titulaire.pharmacieUser.id);

  useEffect(() => {
    if (myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie) {
      const idPharmacie = myPharmacie.data.pharmacie && myPharmacie.data.pharmacie.id;
      setVariables((prevState: any) => ({ ...prevState, ...{ idPharmacie } }));
    }
    setVariables((prevState: any) => ({
      ...prevState,
      ...{ idPharmacieUser, userId: currentUser && currentUser.id },
    }));
  }, [myPharmacie]);

  const values: ResumeItem[] = [
    { label: 'Libellé', value: allValues.libelle, type: RESUMETYPE.INPUT },
    { label: 'Description', value: allValues.description, type: RESUMETYPE.INPUT },
    {
      label: 'Priorité',
      value:
        allValues.niveauPriorite === 1
          ? 'NORMAL'
          : allValues.niveauPriorite === 2
            ? 'IMPORTANT'
            : 'BLOQUANT',
      type: RESUMETYPE.INPUT,
    },
    {
      label: 'Date de début',
      value: allValues && allValues.dateDebut && moment(allValues.dateDebut).format('L'),
      type: RESUMETYPE.INPUT,
    },
    {
      label: 'Date de fin',
      value: allValues && allValues.dateFin && moment(allValues.dateFin).format('L'),
      type: RESUMETYPE.INPUT,
    },
    {
      label: 'Fichier de présentation',
      value:
        allValues &&
        allValues.selectedFiles &&
        allValues.selectedFiles.presentation &&
        allValues.selectedFiles.presentation[0] &&
        allValues.selectedFiles.presentation[0].name,
      type: RESUMETYPE.INPUT,
    },
    {
      label: 'Autres fichiers',
      value:
        allValues && allValues.selectedFiles && allValues.selectedFiles.presentation.length > 1
          ? allValues.selectedFiles.presentation
            .filter((item, index) => index !== 0)
            .map((file: File) => file.name)
          : 'Aucun autre fichier',
      type: allValues.selectedFiles.presentation.length > 1 ? RESUMETYPE.LIST : RESUMETYPE.INPUT,
    },
    {
      label: 'Canal de commande',
      value: allValues && allValues.canalCommande && allValues.canalCommande.libelle,
      type: RESUMETYPE.INPUT,
    },
    {
      label: "Type d'opération",
      value: allValues && allValues.canalType && allValues.canalType.libelle,
      type: RESUMETYPE.INPUT,
    },
    {
      label: 'Accord commercial',
      value: allValues.accordCommercial ? 'OUI' : 'NON',
      type: RESUMETYPE.INPUT,
    },
    {
      label: 'Pharmacies',
      value:
        allValues.pharmaciesType === 'all'
          ? 'Toutes les pharmacies'
          : allValues.pharmaciesType === 'file'
            ? allValues && allValues.selectedFiles && allValues.selectedFiles.pharmacies[0].name
            : pharmacies &&
            pharmacies.data &&
            pharmacies.data.search &&
            pharmacies.data.search.data &&
            pharmacies.data.search.data.map((item: any) =>
              capitalizeFirstLetter((item && item.nom) || ''),
            ),
      type:
        allValues.pharmaciesType === 'all' || allValues.pharmaciesType === 'file'
          ? RESUMETYPE.INPUT
          : RESUMETYPE.LIST,
    },
    {
      label: 'Produits',
      value:
        allValues.produitsType === 'file'
          ? allValues && allValues.selectedFiles && allValues.selectedFiles.produits[0].name
          : produits &&
          produits.data &&
          produits.data.search &&
          produits.data.search.data &&
          produits.data.search.data.map(
            (item: any) =>
              `${capitalizeFirstLetter(
                (item && item.produit && item.produit.libelle) || '',
              )} (${getProduitQte(item && item.id)})`,
          ),
      type: allValues.produitsType === 'file' ? RESUMETYPE.INPUT : RESUMETYPE.LIST,
    },
    {
      label: allValues && allValues.idMarche ? 'Marché' : 'Promotion',
      value: (allValues && allValues.marchePromotion && allValues.marchePromotion.nom) || '-',
      type: RESUMETYPE.INPUT,
    },
    {
      label: 'Projet',
      value:
        (allValues && allValues.generateAction && allValues.project && allValues.project.name) ||
        '-',
      type: RESUMETYPE.INPUT,
    },
    {
      label: 'Priorité de la tâche',
      value: (allValues && allValues.generateAction && allValues.actionPriorite) || '-',
      type: RESUMETYPE.INPUT,
    },
    {
      label: 'Description de la tâche',
      value: (allValues && allValues.generateAction && allValues.actionDescription) || '-',
      type: RESUMETYPE.INPUT,
    },
    {
      label: 'Date limite de la tâche',
      value:
        (allValues &&
          allValues.generateAction &&
          allValues.actionDueDate &&
          allValues.actionDueDate.toLocaleString()) ||
        '-',
      type: RESUMETYPE.INPUT,
    },
    {
      label: 'CA Moyen par pharmacie',
      value: allValues && allValues.caMoyenPharmacie,
      type: RESUMETYPE.INPUT,
    },
    {
      label: 'Nombre total de pharmacie',
      value: allValues && allValues.nbPharmacieCommande,
      type: RESUMETYPE.INPUT,
    },
    {
      label: 'Nombre moyen de lignes',
      value: allValues && allValues.nbMoyenLigne,
      type: RESUMETYPE.INPUT,
    },
    {
      label: 'Item',
      value: allValues && allValues.codeItem && allValues.codeItem.name,
      type: RESUMETYPE.INPUT,
    },
  ];

  const [doCreateOperation] = useMutation<createUpdateOperation, createUpdateOperationVariables>(
    DO_CREATE_OPERATION,
    {
      variables,
      onCompleted: async data => {
        if (data && data.createUpdateOperation) {
          //await Promise.resolve(goToOC()).then(_ => window.location.reload());
          const snackBarData: SnackVariableInterface = {
            type: 'SUCCESS',
            message: `Opération ${allValues && allValues.idOperation ? 'modifiée' : 'créée'
              } avec succès`,
            isOpen: true,
          };
          setIsUploading(false);
          setIsOperationSucceed(true);
          displaySnackBar(client, snackBarData);
          goToOC();
        }
      },

      onError: errors => {
        displaySnackBar(client, {
          isOpen: true,
          type: 'ERROR',
          message: errors.message,
        });
      },
    },
  );

  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      // On Complete presigned url, Upload to S3
      const filePaths: any[] = [];
      if (data && data.createPutPresignedUrls) {
        data.createPutPresignedUrls.map(item => {
          filePaths.push(item && item.filePath);
        });

        const filesPharmacieAccepted: any[] =
          acceptedFiles && acceptedFiles.length
            ? [...acceptedFiles.filter(fileAccepted => fileAccepted.type === 'fichierPharmacie')]
            : [];
        console.log('filesPharmacieAccepted initilal', { filesPharmacieAccepted });

        const filesProduitAccepted: any[] =
          acceptedFiles && acceptedFiles.length
            ? [...acceptedFiles.filter(fileAccepted => fileAccepted.type === 'fichierProduit')]
            : [];
        console.log('filesProduitAccepted initilal', { filesProduitAccepted });

        /* fichier presentation */
        let fichesAccepted: any[] =
          acceptedFiles && acceptedFiles.length
            ? [...acceptedFiles.filter(fileAccepted => fileAccepted.type === 'fichierPresentation')]
            : [];
        console.log('fichesAccepted initilal', { fichesAccepted });
        const presentations =
          fichesAccepted && fichesAccepted.length && fichesAccepted[0] ? [fichesAccepted[0]] : [];

        await Promise.all(
          presentations.map(async fileObject => {
            if (data && data.createPutPresignedUrls && fileObject) {
              return Promise.all(
                data.createPutPresignedUrls.map(async (item: any) => {
                  if (item && item.filePath.includes(formatFilenameWithoutDate(fileObject.file))) {
                    return sendFileToAWS(item, fileObject, 'fichierPresentation', TypeFichier.PDF);
                  }
                }),
              );
            }
          }),
        ).then(async _ => {
          fichesAccepted = fichesAccepted.slice(1);
          const allFiles = [...filesPharmacieAccepted, ...filesProduitAccepted, ...fichesAccepted];
          console.log('fichesAccepted Sliced', { fichesAccepted });
          return Promise.all(
            allFiles.map(async fileObject => {
              if (data && data.createPutPresignedUrls && fileObject) {
                return Promise.all(
                  data.createPutPresignedUrls.map(async (item: any) => {
                    if (
                      item &&
                      item.filePath.includes(formatFilenameWithoutDate(fileObject.file))
                    ) {
                      switch (fileObject.type) {
                        case 'fichierPharmacie':
                          return sendFileToAWS(
                            item,
                            fileObject,
                            'fichierPharmacie',
                            TypeFichier.EXCEL,
                          );
                        case 'fichierProduit':
                          return sendFileToAWS(
                            item,
                            fileObject,
                            'fichierProduit',
                            TypeFichier.EXCEL,
                          );
                        case 'fichierPresentation':
                          return sendFileToAWS(
                            item,
                            fileObject,
                            'fichierPresentation',
                            TypeFichier.PDF,
                          );
                      }
                    }
                  }),
                );
              }
            }),
          ).then(async _ => {
            await doCreateOperation();
            setIsUploading(false);
          });
        });
        /* --------------------------- */
      }
    },
  });

  const sendFileToAWS = async (
    item: any,
    fileObject: any,
    state: string,
    typeFichier: TypeFichier,
  ) => {
    if (fileObject && fileObject.file && item && item.presignedUrl) {
      await uploadToS3(fileObject.file, item.presignedUrl);
      console.log('File sent ==>', { file: fileObject.file, presigned: item.presignedUrl });

      if (state === 'fichierPresentation') {
        return setVariables((prevState: any) => ({
          ...prevState,
          ...{
            fichierPresentations: prevState.fichierPresentations
              ? [
                ...prevState.fichierPresentations,
                ...[
                  {
                    chemin: item && item.filePath,
                    nomOriginal: fileObject.file.name,
                    type: typeFichier,
                  },
                ],
              ]
              : [
                {
                  chemin: item && item.filePath,
                  nomOriginal: fileObject.file.name,
                  type: typeFichier,
                },
              ],
          },
        }));
      } else {
        return setVariables((prevState: any) => ({
          ...prevState,
          ...{
            [state]: {
              chemin: item && item.filePath,
              nomOriginal: fileObject.file.name,
              type: typeFichier,
            },
          },
        }));
      }
    }
  };

  const createOC = () => {
    const filePathsTab: string[] = [];
    if (
      allValues &&
      allValues.selectedFiles.presentation &&
      allValues.selectedFiles.presentation.length > 0
    ) {
      allValues.selectedFiles.presentation.map((fichier: any) => {
        if (fichier && fichier.publicUrl) {
          setVariables((prevState: any) => ({
            ...prevState,
            ...{
              fichierPresentations: prevState.fichierPresentations
                ? [
                  ...prevState.fichierPresentations,
                  ...[
                    {
                      chemin: `files/${((fichier as any).publicUrl as string).split('/')[4]}`,
                      nomOriginal: fichier.name,
                      type: TypeFichier.PDF,
                    },
                  ],
                ]
                : [
                  {
                    chemin: `files/${((fichier as any).publicUrl as string).split('/')[4]}`,
                    nomOriginal: fichier.name,
                    type: TypeFichier.PDF,
                  },
                ],
            },
          }));
        } else {
          console.log('fichier : ', fichier);

          filePathsTab.push(formatFilename(fichier));
          setAcceptedFiles(prevState =>
            prevState.concat([{ type: 'fichierPresentation', file: fichier }]),
          );
        }
      });
    }

    if (
      allValues &&
      allValues.pharmaciesType === 'file' &&
      allValues.selectedFiles.pharmacies &&
      allValues.selectedFiles.pharmacies.length > 0
    ) {
      if ((allValues.selectedFiles.pharmacies[0] as any).publicUrl) {
        setVariables((prevState: any) => ({
          ...prevState,
          ...{
            fichierPharmacie: {
              chemin: `files/${((allValues.selectedFiles.pharmacies[0] as any).publicUrl as string).split('/')[4]
                }`,
              nomOriginal: allValues.selectedFiles.pharmacies[0].name,
              type: TypeFichier.EXCEL,
            },
          },
        }));
      } else {
        allValues.selectedFiles.pharmacies.map((file: File) => {
          filePathsTab.push(formatFilename(file));
          setAcceptedFiles(prevState => prevState.concat([{ type: 'fichierPharmacie', file }]));
        });
      }
    }

    if (
      allValues &&
      allValues.produitsType === 'file' &&
      allValues.selectedFiles.produits &&
      allValues.selectedFiles.produits.length > 0
    ) {
      if ((allValues.selectedFiles.produits[0] as any).publicUrl) {
        setVariables((prevState: any) => ({
          ...prevState,
          ...{
            fichierProduit: {
              chemin: `files/${((allValues.selectedFiles.produits[0] as any).publicUrl as string).split('/')[4]
                }`,
              nomOriginal: allValues.selectedFiles.produits[0].name,
              type: TypeFichier.EXCEL,
            },
          },
        }));
      } else {
        allValues.selectedFiles.produits.map((file: File) => {
          filePathsTab.push(formatFilename(file));
          setAcceptedFiles(prevState => prevState.concat([{ type: 'fichierProduit', file }]));
        });
      }
    }
    // Create presigned url
    setIsUploading(true);
    doCreatePutPresignedUrl({ variables: { filePaths: filePathsTab } });
  };

  return {
    values,
    variables,
    setVariables,
    isUploading,
    isOperationSucceed,
    createOC,
  };
};

export default useResume;
