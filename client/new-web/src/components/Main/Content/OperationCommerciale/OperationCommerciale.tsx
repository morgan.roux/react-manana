import React, { FC, useEffect } from 'react';
import gql from 'graphql-tag';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { ProduitCanal as ArticleType } from '../../../../graphql/ProduitCanal/types/ProduitCanal';
import MainContainer, { LayoutType } from '../../../Common/MainContainer/MainContainer';
import { SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT } from '../../../../Constant/roles';
import AddIcon from '@material-ui/icons/Add';
import { Visibility, ThumbUp, ViewComfy, ChatBubble, ShoppingCart } from '@material-ui/icons';
import MoreOptions from '../../../Common/MainContainer/TwoColumnsContainer/LeftListContainer/MoreOptions/OC';
import RightContentContainer from '../../../Common/MainContainer/TwoColumnsContainer/RightContentContainer';
import { IconButton } from '@material-ui/core';
import { useApolloClient, useQuery } from '@apollo/client';
import { GET_LOCAL_HIDE_PRESENTATION } from '../Actualite/ActualiteView/ActualiteViewBody/AttachedFiles/AttachedFiles';
import { resetSearchFilters } from '../../../Common/withSearch/withSearch';
import { SideMenuButtonInterface } from '../../../Common/SideMenuButton/SideMenuButton';
import { ME_me } from '../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../services/LocalStorage';
import { OC_CREATE_CODE } from '../../../../Constant/authorization';

export interface OCInterface {
  selectedOC: any;
}

export interface ArticleOCInterface extends ArticleType {
  quantite: number;
}

export interface ArticleOCArray {
  articleOcArray: ArticleOCInterface[];
}

// A Fixer
export const GET_SELECTED_OC = gql`
  query SELECTED_OC {
    selectedOC @client {
      id
    }
  }
`;

export const GET_ARTICLE_OC_ARRAY = gql`
  {
    articleOcArray @client {
      id
      quantite
    }
  }
`;

interface OperationProps {
  listResult: any;
  handleScroll: any;
  resetFilters: () => void;
  match: {
    params: { idOperation: string | undefined; take: string | undefined };
  };
}
const OperationCommerciale: FC<OperationProps & RouteComponentProps> = ({
  listResult,
  handleScroll,
  match,
  resetFilters,
}) => {
  const user: ME_me = getUser();
  const userCodesTraitements: string[] = (user && (user.codeTraitements as any)) || [];

  const addOcButton: SideMenuButtonInterface = {
    text: 'Créer une opération',
    url: '/create/operations-commerciales',
    color: 'pink',
    icon: <AddIcon />,
    authorized: [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT],
    userIsAuthorized: userCodesTraitements.includes(OC_CREATE_CODE),
  };

  const client = useApolloClient();
  const hidePresentationResult = useQuery(GET_LOCAL_HIDE_PRESENTATION);
  const hidePresentationValue =
    (hidePresentationResult &&
      hidePresentationResult.data &&
      hidePresentationResult.data.hidePresentation) ||
    false;
  const hidePresentation = () => {
      // TODO: Migration
    /*(client as any).writeData({
      data: {
        hidePresentation: !hidePresentationValue,
      },
    });*/
  };

  const listItemFields = {
    url: '/operations-commerciales',
    type: 'item.name',
    title: 'libelle',
    infoList: [
      { key: 'Canal', value: 'commandeCanal.libelle' },
      { key: 'Labo', value: 'laboratoire.nomLabo' },
      { key: 'CA Global', value: 'CATotal', append: '€' },
      { key: 'CA Moyenne', value: 'CAMoyenParPharmacie', append: '€' },
      { key: 'Créée le', value: 'dateCreation', dateFormat: 'L' },
    ],
    dateDebutFin: true,
    iconList: [
      { icon: <Visibility />, value: 'pharmacieVisitors', length: true, position: 'start' },
      {
        icon: (
          <IconButton onClick={hidePresentation}>
            <ThumbUp />
          </IconButton>
        ),
        value: 'userSmyleys.total',
      },
      {
        icon: (
          <IconButton onClick={hidePresentation}>
            <ChatBubble />
          </IconButton>
        ),
        iconButton: true,
        value: 'comments.total',
        position: 'flex-end',
      },
      {
        icon: <ShoppingCart />,
        value: 'totalCommandePassee',
        position: 'start',
      },
      {
        icon: <ViewComfy />,
        firstValue: 'nombreProduitsCommandes',
        secondValue: 'operationArticles.total',
        position: 'flex-end',
      },
    ],
  };

  const noContentValues = {
    list: {
      title: 'Aucune opérations commerciales dans la liste',
      subtitle: "Crées en une en cliquant sur le bouton d'en haut.",
    },
    content: {
      title: 'Aperçu détaillé de vos opérations commerciales',
      subtitle:
        "Choisissez une opération commerciale dans la liste de gauche pour l'afficher en détails dans cette partie.",
    },
  };

  useEffect(() => {
    return () => {
      resetSearchFilters(client);
    };
  }, []);

  return (
    <MainContainer
      moreOptionsItem={<MoreOptions listResult={listResult} />}
      noContentValues={noContentValues}
      layout={LayoutType.TwoColumns}
      listResult={listResult}
      handleScroll={handleScroll}
      listItemFields={listItemFields}
      rightContentChildren={<RightContentContainer listResult={listResult} />}
      listButton={addOcButton}
    />
  );
};

export default withRouter(OperationCommerciale);
