import { Theme, lighten } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: { flexWrap: 'nowrap', height: '100%' },
    /* FONT STYLE */
    MontserratRegular: {
      fontFamily: 'Montserrat',
      fontWeight: 400,
    },
    MontserratBold: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
    },
    RobotoRegular: {
      fontFamily: 'Roboto',
      fontWeight: 'normal',
    },
    leftOperation: {
      minWidth: 280,
      borderRight: '2px solid #E5E5E5',
      height: 'calc(100vh - 86px)',
      overflowY: 'auto',
    },
    seenBgColor: {
      backgroundColor: '#ffffff',
    },
    activeBgColorBlue: {
      backgroundColor: '#cbf3f5',
    },
    activeBgColorRed: {
      background:
        'linear-gradient(239deg, rgb(227, 71, 65) 0%, rgb(227, 65, 104) 100%) transparent',
      color: '#fff',
      '& p': {
        color: '#ffffff!important',
      },
    },
    rightOperation: {
      height: 'calc(100vh - 86px)',
      overflow: 'hidden',
    },
    annonceTime: {
      textAlign: 'right',
      color: '#343434',
      width: 127,
      fontSize: '0.75rem',
    },
    textList: {
      fontSize: '0.75rem',
    },
    flexColumn: {
      display: 'flex',
      flexDirection: 'column',
    },
    annonceMessage: {
      width: '100%',
      marginBottom: 0,
      marginTop: 0,
      '& span': {
        fontSize: '0.875rem',
        fontWeight: 600,
      },
    },
    padding24: {
      paddingLeft: 24,
    },
    offrePromotionnel: {
      height: 244,
      overflow: 'hidden',
      position: 'relative',
      '&:before': {
        backgroundColor: 'rgba(0, 0, 0, 0.20)',
        content: '""',
        position: 'absolute',
        height: '100%',
        width: '100%',
        zIndex: 1,
      },
    },
    bgOffrePromo: {
      width: '100%',
      height: '100%',
      objectFit: 'cover',
    },
    contentOffrePromo: {
      position: 'absolute',
      top: 0,
      right: 0,
      left: 0,
      margin: '0 auto',
      zIndex: 6,
      padding: '24px 48px',
      height: '100%',
      alignItems: 'center',
      flexWrap: 'nowrap',
      '@media (max-width: 1200px)': {
        flexWrap: 'wrap',
        justifyContent: 'center',
      },
    },
    leftOffrePromo: {
      textAlign: 'center',
      '@media (max-width: 1200px)': {
        maxWidth: '100%',
        flexBasis: '100%',
      },
    },
    rightOffrePromo: {
      textAlign: 'right',
      minWidth: 230,
    },
    underlined: {
      textDecoration: 'underline',
    },
    btnPasserCommande: {
      marginRight: 16,
    },
    textOffrePromo: {
      color: '#F11957',
      textAlign: 'center',
      fontSize: '1.375rem',
      textTransform: 'uppercase',
      padding: '0 32px',
      '@media (max-width: 1300px)': {
        padding: '0 24px 0 0 ',
        fontSize: '1.25rem',
      },
      '@media (max-width: 1200px)': {
        padding: 0,
      },
      '@media (max-width: 768px)': {
        fontSize: '1rem',
      },
    },
  }),
);

export default useStyles;
