import React, { FC, useEffect, useState } from 'react';
import useStyles from '../styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import { ArticleOCArray, GET_ARTICLE_OC_ARRAY } from '../OperationCommerciale';
import CustomButton from '../../../../Common/CustomButton';
import { ArrowForward } from '@material-ui/icons';

const ButtonGoToCommand: FC<RouteComponentProps<any, any, any>> = ({ history }) => {
  const classes = useStyles({});

  // Get (selected) article in OC from apollo local state
  const getArticleOcArray = useQuery<ArticleOCArray>(GET_ARTICLE_OC_ARRAY);
  const [disableBtn, setDisableBtn] = useState<boolean>(true);
  const idPublicite =
    history && history.location && history.location.state && history.location.state.idPublicite;

  // Passer à la commande
  const toOrder = () => {
    history.push('/recapitulatif/operation-commerciale', { idPublicite });
  };

  useEffect(() => {
    if (
      getArticleOcArray &&
      getArticleOcArray.data &&
      getArticleOcArray.data.articleOcArray &&
      getArticleOcArray.data.articleOcArray.length > 0
    ) {
      setDisableBtn(false);
    } else {
      setDisableBtn(true);
    }
  }, [getArticleOcArray]);

  return (
    <CustomButton
      endIcon={<ArrowForward />}
      children="Passer commande"
      className={classes.btnPasserCommande}
      color="secondary"
      onClick={toOrder}
      disabled={disableBtn}
    />
  );
};

export default withRouter(ButtonGoToCommand);
