import { useApolloClient, useMutation } from '@apollo/client';
import { Fab } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { Add } from '@material-ui/icons';
import DeleteIcon from '@material-ui/icons/Delete';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { ApolloQueryResult } from '@apollo/client';
import { differenceBy } from 'lodash';
import React, { Dispatch, FC, Fragment, SetStateAction } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import {
  MESSAGE_DELETED_URL,
  MESSAGE_NEW_URL,
  MESSAGE_SENDED_URL,
  MESSAGE_URL,
} from '../../../../../../Constant/url';
import { DO_DELETE_MESSAGES, GET_MESSAGERIES } from '../../../../../../graphql/Messagerie';
import {
  DELETE_MESSAGES,
  DELETE_MESSAGESVariables,
} from '../../../../../../graphql/Messagerie/types/DELETE_MESSAGES';
import {
  MESSAGERIES,
  MESSAGERIESVariables,
  MESSAGERIES_messageries_data,
} from '../../../../../../graphql/Messagerie/types/MESSAGERIES';
import { TOTAL_MESSAGERIE_NON_LUS } from '../../../../../../graphql/Messagerie/types/TOTAL_MESSAGERIE_NON_LUS';
import { MessagerieType } from '../../../../../../types/graphql-global-types';
import { isMobile } from '../../../../../../utils/Helpers';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import Backdrop from '../../../../../Common/Backdrop';
import { ConfirmDeleteDialog } from '../../../../../Common/ConfirmDialog';
import Filter from './Filter';
import Message from './Message';
import useStyles from './styles';

export interface ListeMessageProps {
  messageList: any[];
  message: MESSAGERIES_messageries_data | null;
  setMessage: Dispatch<SetStateAction<MESSAGERIES_messageries_data | null>>;
  currentUserId: string;
  queryVariables: MESSAGERIESVariables;
  setQueryVariables: Dispatch<SetStateAction<MESSAGERIESVariables>>;
  selectedMsg: any[];
  setSelectedMsg: Dispatch<SetStateAction<any[]>>;
  refetchUnreadMsg: (
    variables?: Record<string, any> | undefined,
  ) => Promise<ApolloQueryResult<TOTAL_MESSAGERIE_NON_LUS>>;
}

export const ListeMessage: FC<ListeMessageProps & RouteComponentProps> = ({
  messageList,
  location: { pathname },
  message,
  setMessage,
  currentUserId,
  queryVariables,
  selectedMsg,
  setSelectedMsg,
  setQueryVariables,
  refetchUnreadMsg,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();

  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef<HTMLButtonElement>(null);

  const [openDialog, setOpenDialog] = React.useState<boolean>(false);

  const isOnNew = pathname === MESSAGE_NEW_URL;
  const isOnSended = pathname === MESSAGE_SENDED_URL || pathname.includes(MESSAGE_SENDED_URL);
  const isOnDeleted = pathname === MESSAGE_DELETED_URL || pathname.includes(MESSAGE_DELETED_URL);

  const title =
    isOnNew || isOnSended
      ? 'Éléments envoyés'
      : isOnDeleted
        ? 'Éléments supprimés'
        : 'Boîte de réception';

  const handleToggle = () => {
    setOpen(prevOpen => !prevOpen);
  };

  const onClickDelete = () => {
    if (selectedMsg.length > 0) {
      setOpenDialog(true);
    }
  };

  /**
   * Mutation delete message
   */
  const [doDeleteMsg, { loading: deleteLoading }] = useMutation<
    DELETE_MESSAGES,
    DELETE_MESSAGESVariables
  >(DO_DELETE_MESSAGES, {
    update: (cache, { data }) => {
      if (data && data.deleteMessages) {
        const req = cache.readQuery<MESSAGERIES, MESSAGERIESVariables>({
          query: GET_MESSAGERIES,
          variables: queryVariables,
        });
        if (req && req.messageries && req.messageries.data) {
          const newData: any[] = differenceBy(req.messageries.data, data.deleteMessages, 'id');
          cache.writeQuery({
            query: GET_MESSAGERIES,
            data: { messageries: { ...req.messageries, ...{ data: newData } } },
            variables: queryVariables,
          });
        }
      }
    },
    onCompleted: data => {
      if (data && data.deleteMessages) {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Message(s) supprimé(s) avec succès',
        });
        setMessage(null);
        setSelectedMsg([]);
        if (refetchUnreadMsg) {
          refetchUnreadMsg();
        }
      }
    },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const onConfirmDelete = () => {
    if (selectedMsg.length > 0) {
      setOpenDialog(false);
      const ids = selectedMsg.map(i => i && i.id);

      const type: MessagerieType = pathname.includes('inbox') ? MessagerieType.R : MessagerieType.E;
      doDeleteMsg({
        variables: { ids, typeMessagerie: type, permanent: isOnDeleted ? true : false },
      });
    }
  };

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current!.focus();
    }

    prevOpen.current = open;
  }, [open]);

  return (
    <Box className={classes.root}>
      {deleteLoading && <Backdrop value="Suppression en cours..." />}
      <Box className={classes.appBar}>
        <Typography className={classes.bold}>{title}</Typography>
        <Box display="flex">
          <Box display="flex">
            <Button
              className={classes.expendMore}
              ref={anchorRef}
              aria-controls={open ? 'menu-list-grow' : undefined}
              aria-haspopup="true"
              onClick={handleToggle}
              endIcon={<ExpandMoreIcon />}
            >
              Trier par
            </Button>
            <Filter
              open={open}
              setOpen={setOpen}
              setQueryVariables={setQueryVariables}
              anchorRef={anchorRef}
            />
          </Box>

          <Fragment>
            <Divider orientation="vertical" flexItem={true} />
            <IconButton aria-label="delete" className={classes.margin} onClick={onClickDelete}>
              <DeleteIcon fontSize="small" />
            </IconButton>
          </Fragment>
        </Box>
      </Box>
      <Message
        messageList={messageList}
        message={message}
        setMessage={setMessage}
        currentUserId={currentUserId}
        selectedMsg={selectedMsg}
        setSelectedMsg={setSelectedMsg}
        queryVariables={queryVariables}
        setQueryVariables={setQueryVariables}
        refetchUnreadMsg={refetchUnreadMsg}
      />
      <ConfirmDeleteDialog
        open={openDialog}
        setOpen={setOpenDialog}
        onClickConfirm={onConfirmDelete}
        content={
          isOnDeleted
            ? 'Voulez-vous supprimer ce(s) message(s) définitivement?'
            : 'Êtes-vous sûre de vouloir supprimer ces messages ?'
        }
      />
    </Box>
  );
};

export default withRouter(ListeMessage);
