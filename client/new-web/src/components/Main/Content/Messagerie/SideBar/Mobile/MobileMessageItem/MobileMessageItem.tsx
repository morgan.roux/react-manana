import React, { FC } from 'react';
import useStyles from './styles';
import { RouteComponentProps, withRouter } from 'react-router';
import { stringToAvatar } from '../../../../../../../utils/Helpers';
import { Avatar, Box, ListItem, ListItemAvatar, Typography } from '@material-ui/core';
import moment from 'moment';
import { nl2br } from '../../../../../../../services/Html';

interface MobileMessageItemProps {
  message: any;
  onClick: (message: any) => () => void;
}

export const MobileMessageItem: FC<MobileMessageItemProps & RouteComponentProps> = ({
  history,
  message,
  onClick,
}) => {
  const classes = useStyles({});

  // const nl2br = (str: string | null): string | null => {
  //   return str ? str.replace(new RegExp('\n', 'g'), '<br/>') : str;
  // };

  const emetteur = message && message.userEmetteur;

  const recepteur =
    message &&
    message.recepteurs &&
    emetteur &&
    message.recepteurs.find(
      recepteur => recepteur && recepteur.userCreation && recepteur.userCreation.id === emetteur.id,
    );

  const handleMessageClick = () => {
    onClick(message)();
  };

  const messageText = message && message.message && message.message.replaceAll(/<[^>]+>/g, '');

  return (
    <ListItem key={`${message.id}-item`} button={true} onClick={handleMessageClick} id={message.id}>
      <ListItemAvatar>
        <Avatar className={classes.avatar}>
          {stringToAvatar(
            (recepteur && recepteur.userRecepteur && recepteur.userRecepteur.userName) || '',
          )}
        </Avatar>
      </ListItemAvatar>
      <Box className={classes.messageInfos} maxWidth="80%">
        <Box className={classes.spaceBetween}>
          <Typography className={classes.receivers}>
            À :{' '}
            {message.recepteurs.map(recepteur => (
              <span>{recepteur.userRecepteur.userName},</span>
            ))}
          </Typography>
          <Typography className={classes.dateCreation}>
            {moment(message.dateCreation).format('DD MMM HH:mm')}
          </Typography>
        </Box>
        <Typography className={classes.objet}>{message.objet}</Typography>
        <Typography className={classes.message}>{messageText}</Typography>
      </Box>
    </ListItem>
  );
};

export default withRouter(MobileMessageItem);
