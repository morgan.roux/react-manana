import React, { ChangeEvent, FC, Fragment, ReactNode } from 'react';
import { Autocomplete as MUIAutocomplete } from '@material-ui/lab';
import { TextField, CircularProgress, Typography } from '@material-ui/core';
import { ArrowDropDown } from '@material-ui/icons';
import { nl2br } from '../../../../../../services/Html';

export interface AutocompleteProps {
  name: string;
  options: any[];
  optionLabel: string;
  value: any;
  multiple?: boolean;
  startAdornment?: ReactNode;
  endAdornment?: ReactNode;
  loading?: boolean;
  popupIcon?: ReactNode;
  variant?: 'filled' | 'outlined' | 'standard';
  label?: string;
  disabled?: boolean;
  style?: any;
  placeholder?: string;
  noOptionsText?: string;
  required?: boolean;
  className?: string;
  handleChange: (name: string) => (event: ChangeEvent<any>, value: any) => void;
  handleInputChange?: (name: string) => (event: ChangeEvent<any>, value: string) => void;
  handleOpenAutocomplete?: () => void;
  popupIndicatorOpen?: any;
}

const Autocomplete: FC<AutocompleteProps> = ({
  multiple = false,
  name,
  handleChange,
  handleInputChange,
  options,
  optionLabel,
  startAdornment,
  endAdornment,
  loading = false,
  handleOpenAutocomplete,
  popupIcon = <ArrowDropDown />,
  value,
  variant = 'standard',
  label = '',
  disabled = false,
  style = null,
  placeholder,
  noOptionsText,
  required,
  className,
  popupIndicatorOpen,
}) => {
  return (
    <MUIAutocomplete
      className={className}
      style={style ? style : {}}
      disabled={disabled}
      multiple={multiple}
      value={value}
      onChange={handleChange(name)}
      onInputChange={handleInputChange ? handleInputChange(name) : undefined}
      options={options || []}
      // tslint:disable-next-line: jsx-no-lambda
      getOptionLabel={(opt: any) => {
        return (opt && opt[optionLabel]) || '';
      }}
      // tslint:disable-next-line: jsx-no-lambda
      renderOption={option => (
        <React.Fragment>
          <Typography
            dangerouslySetInnerHTML={
              { __html: nl2br((option && option[optionLabel]) || '') } as any
            }
          />
        </React.Fragment>
      )}
      // tslint:disable-next-line: jsx-no-lambda
      renderInput={params => (
        <TextField
          {...params}
          variant={variant}
          label={label}
          required={required}
          InputProps={{
            ...params.InputProps,
            startAdornment: (
              <Fragment>
                {startAdornment}
                {params.InputProps.startAdornment}
              </Fragment>
            ),
            endAdornment: (
              <Fragment>
                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                {endAdornment}
                {params.InputProps.endAdornment}
              </Fragment>
            ),
          }}
        />
      )}
      popupIcon={popupIcon}
      onOpen={handleOpenAutocomplete}
      loading={loading}
      loadingText="Chargement..."
      placeholder={placeholder ? placeholder : ''}
      noOptionsText={noOptionsText ? noOptionsText : 'Aucune option'}
      classes={{ popupIndicatorOpen }}
    />
  );
};

export default Autocomplete;
