import React from 'react';
import LensIcon from '@material-ui/icons/Lens';

export const messagerieIcon = (code: string) => {
  switch (code) {
    case 'MY_PHARMACIE':
      return <LensIcon color="primary" />;
    case 'OTHER_PHARMACIE':
      return <LensIcon color="action" />;
    case 'MY_REGION':
      return <LensIcon color="disabled" />;
    case 'MY_GROUPEMENT':
      return <LensIcon color="error" />;
    case 'LABORATOIRE':
      return <LensIcon color="secondary" />;
    case 'PARTENAIRE_SERVICE':
      return <LensIcon style={{ color: 'green' }} />;
    default:
  }
};
