import { useApolloClient, useLazyQuery } from '@apollo/client';
import {
  PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK,
  PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACKVariables,
} from '../../../../../../graphql/Pilotage/types/PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK';
import { GET_PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK } from '../../../../../../graphql/Pilotage';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';

const useQueryFeedbackTable = () => {
  const client = useApolloClient();

  const [queryFeedbackTable, queryFeedbackTableResult] = useLazyQuery<
    PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK,
    PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACKVariables
  >(GET_PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK, {
    fetchPolicy: 'network-only',
    onCompleted: data => { },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        });
      });
    },
  });
  return { queryFeedbackTable, queryFeedbackTableResult };
};

export default useQueryFeedbackTable;
