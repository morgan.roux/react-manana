import { CustomTableColumn } from './CustomTable';
import moment from 'moment';
import { roundNumber } from '../../../../../../utils/Helpers';

export const PHARMA_COLUMNS: CustomTableColumn[] = [
  {
    key: 'pharmacie.nom',
    label: 'Nom Pharmacie',
    disablePadding: true,
    numeric: false,
    renderer: (row: any) => {
      return (row.pharmacie && row.pharmacie.nom) || '-';
    },
  },
  {
    key: 'pharmacie.cip',
    label: 'CIP',
    disablePadding: false,
    numeric: true,
    renderer: (row: any) => {
      return (row.pharmacie && row.pharmacie.cip) || '-';
    },
  },
  {
    key: 'pharmacie.cp',
    label: 'Code postal',
    disablePadding: false,
    numeric: true,
    renderer: (row: any) => {
      return (row.pharmacie && row.pharmacie.cp) || '-';
    },
  },
  {
    key: 'pharmacie.ville',
    label: 'Ville',
    disablePadding: false,
    numeric: false,
    renderer: (row: any) => {
      return (row.pharmacie && row.pharmacie.ville) || '-';
    },
  },
];

export const OP_COLUMNS: CustomTableColumn[] = [
  {
    key: 'operation.libelle',
    disablePadding: true,
    numeric: false,
    label: 'Libelle',
    renderer: (value: any) => {
      return (value && value.operation && value.operation.libelle) || '-';
    },
  },
  {
    key: 'operation.commandeCanal.libelle',
    disablePadding: true,
    numeric: false,
    label: 'Canal',
    renderer: (value: any) => {
      return (
        (value &&
          value.operation &&
          value.operation.commandeCanal &&
          value.operation.commandeCanal.libelle) ||
        '-'
      );
    },
  },
  {
    key: 'operation.dateDebut',
    disablePadding: false,
    numeric: false,
    label: 'Date début',
    renderer: (value: any) => {
      return value && value.operation && value.operation.dateDebut
        ? moment(value.operation.dateDebut).format('DD/MM/YYYY')
        : '-';
    },
  },
  {
    key: 'operation.dateFin',
    label: 'Date fin',
    disablePadding: false,
    numeric: false,
    renderer: (value: any) => {
      return value && value.operation && value.operation.dateFin
        ? moment(value.operation.dateFin).format('DD/MM/YYYY')
        : '-';
    },
  },
];

export const BUSINESS_COLUMNS: CustomTableColumn[] = [
  {
    key: 'caTotal',
    label: 'CA €',
    disablePadding: false,
    numeric: true,
    renderer: (row: any) => {
      return `${roundNumber(row.caTotal || 0)} €`;
    },
  },
  {
    key: 'nbCommandes',
    label: 'Nombre de Commandes',
    disablePadding: false,
    numeric: true,
  },
  // {
  //   key: 'nbPharmacies',
  //   label: 'Nombre de Pharmacie',
  //   disablePadding: false,
  //   numeric: true,
  // },
  {
    key: 'nbProduits',
    label: 'Nombre de produits',
    disablePadding: false,
    numeric: true,
  },
  {
    key: 'nbLignes',
    label: 'Nbre de lignes',
    disablePadding: false,
    numeric: true,
  },
  {
    key: 'caMoyenParPharmacie',
    label: 'CA moyen par Pharmacie',
    disablePadding: false,
    numeric: true,
    renderer: (row: any) => {
      return `${roundNumber(row.caMoyenParPharmacie || 0)} €`;
    },
  },
  {
    key: 'totalRemise',
    label: 'Total remise en €',
    disablePadding: false,
    numeric: true,
    renderer: (row: any) => {
      return `${roundNumber(row.totalRemise || 0)} €`;
    },
  },
  {
    key: 'globaliteRemise',
    label: 'Remise en %',
    disablePadding: false,
    numeric: true,
    renderer: (row: any) => {
      return `${roundNumber(row.globaliteRemise || 0)} %`;
    },
  },
];

export const FEEDBACK_COLUMNS: CustomTableColumn[] = [
  {
    key: 'cibles',
    label: 'Cible',
    disablePadding: false,
    numeric: true,
  },
  {
    key: 'lu',
    label: 'Lu',
    disablePadding: false,
    numeric: true,
  },
  {
    key: 'adore',
    label: 'Adore',
    disablePadding: false,
    numeric: true,
  },
  {
    key: 'content',
    label: 'Content',
    disablePadding: false,
    numeric: true,
  },
  {
    key: 'aime',
    label: 'Aime',
    disablePadding: false,
    numeric: true,
  },
  {
    key: 'mecontent',
    label: 'Mécontent',
    disablePadding: false,
    numeric: true,
  },
  {
    key: 'deteste',
    label: 'Déteste',
    disablePadding: false,
    numeric: true,
  },
  {
    key: 'partages',
    label: 'Partages',
    disablePadding: false,
    numeric: true,
  },
  {
    key: 'recommandations',
    label: 'Recommandations',
    disablePadding: false,
    numeric: true,
  },
  {
    key: 'commentaires',
    label: 'Commentaires',
    disablePadding: false,
    numeric: true,
  },
];

export const ITEM_COLUMNS: CustomTableColumn[] = [
  {
    key: 'itemAssocie.libelle',
    disablePadding: true,
    numeric: false,
    label: 'Libelle',
    renderer: (value: any) => {
      return (value && value.itemAssocie && value.itemAssocie.libelle) || '-';
    },
  },
  {
    key: 'itemAssocie.dateDebut',
    disablePadding: false,
    numeric: false,
    label: 'Date début',
    renderer: (value: any) => {
      return value && value.itemAssocie && value.itemAssocie.dateDebut
        ? moment(value.itemAssocie.dateDebut).format('DD/MM/YYYY')
        : '-';
    },
  },
  {
    key: 'itemAssocie.dateFin',
    label: 'Date fin',
    disablePadding: false,
    numeric: false,
    renderer: (value: any) => {
      return value && value.itemAssocie && value.itemAssocie.dateFin
        ? moment(value.itemAssocie.dateFin).format('DD/MM/YYYY')
        : '-';
    },
  },
  {
    key: 'itemAssocie.item.name',
    label: 'Item',
    disablePadding: false,
    numeric: false,
    renderer: (value: any) => {
      return (
        (value && value.itemAssocie && value.itemAssocie.item && value.itemAssocie.item.name) || '-'
      );
    },
  },
];

export const BUSINESS_PHARMA_COLUMNS: CustomTableColumn[] = [
  ...PHARMA_COLUMNS,
  ...BUSINESS_COLUMNS,
];

export const BUSINESS_OP_COLUMNS: CustomTableColumn[] = [...OP_COLUMNS, ...BUSINESS_COLUMNS];

export const FEEDBACK_PHARMA_COLUMNS: CustomTableColumn[] = [
  ...PHARMA_COLUMNS,
  ...FEEDBACK_COLUMNS,
];

export const FEEDBACK_ITEM_COLUMNS: CustomTableColumn[] = [...ITEM_COLUMNS, ...FEEDBACK_COLUMNS];
