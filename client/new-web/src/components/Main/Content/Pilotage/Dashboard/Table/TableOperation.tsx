import React, { FC, useState, MouseEvent } from 'react';
// import { TypePilotage } from '../Dashboard';
import { PilotageTableProps } from './TableGlobal';
import CustomTable from './CustomTable';
import { BUSINESS_OP_COLUMNS } from './coumns';
import { CustomTablePaginationProps } from './CustomTablePagination';
import { IconButton } from '@material-ui/core';
import { Comment } from '@material-ui/icons';
import { CustomModal } from '../../../../../Common/CustomModal';
import ModalComment from '../../../../../Common/ModalComment';

interface TableOperationProps {
  onClickTableRow: (row: any) => void;
  idsPharmacies: string[];
}

const TableOperation: FC<PilotageTableProps & TableOperationProps & CustomTablePaginationProps> = ({
  // typePilotage,
  data,
  onClickTableRow,
  rowCount,
  total,
  page,
  rowsPerPage,
  take,
  skip,
  setTake,
  setSkip,
  setPage,
  setRowsPerPage,
  order,
  setOrder,
  orderBy,
  setOrderBy,
  codeItem,
  idsPharmacies,
}) => {
  // const { BUSINESS } = TypePilotage;
  const columns = BUSINESS_OP_COLUMNS;
  const [openComments, setOpenComments] = useState<boolean>(false);
  const [idSource, setIdSource] = useState<string>('');

  const handleOpenComments = (row: any) => (event: MouseEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    setOpenComments(true);
    if (row && row.operation && row.operation.id) {
      setIdSource(row.operation.id);
    }
  };

  const voirDetailColumn: any = {
    key: '',
    label: '',
    disablePadding: false,
    numeric: true,
    renderer: (row: any) => (
      <IconButton color="inherit" title="Voir commentaires" onClick={handleOpenComments(row)}>
        <Comment />
      </IconButton>
    ),
  };

  return (
    <>
      <CustomTable
        // columns={[...columns, voirDetailColumn]}
        columns={columns}
        showToolbar={false}
        selectable={false}
        data={data}
        rowCount={rowCount}
        onClickRow={onClickTableRow}
        total={total}
        page={page}
        rowsPerPage={rowsPerPage}
        take={take}
        skip={skip}
        setTake={setTake}
        setSkip={setSkip}
        setPage={setPage}
        setRowsPerPage={setRowsPerPage}
        orderByFromProps={orderBy}
        setOrderByFromProps={setOrderBy}
        orderFromProps={order}
        setOrderFromProps={setOrder}
      />
      <CustomModal
        open={openComments}
        setOpen={setOpenComments}
        title="Commentaires"
        closeIcon={true}
        withBtnsActions={false}
        headerWithBgColor={true}
        children={
          <ModalComment codeItem={codeItem} idSource={idSource} idsPharmacies={idsPharmacies} />
        }
      />
    </>
  );
};

export default TableOperation;
