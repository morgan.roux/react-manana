import { Theme, createStyles, makeStyles, lighten, darken } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    pilotageDashboardRoot: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      minHeight: 'fit-content',
      position: 'relative',
      height: 'auto',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
    },
    pilotageHead: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      height: 70,
      background: lighten(theme.palette.primary.main, 0.1),
      opacity: 1,
      color: theme.palette.common.white,
      padding: '0px 30px',
      position: 'sticky',
      top: 0,
      zIndex: 999,
    },
    pilotageHeadHideBtns: {
      justifyContent: 'center',
    },
    pilotageHeadTitle: {
      letterSpacing: 0,
      opacity: 1,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 20,
    },
    btnBack: {
      boxShadow: 'none',
      textTransform: 'none',
    },
    viewTypeContainer: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      margin: '35px 0px',
      '& button': {
        minWidth: 200,
        margin: '0px 10px',
      },
      '@media (max-width: 681px)': {
        display: 'grid',
        gridTemplateColumns: 'repeat(2, 1fr)',
        '& button:not(:nth-last-child(1))': {
          marginBottom: 10,
        },
      },
      '@media (max-width: 477px)': {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        '& button': {
          width: 280,
          minWidth: 262,
        },
      },
    },
    btnOutlined: {
      borderColor: '#9E9E9E',
    },
    btnView: {
      fontWeight: 'normal',
      fontSize: 14,
      textTransform: 'none',
    },
    headBtnsContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
    },
    mainCntainer: {
      width: '100%',
      marginBottom: 35,
      padding: '0px 20px',
      display: 'flex',
      alignItems: 'flex-start',
      justifyContent: 'space-between',
      '@media (max-width: 1880px)': {
        '& > div': {
          margin: 20,
        },
      },
      '@media (max-width: 1670px)': {
        flexWrap: 'wrap',
        justifyContent: 'center',
      },
    },
    indicatorCard: {
      boxShadow: '0px 10px 12px #00000029 !important',
      borderRadius: 5,
      height: 550,
      width: 280,
      minWidth: 262,
      background: theme.palette.primary.main,
      color: theme.palette.common.white,
    },
    cardTitle: {
      paddingBottom: 0,
      '& *': {
        fontSize: 25,
        fontWeight: 'bold',
      },
    },
    cardContent: {
      position: 'relative',
      '& .MuiListItemIcon-root': {
        minWidth: 30,
      },
      '& .MuiListItemText-root': {
        '& > span': {
          fontSize: 14,
          fontWeight: 'normal',
        },
      },
    },
    evolutionValue: {
      position: 'absolute',
      top: 130,
      left: 480,
      zIndex: 1,
      display: 'flex',
      alignItems: 'center',
      background: '#fff',
      minWidth: 'fit-content',
      '& *': {
        fontSize: 25,
        fontWeight: 'bold',
      },
      '@media (min-width: 1890px)': {
        '& *': {
          fontSize: 35,
        },
      },
    },
    listItem: {
      borderRadius: 5,
      '&:hover': {
        backgroundColor: lighten(theme.palette.secondary.main, 0.2),
        '& > *': {
          color: theme.palette.common.white,
        },
      },
    },
    listItemActive: {
      backgroundColor: lighten(theme.palette.secondary.main, 0.2),
      '& > *': {
        color: theme.palette.common.white,
      },
    },
    chartCard: {
      boxShadow: 'none !important',
      borderRadius: 5,
      border: '1px solid #E0E0E0',
      minWidth: 627,
      width: 700,
      height: 550,
      '@media (max-width: 1828px)': {
        width: 'fit-content',
      },
    },
    chartCardTitle: {
      marginBottom: -40,
      '& *': {
        textAlign: 'center',
        fontSize: 20,
        color: theme.palette.common.black,
      },
      '& .MuiCardHeader-title': {
        fontWeight: 'normal',
      },
      '& .MuiCardHeader-subheader': {
        fontWeight: 'bold',
      },
    },
    statRoot: {
      width: 'auto',
      display: 'flex',
      flexDirection: 'column',
      // height: 550,
      height: 'fit-content',
      '@media (max-width: 855px)': {
        height: 'fit-content',
      },
    },
    statTitle: {
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize: 20,
    },
    statItem: {
      width: 175,
      height: 130,
      padding: '0px 5px',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 5,
      background: theme.palette.primary.main,
      color: theme.palette.common.white,
      margin: '10px 0px 0px 10px',
      textAlign: 'center',
      '@media screen and (min-width: 1671px) and (max-width: 1798px)': {
        width: 145,
      },
      '@media (max-width: 474px)': {
        width: 150,
        margin: '10px 0px 0px 10px',
      },
    },
    statItemOnFeedback: {
      height: '90% !important',
    },
    statItemLabel: {
      fontSize: 14,
      fontWeight: 'normal',
      margin: '8px 0px',
    },
    statItemValue: {
      fontSize: 22,
      fontWeight: 'bold',
    },
    chiffreContainer: {
      display: 'grid',
      gridTemplateColumns: 'repeat(4, 1fr)',
      maxWidth: 800,
      height: '100%',
      '@media (max-width: 855px)': {
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'flex-start',
      },
      '@media (max-width: 474px)': {
        display: 'grid',
        gridTemplateColumns: 'repeat(2, 1fr)',
      },
    },
    noJauge: {
      height: '50%',
    },
    gaugeContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      marginTop: 40,
    },
    gaugeChatsContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
    },
    gaugeTitle: {
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize: 20,
      marginBottom: -20,
    },
    tableContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: '0px 20px 20px',
    },
    tableTitle: {
      fontWeight: 'bold',
      fontSize: 20,
      marginBottom: 20,
    },
    icon: {
      width: 20,
      height: 20,
      margin: '4px 0px',
    },
    iconInStat: {
      width: 30,
      height: 30,
    },
    switchChartBtnsContainer: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-around',
      margin: '0px auto',
      background: theme.palette.secondary.main,
      padding: '8px 0px',
      '& button': {
        textTransform: 'none',
        borderRadius: 18,
      },
    },
    chartTypesBtns: {
      '& > button': {
        minWidth: 147,
      },
      '& > button:nth-child(1)': {
        marginRight: 15,
      },
    },
    semestreBtns: {
      '& > button:nth-child(1)': {
        marginRight: 15,
      },
    },
    btnText: {
      color: theme.palette.common.white,
      '&:hover': {
        color: theme.palette.common.white,
        background: lighten(theme.palette.secondary.main, 0.2),
      },
    },
    btnTextActive: {
      background: lighten(theme.palette.secondary.main, 0.2),
      color: theme.palette.common.white,
      '&:hover': {
        background: lighten(theme.palette.secondary.main, 0.2),
      },
    },
  }),
);

export default useStyles;
