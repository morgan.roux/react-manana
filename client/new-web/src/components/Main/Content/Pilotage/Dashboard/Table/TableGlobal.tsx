import React, { FC, Dispatch, SetStateAction } from 'react';
import { TypePilotage } from '../Dashboard';
import CustomTable, { Order } from './CustomTable';
import { FEEDBACK_PHARMA_COLUMNS, BUSINESS_PHARMA_COLUMNS } from './coumns';
import { CustomTablePaginationProps } from './CustomTablePagination';

export interface PilotageTableProps {
  typePilotage: TypePilotage;
  data: any[];
  rowCount: number;
  orderBy: string;
  setOrderBy: Dispatch<SetStateAction<string | undefined>>;
  order: Order;
  setOrder: Dispatch<SetStateAction<Order | undefined>>;
  codeItem: string;
}

const TableGlobal: FC<PilotageTableProps & CustomTablePaginationProps> = ({
  typePilotage,
  data,
  rowCount,
  total,
  page,
  rowsPerPage,
  take,
  skip,
  setTake,
  setSkip,
  setPage,
  setRowsPerPage,
  order,
  setOrder,
  orderBy,
  setOrderBy,
}) => {
  const { BUSINESS } = TypePilotage;
  const columns = typePilotage === BUSINESS ? BUSINESS_PHARMA_COLUMNS : FEEDBACK_PHARMA_COLUMNS;

  return (
    <CustomTable
      columns={columns}
      showToolbar={false}
      selectable={false}
      data={data}
      rowCount={rowCount}
      total={total}
      page={page}
      rowsPerPage={rowsPerPage}
      take={take}
      skip={skip}
      setTake={setTake}
      setSkip={setSkip}
      setPage={setPage}
      setRowsPerPage={setRowsPerPage}
      orderByFromProps={orderBy}
      setOrderByFromProps={setOrderBy}
      orderFromProps={order}
      setOrderFromProps={setOrder}
    />
  );
};

export default TableGlobal;
