import React, { FC, useState, MouseEvent, useEffect } from 'react';
import { TypePilotage, formatOrderBy } from '../Dashboard';
import { PilotageTableProps } from './TableGlobal';
import CustomTable from './CustomTable';
import { FEEDBACK_PHARMA_COLUMNS, BUSINESS_PHARMA_COLUMNS } from './coumns';
import { CustomTablePaginationProps } from './CustomTablePagination';
import { CustomFullScreenModal } from '../../../../../Common/CustomModal';
import { IconButton } from '@material-ui/core';
import { Comment } from '@material-ui/icons';
import TableItem from './TableItem';
import { withRouter, RouteComponentProps } from 'react-router';
import useQueryBusinessTable from '../hooks/useQueryBusinessTable';
import useQueryFeedbackTable from '../hooks/useQueryFeedbackTable';
import { PilotageBusiness, PilotageFeedback } from '../../../../../../types/graphql-global-types';
import { setDataTable } from '../utils';
import TableOperation from './TableOperation';

interface TablePharmacieProps {
  onClickTableRow: (row: any) => void;
  pharmacieIds: string[];
  operationIds: string[];
  idItemAssocies: string[];
  isOnBusiness: boolean;
  isOnFeedback: boolean;
}

const TablePharmacie: FC<
  PilotageTableProps & TablePharmacieProps & CustomTablePaginationProps & RouteComponentProps
> = ({
  typePilotage,
  onClickTableRow,
  data,
  rowCount,
  total,
  page,
  rowsPerPage,
  take,
  skip,
  setTake,
  setSkip,
  setPage,
  setRowsPerPage,
  order,
  setOrder,
  orderBy,
  setOrderBy,
  codeItem,
  pharmacieIds,
  operationIds,
  idItemAssocies,
  isOnBusiness,
  isOnFeedback,
}) => {
  const { BUSINESS, FEEDBACK } = TypePilotage;
  // const [openComments, setOpenComments] = useState<boolean>(false);
  const [idsPharmacies, setIdsPharmacies] = useState<string[]>([]);
  const [openItemTable, setOpenItemTable] = useState<boolean>(false);

  const [state, setState] = useState<{
    tableData: any[];
    tableTotal: number;
  }>({
    tableData: [],
    tableTotal: 0,
  });

  const [newOrderBy, setNewOrderBy] = useState('libelle');

  /**
   * Query PHARMACIE_OPERATION_STATISTIQUE_BUSINESS [Tableau]
   */
  const { queryBusinessTable, queryBusinessTableResult } = useQueryBusinessTable();

  /**
   * Query GET_PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK [Tableau]
   */
  const { queryFeedbackTable, queryFeedbackTableResult } = useQueryFeedbackTable();

  // const handleOpenComments = (row: any) => (event: MouseEvent<any>) => {
  //   event.preventDefault();
  //   event.stopPropagation();
  //   setOpenComments(true);
  //   if (row && row.pharmacie && row.pharmacie.id) {
  //     setIdsPharmacies([row.pharmacie.id]);
  //   }
  // };

  const handleOpenItemTable = (row: any) => (event: MouseEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    setOpenItemTable(true);
    executeQuery();
    if (row && row.pharmacie && row.pharmacie.id) {
      setIdsPharmacies([row.pharmacie.id]);
    }
  };

  const executeQuery = () => {
    if (pharmacieIds.length > 0 && operationIds.length > 0) {
      if (isOnBusiness) {
        queryBusinessTable({
          variables: {
            pilotage: PilotageBusiness.OPERATION,
            idOperations: operationIds,
            idPharmacies: pharmacieIds,
            take,
            skip,
            orderBy: formatOrderBy(newOrderBy) as any,
            isAsc: order === 'asc' ? true : false,
          },
        });
      }
    }
    if (pharmacieIds.length > 0 && idItemAssocies.length > 0 && codeItem) {
      if (isOnFeedback) {
        queryFeedbackTable({
          variables: {
            codeItem,
            pilotage: PilotageFeedback.ITEM,
            idItemAssocies,
            idPharmacies: pharmacieIds,
            take,
            skip,
            orderBy: formatOrderBy(newOrderBy) as any,
            isAsc: order === 'asc' ? true : false,
          },
        });
      }
    }
  };

  const voirDetailColumn: any = {
    key: '',
    label: '',
    disablePadding: false,
    numeric: true,
    renderer: (row: any) => (
      <IconButton color="inherit" title="Voir commentaires" onClick={handleOpenItemTable(row)}>
        <Comment />
      </IconButton>
    ),
  };

  /**
   * SET TABLE DATA
   */
  useEffect(() => {
    setDataTable(queryBusinessTableResult, queryFeedbackTableResult, setState);
  }, [queryBusinessTableResult, queryFeedbackTableResult]);

  const columns = typePilotage === BUSINESS ? BUSINESS_PHARMA_COLUMNS : FEEDBACK_PHARMA_COLUMNS;
  return (
    <div>
      <CustomTable
        columns={isOnBusiness ? columns : [...columns, voirDetailColumn]}
        showToolbar={false}
        selectable={false}
        data={data}
        rowCount={rowCount}
        onClickRow={onClickTableRow}
        total={total}
        page={page}
        rowsPerPage={rowsPerPage}
        take={take}
        skip={skip}
        setTake={setTake}
        setSkip={setSkip}
        setPage={setPage}
        setRowsPerPage={setRowsPerPage}
        orderByFromProps={orderBy}
        setOrderByFromProps={setOrderBy}
        orderFromProps={order}
        setOrderFromProps={setOrder}
      />
      <CustomFullScreenModal
        title="Voir les commentaires"
        open={openItemTable}
        setOpen={setOpenItemTable}
        closeIcon={true}
        withBtnsActions={false}
        headerWithBgColor={true}
        // className={classes.historyRoot}
        fullScreen={true}
      >
        {typePilotage === FEEDBACK ? (
          <TableItem
            typePilotage={typePilotage}
            onClickTableRow={onClickTableRow}
            data={state.tableData}
            rowCount={state.tableData.length}
            total={state.tableTotal}
            skip={skip}
            take={take}
            page={page}
            rowsPerPage={rowsPerPage}
            setSkip={setSkip}
            setTake={setTake}
            setPage={setPage}
            setRowsPerPage={setRowsPerPage}
            order={order}
            setOrder={setOrder}
            orderBy={newOrderBy}
            setOrderBy={setNewOrderBy as any}
            codeItem={codeItem || ''}
            idsPharmacies={idsPharmacies}
          />
        ) : (
          <TableOperation
            typePilotage={isOnBusiness ? BUSINESS : FEEDBACK}
            onClickTableRow={onClickTableRow}
            data={state.tableData}
            rowCount={state.tableData.length}
            total={state.tableTotal}
            skip={skip}
            take={take}
            page={page}
            rowsPerPage={rowsPerPage}
            setSkip={setSkip}
            setTake={setTake}
            setPage={setPage}
            setRowsPerPage={setRowsPerPage}
            order={order}
            setOrder={setOrder}
            orderBy={newOrderBy}
            setOrderBy={setNewOrderBy as any}
            codeItem={codeItem || ''}
            idsPharmacies={idsPharmacies}
          />
        )}
        {/* <CustomModal
          open={openComments}
          setOpen={setOpenComments}
          title="Commentaires"
          closeIcon={true}
          withBtnsActions={false}
          headerWithBgColor={true}
        >
          <ModalComment codeItem={codeItem} idSource={''} idsPharmacies={idsPharmacies} />
        </CustomModal> */}
      </CustomFullScreenModal>
    </div>
  );
};

export default withRouter(TablePharmacie);
