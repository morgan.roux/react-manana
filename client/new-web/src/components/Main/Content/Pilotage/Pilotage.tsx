import React, { FC, useEffect } from 'react';
import useStyles from './styles';
import classnames from 'classnames';
import { Typography, Box } from '@material-ui/core';
import BusinessImg from '../../../../assets/img/pilotage_illustration/bouton_businnes.png';
import FeedbackImg from '../../../../assets/img/pilotage_illustration/bouton_feedback.png';
import EspacePubImg from '../../../../assets/img/pilotage_illustration/bouton_pub.png';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import { initPilotageApolloLocalState } from './Dashboard/Dashboard';
import { useApolloClient } from '@apollo/client';

const Pilotage: FC<RouteComponentProps> = ({ history: { push } }) => {
  const classes = useStyles({});
  const client = useApolloClient();

  const handleGoToBusiness = () => push('/pilotage/business/operation', { from: '/menu' });

  const handleGoToFeedback = () => push('/pilotage/feedback', { from: '/menu' });

  const handleGoToPublicite = () => push('/pilotage/dashboard/publicite');

  /**
   * Init pilotage local state
   */
  useEffect(() => {
    initPilotageApolloLocalState(client);
  }, []);

  return (
    <Box className={classes.pilotageRoot}>
      <Typography className={classes.title}>Choix du type de pilotage</Typography>
      <Typography className={classes.subTitle}>
        Choisissez un type de pilotage à afficher.
      </Typography>
      <Box className={classes.choiceContainer}>
        <Box className={classes.choice} onClick={handleGoToBusiness}>
          <img src={BusinessImg} alt="Pilotage BUSINESS" className={classes.choiceImg} />
          <Typography className={classnames(classes.choiceText, classes.centerTextVertival)}>
            Pilotage <span>BUSINESS</span>
          </Typography>
        </Box>
        <Box className={classes.choice} onClick={handleGoToFeedback}>
          <img src={FeedbackImg} alt="Pilotage FEEDBACK" className={classes.choiceImg} />
          <Typography className={classnames(classes.choiceText, classes.centerTextVertival)}>
            Pilotage <span>FEEDBACK</span>
          </Typography>
        </Box>
        <Box className={classes.choice} onClick={handleGoToPublicite}>
          <img
            src={EspacePubImg}
            alt="Pilotage des ESPACES PUBLICITAIRES"
            className={classes.choiceImg}
          />
          <Typography className={classes.choiceText}>
            Pilotage des <span>ESPACES PUBLICITAIRES</span>
          </Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default withRouter(Pilotage);
