import moment from 'moment';
import React from 'react';
import CommandeStatus from '../../../Commandes/CommandeStatus/CommandeStatus';
import { Button } from '@material-ui/core';
import { getUser } from '../../../../../../services/LocalStorage';
import { AppAuthorization } from '../../../../../../services/authorization';
const CommandeColumns = (push: any) => {
  const user = getUser();
  const auth = new AppAuthorization(user);

  const viewCommande = (commande: any) => () => {
    push(`/suivi-commandes/${commande.id}`, { from: '/commandes' });
  };

  return [
    {
      name: '',
      label: 'Ref. Cde',
      renderer: (value: any) => {
        return value && value.codeReference ? value.codeReference : '-';
      },
    },
    {
      name: '',
      label: 'Date',
      renderer: (value: any) => {
        return value && value.dateCreation
          ? `${moment(value.dateCreation).format('DD/MM/YYYY')} ${moment(value.dateCreation).format(
              'hh:mm',
            )}`
          : '-';
      },
    },
    {
      name: '',
      label: 'Type Cde',
      renderer: (value: any) => {
        return '-';
      },
    },
    {
      name: 'prixNetTotalHT',
      label: 'Net HT',
      renderer: (value: any) => {
        const prix = value && value.prixNetTotalHT ? value.prixNetTotalHT.toFixed(2) : '';
        return value && value.prixNetTotalHT ? `${prix}€` : '-';
      },
    },
    {
      name: '',
      label: 'Frais de Port',
      renderer: (value: any) => {
        const prix = value && value.frais_Port ? value.frais_Port.toFixed(2) : '';
        return value.frais_Port ? `${prix}€` : '-';
      },
    },
    {
      name: 'owner.userName',
      label: 'Validant',
      renderer: (value: any) => {
        return value && value.owner && value.owner.userName ? value.owner.userName : '-';
      },
    },
    {
      name: 'operation.libelle',
      label: 'OPERATION',
      renderer: (value: any) => {
        return (value && value.operation && value.operation.libelle) || '-';
      },
    },
    {
      name: 'commandeStatut.libelle',
      label: 'STATUT',
      renderer: (value: any) => {
        return value && value.commandeStatut && value.commandeStatut.libelle ? (
          <CommandeStatus
            status={value.commandeStatut.libelle ? value.commandeStatut.libelle : '-'}
            id={value.id}
          />
        ) : (
          '-'
        );
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return value ? (
          <Button
            onClick={viewCommande(value)}
            disabled={!auth.isAuthorizedToViewDetailsCommande()}
          >
            Voir
          </Button>
        ) : (
          '-'
        );
      },
    },
  ];
};

export default CommandeColumns;
