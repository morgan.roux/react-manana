import { useApolloClient, useQuery } from '@apollo/client';
import { Box } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
// import { useStyles } from './styles';
import { RouteComponentProps } from 'react-router-dom';
import { GET_CHECKEDS_ACTUALITE } from '../../../../../graphql/Actualite/local';
import { GET_CHECKEDS_COMMANDE } from '../../../../../graphql/Commande/local';
import { GET_CHECKEDS_LABORATOIRE } from '../../../../../graphql/Laboratoire/local';
import { GET_CHECKEDS_OPERATION } from '../../../../../graphql/OperationCommerciale/local';
import { GET_CHECKEDS_PHARMACIE } from '../../../../../graphql/Pharmacie/local';
import { GET_PILOTAGE_CIBLES } from '../../../../../graphql/Pilotage/local';
import { GET_CHECKEDS_PRODUIT } from '../../../../../graphql/ProduitCanal/local';
import { GET_CHECKEDS_PROJECT } from '../../../../../graphql/Project/local';
import {
  ActualiteFilterProps,
  LaboratoireFilterProps,
  OperationFilterProps,
  ProduitFilterProps,
  ProjectFilterProps,
} from '../../../../Common/newWithSearch/ComponentInitializer';
import Filter from '../../../../Common/newWithSearch/Filter';
import Stepper, { Step } from '../../../../Common/Stepper/Stepper';
import { GLOBAL_URL_BUSINESS, GLOBAL_URL_FEEDBACK } from '../Dashboard/Dashboard';
import ActuColumns from './Columns/Actu';
import CommandeColumns from './Columns/Commande';
import LaboColumns from './Columns/Labo';
import OcColumns from './Columns/Oc';
import ProduitCanalColumns from './Columns/ProduitCanal';
import ProjectColumns from './Columns/Project';
import { PilotageCibleInterface } from './Interface/CibleInterface';
import ItemSelection from './Steps/ItemSelection';
import ItemTable from './Steps/ItemTable';
import Pharmacie from './Steps/Pharmacie';

const Cible: FC<RouteComponentProps> = ({
  // match: { params },
  history,
  location: { pathname },
}) => {
  // const classes = useStyles({});
  const [nextBtnDisabled, setNextBtnDisabled] = useState<boolean>(true);
  const [allValues, setAllValues] = useState<PilotageCibleInterface | null>();
  const client = useApolloClient();

  const checkedPharmacie = useQuery(GET_CHECKEDS_PHARMACIE);
  const allCheckedsPharmacie =
    checkedPharmacie && checkedPharmacie.data && checkedPharmacie.data.checkedsPharmacie;

  // const { item } = params as any;
  const {
    location: { state },
    push,
  } = history;

  const isOnBusiness = pathname.includes('business');
  // const isOnFeedback = pathname.includes('feedback');

  const title = `Pilotage ${isOnBusiness ? 'business' : 'feedback'}`;

  const checkedsProduitQuery = useQuery(GET_CHECKEDS_PRODUIT);
  const checkedsOperationQuery = useQuery(GET_CHECKEDS_OPERATION);
  const checkedsActualiteQuery = useQuery(GET_CHECKEDS_ACTUALITE);
  const checkedsLaboratoireQuery = useQuery(GET_CHECKEDS_LABORATOIRE);
  const checkedsCommandeQuery = useQuery(GET_CHECKEDS_COMMANDE);
  const checkedsProjectQuery = useQuery(GET_CHECKEDS_PROJECT);

  useEffect(() => {
    if (allValues && allValues.item && allValues.item.codeItem) {
      const codeItem = allValues.item.codeItem;
      switch (codeItem.startsWith('A')) {
        case true:
          setAllValues((prevState: any) => ({
            ...prevState,
            type: (allValues.item && allValues.item.type) || 'operation',
            ...{
              items:
                (checkedsOperationQuery &&
                  checkedsOperationQuery.data &&
                  checkedsOperationQuery.data.checkedsOperation) ||
                [],
            },
          }));
          break;
      }
      switch (codeItem.startsWith('C')) {
        case true:
          setAllValues((prevState: any) => ({
            ...prevState,
            type: (allValues.item && allValues.item.type) || 'operation',
            ...{
              items:
                (checkedsLaboratoireQuery &&
                  checkedsLaboratoireQuery.data &&
                  checkedsLaboratoireQuery.data.checkedsLaboratoire) ||
                [],
            },
          }));
          break;
      }
      switch (codeItem.startsWith('B')) {
        case true:
          setAllValues((prevState: any) => ({
            ...prevState,
            type: (allValues.item && allValues.item.type) || 'operation',
            ...{
              items:
                (checkedsActualiteQuery &&
                  checkedsActualiteQuery.data &&
                  checkedsActualiteQuery.data.checkedsActualite) ||
                [],
            },
          }));
          break;
      }
      switch (codeItem.startsWith('J')) {
        case true:
          setAllValues((prevState: any) => ({
            ...prevState,
            type: (allValues.item && allValues.item.type) || 'operation',
            ...{
              items:
                (checkedsCommandeQuery &&
                  checkedsCommandeQuery.data &&
                  checkedsCommandeQuery.data.checkedsCommande) ||
                [],
            },
          }));
          break;
      }
      switch (codeItem.startsWith('K')) {
        case true:
          setAllValues((prevState: any) => ({
            ...prevState,
            type: (allValues.item && allValues.item.type) || 'operation',
            ...{
              items:
                (checkedsProduitQuery &&
                  checkedsProduitQuery.data &&
                  checkedsProduitQuery.data.checkedsProduit) ||
                [],
            },
          }));
          break;
      }
      switch (codeItem.startsWith('L')) {
        case true:
          setAllValues((prevState: any) => ({
            ...prevState,
            type: (allValues.item && allValues.item.type) || 'operation',
            ...{
              items:
                (checkedsProjectQuery &&
                  checkedsProjectQuery.data &&
                  checkedsProjectQuery.data.checkedsProject) ||
                [],
            },
          }));
          break;
      }
    } else {
      setAllValues((prevState: any) => ({
        ...prevState,
        type: 'operation',
        ...{
          items:
            (checkedsOperationQuery &&
              checkedsOperationQuery.data &&
              checkedsOperationQuery.data.checkedsOperation) ||
            [],
        },
      }));
    }
  }, [
    checkedsProduitQuery,
    checkedsProjectQuery,
    checkedsOperationQuery,
    checkedsActualiteQuery,
    checkedsCommandeQuery,
    checkedsLaboratoireQuery,
  ]);

  useEffect(() => {
    if (allCheckedsPharmacie) {
      setAllValues((prevState: any) => ({
        ...prevState,
        pharmacies: allCheckedsPharmacie,
      }));
    }
  }, [allCheckedsPharmacie]);

  const backToHome = (): void => {
    push('/pilotage');
  };

  const isBusiness: boolean = pathname.includes('business');
  const isFeedback: boolean = pathname.includes('feedback');

  const getColumns = (item: any) => {
    let column: any[] = [];
    if (item && item.codeItem) {
      switch (item.codeItem.startsWith('A')) {
        case true:
          column = OcColumns;
          break;
      }
      switch (item.codeItem.startsWith('C')) {
        case true:
          column = LaboColumns;
          break;
      }
      switch (item.codeItem.startsWith('B')) {
        case true:
          column = ActuColumns;
          break;
      }
      switch (item.codeItem.startsWith('J')) {
        case true:
          column = CommandeColumns(history);
          break;
      }
      switch (item.codeItem.startsWith('K')) {
        case true:
          column = ProduitCanalColumns;
          break;
      }
      switch (item.codeItem.startsWith('L')) {
        case true:
          column = ProjectColumns;
          break;
      }
    }
    return column;
  };

  const routeState: any = state as any;
  const fromPath = routeState && routeState.from;

  const columns = allValues && allValues.item ? getColumns(allValues.item) : OcColumns;

  console.log('allValues : ', allValues);

  const pilotageCiblesResult = useQuery(GET_PILOTAGE_CIBLES);

  // console.log('pilotageCiblesResult :>> ', pilotageCiblesResult);
  const pylotageType = isBusiness ? 'business' : 'feedback';

  useEffect(() => {
    if (fromPath) {
      setAllValues(null);
      // TODO: Migration
      /*(client as any).writeData({
        data: {
          pilotageCibles: null,
        },
      });*/
    }
  }, [fromPath]);

  const goToStatistique = () => {
    // TODO: Migration
    /*(client as any).writeData({
      data: {
        pilotageCibles: {
          ...allValues,
          item: allValues && allValues.item ? allValues.item : null,
          pilotageType: pylotageType,
          __typename: 'pilotageCibles',
        },
      },
    });*/

    if (isBusiness) {
      push(GLOBAL_URL_BUSINESS);
    }
    if (isFeedback) {
      push(GLOBAL_URL_FEEDBACK);
    }
  };

  useEffect(() => {
    return () => {
      // TODO: Migration
      /*(client as any).writeData({
        data: {
          checkedsProduit: null,
          checkedsOperation: null,
          checkedsProject: null,
          checkedsLaboratoire: null,
          checkedsActualite: null,
          checkedsCommande: null,
        },
      });*/
    };
  }, []);

  useEffect(() => {
    if (
      pilotageCiblesResult &&
      pilotageCiblesResult.data &&
      pilotageCiblesResult.data.pilotageCibles &&
      !allValues
    ) {
      setAllValues(pilotageCiblesResult.data.pilotageCibles);
    }
  }, [pilotageCiblesResult]);

  const finalStep = {
    buttonLabel: 'Suivant',
    action: goToStatistique,
  };

  const feedbackSteps = [
    {
      title: "Choix de l'item",
      content: (
        <ItemSelection
          allValues={allValues}
          setAllValues={setAllValues}
          setNextBtnDisabled={setNextBtnDisabled}
        />
      ),
    },
    {
      title: `Choix ${(allValues && allValues.item && allValues.item.name) || 'cible'}`,
      content: (
        <ItemTable
          allValues={allValues}
          setAllValues={setAllValues}
          setNextBtnDisabled={setNextBtnDisabled}
          columns={columns}
        />
      ),
    },
    {
      title: 'Choix des pharmacies',
      content: (
        <Pharmacie
          allValues={allValues}
          setAllValues={setAllValues}
          setNextBtnDisabled={setNextBtnDisabled}
        />
      ),
    },
  ];

  const businessSteps = [
    {
      title: 'Choix des opérations commerciales',
      content: (
        <ItemTable
          allValues={allValues}
          setAllValues={setAllValues}
          setNextBtnDisabled={setNextBtnDisabled}
          columns={columns}
        />
      ),
    },
    {
      title: 'Choix des pharmacies',
      content: (
        <Pharmacie
          allValues={allValues}
          setAllValues={setAllValues}
          setNextBtnDisabled={setNextBtnDisabled}
        />
      ),
    },
  ];

  const steps: Step[] = isOnBusiness ? businessSteps : feedbackSteps;

  return (
    <Box display="flex">
      {pathname.includes('/operation') && <Filter filterProps={OperationFilterProps} />}
      {pathname.includes('/actualites') && <Filter filterProps={ActualiteFilterProps} />}
      {pathname.includes('/laboratoire') && <Filter filterProps={LaboratoireFilterProps} />}
      {pathname.includes('/produit') && <Filter filterProps={ProduitFilterProps} />}
      {pathname.includes('/project') && <Filter filterProps={ProjectFilterProps} />}
      <Stepper
        title={title}
        steps={steps}
        backToHome={backToHome}
        backBtnText="Retour au choix de pilotage"
        disableNextBtn={nextBtnDisabled}
        fullWidth={false}
        finalStep={finalStep}
        history={history}
      />
    </Box>
  );
};

export default Cible;
