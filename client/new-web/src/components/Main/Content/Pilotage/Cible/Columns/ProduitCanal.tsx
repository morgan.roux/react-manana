import { Tooltip } from '@material-ui/core';
import { CheckCircle, BlockTwoTone } from '@material-ui/icons';
import moment from 'moment';
import React from 'react';
const ProduitCanalColumns = [
  {
    name: 'codeReference',
    label: 'Code',
  },
  {
    name: 'nomCanalArticle',
    label: 'Libellé',
  },
  {
    name: '',
    label: 'Labo',
    // sortable: true,
    renderer: (value: any) => {
      return value && value.laboratoire && value.laboratoire.nomLabo
        ? value.laboratoire.nomLabo
        : '-';
    },
  },
  {
    name: 'stv',
    label: 'STV',
    sortable: true,
  },
  {
    name: 'produitcanal',
    label: 'Carton',
  },
  {
    name: '',
    label: 'Stock plateforme',
    renderer: (value: any) => {
      if (value.qteStock > 0) {
        return <CheckCircle />;
      } else {
        return <BlockTwoTone color="secondary" />;
      }
    },
  },
  {
    name: 'qteStock',
    label: 'Stock Pharmacie',
  },
  {
    name: '',
    label: 'Tarif HT',
    renderer: (value: any) => {
      return value.prixPhv ? value.prixPhv + '€' : '-';
    },
  },
];

export default ProduitCanalColumns;
