export interface CheckedItemsInterface {
  id: string;
  libelle: string;
}

export interface PilotageCibleInterface {
  item: any;
  items: any[];
  pharmacies: any[];
}

export interface ItemInterface {
  id: string;
  name: string;
}
