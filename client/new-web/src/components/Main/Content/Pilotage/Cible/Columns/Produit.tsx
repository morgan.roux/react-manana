import { Tooltip } from '@material-ui/core';
import { CheckCircle, BlockTwoTone } from '@material-ui/icons';
import moment from 'moment';
import React from 'react';
const ProduitColumns = [
  {
    name: 'produitCode.code',
    label: 'Code',
    renderer: (value: any) => {
      return (value && value.produitCode && value.produitCode.code) || '-';
    },
  },
  {
    name: 'libelle',
    label: 'Libellé',
    renderer: (value: any) => {
      return (value && value.libelle) || '-';
    },
  },
  {
    name: 'produitTechReg.laboExploitant.nomLabo',
    label: 'Labo',
    renderer: (value: any) => {
      return (
        (value &&
          value.produitTechReg &&
          value.produitTechReg.laboExploitant &&
          value.produitTechReg.laboExploitant.nomLabo) ||
        '-'
      );
    },
  },
  /* {
    name: 'unitePetitCond',
    label: 'Carton',
    renderer: (value: any) => {
      return (value && value.unitePetitCond) || '-';
    },
  }, */
];

export default ProduitColumns;
