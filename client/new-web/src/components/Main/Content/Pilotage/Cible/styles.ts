import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      overflowY: 'auto',
      width: '100%',
    },
    title: {
      color: '#1D1D1D',
      fontSize: 24,
      fontWeight: 'bold',
      letterSpacing: 0,
      opacity: 1,
    },
  }),
);
