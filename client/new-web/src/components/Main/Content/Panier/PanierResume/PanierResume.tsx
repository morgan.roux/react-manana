import React, { FC, useEffect, useState } from 'react';
import { useQuery } from '@apollo/client';
import { Box, Button, Paper, Typography } from '@material-ui/core';
import LocalGroceryStoreIcon from '@material-ui/icons/LocalGroceryStore';
import PrintIcon from '@material-ui/icons/Print';
import classnames from 'classnames';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { GET_CODE_CANAL } from '../../../../../graphql/CommandeCanal/local';
import { Panier } from '../../../../../graphql/Panier/types/Panier';
import { getUser } from '../../../../../services/LocalStorage';
import { useStyles } from '../styles';
import { CODE_FRANCO_PORT } from '../../../../../Constant/parameter';
import {
  PARAMETER_BY_CODE,
  PARAMETER_BY_CODEVariables,
} from '../../../../../graphql/Parametre/types/PARAMETER_BY_CODE';
import { GET_PARAMETER_BY_CODE } from '../../../../../graphql/Parametre/query';
import CustomButton from '../../../../Common/CustomButton';
import { AppAuthorization } from '../../../../../services/authorization';

interface PanierResumeProps {
  currentPanier: Panier | null | any;
  match: { params: { type: string } };
}

const PanierResume: FC<PanierResumeProps & RouteComponentProps<any, any, any>> = ({
  history,
  currentPanier,
}) => {
  const classes = useStyles({});
  const user = getUser();
  const [franco, setFranco] = useState<number>(0);

  console.log('currentPanier : ', currentPanier);

  const auth = new AppAuthorization(user);

  const myParameter = useQuery<PARAMETER_BY_CODE, PARAMETER_BY_CODEVariables>(
    GET_PARAMETER_BY_CODE,
    { variables: { code: CODE_FRANCO_PORT } },
  );

  useEffect(() => {
    if (
      myParameter &&
      myParameter.data &&
      myParameter.data.parameterByCode &&
      myParameter.data.parameterByCode.value &&
      myParameter.data.parameterByCode.value.value
    ) {
      console.log('[ FRANCO PORT ]', myParameter.data.parameterByCode.value.value);
      setFranco(parseInt(myParameter.data.parameterByCode.value.value, 10));
    }
  }, [myParameter]);

  // const franco = useValueParameterAsNumber(CODE_FRANCO_PORT);

  // get last path
  const lastPath =
    history.location.state && history.location.state.from ? history.location.state.from : '';

  const pathName = history.location.pathname;

  let commandeCanal: any;

  commandeCanal = useQuery(GET_CODE_CANAL);

  const remiseGlobale =
    currentPanier && currentPanier.remiseGlobale
      ? parseFloat(currentPanier.remiseGlobale.toFixed(2))
      : 0;

  const prixBaseTotalHT =
    currentPanier && currentPanier.prixBaseTotalHT
      ? parseFloat(currentPanier.prixBaseTotalHT.toFixed(2))
      : 0;

  const totalNet =
    currentPanier && currentPanier.prixNetTotalHT
      ? parseFloat(currentPanier.prixNetTotalHT.toFixed(2))
      : 0;

  const nbRef = currentPanier && currentPanier.nbrRef ? currentPanier.nbrRef : 0;

  const totalRemise = parseFloat(((prixBaseTotalHT * remiseGlobale) / 100).toFixed(2));

  const francoPort = franco || 0;

  const lignes =
    (currentPanier && currentPanier.panierLignes && currentPanier.panierLignes.length) ||
    (currentPanier && currentPanier.commandeLignes && currentPanier.commandeLignes.length) ||
    0;

  const resteFranco = francoPort - totalNet > 0 ? francoPort - totalNet : 0;

  const resumeArray = [
    { key: 'Reste pour atteindre le Franco :', value: resteFranco.toFixed(2) + '€ HT' },
    {
      key: 'Total Brut :',
      value: prixBaseTotalHT.toFixed(2) + '€ HT',
    },
    {
      key: 'Total remisé :',
      value: '(' + remiseGlobale.toFixed(2) + '%)' + ' : ' + totalRemise.toFixed(2) + '€ HT',
    },
    {
      key: 'TOTAL NET :',
      value: totalNet.toFixed(2) + '€ HT',
      classes: classes.MontserratBold + ' , ' + classes.pink,
    },
  ];

  useEffect(() => {
    console.log('changees on currentPanier');
    // update();
  }, [currentPanier]);

  const infoArray = [
    // { key: 'Période de la commande:', value: 'du 11/08/2019 au 21/11/2019', marginTop: '0px' },
    // {
    //   key: 'Livraison:',
    //   value:
    //     'environ 4 jours. En cas de première commande les délais de livraison seront augmentés pour la création de votre compte.',
    //   marginTop: '14px',
    // },
    {
      key: 'Franco de port :',
      value: ' ' + francoPort.toFixed(2) + ' € net HT',
      marginTop: '14px',
    },
    // { key: 'Contact laboratoire:', value: '0800442931 email@exemple.fr', marginTop: '14px' },
    // {
    //   key:
    //     "En cas de première commande, le laboratoire vous demandera les informations suivantes : RIB + Kbis + numéro d'affiliation et/ou photocopie de votre diplôme.",
    //   marginTop: '40px',
    // },
  ];

  const goToRecapitulatif = () => {
    history.push('/recapitulatif/panier');
  };

  const disabledValidateCommande = (): boolean => {
    if (!auth.isAuthorizedToValidateCommande()) {
      return true;
    }
    return false;
  };

  return (
    <Paper className={classes.panierResume}>
      <Typography className={classnames(classes.MontserratRegular, classes.small, classes.flexRow)}>
        <span className={classnames(classes.flexRow, classes.underlined)}>Votre panier</span>
        {` : `}
        {lignes} ligne(s), {nbRef} boite(s)
      </Typography>
      {resumeArray.map((item: any) => (
        <Box key={item.key} className={classnames(classes.rowTypo)}>
          <Typography
            className={classnames(
              classes.MontserratRegular,
              classes.small,
              item.classes ? item.classes : '',
            )}
          >
            {item.key}
          </Typography>
          <Typography
            className={classnames(
              classes.MontserratRegular,
              classes.small,
              item.classes ? item.classes : '',
            )}
          >
            {item.value}
          </Typography>
        </Box>
      ))}
      {pathName !== '/recapitulatif/panier' &&
        pathName !== '/recapitulatif/operation-commerciale' &&
        lastPath !== '/commandes' && (
          <div>
            <Box marginBottom="20px" marginTop="40px">
              <Button
                className={classnames(
                  classes.panierResumeButton,
                  classes.grayBackground,
                  classes.MontserratBold,
                  classes.medium,
                  classes.darkLight,
                )}
                disabled={true}
              >
                <LocalGroceryStoreIcon className={classes.panierResumeButtonIcon} /> METTRE EN
                ATTENTE
              </Button>
            </Box>
            <Box marginBottom="20px">
              <CustomButton
                color="secondary"
                className={classnames(
                  classes.panierResumeButton,
                  classes.MontserratBold,
                  classes.small,
                )}
                onClick={goToRecapitulatif}
                disabled={disabledValidateCommande()}
              >
                VALIDER LA COMMANDE
              </CustomButton>
            </Box>

            <Button
              className={classnames(
                classes.panierResumeButton,
                classes.grayBackground,
                classes.MontserratBold,
                classes.medium,
                classes.darkLight,
              )}
              disabled={true}
            >
              <PrintIcon className={classes.panierResumeButtonIcon} /> IMPRIMER
            </Button>
          </div>
        )}

      <Box className={classnames(classes.RobotoRegular, classes.small)} marginTop="40px">
        {infoArray.map((item: any) => (
          <Box key={item.key} marginTop={item.marginTop}>
            {item.key}
            {item.value && (
              <div className={classnames(classes.inline, classes.pink)}>{item.value}</div>
            )}
          </Box>
        ))}
      </Box>
      {currentPanier && currentPanier.commentaireInterne && (
        <Box marginTop="16px">
          <Typography>Commentaire Interne : </Typography>
          {currentPanier.commentaireInterne}
        </Box>
      )}
      {currentPanier && currentPanier.commentaireExterne && (
        <Box marginTop="16px">
          <Typography>Commentaire Externe : </Typography>
          {currentPanier.commentaireExterne}
        </Box>
      )}
    </Paper>
  );
};

export default withRouter(PanierResume);
