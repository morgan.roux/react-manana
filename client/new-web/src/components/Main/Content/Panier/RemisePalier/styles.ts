import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    autocomplete: {
      '& .MuiAutocomplete-inputRoot input': {
        padding: '2.5px 14px !important',
      },
    },
    table: {
      borderSpacing: '0 !important',
    },
    tableHead: {
      background: `${theme.palette.primary.main} !important`,
    },
    tableHeadRow: {
      height: 65,
    },
    /* TOOLBAR */
    toolbar: {
      height: '60px',
      background: '#004354 0% 0% no-repeat padding-box',
      paddingLeft: '32px',
      color: '#FFFFFF',
      fontSize: '12px',
      justifyContent: 'center',
    },
    toolbarItem: {
      display: 'flex',
      flexWrap: 'wrap',
      boxSizing: 'border-box',
      flexDirection: 'row',
      marginRight: '46px',
    },
    inputFont: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
    },
    toolbarText: {
      fontSize: '12px',
      marginLeft: '10px',
      fontFamily: 'Montserrat',
      display: 'flex',
      alignItems: 'center',
      fontWeight: 500,
    },
    toolbarButton: {
      background: '#FFFFFF 0% 0% no-repeat padding-box',
      boxShadow: '0px 2px 2px #00000040',
      borderRadius: '3px',
      opacity: 1,
      width: '120px',
      height: '35px',
      textTransform: 'lowercase',
    },
    fillWhite: {
      fill: '#FFFFFF',
    },

    /* CONTENT HEADER */
    contentHeader: {
      display: 'flex',
      flexDirection: 'row',
      height: '50px',
      marginTop: '32px',
      '& div': {
        cursor: 'pointer',
      },
    },
    iconPeople: {
      position: 'absolute',
      top: -9,
      left: 10,
      padding: 3,
    },
    contentHeaderText: {
      marginLeft: '16px',
      marginRight: '16px',
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: '0.875rem',
    },
    contentHeaderNumberPink: {
      width: '35px',
      borderRadius: '3px',
      backgroundColor: '#FFFFFF',
      fontSize: '12px',
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      color: '#1D1D1D',
      textAlign: 'center',
      lineHeight: 1,
      padding: '4px 0',
    },
    contentHeaderNumberGray: {
      width: '35px',
      borderRadius: '3px',
      backgroundColor: '#004354',
      fontSize: '12px',
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      color: '#FFFFFF',
      textAlign: 'center',
      lineHeight: 1,
      padding: '4px 0',
    },
    contentHeaderItemPink: {
      opacity: 1,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      background:
        'transparent linear-gradient(232deg, #F11957 0%, #E34168 100%) 0% 0% no-repeat padding-box',
      color: '#FFFFFF',
      paddingLeft: '31px',
      paddingRight: '31px',
    },
    contentHeaderItemGray: {
      opacity: 1,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      background: '#EFEFEF',
      color: '#1D1D1D',
      paddingLeft: '31px',
      paddingRight: '31px',
      borderRadius: '3px',
    },

    /* CONTENT BODY */
    contentBodyContainer: {
      paddingLeft: '31px',
      paddingTop: '31px',
      display: 'flex',
      flexDirection: 'row',
    },

    /* PANIER LISTE */
    panierListe: {
      flex: 1,
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
    },
    panierListeItem: {
      flexBasis: '342px',
      display: 'flex',
      flexDirection: 'column',
      paddingLeft: '12px',
      paddingRight: '12px',
      marginRight: '32px',
      borderBottom: '16px solid transparent',
      paddingTop: '24px',
    },
    panierListeItemDescription: {
      textAlign: 'center',
      fontFamily: 'Montserrat',
      fontSize: '12px',
      fontWeight: 600,
      letterSpacing: 0,
      color: '#004354',
      opacity: 1,
      marginTop: '10px',
      marginBottom: '14px',
    },
    panierListeItemRating: {
      marginTop: '16px',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'left',
    },
    widthBtn: {
      maxWidth: 170,
    },
    star: {
      color: '#FDA509',
      height: '20px',
      width: '20px',
    },
    panierListePrixContainer: {
      marginTop: '16px',
      display: 'flex',
      flexDirection: 'row',
    },
    panierListePrixLeft: {
      flex: 1,
      textAlign: 'left',
      fontFamily: 'Montserra',
      fontWeight: 'normal',
      fontSize: '12px',
      color: '#878787',
    },
    panierListePrixRight: {
      flex: 1,
      textAlign: 'right',
      fontFamily: 'Montserra',
      fontWeight: 'normal',
      fontSize: '12px',
    },
    panierListeSocialItem: {
      flex: 1,
      justifyContent: 'center',
      display: 'flex',
      flexDirection: 'row',
      position: 'relative',
      cursor: 'pointer',
    },
    panierListeSocialItemMargin: {
      marginLeft: '-6px',
    },
    stockIcon: {
      width: '20px',
      height: '20px',
    },
    textLeft: {
      textAlign: 'left',
    },
    textCenter: {
      textAlign: 'center',
    },
    libelle: {
      color: '#004354',
      fontWeight: 500,
      maxWidth: 135,
      fontSize: '0.75rem',
    },
    p10: {
      padding: '10px 0 0',
    },

    px10: {
      paddingLeft: 12,
      paddingRight: 12,
    },
    pr12: {
      paddingRight: 12,
    },
    panierListeSocialIcon: {
      height: '20px',
      width: '20px',
      color: '#B1B1B1',
    },
    panierListeButtonLeft: {
      background: '#EFEFEF',
      textTransform: 'lowercase',
    },
    cursorPointer: {
      cursor: 'pointer',
    },
    panierListeQuantite: {
      border: '1px solid',
      // borderColor: theme.palette.gray.light,
      display: 'flex',
      flexDirection: 'row',
      width: '200px',
    },

    /* PANIER Resume */
    panierResume: {
      flexBasis: '342px',
      paddingLeft: '12px',
      paddingRight: '12px',
      paddingTop: '24px',
      paddingBottom: '24px',
      height: 'fit-content',
    },
    panierResumeButton: {
      width: '100%',
      textAlign: 'center',
      boxShadow: '0px 2px 2px #00000040',
      height: '50px',
    },
    panierResumeButtonIcon: {
      marginRight: '8px',
    },
    btnValidationCommande: {
      '&:hover': {
        backgroundColor: 'inherit',
        opacity: 0.95,
      },
    },

    /* FONT STYLE */
    MontserratRegular: {
      fontFamily: 'Montserrat',
      fontWeight: 400,
    },
    MontserratBold: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
    },
    RobotoRegular: {
      fontFamily: 'Roboto',
      fontWeight: 'normal',
    },
    RobotoBold: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
    },
    RobotoMedium: {
      fontFamily: 'Roboto',
      fontWeight: 'bolder',
    },

    underlined: {
      textDecoration: 'underline',
    },
    uppercase: {
      textTransform: 'uppercase',
    },

    /*FONT SIZE */
    small: {
      // fontSize: theme.typography.small.fontSize,
    },
    medium: {
      // fontSize: theme.typography.medium.fontSize,
    },
    big: {
      // fontSize: theme.typography.big.fontSize,
    },

    /* COLORS */
    grayBackground: {
      background: '#EFEFEF',
    },
    gray: {
      // color: theme.palette.gray.main,
    },
    pinkBackground: {
      // background: theme.palette.pink.main,
    },
    pinkLinearBackground: {
      background: 'linear-gradient(45deg, rgba(227,65,103,1) 0%, rgba(227,70,65,1) 100%)',
    },
    pink: {
      color: theme.palette.secondary.main,
    },
    green: {
      // color: theme.palette.green.main,
    },
    white: {
      // color: theme.palette.white.main,
    },
    darkLight: {
      // color: theme.palette.darklight.main,
    },
    yellowBackground: {
      // background: theme.palette.yellow.main,
      borderRadius: '50%',
    },
    orangeBackground: {
      // background: theme.palette.orange.main,
      borderRadius: '50%',
    },
    dark: {
      color: '#1D1D1D',
    },
    red: {
      color: 'red',
    },
    blue: {
      color: '#004354',
    },
    colorDefault: {
      // color: theme.palette.default.main,
    },

    /* FLEX */
    flexColumn: {
      display: 'flex',
      flexDirection: 'column',
    },
    flexRow: {
      display: 'flex',
      flexDirection: 'row',
    },
    inline: {
      display: 'inline',
    },
    rowTypo: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    justifyCenter: {
      justifyContent: 'center',
    },
    alignCenter: {
      alignItems: 'center',
    },
    flex: {
      flex: 1,
    },

    /* AUTRE PANIER */
    autrepanier: {
      marginLeft: '12px',
      marginBottom: '13px',
    },
    autrepanierAvatar: {
      borderRadius: '50%',
      objectFit: 'cover',
      width: '85px',
      height: '85px',
    },

    /* RECAPITULATIF */
    recapContainer: {
      display: 'flex',
      flexDirection: 'row',
      '@media (max-width: 1024px)': {
        flexDirection: 'column',
      },
    },
    recapCommentContainer: {
      display: 'flex',
      flexDirection: 'row',
      '@media (max-width: 1024px)': {
        flexDirection: 'column',
        marginLeft: '0',
        marginTop: '8px',
      },
    },
    recapTextArea: {
      width: 'inherit',
      fontSize: '0.875rem',
      fontFamily: 'Montserrat',
      fontWeight: 'normal',
      padding: '16px',
      border: '1px solid #B1B1B1',
      borderRadius: '3px',
    },

    recapLabelBox: {
      flexBasis: '40px',
      paddingTop: '16px',
    },

    recapTextAreaBox: {
      marginLeft: '70px',
      marginRight: '64px',
      width: '100%',
      '@media (max-width: 1200px)': {
        marginLeft: 24,
        marginRight: 24,
      },
      '@media (max-width: 1024px)': {
        flexDirection: 'column',
        marginLeft: 0,
        marginRight: 0,
      },
    },
    recapPanierResume: {
      width: '342px',
      '@media (max-width: 1024px)': {
        marginTop: '32px',
        width: '100%',
      },
    },
    recapButtonContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'start',
      '@media (max-width: 1024px)': {
        flexDirection: 'column',
        alignItems: 'center',
      },
    },
    boxInvisible: {
      flexBasis: '173px',
      '@media (max-width: 1200px)': {
        flexBasis: '25px',
      },
      '@media (max-width: 1054px)': {
        flexBasis: '0',
      },
      '@media (max-width: 1024px)': {
        display: 'none',
      },
    },
    recapValiderButton: {
      marginLeft: '32px',
      marginRight: '32px',
      '@media (max-width: 1024px)': {
        marginLeft: 0,
        marginRight: 0,
        marginTop: '16px',
        marginBottom: '16px',
      },
    },
    /*REMISE */
    remiseTable: {
      display: 'flex',
      flexDirection: 'row',
    },
    remiseTableHeader: {
      marginRight: '16px',
    },
    remiseTableHeaderFont: {
      fontSize: '16px',
    },
    quantiteTextField: {
      borderLeft: '1px solid',
      borderLeftColor: '#EFEFEF',
      borderRight: '1px solid',
      borderRightColor: '#EFEFEF',
      width: '48px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& > div > input': {
        textAlign: 'center',
        fontSize: '0.875rem',
        fontFamily: 'Roboto',
        fontWeight: 'bold',
      },
    },
    defalutFont: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 16,
    },
    quantiteSaveButton: {
      borderLeft: '1px solid',
      borderLeftColor: '#EFEFEF',
    },
    underline: {
      '&&&:before': {
        borderBottom: 'none',
      },
      '&&:after': {
        borderBottom: 'none',
      },
    },
    panierListeRemiseQuantite: {
      border: '1px solid',
      borderColor: '#EFEFEF',
      display: 'flex',
      flexDirection: 'row',
      borderRadius: '4px',
      height: 30,
      backgroundColor: theme.palette.common.white,
    },
    quantiteButton: {
      // color: theme.palette.green.main,
    },
    th: {
      fontFamily: 'Roboto',
      color: theme.palette.common.white,
      padding: '8px !important',
    },
    tableBody: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 16,
    },
    blueButton: {
      color: '#004354',
    },
    contentPannierList: {
      display: 'flex',
      flexWrap: 'wrap',
      width: '100%',
      '& .MuiPaper-root': {
        width: '30%',
        maxWidth: 326,
        margin: '0 14px 24px',
        '@media (max-width:1280px)': {
          width: '45%',
        },
        '@media (max-width:1024px)': {
          width: 400,
          justifyContent: 'center',
        },
      },
    },
    navBar: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      background: theme.palette.primary.main,
      color: theme.palette.common.white,
      alignItems: 'center',
      padding: 16,
    },
    navBarTitle: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 30,
    },
    primary: {
      color: theme.palette.primary.main,
    },
    secondary: {
      color: theme.palette.secondary.main,
    },
  }),
);
