import React, { FC, Fragment } from 'react';
import { Typography } from '@material-ui/core';
import IconPeople from '@material-ui/icons/People';
import { useStyles } from '../styles';
import { RouteComponentProps, withRouter, Redirect } from 'react-router-dom';
import LocalGroceryStoreIcon from '@material-ui/icons/LocalGroceryStore';
import { myPanier_myPanier } from '../../../../../graphql/Panier/types/myPanier';
import { getPharmaciePanier_pharmaciePanier } from '../../../../../graphql/Panier/types/getPharmaciePanier';
import { getUser } from '../../../../../services/LocalStorage';
import { Panier } from '../../../../../graphql/Panier/types/Panier';
interface PanierHeaderProps {
  currentPanier: Panier | null;
  autrePanier: Array<getPharmaciePanier_pharmaciePanier | null> | null;
}

const PanierHeader: FC<PanierHeaderProps & RouteComponentProps<any, any, any>> = ({
  history,
  currentPanier,
  autrePanier,
}) => {
  const classes = useStyles({});
  const user = getUser();

  const pathName = history.location.pathname;

  const goToPanier = () => {
    if (pathName !== '/panier') history.push('/panier');
  };

  const goToOtherPanier = () => {
    if (pathName !== '/autrepanier') history.push('/autrepanier');
  };

  return (
    <Fragment>
      <div>
        <div className={classes.contentHeader}>
          <div
            className={
              pathName === '/panier' ? classes.contentHeaderItemPink : classes.contentHeaderItemGray
            }
            onClick={goToPanier}
          >
            <LocalGroceryStoreIcon />
            <Typography className={classes.contentHeaderText}>Mon panier </Typography>
            <div className={classes.contentHeaderNumberPink}>
              {currentPanier && currentPanier.panierLignes ? currentPanier.panierLignes.length : 0}
            </div>
          </div>
          <div
            className={
              pathName === '/autrepanier'
                ? classes.contentHeaderItemPink
                : classes.contentHeaderItemGray
            }
            onClick={goToOtherPanier}
          >
            <div style={{ position: 'relative' }}>
              <IconPeople className={classes.iconPeople} />
              <LocalGroceryStoreIcon />
            </div>

            <Typography className={classes.contentHeaderText}>Autres paniers</Typography>
            <div className={classes.contentHeaderNumberGray}>
              {autrePanier && autrePanier.length
                ? autrePanier.filter(
                    panier => panier && panier.owner && panier.owner.id !== user.id,
                  ).length
                : 0}
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default withRouter(PanierHeader);
