import React, { FC, useState, useEffect, createRef } from 'react';
import { useStyles } from '../styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import classnames from 'classnames';
import { TextField, Dialog, DialogContent, Button, Box, Typography } from '@material-ui/core';
import { useQuery, useApolloClient } from '@apollo/client';
import CloseIcon from '@material-ui/icons/Close';
import {
  ProduitCanal,
  ProduitCanal_remises,
} from '../../../../../graphql/ProduitCanal/types/ProduitCanal';
import {
  Article_Same_Panachees,
  Article_Same_Panachees_articleSamePanachees,
} from '../../../../../graphql/ProduitCanal/types/Article_Same_Panachees';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CloseCircleIcon from '@material-ui/icons/Cancel';

interface RemisePanacheesProps {
  handleCloseModal: () => void;
  article: ProduitCanal | null;
  prixHT: number | null;
  currentRemise: string;
  remiseValue: number;
  prixNet: number | null;
  articleSamaPanachees: (Article_Same_Panachees_articleSamePanachees | null)[] | null;
}
const RemisePanachees: FC<RemisePanacheesProps & RouteComponentProps<any, any, any>> = ({
  handleCloseModal,
  article,
  prixHT,
  currentRemise,
  prixNet,
  articleSamaPanachees,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [isOpen, setIsOpen] = useState<boolean>(true);
  const remises = article && article.remises ? article.remises : null;

  const qteMin =
    remises &&
    remises
      .map((remise: ProduitCanal_remises | null) =>
        remise && remise.quantiteMin ? remise.quantiteMin : 0,
      )
      .shift();
  const closeModal = () => {
    handleCloseModal();
  };

  const keyArray = [
    {
      key:
        article && article.produit && article.produit.produitCode
          ? article.produit.produitCode.code
          : '',
      label: 'Code',
      classes: classes.remiseTableHeaderFont,
    },
    {
      key: article && article.produit ? article.produit.libelle : '',
      label: 'Libellé',
      classes: classes.small + ' , ' + classes.MontserratBold + ' , ' + classes.blue,
    },
    {
      key:
        article &&
          article.produit &&
          article.produit.produitTechReg &&
          article.produit.produitTechReg.laboExploitant
          ? article.produit.produitTechReg.laboExploitant.nomLabo
          : '',
      label: 'Laboratoire',
      classes: classes.remiseTableHeaderFont,
    },
    {
      key: currentRemise,
      label: 'Remise',
      classes:
        classes.remiseTableHeaderFont + ' , ' + classes.pink + ' , ' + classes.MontserratBold,
    },
    {
      key: prixHT,
      label: 'Tarif HT',
      classes: classes.remiseTableHeaderFont + ' , ' + classes.MontserratBold,
    },
    {
      key: prixNet + '€',
      label: 'Net HT',
      classes:
        classes.remiseTableHeaderFont + ' , ' + classes.pink + ' , ' + classes.MontserratBold,
    },

    {
      key: article ? article.qteStock : '',
      label: 'Stock plateforme',
    },
    {
      key: article ? article.qteStock : '',
      label: 'Stock pharmacie',
    },
  ];
  return (
    <Dialog open={isOpen} fullWidth={true} maxWidth="md">
      <DialogContent>
        <Box className={classes.rowTypo}>
          <Typography>
            <span className={classes.pink}>Remise Panachees</span>{' '}
            {/*à partir du jours-mois-annee*/}
          </Typography>
          <CloseIcon onClick={closeModal} />
        </Box>
        <Box className={classes.rowTypo} marginBottom="16px">
          <Box>
            {remises &&
              remises.map((remise: ProduitCanal_remises | null) => (
                <Box key={remise ? remise.id : 0}>
                  De {remise ? remise.quantiteMin : 0} unités à {remise ? remise.quantiteMax : 0}{' '}
                  unités , {remise ? remise.pourcentageRemise : 0}% de remise{' '}
                  {remise && remise.nombreUg
                    ? remise.nombreUg === 0
                      ? 'sans'
                      : remise.nombreUg
                    : 0}{' '}
                  UG
                </Box>
              ))}
          </Box>
          <Box>{qteMin} quantités minimum</Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
};

export default withRouter(RemisePanachees);
