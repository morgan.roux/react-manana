import React, { FC } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Box, Typography } from '@material-ui/core';
interface InfoContainerProps {
  canalArticle: any;
  column?: boolean;
}
const InfoContainer: FC<InfoContainerProps & RouteComponentProps<any, any, any>> = ({
  canalArticle,
  column,
}) => {
  const classes = useStyles({});
  if (column) {
    return (
      <div className={classes.infoContainerRoot}>
        <Box display="flex" marginTop="24px" marginBottom="8px">
          <Typography className={classes.label}>Code : </Typography>
          <Typography className={classes.value}>
            {canalArticle && canalArticle.produitCode && canalArticle.produitCode.code}
          </Typography>
        </Box>
        <Box display="flex" marginBottom="8px">
          <Typography className={classes.label}>STV : </Typography>
          <Typography className={classes.value}>{canalArticle.stv}</Typography>
        </Box>
        <Box display="flex" marginBottom="8px">
          <Typography className={classes.label}>Labo : </Typography>
          <Typography className={classes.value}>
            {canalArticle &&
              canalArticle.produitTechReg &&
              canalArticle.produitTechReg.laboExploitant &&
              canalArticle.produitTechReg.laboExploitant.nomLabo}
          </Typography>
        </Box>
      </div>
    );
  }

  return (
    <Box
      paddingBottom="16px"
      marginBottom="16px"
      borderBottom="1px solid #E0E0E0"
      className={classes.infoContainerRoot}
    >
      <Box display="flex" justifyContent="space-between" marginBottom="4px">
        <Box display="flex">
          <Typography className={classes.label}>Code : </Typography>
          <Typography className={classes.value}>
            {canalArticle && canalArticle.produitCode && canalArticle.produitCode.code}
          </Typography>
        </Box>
      </Box>
      <Box display="flex" justifyContent="space-between" marginBottom="4px">
        <Box display="flex">
          <Typography className={classes.label}>Labo : </Typography>
          <Typography className={classes.value}>
            {canalArticle &&
              canalArticle.produitTechReg &&
              canalArticle.produitTechReg.laboExploitant &&
              canalArticle.produitTechReg.laboExploitant.nomLabo}
          </Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default withRouter(InfoContainer);
