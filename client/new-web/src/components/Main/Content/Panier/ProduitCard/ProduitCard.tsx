import React, { FC, Fragment, useState, useEffect } from 'react';
import { Paper, Box, Typography } from '@material-ui/core';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import NoImage from '../../../../../assets/img/product_no_image.jpg';
import { ProduitCanal } from '../../../../../graphql/ProduitCanal/types/ProduitCanal';
import { Panier } from '../../../../../graphql/Panier/types/Panier';
import ArticleImage from './Image/Image';
import ArticleListBox from './ListBox/ListBox';
import ArticleButton from './Button/Button';
import ArticleHistorique from './Historique/Historique';
import { useApolloClient, useMutation } from '@apollo/client';
import {
  SEARCH as SEARCH_INTERFACE,
  SEARCHVariables,
} from '../../../../../graphql/search/types/SEARCH';
import { SEARCH } from '../../../../../graphql/search/query';
import TooltipComponent from '../Tooltip';
import InfoContainer from './InfoContainer';
import classnames from 'classnames';
import CustomButton from '../../../../Common/CustomButton';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { getUser } from '../../../../../services/LocalStorage';
import { AppAuthorization } from '../../../../../services/authorization';
import { PharmacieMinimInfo } from '../../../../../graphql/Pharmacie/types/PharmacieMinimInfo';
import { ProduitInfo } from '../../../../../graphql/Produit/types/ProduitInfo';
import { CustomFullScreenModal, CustomModal } from '../../../../Common/CustomModal';
import { CustomDatePicker } from '../../../../Common/CustomDateTimePicker';
import { UPDATE_PRODUIT_DATE_SUPPRIMER } from '../../../../../graphql/Produit/mutation';
import {
  updateProduitSupprimer,
  updateProduitSupprimerVariables,
} from '../../../../../graphql/Produit/types/updateProduitSupprimer';
import CreationProduit from '../../../../Dashboard/GestionProduits/CreationProduit/CreationProduit';

interface ArticleProps {
  currentPharmacie: PharmacieMinimInfo | null;
  currentPanier: Panier | null;
  currentCanalArticle: ProduitCanal | null | any;
  presignedUrls?: any;
  smyleys?: any;
  qteMin?: number;
  listResult?: any;
  familleResult?: any;
}

interface TooltipContentProps {
  data: any;
  familleResult?: any;
}

export const TarifHTComponent = props => {
  const { currentCanalArticle } = props;
  const classes = useStyles({});
  return (
    <Box display="flex" flexDirection="row">
      <Typography className={classnames(classes.RobotoBold, classes.prixHt)}>
        {(currentCanalArticle && currentCanalArticle.prixPhv) || 0}
      </Typography>
      <Typography className={classnames(classes.RobotoRegular, classes.prixHt)}>€ HT</Typography>
    </Box>
  );
};

export const TooltipContent: FC<TooltipContentProps> = ({ data, familleResult }) => {
  const classes = useStyles({});
  const [familles, setFamilles] = useState<any>(null);
  /* const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const currentPharmacie = myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie; */

  const arborescenceCodeFamille = (codeFamille: string[]): string[] => {
    let letter: string = '';
    const famille: string[] = [];
    codeFamille.map((fam, index) => {
      letter += fam;
      famille[index] = letter;
    });
    return famille;
  };

  const arborescence = (codeFamille: string): string | null => {
    if (codeFamille && familles) {
      const objet = familles.find((data: any) => data.codeFamille === codeFamille);
      return (objet && objet.libelleFamille) || 'Famille non définie';
    }

    return null;
  };

  useEffect(() => {
    if (
      familleResult &&
      familleResult.data &&
      familleResult.data.search &&
      familleResult.data.search
    ) {
      setFamilles(familleResult.data.search.data);
    }
  }, [familleResult]);

  return (
    <div className={classes.paper}>
      {data ? (
        <>
          <h2>Famille</h2>
          <div className={classes.content}>
            <div>
              <div className={classes.contained}>
                {data && data.famille
                  ? arborescenceCodeFamille(Array.from(data.famille.codeFamille)).map(
                    (letter, index) => {
                      return (
                        <div key={`${letter}-${index}`}>
                          <div>{arborescence(letter)}</div>
                          {index !== data.famille.codeFamille.length - 1 ? (
                            <div>&dArr;</div>
                          ) : null}
                        </div>
                      );
                    },
                  )
                  : null}
              </div>
            </div>
          </div>
        </>
      ) : null}
    </div>
  );
};

const Article: FC<ArticleProps & RouteComponentProps<any, any, any>> = ({
  history,
  location,
  currentPanier,
  currentPharmacie,
  currentCanalArticle,
  qteMin,
  listResult,
  familleResult,
}) => {
  const classes = useStyles({});
  const article = currentCanalArticle;
  const client = useApolloClient();
  const nomCanalArticle = article && article && article.libelle ? article.libelle : '';
  const isInGestionProduits = location.pathname.includes('/gestion-produits');
  const [openDialog, setOpenDialog] = useState(false);
  const [dateSupprimer, setDateSupprimer] = useState(null);
  const [openUpdateProduit, setOpenUpdateProduit] = useState(false);
  const [isAfterCreateUpdate, setIsAfterCreateUpdate] = useState(false);

  const user = getUser();
  const auth = new AppAuthorization(user);

  /* const goToDetails = () => {
    if (article) {
      history.push(`/catalogue-produits/card/${article.id}`, { from: location.pathname });
    }
  }; */
  useEffect(() => {
    if (isAfterCreateUpdate) {
      listResult.refetch();
      setIsAfterCreateUpdate(false);
    }
  }, [isAfterCreateUpdate, openUpdateProduit]);

  const handleEdit = (produit: ProduitInfo) => (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ) => {
    e.stopPropagation();

    setOpenUpdateProduit(true);
    /* history.push(`/gestion-produits/create/${produit && produit.id}`); */
  };

  const handleOpenDialog = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.stopPropagation();
    setOpenDialog(true);
  };

  const handleCloseDialog = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.stopPropagation();
    setDateSupprimer(null);
    setOpenDialog(false);
  };

  const [doUpdateDateSupprimer, resultProduitDateSupprimer] = useMutation<
    updateProduitSupprimer,
    updateProduitSupprimerVariables
  >(UPDATE_PRODUIT_DATE_SUPPRIMER, {
    update: (cache, { data }) => {
      if (
        listResult &&
        listResult.variables &&
        data &&
        data.updateProduitSupprimer &&
        data.updateProduitSupprimer.id
      ) {
        const req = cache.readQuery<SEARCH_INTERFACE, SEARCHVariables>({
          query: SEARCH,
          variables: listResult.variables,
        });
        if (req && req.search && req.search.data) {
          cache.writeQuery({
            query: SEARCH,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.map((item: any) => {
                    if (
                      item &&
                      item.id &&
                      data.updateProduitSupprimer &&
                      data.updateProduitSupprimer.id === item.id
                    )
                      return {
                        ...item,
                        supprimer: data.updateProduitSupprimer.supprimer,
                      };

                    return item;
                  }),
                },
              },
            },
            variables: listResult && listResult.variables,
          });
        }
      }
    },
    onCompleted: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'SUCCESS',
        message: 'Article supprimé avec succès',
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const handleUpdateDateSupprimer = () => {
    // do modif
    const date: any = dateSupprimer && dateSupprimer !== null ? dateSupprimer : '';
    doUpdateDateSupprimer({
      variables: { id: currentCanalArticle.id, date },
    });

    setOpenDialog(false);
  };

  const onChange = (date: any) => {
    setDateSupprimer(date);
  };

  useEffect(() => {
    if (article && article.supprimer) setDateSupprimer(article.supprimer);
  }, [article]);

  return (
    <Fragment>
      {/* <Paper className={classes.panierListeItem} onClick={goToDetails}> */}
      <Paper className={classes.panierListeItem}>
        <ArticleImage
          src={
            currentCanalArticle &&
              currentCanalArticle.produitPhoto &&
              currentCanalArticle.produitPhoto.fichier
              ? currentCanalArticle.produitPhoto.fichier.publicUrl
              : NoImage
          }
        />
        <TooltipComponent
          titleLabel={nomCanalArticle}
          famille={
            currentCanalArticle &&
            currentCanalArticle.famille &&
            currentCanalArticle.famille.libelleFamille
          }
          component={<TooltipContent data={currentCanalArticle} familleResult={familleResult} />}
        />
        <InfoContainer canalArticle={currentCanalArticle} />
        {!isInGestionProduits && (
          <ArticleListBox
            currentPanier={currentPanier}
            currentCanalArticle={currentCanalArticle}
            currentPharmacie={currentPharmacie}
            qteMin={qteMin}
          />
        )}
        <Box>
          <Box display="flex" flexDirection="row" marginTop="16px" justifyContent="space-between">
            <ArticleHistorique produit={currentCanalArticle} pharmacie={currentPharmacie} />
            {isInGestionProduits ? (
              <Box width="100%" marginLeft="8px" marginBottom="16px">
                <CustomButton
                  id={currentCanalArticle.id}
                  name={currentCanalArticle.nomCanalArticle}
                  fullWidth={true}
                  onClick={handleOpenDialog}
                  disabled={!auth.isAuthorizedToDeleteProduct()}
                >
                  Supprimer
                </CustomButton>
              </Box>
            ) : (
              <ArticleButton currentCanalArticle={currentCanalArticle} />
            )}
          </Box>

          {isInGestionProduits && (
            <CustomButton
              fullWidth={true}
              color="secondary"
              onClick={handleEdit(currentCanalArticle)}
              disabled={!auth.isAuthorizedToEditProduct()}
            >
              Modifier
            </CustomButton>
          )}

          <CustomModal
            open={openDialog}
            setOpen={setOpenDialog}
            title={`Modifier la date de suppression du produit`}
            withBtnsActions={false}
            closeIcon={false}
            headerWithBgColor={true}
            maxWidth={`xl`}
          >
            <div>
              <div style={{ margin: '15px 0px 22px 0px' }}>
                <CustomDatePicker
                  label="Date suppression"
                  placeholder="Date suppression"
                  onChange={onChange}
                  name="dateSupprimer"
                  value={dateSupprimer}
                  required={true}
                />
              </div>
              <div>
                <CustomButton
                  color="default"
                  onClick={handleCloseDialog}
                  style={{ marginRight: '25px' }}
                >
                  Annuler
                </CustomButton>
                <CustomButton
                  color="secondary"
                  onClick={handleUpdateDateSupprimer}
                  disabled={!dateSupprimer}
                >
                  Modifier
                </CustomButton>
              </div>
            </div>
          </CustomModal>
        </Box>
      </Paper>
      {openUpdateProduit && (
        <CustomFullScreenModal
          open={openUpdateProduit}
          setOpen={setOpenUpdateProduit}
          title={`Produit`}
          withBtnsActions={false}
        >
          <CreationProduit
            openCloseModal={setOpenUpdateProduit}
            setIsAfterCreateUpdate={setIsAfterCreateUpdate}
            idProduit={currentCanalArticle && currentCanalArticle.id}
          />
        </CustomFullScreenModal>
      )}
    </Fragment>
  );
};

export default withRouter(Article);
