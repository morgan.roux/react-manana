import React, { FC } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import classnames from 'classnames';
import { Box, Typography } from '@material-ui/core';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CloseCircleIcon from '@material-ui/icons/Cancel';
interface ArticleListRowProps {
  label: string;
  value: string | number;
  pink: boolean;
  onClick?: (data: any) => void;
}
const ArticleListRow: FC<ArticleListRowProps & RouteComponentProps<any, any, any>> = ({
  label,
  value,
  pink,
  onClick,
}) => {
  const classes = useStyles({});
  return (
    <Box className={classes.rowTypo}>
      <Typography className={classnames(classes.MontserratRegular, classes.small, classes.gray)}>
        {label}
      </Typography>
      {label !== 'Stock plateforme' ? (
        <Typography
          className={classnames(
            classes.MontserratRegular,
            classes.medium,
            classes.textAlignRight,
            pink ? classes.pink : classes.colorDefault,
          )}
          onClick={onClick}
        >
          {value}
        </Typography>
      ) : value > 0 ? (
        <CheckCircleIcon className={classnames(classes.stockIcon)} />
      ) : (
        <CloseCircleIcon className={classnames(classes.stockIcon)} />
      )}
    </Box>
  );
};

export default withRouter(ArticleListRow);
