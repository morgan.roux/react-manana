import React, { FC } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Box } from '@material-ui/core';

interface ArticleTitleProps {
  title: string;
  onMouse?: (data: any) => any;
}
const ArticleTitle: FC<ArticleTitleProps & RouteComponentProps<any, any, any>> = ({ title }) => {
  const classes = useStyles({});
  return <Box className={classes.panierListeItemDescription}>{title}</Box>;
};

export default withRouter(ArticleTitle);
