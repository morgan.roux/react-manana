import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    MontserratBold: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: '16px',
      textDecoration: 'underline',
      marginTop: '16px',
      marginBottom: '16px',
    },
    histoVenteAction: {
      fontSize: '0.875rem',
      color: theme.palette.text.primary,
      cursor: 'pointer',
    },
  }),
);
