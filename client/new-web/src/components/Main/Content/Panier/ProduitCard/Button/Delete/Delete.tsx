import React, { FC, useState, useEffect } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { IconButton, CircularProgress } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import { ProduitCanal } from '../../../../../../../graphql/ProduitCanal/types/ProduitCanal';
import { Panier } from '../../../../../../../graphql/Panier/types/Panier';
import { useMutation, useApolloClient } from '@apollo/client';
import { Panier_PanierLignes_panierLignes } from '../../../../../../../graphql/Panier/types/Panier_PanierLignes';
import {
  DELETE_PANIER_LIGNE,
  DELETE_PANIER_LIGNEVariables,
} from '../../../../../../../graphql/Panier/types/DELETE_PANIER_LIGNE';
import { DO_DELETE_PANIER_LIGNE } from '../../../../../../../graphql/Panier/mutation';
import { myPaniers, myPaniersVariables } from '../../../../../../../graphql/Panier/types/myPaniers';
import { GET_MY_PANIERS } from '../../../../../../../graphql/Panier/query';
import CustomButton from '../../../../../../Common/CustomButton';
import { getUser } from '../../../../../../../services/LocalStorage';
import { AppAuthorization } from '../../../../../../../services/authorization';
import { PharmacieMinimInfo } from '../../../../../../../graphql/Pharmacie/types/PharmacieMinimInfo';

interface ArticleDeleteProps {
  currentCanalArticle: ProduitCanal | null;
  currentPanier: Panier | null;
  currentPharmacie: PharmacieMinimInfo | null;
  view: string;
}

const ArticleDelete: FC<ArticleDeleteProps & RouteComponentProps<any, any, any>> = ({
  location,
  currentPanier,
  currentPharmacie,
  currentCanalArticle,
  view,
}) => {
  const classes = useStyles({});
  const pathName = location.pathname;
  const client = useApolloClient();

  const user = getUser();
  const auth = new AppAuthorization(user);

  const getPanierLigneItem = () => {
    if (currentCanalArticle && currentPanier && currentPanier.panierLignes) {
      const currentLigne = currentPanier.panierLignes.find(
        (ligne: Panier_PanierLignes_panierLignes | null) =>
          ligne && ligne.produitCanal && ligne.produitCanal.id === currentCanalArticle.id,
      );
      return currentLigne ? currentLigne : null;
    } else return null;
  };

  const [currentLigne, setCurrentLigne] = useState<Panier_PanierLignes_panierLignes | null>(
    getPanierLigneItem(),
  );

  useEffect(() => {
    setCurrentLigne(getPanierLigneItem());
  }, [currentPanier]);

  /* DELETING PANIER LIGNE */
  const [isDeleting, setIsDeleting] = useState<boolean>(false);

  const [doDeletePanierLigne, DeletePanierLigneResult] = useMutation<
    DELETE_PANIER_LIGNE,
    DELETE_PANIER_LIGNEVariables
  >(DO_DELETE_PANIER_LIGNE, {
    update: (cache, { data }) => {
      const req = cache.readQuery<myPaniers, myPaniersVariables>({
        query: GET_MY_PANIERS,
        variables: {
          idPharmacie: (currentPharmacie && currentPharmacie.id) || '',
          take: 10,
          skip: 0,
        },
      });
      if (req && req.myPaniers) {
        cache.writeQuery({
          query: GET_MY_PANIERS,
          data: req.myPaniers.map(panier => {
            if (
              panier &&
              data &&
              data.deletePanierLigne &&
              currentPanier &&
              data.deletePanierLigne.id === currentPanier.id
            ) {
              return data.deletePanierLigne;
            } else return panier;
          }),
          variables: {
            idPharmacie: (currentPharmacie && currentPharmacie.id) || '',
            take: 10,
            skip: 0,
          },
        });
      }
    },
    onCompleted: data => {
      const panier = data && data.deletePanierLigne ? data.deletePanierLigne : currentPanier;
      // TODO: Migration
      //(client as any).writeData({ data: { panier } });
      setIsDeleting(false);
    },
  });

  const handleDeleteArticle = (e: any) => {
    e.stopPropagation();
    setIsDeleting(true);
    doDeletePanierLigne({
      variables: {
        id: currentLigne && currentLigne.id ? currentLigne.id : '',
        idPharmacie: currentPharmacie ? currentPharmacie.id : '',
        take: 10,
        skip: 0,
      },
    });
  };

  const disabledBtn = (): boolean => {
    if (!auth.isAuthorizedToRemoveProductInCart()) {
      return true;
    }
    return false;
  };

  return !pathName.includes('/operations-commerciales') ? (
    <div>
      {view === 'list' ? (
        <IconButton color="primary" onClick={handleDeleteArticle} disabled={disabledBtn()}>
          {isDeleting ? <CircularProgress size={20} color="secondary" /> : <DeleteIcon />}
        </IconButton>
      ) : (
        <CustomButton
          startIcon={isDeleting ? <CircularProgress size={20} color="secondary" /> : <DeleteIcon />}
          className={classes.btnDelete}
          color="primary"
          onClick={handleDeleteArticle}
          disabled={disabledBtn()}
        >
          ANNULER
        </CustomButton>
      )}
    </div>
  ) : null;
};

export default withRouter(ArticleDelete);
