import React, { FC, useState, useEffect } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import classnames from 'classnames';
import { CircularProgress, Tooltip, Fade, IconButton } from '@material-ui/core';
import { useMutation, useApolloClient, useQuery } from '@apollo/client';
import { Panier_PanierLignes_panierLignes } from '../../../../../../graphql/Panier/types/Panier_PanierLignes';
import {
  ADD_TO_PANIER,
  DO_TAKE_PRODUCT_TO_MY_PANIER,
} from '../../../../../../graphql/Panier/mutation';
import { Panier } from '../../../../../../graphql/Panier/types/Panier';
import {
  addToPanier,
  addToPanierVariables,
} from '../../../../../../graphql/Panier/types/addToPanier';
import {
  takeProductToMyPanier,
  takeProductToMyPanierVariables,
} from '../../../../../../graphql/Panier/types/takeProductToMyPanier';
import { ProduitCanal } from '../../../../../../graphql/ProduitCanal/types/ProduitCanal';
import { getUser } from '../../../../../../services/LocalStorage';
import { myPaniers, myPaniersVariables } from '../../../../../../graphql/Panier/types/myPaniers';
import { GET_MY_PANIERS } from '../../../../../../graphql/Panier/query';
import {
  ADMINISTRATEUR_GROUPEMENT,
  COLLABORATEUR_COMMERCIAL,
  COLLABORATEUR_NON_COMMERCIAL,
  GROUPEMENT_AUTRE,
} from '../../../../../../Constant/roles';
import { ShoppingCart } from '@material-ui/icons';
import ArticleDelete from './Delete/Delete';
import CustomButton from '../../../../../Common/CustomButton';
import ICurrentPharmacieInterface from '../../../../../../Interface/CurrentPharmacieInterface';
import { GET_CURRENT_PHARMACIE } from '../../../../../../graphql/Pharmacie/local';
import { AppAuthorization } from '../../../../../../services/authorization';
import { PharmacieMinimInfo } from '../../../../../../graphql/Pharmacie/types/PharmacieMinimInfo';

interface ArticleButtonProps {
  currentCanalArticle: ProduitCanal | null;
  view?: string;
}

const ArticleButton: FC<ArticleButtonProps & RouteComponentProps> = ({
  history: { location },

  currentCanalArticle,
  view,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();

  const user = getUser();
  const auth = new AppAuthorization(user);

  const { pathname } = location;
  const state: any = location.state;

  const qteStock =
    currentCanalArticle && currentCanalArticle.qteStock ? currentCanalArticle.qteStock : 0;
  const viewCard: boolean = pathname.includes('card');

  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const myPanierResult = useQuery<myPaniers, myPaniersVariables>(GET_MY_PANIERS, {
    variables: {
      idPharmacie:
        (myPharmacie &&
          myPharmacie.data &&
          myPharmacie.data.pharmacie &&
          myPharmacie.data.pharmacie.id) ||
        '',
      take: 10,
      skip: 0,
    },
    // skip: listResult && listResult.data && !listResult.data.search,
  });

  // currentPharmacie
  const currentPharmacie: PharmacieMinimInfo | null =
    (myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie) || null;

  // currentPanier
  const currentPanier: Panier | null =
    ({
      panierLignes:
        (myPanierResult &&
          myPanierResult.data &&
          myPanierResult.data.myPaniers &&
          myPanierResult.data.myPaniers
            .map(panier => panier && panier.panierLignes)
            .reduce((total, value) => {
              return total && total.concat(value);
            }, [])) ||
        [],
    } as Panier) || null;

  // get last path
  const lastPath = state && state.from ? state.from : '';

  // disable quantite change if user is groupement collaborateur
  const isChangeDisabled =
    user &&
      user.role &&
      user.role.code &&
      user.role.code !== COLLABORATEUR_COMMERCIAL &&
      user.role.code !== ADMINISTRATEUR_GROUPEMENT &&
      user.role.code !== COLLABORATEUR_NON_COMMERCIAL &&
      user.role.code !== GROUPEMENT_AUTRE
      ? false
      : true;

  const getPanierLigneItem = () => {
    if (currentCanalArticle && currentPanier && currentPanier.panierLignes) {
      const currentLigne = currentPanier.panierLignes.find(
        (ligne: Panier_PanierLignes_panierLignes | null) =>
          ligne && ligne.produitCanal && ligne.produitCanal.id === currentCanalArticle.id,
      );
      return currentLigne ? currentLigne : null;
    } else return null;
  };

  const [currentLigne, setCurrentLigne] = useState<Panier_PanierLignes_panierLignes | null>(
    getPanierLigneItem(),
  );

  useEffect(() => {
    setCurrentLigne(getPanierLigneItem());
  }, [currentPanier]);

  /* ADDING PANIER LIGNE */
  const [isAdding, setIsAdding] = useState<boolean>(false);

  const [doAddToPanier, AddToPanierResult] = useMutation<addToPanier, addToPanierVariables>(
    ADD_TO_PANIER,

    {
      update: (cache, { data }) => {
        if (data && data.addToPanier && currentCanalArticle && currentPharmacie) {
          const req = cache.readQuery<myPaniers, myPaniersVariables>({
            query: GET_MY_PANIERS,
            variables: {
              idPharmacie: currentPharmacie.id || '',
              take: 10,
              skip: 0,
            },
          });

          cache.writeQuery({
            query: GET_MY_PANIERS,
            data: {
              myPaniers:
                req && req.myPaniers && req.myPaniers.length
                  ? req.myPaniers.map(panier => {
                    if (panier && data && data.addToPanier && panier.id === data.addToPanier.id) {
                      return data.addToPanier;
                    }
                    return panier;
                  })
                  : [data.addToPanier],
            },
            variables: {
              idPharmacie: currentPharmacie.id || '',
              take: 10,
              skip: 0,
            },
          });
        }
      },
      onCompleted: data => {
        const panier = data && data.addToPanier ? data.addToPanier : currentPanier;

        // TODO: Migration
        //(client as any).writeData({ data: { panier } });
        setIsAdding(false);
      },
    },
  );
  const [doTakeProductToMyPanier, TakeProductToMyPanierResult] = useMutation<
    takeProductToMyPanier,
    takeProductToMyPanierVariables
  >(DO_TAKE_PRODUCT_TO_MY_PANIER, {
    update: (cache, { data }) => {
      if (data && data.takeProductToMyPanier && currentCanalArticle && currentPharmacie) {
        const req = cache.readQuery<myPaniers, myPaniersVariables>({
          query: GET_MY_PANIERS,
          variables: {
            idPharmacie: currentPharmacie.id || '',
            take: 10,
            skip: 0,
          },
        });

        cache.writeQuery({
          query: GET_MY_PANIERS,
          data: {
            myPaniers:
              req && req.myPaniers && req.myPaniers.length
                ? req.myPaniers.map(panier => {
                  if (
                    panier &&
                    data &&
                    data.takeProductToMyPanier &&
                    panier.id === data.takeProductToMyPanier.id
                  ) {
                    return data.takeProductToMyPanier;
                  }
                  return panier;
                })
                : [data.takeProductToMyPanier],
          },
          variables: {
            idPharmacie: currentPharmacie.id || '',
            take: 10,
            skip: 0,
          },
        });
      }
    },
    onCompleted: data => {
      const panier =
        data && data.takeProductToMyPanier ? data.takeProductToMyPanier : currentPanier;

        // TODO: Migration
      //(client as any).writeData({ data: { panier } });
      setIsAdding(false);
    },
  });

  const handleAjouter = (e: any) => {
    e.stopPropagation();
    if (
      currentCanalArticle &&
      currentCanalArticle.qteStock &&
      currentCanalArticle.stv &&
      currentCanalArticle.qteStock >= currentCanalArticle.stv
    ) {
      setIsAdding(true);
      if (currentCanalArticle.inOtherPanier && currentCanalArticle.inOtherPanier.id) {
        doTakeProductToMyPanier({
          variables: {
            id: currentCanalArticle.inOtherPanier.id,
            idPharmacie: currentPharmacie ? currentPharmacie.id : '',
            codeCanal:
              (currentCanalArticle &&
                currentCanalArticle.commandeCanal &&
                currentCanalArticle.commandeCanal.code) ||
              '',
            take: 10,
            skip: 0,
          },
        });
      } else {
        doAddToPanier({
          variables: {
            idCanalArticle: currentCanalArticle ? currentCanalArticle.id : '',
            idPharmacie: currentPharmacie ? currentPharmacie.id : '',
            codeCanal:
              (currentCanalArticle &&
                currentCanalArticle.commandeCanal &&
                currentCanalArticle.commandeCanal.code) ||
              '',
            take: 10,
            skip: 0,
          },
        });
      }
    }
  };

  const disabledAddToCartBtn = (): boolean => {
    if (!auth.isAuthorizedToAddProductToCart() || qteStock === 0 || isChangeDisabled) {
      return true;
    }
    return false;
  };

  if (
    !pathname.includes('/operation-produits') &&
    lastPath !== '/suivi-commandes' &&
    lastPath !== '/create/operations-commerciales'
  ) {
    if (currentLigne) {
      return (
        <ArticleDelete
          view={viewCard ? 'card' : 'list'}
          currentPanier={currentPanier}
          currentCanalArticle={currentCanalArticle}
          currentPharmacie={currentPharmacie}
        />
      );
    } else {
      if (qteStock === 0) {
        return (
          <>
            {view === 'table' ? (
              <IconButton disabled={true} color="secondary">
                <ShoppingCart />
              </IconButton>
            ) : (
              <CustomButton
                startIcon={<ShoppingCart />}
                className={classnames(classes.disabledButton)}
                disabled={true}
              >
                AJOUTER
              </CustomButton>
            )}
          </>
        );
      } else {
        return isAdding ? (
          <>
            {view === 'table' ? (
              <IconButton>
                <CircularProgress color="secondary" size={20} />
              </IconButton>
            ) : (
              <CustomButton
                color="secondary"
                onClick={handleAjouter}
                className={classnames(classes.btnAddPanier)}
                disabled={isChangeDisabled ? true : false}
              >
                <CircularProgress color="inherit" size={20} />
              </CustomButton>
            )}
          </>
        ) : currentCanalArticle && currentCanalArticle.inOtherPanier ? (
          <CustomButton
            color="secondary"
            onClick={handleAjouter}
            className={classnames(classes.btnAddPanier)}
            disabled={disabledAddToCartBtn()}
          >
            Envoyer à mon panier
          </CustomButton>
        ) : (
          <>
            {view === 'table' ? (
              <Tooltip
                TransitionComponent={Fade}
                TransitionProps={{ timeout: 600 }}
                title="Ajouter à mon panier"
              >
                <IconButton
                  onClick={handleAjouter}
                  color="secondary"
                  disabled={disabledAddToCartBtn()}
                >
                  <ShoppingCart />
                </IconButton>
              </Tooltip>
            ) : (
              <CustomButton
                color="secondary"
                onClick={handleAjouter}
                className={classnames(classes.btnAddPanier)}
                disabled={disabledAddToCartBtn()}
                startIcon={<ShoppingCart />}
              >
                AJOUTER
              </CustomButton>
            )}
          </>
        );
      }
    }
  } else return <></>;
};

export default withRouter(ArticleButton);
