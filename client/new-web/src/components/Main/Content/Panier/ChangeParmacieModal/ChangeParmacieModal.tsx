import React, { FC, useState, useEffect, Dispatch, SetStateAction } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Typography } from '@material-ui/core';
import { PharmacieMinimInfo } from '../../../../../graphql/Pharmacie/types/PharmacieMinimInfo';
import ICurrentPharmacieInterface from '../../../../../Interface/CurrentPharmacieInterface';
import { GET_CURRENT_PHARMACIE } from '../../../../../graphql/Pharmacie/local';
import { useQuery, useApolloClient, useLazyQuery } from '@apollo/client';
import { GET_MY_PANIER } from '../../../../../graphql/Panier/query';
import {
  myPanier,
  myPanier_myPanier,
  myPanierVariables,
} from '../../../../../graphql/Panier/types/myPanier';
import { setPharmacie, getGroupement, getUser } from '../../../../../services/LocalStorage';
import { CustomFullScreenModal } from '../../../../Common/CustomModal';
import ConfirmDialog from '../../../../Common/ConfirmDialog';
import { pharmacieColumns } from '../../Pilotage/Cible/Steps/Pharmacie/Pharmacie';
import WithSearch, { GET_SKIP_AND_TAKE } from '../../../../Common/newWithSearch/withSearch';
import CustomContent from '../../../../Common/newCustomContent';
import { GET_SEARCH_CUSTOM_CONTENT_PHARMACIE } from '../../../../../graphql/Pharmacie/query';
import useStyles from './styles';
import SearchInput from '../../../../Common/newCustomContent/SearchInput';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import DetailsTitulaire from '../../../../Dashboard/TitulairesPharmacies/FicheTitulaire';
import FichePharmacie from '../../../../Dashboard/Pharmacies/FichePharmacie';
import { resetSearchFilters } from '../../../../Common/withSearch/withSearch';
import { GET_LOCAL_PANIER } from '../../../../../graphql/Panier/local';

interface ChangeParmacieModalProps {
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
  openDialog: (value: boolean) => void;
}

const PharmacieTable = ({ clickedPharmacie, onClickPharmacie, groupement, push }) => {
  const [openModalTitulaire, setOpenModalTitulaire] = useState<boolean>(false);
  const [titulaireId, setTitulaireId] = useState<string>('');

  const [openModalPharmacie, setOpenModalPharmacie] = useState<boolean>(false);
  const [pharmacieId, setPharmacieId] = useState<string>('');

  const user = getUser();

  const pharmaciePartenaires = user.pharmaciePartenaires;

  const handleOpenTitulaireDetails = (id: string) => {
    setTitulaireId(id);
    setOpenModalTitulaire(true);
  };

  const handleOpenPharmacieDetails = (id: string) => {
    setPharmacieId(id);
    setOpenModalPharmacie(true);
  };

  const closeModalPharma = () => {
    setOpenModalPharmacie(false);
    // setPharmacieId('');
  };

  const childrenProps: any = {
    isSelectable: false,
    paginationCentered: true,
    columns: pharmacieColumns(push, true, handleOpenTitulaireDetails, handleOpenPharmacieDetails),
    activeRow: clickedPharmacie,
    onClickRow: (row: any) => onClickPharmacie(row),
  };

  const must =
    pharmaciePartenaires?.length > 0
      ? [
          { term: { idGroupement: groupement && groupement.id } },
          { terms: { _id: pharmaciePartenaires.map(pharmacie => pharmacie.id) } },
        ]
      : [{ term: { idGroupement: groupement && groupement.id } }];

  return (
    <>
      <WithSearch
        type={'pharmacie'}
        props={childrenProps}
        WrappedComponent={CustomContent}
        searchQuery={GET_SEARCH_CUSTOM_CONTENT_PHARMACIE}
        optionalMust={must}
      />
      <CustomFullScreenModal
        title="Fiche Titulaire"
        open={openModalTitulaire}
        setOpen={setOpenModalTitulaire}
        closeIcon={true}
        withBtnsActions={false}
        headerWithBgColor={true}
        fullScreen={true}
      >
        <DetailsTitulaire id={titulaireId} hideFooterActions={true} />
      </CustomFullScreenModal>
      <CustomFullScreenModal
        title="Détails de pharmacie"
        open={openModalPharmacie}
        setOpen={setOpenModalPharmacie}
        withBtnsActions={false}
        fullScreen={true}
        withHeader={false}
      >
        <FichePharmacie id={pharmacieId} inModalView={true} handleClickBack={closeModalPharma} />
      </CustomFullScreenModal>
    </>
  );
};

const ChangeParmacieModal: FC<ChangeParmacieModalProps & RouteComponentProps<any, any, any>> = ({
  openDialog,
  open,
  setOpen,
  history: { push },
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const groupement = getGroupement();
  const [openConfirm, setConfirm] = useState<boolean>(false);
  const [clickedPharmacie, setClickedPharmacie] = useState<any>(null);

  // Take currentPharmacie
  const [currentPharmacie, setCurrentPharmacie] = useState<PharmacieMinimInfo | null>(null);
  const [selectedPharmacie, setSelectedPharmacie] = useState<PharmacieMinimInfo | null>(null);
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

  useEffect(() => {
    if (myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie) {
      setCurrentPharmacie(myPharmacie.data.pharmacie);
    }
  }, [myPharmacie]);

  useEffect(() => {
    if (currentPharmacie && currentPharmacie.id) {
      setSelectedPharmacie(currentPharmacie);
      setPharmacie(currentPharmacie);
      myPanier();
    }
  }, [currentPharmacie]);

  // myPanier initialisation
  const [myPanier, myPanierResult] = useLazyQuery<myPanier, myPanierVariables>(GET_MY_PANIER, {
    variables: { idPharmacie: selectedPharmacie ? selectedPharmacie.id : '' },
    fetchPolicy: 'network-only',
  });

  useEffect(() => {
    const result =
      myPanierResult && myPanierResult.data && myPanierResult.data.myPanier
        ? myPanierResult.data.myPanier
        : null;
    setCurrentPanier(result);
  }, [myPanierResult]);

  useEffect(() => {
    if (open) {
      resetSearchFilters(client);
      client.writeQuery({
        query: GET_SKIP_AND_TAKE,
        data: {
          skipAndTake: {
            skip: 0,
            take: 12,
          },
        },
      });
    }
  }, [open]);

  const setCurrentPanier = (panier: myPanier_myPanier | null) => {
    client.writeQuery({
      query: GET_LOCAL_PANIER,
      data: { panier },
    });
  };

  const handleClose = () => {
    if (!currentPharmacie) {
      return;
    }
    openDialog(false);
  };

  const handleSubmit = () => {
    if (!currentPharmacie) {
      return;
    }
    client.writeQuery({ query: GET_CURRENT_PHARMACIE, data: { pharmacie: currentPharmacie } });

    setConfirm(false);
    openDialog(false);
    displaySnackBar(client, {
      isOpen: true,
      type: 'SUCCESS',
      message: 'Changement de pharmacie réussi',
    });
    window.location.reload();
  };

  const onClickPharmacie = (row: any) => {
    if (row) {
      setClickedPharmacie(row);
      const pharma: PharmacieMinimInfo = {
        __typename: 'Pharmacie',
        cip: row.cip,
        nom: row.nom,
        adresse1: row.adresse1,
        id: row.id,
        type: row.type,
        ville: row.ville,
        departement: null,
      };
      setCurrentPharmacie(pharma);
    }

    // Open confirm modal
    setConfirm(true);
  };

  const closeConfirm = () => {
    setConfirm(false);
    setClickedPharmacie(null);
  };

  const ConfirmDialogContent = () => {
    return (
      <Typography style={{ marginBottom: 20 }}>
        Êtes-vous sur de vouloir changer de pharmacie ?
      </Typography>
    );
  };

  return (
    <CustomFullScreenModal
      open={open}
      setOpen={setOpen}
      title="Changer de pharmacie"
      withBtnsActions={false}
    >
      <div className={classes.container}>
        <SearchInput searchPlaceholder="Rechercher" />
        <PharmacieTable
          clickedPharmacie={clickedPharmacie}
          onClickPharmacie={onClickPharmacie}
          groupement={groupement}
          push={push}
        />
      </div>
      <ConfirmDialog
        open={openConfirm}
        message={<ConfirmDialogContent />}
        handleClose={closeConfirm}
        handleValidate={handleSubmit}
      />
    </CustomFullScreenModal>
  );
};

export default withRouter(ChangeParmacieModal);
