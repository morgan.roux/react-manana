import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      width: '100%',
      minHeight: 'calc(100vh - 64px)',
      display: 'flex',
      flexDirection: 'column',
      padding: '30px 25px',
      '& > div:nth-child(1)': {
        maxWidth: 500,
        marginBottom: 25,
        '& > div': {
          height: 50,
        },
      },
      '& > div:nth-child(2)': {
        '& > div': {
          padding: 0,
          '& tbody > tr.MuiTableRow-root > td': {
            // height: 50,
            padding: '14px 12px',
          },
        },
      },
    },
  }),
);

export default useStyles;
