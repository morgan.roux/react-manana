import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
    },
    form: {
      margin: '40px',
      maxWidth: 345,
    },
    productList: {
      display: 'flex',
      flexWrap: 'wrap',
    },
  }),
);
