import Tooltip from '@material-ui/core/Tooltip';
import { withStyles, createStyles } from '@material-ui/core/styles';
import { makeStyles, Theme } from '@material-ui/core';

export const HtmlTooltip = withStyles(theme => ({
  tooltip: {
    backgroundColor: '#f5f5f9',
    color: 'rgba(0, 0, 0, 0.87)',
    // fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
  },
}))(Tooltip);

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      flexDirection: 'column',

      marginTop: 8,
      marginBottom: 8,
    },
    title: {
      fontSize: 14,
      cursor: 'pointer',
    },
    RobotoBold: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
    },
  }),
);
