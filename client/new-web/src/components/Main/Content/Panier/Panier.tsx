import { useApolloClient, useMutation, useQuery } from '@apollo/client';
import { Box, CircularProgress } from '@material-ui/core';
import classnames from 'classnames';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { GET_COMMANDCANALS } from '../../../../graphql/CommandeCanal/query';
import { commandeCanals } from '../../../../graphql/CommandeCanal/types/commandeCanals';
import { DO_CLEAR_MY_PANIER } from '../../../../graphql/Panier/mutation';
import { GET_MY_PANIER, GET_PHARMACIE_PANIER } from '../../../../graphql/Panier/query';
import {
  clearMyPanier,
  clearMyPanierVariables,
} from '../../../../graphql/Panier/types/clearMyPanier';
import { myPanier, myPanierVariables } from '../../../../graphql/Panier/types/myPanier';
import {
  getPharmaciePanier,
  getPharmaciePanierVariables,
} from '../../../../graphql/Panier/types/getPharmaciePanier';
import { GET_CURRENT_PHARMACIE } from '../../../../graphql/Pharmacie/local';
import { MY_PHARMACIE_me_pharmacie } from '../../../../graphql/Pharmacie/types/MY_PHARMACIE';
import { DO_CREATE_GET_PRESIGNED_URL } from '../../../../graphql/S3';
import {
  CREATE_GET_PRESIGNED_URL,
  CREATE_GET_PRESIGNED_URLVariables,
} from '../../../../graphql/S3/types/CREATE_GET_PRESIGNED_URL';
import { Panier as MyPanier } from '../../../../graphql/Panier/types/Panier';
import { GET_SMYLEYS } from '../../../../graphql/Smyley/query';
import { SMYLEYS, SMYLEYSVariables } from '../../../../graphql/Smyley/types/SMYLEYS';
import ICurrentPharmacieInterface from '../../../../Interface/CurrentPharmacieInterface';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { getGroupement, getUser } from '../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop';
import Article from './Article/Article';
import PanierHeader from './PanierHeader/PanierHeader';
import PanierResume from './PanierResume/PanierResume';
import { useStyles } from './styles';
import { TextFieldCommandeCanalPanier } from './TextFieldCommandeCanalPanier/TextFieldCommandeCanalPanier';
import NoItemContentImage from '../../../Common/NoItemContentImage';
import CustomButton from '../../../Common/CustomButton';
import { AppAuthorization } from '../../../../services/authorization';

interface PanierProps {
  match: {
    params: { type: string | undefined; id: string };
  };
}

const Panier: FC<PanierProps & RouteComponentProps> = () => {
  const classes = useStyles({});
  const client = useApolloClient();

  const user = getUser();
  const auth = new AppAuthorization(user);

  const [canal, setCanal] = useState<string>('PFL');

  const { data } = useQuery<commandeCanals>(GET_COMMANDCANALS);

  // myPanier initialisation
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const myPanierQuery = useQuery<myPanier, myPanierVariables>(GET_MY_PANIER, {
    variables: {
      idPharmacie:
        (myPharmacie &&
          myPharmacie.data &&
          myPharmacie.data.pharmacie &&
          myPharmacie.data.pharmacie.id) ||
        '',
      codeCanal: canal,
    },
    fetchPolicy: 'cache-and-network',
  });

  const [currentPanier, setCurrentPanier] = useState<MyPanier | null>(null);

  // take currentPanier
  // let currentPanier: MyPanier | null =
  //   myPanierQuery && myPanierQuery.data && myPanierQuery.data.myPanier
  //     ? myPanierQuery.data.myPanier
  //     : null;

  // take currentPharmacie
  const currentPharmacie: MY_PHARMACIE_me_pharmacie | null =
    (myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie && myPharmacie.data.pharmacie) ||
    null;

  // take panier list
  const panierList = useQuery<getPharmaciePanier, getPharmaciePanierVariables>(
    GET_PHARMACIE_PANIER,
    {
      variables: { idPharmacie: currentPharmacie ? currentPharmacie.id : '', take: 10, skip: 0 },
      fetchPolicy: 'cache-and-network',
    },
  );

  const groupement = getGroupement();
  // Get smyleys by groupement
  const getSmyleys = useQuery<SMYLEYS, SMYLEYSVariables>(GET_SMYLEYS, {
    variables: {
      idGroupement: groupement && groupement.id,
    },
  });

  const [doCreateGetPresignedUrls, doCreateGetPresignedUrlsResult] = useMutation<
    CREATE_GET_PRESIGNED_URL,
    CREATE_GET_PRESIGNED_URLVariables
  >(DO_CREATE_GET_PRESIGNED_URL);

  useEffect(() => {
    if (myPanierQuery && myPanierQuery.data && myPanierQuery.data.myPanier) {
      setCurrentPanier(myPanierQuery.data.myPanier);
    }
  }, [myPanierQuery]);

  const [doClearMyPanier, resultClearMyPanier] = useMutation<clearMyPanier, clearMyPanierVariables>(
    DO_CLEAR_MY_PANIER,
    {
      onCompleted: () => {
        setCurrentPanier(null);
      },
    },
  );

  const smyleys = getSmyleys && getSmyleys.data && getSmyleys.data.smyleys;
  const presignedUrls =
    doCreateGetPresignedUrlsResult &&
    doCreateGetPresignedUrlsResult.data &&
    doCreateGetPresignedUrlsResult.data.createGetPresignedUrls;

  useEffect(() => {
    const filePaths: string[] = [];
    if (smyleys) {
      smyleys.map((smyley: any) => {
        if (smyley) filePaths.push(smyley.photo);
      });
      if (filePaths.length > 0) {
        // create presigned
        doCreateGetPresignedUrls({ variables: { filePaths } });
      }
    }
  }, [getSmyleys.data]);

  const handleViderPanier = () => {
    if (currentPharmacie && currentPharmacie.id) {
      doClearMyPanier({
        variables: {
          idPharmacie: currentPharmacie.id,
          codeCanal: canal,
        },
      });
    }
  };

  const handleChoiceCanal = (event: any) => {
    setCanal(event.target.value);
  };

  const disabledEmptyBtn = (): boolean => {
    if (!auth.isAuthorizedToEmptyCart()) {
      return true;
    }

    if (
      !currentPanier ||
      (currentPanier && !currentPanier.panierLignes) ||
      (currentPanier && currentPanier.panierLignes && !currentPanier.panierLignes.length)
    ) {
      return true;
    }

    return false;
  };

  useEffect(() => {
    if (data && data.commandeCanals) {
      const currentCommandeCanal = data.commandeCanals.find(
        commandeCanal => commandeCanal && commandeCanal.code === canal,
      );
      if (currentCommandeCanal && currentCommandeCanal.code && currentCommandeCanal.id) {
        // TODO: Migration
        /*(client as any).writeData({
          data: {
            CurrentCanalCode: {
              id: currentCommandeCanal.id,
              code: currentCommandeCanal.code,
              __typename: 'CurrentCanalCode',
            },
          },
        });*/
      }
    }
  }, [canal, data]);

  if (resultClearMyPanier.loading) {
    return <Backdrop />;
  } else if (resultClearMyPanier.data) {
    const snackBarData: SnackVariableInterface = {
      type: 'SUCCESS',
      message: `Votre panier a été vidé`,
      isOpen: true,
    };

    displaySnackBar(client, snackBarData);
  }

  return (
    <Box height="100%">
      {/*<PanierToolbar choosePharmacie={editParam ? true : false} />*/}
      <PanierHeader
        currentPanier={currentPanier}
        autrePanier={
          panierList && panierList.data && panierList.data.pharmaciePanier
            ? panierList.data.pharmaciePanier
            : null
        }
      />
      <div className={classes.contentBodyContainer} style={{ paddingBottom: 16 }}>
        {(myPanierQuery && myPanierQuery.loading) || (panierList && panierList.loading) ? (
          <Box width="100%" height="100%" justifyContent="center" alignItems="center">
            <CircularProgress />
          </Box>
        ) : (
          <Box className={classes.panierListe} display="flex" flexDirection="column">
            <TextFieldCommandeCanalPanier
              handleChange={handleChoiceCanal}
              currencies={data}
              label="Canal de commande"
              value={canal}
            />
            <Box
              marginBottom="20px"
              display="flex"
              justifyContent="flex-start"
              width="100%"
              marginTop="20px"
            >
              {currentPanier && currentPanier.panierLignes && currentPanier.panierLignes.length && (
                <CustomButton
                  color="secondary"
                  onClick={handleViderPanier}
                  disabled={disabledEmptyBtn()}
                >
                  VIDER MON PANIER
                </CustomButton>
              )}
            </Box>
            <Box className={classes.contentPannierList}>
              {currentPanier && currentPanier.panierLignes && currentPanier.panierLignes.length ? (
                currentPanier.panierLignes.map((ligne: any | null) => {
                  if (
                    ligne &&
                    ligne.produitCanal &&
                    ligne.produitCanal.commandeCanal &&
                    ligne.produitCanal.commandeCanal.code === canal
                  ) {
                    return (
                      <Article
                        key={ligne.id}
                        currentPharmacie={currentPharmacie}
                        currentPanier={currentPanier}
                        currentCanalArticle={ligne.produitCanal}
                        presignedUrls={presignedUrls}
                        smyleys={smyleys}
                      />
                    );
                  }
                })
              ) : (
                <NoItemContentImage
                  title="Vous n'avez aucun produit à commander"
                  subtitle="Merci d'ajouter des produits pour passer commande"
                />
              )}
            </Box>
          </Box>
        )}
        {currentPanier && currentPanier.panierLignes && currentPanier.panierLignes.length && (
          <PanierResume
            currentPanier={myPanierQuery && myPanierQuery.data && myPanierQuery.data.myPanier}
          />
        )}
      </div>
    </Box>
  );
};

export default withRouter(Panier);
