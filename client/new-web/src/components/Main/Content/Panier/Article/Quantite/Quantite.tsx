import React, { FC, useState, useEffect } from 'react';
import { useStyles } from '../../styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { IconButton, CircularProgress, InputBase, Box, Typography } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import { useMutation, useApolloClient, useQuery } from '@apollo/client';
import { Article_Same_Panachees_articleSamePanachees } from '../../../../../../graphql/Panier/types/Article_Same_Panachees';
import SaveIcon from '@material-ui/icons/Save';
import { Panier_PanierLignes_panierLignes } from '../../../../../../graphql/Panier/types/Panier_PanierLignes';
import { DO_UPDATE_QUANTITE, ADD_TO_PANIER } from '../../../../../../graphql/Panier/mutation';
import {
  UPDATE_QUANTITE,
  UPDATE_QUANTITEVariables,
} from '../../../../../../graphql/Panier/types/UPDATE_QUANTITE';
import { Panier } from '../../../../../../graphql/Panier/types/Panier';
import {
  addToPanier,
  addToPanierVariables,
} from '../../../../../../graphql/Panier/types/addToPanier';
import gql from 'graphql-tag';
import { ProduitCanal } from '../../../../../../graphql/ProduitCanal/types/ProduitCanal';
import classnames from 'classnames';
import { getUser } from '../../../../../../services/LocalStorage';
import { myPaniers, myPaniersVariables } from '../../../../../../graphql/Panier/types/myPaniers';
import { GET_MY_PANIERS } from '../../../../../../graphql/Panier/query';
import {
  ADMINISTRATEUR_GROUPEMENT,
  COLLABORATEUR_COMMERCIAL,
  COLLABORATEUR_NON_COMMERCIAL,
  GROUPEMENT_AUTRE,
} from '../../../../../../Constant/roles';
import ICurrentPharmacieInterface from '../../../../../../Interface/CurrentPharmacieInterface';
import { GET_CURRENT_PHARMACIE } from '../../../../../../graphql/Pharmacie/local';
import { GET_OPERATION_PANIER } from '../../../../../../graphql/OperationCommerciale/local';
import { PharmacieMinimInfo } from '../../../../../../graphql/Pharmacie/types/PharmacieMinimInfo';

interface QuantiteProps {
  currentCanalArticle: ProduitCanal | null;
  // currentPharmacie: Pharmacie | null;
  // currentPanier: Panier | null;
  acceptZero?: boolean;
  handleParentQuantite?: (article: any, qte: number) => void;
  parentQuantite?: number;
  qteMin?: number;
  match: {
    params: {
      idOperation: string | undefined;
    };
  };
}

export interface ArticleOC {
  id: string;
  quantite: number;
  article: {
    id: string;
    prixPhv: number;
    remises: {
      id: string;
      quantiteMin: number;
      quantiteMax: number;
      pourcentageRemise: number;
      nombreUg: number;
      codeCipUg: number;
      codeCip13Ug: string;
    };
  };
}

export interface ArticleOCArray {
  articleOcArray: ArticleOC[];
}
export const GET_ARTICLE_OC_ARRAY = gql`
  {
    articleOcArray @client {
      id
      quantite
      produitCanal {
        id
        prixPhv
        remises {
          id
          nombreUg
          pourcentageRemise
          quantiteMax
          quantiteMin
        }
      }
    }
  }
`;

const Quantite: FC<QuantiteProps & RouteComponentProps<any, any, any>> = ({
  currentCanalArticle,
  // currentPharmacie,
  // currentPanier,
  acceptZero,
  location,
  handleParentQuantite,
  parentQuantite,
  qteMin,
  match,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const idOperation = match && match.params && match.params.idOperation;
  // get current user role
  const user = getUser();

  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const myPanierResult = useQuery<myPaniers, myPaniersVariables>(GET_MY_PANIERS, {
    variables: {
      idPharmacie:
        (myPharmacie &&
          myPharmacie.data &&
          myPharmacie.data.pharmacie &&
          myPharmacie.data.pharmacie.id) ||
        '',
      take: 10,
      skip: 0,
    },
    // skip: listResult && listResult.data && !listResult.data.search,
  });

  const operationPanierResult = useQuery(GET_OPERATION_PANIER);
  const operationPanier =
    operationPanierResult &&
    operationPanierResult.data &&
    operationPanierResult.data.operationPanier;

  // currentPharmacie
  const currentPharmacie: PharmacieMinimInfo | null =
    (myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie) || null;

  // currentPanier
  const currentPanier: Panier | null = idOperation
    ? operationPanier
    : ({
      panierLignes:
        (myPanierResult &&
          myPanierResult.data &&
          myPanierResult.data.myPaniers &&
          myPanierResult.data.myPaniers
            .map(panier => panier && panier.panierLignes)
            .reduce((total, value) => {
              return total && total.concat(value);
            }, [])) ||
        [],
    } as Panier);

  // disable quantite change if user is groupement collaborateur
  const isChangeDisabled =
    user &&
      user.role &&
      user.role.code &&
      user.role.code !== COLLABORATEUR_COMMERCIAL &&
      user.role.code !== ADMINISTRATEUR_GROUPEMENT &&
      user.role.code !== COLLABORATEUR_NON_COMMERCIAL &&
      user.role.code !== GROUPEMENT_AUTRE
      ? false
      : user.role.code === ADMINISTRATEUR_GROUPEMENT &&
        location.pathname.includes('operations-commerciales')
        ? false
        : // : currentCanalArticle && currentCanalArticle.qteStock && currentCanalArticle.qteStock > 0
        // ? false
        true;

  const stv = currentCanalArticle && currentCanalArticle.stv ? currentCanalArticle.stv : 0;
  const qteStock =
    currentCanalArticle && currentCanalArticle.qteStock ? currentCanalArticle.qteStock : 0;

  const [showQteSave, setShowQteSave] = useState<boolean>(false);
  const [isQuerying, setIsQuerying] = useState<boolean>(false);

  const [currentQuantite, setCurrentQuantite] = useState<number>(parentQuantite || 0);
  const [currentLigne, setCurrentLigne] = useState<Panier_PanierLignes_panierLignes | null>(null);
  const lastPath = location.state && location.state.from ? location.state.from : '';

  const quantiteStv = (stv: number, qte: number) => {
    if (qte === 0) return 0;
    return stv === 1 || stv === 0 ? qte : qte < stv ? stv : qte - (qte % stv);
  };

  const getPanierLigneItem = () => {
    if (currentCanalArticle && currentPanier && currentPanier.panierLignes) {
      const currentLigne = currentPanier.panierLignes.find(
        (ligne: Panier_PanierLignes_panierLignes | null) =>
          ligne && ligne.produitCanal && ligne.produitCanal.id === currentCanalArticle.id,
      );
      return currentLigne ? currentLigne : null;
    } else return null;
  };

  useEffect(() => {
    const ligne = getPanierLigneItem();
    const quantite =
      ligne && ligne.quantite
        ? ligne.quantite
        : acceptZero
          ? 0
          : currentCanalArticle && currentCanalArticle.stv
            ? currentCanalArticle.stv
            : 0;
    setCurrentLigne(ligne);
    if (acceptZero && handleParentQuantite) {
      setCurrentQuantite(parentQuantite || 0);
    } else {
      setCurrentQuantite(quantite);
    }
  }, [currentPanier]);

  const incQuantite = (e: any) => {
    e.stopPropagation();
    if (handleParentQuantite) {
      const newValue = quantiteStv(stv, currentQuantite + stv);
      setCurrentQuantite(newValue);
      handleParentQuantite(currentCanalArticle, newValue);
    } else {
      const newValue =
        qteMin && qteMin > 0 && currentQuantite < qteMin
          ? qteMin
          : currentQuantite === qteStock
            ? currentQuantite
            : stv
              ? currentQuantite + stv
              : currentQuantite + 1;

      if (qteStock >= stv && currentQuantite <= newValue) {
        if (!location.pathname.includes('/operation-produits')) {
          if (currentLigne && currentLigne.id && currentPharmacie && currentPharmacie.id) {
            setIsQuerying(true);
            doUpdateQte({
              variables: {
                id: currentLigne.id,
                quantite: newValue,
                idPharmacie: currentPharmacie.id,
                take: 10,
                skip: 0,
              },
            });
          } else {
            if (
              currentCanalArticle &&
              currentCanalArticle.id &&
              currentPharmacie &&
              currentPharmacie.id
            ) {
              setIsQuerying(true);
              doAddToPanier({
                variables: {
                  idCanalArticle: currentCanalArticle.id,
                  quantite: newValue,
                  idPharmacie: currentPharmacie.id,
                  codeCanal:
                    (currentCanalArticle &&
                      currentCanalArticle.commandeCanal &&
                      currentCanalArticle.commandeCanal.code) ||
                    '',
                  take: 10,
                  skip: 0,
                },
              });
            }
          }
        } else {
          // checkEditQuantite(newValue);
          if (currentCanalArticle && currentCanalArticle.id) {
            // if (qteStock < newValue) newValue = qteStock;
            saveQuantiteToApolloCache(
              currentCanalArticle,
              newValue === currentQuantite
                ? quantiteStv(stv, newValue + stv)
                : quantiteStv(stv, newValue),
            );
          }
        }

        setShowQteSave(false);
      }
    }
  };

  const decQuantite = (e: any) => {
    e.stopPropagation();
    if (handleParentQuantite) {
      if (acceptZero && currentQuantite > 0) {
        const newValue = quantiteStv(stv, currentQuantite - stv);
        setCurrentQuantite(newValue);
        handleParentQuantite(currentCanalArticle, newValue);
      }
    } else {
      if (
        (currentQuantite > stv &&
          currentCanalArticle &&
          currentCanalArticle.qteStock &&
          currentCanalArticle.qteStock > stv) ||
        (acceptZero && currentQuantite > 0)
      ) {
        // const newValue = qteMin && currentQuantite > qteMin ? currentQuantite - stv : 0;
        const newValue = currentQuantite > stv ? currentQuantite - stv : 0;
        if (!location.pathname.includes('/operation-produits')) {
          if (currentLigne && currentLigne.id && currentPharmacie && currentPharmacie.id) {
            setIsQuerying(true);
            doUpdateQte({
              variables: {
                id: currentLigne.id,
                quantite: newValue,
                idPharmacie: currentPharmacie.id,
                take: 10,
                skip: 0,
              },
            });
          } else {
            if (
              currentPharmacie &&
              currentPharmacie.id &&
              currentCanalArticle &&
              currentCanalArticle.id
            ) {
              setIsQuerying(true);
              doAddToPanier({
                variables: {
                  idCanalArticle: currentCanalArticle.id,
                  quantite: newValue,
                  idPharmacie: currentPharmacie.id,
                  codeCanal:
                    (currentCanalArticle &&
                      currentCanalArticle.commandeCanal &&
                      currentCanalArticle.commandeCanal.code) ||
                    '',
                },
              });
            }
          }
        } else {
          checkEditQuantite(newValue);
          if (currentCanalArticle && currentCanalArticle.id) {
            saveQuantiteToApolloCache(currentCanalArticle, quantiteStv(stv, newValue));
          }
        }

        setShowQteSave(false);
      }
    }
  };

  const handleChangeQuantite = (event: React.ChangeEvent<{ value: string }>) => {
    const quantite = parseInt(event.target.value, 10);

    checkEditQuantite(quantite);
    setShowQteSave(true);
  };

  const checkEditQuantite = (quantite: number) => {
    const qteStock =
      currentCanalArticle && currentCanalArticle.qteStock ? currentCanalArticle.qteStock : 0;
    if (quantite >= qteStock && !handleParentQuantite && !qteMin) {
      if (qteMin !== 0) setCurrentQuantite(qteStock);
      else setCurrentQuantite(quantite);
      return;
    }
    if (isNaN(quantite)) {
      setCurrentQuantite(0);
      return;
    }

    setCurrentQuantite(quantite);
  };

  const saveQte = (e: any) => {
    e.stopPropagation();
    if (handleParentQuantite) {
      handleParentQuantite(currentCanalArticle, quantiteStv(stv, currentQuantite));
      setCurrentQuantite(quantiteStv(stv, currentQuantite));
      setShowQteSave(false);
    } else {
      if (
        currentCanalArticle &&
        currentCanalArticle.qteStock &&
        currentCanalArticle.qteStock >= stv
      ) {
        if (!location.pathname.includes('/operation-produits')) {
          setIsQuerying(true);
          if (currentLigne && currentLigne.id && currentPharmacie && currentPharmacie.id) {
            setIsQuerying(true);
            doUpdateQte({
              variables: {
                id: currentLigne.id,
                quantite: currentQuantite,
                idPharmacie: currentPharmacie.id,
                take: 10,
                skip: 0,
              },
            });
          } else {
            if (
              currentPharmacie &&
              currentPharmacie.id &&
              currentCanalArticle &&
              currentCanalArticle.id
            ) {
              setIsQuerying(true);
              doAddToPanier({
                variables: {
                  idCanalArticle: currentCanalArticle.id,
                  quantite: currentQuantite,
                  codeCanal:
                    (currentCanalArticle &&
                      currentCanalArticle.commandeCanal &&
                      currentCanalArticle.commandeCanal.code) ||
                    '',
                  idPharmacie: currentPharmacie.id,
                },
              });
            }
          }
        } else {
          if (currentCanalArticle && currentCanalArticle.id) {
            saveQuantiteToApolloCache(currentCanalArticle, quantiteStv(stv, currentQuantite));
          }

          setShowQteSave(false);
        }
      }
    }
  };

  const [doAddToPanier, AddToPanierResult] = useMutation<addToPanier, addToPanierVariables>(
    ADD_TO_PANIER,
    {
      update: (cache, { data }) => {
        if (data && data.addToPanier && currentCanalArticle && currentPharmacie) {
          const req = cache.readQuery<myPaniers, myPaniersVariables>({
            query: GET_MY_PANIERS,
            variables: {
              idPharmacie: currentPharmacie.id || '',
              take: 10,
              skip: 0,
            },
          });
          if (req && req.myPaniers) {
            if (
              req.myPaniers.length === 0 ||
              !req.myPaniers.map(panier => panier && panier.id).includes(data.addToPanier.id)
            ) {
              cache.writeQuery({
                query: GET_MY_PANIERS,
                data: { myPaniers: [data.addToPanier] },
                variables: {
                  idPharmacie: currentPharmacie.id || '',
                  take: 10,
                  skip: 0,
                },
              });
            } else {
              cache.writeQuery({
                query: GET_MY_PANIERS,
                data: req.myPaniers.map(panier => {
                  if (panier && data && data.addToPanier && panier.id === data.addToPanier.id) {
                    return data.addToPanier;
                  } else return panier;
                }),
                variables: {
                  idPharmacie: currentPharmacie.id || '',
                  take: 10,
                  skip: 0,
                },
              });
            }
          }
        }
      },
      onCompleted: data => {
        setIsQuerying(false);
        setShowQteSave(false);
      },
    },
  );

  const [doUpdateQte, doUpdateQteResult] = useMutation<UPDATE_QUANTITE, UPDATE_QUANTITEVariables>(
    DO_UPDATE_QUANTITE,
    {
      onCompleted: data => {
        setIsQuerying(false);
        setShowQteSave(false);
      },
    },
  );

  // Get (selected) article in OC from apollo local state
  const getArticleOcArray = useQuery<ArticleOCArray>(GET_ARTICLE_OC_ARRAY);

  /**
   * Function to save the articles chosen by the user in commercial operation to apollo local state
   * @param id - article id
   * @param quantite - article quantity
   */
  const saveQuantiteToApolloCache = (
    article: ProduitCanal | Article_Same_Panachees_articleSamePanachees,
    quantite: number,
  ) => {
    setCurrentQuantite(quantite);

    if (location.pathname.includes('/operation-produits')) {
      if (quantite >= 0) {
        const ligne = {
          id: article.id,
          quantite,
          produitCanal: {
            id: article.id,
            prixPhv: article.prixPhv,
            remises: article.remises,
            __typename: 'remises',
          },
        };

        if (getArticleOcArray && getArticleOcArray.data && getArticleOcArray.data.articleOcArray) {
          const currentArticle = getArticleOcArray.data.articleOcArray.find(
            item => item && item.id === article.id,
          );

          if (currentArticle) {
            let newArticleArray = getArticleOcArray.data.articleOcArray.map(item => {
              if (item && item.id && item.id === article.id) {
                // Get new article with new quantity
                return {
                  ...ligne,
                  quantite,
                  __typename: 'ArticleOCInterface',
                };
              } else {
                return item;
              }
            });

            if (newArticleArray) {
              // Filter argticle, we only want articles greater than 0
              newArticleArray = newArticleArray.filter(item => item && item.quantite > 0);
              // TODO: Migration
              //(client as any).writeData({ data: { articleOcArray: newArticleArray } });
            }
          } else {
            const newArticle = {
              ...ligne,
              quantite,
              __typename: 'ArticleOCInterface',
            };

            // We only want article greater than 0
            if (
              getArticleOcArray.data.articleOcArray.length &&
              newArticle &&
              newArticle.quantite > 0
            ) {
              const newArticleArray = [...getArticleOcArray.data.articleOcArray, newArticle];
              // TODO: Migration
             // (client as any).writeData({ data: { articleOcArray: newArticleArray } });
            } else if (newArticle && newArticle.quantite > 0) {
              // TODO: Migration
              //(client as any).writeData({ data: { articleOcArray: [newArticle] } });
            }
          }
        }
      }
    }
  };

  if (lastPath === '/suivi-commandes' || lastPath === '/create/operations-commerciales') {
    return (
      <Box className={classnames(classes.MontserratRegular, classes.medium, classes.colorDefault)}>
        {currentQuantite}
      </Box>
    );
  } else {
    return qteStock <= 0 ? (
      <Typography className={classes.ruptureText}>Rupture</Typography>
    ) : (
      <Box
        className={classes.panierListeRemiseQuantite}
        width={showQteSave && qteStock > 0 ? '152px' : '112px'}
      >
        <Box display="flex" justifyContent="center" alignItems="center">
          <IconButton
            size="small"
            onClick={decQuantite}
            disabled={
              qteStock <= 0
                ? true
                : isQuerying || location.pathname === '/autrepanier' || isChangeDisabled
                  ? true
                  : false
            }
          >
            <RemoveIcon className={classes.quantiteButton} />
          </IconButton>
        </Box>

        <Box className={classes.quantiteTextField}>
          {isQuerying ? (
            <CircularProgress size={14} color="secondary" />
          ) : (
            <InputBase
              onChange={handleChangeQuantite}
              onClick={e => e.stopPropagation()}
              value={currentQuantite}
              inputProps={{ classes: classes.underline, input: classes.inputFont }}
              disabled={
                qteStock <= 0
                  ? true
                  : location.pathname === '/autrepanier' || isChangeDisabled
                    ? true
                    : false
              }
            />
          )}
        </Box>

        <Box display="flex" justifyContent="center" alignItems="center">
          <IconButton
            size="small"
            onClick={incQuantite}
            disabled={
              qteStock <= 0
                ? true
                : isQuerying || location.pathname === '/autrepanier' || isChangeDisabled
                  ? true
                  : false
            }
          >
            <AddIcon className={classes.quantiteButton} />
          </IconButton>
        </Box>
        {showQteSave && qteStock > 0 && (
          <Box
            display="flex"
            justifyContent="center"
            alignItems="center"
            className={classes.quantiteSaveButton}
          >
            <IconButton size="small" onClick={saveQte} disabled={isQuerying ? true : false}>
              <SaveIcon className={classes.blueButton} />
            </IconButton>
          </Box>
        )}
      </Box>
    );
  }
};

export default withRouter(Quantite);
