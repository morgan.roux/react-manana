import React, { FC } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
// import { LazyLoadImage } from 'react-lazy-load-image-component';
import { Box } from '@material-ui/core';
interface ArticleImageProps {
  src: string;
}
const ArticleImage: FC<ArticleImageProps & RouteComponentProps<any, any, any>> = ({ src }) => {
  const classes = useStyles({});
  return (
    <Box width="100%" display="flex" justifyContent="center">
      <Box className={classes.contentImgProduct}>
        <img alt={src} src={src} />
      </Box>
    </Box>
  );
};

export default withRouter(ArticleImage);
