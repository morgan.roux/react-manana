import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    infoContainerRoot: {
      '& .MuiSvgIcon-root': {
        width: 20,
        height: 20,
      },
    },
    label: {
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 12,
      color: '#616161',
      marginRight: '4px',
      display: 'flex',
      alignItems: 'center',
    },
    value: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 14,
      color: '#616161',
    },
    secondary: {
      color: theme.palette.secondary.main,
    },
  }),
);
