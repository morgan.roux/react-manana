import React, { FC, useState } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import classnames from 'classnames';
import { Box, Typography } from '@material-ui/core';
interface FicheClientProps {}
const FicheClient: FC<FicheClientProps & RouteComponentProps<any, any, any>> = ({}) => {
  const classes = useStyles({});
  const link =
    'https://www.shop-pharmacie.fr/medicaments/F10000168/doliprane-paracetamol-1000-mg.htm';
  return (
    <div>
      <a href={link} target="_blank">
        <Typography className={classnames(classes.MontserratBold, classes.histoVenteAction)}>
          Fiche Internet
        </Typography>
      </a>
    </div>
  );
};

export default withRouter(FicheClient);
