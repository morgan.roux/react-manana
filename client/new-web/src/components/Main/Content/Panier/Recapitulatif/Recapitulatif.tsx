import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import {
  Box,
  Button,
  CircularProgress,
  TextareaAutosize,
  Typography,
  colors,
} from '@material-ui/core';
import LocalGroceryStoreIcon from '@material-ui/icons/LocalGroceryStore';
import PrintIcon from '@material-ui/icons/Print';
import classnames from 'classnames';
import React, { FC, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { ContentContext, ContentStateInterface } from '../../../../../AppContext';
import { DO_CREATE_OPERATION_COMMANDE } from '../../../../../graphql/Commande';
import { ArticleCommande } from '../../../../../graphql/Commande/mutation';
import {
  CREATE_OPERATION_COMMANDE,
  CREATE_OPERATION_COMMANDEVariables,
} from '../../../../../graphql/Commande/types/CREATE_OPERATION_COMMANDE';
import { GET_CODE_CANAL } from '../../../../../graphql/CommandeCanal/local';
import {
  CALCUL_PRIX_TOTAL_OPERATION,
  DO_MARK_OPERATION_AS_SEEN,
  DO_CREATE_OPERATION_VIEWER,
} from '../../../../../graphql/OperationCommerciale/mutation';
import {
  calculPrixTotalOperation,
  calculPrixTotalOperationVariables,
} from '../../../../../graphql/OperationCommerciale/types/calculPrixTotalOperation';
import { DO_VALIDATE_PANIER } from '../../../../../graphql/Panier/mutation';
import { GET_MY_PANIER, GET_MY_PANIERS } from '../../../../../graphql/Panier/query';
import { myPanier, myPanierVariables } from '../../../../../graphql/Panier/types/myPanier';
import {
  VALIDATE_PANIER,
  VALIDATE_PANIERVariables,
} from '../../../../../graphql/Panier/types/VALIDATE_PANIER';
import { GET_CURRENT_PHARMACIE } from '../../../../../graphql/Pharmacie/local';
import { SEARCH as SEARCH_QUERY } from '../../../../../graphql/search/query';
import { Panier as MyPanier } from '../../../../../graphql/Panier/types/Panier';
import { SEARCH, SEARCHVariables } from '../../../../../graphql/search/types/SEARCH';
import ICurrentPharmacieInterface from '../../../../../Interface/CurrentPharmacieInterface';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import {
  ArticleOCArray,
  GET_ARTICLE_OC_ARRAY,
  GET_SELECTED_OC,
} from '../../OperationCommerciale/OperationCommerciale';
import PanierResume from '../PanierResume/PanierResume';
import { useStyles } from '../styles';
import { myPaniers, myPaniersVariables } from '../../../../../graphql/Panier/types/myPaniers';
import {
  OPERATION_PRESENTATION,
  OPERATION_PRESENTATIONVariables,
} from '../../../../../graphql/OperationCommerciale/types/OPERATION_PRESENTATION';
import { GET_OPERATION_PRESENTATION } from '../../../../../graphql/OperationCommerciale/query';
import { getUser } from '../../../../../services/LocalStorage';
import { ME_me } from '../../../../../graphql/Authentication/types/ME';
import {
  markAsSeenOperation,
  markAsSeenOperationVariables,
} from '../../../../../graphql/OperationCommerciale/types/markAsSeenOperation';
import {
  createOperationViewer,
  createOperationViewerVariables,
} from '../../../../../graphql/OperationCommerciale/types/createOperationViewer';
import CustomButton from '../../../../Common/CustomButton';
import { CheckCircle } from '@material-ui/icons';
import { AppAuthorization } from '../../../../../services/authorization';
interface RecapitulatifProps {
  match: { params: { type: string } };
}
interface OCInterface {
  selectedOC: { id: string; __typename: 'operationId' };
}

const snackMessage = ({ type, message, isOpen }: SnackVariableInterface) => {
  return {
    type,
    message,
    isOpen,
  };
};

const Recapitulatif: FC<RecapitulatifProps & RouteComponentProps<any, any, any>> = ({
  history,
  match,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const currentUser: ME_me = getUser();
  const auth = new AppAuthorization(currentUser);

  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [idPanier, setIdPanier] = useState<string>();
  const [commentairePharmacie, setCommentairePharmacie] = useState<string>('');
  const [commentairePlateforme, setCommentiarePlateforme] = useState<string>('');
  const [articleCommandes, setArticleCommandes] = useState<ArticleCommande[]>([]);
  const [currentPanier, setCurrentPanier] = useState<MyPanier | null>();
  const [activeOperation, setActiveOperation] = useState<any>();
  const idPublicite =
    history && history.location && history.location.state && history.location.state.idPublicite;
  const commandeCanal = useQuery(GET_CODE_CANAL);
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const myPanierQuery = useQuery<myPanier, myPanierVariables>(GET_MY_PANIER, {
    variables: {
      idPharmacie:
        (myPharmacie &&
          myPharmacie.data &&
          myPharmacie.data.pharmacie &&
          myPharmacie.data.pharmacie.id) ||
        '',
      codeCanal:
        (commandeCanal &&
          commandeCanal.data &&
          commandeCanal.data.CurrentCanalCode &&
          commandeCanal.data.CurrentCanalCode.code) ||
        'PFL',
    },
    fetchPolicy: 'cache-and-network',
  });

  const [getOperation, currentOperation] = useLazyQuery<
    OPERATION_PRESENTATION,
    OPERATION_PRESENTATIONVariables
  >(GET_OPERATION_PRESENTATION, { fetchPolicy: 'cache-and-network' });

  const idPharmacieUser =
    (currentUser &&
      currentUser.userPpersonnel &&
      currentUser.userPpersonnel.pharmacie &&
      currentUser.userPpersonnel.pharmacie.id) ||
    (currentUser &&
      currentUser.userTitulaire &&
      currentUser.userTitulaire.titulaire &&
      currentUser.userTitulaire.titulaire.pharmacieUser &&
      currentUser.userTitulaire.titulaire.pharmacieUser.id);

  const onCommentairePharmacieChange = (event: React.ChangeEvent<{ value: string }>) => {
    const value = event.target.value as string;
    setCommentairePharmacie(value);
  };

  const onCommentairePlateformeChange = (event: React.ChangeEvent<{ value: string }>) => {
    const value = event.target.value as string;
    setCommentiarePlateforme(value);
  };

  const successSnackBar: SnackVariableInterface = snackMessage({
    type: 'SUCCESS',
    message: 'Panier validé avec succès',
    isOpen: true,
  });

  const goToModuleCommande = () => {
    history.push('/suivi-commandes');
  };

  const {
    content: { type, page, rowsPerPage, query, sortBy, filterBy },
  } = useContext<ContentStateInterface>(ContentContext);

  const queryVariables: SEARCHVariables = {
    type: [type],
    skip: page * rowsPerPage,
    take: rowsPerPage,
    query,
    sortBy,
    filterBy,
    idPharmacie:
      (myPharmacie &&
        myPharmacie.data &&
        myPharmacie.data.pharmacie &&
        myPharmacie.data.pharmacie.id) ||
      '',
  };

  const [doValidatePanier] = useMutation<VALIDATE_PANIER, VALIDATE_PANIERVariables>(
    DO_VALIDATE_PANIER,
    {
      onCompleted: data => {
        // TODO: Migration
        //(client as any).writeData({ data: { panier: null } });
        setIsLoading(false);
        displaySnackBar(client, successSnackBar);
        goToModuleCommande();
      },
      update: (cache, { data }) => {
        if (data && data.validatePanierAndCreateCommande) {
          const query = cache.readQuery<myPaniers, myPaniersVariables>({
            query: GET_MY_PANIERS,
            variables: myPanierQuery.variables,
          });
          if (query && query.myPaniers) {
            cache.writeQuery({
              query: GET_MY_PANIERS,
              data: {
                myPaniers:
                  query &&
                  query.myPaniers &&
                  query.myPaniers.filter(
                    panier => panier && currentPanier && panier.id !== currentPanier.id,
                  ),
              },
              variables: {
                idPharmacie:
                  (myPharmacie &&
                    myPharmacie.data &&
                    myPharmacie.data.pharmacie &&
                    myPharmacie.data.pharmacie.id) ||
                  '',
                take: 10,
                skip: 0,
              },
            });
          }
        }
      },
    },
  );

  const [doCalculPrixTotalOperation] = useMutation<
    calculPrixTotalOperation,
    calculPrixTotalOperationVariables
  >(CALCUL_PRIX_TOTAL_OPERATION, {
    onCompleted: data => {
      if (data && data.calculPrixTotalOperation) {
        const {
          prixBaseTotal,
          prixNetTotal,
          qteTotal,
          uniteGratuite,
        } = data.calculPrixTotalOperation;
        const valeurRemiseTotal =
          prixBaseTotal !== null && prixNetTotal !== null ? prixBaseTotal - prixNetTotal : 0;
        const remiseGlobale =
          valeurRemiseTotal !== null && prixBaseTotal !== null
            ? (valeurRemiseTotal * 100) / prixBaseTotal
            : 0;
        const panier = {
          remiseGlobale,
          prixBaseTotalHT: prixBaseTotal !== null ? prixBaseTotal : 0,
          prixNetTotalHT: prixNetTotal !== null ? prixNetTotal : 0,
          valeurFrancoPort: 0,
          panierLignes: articleCommandes,
          nbrRef: uniteGratuite !== null && qteTotal !== null ? qteTotal + uniteGratuite : 0,
        };
        setCurrentPanier(panier as MyPanier);
      }
    },
  });

  const validatePanier = () => {
    console.log('valider panier');
    setIsLoading(true);
    if (
      currentPanier &&
      currentPanier.id &&
      myPharmacie &&
      myPharmacie.data &&
      myPharmacie.data.pharmacie &&
      myPharmacie.data.pharmacie.id
    ) {
      doValidatePanier({
        variables: {
          idPanier: currentPanier.id,
          idPharmacie: myPharmacie.data.pharmacie.id,
          commentaireExterne: commentairePharmacie,
          commentaireInterne: commentairePlateforme,
          codeCanalCommande:
            (commandeCanal &&
              commandeCanal.data &&
              commandeCanal.data.CurrentCanalCode &&
              commandeCanal.data.CurrentCanalCode.code) ||
            'PFL',
        },
      });
    }
  };

  /**
   * Traitement commande venant opération commérciale
   */

  // Query to confirm OC command
  const [doCreateOperationCommand] = useMutation<
    CREATE_OPERATION_COMMANDE,
    CREATE_OPERATION_COMMANDEVariables
  >(DO_CREATE_OPERATION_COMMANDE, {
    onCompleted: data => {
      if (data && data.createOperationCommande) {
        // TODO: Migration
        //(client as any).writeData({ data: { articleOcArray: null } });
        setIsLoading(false);
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Votre commande a été validée avec succès',
        });

        if (activeOperation && activeOperation.id && !activeOperation.seen) {
          console.log('activeOperation ::: ', activeOperation);
          doMarkOperationAsSeen();
          if (idPharmacieUser) doCreateOperationViewer();
        }
        goToModuleCommande();
      }
    },
    update: (cache, { data }) => {
      if (data && data.createOperationCommande) {
        const query = cache.readQuery<SEARCH, SEARCHVariables>({
          query: SEARCH_QUERY,
          variables: queryVariables,
        });

        if (query && query.search && query.search.data) {
          cache.writeQuery({
            query: SEARCH_QUERY,
            data: {
              search: {
                ...query.search,
                ...{ data: [...query.search.data, ...[data.createOperationCommande]] },
              },
            },
            variables: queryVariables,
          });
        }
      }
    },
    onError: errors => {
      displaySnackBar(client, {
        isOpen: true,
        type: 'ERROR',
        message: errors.message,
      });
    },
  });

  /* MARK AS SEEN OPERATION */
  const [doMarkOperationAsSeen] = useMutation<markAsSeenOperation, markAsSeenOperationVariables>(
    DO_MARK_OPERATION_AS_SEEN,
    {
      variables: {
        id: activeOperation && activeOperation.id,
        userId: currentUser && currentUser.id,
      },
    },
  );

  const [doCreateOperationViewer] = useMutation<
    createOperationViewer,
    createOperationViewerVariables
  >(DO_CREATE_OPERATION_VIEWER, {
    variables: {
      idOperation: activeOperation && activeOperation.id,
      idPharmacie:
        (myPharmacie &&
          myPharmacie.data &&
          myPharmacie.data.pharmacie &&
          myPharmacie.data.pharmacie.id) ||
        '',
      idPharmacieUser: idPharmacieUser ? idPharmacieUser : '',
      userId: currentUser && currentUser.id,
    },
  });

  // Get selected OC from local state apollo
  const selectedOC = useQuery<OCInterface>(GET_SELECTED_OC);
  console.log('selectedOC : ', selectedOC);

  // Get (selected) article in OC from apollo local state
  const getArticleOcArray = useQuery<ArticleOCArray>(GET_ARTICLE_OC_ARRAY);

  // Check if operation commercial command
  const isOperationCommercialCommand = (): boolean => {
    return match &&
      match.params &&
      match.params.type &&
      match.params.type === 'operation-commerciale'
      ? true
      : false;
  };

  // Consfirm OC command
  const confirmOperationCommercialCommand = () => {
    console.log('valider operation : ', selectedOC);
    if (isOperationCommercialCommand()) {
      if (
        getArticleOcArray &&
        getArticleOcArray.data &&
        getArticleOcArray.data.articleOcArray &&
        getArticleOcArray.data.articleOcArray.length > 0 &&
        myPharmacie &&
        myPharmacie.data &&
        myPharmacie.data.pharmacie &&
        myPharmacie.data.pharmacie.id &&
        selectedOC &&
        selectedOC.data &&
        selectedOC.data.selectedOC &&
        selectedOC.data.selectedOC.id
      ) {
        // Format data (article)
        console.log('====> 1');
        const commandedArticles = getArticleOcArray.data.articleOcArray;
        const newCommandedArticles: ArticleCommande[] = commandedArticles.map(item => {
          return {
            id: item.id,
            quantite: item.quantite,
          };
        });
        // Make mutation
        setIsLoading(true);
        doCreateOperationCommand({
          variables: {
            idOperation: selectedOC.data.selectedOC.id,
            idPharmacie: myPharmacie.data.pharmacie.id,
            articlesCommande: newCommandedArticles,
            commentaireExterne: commentairePharmacie,
            commentaireInterne: commentairePlateforme,
            idPublicite,
          },
        });
      }
    }
  };

  useEffect(() => {
    // Calcul command resume
    if (isOperationCommercialCommand()) {
      const selectedOcArticles =
        (getArticleOcArray && getArticleOcArray.data && getArticleOcArray.data.articleOcArray) ||
        null;
      if (selectedOcArticles && selectedOcArticles.length) {
        if (
          myPharmacie &&
          myPharmacie.data &&
          myPharmacie.data.pharmacie &&
          myPharmacie.data.pharmacie.id
        ) {
          setArticleCommandes(selectedOcArticles);
          doCalculPrixTotalOperation({
            variables: {
              idPharmacie: myPharmacie.data.pharmacie.id,
              idOperation:
                (selectedOC &&
                  selectedOC.data &&
                  selectedOC.data.selectedOC &&
                  selectedOC.data.selectedOC.id) ||
                '',
              articles: selectedOcArticles.map(selected => {
                return { id: selected.id, quantite: selected.quantite };
              }),
            },
          });
        }
      }
    } else {
      if (myPanierQuery && myPanierQuery.data && myPanierQuery.data.myPanier) {
        setCurrentPanier(myPanierQuery.data.myPanier);
      }
    }
  }, [getArticleOcArray.data, myPharmacie, myPanierQuery]);
  // FIN Traitement commande venant opération commérciale

  useEffect(() => {
    const path = history.location.pathname.split('/');
    console.log('history.location.pathname', path);
    const eventUnloader = (e: BeforeUnloadEvent) => {
      e.preventDefault();
      const confirmationMessage =
        'Voulez-vous vraiment rechargé cette page? Les modifications que vous avez apportées peuvent ne pas être enregistrées.';
      e.returnValue = confirmationMessage;
      return e.returnValue;
    };
    if (path && path.length && 'operation-commerciale' === path[2]) {
      console.log('path[0]', path[2]);
      window.addEventListener('beforeunload', eventUnloader);
      return function cleanup() {
        window.removeEventListener('beforeunload', eventUnloader);
      };
    }
  }, [history.location.pathname]);

  useEffect(() => {
    if (
      selectedOC &&
      selectedOC.data &&
      selectedOC.data.selectedOC &&
      selectedOC.data.selectedOC.id
    ) {
      getOperation({
        variables: {
          id: selectedOC.data.selectedOC.id,
          userId: currentUser && currentUser.id,
          idPharmacieUser,
        },
      });
    }
  }, [selectedOC]);

  useEffect(() => {
    if (currentOperation && currentOperation.data && currentOperation.data.operation) {
      setActiveOperation(currentOperation.data.operation);
    }
  }, [currentOperation]);

  const handleBack = () => {
    history.goBack();
  };

  console.log('activeOperation ====++++++==>', activeOperation);

  const disabledValidateCommande = (): boolean => {
    if (!auth.isAuthorizedToValidateCommande() || isLoading) {
      return true;
    }
    return false;
  };

  return (
    <div>
      <Box padding="30px" flex="1" height="calc(100vh - 86px)" overflow="auto">
        <Box>
          <Typography className={classnames(classes.uppercase, classes.big, classes.RobotoMedium)}>
            RéCAPITULATIF DE VOTRE COMMANDE
          </Typography>
        </Box>
        <Box marginTop="30px" className={classes.recapContainer}>
          <Box flex="1">
            <Box className={classes.recapCommentContainer}>
              <Box className={classes.recapLabelBox}>
                <Typography className={classnames(classes.MontserratRegular, classes.medium)}>
                  Commentaire Pharmacie
                </Typography>
              </Box>
              <Box className={classes.recapTextAreaBox}>
                <TextareaAutosize
                  rowsMax={6}
                  placeholder="Commentaire Pharmacie"
                  rows={6}
                  className={classes.recapTextArea}
                  onChange={onCommentairePharmacieChange}
                  value={commentairePharmacie}
                />
              </Box>
            </Box>
            <Box className={classes.recapCommentContainer} marginTop="34px">
              <Box className={classes.recapLabelBox}>
                <Typography className={classnames(classes.MontserratRegular, classes.medium)}>
                  Commentaire Plateforme
                </Typography>
              </Box>
              <Box className={classes.recapTextAreaBox}>
                <TextareaAutosize
                  rowsMax={6}
                  placeholder="Commentaire Plateforme"
                  rows={6}
                  className={classes.recapTextArea}
                  onChange={onCommentairePlateformeChange}
                  value={commentairePlateforme}
                />
              </Box>
            </Box>
          </Box>
          <Box className={classes.recapPanierResume}>
            <PanierResume currentPanier={currentPanier} />
          </Box>
        </Box>
        <Box marginTop="3rem" className={classes.recapButtonContainer}>
          <Box className={classes.boxInvisible} />
          <Box width="256px">
            <Button
              className={classnames(
                classes.panierResumeButton,
                classes.grayBackground,
                classes.MontserratBold,
                classes.medium,
                classes.darkLight,
              )}
              onClick={handleBack}
            >
              RETOUR
            </Button>
          </Box>
          <Box width="256px" className={classes.recapValiderButton}>
            <Button
              className={classnames(
                classes.panierResumeButton,
                classes.grayBackground,
                classes.MontserratBold,
                classes.medium,
                classes.darkLight,
              )}
              disabled={true}
            >
              <LocalGroceryStoreIcon className={classes.panierResumeButtonIcon} /> METTRE EN ATTENTE
            </Button>
          </Box>
          <Box width="256px" className={classes.recapValiderButton}>
            <CustomButton
              color="secondary"
              className={classnames(
                classes.panierResumeButton,
                classes.MontserratBold,
                classes.small,
              )}
              onClick={
                isOperationCommercialCommand() ? confirmOperationCommercialCommand : validatePanier
              }
              startIcon={
                isLoading ? <CircularProgress color="inherit" size="1rem" /> : <CheckCircle />
              }
              disabled={disabledValidateCommande()}
            >
              VALIDER
            </CustomButton>
          </Box>
          <Box width="256px">
            <Button
              className={classnames(
                classes.panierResumeButton,
                classes.grayBackground,
                classes.MontserratBold,
                classes.medium,
                classes.darkLight,
              )}
              disabled={true}
            >
              <PrintIcon className={classes.panierResumeButtonIcon} /> IMPRIMER
            </Button>
          </Box>
        </Box>
      </Box>
    </div>
  );
};

export default withRouter(Recapitulatif);
