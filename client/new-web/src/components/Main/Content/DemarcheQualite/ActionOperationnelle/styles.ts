import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    createButton: {
      position: 'absolute',
      bottom: 75,
      right: 16,
      background: theme.palette.secondary.main,
      borderRadius: '50%',
      '& .MuiIconButton-root': {
        color: theme.palette.common.white,
      },
    },
  }),
);

export default useStyles;
