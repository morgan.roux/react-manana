import moment from 'moment';

export const startTime = (date: any): string | null => {
  if (!date) {
    return null;
  }

  const m = moment(date).utcOffset(0);
  m.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
  return m.toISOString();
};

export const endTime = (date: any): string | null => {
  if (!date) {
    return null;
  }

  const m = moment(date).utcOffset(0);
  m.set({ hour: 23, minute: 59, second: 59, millisecond: 59 });
  return m.toISOString();
};



const STATUTS_CODE_ORDER_MAP = {
  NOUVEAU: 1,
  EN_COURS: 2,
  CLOTURE: 3
}

export const sortStatuts = (statuts: any[]): any[] => {
  if (!statuts) {
    return statuts
  }


  return statuts.sort((a, b) => STATUTS_CODE_ORDER_MAP[a.code] - STATUTS_CODE_ORDER_MAP[b.code])
} 
