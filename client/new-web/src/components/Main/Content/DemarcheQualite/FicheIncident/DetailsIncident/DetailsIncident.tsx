import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import { DetailsFicheIncident, FicheIncident } from '@app/ui-kit';
import React, { FC, useEffect } from 'react';
import { RouteComponentProps } from 'react-router';
import Image from '../../../../../../assets/img/Demarche-qualite/ficheAmelioration/success.png';
import {
  CREATE_SOLUTION,
  DELETE_FICHE_INCIDENT,
} from '../../../../../../federation/demarche-qualite/fiche-incident/mutation';
import {
  GET_FICHE_INCIDENT,
  GET_FICHE_INCIDENTS,
  GET_SOLUTION,
} from '../../../../../../federation/demarche-qualite/fiche-incident/query';
import {
  CREATE_SOLUTION as CREATE_SOLUTION_TYPE,
  CREATE_SOLUTIONVariables,
} from '../../../../../../federation/demarche-qualite/fiche-incident/types/CREATE_SOLUTION';
import {
  DELETE_FICHE_INCIDENT as DELETE_FICHE_INCIDENT_TYPE,
  DELETE_FICHE_INCIDENTVariables,
} from '../../../../../../federation/demarche-qualite/fiche-incident/types/DELETE_FICHE_INCIDENT';
import {
  GET_FICHE_INCIDENT as GET_FICHE_INCIDENT_TYPE,
  GET_FICHE_INCIDENTVariables,
} from '../../../../../../federation/demarche-qualite/fiche-incident/types/GET_FICHE_INCIDENT';
import {
  GET_FICHE_INCIDENTS as GET_FICHE_INCIDENTS_TYPE,
  GET_FICHE_INCIDENTSVariables,
} from '../../../../../../federation/demarche-qualite/fiche-incident/types/GET_FICHE_INCIDENTS';
import {
  GET_SOLUTION as GET_SOLUTION_TYPE,
  GET_SOLUTIONVariables,
} from '../../../../../../federation/demarche-qualite/fiche-incident/types/GET_SOLUTION';
import { GET_COLLABORATEURS } from '../../../../../../federation/iam/user/query';
import { GET_COLLABORATEURS as GET_COLLABORATEURSVariables } from '../../../../../../federation/iam/user/types/GET_COLLABORATEURS';
import { GET_MINIM_SERVICES } from '../../../../../../graphql/Service/query';
import { getPharmacie } from '../../../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';

const DetailsIncident: FC<RouteComponentProps> = ({ history: { push }, match: { params } }) => {
  const client = useApolloClient();
  const pharmacie = getPharmacie();

  const ficheIncidentId = (params as any).id;

  const [loadSolution, loadingSolution] = useLazyQuery<GET_SOLUTION_TYPE, GET_SOLUTIONVariables>(
    GET_SOLUTION,
    {
      client: FEDERATION_CLIENT,
      fetchPolicy: 'network-only',
    },
  );

  const loadFicheIncident = useQuery<GET_FICHE_INCIDENT_TYPE, GET_FICHE_INCIDENTVariables>(
    GET_FICHE_INCIDENT,
    {
      onCompleted: ficheResult => {
        if (ficheResult.dQFicheIncident?.idSolution) {
          loadSolution({
            variables: {
              id: ficheResult.dQFicheIncident.idSolution,
            },
          });
        }
      },
      client: FEDERATION_CLIENT,
      variables: {
        id: ficheIncidentId,
      },
      fetchPolicy: 'network-only',
    },
  );

  const collaborateurs = useQuery<GET_COLLABORATEURSVariables>(GET_COLLABORATEURS, {
    client: FEDERATION_CLIENT,
  });

  const [createSolution, creationSolution] = useMutation<
    CREATE_SOLUTION_TYPE,
    CREATE_SOLUTIONVariables
  >(CREATE_SOLUTION, {
    onCompleted: result => {
      displaySnackBar(client, {
        message: 'La solution a été ajoutée avec succès',
        type: 'SUCCESS',
        isOpen: true,
      });

      loadSolution({
        variables: {
          id: result.createOneDQSolution.id,
        },
      });
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs se sont survenues pendant l'ajout de la solution",
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [deleteFicheIncident, deletionFicheIncident] = useMutation<
    DELETE_FICHE_INCIDENT_TYPE,
    DELETE_FICHE_INCIDENTVariables
  >(DELETE_FICHE_INCIDENT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: "La fiche d'incident a été supprimée avec succès !",
        type: 'SUCCESS',
        isOpen: true,
      });

      push('/demarche-qualite/fiche-incident');
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs se sont survenues pendant la suppression de la fiche d'incident",
        type: 'ERROR',
        isOpen: true,
      });
    },
    update: (cache, deletionResult) => {
      if (
        deletionResult.data &&
        deletionResult.data.deleteOneDQFicheIncident &&
        deletionResult.data.deleteOneDQFicheIncident.id
      ) {
        const previousList = cache.readQuery<
          GET_FICHE_INCIDENTS_TYPE,
          GET_FICHE_INCIDENTSVariables
        >({
          query: GET_FICHE_INCIDENTS,
        })?.dQFicheIncidents;

        const deletedId = deletionResult.data.deleteOneDQFicheIncident.id;

        cache.writeQuery<GET_FICHE_INCIDENTS_TYPE>({
          query: GET_FICHE_INCIDENTS,  
          data: {
            dQFicheIncidents: { ...(previousList || {}) , nodes: (previousList?.nodes || []).filter(item => item?.id !== deletedId)} as any,
          },
        });
      }
    },
    client: FEDERATION_CLIENT,
  });

  useEffect(() => {
    if (loadFicheIncident.data?.dQFicheIncident?.idSolution) {
      loadSolution({
        variables: {
          id: loadFicheIncident.data.dQFicheIncident.idSolution,
        },
      });
    }
  }, [loadFicheIncident.data]);

  const handleGoBack = (): void => {
    push(`/demarche-qualite/fiche-incident`);
  };

  const handleDelete = (incident: FicheIncident): void => {
    deleteFicheIncident({
      variables: {
        input: {
          id: incident.id,
        },
      },
    });
  };

  const handleSaveSolution = ({ id }: FicheIncident, { idUser, description }: any): void => {
    createSolution({
      variables: {
        idFicheIncident: id,
        solution: {
          idUser,
          description,
        },
      },
    });
  };

  const handleEdit = (incident: FicheIncident): void => {
    push(`/demarche-qualite/fiche-incident/${incident.id}`);
  };

  return (
    <DetailsFicheIncident
      loading={loadFicheIncident.loading || collaborateurs.loading || deletionFicheIncident.loading}
      error={loadFicheIncident.error || collaborateurs.error}
      noContentActionImageSrc={Image}
      ficheIncident={loadFicheIncident.data?.dQFicheIncident as any}
      solution={{
        ...loadingSolution,
        data: loadingSolution.data?.dQSolution as any,
        saving: creationSolution.loading,
        errorSaving: creationSolution.error,
      }}
      onRequestGoBack={handleGoBack}
      onRequestDelete={handleDelete}
      onRequestSaveSolution={handleSaveSolution}
      onRequestEdit={handleEdit}
      collaborateurs={collaborateurs?.data?.collaborateurs as any}
    />
  );
};

export default DetailsIncident;
