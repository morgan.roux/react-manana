import React, { FC } from 'react';
import {
  DetailsCompteRendu,
  FicheAmelioration,
  FicheIncident,
  ActionOperationnelle,
} from '@app/ui-kit';
import { RouteComponentProps } from 'react-router';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { useQuery, useMutation, useApolloClient } from '@apollo/client';
import { GET_REUNION } from '../../../../../../federation/demarche-qualite/reunion/query';
import {
  GET_REUNION as GET_REUNION_TYPE,
  GET_REUNIONVariables,
} from '../../../../../../federation/demarche-qualite/reunion/types/GET_REUNION';
import { ReunionAction } from '@app/ui-kit/components/pages/CompteRenduFormPage/types';
import {
  GET_STATUTS as GET_STATUTS_TYPE,
  GET_STATUTSVariables,
} from '../../../../../../federation/demarche-qualite/statut/types/GET_STATUTS';
import { GET_STATUTS } from '../../../../../../federation/demarche-qualite/statut/query';
import useReunionAction from './../../hooks/useReunionAction'
import { sortStatuts } from '../../util';

interface DetailsFicheCompteRenduProps {
  match: {
    params: {
      id: string;
    };
  };
}

const DetailsFicheCompteRendu: FC<RouteComponentProps & DetailsFicheCompteRenduProps> = ({
  match: {
    params: { id },
  },
  history: { push },
}) => {
  const client = useApolloClient();
  const { updateReunionAction } = useReunionAction()

  const { loading: loadingReunion, data: reunion, error: errorReunion } = useQuery<
    GET_REUNION_TYPE,
    GET_REUNIONVariables
  >(GET_REUNION, {
    variables: {
      id: id,
    },
    fetchPolicy: 'network-only',
    client: FEDERATION_CLIENT,
  });

  const loadingStatuts = useQuery<GET_STATUTS_TYPE, GET_STATUTSVariables>(GET_STATUTS, {
    client: FEDERATION_CLIENT,
    variables: {
      paging: {
        offset: 0,
        limit: 50,
      },
    },
  });


  const handleGoBack = (): void => {
    push(`/demarche-qualite/compte-rendu`);
  };

  const handleAdd = (): void => {
    push(`/demarche-qualite/compte-rendu/new`);
  };

  const handleSeeAction = (action: any): void => {
    console.log('**Action**', action);
  };

  const handleGoToTodo = (action: any): void => {
    console.log('**Action**', action);
  };

  const handleChangeStatus = (action: ReunionAction, idStatut: string): void => {
    updateReunionAction(action, { idStatut, idUrgence: undefined })
  };

  return (
    <DetailsCompteRendu
      compteRendu={{
        loading: loadingReunion,
        error: errorReunion as any,
        data: reunion?.dQReunion as any,
      }}
      collaborateurs={{
        loading: loadingReunion,
        error: errorReunion as any,
        data: reunion?.dQReunion?.participants as any,
      }}
      statuts={{
        ...loadingStatuts,
        data: sortStatuts((loadingStatuts.data?.dQStatuts.nodes || []) as any),
      }}
      onRequestChangeStatus={handleChangeStatus}
      onRequestAdd={handleAdd}
      onRequestGoBack={handleGoBack}
      onRequestSeeAction={handleSeeAction}
      onRequestGoToTodo={handleGoToTodo}
    />
  );
};

export default DetailsFicheCompteRendu;
