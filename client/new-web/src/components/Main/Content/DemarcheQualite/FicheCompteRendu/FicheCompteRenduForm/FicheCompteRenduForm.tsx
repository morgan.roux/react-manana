import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import { CompteRenduFormPage, FicheAmelioration, FicheIncident, ActionOperationnelle } from '@app/ui-kit';
import { RequestSavingCompteRendu, ReunionAction } from '@app/ui-kit/components/pages/CompteRenduFormPage/types';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router';
import { GET_COLLABORATEURS as GET_COLLABORATEURS_TYPE } from '../../../../../../federation/iam/user/types/GET_COLLABORATEURS';
import { GET_COLLABORATEURS } from '../../../../../../federation/iam/user/query';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { GET_REUNION } from '../../../../../../federation/demarche-qualite/reunion/query';
import {
  GET_REUNION as GET_REUNION_TYPE,
  GET_REUNIONVariables,
} from '../../../../../../federation/demarche-qualite/reunion/types/GET_REUNION';
import {
  CREATE_REUNION as CREATE_REUNION_TYPE,
  CREATE_REUNIONVariables,
} from '../../../../../../federation/demarche-qualite/reunion/types/CREATE_REUNION';
import {
  CREATE_REUNION,
  UPDATE_REUNION,
} from '../../../../../../federation/demarche-qualite/reunion/mutation';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import {
  UPDATE_REUNION as UPDATE_REUNION_TYPE,
  UPDATE_REUNIONVariables,
} from '../../../../../../federation/demarche-qualite/reunion/types/UPDATE_REUNION';
import _ from 'lodash';

import { GET_ENCOURS_ACTION_OPERATIONNELLES } from '../../../../../../federation/demarche-qualite/action-operationnelle/query';
import { GET_ENCOURS_FICHE_AMELIORATIONS } from '../../../../../../federation/demarche-qualite/fiche-amelioration/query';

import { GET_ENCOURS_FICHE_INCIDENTS } from '../../../../../../federation/demarche-qualite/fiche-incident/query';

import {
  GET_ENCOURS_FICHE_INCIDENTS as GET_ENCOURS_FICHE_INCIDENTS_TYPE,
  GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents,
} from '../../../../../../federation/demarche-qualite/fiche-incident/types/GET_ENCOURS_FICHE_INCIDENTS';
import {
  GET_ENCOURS_FICHE_AMELIORATIONS as GET_ENCOURS_FICHE_AMELIORATIONS_TYPE,
  GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations,
} from '../../../../../../federation/demarche-qualite/fiche-amelioration/types/GET_ENCOURS_FICHE_AMELIORATIONS';
import {
  GET_ENCOURS_ACTION_OPERATIONNELLES as GET_ENCOURS_ACTION_OPERATIONNELLES_TYPE,
  GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles,
} from '../../../../../../federation/demarche-qualite/action-operationnelle/types/GET_ENCOURS_ACTION_OPERATIONNELLES';
import {
  GET_ORIGINES as GET_ORIGINES_TYPE,
  GET_ORIGINESVariables,
} from '../../../../../../federation/demarche-qualite/origine/types/GET_ORIGINES';
import { GET_ORIGINES } from '../../../../../../federation/demarche-qualite/origine/query';
import UserInput from '../../../../../Common/UserInput';
import CustomSelectUser from '../../../../../Common/CustomSelectUser';
import { useImportance, useUrgence } from '../../../../../hooks';
import useReunionAction from './../../hooks/useReunionAction';
import moment from 'moment';

interface FicheCompteRenduFormProps {
  match: {
    params: {
      id: string;
    };
  };
}

const FicheCompteRenduForm: FC<FicheCompteRenduFormProps & RouteComponentProps> = ({
  history: { push },
  match: {
    params: { id },
  },
}) => {
  const client = useApolloClient();

  const isNewFicheCompteRendu = 'new' === id;

  const [openModalAnimateur, setOpenModalAnimateur] = useState<boolean>(false);
  const [openModalResponsable, setOpenModalResponsable] = useState<boolean>(false);
  const [openModalParticipants, setOpenModalParticipants] = useState<boolean>(false);
  const [reunionActionToEdit, setReunionActionToEdit] = useState<ReunionAction>();
  const [participantTypeToEdit, setParticipantTypeToEdit] = useState<string>();

  const [selectedUserAnimateurs, setSelectedUserAnimateurs] = useState<any>([]);
  const [selectedUserResponsables, setSelectedUserResponsables] = useState<any>([]);
  const importances = useImportance();
  const urgences = useUrgence();
  const { updateReunionAction } = useReunionAction();

  const origines = useQuery<GET_ORIGINES_TYPE, GET_ORIGINESVariables>(GET_ORIGINES, {
    client: FEDERATION_CLIENT,
  });

  const { loading, error, data: collaborateurs } = useQuery<GET_COLLABORATEURS_TYPE>(
    GET_COLLABORATEURS,
    {
      client: FEDERATION_CLIENT,
    },
  );

  const loadingEncoursActionOperationnelles = useQuery<
    GET_ENCOURS_ACTION_OPERATIONNELLES_TYPE,
    GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles
  >(GET_ENCOURS_ACTION_OPERATIONNELLES, {
    fetchPolicy: 'network-only',
    client: FEDERATION_CLIENT,
  });

  const loadingEncoursFicheAmeliorations = useQuery<
    GET_ENCOURS_FICHE_AMELIORATIONS_TYPE,
    GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations
  >(GET_ENCOURS_FICHE_AMELIORATIONS, {
    fetchPolicy: 'network-only',
    client: FEDERATION_CLIENT,
  });

  const loadingEncoursFicheIncidents = useQuery<
    GET_ENCOURS_FICHE_INCIDENTS_TYPE,
    GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents
  >(GET_ENCOURS_FICHE_INCIDENTS, {
    fetchPolicy: 'network-only',
    client: FEDERATION_CLIENT,
  });

  const { loading: getReunionLoading, error: getReunionError, data: getReunionData } = useQuery<
    GET_REUNION_TYPE,
    GET_REUNIONVariables
  >(GET_REUNION, {
    variables: {
      id: id,
    },
    fetchPolicy: 'network-only',
    client: FEDERATION_CLIENT,
    skip: isNewFicheCompteRendu,
  });

  const [addReunion, creationReunion] = useMutation<CREATE_REUNION_TYPE, CREATE_REUNIONVariables>(
    CREATE_REUNION,
    {
      onCompleted: () => {
        displaySnackBar(client, {
          message: 'La réunion a été ajoutée avec succès !',
          isOpen: true,
          type: 'SUCCESS',
        });
        // if (setOpenFormModal) {
        //   setOpenFormModal(false);
        // }
        push('/demarche-qualite/compte-rendu');
      },
      onError: () => {
        displaySnackBar(client, {
          message: "Des erreurs se sont survenues pendant l'ajout de la réunion",
          isOpen: true,
          type: 'ERROR',
        });
      },
      client: FEDERATION_CLIENT,
    },
  );

  const [updateReunion, modificationReunion] = useMutation<
    UPDATE_REUNION_TYPE,
    UPDATE_REUNIONVariables
  >(UPDATE_REUNION, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La réunion a été modifié avec succès !',
        isOpen: true,
        type: 'SUCCESS',
      });
      push('/demarche-qualite/compte-rendu');
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la modification de la réunion',
        isOpen: true,
        type: 'ERROR',
      });
    },

    client: FEDERATION_CLIENT,
  });

  const handleRequestSave = (dataToSave: RequestSavingCompteRendu) => {
    console.log('**Data to save**', dataToSave);
    if (isNewFicheCompteRendu) {
      addReunion({
        variables: {
          input: {
            idUserAnimateur: dataToSave.idUserAnimateur,
            idUserParticipants: dataToSave.idUserParticipants,
            idResponsable: dataToSave.idResponsable,
            description: dataToSave.description,
            actions: dataToSave.actions.map(action => ({ ...action, id: undefined })),
          },
        },
      });
    } else {
      updateReunion({
        variables: {
          id: id,
          input: {
            idUserAnimateur: dataToSave.idUserAnimateur,
            idUserParticipants: dataToSave.idUserParticipants,
            idResponsable: dataToSave.idResponsable,
            description: dataToSave.description,
            actions: dataToSave.actions.map(action => ({ ...action, id: undefined })),
          },
        },
      });
    }
  };

  const handleGoBack = () => {
    push(`/demarche-qualite/compte-rendu`);
  };

  useEffect(() => {
    if (isNewFicheCompteRendu) {
      setSelectedUserAnimateurs([]);
      setSelectedUserResponsables([]);
    } else if (getReunionData?.dQReunion) {
      setSelectedUserAnimateurs([getReunionData?.dQReunion.animateur]);
      setSelectedUserResponsables([getReunionData?.dQReunion.responsable]);
    }
  }, [id, getReunionData?.dQReunion]);

  const handleParticipantsClicked = (reunionAction: ReunionAction, type: string) => {
    setReunionActionToEdit(reunionAction);
    setParticipantTypeToEdit(type);
    setOpenModalParticipants(true);
  };
  const handleChangeParticipants = (selected: any) => {
    setOpenModalParticipants(false);
    if (reunionActionToEdit && selected) {
      const typeAssocie = reunionActionToEdit.typeAssocie;
      const previousIdUserParticipants = (typeAssocie.participants || []).map(({ id }) => id);
      const previousIdUserParticipantsAInformer = (typeAssocie.participantsAInformer || []).map(
        ({ id }) => id,
      );

      const newParticipants = selected.map(({ id }) => ({
        idUser: id,
        type: participantTypeToEdit,
      }));

      const idUserParticipants =
        participantTypeToEdit === 'PARTICIPANT_EN_CHARGE'
          ? [
              ...previousIdUserParticipantsAInformer.map(idUser => ({
                idUser,
                type: 'PARTICIPANT_A_INFORMER',
              })),
              ...newParticipants,
            ]
          : [
              ...previousIdUserParticipants.map(idUser => ({
                idUser,
                type: 'PARTICIPANT_EN_CHARGE',
              })),
              ...newParticipants,
            ];

      updateReunionAction(reunionActionToEdit, { idUserParticipants });
    }
  };

  const computeDateEcheanceFromUrgence = (code: 'A' | 'B' | 'C'): string => {
    switch (code) {
      case 'A':
        return moment().toString();

      case 'B':
        return moment()
          .endOf('isoWeek')
          .add(1, 'week')
          .toISOString();
      default:
        return moment()
          .startOf('isoWeek')
          .add(2, 'week')
          .toISOString();
    }
  };

  const handleChangeUrgence = (reunionAction: ReunionAction, urgence: any) => {
    updateReunionAction(reunionAction, {
      dateEcheance: computeDateEcheanceFromUrgence(urgence.code),
      idUrgence: undefined,
    });
  };

  const handleChangeImportance = (reunionAction: ReunionAction, importance: any) => {
    updateReunionAction(reunionAction, { idImportance: importance.id, idUrgence: undefined });
  };

  return (
    <>
      <CustomSelectUser
        withNotAssigned={false}
        selected={
          participantTypeToEdit === 'PARTICIPANT_EN_CHARGE'
            ? reunionActionToEdit?.typeAssocie.participants
            : reunionActionToEdit?.typeAssocie.participantsAInformer
        }
        setSelected={handleChangeParticipants}
        openModal={openModalParticipants}
        setOpenModal={setOpenModalParticipants}
      />
      <CompteRenduFormPage
        origines={{
          ...origines,
          data: origines.data?.origines.nodes|| [],
        }}
        importances={importances.data?.importances.nodes || []}
        urgences={urgences.data?.urgences.nodes || []}
        key={id}
        loading={
          loading ||
          getReunionLoading ||
          loadingEncoursFicheAmeliorations.loading ||
          loadingEncoursFicheIncidents.loading ||
          loadingEncoursActionOperationnelles.loading ||
          importances.loading ||
          urgences.loading
        }
        onParticipantsClicked={handleParticipantsClicked}
        onChangeImportance={handleChangeImportance}
        onChangeUrgence={handleChangeUrgence}
        animateurComponent={
          <UserInput
            openModal={openModalAnimateur}
            withNotAssigned={false}
            singleSelect={true}
            setOpenModal={setOpenModalAnimateur}
            selected={selectedUserAnimateurs}
            setSelected={setSelectedUserAnimateurs}
            label="Animateur"
          />
        }
        responsableComponent={
          <UserInput
            openModal={openModalResponsable}
            withNotAssigned={false}
            singleSelect={true}
            setOpenModal={setOpenModalResponsable}
            selected={selectedUserResponsables}
            setSelected={setSelectedUserResponsables}
            label="Responsable Compte Rendu"
          />
        }
        selectedUserAnimateur={selectedUserAnimateurs ? selectedUserAnimateurs[0] : undefined}
        selectedUserResponsable={selectedUserResponsables ? selectedUserResponsables[0] : undefined}
        saving={isNewFicheCompteRendu ? creationReunion.loading : modificationReunion.loading}
        mode={isNewFicheCompteRendu ? 'creation' : 'modification'}
        reunion={getReunionData?.dQReunion as any}
        error={getReunionError}
        onRequestSave={handleRequestSave}
        onRequestGoBack={handleGoBack}
        presence={{
          loading: loading,
          error: error,
          data: collaborateurs?.collaborateurs as any,
        }}
        ficheAmeliorations={{
          ...loadingEncoursFicheAmeliorations,
          data: loadingEncoursFicheAmeliorations.data?.dQEncoursFicheAmeliorations as any,
        }}
        ficheIncidents={{
          ...loadingEncoursFicheIncidents,
          data: loadingEncoursFicheIncidents.data?.dQEncoursFicheIncidents as any,
        }}
        actionOperationnelles={{
          ...loadingEncoursActionOperationnelles,
          data: loadingEncoursActionOperationnelles.data?.dQEncoursActionOperationnelles as any,
        }}
      />
    </>
  );
};

export default FicheCompteRenduForm;
