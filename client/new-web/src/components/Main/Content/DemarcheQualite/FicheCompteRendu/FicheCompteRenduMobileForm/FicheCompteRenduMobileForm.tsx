import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import { CompteRenduMobileFormPage } from '@app/ui-kit';
import {
  RequestSavingCompteRendu,
  Reunion,
} from '@app/ui-kit/components/pages/CompteRenduFormPage/types';
import { Box } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { GET_ENCOURS_ACTION_OPERATIONNELLES } from '../../../../../../federation/demarche-qualite/action-operationnelle/query';
import {
  GET_ENCOURS_ACTION_OPERATIONNELLES as GET_ENCOURS_ACTION_OPERATIONNELLES_TYPE,
  GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles,
} from '../../../../../../federation/demarche-qualite/action-operationnelle/types/GET_ENCOURS_ACTION_OPERATIONNELLES';
import { GET_ENCOURS_FICHE_AMELIORATIONS } from '../../../../../../federation/demarche-qualite/fiche-amelioration/query';
import {
  GET_ENCOURS_FICHE_AMELIORATIONS as GET_ENCOURS_FICHE_AMELIORATIONS_TYPE,
  GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations,
} from '../../../../../../federation/demarche-qualite/fiche-amelioration/types/GET_ENCOURS_FICHE_AMELIORATIONS';
import { GET_ENCOURS_FICHE_INCIDENTS } from '../../../../../../federation/demarche-qualite/fiche-incident/query';
import {
  GET_ENCOURS_FICHE_INCIDENTS as GET_ENCOURS_FICHE_INCIDENTS_TYPE,
  GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents,
} from '../../../../../../federation/demarche-qualite/fiche-incident/types/GET_ENCOURS_FICHE_INCIDENTS';
import {
  CREATE_REUNION,
  UPDATE_REUNION,
} from '../../../../../../federation/demarche-qualite/reunion/mutation';
import { GET_REUNION } from '../../../../../../federation/demarche-qualite/reunion/query';
import {
  CREATE_REUNION as CREATE_REUNION_TYPE,
  CREATE_REUNIONVariables,
} from '../../../../../../federation/demarche-qualite/reunion/types/CREATE_REUNION';
import {
  GET_REUNION as GET_REUNION_TYPE,
  GET_REUNIONVariables,
} from '../../../../../../federation/demarche-qualite/reunion/types/GET_REUNION';
import {
  UPDATE_REUNION as UPDATE_REUNION_TYPE,
  UPDATE_REUNIONVariables,
} from '../../../../../../federation/demarche-qualite/reunion/types/UPDATE_REUNION';
import { GET_ORIGINES as GET_ORIGINES_TYPE, GET_ORIGINESVariables } from '../../../../../../federation/demarche-qualite/origine/types/GET_ORIGINES';
import { GET_ORIGINES } from '../../../../../../federation/demarche-qualite/origine/query';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import AvatarInput from '../../../../../Common/AvatarInput';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import AssignTaskUserModal from '../../../../../Common/CustomSelectUser';
import { useImportance, useUrgence } from '../../../../../hooks';

interface CompteRenduMobileFormProps {
  match: {
    params: {
      id: string | undefined;
    };
  };
  reunionToEdit?: Reunion;
  setOpenFormModal?: (value: boolean) => void;
}

const CompteRenduMobileForm: FC<RouteComponentProps & CompteRenduMobileFormProps> = ({
  history: { push },
  match: {
    params: { id },
  },
  reunionToEdit,
  setOpenFormModal,
}) => {
  const client = useApolloClient();
  const importances = useImportance()
  const urgences = useUrgence()

  const [userParticipants, setUserParticipants] = useState<any[]>([]);
  const [reunion, setReunion] = useState<Reunion | undefined>();
  const [animateur, setAnimateur] = useState<any>([]);
  const [responsable, setResponsable] = useState<any>([]);
  const [openConcernedParticipantModal, setOpenConcernedParticipantModal] = useState<boolean>(
    false,
  );
  const [openAnimateurModal, setOpenAnimateurModal] = useState<boolean>(false);
  const [openResponsableModal, setOpenResponsableModal] = useState<boolean>(false);
  const [mutationLoading, setMutationLoading] = useState<boolean>(false);

  const isNewReunion = reunion?.id || reunionToEdit ? false : true;

  const [loadReunion, reunionQuery] = useLazyQuery<GET_REUNION_TYPE, GET_REUNIONVariables>(
    GET_REUNION,
    {
      client: FEDERATION_CLIENT,
      fetchPolicy: 'network-only',
    },
  );

  const origines = useQuery<GET_ORIGINES_TYPE, GET_ORIGINESVariables>(GET_ORIGINES, {
    client: FEDERATION_CLIENT
  })

  useEffect(() => {
    if (!isNewReunion && !reunionQuery.loading) {
      if (!reunionQuery.client) {
        if (id || reunionToEdit?.id) {
          loadReunion({
            variables: {
              id: reunionToEdit?.id || id || '',
            },
          });
        }
      } else if (reunionQuery.data?.dQReunion) {
        const reunion = reunionQuery.data?.dQReunion;
        const animateur = reunion?.animateur;
        const responsable = reunion?.responsable;
        const userParticipants = reunion?.participants;
        if (reunion) {
          setReunion(reunion as any);
        }
        if (animateur) {
          setAnimateur([animateur]);
        }
        if (responsable) {
          setResponsable([responsable]);
        }
        if (userParticipants) {
          setUserParticipants(userParticipants);
        }
      }
    }
  }, [id, reunionQuery.loading]);

  const loadingEncoursActionOperationnelles = useQuery<
    GET_ENCOURS_ACTION_OPERATIONNELLES_TYPE,
    GET_ENCOURS_ACTION_OPERATIONNELLES_dQEncoursActionOperationnelles
  >(GET_ENCOURS_ACTION_OPERATIONNELLES, {
    fetchPolicy: 'network-only',
    client: FEDERATION_CLIENT,
  });

  const loadingEncoursFicheAmeliorations = useQuery<
    GET_ENCOURS_FICHE_AMELIORATIONS_TYPE,
    GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations
  >(GET_ENCOURS_FICHE_AMELIORATIONS, {
    fetchPolicy: 'network-only',
    client: FEDERATION_CLIENT,
  });

  const loadingEncoursFicheIncidents = useQuery<
    GET_ENCOURS_FICHE_INCIDENTS_TYPE,
    GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents
  >(GET_ENCOURS_FICHE_INCIDENTS, {
    fetchPolicy: 'network-only',
    client: FEDERATION_CLIENT,
  });

  const handleOpenConcernedParticipant = event => {
    event.stopPropagation();
    setOpenConcernedParticipantModal(true);
  };

  const handleOpenAnimateurModal = event => {
    event.stopPropagation();
    setOpenAnimateurModal(true);
  };

  const handleOpenResponsableModal = event => {
    event.stopPropagation();
    setOpenResponsableModal(true);
  };

  const handleSelectedCollaborateurs = (selected: any) => {
    setUserParticipants(selected);
  };

  const handleSelectedAnimateur = (selected: any) => {
    console.log('+++ selected : ', selected);
    setAnimateur(selected);
  };

  const handleSelectedResponsable = (selected: any) => {
    setResponsable(selected);
  };

  const handleGoBack = (): void => {
    push(`/demarche-qualite/compte-rendu`);
  };

  const [addReunion, creationReunion] = useMutation<CREATE_REUNION_TYPE, CREATE_REUNIONVariables>(
    CREATE_REUNION,
    {
      onCompleted: () => {
        displaySnackBar(client, {
          message: 'La réunion a été ajoutée avec succès !',
          isOpen: true,
          type: 'SUCCESS',
        });
        setMutationLoading(false);
        if (setOpenFormModal) {
          setOpenFormModal(false);
        }
        push('/demarche-qualite/compte-rendu');
      },
      onError: () => {
        displaySnackBar(client, {
          message: "Des erreurs se sont survenues pendant l'ajout de la réunion",
          isOpen: true,
          type: 'ERROR',
        });
      },
      client: FEDERATION_CLIENT,
    },
  );

  const [updateReunion, modificationReunion] = useMutation<
    UPDATE_REUNION_TYPE,
    UPDATE_REUNIONVariables
  >(UPDATE_REUNION, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La réunion a été modifié avec succès !',
        isOpen: true,
        type: 'SUCCESS',
      });
      setMutationLoading(false);
      if (setOpenFormModal) {
        setOpenFormModal(false);
      }
      push('/demarche-qualite/compte-rendu');
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la modification de la réunion',
        isOpen: true,
        type: 'ERROR',
      });
    },

    client: FEDERATION_CLIENT,
  });

  const upsertReunion = (reunionToSave: RequestSavingCompteRendu) => {
    if (isNewReunion) {
      addReunion({
        variables: {
          input: {
            idUserAnimateur: reunionToSave.idUserAnimateur,
            idUserParticipants: reunionToSave.idUserParticipants,
            idResponsable: reunionToSave.idResponsable,
            description: reunionToSave.description,
            actions: reunionToSave.actions.map(action => ({ ...action, id: undefined })),
          },
        },
      });
    } else {
      updateReunion({
        variables: {
          id: reunionToSave?.id || '',
          input: {
            idUserAnimateur: reunionToSave.idUserAnimateur,
            idUserParticipants: reunionToSave.idUserParticipants,
            idResponsable: reunionToSave.idResponsable,
            description: reunionToSave.description,
            actions: reunionToSave.actions.map(action => ({ ...action, id: undefined })),
          },
        },
      });
    }
  };

  const handleSave = (ficheToSave: RequestSavingCompteRendu) => {
    setMutationLoading(true);
    upsertReunion(ficheToSave);
  };

  const collaborateurComponent = (
    <Box onClick={handleOpenConcernedParticipant} width="100%">
      <AvatarInput list={userParticipants} label="Presence" />
      <AssignTaskUserModal
        openModal={openConcernedParticipantModal}
        setOpenModal={setOpenConcernedParticipantModal}
        withNotAssigned={false}
        selected={userParticipants}
        setSelected={handleSelectedCollaborateurs}
        title="Presence"
        searchPlaceholder="Rechercher..."
        withAssignTeam={false}
      />
    </Box>
  );

  const idUserParticipants = userParticipants.map(userParticipant => userParticipant.id);
  const idUserAnimateur = animateur[0]?.id;
  const idResponsable = responsable[0]?.id;

  console.log('+++ : iNew', isNewReunion);

  const animateurComponent = (
    <Box onClick={handleOpenAnimateurModal} width="100%">
      <AvatarInput list={animateur} label="Animateur" />
      <AssignTaskUserModal
        openModal={openAnimateurModal}
        setOpenModal={setOpenAnimateurModal}
        withNotAssigned={false}
        selected={animateur}
        setSelected={handleSelectedAnimateur}
        title="Animateur"
        searchPlaceholder="Rechercher..."
        withAssignTeam={false}
        singleSelect={true}
      />
    </Box>
  );

  const responsableComponent = (
    <Box onClick={handleOpenResponsableModal} width="100%">
      <AvatarInput list={responsable} label="Resonsable Compte Rendu" />
      <AssignTaskUserModal
        openModal={openResponsableModal}
        setOpenModal={setOpenResponsableModal}
        withNotAssigned={false}
        selected={responsable}
        setSelected={handleSelectedResponsable}
        title="Resonsable Compte Rendu"
        searchPlaceholder="Rechercher..."
        withAssignTeam={false}
        singleSelect={true}
      />
    </Box>
  );

  return (
    <CompteRenduMobileFormPage
      origines={{
        ...origines,
        data: origines.data?.origines.nodes  || [],
      }}
      importances={importances.data?.importances.nodes || []}
      urgences={urgences.data?.urgences.nodes || []}
      reunion={reunion}
      mode={isNewReunion ? 'creation' : 'modification'}
      loading={reunionQuery.loading ||
        importances.loading ||
        urgences.loading}
      onParticipantsClicked={() => console.log('clicked')}
      onChangeImportance={() => console.log('importance')}
      onChangeUrgence={() => console.log('urgence')}
      saving={mutationLoading}
      idUserParticipants={idUserParticipants}
      selectCollaborateurComponent={collaborateurComponent}
      selectAnimateurComponent={animateurComponent}
      selectResponsableComponent={responsableComponent}
      onRequestGoBack={handleGoBack}
      onRequestSave={handleSave}
      idUserAnimateur={idUserAnimateur}
      idResponsable={idResponsable}
      ficheAmeliorations={{
        ...loadingEncoursFicheAmeliorations,
        data: loadingEncoursFicheAmeliorations.data?.dQEncoursFicheAmeliorations as any,
      }}
      ficheIncidents={{
        ...loadingEncoursFicheIncidents,
        data: loadingEncoursFicheIncidents.data?.dQEncoursFicheIncidents as any,
      }}
      actionOperationnelles={{
        ...loadingEncoursActionOperationnelles,
        data: loadingEncoursActionOperationnelles.data?.dQEncoursActionOperationnelles as any,
      }}
    />
  );
};

export default withRouter(CompteRenduMobileForm);
