import { createMuiTheme } from '@material-ui/core';

const defaultTheme = createMuiTheme();

export default theme =>
  createMuiTheme({
    typography: {
      fontFamily: 'Roboto',
      h1: {
        fontSize: 22,
        fontWeight: 600,
        lineHeight: 1.25,
      },
      h2: {
        fontSize: 18,
        fontWeight: 600,
        lineHeight: 1.25,
      },
      subtitle1: {
        fontSize: 16,
        fontWeight: 500,
        lineHeight: 1.25,
      },
      subtitle2: {
        fontSize: 14,
        fontWeight: 500,
        lineHeight: 1.25,
      },
      caption: {
        fontSize: 12,
      },
    },
    palette: theme && theme.palette ? theme.palette : defaultTheme.palette,
    overrides: {
      MuiAccordion: {
        root: {
          '&::before': {
            content: 'none',
          },
        },
      },
      MuiAccordionSummary: {
        root: {
          minHeight: 40,
          padding: 0,
          '&$expanded': {
            minHeight: 40,
          },
        },
        content: {
          margin: defaultTheme.spacing(1, 0),
          '&$expanded': {
            margin: defaultTheme.spacing(1, 0),
          },
        },
      },
      MuiAccordionDetails: {
        root: { padding: 0 },
      },
      MuiOutlinedInput: {
        input: {
          padding: `15.5px 14px`,
        },
      },
      MuiInputLabel: {
        outlined: {
          transform: `translate(14px, 18px) scale(1)`,
        },
      },
      MuiPaper: {
        elevation2: {
          boxShadow: `0px 4px 12px 0 rgba(0,0,0,.16)`,
        },
      },
      MuiCardContent: {
        root: {
          padding: `${defaultTheme.spacing(1.5)}px !important`,
        },
      },
      // MuiSvgIcon: {
      //   root: {
      //     color: '#212121',
      //   },
      // },
    },
  });
