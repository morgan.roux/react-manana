import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import { Backdrop } from '@app/ui-kit';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { PARTENAIRE_SERVICE } from '../../../../Constant/roles';
import { CREATE_ACTIVITE } from '../../../../federation/basis/mutation';
import {
  CREATE_ACTIVITE as CREATE_ACTIVITE_TYPE,
  CREATE_ACTIVITEVariables,
} from '../../../../federation/basis/types/CREATE_ACTIVITE';
import { GET_MES_CATEGORIES } from '../../../../federation/ged/categorie/query';
import { GET_MES_CATEGORIES as GET_MES_CATEGORIES_TYPE } from '../../../../federation/ged/categorie/types/GET_MES_CATEGORIES';
import {
  CREATE_DOCUMENT_CATEGORIE,
  DELETE_DOCUMENT_CATEGORIE,
  TOGGLE_DOCUMENT_TO_FAVORITE,
  UPDATE_DOCUMENT_CATEGORIE,
} from '../../../../federation/ged/document/mutation';
import {
  GET_DOCUMENTS_CATEGORIE,
  GET_DOCUMENT_CATEGORIE,
  SEARCH_DOCUMENT,
  GET_GED_DOCUMENT_FOURNISSEURS,
} from '../../../../federation/ged/document/query';
import {
  CREATE_DOCUMENT_CATEGORIE as CREATE_DOCUMENT_CATEGORIE_TYPE,
  CREATE_DOCUMENT_CATEGORIEVariables,
} from '../../../../federation/ged/document/types/CREATE_DOCUMENT_CATEGORIE';
import {
  DELETE_DOCUMENT_CATEGORIE as DELETE_DOCUMENT_CATEGORIE_TYPE,
  DELETE_DOCUMENT_CATEGORIEVariables,
} from '../../../../federation/ged/document/types/DELETE_DOCUMENT_CATEGORIE';
import {
  GET_DOCUMENTS_CATEGORIE as GET_DOCUMENTS_CATEGORIE_TYPE,
  GET_DOCUMENTS_CATEGORIEVariables,
} from '../../../../federation/ged/document/types/GET_DOCUMENTS_CATEGORIE';
import {
  GET_DOCUMENT_CATEGORIE as GET_DOCUMENT_CATEGORIE_TYPE,
  GET_DOCUMENT_CATEGORIEVariables,
} from '../../../../federation/ged/document/types/GET_DOCUMENT_CATEGORIE';
import {
  SEARCH_DOCUMENT as SEARCH_DOCUMENT_TYPE,
  SEARCH_DOCUMENTVariables,
} from '../../../../federation/ged/document/types/SEARCH_DOCUMENT';
import {
  GED_DOCUMENT_FOURNISSEURS as GED_DOCUMENT_FOURNISSEURS_TYPE,
  GED_DOCUMENT_FOURNISSEURSVariables,
} from '../../../../federation/ged/document/types/GED_DOCUMENT_FOURNISSEURS';
import {
  TOGGLE_DOCUMENT_TO_FAVORITE as TOGGLE_DOCUMENT_TO_FAVORITE_TYPE,
  TOGGLE_DOCUMENT_TO_FAVORITEVariables,
} from '../../../../federation/ged/document/types/TOGGLE_DOCUMENT_TO_FAVORITE';
import {
  UPDATE_DOCUMENT_CATEGORIE as UPDATE_DOCUMENT_CATEGORIE_TYPE,
  UPDATE_DOCUMENT_CATEGORIEVariables,
} from '../../../../federation/ged/document/types/UPDATE_DOCUMENT_CATEGORIE';
import { GET_COMMENTS } from '../../../../graphql/Comment';
import { COMMENTS, COMMENTSVariables } from '../../../../graphql/Comment/types/COMMENTS';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_USER_SMYLEY } from '../../../../graphql/Smyley';
import {
  CREATE_USER_SMYLEY as CREATE_USER_SMYLEY_TYPE,
  CREATE_USER_SMYLEYVariables,
} from '../../../../graphql/Smyley/types/CREATE_USER_SMYLEY';
import { SMYLEYS_smyleys } from '../../../../graphql/Smyley/types/SMYLEYS';
import { getUser } from '../../../../services/LocalStorage';
import { uploadToS3 } from '../../../../services/S3';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import { formatFilename } from '../../../Common/Dropzone/Dropzone';
import { FEDERATION_CLIENT } from '../../../Dashboard/DemarcheQualite/apolloClientFederation';
import EspaceDocumentaire, { DocumentCategorie, IDocumentSearch } from './EspaceDocumentaire';
import useGedCategoriesAnneeMois from '../../../hooks/ged/useGedCategoriesAnneeMois';

const GestionEspaceDocumentaire: FC<RouteComponentProps> = ({ history, match: { params } }) => {
  const user = getUser();
  const idDocument = (params as any).idDocument;

  const userPartenaireId =
    user?.role?.code === PARTENAIRE_SERVICE ? user.userPartenaire?.id : undefined;

  const take = 12;
  const client = useApolloClient();
  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const [mode, setMode] = useState<'creation' | 'remplacement'>('creation');
  const [skip, setSkip] = useState<number>(0);

  const [searchParameters, setSearchParameters] = useState<IDocumentSearch>({
    proprietaire: 'TOUT',
    accesRapide: userPartenaireId ? 'EN_ATTENTE_APPROBATION' : undefined,
  });

  const {
    loading: categoriesLoading,
    error: categoriesError,
    data: categoriesData,
    refetch: refetchCategories,
  } = useQuery<GET_MES_CATEGORIES_TYPE, GET_MES_CATEGORIES_TYPE>(GET_MES_CATEGORIES, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const facturesFournisseurs = useQuery<
    GED_DOCUMENT_FOURNISSEURS_TYPE,
    GED_DOCUMENT_FOURNISSEURSVariables
  >(GET_GED_DOCUMENT_FOURNISSEURS, {
    client: FEDERATION_CLIENT,
    variables: {
      input: {
        type: 'FACTURE',
        statut: 'APPROUVES',
        validation: userPartenaireId ? 'PRESTATAIRE_SERVICE' : 'INTERNE',
        idPartenaireValidateur: user.userPartenaire?.id,
      },
    },
    fetchPolicy: 'cache-and-network',
  });

  const facturesRejeteesInterneCategories = useGedCategoriesAnneeMois({
    input: {
      statut: 'REFUSES',
      validation: 'INTERNE',
      type: 'FACTURE',
      idPartenaireValidateur: user?.userPartenaire?.id,
    },
  });

  const facturesRejeteesPrestataireServiceCategories = useGedCategoriesAnneeMois({
    input: {
      statut: 'REFUSES',
      validation: 'PRESTATAIRE_SERVICE',
      type: 'FACTURE',
      idPartenaireValidateur: user?.userPartenaire?.id,
    },
  });

  const [
    getCommentaires,
    { loading: commentsLoading, error: commentsError, data: comments, refetch: refetchComments },
  ] = useLazyQuery<COMMENTS, COMMENTSVariables>(GET_COMMENTS);

  const handleRefetchComments = () => {
    refetchComments();
    refetchDocument();
  };

  const handleRefetchSmyleys = () => {
    refetchDocument();
  };

  const [
    getDocument,
    { loading: documentLoading, error: documentError, data: document, refetch: refetchDocument },
  ] = useLazyQuery<GET_DOCUMENT_CATEGORIE_TYPE, GET_DOCUMENT_CATEGORIEVariables>(
    GET_DOCUMENT_CATEGORIE,
    {
      client: FEDERATION_CLIENT,
      fetchPolicy: 'network-only',
    },
  );

  const [addActivite] = useMutation<CREATE_ACTIVITE_TYPE, CREATE_ACTIVITEVariables>(
    CREATE_ACTIVITE,
    {
      client: FEDERATION_CLIENT,
      onCompleted: () => {
        refetchDocument();
      },
    },
  );

  const [
    searchDocuments,
    {
      loading: documentsSearchLoading,
      error: documentsSearchError,
      data: documentsSearch,
      refetch: refetchSearch,
      fetchMore: fetchMoreSearch,
    },
  ] = useLazyQuery<SEARCH_DOCUMENT_TYPE, SEARCH_DOCUMENTVariables>(SEARCH_DOCUMENT, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const documents = documentsSearch?.searchGedDocuments?.data || [];
  const total = documentsSearch?.searchGedDocuments.total || 0;

  const variablesInput = {
    ...(searchParameters as any),
    idPartenaireValidateur: userPartenaireId,
    limit: take,
    offset: skip,
  };

  const [addDocumentCategorie] = useMutation<
    CREATE_DOCUMENT_CATEGORIE_TYPE,
    CREATE_DOCUMENT_CATEGORIEVariables
  >(CREATE_DOCUMENT_CATEGORIE, {
    onCompleted: data => {
      setSaving(false);
      setSaved(true);
      handleRefetchAll(true);

      displaySnackBar(client, {
        message: 'Le document a été ajouté avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      /*searchDocuments({
        variables: {
          input: variablesInput,
        },
      });*/
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs se sont survenues pendant l'ajout du document!",
        type: 'ERROR',
        isOpen: true,
      });

      setSaving(false);
    },
    client: FEDERATION_CLIENT,
  });

  const [
    toggleDocumentFavorite,
    { loading: toggleLoading, error: toggleError, data: toggle },
  ] = useMutation<TOGGLE_DOCUMENT_TO_FAVORITE_TYPE, TOGGLE_DOCUMENT_TO_FAVORITEVariables>(
    TOGGLE_DOCUMENT_TO_FAVORITE,
    {
      onError: () => {
        displaySnackBar(client, {
          message: "Des erreurs se sont survenues pendant l'ajout du document aux favoris!",
          type: 'ERROR',
          isOpen: true,
        });
      },

      onCompleted: () => {
        refetchSearch();
      },

      client: FEDERATION_CLIENT,
    },
  );

  useEffect(() => {
    if (toggle?.toggleToGedDocumentFavoris && !toggleLoading && !toggleError) {
      displaySnackBar(client, {
        message: `Le document a été ${
          toggle?.toggleToGedDocumentFavoris?.favoris ? 'ajouté aux' : 'retiré des'
        } favoris!`,
        type: 'SUCCESS',
        isOpen: true,
      });
    }
  }, [toggle, toggleLoading, toggleError]);

  const [updateDocumentCategorie] = useMutation<
    UPDATE_DOCUMENT_CATEGORIE_TYPE,
    UPDATE_DOCUMENT_CATEGORIEVariables
  >(UPDATE_DOCUMENT_CATEGORIE, {
    onCompleted: () => {
      setSaving(false);
      setSaved(true);
      displaySnackBar(client, {
        message: 'Le document a été remplacé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });

      handleRefetchAll(true);
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant le remplacement du document!',
        type: 'ERROR',
        isOpen: true,
      });
      setSaving(false);
    },
    client: FEDERATION_CLIENT,
  });

  const [deleteDocumentCategorie] = useMutation<
    DELETE_DOCUMENT_CATEGORIE_TYPE,
    DELETE_DOCUMENT_CATEGORIEVariables
  >(DELETE_DOCUMENT_CATEGORIE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le document a été supprimé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });

      handleRefetchAll(true);
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression du document !',
        type: 'ERROR',
        isOpen: true,
      });
    },
    update: (cache, deletionResult) => {
      if (deletionResult?.data?.deleteOneGedDocument?.id) {
        const previousList = cache.readQuery<
          GET_DOCUMENTS_CATEGORIE_TYPE,
          GET_DOCUMENTS_CATEGORIEVariables
        >({
          query: GET_DOCUMENTS_CATEGORIE,
        })?.gedDocuments;

        const deletedId = deletionResult.data.deleteOneGedDocument.id;

        cache.writeQuery<GET_DOCUMENTS_CATEGORIE_TYPE>({
          query: GET_DOCUMENTS_CATEGORIE,
          data: {
            gedDocuments: {
              ...(previousList || {}),
              nodes: (previousList?.nodes || []).filter(item => item?.id !== deletedId),
            } as any,
          },
        });
      }
    },
    client: FEDERATION_CLIENT,
  });

  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
  });

  const [doLike] = useMutation<CREATE_USER_SMYLEY_TYPE, CREATE_USER_SMYLEYVariables>(
    DO_CREATE_USER_SMYLEY,
  );

  useEffect(() => {
    if (toggle?.toggleToGedDocumentFavoris && !toggleLoading && !toggleError) {
      displaySnackBar(client, {
        message: `Le document a été ${
          toggle?.toggleToGedDocumentFavoris?.favoris ? 'ajouté aux' : 'retiré des'
        } favoris!`,
        type: 'SUCCESS',
        isOpen: true,
      });
    }
  }, [toggle, toggleLoading, toggleError]);

  useEffect(() => {
    console.log(
      '---------------------variablesInput----------------------------: ',
      variablesInput,
    );
    searchDocuments({
      variables: {
        input: variablesInput,
      },
    });
  }, [searchParameters]);

  // useEffect(() => {

  //   if (idDocument && !documentsSearchLoading && !documentsSearchError && !documents.some((document) => document.id === idDocument)) {
  //     history.push('/espace-documentaire')
  //   }
  // }, [documents])

  useEffect(() => {
    if (idDocument) {
      getDoc(idDocument);
    }
  }, [idDocument]);

  const handleRefetchAll = (withSearch: boolean = false) => {
    if (withSearch) {
      refetchSearch();
    }
    refetchSearch();
    refetchCategories();
    facturesFournisseurs.refetch();
    facturesRejeteesInterneCategories.refetch();
    facturesRejeteesPrestataireServiceCategories.refetch();
  };

  const handleToggleDocumentToOrFromFavorite = (document: DocumentCategorie): void => {
    toggleDocumentFavorite({
      variables: {
        idDocument: document?.id as any,
      },
    });
  };

  const handleDeleteDocument = (document: DocumentCategorie): void => {
    deleteDocumentCategorie({
      variables: {
        input: {
          id: document?.id as any,
        },
      },
    });
  };

  const handleDetailsDocument = (document: DocumentCategorie): void => {
   // window.history.pushState(null, '', `#/espace-documentaire/${document.id}`);
    history.push(`/espace-documentaire/${document.id}`)
   // window.history.pushState(null, '', `#/espace-documentaire/${document.id}`);

   // getDoc(document.id || '');
  };

  const handleOnRequestDocument = (id: string) => {
    getDoc(id);
  };

  const getDoc = (id: string) => {
    getDocument({
      variables: {
        id: id,
      },
    });

    addActivite({
      variables: {
        input: {
          activiteTypeCode: 'VISIT',
          itemCode: 'GED_DOCUMENT',
          idItemAssocie: id,
        },
      },
    });

    getCommentaires({
      variables: {
        codeItem: 'GED_DOCUMENT',
        idItemAssocie: id,
      },
    });
  };

  const handleScroll = (): void => {
    fetchMoreSearch({
      variables: {
        input: {
          ...variablesInput,
          offset: skip + take,
        },
      },
      updateQuery: (prev: any, fetchMore) => {
        if (!fetchMore) {
          return prev;
        }

        const mergedData: any = {
          ...prev,
          searchGedDocuments: {
            ...prev.searchGedDocuments,
            data: [
              ...prev.searchGedDocuments.data,
              ...(fetchMore.fetchMoreResult?.searchGedDocuments?.data || []),
            ],
            total: fetchMore.fetchMoreResult?.searchGedDocuments?.total,
          },
        };

        return mergedData;
      },
    });
    setSkip(skip + take);
  };

  const upsertDocument = ({
    id,
    idSousCategorie,
    description,
    nomenclature,
    numeroVersion,
    idUserVerificateur,
    idUserRedacteur,
    dateHeureParution,
    dateHeureDebutValidite,
    dateHeureFinValidite,
    motCle1,
    motCle2,
    motCle3,
    fichier,
    idOrigine,
    idOrigineAssocie,
    dateFacture,
    hT,
    tva,
    ttc,
    type,

    numeroFacture,
    factureDateReglement,
    idReglementMode,
    numeroCommande,
    isGenererCommande,
    avoirType,
    avoirCorrespondants,
  }): void => {
    if (!id) {
      setMode('creation');
      addDocumentCategorie({
        variables: {
          input: {
            idSousCategorie: idSousCategorie,
            description: description,
            nomenclature: nomenclature,
            numeroVersion: numeroVersion,
            dateHeureParution: new Date(dateHeureParution),
            dateHeureDebutValidite: new Date(dateHeureDebutValidite),
            dateHeureFinValidite: new Date(dateHeureFinValidite),
            idUserVerificateur: idUserVerificateur,
            idUserRedacteur: idUserRedacteur,
            motCle1: motCle1,
            motCle2: motCle2,
            motCle3: motCle3,
            fichier: fichier,
            idOrigine: idOrigine,
            idOrigineAssocie: idOrigineAssocie,
            factureDate: dateFacture,
            factureTotalHt: hT,
            factureTva: tva,
            factureTotalTtc: ttc,
            type: type,

            numeroFacture,
            factureDateReglement,
            idReglementMode,
            numeroCommande,
            isGenererCommande,
            avoirType,
            avoirCorrespondants,
          },
        },
      }).then(() => {
        refetchSearch();
        refetchCategories();
      });
    } else {
      setMode('remplacement');
      updateDocumentCategorie({
        variables: {
          input: {
            description: description,
            nomenclature: nomenclature,
            numeroVersion: numeroVersion,
            dateHeureParution: dateHeureParution,
            dateHeureDebutValidite: dateHeureDebutValidite,
            dateHeureFinValidite: dateHeureFinValidite,
            idSousCategorie: idSousCategorie,
            idUserVerificateur: idUserVerificateur,
            idUserRedacteur: idUserRedacteur,
            motCle1: motCle1,
            motCle2: motCle2,
            motCle3: motCle3,
            fichier: fichier,
            idOrigine: idOrigine,
            idOrigineAssocie: idOrigineAssocie,
            factureDate: dateFacture,
            factureTotalHt: hT,
            factureTva: tva,
            factureTotalTtc: ttc,
            type: type,

            numeroFacture,
            factureDateReglement,
            idReglementMode,
            numeroCommande,
            isGenererCommande,
            avoirType,
            avoirCorrespondants,
          },
          id: id,
        },
      }).then(() => {
        refetchCategories();
      });
    }
  };

  const showError = () => {
    displaySnackBar(client, {
      message: `Une erreur est survenue pendant le chargement du document`,
      type: 'ERROR',
      isOpen: true,
    });
  };
  const handleSave = (doc: any): void => {
    setSaving(true);
    setSaved(false);
    if (doc.launchUpload) {
      const filePaths = [doc.selectedFile].map(fichier => formatFilename(fichier, ``));
      doCreatePutPresignedUrl({
        variables: {
          filePaths,
        },
      })
        .then(async response => {
          if (response.data?.createPutPresignedUrls) {
            try {
              const presignedUrl = response.data.createPutPresignedUrls[0];
              let fichier: any = undefined;
              if (presignedUrl) {
                try {
                  const uploadResult = await uploadToS3(
                    doc.selectedFile,
                    presignedUrl.presignedUrl,
                  );
                  if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                    fichier = {
                      chemin: presignedUrl.filePath,
                      nomOriginal: doc.selectedFile.name,
                      type: doc.selectedFile.type,
                    };
                  }
                } catch (error) {
                  console.log(
                    '*******************************************ERROR 01************************',
                    error,
                  );
                  setSaving(false);
                  showError();
                  return;
                }
              }
              upsertDocument({
                id: doc.id,
                idSousCategorie: doc.idSousCategorie,
                description: doc.description,
                nomenclature: doc.nomenclature,
                numeroVersion: doc.numeroVersion,
                idUserVerificateur: doc.idUserVerificateur,
                idUserRedacteur: doc.idUserRedacteur,
                dateHeureParution: doc.dateHeureParution,
                dateHeureDebutValidite: doc.dateHeureDebutValidite,
                dateHeureFinValidite: doc.dateHeureFinValidite,
                motCle1: doc.motCle1,
                motCle2: doc.motCle2,
                motCle3: doc.motCle3,
                fichier: fichier,
                idOrigine: doc.origine?.id,
                idOrigineAssocie: doc.correspondant?.id || doc.origineAssocie?.id,
                dateFacture: doc.dateFacture,
                hT: doc.hT ? parseFloat(doc.hT) : undefined,
                tva: doc.tva ? parseFloat(doc.tva) : undefined,
                ttc: doc.ttc ? parseFloat(doc.ttc) : undefined,
                type: doc.type,

                numeroFacture: doc?.numeroFacture,
                factureDateReglement: doc?.factureDateReglement,
                idReglementMode: doc?.idReglementMode,
                numeroCommande: doc?.numeroCommande,
                isGenererCommande: doc?.isGenererCommande,
                avoirType: doc?.avoirType,
                avoirCorrespondants: doc?.avoirCorrespondants,
              });
              return;
            } catch (error) {
              console.log(
                '*******************************************ERROR 02************************',
                error,
              );

              showError();
              setSaving(false);
            }
          }
        })
        .catch(error => {
          console.log(
            '*******************************************ERROR 03************************',
            error,
          );
          showError();
        });
    } else {
      const { nomOriginal, chemin, type } = doc.previousFichier || {};

      upsertDocument({
        id: doc.id,
        idSousCategorie: doc.idSousCategorie,
        description: doc.description,
        nomenclature: doc.nomenclature,
        numeroVersion: doc.numeroVersion,
        idUserVerificateur: doc.idUserVerificateur,
        idUserRedacteur: doc.idUserRedacteur,
        dateHeureParution: doc.dateHeureParution,
        dateHeureDebutValidite: doc.dateHeureDebutValidite,
        dateHeureFinValidite: doc.dateHeureFinValidite,
        motCle1: doc.motCle1,
        motCle2: doc.motCle2,
        motCle3: doc.motCle3,
        fichier: doc.previousFichier ? { nomOriginal, chemin, type } : undefined,
        idOrigine: doc.origine?.id,
        idOrigineAssocie: doc.correspondant?.id,
        dateFacture: 'FACTURE' === doc.type ? doc.dateFacture : undefined,
        hT: 'FACTURE' === doc.type && doc.hT ? parseFloat(doc.hT) : undefined,
        tva: 'FACTURE' === doc.type && doc.tva ? parseFloat(doc.tva) : undefined,
        ttc: 'FACTURE' === doc.type && doc.ttc ? parseFloat(doc.ttc) : undefined,
        type: doc.type,

        numeroFacture: doc?.numeroFacture,
        factureDateReglement: doc?.factureDateReglement,
        idReglementMode: doc?.idReglementMode,
        numeroCommande: doc?.numeroCommande,
        isGenererCommande: doc?.isGenererCommande,
        avoirType: doc?.avoirType,
        avoirCorrespondants: doc?.avoirCorrespondants,
      });
    }
  };

  const handleLike = (document: DocumentCategorie, smyley: SMYLEYS_smyleys | null): void => {
    doLike({
      variables: {
        codeItem: 'GED_DOCUMENT',
        idSource: document?.id as any,
        idSmyley: smyley?.id as any,
      },
    });
  };

  const handleCompleteDownload = (document: DocumentCategorie): void => {
    addActivite({
      variables: {
        input: {
          activiteTypeCode: 'DOWNLOAD',
          itemCode: 'GED_DOCUMENT',
          idItemAssocie: document.id as any,
        },
      },
    });
  };

  const handleRefresh = (): void => {
    refetchSearch();
    setSearchParameters({
      searchText: undefined,
      accesRapide: undefined,
      proprietaire: 'TOUT',
      idSousCategorie: undefined,
      filterBy: undefined,
      sortBy: undefined,
      sortDirection: undefined,
      type: undefined,
      validation: 'TOUT',
      idOrigine: undefined,
      idOrigineAssocie: undefined,
      dateDebut: undefined,
      dateFin: undefined,
    });
    history.push('/espace-documentaire');
  };

  const handleSearchVariables = (variables: IDocumentSearch) => {
    history.push('/espace-documentaire');
    setSearchParameters(variables);
  };

  const handleDeplace = (document: DocumentCategorie): void => {};

  const handleFetchMoreComments = (document: DocumentCategorie): void => {};

  const handleGoBack = () => {
    history.push('/');
  };

  if (
    categoriesLoading ||
    facturesFournisseurs.loading ||
    facturesRejeteesInterneCategories.loading ||
    facturesRejeteesPrestataireServiceCategories.loading
  )
    return <Backdrop open={true} />;

  return (
    <EspaceDocumentaire
      facturesRejeteesInterneCategories={
        facturesRejeteesInterneCategories.data?.gedCategoriesAnneeMois || ([] as any)
      }
      facturesRejeteesPrestataireServiceCategories={
        facturesRejeteesPrestataireServiceCategories.data?.gedCategoriesAnneeMois || ([] as any)
      }
      factureFournisseurs={facturesFournisseurs.data?.gedDocumentFournisseurs || []}
      onGoback={handleGoBack}
      categories={{
        loading: categoriesLoading,
        error: categoriesError as any,
        data: categoriesData?.gedMesCategories as any,
      }}
      documents={{
        loading: documentsSearchLoading,
        error: documentsSearchError as any,
        data: documents as any,
        total,
      }}
      content={{
        loading: documentLoading,
        error: documentError as any,
        data: document?.gedDocument as any,
      }}
      commentaires={{
        loading: commentsLoading,
        error: commentsError as any,
        data: comments?.comments as any,
      }}
      mode={mode}
      activeDocument={idDocument ? (document?.gedDocument as any) : undefined}
      saving={saving}
      saved={saved}
      setSaved={() => setSaved(false)}
      searchVariables={searchParameters}
      onRequestCreateDocument={handleSave}
      onRequestAddDocumentToFavorite={handleToggleDocumentToOrFromFavorite}
      onRequestDeleteDocument={handleDeleteDocument}
      onRequestDetailsDocument={handleDetailsDocument}
      onRequestRemoveDocumentFromFavorite={handleToggleDocumentToOrFromFavorite}
      onRequestReplaceDocument={handleSave}
      onRequestScroll={handleScroll}
      onRequestGetComments={() => {}}
      onRequestFetchMoreComments={handleFetchMoreComments}
      onRequestLike={handleLike}
      onRequestRefresh={handleRefresh}
      onRequestDeplace={handleDeplace}
      setSearchVariables={parameters => handleSearchVariables(parameters as any)}
      refetchComments={handleRefetchComments}
      refetchSmyleys={handleRefetchSmyleys}
      onCompleteDownload={handleCompleteDownload}
      refetchDocument={refetchDocument}
      onRequestDocument={handleOnRequestDocument}
      onRequestFetchAll={handleRefetchAll}
    />
  );
};

export default withRouter(GestionEspaceDocumentaire);
