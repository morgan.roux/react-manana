import {
  Box,
  Card,
  CardContent,
  Chip,
  IconButton,
  Menu,
  SvgIcon,
  SvgIconProps,
  Typography,
} from '@material-ui/core';
import { ChatBubble, Event, GetApp, MoreHoriz, ThumbUp, Visibility } from '@material-ui/icons';
import classNames from 'classnames';
import moment from 'moment';
import React, { FC, CSSProperties } from 'react';
import { getUser } from '../../../../../../services/LocalStorage';
import {
  useValueParameterAsBoolean,
  useValueParameterAsString,
} from '../../../../../../utils/getValueParameter';
import { isMobile } from '../../../../../../utils/Helpers';
import { DocumentCategorie, IDocumentSearch, User } from '../../EspaceDocumentaire';
import { DocumentCardMenu } from '../DocumentCardMenu';
import useStyles from './style';

const ContentCopy = (props: SvgIconProps) => (
  <SvgIcon {...props} viewBox="0 0 24 24">
    <path d="M16 1H4c-1.1 0-2 .9-2 2v14h2V3h12V1zm3 4H8c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h11c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zm0 16H8V7h11v14z" />
  </SvgIcon>
);

interface DocumentCardProps {
  info: DocumentCategorie;
  activeSousCategorie: string;
  document: any;
  style?: CSSProperties;
  saving: boolean;
  saved: boolean;
  setSaved: (newValue: boolean) => void;
  onRequestAddToFavorite: (document: DocumentCategorie) => void;
  onRequestRemoveFromFavorite: (document: DocumentCategorie) => void;
  onRequestDelete: (document: DocumentCategorie) => void;
  onRequestReplaceDocument: (document: DocumentCategorie) => void;
  onRequestDeplace: (document: DocumentCategorie) => void;
  onClick: () => void;
  active?: boolean;
  searchVariables?: IDocumentSearch;
}

const STATUT_LABELS = {
  EN_ATTENTE_APPROBATION: 'à valider',
  REFUSE: 'rejetée',
  APPROUVE: 'validée',
};

const getLibelleFacture = (
  info: DocumentCategorie,
  isPrestataire: boolean,
  withPrestataireServiceValidateur: boolean,
) => {
  const changementStatutValidationInterne = info.changementStatuts?.find(
    ({ createdBy }) => !createdBy.prestataireService,
  );
  const changementStatutValidationPrestataireValidateur = info.changementStatuts?.find(
    ({ createdBy }) => createdBy.prestataireService,
  );

  if (!withPrestataireServiceValidateur) {
    if (!info.dernierChangementStatut?.status) {
      return 'Facture à valider';
    }
    return `Facture ${STATUT_LABELS[info.dernierChangementStatut?.status]}`
  }

  if (isPrestataire) {
    if (!changementStatutValidationPrestataireValidateur?.status) {
      return 'Facture à valider';
    }
    return `Facture ${STATUT_LABELS[changementStatutValidationPrestataireValidateur.status]}`;
  }

  if (!changementStatutValidationInterne?.status) {
    return 'Facture à valider';
  }

  if (changementStatutValidationInterne.status === 'REFUSE') {
    return `Facture rejetée pharmacie`;
  }

  if (!changementStatutValidationPrestataireValidateur?.status) {
    return `Facture comptabilisée`;
  }

  if (changementStatutValidationPrestataireValidateur.status === 'REFUSE') {
    return `Facture rejetée comptable`;
  }

  return 'Facture validée';
};

const DocumentCard: FC<DocumentCardProps> = ({
  info,
  activeSousCategorie,
  document,
  saving,
  saved,
  setSaved,
  onRequestDeplace,
  onRequestAddToFavorite,
  onRequestRemoveFromFavorite,
  onRequestReplaceDocument,
  onRequestDelete,
  onClick,
  active,
  searchVariables,
}) => {
  const classes = useStyles({});
  const [openOptions, setOpenOptions] = React.useState<boolean>(false);
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);

  const handleCloseOptions = () => {
    setOpenOptions(false);
    setAnchorEl(null);
  };

  const handleClickOptions = (event: React.MouseEvent) => {
    event.preventDefault();
    event.stopPropagation();
    setAnchorEl(event.currentTarget as HTMLElement);
    setOpenOptions(prev => !prev);
  };

  const user = getUser();
  const isPrestataire = !!user?.userPartenaire?.id;
  const enableValidationPrestataire = useValueParameterAsBoolean('0850');
  const idPrestataireValidateur = useValueParameterAsString('0851');
  const withPrestataireServiceValidateur = enableValidationPrestataire && idPrestataireValidateur;

  const open = Boolean(openOptions);

  return (
    <>
      <Card
        className={classNames({ [classes.active]: active })}
        classes={{ root: classes.card }}
        elevation={active ? 0 : 1}
      >
        <Box onClick={onClick}>
          <Box className={classNames(classes.libelleContainer, classes.padding)}>
            <Typography color="secondary" variant="subtitle2" gutterBottom>
              {'FACTURE' === info?.type && searchVariables?.type
                ? getLibelleFacture(info, isPrestataire, !!withPrestataireServiceValidateur)
                : info?.sousCategorie?.libelle}
            </Typography>
            <span onClick={handleClickOptions}>
              <MoreHoriz />
            </span>
          </Box>
          <Box className={classNames(classes.padding)}>
            <span
              className={classNames(
                { [classes.bold]: !isMobile() },
                { [classes.light]: isMobile() },
              )}
            >
              <div dangerouslySetInnerHTML={{ __html: info.description || '' }} />
            </span>
          </Box>
          {isMobile() && <Box className={classes.separateur} />}
          {'FACTURE' === info?.type ? (
            <Box
              className={classNames(classes.details, classes.padding)}
              marginTop={isMobile() ? 0 : 2}
            >
              <Box marginBottom={2}>
                <Typography component="span" variant="caption" color="textSecondary">
                  Facture n°&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  {info?.numeroFacture}
                </Typography>
              </Box>
              <Box marginBottom={2}>
                <Typography component="span" variant="caption" color="textSecondary">
                  Total TTC&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  {info?.factureTotalTtc}
                </Typography>
              </Box>
              <Box>
                <Typography component="span" variant="caption" color="textSecondary">
                  TVA&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  {info?.factureTva}
                </Typography>
              </Box>
              <Box>
                <Typography component="span" variant="caption" color="textSecondary">
                  Total HT&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  {info?.factureTotalHt}
                </Typography>
              </Box>
            </Box>
          ) : (
            <Box
              className={classNames(classes.details, classes.padding)}
              marginTop={isMobile() ? 0 : 2}
            >
              <Box marginBottom={2}>
                <Typography component="span" variant="caption" color="textSecondary">
                  Rédacteur&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  {info?.redacteur?.fullName}
                </Typography>
              </Box>
              <Box marginBottom={2}>
                <Typography component="span" variant="caption" color="textSecondary">
                  Nomenclature&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  {info.nomenclature}
                </Typography>
              </Box>
              <Box>
                <Typography component="span" variant="caption" color="textSecondary">
                  Vérificateur&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  {info?.verificateur?.fullName}
                </Typography>
              </Box>
              <Box>
                <Typography component="span" variant="caption" color="textSecondary">
                  N° version&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  {info.numeroVersion}
                </Typography>
              </Box>
            </Box>
          )}
          {isMobile() && <Box className={classes.separateur} />}
          <Box
            marginTop={isMobile() ? -1 : 1}
            className={classNames(classes.status, classes.padding)}
          >
            <Chip
              icon={<Visibility />}
              label={
                <Typography variant="subtitle2" color="textSecondary" component="span">
                  {info?.nombreConsultations}
                </Typography>
              }
            />
            <Chip
              icon={<ThumbUp />}
              label={
                <Typography variant="subtitle2" color="textSecondary" component="span">
                  {info?.nombreReactions}
                </Typography>
              }
            />
            <Chip
              icon={<ChatBubble />}
              label={
                <Typography variant="subtitle2" color="textSecondary" component="span">
                  {info?.nombreCommentaires}
                </Typography>
              }
            />
            <Chip
              icon={<GetApp />}
              label={
                <Typography variant="subtitle2" color="textSecondary" component="span">
                  {info?.nombreTelechargements}
                </Typography>
              }
            />
          </Box>
        </Box>
      </Card>

      <Menu
        open={open}
        anchorEl={anchorEl}
        onClose={handleCloseOptions}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <DocumentCardMenu
          setOpenOptions={setOpenOptions}
          saving={saving}
          saved={saved}
          setSaved={setSaved}
          activeSousCategorie={activeSousCategorie}
          document={document}
          onRequestDelete={onRequestDelete}
          onRequestAddToFavorite={onRequestAddToFavorite}
          onRequestRemoveFromFavorite={onRequestRemoveFromFavorite}
          onRequestReplaceDocument={onRequestReplaceDocument}
          onRequestDeplace={onRequestDeplace}
          searchVariables={searchVariables}
        />
      </Menu>
    </>
  );
};

export default DocumentCard;
