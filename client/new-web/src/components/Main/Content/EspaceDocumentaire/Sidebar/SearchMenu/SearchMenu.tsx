import { Box } from '@material-ui/core';
import React, { Dispatch, FC, SetStateAction, useCallback, useState } from 'react';
import { IDocumentSearch } from '../../EspaceDocumentaire';
import { FilterRadio } from '../../GEDFilter/GEDFilter';

interface SearchMenuProps {
  searchVariables: any;
  setSearchVariables: Dispatch<SetStateAction<IDocumentSearch>>;
}

const SearchMenu: FC<SearchMenuProps> = ({ setSearchVariables, searchVariables }) => {

  /*
    const handleChangeProprietaire = useCallback(
      (value: any) => {
        setSearchVariables(prev => ({
          ...prev,
          proprietaire: value.code,
        }));
      },
      [setSearchVariables],
    );
  
    const handleChangeTime = useCallback(
      (value: any) => {
        setSearchVariables(prev => ({
          ...prev,
          filterBy: value.code,
        }));
      },
      [setSearchVariables],
    );
    */

  const handleChangeStatut = useCallback(
    (value: any) => {
      setSearchVariables(prev => ({
        ...prev,
        idSousCategorie: undefined,
        accesRapide: value === 'TOUT' ? undefined : value,
      }));
    },
    [setSearchVariables],
  );


  return (
    <Box padding={2}>
      <FilterRadio
        // valueFilterBy={valueProprietaire}
        // valueFilterTime={valueTime}
        valueFilterSatut={searchVariables.accesRapide}
        // onChangeFilterBy={handleChangeProprietaire}
        // onChangeFilterTime={handleChangeTime}
        onChangeFilterStatut={handleChangeStatut}
      />
    </Box>
  );
};

export default SearchMenu;
