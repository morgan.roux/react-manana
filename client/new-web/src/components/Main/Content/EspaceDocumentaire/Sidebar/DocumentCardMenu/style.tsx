import { createStyles, makeStyles, Theme } from "@material-ui/core";

const useStyles = makeStyles( ( theme : Theme ) => 
    createStyles({
        menuList : {
            color: theme.palette.grey[700],
            '& svg': {
              marginRight: theme.spacing(1),
              color: 'inherit',
            },
        }
    })
);

export default useStyles;