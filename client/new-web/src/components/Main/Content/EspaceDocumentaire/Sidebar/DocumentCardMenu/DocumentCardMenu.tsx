import { ConfirmDeleteDialog, CustomModal } from '@app/ui-kit';
import { MenuItem, MenuList, Typography } from '@material-ui/core';
import {
  Check,
  Close,
  DeleteForever,
  Details,
  Favorite,
  FavoriteBorder,
  Flip,
} from '@material-ui/icons';
import React, { FC, useEffect, useState } from 'react';
import CreateDocument from '../../Content/CreateDocument';
import InfoDocument from '../../Content/InfoDocument';
import ModalDocument from '../../Content/ModalDocument';
import { DocumentCategorie, IDocumentSearch } from '../../EspaceDocumentaire';
import { getUser } from '../../../../../../services/LocalStorage';
import useStyles from './style';
import { isMobile } from '../../../../../../utils/Helpers';
interface DocumentCardMenuProps {
  saving: boolean;
  saved: boolean;
  setSaved: (newValue: boolean) => void;
  activeSousCategorie?: string;
  document?: DocumentCategorie;
  isOpenDetail?: boolean;
  setOpenOptions: (openOptions: boolean) => void;
  onRequestReplaceDocument?: (document: DocumentCategorie) => void;
  onRequestAddToFavorite: (document: DocumentCategorie) => void;
  onRequestRemoveFromFavorite: (document: DocumentCategorie) => void;
  onRequestDelete: (document: DocumentCategorie) => void;
  onRequestDeplace: (document: DocumentCategorie) => void;
  onRequestValider?: (id: string) => void;
  onRequestRefuser?: (id: string) => void;
  searchVariables?: IDocumentSearch;
}

const DocumentCardMenu: FC<DocumentCardMenuProps> = ({
  activeSousCategorie,
  document,
  saving,
  saved,
  setSaved,
  setOpenOptions,
  onRequestReplaceDocument,
  onRequestAddToFavorite,
  onRequestDelete,
  onRequestRemoveFromFavorite,
  isOpenDetail,
  onRequestRefuser,
  onRequestValider,
  searchVariables,
}) => {
  const classes = useStyles({});

  const user = getUser();

  const [openReplaceDocument, setOpenReplaceDocument] = useState<boolean>(false);
  const [openDetails, setOpenDetails] = useState<boolean>(false);
  const [openDeleteDocumentConfirm, setOpenDeleteDocumentConfirm] = useState<boolean>(false);

  useEffect(() => {
    if (saved) {
      setOpenReplaceDocument(false);
      setSaved(false);
    }
  }, [saved]);

  const handleReplace = (): void => {
    setOpenReplaceDocument(true);
  };

  const handleDetails = (): void => {
    setOpenDetails(true);
  };

  const handleDelete = (): void => {
    onRequestDelete(document as any);
    setOpenDeleteDocumentConfirm(false);
  };

  const handleAddToFavorite = (): void => {
    onRequestAddToFavorite(document as any);
    closeActionsMenu();
  };

  const handleRemoveFromFavorite = (): void => {
    onRequestRemoveFromFavorite(document as any);
    closeActionsMenu();
  };

  const closeActionsMenu = (): void => setOpenOptions(false);

  const handleOnRequestValider = () => {
    if (onRequestValider && document && document.id) onRequestValider(document.id);
  };

  const handleOnRequestRefuser = () => {
    if (onRequestRefuser && document && document.id) onRequestRefuser(document.id);
  };

  return (
    <>
      <MenuList className={classes.menuList}>
        <MenuItem button disabled={document?.favoris} onClick={handleAddToFavorite}>
          <Favorite />
          <Typography variant="body2">Ajouter aux Favoris</Typography>
        </MenuItem>
        <MenuItem button disabled={!document?.favoris} onClick={handleRemoveFromFavorite}>
          <FavoriteBorder />
          <Typography variant="body2">Retirer des Favoris</Typography>
        </MenuItem>
        {/*<MenuItem button onClick={handleReplace}>
          <Flip />
          <Typography variant="body2">Modifier</Typography>
        </MenuItem>*/}
        {user && user.id === document?.createdBy?.id && (
          <MenuItem
            button
            onClick={() => {
              setOpenDeleteDocumentConfirm(true);
            }}
          >
            <DeleteForever />
            <Typography variant="body2" color="textSecondary">
              Supprimer
            </Typography>
          </MenuItem>
        )}
        {/*<MenuItem button>
          <Details />
          <Typography variant="body2" color="textSecondary" onClick={handleDetails}>
            Détails
          </Typography>
        </MenuItem>*/}
        {isOpenDetail && isMobile() && (
          <>
            <MenuItem button>
              <Check />
              <Typography variant="body2" onClick={handleOnRequestValider}>
                Valider
              </Typography>
            </MenuItem>
            <MenuItem button>
              <Close />
              <Typography variant="body2" onClick={handleOnRequestRefuser}>
                Refuser
              </Typography>
            </MenuItem>
          </>
        )}
      </MenuList>

      <CreateDocument
        open={openReplaceDocument}
        setOpen={setOpenReplaceDocument}
        title="Modification de document"
        saving={saving}
        activeSousCategorie={activeSousCategorie}
        document={document}
        mode="remplacement"
        onRequestCreateDocument={onRequestReplaceDocument as any}
        searchVariables={searchVariables}
      />

      <ModalDocument
        open={openDetails}
        onClose={() => setOpenDetails(false)}
        handleClose={() => setOpenDetails(false)}
        label="Détails"
        scroll="paper"
      >
        <InfoDocument
          document={{
            loading: false,
            error: false as any,
            data: document as any,
          }}
        />
      </ModalDocument>

      <ConfirmDeleteDialog
        open={openDeleteDocumentConfirm}
        setOpen={setOpenDeleteDocumentConfirm}
        title="Suppression de document"
        onClickConfirm={handleDelete}
      />
    </>
  );
};

export default DocumentCardMenu;
