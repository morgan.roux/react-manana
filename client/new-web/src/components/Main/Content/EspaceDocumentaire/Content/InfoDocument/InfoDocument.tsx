import { ErrorPage, Loader } from '@app/ui-kit';
import { Box, DialogContent, Typography } from '@material-ui/core';
import moment from 'moment';
import React, { FC } from 'react';
import { DocumentCategorie } from '../../EspaceDocumentaire';
import styles from './style';

interface InfoDocumentProps {
  document: {
    loading: boolean;
    error: Error;
    data: DocumentCategorie;
  };
}

const InfoDocument: FC<InfoDocumentProps> = ({ document }) => {
  const classes = styles();

  return (
    <DialogContent className={classes.detail}>
      {document.loading ? (
        <Loader />
      ) : !document.error && document.data ? (
        <>
          <Box>
            <Typography variant="subtitle2">Emplacement</Typography>
            <Typography variant="caption">{`${document.data.categorie?.libelle} > ${document.data.sousCategorie?.libelle}`}</Typography>
          </Box>
          <Box>
            <Typography variant="subtitle2">Nomenclature</Typography>
            <Typography variant="caption">{document.data.nomenclature}</Typography>
          </Box>
          <Box>
            <Typography variant="subtitle2">Date de parution</Typography>
            <Typography variant="caption">
              {document.data.dateHeureParution
                ? moment(new Date(document.data.dateHeureParution as any)).format('L')
                : '-'}
            </Typography>
          </Box>
          <Box>
            <Typography variant="subtitle2">Date de validité</Typography>
            <Typography variant="caption">{`Début : ${
              document.data.dateHeureDebutValidite
                ? moment(new Date(document.data.dateHeureDebutValidite as any)).format('L')
                : '-'
            } `}</Typography>
            <Typography variant="caption">{`Fin : ${
              document.data.dateHeureFinValidite
                ? moment(new Date(document.data.dateHeureFinValidite as any)).format('L')
                : '-'
            }`}</Typography>
          </Box>
          <Box>
            <Typography variant="subtitle2">Numéro de version</Typography>
            <Typography variant="caption">{document.data.numeroVersion}</Typography>
          </Box>
          <Box>
            <Typography variant="subtitle2">Rédacteur</Typography>
            <Typography variant="caption">{document.data.redacteur?.fullName}</Typography>
          </Box>
          <Box>
            <Typography variant="subtitle2">Description</Typography>
            <Typography variant="caption">
              <div dangerouslySetInnerHTML={{__html: document.data?.description || ''}} />
            </Typography>
          </Box>
          <Box>
            <Typography variant="subtitle2">Mot clés</Typography>
            <Typography variant="caption">
              {document?.data?.motCle1 || document?.data?.motCle2 || document?.data?.motCle3
                ? ` 
                        ${document?.data?.motCle1 && document?.data?.motCle1 + ' , '}
                        ${document?.data?.motCle2 && document?.data?.motCle2 + ' , '}
                        ${document?.data?.motCle3 && document?.data?.motCle3}

                      `
                : 'Aucun mot clé renseigné'}
            </Typography>
          </Box>
          <Box>
            <Typography variant="subtitle2">Valideur</Typography>
            <Typography variant="caption">{document?.data?.verificateur?.fullName}</Typography>
          </Box>
          <Box>
            <Typography variant="subtitle2">Nombre de consultation</Typography>
            <Typography variant="caption">{document?.data?.nombreConsultations}</Typography>
          </Box>
        </>
      ) : (
        <ErrorPage />
      )}
    </DialogContent>
  );
};

export default InfoDocument;
