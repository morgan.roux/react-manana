import { DocumentCategorie, IDocumentSearch } from '../../../EspaceDocumentaire';

export interface CreateDocumentProps {
  activeSousCategorie?: any | undefined;
  mode: 'creation' | 'remplacement';
  document?: DocumentCategorie;
  saving: boolean;
  onRequestCreateDocument: (document: DocumentCategorie) => void;
  open: boolean;
  setOpen: (value: boolean) => void;
  title: string;
  defaultType?: string;
  idDefaultOrigine?: string;
  defaultCorrespondant?: any;
  searchVariables?: IDocumentSearch;
}

export interface TotalProps {
  hT?: boolean;
  ttc?: boolean;
  tva?: boolean;
}

export interface HandleChangeInterface {
    name: string;
    value: any;
}

export interface ChangeDocumentProps {
    description: string;
    nomenclature: string;
    numeroVersion: string;
    dateParution: any | null;
    dateDebutValidite: any | null;
    dateFinValidite: any | null;
    idUserRedacteur: string;
    idUserVerificateur: string;
    userRedacteur: any[];
    userVerificateur: any[];
    idSousCategorie: any; 
    dateFacture: any;
    hT: any;
    tva: any;
    ttc: any;
    type: string;
    numeroFacture: string;
    dateReglement: any;
    modeReglement?: string;
    associer?: string;
    numeroCommande?: any;
    isGenererCommande: boolean;
    changeCondition?: any;
    conditionIds: string[];
    changeFacture?: any;
    factureIds: string[];
    remunerationIds: string[];
    reprisePerimes: string;
  }