import { useLazyQuery, useQuery } from '@apollo/client';
import { CustomButton, Loader, CustomEditorText,CustomModal } from '@app/ui-kit';
import { Box, TextField, FormControlLabel, Switch, Checkbox } from '@material-ui/core';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import { GET_MES_CATEGORIES } from '../../../../../../federation/ged/categorie/query';
import { GET_MES_CATEGORIES as GET_MES_CATEGORIES_TYPE } from '../../../../../../federation/ged/categorie/types/GET_MES_CATEGORIES';
import { CustomDatePicker } from '../../../../../Common/CustomDateTimePicker';
import CustomSelect from '../../../../../Common/CustomSelect';
import Dropzone from '../../../../../Common/Dropzone';
import UserInput from '../../../../../Common/UserInput';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { DocumentCategorie, IDocumentSearch, Role, User } from '../../EspaceDocumentaire';
import OrigineInput from '../../../../../Common/OrigineInput/OrigineInput';
import styles from './style';
import { isMobile } from '../../../../../../utils/Helpers';
import ConditionCommerciale from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/ConditionCommerciale';
import { useConditionCommerciale } from '../../../PartenairesLaboratoires/utils/useConditionCommerciale';
import { ConditionCommandeItem } from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/ConditionCommerciale/ConditionCommerciale';
import {
  GET_PRESTATION_TYPE,
  GET_PRT_REGLEMENT_MODE,
  GET_PRT_REMUNERATIONS_REGLEMENTS,
} from '../../../../../../federation/partenaire-service/remuneration/query';
import {
  GET_PRT_REGLEMENT_MODE as GET_PRT_REGLEMENT_MODE_TYPE,
  GET_PRT_REGLEMENT_MODEVariables,
} from '../../../../../../federation/partenaire-service/remuneration/types/GET_PRT_REGLEMENT_MODE';
import {
  DocumentFacture,
  HistoriqueFacturePage,
} from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/HistoriqueFacture/HistoriqueFacture';
import { useHistoriqueFacture } from '../../../PartenairesLaboratoires/utils/useHistoriqueFacture';
import { PRTRemunerationReglement } from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/Remuneration/TableauRemuneration/TableChart/interface';
import { HistoriqueRemuneration } from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/Remuneration/Remuneration';
import {
  GET_PRT_REMUNERATIONS_REGLEMENT as GET_PRT_REMUNERATIONS_REGLEMENT_TYPE,
  GET_PRT_REMUNERATIONS_REGLEMENTVariables,
} from '../../../../../../federation/partenaire-service/remuneration/types/GET_PRT_REMUNERATIONS_REGLEMENT';
import { getPharmacie } from '../../../../../../services/LocalStorage';
import moment from 'moment';
import {
  GET_PRESTATION_TYPE as GET_PRESTATION_TYPE_TYPE,
  GET_PRESTATION_TYPEVariables,
} from '../../../../../../federation/partenaire-service/remuneration/types/GET_PRESTATION_TYPE';
import useOrigines from '../../../../../hooks/basis/useOrigines';
import {
  GET_DOCUMENTS,
  GET_TOTAUX_FACTURES,
} from '../../../../../../federation/ged/document/query';
import {
  GET_DOCUMENTS as GET_DOCUMENTS_TYPE,
  GET_DOCUMENTSVariables,
} from '../../../../../../federation/ged/document/types/GET_DOCUMENTS';
import {
  TOTAUX_FACTURES,
  TOTAUX_FACTURESVariables,
} from '../../../../../../federation/ged/document/types/TOTAUX_FACTURES';
import { PRTConditionCommercialeFilter } from '../../../../../../types/federation-global-types';
import {
  GET_CONDITIONS,
  GET_ROW_CONDITIONS,
} from '../../../../../../federation/partenaire-service/laboratoire/condition/query';
import {
  GET_CONDITIONS as GET_CONDITIONS_TYPE,
  GET_CONDITIONSVariables,
} from '../../../../../../federation/partenaire-service/laboratoire/condition/types/GET_CONDITIONS';
import {
  GET_ROW_CONDITIONS as GET_ROW_CONDITION_TYPES,
  GET_ROW_CONDITIONSVariables,
} from '../../../../../../federation/partenaire-service/laboratoire/condition/types/GET_ROW_CONDITIONS';
import { GET_TYPES_CONDITION } from '../../../../../../federation/partenaire-service/laboratoire/condition/type/query';
import {
  GET_TYPES_CONDITION as GET_TYPES_CONDITION_TYPE,
  GET_TYPES_CONDITIONVariables,
} from '../../../../../../federation/partenaire-service/laboratoire/condition/type/types/GET_TYPES_CONDITION';
import { GET_STATUS_CONDITION } from '../../../../../../federation/partenaire-service/laboratoire/condition/status/query';
import {
  GET_STATUS_CONDITION as GET_STATUS_CONDITION_TYPE,
  GET_STATUS_CONDITIONVariables,
} from '../../../../../../federation/partenaire-service/laboratoire/condition/status/types/GET_STATUS_CONDITION';
import { GET_LABORATOIRE } from '../../../../../../federation/partenaire-service/laboratoire/query';
import {
  GET_LABORATOIRE as GET_LABORATOIRE_TYPE,
  GET_LABORATOIREVariables,
} from '../../../../../../federation/partenaire-service/laboratoire/types/GET_LABORATOIRE';
import { Alert } from '@material-ui/lab';

interface CreateDocumentProps {
  activeSousCategorie?: any | undefined;
  mode: 'creation' | 'remplacement';
  document?: DocumentCategorie;
  saving: boolean;
  onRequestCreateDocument: (document: DocumentCategorie) => void;
  open: boolean;
  setOpen: (value: boolean) => void;
  title: string;
  defaultType?: string;
  idDefaultOrigine?: string;
  defaultCorrespondant?: any;
  searchVariables?: IDocumentSearch;
}

interface TotalProps {
  hT?: boolean;
  ttc?: boolean;
  tva?: boolean;
}

const initialTotalProps: TotalProps = {
  hT: false,
  ttc: false,
  tva: false,
};

const typeDocuments = [
  {
    id: 'AUTRES',
    code: 'AUTRES',
    libelle: 'Autres',
  },
  {
    id: 'FACTURE',
    code: 'FACTURE',
    libelle: 'Facture',
  },
];

const associations = [
  {
    id: 'FACTURE',
    code: 'FACTURE',
    libelle: 'Facture',
  },
  {
    id: 'REPRISE_PERIMES',
    code: 'REPRISE_PERIMES',
    libelle: 'Reprise des périmés',
  },
];

const associationsPartenaires = [
  {
    id: 'FACTURE',
    code: 'FACTURE',
    libelle: 'Facture',
  },
  {
    id: 'BRI',
    code: 'BRI',
    libelle: 'BRI',
  },
  {
    id: 'CONDITION_COMMERCIALE',
    code: 'CONDITION_COMMERCIALE',
    libelle: 'Condition Commerciale',
  },
  {
    id: 'REPRISE_PERIMES',
    code: 'REPRISE_PERIMES',
    libelle: 'Reprise des périmés',
  },
];

const CreateDocument: FC<CreateDocumentProps> = ({
  saving,
  activeSousCategorie,
  mode,
  document,
  onRequestCreateDocument,
  open,
  setOpen,
  title,
  defaultType,
  idDefaultOrigine,
  defaultCorrespondant,
  searchVariables,
}) => {
  const classes = styles();
  const [motsCles, setMotsCles] = React.useState<string[] | string>('');

  const pharmacie = getPharmacie();

  console.log('searchVariables', searchVariables);

  const { loading: categoriesLoading, error: categoriesError, data: categoriesData } = useQuery<
    GET_MES_CATEGORIES_TYPE,
    any
  >(GET_MES_CATEGORIES, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const loadingReglementMode = useQuery<
    GET_PRT_REGLEMENT_MODE_TYPE,
    GET_PRT_REGLEMENT_MODEVariables
  >(GET_PRT_REGLEMENT_MODE, { client: FEDERATION_CLIENT });

  const [loadRemunerationReglement, loadingRemunerationReglement] = useLazyQuery<
    GET_PRT_REMUNERATIONS_REGLEMENT_TYPE,
    GET_PRT_REMUNERATIONS_REGLEMENTVariables
  >(GET_PRT_REMUNERATIONS_REGLEMENTS, {
    client: FEDERATION_CLIENT,
  });

  const loadingPrestationType = useQuery<GET_PRESTATION_TYPE_TYPE, GET_PRESTATION_TYPEVariables>(
    GET_PRESTATION_TYPE,
    { client: FEDERATION_CLIENT },
  );

  const [getDocuments, gettingDocuments] = useLazyQuery<GET_DOCUMENTS_TYPE, GET_DOCUMENTSVariables>(
    GET_DOCUMENTS,
    {
      client: FEDERATION_CLIENT,
    },
  );

  const [loadTotauxFacture, loadingTotauxFacture] = useLazyQuery<
    TOTAUX_FACTURES,
    TOTAUX_FACTURESVariables
  >(GET_TOTAUX_FACTURES, {
    client: FEDERATION_CLIENT,
  });

  const [loadConditions, loadingConditions] = useLazyQuery<
    GET_CONDITIONS_TYPE,
    GET_CONDITIONSVariables
  >(GET_CONDITIONS, {
    client: FEDERATION_CLIENT,
  });

  const [loadRowConditions, loadingRowConditions] = useLazyQuery<
    GET_ROW_CONDITION_TYPES,
    GET_ROW_CONDITIONSVariables
  >(GET_ROW_CONDITIONS, {
    client: FEDERATION_CLIENT,
  });

  const loadType = useQuery<GET_TYPES_CONDITION_TYPE, GET_TYPES_CONDITIONVariables>(
    GET_TYPES_CONDITION,
    {
      client: FEDERATION_CLIENT,
    },
  );

  const loadStatus = useQuery<GET_STATUS_CONDITION_TYPE, GET_STATUS_CONDITIONVariables>(
    GET_STATUS_CONDITION,
    {
      client: FEDERATION_CLIENT,
    },
  );

  const [loadLaboratoire] = useLazyQuery<GET_LABORATOIRE_TYPE, GET_LABORATOIREVariables>(
    GET_LABORATOIRE,
    {
      onCompleted: result => {
        setCorrespondant(result.laboratoire);
      },
      client: FEDERATION_CLIENT,
    },
  );

  const handleChangeMotCle = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const { value } = event.target as HTMLInputElement;
    const temp: string[] = [];
    const input: string[] = value.split(',');
    input.forEach((motCle: string) => temp.push(motCle));

    setMotsCles(temp);
  };

  const handleDeleteMotsCle = (index: number) => (_: React.MouseEvent) => {
    if (typeof motsCles === 'object') {
      motsCles.splice(index, 1);
      setMotsCles([...motsCles]);
    } else setMotsCles(['']);
  };
  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);
  const [description, setDescription] = useState<string>(
    mode === 'creation' ? '' : document ? (document.description as any) : 'XXX',
  );
  const [nomenclature, setNomenclature] = useState<string>('');
  const [numeroVersion, setNumeroVersion] = useState<string>('');
  const [dateParution, setDateParution] = useState<any | null>(null);
  const [dateDebutValidite, setDateDebutValidite] = useState<any | null>(null);
  const [dateFinValidite, setDateFinValidite] = useState<any | null>(null);
  const [idUserRedacteur, setIdUserRedacteur] = useState<string>('');
  const [idUserVerificateur, setIdUserVerificateur] = useState<string>('');
  const [userRedacteur, setUserRedacteur] = useState<any[]>([]);
  const [userVerificateur, setUserVerificateur] = useState<any[]>([]);
  const [openUserVerificateurInput, setOpenUserVerificateurInput] = useState<boolean>(false);
  const [openUserRedacteurInput, setOpenUserRedacteurInput] = useState<boolean>(false);
  const [idSousCategorie, setIdSousCategorie] = useState<any>(activeSousCategorie?.id);

  const [origine, setOrigine] = useState<any>();
  const [correspondant, setCorrespondant] = useState<any>();
  const [dateFacture, setDateFacture] = useState<any>(new Date());
  const [hT, setHT] = useState<any>();
  const [tva, setTVA] = useState<any>();
  const [ttc, setTTC] = useState<any>();
  const [type, setType] = useState<string>(
    searchVariables?.type || defaultType || typeDocuments[0].id,
  );
  const [showAdditionnalInfosFields, setShowAdditionnalInfosFields] = useState<boolean>(false);

  const [numeroFacture, setNumeroFacture] = useState<string>('');
  const [dateReglement, setDateReglement] = useState<any>(
    moment()
      .add(1, 'days')
      .toDate(),
  );
  const [modeReglement, setModeReglement] = useState<string>();
  const [associer, setAssocier] = useState<string>();
  const [numeroCommande, setNumeroCommande] = useState<any>();
  const [isGenererCommande, setIsGenererCommande] = useState<boolean>(false);
  const [openAssocieDialog, setOpenAssocieDialog] = useState<boolean>(false);
  const [changeCondition, setChangeCondition] = useState<any>();
  const [conditionIds, setConditionIds] = useState<string[]>([]);
  const [changeFacture, setChangeFacture] = useState<any>();
  const [factureIds, setFactureIds] = useState<string[]>([]);
  const [remunerationIds, setRemunerationIds] = useState<string[]>([]);
  const [reprisePerimes, setReprisePerimes] = useState<string>('');
  const [isModifie, setIsModifie] = useState<TotalProps>(initialTotalProps);

  const origines = useOrigines();
  const idOrigineLaboratoire = (origines.data?.origines.nodes || []).find(
    ({ code }) => code === 'LABORATOIRE',
  )?.id;

  useEffect(() => {
    if (document && mode === 'remplacement') {
      const fic: any = {
        name: document?.fichier?.nomOriginal as any,
        text: document?.fichier?.nomOriginal as any,
      };
      const keywords: string[] = [];
      document?.motCle1 && keywords.push(document?.motCle1 as any);
      document?.motCle2 && keywords.push(document?.motCle2 as any);
      document?.motCle3 && keywords.push(document?.motCle3 as any);

      if (
        !defaultCorrespondant &&
        document.idOrigineAssocie &&
        document.idOrigine === idOrigineLaboratoire
      ) {
        loadLaboratoire({
          variables: {
            id: document.idOrigineAssocie,
            idPharmacie: getPharmacie().id,
          },
        });
      }

      setDescription(document.description as any);
      setNomenclature(document.nomenclature as any);
      setIdUserRedacteur(document.idUserRedacteur as any);
      setIdUserVerificateur(document.idUserVerificateur as any);
      setNumeroVersion(document.numeroVersion as any);
      setDateParution(document.dateHeureParution as any);
      setDateDebutValidite(document.dateHeureDebutValidite as any);
      setDateFinValidite(document.dateHeureFinValidite as any);
      setSelectedFiles([fic]);
      setMotsCles(keywords);
      setIdSousCategorie(document?.sousCategorie?.id);

      const idOrigine = document.idOrigine || idDefaultOrigine || searchVariables?.idOrigine;
      setOrigine((origines.data?.origines.nodes || []).find(({ id }) => id === idOrigine));
      setCorrespondant(defaultCorrespondant || searchVariables?.idOrigineAssocie);
      setDateFacture(document.factureDate);
      setHT(document.factureTotalHt);
      setTVA(document.factureTva);
      setTTC(document.factureTotalTtc);
      setType(searchVariables?.type || document.type || 'AUTRES');
      setNumeroFacture(document.numeroFacture || '');
      setDateReglement(document.factureDateReglement);
      setModeReglement(document.idReglementMode);
      setAssocier(
        document.typeAvoirAssociations?.length ? document.typeAvoirAssociations[0].type : undefined,
      );
      setNumeroCommande(document.numeroCommande);
      setIsGenererCommande(document.isGenererCommande || false);
      setIsModifie(initialTotalProps);

      if (document.typeAvoirAssociations?.length) {
        setConditionIds(
          document.typeAvoirAssociations
            .filter(avoirAssociation => avoirAssociation.type === 'CONDITION_COMMERCIALE')
            .map(avoirAssociation => avoirAssociation.correspondant),
        );
        setFactureIds(
          document.typeAvoirAssociations
            .filter(avoirAssociation => avoirAssociation.type === 'FACTURE')
            .map(avoirAssociation => avoirAssociation.correspondant),
        );
        setRemunerationIds(
          document.typeAvoirAssociations
            .filter(avoirAssociation => avoirAssociation.type === 'BRI')
            .map(avoirAssociation => avoirAssociation.correspondant),
        );
        setReprisePerimes(
          document.typeAvoirAssociations?.length &&
            document.typeAvoirAssociations[0].type === 'REPRISE_PERIMES'
            ? document.typeAvoirAssociations[0].correspondant
            : '',
        );
      }
    } else {
      setDescription('');
      setNomenclature('');
      setNumeroVersion('');
      setMotsCles([]);
      setSelectedFiles([]);
      setIdUserVerificateur('');
      setIdUserRedacteur('');
      setDateFinValidite(null);
      setDateDebutValidite(null);
      setDateParution(null);
      setIdSousCategorie(activeSousCategorie?.id);
      setType(defaultType || searchVariables?.type || typeDocuments[0].id);
      const idOrigine =
        idDefaultOrigine ||
        searchVariables?.idOrigine ||
        ('FACTURE' === searchVariables?.type ? idOrigineLaboratoire : undefined);
      setOrigine((origines.data?.origines.nodes || []).find(({ id }) => id === idOrigine));
      setCorrespondant(defaultCorrespondant || searchVariables?.idOrigineAssocie);
      setDateFacture(new Date());
      setHT(undefined);
      setTVA(undefined);
      setTTC(undefined);
      setNumeroFacture('');
      setDateReglement(
        moment()
          .add(1, 'days')
          .toDate(),
      );
      setModeReglement(undefined);
      setAssocier(undefined);
      setNumeroCommande(undefined);
      setConditionIds([]);
      setFactureIds([]);
      setRemunerationIds([]);
      setReprisePerimes('');
      setIsGenererCommande(false);
      setIsModifie(initialTotalProps);
    }
  }, [document, mode, searchVariables]);

  useEffect(() => {
    if ('FACTURE' === type) {
      setOrigine(idOrigineLaboratoire);
    }
  }, [type]);

  console.log('document to edit', document);

  useEffect(() => {
    setCorrespondant(defaultCorrespondant);
  }, [defaultCorrespondant]);

  const briType = loadingPrestationType.data?.prestationTypes.nodes.filter(
    prestation => prestation.code === 'BRI',
  );
  console.log('briType', briType);

  useEffect(() => {
    if (briType?.length && correspondant?.id) {
      loadRemunerationReglement({
        variables: {
          filter: {
            and: [
              {
                partenaireType: {
                  eq: 'LABORATOIRE',
                },
              },
              {
                idPartenaireTypeAssocie: {
                  eq: correspondant?.id,
                },
              },
              {
                idPharmacie: {
                  eq: pharmacie.id,
                },
              },
              {
                idPrestationType: {
                  eq: briType[0].id,
                },
              },
            ],
          },
        },
      });
    }
  }, [correspondant, defaultCorrespondant]);

  useEffect(() => {
    const filterAnd = [
      {
        idOrigine: {
          eq: origine?.id,
        },
      },
      {
        idOrigineAssocie: {
          eq: correspondant?.id || '',
        },
      },
      {
        idPharmacie: {
          eq: pharmacie.id,
        },
      },
      {
        type: {
          eq: 'FACTURE',
        },
      },
      {
        factureTotalHt: {
          gte: 0,
        },
      },
      {
        factureTva: {
          gte: 0,
        },
      },
    ];

    if (changeFacture?.searchText) {
      filterAnd.push({
        numeroFacture: {
          iLike: `%${changeFacture?.searchText}%`,
        },
      } as any);
    }

    if (origine?.id && correspondant?.id) {
      getDocuments({
        variables: {
          filter: {
            and: filterAnd,
          },
          paging: {
            offset: changeFacture?.skip,
            limit: changeFacture?.take,
          },
        },
      });
      loadTotauxFacture({
        variables: {
          idPharmacie: pharmacie.id,
          idOrigine: origine.id,
          idOrigineAssocie: correspondant?.id,
          noAvoir: true,
          searchText: changeFacture?.searchText ? `%${changeFacture?.searchText}%` : undefined,
        },
      });
    }
  }, [origine, correspondant, changeFacture]);

  useEffect(() => {
    const keywords = ['idType', 'idCanal', 'idStatut'];
    if (correspondant?.id && changeCondition) {
      const _filters = keywords
        .map(keyword => {
          const tempObject = { or: [] };
          for (let filter of changeCondition.filters) {
            if (filter.keyword === keyword) {
              tempObject.or.push({
                [keyword]: {
                  eq: filter.id,
                },
              } as never);
            }
          }
          return tempObject.or.length > 0 ? tempObject : undefined;
        })
        .filter(e => e);

      const filterAnd: PRTConditionCommercialeFilter[] = [
        {
          partenaireType: {
            eq: 'LABORATOIRE',
          },
        },
        {
          idPartenaireTypeAssocie: {
            eq: correspondant?.id,
          },
        },
        {
          idPharmacie: {
            eq: pharmacie.id,
          },
        },
        {
          or: [
            {
              titre: {
                iLike: `%${changeCondition?.searchText}%`,
              },
            },
            {
              description: {
                iLike: `%${changeCondition?.searchText}%`,
              },
            },
          ],
        },
      ];

      if (_filters.length > 0) {
        _filters.map(filter => {
          filterAnd.push(filter as any);
        });
      }

      loadConditions({
        variables: {
          filter: {
            and: filterAnd,
          },
          paging: {
            offset: changeCondition?.skip,
            limit: changeCondition?.take,
          },
        },
      });
      loadRowConditions({
        variables: {
          filter: {
            and: filterAnd,
          },
        },
      });
    }
  }, [correspondant, changeCondition]);

  const searchDocuments = (change: any) => {
    setChangeFacture(change);
  };

  const onRequestSearchCondition = ({ skip, take, searchText, filters, sortTable }: any) => {
    setChangeCondition({ skip, take, searchText, filters });
  };

  console.log('origine associe', origine, gettingDocuments.data?.gedDocuments);

  const handleDescriptionChange = (value: string): void => {
    setDescription(value);
  };

  const handleNomenclatureChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setNomenclature((event.target as HTMLInputElement).value);
  };

  const handleNumeroVersionChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setNumeroVersion((event.target as HTMLInputElement).value);
  };

  const handleChangeDateFacture = (date: MaterialUiPickersDate) => {
    setDateFacture(date);
  };

  const handleChangeHT = (event: ChangeEvent<HTMLInputElement>): void => {
    setHT(event.target.value);
    setIsModifie(prev => ({
      ...prev,
      hT: true,
    }));
    if (tva && !isModifie.ttc) setTTC(parseFloat(event.target.value || '0') + parseFloat(tva));
    else if (ttc && !isModifie.tva) setTVA(parseFloat(ttc) - parseFloat(event.target.value || '0'));
  };

  const handleChangeTVA = (event: ChangeEvent<HTMLInputElement>): void => {
    setTVA(event.target.value);
    setIsModifie(prev => ({
      ...prev,
      tva: true,
    }));
    if (hT && !isModifie.ttc) setTTC(parseFloat(event.target.value || '0') + parseFloat(hT));
    else if (ttc && !isModifie.hT) setHT(parseFloat(ttc) - parseFloat(event.target.value || '0'));
  };

  const handleChangeTTC = (event: ChangeEvent<HTMLInputElement>): void => {
    setTTC(event.target.value);
    setIsModifie(prev => ({
      ...prev,
      ttc: true,
    }));
    if (hT && !isModifie.tva) setTVA(parseFloat(event.target.value || '0') - parseFloat(hT));
    else if (tva && !isModifie.hT) setHT(parseFloat(event.target.value || '0') - parseFloat(tva));
  };

  const handleChangeNumeroFacture = (event: ChangeEvent<HTMLInputElement>): void => {
    setNumeroFacture(event.target.value);
  };

  const handleChangeDateReglement = (date: MaterialUiPickersDate) => {
    setDateReglement(date);
  };

  const handleChangeAssocier = (value: string) => {
    setAssocier(value);
    if (origine) {
      setOpenAssocieDialog(true);
    }
  };

  const selectedConditionIds = (conditions: ConditionCommandeItem[]) => {
    setConditionIds((conditions || []).map(condition => condition.id || ''));
    setOpenAssocieDialog(false);
  };

  const handleSelectedFacture = (factures: DocumentFacture[]) => {
    setFactureIds((factures || []).map(facture => facture.id || ''));
    setOpenAssocieDialog(false);
  };

  const handleSelectedBRI = (remunerations: PRTRemunerationReglement[]) => {
    setRemunerationIds((remunerations || []).map(remuneration => remuneration.id || ''));
    setOpenAssocieDialog(false);
  };

  const createDocument = (): void => {
    if (selectedFiles[0]) {
      const doc: any = {
        idSousCategorie: 'FACTURE' !== type ? idSousCategorie : undefined,
        description: description,
        nomenclature: nomenclature,
        numeroVersion: numeroVersion,
        idUserVerificateur: idUserVerificateur,
        idUserRedacteur: idUserRedacteur,
        dateHeureParution: dateParution ? new Date(dateParution) : undefined,
        dateHeureDebutValidite: dateDebutValidite ? new Date(dateDebutValidite) : undefined,
        dateHeureFinValidite: dateFinValidite ? new Date(dateFinValidite) : undefined,
        motCle1: motsCles[0],
        motCle2: motsCles[1],
        motCle3: motsCles[2],
        origine: origine,
        correspondant: correspondant,
        dateFacture: 'FACTURE' === type ? dateFacture : undefined,
        hT: 'FACTURE' === type && hT ? parseFloat(hT) : undefined,
        tva: 'FACTURE' === type && tva ? parseFloat(tva) : undefined,
        ttc: 'FACTURE' === type && ttc ? parseFloat(ttc) : undefined,
        type: type ?? 'AUTRES',

        numeroFacture: 'FACTURE' === type ? numeroFacture : undefined,
        factureDateReglement: 'FACTURE' === type ? dateReglement : undefined,
        idReglementMode: 'FACTURE' === type ? modeReglement : undefined,
        numeroCommande: 'FACTURE' === type && numeroCommande ? parseInt(numeroCommande) : undefined,
        isGenererCommande: 'FACTURE' === type ? isGenererCommande : undefined,
        avoirType: 'FACTURE' === type && associer ? associer : undefined,
        avoirCorrespondants: !(
          (parseFloat(hT || '0') < 0 || parseFloat(tva || '0') < 0 || parseFloat(ttc || '0') < 0) &&
          origine &&
          correspondant
        )
          ? []
          : 'FACTURE' === type && associer === 'CONDITION_COMMERCIALE'
          ? conditionIds
          : associer === 'FACTURE'
          ? factureIds
          : associer === 'BRI'
          ? remunerationIds
          : associer === 'REPRISE_PERIMES'
          ? [reprisePerimes]
          : [],
      };
      const dataToSave =
        mode === 'creation'
          ? {
              ...doc,
              selectedFile: selectedFiles[0],
              launchUpload: !!(selectedFiles[0] && selectedFiles[0].size),
            }
          : {
              ...doc,
              id: document?.id,
              selectedFile: selectedFiles[0],
              previousFichier: document?.fichier,
              launchUpload: !!(selectedFiles.length > 0 && selectedFiles[0].size),
            };
      onRequestCreateDocument(dataToSave);
    }
  };

  const isFormValuesValid = () => {
    return !type || type === 'AUTRES'
      ? selectedFiles.length > 0 && description && idSousCategorie
      : type === 'FACTURE'
      ? selectedFiles.length > 0 &&
        description &&
        // origine &&
        correspondant &&
        dateFacture &&
        // dateReglement &&
        // dateReglement > dateFacture &&
        // numeroFacture &&
        modeReglement
      : false;
  };

  const sousCategories =
    categoriesData?.gedMesCategories.flatMap(categorie => categorie.mesSousCategories) || [];
  const idUserParticipants =
    (idSousCategorie && sousCategories?.find(({ id }) => id === idSousCategorie))?.participants.map(
      ({ id }) => id,
    ) || [];

  return (
    <>
      <CustomModal
        open={open}
        setOpen={setOpen}
        title={title}
        disableBackdropClick={true}
        closeIcon={true}
        withBtnsActions={true}
        headerWithBgColor={true}
        fullWidth={true}
        disabledButton={!isFormValuesValid() || saving}
        onClickConfirm={createDocument}
        maxWidth="sm"
        fullScreen={!!isMobile()}
        isDraggable
      >
        <Box className={classes.form}>
          <form>
            {mode === 'creation' && (
              <Dropzone selectedFiles={selectedFiles} setSelectedFiles={setSelectedFiles} />
            )}
            <Box pb={2}>
              <CustomEditorText
                placeholder="Description"
                value={description}
                onChange={handleDescriptionChange}
              />
            </Box>

            {/*<Box className={classes.multiple}>
          <InputBase
            placeholder="Mots clés"
            value={motsCles}
            onChange={handleChangeMotCle}
            fullWidth
          />
          <Box>
            {typeof motsCles === 'object' && motsCles.length > 0
              ? motsCles.map((motsCle, index: number) => (
                <Chip
                  size="small"
                  label={motsCle}
                  onDelete={handleDeleteMotsCle(index)}
                  key={index}
                />
              ))
              : ''}
          </Box>
        </Box>*/}

            <CustomSelect
              label="Type document"
              list={typeDocuments}
              onChange={event => {
                const { value } = event.target;
                setType(value);
                setIdSousCategorie(undefined);
              }}
              listId="id"
              index="libelle"
              value={type}
              inputProps={{
                readOnly: 'FACTURE' === defaultType,
              }}
              required
            />

            <OrigineInput
              onChangeOrigine={setOrigine}
              origine={origine}
              origineAssocie={correspondant}
              onChangeOrigineAssocie={setCorrespondant}
              readonly={idDefaultOrigine ? true : false}
            />

            {categoriesData && type === 'AUTRES' && (
              <CustomSelect
                disabled={categoriesLoading}
                error={categoriesError}
                label="Sous-espace documentaire"
                list={sousCategories}
                name="sousCategorie"
                onChange={event => {
                  const { value, name } = event.target;
                  setIdSousCategorie(value);
                }}
                listId="id"
                index="libelle"
                value={idSousCategorie}
                required={true}
              />
            )}

            {/* <FormControlLabel
          labelPlacement="start"
          className={classes.switchControl}
          onChange={(_event, checked) => setType(checked ? 'FACTURE' : '')}
          value={'FACTURE' === type}
          control={<Switch />}
          label="Le document est une facture"
        /> */}

            {type === 'FACTURE' && (
              <>
                <Box className={classes.flex}>
                  <TextField
                    label="N° de facture"
                    variant="outlined"
                    value={numeroFacture}
                    onChange={handleChangeNumeroFacture}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <CustomDatePicker
                    label="Date facture"
                    value={dateFacture}
                    onChange={handleChangeDateFacture}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    required
                  />
                </Box>

                <Box className={classes.flex}>
                  <CustomSelect
                    label="Mode règlement"
                    list={loadingReglementMode.data?.pRTReglementModes.nodes}
                    onChange={event => {
                      const { value } = event.target;
                      setModeReglement(value);
                    }}
                    listId="id"
                    index="libelle"
                    value={modeReglement}
                    required
                  />
                  <CustomDatePicker
                    label="Date règlement"
                    value={dateReglement}
                    onChange={handleChangeDateReglement}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                </Box>

                <Box className={classes.flex}>
                  <TextField
                    type="number"
                    label="Total HT"
                    variant="outlined"
                    value={hT}
                    onChange={handleChangeHT}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    inputProps={{
                      step: 0.5,
                    }}
                    required
                  />
                  <TextField
                    type="number"
                    label="Total TVA"
                    variant="outlined"
                    value={tva}
                    onChange={handleChangeTVA}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    inputProps={{
                      step: 0.5,
                    }}
                    required
                  />
                </Box>
                <Box>
                  <TextField
                    type="number"
                    label="Total TTC"
                    variant="outlined"
                    value={ttc}
                    onChange={handleChangeTTC}
                    fullWidth
                    InputLabelProps={{
                      shrink: true,
                    }}
                    inputProps={{
                      step: 0.5,
                    }}
                    required
                  />
                </Box>
                {parseFloat(ttc || '0') !== parseFloat(hT || '0') + parseFloat(tva || '0') &&
                  hT &&
                  tva &&
                  ttc && (
                    <Box pb={2}>
                      <Alert severity="warning">
                        Le total TTC est différent de Total HT + Total TVA
                      </Alert>
                    </Box>
                  )}

                {(parseFloat(hT || '0') < 0 ||
                  parseFloat(tva || '0') < 0 ||
                  parseFloat(ttc || '0') < 0) &&
                  origine &&
                  correspondant && (
                    <Box>
                      <CustomSelect
                        label="Associer"
                        list={
                          origine.code === 'LABORATOIRE' ? associationsPartenaires : associations
                        }
                        onChange={event => {
                          const { value } = event.target;
                          handleChangeAssocier(value);
                        }}
                        listId="id"
                        index="libelle"
                        value={associer}
                        required
                      />
                    </Box>
                  )}
                <Box className={classes.flex}>
                  <TextField
                    type="number"
                    label="N° de commande"
                    variant="outlined"
                    value={numeroCommande}
                    onChange={(event: ChangeEvent<HTMLInputElement>): void => {
                      setNumeroCommande(event.target.value);
                    }}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <Box>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={isGenererCommande}
                          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                            setIsGenererCommande(event.target.checked);
                          }}
                        />
                      }
                      label="Générer une commande"
                    />
                  </Box>
                </Box>
              </>
            )}

            <FormControlLabel
              labelPlacement="start"
              className={classes.switchControl}
              value={showAdditionnalInfosFields}
              onChange={(_event, checked) => setShowAdditionnalInfosFields(checked)}
              control={<Switch />}
              label="Saisissez des informations supplémentaires"
            />

            {showAdditionnalInfosFields && (
              <>
                <Box className={classes.flex}>
                  {categoriesLoading ? (
                    <Loader />
                  ) : (
                    <UserInput
                      label="Vérificateur"
                      idParticipants={idUserParticipants}
                      singleSelect={true}
                      selected={userVerificateur}
                      setSelected={users => {
                        if (users.length > 0) {
                          setUserVerificateur(users);
                          setIdUserVerificateur(users[0].id);
                        } else {
                          setUserVerificateur([]);
                          setIdUserVerificateur('');
                        }
                      }}
                      openModal={openUserVerificateurInput}
                      setOpenModal={setOpenUserVerificateurInput}
                      withAssignTeam={false}
                      withNotAssigned={false}
                    />
                  )}
                  {categoriesLoading ? (
                    <Loader />
                  ) : (
                    <UserInput
                      label="Rédacteur"
                      idParticipants={idUserParticipants}
                      singleSelect={true}
                      selected={userRedacteur}
                      setSelected={users => {
                        if (users.length > 0) {
                          setUserRedacteur(users);
                          setIdUserRedacteur(users[0].id);
                        } else {
                          setUserRedacteur([]);
                          setIdUserRedacteur('');
                        }
                      }}
                      openModal={openUserRedacteurInput}
                      setOpenModal={setOpenUserRedacteurInput}
                      withAssignTeam={false}
                      withNotAssigned={false}
                    />
                  )}
                </Box>

                <Box className={classes.flex}>
                  <TextField
                    variant="outlined"
                    label="Nomenclature"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    fullWidth
                    value={nomenclature}
                    onChange={handleNomenclatureChange}
                  />
                  <TextField
                    variant="outlined"
                    label="Numéro de version"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    fullWidth
                    value={numeroVersion}
                    onChange={handleNumeroVersionChange}
                  />
                </Box>

                <Box className={classes.endValidationBox}>
                  <CustomDatePicker
                    label="Date de parution"
                    value={dateParution}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    onChange={value => setDateParution(value)}
                    fullWidth
                  />
                </Box>
                <Box className={classes.flex}>
                  <CustomDatePicker
                    label="Date début validité"
                    value={dateDebutValidite}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    onChange={value => setDateDebutValidite(value)}
                    fullWidth
                  />
                  <CustomDatePicker
                    label="Date fin de validité"
                    value={dateFinValidite}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    onChange={value => setDateFinValidite(value)}
                    fullWidth
                  />
                </Box>
              </>
            )}
          </form>
        </Box>
      </CustomModal>
      <CustomModal
        open={openAssocieDialog}
        setOpen={setOpenAssocieDialog}
        title={
          associer === 'CONDITION_COMMERCIALE'
            ? 'Joindre une condition commerciale'
            : associer === 'FACTURE'
            ? 'Joindre une facture'
            : associer === 'BRI'
            ? 'Joindre BRI'
            : associer === 'REPRISE_PERIMES'
            ? 'Joindre une reprise périmés'
            : ''
        }
        closeIcon
        withBtnsActions={false}
        headerWithBgColor
        disableBackdropClick={true}
        fullScreen={!!isMobile()}
        isDraggable
      >
        <Box style={{ minWidth: 960 }}>
          {associer === 'CONDITION_COMMERCIALE' && (
            <ConditionCommerciale
              onRequestSearch={onRequestSearchCondition}
              data={
                (loadingConditions?.data?.pRTConditionCommerciales.nodes || []).map(item => ({
                  id: item.id,
                  canal: item.canal,
                  type: item.type,
                  status: item.statut,
                  libelle: item.titre,
                  fichiers: item.fichiers || null,
                  details: item.description || '',
                  periode: {
                    debut: item.dateDebut,
                    fin: item.dateFin,
                  },
                })) as any
              }
              loading={loadingConditions?.loading || false}
              total={loadingRowConditions.data?.pRTConditionCommercialeAggregate.count?.id || 0}
              listStatusConditionCommande={loadType.data?.pRTConditionCommercialeTypes.nodes || []}
              typesConditionCommande={loadStatus.data?.pRTConditionCommercialeStatuts.nodes || []}
              isFacture
              onRequestSelected={selectedConditionIds}
              selectedItemsIds={
                document?.typeAvoirAssociations?.length
                  ? document.typeAvoirAssociations
                      .filter(avoirAssociation => avoirAssociation.type === 'CONDITION_COMMERCIALE')
                      .map(avoirAssociation => avoirAssociation.correspondant)
                  : []
              }
            />
          )}
          {associer === 'FACTURE' && (
            <HistoriqueFacturePage
              loading={gettingDocuments.loading}
              error={gettingDocuments.error as any}
              data={gettingDocuments.data?.gedDocuments.nodes as any}
              rowsTotal={gettingDocuments.data?.gedDocuments.nodes.length || 0}
              onRequestSearch={searchDocuments}
              totaux={{
                totalHT: loadingTotauxFacture?.data?.getTotauxFactures?.totalHT || 0,
                totalTVA: loadingTotauxFacture?.data?.getTotauxFactures?.totalTVA || 0,
                totalTTC: loadingTotauxFacture?.data?.getTotauxFactures?.totalTTC || 0,
              }}
              isSelectable={true}
              onRequestSelected={handleSelectedFacture}
              selectedItemsIds={
                document?.typeAvoirAssociations?.length
                  ? document.typeAvoirAssociations
                      .filter(avoirAssociation => avoirAssociation.type === 'FACTURE')
                      .map(avoirAssociation => avoirAssociation.correspondant)
                  : []
              }
            />
          )}
          {associer === 'BRI' && (
            <HistoriqueRemuneration
              loading={loadingRemunerationReglement.loading}
              error={loadingRemunerationReglement.error}
              data={
                loadingRemunerationReglement.data?.pRTRemunerationReglements.nodes.map(element => {
                  return {
                    ...element,
                    dateReglement: moment(element.dateReglement),
                    remuneration: {
                      ...element.remuneration,
                      dateEcheance: moment(element.remuneration.dateEcheance),
                    },
                  };
                }) as any
              }
              isSelectable
              onRequestSelected={handleSelectedBRI}
              selectedItemsIds={
                document?.typeAvoirAssociations?.length
                  ? document.typeAvoirAssociations
                      .filter(avoirAssociation => avoirAssociation.type === 'BRI')
                      .map(avoirAssociation => avoirAssociation.correspondant)
                  : []
              }
            />
          )}
          {associer === 'REPRISE_PERIMES' && (
            <Box>
              <Box p={2}>
                <CustomEditorText
                  placeholder="Reprise des périmés"
                  value={reprisePerimes}
                  onChange={(value: string) => {
                    setReprisePerimes(value);
                  }}
                />
              </Box>
              <Box display="flex" justifyContent="flex-end">
                <CustomButton
                  color="secondary"
                  onClick={(event: React.MouseEvent<HTMLButtonElement>) => {
                    event.preventDefault();
                    event.stopPropagation();
                    setOpenAssocieDialog(false);
                  }}
                  disabled={!reprisePerimes}
                  className={classes.joindreButton}
                >
                  Joindre
                </CustomButton>
              </Box>
            </Box>
          )}
        </Box>
      </CustomModal>
    </>
  );
};

export default CreateDocument;
