import { Box, Typography } from '@material-ui/core';
import classnames from 'classnames';
import React, { FC } from 'react';
import useStyles from './styles';

interface InfoLineItem {
    label: string;
    value: string
}

interface InfoLineProps {
    items: InfoLineItem[]
}


export const InfoLine: FC<InfoLineProps> = ({ items }) => {
    const classes = useStyles()
    const validItems = items.filter(info => info.value)
    return validItems.length === 0 ? null : (<Box className={classes.infoSupContainer}>
        {validItems.map(({ label, value }, index) => {
            return (
                <Box key={index} display="flex" marginRight="15px" alignItems="center">
                    <Typography
                        className={classnames(classes.labelContent)}
                        style={{ minWidth: 'fit-content' }}
                    >
                        {label} :
                    </Typography>
                    <Typography
                        className={classnames(
                            classes.labelContentValue,
                            classes.infoValue,
                        )}
                        style={{ cursor: 'auto' }}
                    >
                        {value || '-'}
                    </Typography>
                </Box>
            );
        })}
    </Box>)

}

