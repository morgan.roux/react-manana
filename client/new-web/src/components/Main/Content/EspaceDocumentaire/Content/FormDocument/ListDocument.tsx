import React, { FC, useState, ChangeEvent, useEffect } from 'react';
import { Box, Typography, TextField } from '@material-ui/core';
import { Close } from '@material-ui/icons';
import { CustomDatePicker } from './../../../../../Common/CustomDateTimePicker';
import { useStyles } from './styles';
import OrigineInput from '../../../../../Common/OrigineInput/OrigineInput';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';

interface ListDocumentProps {
  file: File;
}

export interface Facture {
  file?: File;
  origine?: any;
  correspondant?: any;
  date?: any;
  ht?: number;
  tva?: number;
  ttc?: number;
}

const factureInitValue = {
  file: undefined,
  origine: undefined,
  correspondant: undefined,
  date: new Date(),
  ht: undefined,
  tva: undefined,
  ttc: undefined,
}

const ListDocument: FC<ListDocumentProps> = ({ file }) => {
  const classes = useStyles();

  const [origine, setOrigine] = useState<any>();
  const [correspondant, setCorrespondant] = useState<any>();
  const [facture, setFacture] = useState<Facture>(factureInitValue);
  const [date, setDate] = useState<any>(new Date());

  useEffect(() => {
    setFacture({...facture, origine: origine, correspondant: correspondant})
  },[origine, correspondant]);

  const handleChangeFacture = (event: ChangeEvent<HTMLInputElement>) => {
    setFacture({ ...facture, [event.target.name]: event.target.value });
  };

  const handleChangeDateFacture = (date: MaterialUiPickersDate) => {
    setDate(date);
  };

  console.log('----------FACTURE---------------: ', facture);

  return (
    <Box className={classes.listField}>
      <Box display="flex" flexDirection="row" justifyContent="space-between" py={1}>
        <Typography className={classes.bold}>{file.name}</Typography>
        <Close />
      </Box>
      <Box display="flex" flexDirection="row">
        <OrigineInput
          onChangeOrigine={setOrigine}
          origine={origine}
          origineAssocie={correspondant}
          onChangeOrigineAssocie={setCorrespondant}
        />
        <Box className={classes.field}>
          <CustomDatePicker
            label="Date"
            value={date}
            InputLabelProps={{
              shrink: true,
            }}
            onChange={handleChangeDateFacture}
            name="date"
          />
        </Box>
      </Box>
      <Box display="flex" flexDirection="row">
        <Box className={classes.field} pr={1}>
          <TextField
            label="Total HT"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
            }}
            onChange={handleChangeFacture}
            name="ht"
            value={facture?.ht}
          />
        </Box>
        <Box className={classes.field} pr={1}>
          <TextField
            label="TVA"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
            }}
            onChange={handleChangeFacture}
            name="tva"
            value={facture?.tva}
          />
        </Box>
        <Box className={classes.field}>
          <TextField
            label="Total TTC"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
            }}
            onChange={handleChangeFacture}
            name="ttc"
            value={facture?.ttc}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default ListDocument;
