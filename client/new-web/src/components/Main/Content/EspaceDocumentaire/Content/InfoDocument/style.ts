import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
  detail: {
    padding: theme.spacing(4),
    '&>div': {
      marginBottom: theme.spacing(2),
      '@media(min-width: 1280px)': {
        maxWidth: '50%',
      },
    },
  },
}));
