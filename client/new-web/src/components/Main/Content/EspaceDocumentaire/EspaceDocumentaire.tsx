import { Box, IconButton, ThemeProvider } from '@material-ui/core';
import React, { Dispatch, FC, SetStateAction, useCallback, useState } from 'react';
import GEDFilter from './GEDFilter';
import createTheme from './theme';
import style from './style';
import { ME } from '../../../../graphql/Authentication/types/ME';
import { GET_ME } from '../../../../graphql/Authentication/query';
import { useQuery } from '@apollo/client';
import { availableThemes } from '../../../../Theme';
import { Fichier } from '@app/ui-kit';
import { SMYLEYS_smyleys } from '../../../../graphql/Smyley/types/SMYLEYS';
import LeftSidebarAndMainPage from '../../../Common/Layouts/LeftSidebarAndMainPage';
import { MainContainer } from './MainContainer/MainContainer';
import FilterAlt from '../../../../assets/icons/todo/filter_alt_white.svg';
import { Refresh } from '@material-ui/icons';

export interface User {
  id: string;
  fullName: string;
  prestataireService?: {
    id: string;
    nom: string;
  };
}

export interface Role {
  id: string;
  type: string;
  nom: string;
}

export interface IDocumentSearch {
  searchText?: string | undefined;
  accesRapide?:
    | 'FAVORIS'
    | 'NOUVEAUX'
    | 'RECENTS'
    | 'PLUS_CONSULTES'
    | 'PLUS_TELECHARGES'
    | 'REFUSES'
    | 'APPROUVES'
    | 'EN_ATTENTE_APPROBATION'
    | undefined;
  proprietaire?: 'MOI' | 'AUTRE' | 'TOUT';
  idSousCategorie?: string | undefined;
  filterBy?: 'DERNIERE_SEMAINE' | 'DERNIER_MOIS' | 'TOUT' | '12_DERNIERS_MOIS' | undefined;
  sortBy?: string;
  sortDirection?: 'ASC' | 'DESC' | undefined;

  type?: string;
  validation?: 'INTERNE' | 'PRESTATAIRE_SERVICE' | 'TOUT'
  idOrigine?: string
  idOrigineAssocie?: string
  dateDebut?: string
  dateFin?: string
}
export interface DocumentCategorie {
  id?: string;
  description?: string;
  nomenclature?: string;
  numeroVersion?: string;
  motCle1?: string;
  motCle2?: string;
  motCle3?: string;
  dateHeureParution?: Date;
  dateHeureDebutValidite?: Date;
  dateHeureFinValidite?: Date;
  idDocumentARemplacer?: string;
  idUserRedacteur?: string;
  idUserVerificateur?: string;
  idFichier?: string;
  idSousCategorie?: string;
  idPharmacie?: string;
  idGroupement?: string;
  createdAt?: Date;
  updatedAt?: Date;
  createdBy?: {
    id: string;
  };
  favoris?: boolean;
  nombreConsultations?: number;
  nombreTelechargements?: number;
  nombreCommentaires?: number;
  nombreReactions?: number;
  sousCategorie?: {
    id?: string;
    libelle?: string;
  };
  fichier?: Fichier;
  verificateur?: User;
  redacteur?: User;
  categorie?: {
    id?: string;
    libelle?: string;
  };
  dernierChangementStatut?: {
    id?: string;
    idDocument?: string;
    status?: string;
    commentaire?: string;
    idGroupement?: string;
    createdBy?: string;
    updatedBy?: string;
    createdAt?: Date;
    updatedAt?: Date;
  };
  documentARemplacer?: DocumentCategorie;
  idOrigine?: string;
  idOrigineAssocie?: string;
  factureDate?: Date;
  factureTotalHt?: number;
  factureTva?: number;
  factureTotalTtc?: number;
  type: 'FACTURE' | 'CATEGORIE';

  factureDateReglement?: Date;
  idCommandes?: string[];
  idReglementMode?: string;
  numeroFacture?: string;
  typeAvoirAssociations?: {
    idDocument: string;
    type: string;
    correspondant: string;
  }[];
  commande?: {
    id?: string;
    nbrRef?: number;
  };
  isGenererCommande?: boolean;
  numeroCommande?: number;

  changementStatuts?: {
    id: string;
    idDocument: string;
    status: string;
    commentaire: string;
    idGroupement: string;
    createdAt: Date;
    updatedAt: Date;
    createdBy: User;
    updatedBy: User;
  }[];
}
interface EspaceDocumentaireProps {
  categories: {
    loading: boolean;
    error: Error;
    data: any[];
  };
  documents: {
    loading: boolean;
    error: Error;
    data: DocumentCategorie[];
    total: number;
  };
  content: {
    loading: boolean;
    error: Error;
    data: DocumentCategorie;
  };
  commentaires: {
    loading: boolean;
    error: Error;
    data: any[];
  };
  mode: 'creation' | 'remplacement';
  saving: boolean;
  saved: boolean;
  setSaved: (newValue: boolean) => void;
  activeDocument?: DocumentCategorie;
  onRequestCreateDocument: (document: any) => void;
  onRequestAddDocumentToFavorite: (document: any) => void;
  onRequestDeleteDocument: (document: any) => void;
  onRequestDetailsDocument: (document: any) => void;
  onRequestRemoveDocumentFromFavorite: (document: any) => void;
  onRequestReplaceDocument: (document: any) => void;
  onRequestFetchMoreComments: (document: DocumentCategorie) => void;
  refetchComments: () => void;
  refetchSmyleys: () => void;
  onRequestScroll: () => void;
  onRequestGetComments: (document: DocumentCategorie) => void;
  onRequestRefresh: () => void;
  onRequestDeplace: (document: DocumentCategorie) => void;
  onRequestLike: (document: DocumentCategorie, smyley: SMYLEYS_smyleys | null) => void;
  setSearchVariables: Dispatch<SetStateAction<IDocumentSearch>>;
  searchVariables: IDocumentSearch;
  factureFournisseurs: any[];
  facturesRejeteesInterneCategories: {annee:number, mois:number}[]
  facturesRejeteesPrestataireServiceCategories: {annee:number, mois:number}[]
  onCompleteDownload: (document: DocumentCategorie) => void;
  onGoback: () => void;
  refetchDocument: () => void;
  onRequestFetchAll : (withSearch:boolean) => void;
  onRequestDocument: (id: string) => void;
}

const EspaceDocumentaire: FC<EspaceDocumentaireProps> = ({
  categories,
  documents,
  commentaires,
  content,
  saving,
  mode,
  factureFournisseurs,
  facturesRejeteesInterneCategories,
  facturesRejeteesPrestataireServiceCategories,
  saved,
  setSaved,
  searchVariables,
  onRequestCreateDocument,
  onRequestAddDocumentToFavorite,
  onRequestDeplace,
  onRequestDeleteDocument,
  onRequestDetailsDocument,
  onRequestRemoveDocumentFromFavorite,
  onRequestReplaceDocument,
  onRequestScroll,
  onRequestFetchMoreComments,
  onRequestGetComments,
  onRequestRefresh,
  onRequestLike,
  setSearchVariables,
  refetchComments,
  refetchSmyleys,
  onCompleteDownload,
  onGoback,
  refetchDocument,
  activeDocument,
  onRequestDocument,
  onRequestFetchAll
}) => {
  const classes = style();

  const me = useQuery<ME>(GET_ME);
  const themeName = me && me.data && me.data.me && me.data.me.theme;
  const theme = createTheme(themeName && availableThemes[themeName]);

  // Handling drawer menu
  // filter
  const [openFilter, setOpenFilter] = useState<boolean>(false);
  const [filter, setFilter] = useState(typeof window !== 'undefined' && window.innerWidth < 1366);
  const handleClickFilterMenu = () => setOpenFilter(prev => !prev);
  const [activeSousCategorie, setActiveSousCategorie] = useState<any | undefined>(undefined);
  // sidebar
  const [openSidebar, setOpenSidebar] = useState<boolean>(false);
  const [sidebar, setSidebar] = useState(typeof window !== 'undefined' && window.innerWidth < 1024);
  const handleClickSidebarMenu = () => setOpenSidebar(prev => !prev);
  const [openDrawer, setOpenDrawer] = useState<boolean>(false);

  const handleDrawerToggle = () => {
    setOpenDrawer(prev => !prev);
  };

  React.useEffect(() => {
    window.addEventListener('resize', () => {
      if (window.innerWidth < 1366) {
        setFilter(true);
        if (window.innerWidth < 1024) setSidebar(true);
        else setSidebar(false);
      } else setFilter(false);
    });
  });

  const handleClickFilter = () => {
    setOpenDrawer(true);
  };

  const filterBtn = (
    <IconButton color="inherit" aria-label="settings" edge="start" onClick={handleClickFilter}>
      <img src={FilterAlt} />
    </IconButton>
  );

  const refreshBtn = (
    <IconButton color="inherit" aria-label="settings" edge="start" onClick={onRequestRefresh}>
      <Refresh />
    </IconButton>
  );

  return (
    <ThemeProvider theme={theme}>
      <Box className={classes.GEDContainer}>
        <LeftSidebarAndMainPage
          drawerTitle="Espace documetaire"
          sidebarChildren={
            <GEDFilter
              factureFournisseurs={factureFournisseurs}
              facturesRejeteesInterneCategories={facturesRejeteesInterneCategories}
              facturesRejeteesPrestataireServiceCategories={facturesRejeteesPrestataireServiceCategories}
              searchVariables={searchVariables}
              setSearchVariables={setSearchVariables}
              activeSousCategorie={activeSousCategorie}
              setActiveSousCategorie={setActiveSousCategorie}
              categories={categories}
              onGoBack={onGoback}
              onRequestRefresh={onRequestRefresh}
            />
          }
          mainChildren={
            <MainContainer
              sideBarProps={{
                saving: saving,
                saved: saved,
                setSaved: setSaved,
                activeSousCategorie: activeSousCategorie,
                documents: documents,
                searchVariables: searchVariables,
                setActiveDocument: onRequestDetailsDocument,
                onRequestDeplace: onRequestDeplace,
                onRequestAddToFavorite: onRequestAddDocumentToFavorite,
                onRequestDelete: onRequestDeleteDocument,
                onRequestDetails: onRequestDetailsDocument,
                onRequestRemoveFromFavorite: onRequestRemoveDocumentFromFavorite,
                onRequestReplace: onRequestReplaceDocument,
                onRequestScroll: onRequestScroll,
                onRequestGetComments: onRequestGetComments,
                setSearchVariables: setSearchVariables,
                onRequestCreateDocument: onRequestCreateDocument,
                onRequestRefresh: onRequestRefresh,
                document: content.data,
              }}
              contentProps={{
                saving: saving,
                saved: saved,
                setSaved: setSaved,
                activeSousCategorie: activeSousCategorie,
                activeDocument: activeDocument,
                mobileView: sidebar,
                tabletView: filter,
                content: content,
                commentaires: commentaires,
                onRequestDocument: onRequestDocument,
                refetchComments: refetchComments,
                handleSidebarMenu: handleClickSidebarMenu,
                handleFilterMenu: handleClickFilterMenu,
                onRequestCreateDocument: onRequestCreateDocument,
                onRequestAddToFavorite: onRequestAddDocumentToFavorite,
                onRequestDelete: onRequestDeleteDocument,
                onRequestRemoveFromFavorite: onRequestRemoveDocumentFromFavorite,
                onRequestReplace: onRequestReplaceDocument,
                onRequestFetchMoreComments: onRequestFetchMoreComments,
                onRequestLike: onRequestLike,
                onRequestDeplace: onRequestDeplace,
                onCompleteDownload: onCompleteDownload,
                refetchSmyleys: refetchSmyleys,
                refetchDocument: refetchDocument,
                onRequestFetchAll: onRequestFetchAll,
                setSearchVariables: setSearchVariables,
                searchVariables: searchVariables,
              }}
            />
          }
          optionBtn={[refreshBtn, filterBtn]}
          handleDrawerToggle={handleDrawerToggle}
          openDrawer={openDrawer}
          setOpenDrawer={setOpenDrawer}
        />
      </Box>
    </ThemeProvider>
  );
};

export default EspaceDocumentaire;
