import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
  drawerTransition: {
    // padding: theme.spacing(2),
    height: '100%',
    overflowY: 'auto',
    borderRight: `1px solid ${theme.palette.divider}`,
    transition: theme.transitions.create('all', {
      duration: theme.transitions.duration.enteringScreen,
      easing: theme.transitions.easing.easeInOut,
      delay: '0.2s',
    }),
  },
  drawerOpen: {
    top: 86,
    height: 'calc(100% - 86px)',
  },
}));
