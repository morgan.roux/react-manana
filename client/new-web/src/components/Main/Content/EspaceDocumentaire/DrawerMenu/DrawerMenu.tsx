import { Box, Drawer, DrawerProps } from '@material-ui/core';
import React, { FC } from 'react';
import style from './style';
import classnames from 'classnames';

const DrawerMenu: FC<DrawerProps & { mobile: boolean; className?: string }> = ({
  mobile,
  children,
  className,
  ...props
}) => {
  const classes = style();
  const containerRef = React.createRef<HTMLDivElement>();

  return mobile ? (
    <div ref={containerRef}>
      <Drawer
        {...props}
        PaperProps={{
          className: classes.drawerTransition,
          classes: {
            root: classnames(classes.drawerOpen, { [className ? className : '']: className }),
          },
        }}
        keepMounted
        container={containerRef.current}
      >
        {children}
      </Drawer>
    </div>
  ) : (
    <Box
      className={classnames(classes.drawerTransition, { [className ? className : '']: className })}
    >
      {children}
    </Box>
  );
};

export default DrawerMenu;
