import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
  menu: {
    minWidth: 300,
    padding: theme.spacing(2),
  },
  treeItemSelected: {
    background: '#FFFFFF !important',
    paddingLeft: 15
  },

  treeItemIconContainer: {
    display: 'none'
  },

  heading: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: theme.spacing(1.5),
    paddingBottom: theme.spacing(1),
    borderBottom: `1px solid ${theme.palette.divider}`,
    justifyContent: 'space-between',
  },
  active: {
    backgroundColor: theme.palette.grey[200],
    color: theme.palette.common.black,
    height: 48,
  },
  fontMenu: {
    fontSize: 14,
    fontFamily: 'Roboto',
    display: 'flex',
    justifyContent: 'space-between',
  },
  treeItem: {
    marginBottom: 16,
    '& li': {
      margin: 0,
      marginLeft: 30,
      padding: 0,
    }
  },
}));
