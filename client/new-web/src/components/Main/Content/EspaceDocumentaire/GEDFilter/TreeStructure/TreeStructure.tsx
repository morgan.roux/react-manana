import {
  Accordion,
  AccordionDetails,
  AccordionProps,
  AccordionSummary,
  Typography,
} from '@material-ui/core';
import { KeyboardArrowDown } from '@material-ui/icons';
import React, { FC } from 'react';

const TreeStructure: FC<AccordionProps & { summary: string }> = ({
  children,
  summary,
  ...props
}) => {
  return (
    <Accordion {...props} elevation={0} style={{ width: '100%' }} square={true}>
      <AccordionSummary IconButtonProps={{ size: 'small' }} expandIcon={<KeyboardArrowDown />}>
        <Typography style={{ fontSize: 14, fontWeight: 'bold', marginBottom: 8 }}>
          {summary}
        </Typography>
      </AccordionSummary>
      <AccordionDetails>
        <div style={{ width: '100%' }}>{children}</div>
      </AccordionDetails>
    </Accordion>
  );
};

export default TreeStructure;
