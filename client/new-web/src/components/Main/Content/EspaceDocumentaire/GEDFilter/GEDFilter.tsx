import {
  Box,
  FormControl,
  FormControlLabel,
  IconButton,
  MenuItem,
  Radio,
  RadioGroup,
  TextField,
  Typography,
} from '@material-ui/core';
import React, { FC, useState, useEffect, Dispatch, SetStateAction } from 'react';
import style from './style';
import TreeStructure from './TreeStructure';
import { DebouncedSearchInput, ErrorPage } from '@app/ui-kit';
import { IDocumentSearch } from '../EspaceDocumentaire';
import TreeItem from '@material-ui/lab/TreeItem';
import { TreeView } from '@material-ui/lab';
import ReplayIcon from '@material-ui/icons/Replay';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { isMobile } from '../../../../../utils/Helpers';
import useOrigines from './../../../../hooks/basis/useOrigines';
import classNames from 'classnames';
import moment from 'moment';
import {
  useValueParameterAsBoolean,
  useValueParameterAsString,
} from '../../../../../utils/getValueParameter';
import { getUser } from '../../../../../services/LocalStorage';
import { endTime, startTime } from '../../DemarcheQualite/util';

const filterStatutsDocument = [
  {
    id: '1',
    libelle: 'Tous les documents',
    code: 'TOUT',
  },
  {
    id: '2',
    libelle: 'Documents à traiter',
    code: 'EN_ATTENTE_APPROBATION',
  },

  {
    id: '3',
    libelle: 'Documents validés',
    code: 'APPROUVES',
  },

  {
    id: '4',
    libelle: 'Documents refusés',
    code: 'REFUSES',
  },
];

interface TemporalCategorie {
  nom: string;
  dateDebut: Date;
  dateFin: Date;
}

export interface FilterRadioProps {
  // valueFilterBy?: any;
  // valueFilterTime?: any;
  valueFilterSatut?: any;
  // onChangeFilterBy: (value: any) => void;
  // onChangeFilterTime: (value: any) => void;
  onChangeFilterStatut: (value: any) => void;
}

export const FilterRadio: FC<FilterRadioProps> = ({
  // valueFilterBy,
  // valueFilterTime,
  valueFilterSatut = 'TOUT',
  // onChangeFilterBy,
  // onChangeFilterTime,
  onChangeFilterStatut,
}) => {
  /* const handleChangeFilterType = (__event: React.ChangeEvent<HTMLInputElement>, value: string) => {
     onChangeFilterBy(JSON.parse(value));
   };
 
   const handleChangeFilterTime = (__event: React.ChangeEvent<HTMLInputElement>, value: string) => {
     onChangeFilterTime(JSON.parse(value));
   };
   */

  const handleChangeFilterStatut = (
    __event: React.ChangeEvent<HTMLInputElement>,
    value: string,
  ) => {
    onChangeFilterStatut(value);
  };

  console.log('********************VALUE', valueFilterSatut);
  return (
    <>
      <FormControl style={{ marginBottom: 24 }} component="fieldset">
        <RadioGroup name="doc" value={valueFilterSatut} onChange={handleChangeFilterStatut}>
          {filterStatutsDocument.map(value => (
            <FormControlLabel
              value={value.code}
              control={<Radio />}
              label={value.libelle}
              checked={value.code === valueFilterSatut}
            />
          ))}
        </RadioGroup>
      </FormControl>

      {/*<FormControl style={{ marginBottom: 24 }} component="fieldset">
        <RadioGroup name="doc" value={valueFilterBy} onChange={handleChangeFilterType}>
          {filterDocumentProprietaire.map(value => (
            <FormControlLabel
              value={JSON.stringify(value)}
              control={<Radio />}
              label={value.libelle}
            />
          ))}
        </RadioGroup>
      </FormControl>
      <hr />
      <FormControl style={{ margin: '24px 0' }} component="fieldset">
        <RadioGroup name="doc" value={valueFilterTime} onChange={handleChangeFilterTime}>
          {filterDocumentFilterBy.map(value => (
            <FormControlLabel
              value={JSON.stringify(value)}
              control={<Radio />}
              label={value.libelle}
            />
          ))}
        </RadioGroup>
      </FormControl>
      <hr />*/}
    </>
  );
};

interface GedFilterProps {
  categories: {
    loading: boolean;
    error: Error;
    data: any[];
  };
  factureFournisseurs: any[]; // Laboratoire ou PrestataireService
  facturesRejeteesInterneCategories: { annee: number; mois: number }[];
  facturesRejeteesPrestataireServiceCategories: { annee: number; mois: number }[];
  activeSousCategorie: any | undefined;
  setActiveSousCategorie: (sousCategorie: any | undefined) => void;
  setSearchVariables: Dispatch<SetStateAction<IDocumentSearch>>;
  searchVariables: IDocumentSearch;
  onRequestRefresh: () => void;
  onGoBack: () => void;
}

const GEDFilter: FC<GedFilterProps> = ({
  setActiveSousCategorie,
  setSearchVariables,
  searchVariables,
  categories,
  factureFournisseurs,
  facturesRejeteesInterneCategories,
  facturesRejeteesPrestataireServiceCategories,
  onRequestRefresh,
  onGoBack,
}) => {
  const classes = style();
  const [expanded, setExpanded] = useState<any>({
    accesRapide: true,
    pharmacie: false,
    partage: false,
  });
  const user = getUser();

  const origines = useOrigines();
  const idOrigineLaboratoire = (origines.data?.origines.nodes || []).find(
    ({ code }) => code === 'LABORATOIRE',
  )?.id;
  const idOriginePrestataireService = (origines.data?.origines.nodes || []).find(
    ({ code }) => code === 'PRESTATAIRE_SERVICE',
  )?.id;
  const enableValidationPrestataire = useValueParameterAsBoolean('0850');
  const idPrestataireValidateur = useValueParameterAsString('0851');

  const [currentSousCategorie, setCurrentSousCategorie] = useState<any | undefined>();
  const [textToSearch, setTextToSearch] = useState<string>('');
  const [partagerSousCategorie, setPartagerSousCategorie] = useState<any[]>([]);
  const [pharmacieSousCategorie, setPharmacieSousCategorie] = useState<any[]>([]);
  const [valueFilterTime, setValueFilterTime] = useState<string | undefined>();
  const [valueFilterStatut, setValueFilterStatut] = useState<string | undefined>();

  useEffect(() => {
    if (currentSousCategorie) {
      setActiveSousCategorie(currentSousCategorie);
    }
  }, [currentSousCategorie]);

  useEffect(() => {
    setValueFilterStatut(searchVariables.accesRapide || 'TOUT');
  }, [searchVariables]);

  useEffect(() => {
    const _partageSousCategorie = [];
    const _pharmacieSousCategorie = [];
    (categories.data || []).map(categorie => {
      const sousCategoriePartage = [];
      const sousCategoriePharmacie = [];
      for (let sousCategorie of categorie.mesSousCategories || []) {
        if (sousCategorie.idPartenaireValidateur) {
          sousCategoriePartage.push(sousCategorie as never);
        } else {
          sousCategoriePharmacie.push(sousCategorie as never);
        }
      }
      if (sousCategoriePartage.length > 0)
        _partageSousCategorie.push({
          ...categorie,
          mesSousCategories: sousCategoriePartage,
        } as never);

      if (sousCategoriePharmacie.length > 0)
        _pharmacieSousCategorie.push({
          ...categorie,
          mesSousCategories: sousCategoriePharmacie,
        } as never);
    });
    setPartagerSousCategorie(_partageSousCategorie);
    setPharmacieSousCategorie(_pharmacieSousCategorie);
  }, [categories.data]);

  const handleExpandAccordion = (id: string) => (_: React.ChangeEvent<{}>, expanded: boolean) => {
    setExpanded(prevState => ({ ...prevState, [id]: expanded }));
  };

  const handleSousCategorieClick = (sousCategorie: any): void => {
    setCurrentSousCategorie(sousCategorie);
    // setSearchVariables(prev => ({
    //   ...prev,
    //   accesRapide: undefined,
    //   idSousCategorie: sousCategorie?.id as any,
    // }));
    handleChangeSearchVariables({
      accesRapide: undefined,
      idSousCategorie: sousCategorie?.id as any,
    });
  };

  const quickAccess = (type: string): void => {
    // setSearchVariables(prev => ({
    //   ...prev,
    //   idSousCategorie: undefined,
    //   accesRapide: type === 'TOUT' ? undefined : type as any,
    // }));
    handleChangeSearchVariables({ accesRapide: type === 'TOUT' ? undefined : (type as any) });
  };

  const handleTextToSearchChange = (value: string): void => {
    setTextToSearch(value);
    setSearchVariables(prev => ({
      ...prev,
      idSousCategorie: undefined,
      accesRapide: undefined,
      searchText: value,
    }));
  };

  const handleChangeSearchVariables = (newValues: any): void => {
    setSearchVariables(prev => ({
      ...prev,
      idSousCategorie: undefined,
      filterBy: 'TOUT',
      sortBy: undefined,
      type: undefined,
      validation: 'TOUT',
      idOrigine: undefined,
      idOrigineAssocie: undefined,
      dateDebut: undefined,
      dateFin: undefined,
      ...newValues,
    }));
  };

  /*  const handleChangeFilterType = (value: any) => {
      setValueFilterByDoc(value);
      setSearchVariables(prev => ({
        ...prev,
        proprietaire: value.code,
      }));
    };
  
  */

  const handleChangeFilterTime = (value: any) => {
    setValueFilterTime(value);
    setSearchVariables(prev => ({
      ...prev,
      filterBy: value.code,
    }));
  };

  const withPrestataireServiceValidateur = enableValidationPrestataire && idPrestataireValidateur;
  const isPrestataire = !!user?.userPartenaire?.id;
  return (
    <Box>
      {!categories.loading &&
        (!categories?.error && categories?.data ? (
          <>
            {!isMobile() ? (
              <>
                <Box className={classes.heading}>
                  <IconButton size="small" onClick={onGoBack}>
                    <ArrowBackIcon />
                  </IconButton>
                  <Typography variant="h2" color="secondary">
                    Espace documentaire
                  </Typography>
                  <IconButton size="small" onClick={onRequestRefresh}>
                    <ReplayIcon />
                  </IconButton>
                </Box>
                <Box marginBottom={3} marginTop={3}>
                  <DebouncedSearchInput
                    wait={500}
                    outlined
                    onChange={handleTextToSearchChange}
                    value={textToSearch}
                  />

                  {/* <TextField
                    variant="outlined"
                    placeholder="Rechercher..."
                    InputProps={{
                      endAdornment: <Search />,
                    }}
                    type="search"
                    fullWidth={true}
                    value={textToSearch}
                    onChange={handleTextToSearchChange}
                  />*/}
                </Box>
              </>
            ) : (
              <FilterRadio
                //  onChangeFilterBy={handleChangeFilterType}
                //  onChangeFilterTime={handleChangeFilterTime}
                onChangeFilterStatut={quickAccess}
                // valueFilterBy={valueFilterByDoc}
                // valueFilterTime={valueFilterTime}
                valueFilterSatut={valueFilterStatut}
              />
            )}
            <TreeStructure
              summary="Accès rapide"
              expanded={expanded.accesRapide === true}
              onChange={handleExpandAccordion('accesRapide')}
              id="acces-rapide"
            >
              <MenuItem
                style={{ marginBottom: 8 }}
                className={classNames(
                  {
                    [classes.active]: searchVariables.accesRapide === 'FAVORIS',
                  },
                  classes.fontMenu,
                )}
                onClick={() => quickAccess('FAVORIS')}
              >
                Document Favoris
              </MenuItem>
              <MenuItem
                style={{ marginBottom: 8 }}
                className={classNames(
                  {
                    [classes.active]: searchVariables.accesRapide === 'NOUVEAUX',
                  },
                  classes.fontMenu,
                )}
                onClick={() => quickAccess('NOUVEAUX')}
              >
                Nouveaux document
              </MenuItem>
              <MenuItem
                style={{ marginBottom: 8 }}
                className={classNames(
                  {
                    [classes.active]: searchVariables.accesRapide === 'RECENTS',
                  },
                  classes.fontMenu,
                )}
                onClick={() => quickAccess('RECENTS')}
              >
                Document récents
              </MenuItem>
              <MenuItem
                style={{ marginBottom: 8 }}
                className={classNames(
                  {
                    [classes.active]: searchVariables.accesRapide === 'PLUS_CONSULTES',
                  },
                  classes.fontMenu,
                )}
                onClick={() => quickAccess('PLUS_CONSULTES')}
              >
                Document les plus consultés
              </MenuItem>
              <MenuItem
                className={classNames(
                  {
                    [classes.active]: searchVariables.accesRapide === 'PLUS_TELECHARGES',
                  },
                  classes.fontMenu,
                )}
                onClick={() => quickAccess('PLUS_TELECHARGES')}
              >
                Document les plus téléchargés
              </MenuItem>
            </TreeStructure>
            {!isPrestataire && (
              <TreeStructure
                summary="Espace pharmacie"
                expanded={expanded.pharmacie === true}
                onChange={handleExpandAccordion('pharmacie')}
                id="espace-pharmacie"
              >
                <TreeView>
                  {(pharmacieSousCategorie || []).map((categorie: any, index: number) => {
                    const reducer = (accumulator, currentValue) =>
                      accumulator + currentValue.nombreDocuments;
                    const nombreDocumentCategorie = (categorie?.mesSousCategories || []).reduce(
                      reducer,
                      0,
                    );
                    return (
                      <TreeItem
                        className={classes.treeItem}
                        key={categorie?.id || index}
                        label={
                          <span className={classes.fontMenu}>
                            <span>{categorie.libelle}</span>{' '}
                            <span style={{ fontWeight: 'bold' }}>{nombreDocumentCategorie}</span>
                          </span>
                        }
                        nodeId={categorie?.id}
                        id={categorie?.id}
                        classes={{ label: classes.treeItemSelected }}
                      >
                        {(categorie?.mesSousCategories || []).map(
                          (sousCategorie: any, indice: number) => (
                            <MenuItem
                              style={{ marginTop: 16 }}
                              className={classNames(
                                {
                                  [classes.active]: currentSousCategorie?.id === sousCategorie?.id,
                                },
                                classes.fontMenu,
                              )}
                              onClick={() => handleSousCategorieClick(sousCategorie)}
                              key={sousCategorie?.id || indice}
                            >
                              <span>{sousCategorie.libelle}</span>{' '}
                              <span style={{ fontWeight: 'bold' }}>
                                {sousCategorie.nombreDocuments}
                              </span>
                            </MenuItem>
                          ),
                        )}
                      </TreeItem>
                    );
                  })}
                </TreeView>
              </TreeStructure>
            )}
            <TreeStructure
              summary={isPrestataire ? 'Espace des factures partenaires' : 'Espace factures'}
              expanded={expanded.facture === true}
              onChange={handleExpandAccordion('facture')}
              id="espace-facture"
            >
              <TreeView>
                <MenuItem
                  style={{ marginBottom: 8 }}
                  className={classNames(
                    {
                      [classes.active]:
                        searchVariables.type === 'FACTURE' &&
                        searchVariables.accesRapide === 'EN_ATTENTE_APPROBATION',
                    },
                    classes.fontMenu,
                  )}
                  onClick={() =>
                    handleChangeSearchVariables({
                      type: 'FACTURE',
                      accesRapide: 'EN_ATTENTE_APPROBATION',
                    })
                  }
                >
                  Factures à valider
                </MenuItem>

                <TreeItem
                  className={classes.treeItem}
                  label={
                    <span className={classes.fontMenu}>
                      <span>Factures validées</span>{' '}
                    </span>
                  }
                  nodeId="factures-validees"
                  id="factures-validees"
                  classes={{
                    label: classes.treeItemSelected,
                    iconContainer: classes.treeItemIconContainer,
                  }}
                >
                  {(factureFournisseurs || []).map((partenaire: any, indice: number) => (
                    <MenuItem
                      style={{ marginTop: 16 }}
                      className={classNames(
                        {
                          [classes.active]:
                            searchVariables.type === 'FACTURE' &&
                            searchVariables.accesRapide === 'APPROUVES' &&
                            searchVariables.idOrigineAssocie === partenaire.id,
                        },
                        classes.fontMenu,
                      )}
                      onClick={() =>
                        handleChangeSearchVariables({
                          accesRapide: 'APPROUVES',
                          type: 'FACTURE',
                          idOrigineAssocie: partenaire.id,
                          idOrigine:
                            partenaire.dataType === 'Laboratoire'
                              ? idOrigineLaboratoire
                              : idOriginePrestataireService,
                        })
                      }
                      key={partenaire?.id || indice}
                    >
                      <span>{partenaire.nom}</span>
                    </MenuItem>
                  ))}
                </TreeItem>

                {!isPrestataire && (
                  <TreeItem
                    className={classes.treeItem}
                    label={
                      <span className={classes.fontMenu}>
                        <span>
                          {withPrestataireServiceValidateur
                            ? 'Rejetées pharmacie'
                            : 'Factures rejetées'}
                        </span>{' '}
                      </span>
                    }
                    nodeId="factures-rejetees-interne"
                    id="factures-rejetees-interne"
                    classes={{
                      label: classes.treeItemSelected,
                      iconContainer: classes.treeItemIconContainer,
                    }}
                  >
                    {facturesRejeteesInterneCategories.map(({ mois, annee }) => {
                      const factureMonth = moment
                        .utc()
                        .set('year', annee)
                        .set('month', mois - 1);

                      const dateDebut = factureMonth
                        .clone()
                        .startOf('month')
                        .toISOString();
                      const dateFin = factureMonth
                        .clone()
                        .endOf('month')
                        .endOf('day')
                        .toISOString();
                      const nom = moment(dateDebut).format('YYYY-MM');

                      return (
                        <MenuItem
                          style={{ marginTop: 16 }}
                          className={classNames(
                            {
                              [classes.active]:
                                searchVariables.type === 'FACTURE' &&
                                searchVariables.accesRapide === 'REFUSES' &&
                                (!withPrestataireServiceValidateur ||
                                  searchVariables.validation === 'INTERNE') &&
                                searchVariables.dateDebut === dateDebut &&
                                searchVariables.dateFin === dateFin,
                            },
                            classes.fontMenu,
                          )}
                          onClick={() =>
                            handleChangeSearchVariables({
                              dateDebut,
                              dateFin,
                              validation: withPrestataireServiceValidateur ? 'INTERNE' : undefined,
                              type: 'FACTURE',
                              accesRapide: 'REFUSES',
                            })
                          }
                          key={nom}
                        >
                          <span>{nom}</span>
                        </MenuItem>
                      );
                    })}
                  </TreeItem>
                )}

                {!isPrestataire && withPrestataireServiceValidateur && (
                  <MenuItem
                    style={{ marginBottom: 8 }}
                    className={classNames(
                      {
                        [classes.active]:
                          searchVariables.type === 'FACTURE' &&
                          searchVariables.accesRapide === 'APPROUVES' &&
                          searchVariables.validation === 'INTERNE'
                      },
                      classes.fontMenu,
                    )}
                    onClick={() =>
                      handleChangeSearchVariables({
                        type: 'FACTURE',
                        accesRapide: 'APPROUVES',
                        validation: 'INTERNE',
                      })
                    }
                  >
                    Factures comptabilisées
                  </MenuItem>
                )}

                {withPrestataireServiceValidateur && (
                  <TreeItem
                    className={classes.treeItem}
                    label={
                      <span className={classes.fontMenu}>
                        <span>
                          {isPrestataire
                            ? `Factures rejetées`
                            : `Rejetées comptable`}
                        </span>
                      </span>
                    }
                    nodeId="factures-rejetees-comptable"
                    id="factures-rejetees-comptable"
                    classes={{
                      label: classes.treeItemSelected,
                      iconContainer: classes.treeItemIconContainer,
                    }}
                  >
                    {facturesRejeteesPrestataireServiceCategories.map(({ mois, annee }) => {
                      const factureMonth = moment
                        .utc()
                        .set('year', annee)
                        .set('month', mois - 1);

                      const dateDebut = factureMonth
                        .clone()
                        .startOf('month')
                        .toISOString();
                      const dateFin = factureMonth
                        .clone()
                        .endOf('month')
                        .endOf('day')
                        .toISOString();
                      const nom = moment(dateDebut).format('YYYY-MM');

                      return (
                        <MenuItem
                          style={{ marginTop: 16 }}
                          className={classNames(
                            {
                              [classes.active]:
                                searchVariables.type === 'FACTURE' &&
                                searchVariables.accesRapide === 'REFUSES' &&
                                searchVariables.validation === 'PRESTATAIRE_SERVICE' &&
                                searchVariables.dateDebut === dateDebut &&
                                searchVariables.dateFin === dateFin,
                            },
                            classes.fontMenu,
                          )}
                          onClick={() =>
                            handleChangeSearchVariables({
                              dateDebut,
                              dateFin,
                              validation: 'PRESTATAIRE_SERVICE',
                              type: 'FACTURE',
                              accesRapide: 'REFUSES',
                            })
                          }
                          key={nom}
                        >
                          <span>{nom}</span>
                        </MenuItem>
                      );
                    })}
                  </TreeItem>
                )}
              </TreeView>
            </TreeStructure>

            <TreeStructure
              summary="Espace partagé"
              expanded={expanded.partage === true}
              onChange={handleExpandAccordion('partage')}
              id="espace-partage"
            >
              <TreeView>
                {(partagerSousCategorie || []).map((categorie: any, index: number) => {
                  const reducer = (accumulator, currentValue) =>
                    accumulator + currentValue.nombreDocuments;
                  const nombreDocumentCategorie = (categorie?.mesSousCategories || []).reduce(
                    reducer,
                    0,
                  );
                  return (
                    <TreeItem
                      className={classes.treeItem}
                      key={categorie?.id || index}
                      label={
                        <span className={classes.fontMenu}>
                          <span>{categorie.libelle}</span>{' '}
                          <span style={{ fontWeight: 'bold' }}>{nombreDocumentCategorie}</span>
                        </span>
                      }
                      nodeId={categorie?.id}
                      id={categorie?.id}
                      classes={{
                        label: classes.treeItemSelected,
                        iconContainer: classes.treeItemIconContainer,
                      }}
                    >
                      {(categorie?.mesSousCategories || []).map(
                        (sousCategorie: any, indice: number) => (
                          <MenuItem
                            style={{ marginTop: 16 }}
                            className={classNames(
                              {
                                [classes.active]: currentSousCategorie?.id === sousCategorie?.id,
                              },
                              classes.fontMenu,
                            )}
                            onClick={() => handleSousCategorieClick(sousCategorie)}
                            key={sousCategorie?.id || indice}
                          >
                            <span>{sousCategorie.libelle}</span>{' '}
                            <span style={{ fontWeight: 'bold' }}>
                              {sousCategorie.nombreDocuments}
                            </span>
                          </MenuItem>
                        ),
                      )}
                    </TreeItem>
                  );
                })}
              </TreeView>
            </TreeStructure>
          </>
        ) : (
          <ErrorPage />
        ))}
    </Box>
  );
};

export default GEDFilter;
