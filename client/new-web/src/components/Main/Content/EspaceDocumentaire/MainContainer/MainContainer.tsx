import { CustomModal } from '@app/ui-kit';
import { Box } from '@material-ui/core';
import React, { FC, useMemo } from 'react';
import { isMobile } from '../../../../../utils/Helpers';
import Content, { IContent } from '../Content/Content';
import { DocumentCategorie } from '../EspaceDocumentaire';
import Sidebar, { SideBarProps } from '../Sidebar/Sidebar';
import useStyles from './styles';

export interface MainContainerProps {
  sideBarProps: SideBarProps;
  contentProps: IContent;
}

export const MainContainer: FC<MainContainerProps> = ({ sideBarProps, contentProps }) => {
  const classes = useStyles();
  const [openModalContent, setOpenModalContent] = React.useState<boolean>(false);

  const content = useMemo(
    () => (
      <Content
        saving={contentProps.saving}
        saved={contentProps.saved}
        setSaved={contentProps.setSaved}
        activeSousCategorie={contentProps.activeSousCategorie}
        activeDocument={contentProps.activeDocument}
        mobileView={contentProps.mobileView}
        tabletView={contentProps.tabletView}
        validation
        content={contentProps.content}
        commentaires={contentProps.commentaires}
        refetchComments={contentProps.refetchComments}
        handleSidebarMenu={contentProps.handleFilterMenu}
        handleFilterMenu={contentProps.handleFilterMenu}
        onRequestCreateDocument={contentProps.onRequestCreateDocument}
        onRequestAddToFavorite={contentProps.onRequestAddToFavorite}
        onRequestDelete={contentProps.onRequestDelete}
        onRequestRemoveFromFavorite={contentProps.onRequestRemoveFromFavorite}
        onRequestReplace={contentProps.onRequestReplace}
        onRequestFetchMoreComments={contentProps.onRequestFetchMoreComments}
        onRequestLike={contentProps.onRequestLike}
        onRequestDeplace={contentProps.onRequestDeplace}
        onCompleteDownload={contentProps.onCompleteDownload}
        refetchSmyleys={contentProps.refetchSmyleys}
        hidePartage
        hideRecommandation
        refetchDocument={contentProps.refetchDocument}
        documentIds={(sideBarProps.documents.data || []).map(({ id }) => id).filter(e => e) as any}
        onRequestDocument={contentProps.onRequestDocument}
        onRequestFetchAll={contentProps.onRequestFetchAll}
        setSearchVariables={contentProps.setSearchVariables}
        searchVariables={contentProps.searchVariables}
      />
    ),
    [contentProps],
  );

  const handleShowDetails = (document: DocumentCategorie) => {
    sideBarProps.onRequestDetails(document);
    if (isMobile()) {
      setOpenModalContent(true);
    }
  };

  return (
    <Box className={classes.root}>
      <Box className={classes.listContainer}>
        <Sidebar
          onRequestDeplace={sideBarProps.onRequestDeplace}
          saving={sideBarProps.saving}
          saved={sideBarProps.saved}
          setSaved={sideBarProps.setSaved}
          searchVariables={sideBarProps.searchVariables}
          activeSousCategorie={sideBarProps.activeSousCategorie}
          setActiveDocument={sideBarProps.setActiveDocument}
          documents={sideBarProps.documents}
          onRequestAddToFavorite={sideBarProps.onRequestAddToFavorite}
          onRequestDelete={sideBarProps.onRequestDelete}
          onRequestDetails={handleShowDetails}
          onRequestRemoveFromFavorite={sideBarProps.onRequestRemoveFromFavorite}
          onRequestReplace={sideBarProps.onRequestReplace}
          onRequestScroll={sideBarProps.onRequestScroll}
          onRequestGetComments={sideBarProps.onRequestGetComments}
          setSearchVariables={sideBarProps.setSearchVariables}
          onRequestCreateDocument={sideBarProps.onRequestCreateDocument}
          onRequestRefresh={sideBarProps.onRequestRefresh}
          document={contentProps.content.data}
        />
      </Box>
      <Box className={classes.contentContainer}>{content}</Box>
      <CustomModal
        // padding="padding16"
        open={openModalContent}
        setOpen={setOpenModalContent}
        title="Espace Documentaire"
        closeIcon
        withBtnsActions={false}
        headerWithBgColor
        fullWidth
        maxWidth="md"
        fullScreen={!!isMobile()}
      >
        {content}
      </CustomModal>
    </Box>
  );
};
