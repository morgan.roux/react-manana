import React, { FC, MouseEvent } from 'react';
import { CustomModal } from '../../../../Common/CustomModal';
import useStyles from './styles';
import { Typography, CircularProgress } from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import { trimmedString } from '../../../../../utils/Helpers';
import CustomButton from '../../../../Common/CustomButton';
import { ArrowForward } from '@material-ui/icons';
import { RouteComponentProps, withRouter } from 'react-router-dom';

export interface PopupActualiteProps {
  data: any;
  total: number;
  open: boolean;
  loading: boolean;
  setOpen: (open: boolean) => void;
  setPage: (page: number) => void;
  onClickConfirm?: (event: MouseEvent<any>) => void;
  handleCloseActualite?: () => any;
}

const PopupActualite: FC<PopupActualiteProps & RouteComponentProps> = ({
  open,
  setOpen,
  data,
  total,
  setPage,
  loading,
  history: { push },
  handleCloseActualite,
}) => {
  const classes = useStyles({});

  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value - 1);
  };

  const goToActu = () => {
    if (data && data.id) push(`/actualite/${data.id}`);
  };

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title={`Il y a ${total} actualité(s) de type bloquant`}
      closeIcon={true}
      withBtnsActions={false}
      headerWithBgColor={true}
      fullWidth={true}
      maxWidth="md"
      customHandleClose={handleCloseActualite}
    >
      <div className={classes.popupActuRoot}>
        <div className={classes.actuInfoContainer}>
          <Typography className={classes.title}>
            {data.libelle}
            {loading && <CircularProgress size={20} />}
          </Typography>
          <Typography className={classes.description}>
            {trimmedString(data.description, 100)}
          </Typography>
          <Typography className={classes.origine}>
            Origine : <span>{data.origine && data.origine.libelle}</span>
          </Typography>
          <CustomButton
            className={classes.btn}
            color="secondary"
            variant="outlined"
            endIcon={<ArrowForward />}
            onClick={goToActu}
          >
            Consulter
          </CustomButton>
        </div>
        <Pagination
          count={total}
          variant="outlined"
          color="primary"
          onChange={handleChange}
          showFirstButton={true}
          showLastButton={true}
        />
      </div>
    </CustomModal>
  );
};

export default withRouter(PopupActualite);
