import { useQuery } from '@apollo/client';
import { GET_SEARCH_ACTION } from '../../../../../graphql/Action';
import { ME_me } from '../../../../../graphql/Authentication/types/ME';
import { DO_SEARCH_INFORMATION_LIAISON } from '../../../../../graphql/InformationLiaison';
import {
  SEARCH_INFORMATION_LIAISON,
  SEARCH_INFORMATION_LIAISONVariables,
} from '../../../../../graphql/InformationLiaison/types/SEARCH_INFORMATION_LIAISON';
import { getGroupement, getUser } from '../../../../../services/LocalStorage';
import { getQueryParameter } from '../../TodoNew/FilterSearch';

interface StatResult {
  liaison: {
    loading?: boolean;
    error?: Error;
    total?: number;
  };
  actionTodo: {
    loading?: boolean;
    error?: Error;
    total?: number;
  };
}

const useStats = (): StatResult => {
  const groupement = getGroupement();
  const user: ME_me = getUser();
  const userId = (user && user.id) || '';

  const loadLiaison = useQuery<SEARCH_INFORMATION_LIAISON, SEARCH_INFORMATION_LIAISONVariables>(
    DO_SEARCH_INFORMATION_LIAISON,
    {
      variables: {
        type: ['informationliaison'],
        query: {
          query: {
            bool: {
              must: [
                { term: { isRemoved: false } },
                { term: { 'groupement.id': groupement.id } },
                //{ term: { bloquant: true } },
                //{ term: { priority: 3 } }, // Bloquant
                {
                  terms: {
                    'colleguesConcernees.coupleStatutUserConcernee': [`${userId}-NON_LUES`],
                  },
                },
                ,
                //{ terms: { 'colleguesConcernees.lu': [false] } },
                {
                  script: {
                    script: {
                      source: "doc['colleguesConcernees.userConcernee.id'].length >= 1",
                      lang: 'painless',
                    },
                  },
                },
              ],
            },
          },
        },
        sortBy: [{ dateCreation: { order: 'desc' } }],
      },
      fetchPolicy: 'cache-and-network',
    },
  );

  const searchAction = useQuery<any>(GET_SEARCH_ACTION, {
    variables: {
      filterBy: [{ term: { isRemoved: false } }],
      query: getQueryParameter(userId),
    },
    fetchPolicy: 'cache-and-network',
  });

  return {
    liaison: {
      loading: loadLiaison.loading,
      error: loadLiaison.error,
      total: loadLiaison.data?.search?.total,
    },
    actionTodo: {
      loading: searchAction.loading,
      error: searchAction.error,
      total: searchAction.data?.search?.total,
    },
  };
};

export default useStats;
