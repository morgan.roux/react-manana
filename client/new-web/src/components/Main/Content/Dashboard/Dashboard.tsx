import { DashboardFilter, DashboardPage } from '@app/ui-kit';
import { CenterContentProps } from '@app/ui-kit/components/pages/DashboardPage/DashboardMain/CenterContent/CenterContent';
import { RightContentProps } from '@app/ui-kit/components/pages/DashboardPage/DashboardMain/RightContent/RightContent';
import React, { FC, useState, useMemo } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { useStatistique } from './utils/useStatistique';
import { getPharmacie, getUser } from '../../../../services/LocalStorage';
import { useRendezVous, useScoring, useImportanceUrgence, useOccurence, useComment } from './utils';
import { Item } from '@app/ui-kit/components/pages/DashboardPage/types';
import useReunion from './useReunion';
import { Comment } from './../../../Comment';
import CommentList from './../../../Comment/CommentList';
import filterAltWhite from '../../../../assets/icons/todo/filter_alt_white.svg';

const Dashboard: FC<RouteComponentProps<any, any, any>> = ({ history, location: { pathname } }) => {
  const user = getUser();
  const pharmacie = getPharmacie()

  /** ------------------------------------ USE STATE -------------------------------------------------------- */
  const [filter, setFilter] = useState<any>({
    paging: {
      offset: 0,
      limit: 12,
    },
  });

  const [dashboard, setDashboard] = useState<any>();

  /** ------------------------------------USE SCORING-------------------------------------------------------- */
  const [
    dataScoring,
    loadingScoring,
    errorScoring,
    countScoring,
    loadScoring,
    coutingScoting,
  ] = useScoring(filter, user, pharmacie);

  /*** -----------------------------------USE OCCURRENCE -----------------------------------------------------  */
  const [setFilterOccurence, dataOccurence, occurence] = useOccurence();

  /** ------------------------------------USE IMPORTANCES URGENCES -------------------------------------------- */
  const [importances, urgences] = useImportanceUrgence();

  /** ------------------------------------USE COMMENT ---------------------------------------------------------- */

  const [
    getCommentaires,
    commentsLoading,
    commentsError,
    commentaires,
    refetchComments,
  ] = useComment();

  /** ------------------------------------ USE RENDEZ-VOUS -------------------------------------------------------- */
  const [
    onRequestSearchRdv,
    onRequestSave,
    rendezVous,
    loadingRdv,
    errorRdv,
    totalRdv,
  ] = useRendezVous();

  const handleRequestGoBack = (): void => {
    history.push('/');
  };

  const { series, dashboardFilter, loadStatistique } = useStatistique();
  const statuts = {
    error: false,
    loading: false,
    data: [
      {
        id: 'NOUVEAU',
        code: 'NOUVEAU',
        libelle: 'Nouvelle',
      },
      {
        id: 'NOUVEAU',
        code: 'EN_COURS',
        libelle: 'En cours',
      },
      {
        id: 'NOUVEAU',
        code: 'CLOTURE',
        libelle: 'Clôturée',
      },
    ],
  };

  const dashboards = {
    error: errorScoring as any,
    loading: loadingScoring as any,
    data: dataScoring as any,
  };

  const handleRequestLike = (dashboard: any) => {
    console.log('like', dashboard.id);
  };
  const handleRequestShare = (dashboard: any) => {
    console.log('share', dashboard.id);
  };

  const handleCompleteChangeStatut = () => {
    occurence.refetch();
    loadScoring.refetch();
    coutingScoting.refetch();
    loadStatistique.refetch();
  };

  const { updateReunion } = useReunion(loadScoring, handleCompleteChangeStatut);
  const handleRequestCloture = (dashboard: any, commentaire: string) => {
    dashboard &&
      updateReunion({
        idItemAssocie: dashboard.idItemAssocie,
        codeStatut: 'CLOTURE',
        item: dashboard.item,
      });
  };

  const handleRequestChangeStatut = (id: string, value: string, item: Item) => {
    updateReunion({ idItemAssocie: id, codeStatut: value, item });
  };

  const handleOnRequestSearch = (filter: DashboardFilter) => {
    setFilter(filter);
    setFilterOccurence({
      date: filter.date,
      dateDebut: filter.dateDebut,
      dateFin: filter.dateFin,
      idItems: (filter.items || []).map(item => item.id),
    });
  };

  const handleGetItemAssocie = (dashboardItem: any) => {
    setDashboard(dashboardItem);
    getCommentaires({
      variables: {
        codeItem: dashboardItem.item.code,
        idItemAssocie: dashboardItem.idItemAssocie as any,
      },
    });
  };

  /*** -------------------------------------- COMPONENTS ------------------------------- */

  const ListComment = useMemo(() => {
    return (
      <CommentList
        key={`comment_list`}
        comments={commentaires?.comments}
        fetchMoreComments={refetchComments}
        loading={commentsLoading}
      />
    );
  }, [commentaires]);

  const centerContentProps: CenterContentProps = {
    rowsTotal: countScoring as number,
    series,
    statuts,
    dashboards,
    clotureSaving: false,
    onRequestLike: handleRequestLike,
    onRequestChangeStatut: handleRequestChangeStatut,
    onRequestCloture: handleRequestCloture,
    onRequestShare: handleRequestShare,
    onRequestGetItemAssocie: handleGetItemAssocie,
    commentListComponent: ListComment,
    commentComponent: (
      <Comment
        refetch={refetchComments}
        codeItem={dashboard?.item?.code || ''}
        idSource={dashboard?.idItemAssocie}
      />
    ),
  };

  const sideNavProps = {
    filters: dashboardFilter?.data as any,
    user: { fullName: user.userName } as any,
    loading: dashboardFilter?.loading as any,
    error: dashboardFilter?.error as any,
  };

  const rightContentProps: RightContentProps = {
    rendezVousProps: {
      data: rendezVous as any,
      loading: loadingRdv,
      error: errorRdv,
      nombreRdv: totalRdv,
      onRequestSearchRendezVous: onRequestSearchRdv,
      onRequestSave: onRequestSave,
    },
    importanceFilterProps: {
      urgencesImportances: {
        urgences: ((urgences.data as any) || []).map(
          (element: { id: any; libelle: any; code: any }) => ({
            id: element.id,
            libelle: element.libelle,
            code: element.code,
          }),
        ),
        importances: ((importances.data as any) || []).map(
          (element: { id: any; ordre: any; libelle: any }) => ({
            id: element.id,
            order: element.ordre,
            libelle: element.libelle,
          }),
        ),
      },
      nombreTaches: {
        hauteJournee:
          dataOccurence.find(
            (element: { importance: { ordre: number }; urgence: { code: string } }) =>
              element.importance.ordre === 1 && element.urgence.code === 'A',
          )?.occurence || 0,
        hauteSemaine:
          dataOccurence.find(
            (element: { importance: { ordre: number }; urgence: { code: string } }) =>
              element.importance.ordre === 1 && element.urgence.code === 'C',
          )?.occurence || 0,
        hauteMois:
          dataOccurence.find(
            (element: { importance: { ordre: number }; urgence: { code: string } }) =>
              element.importance.ordre === 1 && element.urgence.code === 'B',
          )?.occurence || 0,
        moyenneJournee:
          dataOccurence.find(
            (element: { importance: { ordre: number }; urgence: { code: string } }) =>
              element.importance.ordre === 2 && element.urgence.code === 'A',
          )?.occurence || 0,
        moyenneMois:
          dataOccurence.find(
            (element: { importance: { ordre: number }; urgence: { code: string } }) =>
              element.importance.ordre === 2 && element.urgence.code === 'B',
          )?.occurence || 0,
        moyenneSemaine:
          dataOccurence.find(
            (element: { importance: { ordre: number }; urgence: { code: string } }) =>
              element.importance.ordre === 2 && element.urgence.code === 'C',
          )?.occurence || 0,
      },
      loading: urgences.loading || importances.loading,
      error: urgences.error || importances.error,
    },
  };

  return (
    <DashboardPage
      centerContentProps={centerContentProps}
      rightContentProps={rightContentProps}
      sideNavProps={sideNavProps}
      onRequestSearch={handleOnRequestSearch}
      onGoBack={handleRequestGoBack}
      iconFilter={filterAltWhite}
    />
  );
};

export default withRouter(Dashboard);
