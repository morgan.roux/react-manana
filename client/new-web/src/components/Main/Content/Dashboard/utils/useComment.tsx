import { useLazyQuery } from '@apollo/client';
import { COMMENTS, COMMENTSVariables } from './../../../../../graphql/Comment/types/COMMENTS';
import { GET_COMMENTS } from './../../../../../graphql/Comment/query';

export const useComment = (): [any, boolean?, Error?, COMMENTS?, (() => void)?] => {
  const [
    getCommentaires,
    { loading: commentsLoading, error: commentsError, data: commentaires, refetch: refetchComments },
  ] = useLazyQuery<COMMENTS, COMMENTSVariables>(GET_COMMENTS);

  return [getCommentaires, commentsLoading, commentsError, commentaires, refetchComments];
};
