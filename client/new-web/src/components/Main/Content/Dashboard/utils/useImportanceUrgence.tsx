import { useEffect } from 'react';
import { useLazyQuery } from '@apollo/client';
import {
  GET_URGENCES as GET_URGENCES_TYPES,
  GET_URGENCESVariables,
} from './../../../../../federation/basis/types/GET_URGENCES';
import {
  IMPORTANCES as GET_IMPORTANCES_TYPES,
  IMPORTANCESVariables,
} from './../../../../../federation/basis/types/IMPORTANCES';
import { GET_URGENCES, GET_IMPORTANCES } from './../../../../../federation/basis/query';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { SortDirection, UrgenceSortFields } from '../../../../../types/federation-global-types';

export const useImportanceUrgence = () => {
  const [loadUrgences, loadingUrgences] = useLazyQuery<GET_URGENCES_TYPES, GET_URGENCESVariables>(
    GET_URGENCES,
    {
      client: FEDERATION_CLIENT,
      variables: {
        sorting: [{ field: UrgenceSortFields.code, direction: SortDirection.DESC }],
      },
    },
  );
  const [loadImportances, loadingImportances] = useLazyQuery<
    GET_IMPORTANCES_TYPES,
    IMPORTANCESVariables
  >(GET_IMPORTANCES, { client: FEDERATION_CLIENT });

  useEffect(() => {
    loadUrgences();
    loadImportances();
  }, []);

  const importances = {
    data: loadingImportances.data?.importances.nodes,
    error: loadingImportances.error,
    loading: loadingImportances.loading,
  };

  const urgences = {
    data: loadingUrgences.data?.urgences.nodes,
    error: loadingUrgences.error,
    loading: loadingUrgences.loading,
  };

  return [importances, urgences];
};
