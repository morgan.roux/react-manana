import {
  GET_COUNT_SCORING as GET_COUNT_SCORING_TYPES,
  GET_COUNT_SCORINGVariables,
  GET_COUNT_SCORING_itemScoringsCount,
} from './../../../../../federation/tools/itemScoring/types/GET_COUNT_SCORING';
import { GET_COUNT_SCORING } from './../../../../../federation/tools/itemScoring/query';
import { useLazyQuery } from '@apollo/client';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { useEffect, useState } from 'react';
import { QueryResult } from 'react-apollo';

interface FilterOccurence {
  date?: Date;
  dateDebut?: Date;
  dateFin?: Date;
  idItems?: string[];
}

export const useOccurence = (): [
  React.Dispatch<React.SetStateAction<FilterOccurence | undefined>>,
  GET_COUNT_SCORING_itemScoringsCount[],
  QueryResult<GET_COUNT_SCORING_TYPES, GET_COUNT_SCORINGVariables>,
  boolean,
  Error?,
] => {
  const [countScoringUser, countingScoringUser] = useLazyQuery<
    GET_COUNT_SCORING_TYPES,
    GET_COUNT_SCORINGVariables
  >(GET_COUNT_SCORING, {
    client: FEDERATION_CLIENT,
  });
  const [filter, setFilter] = useState<FilterOccurence>();

  useEffect(() => {
    countScoringUser({
      variables: {
        idItems: filter?.idItems || [],
        dateEcheance: filter?.date,
        dateDebut: filter?.dateDebut,
        dateFin: filter?.dateFin,
      },
    });
  }, [filter]);

  return [
    setFilter,
    countingScoringUser.data?.itemScoringsCount || [],
    countingScoringUser,
    countingScoringUser.loading,
    countingScoringUser.error,
  ];
};
