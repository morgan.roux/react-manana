import { useApolloClient, useMutation } from '@apollo/client';
import { FEDERATION_CLIENT } from '../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import { UPDATE_FICHE_AMELIORATION_STATUT } from '../../../../federation/demarche-qualite/fiche-amelioration/mutation';
import {
  UPDATE_FICHE_AMELIORATION_STATUT as UPDATE_FICHE_AMELIORATION_STATUT_TYPE,
  UPDATE_FICHE_AMELIORATION_STATUTVariables,
} from '../../../../federation/demarche-qualite/fiche-amelioration/types/UPDATE_FICHE_AMELIORATION_STATUT';
import { UPDATE_FICHE_INCIDENT_STATUT } from '../../../../federation/demarche-qualite/fiche-incident/mutation';
import {
  UPDATE_FICHE_INCIDENT_STATUT as UPDATE_FICHE_INCIDENT_STATUT_TYPE,
  UPDATE_FICHE_INCIDENT_STATUTVariables,
} from '../../../../federation/demarche-qualite/fiche-incident/types/UPDATE_FICHE_INCIDENT_STATUT';
import { Item } from '@app/ui-kit/components/pages/DashboardPage/types';
import { UPDATE_ACTION_OPERATIONNELLE_STATUT } from '../../../../federation/demarche-qualite/action-operationnelle/mutation';
import {
  UPDATE_ACTION_OPERATIONNELLE_STATUT as UPDATE_ACTION_OPERATIONNELLE_STATUT_TYPE,
  UPDATE_ACTION_OPERATIONNELLE_STATUTVariables,
} from '../../../../federation/demarche-qualite/action-operationnelle/types/UPDATE_ACTION_OPERATIONNELLE_STATUT';
import { UPDATE_TODO_ACTION_STATUT } from '../../../../federation/demarche-qualite/reunion/mutation';
import {
  UPDATE_TODO_ACTION_STATUT as UPDATE_TODO_ACTION_STATUT_TYPE,
  UPDATE_TODO_ACTION_STATUTVariables,
} from '../../../../federation/demarche-qualite/reunion/types/UPDATE_TODO_ACTION_STATUT';
import {
  UPDATE_INFORMATION_LIAISON as UPDATE_INFORMATION_LIAISON_TYPE,
  UPDATE_INFORMATION_LIAISONVariables,
} from '../../../../federation/basis/information-liaison/types/UPDATE_INFORMATION_LIAISON';
import { UPDATE_INFORMATION_LIAISON } from '../../../../federation/basis/information-liaison/mutation';

export interface UpdateReunionProps {
  item: Item;
  idItemAssocie: string;
  codeStatut: string;
}

const useReunion = (loading: any, onCompletChangeStatut: () => void) => {
  const client = useApolloClient();

  const [updateFicheAmeliorationStatut] = useMutation<
    UPDATE_FICHE_AMELIORATION_STATUT_TYPE,
    UPDATE_FICHE_AMELIORATION_STATUTVariables
  >(UPDATE_FICHE_AMELIORATION_STATUT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: `Le statut a été modifié avec succès !`,
        isOpen: true,
        type: 'SUCCESS',
      });
      loading && loading.refetch();
      onCompletChangeStatut();
    },
    onError: () => {
      displaySnackBar(client, {
        message:
          'Des erreurs se sont survenues pendant la modification du statut de la fiche amélioration',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [updateFicheIncidentStatut] = useMutation<
    UPDATE_FICHE_INCIDENT_STATUT_TYPE,
    UPDATE_FICHE_INCIDENT_STATUTVariables
  >(UPDATE_FICHE_INCIDENT_STATUT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: `Le statut a été modifié avec succès !`,
        isOpen: true,
        type: 'SUCCESS',
      });
      loading && loading.refetch();
      onCompletChangeStatut();
    },
    onError: () => {
      displaySnackBar(client, {
        message:
          'Des erreurs se sont survenues pendant la modification du statut de la fiche incident',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [updateActionOperationnelleStatut] = useMutation<
    UPDATE_ACTION_OPERATIONNELLE_STATUT_TYPE,
    UPDATE_ACTION_OPERATIONNELLE_STATUTVariables
  >(UPDATE_ACTION_OPERATIONNELLE_STATUT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: `Le statut a été modifié avec succès !`,
        isOpen: true,
        type: 'SUCCESS',
      });
      loading && loading.refetch();
      onCompletChangeStatut();
    },
    onError: () => {
      displaySnackBar(client, {
        message:
          "Des erreurs se sont survenues pendant la modification du statut de l'action opérationnelle",
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [updateTodoActionStatut] = useMutation<
    UPDATE_TODO_ACTION_STATUT_TYPE,
    UPDATE_TODO_ACTION_STATUTVariables
  >(UPDATE_TODO_ACTION_STATUT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: `Le statut de la To-do action a été modifié avec succès !`,
        isOpen: true,
        type: 'SUCCESS',
      });
      loading && loading.refetch();
      onCompletChangeStatut();
    },
    onError: () => {
      displaySnackBar(client, {
        message:
          'Des erreurs se sont survenues pendant la modification du statut de la To-Do Action',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [updateInformationLiaisonStatut] = useMutation<
    UPDATE_INFORMATION_LIAISON_TYPE,
    UPDATE_INFORMATION_LIAISONVariables
  >(UPDATE_INFORMATION_LIAISON, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: `Le statut de l'information de liaison a été modifié avec succès !`,
        isOpen: true,
        type: 'SUCCESS',
      });
      loading && loading.refetch();
      onCompletChangeStatut();
    },
    onError: () => {
      displaySnackBar(client, {
        message:
          "Des erreurs se sont survenues pendant la modification du statut de l'information de liaison",
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const updateReunion = ({ item, idItemAssocie, codeStatut }: UpdateReunionProps) => {
    if (item.codeItem === 'SA') {
      updateFicheAmeliorationStatut({
        variables: {
          id: idItemAssocie,
          code: codeStatut,
        },
      });
    } else if (item.codeItem === 'SB') {
      updateFicheIncidentStatut({
        variables: {
          id: idItemAssocie,
          code: codeStatut,
        },
      });
    } else if (item.codeItem === 'SC') {
      updateActionOperationnelleStatut({
        variables: {
          id: idItemAssocie,
          code: codeStatut,
        },
      });
    } else if (item.codeItem === 'L') {
      updateTodoActionStatut({
        variables: {
          id: idItemAssocie,
          code: codeStatut,
        },
      });
    } else if (item.codeItem === 'Q') {
      updateInformationLiaisonStatut({
        variables: {
          id: idItemAssocie,
          code: codeStatut,
        },
      });
    }
  };

  return { updateReunion };
};

export default useReunion;
