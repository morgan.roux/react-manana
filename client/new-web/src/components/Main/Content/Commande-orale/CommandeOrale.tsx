import React, { FC, useState } from 'react';
import { COCommandeOrale } from '@app/types';
import { useStyles } from './styles';
import {
  MinGroupeSourceAppro,
  CommandeOralePage,
  PassationOutput,
  ReceptionOutput,
  CommandeOraleOutput,
  SearchRequest,
} from '@app/ui-kit';
import {
  CREATE_COMMANDE_ORALE,
  UPDATE_COMMANDE_ORALE,
  DELETE_ONE_COMMANDE_ORALE,
} from '../../../../federation/tools/commande-orale/commande-orale/mutation';
import { GET_GROUPES_CO } from '../../../../federation/tools/commande-orale/groupe/query';
import {
  GET_GROUPES_CO as GET_GROUPES_CO_TYPES,
  GET_GROUPES_COVariables,
} from '../../../../federation/tools/commande-orale/groupe/types/GET_GROUPES_CO';
import {
  GET_COMMANDES_ORALES as GET_COMMANDES_ORALES_TYPE,
  GET_COMMANDES_ORALESVariables,
} from '../../../../federation/tools/commande-orale/commande-orale/types/GET_COMMANDES_ORALES';
import {
  GET_ROW_COMMANDES_ORALES as GET_ROW_COMMANDES_ORALES_TYPE,
  GET_ROW_COMMANDES_ORALESVariables,
} from '../../../../federation/tools/commande-orale/commande-orale/types/GET_ROW_COMMANDES_ORALES';
import {
  GET_COMMANDES_ORALES,
  GET_ROW_COMMANDES_ORALES,
} from '../../../../federation/tools/commande-orale/commande-orale/query';
import {
  CREATE_COMMANDE_ORALE as CREATE_COMMANDE_ORALE_TYPE,
  CREATE_COMMANDE_ORALEVariables,
} from '../../../../federation/tools/commande-orale/commande-orale/types/CREATE_COMMANDE_ORALE';

import {
  UPDATE_COMMANDE_ORALE as UPDATE_COMMANDE_ORALE_TYPE,
  UPDATE_COMMANDE_ORALEVariables,
} from '../../../../federation/tools/commande-orale/commande-orale/types/UPDATE_COMMANDE_ORALE';

import {
  DELETE_ONE_COMMANDE_ORALE as DELETE_ONE_COMMANDE_ORALE_TYPE,
  DELETE_ONE_COMMANDE_ORALEVariables,
} from '../../../../federation/tools/commande-orale/commande-orale/types/DELETE_ONE_COMMANDE_ORALE';

import { CREATE_PASSATION } from '../../../../federation/tools/commande-orale/passation/mutation';
import {
  CREATE_PASSATION as CREATE_PASSATION_TYPE,
  CREATE_PASSATIONVariables,
} from '../../../../federation/tools/commande-orale/passation/types/CREATE_PASSATION';

import { CREATE_RECEPTION } from '../../../../federation/tools/commande-orale/reception/mutation';
import {
  CREATE_RECEPTION as CREATE_RECEPTION_TYPE,
  CREATE_RECEPTIONVariables,
} from '../../../../federation/tools/commande-orale/reception/types/CREATE_RECEPTION';

import { CLOTURER_COMMANDE } from '../../../../federation/tools/commande-orale/commande-orale/mutation';
import {
  CLOTURER_COMMANDE as CLOTURER_COMMANDE_TYPE,
  CLOTURER_COMMANDEVariables,
} from '../../../../federation/tools/commande-orale/commande-orale/types/CLOTURER_COMMANDE';

import { useLazyQuery, useMutation } from '@apollo/client';
import { FEDERATION_CLIENT } from '../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { getPharmacie } from '../../../../services/LocalStorage';
import { COCommandeOraleFilter } from '../../../../types/federation-global-types';
import { Avatar, Box, IconButton } from '@material-ui/core';
import { PersonAdd } from '@material-ui/icons';
import AssignTaskUserModal from '../../../Common/CustomSelectUser';
import { stringToAvatar } from '../../../../utils/Helpers';
import { uniqBy } from 'lodash';
import { RouteComponentProps, withRouter } from 'react-router-dom';

interface CommandeOraleProps {
  props?: any;
}

const CommandeoralePage: FC<CommandeOraleProps & RouteComponentProps<any, any, any>> = ({
  match: { params },
  history: {
    location: { state },
    push,
    goBack,
  },
}) => {
  const classes = useStyles();

  const [assignedUsers, setAssingedUsers] = useState<any[]>([]);
  const [assignParticipant, setAssignParticipant] = React.useState(false);

  const handleUserDestinationChanges = (selected: any) => {
    setAssingedUsers(selected);
  };

  const [loadCommandesOrales, loadingCommandesOrales] = useLazyQuery<
    GET_COMMANDES_ORALES_TYPE,
    GET_COMMANDES_ORALESVariables
  >(GET_COMMANDES_ORALES, {
    client: FEDERATION_CLIENT,
  });

  const [createCommandeOrale, creationCommandeOrale] = useMutation<
    CREATE_COMMANDE_ORALE_TYPE,
    CREATE_COMMANDE_ORALEVariables
  >(CREATE_COMMANDE_ORALE, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      loadingCommandesOrales.refetch();
    },
  });

  const [deleteOneCommandeOrale, deletetingOneCommandeOrale] = useMutation<
    DELETE_ONE_COMMANDE_ORALE_TYPE,
    DELETE_ONE_COMMANDE_ORALEVariables
  >(DELETE_ONE_COMMANDE_ORALE, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      loadingCommandesOrales.refetch();
    },
  });

  const [loadRowCOCommandesOrales, loadingRowCommandesOrales] = useLazyQuery<
    GET_ROW_COMMANDES_ORALES_TYPE,
    GET_ROW_COMMANDES_ORALESVariables
  >(GET_ROW_COMMANDES_ORALES, {
    client: FEDERATION_CLIENT,
  });

  const [loadSourceAppros, loadinSourceAppros] = useLazyQuery<
    GET_GROUPES_CO_TYPES,
    GET_GROUPES_COVariables
  >(GET_GROUPES_CO, {
    client: FEDERATION_CLIENT,
  });

  const [updateCommandeOrale, updatingCommandeorale] = useMutation<
    UPDATE_COMMANDE_ORALE_TYPE,
    UPDATE_COMMANDE_ORALEVariables
  >(UPDATE_COMMANDE_ORALE, {
    client: FEDERATION_CLIENT,
  });

  const [createPassation, creationPassation] = useMutation<
    CREATE_PASSATION_TYPE,
    CREATE_PASSATIONVariables
  >(CREATE_PASSATION, { client: FEDERATION_CLIENT });

  const [createReception, creationReception] = useMutation<
    CREATE_RECEPTION_TYPE,
    CREATE_RECEPTIONVariables
  >(CREATE_RECEPTION, {
    client: FEDERATION_CLIENT,
  });

  const [cloturerCommandeOrale, clotureeCommandeOrale] = useMutation<
    CLOTURER_COMMANDE_TYPE,
    CLOTURER_COMMANDEVariables
  >(CLOTURER_COMMANDE, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      loadingCommandesOrales.refetch();
    },
  });

  const videCollaborateur = () => {
    setAssingedUsers([]);
  };

  const handleResearchRequest = ({ take, searchText, skip, filter }: SearchRequest) => {
    const filterAnd: COCommandeOraleFilter[] = [
      {
        idPharmacie: {
          eq: getPharmacie().id,
        },
      },
    ];

    if (searchText !== '') {
      filterAnd.push({
        or: [
          {
            designation: {
              iLike: `%${searchText}%`,
            },
          },
          {
            commentaire: {
              iLike: `%${searchText}%`,
            },
          },
          {
            forme: {
              iLike: `%${searchText}%`,
            },
          },
        ],
      });
    }

    if (filter !== 'all' && filter === 'new') {
      filterAnd.push({
        and: [
          {
            idPassation: {
              is: null,
            },
          },
          {
            idReception: {
              is: null,
            },
          },
          {
            cloturee: {
              is: false,
            },
          },
        ],
      });
    }

    if (filter !== 'all' && filter === 'past') {
      filterAnd.push({
        idPassation: {
          isNot: null,
        },
      });
    }

    if (filter !== 'all' && filter === 'received') {
      filterAnd.push({
        idReception: {
          isNot: null,
        },
      });
    }

    if (filter !== 'all' && filter === 'closed') {
      filterAnd.push({
        cloturee: {
          is: true,
        },
      });
    }

    loadRowCOCommandesOrales({
      variables: {
        filter: {
          and: filterAnd,
        },
      },
    });

    loadCommandesOrales({
      variables: {
        paging: {
          offset: skip,
          limit: take,
        },
        filter: {
          and: filterAnd,
        },
      },
    });
  };

  const handleRequestGroupesSourceAppro = () => {
    loadSourceAppros({
      variables: {
        filter: {
          idPharmacie: {
            eq: getPharmacie().id,
          },
        },
      },
    });
  };

  const onRequestSaveCommandeOrale = (data: CommandeOraleOutput) => {
    if (data.id) {
      if (assignedUsers[0]?.id || data.idUser) {
        updateCommandeOrale({
          variables: {
            id: data.id,
            input: {
              idUser: assignedUsers[0].id || data.idUser,
              designation: data.designation,
              forme: data.forme,
              dateHeure: data.dateHeure,
              commentaire: data.commentaire || '',
              quantite: parseInt((data.quantite as unknown) as string, 10),
              cloturee: false,
            },
          },
        });
      }
    } else {
      if (assignedUsers[0]?.id) {
        createCommandeOrale({
          variables: {
            input: {
              idUser: assignedUsers[0].id,
              designation: data.designation,
              forme: data.forme,
              dateHeure: data.dateHeure,
              commentaire: data.commentaire || '',
              quantite: parseInt((data.quantite as unknown) as string, 10),
              cloturee: false,
            },
          },
        });
      }
    }
    videCollaborateur();
  };

  const handleRequestRequestSavePassationCommande = (data: PassationOutput) => {
    if (assignedUsers[0]?.id) {
      createPassation({
        variables: {
          input: {
            idCommandeOrale: data.idCommandeOrale,
            idSourceAppro: data.idSourceAppro,
            commentaire: data.commentaire || '',
            dateHeure: data.dateHeure,
            idUser: assignedUsers[0].id,
          },
        },
      });
    }
    videCollaborateur();
  };

  const RequestSaveReceptionCommande = (data: ReceptionOutput) => {
    if (assignedUsers[0]?.id) {
      createReception({
        variables: {
          input: {
            idCommandeOrale: data.idCommandeOrale,
            commentaire: data.commentaire || '',
            idUser: assignedUsers[0].id,
            dateHeure: data.dateHeure,
          },
        },
      });
    }
    videCollaborateur();
  };

  const handleRequestCloturerCommande = (id: string) => {
    cloturerCommandeOrale({
      variables: {
        id,
      },
    });
  };

  const handleRequestDeleteCommande = (id: string) => {
    deleteOneCommandeOrale({
      variables: {
        id,
      },
    });
  };

  const handleClosePage = () => {
    goBack();
  };

  return (
    <CommandeOralePage
      onPageClose={handleClosePage}
      loading={loadingCommandesOrales.loading}
      error={loadingCommandesOrales.error}
      rowsTotal={loadingRowCommandesOrales.data?.cOCommandeOraleAggregate.count?.id || 0}
      data={
        (loadingCommandesOrales.data?.cOCommandeOrales.nodes || []).map((item: any) => ({
          commande: {
            id: item.id,
            collaborateur: item.collaborateur,
            dateHeure: item.dateHeure,
            quantite: item.quantite,
            forme: item.forme,
            designation: item.designation,
            commentaire: item.commentaire,
            cloturee: item.cloturee,
          },
          passation: item.passation,
          reception: item.reception,
        })) as COCommandeOrale[]
      }
      passation={{
        groupesSourceAppro: loadinSourceAppros.data?.cOGroupes.nodes as MinGroupeSourceAppro[],
        error: loadinSourceAppros.error || null,
        loading: loadinSourceAppros.loading,
      }}
      onRequestDeleteCommande={handleRequestDeleteCommande}
      onRequestCloturerCommande={handleRequestCloturerCommande}
      onRequestSavePassationCommande={handleRequestRequestSavePassationCommande}
      onRequestSearch={handleResearchRequest}
      onRequestSaveCommandeOrale={onRequestSaveCommandeOrale}
      onRequestGroupesSourceAppro={handleRequestGroupesSourceAppro}
      onRequestSaveReceptionCommande={RequestSaveReceptionCommande}
      collaborateurField={
        <Box>
          <Box className={classes.section}>
            <fieldset className={classes.collaborateurInput}>
              <legend>Collaborateur</legend>
              <Box className={classes.collaborateurBox} id="collaborateur-input">
                <Box display="flex" flexDirection="row" alignItems="center" paddingLeft="8px">
                  {uniqBy(assignedUsers, 'id').map((u: any) => {
                    const photo =
                      u.userPhoto && u.userPhoto.fichier && u.userPhoto.fichier.publicUrl;
                    return photo ? (
                      <img src={photo} style={{ width: 30, height: 30 }} />
                    ) : (
                      <Avatar style={{ width: 30, height: 30, fontSize: 12 }} key={u.id}>
                        {stringToAvatar(u.userName || '')}
                      </Avatar>
                    );
                  })}
                </Box>

                <IconButton onClick={() => setAssignParticipant(true)}>
                  <PersonAdd />
                </IconButton>
              </Box>
            </fieldset>
          </Box>
          <AssignTaskUserModal
            openModal={assignParticipant}
            setOpenModal={setAssignParticipant}
            selected={assignedUsers}
            setSelected={handleUserDestinationChanges}
          />
        </Box>
      }
    />
  );
};

export default withRouter(CommandeoralePage);
