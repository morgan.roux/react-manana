import {
  Box,
  CssBaseline,
  FormControlLabel,
  Hidden,
  IconButton,
  InputAdornment,
  ListItem,
  ListItemText,
  OutlinedInput,
  Radio,
  Switch,
  Tooltip,
  Typography,
} from '@material-ui/core';
import { ArrowBack, ExpandLess, ExpandMore } from '@material-ui/icons';
import ReplayIcon from '@material-ui/icons/Replay';
import SearchIcon from '@material-ui/icons/Search';
import React, { Fragment, useState } from 'react';
import { CAHIER_LIAISON_URL } from '../../../../../Constant/url';
import { isMobile } from '../../../../../utils/Helpers';
import AvatarInput from '../../../../Common/AvatarInput';
import AssignTaskUserModal from '../../../../Common/CustomSelectUser';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import DateRangePicker from '../../../../Common/DateRangePicker';
import CustomFilterContent from '../../../../Common/Filter/CustomFilterContent/CustomFilterContent';
import ImportanceFilter from '../../TodoNew/Common/ImportanceFilter/ImportanceFilter';
import UrgenceFilter from '../../TodoNew/Common/UrgenceFilter/UrgenceFilter';
import useStyles from './styles';
import { DebouncedSearchInput } from '@app/ui-kit'

interface ICahierLiaisonFilterProps {
  state: any;
  isInConcernedParticipant: any;
  currentUser: any;
  drawerTitle?: string;
  handleSearchTxt: (arg0: any) => void;
  handleStatus: (arg0: any) => void;
  handleStartDate: (arg0: any) => void;
  handleEndDate: (arg0: any) => void;
  setState: (arg0: any) => void;
  handleBack: () => void;
  handleClickMyInfos: (value: boolean) => void;
  informations: string;
  setInformations: (information: string) => void;
  urgence: any;
  setUrgence: (urgence: any) => void;
  etiquette: any;
  setEtiquette: (etiquette: any) => void;
  activateFilter: boolean;
  setActivateFilter: (activate: boolean) => void;
  countInformationLiaisons: any;
}

const CahierLiaisonFilter: React.FC<ICahierLiaisonFilterProps> = ({
  handleSearchTxt,
  handleStatus,
  handleStartDate,
  handleEndDate,
  isInConcernedParticipant,
  state,
  handleBack,
  currentUser,
  setState,
  handleClickMyInfos,
  informations,
  setInformations,
  urgence,
  setUrgence,
  etiquette,
  setEtiquette,
  activateFilter,
  setActivateFilter,
  countInformationLiaisons,
}) => {
  const classes = useStyles({});

  const currentUserId = (currentUser && currentUser.id) || '';

  const findStatutCount = (statut: string) => {
    return countInformationLiaisons?.statuts?.find(item => item.statut === statut)?.count || 0;
  };

  const informationsList = {
    emise: 'coupleStatutDeclarant',
    recue: 'colleguesConcernees.coupleStatutUserConcernee',
  };

  const statusCounts: any[] =
    informations === informationsList.emise
      ? [findStatutCount('EN_COURS'), findStatutCount('CLOTURE')]
      : [findStatutCount('NON_LUES'), findStatutCount('LUES'), findStatutCount('CLOTURE')];

  const initialStatutState =
    informations === informationsList.recue
      ? [
        {
          nom: 'Non lues',
          checked: true,
          value: 1,
          id: 'NON_LUES',
        },
        {
          nom: 'Lues',
          checked: false,
          value: 2,
          id: 'LUES',
        },
        {
          nom: 'Clôturées',
          checked: false,
          value: 3,
          id: 'CLOTURE',
        },
      ]
      : [
        {
          nom: 'En cours',
          checked: true,
          value: 1,
          id: 'EN_COURS',
        },
        {
          nom: 'Clôturées',
          checked: false,
          value: 2,
          id: 'CLOTURE',
        },
      ];

  const [statutData, setStatutData] = useState(initialStatutState);

  const [expandedMorePeriodeFilter, setExpandedMorePeriodeFilter] = useState<boolean>(true);
  const [expandedMore, setExpandedMore] = useState<boolean>(true);
  const [expandedMoreInformations, setExpandedMoreInformations] = useState<boolean>(true);
  const [expandedMoreUrgence, setExpandedMoreUrgence] = useState<boolean>(true);
  const [expandedMoreImportance, setExpandedMoreImportance] = useState<boolean>(true);
  const [focusedInput, onFocusChange] = React.useState<'startDate' | 'endDate' | null>(null);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [searchTxt, setSearchTxt] = useState('');
  const [openConcernedParticipantModal, setOpenConcernedParticipantModal] = useState<boolean>(
    false,
  );

  const [selectedCollegues, setSelectedCollegues] = useState<any[]>([currentUser]);
  const [isCheckedTeam, setIsCheckedTeam] = useState<boolean>(false);

  const handleOpenConcernedParticipant = event => {
    event.stopPropagation();
    setOpenConcernedParticipantModal(true);
  };

  const handleGetDates = (startDate, endDate) => {
    setStartDate(startDate);
    setEndDate(endDate);
    handleStartDate(startDate);
    handleEndDate(endDate);
  };

  const handleSearch = value => {
    setSearchTxt(value);
    handleSearchTxt(value);
  };

  const handleStatutData = value => {
    setStatutData(value);
    const status: any[] = [];
    value.map(v => {
      if (v.checked) status.push(v.id);
    });

    if (status && status.length > 0) handleStatus(status);
    else handleStatus([]);
  };

  // reset filters

  const handleResetFilter = () => {
    handleStatus([]);
    setStatutData(prevState => prevState.map(statut => ({ ...statut, checked: false })));
    const concernedColleagueInitialState = {
      concernedColleagueId: currentUser.id || 'all',
      concernedColleagueName:
        (isInConcernedParticipant && currentUser && currentUser.userName) || 'all',
    };

    setInformations('recues');
    setState(concernedColleagueInitialState);
    setStartDate(null);
    setEndDate(null);
    handleStartDate('');
    handleEndDate('');
    handleSearchTxt('');
    setSearchTxt('');
    handleClickMyInfos(!false);
  };

  // isCheckedTeam, selectedCollegues
  React.useMemo(() => {
    if (isCheckedTeam) {
      setState(prevState => ({
        ...prevState,
        concernedColleagueName: 'all',
        concernedColleagueId: 'all',
        concernedColleaguesIds: selectedCollegues
          .filter(sc => sc.id !== currentUserId)
          .map(i => i.id),
      }));
    } else {
      setState(prevState => ({
        ...prevState,
        concernedColleagueName: selectedCollegues
          .map(i => (i.id === currentUserId ? 'Moi' : i.userName))
          .join(', '),
        concernedColleaguesIds: selectedCollegues.map(i => i.id),
      }));
    }
  }, [isCheckedTeam, selectedCollegues]);

  React.useEffect(() => {
    handleClickMyInfos(isCheckedTeam);
  }, [isCheckedTeam]);

  React.useEffect(() => {
    if (selectedCollegues.length > 0) {
      handleClickMyInfos(true);
    }
  }, [selectedCollegues]);

  React.useEffect(() => {
    setStatutData(initialStatutState);
  }, [informations]);

  const isMe =
    selectedCollegues.length === 1 && currentUser && selectedCollegues[0].id === currentUser.id;

  const handleInformationsClick = (informations: string) => {
    window.history.pushState(
      null,
      '',
      `#${CAHIER_LIAISON_URL}/${informations === informationsList.emise ? 'emise' : 'recue'}`,
    );
    setInformations(informations);
  };

  console.log('*************************Urgence********************', urgence)

  return (
    <Fragment>
      <CssBaseline />
      <Hidden smDown={true} implementation="css">
        <Box padding="8px 0 12px" display="flex" justifyContent="space-between" alignItems="center">
          <Box display="flex" alignItems="center">
            <IconButton size="small" onClick={handleBack}>
              <ArrowBack />
            </IconButton>
            <Typography className={classes.toolbarTitle}>Cahier de liaison</Typography>
          </Box>
          {/*<IconButton size="small" onClick={handleResetFilter}>
            <Tooltip title="Réinitialiser les filtres">
              <ReplayIcon />
            </Tooltip>
          </IconButton>*/}
        </Box>
        <Box display="flex" borderBottom="1px solid #E1E1E1" paddingBottom="16px">

          <DebouncedSearchInput
              wait={300}
              value={searchTxt}
              onChange={handleSearch}
              placeholder="Rechercher une information..."
          />
          {/*<FormControlLabel
            control={
              <Switch
                size="small"
                checked={activateFilter}
                onChange={() => setActivateFilter(!activateFilter)}
              />
            }
            label="Filtré"
            labelPlacement="bottom"
          />*/}
        </Box>
      </Hidden>
      <Box>
        <Box
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          // tslint:disable-next-line: jsx-no-lambda
          onClick={() => setExpandedMoreInformations(!expandedMoreInformations)}
        >
          <Typography className={classes.fontSize14}>Informations</Typography>
          <IconButton size="small">{expandedMore ? <ExpandLess /> : <ExpandMore />}</IconButton>
        </Box>
        {expandedMoreInformations && (
          <Box display="flex" justifyContent="space-between">
            <ListItem
              role={undefined}
              dense={true}
              button={true}
              onClick={() => handleInformationsClick(informationsList.recue)}
            >
              <Box display="flex" alignItems="center">
                <Radio
                  checked={informations === informationsList.recue}
                  tabIndex={-1}
                  disableRipple={true}
                />
                <ListItemText primary="Reçue(s)" />
              </Box>
            </ListItem>
            <ListItem
              role={undefined}
              dense={true}
              button={true}
              onClick={() => handleInformationsClick(informationsList.emise)}
            >
              <Box display="flex" alignItems="center">
                <Radio
                  checked={informations === informationsList.emise}
                  tabIndex={-1}
                  disableRipple={true}
                />
                <ListItemText primary="Émise(s)" />
              </Box>
            </ListItem>
          </Box>
        )}
      </Box>

      <Box>
        <Box
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          // tslint:disable-next-line: jsx-no-lambda
          onClick={() => setExpandedMore(!expandedMore)}
        >
          <Typography className={classes.fontSize14}>Collègue(s) concerné(s)</Typography>
          <IconButton size="small">{expandedMore ? <ExpandLess /> : <ExpandMore />}</IconButton>
        </Box>
        <Box className={classes.noStyle}>
          {expandedMore &&
            (!isMobile() ? (
              <CustomFormTextField
                name="concernedParticipant"
                label=""
                value={
                  isMe
                    ? 'Moi'
                    : state.concernedColleagueName !== 'all'
                      ? state.concernedColleagueName
                      : "Toute l'équipe"
                }
                onClick={handleOpenConcernedParticipant}
              />
            ) : (
                <Box onClick={handleOpenConcernedParticipant}>
                  <AvatarInput list={selectedCollegues} />
                </Box>
              ))}
        </Box>
        <AssignTaskUserModal
          openModal={openConcernedParticipantModal}
          setOpenModal={setOpenConcernedParticipantModal}
          withNotAssigned={false}
          selected={selectedCollegues}
          setSelected={setSelectedCollegues}
          title="Collègue(s) concerné(s)"
          searchPlaceholder="Rechercher..."
          assignTeamText="Toute l'équipe"
          withAssignTeam={true}
          singleSelect={false}
          isCheckedTeam={isCheckedTeam}
          setIsCheckedTeam={setIsCheckedTeam}
        />
      </Box>
      <Box>
        <Box
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          marginBottom="8px"
          // tslint:disable-next-line: jsx-no-lambda
          onClick={() => setExpandedMorePeriodeFilter(!expandedMorePeriodeFilter)}
        >
          <Typography className={classes.fontSize14}>Intervalle de date</Typography>
          <IconButton size="small">
            {expandedMorePeriodeFilter ? <ExpandLess /> : <ExpandMore />}
          </IconButton>
        </Box>
        <Box className={classes.noStyle}>
          {expandedMorePeriodeFilter && (
            <DateRangePicker
              getDates={handleGetDates}
              startDate={startDate}
              endDate={endDate}
              startDatePlaceholderText="Du"
              endDatePlaceholderText="Au"
              label="Période"
              variant="outlined"
              numberOfMonths={1}
              startDateId="startCreationDateUniqueId"
              endDateId="endCreationDateUniqueId"
              showDefaultInputIcon={false}
              // tslint:disable-next-line: jsx-no-lambda
              isOutsideRange={() => false}
              focusedInput={focusedInput}
              onFocusChange={onFocusChange}
            />
          )}
        </Box>
      </Box>

      <CustomFilterContent
        title="Statut"
        datas={statutData}
        updateDatas={handleStatutData}
        counts={statusCounts}
      />
      <Box>
        <Box
          paddingTop={2}
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          onClick={() => setExpandedMoreUrgence(!expandedMoreUrgence)}
        >
          <Typography className={classes.fontSize14}>Urgence</Typography>
          <IconButton size="small">
            {expandedMoreUrgence ? <ExpandLess /> : <ExpandMore />}
          </IconButton>
        </Box>
        {expandedMoreUrgence && (
          <Box className={classes.noStyle}>
            <UrgenceFilter withDropdown={false} urgence={urgence} setUrgence={setUrgence} />
          </Box>
        )}
      </Box>

      <Box>
        <Box
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          onClick={() => setExpandedMoreImportance(!expandedMoreImportance)}
        >
          <Typography className={classes.fontSize14}>Importance</Typography>
          <IconButton size="small">
            {expandedMoreImportance ? <ExpandLess /> : <ExpandMore />}
          </IconButton>
        </Box>
        {expandedMoreImportance && (
          <Box className={classes.noStyle}>
            <ImportanceFilter
              withDropdown={false}
              importances={etiquette}
              setImportances={setEtiquette}
            />
          </Box>
        )}
      </Box>
    </Fragment>
  );
};

export default CahierLiaisonFilter;
