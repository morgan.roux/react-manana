import { Typography } from '@material-ui/core';
import classnames from 'classnames';
import React, { Fragment } from 'react';
import { ME_me } from '../../../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../../../services/LocalStorage';
import { HtmlTooltip } from '../../../../../Common/Tooltip';
import useStyles from '../../PartageIdeeBonnePratique/PartageIdeeBonnePratiqueMain/LeftContainer/Item/styles';
import useOthersStyles from './styles';

interface ColleguesConcerneesProps {
  info: any;
}

const ColleguesConcernees: React.FC<ColleguesConcerneesProps> = ({ info }) => {
  const classes = useStyles();
  const othersClasses = useOthersStyles();

  const currentUser: ME_me = getUser();
  const currentUserId = currentUser && currentUser.id;
  const currentUserName = currentUser && currentUser.userName;

  const colleguesConcerneesIds: string[] =
    (info &&
      info.colleguesConcernees &&
      info.colleguesConcernees.length > 0 &&
      info.colleguesConcernees.map(i => i.userConcernee.id)) ||
    [];

  const colleguesConcerneesNames: string[] =
    (info &&
      info.colleguesConcernees &&
      info.colleguesConcernees.length > 0 &&
      info.colleguesConcernees.map(i => i.userConcernee.userName)) ||
    [];

  const isCollegueConcerned: boolean = colleguesConcerneesIds.includes(currentUserId);

  const defaultCollegueConcerneeName = isCollegueConcerned
    ? currentUserName
    : colleguesConcerneesNames[0];
  const othersColleguesConcerneesNames: string[] = colleguesConcerneesNames.filter(
    i => i !== defaultCollegueConcerneeName,
  );

  return defaultCollegueConcerneeName && othersColleguesConcerneesNames.length === 0 ? (
    <Typography
      className={classnames(
        classes.labelContentValue,
        classes.lenghtValueName,
        othersClasses.collegueConcerneeName,
        othersClasses.infoValue,
      )}
    >
      {defaultCollegueConcerneeName === currentUserName ? 'Moi' : defaultCollegueConcerneeName}
    </Typography>
  ) : defaultCollegueConcerneeName && othersColleguesConcerneesNames.length > 0 ? (
    <HtmlTooltip
      arrow={true}
      title={
        <Fragment>
          {[defaultCollegueConcerneeName, ...othersColleguesConcerneesNames].map((name, index) => {
            return (
              <Fragment key={index}>
                {name === currentUserName ? `${name} (Moi)` : name}
                <br />
              </Fragment>
            );
          })}
        </Fragment>
      }
    >
      <Typography
        className={classnames(
          classes.labelContentValue,
          classes.lenghtValueName,
          othersClasses.collegueConcerneeName,
          othersClasses.infoValue,
        )}
      >
        {`${
          defaultCollegueConcerneeName === currentUserName ? 'Moi' : defaultCollegueConcerneeName
        } et ${othersColleguesConcerneesNames.length} autre(s) personne(s)`}
      </Typography>
    </HtmlTooltip>
  ) : (
    <span>{'-'}</span>
  );
};

export default ColleguesConcernees;
