import {
  Fade,
  IconButton,
  ListItemIcon,
  Menu,
  MenuItem,
  SvgIcon,
  Typography,
} from '@material-ui/core';
import { SvgIconProps } from '@material-ui/core/SvgIcon';
import { Delete, Edit, MoreHoriz, Visibility } from '@material-ui/icons';
import React, { FC, MouseEvent, ReactNode, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { CAHIER_LIAISON_URL } from '../../../../../../Constant/url';
import {
  FichierInput,
  InformationLiaisonInput,
} from '../../../../../../types/graphql-global-types';
import { ConfirmDeleteDialog } from '../../../../../Common/ConfirmDialog';
import useCreateUpdateCahierLiaison from './useCreateUpdateCahierLiaison';
import { removeEmptyString } from './utils';

export interface TableActionColumnMenu {
  label: any;
  icon: ReactNode;
  disabled: boolean;
  onClick: (event: any, row: any) => void;
}
export interface TableActionColumnProps {
  row: any;
  baseUrl: string;
  setOpenDeleteDialog?: React.Dispatch<React.SetStateAction<boolean>>;
  withViewDetails?: boolean;
  showResendEmail?: boolean;
  customMenuItems?: TableActionColumnMenu[];
  handleClickDelete?: (row: any) => void;
  handleClickAffectation?: (action: any, row: any) => void;
}

export const ArretSansRemplacementIcon = (props: SvgIconProps) => {
  return (
    <SvgIcon {...props} viewBox="0 0 24 24">
      <g id="delete-24px" width="24" height="24" viewBox="0 0 24 24">
        <path
          id="Tracé_2876"
          data-name="Tracé 2876"
          d="M206.063,71.652h0a.968.968,0,0,1,.978.944.289.289,0,0,0,.292.282h7.4a.288.288,0,0,0,.293-.28V62.641a.289.289,0,0,0-.292-.281h-7.4a.289.289,0,0,0-.293.281.981.981,0,0,1-1.961,0,2.216,2.216,0,0,1,2.253-2.175h7.4a2.217,2.217,0,0,1,2.245,2.17V72.6a2.214,2.214,0,0,1-2.247,2.17h-7.4a2.216,2.216,0,0,1-2.253-2.17A.968.968,0,0,1,206.063,71.652Z"
          transform="translate(-200.243 -60.467)"
          fill="#424242"
        />
        <path
          id="Tracé_2877"
          data-name="Tracé 2877"
          d="M220.179,66.837l-3.032-3.479h0a.816.816,0,0,0-.6-.291h-.013a.8.8,0,0,0-.606.288,1.106,1.106,0,0,0,0,1.419l1.64,1.854h-8.092a1.005,1.005,0,0,0,0,1.991h8.119l-1.606,1.852A1.125,1.125,0,0,0,216,71.895a.832.832,0,0,0,.594.271h.013a.8.8,0,0,0,.606-.291L220.1,68.56A1.354,1.354,0,0,0,220.179,66.837Z"
          transform="translate(-208.604 -60.467)"
          fill="#424242"
        />
      </g>
    </SvgIcon>
  );
};

export const AttributionDepartementIcon = (props: SvgIconProps) => {
  const { color } = props;
  return (
    <SvgIcon {...props} viewBox="0 0 24 24">
      <g width="18.68" height="17" viewBox="0 0 18.68 17">
        <g id="Groupe_13167" data-name="Groupe 13167" transform="translate(-177.231 -57.446)">
          <path
            id="Icon_material-person-pin-circle"
            data-name="Icon material-person-pin-circle"
            d="M183.881,57.446a6.657,6.657,0,0,0-6.65,6.65c0,4.987,6.65,10.35,6.65,10.35s6.65-5.363,6.65-10.35A6.657,6.657,0,0,0,183.881,57.446Zm0,1.9a1.9,1.9,0,1,1-1.9,1.9h0A1.908,1.908,0,0,1,183.881,59.346Zm0,9.5a4.541,4.541,0,0,1-3.8-2.043c.019-1.257,2.537-1.947,3.8-1.947s3.781.694,3.8,1.948A4.541,4.541,0,0,1,183.881,68.846Z"
            fill={color ? color : '#424242'}
          />
          <path
            id="Tracé_2878"
            data-name="Tracé 2878"
            d="M195.437,59.5h-1.581V57.92a.473.473,0,0,0-.473-.474h-.645a.474.474,0,0,0-.473.474V59.5h-1.581a.473.473,0,0,0-.473.473v.645a.473.473,0,0,0,.473.473h1.581v1.581a.473.473,0,0,0,.473.473h.645a.472.472,0,0,0,.473-.473V61.092h1.581a.474.474,0,0,0,.474-.473v-.645A.473.473,0,0,0,195.437,59.5Z"
            fill={color ? color : '#424242'}
          />
        </g>
      </g>
    </SvgIcon>
  );
};

const TableActionColumn: FC<TableActionColumnProps & RouteComponentProps> = ({
  row,
  baseUrl,
  setOpenDeleteDialog,
  handleClickDelete,
  history: { push },
  customMenuItems,
}) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    event.preventDefault();
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const onClickDelete = (event: MouseEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    setOpenDeleteDg(true);
  };

  const goToEdit = () => {
    handleClose();
    push(`${CAHIER_LIAISON_URL}/edit/${row.id}`);
  };

  const handleClickShowDetails = () => {
    push(`${CAHIER_LIAISON_URL}/viewdetails/${row.id}`);
  };

  const basicMenu: TableActionColumnMenu[] = [
    { label: 'Modifier', icon: <Edit />, onClick: goToEdit, disabled: row.statut === 'CLOTURE' },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: onClickDelete,
      disabled: false,
    },
    {
      label: 'Voir les détails',
      icon: <Visibility />,
      onClick: handleClickShowDetails,
      disabled: false,
    },
  ];

  const menuItems: TableActionColumnMenu[] = basicMenu;

  const [openDeleteDg, setOpenDeleteDg] = useState<boolean>(false);

  const DeleteDialogContent = () => {
    return <span>Êtes-vous sur de vouloir supprimer ?</span>;
  };

  const idColleguesConcernees: string[] =
    (row &&
      row.colleguesConcernees &&
      row.colleguesConcernees.length > 0 &&
      row.colleguesConcernees.map(i => i.userConcernee.id)) ||
    [];

  let fichiers: FichierInput[] = [];
  if (row.fichiersJoints && row.fichiersJoints.length > 0) {
    fichiers = row.fichiersJoints.map(fichierJoint => {
      const fichierInput: FichierInput = {
        id: fichierJoint.fichier.id,
        chemin: fichierJoint.fichier.chemin,
        nomOriginal: fichierJoint.fichier.nomOriginal,
        type: fichierJoint.fichier.type,
      };
      return fichierInput;
    });
  }

  const [isRemoved, setIsRemoved] = useState(false);

  const input: InformationLiaisonInput = {
    id: row.id,
    idType: row.idType,
    idItem: row.idItem,
    idItemAssocie: row.idItemAssocie,
    idOrigine: row.idOrigine,
    idOrigineAssocie: row.idOrigineAssocie,
    statut: row.statut,
    titre: row.titre,
    description: row.description,
    idDeclarant: row.declarant && row.declarant.id,
    idColleguesConcernees,
    idFicheIncident: row.idFicheIncident,
    idFicheAmelioration: row.idFicheAmelioration,
    idFicheReclamation: row.idFicheReclamation,
    idTodoAction: row.idTodoAction,
    codeMaj: row.codeMaj,
    isRemoved,
    bloquant: row.bloquant,
    fichiers,
    idImportance: row?.importance?.id,
    idUrgence: row?.urgence?.id,
    idFonction: row?.fonction?.id,
    idTache: row?.tache?.id,
  };
  

  const { createUpdateCahierLiaison, mutationSuccess } = useCreateUpdateCahierLiaison({ input:removeEmptyString(input) });

  const onClickConfirmDelete = () => {
    setIsRemoved(true);
  };

  useEffect(() => {
    setIsRemoved(isRemoved);
    if (isRemoved) createUpdateCahierLiaison();
  }, [isRemoved]);

  return (
    <div>
      <ConfirmDeleteDialog
        open={openDeleteDg}
        setOpen={setOpenDeleteDg}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
      <IconButton aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        <MoreHoriz />
      </IconButton>
      <Menu
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        {customMenuItems
          ? customMenuItems.map((i: TableActionColumnMenu, index: number) => (
            <MenuItem
              // tslint:disable-next-line: jsx-no-lambda
              onClick={event => {
                handleClose();
                i.onClick(event, row);
              }}
              key={`table_menu_item_${index}`}
              disabled={i.disabled}
            >
              <ListItemIcon>{i.icon}</ListItemIcon>
              <Typography variant="inherit">{i.label}</Typography>
            </MenuItem>
          ))
          : menuItems &&
          menuItems.length > 0 &&
          menuItems.map((i: TableActionColumnMenu, index: number) => (
            <MenuItem
              // tslint:disable-next-line: jsx-no-lambda
              onClick={event => {
                handleClose();
                i.onClick(event, row);
              }}
              key={`table_menu_item_${index}`}
              disabled={i.disabled}
            >
              <ListItemIcon>{i.icon}</ListItemIcon>
              <Typography variant="inherit">{i.label}</Typography>
            </MenuItem>
          ))}
      </Menu>
    </div>
  );
};

export default withRouter(TableActionColumn);
