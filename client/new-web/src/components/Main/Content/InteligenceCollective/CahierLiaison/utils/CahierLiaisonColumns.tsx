import moment from 'moment';
import React from 'react';
import { CAHIER_LIAISON_URL } from '../../../../../../Constant/url';
import { ME_me } from '../../../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../../../services/LocalStorage';
import TableActionColumn from './TableActionColumn';
import TableUpdateStateColumn from './TableUpdateStateColumn';
// import useStyles from '../styles';

export const CahierLiaisonColumns = () => {
  const currentUser: ME_me = getUser();
  const currentUserId = (currentUser && currentUser.id) || '';

  const type = {
    INCIDENT: 'Incident',
    AMELIORATION: 'Amélioration',
    RECLAMATION: 'Réclamation',
    ACTUALITE: 'Actualité',
  };

  return [
    {
      name: 'typeInfo',
      label: 'Type',
      renderer: (value: any) => {
        return type[value.typeInfo];
      },
    },
    {
      name: 'dateCreation',
      label: 'Date de création',
      renderer: (value: any) => {
        return value.dateCreation ? moment(value.dateCreation).format('DD/MM/YYYY') : '-';
      },
    },
    {
      name: 'bloquant',
      label: 'Bloquante',
      renderer: (value: any) => {
        return value.bloquant === true ? 'Oui' : value.bloquant === false ? 'Non' : '-';
      },
    },
    {
      name: 'description',
      label: 'Détails',
      renderer: (value: any) => {
        return value.description ? value.description : '-';
      },
    },
    {
      name: 'declarant.userName',
      label: 'Déclarant',
      renderer: (value: any) => {
        if (value.declarant && value.declarant.id && value.declarant.userName) {
          return value.declarant.id === currentUserId ? 'Moi' : value.declarant.userName;
        } else {
          return '-';
        }
      },
    },
    {
      name: 'colleguesConcernees.userConcernee.userName',
      label: 'Collègue(s) concernée(s)',
      renderer: (value: any) => {
        const colleguesConcerneesNames: string[] =
          (value &&
            value.colleguesConcernees &&
            value.colleguesConcernees.length > 0 &&
            value.colleguesConcernees.map(i => i.userConcernee.userName)) ||
          [];
        if (colleguesConcerneesNames.length > 0) {
          return colleguesConcerneesNames.join(', ');
        } else {
          return '-';
        }
      },
    },
    {
      name: 'idFicheIncident',
      label: "Fiche d'incident",
      renderer: (value: any) => {
        return value.idFicheIncident && value.idFicheIncident !== '' ? 'Oui' : 'Non';
      },
    },
    {
      name: 'idFicheAmelioration',
      label: "Fiche d'amélioration",
      renderer: (value: any) => {
        return value.idFicheAmelioration && value.idFicheAmelioration !== '' ? 'Oui' : 'Non';
      },
    },
    {
      name: 'idFicheReclamation',
      label: 'Fiche de réclamation',
      renderer: (value: any) => {
        return value.ficheReclamation && value.ficheReclamation !== null ? 'Oui' : 'Non';
      },
    },
    {
      name: 'idTodoAction',
      label: 'Action todo',
      renderer: (value: any) => {
        return value.todoAction && value.todoAction !== null ? 'Oui' : 'Non';
      },
    },
    {
      name: 'statut',
      label: 'Statut',
      renderer: (value: any) => {
        return (
          <TableUpdateStateColumn
            row={value}
            baseUrl={CAHIER_LIAISON_URL}
            setOpenDeleteDialog={() => {}}
            withViewDetails={true}
            handleClickDelete={() => console.log('click delete')}
          />
        );
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <TableActionColumn
            row={value}
            baseUrl={CAHIER_LIAISON_URL}
            setOpenDeleteDialog={() => {}}
            withViewDetails={true}
            handleClickDelete={() => console.log('click delete')}
          />
        );
      },
    },
  ];
};
export default CahierLiaisonColumns;
