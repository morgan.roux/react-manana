import { useApolloClient, useMutation, useQuery } from '@apollo/client';
import { Box, Divider, Paper, TextField, Typography } from '@material-ui/core';
import { PictureAsPdf } from '@material-ui/icons';
import { AxiosResponse } from 'axios';
import { uniqBy } from 'lodash';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import Image from '../../../../../../assets/img/mutation-success.png';
import { GET_INFORMATION_LIAISON } from '../../../../../../graphql/CahierLiaison';
import { CREATE_UPDATE_INFORMATION_LIAISON } from '../../../../../../graphql/CahierLiaison/types/CREATE_UPDATE_INFORMATION_LIAISON';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { GET_INFORMATION_LIAISON_TYPES } from '../../../../../../federation/basis/information-liaison/query';
import {
  INFORMATION_LIAISON_TYPES,
  INFORMATION_LIAISON_TYPESVariables,
} from '../../../../../../federation/basis/information-liaison/types/INFORMATION_LIAISON_TYPES';
import {
  ORIGINES as ORIGINES_TYPES,
  ORIGINESVariables,
} from '../../../../../../federation/basis/origine/types/ORIGINES';
import { ORIGINES } from '../../../../../../federation/basis/origine/query';
import SnackVariableInterface from '../../../../../../Interface/SnackVariableInterface';
import { uploadToS3 } from '../../../../../../services/S3';
import {
  FichierInput,
  InformationLiaisonInput,
  TakeChargeInformationLiaisonInput,
  TypeFichier,
} from '../../../../../../types/graphql-global-types';
import { formatFilenameWithoutDate } from '../../../../../../utils/filenameFormater';
import { formatBytes } from '../../../../../../utils/Helpers';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import Backdrop from '../../../../../Common/Backdrop';
import { ConfirmDeleteDialog } from '../../../../../Common/ConfirmDialog';
import CustomButton from '../../../../../Common/CustomButton';
import SubToolbar from '../../../../../Common/newCustomContent/SubToolbar';
import NoItemContentImage from '../../../../../Common/NoItemContentImage';
import useCommonStyles from '../../../../Content/Messagerie/SideBar/FormMessage/styles';
import { useCreateUpdateCahierLiaison } from '../utils';
import useTakeChargeInformationLiaison from '../utils/useTakeChargeInformationLiaison';
import PriseEnCharge from './Modals/PriseEnCharge';
import { AppAuthorization } from './../../../../../../services/authorization';

import useStyles from './styles';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { removeEmptyString } from '../utils/utils';

interface InfoProps {
  info?: any;
}

const DetailsInfo: FC<InfoProps & RouteComponentProps> = ({
  // info,
  match: { params },
  history: { push, goBack },
}) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const { idliaison, from } = params as any;

  const handleGoBack = () => {
    if (from === 'partage-info') {
      goBack();
    } else {
      push('/intelligence-collective/cahier-de-liaison');
    }
  };

  const subToolbarTitle = "Détails d'une information";
  const [openModal, setopenModal] = useState<boolean>(false);

  const informationLiaisonTypesQuery = useQuery<
    INFORMATION_LIAISON_TYPES,
    INFORMATION_LIAISON_TYPESVariables
  >(GET_INFORMATION_LIAISON_TYPES, {
    client: FEDERATION_CLIENT
  });


  const originesQuery = useQuery<
    ORIGINES_TYPES,
    ORIGINESVariables
  >(ORIGINES, {
    client: FEDERATION_CLIENT
  });


  const informationLiaisonTypes = informationLiaisonTypesQuery.data?.informationLiaisonTypes.nodes || [];
  const origines = originesQuery.data?.origines.nodes || [];

  const infoliaisonQuery = useQuery<any & CREATE_UPDATE_INFORMATION_LIAISON>(
    GET_INFORMATION_LIAISON,
    {
      variables: { id: idliaison || '' },
    },
  );

  const client = useApolloClient();

  const infoResult =
    infoliaisonQuery && infoliaisonQuery.data && infoliaisonQuery.data.informationLiaison;

  const nomDeclarant = (infoResult && infoResult.declarant && infoResult.declarant.userName) || '-';
  const type = (infoResult?.idType && informationLiaisonTypes.find(({ id }) => id === infoResult.idType)?.libelle) || '-'
  const origine = (infoResult?.idOrigine && origines.find(({ id }) => id === infoResult.idOrigine)?.libelle) || '-'
  const correspondant = infoResult?.origineAssocie ?
    (infoResult.origineAssocie as any).userName
    || (infoResult.origineAssocie as any).nomLabo
    || (infoResult.origineAssocie as any).nom : ''

  const statusInfo = (infoResult && infoResult.statut) || '-';
  const bloquantInfo = (infoResult && infoResult.bloquant === true ? 'Oui' : 'Non') || '-';

  const ficheIncidentInfo = infoResult && infoResult.idFicheIncident;
  const ficheAmeliorationInfo = infoResult && infoResult.idFicheAmelioration;
  const ficheReclamationInfo = infoResult && infoResult.ficheReclamation;
  const todoActionInfo = infoResult && infoResult.todoAction;
  const descriptionInfo = (infoResult && infoResult.description) || '-';
  const dateCreationInfo = (infoResult && infoResult.dateCreation) || '-';
  const hasPrisEnCharge = infoResult && infoResult.prisEnCharge;
  const descriptionPrisEnCharge = hasPrisEnCharge ? infoResult.prisEnCharge.description : '-';
  const datePrisEnCharge = hasPrisEnCharge ? infoResult.prisEnCharge.date : '-';
  const isRemoved = (infoResult && infoResult.isRemoved) || false;
  const fichiersJoints =
    (hasPrisEnCharge &&
      infoResult.prisEnCharge.fichiersJoints &&
      infoResult.prisEnCharge.fichiersJoints.length > 0 &&
      infoResult.prisEnCharge.fichiersJoints) ||
    [];
  const id = (infoResult && infoResult.id) || '';
  const [loading, setLoading] = useState<boolean>(false);
  const goToEdit = () => {
    push('/intelligence-collective/cahier-de-liaison/edit/' + id);
  };

  const [selectStatueValue, setSelectStatueValue] = useState(statusInfo);
  const [oldFiles, setOldFiles] = useState<FichierInput[]>([]);
  useEffect(() => {
    setSelectStatueValue(selectStatueValue);
  }, [selectStatueValue]);


  const statut = {
    NON_LUES: 'Non lues',
    LUES: 'Lues',
    CLOTURE: 'Clôturée',
  };

  const DividerComponent: FC<{ marginTop?: number; marginBottom?: number }> = ({
    marginTop,
    marginBottom,
  }) => {
    return (
      <Box style={{ marginTop, marginBottom }}>
        <Divider />
      </Box>
    );
  };

  const InfoTitle: FC<{ label: string }> = ({ label }) => {
    return <Typography className={classes.infoTitle}>{label}</Typography>;
  };
  const InfoTitleContent: FC<{ label: string }> = ({ label }) => {
    return <Typography className={classes.infoTitleContent}>{label}</Typography>;
  };

  const InfoRow: FC<{ label: string; value: string }> = ({ label, value }) => {
    return (
      <Box className={classes.infoRowContainer}>
        <Typography className={classes.infoRowLabel}>{label} : </Typography>
        <Typography className={classes.infoRowValue}>{value}</Typography>
      </Box>
    );
  };

  const idColleguesConcernees: string[] =
    (infoResult &&
      infoResult.colleguesConcernees &&
      infoResult.colleguesConcernees.length > 0 &&
      infoResult.colleguesConcernees.map(i => i.userConcernee.id)) ||
    [];

  let fichiers: FichierInput[] = [];
  if (infoResult.fichiersJoints && infoResult.fichiersJoints.length > 0) {
    fichiers = infoResult.fichiersJoints.map(fichierJoint => {
      const fichierInput: FichierInput = {
        id: fichierJoint.fichier.id,
        chemin: fichierJoint.fichier.chemin,
        nomOriginal: fichierJoint.fichier.nomOriginal,
        type: fichierJoint.fichier.type,
      };
      return fichierInput;
    });
  }

  const input: InformationLiaisonInput = {
    id: infoResult && infoResult.id,
    idType: infoResult && infoResult.idType,
    idItem: infoResult.idItem,
    idItemAssocie: infoResult.idItemAssocie,
    idOrigine: infoResult && infoResult.idOrigine,
    idOrigineAssocie: infoResult && infoResult.idOrigineAssocie,
    statut: statusInfo,
    titre: infoResult.titre,
    description: infoResult && infoResult.description,
    idDeclarant: infoResult && infoResult.declarant.id,
    idColleguesConcernees,
    idFicheIncident: infoResult && infoResult.idFicheIncident,
    idFicheAmelioration: infoResult && infoResult.idFicheAmelioration,
    idFicheReclamation: infoResult && infoResult.idFicheReclamation,
    idTodoAction: infoResult && infoResult.idTodoAction,
    codeMaj: infoResult && infoResult.codeMaj,
    isRemoved,
    fichiers,
    idImportance: infoResult?.importance?.id,
    idUrgence: infoResult?.urgence?.id,
    idTache: infoResult?.tache?.id,
    idFonction: infoResult?.fonction?.id,
  };

  const {
    createUpdateCahierLiaison,
    mutationLoading,
    mutationSuccess,
  } = useCreateUpdateCahierLiaison({ input: removeEmptyString(input) });

  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);
  let files: FichierInput[] = [];
  let uploadResult: AxiosResponse<any> | null = null;

  const [stateTakeCharge, setStateTakeCharge] = useState({
    date: new Date(),
    description: '',
    files,
  });

  const takeCharge: TakeChargeInformationLiaisonInput = {
    id: infoResult && infoResult.id,
    date: stateTakeCharge.date,
    description: stateTakeCharge.description,
    files: stateTakeCharge.files,
  };

  const {
    takeChargeInformationLiaison,
    mutationTakeChargeSuccess,
  } = useTakeChargeInformationLiaison(takeCharge);

  const [uploadFiles, uploadFilesResult] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (data && data.createPutPresignedUrls && data.createPutPresignedUrls.length > 0) {
        Promise.all(
          data.createPutPresignedUrls.map(async (presigned, index) => {
            if (presigned && selectedFiles.length > 0) {
              await Promise.all(
                selectedFiles.map(async file => {
                  if (presigned.filePath.includes(formatFilenameWithoutDate(file))) {
                    const type: TypeFichier =
                      file.type && file.type.includes('pdf')
                        ? TypeFichier.PDF
                        : file.type.includes('xlsx')
                          ? TypeFichier.EXCEL
                          : TypeFichier.PHOTO;
                    const newFile: FichierInput = {
                      id: '',
                      type,
                      nomOriginal: file.name,
                      chemin: presigned.filePath,
                    };

                    files = uniqBy([...files, newFile], 'chemin');

                    setStateTakeCharge(prevState => ({ ...prevState, ['files']: files }));

                    await uploadToS3(selectedFiles[index], presigned.presignedUrl)
                      .then(result => {
                        if (result && result.status === 200) {
                          uploadResult = result;
                        }
                      })
                      .catch(error => {
                        setLoading(false);
                        console.log('error :>> ', error);
                        displaySnackBar(client, {
                          type: 'ERROR',
                          message: "Erreur lors de l'envoye de(s) fichier(s)",
                          isOpen: true,
                        });
                      });
                  }
                }),
              );
            }
          }),
        ).then(_ => {
          // Execute doCreateUpdate
          if ((uploadResult && uploadResult.status === 200) || files) {
            takeChargeInformationLiaison();
            setLoading(false);
          } else {
            setLoading(false);
          }
        });
      } else {
        takeChargeInformationLiaison();
      }
    },
    onError: errors => {
      setLoading(false);
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const handleCharge = () => {
    setopenModal(!openModal);
  };

  const deleteLiaisonInformation = () => {
    setOpenDeleteDialog(true);
  };

  const onClickConfirmDelete = () => {
    createUpdateCahierLiaison();
  };

  const DeleteDialogContent = () => {
    return <span>Êtes-vous sur de vouloir supprimer ?</span>;
  };

  const handleAddClickTakeCharge = () => {
    setLoading(true);
    if (selectedFiles && selectedFiles.length > 0) {
      // Presigned file
      const filePathsTab: string[] = [];
      selectedFiles.map((file: File) => {
        if (file.type) {
          filePathsTab.push(
            `priseencharge/${infoResult && infoResult.id}/${formatFilenameWithoutDate(file)}`,
          );
        }
      });
      // Create presigned url
      uploadFiles({ variables: { filePaths: filePathsTab } });
    } else {
      takeChargeInformationLiaison();
      // setLoading(false);
    }
    setSelectStatueValue('CLOTURE');
  };

  React.useMemo(() => {
    if (mutationTakeChargeSuccess) {
      setLoading(false);
    }
  }, [mutationTakeChargeSuccess]);

  const openFile = (url: string) => {
    window.open(url, '_blank');
  };

  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  if (mutationSuccess || isRemoved) handleGoBack();

  return (
    <Box>
      {(loading || uploadFilesResult.loading) && <Backdrop value={'Opération en cours...'} />}

      <SubToolbar
        title={subToolbarTitle}
        dark={true}
        withBackBtn={true}
        onClickBack={handleGoBack}
      />

      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
      <PriseEnCharge
        open={openModal}
        setSelectedFiles={setSelectedFiles}
        selectedFiles={selectedFiles}
        stateTakeCharge={stateTakeCharge}
        setStateTakeCharge={setStateTakeCharge}
        setOpen={setopenModal}
        handleAddClickTakeCharge={handleAddClickTakeCharge}
      />
      <Box className={classes.rootContainer}>
        <Paper elevation={3} className={classes.infoContainerLeft}>
          <InfoTitle label={'Information de liaison'} />
          <InfoRow label="Déclarant" value={nomDeclarant} />
          <InfoRow label="Type" value={type} />
          <InfoRow label="Origine" value={origine} />
          <InfoRow label="Correspondant" value={correspondant} />
          <InfoRow label="Bloquante" value={bloquantInfo} />
          <InfoRow label="Date de création" value={moment(dateCreationInfo).format('DD/MM/YYYY')} />
          <InfoRow label="Statut d'information" value={statut[statusInfo]} />
          <DividerComponent marginTop={24} marginBottom={16} />
          <InfoTitle label="Statut" />
          <InfoRow label="Fiche d'incident" value={ficheIncidentInfo ? 'Oui' : 'Non' || '-'} />
          <InfoRow
            label="Fiche d'amélioration"
            value={ficheAmeliorationInfo ? 'Oui' : 'Non' || '-'}
          />
          <InfoRow
            label="Fiche de réclamation"
            value={ficheReclamationInfo ? 'Oui' : 'Non' || '-'}
          />
          <InfoRow label="Action todo" value={todoActionInfo ? 'Oui' : 'Non' || '-'} />
          <DividerComponent marginTop={30} marginBottom={24} />
          <Box>
            <InfoTitle label="Détails" />

            <TextField
              id="outlined-textarea"
              label="Description"
              placeholder="Placeholder"
              multiline={true}
              variant="outlined"
              rows={4}
              className={classes.infoCommentsContainer}
              value={descriptionInfo}
            />
          </Box>
          <Box>
            <CustomButton className={classes.transformButton} onClick={deleteLiaisonInformation}>
              SUPPRIMER
            </CustomButton>
            {!hasPrisEnCharge && (
              <CustomButton
                className={classes.transformButton}
                color="secondary"
                onClick={goToEdit}
              >
                MODIFIER
              </CustomButton>
            )}
          </Box>
        </Paper>
        {!hasPrisEnCharge ? (
          <Box className={classes.infoContainerRight}>
            <NoItemContentImage
              title="PRISE EN CHARGE"
              subtitle="Vous pouvez prendre en charge l'information pour la traiter    "
              src={Image}
            >
              <CustomButton
                onClick={handleCharge}
                className={classes.transformButton}
                color="secondary"
              >
                PRENDRE EN CHARGE
              </CustomButton>
            </NoItemContentImage>
          </Box>
        ) : (
          <Paper className={classes.infoContainerRightPaper}>
            <InfoTitle label={'Prise en charge'} />
            <Divider />
            <InfoRow
              label="Date de prise en charge "
              value={moment(datePrisEnCharge).format('DD/MM/YYYY')}
            />
            {descriptionPrisEnCharge !== '' && (
              <TextField
                id="outlined-textarea"
                label="Détails"
                placeholder="Placeholder"
                multiline={true}
                variant="outlined"
                rowsMax={8}
                className={classes.infoCommentsContainerDetail}
                value={descriptionPrisEnCharge}
              />
            )}

            <Box className={classes.filesContainer}>
              <InfoTitleContent label={'Fichier(s) joint(s) :'} />
              <Box display="flex" flexWrap="wrap">
                {fichiersJoints.map((file, index) => (
                  <Box
                    key={`${file.fichier.nomOriginal}_${index}`}
                    className={commonClasses.fileItem}
                    // tslint:disable-next-line: jsx-no-lambda
                    onClick={() => openFile(file.fichier.publicUrl)}
                  >
                    <PictureAsPdf />
                    <Box className={commonClasses.filenameContainer}>
                      <Typography className={classes.nomFile}>
                        {file.fichier.nomOriginal}
                      </Typography>
                      {file.fichier.size && (
                        <Typography>{formatBytes(file.fichier.size, 1)}</Typography>
                      )}
                    </Box>
                  </Box>
                ))}
              </Box>
            </Box>
          </Paper>
        )}
      </Box>
    </Box>
  );
};

export default withRouter(DetailsInfo);
