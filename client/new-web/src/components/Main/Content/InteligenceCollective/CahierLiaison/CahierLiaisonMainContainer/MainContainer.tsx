import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import {
  Box,
  CssBaseline,
  Hidden,
  IconButton,
  Menu,
  MenuItem,
  Typography,
} from '@material-ui/core';
import { Close, History, MoreVert } from '@material-ui/icons';
import { AxiosResponse } from 'axios';
import classnames from 'classnames';
import { uniqBy } from 'lodash';
import moment from 'moment';
import React, { FC, Fragment, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { CAHIER_LIAISON_URL } from '../../../../../../Constant/url';
import { GET_INFORMATION_LIAISON_TYPES } from '../../../../../../federation/basis/information-liaison/query';
import {
  INFORMATION_LIAISON_TYPES,
  INFORMATION_LIAISON_TYPESVariables,
} from '../../../../../../federation/basis/information-liaison/types/INFORMATION_LIAISON_TYPES';
import {
  ORIGINES as ORIGINES_TYPES,
  ORIGINESVariables,
} from '../../../../../../federation/basis/origine/types/ORIGINES';
import { ORIGINES } from '../../../../../../federation/basis/origine/query';
import { GET_FONCTION, GET_TACHE } from '../../../../../../federation/basis/query';
import { ME_me } from '../../../../../../graphql/Authentication/types/ME';
import {
  DO_UPDATE_INFORMATION_LIAISON_STATUS,
  DO_UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNED,
  GET_INFORMATION_LIAISON,
} from '../../../../../../graphql/CahierLiaison';
import { InformationLiaisonInfo } from '../../../../../../graphql/CahierLiaison/types/InformationLiaisonInfo';
import {
  INFORMATION_LIAISON,
  INFORMATION_LIAISONVariables,
} from '../../../../../../graphql/CahierLiaison/types/INFORMATION_LIAISON';

import {
  UPDATE_INFORMATION_LIAISON_STATUS,
  UPDATE_INFORMATION_LIAISON_STATUSVariables,
} from '../../../../../../graphql/CahierLiaison/types/UPDATE_INFORMATION_LIAISON_STATUS';
import {
  UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNED,
  UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNEDVariables,
} from '../../../../../../graphql/CahierLiaison/types/UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNED';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import SnackVariableInterface from '../../../../../../Interface/SnackVariableInterface';
import { nl2br } from '../../../../../../services/Html';
import { getUser } from '../../../../../../services/LocalStorage';
import { uploadToS3 } from '../../../../../../services/S3';
import {
  FichierInput,
  TakeChargeInformationLiaisonInput,
  TypeFichier,
} from '../../../../../../types/graphql-global-types';
import { formatFilenameWithoutDate } from '../../../../../../utils/filenameFormater';
import { isMobile } from '../../../../../../utils/Helpers';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import AttachedFiles from '../../../../../Common/AttachedFiles';
import AvatarInput from '../../../../../Common/AvatarInput';
import Backdrop from '../../../../../Common/Backdrop';
// import { ConfirmDeleteDialog } from '../../../../../Common/ConfirmDialog';
import CustomButton from '../../../../../Common/CustomButton';
import RightContentComment from '../../../../../Common/MainContainer/TwoColumnsContainer/RightContentContainer/RightContentComment';
import MobileTopBar from '../../../../../Common/MobileTopBar';
import NoItemContentImage from '../../../../../Common/NoItemContentImage';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import useCommonStyles from '../../PartageIdeeBonnePratique/PartageIdeeBonnePratiqueMain/LeftContainer/Item/styles';
import useStyles from '../../PartageIdeeBonnePratique/PartageIdeeBonnePratiqueMain/MainContainer/styles';
import DetailsInfoPrisEnCharge from '../DetailsInformation/DetailsInfoPrisEnCharge';
import PriseEnCharge from '../DetailsInformation/Modals/PriseEnCharge';
import useTakeChargeInformationLiaison from '../utils/useTakeChargeInformationLiaison';
import ColleguesConcernees from './ColleguesConcernees';
import ModalHistory from './ModalHistory';
import useOthersStyles from './styles';
import { INFO_STATUS } from '../../../../../../Constant/informationLiaison';

export interface MainContainerProps {
  activeItem: any;
  refetch: any;
  handleDrawerToggle?: () => void;
}

const MainContainer: FC<MainContainerProps & RouteComponentProps> = ({
  activeItem,
  refetch,
  match: { params },
  location: { pathname },
  handleDrawerToggle,
}) => {
  const classes = useStyles();
  const othersClasses = useOthersStyles();
  const commonClasses = useCommonStyles();

  const client = useApolloClient();
  const currentUser: ME_me = getUser();
  const currentUserId = currentUser && currentUser.id;

  const { idliaison } = params as any;

  const [realActiveItem, setRealActiveItem] = React.useState<InformationLiaisonInfo | null>(
    activeItem,
  );

  // const [openConfirm, setOpenConfirm] = React.useState<boolean>(false);

  const [openModalHistory, setOpenModalHistory] = React.useState(false);

  const [openModalTakeCharge, setOpenModalTakeCharge] = React.useState(false);
  const [selectedFiles, setSelectedFiles] = React.useState<File[]>([]);
  const [loading, setLoading] = React.useState(false);

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const open = anchorEl ? true : false;

  let fichiersInputs: FichierInput[] = [];
  let uploadResult: AxiosResponse<any> | null = null;

  const [stateTakeCharge, setStateTakeCharge] = React.useState({
    date: new Date(),
    description: '',
    files: fichiersInputs,
  });

  const informationLiaisonTypesQuery = useQuery<
    INFORMATION_LIAISON_TYPES,
    INFORMATION_LIAISON_TYPESVariables
  >(GET_INFORMATION_LIAISON_TYPES, {
    client: FEDERATION_CLIENT
  });


  const originesQuery = useQuery<
    ORIGINES_TYPES,
    ORIGINESVariables
  >(ORIGINES, {
    client: FEDERATION_CLIENT
  });


  const informationLiaisonTypes = informationLiaisonTypesQuery.data?.informationLiaisonTypes.nodes || [];
  const origines = originesQuery.data?.origines.nodes || [];

  const takeCharge: TakeChargeInformationLiaisonInput = {
    id: (realActiveItem && realActiveItem.id) || '',
    date: stateTakeCharge.date,
    description: stateTakeCharge.description,
    files: stateTakeCharge.files,
  };

  const collegueConcerneeIds: string[] =
    (realActiveItem &&
      realActiveItem.colleguesConcernees &&
      realActiveItem.colleguesConcernees.length > 0 &&
      realActiveItem.colleguesConcernees.map(i => i.userConcernee.id)) ||
    [];

  const collegueConcerneeNames: string[] =
    (realActiveItem &&
      realActiveItem.colleguesConcernees &&
      realActiveItem.colleguesConcernees.length > 0 &&
      realActiveItem.colleguesConcernees.map(i => i.userConcernee.userName || '')) ||
    [];

  const isCollegueConcerned: boolean = collegueConcerneeIds.includes(currentUserId);

  const collegueConcernee = realActiveItem?.colleguesConcernees?.find(
    i => i.userConcernee.id === currentUserId,
  );

  const declarantId = realActiveItem && realActiveItem.declarant && realActiveItem.declarant.id;
  const declarantName =
    realActiveItem && realActiveItem.declarant && realActiveItem.declarant.userName;

  const declarantPhotoUrl =
    realActiveItem &&
    realActiveItem.declarant &&
    realActiveItem.declarant.userPhoto &&
    realActiveItem.declarant.userPhoto.fichier &&
    (realActiveItem.declarant.userPhoto.fichier.urlPresigned ||
      realActiveItem.declarant.userPhoto.fichier.publicUrl);

  const fichiersJoints: any[] =
    (realActiveItem &&
      realActiveItem.fichiersJoints &&
      realActiveItem.fichiersJoints.length > 0 &&
      realActiveItem.fichiersJoints.map((fichierJoint: any) => fichierJoint.fichier)) ||
    [];

  const userCreationId =
    realActiveItem && realActiveItem.userCreation && realActiveItem.userCreation.id;

  const type =
    realActiveItem?.idType && informationLiaisonTypes.find(el => el.id === realActiveItem.idType)?.libelle;

  const urgence = realActiveItem?.urgence
    ? `${realActiveItem.urgence.code}:${realActiveItem.urgence.libelle}`
    : '';
  const importance = realActiveItem?.importance?.libelle || '';

  const origine = realActiveItem?.idOrigine ? origines.find(({ id }) => id === realActiveItem?.idOrigine)?.libelle : ''
  const correspondant = realActiveItem?.origineAssocie ?
    (realActiveItem.origineAssocie as any).userName
    || (realActiveItem.origineAssocie as any).nomLabo
    || (realActiveItem.origineAssocie as any).nom : ''

  const statut =
    isCollegueConcerned && collegueConcernee && collegueConcernee.statut
      ? INFO_STATUS[collegueConcernee.statut]
      : realActiveItem && realActiveItem.statut && INFO_STATUS[realActiveItem.statut];


  const dateCreation =
    realActiveItem &&
    realActiveItem.dateCreation &&
    moment(realActiveItem.dateCreation).format('DD/MM/YYYY');

  const tacheQuery = useQuery(GET_TACHE, {
    client: FEDERATION_CLIENT,
    variables: { id: realActiveItem?.idTache },
    skip: !realActiveItem?.idTache,
  });
  const fonctionQuery = useQuery(GET_FONCTION, {
    client: FEDERATION_CLIENT,
    variables: { id: realActiveItem?.idFonction },
    skip: !realActiveItem?.idFonction,
  });

  const tache = tacheQuery.data?.dQMTTache;
  const fonction = fonctionQuery.data?.dQMTFonction;

  const todoAction = realActiveItem && realActiveItem.todoAction ? 'Oui' : 'Non';

  const MORE_INFOS = isMobile()
    ? [
      { id: 3, label: 'Type', value: type },
      //  { id: 4, label: 'Origine', value: origine },
      { id: 4, label: 'Origine', value: origine },
      { id: 16, label: 'Urgence', value: urgence },
      { id: 17, label: 'Importance', value: importance },
      { id: 10, label: 'Date de création', value: dateCreation },
      { id: 12, label: 'Tâche', value: tache?.libelle || '-' },
      { id: 13, label: 'Fonction', value: fonction?.libelle || '-' },
      { id: 15, label: 'Action todo', value: todoAction },
    ]
    : [
      {
        id: 1,
        label: 'Déclarant',
        value: declarantId === currentUserId ? 'Moi' : declarantName,
      },
      {
        id: 2,
        label: 'Collègue(s) Concernée(s)',
        value: (collegueConcerneeNames.length > 0 && collegueConcerneeNames.join(', ')) || '-',
      },
      { id: 3, label: 'Type', value: type },
      { id: 4, label: 'Origine', value: origine },
      { id: 4, label: 'Correspondant', value: correspondant },
      { id: 16, label: 'Urgence', value: urgence },
      { id: 17, label: 'Importance', value: importance },
      { id: 10, label: 'Date de création', value: dateCreation },
      { id: 12, label: 'Tâche', value: tache?.libelle || '-' },
      { id: 13, label: 'Fonction', value: fonction?.libelle || '-' },
      { id: 15, label: 'Action todo', value: todoAction },
    ];

  const {
    takeChargeInformationLiaison,
    mutationTakeChargeSuccess,
    mutationLoading,
    mutationData,
  } = useTakeChargeInformationLiaison(takeCharge, refetch);

  const [getInformation, { refetch: refetchInformation }] = useLazyQuery<
    INFORMATION_LIAISON,
    INFORMATION_LIAISONVariables
  >(GET_INFORMATION_LIAISON, {
    onCompleted: data => {
      if (data && data.informationLiaison && !realActiveItem) {
        setRealActiveItem(data.informationLiaison);
      }
    },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const [uploadFiles, { loading: uploadLoading }] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (data && data.createPutPresignedUrls && data.createPutPresignedUrls.length > 0) {
        Promise.all(
          data.createPutPresignedUrls.map(async (presigned, index) => {
            if (presigned && selectedFiles.length > 0) {
              await Promise.all(
                selectedFiles.map(async file => {
                  if (presigned.filePath.includes(formatFilenameWithoutDate(file))) {
                    const type: TypeFichier =
                      file.type && file.type.includes('pdf')
                        ? TypeFichier.PDF
                        : file.type.includes('xlsx')
                          ? TypeFichier.EXCEL
                          : TypeFichier.PHOTO;
                    const newFile: FichierInput = {
                      id: '',
                      type,
                      nomOriginal: file.name,
                      chemin: presigned.filePath,
                    };

                    fichiersInputs = uniqBy([...fichiersInputs, newFile], 'chemin');

                    setStateTakeCharge(prevState => ({
                      ...prevState,
                      files: fichiersInputs,
                    }));

                    await uploadToS3(selectedFiles[index], presigned.presignedUrl)
                      .then(result => {
                        if (result && result.status === 200) {
                          uploadResult = result;
                        }
                      })
                      .catch(error => {
                        setLoading(false);
                        console.log('error :>> ', error);
                        displaySnackBar(client, {
                          type: 'ERROR',
                          message: "Erreur lors de l'envoye de(s) fichier(s)",
                          isOpen: true,
                        });
                      });
                  }
                }),
              );
            }
          }),
        ).then(_ => {
          // Execute doCreateUpdate
          if ((uploadResult && uploadResult.status === 200) || fichiersInputs) {
            takeChargeInformationLiaison();
            setLoading(false);
          } else {
            setLoading(false);
          }
        });
      } else {
        takeChargeInformationLiaison();
      }
    },
    onError: errors => {
      setLoading(false);
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const [doUpdateStatus, { loading: updateStatusLoading }] = useMutation<
    UPDATE_INFORMATION_LIAISON_STATUS,
    UPDATE_INFORMATION_LIAISON_STATUSVariables
  >(DO_UPDATE_INFORMATION_LIAISON_STATUS, {
    onCompleted: data => {
      if (data && data.updateInformationLiaisonStatut) {
        if (refetch) refetch();
        setRealActiveItem(data.updateInformationLiaisonStatut);
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Information clôturée avec succès',
        });
      }
    },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const [doUpdateStatusByUserConcerned] = useMutation<
    UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNED,
    UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNEDVariables
  >(DO_UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNED, {
    onCompleted: data => {
      if (data && data.updateInformationLiaisonStatutByUserConcerned) {
        if (refetch) refetch(data.updateInformationLiaisonStatutByUserConcerned.statut !== 'LUES');
        if (refetchInformation) {
          refetchInformation();
        }
        if (collegueConcernee) {
          collegueConcernee.statut = data.updateInformationLiaisonStatutByUserConcerned.statut;
        }
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Information clôturée avec succès',
        });
      }
    },
    onError: error => {
      if (error && error.graphQLErrors) {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      }
    },
  });

  const handleClickPrendreCharge = () => {
    if (isCollegueConcerned && collegueConcernee?.id) {
      doUpdateStatusByUserConcerned({
        variables: { id: collegueConcernee.id, statut: 'CLOTURE' },
      });
    } else {
      setAnchorEl(null);
      setOpenModalTakeCharge(true);
    }
  };

  const handleClickAddTakeCharge = () => {
    setLoading(true);
    if (selectedFiles && selectedFiles.length > 0) {
      // Presigned file
      const filePathsTab: string[] = [];
      selectedFiles.map((file: File) => {
        if (file.type) {
          filePathsTab.push(
            `priseencharge/${realActiveItem && realActiveItem.id}/${formatFilenameWithoutDate(
              file,
            )}`,
          );
        }
      });
      // Create presigned url
      uploadFiles({ variables: { filePaths: filePathsTab } });
    } else {
      takeChargeInformationLiaison();
    }
  };

  // const handleClickCloturer = () => {
  //   setOpenConfirm(true);
  // };

  const handleClickConfirmCloturer = () => {
    if (isCollegueConcerned && collegueConcernee) {
      doUpdateStatusByUserConcerned({
        variables: { id: collegueConcernee.id, statut: 'CLOTURE' },
      });
    } else {
      doUpdateStatus({
        variables: {
          id: (realActiveItem && realActiveItem.id) || '',
          statut: 'CLOTURE',
        },
      });
    }
    // setOpenConfirm(false);
  };

  const handleClickHistory = () => {
    if (refetchInformation) {
      refetchInformation();
    }
    setAnchorEl(null);
    setOpenModalHistory(true);
  };

  React.useEffect(() => {
    setRealActiveItem(activeItem);
  }, [activeItem]);

  React.useMemo(() => {
    if (mutationTakeChargeSuccess) {
      setLoading(false);
      if (refetch) refetch();
    }
  }, [mutationTakeChargeSuccess]);

  React.useEffect(() => {
    if (!activeItem) {
      setRealActiveItem(null);
    }
  }, [activeItem]);

  React.useEffect(() => {
    if (mutationData && mutationData.takeChargeInformationLiaison) {
      setRealActiveItem(mutationData.takeChargeInformationLiaison);
      setLoading(false);
    }
  }, [mutationData]);

  React.useEffect(() => {
    if (idliaison && !realActiveItem && activeItem) {
      getInformation({ variables: { id: idliaison } });
    }
  }, []);

  const disabledBtnCloturer = (): boolean => {
    if (currentUserId && realActiveItem) {
      //if (userCreationId === currentUserId && !isCollegueConcerned) return true;

      // For collegue concerned
      if (isCollegueConcerned && collegueConcernee) {
        if (collegueConcernee.statut === 'CLOTURE') return true;
        // if (
        //   realActiveItem.priority === 3 &&
        //   collegueConcernee.status !== InformationLiaisonStatus.EN_CHARGE
        // ) {
        //   return true;
        // }
      }

      // For user creation
      if (userCreationId === currentUserId) {
        if (realActiveItem.statut === 'CLOTURE') return true;
        // if (
        //   realActiveItem.priority === 3 &&
        //   realActiveItem.status !== InformationLiaisonStatus.EN_CHARGE
        // ) {
        //   return true;
        // }
      }
    }
    return false;
  };

  // const disabledBtnPrendreEnCharge = (): boolean => {
  //   if (currentUserId && realActiveItem) {
  //     if (userCreationId === currentUserId) return true;

  //     // For collegue concerned
  //     if (isCollegueConcerned && collegueConcernee) {
  //       if (collegueConcernee.status === InformationLiaisonStatus.CLOTURE) {
  //         return true;
  //       }
  //     }

  //     // For user creation
  //     if (userCreationId === currentUserId) {
  //       if (realActiveItem.status === InformationLiaisonStatus.CLOTURE) {
  //         return true;
  //       }
  //     }
  //   }

  //   return false;
  // };

  // const DeleteDialogContent = () => {
  //   return <React.Fragment>Etes-vous sûre de vouloir clôturer cette information ?</React.Fragment>;
  // };

  const handleMoreOptionClick = (event: React.MouseEvent<HTMLElement>) => {
    if (event) {
      event.stopPropagation();
      setAnchorEl(event.currentTarget);
    }
  };

  const optionBtn = (
    <>
      <IconButton color="inherit" onClick={handleMoreOptionClick}>
        <MoreVert />
      </IconButton>
      <Menu id="fade-menu" anchorEl={anchorEl} keepMounted={true} open={open}>
        <MenuItem onClick={handleClickHistory}>
          <Typography variant="inherit">Historique</Typography>
        </MenuItem>
        <MenuItem disabled={disabledBtnCloturer()} onClick={handleClickPrendreCharge}>
          <Typography variant="inherit">Clôturer</Typography>
        </MenuItem>
      </Menu>
    </>
  );

  if (window.location.hash === `#${CAHIER_LIAISON_URL}` && realActiveItem) {
    setRealActiveItem(null);
  }

  return (
    <Box className={classes.mainContainer} key={activeItem?.id}>
      {(loading || uploadLoading || mutationLoading || updateStatusLoading) && (
        <Backdrop value="Opération en cours..." />
      )}

      <Box className={classes.mainContent}>
        <CssBaseline />
        <Hidden mdUp={true} implementation="css">
          <MobileTopBar
            title={`${origine || ''}${realActiveItem?.bloquant ? ` - Bloquante` : ''
              }`}
            withBackBtn={true}
            handleDrawerToggle={handleDrawerToggle}
            optionBtn={[optionBtn]}
            withTopMargin={false}
            onClickBack={handleDrawerToggle}
          />
        </Hidden>

        {realActiveItem ? (
          <Box className={classes.detailsContainer}>
            <Hidden mdUp={true} implementation="css">
              <Typography
                className={classes.labelContent}
                dangerouslySetInnerHTML={
                  { __html: nl2br(realActiveItem && realActiveItem.description) } as any
                }
              />
            </Hidden>
            <Hidden smDown={true} implementation="css">
              <Box display="flex" flexDirection="row" marginBottom="15px">
                {/*
              <CustomButton color="secondary" startIcon={<InsertChart />}>
                Feedback
              </CustomButton>
            */}

                <CustomButton
                  color="secondary"
                  startIcon={<History />}
                  onClick={handleClickHistory}
                >
                  Historique
                </CustomButton>

                {/* <CustomButton
                color="secondary"
                startIcon={<Done />}
                disabled={disabledBtnPrendreEnCharge()}
                onClick={handleClickPrendreCharge}
                style={{ marginLeft: 10 }}
              >
                Prendre en charge
              </CustomButton> */}

                <CustomButton
                  color="secondary"
                  startIcon={<Close />}
                  style={{ marginLeft: 10 }}
                  disabled={disabledBtnCloturer()}
                  // onClick={handleClickCloturer}
                  onClick={handleClickPrendreCharge}
                >
                  Clôturer
                </CustomButton>
              </Box>

              <Box display="flex" justifyContent="space-between" alignItems="center">
                {origine && (
                  <Typography className={classes.title}>
                    {`${origine || ''}${realActiveItem?.bloquant ? ` - Bloquante` : ''
                      }`}
                  </Typography>
                )}
              </Box>
            </Hidden>


            <Hidden smDown={true} implementation="css">
              <Box className={othersClasses.infoSupContainer}>
                {MORE_INFOS.filter(info => info.value && info.value !== '-').map((info, index) => {
                  return (
                    <Box key={index} display="flex" alignItems="center">
                      <Typography
                        className={classnames(commonClasses.labelContent)}
                        style={{ minWidth: 'fit-content' }}
                      >
                        {info.label} :
                      </Typography>
                      {info.id === 2 ? (
                        <ColleguesConcernees info={realActiveItem} />
                      ) : (
                        <Typography
                          className={classnames(
                            commonClasses.labelContentValue,
                            othersClasses.infoValue,
                          )}
                          style={{ cursor: 'auto' }}
                        >
                          {info.value || '-'}
                        </Typography>
                      )}
                    </Box>
                  );
                })}
              </Box>
            </Hidden>

            <Hidden smUp={true} implementation="css">
              <Box className={classes.mobileInfoSupContainer}>
                <Box display="flex" justifyContent="space-between" marginBottom="16px">
                  <Typography className={classes.mobileInfoSupLabel}>Déclarant</Typography>
                  <AvatarInput
                    list={
                      (realActiveItem && realActiveItem.declarant && [realActiveItem.declarant]) ||
                      []
                    }
                    small={true}
                    standard={true}
                  />
                </Box>
                <Box display="flex" justifyContent="space-between" marginBottom="16px">
                  <Typography className={classes.mobileInfoSupLabel}>
                    Collègue(s) Concernée(s)
                  </Typography>

                  <Typography className={classes.mobileInfoSupValue}>
                    <AvatarInput
                      list={
                        (realActiveItem &&
                          realActiveItem.colleguesConcernees &&
                          realActiveItem.colleguesConcernees.map(
                            collegue => collegue && collegue.userConcernee,
                          )) ||
                        []
                      }
                      small={true}
                      standard={true}
                    />
                  </Typography>
                </Box>
                {MORE_INFOS.filter(info => info.value && info.value !== '-').map((info, index) => {
                  return (
                    <Box display="flex" justifyContent="space-between" marginBottom="16px">
                      <Typography className={classes.mobileInfoSupLabel}>{info.label}</Typography>

                      <Typography className={classes.mobileInfoSupValue}>{info.value}</Typography>
                    </Box>
                  );
                })}
              </Box>
            </Hidden>

            <Hidden smDown={true} implementation="css">
              <Box display="flex" flexDirection="column">
                {realActiveItem.description && (
                  <Fragment>
                    <Typography className={classes.labelTitle}>Description</Typography>
                    <Typography
                      className={classes.labelContent}
                      dangerouslySetInnerHTML={{ __html: nl2br(realActiveItem.description) } as any}
                    />
                  </Fragment>
                )}
              </Box>
            </Hidden>

            {realActiveItem && realActiveItem.prisEnCharge && (
              <DetailsInfoPrisEnCharge information={realActiveItem} />
            )}

            {fichiersJoints.length > 0 && (
              <Box width="100%" marginTop="10px">
                <AttachedFiles files={fichiersJoints} />
              </Box>
            )}
            <Box width="100%" marginTop="10px">
              <RightContentComment
                listResult={[]}
                codeItem="INFORMATION_LIAISON"
                item={realActiveItem as any}
                refetch={() => refetch(false)}
              />
            </Box>
          </Box>
        ) : (
          <Box
            minHeight="calc(100vh - 162px)"
            width="100%"
            display="flex"
            alignItems="center"
            justifyContent="center"
          >
            <NoItemContentImage
              title="Aperçu détaillé de vos informations"
              subtitle="Choisissez une information dans la liste de droite pour l'afficher en détails dans cette partie."
            />
          </Box>
        )}
      </Box>

      <PriseEnCharge
        open={openModalTakeCharge}
        setOpen={setOpenModalTakeCharge}
        setSelectedFiles={setSelectedFiles}
        selectedFiles={selectedFiles}
        stateTakeCharge={stateTakeCharge}
        setStateTakeCharge={setStateTakeCharge}
        // tslint:disable-next-line: jsx-no-lambda
        handleAddClickTakeCharge={() => {
          handleClickAddTakeCharge();
          handleClickConfirmCloturer();
        }}
      />

      {/* <ConfirmDeleteDialog
        title="Clôture d'information"
        content={<DeleteDialogContent />}
        open={openConfirm}
        setOpen={setOpenConfirm}
        onClickConfirm={handleClickConfirmCloturer}
        confirmBtnLabel="Clôturer"
      /> */}

      {realActiveItem && (
        <ModalHistory info={realActiveItem} open={openModalHistory} setOpen={setOpenModalHistory} />
      )}
    </Box>
  );
};

export default withRouter(MainContainer);
