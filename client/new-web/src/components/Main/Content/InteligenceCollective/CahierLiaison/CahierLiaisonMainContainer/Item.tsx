import { useApolloClient, useMutation, useQuery } from '@apollo/client';
import {
  Box,
  CssBaseline,
  Fade,
  Hidden,
  IconButton,
  ListItemIcon,
  Menu,
  MenuItem,
  Typography,
} from '@material-ui/core';
import {
  ChatBubble,
  Close,
  Delete,
  Edit,
  ErrorOutline,
  Event,
  MoreHoriz,
  Update,
  Visibility,
  VisibilityOff,
  Assignment
} from '@material-ui/icons';
import classnames from 'classnames';
import moment from 'moment';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { CAHIER_LIAISON_URL } from '../../../../../../Constant/url';
import { ME_me } from '../../../../../../graphql/Authentication/types/ME';
import {
  DO_DELETE_SOFT_INFORMATION_LIAISONS,
  DO_UPDATE_INFORMATION_LIAISON_STATUS,
  DO_UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNED,
} from '../../../../../../graphql/CahierLiaison';
import {
  DELETE_SOFT_INFORMATION_LIAISONS,
  DELETE_SOFT_INFORMATION_LIAISONSVariables,
} from '../../../../../../graphql/CahierLiaison/types/DELETE_SOFT_INFORMATION_LIAISONS';
import {
  UPDATE_INFORMATION_LIAISON_STATUS,
  UPDATE_INFORMATION_LIAISON_STATUSVariables,
} from '../../../../../../graphql/CahierLiaison/types/UPDATE_INFORMATION_LIAISON_STATUS';
import {
  UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNED,
  UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNEDVariables,
} from '../../../../../../graphql/CahierLiaison/types/UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNED';
import {
  ORIGINES as ORIGINES_TYPES,
  ORIGINESVariables,
} from '../../../../../../federation/basis/origine/types/ORIGINES';
import { ORIGINES } from '../../../../../../federation/basis/origine/query';
import { GET_INFORMATION_LIAISON_TYPES } from '../../../../../../federation/basis/information-liaison/query';
import {
  INFORMATION_LIAISON_TYPES,
  INFORMATION_LIAISON_TYPESVariables,
} from '../../../../../../federation/basis/information-liaison/types/INFORMATION_LIAISON_TYPES';
import { getUser } from '../../../../../../services/LocalStorage';
import { FichierInput } from '../../../../../../types/graphql-global-types';
import { isMobile, strippedString } from '../../../../../../utils/Helpers';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import Backdrop from '../../../../../Common/Backdrop';
import { ConfirmDeleteDialog } from '../../../../../Common/ConfirmDialog';
import CustomAvatar from '../../../../../Common/CustomAvatar';
import UrgenceImportanceBadge from '../../../../../Common/UrgenceImportanceBadge/UrgenceImportanceBadge';
import useStyles from '../../PartageIdeeBonnePratique/PartageIdeeBonnePratiqueMain/LeftContainer/Item/styles';
import useOthersStyles from './styles';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { isReunionQualiteAction } from '../../../TodoNew/util';

interface ItemProps {
  item: any;
  activeItem: any;
  refetch: any;
  handleClickItem: (item: any, event: any) => void;
  setOpenFormModal?: (value: boolean) => void;
  setState?: (value: any) => void;
  setFichiersJoints?: (value: any) => void;
}

const Item: FC<ItemProps & RouteComponentProps> = ({
  item,
  activeItem,
  refetch,
  handleClickItem,
  history: { push },
  setOpenFormModal,
  setState,
  setFichiersJoints,
}) => {
  const classes = useStyles();
  const othersClasses = useOthersStyles();

  const isActive = item && activeItem && item.id === activeItem.id;

  const client = useApolloClient();

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const [openDeleteDialog, setOpenDeleteDialog] = React.useState<boolean>(false);

  const currentUser: ME_me = getUser();
  const currentUserId = currentUser && currentUser.id;
  const currentUserName = currentUser && currentUser.userName;

  const EDIT_URL = `${CAHIER_LIAISON_URL}/edit/${item && item.id}`;

  const [deleteInformationLiaisons, { loading }] = useMutation<
    DELETE_SOFT_INFORMATION_LIAISONS,
    DELETE_SOFT_INFORMATION_LIAISONSVariables
  >(DO_DELETE_SOFT_INFORMATION_LIAISONS, {
    variables: { ids: [item.id] },
    onCompleted: data => {
      if (
        data &&
        data.softDeleteInformationLiaisons &&
        data.softDeleteInformationLiaisons.length > 0
      ) {
        if (refetch) refetch();
        setOpenDeleteDialog(false);
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Suppression effectuée avec succès',
        });
        push(CAHIER_LIAISON_URL);
      }
    },
    onError: error => {
      setOpenDeleteDialog(false);
      if (error && error.graphQLErrors && error.graphQLErrors.length > 0) {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      }
    },
  });

  const [doUpdateStatus, { loading: updateStatusLoading }] = useMutation<
    UPDATE_INFORMATION_LIAISON_STATUS,
    UPDATE_INFORMATION_LIAISON_STATUSVariables
  >(DO_UPDATE_INFORMATION_LIAISON_STATUS, {
    onCompleted: data => {
      if (data && data.updateInformationLiaisonStatut) {
        if (refetch) refetch(data.updateInformationLiaisonStatut.statut === 'CLOTURE');
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Statut mis à jour avec succès',
        });
        handleClickItem(data.updateInformationLiaisonStatut.statut === 'NON_LUES' ? null : item, null);
      }


    },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const [doUpdateStatusByUserConcerned] = useMutation<
    UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNED,
    UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNEDVariables
  >(DO_UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNED, {
    onCompleted: data => {
      if (data && data.updateInformationLiaisonStatutByUserConcerned) {
        if (refetch) refetch(data.updateInformationLiaisonStatutByUserConcerned.statut === 'CLOTURE');
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Statut mis à jour avec succès',
        });
        handleClickItem(data.updateInformationLiaisonStatutByUserConcerned.statut === 'NON_LUES' ? null : item, null);
      }
    },
    onError: error => {
      /*error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });*/
    },
  });

  const informationLiaisonTypesQuery = useQuery<
    INFORMATION_LIAISON_TYPES,
    INFORMATION_LIAISON_TYPESVariables
  >(GET_INFORMATION_LIAISON_TYPES, {
    client: FEDERATION_CLIENT
  });



  const originesQuery = useQuery<
    ORIGINES_TYPES,
    ORIGINESVariables
  >(ORIGINES, {
    client: FEDERATION_CLIENT
  });

  const informationLiaisonTypes = informationLiaisonTypesQuery.data?.informationLiaisonTypes.nodes || [];
  const origines = originesQuery.data?.origines.nodes || [];

  const colleguesConcerneesIds: string[] =
    (item &&
      item.colleguesConcernees &&
      item.colleguesConcernees.length > 0 &&
      item.colleguesConcernees.map(i => i.userConcernee.id)) ||
    [];

  const colleguesConcerneesNames: string[] =
    (item &&
      item.colleguesConcernees &&
      item.colleguesConcernees.length > 0 &&
      item.colleguesConcernees.map(i => i.userConcernee.userName)) ||
    [];

  const userCreationId = item && item.userCreation && item.userCreation.id;

  const isCollegueConcerned: boolean = colleguesConcerneesIds.includes(currentUserId);

  const defaultCollegueConcerneeName = isCollegueConcerned
    ? currentUserName
    : colleguesConcerneesNames[0];
  const othersColleguesConcerneesNames: string[] = colleguesConcerneesNames.filter(
    i => i !== defaultCollegueConcerneeName,
  );

  const collegueConcernee =
    item &&
    item.colleguesConcernees &&
    item.colleguesConcernees.length > 0 &&
    item.colleguesConcernees.find(i => i.userConcernee.id === currentUserId);



  const initState = () => {
    if (item) {
      const p = item;

      const fakeFiles: File[] = [];
      const fichiers: FichierInput[] = [];
      if (p.fichiersJoints && p.fichiersJoints.length > 0) {
        p.fichiersJoints.map(fichierJoint => {
          const fichierInput: FichierInput = {
            id: fichierJoint.fichier.id,
            chemin: fichierJoint.fichier.chemin,
            nomOriginal: fichierJoint.fichier.nomOriginal,
            type: fichierJoint.fichier.type as any,
          };

          const fakeFile: File = { name: fichierJoint.fichier.nomOriginal } as any;

          if (!fichiers.map(elem => elem.chemin).includes(fichierJoint.fichier.chemin)) {
            fichiers.push(fichierInput);
          }

          if (!fakeFiles.map(elem => elem.name).includes(fichierJoint.fichier.nomOriginal)) {
            fakeFiles.push(fakeFile);
          }
        });
      }

      if (setFichiersJoints) setFichiersJoints(fakeFiles);

      return {
        id: p.id || '',
        idOrigine: p.idOrigine || '',
        idOrigineAssocie: p.idOrigineAssocie || '',
        idType: p.idType || '',
        statut: p.statut || '',
        bloquant: p.bloquant || false,
        description: p.description || '',
        idUserDeclarant: (p.declarant && p.declarant.id) || '',
        declarantFullName: (p.declarant && p.declarant.userName) || '',
        idConcernedColleagues:
          (p.colleguesConcernees &&
            p.colleguesConcernees.length > 0 &&
            p.colleguesConcernees.map(i => i.userConcernee.id)) ||
          [],
        teamAll: false,
        fichiers,
      };
    }
  };

  const activeMenu = (): boolean => {
    if (item && currentUserId) {
      if ((item.userCreation && item.userCreation.id === currentUserId) || isCollegueConcerned) {
        return true;
      }
    }
    return false;
  };

  const activeEditAndDeleteMenu = (): boolean => {
    if (!item.idItem && userCreationId === currentUserId) return true;
    return false;
  };

  const handleClickMenu = (event: React.MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClickEdit = (event: React.MouseEvent<HTMLLIElement, MouseEvent>) => {
    event.stopPropagation();
    handleClose();
    if (isMobile()) {
      if (setOpenFormModal && setState) {
        const initialState = initState();
        setState(initState);
        setOpenFormModal(true);
      }
    } else {
      push(EDIT_URL);
    }
  };

  const handleClickDelete = (event: React.MouseEvent<HTMLLIElement, MouseEvent>) => {
    event.stopPropagation();
    handleClose();
    setOpenDeleteDialog(true);
  };

  const handleClickUpdateStatus = (statut: string) => (
    event: React.MouseEvent<HTMLLIElement, MouseEvent>,
  ) => {
    event.stopPropagation();
    handleClose();
    if (isCollegueConcerned && collegueConcernee) {
      doUpdateStatusByUserConcerned({
        variables: { id: collegueConcernee.id, statut },
      });
    } else {
      doUpdateStatus({ variables: { id: item.id, statut } });
    }
  };

  const onClickConfirmDelete = () => {
    deleteInformationLiaisons();
  };


  const DeleteDialogContent = () => {
    return <span>Êtes-vous sur de vouloir supprimer ?</span>;
  };


  const menuItem = (
    <Menu
      id="fade-menu"
      anchorEl={anchorEl}
      keepMounted={true}
      open={open}
      onClose={handleClose}
      TransitionComponent={Fade}
      // tslint:disable-next-line: jsx-no-lambda
      onClick={event => {
        event.stopPropagation();
      }}
    >
      {/* TODO: isReunionQualiteAction(item) &&
        (<MenuItem onClick={() => push(`/demarche-qualite/compte-rendu/details/${item.idItemAssocie}`)} >
          <ListItemIcon>
            <Assignment fontSize="small" />
          </ListItemIcon>
          <Typography variant="inherit">Consulter la réunion</Typography>
      </MenuItem>)*/}

      <MenuItem onClick={handleClickEdit} disabled={!activeEditAndDeleteMenu()}>
        <ListItemIcon>
          <Edit fontSize="small" />
        </ListItemIcon>
        <Typography variant="inherit">Modifier</Typography>
      </MenuItem>
      <MenuItem onClick={handleClickDelete} disabled={!activeEditAndDeleteMenu()}>
        <ListItemIcon>
          <Delete fontSize="small" />
        </ListItemIcon>
        <Typography variant="inherit">Supprimer</Typography>
      </MenuItem>
      {((isCollegueConcerned && collegueConcernee && collegueConcernee.statut === 'NON_LUES') ||
        (currentUserId === userCreationId && item.statut === 'NON_LUES')) && (
          <MenuItem onClick={handleClickUpdateStatus('LUES')}>
            <ListItemIcon>
              <Update fontSize="small" />
            </ListItemIcon>
            <Typography variant="inherit">Changer le statut à Lue</Typography>
          </MenuItem>
        )}
      {((isCollegueConcerned && collegueConcernee && collegueConcernee.statut === 'LUES') ||
        (currentUserId === userCreationId && item.statut === 'LUES')) && (
          <MenuItem onClick={handleClickUpdateStatus('NON_LUES')} >
            <ListItemIcon>
              <ErrorOutline fontSize="small" />
            </ListItemIcon>
            <Typography variant="inherit">Changer le statut à Non Lue</Typography>
          </MenuItem>
        )}
    </Menu>
  );

  return (
    <>
      <CssBaseline />
      <Hidden mdUp={true} implementation="css">
        <Box className={classes.mobileRoot} onClick={event => handleClickItem(item, event)}>
          <Box padding="16px" borderBottom="1.5px solid #E1E1E1">
            <Box display="flex" justifyContent="space-between">
              <Typography className={classes.title}>
                {(item?.idOrigine && origines.find(({ id }) => id === item.idOrigine)?.libelle) || '-'}
              </Typography>
              <Box display="flex" alignItems="center">
                <Typography className={classnames(classes.dateFromNow)}>
                  {(item && item.dateCreation && moment(item.dateCreation).fromNow()) || '-'}
                </Typography>
                <IconButton
                  size="small"
                  aria-controls="fade-menu"
                  aria-haspopup="true"
                  onClick={handleClickMenu}
                  disabled={!activeMenu()}
                >
                  <MoreHoriz />
                </IconButton>
                {menuItem}
              </Box>
            </Box>
            <Box>
              <Typography>
                {item.titre?.length > 0
                  ? item.titre
                  : item.description.length > 50
                    ? `${strippedString(item.description.substring(0, 50))}...`
                    : strippedString(item.description)}
              </Typography>
            </Box>
            <Box className={classes.spaceBetween} marginTop="16px">
              <Box display="flex" flexDirection="column">
                <Typography className={classes.mobileLabel}>Date :</Typography>
                <Typography className={classes.mobileLabelContent}>
                  {(item && item.dateCreation && moment(item.dateCreation).format('DD/MM/YYYY')) ||
                    '-'}
                </Typography>
              </Box>
              <Box>
                {item?.importance && item?.urgence && (
                  <UrgenceImportanceBadge
                    importanceOrdre={item.importance.ordre}
                    urgenceCode={item.urgence.code}
                  />
                )}
              </Box>
            </Box>
          </Box>
          <Box padding="16px">
            <Box className={classes.spaceBetween}>
              <Box display="flex" flexDirection="column">
                <Typography className={classes.mobileLabel}>Déclarant :</Typography>
                <CustomAvatar
                  name={
                    item && item.declarant && item.declarant.id === currentUserId
                      ? currentUserName
                      : item && item.declarant && item.declarant.userName
                  }
                />
              </Box>
              <Box display="flex" flexDirection="column">
                <Typography className={classes.mobileLabel}>Type :</Typography>
                <Typography className={classes.mobileLabelContent}>
                  {(item?.idType && informationLiaisonTypes.find(({ id }) => id === item.idType)?.libelle) || '-'}
                </Typography>
              </Box>
            </Box>
            {/* <Typography
              className={classes.subTitle}
              dangerouslySetInnerHTML={
                { __html: nl2br(trimmedString(item.description || '', 40)) } as any
              }
            /> */}
          </Box>
        </Box>
      </Hidden>
      <Hidden smDown={true} implementation="css">
        <Box
          className={isActive ? classnames(classes.root, classes.isActive) : classes.root}
          // tslint:disable-next-line: jsx-no-lambda
          onClick={event => handleClickItem(item, event)}
        >
          {loading && <Backdrop value="Suppression en cours..." />}
          {updateStatusLoading && <Backdrop value="Changement de statut en cours..." />}

          <Box className={classnames(classes.flexRow)}>
            <Typography className={classes.title}>
              {(item?.idOrigine && origines.find(({ id }) => id === item.idOrigine)?.libelle) || '-'}
            </Typography>
            <Box display="flex" alignItems="center">
              <Typography className={classnames(classes.dateFromNow)}>
                {(item && item.dateCreation && moment(item.dateCreation).fromNow()) || '-'}
              </Typography>
              <IconButton
                size="small"
                aria-controls="fade-menu"
                aria-haspopup="true"
                onClick={handleClickMenu}
                disabled={!activeMenu()}
              >
                <MoreHoriz />
              </IconButton>
            </Box>

            {menuItem}
          </Box>

          <Box
            justifyContent="space-between"
            marginBottom="8px"
            marginTop="8px"
            className={classnames(classes.flexRow, classes.titleContainer)}
          >
            <Typography className={classes.subTitle}>
              {item.titre.length > 0
                ? item.titre
                : item.description.length > 50
                  ? `${strippedString(item.description.substring(0, 50))}...`
                  : strippedString(item.description)}
            </Typography>
            <Box display="flex" alignItems="center">
              <Event />
              <Typography className={classes.date}>
                {(item && item.dateCreation && moment(item.dateCreation).format('DD/MM/YYYY')) ||
                  '-'}
              </Typography>
            </Box>
          </Box>

          <Box display="flex" justifyContent="space-between">
            <Box>
              <Box display="flex" alignItems="center">
                <Typography className={classnames(classes.labelContent, classes.widthLabel69)}>
                  Déclarant :
                </Typography>
                <Typography
                  className={classnames(classes.labelContentValue, classes.lenghtValueName)}
                >
                  {item && item.declarant && item.declarant.id === currentUserId
                    ? 'Moi'
                    : (item && item.declarant && item.declarant.userName) || '-'}
                </Typography>
              </Box>
              <Box display="flex" alignItems="center">
                <Typography className={classes.labelContent}>Type:</Typography>
                <Typography className={classes.labelContentValue}>
                  {(item?.idType && informationLiaisonTypes.find(({ id }) => id === item.idType)?.libelle) || '-'}
                </Typography>
              </Box>
            </Box>
            <Box>
              {item?.importance && item?.urgence && (
                <UrgenceImportanceBadge
                  importanceOrdre={item.importance.ordre}
                  urgenceCode={item.urgence.code}
                />
              )}
            </Box>
          </Box>
          <Box display="flex" justifyContent="space-between" marginTop="16px">
            <Box display="flex">
              <ChatBubble />
              <Typography>{item.nbComment}</Typography>
            </Box>
            <Box>
              {item.nbCollegue > 0 && (
                <Box display="flex">
                  {isCollegueConcerned && collegueConcernee && collegueConcernee.statut === 'NON_LUES' ? <VisibilityOff /> : <Visibility />}
                  <Typography>{`${item.nbLue}/${item.nbCollegue}`}</Typography>
                </Box>
              )}
            </Box>
            <Box>
              {item.statut === 'CLOTURE' && (
                <Box display="flex">
                  <Close />
                  <Typography>Cloturé</Typography>
                </Box>
              )}
            </Box>
          </Box>
        </Box>
      </Hidden>
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
    </>
  );
};

export default withRouter(Item);
