import { useContext, useState } from 'react';
import { useApolloClient, useMutation } from '@apollo/client';
import { ContentContext, ContentStateInterface } from '../../../../../../AppContext';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import { DO_DELETE_SOFT_INFORMATION_LIAISONS } from '../../../../../../graphql/CahierLiaison';
import { DELETE_SOFT_INFORMATION_LIAISONS, DELETE_SOFT_INFORMATION_LIAISONSVariables } from '../../../../../../graphql/CahierLiaison/types/DELETE_SOFT_INFORMATION_LIAISONS';

/**
 *  Create cahier de liaison hooks
 */
const useDeleteCahierLiaison = (values: DELETE_SOFT_INFORMATION_LIAISONSVariables) => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const client = useApolloClient();
  const [mutationDeleteSuccess, setMutationDeleteSuccess] = useState<boolean>(false);

  const [deleteCahierLiaison, { loading: mutationLoadingDelete }] = useMutation<
    DELETE_SOFT_INFORMATION_LIAISONS,
    DELETE_SOFT_INFORMATION_LIAISONSVariables
  >(DO_DELETE_SOFT_INFORMATION_LIAISONS, {
    variables: {
      ids: values.ids,
    },
    update: (cache, { data }) => {
      if (data && data.softDeleteInformationLiaisons) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables,
          });
          if (req && req.search && req.search.data) {
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    total: req.search.total + 1,
                    data: [...req.search.data, data.softDeleteInformationLiaisons],
                  },
                },
              },
              variables,
            });
          }
        }
      }
    },
    onCompleted: (data) => {
      if (data && data.softDeleteInformationLiaisons) {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Suppression effectuée avec succès',
        });
        setMutationDeleteSuccess(true);
          // TODO: Migration
       // (client as any).writeData({ data: { checkedsCahierLiaison: null } });
      }
    },
    onError: (errors) => {
      console.log(errors);
      if (errors.graphQLErrors) {
        errors.graphQLErrors.map((err) => {
          displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
        });
      }
    },
  });

  return { useDeleteCahierLiaison, deleteCahierLiaison, mutationDeleteSuccess };
};

export default useDeleteCahierLiaison;
