import { useContext, useState } from 'react';
import { useApolloClient, useMutation } from '@apollo/client';
import { ContentContext, ContentStateInterface } from '../../../../../../AppContext';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';

import { DO_TAKE_CHARGE_INFORMATION_LIAISON } from '../../../../../../graphql/CahierLiaison';
import {
  TAKE_CHARGE_INFORMATION_LIAISON,
  TAKE_CHARGE_INFORMATION_LIAISONVariables,
} from '../../../../../../graphql/CahierLiaison/types/TAKE_CHARGE_INFORMATION_LIAISON';
import { TakeChargeInformationLiaisonInput } from '../../../../../../types/graphql-global-types';

/**
 *  Create take charge hooks
 */
const useTakeChargeInformationLiaison = (
  takeCharge: TakeChargeInformationLiaisonInput,
  refetch?: () => void,
) => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const client = useApolloClient();
  const [mutationTakeChargeSuccess, setMutationTakeChargeSuccess] = useState<boolean>(false);

  const [
    takeChargeInformationLiaison,
    { loading: mutationLoading, data: mutationData },
  ] = useMutation<TAKE_CHARGE_INFORMATION_LIAISON, TAKE_CHARGE_INFORMATION_LIAISONVariables>(
    DO_TAKE_CHARGE_INFORMATION_LIAISON,
    {
      variables: {
        input: takeCharge,
      },
      update: (cache, { data }) => {
        if (data && data.takeChargeInformationLiaison && takeCharge && !takeCharge.id) {
          if (variables && operationName) {
            const req: any = cache.readQuery({
              query: operationName,
              variables,
            });
            if (req && req.search && req.search.data) {
              cache.writeQuery({
                query: operationName,
                data: {
                  search: {
                    ...req.search,
                    ...{
                      total: req.search.total + 1,
                      data: [...req.search.data, data.takeChargeInformationLiaison],
                    },
                  },
                },
                variables,
              });
            }
          }
        }
      },
      onCompleted: data => {
        if (data && data.takeChargeInformationLiaison) {
          displaySnackBar(client, {
            isOpen: true,
            type: 'SUCCESS',
            message: 'Prise en charge réussie',
          });
          if (refetch) refetch();
          setMutationTakeChargeSuccess(true);
        }
      },
      onError: errors => {
        console.log(errors);
        if (errors.graphQLErrors) {
          errors.graphQLErrors.map(err => {
            displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
          });
        }
      },
    },
  );

  return { takeChargeInformationLiaison, mutationTakeChargeSuccess, mutationLoading, mutationData };
};

export default useTakeChargeInformationLiaison;
