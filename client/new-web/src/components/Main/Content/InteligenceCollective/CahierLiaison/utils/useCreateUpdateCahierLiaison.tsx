import { useApolloClient, useMutation } from '@apollo/client';
import { useContext, useState } from 'react';
import { ContentContext, ContentStateInterface } from '../../../../../../AppContext';
import { DO_CREATE_UPDATE_INFORMATION_LIAISON } from '../../../../../../graphql/CahierLiaison';
import {
  CREATE_UPDATE_INFORMATION_LIAISON,
  CREATE_UPDATE_INFORMATION_LIAISONVariables,
} from '../../../../../../graphql/CahierLiaison/types/CREATE_UPDATE_INFORMATION_LIAISON';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';

/**
 *  Create cahier liaison hooks
 */
const useCreateUpdateCahierLiaison = (
  values: CREATE_UPDATE_INFORMATION_LIAISONVariables,
  setOpenFormModal?: (value: boolean) => void,
  setLoading?: (value: boolean) => void,
) => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const client = useApolloClient();
  const [mutationSuccess, setMutationSuccess] = useState<boolean>(false);

  const [createUpdateCahierLiaison, { loading: mutationLoading }] = useMutation<
    CREATE_UPDATE_INFORMATION_LIAISON,
    CREATE_UPDATE_INFORMATION_LIAISONVariables
  >(DO_CREATE_UPDATE_INFORMATION_LIAISON, {
    variables: {
      input: values.input,
    },
    update: (cache, { data }) => {
      if (data && data.createUpdateInformationLiaison && values && !values.input.id) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables,
          });
          if (req && req.search && req.search.data) {
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    total: req.search.total + 1,
                    data: [data.createUpdateInformationLiaison, ...req.search.data],
                  },
                },
              },
              variables,
            });
          }
        }
      }
    },
    onCompleted: data => {
      if (data && data.createUpdateInformationLiaison) {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message:
            (values.input.isRemoved && 'Suppression effectuée avec succès') ||
            `${values.input.id ? 'Modification' : 'Création'} réussie`,
        });
        setMutationSuccess(true);
          // TODO: Migration    
        // (client as any).writeData({ data: { checkedsCahierLiaison: null } });
        if (setOpenFormModal) setOpenFormModal(false);
        if (setLoading) setLoading(false);
      }
    },
    onError: errors => {
      console.log(errors);
      if (errors.graphQLErrors) {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
        });

        if (setOpenFormModal) setOpenFormModal(false);
        if (setLoading) setLoading(false);
      }
    },
  });
  return { createUpdateCahierLiaison, mutationSuccess, mutationLoading };
};

export default useCreateUpdateCahierLiaison;
