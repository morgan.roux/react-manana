import { useQuery } from '@apollo/client';
import { Box, IconButton } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import FilterAlt from '../../../../../assets/icons/todo/filter_alt_white.svg';
import { ME_me } from '../../../../../graphql/Authentication/types/ME';
import { DO_COUNT_INFORMATION_LIAISONS } from '../../../../../graphql/InformationLiaison/query';
import {
  COUNT_INFORMATION_LIAISONS,
  COUNT_INFORMATION_LIAISONSVariables,
} from '../../../../../graphql/InformationLiaison/types/COUNT_INFORMATION_LIAISONS';
import { getPharmacie, getUser } from '../../../../../services/LocalStorage';
import LeftSidebarAndMainPage from '../../../../Common/Layouts/LeftSidebarAndMainPage';
import { useImportance, useUrgence } from '../../../../hooks';
import { ImportanceInterface } from '../../TodoNew/Common/ImportanceFilter/ImportanceFilterList';
import { UrgenceInterface } from '../../TodoNew/Common/UrgenceFilter/UrgenceFilter';
import CahierLiaisonFilter from './CahierLiaisonFilter';
import CahierLiaisonMain from './CahierLiaisonMain';
import useStyles from './styles';

const CahierLiaison: React.FC<RouteComponentProps> = ({
  history: { push },
  location: { pathname },
}) => {
  const classes = useStyles({});

  const currentUser: ME_me = getUser();

  const pharmacie = getPharmacie();

  const isOnEmise = pathname.includes('/emise');

  const informationsList = {
    emise: 'coupleStatutDeclarant',
    recue: 'colleguesConcernees.coupleStatutUserConcernee',
  };

  const [informations, setInformations] = useState<string>(
    isOnEmise ? informationsList.emise : informationsList.recue,
  );

  const statusInitialState =
    informations === informationsList.recue ? ['NON_LUES'] : ['EN_COURS'];

  const [searchTxt, setSearchTxt] = useState<any>('');
  const [statuts, setStatuts] = useState<any>(statusInitialState);
  const [startDate, setStartDate] = useState<any>('');
  const [endDate, setEndDate] = useState<any>('');
  const [state, setState] = useState<any>({
    concernedColleagueId: 'all',
    concernedColleagueName: 'all',
    concernedColleaguesIds: [],
  });

  const [isInConcernedParticipant, setIsInConcernedParticipant] = useState(false);

  const [ckeckedMyInfo, setCkeckedMyInfo] = useState<boolean>(false);

  const [activateFilter, setActivateFilter] = useState<boolean>(true);

  const handleBack = () => {
    push('/');
  };

  const handleClickMyInfos = React.useCallback((value: boolean) => {
    setCkeckedMyInfo(!value);
  }, []);

  const [openDrawer, setOpenDrawer] = React.useState(false);

  const handleDrawerToggle = () => {
    setOpenDrawer(!openDrawer);
  };

  const handleShowFiltersClick = () => {
    setOpenDrawer(!openDrawer);
  };

  const [urgence, setUrgence] = useState<UrgenceInterface[] | null | undefined>();
  const [importance, setImportance] = useState<ImportanceInterface[] | null | undefined>();

  const loadingImportances = useImportance();
  const loadingUrgences = useUrgence();

  const importancesData = loadingImportances.data?.importances.nodes;
  const urgencesData = loadingUrgences.data?.urgences.nodes;

  useEffect(() => {
    if (!loadingUrgences.loading && urgencesData) {
      setUrgence((urgencesData || []) as any);
    }
  }, [loadingUrgences.loading, urgencesData]);

  useEffect(() => {
    if (!loadingImportances.loading && importancesData) {
      setImportance((importancesData || []) as any);
    }
  }, [loadingImportances.loading, importancesData]);

  const idImportances =
    importance?.filter(etiquette => etiquette.checked).map(({ id }) => id) || [];
  const idUrgences = urgence?.filter(urgence => urgence.checked).map(({ id }) => id) || [];

  const filters = {
    idImportances,
    idUrgences,
  };

  useEffect(() => {
    setStatuts(statusInitialState);
  }, [informations]);

  const optionBtn = (
    <IconButton
      className={classes.iconAction}
      color="inherit"
      aria-label="settings"
      edge="start"
      onClick={handleShowFiltersClick}
    >
      <img src={FilterAlt} />
    </IconButton>
  );

  const countInformationLiaisonsQuery = useQuery<
    COUNT_INFORMATION_LIAISONS,
    COUNT_INFORMATION_LIAISONSVariables
  >(DO_COUNT_INFORMATION_LIAISONS, {
    fetchPolicy: 'cache-and-network',
    variables: {
      input: {
        idPharmacie: pharmacie?.id,
        dateDebut: startDate !== '' ? startDate : undefined,
        dateFin: endDate !== '' ? endDate : undefined,
        idsCollegueConcernee:
          informations === informationsList.recue ? state.concernedColleaguesIds : undefined,
        idsDeclarant:
          informations === informationsList.emise ? state.concernedColleaguesIds : undefined,
      },
    },
  });

  const countInformationLiaisons = countInformationLiaisonsQuery.data?.countInformationLiaisons;

  return (
    <Box className={classes.root}>
      <LeftSidebarAndMainPage
        drawerTitle="Cahier de liaison"
        drawerBackUrl="/"
        sidebarChildren={
          <CahierLiaisonFilter
            currentUser={currentUser}
            handleSearchTxt={setSearchTxt}
            handleStatus={setStatuts}
            handleEndDate={setEndDate}
            handleStartDate={setStartDate}
            state={state}
            isInConcernedParticipant={isInConcernedParticipant}
            setState={setState}
            handleBack={handleBack}
            handleClickMyInfos={handleClickMyInfos}
            informations={informations}
            setInformations={setInformations}
            urgence={urgence}
            setUrgence={setUrgence}
            etiquette={importance}
            setEtiquette={setImportance}
            activateFilter={activateFilter}
            setActivateFilter={setActivateFilter}
            countInformationLiaisons={countInformationLiaisons}
          />
        }
        mainChildren={
          <CahierLiaisonMain
            filters={filters}
            searchTxt={searchTxt}
            statuts={statuts}
            startDate={startDate}
            endDate={endDate}
            stateCollegeConcernees={state}
            ckeckedMyInfo={ckeckedMyInfo}
            handleSearchTxt={setSearchTxt}
            informations={informations}
            activateFilter={activateFilter}
            setActivateFilter={setActivateFilter}
            refetchCount={countInformationLiaisonsQuery.refetch}
          />
        }
        optionBtn={[optionBtn]}
        handleDrawerToggle={handleDrawerToggle}
        openDrawer={openDrawer}
        setOpenDrawer={setOpenDrawer}
      />
    </Box>
  );
};

export default withRouter(CahierLiaison);
