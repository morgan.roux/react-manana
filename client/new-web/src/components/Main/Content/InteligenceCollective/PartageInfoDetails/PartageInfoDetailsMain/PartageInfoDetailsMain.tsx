import { useApolloClient, useQuery } from '@apollo/client';
import { Box, Button, Typography } from '@material-ui/core';
import React, { useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import {
  CAHIER_LIAISON_URL,
  SHAREINFO_DETAILS_URL,
  SHAREINFO_FRIEND_GROUP_URL,
  SHAREINFO_ITEMS_CHOICE_URL,
  SHAREINFO_LABO_PARTENAIRE_URL,
  SHAREINFO_PARTENAIRE_SERVICE_URL,
} from '../../../../../../Constant/url';
import { GET_SEARCH_ACTION } from '../../../../../../graphql/Action';
import {
  SEARCH_ACTION,
  SEARCH_ACTIONVariables,
} from '../../../../../../graphql/Action/types/SEARCH_ACTION';
import { GET_SEARCH_CUSTOM_INFORMATION_LIAISON } from '../../../../../../graphql/CahierLiaison';
import {
  SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON,
  SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISONVariables,
} from '../../../../../../graphql/CahierLiaison/types/SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON';
import { GET_GROUPE_AMIS } from '../../../../../../graphql/GroupAmis/query';
import {
  GROUPE_AMIS,
  GROUPE_AMISVariables,
} from '../../../../../../graphql/GroupAmis/types/GROUPE_AMIS';
import { GroupementInfo } from '../../../../../../graphql/Groupement/types/GroupementInfo';
import { GET_SHARED_COMMENTS } from '../../../../../../graphql/IntelligenceCollective/query';
import {
  SHARED_COMMENTS,
  SHARED_COMMENTSVariables,
} from '../../../../../../graphql/IntelligenceCollective/types/SHARED_COMMENTS';
import { GET_LABORATOIRE } from '../../../../../../graphql/Laboratoire/query';
import {
  LABORATOIRE,
  LABORATOIREVariables,
} from '../../../../../../graphql/Laboratoire/types/LABORATOIRE';
import { GET_PARTENAIRE } from '../../../../../../graphql/Partenaire/query';
import {
  PARTENAIRE,
  PARTENAIREVariables,
} from '../../../../../../graphql/Partenaire/types/PARTENAIRE';
import { getGroupement } from '../../../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import Backdrop from '../../../../../Common/Backdrop';
import CustomTabs from '../../../../../Common/CustomTabs';
import { TabInterface } from '../../../../../Common/CustomTabs/CustomTabs';
import SubToolbar from '../../../../../Common/newCustomContent/SubToolbar';
import SharedAmelioration from '../../PartageInformations/SharedAmeliorations';
import SharedComments from '../../PartageInformations/SharedComments';
import SharedIncidents from '../../PartageInformations/SharedIncidents';
import SharedInfo from '../../PartageInformations/SharedInfo';
import SharedReclamation from '../../PartageInformations/SharedReclamation';
import SharedTodo from '../../PartageInformations/SharedTodo/SharedTodo';
import useStyles from './styles';

interface PartageInfoDetailSMain {
  setPath: any;
  setClickTodo: (arg0: any) => void;
  isClickTodo: any;
  setClickComment: (arg0: any) => void;
  isClickComment: any;
  statusTodo: any[];
  setListResult: (arg0: any) => void;
  statuts: any[];
  startDate?: any;
  endDate?: any;
}
const PartageInfoDetailsMain: React.FC<PartageInfoDetailSMain & RouteComponentProps> = ({
  history: { push },
  match: { params },
  setPath,
  setClickTodo,
  statusTodo,
  isClickTodo,
  setClickComment,
  // isClickComment,
  startDate,
  endDate,
  statuts,
  setListResult,
}) => {
  const classes = useStyles();

  const { item, itemId, tab } = params as any;

  const client = useApolloClient();
  const groupement: GroupementInfo = getGroupement();

  const [typeItem, setTypeItem] = useState('');

  const isLabo: boolean = item === 'LABO';
  const isPartenaire: boolean = item === 'PSERVICE';
  const isGroupeAmis: boolean = item === 'GROUPE_AMIS';

  React.useEffect(() => {
    if (item) setTypeItem(item);
  }, [item]);

  // setting tab
  const [currentTab, setCurrentTab] = useState<string | null>(tab || null);

  React.useEffect(() => {
    setPath(currentTab);
  }, [currentTab, setPath]);

  const { data: laboData, loading: laboLoading } = useQuery<LABORATOIRE, LABORATOIREVariables>(
    GET_LABORATOIRE,
    {
      variables: { id: itemId || '' },
      onError: error => {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );
  const laboratoire = laboData && laboData.laboratoire;

  const { data: parteData, loading: parteLoading } = useQuery<PARTENAIRE, PARTENAIREVariables>(
    GET_PARTENAIRE,
    {
      variables: { id: itemId || '' },
      onError: error => {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );
  const partenaire = parteData && parteData.partenaire;

  const { data: groupAmisData, loading: groupAmisLoading } = useQuery<
    GROUPE_AMIS,
    GROUPE_AMISVariables
  >(GET_GROUPE_AMIS, {
    variables: { id: itemId || '' },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });
  const groupeAmis = groupAmisData && groupAmisData.groupeAmis;
  const groupeAmisPharmacieMembres: any[] = (groupeAmis && groupeAmis.pharmacieMembres) || [];

  let userId = '';
  let title = '';

  // TODO : Check
  let groupeAmisId = '';

  if (isLabo) {
    userId = (laboratoire && laboratoire.user && laboratoire.user.id) || '';
    title = (laboratoire && laboratoire.nomLabo) || '';
  } else if (isPartenaire) {
    userId = (partenaire && partenaire.user && partenaire.user.id) || '';
    title = (partenaire && partenaire.nom) || '';
  } else if (isGroupeAmis) {
    title = (groupeAmis && groupeAmis.nom) || '';
    groupeAmisId = (groupeAmis && groupeAmis.id) || '';
  }

  const defaultMust: any[] = [
    { term: { 'groupement.id': groupement && groupement.id } },
    { term: { isRemoved: false } },
  ];
  const defaultSort: any[] = [{ dateCreation: { order: 'desc' } }];

  const commentMust: any[] = [...defaultMust, { term: { 'item.code': 'PROD' } }];
  const informationMust: any[] = [...defaultMust];
  const todoMust: any[] = [...defaultMust];

  const commentShould: any[] = [];
  const informationShould: any[] = [];
  const todoShould: any[] = [];

  const minimumShouldMatch: number = 1;

  if (userId) {
    commentMust.push({ term: { 'user.id': userId } });
    informationShould.push(
      { term: { 'declarant.id': userId } },
      { terms: { 'colleguesConcernees.userConcernee.id': [userId] } },
    );
    todoShould.push(
      { term: { 'userCreation.id': userId } },
      { terms: { 'assignedUsers.id': [userId] } },
    );
  }

  // For information only
  if (statuts && statuts.length > 0) {
    const newTerm = { terms: { status: statuts } };
    informationMust.push(newTerm);
  }

  // For todo only
  if (statusTodo && statusTodo.length > 0) {
    const newTerm = { terms: { status: statusTodo } };
    todoMust.push(newTerm);
  }

  if (startDate && startDate !== '' && endDate && endDate !== '') {
    const newTerm = { range: { dateCreation: { gte: startDate, lte: endDate } } };
    commentMust.push(newTerm);
    informationMust.push(newTerm);
    todoMust.push(newTerm);
  }

  if (groupeAmisPharmacieMembres.length > 0) {
    const groupeAmisMembreIds = groupeAmisPharmacieMembres.map((i: any) => i.id);
    //  Comment
    commentMust.push({ terms: { 'user.pharmacie.id': groupeAmisMembreIds } });

    // Action
    informationShould.push(
      { terms: { 'userCreation.pharmacie.id': groupeAmisMembreIds } },
      { terms: { 'colleguesConcernees.userConcernee.pharmacie.id': groupeAmisMembreIds } },
    );

    // Action
    todoShould.push(
      { terms: { 'userCreation.pharmacie.id': groupeAmisMembreIds } },
      { terms: { 'assignedUsers.pharmacie.id': groupeAmisMembreIds } },
    );
  }

  const skipQuery = (): boolean => {
    if (isGroupeAmis && groupeAmisPharmacieMembres.length === 0) return true;
    if ((isLabo || isPartenaire) && !userId) return true;
    return false;
  };

  // Query to get comments
  const { data: commentData, loading: commentLoading } = useQuery<
    SHARED_COMMENTS,
    SHARED_COMMENTSVariables
  >(GET_SHARED_COMMENTS, {
    variables: {
      type: ['comment'],
      query: {
        query: {
          bool:
            commentShould.length > 0
              ? {
                must: commentMust,
                should: commentShould,
                minimum_should_match: minimumShouldMatch,
              }
              : { must: commentMust },
        },
      },
      sortBy: defaultSort,
    },
    skip: skipQuery(),
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const comments = (commentData && commentData.search && commentData.search.data) || [];

  // Query to get informations
  const { data: infoData, loading: infoLoading } = useQuery<
    SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON,
    SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISONVariables
  >(GET_SEARCH_CUSTOM_INFORMATION_LIAISON, {
    variables: {
      type: ['informationliaison'],
      query: {
        query: {
          bool:
            informationShould.length > 0
              ? {
                must: informationMust,
                should: informationShould,
                minimum_should_match: minimumShouldMatch,
              }
              : { must: informationMust },
        },
      },
      sortBy: defaultSort,
    },
    skip: skipQuery(),
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const informations = (infoData && infoData.search && infoData.search.data) || [];

  // Commenter : Selon retour de test
  // const resultInformations = informations.filter(info => info.typeInfo === 'ACTUALITE');
  const resultInformations = informations;
  const resultIncident = informations.filter((info: any) => info.typeInfo === 'INCIDENT');
  const resultImprovement = informations.filter((info: any) => info.typeInfo === 'AMELIORATION');
  const resultClaim = informations.filter((info: any) => info.typeInfo === 'RECLAMATION');

  // Query to get todo actions
  const { data: todoData, loading: todoLoading } = useQuery<SEARCH_ACTION, SEARCH_ACTIONVariables>(
    GET_SEARCH_ACTION,
    {
      variables: {
        type: ['action'],
        query: {
          query: {
            bool:
              todoShould.length > 0
                ? {
                  must: todoMust,
                  should: todoShould,
                  minimum_should_match: minimumShouldMatch,
                }
                : { must: todoMust },
          },
        },
        sortBy: defaultSort,
      },
      skip: skipQuery(),
      onError: error => {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );

  // Parsing data
  const todos = (todoData && todoData.search && todoData.search.data) || [];

  const goToInformationLiaisonDetail = id => {
    // push(`/intelligence-collective/cahier-de-liaison/viewdetails/${id}/partage-info`);
    push(`${CAHIER_LIAISON_URL}/${id}/partage-info`);
  };

  // tabs
  const tabs: TabInterface[] = [
    {
      id: 0,
      label: 'Commentaires',
      content: <SharedComments listResult={comments} />,
      clickHandlerParams: 'comment',
      count: comments.length,
    },
    {
      id: 1,
      label: 'Informations',
      content: (
        <SharedInfo listResult={resultInformations} goToDetail={goToInformationLiaisonDetail} />
      ),
      clickHandlerParams: 'informationliaison',
      count: resultInformations.length,
    },
    {
      id: 2,
      label: 'Incidents',
      content: (
        <SharedIncidents listResult={resultIncident} goToDetail={goToInformationLiaisonDetail} />
      ),
      clickHandlerParams: 'incidents',
      count: resultIncident.length,
    },
    {
      id: 3,
      label: 'Améliorations',
      content: (
        <SharedAmelioration
          listResult={resultImprovement}
          goToDetail={goToInformationLiaisonDetail}
        />
      ),
      clickHandlerParams: 'amelioration',
      count: resultImprovement.length,
    },
    {
      id: 4,
      label: 'Réclamations',
      content: (
        <SharedReclamation listResult={resultClaim} goToDetail={goToInformationLiaisonDetail} />
      ),
      clickHandlerParams: 'ticket',
      count: resultClaim.length,
    },
    {
      id: 5,
      label: 'Todo',
      content: <SharedTodo listResult={todos} isLoading={false} />,
      clickHandlerParams: 'action',
      count: todos.length,
    },
  ];

  // handle retour
  const handleGoBack = () => {
    if (typeItem === 'LABO') push(`${SHAREINFO_LABO_PARTENAIRE_URL}/${itemId}`);
    else if (typeItem === 'PSERVICE') push(`${SHAREINFO_PARTENAIRE_SERVICE_URL}/${itemId}`);
    else push(`${SHAREINFO_FRIEND_GROUP_URL}/${itemId}`);
  };

  const handleBackChoiceItem = () => {
    push(SHAREINFO_ITEMS_CHOICE_URL);
  };

  const handleTabClick = (param: string) => {
    window.history.pushState(null, '', `#${SHAREINFO_DETAILS_URL}/${item}/${param}/${itemId}`);
    setCurrentTab(param);

    if (param === 'comment') {
      setListResult([]);
      setClickComment(true);
      setClickTodo(false);
    } else if (param === 'action') {
      setListResult(todos);
      setClickComment(false);
      setClickTodo(true);
    } else if (param === 'informationliaison') {
      setListResult(resultInformations);
      setClickComment(false);
      setClickTodo(false);
    } else if (param === 'incidents') {
      setListResult(resultIncident);
      setClickComment(false);
      setClickTodo(false);
    } else if (param === 'amelioration') {
      setListResult(resultImprovement);
      setClickComment(false);
      setClickTodo(false);
    } else if (param === 'ticket') {
      setListResult(resultClaim);
      setClickComment(false);
      setClickTodo(false);
    }
  };

  const tabStep = [
    { index: 0, tab: 'comment' },
    { index: 1, tab: 'informationliaison' },
    { index: 2, tab: 'incidents' },
    { index: 3, tab: 'amelioration' },
    { index: 4, tab: 'ticket' },
    { index: 5, tab: 'action' },
  ];

  const activeStepItem = tabStep.find(tabItem => tabItem.tab === tab);
  const activeStep = activeStepItem && activeStepItem.index;

  return (
    <Box className={classes.container}>
      {(todoLoading ||
        commentLoading ||
        infoLoading ||
        groupAmisLoading ||
        laboLoading ||
        parteLoading) && <Backdrop />}

      <SubToolbar withBackBtn={true} onClickBack={handleGoBack} title="Partage des informations">
        <Button variant="contained" className={classes.btnGoItem} onClick={handleBackChoiceItem}>
          RETOUR AU CHOIX D'ITEM
        </Button>
      </SubToolbar>
      <Box height="calc(100vh - 157px)" overflow="auto" padding="24px">
        <Box marginBottom="16px">
          <Typography className={classes.title}>Résultat pour :</Typography>
          <Box display="flex">
            <Typography className={classes.titleItem}>
              {item === 'GROUPE_AMIS'
                ? "Groupe d'amis : "
                : item === 'PSERVICE'
                  ? 'Partenaire de service : '
                  : 'Laboratoire partenaire : '}
            </Typography>
            <Typography className={classes.titleItemValue}>{title}</Typography>
          </Box>
        </Box>
        <Box className={classes.fichePharmacieMainContent}>
          <CustomTabs
            hasBadge={true}
            activeStep={activeStep}
            tabs={tabs}
            clickHandler={handleTabClick}
            hideArrow={true}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default withRouter(PartageInfoDetailsMain);
