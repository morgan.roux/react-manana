import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    noStyle: {
      listStyleType: 'none',
      '& .MuiListItem-root': {
        padding: '4px 0',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
      },
      '& .MuiRadio-root': {
        padding: '0 8px 0 0!important',
      },
    },
    titleFilter: {
      fontSize: '0.875rem',
    },
    title: {
      fontSize: 20,
      fontWeight: 'bold',
      color: '#424242',
      padding: '14px 0 24px',
    },
  }),
);

export default useStyles;
