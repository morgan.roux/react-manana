import { FieldsOptions } from './Children/interfaces';

export const PARTNER_LABORATORY_FILTER_NAME: string = '0029';
export const PARTNER_LABORATORY_ADDRESS: string = '0030';
export const PARTNER_LABORATORY_FILTER_CP: string = '0031';
export const PARTNER_LABORATORY_FILTER_CITY: string = '0038';

const placeholder = 'Tapez ici';

/**
 * Search fields
 *
 */
export const SEARCH_FIELDS_OPTIONS: FieldsOptions[] = [
  {
    name: 'nomLabo',
    label: 'Nom Laboratoire',
    value: '',
    type: 'Search',
    placeholder,
    ordre: 1,
  },
  {
    name: 'adresse',
    label: 'Adresse',
    value: '',
    type: 'Search',
    extraNames: ['laboSuite.adresse'],
    placeholder,
    ordre: 2,
  },
  {
    name: 'codePostal',
    label: 'CP',
    value: '',
    type: 'Search',
    extraNames: ['laboSuite.codePostal'],
    filterType: 'StartsWith',
    placeholder,
    ordre: 3,
  },
  {
    name: 'city',
    label: 'Ville',
    value: '',
    type: 'Search',
    extraNames: ['laboSuite.ville'],
    placeholder,
    ordre: 4,
  }
];
