import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import CahierLiaisonImage from '../../../../assets/img/intelligence_collective/cahier_de _iaison.png';
import PartageIdeeImage from '../../../../assets/img/intelligence_collective/partage_idee_bonne_pratique.png';
import PartageInfoImage from '../../../../assets/img/intelligence_collective/partage_information.png';
import {
  CAHIER_LIAISON_URL,
  PARTAGE_IDEE_BONNE_PRATIQUE_URL,
  PARTAGE_INFO_URL,
  SHAREINFO_ITEMS_CHOICE_URL,
} from '../../../../Constant/url';
import ChoicePage, { ChoicePageItem } from '../../../Common/ChoicePage/ChoicePage';
import CahierLiaison from './CahierLiaison';
import PartageIdeeBonnePratique from './PartageIdeeBonnePratique';

const IntelligenceCollective: FC<RouteComponentProps> = ({
  history: { push },
  location: { pathname },
}) => {
  const isOnCachierLiaison = pathname.startsWith(CAHIER_LIAISON_URL);

  const isOnPartageIdee = pathname.startsWith(PARTAGE_IDEE_BONNE_PRATIQUE_URL);

  console.log({ pathname, PARTAGE_INFO_URL });
  console.log(pathname.startsWith(PARTAGE_INFO_URL));

  const handleGotoCahierLiaison = () => push(CAHIER_LIAISON_URL);

  const handleGoToPartageInfo = () => push(SHAREINFO_ITEMS_CHOICE_URL);

  const handleGotoPartageIdee = () => push(PARTAGE_IDEE_BONNE_PRATIQUE_URL);
  const resetFilters = () => {};

  const items: ChoicePageItem[] = [
    { text: 'Cahier de liaison', img: CahierLiaisonImage, onClick: handleGotoCahierLiaison },
    { text: 'Partage des informations', img: PartageInfoImage, onClick: handleGoToPartageInfo },
    {
      text: 'Partage des idées ou bonnes pratiques',
      img: PartageIdeeImage,
      onClick: handleGotoPartageIdee,
    },
  ];

  if (isOnCachierLiaison) {
    return <CahierLiaison />;
  }

  if (isOnPartageIdee) {
    return <PartageIdeeBonnePratique />;
  }

  return <ChoicePage items={items} />;
};

export default withRouter(IntelligenceCollective);
