import React, { Dispatch, FC, SetStateAction } from 'react';
import useStyles from './styles';
import classnames from 'classnames';
import { InputAdornment, IconButton, Menu, MenuItem, Box } from '@material-ui/core';
import { Search, Refresh, Sort, MoreVert, Add } from '@material-ui/icons';
import { CustomFormTextField } from '../../../../../../Common/CustomTextField';
import Item from './Item';
import {
  FilterProps,
  PartageIdeeBonnePratiqueMainProps,
  QueryProps,
} from '../PartageIdeeBonnePratiqueMain';
import { ActiveItemProps } from './Item/Item';
import { defaultQueryVariables } from '../../PartageIdeeBonnePratique';
import { uniqBy } from 'lodash';
import CustomButton from '../../../../../../Common/CustomButton';
import { PARTAGE_IDEE_BONNE_PRATIQUE_URL } from '../../../../../../../Constant/url';
import { RouteComponentProps, withRouter } from 'react-router';

export interface LeftContainerProps extends QueryProps, FilterProps {
  searchKeyWord: string;
  setSearchKeyWord: Dispatch<SetStateAction<string>>;
  fetchMore: any;
  onClickAdd?: any;
}

const LeftContainer: FC<RouteComponentProps &
  PartageIdeeBonnePratiqueMainProps &
  LeftContainerProps &
  ActiveItemProps> = ({
  data,
  searchKeyWord,
  setSearchKeyWord,
  setActiveItem,
  activeItem,
  setQueryVariables,
  queryVariables,
  filter,
  setFilter,
  fetchMore,
  onClickAdd,
  history: { push },
}) => {
  const classes = useStyles({});

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const list = (data && data.search && data.search.data) || [];
  const total = (data && data.search && data.search.total) || 0;

  const realList: any[] = uniqBy([...list], 'id') || [];

  React.useEffect(() => {
    if (!activeItem && realList && realList.length && realList[0]) {
      window.history.pushState(
        null,
        '',
        `#${PARTAGE_IDEE_BONNE_PRATIQUE_URL}/${realList[0] && realList[0].id}`,
      );
      setActiveItem(realList[0]);
    }
  }, [realList]);

  const handleClickMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClickMenuItem = (value: string) => () => {
    handleClose();
    setFilter(prev => ({ ...prev, sort: value }));
  };

  const handleChange = (event: React.ChangeEvent<any>) => {
    setSearchKeyWord(event.target.value);
  };

  const resetQueryVariables = () => {
    setQueryVariables(defaultQueryVariables);
    setSearchKeyWord('');
    setFilter({
      checkedClassification: [],
      checkedOrigine: [],
      checkedStatus: [],
      date: null,
      order: 'desc',
      sort: 'dateCreation',
    });
  };

  const handleToggleOrder = () => {
    if (filter.order === 'asc') {
      setFilter(prev => ({ ...prev, order: 'desc' }));
    } else {
      setFilter(prev => ({ ...prev, order: 'asc' }));
    }
  };

  const el = document.getElementById('leftContainerId');
  if (el && realList.length > 0 && realList.length < total) {
    el.addEventListener('scroll', () => {
      if (el.scrollHeight - el.scrollTop === el.clientHeight) {
        if (fetchMore) {
          fetchMore({
            variables: { ...queryVariables, skip: realList.length },
            updateQuery: (prev: any, { fetchMoreResult }) => {
              if (!fetchMoreResult) return prev;
              const prevData = (prev.search && prev.search.data) || [];
              const nextData =
                (fetchMoreResult && fetchMoreResult.search && fetchMoreResult.search.data) || [];
              const newData = [...prevData, ...nextData];
              return { ...prev, search: { ...prev.search, data: newData } } as any;
            },
          });
        }
      }
    });
  }

  return (
    <Box className={classes.leftContainer}>
      <Box display="flex" alignItems="center" justifyContent="center" paddingTop={4}>
        <CustomButton color="secondary" onClick={onClickAdd} startIcon={<Add />}>
          Ajouter une idée ou pratique
        </CustomButton>
      </Box>

      <Box className={classnames(classes.mainContainerHeader, classes.leftContainerHeader)}>
        <CustomFormTextField
          placeholder="Rechercher..."
          value={searchKeyWord}
          onChange={handleChange}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <Search />
              </InputAdornment>
            ),
          }}
        />
        <IconButton size="small" onClick={resetQueryVariables}>
          <Refresh />
        </IconButton>
        <IconButton size="small" onClick={handleToggleOrder}>
          <Sort />
        </IconButton>
        <IconButton size="small" onClick={handleClickMenu}>
          <MoreVert />
        </IconButton>
        <Menu
          id="header-simple-menu"
          anchorEl={anchorEl}
          keepMounted={true}
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem onClick={handleClickMenuItem('title')}>Trier par titre</MenuItem>
          <MenuItem onClick={handleClickMenuItem('dateCreation')}>
            Trier par date de création
          </MenuItem>
        </Menu>
      </Box>
      <Box className={classes.itemsContainer} id="leftContainerId">
        {realList.length > 0 ? (
          realList.map((item, index) => {
            return (
              <Item key={index} item={item} setActiveItem={setActiveItem} activeItem={activeItem} />
            );
          })
        ) : (
          <Box
            height="50vh"
            width="100%"
            display="flex"
            alignItems="center"
            justifyContent="center"
          >
            Aucune données
          </Box>
        )}
      </Box>
    </Box>
  );
};

export default withRouter(LeftContainer);
