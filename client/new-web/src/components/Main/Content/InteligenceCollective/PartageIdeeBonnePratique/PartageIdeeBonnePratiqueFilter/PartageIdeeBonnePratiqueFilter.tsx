import { useApolloClient, useLazyQuery, useQuery } from '@apollo/client';
import {
  Box,
  Checkbox,
  Collapse,
  Divider,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText,
  Typography,
} from '@material-ui/core';
import classnames from 'classnames';
import { ArrowBack, ExpandLess, ExpandMore } from '@material-ui/icons';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import moment from 'moment';
import React, { FC, Fragment, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import {
  DO_SEARCH_IDEE_BONNE_PRATIQUE,
  GET_IDEE_CLASSIFICATIONS,
} from '../../../../../../graphql/IdeeBonnePratique';
import {
  GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONS,
  GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONSVariables,
} from '../../../../../../graphql/IdeeBonnePratique/types/GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONS';
import {
  SEARCH_IDEE_BONNE_PRATIQUE,
  SEARCH_IDEE_BONNE_PRATIQUEVariables,
} from '../../../../../../graphql/IdeeBonnePratique/types/SEARCH_IDEE_BONNE_PRATIQUE';
import { getGroupement } from '../../../../../../services/LocalStorage';
import {
  IdeeOuBonnePratiqueOrigine,
  IdeeOuBonnePratiqueStatus,
} from '../../../../../../types/graphql-global-types';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import { CustomDatePicker } from '../../../../../Common/CustomDateTimePicker';
import { defaultFilterBy, defaultQueryVariables } from '../PartageIdeeBonnePratique';
import { FilterProps } from '../PartageIdeeBonnePratiqueMain/PartageIdeeBonnePratiqueMain';
import useStyles from './styles';

const PartageIdeeBonnePratiqueFilter: FC<RouteComponentProps & FilterProps> = ({
  history: { push },
  filter,
  setFilter,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();

  const groupement = getGroupement();
  const idGroupement = (groupement && groupement.id) || '';

  const [expandedMore, setExpandedMore] = useState({
    periode: true,
    status: true,
    origine: true,
    classification: true,
  });

  const { checkedStatus, checkedClassification, checkedOrigine, date } = filter;

  const handleCheck = (
    value: IdeeOuBonnePratiqueStatus | IdeeOuBonnePratiqueOrigine | string,
    name: 'checkedStatus' | 'checkedClassification' | 'checkedOrigine',
  ) => () => {
    const currentIndex = filter[name].indexOf(value as never);
    const newChecked = [...filter[name]];
    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    setFilter(prev => ({ ...prev, [name]: newChecked }));
  };

  const [queryVariables, setQueryVariables] = React.useState<SEARCH_IDEE_BONNE_PRATIQUEVariables>(
    defaultQueryVariables,
  );

  const [searchIdeeOuPratique, { data }] = useLazyQuery<
    SEARCH_IDEE_BONNE_PRATIQUE,
    SEARCH_IDEE_BONNE_PRATIQUEVariables
  >(DO_SEARCH_IDEE_BONNE_PRATIQUE, {
    variables: { ...queryVariables, take: null },
    fetchPolicy: 'cache-and-network',
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  React.useEffect(() => {
    searchIdeeOuPratique({
      variables: {
        ...queryVariables,
        take: null,
        filterBy: [...queryVariables.filterBy, { term: { 'groupement.id': idGroupement } }],
      },
    });
  }, []);

  React.useEffect(() => {
    searchIdeeOuPratique({
      variables: {
        ...queryVariables,
        take: null,
        filterBy: [...queryVariables.filterBy, { term: { 'groupement.id': idGroupement } }],
      },
    });
  }, [queryVariables]);

  // filter
  React.useEffect(() => {
    const newFilterBy: any[] = [];
    if (filter.date) {
      const dateFilter = { term: { dateCreation: moment(filter.date).format('YYYY-MM-DD') } };
      newFilterBy.push(dateFilter);
    }
    const newVars: SEARCH_IDEE_BONNE_PRATIQUEVariables = {
      ...queryVariables,
      filterBy: [...defaultFilterBy, ...newFilterBy],
    };
    setQueryVariables(newVars);
  }, [filter]);

  const { data: classifData } = useQuery<
    GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONS,
    GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONSVariables
  >(GET_IDEE_CLASSIFICATIONS, {
    variables: { isRemoved: false },
    fetchPolicy: 'cache-and-network',
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const d = (data && data.search && data.search.data) || [];

  const nbNouvelle = d.filter((i: any) => i.status === IdeeOuBonnePratiqueStatus.NOUVELLE).length;
  const nbEnCours = d.filter((i: any) => i.status === IdeeOuBonnePratiqueStatus.EN_COURS).length;
  const nbValidee = d.filter((i: any) => i.status === IdeeOuBonnePratiqueStatus.VALIDEE).length;
  const nbRejete = d.filter((i: any) => i.status === IdeeOuBonnePratiqueStatus.REJETEE).length;

  const nbInterne = d.filter((i: any) => i._origine === IdeeOuBonnePratiqueOrigine.INTERNE).length;
  const nbPatient = d.filter((i: any) => i._origine === IdeeOuBonnePratiqueOrigine.PATIENT).length;
  const nbGroupeAmis = d.filter((i: any) => i._origine === IdeeOuBonnePratiqueOrigine.GROUPE_AMIS)
    .length;
  const nbGroupement = d.filter((i: any) => i._origine === IdeeOuBonnePratiqueOrigine.GROUPEMENT)
    .length;
  const nbFournisseur = d.filter((i: any) => i._origine === IdeeOuBonnePratiqueOrigine.FOURNISSEUR)
    .length;
  const nbPrestataire = d.filter((i: any) => i._origine === IdeeOuBonnePratiqueOrigine.PRESTATAIRE)
    .length;

  const [classifList, setClassifList] = useState<any[]>([]);

  // set classif list
  React.useMemo(() => {
    if (classifData && classifData.ideeOuBonnePratiqueClassifications) {
      const classif = classifData.ideeOuBonnePratiqueClassifications;
      const classifFomatted = classif
        .filter((el: any) => el.parent === null)
        .map((item: any) => {
          return {
            label: item.nom,
            value: item.id,
            count: d.filter((i: any) => i.classification && i.classification.id === item.id).length,
            open: true,
            childs: item.childs.map((chil: any) => {
              return {
                label: chil.nom,
                value: chil.id,
                count: d.filter((i: any) => i.classification && i.classification.id === chil.id)
                  .length,
              };
            }),
          };
        });
      setClassifList(classifFomatted);
    }
  }, [classifData]);

  const statusList = [
    { label: 'Nouvelle', value: IdeeOuBonnePratiqueStatus.NOUVELLE, count: nbNouvelle },
    { label: 'En cous', value: IdeeOuBonnePratiqueStatus.EN_COURS, count: nbEnCours },
    { label: 'Validée', value: IdeeOuBonnePratiqueStatus.VALIDEE, count: nbValidee },
    { label: 'Rejetée', value: IdeeOuBonnePratiqueStatus.REJETEE, count: nbRejete },
  ];

  const origineList = [
    { label: 'Interne', value: IdeeOuBonnePratiqueOrigine.INTERNE, count: nbInterne },
    { label: 'Patient', value: IdeeOuBonnePratiqueOrigine.PATIENT, count: nbPatient },
    { label: "Groupe d'amis", value: IdeeOuBonnePratiqueOrigine.GROUPE_AMIS, count: nbGroupeAmis },
    { label: 'Groupement', value: IdeeOuBonnePratiqueOrigine.GROUPEMENT, count: nbGroupement },
    { label: 'Fournisseur', value: IdeeOuBonnePratiqueOrigine.FOURNISSEUR, count: nbFournisseur },
    { label: 'Prestataire', value: IdeeOuBonnePratiqueOrigine.PRESTATAIRE, count: nbPrestataire },
  ];

  const handleToggle = (name: 'periode' | 'status' | 'origine' | 'classification') => () => {
    setExpandedMore(prev => ({ ...prev, [name]: !prev[name] }));
  };

  const handleDateFilterChange = (date: MaterialUiPickersDate) => {
    if (date) setFilter(prev => ({ ...prev, date }));
  };

  const handleToggleClassif = (item: any) => () => {
    setClassifList(prev =>
      prev.map(i => {
        if (i.id === item.id) {
          return { ...i, open: !i.open };
        } else {
          return i;
        }
      }),
    );
  };

  const handleBack = () => push('/intelligence-collective');

  return (
    <>
      <Box display="flex" alignItems="center">
        <IconButton size="small" onClick={handleBack}>
          <ArrowBack />
        </IconButton>
        <Typography className={classes.toolbarTitle}>
          Partage des idées ou bonnes pratiques
        </Typography>
      </Box>
      <Divider orientation="horizontal" className={classes.divider} />
      <Box className={classes.filterContainer}>
        <Typography className={classes.fontSize14}>Période</Typography>
        <IconButton size="small" onClick={handleToggle('periode')}>
          {expandedMore.periode ? <ExpandLess /> : <ExpandMore />}
        </IconButton>
      </Box>
      {expandedMore.periode && <CustomDatePicker onChange={handleDateFilterChange} value={date} />}

      {classifList.length > 0 && (
        <Fragment>
          {/* Classification */}
          <Divider orientation="horizontal" className={classes.divider} />
          <Box className={classes.filterContainer}>
            <Typography className={classes.fontSize14}>Classification</Typography>
            <IconButton size="small" onClick={handleToggle('classification')}>
              {expandedMore.classification ? <ExpandLess /> : <ExpandMore />}
            </IconButton>
          </Box>
          <Box className={classes.noStyle}>
            {expandedMore.classification && (
              <List className={classes.root} dense={true}>
                {classifList.map((item, index) => {
                  const labelId = `checkbox-classification-list-label-${index}`;
                  return (
                    <Fragment key={index}>
                      <ListItem
                        key={`${index}_list_item`}
                        role={undefined}
                        dense={true}
                        button={true}
                        onClick={handleCheck(item.value, 'checkedClassification')}
                      >
                        <ListItemIcon>
                          <Checkbox
                            edge="start"
                            checked={checkedClassification.indexOf(item.value) !== -1}
                            tabIndex={-1}
                            disableRipple={true}
                            inputProps={{ 'aria-labelledby': labelId }}
                          />
                          <ListItemText id={labelId} primary={item.label} />
                        </ListItemIcon>

                        <ListItemSecondaryAction className={classes.listItemSecondaryAction}>
                          <Typography className={classes.count}>{item.count}</Typography>
                        </ListItemSecondaryAction>
                        <Box>
                          {item.childs && item.childs.length > 0 && (
                            <IconButton size="small" onClick={handleToggleClassif(item)}>
                              {item.open ? <ExpandLess /> : <ExpandMore />}
                            </IconButton>
                          )}
                        </Box>
                      </ListItem>
                      {item.childs.length > 0 && (
                        <Collapse in={item.open} timeout="auto" unmountOnExit={true}>
                          <List component="div" disablePadding={true}>
                            {item.childs.map((child, index) => {
                              return (
                                <ListItem
                                  button={true}
                                  key={index}
                                  onClick={handleCheck(child.value, 'checkedClassification')}
                                  className={classnames(classes.nested, classes.subListItem)}
                                >
                                  <ListItemIcon>
                                    <Checkbox
                                      edge="start"
                                      checked={checkedClassification.indexOf(child.value) !== -1}
                                      tabIndex={-1}
                                      disableRipple={true}
                                    />
                                    <ListItemText primary={child.label} />
                                  </ListItemIcon>

                                  <ListItemSecondaryAction
                                    className={classnames(classes.margeCount, classes.count)}
                                  >
                                    {child.count}
                                  </ListItemSecondaryAction>
                                </ListItem>
                              );
                            })}
                          </List>
                        </Collapse>
                      )}
                    </Fragment>
                  );
                })}
              </List>
            )}
          </Box>
        </Fragment>
      )}

      {/* Origine */}
      <Divider orientation="horizontal" className={classes.divider} />
      <Box className={classes.filterContainer}>
        <Typography className={classes.fontSize14}>Origine</Typography>
        <IconButton size="small" onClick={handleToggle('origine')}>
          {expandedMore.origine ? <ExpandLess /> : <ExpandMore />}
        </IconButton>
      </Box>
      <Box className={classes.noStyle}>
        {expandedMore.origine && (
          <List className={classes.root} dense={true}>
            {origineList.map((item, index) => {
              const labelId = `checkbox-origine-list-label-${index}`;
              return (
                <ListItem
                  key={index}
                  role={undefined}
                  dense={true}
                  button={true}
                  onClick={handleCheck(item.value, 'checkedOrigine')}
                >
                  <ListItemIcon>
                    <Checkbox
                      edge="start"
                      checked={checkedOrigine.indexOf(item.value) !== -1}
                      tabIndex={-1}
                      disableRipple={true}
                      inputProps={{ 'aria-labelledby': labelId }}
                    />
                    <ListItemText id={labelId} primary={item.label} />
                  </ListItemIcon>

                  <ListItemSecondaryAction className={classes.count}>
                    {item.count}
                  </ListItemSecondaryAction>
                </ListItem>
              );
            })}
          </List>
        )}
      </Box>
      {/* Status */}
      <Divider orientation="horizontal" className={classes.divider} />
      <Box className={classes.filterContainer}>
        <Typography className={classes.fontSize14}>Statut</Typography>
        <IconButton size="small" onClick={handleToggle('status')}>
          {expandedMore.status ? <ExpandLess /> : <ExpandMore />}
        </IconButton>
      </Box>
      <Box className={classes.noStyle}>
        {expandedMore.status && (
          <List className={classes.root} dense={true}>
            {statusList.map((item, index) => {
              const labelId = `checkbox-status-list-label-${index}`;
              return (
                <ListItem
                  key={index}
                  role={undefined}
                  dense={true}
                  button={true}
                  onClick={handleCheck(item.value, 'checkedStatus')}
                >
                  <ListItemIcon>
                    <Checkbox
                      edge="start"
                      checked={checkedStatus.indexOf(item.value) !== -1}
                      tabIndex={-1}
                      disableRipple={true}
                      inputProps={{ 'aria-labelledby': labelId }}
                    />
                    <ListItemText id={labelId} primary={item.label} />
                  </ListItemIcon>

                  <ListItemSecondaryAction className={classes.count}>
                    {item.count}
                  </ListItemSecondaryAction>
                </ListItem>
              );
            })}
          </List>
        )}
      </Box>
    </>
  );
};

export default withRouter(PartageIdeeBonnePratiqueFilter);
