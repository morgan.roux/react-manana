import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import { calculateBorderBoxPath } from 'html2canvas/dist/types/render/bound-curves';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      overflow: 'hidden',
      height: 'calc(100vh - 93px)',
      '& .MuiDialogActions-root': {
        justifyContent: 'center',
      },
    },
    leftContainer: {
      width: 408,
      minHeight: 'calc(100vh - 86px)',
      borderRight: '1px solid #E5E5E5',
    },
    leftContainerHeader: {
      justifyContent: 'space-between',
      '& .MuiFormControl-root': {
        marginBottom: 0,
      },
    },
    mainContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    mainContainerHeader: {
      width: '100%',
      height: 76,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-end',
      borderBottom: '1px solid #E5E5E5',
      padding: '0px 20px',
      position: 'sticky',
      top: 0,
      background: '#fff',
      zIndex: 1,
    },
    mainContainerHeaderBtns: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      '& > button:nth-child(2)': {
        marginLeft: 10,
      },
    },
  }),
);

export default useStyles;
