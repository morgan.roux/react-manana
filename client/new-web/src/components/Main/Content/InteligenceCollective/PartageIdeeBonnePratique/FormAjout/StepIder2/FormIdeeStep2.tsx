import { Box, Typography } from '@material-ui/core';
import React, { FC, useState } from 'react';
import ReactQuill from 'react-quill';
import { RouteComponentProps, withRouter } from 'react-router';
import { State } from '../StepperIdea';
import useStyles from './styles';

interface IFormProps {
  handleChangeCommentaire: (content: string, name: string) => void;
  values: State;
}

const FormIdeeStep2: FC<IFormProps & RouteComponentProps> = ({
  handleChangeCommentaire,
  values,
}) => {
  const classes = useStyles({});

  return (
    <Box className={classes.root}>
      <Box className={classes.container}>
        <Typography className={classes.inputTitle1}>Bénéficiaires clés</Typography>
        <ReactQuill
          theme="snow"
          value={values.beneficiaire}
          onChange={content => {
            handleChangeCommentaire(content, 'beneficiaire');
          }}
          className={classes.hauteur}
        />
      </Box>
      <Box className={classes.container}>
        <Typography className={classes.inputTitle}> Contexte</Typography>
        <ReactQuill
          className={classes.hauteur}
          theme="snow"
          value={values.context}
          onChange={content => {
            handleChangeCommentaire(content, 'context');
          }}
        />
      </Box>
      <Box className={classes.container}>
        <Typography className={classes.inputTitle}>Objectifs</Typography>
        <ReactQuill
          className={classes.hauteur}
          theme="snow"
          value={values.objectif}
          onChange={content => {
            handleChangeCommentaire(content, 'objectif');
          }}
        />
      </Box>
    </Box>
  );
};

export default withRouter(FormIdeeStep2);
