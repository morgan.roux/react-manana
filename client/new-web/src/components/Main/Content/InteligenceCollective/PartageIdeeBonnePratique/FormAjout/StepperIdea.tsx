import { Add } from '@material-ui/icons';
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { PARTAGE_IDEE_BONNE_PRATIQUE_URL } from '../../../../../../Constant/url';
import CustomButton from '../../../../../Common/CustomButton';
import SubToolbar from '../../../../../Common/newCustomContent/SubToolbar';
import Stepper, { Step } from '../../../../../Common/Stepper/Stepper';
import useStyles from './StepIder1/styles';
import FormIdeeStep1 from './StepIder1/FormIdeeStep1';
import FormIdeeStep2 from './StepIder2/FormIdeeStep2';
import FormIdeeStep3 from './StepIder3/FormIdeeStep3';
import { Box } from '@material-ui/core';
import { __InputValue } from 'graphql';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import { useApolloClient, useMutation, useQuery } from '@apollo/client';
import { DO_CREATE_UPDATE_IDEE_BONNE_PRATIQUE } from '../../../../../../graphql/IdeeBonnePratique/mutation';
import {
  CREATE_UPDATE_IDEE_BONNE_PRATIQUE,
  CREATE_UPDATE_IDEE_BONNE_PRATIQUEVariables,
  CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_fournisseur,
} from '../../../../../../graphql/IdeeBonnePratique/types/CREATE_UPDATE_IDEE_BONNE_PRATIQUE';
import {
  SEARCH_IDEE_BONNE_PRATIQUE,
  SEARCH_IDEE_BONNE_PRATIQUEVariables,
} from '../../../../../../graphql/IdeeBonnePratique/types/SEARCH_IDEE_BONNE_PRATIQUE';
import { FichierInput, TypeFichier } from '../../../../../../types/graphql-global-types';
import { LaboratoireInterface } from '../../../Actualite/Interface';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { formatFilenameWithoutDate } from '../../../../../../utils/filenameFormater';
import { uploadToS3 } from '../../../../../../services/S3';
import { AxiosResponse } from 'axios';
import SnackVariableInterface from '../../../../../../Interface/SnackVariableInterface';
import { getGroupement } from '../../../../../../services/LocalStorage';
import Backdrop from '../../../../../Common/Backdrop';
import { uniqBy } from 'lodash';
import NoItemContentImage from '../../../../../Common/NoItemContentImage';
import mutationSuccessImg from '../../../../../../assets/img/mutation-success.png';
import {
  DO_SEARCH_IDEE_BONNE_PRATIQUE,
  GET_IDEE_BONNE_PRATIQUE,
} from '../../../../../../graphql/IdeeBonnePratique';
import {
  IDEE_BONNE_PRATIQUE,
  IDEE_BONNE_PRATIQUEVariables,
} from '../../../../../../graphql/IdeeBonnePratique/types/IDEE_BONNE_PRATIQUE';

export interface State {
  id: string | null;
  origine: any;
  classification: any;
  title: string;
  files: FichierInput[] | [];
  idAuteur: any;
  concurrence: string;
  fournisseur: any;
  prestataire: any;
  groupesamis: any;
  beneficiaire: string | any;
  context: string | any;
  objectif: string | any;
  resultats: string | any;
  succ: string | any;
  contrainte: string | any;
  status: any;
  nameAuteur: string;
  service: any;
}

export const initialState: State = {
  id: null,
  origine: '',
  classification: '',
  title: '',
  files: [],
  idAuteur: '',
  concurrence: '',
  fournisseur: '',
  prestataire: '',
  groupesamis: '',
  beneficiaire: '',
  context: null,
  objectif: null,
  resultats: '',
  succ: '',
  contrainte: '',
  nameAuteur: '',
  status: 'NOUVELLE',
  service: '',
};
interface InfoProps {
  baseUrl;
  match?: {
    params: {
      ideeId: string | undefined;
    };
  };
}

const FormIdea: FC<InfoProps & RouteComponentProps> = props => {
  const {
    history,
    location: { pathname },
    baseUrl,
    match,
  } = props;
  const classes = useStyles({});

  const isOnCreate = pathname === `${baseUrl}/create`;
  const isOnEdit: boolean = pathname.startsWith(`${baseUrl}/edit`);
  const goBack = () => history.push(`${baseUrl}`);

  const idpratique = match && match.params && match.params.ideeId;

  const mutationSuccessTitle = `${isOnCreate ? 'Ajout' : 'Modification'} de l'idée réussie`;
  const mutationSuccessSubTitle = `${isOnCreate
    ? "La nouvelle idée que vous venez d'ajouter est désormais dans la liste."
    : 'La nouvelle idée que vous venez de modifier est désormais dans la liste'
    } `;

  const [values, setValues] = useState<State>(initialState);
  const [loading, setLoading] = useState<boolean>(false);

  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);
  const [mutationSuccess, setMutationSuccess] = useState<boolean>(false);
  const [disabledfournisseur, setDisabledfournisseur] = useState<boolean>(true);
  const [disabledprestaire, setDisabledprestaire] = useState<boolean>(true);
  const [disabledgroupeAmis, setDisabledgroupeAmis] = useState<boolean>(true);
  const [disabledgroupement, setDisabledgroupement] = useState<boolean>(true);
  const [activeStep, setActiveStep] = useState<number | undefined>(0);
  let files: FichierInput[] = [];
  let uploadResult: AxiosResponse<any> | null = null;
  const groupement = getGroupement();
  const goToHome = () => history.push(`${baseUrl}`);

  const infoideepratique = useQuery<IDEE_BONNE_PRATIQUE, IDEE_BONNE_PRATIQUEVariables>(
    GET_IDEE_BONNE_PRATIQUE,
    {
      variables: { id: idpratique || '' },
    },
  );

  const [oldFiles, setOldFiles] = useState<FichierInput[]>([]);

  useEffect(() => {
    const val =
      infoideepratique.data &&
      infoideepratique.data.ideeOuBonnePratique &&
      infoideepratique.data.ideeOuBonnePratique;
    const newFichierPresentation: FichierInput[] = [];
    const fakeFiles: File[] = [];
    if (val && val.ideeOuBonnePratiqueFichierJoints) {
      val.ideeOuBonnePratiqueFichierJoints.map(item => {
        if (
          item &&
          item.fichier &&
          item.fichier.chemin &&
          item.fichier.nomOriginal &&
          item.fichier.type
        ) {
          const file: FichierInput = {
            chemin: item.fichier.chemin,
            type: item.fichier.type as any,
            nomOriginal: item.fichier.nomOriginal,
          };
          const fakeFile: File = { name: item.fichier.nomOriginal } as any;

          if (!newFichierPresentation.map(elem => elem.chemin).includes(item.fichier.chemin)) {
            newFichierPresentation.push(file);
          }
          if (!fakeFiles.map(elem => elem.name).includes(item.fichier.nomOriginal)) {
            fakeFiles.push(fakeFile);
          }
        }
      });
    }
    setSelectedFiles(fakeFiles);
    setOldFiles(newFichierPresentation);

    switch (val && val._origine) {
      case 'FOURNISSEUR':
        setDisabledfournisseur(false);
        break;
      case 'GROUPEMENT':
        setDisabledgroupement(false);
        break;
      case 'PRESTATAIRE':
        setDisabledprestaire(false);
        break;
      case 'GROUPE_AMIS':
        setDisabledgroupeAmis(false);
        break;
      default:
        break;
    }
    setValues(prevState => ({
      ...prevState,
      id: idpratique || '',
      origine: val && val._origine,
      classification: val && val.classification,
      title: (val && val.title) || '',
      file: selectedFiles,
      idAuteur: (val && val.auteur.id) || '',
      concurrence: (val && val.concurent) || '',
      fournisseur: (val && val.fournisseur) || '',
      prestataire: (val && val.prestataire) || '',
      groupesamis: (val && val.groupeAmis) || '',
      beneficiaire: (val && val.beneficiaires_cles) || '',
      context: (val && val.contexte) || '',
      objectif: (val && val.objectifs) || '',
      resultats: (val && val.resultats) || '',
      succ: (val && val.facteursClesDeSucces) || '',
      contrainte: (val && val.contraintes) || '',
      nameAuteur: (val && val.auteur && val.auteur.userName) || '',
      service: val && val.service,
    }));
    // setSelectedFiles()
  }, [infoideepratique.data]);

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      if ((name === 'origine' && value === 'INTERNE') || value === 'PATIENT') {
        setDisabledfournisseur(true);
        setDisabledprestaire(true);
        setDisabledgroupeAmis(true);
        setDisabledgroupement(true);
        setValues(prevState => ({
          ...prevState,
          fournisseur: '',
          service: '',
          prestataire: '',
          groupesamis: '',
        }));
      } else if (name === 'origine' && value === 'FOURNISSEUR') {
        setDisabledfournisseur(false);
        setDisabledprestaire(true);
        setDisabledgroupeAmis(true);
        setDisabledgroupement(true);
        setValues(prevState => ({
          ...prevState,
          service: '',
          prestataire: '',
          groupesamis: '',
        }));
      } else if (name === 'origine' && value === 'GROUPE_AMIS') {
        setDisabledfournisseur(true);
        setDisabledprestaire(true);
        setDisabledgroupeAmis(false);
        setDisabledgroupement(true);
        setValues(prevState => ({
          ...prevState,
          fournisseur: '',
          service: '',
          prestataire: '',
        }));
      } else if (name === 'origine' && value === 'PRESTATAIRE') {
        setDisabledfournisseur(true);
        setDisabledprestaire(false);
        setDisabledgroupeAmis(true);
        setDisabledgroupement(true);
        setValues(prevState => ({
          ...prevState,
          fournisseur: '',
          service: '',
          groupesamis: '',
        }));
      } else if (name === 'origine' && value === 'GROUPEMENT') {
        setDisabledfournisseur(true);
        setDisabledprestaire(true);
        setDisabledgroupeAmis(true);
        setDisabledgroupement(false);
        setValues(prevState => ({
          ...prevState,
          fournisseur: '',
          prestataire: '',
          groupesamis: '',
        }));
      }
      setValues(prevState => ({
        ...prevState,
        [name]: value,
      }));
    }
    console.log(values);
  };

  const handleChangeCommentaire = (content: string, name: string) => {
    setValues(prevState => ({ ...prevState, [name]: content }));
  };

  const handleChangeClassificationAutocomplete = (inputValue: any) => {
    setValues(prevState => ({ ...prevState, classification: inputValue }));
  };

  const handleChangeFournisseurAutocomplete = (inputValue: string) => {
    setValues(prevState => ({ ...prevState, fournisseur: inputValue }));
  };
  const handleChangePrestataireAutocomplete = (inputValue: string) => {
    setValues(prevState => ({ ...prevState, prestataire: inputValue }));
  };

  const handleChangeServiceAutocomplete = (inputValue: string) => {
    setValues(prevState => ({ ...prevState, service: inputValue }));
  };

  const handleChangeAuteursAutocomplete = user => {
    setValues(prevState => ({ ...prevState, idAuteur: user }));
  };
  const handleChangeGroupeAmisAutocomplete = (inputValue: string) => {
    setValues(prevState => ({ ...prevState, groupesamis: inputValue }));
  };

  const steps: Step[] = [
    {
      title: 'Information de base',
      content: (
        <FormIdeeStep1
          {...{
            handleChange,
            values,
            disabledfournisseur,
            disabledprestaire,
            disabledgroupeAmis,
            disabledgroupement,
            handleChangeClassificationAutocomplete,
            handleChangeFournisseurAutocomplete,
            handleChangePrestataireAutocomplete,
            handleChangeAuteursAutocomplete,
            handleChangeGroupeAmisAutocomplete,
            selectedFiles,
            setSelectedFiles,
            handleChangeServiceAutocomplete,
          }}
        />
      ),
    },
    {
      title: 'Détails',
      content: (
        <FormIdeeStep2
          {...{
            handleChangeCommentaire,
            values,
          }}
        />
      ),
    },
    {
      title: 'Finalités',
      content: (
        <FormIdeeStep3
          {...{
            handleChangeCommentaire,
            values,
          }}
        />
      ),
    },
  ];

  const disablebutton = () => {
    if (
      (activeStep === 0 && values.title === '') ||
      values.idAuteur === '' ||
      values.origine === ''
    ) {
      return true;
    } else {
      if (
        (values.origine === 'FOURNISSEUR' && values.fournisseur !== '') ||
        (values.origine === 'GROUPE_AMIS' && values.groupesamis !== '') ||
        (values.origine === 'PRESTATAIRE' && values.prestataire !== '') ||
        (values.origine === 'GROUPEMENT' && values.service !== '') ||
        values.origine === 'INTERNE' ||
        values.origine === 'PATIENT'
      ) {
        return false;
      }
      return true;
    }
  };

  const client = useApolloClient();

  const [createAndUpdateIdeeBonnePratique, { loading: iddeeLoading }] = useMutation<
    CREATE_UPDATE_IDEE_BONNE_PRATIQUE,
    CREATE_UPDATE_IDEE_BONNE_PRATIQUEVariables
  >(DO_CREATE_UPDATE_IDEE_BONNE_PRATIQUE, {
    onCompleted: data => {
      if (data && data.createUpdateIdeeOuBonnePratique) {
        // ! IMPORTANT
        // if (refetchTasks) refetchTasks();
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: values.id
            ? 'Modification effectuée avec succès'
            : 'Création effectuée avec succès',
        });
        setValues(initialState);
        setActiveStep(0);
        setLoading(false);
        setMutationSuccess(true);
      }
    },
    update: (cache, { data }) => {
      if (data && data.createUpdateIdeeOuBonnePratique) {
        const req = cache.readQuery<
          SEARCH_IDEE_BONNE_PRATIQUE,
          SEARCH_IDEE_BONNE_PRATIQUEVariables
        >({
          query: DO_SEARCH_IDEE_BONNE_PRATIQUE,
          // variables: queryVariables,
        });
        if (req && req.search && req.search.data) {
          cache.writeQuery<SEARCH_IDEE_BONNE_PRATIQUE, SEARCH_IDEE_BONNE_PRATIQUEVariables>({
            query: DO_SEARCH_IDEE_BONNE_PRATIQUE,
            data: {
              search: {
                ...req.search,
                ...{
                  total: req.search.total + 1,
                  data: [data.createUpdateIdeeOuBonnePratique, ...req.search.data],
                },
              },
            },
            // variables: queryVariables,
          });
        }
      }
    },
    onError: error => {
      console.log('error :>> ', error);
      displaySnackBar(client, { isOpen: true, type: 'ERROR', message: error.message });
    },
  });

  const finalStep = {
    buttonLabel: 'Ajouter',
    action: () => {
      handleCreateUpdatePublicite();
    },
  };

  const ActionSuccess = () => {
    return (
      <div className={classes.mutationSuccessContainer}>
        <NoItemContentImage
          src={mutationSuccessImg}
          title={mutationSuccessTitle}
          subtitle={mutationSuccessSubTitle}
        >
          <CustomButton color="default" onClick={goBack}>
            Retour à la liste
          </CustomButton>
        </NoItemContentImage>
      </div>
    );
  };

  const [uploadFiles, uploadFilesResult] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (data && data.createPutPresignedUrls && data.createPutPresignedUrls.length > 0) {
        Promise.all(
          data.createPutPresignedUrls.map(async (presigned, index) => {
            if (presigned && selectedFiles.length > 0) {
              await Promise.all(
                selectedFiles.map(async file => {
                  if (presigned.filePath.includes(formatFilenameWithoutDate(file))) {
                    const type: TypeFichier =
                      file.type && file.type.includes('pdf')
                        ? TypeFichier.PDF
                        : file.type.includes('xlsx')
                          ? TypeFichier.EXCEL
                          : TypeFichier.PHOTO;
                    const newFile: FichierInput = {
                      id: '',
                      type,
                      nomOriginal: file.name,
                      chemin: presigned.filePath,
                    };

                    files = uniqBy([...files, newFile], 'chemin');

                    await uploadToS3(selectedFiles[index], presigned.presignedUrl)
                      .then(result => {
                        if (result && result.status === 200) {
                          uploadResult = result;
                        }
                      })
                      .catch(error => {
                        setLoading(false);
                        console.log('error :>> ', error);
                        displaySnackBar(client, {
                          type: 'ERROR',
                          message: "Erreur lors de l'envoye de(s) fichier(s)",
                          isOpen: true,
                        });
                      });
                  }
                }),
              );
            }
          }),
        ).then(_ => {
          // Execute doCreateUpdate
          if ((uploadResult && uploadResult.status === 200) || values || files) {
            createAndUpdateIdeeBonnePratique({
              variables: {
                input: {
                  id: values.id,
                  origine: values.origine,
                  status: values.status,
                  title: values.title,
                  beneficiaires_cles: values.beneficiaire,
                  idAuteur: values.idAuteur,
                  concurent: values.concurrence,
                  contexte: values.context,
                  contraintes: values.contrainte,
                  facteursClesDeSucces: values.succ,
                  idClassification:
                    (values.classification && values.classification.idclassification) || '',
                  idFournisseur: (values.fournisseur && values.fournisseur.id) || '',
                  idPrestataire: (values.prestataire && values.prestataire.id) || '',
                  objectifs: values.objectif,
                  idGroupeAmis: (values.groupesamis && values.groupesamis.id) || '',
                  resultats: values.resultats,
                  idService: (values.service && values.service.id) || '',
                  fichiers: [...files, ...oldFiles],
                },
              },
            });
            setLoading(false);
          } else {
            setLoading(false);
          }
        });
      } else {
        createAndUpdateIdeeBonnePratique({
          variables: {
            input: {
              id: values.id,
              origine: values.origine,
              status: values.status,
              title: values.title,
              beneficiaires_cles: values.beneficiaire,
              idAuteur: values.idAuteur,
              concurent: values.concurrence,
              contexte: values.context,
              contraintes: values.contrainte,
              facteursClesDeSucces: values.succ,
              idClassification:
                (values.classification && values.classification.idclassification) || '',
              idFournisseur: (values.fournisseur && values.fournisseur.id) || '',
              idPrestataire: (values.prestataire && values.prestataire.id) || '',
              objectifs: values.objectif,
              idGroupeAmis: (values.groupesamis && values.groupesamis.id) || '',
              resultats: values.resultats,
              fichiers: selectedFiles.length > 0 && oldFiles.length > 0 ? oldFiles : [],
              idService: (values.service && values.service.id) || '',
            },
          },
        });
      }
    },
    onError: errors => {
      setLoading(false);
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const handleCreateUpdatePublicite = () => {
    // If form is valid
    // Start loading

    setLoading(true);
    if (values && selectedFiles && selectedFiles.length > 0) {
      // Presigned file
      const filePathsTab: string[] = [];
      selectedFiles.map((file: File) => {
        if (file.type) {
          filePathsTab.push(
            `ideeoubonnepratique/${groupement && groupement.id}/${formatFilenameWithoutDate(file)}`,
          );
        }
      });
      // Create presigned url
      uploadFiles({ variables: { filePaths: filePathsTab } });
    } else {
      createAndUpdateIdeeBonnePratique({
        variables: {
          input: {
            id: values.id,
            origine: values.origine,
            status: values.status,
            title: values.title,
            beneficiaires_cles: values.beneficiaire,
            idAuteur: values.idAuteur,
            concurent: values.concurrence,
            contexte: values.context,
            contraintes: values.contrainte,
            facteursClesDeSucces: values.succ,
            idClassification:
              (values.classification && values.classification.idclassification) || '',
            idFournisseur: (values.fournisseur && values.fournisseur.id) || '',
            idPrestataire: (values.prestataire && values.prestataire.id) || '',
            objectifs: values.objectif,
            idGroupeAmis: (values.groupesamis && values.groupesamis.id) || '',
            resultats: values.resultats,
            idService: (values.service && values.service.id) || '',
          },
        },
      });
    }
  };

  if (mutationSuccess) return <ActionSuccess />;

  return (
    <Box className={classes.container}>
      {(loading || uploadFilesResult.loading || iddeeLoading) && (
        <Backdrop value={'Opération en cours...'} />
      )}
      <Stepper
        title={
          isOnEdit
            ? "Modification d'idée ou de bonne pratique"
            : "Ajout d'idée ou de bonne pratique "
        }
        steps={steps}
        backToHome={goToHome}
        disableNextBtn={disablebutton() ? true : false}
        fullWidth={false}
        activeStepFromProps={activeStep}
        setActiveStepFromProps={setActiveStep}
        finalStep={finalStep}
      />
    </Box>
  );
};
export default withRouter(FormIdea);
