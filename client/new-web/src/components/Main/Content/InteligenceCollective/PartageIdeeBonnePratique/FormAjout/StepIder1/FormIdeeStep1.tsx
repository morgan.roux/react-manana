import { useApolloClient, useLazyQuery, useQuery } from '@apollo/client';
import { Box, debounce, InputAdornment, ListItem, Typography } from '@material-ui/core';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import ListAltIcon from '@material-ui/icons/ListAlt';
import React, { useEffect, useRef, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { DO_SEARCH_GROUPE_AMIS } from '../../../../../../../graphql/GroupeAmis/query';
import {
  SEARCH_GROUPE_AMIS,
  SEARCH_GROUPE_AMISVariables,
} from '../../../../../../../graphql/GroupeAmis/types/SEARCH_GROUPE_AMIS';
import { GET_IDEE_CLASSIFICATIONS } from '../../../../../../../graphql/IdeeBonnePratique/query';
import {
  GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONS,
  GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONSVariables,
} from '../../../../../../../graphql/IdeeBonnePratique/types/GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONS';
import { DO_SEARCH_LABORATOIRE } from '../../../../../../../graphql/Laboratoire/query';
import {
  SEARCH_LABORATOIRE,
  SEARCH_LABORATOIREVariables,
} from '../../../../../../../graphql/Laboratoire/types/SEARCH_LABORATOIRE';
import { GET_SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST } from '../../../../../../../graphql/PartenaireRepresentant/query';
import {
  SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST,
  SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LISTVariables,
} from '../../../../../../../graphql/PartenaireRepresentant/types/SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST';
import { GET_SEARCH_CUSTOM_CONTENT_SERVICE } from '../../../../../../../graphql/Service';
import {
  SEARCH_CUSTOM_CONTENT_SERVICE,
  SEARCH_CUSTOM_CONTENT_SERVICEVariables,
} from '../../../../../../../graphql/Service/types/SEARCH_CUSTOM_CONTENT_SERVICE';
import { DO_SEARCH_MIN_USERS } from '../../../../../../../graphql/User/query';
import {
  SEARCH_MIN_USERS,
  SEARCH_MIN_USERSVariables,
} from '../../../../../../../graphql/User/types/SEARCH_MIN_USERS';
import SnackVariableInterface from '../../../../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';
import CustomAutocomplete from '../../../../../../Common/CustomAutocomplete';
import CustomSelect from '../../../../../../Common/CustomSelect';
import { CustomFormTextField } from '../../../../../../Common/CustomTextField';
import Dropzone from '../../../../../../Common/Dropzone/Dropzone';
import ConcernedParticipant from '../../../Modals/ConcernedParticipant';
import useStyles from './styles';

interface IFormProps {
  handleChange?: (e: React.ChangeEvent<any>) => void;
  values?: any;
  handleChangeClassificationAutocomplete: (inputValue: any) => void;
  handleChangeFournisseurAutocomplete: (inputValue: any) => void;
  handleChangePrestataireAutocomplete: (inputValue: any) => void;
  handleChangeAuteursAutocomplete: (state: any) => void;
  handleChangeGroupeAmisAutocomplete: (inputValue: any) => void;
  handleChangeServiceAutocomplete: (inputValue: any) => void;
  setSelectedFiles: (val: any) => void;
  disabledfournisseur?: boolean;
  disabledprestaire?: boolean;
  disabledgroupeAmis?: boolean;
  disabledgroupement?: boolean;
  selectedFiles: any;
}

const FormIdeeStep1: React.FC<IFormProps & RouteComponentProps> = props => {
  const {
    handleChange,
    values,
    disabledfournisseur,
    disabledprestaire,
    disabledgroupeAmis,
    disabledgroupement,
    handleChangeClassificationAutocomplete,
    handleChangeFournisseurAutocomplete,
    handleChangePrestataireAutocomplete,
    handleChangeAuteursAutocomplete,
    handleChangeGroupeAmisAutocomplete,
    handleChangeServiceAutocomplete,
    setSelectedFiles,
    selectedFiles,
  } = props;

  const classes = useStyles({});
  const client = useApolloClient();

  const originList = [
    { value: 'INTERNE', label: 'INTERNE' },
    { value: 'PATIENT', label: 'PATIENT' },
    { value: 'GROUPE_AMIS', label: 'GROUPE AMIS' },
    { value: 'GROUPEMENT', label: 'GROUPEMENT' },
    { value: 'FOURNISSEUR', label: 'FOURNISSEUR' },
    { value: 'PRESTATAIRE', label: 'PRESTATAIRE' },
    { value: 'DIGITAL4WIN', label: 'DIGITAL4WIN' },
  ];

  const dataClassification = useQuery<
    GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONS,
    GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONSVariables
  >(GET_IDEE_CLASSIFICATIONS, {
    variables: { isRemoved: false },
    fetchPolicy: 'network-only',
  });

  const [openAuteurModal, setOpenAuteurModal] = useState<boolean>(false);
  const handleOpenAuteur = event => {
    event.stopPropagation();
    setOpenAuteurModal(true);
  };

  const [listClassique1, setlistClassique1] = useState<
    Array<{ idclassification: string; nom: string; isChild: boolean }>
  >();

  useEffect(() => {
    if (dataClassification.data) {
      const test: Array<{ idclassification: string; nom: string; isChild: boolean }> = [];
      dataClassification.data?.ideeOuBonnePratiqueClassifications
        .filter(i => i.parent === null)
        .map(item => {
          test.push({
            idclassification: item.id,
            isChild: false,
            nom: item.nom,
          });
          if (item && item.childs) {
            item.childs.map(o => {
              test.push({
                idclassification: o.id,
                isChild: true,
                nom: o.nom,
              });
            });
          }
        });

      setlistClassique1(test);
    }
  }, [dataClassification.data]);

  const laboSortBy = [{ nomLabo: { order: 'asc' } }];
  const laboFilterBy = [{ term: { sortie: 0 } }];
  const laboType = ['laboratoire'];
  const partenaireType = ['partenairerepresentant'];
  const usersSortBy = [{ userName: { order: 'asc' } }];
  const usersFilterBy = [{ term: { sortie: 0 } }];
  const groupesAmisType = ['groupeamis'];
  const serviceType = ['service'];
  const seviceSortBy = [{ nom: { order: 'asc' } }];

  const [searchLaboratoires, searchLaboratoiresResult] = useLazyQuery<
    SEARCH_LABORATOIRE,
    SEARCH_LABORATOIREVariables
  >(DO_SEARCH_LABORATOIRE, {
    fetchPolicy: 'cache-first',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
    onCompleted: data => {
      console.log('data', data);
    },
  });

  const debouncedSearch = useRef(
    debounce((query: string, dataType: string) => {
      if (query.length > 0) {
        switch (dataType) {
          case 'laboratoire':
            searchLaboratoires({
              variables: {
                type: laboType,
                query,
                sortBy: laboSortBy,
                filterBy: laboFilterBy,
              },
            });
            break;
          case 'partenaire':
            searchPartenaire({
              variables: {
                type: partenaireType,
                query,
              },
            });
            break;
          case 'users':
            searchAuteurs({
              variables: {
                query,
                sortBy: usersSortBy,
                filterBy: usersFilterBy,
              },
            });
            break;
          case 'groupeAmis':
            searchGroupeAmis({
              variables: {
                query: { query: { bool: { must: [{ term: { isRemoved: 'false' } }] } } },
                type: ['groupeamis'],
                take: 100,
                skip: 0,
              },
            });
            break;
          case 'service':
            searchService({
              variables: {
                type: ['service'],
                take: 100,
                skip: 0,
              },
            });
            break;
          default:
            break;
        }
      }
    }, 1000),
  );
  // setQueryTextLaboratoire(null);

  // Debounced Search Laboratoire
  useEffect(() => {
    switch (values) {
      case values.fournisseur:
        debouncedSearch.current(values.fournisseur, 'laboratoire');
        break;
      case values.prestataire:
        debouncedSearch.current(values.prestataire, 'partenaire');
        break;
      case values.auteurs:
        debouncedSearch.current(values.idAuteurs, 'users');
        break;
      case values.groupeamis:
        debouncedSearch.current(values.groupesamis, 'groupeAmis');
        break;
      case values.service:
        debouncedSearch.current(values.service, 'service');
        break;
      default:
        break;
    }
  }, [values]);

  const handleOpenFournisseur = () => {
    searchLaboratoires({
      variables: {
        type: laboType,
      },
    });
  };

  // Partenaire Representant

  const [searchPartenaire, searchPartenaireResult] = useLazyQuery<
    SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST,
    SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LISTVariables
  >(GET_SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST, {
    fetchPolicy: 'cache-and-network',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
    onCompleted: data => {
      console.log('data', data);
    },
  });
  const handleOpenPrestataire = () => {
    console.log('PARTENAIRE CLICKEE');
    searchPartenaire({
      variables: {
        type: partenaireType,
      },
    });
  };

  const [searchAuteurs, searchAuteursResult] = useLazyQuery<
    SEARCH_MIN_USERS,
    SEARCH_MIN_USERSVariables
  >(DO_SEARCH_MIN_USERS, {
    fetchPolicy: 'cache-first',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
    onCompleted: data => {
      console.log('data', data);
    },
  });

  const handleOpenUsers = () => {
    searchAuteurs({
      variables: {
        sortBy: usersSortBy,
      },
    });
  };

  const [searchGroupeAmis, searchGroupeAmisResult] = useLazyQuery<
    SEARCH_GROUPE_AMIS,
    SEARCH_GROUPE_AMISVariables
  >(DO_SEARCH_GROUPE_AMIS, {
    fetchPolicy: 'cache-and-network',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
    onCompleted: data => {
      console.log('data', data);
    },
  });

  const handleOpenGroupeAmis = () => {
    searchGroupeAmis({
      variables: {
        type: groupesAmisType,
      },
    });
  };

  const [nameAuteur, setnameAuteur] = useState('');

  useEffect(() => {
    if (nameAuteur === '') {
      setnameAuteur(values.nameAuteur || '');
    } else {
      setnameAuteur(nameAuteur);
    }
  }, [values.nameAuteur, nameAuteur]);

  const handleUserAuteursNameSelected = user => {
    setnameAuteur(user);
  };

  const [searchService, searchServiceResult] = useLazyQuery<
    SEARCH_CUSTOM_CONTENT_SERVICE,
    SEARCH_CUSTOM_CONTENT_SERVICEVariables
  >(GET_SEARCH_CUSTOM_CONTENT_SERVICE, {
    fetchPolicy: 'cache-and-network',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
    onCompleted: data => {
      console.log('data', data);
    },
  });
  const handleOpenService = () => {
    searchService({
      variables: {
        type: serviceType,
      },
    });
  };

  return (
    <Box className={classes.root}>
      <Typography className={classes.inputTitle}>Informations générales</Typography>
      <Box width="100%">
        <CustomSelect
          label="Origine"
          list={originList}
          listId="value"
          index="label"
          required={true}
          name="origine"
          value={values.origine}
          onChange={handleChange}
        />
      </Box>
      <Box mb={2.5} width="100%">
        <CustomAutocomplete
          required={false}
          id="idclassification"
          options={listClassique1 || []}
          optionLabelKey="nom"
          value={values.classification}
          onAutocompleteChange={handleChangeClassificationAutocomplete}
          loading={dataClassification && dataClassification.loading}
          label="Classification"
          // tslint:disable-next-line: jsx-no-lambda
          renderOption={(option, state) => {
            return option && option.isChild ? (
              <ListItem className={classes.listItemMenu} style={{ marginLeft: 14 }}>
                {option.nom}
              </ListItem>
            ) : (
              <ListItem className={classes.listItemMenu}>{option.nom}</ListItem>
            );
          }}
        />
      </Box>
      <CustomFormTextField
        label="Titre"
        name="title"
        onChange={handleChange}
        value={values.title}
        required={true}
      />
      <Dropzone
        contentText="Glissez et déposez ici votre fichier"
        selectedFiles={selectedFiles}
        setSelectedFiles={setSelectedFiles}
        multiple={true}
        acceptFiles="application/pdf"
      />
      <Box className={classes.contentIntervenant}>
        <Typography className={classes.inputTitle}>Intervenants</Typography>
        <ConcernedParticipant
          isOpen={openAuteurModal}
          setOpen={setOpenAuteurModal}
          title="Auteurs"
          userIdSelected={handleChangeAuteursAutocomplete}
          userNameSelected={handleUserAuteursNameSelected}
          isOnEquip={false}
          withBtnsActions={false}
        />
        <CustomFormTextField
          name="auteur"
          label="Auteurs"
          value={nameAuteur}
          onClick={handleOpenAuteur}
          required={true}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <AssignmentIndIcon />
              </InputAdornment>
            ),
          }}
        />
        <CustomFormTextField
          required={false}
          label="Concurrence"
          name="concurrence"
          onChange={handleChange}
          value={values.concurrence}
        />
        {!disabledfournisseur && (
          <CustomAutocomplete
            required={!disabledfournisseur}
            id="idFrournisseur"
            options={
              (searchLaboratoiresResult &&
                searchLaboratoiresResult.data &&
                searchLaboratoiresResult.data.search &&
                (searchLaboratoiresResult.data.search.data as any)) ||
              []
            }
            optionLabelKey="nomLabo"
            value={values.fournisseur}
            onAutocompleteChange={handleChangeFournisseurAutocomplete}
            label="Fournisseur"
            popupIcon={<ListAltIcon />}
            onOpen={handleOpenFournisseur}
            loading={searchLaboratoiresResult && searchLaboratoiresResult.loading}
            disabled={disabledfournisseur}
          />
        )}
        {!disabledgroupement && (
          <CustomAutocomplete
            required={!disabledgroupement}
            id="idService"
            options={
              (searchServiceResult &&
                searchServiceResult.data &&
                searchServiceResult.data.search &&
                (searchServiceResult.data.search.data as any)) ||
              []
            }
            optionLabelKey="nom"
            value={values.service}
            onAutocompleteChange={handleChangeServiceAutocomplete}
            label="Service"
            popupIcon={<AssignmentIndIcon />}
            onOpen={handleOpenService}
            loading={searchServiceResult && searchServiceResult.loading}
          />
        )}

        {!disabledprestaire && (
          <CustomAutocomplete
            required={!disabledprestaire}
            options={
              (searchPartenaireResult &&
                searchPartenaireResult.data &&
                searchPartenaireResult.data.search &&
                (searchPartenaireResult.data.search.data as any)) ||
              []
            }
            optionLabelKey="nom"
            value={values.prestataire}
            onAutocompleteChange={handleChangePrestataireAutocomplete}
            label="Prestataire"
            popupIcon={<ListAltIcon />}
            onOpen={handleOpenPrestataire}
            loading={searchPartenaireResult && searchPartenaireResult.loading}
            disabled={disabledprestaire}
          />
        )}
        {!disabledgroupeAmis && (
          <CustomAutocomplete
            required={!disabledgroupeAmis}
            options={
              (searchGroupeAmisResult &&
                searchGroupeAmisResult.data &&
                searchGroupeAmisResult.data.search &&
                (searchGroupeAmisResult.data.search.data as any)) ||
              []
            }
            optionLabelKey="nom"
            value={values.groupesamis}
            onAutocompleteChange={handleChangeGroupeAmisAutocomplete}
            label="Groupe d'amis"
            popupIcon={<ListAltIcon />}
            onOpen={handleOpenGroupeAmis}
            loading={searchGroupeAmisResult && searchGroupeAmisResult.loading}
            disabled={disabledgroupeAmis}
          />
        )}
      </Box>
    </Box>
  );
};

export default withRouter(FormIdeeStep1);
