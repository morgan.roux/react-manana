import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    bigAvatar: {
      width: 49,
      height: 49,
      marginRight: 6,
      borderRadius: '50%',
    },
    userName: {
      fontSize: 16,
      fontWeight: 500,
    },
    commentDate: {
      textAlign: 'right',
      '& .MuiTypography-root': {
        color: '#9E9E9E',
      },
    },
    fileContainer: {
      cursor: 'pointer',
      marginTop: 16,
    },
    contentMessage: {
      backgroundColor: '#F5F6FA',
      padding: 15,
      borderRadius: 12,
      marginTop: '0 !important',
      marginBottom: '0!important',
    },
    contentListItem: {
      display: 'flex',
      alignItems: 'start',
    },
    fonctionName: {
      fontSize: '0.875rem',
      color: '#E34168',
      fontWeight: 'bold',
      lineHeight: 1,
    },
    labelPharmacie: {
      fontSize: '1rem',
      color: '#616161',
      fontWeight: 'bold',
      marginRight: 8,
    },
    namePharmacie: {
      fontSize: '1rem',
      color: '#212121',
      fontWeight: 'bold',
    },
    labels: {
      display: 'flex',
      alignItems: 'center',
      marginRight: 28,
      marginBottom: 8,
    },
    labelOrigine: {
      fontFamily: 'Montserrat',
      fontSize: 12,
      //textDecoration: 'underline',
      marginRight: 4,
    },
    labelText: {
      marginLeft: 8,
      fontSize: 12,
      lineHeight: '10px',
      fontFamily: 'Montserrat',
    },
    contentLabels: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'start',
    },
  }),
);
