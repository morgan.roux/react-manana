import { Box, IconButton, Typography } from '@material-ui/core';
import { Visibility } from '@material-ui/icons';
import classnames from 'classnames';
import React, { FC } from 'react';

import useStyles from './styles';

interface PartageInfoItemProps {
  incident: any;
  bottomItems: any[];
  infoLabel: string;
  infoKey: string;
  handleVisibility: (any) => void;
}

const PartageInfoItem: FC<PartageInfoItemProps> = ({
  incident,
  bottomItems,
  infoKey,
  infoLabel,
  handleVisibility,
}) => {
  const classes = useStyles({});

  return (
    <Box className={classes.contentListItem}>
      <Box display="flex" justifyContent="space-between">
        <Box display="flex" marginBottom="15px">
          <Typography className={classes.title}>
            {[infoLabel]}
            {` : `}{' '}
          </Typography>
          <Typography className={classes.titleValue}>{` ` + incident[infoKey]}</Typography>
        </Box>
        <IconButton onClick={handleVisibility}>
          <Visibility />
        </IconButton>
      </Box>
      <Typography>{incident.content}</Typography>
      <Box className={classes.contentLabels}>
        {bottomItems.map((item, index) => {
          return (
            <Box className={classes.labels} key={index}>
              <Typography className={classnames(classes.labelOrigine)}>
                {item.label}
                {` : `}
              </Typography>
              <Typography className={classnames(classes.labelText)}>
                {incident[item.key]}
              </Typography>
            </Box>
          );
        })}
      </Box>
    </Box>
  );
};

export default PartageInfoItem;
