import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    contentListItem: {
      border: '1px solid #E3E3E3',
      borderRadius: 6,
      marginBottom: 10,
      display: 'flex',
      flexDirection: 'column',
      padding: '13px 15px',
    },
    contentMessage: {},
    contentLabels: {
      display: 'flex',
      marginTop: 5,
    },
    labels: {
      display: 'flex',
      marginRight: 24,
    },
    labelOrigine: {
      fontSize: '12px',
      color:'#616161',
      marginRight: '6px' 
    },
    labelText: {
      fontSize: '14px',
      fontWeight: 'bold',
      color:'#616161'
    },
    title:{
      font: 'normal normal bold 16px/21px',
      color: '#616161',
      marginRight: '6px' 
    },
    titleValue:{
      font: 'normal normal bold 16px/21px',
      color: '#212121'
    }
  }),
);

export default useStyles;
