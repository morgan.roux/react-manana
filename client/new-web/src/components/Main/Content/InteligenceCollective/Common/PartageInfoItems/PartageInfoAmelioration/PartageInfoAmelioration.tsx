import {
  Avatar,
  Box,
  IconButton,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from '@material-ui/core';
import { AccountCircle, Visibility } from '@material-ui/icons';
import classnames from 'classnames';
import moment from 'moment';
import React, { FC } from 'react';
import useCommonStyles from '../../../../../../Dashboard/commonStyles';

import useStyles from './styles';

interface PartageInfoItemProps {
  incident: any;
  bottomItems: any[];
  infoLabel: string;
  infoKey: string;
  handleVisibility: () => void;
}

const PartageInfoAmelioration: FC<PartageInfoItemProps> = ({
  incident,
  bottomItems,
  infoKey,
  infoLabel,
  handleVisibility,
}) => {
  const classes = useStyles({});

  const openFile = (url: string) => () => {
    window.open(url, '_blank');
  };

  const tab = ['Origine', 'Motif'];
  return (
    <Box className={classes.contentListItem}>
      <Box display="flex" justifyContent="space-between">
        <Box display="flex" marginBottom="15px">
          <Typography className={classes.title}>
            {[infoLabel]}
            {` : `}{' '}
          </Typography>
          <Typography className={classes.titleValue}>{` ` + incident[infoKey]}</Typography>
        </Box>
        <IconButton onClick={handleVisibility}>
          <Visibility />
        </IconButton>
      </Box>
      <Typography>{incident.content}</Typography>
      <Box className={classes.contentLabels}>
        {bottomItems.map(item => {
          return (
            <Box className={classes.labels}>
              <Typography className={classnames(classes.labelOrigine)}>
                {item.label}
                {` : `}
              </Typography>
              <Typography className={classnames(classes.labelText)}>
                {incident[item.key]}
              </Typography>
            </Box>
          );
        })}
      </Box>
    </Box>
  );
};

export default PartageInfoAmelioration;
