import React, { FC } from 'react';
import { RouteComponentProps } from 'react-router';
import PartageInfoItem from '../../Common/PartageInfoItems/PartageInfoItem';
import ShareNoResult from '../ShareNoResult/ShareNoResult';
import { BOTTON_ITEMS } from '../utils/constants';
import GetData from '../utils/GetData';

interface SharedReclamationProps {
  listResult: any;
  goToDetail: (any) => void;
}
const SharedReclamation: FC<SharedReclamationProps> = ({ listResult, goToDetail }) => {
  return (
    <>
      {(listResult &&
        listResult.length > 0 &&
        listResult.map((incident) => (
          <PartageInfoItem
            handleVisibility={() => goToDetail(incident.id)}
            incident={GetData(incident)}
            bottomItems={BOTTON_ITEMS}
            infoKey="declarant"
            infoLabel="Ouvert par"
          />
        ))) || <ShareNoResult />}
    </>
  );
};
export default SharedReclamation;
