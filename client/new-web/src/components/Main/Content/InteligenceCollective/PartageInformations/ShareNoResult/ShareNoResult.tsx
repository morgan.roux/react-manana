import { Box, makeStyles, Typography } from '@material-ui/core';
import React, { FC } from 'react';

const useStyles = makeStyles(theme => ({
  title: {
    color: '#9E9E9E',
    fontWeight: 'bold',
    fontSize: '16px',
  },
  content: {
    textAlign: 'center',
  },
  detail: {
    color: '#9E9E9E',
    fontSize: '14px',
  },
}));

interface ShareNoResultProps {}
const ShareNoResult: FC<ShareNoResultProps> = ({}) => {
  const classes = useStyles({});
  return (
    <Box className={classes.content}>
      <Typography className={classes.title}>Aucun résultat</Typography>
      <Typography className={classes.detail}>
        il n'y aucun résultat pour ce type d'information
      </Typography>
    </Box>
  );
};
export default ShareNoResult;
