import React, { FC, Fragment } from 'react';
import SharedTodoItem from '../../Common/PartageInfoItems/SharedTodoItem/SharedTodoItem';

import ShareNoResult from '../ShareNoResult/ShareNoResult';

interface SharedTodoProps {
  listResult: any;
  isLoading: boolean;
}

const SharedTodo: FC<SharedTodoProps> = ({ listResult, isLoading }) => {
  return (
    <Fragment>
      {listResult && listResult.length > 0 ? (
        <SharedTodoItem tasks={listResult} isLoading={isLoading} />
      ) : (
        <ShareNoResult />
      )}
    </Fragment>
  );
};
export default SharedTodo;
