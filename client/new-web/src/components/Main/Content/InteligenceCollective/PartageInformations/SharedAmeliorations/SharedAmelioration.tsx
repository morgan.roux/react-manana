import React, { FC } from 'react';
import PartageInfoAmelioration from '../../Common/PartageInfoItems/PartageInfoAmelioration';
import ShareNoResult from '../ShareNoResult/ShareNoResult';
import { BOTTON_ITEMS_AMELIORATION } from '../utils/constants';
import GetData from '../utils/GetData';
interface SharedAmeliorationProps {
  listResult: any;
  goToDetail: (any) => void;
}
const SharedAmelioration: FC<SharedAmeliorationProps> = ({ listResult, goToDetail }) => {
  return (
    <>
      {(listResult &&
        listResult.length > 0 &&
        listResult.map((amelioration) => (
          <PartageInfoAmelioration
            incident={GetData(amelioration)}
            bottomItems={BOTTON_ITEMS_AMELIORATION}
            handleVisibility={() => goToDetail(amelioration.id)}
            infoKey="auteur"
            infoLabel="Auteur"
          />
        ))) || <ShareNoResult />}
    </>
  );
};
export default SharedAmelioration;
