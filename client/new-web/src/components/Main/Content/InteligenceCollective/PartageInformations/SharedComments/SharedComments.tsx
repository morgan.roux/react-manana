import React, { FC, Fragment } from 'react';
import PartageInfoCommentItem from '../../Common/PartageInfoItems/PartageInfoCommentItem';
import ShareNoResult from '../ShareNoResult/ShareNoResult';

interface SharedCommentsProps {
  listResult: any;
}

const SharedComments: FC<SharedCommentsProps> = ({ listResult }) => {
  return (
    <Fragment>
      {listResult && listResult.length > 0 ? (
        listResult.map((comment, index) => <PartageInfoCommentItem key={index} comment={comment} />)
      ) : (
        <ShareNoResult />
      )}
    </Fragment>
  );
};
export default SharedComments;
