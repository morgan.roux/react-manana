import React, { FC } from 'react';
import PartageInfoItem from '../../Common/PartageInfoItems/PartageInfoItem';
import ShareNoResult from '../ShareNoResult/ShareNoResult';
import GetData from '../utils/GetData';
import { BOTTON_ITEMS } from '../utils/constants';

interface SharedIncidentsProps {
  listResult: any;
  goToDetail: (any) => void;
}
const SharedIncidents: FC<SharedIncidentsProps> = ({ listResult, goToDetail }) => {
  return (
    <>
      {(listResult &&
        listResult.length > 0 &&
        listResult.map((incident) => (
          <PartageInfoItem
            handleVisibility={() => goToDetail(incident.id)}
            incident={GetData(incident)}
            bottomItems={BOTTON_ITEMS}
            infoKey="declarant"
            infoLabel="Déclarant"
          />
        ))) || <ShareNoResult />}
    </>
  );
};
export default SharedIncidents;
