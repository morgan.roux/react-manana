import { Dispatch, SetStateAction, ReactNode } from 'react';
import { Column } from '../../../../../Dashboard/Content/Interface';

export type Order = 'asc' | 'desc';

export interface TableToolbarProps {
  checkedItems: any;
  listResult: any;
  setShowCheckeds: Dispatch<SetStateAction<boolean>>;
  showCheckeds: boolean;
  allTotal: number;
  checkedItemsQuery?: CheckItemsQueryInterface;
  selected?: any[];
  setSelected?: Dispatch<SetStateAction<any[]>>;
  clearSelection: () => void;
}

export interface TableHeadProps {
  isSelectable: boolean | undefined;
  checkedItemsQuery?: CheckItemsQueryInterface;
  columns: Column[] | undefined;
  checkedItems: any;
  listResult?: any;
  selected?: any[];
  setSelected?: Dispatch<SetStateAction<any[]>>;
  allTotal: number;
  isAllCheckable: boolean;
}

export interface ItemSelected {
  [key: string]: any;
}

export interface TableProps {
  listResult: any;
  isSelectable?: boolean;
  columns: Column[] | undefined;
  hidePagination?: boolean;
  checkedItemsQuery?: CheckItemsQueryInterface;
  paginationCentered?: boolean;
  selected?: any[];
  setSelected?: Dispatch<SetStateAction<any[]>>;
  onClickRow?: (row: any) => void;
  activeRow?: any;
}

export interface CheckItemsQueryInterface {
  type: string;
  name: string;
  query: any;
}
