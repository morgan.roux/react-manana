import React, { FC, useState, useEffect, useMemo, useContext } from 'react';

import classnames from 'classnames';
import { useToolbarStyles } from './styles';
import { Typography, Box } from '@material-ui/core';
import ReplayIcon from '@material-ui/icons/Replay';
import { TableToolbarProps } from '../interfaces';
import { useApolloClient, useLazyQuery } from '@apollo/client';
//import { resetSearchFilters } from '../../withSearch/withSearch';

import { withRouter, RouteComponentProps } from 'react-router-dom';
import { Refresh, Delete, Tune } from '@material-ui/icons';

import {
  ADMINISTRATEUR_GROUPEMENT,
  SUPER_ADMINISTRATEUR,
} from '../../../../../../../Constant/roles';
import { ME_me } from '../../../../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../../../../services/LocalStorage';
import CustomButton from '../../../../../../Common/CustomButton';
import CustomCheckbox from '../../../../../../Common/CustomCheckbox/CustomCheckbox';
import Backdrop from '../../../../../../Common/Backdrop/Backdrop';
import { GET_MINIMAL_SEARCH_ID } from '../../../../../../../graphql/MinimalSearch/query';
import {
  MINIMAL_SEARCH_ID,
  MINIMAL_SEARCH_IDVariables,
} from '../../../../../../../graphql/MinimalSearch/types/MINIMAL_SEARCH_ID';
import { CustomModal } from '../../../../../../Common/CustomModal';
import { ContentContext, ContentStateInterface } from '../../../../../../../AppContext';
//import PharmacieFilter from '../../ListesGroupeAmisFilter';
import PersonnelGroupementFilter from '../../../../../../Common/newCustomContent/Filters/PersonnelGroupementFilter';
import TitulaireFilter from '../../../../../../Common/newCustomContent/Filters/TitulaireFilter';
import PersonnelAffectationFilter from '../../../../../../Common/newCustomContent/Filters/PersonnelAffectationFilter';
import LaboratoireFilter from '../../../../../../Common/newCustomContent/Filters/LaboratoireFilter';
import ListesGroupeAmisFilter from '../../ListesGroupeAmisFilter';
import ListesLaboPartenaireFilter from '../../../ListesLaboPartenaire/ListesLaboPartenaireFilter';
import ListesPartenaireServiceFilter from '../../../ListesPartenaireService/ListesPartenaireServiceFilter';

const EnhancedTableToolbar: FC<TableToolbarProps & RouteComponentProps> = ({
  checkedItems,
  listResult,
  clearSelection,
  setShowCheckeds,
  allTotal,
  showCheckeds,
  checkedItemsQuery,
  selected,
  setSelected,
  location: { pathname },
}) => {
  const classes = useToolbarStyles();
  const client = useApolloClient();
  const currentUser: ME_me = getUser();
  const isAdmin =
    currentUser &&
    currentUser.role &&
    (currentUser.role.code === SUPER_ADMINISTRATEUR ||
      currentUser.role.code === ADMINISTRATEUR_GROUPEMENT);

  const onGroupeDAmis = pathname.startsWith(
    '/intelligence-collective/partage-des-informatons/friend-list',
  );

  const onLaboPartenaire = pathname.startsWith(
    '/intelligence-collective/partage-des-informatons/laboratoires',
  );

  const onServicePartner = pathname.startsWith(
    '/intelligence-collective/partage-des-informatons/partenaires-services',
  );

  const type = listResult && listResult.variables && listResult.variables.type;

  const [checkedAll, setCheckedAll] = useState<boolean>(
    (checkedItems && allTotal > 0 && checkedItems.length === allTotal) || false,
  );

  const [openFilter, setOpenFilter] = useState<boolean>(false);

  const [minimalData, setMinimalData] = useState<any[]>([]);

  const [getAllItems, { data: allData, loading }] = useLazyQuery<
    MINIMAL_SEARCH_ID,
    MINIMAL_SEARCH_IDVariables
  >(GET_MINIMAL_SEARCH_ID, {
    variables: { type, query: listResult && listResult.variables && listResult.variables.query },
    onCompleted: data => {
      const results = data && data.search && data.search.data;
      setMinimalData(results || []);
      if (selected && setSelected) {
        const newSelected = results ? results : [];
        setSelected(newSelected);
        //setCheckedAll(true);
        return;
      }

      if (results && checkedItemsQuery) {
          // TODO: Migration
        /*(client as any).writeData({
          data: {
            [checkedItemsQuery.name]: results,
          },
        });*/
        //setCheckedAll(true);
      }
    },
    fetchPolicy: selected ? 'network-only' : 'cache-first',
  });

  useEffect(() => {
    if (checkedItems && checkedItems.length > 0 && checkedItems.length === minimalData.length) {
      setCheckedAll(true);
    } else {
      setCheckedAll(false);
    }
  }, [checkedItems, minimalData.length]);

  useEffect(() => {
    if (selected) {
        // TODO: Migration
      /*(client as any).writeData({
        data: {
          idCheckeds: showCheckeds ? selected && selected.map(item => item && item.id) : [],
        },
      });*/
      return;
    }

      // TODO: Migration
    /*(client as any).writeData({
      data: {
        idCheckeds: showCheckeds ? checkedItems && checkedItems.map(item => item && item.id) : [],
      },
    });*/
  }, [checkedItems, client, selected, showCheckeds]);

  const total =
    (listResult && listResult.data && listResult.data.search && listResult.data.search.total) || 0;

  const isActualite: boolean =
    window.location.hash === '#/' || window.location.hash.startsWith('#/actualite');
  const isOperation: boolean = window.location.hash.startsWith('#/operations-commerciales');
  const dataType: string = isActualite ? 'actualite' : isOperation ? 'operation' : '';

  const handleResetFields = () => {
    setFieldsState({});
    setContent({
      page,
      type,
      searchPlaceholder,
      rowsPerPage,
      contextQuery: null,
    });
  };

  const handleResetFilter = () => {
    setFieldsState({});
    setContent({
      page,
      type,
      searchPlaceholder,
      rowsPerPage,
      contextQuery: null,
    });
    //resetSearchFilters(client, isAdmin, dataType);
  };

  const handleCheckAll = () => {
    setCheckedAll(prev => !prev);
    if (type && !checkedAll) {
      getAllItems();
    }
    if (selected && setSelected) {
      if (selected.length === allTotal) {
        setSelected([]);
      }
      console.log('minimalData :>> ', minimalData);
      return;
    } else {
      if (checkedItemsQuery) {
        const results = allData && allData.search && allData.search.data;
          // TODO: Migration
        /*(client as any).writeData({
          data: {
            [checkedItemsQuery.name]: checkedAll ? null : results,
          },
        });*/
      }
    }
  };

  const handleOpenFilter = () => {
    setOpenFilter(true);
  };

  const handleShowCheckeds = () => {
      // TODO: Migration
    /*(client as any).writeData({
      data: {
        skipAndTake: {
          skip: 0,
          take: 12,
          __typename: 'SkipAndTake',
        },
      },
    });*/
    setShowCheckeds(!showCheckeds);
  };

  const onClickRefresh = () => {
    /*if (listResult && listResult.refetch) {
      listResult.refetch();
    }*/
    setFieldsState({});
    setContent({
      page,
      type,
      searchPlaceholder,
      rowsPerPage,
      contextQuery: null,
    });
  };

  const {
    content: { page, rowsPerPage, searchPlaceholder },
    setContent,
  } = useContext<ContentStateInterface>(ContentContext);

  const [fieldsState, setFieldsState] = useState<any>({});

  const handleFieldChange = (event: any): void => {
    setFieldsState({
      ...fieldsState,
      [event.target.name]:
        event.target.name === 'actived' || event.target.name === 'pharmacies.actived'
          ? event.target.value !== -1
            ? event.target.value === 1
              ? true
              : false
            : undefined
          : event.target.value,
    });
  };

  const handleRunSearch = (query: any) => {
    setOpenFilter(false);
    setContent({
      page,
      type,
      searchPlaceholder,
      rowsPerPage,
      contextQuery: query,
    });
  };

  const filter = onGroupeDAmis ? (
    <ListesGroupeAmisFilter
      fieldsState={fieldsState}
      handleFieldChange={handleFieldChange}
      handleRunSearch={handleRunSearch}
      handleResetFields={handleResetFields}
    />
  ) : onLaboPartenaire ? (
    <ListesLaboPartenaireFilter
      fieldsState={fieldsState}
      handleFieldChange={handleFieldChange}
      handleRunSearch={handleRunSearch}
      handleResetFields={handleResetFields}
    />
  ) : onServicePartner ? (
    <ListesPartenaireServiceFilter
      fieldsState={fieldsState}
      handleFieldChange={handleFieldChange}
      handleRunSearch={handleRunSearch}
      handleResetFields={handleResetFields}
    />
  ) : onGroupeDAmis ? (
    <ListesGroupeAmisFilter
      fieldsState={fieldsState}
      handleFieldChange={handleFieldChange}
      handleRunSearch={handleRunSearch}
      handleResetFields={handleResetFields}
    />
  ) : onServicePartner ? (
    <ListesPartenaireServiceFilter
      fieldsState={fieldsState}
      handleFieldChange={handleFieldChange}
      handleRunSearch={handleRunSearch}
      handleResetFields={handleResetFields}
    />
  ) : null;

  const numSelected = (): number => {
    // if (selected && setSelected) {
    //   return selected.length > total ? total : selected.length;
    // }
    return checkedItems.length;
  };

  return (
    <Box className={classnames(classes.root)}>
      {loading && <Backdrop />}
      {/* {filter} */}
      <Box className={classes.title}>
        <Typography color="inherit" variant="subtitle1">
          {`${numSelected()}/${allTotal}`} {numSelected() > 1 ? 'sélectionnés' : 'sélectionné'}{' '}
          {`(${total})`}
        </Typography>
      </Box>
      <Box>
        <Box className={classes.buttons}>
          {/* <CustomCheckbox
            variant="text"
            color="inherit"
            size="medium"
            name="checkedAll"
            label={`Tout sélectionner (${allTotal})`}
            value={checkedAll}
            checked={checkedAll}
            onChange={handleCheckAll}
          /> */}
          {filter && (
            <CustomButton
              variant="text"
              color="inherit"
              size="medium"
              startIcon={<Tune />}
              className={classes.btn}
              onClick={handleOpenFilter}
            >
              Filtres
            </CustomButton>
          )}
          {/*showResetFilters && (
            <CustomButton
              variant="text"
              color="inherit"
              size="medium"
              startIcon={<Tune />}
              className={classes.btn}
              onClick={handleResetFilter}
            >
              Filtres
            </CustomButton>
          )*/}
          {checkedItems && numSelected() > 0 && (
            <>
              {/* <CustomButton
                variant="text"
                color="inherit"
                size="medium"
                startIcon={showCheckeds ? <ListIcon /> : <ListAltIcon />}
                className={classes.btn}
                onClick={handleShowCheckeds}
              >
                {showCheckeds ? 'Afficher les résultats' : 'Afficher les éléments selectionnés'}
              </CustomButton> */}

              <CustomButton
                variant="text"
                color="inherit"
                size="medium"
                startIcon={<Delete />}
                className={classes.btn}
                onClick={clearSelection}
              >
                Vider la sélection
              </CustomButton>
            </>
          )}
          <CustomButton
            variant="text"
            color="inherit"
            size="medium"
            startIcon={<Refresh />}
            className={classes.btn}
            onClick={onClickRefresh}
          >
            Réactualiser
          </CustomButton>
        </Box>
      </Box>
      <CustomModal
        title="Filtres de recherche"
        children={filter}
        open={openFilter}
        setOpen={setOpenFilter}
        withBtnsActions={false}
        headerWithBgColor={true}
        closeIcon={true}
      />
    </Box>
  );
};

export default withRouter(EnhancedTableToolbar);
