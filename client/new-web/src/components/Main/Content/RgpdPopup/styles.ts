import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      fontFamily: 'Roboto',
      letterSpacing: 0,
    },
    image: {
      width: 413,
      height: 238,
    },
    flexRowContainer: {
      width: '100%',
      position: 'relative',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
    },
    title: {
      // maxWidth: 490,
      textAlign: 'center',
      marginTop: 35,
      marginBottom: 20,
      fontSize: 18,
      fontWeight: 'bold',
      color: theme.palette.common.black,
    },
    titleWithoutMargin: {
      margin: 0,
    },
    description: {
      fontSize: 16,
      fontWeight: 'normal',
      color: '#9E9E9E',
      //  maxWidth: 490,
      margin: '0px 20px',
      textAlign: 'justify',
    },
    btnsContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      margin: '50px 0px',
      '& > button': {
        height: 50,
        width: 235,
      },
      '& > button:nth-child(2)': {
        marginLeft: 30,
      },
    },
    btnWithUnderline: {
      textDecoration: 'underline',
    },
    arrowBack: {
      // position: 'absolute',
      // left: -35,
      marginRight: 15,
    },
    accueilPlusDesc: {
      marginTop: 20,
      marginBottom: 30,
    },
    row: {
      marginBottom: 20,
      display: 'flex',
      justifyContent: 'space-between',
      width: '100%',
    },
    list: {
      width: '100%',
    },
    loaderContainer: {
      width: '100%',
      display: 'flex',
      minHeight: 100,
    },
    userInfoBtnContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      cursor: 'pointer',
      width: 'fit-content',
      '& > p': {
        marginLeft: 10,
      },
    },
    userInfoContainer: {
      display: 'flex',
      flexDirection: 'column',
      margin: '10px 35px',
      '& p': {
        wordBreak: 'break-all',
        fontSize: 14,
        fontWeight: 'normal',
        letterSpacing: 0,
        '& > span': {
          fontWeight: 'bold',
        },
      },
    },
  }),
);

export default useStyles;
