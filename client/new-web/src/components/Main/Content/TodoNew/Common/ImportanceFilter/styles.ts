import { createStyles, Theme } from '@material-ui/core/styles';
import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      '& .MuiAccordionSummary-root': {
        flexDirection: 'row-reverse',
        '&.Mui-expanded': {
          minHeight: 45,
        },
        '& .MuiAccordionSummary-expandIcon': {
          padding: 0,
          marginRight: 10,
        },
      },
      '& .MuiAccordionSummary-content': {
        justifyContent: 'space-between',
        margin: 0,
        '&.Mui-expanded': {
          margin: '11px 0',
        },
      },
      '& .MuiAccordion-root': {
        boxShadow: 'none!important',
        '&.Mui-expanded': {
          margin: '0',
        },
      },
      '& .MuiAccordionDetails-root': {
        padding: '0px!important',
        '& .MuiList-root': {
          width: '100%',
          paddingTop: 0,
          paddingBottom: 0,
          '& .MuiListItem-button': {
            paddingRight: 0,
            paddingTop: 0,
            paddingBottom: 0,
            '& .MuiIconButton-root': {
              padding: 10,
            },
          },
        },
      },
      // '& .MuiListItem-gutters': {
      //   paddingLeft: 8,
      // },
    },
    icon: {},
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular,
      display: 'flex',
      alignItems: 'center',
      paddingLeft: 8,
    },
    title: {
      color: '#212121',
      fontFamily: 'Roboto',
      marginLeft: 10,
      fontSize: '0.875rem',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      '-webkit-line-clamp': 1,
      '-webkit-box-orient': 'vertical',
      maxWidth: 280,
    },
  }),
);
