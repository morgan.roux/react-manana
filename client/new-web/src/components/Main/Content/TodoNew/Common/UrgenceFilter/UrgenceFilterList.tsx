import { Box, List, ListItem, ListItemText, Checkbox, Typography } from '@material-ui/core';
import { Error } from '@material-ui/icons';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { useStyles } from './styles';

export interface UrgenceInterface {
  checked: boolean;
  __typename?: 'Urgence';
  id?: string;
  code?: string | null;
  couleur?: string | null;
  libelle?: string | null;
}

interface UrgenceFilterListProps {
  urgence: UrgenceInterface[] | null | undefined;
  setUrgence: (urgence: UrgenceInterface[] | null | undefined) => void;
}

const UrgenceFilterList: FC<UrgenceFilterListProps & RouteComponentProps> = ({
  urgence,
  setUrgence,
}) => {
  const classes = useStyles({});

  const handleChange = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>,
    item: UrgenceInterface,
  ) => {
    setUrgence(
      (urgence || []).map(currentItem => {
        if (currentItem.id === item.id) {
          currentItem.checked = !currentItem.checked;
        }

        return currentItem;
      }),
    );
  };

  console.log('**************************Urgence filter', urgence)
  return (
    <List>
      {urgence?.map(
        (item, index) =>
          item && (
            <ListItem
              dense={true}
              button={true}
              key={`${item.id}-${item.checked}`}
              onClick={event => handleChange(event, item)}
            >
              <Box display="flex" alignItems="center">
                <Checkbox checked={item.checked} tabIndex={-1} disableRipple={true} />
                <ListItemText
                  primary={
                    <Box display="flex" alignItems="center">
                      <Error style={{ color: item.couleur || 'black' }} />
                      <Typography
                        className={classes.title}
                        style={{ color: item.couleur || 'black' }}
                      >
                        {`${item.code} : ${item.libelle}`}
                      </Typography>
                    </Box>
                  }
                />
              </Box>
              {/*
                      <Box>
                        <Typography className={classes.nbrProduits}>{item.count}</Typography>
                      </Box>
                    */}
            </ListItem>
          ),
      )}
    </List>
  );
};

export default withRouter(UrgenceFilterList);
