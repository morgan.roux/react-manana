import {
  AppBar,
  Badge,
  Box,
  CssBaseline,
  Hidden,
  IconButton,
  Tab,
  Tabs,
  Typography,
} from '@material-ui/core';
import { Add, KeyboardArrowLeft, KeyboardArrowRight } from '@material-ui/icons';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import moment from 'moment';
import React, { FC, ReactNode, useState } from 'react';
import { RouteComponentProps, useLocation, withRouter } from 'react-router';
import AjoutParticipant from '../../Modals/AjoutParticipants';
import { CountTodosProps } from '../../TodoNew';
import ActionButton from '../ActionButton';
import { countActionsAfterOffsetFromNow, filterActionsByOffsetFromNow } from '../utils/actions';
import useStyles from './styles';

const daysRequired = 7;

interface MainHeaderProps {
  match: {
    params: {
      filter: string | undefined;
      filterId: string | undefined;
    };
  };
  withDate?: boolean;
  title: string;
  actionIconButton?: ReactNode;
  handleSetOpenModalTache: (dateDebut: any) => void;
  onChangeOffsetFromNow: (offset: number) => void;
  actions?: any;
  participants?: number | null | undefined;
  setTriage?: (triage: any[]) => void;
  refetchSection: any;
  setCacheTacheAchevE?: (cacheTacheAchevE: boolean) => void;
  cacheTacheAchevE?: boolean;
  statutMainHeader?: boolean;
  row?: any;
  parameters: any;
  refetchAll?: () => void;
  useMatriceResponsabilite: boolean
}

const MainHeader: FC<MainHeaderProps & RouteComponentProps & CountTodosProps> = ({
  title,
  actions,
  withDate,
  actionIconButton,
  statutMainHeader,
  row,
  handleSetOpenModalTache,
  participants,
  match: {
    params: { filter },
  },
  history,
  onChangeOffsetFromNow,
  setTriage,
  refetchSection,
  setCacheTacheAchevE,
  cacheTacheAchevE,
  refetchCountTodos,
  parameters,
  refetchAll,
  useMatriceResponsabilite
}) => {




  const location = useLocation();
  const handleClickIcon = () => { };
  const date = moment().format('dddd Do MMMM YYYY');
  const classes = useStyles({});
  const [openModal, setOpenModal] = React.useState(false);
  const [selectedUsersIds, setselectedUsersIds] = useState<string[]>([]);
  const [daysOffset, setDaysOffset] = useState<number>(1)
  const [daysTabs, setDaysTabs] = useState<any>([])
  const [totalTacheRestantes, setTotalTacheRestantes] = useState<number>(0)


  const handleOpenModal = () => {
    setOpenModal(!openModal);
  };

  const isOnNextWeek = filter === 'sept-jours';
  const isOnNextWeekTeam = filter === 'sept-jours-equipe';
  const isOnProject = filter === 'projet';


  const [currentTab, setCurrentTab] = useState<number>(0);

  const handleTabsChange = (event: React.ChangeEvent<{}>, value: any) => {
    setCurrentTab(value);
    const activeDate = daysTabs[value];
    if (onChangeOffsetFromNow) {
      onChangeOffsetFromNow(activeDate.offset)
    }

  };


  React.useEffect(() => {
    const days: any[] = [];


    const monday = moment().startOf('week').isoWeekday(0);
    const diffDays = moment().diff(monday, 'days')

    for (let i = daysOffset - diffDays; i <= daysRequired + daysOffset - diffDays; i++) {
      days.push({
        id: `tab-${i}`,
        label: moment()
          .add(i, 'days')
          .format('DD'),
        date: moment()
          .add(i, 'days')
          .format('ddd'),

        value: moment()
          .add(i, 'days')
          .format('YYYY-MM-DD'),

        offset: i
      });
    }


    setDaysTabs(days)



  }, [daysOffset])


  React.useEffect(() => {

    if (daysTabs.length > 0) {
      const minDayTabIndex = daysTabs.findIndex((dayTab) => dayTab.offset > 0)

      setCurrentTab(minDayTabIndex)
      if (onChangeOffsetFromNow) {
        onChangeOffsetFromNow(daysTabs[minDayTabIndex].offset)
      }
    }


  }, [daysTabs, filter])

  React.useEffect(() => {
    if (daysTabs.length > 0) {
      setTotalTacheRestantes(daysTabs[daysTabs.length - 1]?.offset ? countActionsAfterOffsetFromNow(actions, daysTabs[daysTabs.length - 1].offset) : 0)
    }
  }, [daysTabs, actions])




  const handleScrollNext = () => {
    //history.push(`/todo/${filter}?offset=${daysOffset + daysRequired}`)
    setDaysOffset((prev) => prev + daysRequired)
  }

  const handleScrollPrev = () => {
    if (daysOffset > 1) {
      //history.push(`/todo/${filter}?offset=${daysOffset - daysRequired}`)

      setDaysOffset((prev) => prev - daysRequired)
    }
  }

  const onTaskCalendar = isOnNextWeek || isOnNextWeekTeam

  return (
    <Box className={classes.root}>
      <Box display="flex" flexDirection="column" width="100%">
        <Box display="flex" alignItems="flex-end">
          <CssBaseline />
          <Hidden mdDown={true}>
            <Typography className={classes.title}>{onTaskCalendar ? `${moment().add(daysOffset, 'days').format('MMM YYYY')}` : title}</Typography>
          </Hidden>

          {withDate && <Typography className={classes.date}>{date}</Typography>}
        </Box>
        <Box className={classes.nextWeekContainer}>
          {(onTaskCalendar) && (
            <AppBar className={classes.appBarCalendar} position="static">
              <Tabs
                value={currentTab}
                onChange={handleTabsChange}
                variant="scrollable"
                scrollButtons="on"
                aria-label="simple tabs example"

                ScrollButtonComponent={(item) => {
                  if (item.direction === 'left' && daysOffset === 1) {
                    return null
                  }
                  return <IconButton onClick={item.direction === 'right' ? handleScrollNext : handleScrollPrev} style={{ borderRadius: 0 }}>{item.direction === 'right' ? (<KeyboardArrowRight />) : (<KeyboardArrowLeft />)}{item.direction === 'right' && totalTacheRestantes > 0 ? <span style={{ fontSize: 12, marginRight: 2 }}>{totalTacheRestantes} </span> : null}</IconButton>
                }}
              >
                {(onTaskCalendar) &&
                  daysTabs.map(day => {

                    const count = filterActionsByOffsetFromNow(actions, day.offset).length

                    return (<Tab
                      disabled={day.offset <= 0}
                      key={day.id}
                      label={
                        <Box height={75}>
                          <Typography className={classes.jourName}>{day.date}</Typography>
                          <Typography className={classes.jourNombre}>{day.label}</Typography>
                          {count > 0 && <Typography >({count})</Typography>}
                        </Box>
                      }
                    />
                    )
                  })}
              </Tabs>
            </AppBar>
          )}
        </Box>
        {isOnProject && (
          <Box display="flex" alignItems="center">
            <Typography className={classes.labelParticipant}>Participant(s)</Typography>
            <Typography className={classes.count}>{participants}</Typography>
          </Box>
        )}

        <Typography className={classes.addTaskButton} onClick={() => handleSetOpenModalTache(isOnNextWeek || isOnNextWeekTeam ? moment().add(daysTabs[currentTab].offset, 'days').toDate() : moment().toDate())}>
          <Add />
          Ajouter une tâche
        </Typography>
      </Box>
      <Box display="flex" alignItems="center">
        <IconButton onClick={handleClickIcon}>{actionIconButton}</IconButton>
        {((!isOnNextWeek || !isOnNextWeekTeam)) && (
          <Box display="flex">
            {/*location && location.pathname && location.pathname.includes('/todo/projet') && (
              <IconButton onClick={handleOpenModal} className={classes.addParticipantIcon}>
                <GroupAddIcon />
              </IconButton>
            )*/}
            <CssBaseline />
            <Hidden mdDown={true}>
              <ActionButton
                actionMenu="HEADER_MENU"
                setTriage={setTriage}
                refetchSection={refetchSection}
                setCacheTacheAchevE={setCacheTacheAchevE}
                cacheTacheAchevE={cacheTacheAchevE}
                statutMainHeader={statutMainHeader}
                refetchCountTodos={refetchCountTodos}
                row={row}
                parameters={parameters}
                refetchAll={refetchAll}
              />
            </Hidden>

            <AjoutParticipant
              openModalParticipant={openModal}
              setOpenParticipant={setOpenModal}
              userIds={selectedUsersIds}
              setUserIds={setselectedUsersIds}
              idProjectTask={(row && row.id) || ''}
            />
          </Box>
            )}
      </Box>
    </Box>
  );
};

export default withRouter(MainHeader);
