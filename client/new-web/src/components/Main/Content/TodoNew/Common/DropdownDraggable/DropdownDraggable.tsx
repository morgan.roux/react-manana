import { Box, Icon, IconButton, List, ListItem } from '@material-ui/core';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import { Add } from '@material-ui/icons';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import React, { ReactNode } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import useStyles from './styles';

interface Ielement {
  isActive?: boolean;
  icon: ReactNode;
  type?: string;
  subProjects?: any;
  actionButton: ReactNode;
  title: string;
  count: number;
  id?: string;
  typeName?: string;
}
interface Ilist {
  title: string;
  withAddAction: boolean;
  element: Ielement[];
  addAction?: (item?: any) => void;
}
interface DropdownDraggableProps {
  list: Ilist;
  handleProjectClick?: (project: Ielement) => void;
  handleSubProjectClick?: (project: Ielement) => void;
  handleEtiquetteClick?: (project: Ielement) => void;
  clickable?: boolean;
  setShowMainContent?: (value: boolean) => void;
  match?: {
    params: {
      filterId: string;
    };
  };
  defaultExpanded?: boolean;
  disabled?: boolean;
  project?: Ielement;
  countSubProjects?: number;
  parent?: Ielement;
  niveauMatriceFonctions?: number
}

const DropdownDraggable: React.FC<DropdownDraggableProps & RouteComponentProps> = props => {
  const {
    list,
    handleProjectClick,
    handleSubProjectClick,
    handleEtiquetteClick,
    clickable,
    match: {
      params: { filterId },
    },
    defaultExpanded,
    disabled = false,
    setShowMainContent,
    parent,
    project,
    countSubProjects,
    niveauMatriceFonctions
  } = props;
  const classes = useStyles({});

  const clickSubProject = project => {
    if (handleSubProjectClick) {
      handleSubProjectClick(project);
    }
  };

  const onClickProject = (item: any) => {
    if (handleProjectClick) handleProjectClick(item);
    if (setShowMainContent) setShowMainContent(true);
  };

  const onClickEtiquette = (item: any) => {
    if (handleEtiquetteClick) handleEtiquetteClick(item);
    if (setShowMainContent) setShowMainContent(true);
  };

  return (
    <Box className={classes.root}>
      <Accordion key={list.title} defaultExpanded={defaultExpanded}>
        <AccordionSummary
          expandIcon={((project && (niveauMatriceFonctions !== 1)) || !project) && <ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          style={{
            background: project && project.id === filterId ? '#eee' : undefined
          }}
        >
          <Box onClick={(event) => {
            if (project) {
              event.stopPropagation()
              onClickProject(project)
            }

          }}
            display="flex"
            alignItems="center"
            width="100%"
            justifyContent="space-between"
          >
            <Typography className={classes.heading}>
              <Box marginRight="8px">{parent && parent.icon}</Box>

              {list.title}
            </Typography>
            <Box display="flex" alignItems="center">
              {parent && parent.actionButton}

              {project && (<Typography className={classes.count}>
                {niveauMatriceFonctions===1? project.count :countSubProjects}
              </Typography>)}


              {list.withAddAction && (
                <IconButton
                  className={classes.addButton}
                  onClick={(event: React.MouseEvent<HTMLElement>) => {
                    event.stopPropagation();
                    if (list.addAction) list.addAction();
                  }}
                >
                  <Add />
                </IconButton>
              )}
            </Box>
          </Box>
        </AccordionSummary>
        <AccordionDetails>
          <List>
            {list && list.element ? (
              list.element.map(item => {
                if (disabled && (item.subProjects || []).length === 0) {
                  return null;
                }

                return (
                  <>
                    {item && item.subProjects ? (
                      <Box marginLeft={3} className={classes.subDropdown}>
                        <DropdownDraggable
                          {...props}
                          disabled={false}
                          project={item}
                          niveauMatriceFonctions={niveauMatriceFonctions}
                          countSubProjects={item.subProjects.reduce((total, currentSubProject) => total + currentSubProject.count, 0)}
                          list={{
                            title: item && item.title,
                            element: item.subProjects,
                            addAction: list.addAction,
                            withAddAction: list.withAddAction,
                          }}
                          handleProjectClick={handleProjectClick}
                          handleSubProjectClick={handleSubProjectClick}
                          clickable={false}
                          setShowMainContent={setShowMainContent}
                          parent={item}
                        />
                      </Box>
                    ) : (
                        <Box marginLeft={parent ? 6 : 3} paddingRight={parent ? 2 : 0}>
                          <ListItem
                            button={true}
                            selected={filterId === item.id}
                            // tslint:disable-next-line: jsx-no-lambda
                            onClick={() => {
                              if (!disabled && item.typeName) {
                                switch (item.typeName) {
                                  case 'TodoEtiquette':
                                    if (handleEtiquetteClick) {
                                      onClickEtiquette(item);
                                    }
                                    break;
                                  case 'Project':
                                    if (handleSubProjectClick && handleProjectClick) {
                                      onClickProject(item);
                                    }
                                    break;
                                  default:
                                    break;
                                }
                              }
                            }}
                          >
                            <Box
                              width="100%"
                              display="flex"
                              alignItems="center"
                              justifyContent="space-between"
                              paddingBottom="10px"
                              paddingTop="10px"
                            >
                              <Box display="flex" alignItems="center">
                                <Icon className={classes.icon}>{item.icon}</Icon>
                                <Typography className={classes.title}>
                                  {item && item.title}
                                </Typography>
                              </Box>
                              <Box display="flex" alignItems="center">
                                {item.type && (
                                  <Typography className={classes.type}>
                                    {item && item.type}
                                  </Typography>
                                )}
                                {!disabled && (
                                  <Typography className={classes.count}>
                                    {item?.count ?? '0'}
                                  </Typography>
                                )}
                                {item.typeName === 'TodoEtiquette' && item.actionButton}
                              </Box>
                            </Box>
                          </ListItem>
                        </Box>
                      )}
                  </>
                );
              })
            ) : (
                <Typography className={classes.placeholder}>
                  Votre liste de {list.title} s'affichera ici
                </Typography>
              )}
          </List>
        </AccordionDetails>
      </Accordion>
    </Box>
  );
};
export default withRouter(DropdownDraggable);
