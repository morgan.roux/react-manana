import { useApolloClient, useMutation } from '@apollo/client';
import { ChangeEvent, useState } from 'react';
import { DO_CREATE_UPDATE_SECTION } from '../../../../../../graphql/Section/mutation';
import SnackVariableInterface from '../../../../../../Interface/SnackVariableInterface';
import { getUser } from '../../../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';

export const useCreateUpdateSection = (
  values: any,
  refetch: any,
  filter: string | undefined,
  setOpenAddSection: (value: boolean) => void,
): [any] => {
  const isOnReception = !filter || filter === 'recu';
  const client = useApolloClient();
  const user = getUser();
  const inBoxVariable = {
    isInInbox: true,
    libelle: values && values.libelle,
    idUser: user.id,
  };
  const [doCreateUpdateSection] = useMutation(DO_CREATE_UPDATE_SECTION, {
    variables: {
      input: inBoxVariable,
    },
    onCompleted: data => {
      if (refetch) refetch();
      const snackBarData: SnackVariableInterface = {
        type: 'SUCCESS',
        message: 'Section ajoutée avec succès',
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
      setOpenAddSection(false);
    },
  });
  return [doCreateUpdateSection];
};

export const useSection = () => {
  const [values, setValues] = useState<any>({});
  const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement | any>) => {
    const { name, value } = e.target;
    setValues(prevState => ({
      ...prevState,
      [name]: value,
    }));
  };
  return [values, setValues, handleChange];
};
