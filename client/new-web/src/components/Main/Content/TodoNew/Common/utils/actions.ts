import moment from 'moment';
import { endTime, startTime } from '../../../DemarcheQualite/util';

export const filterActionsByOffsetFromNow = (actions, offsetFromNow: number) => {
    if (!actions) {
        return []
    }
    const startDay = startTime(moment().add(offsetFromNow, 'days'))
    const startDayAsMoment = moment(startDay)

    return actions.filter((action: any) => {
        if (!action.dateDebut) {
            return false
        }
        if (action.dateFin) {
            const startDateDebut = moment(startTime(moment(action.dateDebut)))
            const endDateFin = moment(endTime(moment(action.dateFin)))

            return startDayAsMoment.isSameOrAfter(startDateDebut) && startDayAsMoment.isSameOrBefore(endDateFin)
        }

        const dateDebut = startTime(moment(action.dateDebut))
        return dateDebut === startDay

    })
}

export const countActionsAfterOffsetFromNow = (actions: any[], offsetFromNow: number) => {
    if (!actions) {
        return 0
    }

    const startDay = startTime(moment().add(offsetFromNow, 'days'))
    const startDayAsMoment = moment(startDay)

    return actions.filter((action: any) => {
        if (!action.dateDebut) {
            return false
        }

        const startDateDebut = moment(startTime(moment(action.dateDebut)))
        return startDateDebut.valueOf() >= startDayAsMoment.valueOf()
    }).length


} 