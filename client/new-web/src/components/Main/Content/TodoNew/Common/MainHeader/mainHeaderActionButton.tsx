import { Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Edit, Favorite, Person, MoreHoriz } from '@material-ui/icons';
import React, { useState } from 'react';
import { defaultContent } from '../../../../../../AppContext';
import { ActionButtonMenu } from '../ActionButton';

interface ActionButtonProps {
  actions: any;
  SetHideDoneTask: (task: any) => void;
  hideDoneTask: any;
  setTriage?: (triage: any[]) => void;
}
const MainHeaderActionButton: React.FC<ActionButtonProps> = props => {
  const { SetHideDoneTask, hideDoneTask, actions, setTriage } = props;
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const open = anchorEl ? true : false;

  const handleDoneTask = () => {
    SetHideDoneTask && SetHideDoneTask(!hideDoneTask);
  };

  const handleFiltre = e => {
    if (setTriage) {
      switch (e) {
        case 1:
          setTriage([{ dateCreation: { order: 'asc' } }]);
          break;
        case 2:
          setTriage([{ dateDebut: { order: 'asc' } }]);
          break;
        case 3:
          setTriage([{ priority: { order: 'asc' } }]);
          break;
        case 4:
          setTriage([{ description: { order: 'asc' } }]);
          break;
        case 5:
          setTriage([{ isAssigned: { order: 'desc' } }]);
          break;
        default:
          setTriage([{ dateCreation: { order: 'asc' } }]);
          break;
      }
    }
  };

  const headerMenuItems: ActionButtonMenu[] = [
    //   {
    //     label: handleDoneTask
    //       ? 'Affichez tous les taches achevées'
    //       : 'Masquez tous les taches achevées',
    //     icon: handleDoneTask ? <VisibilityIcon /> : <VisibilityOffIcon />,
    //     onClick: handleDoneTask,
    //     disabled: false,
    //   },
    //   { label: 'Modifier', icon: <Edit />, onClick: handleClick, disabled: false },
    //   {
    //     label: 'Ajouter une section',
    //     icon: <ViewDayIcon />,
    //     onClick: handleOpenAjoutSectionDialog,
    //     disabled: false,
    //   },
    //   {
    //     label: 'Retirer des favoris',
    //     icon: <Favorite />,
    //     onClick: handleClick,
    //     disabled: false,
    //   },
    //   {
    //     label: 'Trier par date de création',
    //     icon: <CalendarTodayIcon />,
    //     onClick: () => handleFiltre(1),
    //     disabled: false,
    //   },
    //   {
    //     label: "Trier par date d'echéance",
    //     icon: <EventIcon />,
    //     onClick: () => handleFiltre(2),
    //     disabled: false,
    //   },
    //   {
    //     label: 'Trier par priorité',
    //     icon: <FlagIcon />,
    //     onClick: () => handleFiltre(3),
    //     disabled: false,
    //   },
    //   {
    //     label: 'Trier par nom',
    //     icon: <SortByAlphaIcon />,
    //     onClick: () => handleFiltre(4),
    //     disabled: false,
    //   },
    //   {
    //     label: 'Trier par responsable',
    //     icon: <Person />,
    //     onClick: () => handleFiltre(5),
    //     disabled: false,
    //   },
    //   { label: 'Archiver', icon: <ArchiveIcon />, onClick: handleClick, disabled: false },
  ];

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <IconButton aria-controls="simple-menu" aria-haspopup="true">
        <MoreHoriz />
      </IconButton>
      <Menu
        // tslint:disable-next-line: jsx-no-lambda
        onClick={event => {
          event.preventDefault();
          event.stopPropagation();
        }}
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        {headerMenuItems &&
          headerMenuItems.length > 0 &&
          headerMenuItems.map((i: ActionButtonMenu, index: number) => (
            <MenuItem
              // tslint:disable-next-line: jsx-no-lambda
              onClick={event => {
                event.preventDefault();
                event.stopPropagation();
                handleClose();
                i.onClick(event);
              }}
              key={`table_menu_item_${index}`}
              disabled={i.disabled}
            >
              <ListItemIcon>{i.icon}</ListItemIcon>
              <Typography variant="inherit">{i.label}</Typography>
            </MenuItem>
          ))}
      </Menu>
    </>
  );
};
export default MainHeaderActionButton;
