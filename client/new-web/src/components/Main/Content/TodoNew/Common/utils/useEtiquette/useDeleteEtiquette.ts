import { useMutation, useApolloClient } from '@apollo/client';
import { differenceBy } from 'lodash';
import { DO_DELETE_ETIQUETTE, GET_SEARCH_ETIQUETTE } from '../../../../../../../graphql/Etiquette';
import {
  DELETE_ETIQUETTE,
  DELETE_ETIQUETTEVariables,
} from '../../../../../../../graphql/Etiquette/types/DELETE_ETIQUETTE';
import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';

export const useDeleteEtiquette = (
  queryVariable,
  setOpenDeleteDialog: (value: boolean) => void,
) => {
  const client = useApolloClient();
  const [
    removeMutation,
    { loading: deleteEtiquetteLoading, error: deleteEtiquetteError },
  ] = useMutation<DELETE_ETIQUETTE, DELETE_ETIQUETTEVariables>(DO_DELETE_ETIQUETTE, {
    update: (cache, { data }) => {
      if (data && data.softDeleteTodoEtiquettes) {
        if (queryVariable) {
          const req: any = cache.readQuery({
            query: GET_SEARCH_ETIQUETTE,
            variables: queryVariable,
          });
          if (req && req.search && req.search.data) {
            const source = req.search.data;
            const result = data.softDeleteTodoEtiquettes;
            const dif = differenceBy(source, result, 'id');
            cache.writeQuery({
              query: GET_SEARCH_ETIQUETTE,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: dif,
                  },
                },
              },
              variables: queryVariable,
            });
          }
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
    onCompleted: data => {
      if (data && data.softDeleteTodoEtiquettes) {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Suppression réussie',
        });
      }
      setOpenDeleteDialog(false);
    },
  });
  const removeEtiquette = row => {
    removeMutation({
      variables: {
        ids: [row.id],
      },
    });
  };

  return [removeEtiquette];
};
