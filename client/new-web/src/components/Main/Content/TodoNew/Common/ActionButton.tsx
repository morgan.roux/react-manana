import { useApolloClient, useMutation } from '@apollo/client';
import { Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Add, Delete, Edit, Favorite, MoreHoriz, PersonAdd, Person } from '@material-ui/icons';
import React, { FC, ReactNode, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import AjouterDessous from '../../../../../assets/icons/todo/ajouter_dessous.svg';
import AjouterDessus from '../../../../../assets/icons/todo/ajouter_dessus.svg';
import Archiver from '../../../../../assets/icons/todo/archiver.svg';
import EtiquettePartagee from '../../../../../assets/icons/todo/deplacer_etiquette_partage.svg';
import RetirerFavoris from '../../../../../assets/icons/todo/retirer_favoris.svg';
import {
  DO_DELETE_ETIQUETTE,
  DO_CREATE_UPDATE_ETIQUETTE,
} from '../../../../../graphql/Etiquette/mutation';
import { GET_SEARCH_ETIQUETTE } from '../../../../../graphql/Etiquette/query';
import {
  DELETE_ETIQUETTE,
  DELETE_ETIQUETTEVariables,
} from '../../../../../graphql/Etiquette/types/DELETE_ETIQUETTE';
import { DO_DELETE_PROJECT, DO_CREATE_PROJECT } from '../../../../../graphql/Project/mutation';
import {
  DELETE_PROJECTS,
  DELETE_PROJECTSVariables,
} from '../../../../../graphql/Project/types/DELETE_PROJECTS';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { differenceBy } from 'lodash';
import { GET_SEARCH_CUSTOM_CONTENT_PROJECT } from '../../../../../graphql/Project/query';
import AjoutSectionModal from '../Modals/AjoutSection';
import { DO_DELETE_ACTIONS } from '../../../../../graphql/ActionNew/mutation';
import {
  DELETE_ACTIONS,
  DELETE_ACTIONSVariables,
} from '../../../../../graphql/ActionNew/types/DELETE_ACTIONS';
import { GET_SEARCH_ACTION } from '../../../../../graphql/Action/query';
import Backdrop from '../../../../Common/Backdrop/Backdrop';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import ViewDayIcon from '@material-ui/icons/ViewDay';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import EventIcon from '@material-ui/icons/Event';
import FlagIcon from '@material-ui/icons/Flag';
import SortByAlphaIcon from '@material-ui/icons/SortByAlpha';
import ArchiveIcon from '@material-ui/icons/Archive';
import { useCreateUpdateSection, useSection } from './utils/useSection';
import ConfirmDialog, { ConfirmDeleteDialog } from '../../../../Common/ConfirmDialog';
import AjoutTaches from '../Modals/AjoutTache/AjoutTaches';
import AjoutEtiquette from '../Modals/AjoutEtiquette';
import AjoutProjet from '../Modals/AjoutProjet';
import {
  CREATE_UPDATE_TODO_ETIQUETTE,
  CREATE_UPDATE_TODO_ETIQUETTEVariables,
} from '../../../../../graphql/Etiquette/types/CREATE_UPDATE_TODO_ETIQUETTE';
import {
  CREATE_PROJECT,
  CREATE_PROJECTVariables,
} from '../../../../../graphql/Project/types/CREATE_PROJECT';
import { ME_me } from '../../../../../graphql/Authentication/types/ME';
import { getPharmacie, getUser } from '../../../../../services/LocalStorage';
import AjoutParticipant from '../Modals/AjoutParticipants';
import AddIcon from '@material-ui/icons/Add';
import PlaylistAddIcon from '@material-ui/icons/PlaylistAdd';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { CountTodosProps } from '../TodoNew';

export interface ActionButtonMenu {
  label: any;
  icon: ReactNode;
  disabled: boolean;
  onClick: (event) => any;
  setState?: (row: any) => void;
  setOperationName?: (operationName: string) => void;
  hide?: boolean;
}

export interface ActionButtonProps {
  task?: any;
  content?: any;
  row?: any; // this will remain optional till we get the shared etiquette
  actionMenu?: string;
  queryVariable?: any;
  projectVariable?: any;
  etiquetteVariable?: any;
  variableTache?: any;
  refetch?: any;
  match: {
    params: {
      filter: string | undefined;
      filterId: string | undefined;
    };
  };
  setTriage?: (triage: any[]) => void;
  refetchSection?: any;
  setCacheTacheAchevE?: (cacheTacheAchevE: boolean) => void;
  cacheTacheAchevE?: boolean;
  statutMainHeader?: boolean;
  parameters?: any;
  refetchAll?: () => void;
}

const Actionbutton: FC<ActionButtonProps & RouteComponentProps & CountTodosProps> = ({
  actionMenu,
  row,
  statutMainHeader,
  queryVariable,
  variableTache,
  refetch,
  task,
  match: {
    params: { filter, filterId },
  },
  location: { pathname },
  setTriage,
  refetchSection,
  setCacheTacheAchevE,
  cacheTacheAchevE,
  refetchCountTodos,
  parameters,
  refetchAll,
}) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const open = anchorEl ? true : false;
  const [openAddSection, setOpenAddSection] = React.useState(false);
  const [openModalTache, setOpenModalTache] = useState<boolean>(false);
  const [openDeleteDialogTask, setOpenDeleteDialogTask] = useState<boolean>(false);
  const [openDeleteProjectDialog, setOpenDeleteProjectDialog] = useState<boolean>(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [openEditEtiquette, setOpenEditEtiquette] = useState<boolean>(false);

  // const [triage, setTriage] = useState<any[]>();

  const [sectionValues, setSectionValues, handleSectionChange] = useSection();
  const [parentProject, setParentProject] = useState<any>();
  const [projectToEdit, setProjectToEdit] = useState<any>();
  const [doCreateUpdateSection] = useCreateUpdateSection(
    sectionValues,
    refetchSection,
    filter,
    filterId,
    setSectionValues,
    setOpenAddSection,
  );

  const [openShare, setOpenShare] = useState<boolean>(false);
  const [selectedUsersIds, setselectedUsersIds] = useState<string[]>([]);

  const [openShareEtiquette, setOpenShareEtiquette] = useState<boolean>(false);

  const [openAddToFavorite, setOpenAddToFavorite] = useState<boolean>(false);
  const [openArchived, setOpenArchived] = useState<boolean>(false);
  const [openProject, setOpenProject] = useState<boolean>(false);

  const handleTacheAchevE = () => {
    if (setCacheTacheAchevE) setCacheTacheAchevE(!cacheTacheAchevE);
  };

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    if (event) {
      event.stopPropagation();
      setAnchorEl(event.currentTarget);
    }
  };

  const handlesetOpenDialogDelete = () => {
    setOpenDeleteDialogTask(!openDeleteDialogTask);
  };

  const client = useApolloClient();
  const [order, setorder] = useState('asc');

  const handleFiltre = e => {
    setorder(order === 'asc' ? 'desc' : 'asc');
    if (setTriage) {
      switch (e) {
        case 1:
          setTriage([{ dateCreation: { order: order === 'asc' ? 'desc' : 'asc' } }]);
          break;
        case 2:
          setTriage([{ dateDebut: { order: order === 'asc' ? 'desc' : 'asc' } }]);
          break;
        case 3:
          setTriage([{ priority: { order: order === 'asc' ? 'desc' : 'asc' } }]);
          break;
        case 4:
          setTriage([{ description: { order: order === 'asc' ? 'desc' : 'asc' } }]);
          break;
        case 5:
          setTriage([{ isAssigned: { order: order === 'asc' ? 'desc' : 'asc' } }]);
          break;
        default:
          setTriage([{ dateCreation: { order: order === 'asc' ? 'desc' : 'asc' } }]);
          break;
      }
    }
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleOpenModalTacheAction = () => {
    setOpenModalTache(!openModalTache);
  };

  const handleClickDeleteProject = (event: React.MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    setOpenDeleteDialog(!openDeleteDialog);
  };

  const handleclickOpenEditProject = (event: React.MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    setOpenProject(!openProject);
  };
  const handleClickaddSection = (event: React.MouseEvent<HTMLElement>) => {
    setOpenAddSection(!openAddSection);
    handleClick(event);
  };
  const handleclickEditEtiquette = (event: React.MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    setOpenEditEtiquette(!openEditEtiquette);
  };

  const handleOpenAddSection = (event: React.MouseEvent<HTMLElement>) => {
    setOpenAddSection(!openAddSection);
    handleClick(event);
  };
  const handleclickDeleteEtiquette = (event: React.MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    setOpenDeleteDialog(!openDeleteDialog);
  };

  const handleFavorite = (event: React.MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    setOpenAddToFavorite(!openAddToFavorite);
  };

  const handleArchived = (event: React.MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    setOpenArchived(!openArchived);
  };

  const handleAddTop = (event: React.MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    setOpenEditEtiquette(!openEditEtiquette);
  };

  const currentUser: ME_me = getUser();
  const currentPharmacie = getPharmacie();
  const myId = (currentUser && currentUser.id) || '';

  // MUTATIONS

  const [switchEtiquette] = useMutation<
    CREATE_UPDATE_TODO_ETIQUETTE,
    CREATE_UPDATE_TODO_ETIQUETTEVariables
  >(DO_CREATE_UPDATE_ETIQUETTE, {
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
    onCompleted: data => {
      if (data && data.createUpdateTodoEtiquette) {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Opération réussie',
        });
      }
      setOpenAddToFavorite(false);
    },
  });
  const [switchProject] = useMutation<CREATE_PROJECT, CREATE_PROJECTVariables>(DO_CREATE_PROJECT, {
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
    onCompleted: data => {
      if (data && data.createUpdateProject) {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Opération réussie',
        });
      }
      setOpenAddToFavorite(false);
      setOpenArchived(false);
    },
  });

  const projectInputVariables = {
    id: row && row.id,
    name: row && row._name,
    typeProject: row && row.typeProject,
    idUser: currentUser && currentUser.id,
    idPharmacie: currentPharmacie && currentPharmacie.id,
  };

  const switchFavs = row => {
    if (row && row.typeProject) {
      switchProject({
        variables: {
          input: {
            ...projectInputVariables,
            isFavoris: !(row && row.isInFavoris),
          },
        },
      });
    } else {
      switchEtiquette({
        variables: {
          input: {
            id: row && row.id,
            nom: row && row._name,
            isFavoris: !(row && row.isInFavoris),
          },
        },
      });
    }
  };

  const switchArchived = row => {
    switchProject({
      variables: {
        input: {
          ...projectInputVariables,
          isArchived: !(row && row.isArchived),
        },
      },
    });
  };

  const [
    removeEtiquette,
    { loading: deleteEtiquetteLoading, error: deleteEtiquetteError },
  ] = useMutation<DELETE_ETIQUETTE, DELETE_ETIQUETTEVariables>(DO_DELETE_ETIQUETTE, {
    update: (cache, { data }) => {
      if (data && data.softDeleteTodoEtiquettes) {
        if (queryVariable && queryVariable.etiquetteVariable) {
          const req: any = cache.readQuery({
            query: GET_SEARCH_ETIQUETTE,
            variables: queryVariable && queryVariable.etiquetteVariable,
          });
          if (req && req.search && req.search.data) {
            const source = req.search.data;
            const result = data.softDeleteTodoEtiquettes;
            const dif = differenceBy(source, result, 'id');
            cache.writeQuery({
              query: GET_SEARCH_ETIQUETTE,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: dif,
                  },
                },
              },
              variables: queryVariable && queryVariable.etiquetteVariable,
            });
          }
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
    onCompleted: data => {
      if (data && data.softDeleteTodoEtiquettes) {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Suppression réussie',
        });
      }
      setOpenDeleteDialog(false);
    },
  });

  const [removeProject, { loading: deleteProjectLoading, error: deleteProjectError }] = useMutation<
    DELETE_PROJECTS,
    DELETE_PROJECTSVariables
  >(DO_DELETE_PROJECT, {
    update: (cache, { data }) => {
      if (data && data.softDeleteProjects) {
        if (queryVariable && queryVariable.projectVariable) {
          const req: any = cache.readQuery({
            query: GET_SEARCH_CUSTOM_CONTENT_PROJECT,
            variables: queryVariable && queryVariable.projectVariable,
          });
          if (req && req.search && req.search.data) {
            const source = req.search.data;
            const result = data.softDeleteProjects;
            const dif = differenceBy(source, result, 'id');
            cache.writeQuery({
              query: GET_SEARCH_CUSTOM_CONTENT_PROJECT,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: dif,
                  },
                },
              },
              variables: queryVariable && queryVariable.projectVariable,
            });
          }
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
    onCompleted: data => {
      if (data && data.softDeleteProjects) {
        if (refetchCountTodos) refetchCountTodos();
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Suppression réussie',
        });
      }
      setOpenDeleteDialog(false);
    },
  });

  const handleRemove = row => {
    if (row && row.type === 'project') {
      removeProject({
        variables: {
          ids: [row && row.id],
        },
      });
    } else {
      removeEtiquette({
        variables: {
          ids: [row && row.id],
        },
      });
    }
  };

  const [removeTache, { loading: loadingDelete }] = useMutation<
    DELETE_ACTIONS,
    DELETE_ACTIONSVariables
  >(DO_DELETE_ACTIONS, {
    update: (cache, { data }) => {
      if (data && data.softDeleteActions) {
        if (variableTache) {
          const req: any = cache.readQuery({
            query: GET_SEARCH_ACTION,
            variables: variableTache,
          });
          if (req && req.search && req.search.data) {
            const result = data.softDeleteActions;
            cache.writeQuery({
              query: GET_SEARCH_ACTION,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: result,
                  },
                },
              },
              variables: variableTache,
            });
          }
        }
      }
    },
    onCompleted: data => {
      displaySnackBar(client, {
        isOpen: true,
        type: 'SUCCESS',
        message: 'Suppression effectuée avec succès',
      });
      if (refetch) refetch();
      if (refetchCountTodos) refetchCountTodos();
      // setdeletedId(null);
    },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const onClickConfirmDeleteTask = () => {
    removeTache({
      variables: {
        ids: task && task.id,
      },
    });
    setOpenDeleteDialogTask(!openDeleteDialogTask);
  };

  const handleClickShareProject = () => {
    handleClose();
    setOpenShare(true);
  };

  const handleClickShareEtiquette = () => {
    handleClose();
    setOpenShareEtiquette(true);
  };

  const handleOpenAjoutSectionDialog = () => {
    setOpenAddSection(true);
  };

  // FAVORIS: project + etiquette
  const favorisMenuItems: ActionButtonMenu[] = [
    { label: 'Modifier', icon: <Edit />, onClick: handleClick, disabled: false },
    {
      label: 'Retirer des favoris',
      icon: <img src={RetirerFavoris} />,
      onClick: handleFavorite,
      disabled: false,
    },
  ];

  // PROJECT
  const projectMenuItems: ActionButtonMenu[] = [
    {
      label: 'Créer un sous-projet',
      icon: <Add />,
      onClick: handleClick,
      disabled: false,
      hide:
        row && row.projetParent
          ? true
          : parameters &&
            parameters.length &&
            parameters.filter(parameter => parameter.code === '0120' && parameter.value === 'true')
              .length
            ? false
            : true,
    },
    {
      label: 'Ajouter un projet au-dessus',
      icon: <img src={AjouterDessus} />,
      onClick: handleClick,
      disabled: false,
      hide:
        row && row.projetParent
          ? true
          : parameters &&
            parameters.length &&
            parameters.filter(parameter => parameter.code === '0120' && parameter.value === 'true')
              .length
            ? false
            : true,
    },
    {
      label: 'Ajouter un projet au-dessous',
      icon: <img src={AjouterDessous} />,
      onClick: handleClick,
      disabled: false,
      hide:
        row && row.projetParent
          ? true
          : parameters &&
            parameters.length &&
            parameters.filter(parameter => parameter.code === '0120' && parameter.value === 'true')
              .length
            ? false
            : true,
    },
    {
      label: 'Modifier',
      icon: <Edit />,
      onClick: handleclickOpenEditProject,
      disabled: false,
    },
    /*{
      label: 'Partager',
      icon: <PersonAdd />,
      onClick: handleClickShareProject,
      disabled: false,
    },*/
    {
      label: 'Ajouter au favoris',
      icon: <Favorite />,
      onClick: handleFavorite,
      disabled: row && row.isInFavoris === true,
    },
    {
      label: 'Archiver',
      icon: <img src={Archiver} />,
      onClick: handleArchived,
      disabled:
        (row && row.isArchived === true) ||
        (parameters &&
          parameters.length &&
          parameters.filter(parameter => parameter.code === '0121' && parameter.value === 'true')
            .length
          ? false
          : true),
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: handleClickDeleteProject,
      disabled: false,
    },
  ];

  // PROJET ARCHIVEE
  const archivedMenuItems: ActionButtonMenu[] = [
    {
      label: 'Désarchiver',
      icon: <img src={Archiver} />,
      onClick: handleArchived,
      disabled:
        parameters &&
          parameters.length &&
          parameters.filter(parameter => parameter.code === '0121' && parameter.value === 'true')
            .length
          ? false
          : true,
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: handleClickDeleteProject,
      disabled: false,
    },
  ];

  // ETIQUETTE
  const etiquetteMenuItems: ActionButtonMenu[] = [
    {
      label: 'Ajouter une étiquette au-dessus',
      icon: <img src={AjouterDessus} />,
      onClick: handleClick,
      disabled: false,
    },
    {
      label: 'Ajouter une étiquette au-dessous',
      icon: <img src={AjouterDessous} />,
      onClick: handleClick,
      disabled: false,
    },
    {
      label: 'Modifier',
      icon: <Edit />,
      onClick: handleclickEditEtiquette,
      disabled: false,
    },
    {
      label: 'Ajouter au favoris',
      icon: <Favorite />,
      onClick: handleFavorite,
      disabled: row && row.isInFavoris === true,
    },
    {
      label: 'Deplacer vers étiquettes partagées',
      icon: <img src={EtiquettePartagee} />,
      onClick: handleClickShareEtiquette,
      disabled: false,
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: handleclickDeleteEtiquette,
      disabled: false,
    },
  ];

  // ETIQUETTE PARTAGEE
  const sharedEtiquetteMenuItems: ActionButtonMenu[] = [
    { label: 'Modifier', icon: <Edit />, onClick: handleClick, disabled: false },
    {
      label: 'Ajouter au favoris',
      icon: <Favorite />,
      onClick: handleClick,
      disabled: false,
    },
    {
      label: 'Retirer des étiquettes partagées',
      icon: <img src={EtiquettePartagee} />,
      onClick: handleClick,
      disabled: false,
    },
    { label: 'Supprimer', icon: <Delete />, onClick: handleClick, disabled: false },
  ];

  let defaultContent: ActionButtonMenu[] = [
    {
      label: 'Modifier',
      icon: <Edit />,
      onClick: () => setOpenEditEtiquette,
      disabled: false,
    },
    {
      label: 'Retirer des favoris',
      icon: <img src={RetirerFavoris} />,
      onClick: handleClick,
      disabled: false,
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: handlesetOpenDialogDelete,
      disabled: false,
    },
  ];

  // MENUTASKLIST
  const MenuTaskList: ActionButtonMenu[] = [
    /*{
      label: 'Créer une sous-tâche',
      icon: <AddIcon />,
      onClick: () => handleClick,
      disabled: false,
    },*/
    {
      label: 'Ajouter une tâche au-dessus',
      icon: <PlaylistAddIcon />,
      onClick: () => handleClick,
      disabled: false,
    },
    {
      label: 'Ajouter une tâche en-dessous',
      icon: <PlaylistAddIcon />,
      onClick: () => handleClick,
      disabled: false,
    },
    {
      label: 'Modifier',
      icon: <Edit />,
      onClick: () => handleOpenModalTacheAction(),
      disabled: false,
    },
    {
      label: 'Déplacer',
      icon: <ExitToAppIcon />,
      onClick: handleClick,
      disabled: false,
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: handlesetOpenDialogDelete,
      disabled: false,
    },
  ];

  const isOnAllSee = filter === 'tout-voir';
  const isOnProject = filter === 'projet';
  const isInbox = pathname.includes('recu') && !pathname.includes('recu-equipe');
  const isInboxTeam = pathname.includes('recu-equipe');

  const hide =
    parameters &&
      parameters.length &&
      parameters.filter(parameter => parameter.code === '0120' && parameter.value === 'true').length
      ? isOnProject && row && row.user && row.user.id === myId
        ? false
        : isOnAllSee || isInbox || isInboxTeam
          ? false
          : true
      : true;

  // MainHeader statutMainHeader
  const headerMenuItems: ActionButtonMenu[] = statutMainHeader
    ? [
      {
        label: cacheTacheAchevE
          ? 'Affichez tous les taches achevées'
          : 'Masquez tous les taches achevées',
        icon: cacheTacheAchevE ? <VisibilityIcon /> : <VisibilityOffIcon />,
        onClick: handleTacheAchevE,
        disabled: false,
      },
      { label: 'Modifier', icon: <Edit />, onClick: handleclickOpenEditProject, disabled: false },
      {
        label: 'Ajouter une section',
        icon: <ViewDayIcon />,
        onClick: handleOpenAjoutSectionDialog,
        disabled: false,
        hide,
      },
      {
        label: 'Ajouter au favoris',
        icon: <Favorite />,
        onClick: handleFavorite,
        disabled: row && row.isInFavoris === true,
      },
      {
        label: 'Trier par date de création',
        icon: <CalendarTodayIcon />,
        onClick: () => handleFiltre(1),
        disabled: false,
      },
      {
        label: "Trier par date d'echéance",
        icon: <EventIcon />,
        onClick: () => handleFiltre(2),
        disabled: false,
      },
      {
        label: 'Trier par priorité',
        icon: <FlagIcon />,
        onClick: () => handleFiltre(3),
        disabled: false,
      },
      {
        label: 'Trier par nom',
        icon: <SortByAlphaIcon />,
        onClick: () => handleFiltre(4),
        disabled: false,
      },
      {
        label: 'Trier par responsable',
        icon: <Person />,
        onClick: () => handleFiltre(5),
        disabled: false,
      },
      {
        label: 'Archiver',
        icon: <ArchiveIcon />,
        onClick: handleArchived,
        disabled:
          (row && row.isArchived === true) ||
            (parameters &&
              parameters.length &&
              parameters.filter(
                parameter => parameter.code === '0121' && parameter.value === 'true',
              ).length)
            ? false
            : true,
      },
    ]
    : [
      {
        label: 'Ajouter une section',
        icon: <ViewDayIcon />,
        onClick: handleOpenAjoutSectionDialog,
        disabled: false,
        hide,
      },
      {
        label: 'Trier par date de création',
        icon: <CalendarTodayIcon />,
        onClick: () => handleFiltre(1),
        disabled: false,
      },
      {
        label: "Trier par date d'echéance",
        icon: <EventIcon />,
        onClick: () => handleFiltre(2),
        disabled: false,
      },
      {
        label: 'Trier par priorité',
        icon: <FlagIcon />,
        onClick: () => handleFiltre(3),
        disabled: false,
      },
      {
        label: 'Trier par nom',
        icon: <SortByAlphaIcon />,
        onClick: () => handleFiltre(4),
        disabled: false,
      },
      {
        label: 'Trier par responsable',
        icon: <Person />,
        onClick: () => handleFiltre(5),
        disabled: false,
      },
    ];

  switch (actionMenu) {
    case 'FAVORITE':
      defaultContent = favorisMenuItems;
      break;
    case 'PROJECT':
      defaultContent = projectMenuItems;
      break;
    case 'ARCHIVED':
      defaultContent = archivedMenuItems;
      break;
    case 'ETIQUETTE':
      defaultContent = etiquetteMenuItems;
      break;
    case 'SHARED_ETIQUETTE':
      defaultContent = sharedEtiquetteMenuItems;
      break;
    case 'MENU_TASK':
      defaultContent = MenuTaskList;
      break;
    case 'HEADER_MENU':
      defaultContent = headerMenuItems; // A modifier
      break;
    default:
      defaultContent = defaultContent;
      break;
  }

  const DeleteDialogContent = () => {
    return <span>Êtes-vous sur de vouloir supprimer ?</span>;
  };

  if (actionMenu === 'PROJECT') console.log('*********', defaultContent);

  return (
    <>
      {loadingDelete && <Backdrop value="Suppression en cours, veuillez patienter..." />}
      <ConfirmDeleteDialog
        open={openDeleteDialogTask}
        setOpen={handlesetOpenDialogDelete}
        onClickConfirm={onClickConfirmDeleteTask}
      />

      <AjoutSectionModal
        openModal={openAddSection}
        setOpen={handleOpenAddSection}
        handleChange={handleSectionChange}
        values={sectionValues}
        handleSubmit={doCreateUpdateSection}
      />
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        // tslint:disable-next-line: jsx-no-lambda
        onClickConfirm={() => handleRemove(row)}
      />
      <ConfirmDialog
        open={openAddToFavorite}
        message={
          row && row.isInFavoris
            ? 'Êtes-vous sur de vouloir retirer des favoris ?'
            : 'Êtes-vous sur de vouloir ajouter au favoris ?'
        }
        handleValidate={() => switchFavs(row)}
        handleClose={handleFavorite}
      />
      <ConfirmDialog
        open={openArchived}
        message={
          row && row.isArchived
            ? 'Êtes-vous sur de vouloir désarchiver ?'
            : 'Êtes-vous sur de vouloir archiver ?'
        }
        handleValidate={() => switchArchived(row)}
        handleClose={handleArchived}
      />
      <AjoutTaches
        openModalTache={openModalTache}
        setOpenTache={setOpenModalTache}
        task={task}
        refetchCountTodos={refetchCountTodos}
        refetchTasks={refetch}
        refetchAll={refetchAll}
      />
      <AjoutProjet open={openProject} setOpen={setOpenProject} projectToEdit={row} />
      <AjoutEtiquette open={openEditEtiquette} handleOpen={setOpenEditEtiquette} etiquette={row} />
      <IconButton
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
        color="inherit"
      >
        <MoreHoriz />
      </IconButton>
      <Menu
        // tslint:disable-next-line: jsx-no-lambda
        onClick={event => {
          event.preventDefault();
          event.stopPropagation();
        }}
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        {defaultContent &&
          defaultContent.length > 0 &&
          defaultContent.map(
            (i: ActionButtonMenu, index: number) =>
              !i.hide && (
                <MenuItem
                  // tslint:disable-next-line: jsx-no-lambda
                  onClick={event => {
                    event.preventDefault();
                    event.stopPropagation();
                    handleClose();
                    i.onClick(event);
                  }}
                  key={`table_menu_item_${index}`}
                  disabled={i.disabled}
                >
                  <ListItemIcon>{i.icon}</ListItemIcon>
                  <Typography variant="inherit">{i.label}</Typography>
                </MenuItem>
              ),
          )}
      </Menu>

      <AjoutParticipant
        openModalParticipant={openShare}
        setOpenParticipant={setOpenShare}
        userIds={selectedUsersIds}
        setUserIds={setselectedUsersIds}
        idProjectTask={(row && row.id) || ''}
      />

      <AjoutParticipant
        openModalParticipant={openShareEtiquette}
        setOpenParticipant={setOpenShareEtiquette}
        idEtiquette={(row && row.id) || ''}
        userIds={selectedUsersIds}
        setUserIds={setselectedUsersIds}
      />
      <AjoutProjet
        open={openProject}
        setOpen={setOpenProject}
      // projectToEdit={projectToEdit}
      // idParentProject={parentProject && parentProject.id}
      />
    </>
  );
};

export default withRouter(Actionbutton);
