import { todayFilter } from './terms/filter'

export const getQueryParameter = (isAssignedTerms: string) => {
  return {
    query: {
      bool: {

        must: [
          { term: { isRemoved: false } },
          { term: { status: 'ACTIVE' } },
          { range: { dateDebut: { lte: 'now/d' } } },
          { exists: { field: 'dateDebut' } },
        ],
        should: todayFilter({ myId: isAssignedTerms, useMatriceResponsabilite: true }) /* [
          {
            bool: {
              must_not: [
                {
                  exists: {
                    field: 'assignedUsers',
                  },
                },
              ],
              must: [
                {
                  term: { 'userCreation.id': isAssignedTerms },
                },
                {
                  term: {
                    isInInboxTeam: false,
                  },
                },
              ],
            },
          },
          { term: { 'assignedUsers.id': isAssignedTerms } },
        ]*/,
        minimum_should_match: 1,
      },
    },
  };
};
