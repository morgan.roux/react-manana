import moment from 'moment';
import { UrgenceInterface } from './Common/UrgenceFilter/UrgenceFilter';
import { ImportanceInterface } from './Common/ImportanceFilter/ImportanceFilterList';
import { todayFilter, todayTeamFilter } from './terms/filter';
import { extractProjectIds, extractPersoProject } from './util';
export const buildTermActions = ({
  filters,
  useActionListFilters,
  actionListFilters,
  isOnNextWeekTeam,
  isProject,
  filterId,
  isEtiquette,
  isTodayOnly,
  isTodayTeam,
  isInboxTeam,
  isOnSevenDays,
  isOnSeptJoursEquipe,
  isInbox,
  withoutDate,
  groupement,
  myId,
  isAllSee,
  me,
  useMatriceResponsabilite,
  urgence,
  importances,
  selectedProject,
}) => {
  moment.locale('fr');

  const DEFAULT_MINIMUM_SHOULD_MATCH: number = 1;

  /* BEGIN TERM MUST FOR SEARCH */
  const termNextSevenDays =
    useActionListFilters && actionListFilters
      ? [actionListFilters]
      : isOnNextWeekTeam && actionListFilters
        ? [actionListFilters]
        : [];

  const termActive =
    filters?.taskStatus === 'DONE'
      ? [{ term: { status: 'DONE' } }]
      : [{ term: { status: 'ACTIVE' } }];

  const isCoupleUrgence = (urgences: UrgenceInterface[], code1: string, code2: string): boolean => {
    if (urgences.length === 2) {
      return (
        urgences.some(({ code }) => code === code1) && urgences.some(({ code }) => code === code2)
      );
    }

    return false;
  };
  const allUrgences = urgence || [];
  const activeUrgences: UrgenceInterface[] = allUrgences.filter(item => item.checked);

  let urgenceFilter: any = [];

  if (activeUrgences.length !== 0 && allUrgences.length !== activeUrgences.length) {
    if (activeUrgences.length === 1) {
      const activeUrgence = activeUrgences[0];
      if (activeUrgence.code === 'B') {
        urgenceFilter = [
          { range: { dateDebut: { gte: 'now+1d/d' } } },
          { range: { dateDebut: { lt: 'now+7d/d' } } },
        ];
      } else if (activeUrgence.code === 'C') {
        urgenceFilter = [{ range: { dateDebut: { gte: 'now+13d/d' } } }];
      } else if (activeUrgence.code === 'A') {
        urgenceFilter = [
          { range: { dateDebut: { lte: 'now/d' } } },
          // { range: { dateDebut: { lt: 'now+1d/d' } } },
        ];
      }
    } // End one filter
    else {
      // 2 filter
      if (isCoupleUrgence(activeUrgences, 'A', 'B')) {
        // Journée-semaine
        urgenceFilter = [
          // { range: { dateDebut: { gte: 'now/d' } } },
          { range: { dateDebut: { lte: 'now+7d/d' } } },
        ];
      } else if (isCoupleUrgence(activeUrgences, 'A', 'C')) {
        // Journée-mois
        urgenceFilter = [
          { range: { dateDebut: { gte: 'now/d' } } }, // TODO : Filter mois
        ];
      } else if (isCoupleUrgence(activeUrgences, 'B', 'C')) {
        // semaine-mois
        urgenceFilter = [{ range: { dateDebut: { gte: 'now+1d/d' } } }];
      }
    }
  }

  const allImportances = importances || [];
  const activeImportances: ImportanceInterface[] = allImportances.filter(item => item.checked);

  let importanceFilter: any = [];
  if (activeImportances.length !== 0 && allImportances.length !== activeImportances.length) {
    //  importanceFilter = activeImportances.map(({ id }) => ({ term: { 'etiquettes.id': id } }))
    importanceFilter =
      activeImportances.length === 1
        ? [{ term: { 'importance.id': activeImportances[0].id } }]
        : [{ terms: { 'importance.id': activeImportances.map(({ id }) => id) } }];
  }

  /// const typeProject = filters && filters.typeProject && filters.typeProject[0];

  const defaultMust: any[] = [
    { term: { isRemoved: false } },
    ...termActive,
    //{ terms: { priority: taskPriorityFilters } },
    ...termNextSevenDays,
  ];

  const persoProject = extractPersoProject(selectedProject, me);
  const selectedProjectIsPersonnel = !!persoProject;
  const selectedProjectIds = selectedProject ? extractProjectIds(selectedProject, me) : [];

  const computeProjectFilter = () => {
    return selectedProjectIds.length === 1
      ? { term: { 'project.id': selectedProjectIds[0] } }
      : { terms: { 'project.id': selectedProjectIds } };
  };

  const mustProject: any[] = isProject
    ? [
      ...defaultMust,
      ...(selectedProjectIsPersonnel ? [] : [computeProjectFilter()]),
      ...importanceFilter,
      ...urgenceFilter,
    ]
    : [];

  const mustEtiquette: any[] = isEtiquette
    ? [
      ...defaultMust,
      { term: { 'etiquettes.id': filterId } },
      { term: { 'project.participants.id': myId } }, // useMatriceResponsabilite
      ...urgenceFilter,
    ]
    : [];

  const defaultMusToday = [
    ...defaultMust,
    { range: { dateDebut: { lte: 'now/d' } } },
    { exists: { field: 'dateDebut' } },
  ];

  const must = isProject
    ? [...mustProject]
    : isEtiquette
      ? [...mustEtiquette]
      : isTodayOnly || isTodayTeam
        ? [...defaultMusToday]
        : isOnSevenDays
          ? [
            //{ range: { dateDebut: { gt: 'now/d' } } },
            //{ range: { dateDebut: { lte: sevenDays } } },
            ...defaultMust,
          ]
          : isOnSeptJoursEquipe
            ? [
              { range: { dateDebut: { gt: 'now/d' } } },
              //   { range: { dateDebut: { lte: sevenDays } } },
              ...termNextSevenDays,
              ...defaultMust,
            ]
            : [...defaultMust, ...urgenceFilter];

  /* END TERM MUST FOR SEARCH */

  /* BEGIN TERM MUST NOT FOR SEARCH */
  const mustNotDate = [{ exists: { field: 'dateDebut' } }];

  const mustNot = withoutDate ? [...mustNotDate] : [];
  /* isTodayTeam || isOnSeptJoursEquipe || isInboxTeam
      ? [
          { term: { 'assignedUsers.id': myId } },
          {
            bool: {
              must_not: [
                {
                  exists: {
                    field: 'assignedUsers',
                  },
                },
              ],
              must: {
                term: { 'userCreation.id': myId },
              },
            },
          },
        ]
      :  */
  /* END TERM MUST NOT FOR SEARCH */

  /* BEGIN TERM SHOULD FOR SEARCH */
  const defaultShould: any[] = [
    [
      { term: { 'assignedUsers.idGroupement': groupement && groupement.id } },
      { term: { 'userCreation.idGroupement': groupement && groupement.id } },
    ],
  ];

  let idPharmacie = '';
  let idService = '';
  if (me.userPersonnel)
    idService = (me.userPersonnel && me.userPersonnel.service && me.userPersonnel.service.id) || '';
  else if (me.userTitulaire || me.userPpersonnel)
    idPharmacie = (me.pharmacie && me.pharmacie.id) || '';

  const defaultShouldAssignTeam = me.userPersonnel
    ? [
      {
        term: {
          'assignedUsers.userPersonnel.service.id': idService,
        },
      },
    ]
    : me.userTitulaire || me.userPpersonnel
      ? [
        {
          term: {
            'assignedUsers.pharmacie.id': idPharmacie,
          },
        },
      ]
      : [...defaultShould];

  const defaultShouldCreatedTeam = me.userPersonnel
    ? [
      {
        term: {
          'userCreation.userPersonnel.service.id': idService,
        },
      },
    ]
    : me.userTitulaire || me.userPpersonnel
      ? [
        {
          term: {
            'userCreation.pharmacie.id': idPharmacie,
          },
        },
      ]
      : [];

  const defaultShouldOtherTeam = todayTeamFilter({
    myId,
    useMatriceResponsabilite,
    defaultShouldAssignTeam,
    defaultShouldCreatedTeam,
  });

  const should = selectedProjectIsPersonnel
    ? [
      {
        bool: {
          must: [
            {
              exists: {
                field: 'project',
              },
            },
            { term: { 'project.id': persoProject.id } },
          ],
        },
      },
      {
        bool: {
          must_not: [
            {
              exists: {
                field: 'project',
              },
            },
          ],
          must: [
            {
              term: { 'assignedUsers.id': myId },
            },
          ],
        },
      },

      {
        bool: {
          must_not: [
            {
              exists: {
                field: 'assignedUsers',
              },
            },
            {
              exists: {
                field: 'project',
              },
            },
          ],
          must: [
            {
              term: { 'userCreation.id': myId },
            },
          ],
        },
      },
    ]
    : isInboxTeam
      ? [
        {
          bool: {
            must_not: [
              {
                exists: {
                  field: 'assignedUsers',
                },
              },
              {
                exists: {
                  field: 'project',
                },
              },
            ],
            must: [
              {
                term: { 'userCreation.id': myId },
              },
              {
                term: {
                  isInInboxTeam: true,
                },
              },
            ],
          },
        },
        {
          bool: {
            must_not: [
              {
                exists: {
                  field: 'assignedUsers',
                },
              },
              {
                exists: {
                  field: 'project',
                },
              },
              {
                term: { 'userCreation.id': myId },
              },
            ],
            must: [...defaultShouldCreatedTeam],
          },
        },
        {
          bool: {
            must_not: [
              {
                exists: {
                  field: 'assignedUsers',
                },
              },
            ],
            must: [
              {
                term: { 'userCreation.id': myId },
              },
              {
                exists: {
                  field: 'project',
                },
              },
              {
                exists: {
                  field: 'project.participants',
                },
              },
            ],
          },
        },
        {
          bool: {
            must_not: [
              {
                exists: {
                  field: 'assignedUsers',
                },
              },
            ],
            must: [
              {
                term: { 'project.participants.id': myId },
              },
              {
                exists: {
                  field: 'project',
                },
              },
              {
                exists: {
                  field: 'project.participants',
                },
              },
            ],
          },
        },
      ]
      : isTodayTeam || isOnSeptJoursEquipe
        ? [...defaultShouldOtherTeam]
        : isInbox
          ? [
            {
              bool: {
                must_not: [
                  {
                    exists: {
                      field: 'assignedUsers',
                    },
                  },
                  {
                    exists: {
                      field: 'project',
                    },
                  },
                ],
                must: [
                  {
                    term: { 'userCreation.id': myId },
                  },
                  {
                    term: {
                      isInInboxTeam: false,
                    },
                  },
                ],
              },
            },
            {
              bool: {
                must_not: [
                  {
                    exists: {
                      field: 'assignedUsers',
                    },
                  },
                  {
                    exists: {
                      field: 'project',
                    },
                  },
                ],
                must: [
                  {
                    term: { 'userCreation.id': myId },
                  },
                  {
                    term: {
                      isInInbox: true,
                    },
                  },
                  {
                    term: {
                      isInInboxTeam: false,
                    },
                  },
                ],
              },
            },
          ]
          : isTodayOnly || isOnSevenDays || withoutDate
            ? todayFilter({ myId, useMatriceResponsabilite })
            : isEtiquette || isProject
              ? [
                {
                  bool: {
                    must_not: [
                      {
                        exists: {
                          field: 'assignedUsers',
                        },
                      },
                    ],
                    must: {
                      term: { 'userCreation.id': myId },
                    },
                  },
                },
                { term: { 'assignedUsers.id': myId } },
                {
                  bool: {
                    must: [
                      {
                        exists: {
                          field: 'project.participants.id',
                        },
                      },
                      ...defaultShouldCreatedTeam,
                    ],
                  },
                },
              ]
              : isAllSee
                ? [
                  ...todayTeamFilter({ myId, useMatriceResponsabilite, defaultShouldCreatedTeam, defaultShouldAssignTeam }),
                  ...todayFilter({ myId, useMatriceResponsabilite })
                ]

                /*[
                { term: { 'assignedUsers.id': myId } },
                {
                  bool: {
                    must_not: [
                      {
                        exists: {
                          field: 'assignedUsers',
                        },
                      },
                    ],
                    must: {
                      term: { 'userCreation.id': myId },
                    },
                  },
                },
                {
                  bool: {
                    must: [
                      {
                        term: {
                          'userCreation.id': myId,
                        },
                      },
                    ],
                  },
                },
                ] */
                : [...defaultShould];
  /* END TERM SHOULD FOR SEARCH */

  return {
    query: {
      bool: {
        must,
        must_not: mustNot,
        should,
        minimum_should_match: DEFAULT_MINIMUM_SHOULD_MATCH,
      },
    },
  };
};

/* let pharmacie = '';
  let service = '';
  if (me.userPersonnel)
    service = (me.userPersonnel && me.userPersonnel.service && me.userPersonnel.service.id) || '';
  else if (me.userTitulaire || me.userPpersonnel)
    pharmacie = (me.pharmacie && me.pharmacie.id) || '';

  const defaultShouldOtherTeam = me.userPersonnel
    ? [
        {
          term: {
            'assignedUsers.userPersonnel.service.id': service,
          },
        },
        {
          term: {
            'userCreation.userPersonnel.service.id': service,
          },
        },
      ]
    : me.userTitulaire || me.userPpersonnel
    ? [
        {
          term: {
            'assignedUsers.pharmacie.id': pharmacie,
          },
        },
        {
          term: {
            'userCreation.pharmacie.id': pharmacie,
          },
        },
      ]
    : [...defaultShould];

    ==> [
    {
      term: {
        'project.participants.id': myId,
      },
    },
    {
      bool: {
        must_not: [
          {
            exists: {
              field: 'project',
            },
          },
        ],
        must: {
          term: { 'assignedUsers.id': myId },
        },
      },
    },
    {
      term: {
        isInInboxTeam: true,
      },
    },
  ];

    */
