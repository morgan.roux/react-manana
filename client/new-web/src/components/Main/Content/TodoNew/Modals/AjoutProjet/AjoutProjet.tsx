import { useApolloClient, useMutation, useQuery } from '@apollo/client';
import { Typography } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Switch from '@material-ui/core/Switch';
import { FormatShapesTwoTone } from '@material-ui/icons';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { ME_me } from '../../../../../../graphql/Authentication/types/ME';
import { DO_SEARCH_COULEURS, GET_COULEURS } from '../../../../../../graphql/Couleurs/query';
import { couleurs } from '../../../../../../graphql/Couleurs/types/couleurs';
import { GET_PARAMETERS_GROUPES_CATEGORIES } from '../../../../../../graphql/Parametre/query';
import {
  PARAMETERS_GROUPES_CATEGORIES,
  PARAMETERS_GROUPES_CATEGORIESVariables,
} from '../../../../../../graphql/Parametre/types/PARAMETERS_GROUPES_CATEGORIES';
import { DO_CREATE_PROJECT } from '../../../../../../graphql/Project/mutation';
import {
  CREATE_PROJECT,
  CREATE_PROJECTVariables,
} from '../../../../../../graphql/Project/types/CREATE_PROJECT';
import { getPharmacie, getUser } from '../../../../../../services/LocalStorage';
import { ParamCategory, TypeProject } from '../../../../../../types/graphql-global-types';
import { capitalizeFirstLetter } from '../../../../../../utils/capitalizeFirstLetter';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import Backdrop from '../../../../../Common/Backdrop';
import { CustomModal } from '../../../../../Common/CustomModal';
import CustomSelect from '../../../../../Common/CustomSelect';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import { hiddenTypeProject } from '../../Filters/filter';
import { CountTodosProps } from '../../TodoNew';
import CustomSelectColor from '../AjoutEtiquette/utils/CustomSelectColor';
import { useStyles } from './styles';

interface UsersModalProps {
  open: boolean;
  setOpen: (value: boolean) => any;
  queryVariables?: any;
  projectToEdit?: any;
  parentProject?: any;
  setParentProject?: (value: any) => void;
  setNextProject?: (value: any) => void;
  setPrevProject?: (value: any) => void;
  setProjectToEdit?: (value: any) => void;
  refetch?: any;
  prevProject?: any;
  nextProject?: any;
  termologie?: string;
}

interface FormStateInterface {
  name: string;
  typeProject: TypeProject;
  idCouleur: string;
  isFavoris: boolean;
  idUser: string;
  idPharmacie: string;
  ordre?: number;
}

const AjoutProjet: FC<UsersModalProps & RouteComponentProps & CountTodosProps> = props => {
  const {
    open,
    setOpen,
    queryVariables,
    projectToEdit,
    parentProject,
    setParentProject,
    setNextProject,
    setPrevProject,
    setProjectToEdit,
    refetch,
    refetchCountTodos,
    prevProject,
    nextProject,
    termologie,
  } = props;
  const isEdit: boolean = projectToEdit?.id ?? false;
  const isSubProject: boolean = parentProject ? true : false;
  const classes = useStyles({});
  const client = useApolloClient();
  const currentUser: ME_me = getUser();
  const currentPharmacie = getPharmacie();

  const successMessage: string = isEdit
    ? 'Projet modifié avec succès'
    : `Projet ajouté avec succès`;

  const { data: dataParameters } = useQuery<
    PARAMETERS_GROUPES_CATEGORIES,
    PARAMETERS_GROUPES_CATEGORIESVariables
  >(GET_PARAMETERS_GROUPES_CATEGORIES, {
    variables: {
      categories: [ParamCategory.GROUPEMENT, ParamCategory.USER],
      groupes: ['TODO'],
    },
  });

  const typeGroupement = hiddenTypeProject(
    'GROUPEMENT',
    dataParameters &&
      dataParameters.parametersGroupesCategories &&
      dataParameters.parametersGroupesCategories.length
      ? dataParameters.parametersGroupesCategories
      : ([] as any),
  )
    ? []
    : [{ id: TypeProject.GROUPEMENT, value: 'Groupement' }];

  const typePersonnel = hiddenTypeProject(
    'PERSONNEL',
    dataParameters &&
      dataParameters.parametersGroupesCategories &&
      dataParameters.parametersGroupesCategories.length
      ? dataParameters.parametersGroupesCategories
      : ([] as any),
  )
    ? []
    : [{ id: TypeProject.PERSONNEL, value: 'Personnel' }];

  const typeProjectList = [
    ...typeGroupement,
    ...typePersonnel,
    { id: TypeProject.PROFESSIONNEL, value: 'Professionnel' },
  ];

  // console.log('Project to edit', projectToEdit);
  // console.log('Id parentProject', parentProject && parentProject.id);

  const defaultValue = {
    id: '',
    name: '',
    typeProject:
      projectToEdit && projectToEdit.typeProject
        ? projectToEdit.typeProject
        : TypeProject.PROFESSIONNEL,
    idCouleur: '',
    isFavoris: false,
    idUser: currentUser && currentUser.id,
    idPharmacie: currentPharmacie && currentPharmacie.id,
  };

  const [formState, setFormState] = useState<FormStateInterface>(defaultValue);

  // console.log('NEXT-PROJECT', nextProject && nextProject.ordre);
  // console.log('PREV-PROJECT', prevProject && prevProject.ordre);
  // console.log('ID-PROJECT', parentProject && parentProject.id);
  useEffect(() => {
    if (!open && setProjectToEdit) {
      setFormState(defaultValue);
      setProjectToEdit(null);
    }
  }, [open]);

  useEffect(() => {
    if (projectToEdit) {
      if (setPrevProject) {
        setPrevProject(null);
      }
      if (setNextProject) {
        setNextProject(null);
      }
      if (setParentProject) {
        setParentProject(null);
      }
      const projectInitialStateValue = {
        id: projectToEdit && projectToEdit.id,
        name: projectToEdit && projectToEdit._name,
        typeProject: projectToEdit && projectToEdit.typeProject,
        idCouleur: (projectToEdit && projectToEdit.couleur && projectToEdit.couleur.id) || 1,
        isFavoris: (projectToEdit && projectToEdit.isInFavoris) || false,
        idUser: currentUser && currentUser.id,
        idPharmacie: currentPharmacie && currentPharmacie.id,
      };
      setFormState(projectInitialStateValue);
    }
  }, [projectToEdit]);

  useEffect(() => {
    if (parentProject) {
      if (setPrevProject) {
        setPrevProject(null);
      }
      if (setNextProject) {
        setNextProject(null);
      }
      if (setProjectToEdit) {
        setProjectToEdit(null);
      }
      const projectInitialStateValue = {
        id: '',
        name: '',
        typeProject: parentProject && parentProject.typeProject,
        idCouleur: '',
        isFavoris: false,
        idUser: currentUser && currentUser.id,
        idPharmacie: currentPharmacie && currentPharmacie.id,
        idParent: parentProject && parentProject.id,
      };
      setFormState(projectInitialStateValue);
    }
  }, [parentProject]);

  useEffect(() => {
    if (prevProject) {
      if (setParentProject) {
        setParentProject(null);
      }
      if (setNextProject) {
        setNextProject(null);
      }
      if (setProjectToEdit) {
        setProjectToEdit(null);
      }
      const projectInitialStateValue = {
        ...defaultValue,
        ordre: prevProject && prevProject.ordre === 1 ? 1 : prevProject.ordre - 1,
      };
      setFormState(projectInitialStateValue);
    }
  }, [prevProject]);

  useEffect(() => {
    if (nextProject) {
      if (setParentProject) {
        setParentProject(null);
      }
      if (setPrevProject) {
        setPrevProject(null);
      }
      if (setProjectToEdit) {
        setProjectToEdit(null);
      }
      const projectInitialStateValue = {
        ...defaultValue,
        ordre: nextProject.ordre + 1,
      };
      setFormState(projectInitialStateValue);
    }
    // console.log('get exec');
  }, [nextProject]);

  const handleChange = e => {
    const { name, value } = e.target;
    setFormState(prevState => ({ ...prevState, [name]: value }));
  };

  const handleFavorise = () => {
    setFormState(prevState => ({ ...prevState, isFavoris: !prevState.isFavoris }));
  };

  const { loading, data } = useQuery<couleurs>(GET_COULEURS, { fetchPolicy: 'cache-and-network' });

  const couleurList =
    (data &&
      data.couleurs &&
      data.couleurs.filter(item => {
        if (!item.isRemoved) {
          return { id: item.id, libelle: item.libelle, code: item.code };
        }
      })) ||
    [];

  const [saveData, { error, loading: createProjectLoading }] = useMutation<
    CREATE_PROJECT,
    CREATE_PROJECTVariables
  >(DO_CREATE_PROJECT, {
    onCompleted: data => {
      if (data && data.createUpdateProject) {
        if (refetch) refetch();
        if (refetchCountTodos) refetchCountTodos();

        displaySnackBar(client, {
          type: 'SUCCESS',
          message: successMessage,
          isOpen: true,
        });
        setFormState(defaultValue);
        setOpen(false);
        if (setParentProject) {
          setParentProject(null);
        }
      }
    },
  });

  const handleSubmit = e => {
    // console.log(formState);
    saveData({
      variables: { input: formState },
    });
  };

  const { name, idCouleur, typeProject } = formState;
  const fieldValidator = () => {
    if (!name) {
      return true;
    }
    if (!idCouleur) {
      return true;
    }
    if (!typeProject) {
      return true;
    }
  };

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title={
        isEdit
          ? `Modification de ${termologie ? capitalizeFirstLetter(termologie) : 'Projet'}`
          : isSubProject
            ? `Ajout de sous-${termologie ? termologie : 'projet'}`
            : `Ajout de ${termologie ? capitalizeFirstLetter(termologie) : 'Projet'}`
      }
      withBtnsActions={true}
      closeIcon={true}
      headerWithBgColor={true}
      maxWidth="sm"
      fullWidth={true}
      className={classes.usersModalRoot}
      centerBtns={true}
      actionButton={isEdit ? 'Modifier' : 'Ajouter'}
      onClickConfirm={handleSubmit}
      disabledButton={fieldValidator()}
      withCancelButton={false}
    >
      {<Backdrop open={createProjectLoading} />}
      <Box className={classes.tableContainer}>
        <CustomFormTextField
          label="Nom"
          name="name"
          placeholder="Nom "
          required={true}
          onChange={handleChange}
          value={formState.name}
          autoFocus={true}
        />
        <CustomSelect
          label="Type du projet"
          list={typeProjectList}
          name="typeProject"
          listId="id"
          index="value"
          required={true}
          onChange={handleChange}
          value={formState.typeProject}
          disabled={
            parentProject ? true : projectToEdit && projectToEdit.projetParent ? true : false
          }
        />
        <CustomSelectColor
          label="Couleur de l'étiquette"
          list={couleurList}
          name="idCouleur"
          className={classes.espaceHaut}
          onChange={handleChange}
          required={true}
          listId="id"
          index="libelle"
          value={formState.idCouleur}
        />
        <Box className={classes.espacedroite}>
          <Typography>Ajouter au favoris</Typography>
          <Switch
            checked={formState.isFavoris}
            name="isFavoris"
            inputProps={{ 'aria-label': 'secondary checkbox' }}
            onChange={handleFavorise}
          />
        </Box>
      </Box>
    </CustomModal>
  );
};
export default withRouter(AjoutProjet);
