import { createStyles, Theme } from '@material-ui/core/styles';
import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    usersModalRoot: {
      '& .MuiDialogActions-root .MuiButtonBase-root': {
        width: 219,
        height: 50,
      },
      '& .MuiDialogContent-root': {
        padding: '8px 48px',
      },
      '& .MuiDialogActions-root': {
        padding: '0px 48px 48px',
        [theme.breakpoints.down('md')]: {
          width: '100vw',
          padding: 8,
          justifyContent: 'left',
        },
      },
      [theme.breakpoints.down('md')]: {
        '& .MuiDialog-scrollPaper': {
          justifyContent: 'left',
        },
        '& .MuiDialog-paperFullWidth': {
          width: '100%',
          maxWidth: '100%',
          minHeight: '100%',
        },
        '& .MuiDialog-paper': {
          margin: 0,
        },
        '& .MuiDialogContent-root': {
          padding: 0,
        },
        '& .MuiDialogTitle-root': {
          background: theme.palette.secondary.main,
        },
      },
    },
    tableContainer: {
      width: '100vw',
      padding: '50px 0 38px',
      '& .MuiFormControl-root': {
        marginBottom: 34,
      },
      [theme.breakpoints.down('md')]: {
        padding: '50px 8px 0px 8px',
      },
    },
    espaceHaut: {
      margin: '50px 0',
    },
    espacedroite: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
  }),
);
