import { useApolloClient, useLazyQuery, useMutation } from '@apollo/client';
import { AppBar, Box, Tab, Tabs } from '@material-ui/core';
import { InsertComment, ShowChart } from '@material-ui/icons';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import ReactQuill from 'react-quill';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { GET_UPDATE_ITEM_SCORING_FOR_ITEM } from '../../../../../../federation/tools/itemScoring/query';
import { DO_CREATE_ACTION } from '../../../../../../graphql/Action/mutation';
import {
  CREATE_ACTION,
  CREATE_ACTIONVariables,
} from '../../../../../../graphql/Action/types/CREATE_ACTION';
import { ActionInput, ActionStatus } from '../../../../../../types/graphql-global-types';
import { isMobile } from '../../../../../../utils/Helpers';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import { Comment } from '../../../../../Comment';
import { CommentListNew } from '../../../../../Comment/CommentList';
import Backdrop from '../../../../../Common/Backdrop';
import { CustomDatePicker } from '../../../../../Common/CustomDateTimePicker';
import { CustomModal } from '../../../../../Common/CustomModal';
import CustomTextarea from '../../../../../Common/CustomTextarea';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { computeCodeFromdateDebut } from '../../Common/UrgenceLabel/UrgenceLabel';
import { operationVariable } from '../../MainContent/MainContent';
import TaskActivities from '../../Task/TaskActivity';
import { CountTodosProps } from '../../TodoNew';
import { isManuelAction, isReunionQualiteAction, isTraitementAutomatiqueAction } from '../../util';
import CommonFieldsForm from './../../../../../Common/CommonFieldsForm';
import CustomAlertDonneesMedicales from './../../../../../Common/CustomAlertDonneesMedicales';
import { useStyles } from './styles';

interface UsersModalProps {
  openModalTache: boolean;
  operationName?: string;
  task?: any;
  queryVariables?: any;
  setOpenTache: (value: boolean) => void;
  idSection?: string;
  tacheRapide?: boolean;
  currentProjectId?: string;
  match: {
    params: {
      filter: string | undefined;
      filterId: string | undefined;
    };
  };
  refetchTasks?: any;
  refetchEtiquette?: any;
  refetchProject?: any;
  refetchAll?: () => void;
  section?: string;

  defaultValues?: any;
}

interface UrgenceInterface {
  value: string;
  label: string;
}

const dispatcherSaveCommentWrapper: any = [];

const AddTask: FC<UsersModalProps & RouteComponentProps & CountTodosProps> = props => {
  const classes = useStyles({});
  const client = useApolloClient();

  // data to fetch from back
  const {
    openModalTache,
    setOpenTache,
    task,
    // queryVariables,
    tacheRapide,
    idSection,
    currentProjectId,
    match: {
      params: { filterId },
    },
    // refetchCountTodos,
    // refetchTasks,
    location: { pathname },
    operationName,
    // refetchEtiquette,
    // refetchProject,
    refetchAll,
    section,
    defaultValues,
  } = props;

  console.log('=================================', task);
  //const onAddSub = operationName === operationVariable.addSubs;

  const isInbox = pathname.includes('recu') && !pathname.includes('recu-equipe');
  const isInboxTeam = pathname.includes('recu-equipe');
  //const isTodayOnly = pathname.includes('aujourdhui') && !pathname.includes('aujourdhui-equipe');
  //const isOnSevenDays: boolean = pathname.includes('/sept-jours');
  const isEtiquette = pathname.includes('etiquette');
  //const isProject = pathname.includes('projet');
  const [desabledPlanif, setdesabledPlanif] = useState<boolean>(false);
  //const isAllSee: boolean = pathname.includes('/tout-voir');
  //const [assignParticipant, setAssignParticipant] = React.useState(false);

  const defaultCheckeds: string[] =
    (task && task.etiquettes && task.etiquettes.map(i => i.id)) || [];

  // const enableMatriceResponsabilite = useValueParameterAsBoolean('0501');
  const [checkeds, setCheckeds] = useState<string[]>(defaultCheckeds);
  const [activeTab, setActiveTab] = useState<number>(0);
  /*
    const priorityList = [
      { id: 1, nom: 'Priorité 1' },
      { id: 2, nom: 'Priorité 2' },
      { id: 3, nom: 'Priorité 3' },
      { id: 4, nom: 'Priorité 4' },
    ];
  */
  const [assignedUsers, setAssingedUsers] = useState<any[]>([]);

  const urgenceList: UrgenceInterface[] = [
    {
      value: 'today',
      label: 'A : Journée',
    },
    { value: 'week', label: 'B : Semaine' },
    { value: 'month', label: 'C : Mois' },
  ];

  const [urgence, setUrgence] = useState<string>(urgenceList[0].value);

  const taskValueInitialState: ActionInput = {
    id: '',
    description: '',
    idProject: currentProjectId || '',
    idsEtiquettes: isEtiquette ? [filterId || ''] : [],
    idOrigine: '',
    idActionType: '',
    codeMaj: '',
    dateDebut: defaultValues?.dateDebut || null,
    dateFin: null,
    isInInbox: false,
    isInInboxTeam: false,
    isPrivate: false,
    codeItem: 'TODO',
    idActionParent: '',
    idItemAssocie: '',
    idSection: '',
    status: ActionStatus.ACTIVE,
    ordre: null,
    isRemoved: false,
    idsUsersDestinations: [],
    commentaire: {
      id: '',
      content: '',
    },
  };

  const [taskValue, setTaskValue] = useState<ActionInput>(taskValueInitialState);

  let title = task
    ? 'Modification de tâche'
    : tacheRapide
    ? 'Ajout de tâche Rapide'
    : `Ajout de tâche`;

  switch (operationName) {
    case operationVariable.edit:
      title = 'Modification de tâche';
      break;
    case operationVariable.addSubs:
      title = 'Ajout de sous-tâche';
      break;
    default:
      title = `Ajout de tâche`;
      break;
  }
  const nomBoutton = taskValue.id ? 'Modifier' : 'Ajouter';

  /*
  const user: ME_me = getUser();
  const sectionQueryVariables = id =>
    (id && id === 'INBOXTEAM') ||
    (idSection && isInboxTeam) ||
    (task && task.section && isInboxTeam) ||
    isInboxTeam
      ? {
          query: {
            bool: {
              must: [{ term: { isRemoved: false } }, { term: { isInInboxTeam: true } }],
              must_not: [
                {
                  exists: {
                    field: 'project',
                  },
                },
              ],
            },
          },
        }
      : id && id !== 'INBOX' && id !== 'INBOXTEAM'
      ? {
          query: {
            bool: {
              must: [{ term: { 'project.id': id } }, { term: { isRemoved: false } }],
            },
          },
        }
      : {
          query: {
            bool: {
              must: [{ term: { isRemoved: false } }, { term: { isInInbox: true } }],
              must_not: [
                {
                  exists: {
                    field: 'project',
                  },
                },
              ],
            },
          },
        };
        */

  const [updateItemScoring, updateItemScoringLoading] = useLazyQuery(
    GET_UPDATE_ITEM_SCORING_FOR_ITEM,
    {
      fetchPolicy: 'cache-and-network',
      client: FEDERATION_CLIENT,
    },
  );

  /* const [searchSection, { data: sectionQuery }] = useLazyQuery<any, SEARCH_TODO_SECTIONVariables>(
    GET_SEARCH_TODO_SECTION,
    {
      variables: {
        type: ['todosection'],
        query: sectionQueryVariables(filterId),
      },
    },
  );
*/
  // const sectionResult = (sectionQuery && sectionQuery.search && sectionQuery.search.data) || [];

  /*  const [getEtiquettes, { data: dataEtiquette }] = useLazyQuery<
    ETIQUETTES_BY_USER_WITH_MIN_INFO,
    ETIQUETTES_BY_USER_WITH_MIN_INFOVariables
  >(GET_ETIQUETTES_BY_USER_WITH_MIN_INFO, {
    fetchPolicy: 'network-only',
    variables: { idUser, isRemoved: false },
  });
*/

  // const [disableOrigin, setDisableOrigin] = useState<boolean>(false);

  const [createAndUpdateAction, { loading: addTaskLoading }] = useMutation<
    CREATE_ACTION,
    CREATE_ACTIONVariables
  >(DO_CREATE_ACTION, {
    onCompleted: data => {
      if (data && data.createUpdateAction) {
        updateItemScoring({
          variables: { codeItem: 'L', idItemAssocie: data.createUpdateAction.id },
        });
        if (refetchAll) refetchAll();

        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: taskValue.id
            ? 'Modification effectuée avec succès'
            : ' Création effectuée avec succès',
        });
        setOpenTache(false);
        setCheckeds([]);
        setTaskValue(taskValueInitialState);
        handleUserDestinationChanges([]);
      }
    },

    onError: error => {
      console.log('error :>> ', error);
      displaySnackBar(client, { isOpen: true, type: 'ERROR', message: error.message });
    },
  });

  /*const [getTodoActionOrigines, { data: dataOrigine }] = useLazyQuery<
    TODO_ACTION_ORIGINES,
    TODO_ACTION_ORIGINESVariables
  >(GET_TODO_ACTION_ORIGINES, {
    variables: { isRemoved: false },
    fetchPolicy: 'network-only',
  });
*/

  // const originList = (dataOrigine && dataOrigine.todoActionOrigines) || [];

  /*  const [getTodoActionTypes, { data: dataTypeAction }] = useLazyQuery<
      TODO_ACTION_TYPES,
      TODO_ACTION_TYPESVariables
    >(GET_TODO_ACTION_TYPES, { variables: { isRemoved: false }, fetchPolicy: 'network-only' });
  */

  /* const createUpdateActionEquipe = itemIserDestination => {
    const { idProject, dateDebut, idSection } = taskValue;
    createAndUpdateAction({
      variables: {
        input: {
          ...taskValue,
          idProject: idProject ? idProject : '',
          id: task ? task.id : '',
          idSection: taskValue.idSection,
          isInInbox: filterId ? false : idProject === '1' ? true : false,
          isInInboxTeam: filterId ? false : idProject === '2' ? true : false,
          idsEtiquettes: checkeds,
          dateDebut: dateDebut,
          codeItem: 'TODO',
          idsUsersDestinations: itemIserDestination,
        },
      },
    });
  }; */

  const createUpdateAction = () => {
    if (dispatcherSaveCommentWrapper[0]) {
      dispatcherSaveCommentWrapper[0](); //TODO : Find better solution
    }

    const { idProject, dateDebut, dateFin } = taskValue;
    createAndUpdateAction({
      variables: {
        input: {
          ...taskValue,
          idProject: idProject
            ? idProject === 'INBOXTEAM' || idProject === 'INBOX'
              ? ''
              : idProject
            : '',
          id: operationName === operationVariable.edit ? task.id : '',
          idSection: taskValue.idSection,
          isInInbox: idProject && idProject === 'INBOX' ? true : false,
          isInInboxTeam: idProject && idProject === 'INBOXTEAM' ? true : false,
          idImportance: taskValue.idImportance,
          dateDebut,
          dateFin,
          codeItem: 'TODO',
          idsUsersDestinations: taskValue.idsUsersDestinations,
        },
      },
    });
  };

  // ! IMPORTANT :  Execute lazy queries
  /*React.useEffect(() => {
    if (openModalTache) {
      //  getTodoActionOrigines();
      getTodoActionTypes();
      // searchSection();
    }
  }, [openModalTache]);
*/
  /*  React.useEffect(() => {
    if (dataOrigine && dataOrigine.todoActionOrigines) {
      let code = '';
      if (user.userPartenaire) code = 'PARTENAIRE_SERVICE';
      else if (user.userPersonnel) code = 'GROUPEMENT';
      else if (user.userLaboratoire) code = 'LABO_PARTENAIRE';
      else if (user.userTitulaire || user.userPpersonnel) code = 'PHARMACIE';

      const origines = code
        ? dataOrigine.todoActionOrigines.filter(origine => origine.code === code)
        : [];

      if (origines && origines.length) {
        setDisableOrigin(true);
        setTaskValue(prev => ({ ...prev, idOrigine: origines[0].id }));
      }
    }
  }, [dataOrigine]);
  */

  React.useEffect(() => {
    if (currentProjectId) {
      setTaskValue(prev => ({ ...prev, idProject: currentProjectId }));
    }
  }, [currentProjectId]);

  // On edit
  React.useEffect(() => {
    if (task && operationName) {
      /* if (task.project)
        searchSection({
          variables: {
            type: ['todosection'],
            query: sectionQueryVariables(task.project.id || ''),
          },
        });
*/
      if (operationName === operationVariable.edit) {
        const idsEtiquettes = (task.etiquettes && task.etiquettes.map(i => i.id)) || [];
        const idsUsersDestinations =
          (task.assignedUsers && task.assignedUsers.map(i => i.id)) || [];
        setAssingedUsers((task && task.assignedUsers) || []);
        const isInInbox = task.isInInbox;
        const isInInboxTeam = task.isInInboxTeam;
        const dateDebut = task.dateDebut || null;
        const dateFin = task.dateFin || null;
        const isPrivate = task.isPrivate ? true : false;

        const isToday: boolean = dateDebut && moment(dateDebut).format() === moment().format();
        // const isTodayTeam: boolean =
        //  isToday && task.assignedUsers && task.assignedUsers.length > 0 ? true : false;

        const idProject = task.project ? task.project.id : '';

        const newTaskValue: ActionInput = {
          id: task.id,
          description: task.description,
          idProject: idProject
            ? idProject
            : isInboxTeam
            ? 'INBOXTEAM'
            : isInbox
            ? 'INBOX'
            : 'INBOX',
          idsEtiquettes: idsEtiquettes && idsEtiquettes[0],
          idOrigine: (task.origine && task.origine.id) || '',
          idActionType: (task.actionType && task.actionType.id) || '',
          dateDebut,
          dateFin,
          isInInbox,
          isInInboxTeam,
          isPrivate,
          commentaire: {
            id: (task.firstComment && task.firstComment.id) || '',
            content: (task.firstComment && task.firstComment.content) || '',
          },
          idActionParent: (task.actionParent && task.actionParent.id) || '',
          status: task.status || ActionStatus.ACTIVE,
          idItemAssocie: task.idItemAssocie || task.id,
          idSection: (task.section && task.section.id) || '',
          idsUsersDestinations,
          ordre: task.ordre,
          isRemoved: task.isRemoved,
          idImportance: task.importance?.id,
        };
        setTaskValue(newTaskValue);
        setCheckeds(idsEtiquettes);
        setUrgence(computeCodeFromdateDebut(task.dateDebut));
      }
      if (operationName === operationVariable.addSubs) {
        const newTaskValue: ActionInput = {
          ...taskValueInitialState,
          idProject: task.project ? task.project.id : '',
          dateDebut: new Date(Date.now()),
          dateFin: null,
          idActionParent: task.id,
        };
        setTaskValue(newTaskValue);
      }
      if (operationName === operationVariable.addUpper) {
        const newTaskValue: ActionInput = {
          ...taskValueInitialState,
          ordre: task.ordre === 1 ? 1 : task.ordre - 1,
        };
        setTaskValue(newTaskValue);
      }
      if (operationName === operationVariable.addDown) {
        const newTaskValue: ActionInput = {
          ...taskValueInitialState,
          ordre: task.ordre + 1,
        };
        setTaskValue(newTaskValue);
      }
    }
  }, [task, operationName]);

  useEffect(() => {
    if (!task) {
      //setAssingedUsers([]);
    }
  }, [task]);

  React.useEffect(() => {
    if (!openModalTache && !task) {
      setTaskValue({ ...taskValueInitialState });
    } else if (openModalTache && !task) {
      setTaskValue({
        ...taskValueInitialState,
        dateDebut: new Date(Date.now()),
        dateFin: null,
        idSection: idSection || '',
        idProject: filterId ? filterId : isInboxTeam ? 'INBOXTEAM' : isInbox ? 'INBOX' : 'INBOX',
      });
    }
  }, [openModalTache, task]);

  const formChange = event => {
    const { value, name } = event.target;

    if (name === 'idProject') {
      /*searchSection({
        variables: {
          type: ['todosection'],
          query: sectionQueryVariables(value || ''),
        },
      });
      */
      setTaskValue(prevState => ({ ...prevState, idSection: '', [name]: value || '' }));
      setdesabledPlanif(false);

      return;
    }

    if (name === 'commentaire') {
      setTaskValue(prevState => ({
        ...prevState,
        commentaire: { ...prevState.commentaire, content: value },
      }));
      return;
    }

    setTaskValue(prevState => ({ ...prevState, [name]: value }));
  };

  const descriptionChange = (content: string) => {
    setTaskValue({ ...taskValue, description: content });
  };

  const handleChangeDate = (name: string) => (date: any) => {
    if (name) {
      setTaskValue(prevState => ({ ...prevState, [name]: date || null }));
    }
  };

  const handleTabsChange = (event: React.ChangeEvent<{}>, value: any) => {
    setActiveTab(value);
  };

  const handleUserDestinationChanges = (selected: any) => {
    const idsUsersDestinations = selected && selected.map(selected => selected.id);
    setAssingedUsers(selected);
    setTaskValue(prevState => ({ ...prevState, idsUsersDestinations }));
  };

  const handleChangeProjet = (idProjet: string) => {
    formChange({ target: { name: 'idProject', value: idProjet } });
  };

  useEffect(() => {
    const today = moment();

    const dateDebut = moment(taskValue.dateDebut);

    const nextSunday = moment()
      .endOf('isoWeek')
      .add(1, 'week');

    if (dateDebut.day() === today.day()) {
      setUrgence('A');
    }
    if (dateDebut > today && dateDebut <= nextSunday) {
      setUrgence('B');
    }
    if (dateDebut > nextSunday) {
      setUrgence('C');
    }
  }, [taskValue.dateDebut]);

  React.useEffect(() => {
    if (defaultValues) {
      setTaskValue(prev => ({ ...prev, ...defaultValues }));
    }
  }, [defaultValues]);

  const handleUrgenceChange = (event: any) => {
    const { value } = event.target;

    const today = moment();

    const dateDebut = moment(taskValue.dateDebut);

    const nextDay = moment().add(1, 'day');

    const nextSunday = moment()
      .endOf('isoWeek')
      .add(1, 'week');

    const nextMonth = moment()
      .startOf('isoWeek')
      .add(2, 'week');

    switch (value) {
      case 'A':
        if (dateDebut.day() !== today.day()) {
          setTaskValue(prevState => ({ ...prevState, dateDebut: moment().format() }));
        }
        break;

      case 'B':
        if (dateDebut < nextDay || dateDebut > nextSunday) {
          setTaskValue(prevState => ({ ...prevState, dateDebut: nextDay.format() }));
        }
        break;

      case 'C':
        if (dateDebut < nextMonth) {
          setTaskValue(prevState => ({ ...prevState, dateDebut: nextMonth.format() }));
        }
        break;

      default:
        break;
    }
    setUrgence(value);
  };

  const [elFocus, setElFocus] = useState<ReactQuill | null>(null);

  // useEffect(() => {
  //   if (elFocus) {
  //     elFocus.focus();
  //   }
  // });

  /*
  const activeProject =
    taskValue &&
    taskValue.idProject &&
    dataProject &&
    dataProject.search &&
    dataProject.search.data &&
    dataProject.search.data.length &&
    dataProject.search.data.find(project => project.id === taskValue.idProject);
    */

  if (section && section === 'DATE') {
    return (
      <CustomModal
        // tslint:disable-next-line: jsx-no-lambda
        onClick={event => event.stopPropagation()}
        open={openModalTache}
        setOpen={setOpenTache}
        title={title}
        withBtnsActions={true}
        withCancelButton={isMobile() ? false : true}
        closeIcon={true}
        headerWithBgColor={true}
        maxWidth="sm"
        fullWidth={true}
        className={classes.usersModalRoot}
        onClickConfirm={createUpdateAction}
        actionButton={nomBoutton}
        disabledButton={!taskValue.description ? true : false}
        disableBackdropClick={true}
      >
        <Box className={classes.tableContainer}>
          <Box className={classes.row}>
            <Box
              className={classes.sectionDatePicker}
              // tslint:disable-next-line: jsx-no-lambda
              onClick={event => {
                event.preventDefault();
                event.stopPropagation();
              }}
            >
              <CustomDatePicker
                label="Date Début"
                placeholder="Date début"
                onChange={handleChangeDate('dateDebut')}
                name="dateDebut"
                value={taskValue.dateDebut}
                disabled={desabledPlanif}
              />
            </Box>
            <Box
              className={classes.sectionDatePicker}
              // tslint:disable-next-line: jsx-no-lambda
              onClick={event => {
                event.preventDefault();
                event.stopPropagation();
              }}
            >
              <CustomDatePicker
                label="Date Fin"
                placeholder="Date fin"
                onChange={handleChangeDate('dateFin')}
                name="dateFin"
                value={taskValue.dateFin}
                disabled={desabledPlanif}
              />
            </Box>
          </Box>
        </Box>
        <Backdrop
          open={addTaskLoading}
          value={`${task ? 'Modification' : 'Création'} en cours...`}
        />
      </CustomModal>
    );
  }

  const isGeneratedFromTA = !isManuelAction(task);
  const alertMessage = isTraitementAutomatiqueAction(task)
    ? 'Vous ne pouvez pas modifier cette tâche qui a été générée à partir de traitement automatique'
    : isReunionQualiteAction(task)
    ? 'Vous ne pouvez pas modifier cette tâche qui a été générée à partir de la réunion qualité'
    : '';

  return (
    <CustomModal
      // tslint:disable-next-line: jsx-no-lambda
      onClick={event => event.stopPropagation()}
      open={openModalTache}
      setOpen={setOpenTache}
      title={isGeneratedFromTA ? 'Détail de tâche' : title}
      withBtnsActions={true}
      withCancelButton={isMobile() ? false : true}
      closeIcon={true}
      headerWithBgColor={true}
      maxWidth="sm"
      fullWidth={true}
      className={classes.usersModalRoot}
      onClickConfirm={createUpdateAction}
      actionButton={nomBoutton}
      disableBackdropClick={true}
      cancelButtonTitle={isGeneratedFromTA ? 'Fermer' : undefined}
      withConfirmButton={!isGeneratedFromTA}
      disabledButton={!taskValue.description || !taskValue.idImportance ? true : false}
    >
      <Box className={classes.tableContainer}>
        {isGeneratedFromTA && (
          <Box style={{ paddingBottom: 10 }}>
            <CustomAlertDonneesMedicales message={alertMessage} />
          </Box>
        )}
        <Box className={classes.section}>
          <ReactQuill
            readOnly={isGeneratedFromTA}
            className="customized-title"
            theme="snow"
            value={taskValue.description}
            onChange={descriptionChange}
            ref={el => {
              setElFocus(el);
            }}
          />
        </Box>

        <CommonFieldsForm
          disabled={isGeneratedFromTA}
          style={{ padding: 0 }}
          selectedUsers={assignedUsers}
          urgence={urgence}
          isPrivate={taskValue.isPrivate ? true : false}
          urgenceProps={{
            useCode: true,
          }}
          usersModalProps={{
            withNotAssigned: true,
            searchPlaceholder: 'Rechercher...',
            withAssignTeam: false,
            singleSelect: false,
          }}
          onlyMatriceFonctions={false}
          projet={taskValue.idProject}
          importance={taskValue.idImportance ? taskValue.idImportance : undefined}
          onChangeUsersSelection={handleUserDestinationChanges}
          onChangeUrgence={urgence =>
            handleUrgenceChange({ target: { name: 'idUrgence', value: urgence.code } })
          }
          onChangeImportance={importance =>
            formChange({ target: { name: 'idImportance', value: importance.id } })
          }
          onChangeProjet={projet => {
            handleChangeProjet(projet?.id);
          }}
          onChangePrivate={isPrivate => setTaskValue(prevState => ({ ...prevState, isPrivate }))}
        />

        <Box
          className={classes.sectionDatePicker}
          // tslint:disable-next-line: jsx-no-lambda
          onClick={event => {
            event.preventDefault();
            event.stopPropagation();
          }}
        >
          <CustomDatePicker
            label="Date Début"
            placeholder="Date début"
            onChange={handleChangeDate('dateDebut')}
            name="dateDebut"
            value={taskValue.dateDebut}
            disabled={isGeneratedFromTA || desabledPlanif}
            disablePast={false}
          />
        </Box>

        <Box
          className={classes.sectionDatePicker}
          // tslint:disable-next-line: jsx-no-lambda
          onClick={event => {
            event.preventDefault();
            event.stopPropagation();
          }}
        >
          <CustomDatePicker
            label="Date Fin"
            placeholder="Date fin"
            onChange={handleChangeDate('dateFin')}
            name="dateFin"
            value={taskValue.dateFin}
            disabled={isGeneratedFromTA || desabledPlanif}
            disablePast={false}
          />
        </Box>

        {/*<CssBaseline />
         <Hidden mdDown={true} implementation="css">
          <Box className={classes.row}>
            <Box className={classes.section}>
              <CustomSelect
                label="Origine"
                list={originList}
                name="idOrigine"
                className={classes.espaceHaut}
                onChange={formChange}
                listId="id"
                index="libelle"
                value={taskValue.idOrigine}
                disabled={disableOrigin}
              />
            </Box>
            <Box className={classes.section}>
              <CustomSelect
                label="Type d'action"
                list={idActionType}
                name="idActionType"
                listId="id"
                index="libelle"
                className={classes.espaceHaut}
                onChange={formChange}
                value={taskValue.idActionType}
              />
            </Box>
          </Box>
          <Box className={classes.section}>
            <CustomSelect
              label="Section"
              list={[...sectionResult]}
              name="idSection"
              index="libelle"
              className={classes.espaceHaut}
              onChange={formChange}
              listId="id"
              value={taskValue.idSection}
              disabled={idSection ? true : false}
            />
          </Box>
              </Hidden>*/}

        {task ? (
          <div className={classes.tabPaper}>
            <AppBar position="static" className={classes.tabs}>
              <Tabs
                centered={true}
                value={activeTab}
                onChange={handleTabsChange}
                indicatorColor="secondary"
                variant="scrollable"
                scrollButtons="auto"
                aria-label="add task modal tabs"
              >
                {/*<Tab icon={<LibraryBooks />} label="Sous-Tâche" />*/}
                <Tab icon={<InsertComment />} label="Commentaires" />
                <Tab icon={<ShowChart />} label="Activités" />
              </Tabs>
            </AppBar>
            {/*activeTab === 0 && <Box></Box>*/}
            {activeTab === 0 && (
              <Box>
                <CommentListNew codeItem="TODO" idItemAssocie={task.id} />
                <Comment
                  codeItem="TODO"
                  idSource={task.id}
                  withAttachement={true}
                  dispatchSubmitCb={dispatcher => {
                    dispatcherSaveCommentWrapper[0] = dispatcher;
                  }}
                />
              </Box>
            )}

            {activeTab === 1 && (
              <Box padding="15px">
                <TaskActivities idTask={task.id} />
              </Box>
            )}
          </div>
        ) : (
          <Box className={classes.section}>
            <CustomTextarea
              label="Commentaire"
              value={(taskValue.commentaire && taskValue.commentaire.content) || ''}
              name="commentaire"
              onChangeTextarea={formChange}
              rows={3}
            />
          </Box>
        )}
      </Box>
      <Backdrop open={addTaskLoading} value={`${task ? 'Modification' : 'Création'} en cours...`} />
    </CustomModal>
  );
};

export default withRouter(AddTask);
