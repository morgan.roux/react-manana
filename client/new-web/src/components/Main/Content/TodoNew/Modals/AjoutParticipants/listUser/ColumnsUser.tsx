import React, { useState } from 'react';
//import { Column } from "../../../../../../Common/newCustomContent/interfaces";
import CustomAvatar from '../../../../../../Common/CustomAvatar';
import { Column } from '../../../../../../Dashboard/Content/Interface';

export const columnUser = (history: any): [Column[]] => {
  const columns: Column[] = [
    {
      name: '',
      label: 'Photo',
      renderer: (value: any) => {
        const valueUser =
          value.users &&
          value.pharmacies &&
          value.pharmacies.length === 1 &&
          value.users.length === 1
            ? value.users[0]
            : null;
        return (
          <CustomAvatar
            name={value && value.fullName}
            url={
              valueUser &&
              valueUser.userPhoto &&
              valueUser.userPhoto.fichier &&
              valueUser.userPhoto.fichier.publicUrl
            }
          />
        );
      },
    },
    {
      name: 'userName',
      label: 'Nom',
      renderer: (value: any) => {
        return (value && value.userName) || '-';
      },
    },
    {
      name: 'role.nom',
      label: 'Fonction',
      renderer: (value: any) => {
        return value && value.role && value.role.nom ? value.role.nom : '-';
      },
    },
    {
      name: 'email',
      label: 'email',
    },
  ];

  return [columns];
};
