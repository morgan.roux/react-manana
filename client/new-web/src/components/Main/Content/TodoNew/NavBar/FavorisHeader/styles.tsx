import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      [theme.breakpoints.down('md')]: {
        borderTop: '1px solid rgb(227, 227, 227)',
      },
    },
    favorisContainer: {
      display: 'flex',
      flexDirection: 'column',
    },
    count: {
      marginLeft: 10,
      textAlign: 'center',
      background: '#E0E0E0',
      height: 20,
      minWidth: 20,
      borderRadius: 2,
      fontSize: 10,
      color: '#212121',
      lineHeight: '20px',
    },
    icons: {
      color: 'secondary',
      marginLeft: 16,
    },
    title: {
      color: '#212121',
      fontFamily: 'Roboto',
      marginLeft: 10,
      fontSize: '0.875rem',
    },
  }),
);

export default useStyles;
