import { Delete, Edit, Favorite } from '@material-ui/icons';
import EtiquettePartagee from '../../../../../../../assets/icons/todo/deplacer_etiquette_partage.svg';
import { ActionButtonMenu } from '../../../Common/ActionButton';
import React from 'react';
export const useSharedEtiquetteList = (
  editEtiquetteClick: (row: any) => void,
  addProjectTofavoriteClick: (row: any) => void,
  //removeFromSharedClick: (row: any) => void,
  deleteEtiquetteClick: (row: any) => void,
) => {
  const sharedEtiquetteMenuItems: ActionButtonMenu[] = [
    { label: 'Modifier', icon: <Edit />, onClick: editEtiquetteClick, disabled: false },
    {
      label: 'Ajouter au favoris',
      icon: <Favorite />,
      onClick: addProjectTofavoriteClick,
      disabled: false,
    },
    {
      label: 'Retirer des étiquettes partagées',
      icon: <img src={EtiquettePartagee} />,
      onClick: deleteEtiquetteClick,
      disabled: true,
    },
    { label: 'Supprimer', icon: <Delete />, onClick: deleteEtiquetteClick, disabled: false },
  ];
  return [sharedEtiquetteMenuItems];
};
