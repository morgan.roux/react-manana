export const todayFilter = ({ myId, useMatriceResponsabilite }) => {
  return useMatriceResponsabilite
    ? [
        {
          bool: {
            must_not: [
              {
                exists: {
                  field: 'assignedUsers',
                },
              },
              {
                term: { 'project.typeProject': 'PERSONNEL' },
              },
            ],
            must: [
              {
                term: { 'project.participants.id': myId },
              },
              /* {
               term: {
                 isInInboxTeam: false,
               },
             },*/
            ],
          },
        },
        {
          bool: {
            must_not: [
              {
                exists: {
                  field: 'assignedUsers',
                },
              },
              {
                exists: {
                  field: 'project.participants',
                },
              },
            ],
            must: [
              {
                term: { 'userCreation.id': myId },
              } /*,
              {
                term: { 'project.typeProject': 'PERSONNEL' },
              }*/,
              /* {
               term: {
                 isInInboxTeam: false,
               },
             },*/
            ],
          },
        },
        { term: { 'assignedUsers.id': myId } },
      ]
    : [
        {
          bool: {
            must_not: [
              {
                exists: {
                  field: 'assignedUsers',
                },
              },
            ],
            must: [
              {
                term: { 'userCreation.id': myId },
              },
              {
                term: {
                  isInInboxTeam: false,
                },
              },
            ],
          },
        },
        { term: { 'assignedUsers.id': myId } },
      ];
};

export const todayTeamFilter = ({
  myId,
  useMatriceResponsabilite,
  defaultShouldCreatedTeam,
  defaultShouldAssignTeam,
}) => {
  return useMatriceResponsabilite
    ? [
        {
          bool: {
            must_not: [
              
              {
                term: { 'project.typeProject': 'PERSONNEL' },
              },
            ],
            must: defaultShouldCreatedTeam,
            should: [
              {
                bool: {
                  must_not: todayFilter({ myId, useMatriceResponsabilite }),
                  must: [
                    {
                      term: {
                        'userCreation.id': myId,
                      },
                    },
                    { bool: { should: privateShould({ myId }) } },
                  ],
                },
              },
              {
                bool: {
                  must: [
                    {
                      exists: {
                        field: 'assignedUsers',
                      },
                    },
                    { bool: { should: privateShould({ myId }) } },
                  ],
                  must_not: [{ term: { 'assignedUsers.id': myId } }],
                },
              },
              {
                bool: {
                  must_not: [
                    {
                      exists: {
                        field: 'assignedUsers',
                      },
                    },
                    { term: { 'project.participants.id': myId } },
                  ],
                  should: [
                    {
                      exists: {
                        field: 'project',
                      },
                    },
                  ],
                  must: [{ bool: { should: privateShould({ myId }) } }],
                },
              },
            ],
            minimum_should_match: 1,
          },
        },
      ]
    : [
        {
          bool: {
            must_not: [
              {
                term: {
                  'assignedUsers.id': myId,
                },
              },
            ],
            must: [
              {
                exists: {
                  field: 'project.participants.id',
                },
              },
              ...defaultShouldCreatedTeam,
            ],
          },
        },
        {
          bool: {
            must_not: [
              {
                term: {
                  'assignedUsers.id': myId,
                },
              },
            ],
            must: [
              {
                term: {
                  'userCreation.id': myId,
                },
              },
              ...defaultShouldAssignTeam,
            ],
          },
        },
        {
          bool: {
            must: [
              {
                term: {
                  isInInboxTeam: true,
                },
              },
              {
                term: {
                  'userCreation.id': myId,
                },
              },
            ],
          },
        },
      ];
};

export const privateShould = ({ myId }) => {
  return [
    {
      bool: {
        must: [
          {
            term: { isPrivate: false },
          },
        ],
      },
    },
    {
      bool: {
        should: [
          {
            bool: {
              must: [{ term: { isPrivate: true } }, { term: { 'assignedUsers.id': myId } }],
            },
          },
          {
            bool: {
              must: [
                { term: { isPrivate: true } },
                {
                  term: {
                    'userCreation.id': myId,
                  },
                },
              ],
            },
          },
          {
            bool: {
              must: [{ term: { isPrivate: true } }, { term: { 'project.participants.id': myId } }],
              must_not: [
                {
                  exists: {
                    field: 'assignedUsers',
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ];
};
