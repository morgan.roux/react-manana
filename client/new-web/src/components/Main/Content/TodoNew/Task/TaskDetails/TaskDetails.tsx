import { useApolloClient, useLazyQuery, useMutation } from '@apollo/client';
import {
  Box,
  Icon,
  IconButton,
  ListItemIcon,
  Menu,
  MenuItem,
  Radio,
  Typography,
} from '@material-ui/core';
import { CheckCircleOutline, Delete, Edit, LocalOffer, MoreHoriz, Today } from '@material-ui/icons';
import classnames from 'classnames';
import moment from 'moment';
import React from 'react';
import { DO_UPDATE_ACTION_STATUS } from '../../../../../../graphql/Action/mutation';
import { GET_UPDATE_ITEM_SCORING_FOR_ITEM } from '../../../../../../federation/tools/itemScoring/query';
import {
  UPDATE_ITEM_SCORING_FOR_ITEM,
  UPDATE_ITEM_SCORING_FOR_ITEMVariables,
} from '../../../../../../federation/tools/itemScoring/types/UPDATE_ITEM_SCORING_FOR_ITEM';
import { SEARCH_ACTION_search_data_Action } from '../../../../../../graphql/Action/types/SEARCH_ACTION';
import {
  UPDATE_ACTION_STATUS,
  UPDATE_ACTION_STATUSVariables,
} from '../../../../../../graphql/Action/types/UPDATE_ACTION_STATUS';
import { nl2br } from '../../../../../../services/Html';
import { getUser } from '../../../../../../services/LocalStorage';
import { ActionStatus } from '../../../../../../types/graphql-global-types';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import { Comment } from '../../../../../Comment';
import { CommentListNew } from '../../../../../Comment/CommentList';
import { ConfirmDeleteDialog } from '../../../../../Common/ConfirmDialog';
import CustomTabs, { TabInterface } from '../../../../../Common/CustomTabs/CustomTabs';
import useCommonStyles from '../../MainContent/Section/styles';
import AjoutTaches from '../../Modals/AjoutTache/AjoutTaches';
import { CountTodosProps } from '../../TodoNew';
import TaskActivities from '../TaskActivity';
import useStyles from './styles';
import { useValueParameterAsBoolean } from '../../../../../../utils/getValueParameter';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';

interface TaskDetailsProps {
  task: SEARCH_ACTION_search_data_Action;
  refetchTasks?: any;
  disabledActions?: boolean;
  doSoftDeleteActions?: Function;
  refetchAll?: () => void;
}

const TaskDetails: React.FC<TaskDetailsProps & CountTodosProps> = ({
  task,
  doSoftDeleteActions,
  refetchTasks,
  refetchCountTodos,
  disabledActions = false,
  refetchAll,
}) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();
  const client = useApolloClient();
  const useMatriceResponsabilite = useValueParameterAsBoolean('0501');

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [openFormModal, setOpenFormModal] = React.useState<boolean>(false);
  const [openDeleteDialog, setOpenDeleteDialog] = React.useState(false);

  const tabs: TabInterface[] = [
    {
      id: 0,
      label: 'Commentaires',
      content: (
        <Box display="flex" flexDirection="column">
          <CommentListNew codeItem="TODO" idItemAssocie={task.id} />
          {!disabledActions && (
            <Comment codeItem="TODO" idSource={task.id} withAttachement={true} />
          )}
        </Box>
      ),
    },
    {
      id: 1,
      label: 'Activités',
      content: <TaskActivities idTask={task.id} />,
    },
  ];

  const user = getUser();
  const assigns: any[] =
    task && task.assignedUsers && task.assignedUsers.length
      ? task.assignedUsers.map(user => user.id)
      : [];
  const idCreation = task && task.userCreation ? task.userCreation.id : '';

  const [updateItemScoring] = useLazyQuery<
    UPDATE_ITEM_SCORING_FOR_ITEM,
    UPDATE_ITEM_SCORING_FOR_ITEMVariables
  >(GET_UPDATE_ITEM_SCORING_FOR_ITEM, {
    client: FEDERATION_CLIENT,
  });

  const [doUpdateActionStatus] = useMutation<UPDATE_ACTION_STATUS, UPDATE_ACTION_STATUSVariables>(
    DO_UPDATE_ACTION_STATUS,
    {
      onError: error => {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
      onCompleted: data => {
        if (data && data.updateActionStatus) {
          updateItemScoring({
            variables: {
              idItemAssocie: data.updateActionStatus.id,
              codeItem: 'L',
            },
          });
          task.status = data.updateActionStatus.status;
          refetchTasks();
          if (refetchCountTodos) refetchCountTodos();
        }
      },
    },
  );
  const handleChange = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    event.preventDefault();
    event.stopPropagation();
    if (task) {
      if (task.status === ActionStatus.DONE) {
        doUpdateActionStatus({
          variables: { input: { idAction: task.id, status: ActionStatus.ACTIVE } },
        });
      } else if (task.status === ActionStatus.ACTIVE) {
        doUpdateActionStatus({
          variables: { input: { idAction: task.id, status: ActionStatus.DONE } },
        });
      }
    }
  };

  const onClickMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMenu = () => setAnchorEl(null);

  const onClickEdit = () => {
    handleCloseMenu();
    setOpenFormModal(true);
  };

  const onClickDelete = () => {
    handleCloseMenu();
    setOpenDeleteDialog(true);
  };

  const onClickConfirmDelete = () => {
    setOpenDeleteDialog(false);
    if (doSoftDeleteActions) {
      doSoftDeleteActions({ variables: { ids: [task.id] } });
    }
  };

  return (
    <div className={classes.taskDetailsRoot}>
      <div className={classes.taskContainer}>
        <div className={classes.taskTitleContainer}>
          <div className={classes.radioContainer}>
            <Radio
              className={classes.radio}
              checked={task.status === ActionStatus.DONE}
              onClick={handleChange}
              checkedIcon={<CheckCircleOutline />}
              disabled={
                (task.assignedUsers &&
                  !task.assignedUsers.length &&
                  task.userCreation &&
                  user &&
                  task.userCreation.id === user.id) ||
                  (task.assignedUsers &&
                    task.assignedUsers.length &&
                    user &&
                    task.assignedUsers.map(assign => assign.id).includes(user.id))
                  ? false
                  : !useMatriceResponsabilite
              }
            />
            <Typography
              className={classes.taskTitle}
              dangerouslySetInnerHTML={{ __html: nl2br(task.description) } as any}
            />
          </div>
          <IconButton onClick={onClickMenu} disabled={disabledActions}>
            <MoreHoriz />
          </IconButton>
          <Menu
            id="task-details-menu"
            anchorEl={anchorEl}
            keepMounted={true}
            open={Boolean(anchorEl)}
            onClose={handleCloseMenu}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
          >
            <MenuItem onClick={onClickEdit}>
              <ListItemIcon>
                <Edit fontSize="small" />
              </ListItemIcon>
              <Typography variant="inherit">Modifier</Typography>
            </MenuItem>
            <MenuItem
              onClick={onClickDelete}
              disabled={
                user && user.id && (assigns.includes(user.id) || idCreation === user.id)
                  ? false
                  : true
              }
            >
              <ListItemIcon>
                <Delete fontSize="small" />
              </ListItemIcon>
              <Typography variant="inherit">Supprimer</Typography>
            </MenuItem>
          </Menu>
        </div>
        <Box className={classnames(commonClasses.contentLabels, classes.taskInfoContainer)}>
          {task.dateDebut && (
            <Box className={commonClasses.labels}>
              <Icon color="secondary">
                <Today />
              </Icon>
              <Typography className={classnames(commonClasses.labelText, commonClasses.date)}>
                {moment(task.dateDebut).format('DD MMM YYYY')}
              </Typography>
            </Box>
          )}
          {task.dateFin && (
            <Box className={commonClasses.labels}>
              <Icon color="secondary">
                <Today />
              </Icon>
              <Typography className={classnames(commonClasses.labelText, commonClasses.date)}>
                {moment(task.dateFin).format('DD MMM YYYY')}
              </Typography>
            </Box>
          )}
          {/* <Box className={commonClasses.labels}>
            <AssistantPhoto
              style={{
                color:
                  task.priority === 1
                    ? 'red'
                    : task.priority === 2
                    ? 'yellow'
                    : task.priority === 3
                    ? 'green'
                    : 'gray',
              }}
            />
            <Typography className={classnames(commonClasses.labelText)}>
              Priorité {task.priority}
            </Typography>
            </Box>*/}
          {(task?.etiquettes || []).map(etiquette => (
            <Box className={commonClasses.labels} key={etiquette.id}>
              <Icon>
                <LocalOffer />
              </Icon>
              <Typography className={classnames(commonClasses.bold, commonClasses.labelText)}>
                {etiquette.nom}
              </Typography>
            </Box>
          ))}
        </Box>
      </div>
      <CustomTabs tabs={tabs} hideArrow={true} />
      <AjoutTaches
        openModalTache={openFormModal}
        setOpenTache={setOpenFormModal}
        task={task}
        refetchCountTodos={refetchCountTodos}
        refetchTasks={refetchTasks}
        operationName={'EDIT'}
        refetchAll={refetchAll}
      />
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content="Êtes-vous sur de vouloir supprimer ?"
        onClickConfirm={onClickConfirmDelete}
      />
    </div>
  );
};

export default TaskDetails;
