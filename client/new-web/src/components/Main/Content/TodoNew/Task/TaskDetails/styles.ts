import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    taskDetailsRoot: {
      width: '100%',
      marginBottom: 30,
      '& header': {
        marginBottom: 30,
      },
      '& .MuiTabs-root': {
        width: '100%',
        '& .MuiTabs-flexContainer': {
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          '& > button': {
            width: '100%',
          },
        },
      },
    },
    taskContainer: {
      border: '1px solid #E3E3E3',
      borderRadius: 5,
      padding: '5px 0px',
      marginTop: 5,
      marginBottom: 40,
    },
    taskTitleContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    radioContainer: {
      display: 'flex',
      alignItems: 'center',
    },
    radio: {
      '& .MuiSvgIcon-root': {
        width: 30,
        height: 30,
      },
    },
    taskTitle: {
      fontFamily: 'Montserrat',
      fontSize: 16,
      fontWeight: 'normal',
    },
    taskInfoContainer: {
      marginLeft: 45,
    },
  }),
);

export default useStyles;
