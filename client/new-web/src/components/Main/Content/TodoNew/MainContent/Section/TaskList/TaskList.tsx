import { useApolloClient, useLazyQuery, useMutation } from '@apollo/client';
import { RouteComponentProps, withRouter } from 'react-router';
import { Box, List, ListItem } from '@material-ui/core';
import { ApolloQueryResult } from '@apollo/client';
import React, { Fragment, useState } from 'react';
import {
  DO_SOFT_DELETE_ACTIONS,
  DO_UPDATE_ACTION_STATUS,
} from '../../../../../../../graphql/Action/mutation';
import {
  GET_UPDATE_ITEM_SCORING_FOR_ITEM,
  UPDATE_ITEM_SCORING_FOR_USER,
} from '../../../../../../../federation/tools/itemScoring/query';
import {
  UPDATE_ITEM_SCORING_FOR_USER as UPDATE_ITEM_SCORING_FOR_USER_TYPE,
  UPDATE_ITEM_SCORING_FOR_USERVariables,
} from '../../../../../../../federation/tools/itemScoring/types/UPDATE_ITEM_SCORING_FOR_USER';
import {
  UPDATE_ITEM_SCORING_FOR_ITEM,
  UPDATE_ITEM_SCORING_FOR_ITEMVariables,
} from '../../../../../../../federation/tools/itemScoring/types/UPDATE_ITEM_SCORING_FOR_ITEM';
import {
  SEARCH_ACTION,
  SEARCH_ACTIONVariables,
  SEARCH_ACTION_search_data_Action,
} from '../../../../../../../graphql/Action/types/SEARCH_ACTION';
import {
  SOFT_DELETE_ACTIONS,
  SOFT_DELETE_ACTIONSVariables,
} from '../../../../../../../graphql/Action/types/SOFT_DELETE_ACTIONS';
import {
  UPDATE_ACTION_STATUS,
  UPDATE_ACTION_STATUSVariables,
} from '../../../../../../../graphql/Action/types/UPDATE_ACTION_STATUS';
import {
  DO_ASSIGN_TASK_TO_USERS,
  DO_UNASSIGN_TASK_TO_USERS,
} from '../../../../../../../graphql/Todo/mutation';
import {
  ASSIGN_TASK_TO_USERS,
  ASSIGN_TASK_TO_USERSVariables,
} from '../../../../../../../graphql/Todo/types/ASSIGN_TASK_TO_USERS';
import {
  UNASSIGN_TASK_TO_USERS,
  UNASSIGN_TASK_TO_USERSVariables,
} from '../../../../../../../graphql/Todo/types/UNASSIGN_TASK_TO_USERS';
import { ActionStatus } from '../../../../../../../types/graphql-global-types';
import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';
import Backdrop from '../../../../../../Common/Backdrop';
import { CustomModal } from '../../../../../../Common/CustomModal';
import AssignTaskUserModal from '../../../../../../Common/CustomSelectUser';
import TaskDetails from '../../../Task/TaskDetails';
import { CountTodosProps } from '../../../TodoNew';
import useStyles from '../styles';
import { ME_me } from '../../../../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../../../../services/LocalStorage';
import _ from 'lodash';
import Task from './Task/Task';
import AjoutTaches from '../../../Modals/AjoutTache';
import { isMobile } from '../../../../../../../utils/Helpers';
import { FEDERATION_CLIENT } from '../../../../../../Dashboard/DemarcheQualite/apolloClientFederation';

interface TaskListProps {
  refetchEtiquette: any;
  refetchProject: any;
  parameters: any;
  tasks: SEARCH_ACTION_search_data_Action[];
  refetch?: (
    variables?: SEARCH_ACTIONVariables | undefined,
  ) => Promise<ApolloQueryResult<SEARCH_ACTION>>;
  refetchAll?: () => void;
}

const TaskList: React.FC<TaskListProps & RouteComponentProps & CountTodosProps> = ({
  tasks,
  refetch,
  refetchCountTodos,
  refetchEtiquette,
  refetchProject,
  location: { pathname },
  parameters,
  refetchAll,
}) => {
  const classes = useStyles({});

  const [open, setOpen] = React.useState<boolean>(false);
  const [clickedTask, setClickedTask] = React.useState<SEARCH_ACTION_search_data_Action | null>(
    null,
  );
  const client = useApolloClient();
  const me: ME_me = getUser();
  const [openModalTache, setOpenModalTache] = useState<boolean>(false);

  const taskParents = _.filter(tasks || [], task => task.actionParent === null);
  const taskParentIds = taskParents.map(parent => parent.id);
  const taskChildIds = tasks
    .filter(task => task && task.actionParent)
    .map(child => child && child.id);

  const listTasks = [
    ...taskParents,
    ...tasks.filter(
      task =>
        task.actionParent && task.actionParent.id && !taskParentIds.includes(task.actionParent.id),
    ),
  ];

  const onClickTask = (task: any) => (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    event.preventDefault();
    event.stopPropagation();
    setClickedTask(task);
    setOpenModalTache(true);
  };

  const [updateItemScoring, updatingItemScoring] = useLazyQuery<
    UPDATE_ITEM_SCORING_FOR_ITEM,
    UPDATE_ITEM_SCORING_FOR_ITEMVariables
  >(GET_UPDATE_ITEM_SCORING_FOR_ITEM, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
  });

  const [updateItemScoringByUser, updatingItemScoringByUser] = useLazyQuery<
    UPDATE_ITEM_SCORING_FOR_USER_TYPE,
    UPDATE_ITEM_SCORING_FOR_USERVariables
  >(UPDATE_ITEM_SCORING_FOR_USER, { fetchPolicy: 'cache-and-network', client: FEDERATION_CLIENT });

  const [doUpdateActionStatus, { loading: loadingChangeStatus }] = useMutation<
    UPDATE_ACTION_STATUS,
    UPDATE_ACTION_STATUSVariables
  >(DO_UPDATE_ACTION_STATUS, {
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
    onCompleted: data => {
      if (data && data.updateActionStatus) {
        updateItemScoring({
          variables: {
            idItemAssocie: data.updateActionStatus.id,
            codeItem: 'L',
          },
        });
        if (refetch) {
          refetch();
        }
        if (refetchCountTodos) refetchCountTodos();
        if (refetchEtiquette) refetchEtiquette();
        if (refetchProject) refetchProject();
      }
    },
  });

  const [assignParticipant, setAssignParticipant] = React.useState(false);
  const [taskToAssign, setTaskToAssign] = React.useState<any>(null);

  const [assignTaskToParticipant, { loading: loadingAssign }] = useMutation<
    ASSIGN_TASK_TO_USERS,
    ASSIGN_TASK_TO_USERSVariables
  >(DO_ASSIGN_TASK_TO_USERS, {
    onCompleted: data => {
      updateItemScoringByUser({
        variables: {
          idUser: getUser().id,
          codeItem: 'L',
        },
      });
      displaySnackBar(client, {
        isOpen: true,
        type: 'SUCCESS',
        message: 'Tâche assignée avec succès',
      });
      if (data && data.assignActionUsers) {
        setTaskToAssign(data.assignActionUsers);
      }
      if (refetchAll) refetchAll();
      else {
        if (refetch) refetch();
        if (refetchCountTodos) refetchCountTodos();
      }
    },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  // désassignation
  const [unAssignTaskToParticipant, { loading: loadingUnAssign }] = useMutation<
    UNASSIGN_TASK_TO_USERS,
    UNASSIGN_TASK_TO_USERSVariables
  >(DO_UNASSIGN_TASK_TO_USERS, {
    onCompleted: data => {
      updateItemScoringByUser({
        variables: {
          idUser: getUser().id,
          codeItem: 'L',
        },
      });
      displaySnackBar(client, {
        isOpen: true,
        type: 'SUCCESS',
        message: 'Tâche désassignée avec succès',
      });
      if (data && data.unAssignActionUsers) {
        setTaskToAssign(data.unAssignActionUsers);
      }
      if (refetchAll) refetchAll();
    },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const onClickCheckbox = (task: SEARCH_ACTION_search_data_Action) => (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ) => {
    event.preventDefault();
    event.stopPropagation();
    if (task) {
      if (task.status === ActionStatus.DONE) {
        doUpdateActionStatus({
          variables: { input: { idAction: task.id, status: ActionStatus.ACTIVE } },
        });
      } else if (task.status === ActionStatus.ACTIVE) {
        doUpdateActionStatus({
          variables: { input: { idAction: task.id, status: ActionStatus.DONE } },
        });
      }
    }
  };

  const handleValidateParticipants = (newParticipants: any) => {
    const newAssignUsers = newParticipants.filter(
      ({ id }) => !taskToAssign.assignedUsers.some((assignment: any) => assignment.id === id),
    );
    if (newAssignUsers.length > 0) {
      assignTaskToParticipant({
        variables: {
          idAction: taskToAssign.id,
          idUserDestinations: newAssignUsers.map(({ id }) => id),
        },
      });
    }

    const oldAssignUsers = taskToAssign.assignedUsers.filter(
      ({ id }) => !newParticipants.some(participant => participant.id === id),
    );
    if (oldAssignUsers.length > 0) {
      unAssignTaskToParticipant({
        variables: {
          idAction: taskToAssign.id,
          idUserDestinations: oldAssignUsers.map(({ id }) => id),
        },
      });
    }
  };
  const handleOpenModal = (task: SEARCH_ACTION_search_data_Action) => (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ) => {
    event.preventDefault();
    event.stopPropagation();
    setAssignParticipant(true);
    setTaskToAssign(task);
  };

  // Update task on details after edit
  React.useMemo(() => {
    if (tasks.length > 0 && clickedTask) {
      const newClickedTask = tasks.find(t => t.id === clickedTask.id);
      if (newClickedTask) setClickedTask(newClickedTask);
    }
  }, [clickedTask, tasks]);

  // Mutation delete tasks
  const [doSoftDeleteActions, { loading: deleteLoading, called }] = useMutation<
    SOFT_DELETE_ACTIONS,
    SOFT_DELETE_ACTIONSVariables
  >(DO_SOFT_DELETE_ACTIONS, {
    onCompleted: data => {
      if (data && data.softDeleteActions) {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Suppression effectuée avec succès',
        });
        if (refetch) {
          refetch();
        }
        if (refetchCountTodos) {
          refetchCountTodos();
        }
      }
    },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  // Close modal details on delete
  React.useMemo(() => {
    if (called) setOpen(false);
  }, [called]);

  /* const locationAccepted = ['/todo/projet', '/todo/aujourdhui-equipe']; */

  return (
    <Fragment>
      {deleteLoading && <Backdrop value="Suppression en cours, veuillez patienter ..." />}
      {loadingChangeStatus && (
        <Backdrop value="Changement de status en cours, veuillez patienter ..." />
      )}
      {(loadingAssign || loadingUnAssign) && <Backdrop />}
      <List className={classes.list}>
        {listTasks &&
          listTasks.length > 0 &&
          listTasks.map(task => (
            <Box key={task.id}>
              <ListItem
                button={true}
                className={classes.list}
                key={task.id}
                onClick={onClickTask(task)}
              >
                <Task
                  handleOpenModal={handleOpenModal}
                  onClickCheckbox={onClickCheckbox}
                  parameters={parameters}
                  refetchEtiquette={refetchEtiquette}
                  refetchProject={refetchProject}
                  task={task}
                  refetch={refetch}
                  refetchCountTodos={refetchCountTodos}
                  refetchAll={refetchAll}
                />
              </ListItem>
              {task &&
                task.subActions &&
                task.subActions.length > 0 &&
                task.subActions.map(
                  subAction =>
                    subAction &&
                    subAction.id &&
                    taskChildIds.includes(subAction.id) && (
                      <ListItem
                        button={true}
                        className={classes.list}
                        key={subAction.id}
                        onClick={onClickTask(subAction)}
                      >
                        <Task
                          handleOpenModal={handleOpenModal}
                          onClickCheckbox={onClickCheckbox}
                          parameters={parameters}
                          refetchEtiquette={refetchEtiquette}
                          refetchProject={refetchProject}
                          task={subAction}
                          refetch={refetch}
                          refetchCountTodos={refetchCountTodos}
                          isSubTask={true}
                          refetchAll={refetchAll}
                        />
                      </ListItem>
                    ),
                )}
            </Box>
          ))}
      </List>
      {/*<CustomModal
        open={open}
        setOpen={setOpen}
        title="Détails de tâche"
        withBtnsActions={false}
        closeIcon={true}
        headerWithBgColor={true}
        maxWidth="md"
        fullWidth={true}
        className={classes.taskDetailsModal}
      >
        {clickedTask && (
          <TaskDetails
            task={clickedTask}
            doSoftDeleteActions={doSoftDeleteActions}
            refetchTasks={refetch}
            refetchCountTodos={refetchCountTodos}
            refetchAll={refetchAll}
          />
        )}
      </CustomModal>*/}

      <AssignTaskUserModal
        openModal={assignParticipant}
        setOpenModal={setAssignParticipant}
        refetchCountTodos={refetchCountTodos}
        idParticipants={(taskToAssign?.project?.participants || []).map(({ id }) => id)}
        selected={taskToAssign?.assignedUsers || []}
        setSelected={participants => handleValidateParticipants(participants)}
      />
      <AjoutTaches
        openModalTache={openModalTache}
        setOpenTache={setOpenModalTache}
        task={clickedTask}
        operationName="EDIT"
        refetchCountTodos={refetchCountTodos}
        refetchTasks={refetch}
        refetchEtiquette={refetchEtiquette}
        refetchProject={refetchProject}
        refetchAll={refetchAll}
      />
    </Fragment>
  );
};

export default withRouter(TaskList);
