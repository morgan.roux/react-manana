import Box from '@material-ui/core/Box';
import { useTheme } from '@material-ui/core/styles';
import React, { useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import MainHeader from '../Common/MainHeader';
import AjoutTaches from '../Modals/AjoutTache/AjoutTaches';
import Section from './Section';
import useStyles from './styles';
import moment from 'moment';
import { capitalizeFirstLetter } from '../../../../../utils/capitalizeFirstLetter';
import { GET_PROJECT } from '../../../../../graphql/Project/query';
import { PROJECT, PROJECTVariables } from '../../../../../graphql/Project/types/PROJECT';
import { useQuery } from '@apollo/client';
import { CountTodosProps } from '../TodoNew';
import { ETIQUETTE, ETIQUETTEVariables } from '../../../../../graphql/Etiquette/types/ETIQUETTE';
import { GET_ETIQUETTE } from '../../../../../graphql/Etiquette/query';
import MobileTopBar from '../../../../Common/MobileTopBar';
import ActionButton from '../Common/ActionButton';
import { filterActionsByOffsetFromNow } from '../Common/utils/actions';

interface MainContentProps {
  window?: () => Window;
  parameters: any[];
  filters: any;
  match: {
    params: {
      filter;
      filterId;
    };
  };
  actions: any;
  setActionListFilters: (value: any) => void;
  actionListFilters?: any;
  setTriage?: (triage: any[]) => void;
  triage?: any;
  setTaskListVariables?: (values: any) => void;
  taskListVariables?: any;
  refetchTask?: any;
  loadingTask?: any;
  etiquetteList?: any;
  projectList?: any;
  sectionQuery?: any;
  refetchSection?: any;
  refetchAll?: () => void;
  handleDrawerToggle: () => void;
  useMatriceResponsabilite: boolean;
}

export const defaultDateTerm = { range: { dateDebut: { gt: 'now/d' } } } /*  {
  term: {
    dateDebut: moment()
      .add(1, 'days')
      .endOf('day')
      .format('YYYY-MM-DD'),
  },
};*/

export const operationVariable = {
  edit: 'EDIT',
  addSubs: 'ADD_SUBS',
  addUpper: 'ADD_UPPER',
  addDown: 'ADD_DOWN',
};

const MainContent: React.FC<RouteComponentProps & MainContentProps & CountTodosProps> = props => {
  const {
    parameters,
    location: { pathname },
    filters,
    match: {
      params: { filter, filterId },
    },
    refetchCountTodos,
    actions,
    setActionListFilters,
    actionListFilters,
    setTriage,
    triage,
    taskListVariables,
    loadingTask,
    refetchTask,
    etiquetteList,
    projectList,
    sectionQuery,
    refetchSection,
    refetchAll,
    handleDrawerToggle,
    useMatriceResponsabilite,
  } = props;
  const [cacheTacheAchevE, setCacheTacheAchevE] = useState<boolean>(true);
  const [statutMainHeader, setStatutMainHeader] = useState<boolean>(false);
  const [refetchTasks, setRefetchTasks] = useState<any>(null);
  const [nextTaskOffsetFromNow, setNextTaskOffsetFromNow] = useState<number>(1)
  const [defaultValues, setDefaultValues] = useState<any>()

  const isTodayOnly: boolean =
    pathname.includes('aujourdhui') && !pathname.includes('aujourdhui-equipe');
  const isOnProject = pathname.includes('projet');
  const isOnReceptionEquipe = filter === 'recu-equipe';
  const isOnAujourdhui: boolean = pathname.includes('/aujourdhui');
  const isOnAujourhduiEquipe: boolean = pathname.includes('aujourdui-equipe');
  const isOnSeptJours: boolean =
    pathname.includes('/sept-jours') && !pathname.includes('sept-jours-equipe');
  const isEtiquette = pathname.includes('etiquette');
  const withoutDate: boolean = pathname.includes('sans-date');
  const isAllSee: boolean = pathname.includes('/tout-voir');
  const isOnSeptJoursEquipe: boolean = pathname.includes('/sept-jours-equipe');

  const classes = useStyles({});
  const isOnReception = filter === 'recu' || filter === undefined;

  const refetchEtiquette = etiquetteList && etiquetteList.refetch;
  const refetchProject = projectList && projectList.refetch;
  const sectionResult = sectionQuery && sectionQuery.search && sectionQuery.search.data;

  const [openModalTache, setOpenModalTache] = useState(false);
  const [currentProjectId, setCurrentProjectId] = useState();

  const handleSetOpenModalTache = (dateDebut: any) => {
    setCurrentProjectId(filterId);
    setDefaultValues(prev => ({ ...prev, dateDebut }))
    setOpenModalTache(!openModalTache);
  };

  const getProject = useQuery<PROJECT, PROJECTVariables>(GET_PROJECT, {
    variables: { id: filterId },
    skip: !pathname.includes('todo/projet/'),
  });

  const getEtiquette = useQuery<ETIQUETTE, ETIQUETTEVariables>(GET_ETIQUETTE, {
    variables: { id: filterId },
    skip: !pathname.includes('todo/etiquette/'),
  });

  const projectInfo = getProject && getProject.data && getProject.data.project;
  const etiquetteName =
    (getEtiquette &&
      getEtiquette.data &&
      getEtiquette.data.todoEtiquette &&
      getEtiquette.data.todoEtiquette.nom) ||
    '';

  const title = isTodayOnly
    ? "Aujourd'hui"
    : isOnAujourhduiEquipe
      ? "Aujourd'hui de l'équipe"
      : isOnReceptionEquipe
        /*? "Boîte de réception de l'équipe"
        : isOnReception
        ? 'Boîte de réception'
        : isOnSeptJours
        */
        ? capitalizeFirstLetter(moment().format('MMMM YYYY'))
        : isOnSeptJoursEquipe
          ? `${capitalizeFirstLetter(moment().format('MMMM YYYY'))} (Equipe)`
          : isOnProject
            ? projectInfo && projectInfo._name
            : isEtiquette
              ? etiquetteName
              : withoutDate
                ? 'Sans date'
                : isAllSee
                  ? 'Tout voir'
                  : 'Boîte de réception';

  const optionBtn = (
    <ActionButton
      actionMenu="HEADER_MENU"
      setTriage={setTriage}
      refetchSection={refetchSection}
      setCacheTacheAchevE={setCacheTacheAchevE}
      cacheTacheAchevE={cacheTacheAchevE}
      statutMainHeader={statutMainHeader}
      refetchCountTodos={refetchCountTodos}
      parameters={parameters}
      refetchAll={refetchAll}
    />
  );

  return (
    <Box className={classes.root}>
      <MobileTopBar
        title={title}
        withBackBtn={true}
        handleDrawerToggle={handleDrawerToggle}
        optionBtn={[optionBtn]}
      />
      <main className={classes.content}>
        <MainHeader
          actions={actions?.search?.data}
          title={title}
          row={projectInfo}
          withDate={isOnAujourdhui ? true : isOnAujourhduiEquipe ? true : false}
          handleSetOpenModalTache={handleSetOpenModalTache}
          onChangeOffsetFromNow={setNextTaskOffsetFromNow}
          participants={
            projectInfo && projectInfo.participants ? projectInfo.participants.length : 0
          }
          setTriage={setTriage}
          refetchSection={refetchSection}
          setCacheTacheAchevE={setCacheTacheAchevE}
          cacheTacheAchevE={cacheTacheAchevE}
          statutMainHeader={statutMainHeader}
          refetchCountTodos={refetchCountTodos}
          parameters={parameters}
          refetchAll={refetchAll}
          useMatriceResponsabilite={useMatriceResponsabilite}
        />
        <Section
          filters={filters}
          actionListFilters={actionListFilters}
          triage={triage}
          sections={sectionResult}
          refetchSection={refetchSection}
          cacheTacheAchevE={cacheTacheAchevE}
          setStatutMainHeader={setStatutMainHeader}
          refetchCountTodos={refetchCountTodos}
          refetchEtiquette={refetchEtiquette}
          refetchProject={refetchProject}
          parameters={parameters}
          data={isOnSeptJours || isOnSeptJoursEquipe ? { ...(actions || {}), search: { data: filterActionsByOffsetFromNow(actions?.search?.data, nextTaskOffsetFromNow) } } : actions}
          loadingTask={loadingTask}
          refetchTask={refetchTask}
          refetchAll={refetchAll}
        />

        <AjoutTaches
          defaultValues={defaultValues}
          queryVariables={taskListVariables}
          openModalTache={openModalTache}
          setOpenTache={handleSetOpenModalTache}
          refetchCountTodos={refetchCountTodos}
          currentProjectId={currentProjectId}
          refetchTasks={refetchTasks}
          refetchEtiquette={refetchEtiquette}
          refetchProject={refetchProject}
          refetchAll={refetchAll}
        />
      </main>
    </Box>
  );
};

export default withRouter(MainContent);
