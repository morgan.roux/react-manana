import { RouteComponentProps, withRouter } from 'react-router';
import { Box, Popover } from '@material-ui/core';
import React, { useState } from 'react';
import { useApolloClient, useMutation } from '@apollo/client';
import { ME_me } from '../../../../../../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../../../../../../services/LocalStorage';
import { CustomDatePickerHiddenInput } from '../../../../../../../../Common/CustomDateTimePicker';
import moment from 'moment';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { DO_CREATE_ACTION } from '../../../../../../../../../graphql/Action';
import {
  CREATE_ACTION,
  CREATE_ACTIONVariables,
} from '../../../../../../../../../graphql/Action/types/CREATE_ACTION';
import { displaySnackBar } from '../../../../../../../../../utils/snackBarUtils';

interface TaskContentPopoverProps {
  open: boolean;
  id: string | undefined;
  anchorEl: any | null;
  section: string | undefined;
  task: any;
  setOpen: (value: boolean) => void;
  refetchAll?: any;
}

const TaskContentPopover: React.FC<TaskContentPopoverProps & RouteComponentProps> = ({
  id,
  open,
  anchorEl,
  section,
  task,
  setOpen,
  refetchAll,
}) => {
  //  const classes = useStyles({});

  const user: ME_me = getUser();
  //const idUser = (user && user.id) || '';

  const defaultCheckeds: string[] =
    (task && task.etiquettes && task.etiquettes.map(i => i.id)) || [];

  const [checkeds, setCheckeds] = useState<string[]>(defaultCheckeds);

  // const [openDatePicker, setOpenDatePicker] = useState<boolean>(false);

  const [values, setValues] = useState<any>(task);

  const client = useApolloClient();

  /*
  const getEtiquettes = useQuery<
    ETIQUETTES_BY_USER_WITH_MIN_INFO,
    ETIQUETTES_BY_USER_WITH_MIN_INFOVariables
  >(GET_ETIQUETTES_BY_USER_WITH_MIN_INFO, {
    fetchPolicy: 'cache-and-network',
    variables: { idUser, isRemoved: false },
  });

  const listEtiquette =
    (getEtiquettes && getEtiquettes.data && getEtiquettes.data.todoEtiquettesByUser) || [];
*/
  /*
const priorityList = [
  { id: 1, nom: 'Priorité 1' },
  { id: 2, nom: 'Priorité 2' },
  { id: 3, nom: 'Priorité 3' },
  { id: 4, nom: 'Priorité 4' },
];
*/

  const [createAndUpdateAction, { loading: addTaskLoading }] = useMutation<
    CREATE_ACTION,
    CREATE_ACTIONVariables
  >(DO_CREATE_ACTION, {
    onCompleted: data => {
      if (data && data.createUpdateAction) {
        // ! IMPORTANT
        /* if (refetchTasks) refetchTasks(); */
        if (refetchAll) refetchAll();
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: values.id
            ? 'Modification effectuée avec succès'
            : ' Création effectuée avec succès',
        });
      }
    },

    onError: error => {
      console.log('error :>> ', error);
      displaySnackBar(client, { isOpen: true, type: 'ERROR', message: error.message });
    },
  });

  const onChange = (event: any) => {
    event.stopPropagation();
    const { value, name } = event.target;
    const { idProject, dateDebut, id, idSection } = values;
    setValues(prevState => ({ ...prevState, [name]: value }));

    createAndUpdateAction({
      variables: {
        input: {
          ...values,
          idProject: idProject
            ? idProject === 'INBOXTEAM' || idProject === 'INBOX'
              ? ''
              : idProject
            : '',
          id,
          idSection,
          isInInbox: idProject && idProject === 'INBOX' ? true : false,
          isInInboxTeam: idProject && idProject === 'INBOXTEAM' ? true : false,
          idsEtiquettes: checkeds,
          dateDebut: dateDebut,
          codeItem: 'TODO',
        },
      },
    });
    setOpen(false);
  };

  const onDateChange = (date: MaterialUiPickersDate, value?: string | null | undefined) => {
    setValues(prevState => ({ ...prevState, dateDebut: moment(date).format() }));
  };

  return (
    <Popover id={id} open={open} anchorEl={anchorEl}>
      {section === 'DATE' && (
        <Box>
          <CustomDatePickerHiddenInput
            //open={openDatePicker}
            name="dateDebut"
            variant="static"
            onChange={onDateChange}
            value={values.dateDebut}
            autoOk={true}
            TextFieldComponent={() => null}
            autoFocus={true}
          />
        </Box>
      )}
      {/*section === 'PRIORITY' && (
        <Box className={classes.priorityRoot}>
          <CustumSelectTachePriorite
            label="Priorité"
            list={priorityList}
            name="priority"
            onChange={onChange}
            listId="id"
            index="nom"
            value={values.priority}
            autoFocus={true}
          />
        </Box>
      )*/}
      {/*section === 'ETIQUETTE' && (
        <Box className={classes.etiquetteRoot}>
          <CustomSelectTask
            label="Etiquette"
            list={listEtiquette}
            name="idsEtiquette"
            onChange={onChange}
            listId="id"
            index="nom"
            checkeds={checkeds}
            setCheckeds={setCheckeds}
            value={values && values.etiquettes && values.etiquettes.map(etiquette => etiquette.id)}
            autoFocus={true}
          />
        </Box>
      )*/}
    </Popover>
  );
};

export default withRouter(TaskContentPopover);
