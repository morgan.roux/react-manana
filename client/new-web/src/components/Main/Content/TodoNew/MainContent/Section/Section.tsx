// ! IMPORTANT : ATTENTION AU REBASE

import { useApolloClient, useMutation } from '@apollo/client';
import DateFnsUtils from '@date-io/date-fns';
import { Box, CircularProgress, Typography } from '@material-ui/core';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import localization from 'date-fns/locale/fr';
import moment from 'moment';
import React, { FC, Fragment } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import InboxImg from '../../../../../../assets/img/Todo/alerte.svg';
import TodayEmptyImg from '../../../../../../assets/img/Todo/aujourdhui.svg';
import TodayTeamEmptyImg from '../../../../../../assets/img/Todo/aujourdhui_equipe.svg';
import InboxTeamImg from '../../../../../../assets/img/Todo/autres.svg';
import { DO_UPDATE_ACTION_DATE_DEBUT_FINS } from '../../../../../../graphql/Action/mutation';
import {
  UPDATE_ACTION_DATE_DEBUT_FINS,
  UPDATE_ACTION_DATE_DEBUT_FINSVariables,
} from '../../../../../../graphql/Action/types/UPDATE_ACTION_DATE_DEBUT_FINS';
import { ME_me } from '../../../../../../graphql/Authentication/types/ME';
import { getGroupement, getUser } from '../../../../../../services/LocalStorage';
import { UpdateActionDateDebutFinInput } from '../../../../../../types/graphql-global-types';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import Backdrop from '../../../../../Common/Backdrop';
import NoItemContentImage from '../../../../../Common/NoItemContentImage';
import { CountTodosProps } from '../../TodoNew';
import SectionActionButton from './SectionActionButton';
import useStyles from './styles';
import TaskList from './TaskList/TaskList';

interface DateInterface {
  dateDebut: Date | null;
  dateFin: Date | null;
}

interface SectionProps {
  triage?: any[];
  actionListFilters: any;
  filters: any;
  match: {
    params: {
      filter: string | undefined;
      filterId: string | undefined;
    };
  };
  sections: any;
  refetchSection: any;
  cacheTacheAchevE?: boolean;
  setStatutMainHeader?: (test: boolean) => void;
  refetchEtiquette?: any;
  refetchProject?: any;
  parameters: any;
  data: any;
  refetchTask?: any;
  loadingTask?: any;
  refetchAll?: () => void;
}

const Section: FC<SectionProps & RouteComponentProps & CountTodosProps> = ({
  match: {
    params: { filter, filterId },
  },
  location: { pathname },
  filters,
  actionListFilters,
  triage,
  sections,
  refetchSection,
  cacheTacheAchevE,
  setStatutMainHeader,
  refetchCountTodos,
  refetchEtiquette,
  refetchProject,
  parameters,
  data,
  loadingTask,
  refetchTask,
  refetchAll,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [openDate, setOpenDate] = React.useState<boolean>(false);
  const [date, setDate] = React.useState<DateInterface>({ dateDebut: null, dateFin: null });

  moment.locale('fr');
  const sevenDays = moment()
    .add(7, 'days')
    .startOf('day')
    .format('YYYY-MM-DD');

  const me: ME_me = getUser();
  const groupement = getGroupement();
  const myId = (me && me.id) || '';

  const Boxision = [
    {
      id: 1,
      sectionTitle: 'En retard',
    },
    {
      id: 2,
      sectionTitle: "Aujourd'hui",
    },
  ];

  const isOnNextWeek = filter === 'sept-jours';
  const useActionListFilters = isOnNextWeek;
  const isOnNextWeekTeam = filter === 'sept-jours-equipe';

  const isOnSevenDays: boolean =
    pathname.includes('/sept-jours') && !pathname.includes('/sept-jours-equipe');
  const isOnSeptJoursEquipe: boolean = pathname.includes('/sept-jours-equipe');

  const isInbox = pathname.includes('recu') && !pathname.includes('recu-equipe');
  const isInboxTeam = pathname.includes('recu-equipe');
  const isToday = pathname.includes('aujourdhui');
  const isTodayOnly = pathname.includes('aujourdhui') && !pathname.includes('aujourdhui-equipe');
  const isTodayTeam = pathname.includes('aujourdhui-equipe');
  const isProject = pathname.includes('projet');
  const isEtiquette = pathname.includes('etiquette');
  const withoutDate: boolean = pathname.includes('sans-date');
  const isAllSee: boolean = pathname.includes('/tout-voir');

  if (setStatutMainHeader) {
    if (isProject) {
      setStatutMainHeader(true);
    } else {
      setStatutMainHeader(false);
    }
  }

  const sectionIds = sections && sections.length ? sections.map(section => section.id) : [];

  const getSectionTask = (section: any) => {
    return (
      (data &&
        data.search &&
        data.search.data &&
        data.search.data.filter(task => task && task.section && task.section.id === section.id)) ||
      []
    );
  };

  const getTaskWithoutSection = () => {
    return (
      (data &&
        data.search &&
        data.search.data &&
        data.search.data.filter(
          task =>
            task && (!task.section || (task.section && !sectionIds.includes(task.section.id))),
        )) ||
      []
    );
  };

  const tasks = (data && data.search && data.search.data) || [];
  let overdueTasks: any[] = [];
  let todayTasks: any[] = [];

  if (isToday) {
    overdueTasks = tasks.filter((task: any) => {
      if (task.dateDebut) {
        const dateDebut = moment(task.dateDebut).format('YYYY-MM-DD');
        const now = moment().format('YYYY-MM-DD');
        return moment(dateDebut).isBefore(moment(now));
      }
    });
    todayTasks = tasks.filter((task: any) => {
      if (task.dateDebut) {
        const dateDebut = moment(task.dateDebut).format('YYYY-MM-DD');
        const now = moment().format('YYYY-MM-DD');
        return moment(dateDebut).isSame(moment(now));
      }
    });
  }

  // Mutation update tasks dates
  const [doUpdateActionDateDebutFins, { loading: uLoading }] = useMutation<
    UPDATE_ACTION_DATE_DEBUT_FINS,
    UPDATE_ACTION_DATE_DEBUT_FINSVariables
  >(DO_UPDATE_ACTION_DATE_DEBUT_FINS, {
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
    onCompleted: data => {
      if (data && data.updateActionDateDebutFins && refetchTask) {
        refetchTask();
      }
    },
  });

  const handleOpenDate = () => {
    setOpenDate(true);
  };

  const handleCloseDate = () => {
    setOpenDate(false);
  };

  const handleDateChange = (date: Date | null, name: string) => {
    setDate(prevState => ({ ...prevState, [name]: date }));
  };

  const onClickReport = (event: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
    event.preventDefault();
    event.stopPropagation();
    if (overdueTasks && overdueTasks.length > 0) handleOpenDate();
  };

  // Execute update many tasks
  React.useMemo(() => {
    const dateDebut = date.dateDebut;
    const dateFin = date.dateFin;
    if (dateDebut && dateFin && overdueTasks.length > 0) {
      // Update overdueTasks
      const inputs: UpdateActionDateDebutFinInput[] = [];
      overdueTasks.map(i => {
        const input: UpdateActionDateDebutFinInput = { idAction: i.id, dateDebut, dateFin };
        inputs.push(input);
      });
      doUpdateActionDateDebutFins({ variables: { inputs } });
    }
  }, [date]);

  const todayEmptyTxt = 'Un aperçu de la journée qui vous attend';
  const todayEmptyTxtSub = "Toutes les tâches dues aujourd'hui s'afficheront ici.";

  const todayTeamEmptyTxt = 'Un aperçu de la journée qui attend vos collègues';
  const todayTeamEmptyTxtSub = "Toutes les tâches dues aujourd'hui s'afficheront ici.";

  const inboxEmptyTxt = 'Terminé !';
  const inboxEmptyTxtSub = 'Il semble que tout soit organisé au bon endroit.';

  const inboxTeamEmptyTxt = 'Gardez vos tâches organisées par projet';
  const inboxTeamEmptyTxtSub =
    'Regroupez vos tâches par objectif et domaine. Glissez-dépossez les tâches pour les réorganiser ou créer des sous-tâches.';

  const emptyTitle = isTodayOnly
    ? todayEmptyTxt
    : isTodayTeam
      ? todayTeamEmptyTxt
      : isInbox
        ? inboxEmptyTxt
        : inboxTeamEmptyTxt;

  const emptySubTitle = isTodayOnly
    ? todayEmptyTxtSub
    : isTodayTeam
      ? todayTeamEmptyTxtSub
      : isInbox
        ? inboxEmptyTxtSub
        : inboxTeamEmptyTxtSub;

  const emptyImg = isTodayOnly
    ? TodayEmptyImg
    : isTodayTeam
      ? TodayTeamEmptyImg
      : isInbox
        ? InboxImg
        : InboxTeamImg;

  if (loadingTask)
    return (
      <div className={classes.loading}>
        <CircularProgress size={35} />
      </div>
    );

  return (
    <Box className={classes.root}>
      {uLoading && <Backdrop value={'Mise à jour en cours...'} />}

      <Fragment>
        {isToday && (
          <Fragment>
            {Boxision.map(section => (
              <Accordion defaultExpanded={true}>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography>{section.sectionTitle}</Typography>
                  {/*section.id === 1 && overdueTasks.length > 0 && (
                    <Typography onClick={onClickReport} className={classes.reporteText}>
                      Reporter
                    </Typography>
                  )*/}
                </AccordionSummary>
                <AccordionDetails>
                  <TaskList
                    tasks={section.id === 1 ? overdueTasks : todayTasks}
                    refetch={refetchTask}
                    refetchCountTodos={refetchCountTodos}
                    refetchEtiquette={refetchEtiquette}
                    refetchProject={refetchProject}
                    parameters={parameters}
                    refetchAll={refetchAll}
                  />
                </AccordionDetails>
              </Accordion>
            ))}
          </Fragment>
        )}
        {sections &&
          sections.map(section => (
            <Accordion defaultExpanded={true}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Box display="flex" alignItems="center">
                  <Typography className={classes.sectionLibelle}>{section.libelle}</Typography>
                  <Typography className={classes.nbrProduits}>{section.nbAction}</Typography>
                </Box>
                <SectionActionButton
                  section={section}
                  refetchSection={refetchSection}
                  refetchAll={refetchAll}
                />
              </AccordionSummary>
              <AccordionDetails>
                <TaskList
                  tasks={getSectionTask(section)}
                  refetch={refetchTask}
                  refetchCountTodos={refetchCountTodos}
                  refetchEtiquette={refetchEtiquette}
                  refetchProject={refetchProject}
                  parameters={parameters}
                  refetchAll={refetchAll}
                />
              </AccordionDetails>
            </Accordion>
          ))}
        {!isToday && (
          <TaskList
            tasks={getTaskWithoutSection()}
            refetch={refetchTask}
            refetchCountTodos={refetchCountTodos}
            refetchEtiquette={refetchEtiquette}
            refetchProject={refetchProject}
            parameters={parameters}
            refetchAll={refetchAll}
          />
        )}
      </Fragment>

      {(isTodayOnly || isTodayTeam || isInbox || isInboxTeam) &&
        tasks.length === 0 &&
        !loadingTask && (
          <div className={classes.emptyContainer}>
            <NoItemContentImage src={emptyImg} title={emptyTitle} subtitle={emptySubTitle} />
          </div>
        )}

      <MuiPickersUtilsProvider utils={DateFnsUtils} libInstance={moment} locale={localization}>
        <KeyboardDatePicker
          margin="normal"
          id="date-picker-dialog"
          label="Date picker dialog"
          format="dd/MM/yyyy"
          value={date.dateDebut}
          onChange={date => handleDateChange(date, 'dateDebut')}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
          autoOk={false}
          disableToolbar={true}
          open={openDate}
          onOpen={handleOpenDate}
          onClose={handleCloseDate}
          okLabel="Valider"
          cancelLabel="Annuler"
          disablePast={true}
        />
      </MuiPickersUtilsProvider>
      <MuiPickersUtilsProvider utils={DateFnsUtils} libInstance={moment} locale={localization}>
        <KeyboardDatePicker
          margin="normal"
          id="date-picker-dialog"
          label="Date picker dialog"
          format="dd/MM/yyyy"
          value={date.dateFin}
          onChange={date => handleDateChange(date, 'dateFin')}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
          autoOk={false}
          disableToolbar={true}
          open={openDate}
          onOpen={handleOpenDate}
          onClose={handleCloseDate}
          okLabel="Valider"
          cancelLabel="Annuler"
          disablePast={true}
        />
      </MuiPickersUtilsProvider>
    </Box>
  );
};

export default withRouter(Section);
