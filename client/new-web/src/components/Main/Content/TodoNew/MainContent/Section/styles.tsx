import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      flexWrap: 'nowrap',
      '& .MuiAccordion-root': {
        boxShadow: 'none!important',
        '&.Mui-expanded': {
          margin: '0',
        },
      },
      '& .MuiAccordionSummary-root': {
        flexDirection: 'row-reverse',
        padding: 0,
        '&.Mui-expanded': {
          minHeight: 45,
        },
        '& .MuiAccordionSummary-expandIcon': {
          padding: 0,
          marginRight: 10,
        },
      },
      '& .MuiAccordionSummary-content': {
        justifyContent: 'space-between',
        '&.Mui-expanded': {
          margin: '11px 0',
        },
        [theme.breakpoints.down('lg')]: {
          margin: '11px 16px 11px 0px !important',
        },
      },

      '& .MuiAccordionDetails-root': {
        padding: '0px 0px 0px 40px!important',
        '& .MuiList-root': {
          width: '100%',
          paddingTop: 0,
          paddingBottom: 0,
          '& .MuiListItem-button': {
            paddingRight: 0,
            paddingTop: 0,
            paddingBottom: 0,
            paddingLeft: 0,
            marginBottom: 16,
            '& .MuiIconButton-root': {
              padding: 0,
            },
          },
        },
        [theme.breakpoints.down('lg')]: {
         // padding: '0px 16px 0px 40px!important',
        },
      },
      // ! IMPORTANT : Hide date picker input
      '& .MuiFormControl-marginNormal': {
        display: 'none',
      },
      [theme.breakpoints.down('md')]: {
        paddingBottom: 16,
      },
    },
    date: {
      fontSize: 12,
      color: theme.palette.secondary.main,
    },
    labelText: {
      marginLeft: 8,
      fontSize: 12,
      lineHeight: '10px',
      fontFamily: 'Montserrat',
    },
    bold: {
      fontWeight: 600,
    },
    contentLabels: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'start',
    },
    labelOrigine: {
      fontFamily: 'Montserrat',
      fontSize: 12,
      textDecoration: 'underline',
      marginRight: 4,
    },
    rootButton: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      '& .MuiIconButton-root': {
        color: '#000000',
        padding: 0,
      },
      '& .MuiList-root': {
        width: '100%',
        paddingTop: 0,
        paddingBottom: 0,
        '& .MuiListItem-button': {
          paddingRight: 0,
          paddingTop: 0,
          paddingBottom: 0,
          paddingLeft: 0,
          marginBottom: 16,
        },
      },
    },
    labels: {
      display: 'flex',
      alignItems: 'center',
      marginRight: 28,
      marginBottom: 8,
    },
    taskTitle: {
      textAlign: 'left',
      fontFamily: 'Montserrat',
      color: '#1D1D1D',
      opacity: 1,
    },
    addPersonButton: {
      marginRight: 16,
    },
    tasks: {
      borderRadius: 5,
      boxShadow: 'none',
      width: '100%',
      padding: 15,
      border: '1px solid #E3E3E3',
      display: 'flex',
      alignItems: 'start',
      '& .MuiCheckbox-root': {
        marginRight: 15,
        padding: 0,
      },
    },
    subTasks: {
      borderRadius: 5,
      boxShadow: 'none',
      width: '100%',
      padding: 15,
      border: '1px solid #E3E3E3',
      display: 'flex',
      alignItems: 'start',
      marginLeft: 30,
      '& .MuiCheckbox-root': {
        marginRight: 15,
        padding: 0,
      },
    },
    list: {
      width: '100%',
      padding: '0px',
      marginBottom: 16,
    },
    reporteText: {
      fontSize: 14,
      fontFamily: 'Roboto',
      fontWeight: 500,
      letterSpacing: 0,
      textDecoration: 'underline',
      color: theme.palette.secondary.main,
      cursor: 'pointer',
      '&:hover': {
        textDecoration: 'underline',
      },
    },
    taskDetailsModal: {
      '& .MuiDialog-paperWidthMd': {
        maxWidth: '762px !important',
        minHeight: '700px !important',
      },
      '& .MuiListItemIcon-root': {
        minWidth: '35px !important',
      },
    },
    emptyContainer: {
      width: '100%',
      minHeight: 'calc(100vh - 327px)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      [theme.breakpoints.down('md')]: {},
    },
    sectionLibelle: {
      fontFamily: 'Roboto',
      fontSize: 14,
      fontWeight: 500,
    },
    nbrProduits: {
      padding: '2px 6px',
      fontSize: 10,
      background: '#E0E0E0',
      borderRadius: 2,
      height: 20,
      minWidth: 20,
      textAlign: 'center',
      marginLeft: 10,
    },
    loading: {
      flex: 1,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
  }),
);

export default useStyles;
