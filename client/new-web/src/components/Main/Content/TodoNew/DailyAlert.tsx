import { useQuery } from '@apollo/client';
import {
  Box,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  IconButton,
  List,
  ListItem,
  Typography,
} from '@material-ui/core';
import { Close } from '@material-ui/icons';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import AlertImage from '../../../../assets/img/Todo/alerte.svg';
import { GET_ME } from '../../../../graphql/Authentication/query';
import { ME } from '../../../../graphql/Authentication/types/ME';
import { GET_SEARCH_ACTION } from '../../../../graphql/Action/query';
import CustomButton from '../../../Common/CustomButton';
import useStyles from './styles';
import moment from 'moment';
import { getQueryParameter } from './FilterSearch';
interface DailyAlertProps {
  openDailyAlert: boolean;
  handleCloseDailyAlert: () => any;
  resultAction?: any;
}

const DailyAlert: React.FC<RouteComponentProps & DailyAlertProps> = props => {
  const {
    openDailyAlert,
    handleCloseDailyAlert,
    history: { push },
    location: { pathname },
    resultAction,
  } = props;

  const classes = useStyles({});
  const me = useQuery<ME>(GET_ME);
  const resultMe = me && me.data && me.data.me;
  const isAssignedTerms = (resultMe && resultMe.id) || '';
  const firstName =
    me && me.data && me.data.me && me.data.me.userName && me.data.me.userName.split(' ')[0]
      ? me.data.me.userName?.split(' ')[0]
      : me && me.data && me.data.me
        ? me.data.me.userName
        : '';


  const searchAction = useQuery<any>(GET_SEARCH_ACTION, {
    variables: {
      filterBy: [{ term: { isRemoved: false } }],
      query: getQueryParameter(isAssignedTerms),
    },
    skip:
      pathname !== '/' ||
      (resultAction &&
        resultAction.data &&
        resultAction.data.search &&
        resultAction.data.search.data &&
        resultAction.data.search.data.length),
  });

  const result: any = resultAction ? resultAction : searchAction;

  const resultData: any[] =
    (result &&
      result.data &&
      result.data.search &&
      result.data.search.data &&
      result.data.search.data) ||
    [];

  const retard = resultData.filter((task: any) => {
    if (task.dateDebut) {
      const dateDebut = moment(task.dateDebut).format('YYYY-MM-DD');
      const now = moment().format('YYYY-MM-DD');
      return moment(dateDebut).isBefore(moment(now));
    }
  }).length;

  const aujourdhui = resultData.filter((task: any) => {
    if (task.dateDebut) {
      const dateDebut = moment(task.dateDebut).format('YYYY-MM-DD');
      const now = moment().format('YYYY-MM-DD');
      return moment(dateDebut).isSame(moment(now));
    }
  }).length;

  const gotoTodo = () => {
    handleCloseDailyAlert();
    push('/todo/aujourdhui');
  };

  return (
    <Dialog
      open={openDailyAlert}
      onClose={handleCloseDailyAlert}
      className={classes.rootDialog}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      keepMounted
    >
      <DialogContent>
        <Box display="flex" position="relative" flexDirection="column" alignItems="center">
          <IconButton className={classes.iconContentButton} onClick={handleCloseDailyAlert}>
            <Close />
          </IconButton>
          <Box
            display="flex"
            alignItems="flex-start"
            justifyContent="center"
            className={classes.alertModalImages}
          >
            <img src={AlertImage} />
          </Box>
          <Box>
            <Typography className={classes.alertModalTitle}>Bonjour {firstName || '!'}</Typography>
            <Typography color="textSecondary">
              D-TODO vous donne un aperçu des actions de votre journée. Suivez et gérez en toute
              simplicité le déroulement de vos projets.
            </Typography>
            <List component="nav" aria-label="main mailbox folders">
              <ListItem>
                <FiberManualRecordIcon fontSize="small" color="disabled" />
                <Typography color="textSecondary">
                  {aujourdhui <= 1 ? 'Action journalière' : 'Actions journalière'}:
                </Typography>
                <Typography className={classes.label} color="secondary">
                  {result && result.loading && <CircularProgress size={15} />}
                  {result && !result.loading && aujourdhui}
                </Typography>
              </ListItem>
              <ListItem>
                <FiberManualRecordIcon fontSize="small" color="disabled" />
                <Typography color="textSecondary">
                  {retard <= 1 ? 'Action en retard' : 'Actions en retard'}:
                </Typography>
                <Typography className={classes.label} color="secondary">
                  {result && result.loading && <CircularProgress size={15} />}
                  {result && !result.loading && retard}
                </Typography>
              </ListItem>
            </List>
          </Box>
        </Box>
      </DialogContent>

      <DialogActions className={classes.alertModalButton}>
        <CustomButton color="secondary" onClick={gotoTodo}>
          Acceder à to do
        </CustomButton>
      </DialogActions>
    </Dialog>
  );
};

export default withRouter(DailyAlert);
