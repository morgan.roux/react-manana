import React, { FC, useState, useEffect } from 'react';
import useStyles from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import {
  Box,
  Typography,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Checkbox,
  IconButton,
  Radio,
  CssBaseline,
  Hidden,
} from '@material-ui/core';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';

interface ContentProps {
  title: string;
  datas: any;
  updateDatas: (list: any) => void;
  useRadios?: boolean;
}
const Content: FC<ContentProps & RouteComponentProps<any, any, any>> = ({
  title,
  datas,
  updateDatas,
  useRadios,
}) => {
  const classes = useStyles({});
  const [expandedMore, setExpandedMore] = useState<boolean>(true);

  const onGetValue = (itemIndex: number) => {
    const newList = datas.map((item: any, index) => {
      if (itemIndex === index) {
        item.checked = !item.checked;
        return item;
      }
      return item;
    });

    updateDatas(newList);
  };

  const onGetRadioValue = (itemIndex: number) => {
    const newList = datas.map((item: any, index) => {
      if (itemIndex === index) {
        item.checked = true;
        return item;
      } else {
        item.checked = false;
        return item;
      }
    });
    updateDatas(newList);
  };

  return (
    <Box borderTop="1px solid #E3E3E3" padding="16px 0" overflow="auto">
      <CssBaseline />
      {/* <Hidden mdDown={true}>
        {' '}
        <Box
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          marginBottom="8px"
          onClick={() => setExpandedMore(!expandedMore)}
        >
          <Typography className={classes.name}>{title}</Typography>
          <IconButton size="small" className={classes.expandBtnContainer}>
            {expandedMore ? <ExpandLess /> : <ExpandMore />}
          </IconButton>
        </Box>
      </Hidden> */}

      {useRadios ? (
        <Box className={classes.noStyle}>
          {datas.map(
            (item: any, index) =>
              item && (
                <ListItem
                  role={undefined}
                  dense={true}
                  button={true}
                  key={`${index}-${item.nom}`}
                  onClick={() => onGetRadioValue(index)}
                >
                  <Box display="flex" alignItems="center">
                    <Radio checked={item.checked} tabIndex={-1} disableRipple={true} />
                    <ListItemText primary={item.nom} />
                  </Box>
                  {
                    <Box>
                      <Typography className={classes.nbrProduits}>{item.count}</Typography>
                    </Box>
                  }
                </ListItem>
              ),
          )}
        </Box>
      ) : (
        <Box className={classes.noStyle}>
          {expandedMore &&
            datas.map(
              (item: any, index) =>
                item &&
                !item.hidden && (
                  <ListItem
                    role={undefined}
                    dense={true}
                    button={true}
                    onClick={() => (!item.disable ? onGetValue(index) : {})}
                    key={`${index}-${item.nom}`}
                    disabled={!item.disable ? false : true}
                  >
                    <Box className={classes.checkBoxLeftName} display="flex" alignItems="center">
                      <Checkbox
                        tabIndex={-1}
                        checked={item.checked}
                        disableRipple={true}
                        disabled={!item.disable ? false : true}
                      />
                      <ListItemText className={classes.nom} primary={item.nom} />
                    </Box>

                    {
                      <Box>
                        <Typography className={classes.nbrProduits}>{item.count}</Typography>
                      </Box>
                    }
                  </ListItem>
                ),
            )}
        </Box>
      )}
    </Box>
  );
};
export default withRouter(Content);
