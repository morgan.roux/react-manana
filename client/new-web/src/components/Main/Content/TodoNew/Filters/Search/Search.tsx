import React, { FC, useRef, useState, useEffect, ReactNode } from 'react';
import { useStyles } from './style';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import { useApolloClient, useLazyQuery, useMutation } from '@apollo/client';
import {
  SEARCH_ACTION,
  SEARCH_ACTIONVariables,
  SEARCH_ACTION_search_data_Action,
} from '../../../../../../graphql/Action/types/SEARCH_ACTION';
import { DO_SOFT_DELETE_ACTIONS, GET_SEARCH_ACTION } from '../../../../../../graphql/Action';
import { GET_UPDATE_ITEM_SCORING_FOR_ITEM } from '../../../../../../federation/tools/itemScoring/query';
import {
  UPDATE_ITEM_SCORING_FOR_ITEM as UPDATE_ITEM_SCORING_FOR_ITEM_TYPE,
  UPDATE_ITEM_SCORING_FOR_ITEMVariables,
} from '../../../../../../federation/tools/itemScoring/types/UPDATE_ITEM_SCORING_FOR_ITEM';
import { debounce } from 'lodash';
import Autocomplete from '../../../Messagerie/SideBar/FormMessage/Autocomplete';
import { CustomModal } from '../../../../../Common/CustomModal';
import {
  SOFT_DELETE_ACTIONS,
  SOFT_DELETE_ACTIONSVariables,
} from '../../../../../../graphql/Action/types/SOFT_DELETE_ACTIONS';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import TaskDetails from '../../Task/TaskDetails';
import { getGroupement } from '../../../../../../services/LocalStorage';
import AjoutTaches from '../../Modals/AjoutTache';
import { isMobile } from '../../../../../../utils/Helpers';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';

interface SearchContentProps {
  placeholder: string;
  refetchAll?: () => void;
  popupIcon?: ReactNode;
}

const SearchContent: FC<SearchContentProps> = ({ placeholder, refetchAll, popupIcon }) => {
  const classes = useStyles({});
  const client = useApolloClient();

  const [todos, setTodos] = React.useState<any[]>([]);
  const [todo, setTodo] = React.useState<any[]>([]);

  const [openModalTache, setOpenModalTache] = useState<boolean>(false);

  const [open, setOpen] = React.useState<boolean>(false);
  const [clickedTask, setClickedTask] = React.useState<SEARCH_ACTION_search_data_Action | null>(
    null,
  );

  const groupement = getGroupement();

  const [doSearchAction, resultActions] = useLazyQuery<any, SEARCH_ACTIONVariables>(
    GET_SEARCH_ACTION,
    {
      fetchPolicy: 'cache-and-network',
    },
  );

  React.useEffect(() => {
    if (
      resultActions &&
      resultActions.data &&
      resultActions.data.search &&
      resultActions.data.search.data &&
      resultActions.data.search.data.length
    ) {
      // Execute query get message
      setTodos(resultActions.data.search.data);
    }
  }, [resultActions]);

  const debouncedSearchUser = useRef(
    debounce((text: string) => {
      if (!text) setTodos([]);
      else
        doSearchAction({
          variables: {
            type: ['action'],
            query: {
              query: {
                bool: {
                  must: [
                    {
                      query_string: {
                        query: text.startsWith('*') || text.endsWith('*') ? text : `*${text}*`,
                        analyze_wildcard: true,
                      },
                    },
                    {
                      term: { isRemoved: false },
                    },
                  ],
                  should: [
                    { term: { 'assignedUsers.idGroupement': groupement && groupement.id } },

                    { term: { 'userCreation.idGroupement': groupement && groupement.id } },
                  ],
                  minimum_should_match: 1,
                },
              },
            },
          },
        });
    }, 1000),
  );

  const handleChangeAutocomplete = (name: string) => (e: React.ChangeEvent<any>, value: any) => {
    debouncedSearchUser.current(value);
  };

  const handleChange = (name: string) => (e: React.ChangeEvent<any>, value: any) => {
    if (value) {
      setClickedTask(value);
      if (isMobile()) {
        setOpenModalTache(true);
      } else {
        setOpen(true);
      }
    }
  };

  const [updateItemScoring, updatingItemScoring] = useLazyQuery<
    UPDATE_ITEM_SCORING_FOR_ITEM_TYPE,
    UPDATE_ITEM_SCORING_FOR_ITEMVariables
  >(GET_UPDATE_ITEM_SCORING_FOR_ITEM, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
  });

  // Mutation delete tasks
  const [doSoftDeleteActions, doingSoftDeleteActions] = useMutation<
    SOFT_DELETE_ACTIONS,
    SOFT_DELETE_ACTIONSVariables
  >(DO_SOFT_DELETE_ACTIONS, {
    onCompleted: data => {
      if (data && data.softDeleteActions) {
        updateItemScoring({
          variables: {
            idItemAssocie: data.softDeleteActions[0].id,
            codeItem: 'L',
          },
        });
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Suppression effectuée avec succès !!!',
        });
      }
    },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  return (
    <>
      <Box className={classes.search}>
        <Autocomplete
          value={''}
          name="todo"
          multiple={false}
          variant="outlined"
          handleChange={handleChange}
          handleInputChange={handleChangeAutocomplete}
          options={todos}
          optionLabel="description"
          placeholder={placeholder}
          style={{ width: '100%' }}
          noOptionsText={'Aucune tâche correspondant'}
          popupIcon={popupIcon}
          popupIndicatorOpen={classes.popupIndicatorOpen}
        />
      </Box>
      <AjoutTaches
        openModalTache={openModalTache}
        setOpenTache={setOpenModalTache}
        task={clickedTask}
        operationName="EDIT"
        refetchAll={refetchAll}
      />
      {open && (
        <CustomModal
          open={open}
          setOpen={setOpen}
          title="Détails de tâche"
          withBtnsActions={false}
          closeIcon={true}
          headerWithBgColor={true}
          maxWidth="md"
          fullWidth={true}
          className={classes.taskDetailsModal}
        >
          {clickedTask && clickedTask.id && (
            <TaskDetails
              task={clickedTask}
              doSoftDeleteActions={doSoftDeleteActions}
              refetchAll={refetchAll}
            />
          )}
        </CustomModal>
      )}
    </>
  );
};

export default SearchContent;
