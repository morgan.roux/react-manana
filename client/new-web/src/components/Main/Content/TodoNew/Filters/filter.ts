import { PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories } from '../../../../../graphql/Parametre/types/PARAMETERS_GROUPES_CATEGORIES';

export const hiddenTypeProject = (
  codeFilter: string,
  parameters: PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories[],
) => {
  let codePersonnel;

  switch (codeFilter) {
    case 'PERSONNEL':
      codePersonnel = '0118';
      break;
    case 'GROUPEMENT':
      codePersonnel = '0119';
      break;
  }

  const parameter = parameters.find((parameter) => parameter?.code === codePersonnel);


  return !('true' === parameter?.value?.value ? true : 'true' === parameter?.defaultValue);
  /*
  return parameters.reduce((results, parameter) => {
    if (
      parameter &&
      parameter.code &&
      codePersonnels.includes(parameter.code) &&
      (parameter.value && parameter.value.id
        ? parameter.value.value === 'true'
        : parameter.defaultValue === 'true')
    )
      return [...results, parameter];

    if (
      parameter &&
      parameter.code &&
      codeGroupements.includes(parameter.code) &&
      (parameter.value && parameter.value.id
        ? parameter.value.value === 'true'
        : parameter.defaultValue === 'true')
    )
      return [...results, parameter];

    return results;
  }, [] as any[]).length > 1
    ? false
    : true;
    */
};
