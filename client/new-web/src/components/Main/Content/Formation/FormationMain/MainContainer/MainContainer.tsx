import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import { Box, IconButton, Menu, MenuItem, Typography } from '@material-ui/core';
import { DeleteForever, Edit, MoreHoriz } from '@material-ui/icons';
import React, { FC, Fragment } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { PARTAGE_IDEE_BONNE_PRATIQUE_URL } from '../../../../../../Constant/url';
import { GET_ME } from '../../../../../../graphql/Authentication/query';
import { ME } from '../../../../../../graphql/Authentication/types/ME';
import {
  DO_DELETE_IDEE_BONNE_PRATIQUE,
  GET_IDEE_BONNE_PRATIQUE,
} from '../../../../../../graphql/IdeeBonnePratique';
import {
  DELETE_IDEE_BONNE_PRATIQUE,
  DELETE_IDEE_BONNE_PRATIQUEVariables,
} from '../../../../../../graphql/IdeeBonnePratique/types/DELETE_IDEE_BONNE_PRATIQUE';
import {
  IDEE_BONNE_PRATIQUE,
  IDEE_BONNE_PRATIQUEVariables,
} from '../../../../../../graphql/IdeeBonnePratique/types/IDEE_BONNE_PRATIQUE';
import { nl2br } from '../../../../../../services/Html';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import Backdrop from '../../../../../Common/Backdrop';
import RightContentComment from '../../../../../Common/MainContainer/TwoColumnsContainer/RightContentContainer/RightContentComment';
import { ActiveItemProps } from '../LeftContainer/Item/Item';
import useStyles from './styles';

const MainContainer: FC<RouteComponentProps & ActiveItemProps> = ({
  history: { push },
  activeItem,
  setActiveItem,
}) => {
  const classes = useStyles({});
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClickMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const client = useApolloClient();

  const id = (activeItem && activeItem.id) || '';

  const [getIdeeOuBonnePratique, { data, loading, refetch }] = useLazyQuery<
    IDEE_BONNE_PRATIQUE,
    IDEE_BONNE_PRATIQUEVariables
  >(GET_IDEE_BONNE_PRATIQUE, {
    fetchPolicy: 'cache-and-network',
    onCompleted: data => {
      if (data && data.ideeOuBonnePratique) {
        if (!activeItem || (activeItem && activeItem.id !== data.ideeOuBonnePratique)) {
          // Set activeItem
          setActiveItem(data.ideeOuBonnePratique);
        }
      }
    },
    onError: error => {
      console.log('error :>> ', error);
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const [doDeleteIdee, { data: datadeleted, loading: deleteLoading }] = useMutation<
    DELETE_IDEE_BONNE_PRATIQUE,
    DELETE_IDEE_BONNE_PRATIQUEVariables
  >(DO_DELETE_IDEE_BONNE_PRATIQUE, {
    onCompleted: data => refetch(),
  });

  // Execute query on component mounted
  React.useEffect(() => {
    const pathnameTabs = window.location.hash.substr(2).split('/');
    if (pathnameTabs.length === 3) {
      const idFromPathname = pathnameTabs.pop();
      if (idFromPathname) {
        getIdeeOuBonnePratique({ variables: { id: idFromPathname } });
      }
    }
  }, []);

  // Execute query on activeItem change
  React.useMemo(() => {
    if (
      activeItem &&
      activeItem.id &&
      window.location.hash === `#${PARTAGE_IDEE_BONNE_PRATIQUE_URL}/${id}`
    ) {
      getIdeeOuBonnePratique({ variables: { id: activeItem.id } });
    }
  }, [activeItem]);

  const ideeOuBonnePratique = data && data.ideeOuBonnePratique;

  const handleClickAdd = () => push('/db/partage-idee/create');

  const handleClickEdit = () => push(`db/partage-idee/edit/${id}`);

  const handleClickDelete = () => doDeleteIdee({ variables: { ids: [id] } });
  const me = useQuery<ME>(GET_ME);
  const resultMe = me && me.data && me.data.me;

  const isAuthorizedSettigs: Boolean =
    resultMe?.role?.code === ('SUPADM' || 'GRPADM') ? true : false;
  return (
    <Box className={classes.mainContainer}>
      {(loading || deleteLoading) && <Backdrop />}

      <Box style={{ height: 'calc(100vh - 162px)', overflowY: 'auto' }}>
        {ideeOuBonnePratique ? (
          <Box className={classes.detailsContainer}>
            <Box display="flex" justifyContent="space-between" alignItems="center">
              {ideeOuBonnePratique._origine && (
                <Typography className={classes.title}>{ideeOuBonnePratique._origine}</Typography>
              )}
              <Box className={classes.moreContainer}>
                <IconButton size="small" onClick={handleClickMenu}>
                  <MoreHoriz />
                </IconButton>
                <Menu
                  id="header-simple-menu"
                  anchorEl={anchorEl}
                  keepMounted={true}
                  open={Boolean(anchorEl)}
                  onClose={handleClose}
                >
                  <MenuItem onClick={handleClickEdit}>
                    <Edit /> Modifier
                  </MenuItem>
                  <MenuItem onClick={handleClickDelete}>
                    <DeleteForever /> Supprimer
                  </MenuItem>
                </Menu>
              </Box>
            </Box>
            <Box>
              {ideeOuBonnePratique.beneficiaires_cles && (
                <Fragment>
                  <Typography className={classes.labelTitle}>Bénéficiaires clés</Typography>
                  <Typography
                    className={classes.labelContent}
                    dangerouslySetInnerHTML={
                      { __html: nl2br(ideeOuBonnePratique.beneficiaires_cles) } as any
                    }
                  />
                </Fragment>
              )}
            </Box>
            <Box>
              {ideeOuBonnePratique.contexte && (
                <Fragment>
                  <Typography className={classes.labelTitle}>Contextes</Typography>
                  <Typography
                    className={classes.labelContent}
                    dangerouslySetInnerHTML={{ __html: nl2br(ideeOuBonnePratique.contexte) } as any}
                  />
                </Fragment>
              )}
            </Box>
            <Box>
              {ideeOuBonnePratique.objectifs && (
                <Fragment>
                  <Typography className={classes.labelTitle}>Objectifs</Typography>
                  <Typography
                    className={classes.labelContent}
                    dangerouslySetInnerHTML={
                      { __html: nl2br(ideeOuBonnePratique.objectifs) } as any
                    }
                  />
                </Fragment>
              )}
            </Box>
            <Box>
              {ideeOuBonnePratique.resultats && (
                <Fragment>
                  <Typography className={classes.labelTitle}>Résultats</Typography>
                  <Typography
                    className={classes.labelContent}
                    dangerouslySetInnerHTML={
                      { __html: nl2br(ideeOuBonnePratique.resultats) } as any
                    }
                  />
                </Fragment>
              )}
            </Box>
            <Box>
              {ideeOuBonnePratique.facteursClesDeSucces && (
                <Fragment>
                  <Typography className={classes.labelTitle}>Facteurs clés de succès</Typography>
                  <Typography
                    className={classes.labelContent}
                    dangerouslySetInnerHTML={
                      { __html: nl2br(ideeOuBonnePratique.facteursClesDeSucces) } as any
                    }
                  />
                </Fragment>
              )}
            </Box>
            <Box>
              {ideeOuBonnePratique.contraintes && (
                <Fragment>
                  <Typography className={classes.labelTitle}>Contraintes</Typography>
                  <Typography
                    className={classes.labelContent}
                    dangerouslySetInnerHTML={
                      { __html: nl2br(ideeOuBonnePratique.contraintes) } as any
                    }
                  />
                </Fragment>
              )}
            </Box>
          </Box>
        ) : (
          <Box
            height="30vh"
            width="100%"
            display="flex"
            alignItems="center"
            justifyContent="center"
          >
            Aucune données
          </Box>
        )}
        <RightContentComment
          listResult={[]}
          codeItem={'IDEE_AND_BEST_PRACTICE'}
          item={activeItem as any}
          refetch={refetch}
        />
      </Box>
    </Box>
  );
};

export default withRouter(MainContainer);
