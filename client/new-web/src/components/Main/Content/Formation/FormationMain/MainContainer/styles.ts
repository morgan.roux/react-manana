import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
    },
    mainContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      [theme.breakpoints.down('md')]: {
        marginTop: 64,
      },
    },
    title: {
      color: theme.palette.secondary.main,
      fontSize: '1rem',
      fontWeight: 'bold',
    },
    labelTitle: {
      fontSize: '1rem',
      color: '#212121',
      fontWeight: 'bold',
      marginTop: 23,
    },
    labelContent: {
      fontSize: '1rem',
      color: '#212121',
    },
    mainContainerHeader: {
      width: '100%',
      height: 70,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-end',
      borderBottom: '1px solid #E5E5E5',
      padding: '0px 20px',
    },
    mainContainerHeaderBtns: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      '& > button:nth-child(2)': {
        marginLeft: 10,
      },
    },
    detailsContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      padding: '15px 28px',
    },
    moreContainer: {
      display: 'flex',
      width: '100%',
      justifyContent: 'flex-end',
    },
  }),
);

export default useStyles;
