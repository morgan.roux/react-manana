import { Box, FormControlLabel, IconButton, Radio, Switch, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import { FormMode } from '../../../../../Interface/form';
import CustomSelect from '../../../../Common/CustomSelect';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import useStyles from './styles';
import classnames from 'classnames';
import { CustomDatePicker, CustomTimePicker } from '../../../../Common/CustomDateTimePicker';
import ReactQuill from 'react-quill';
import { AddCircle } from '@material-ui/icons';

interface FormationFormProps {
  mode?: FormMode;
  data: any; // TODO : Change type
}

const FormationForm: FC<FormationFormProps> = ({ mode = 'create', data }) => {
  const classes = useStyles();
  return (
    <Box className={classes.root}>
      <Box className={classes.formRow}>
        <CustomSelect className={classes.mr} label="Type de formation" />
        <CustomSelect label="Public Concerné" />
      </Box>
      <Box className={classes.formRow}>
        <Box className={classnames(classes.colContainer, classes.mr)}>
          <Typography className={classes.switchTitle}>
            S’agit-il d’une Formation payante ?
          </Typography>
          <Box className={classes.switch}>
            <span>Non</span>
            <Switch
              // checked={state.checkedA}
              // onChange={handleChange}
              name="checkedA"
            />
            <span>Oui</span>
          </Box>
        </Box>
        <CustomFormTextField label="Frais d'inscription" />
      </Box>
      <CustomFormTextField label="Titre de la Formation" />
      <CustomSelect label="Organisateur" />
      <Box className={classes.formRow}>
        <Box className={classnames(classes.colContainer, classes.mr)}>
          <Typography className={classes.switchTitle}>
            S’agit-il d’une Formation en ligne ?
          </Typography>
          <Box className={classes.switch}>
            <span>Non</span>
            <Switch
              // checked={state.checkedA}
              // onChange={handleChange}
              name="checkedA"
            />
            <span>Oui</span>
          </Box>
        </Box>
        <CustomFormTextField label="Lieu" placeholder="Recherchez un lieu" />
      </Box>
      <CustomFormTextField label="Informations sur le lieu" placeholder="adresse, étage..." />
      <CustomFormTextField
        label="Lien de diffusion"
        placeholder="Ajoutez une diffusion en direct ou un lien de webinaire, maintenant ou plus tard"
      />
      <CustomDatePicker
        InputLabelProps={{ shrink: true }}
        className={classes.singleInput}
        label="Date limite d'inscription"
        value={null}
        onChange={() => null}
      />
      <Box className={classnames(classes.formRow, classes.startEndDate)}>
        <CustomDatePicker
          InputLabelProps={{ shrink: true }}
          label="Date de début"
          value={null}
          onChange={() => null}
        />
        <CustomTimePicker
          InputLabelProps={{ shrink: true }}
          label="Heure"
          value={null}
          onChange={() => null}
        />
        <span>à</span>
        <CustomDatePicker
          InputLabelProps={{ shrink: true }}
          label="Date de fin"
          value={null}
          onChange={() => null}
        />
        <CustomTimePicker
          InputLabelProps={{ shrink: true }}
          label="Heure"
          value={null}
          onChange={() => null}
        />
      </Box>
      <Box className={classnames(classes.colContainer, classes.mb)}>
        <Typography className={classes.inputSubText}>Résumé de la formation</Typography>
        <ReactQuill
          className={classes.reactQuill}
          theme="snow"
          value={''}
          // onChange={(value) => updateField("abstract", value)}
        />
      </Box>
      <Box className={classnames(classes.colContainer, classes.mb)}>
        <Typography className={classes.inputSubText}>Objectif de la formation</Typography>
        <ReactQuill
          className={classes.reactQuill}
          theme="snow"
          value={''}
          // onChange={(value) => updateField("target", value)}
        />
      </Box>
      <CustomSelect className={classes.singleInput} label="Méthode d'évaluation" />
      <Box className={classnames(classes.colContainer, classes.mb)}>
        <Typography className={classes.inputSubText}>Méthode pédagogique mise en oeuvre</Typography>
        <ReactQuill
          className={classes.reactQuill}
          theme="snow"
          value={''}
          // onChange={(value) => updateField("target", value)}
        />
      </Box>
      <Box className={classnames(classes.colContainer, classes.mb)}>
        <Typography className={classes.inputSubText}>Contenu pédagogique détaillé</Typography>
        <ReactQuill
          className={classes.reactQuill}
          theme="snow"
          value={''}
          // onChange={(value) => updateField("target", value)}
        />
      </Box>
      <Box className={classnames(classes.colContainer, classes.mb)}>
        <Typography className={classes.inputSubText}>Autre document</Typography>
        <Box className={classes.docContainer}>
          <IconButton color="primary">
            <AddCircle />
          </IconButton>
        </Box>
      </Box>
      <Box className={classnames(classes.colContainer, classes.mb)}>
        <Typography className={classes.visibilityTitle}>Visibilté de la formation</Typography>
        <Typography className={classes.visibilitySubTitle}>
          La visibilité de la formation ne plus être modifiée une fois la formation créée.
        </Typography>
        <Box className={classes.formRow}>
          <FormControlLabel value="public" control={<Radio />} label="Formation public" />
          <FormControlLabel value="prive" control={<Radio />} label="Formation privé" />
        </Box>
      </Box>
    </Box>
  );
};

export default FormationForm;
