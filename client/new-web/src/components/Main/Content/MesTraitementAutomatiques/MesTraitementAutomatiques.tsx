import React, { FC } from 'react';
import { MesTraitementsAutomatiquesPage } from '@app/ui-kit';
import { RouteComponentProps, withRouter } from 'react-router';
import {
  CHANGER_STATUT_TRAITEMENT_EXECUTION as CHANGER_STATUT_TRAITEMENT_EXECUTION_TYPE,
  CHANGER_STATUT_TRAITEMENT_EXECUTIONVariables,
} from '../../../../federation/auto/traitement-automatique/types/CHANGER_STATUT_TRAITEMENT_EXECUTION';
import { CHANGER_STATUT_TRAITEMENT_EXECUTION } from '../../../../federation/auto/traitement-automatique/mutation';
import { GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE } from './../../../../federation/auto/traitement-execution/query'
import { GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE as GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE_TYPE, GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATEVariables } from './../../../../federation/auto/traitement-execution/types/GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE'
import { GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS } from '../../../../federation/auto/traitement-execution/query'
import { GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS as GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_TYPE, GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONSVariables } from '../../../../federation/auto/traitement-execution/types/GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS'


import { FEDERATION_CLIENT } from '../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { useLazyQuery, useMutation } from '@apollo/client';
import { getPharmacie, getUser } from './../../../../services/LocalStorage';
import { SEARCH_search_data_TATraitement } from './../../../../federation/search/types/SEARCH'
import { useSearch } from './../../../Common/useSearch'
import { ME_me } from '../../../../graphql/Authentication/types/ME';
import moment from 'moment';


const MesTraitementsAutomatiques: FC<RouteComponentProps> = ({ history }) => {
  const currentUser: ME_me = getUser();
  const idPharmacie = getPharmacie().id

  const [search, searchResult] = useSearch<SEARCH_search_data_TATraitement>()
  const [loadPreviousAggregate, loadingPreviousAggregate] = useLazyQuery<GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE_TYPE, GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATEVariables>(GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE, {
    client: FEDERATION_CLIENT
  });
  const [loadNextAggregate, loadingNextAggregate] = useLazyQuery<GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE_TYPE, GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATEVariables>(GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE, {
    client: FEDERATION_CLIENT
  });

  const [searchPreviousExecutions, searchingPreviousExecutions] = useLazyQuery<GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_TYPE, GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONSVariables>(GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS, {
    client: FEDERATION_CLIENT
  });

  const [searchNextExecutions, searchingNextExecutions] = useLazyQuery<GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_TYPE, GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONSVariables>(GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS, {
    client: FEDERATION_CLIENT
  });



  const [changeStatut, ChangingStatut] = useMutation<
    CHANGER_STATUT_TRAITEMENT_EXECUTION_TYPE,
    CHANGER_STATUT_TRAITEMENT_EXECUTIONVariables
  >(CHANGER_STATUT_TRAITEMENT_EXECUTION, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      if (loadingPreviousAggregate.refetch) loadingPreviousAggregate.refetch()
      if (loadingNextAggregate.refetch) loadingNextAggregate.refetch()
      if (searchingPreviousExecutions.refetch) searchingPreviousExecutions.refetch()
      if (searchingNextExecutions.refetch) searchingNextExecutions.refetch()
    }
  });

  const handleRequestGoBack = (): void => {
    history.push('/');
  };

  const handleConsultToDo = () => {
    history.push('/todo/aujourdhui');
  }

  return (
    <MesTraitementsAutomatiquesPage
      previousExecutions={{
        data: searchingPreviousExecutions.data?.tATraitementExecutions.nodes,
        loading: searchingPreviousExecutions.loading || loadingPreviousAggregate.loading,
        error: searchingPreviousExecutions.error || loadingPreviousAggregate.error,
        rowsTotal: loadingPreviousAggregate.data?.tATraitementExecutionAggregate.count?.id || 0,
        onRunSearch: (traitement, change) => {
          const now = moment().toISOString()


          loadPreviousAggregate({
            variables: {
              filter: {
                and: [
                  {
                    idPharmacie: {
                      eq: idPharmacie
                    }
                  },

                  {
                    idTraitement: {
                      eq: traitement.id,
                    }
                  },
                  {
                    dateEcheance: {
                      gt: now
                    }
                  }

                ]

              }
            }
          })
          searchPreviousExecutions({
            variables: {
              filter: {
                and: [
                  {
                    idPharmacie: {
                      eq: idPharmacie
                    }
                  },

                  {
                    idTraitement: {
                      eq: traitement.id,
                    }
                  },
                  {
                    dateEcheance: {
                      gt: now
                    }
                  }

                ]
              },
              paging: {
                offset: change.skip,
                limit: change.take
              },
              sorting: [{ field: 'dateEcheance', direction: 'DESC' } as any]
            }
          })


        }
      }}
      nextExecutions={{
        data: searchingNextExecutions.data?.tATraitementExecutions.nodes,
        loading: searchingNextExecutions.loading || loadingNextAggregate.loading,
        error: searchingNextExecutions.error || loadingNextAggregate.error,
        rowsTotal: loadingNextAggregate.data?.tATraitementExecutionAggregate.count?.id || 0,
        onRunSearch: (traitement, change) => {
          const now = moment().toISOString()

          loadNextAggregate({
            variables: {
              filter: {
                and: [
                  {
                    idPharmacie: {
                      eq: idPharmacie
                    }
                  },
                  {
                    idTraitement: {
                      eq: traitement.id,
                    }
                  },
                  {
                    dateEcheance: {
                      lte: now
                    }
                  }

                ]
              }
            }
          })



          searchNextExecutions({
            variables: {
              filter: {
                and: [
                  {
                    idPharmacie: {
                      eq: idPharmacie
                    }
                  },
                  {
                    idTraitement: {
                      eq: traitement.id,
                    }
                  },
                  {
                    dateEcheance: {
                      lte: now
                    }
                  }

                ]
              },
              paging: {
                offset: change.skip,
                limit: change.take
              },
              sorting: [{ field: 'dateEcheance', direction: 'DESC' } as any]
            }
          })


        }
      }}
      onRequestGoBack={handleRequestGoBack}
      onConsultTodo={handleConsultToDo}
      onRequestSearch={(change) => {
        search({
          variables: {
            skip: change.skip,
            take: change.take,
            query: {
              query: {
                bool: {
                  must_not: [
                    { term: { "termine": true } },
                  ],
                  must: [
                    { term: { idPharmacie } },
                    // { term: { "participants.id": currentUser.id } },
                    {
                      query_string: {
                        query: `*${change.searchText}*`, analyze_wildcard: true,
                        fields: []
                      }
                    }
                  ]
                }
              },
              sort: [{ 'execution.dateEcheance': { order: "asc" } }]
            },
            index: ['tatraitement']
          }
        })
      }}

      onRequestChangeStatut={(traitement, execution, newStatut) => {
        changeStatut({
          variables: {
            input: {
              idTraitementExecution: execution.id,
              commentaire: newStatut.commentaire || '',
              status: newStatut.status,
            },
          },
        });
      }}
      loading={searchResult.loading}
      error={searchResult.error}
      data={(searchResult.data || []) as any}
      rowsTotal={searchResult.total || 0}
    />
  );
};

export default withRouter(MesTraitementsAutomatiques);
