import React, { FC, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { ReleveTemperaturePage, Backdrop, TableChange } from '@app/ui-kit';
import { FEDERATION_CLIENT } from './../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { useMutation, useQuery, useApolloClient, useLazyQuery } from '@apollo/client';
import { GET_RELEVES, GET_ROW_RELEVE } from './../../../../federation/tools/releve-temperature/releve/query';
import {
    GET_RELEVES as GET_RELEVES_TYPE,
    GET_RELEVESVariables,
} from './../../../../federation/tools/releve-temperature/releve/types/GET_RELEVES';
import {
    GET_ROW_RELEVE as GET_ROW_RELEVE_TYPE,
    GET_ROW_RELEVEVariables,
} from './../../../../federation/tools/releve-temperature/releve/types/GET_ROW_RELEVE';
import { GET_FRIGOS } from './../../../../federation/tools/releve-temperature/frigo/query';
import { GET_LIST_RELEVE_STATUS } from './../../../../federation/tools/releve-temperature/releve/query';
import { GET_Status as GET_STATUS_TYPE, GET_StatusVariables } from './../../../../federation/tools/releve-temperature/releve/types/GET_Status';
import {
    GET_FRIGOSVariables,
    GET_FRIGOS as GET_FRIGOS_TYPE
} from './../../../../federation/tools/releve-temperature/frigo/types/GET_FRIGOS';
import { CREATE_RELEVE } from './../../../../federation/tools/releve-temperature/releve/mutation';
import {
    CREATE_RELEVE as CREATE_RELEVE_TYPE,
    CREATE_RELEVEVariables,
} from './../../../../federation/tools/releve-temperature/releve/types/CREATE_RELEVE';
import { displaySnackBar } from './../../../../utils/snackBarUtils';
import { endTime, startTime } from '../DemarcheQualite/util';
import { getPharmacie } from '../../../../services/LocalStorage';

const ReleveTemperature: FC<RouteComponentProps> = ({ history }) => {
    const [relevesListType, setRelevesListType] = useState<'ALL' | 'STATS'>('ALL')

    const client = useApolloClient();
    const pharmacie = getPharmacie()

    /**
     * GET ALL FRIGOS
     */
    const loadFrigos = useQuery<GET_FRIGOS_TYPE, GET_FRIGOSVariables>(GET_FRIGOS, {
        client: FEDERATION_CLIENT,
        fetchPolicy: 'cache-and-network',
        variables: {
            filter: {
                idPharmacie: {
                    eq: pharmacie.id
                }
            }
        }
    })


    const loadStatus = useQuery<GET_STATUS_TYPE, GET_StatusVariables>(GET_LIST_RELEVE_STATUS, {
        client: FEDERATION_CLIENT
    });

    const [loadReleves, loadingReleves] = useLazyQuery<GET_RELEVES_TYPE, GET_RELEVESVariables>(GET_RELEVES,
        {
            client: FEDERATION_CLIENT,
            fetchPolicy: 'cache-and-network',

        })
    const [loadAggregate, loadingAggregate] = useLazyQuery<GET_ROW_RELEVE_TYPE, GET_ROW_RELEVEVariables>(GET_ROW_RELEVE,
        {
            client: FEDERATION_CLIENT,
            fetchPolicy: 'cache-and-network',

        })

    /**
     * MUTATION FOR CREATE RELEVE
     */
    const [createReleve, creatingReleve] = useMutation<
        CREATE_RELEVE_TYPE,
        CREATE_RELEVEVariables
    >(CREATE_RELEVE, {
        onCompleted: (data) => {

            if (data.createOneRTReleve.status.code === '002') {
                displaySnackBar(client, {
                    message: 'Le rélevé du frigo a été créé avec succès !',
                    type: 'SUCCESS',
                    isOpen: true,
                })
            }

            loadFrigos.refetch()
        },
        onError: () => {
            displaySnackBar(client, {
                message: 'Des erreurs se sont survenues pendant la création du rélevé',
                type: 'ERROR',
                isOpen: true,
            });
        },
        client: FEDERATION_CLIENT,
    });

    /** SUBMIT FORM CREATE RELEVE */

    const handleRequestSave = (releve: any) => {
        createReleve({
            variables: {
                input: {
                    ...releve
                }
            }
        })
    }
    const handleRequestGoBack = () => {
        history.push('/')
    }

    const handleRequestSearch = (frigo: any, change: TableChange) => {
        setRelevesListType('ALL')
        loadReleves({
            variables: {
                filter: {
                    idFrigo: {
                        eq: frigo.id
                    }
                },
                paging: {
                    offset: change.skip,
                    limit: change.take
                }
            }
        })

        loadAggregate({
            variables: {
                filter: {
                    idFrigo: {
                        eq: frigo.id
                    }
                }
            }
        })

    }


    const handleRequestStatsReleves = (frigo: any, change: any) => {
        setRelevesListType('STATS')
        loadReleves({
            variables: {
                filter: {
                    and: [
                        {
                            idFrigo: {
                                eq: frigo.id
                            }
                        },
                        {

                            createdAt: {
                                gte: change.start
                            }
                        },
                        {
                            createdAt: {
                                lte: change.end
                            }
                        }

                    ]
                },
                paging: {
                    offset: 0,
                    limit: 500
                }
            }
        })

    }


    const total = (relevesListType === 'ALL' ? loadingAggregate.data?.rTReleveAggregate.count?.id : loadingReleves.data?.rTReleves.nodes.length)

    return <ReleveTemperaturePage
        releves={{
            loading: relevesListType === 'ALL' ? loadingReleves.loading && loadingAggregate.loading : loadingReleves.loading,
            error: loadingReleves.error || loadingAggregate.error,
            data: loadingReleves.data?.rTReleves.nodes,
            total: total || 0
        }}
        saving={creatingReleve.loading}
        errorSaving={creatingReleve.error as any}
        onRequestStatsReleves={handleRequestStatsReleves}
        onRequestSearch={handleRequestSearch}
        onRequestSave={handleRequestSave}
        frigos={loadFrigos.data?.rTFrigos.nodes}
        loading={loadFrigos.loading}
        error={loadFrigos.error}
        statusFrigo={{
            ...loadStatus,
            data: loadStatus.data?.rTStatuses.nodes || []
        }}
        onRequestGoBack={handleRequestGoBack}
    />;

}

export default withRouter(ReleveTemperature);