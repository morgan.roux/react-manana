import { useLazyQuery } from '@apollo/client';
import {
  ItemProps,
  Periode,
  PeriodeItem,
  PeriodeList,
  RapportData,
  RapportPage,
} from '@app/ui-kit';
import { Box } from '@material-ui/core';
import moment from 'moment';
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import { RouteComponentProps, useParams, withRouter } from 'react-router';
import { PILOTAGE_STATISTIQUE } from '../../../../federation/tools/pilotage-statistique/pilotage-statistique/query';
import {
  GET_PILOTAGE_STATISTIQUE,
  GET_PILOTAGE_STATISTIQUEVariables,
} from '../../../../federation/tools/pilotage-statistique/pilotage-statistique/types/GET_PILOTAGE_STATISTIQUE';
import UserInput from '../../../Common/UserInput';
import { UserAutocompleteProps } from '../../../Common/UserInput/UserInput';
import { FEDERATION_CLIENT } from '../../../Dashboard/DemarcheQualite/apolloClientFederation';
import useStyles from './styles';

// import data from './fichier.json';
moment.locale('fr');

const anneeEnCours = parseInt(moment(new Date()).format('YYYY'));

let i: number;
let annees: [PeriodeItem] = [
  {
    ordre: anneeEnCours,
    libelle: `${anneeEnCours}`,
  },
];
for (i = 1; i < 3; i++) {
  annees.push({
    ordre: anneeEnCours - i,
    libelle: `${anneeEnCours - i}`,
  });
}

let semaines: [PeriodeItem] = [
  {
    ordre: 1,
    libelle: `S01`,
  },
];
for (i = 2; i < 53; i++) {
  semaines.push({
    ordre: i,
    libelle: `S${i < 10 ? '0' : ''}${i}`,
  });
}

const listMois = [
  {
    ordre: 1,
    libelle: 'Janvier',
  },
  {
    ordre: 2,
    libelle: 'Février',
  },
  {
    ordre: 3,
    libelle: 'Mars',
  },
  {
    ordre: 4,
    libelle: 'Avril',
  },
  {
    ordre: 5,
    libelle: 'Mai',
  },
  {
    ordre: 6,
    libelle: 'Juin',
  },
  {
    ordre: 7,
    libelle: 'Juillet',
  },
  {
    ordre: 8,
    libelle: 'Août',
  },
  {
    ordre: 9,
    libelle: 'Septembre',
  },
  {
    ordre: 10,
    libelle: 'Octobre',
  },
  {
    ordre: 11,
    libelle: 'Novembre',
  },
  {
    ordre: 12,
    libelle: 'Décembre',
  },
];

const periodes: PeriodeList = {
  anneePrecedentes: annees,
  mois: listMois,
  moisPrecedents: listMois,
  semaines: semaines,
  semainePrecedentes: semaines,
};

const demarcheQualiteItems: ItemProps = {
  loading: false,
  error: false,
  data: [
    {
      code: 'S',
      libelle: 'Tous',
    },
    {
      code: 'SB',
      libelle: 'Incident',
    },
    {
      code: 'SA',
      libelle: 'Amélioration',
    },
    {
      code: 'SC',
      libelle: 'Action opérationnelle',
    },
  ],
};

const todoItems: ItemProps = {
  loading: false,
  error: false,
  data: [],
};

const Rapport: FC<RouteComponentProps> = ({ history }) => {
  const classes = useStyles();

  const params: any = useParams();

  const [selectedPeriode, setSelectedPeriode] = useState<string>('ANNEE');
  const [openEquipe, setOpenEquipe] = useState<boolean>(false);
  const [annee, setAnnee] = useState<Periode>({
    cours: anneeEnCours,
    compare: anneeEnCours - 1,
    type: anneeEnCours,
  });
  const [semaine, setSemaine] = useState<Periode>({
    cours: moment().week(),
    compare: moment().week() - 1,
    type: moment().week(),
  });
  const [mois, setMois] = useState<Periode>({
    cours: moment().month() + 1,
    compare:
      moment()
        .year(annee.compare || 1)
        .week(semaine.compare || 1)
        .hour(0)
        .month() + 1,
    type: moment().month() + 1,
  });
  // const initialCodeItem = history.location.state
  const pilotageType: 'TODO' | 'DEMARCHE_QUALITE' = params.pilotageType || 'DEMARCHE_QUALITE';

  const [titre, setTitre] = useState<string>('');
  const [items, setItems] = useState<ItemProps>(todoItems);
  const [selectedItem, setSelectedItem] = useState<string>('S');
  const [selectedUser, setSelectedUser] = useState<any>({ id: 'all', fullName: "Toute l'équipe" });
  const [userParticipant, setUserParticipant] = useState<any>();
  const [dateDebut, setDateDebut] = useState<Date>(moment(`${anneeEnCours - 1}-01-01`).toDate());
  const [dateFin, setDateFin] = useState<Date>(moment(`${anneeEnCours}-01-01`).toDate());
  const allTeam = userParticipant?.id === 'all';
  const idUserParticipants = allTeam ? [] : userParticipant?.id ? [userParticipant.id] : [];

  const [loadPilotageStatistique, loadingPilotageStatistique] = useLazyQuery<
    GET_PILOTAGE_STATISTIQUE,
    GET_PILOTAGE_STATISTIQUEVariables
  >(PILOTAGE_STATISTIQUE, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
  });

  const [loadPilotagePrecedent, loadingPilotagePrecedent] = useLazyQuery<
    GET_PILOTAGE_STATISTIQUE,
    GET_PILOTAGE_STATISTIQUEVariables
  >(PILOTAGE_STATISTIQUE, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
  });

  useEffect(() => {
    if (pilotageType === 'TODO') {
      // TODO
      setSelectedItem('L');
      setItems(todoItems);
      setTitre('Pilotage TODO');
    } else {
      setItems(demarcheQualiteItems);
      setSelectedItem('S');
      setTitre('Pilotage démarche qualité');
    }
  }, [pilotageType]);

  useEffect(() => {
    if (selectedPeriode === 'SEMAINE') {
      setMois(prev => ({
        ...prev,
        compare:
          moment()
            .year(annee.compare || 1)
            .week(semaine.compare || 1)
            .hour(0)
            .month() + 1,
      }));
    }
    if (selectedPeriode === 'MOIS') {
      setMois(prev => ({
        ...prev,
        compare: moment()
          .year(annee.compare || 1)
          .month(prev.compare || 1)
          .hour(0)
          .month(),
      }));
    }
  }, [annee, semaine]);

  useEffect(() => {
    if (selectedPeriode === 'SEMAINE') {
      setDateDebut(
        moment
          .utc()
          .year(annee.compare || 1)
          .week(semaine.compare || 1)
          .startOf('week')
          .toDate(),
      );
      setDateFin(
        moment
          .utc()
          .week((semaine.compare || 1) + 1)
          .year((annee.compare || 1) + 1)
          .startOf('week')
          .toDate(),
      );
      // console.log('semaine', moment().week(semaine.type || 1).format(`YYYY/MM/DD`));
    }
    if (selectedPeriode === 'MOIS') {
      setDateDebut(
        moment
          .utc()
          .month((mois.compare || 1) - 1)
          .startOf('month')
          .toDate(),
      );
      setDateFin(
        moment
          .utc()
          .month(mois.compare || 1)
          .startOf('month')
          .toDate(),
      );
      // console.log('mois', moment().month((mois.type || 1) - 1).date(1).format(`YYYY/MM/DD`));
    }
    if (selectedPeriode === 'ANNEE') {
      setDateDebut(
        moment
          .utc()
          .year(annee.compare || 0)
          .startOf('year')
          .toDate(),
      );
      setDateFin(
        moment
          .utc()
          .year(annee.compare || 0)
          .endOf('year')
          .toDate(),
      );
    }
  }, [selectedPeriode, semaine, mois, annee]);

  useEffect(() => {
    loadPilotageStatistique({
      variables: {
        input: {
          codeItem: selectedItem,
          idUserParticipants,
          uniteTemps: selectedPeriode,
        },
      },
    });
  }, [selectedPeriode, selectedItem, userParticipant]);

  useEffect(() => {
    loadPilotagePrecedent({
      variables: {
        input: {
          codeItem: selectedItem,
          idUserParticipants,
          uniteTemps: selectedPeriode,
          dateDebut: moment.utc(dateDebut).toDate(),
          dateFin: moment.utc(dateFin).toDate(),
        },
      },
    });
  }, [selectedPeriode, selectedItem, dateDebut, dateFin, userParticipant]);

  const cours = loadingPilotageStatistique.data?.pfPilotageStatistique.evolutions || [];
  console.log('---------------date debut', moment.utc(dateDebut).format(`YYYY-MM-DD HH-mm-SS`));

  const rapport: RapportData = {
    loading: loadingPilotageStatistique.loading,
    error: loadingPilotageStatistique.error as any,
    data: {
      cours,
      compare: loadingPilotagePrecedent.data?.pfPilotageStatistique.evolutions || [],
      type: loadingPilotageStatistique.data?.pfPilotageStatistique.items || [],
      titreCours: pilotageType === 'DEMARCHE_QUALITE' ? 'Nombre de Fiches' : 'Nombre de Tâches',
      titreCompare: pilotageType === 'DEMARCHE_QUALITE' ? 'Nombre de Fiches' : 'Nombre de Tâches',
      titreType: pilotageType === 'DEMARCHE_QUALITE' ? 'Types' : 'Fonctions',
      titreHorizontal: 'Nombre Traités/En cours',
    },
  };

  const handleGoBack = () => {
    history.goBack();
  };

  const handleClickPeriode = (periode: string) => {
    setSelectedPeriode(periode);
    if (periode === 'ANNEE') {
      setAnnee(prev => ({ ...prev, compare: (prev.cours || 1) - 1 }));
    }
    if (periode === 'MOIS') {
      if (mois.compare === 1) {
        setAnnee(prev => ({ ...prev, compare: (prev.cours || 1) - 1 }));
      } else {
        setAnnee(prev => ({ ...prev, compare: prev.cours || 1 }));
      }
      setMois(prev => ({ ...prev, compare: (prev.cours || 1) - 1 || 12 }));
    }
    if (periode === 'SEMAINE') {
      if (semaine.compare === 1) {
        setAnnee(prev => ({ ...prev, compare: (prev.cours || 1) - 1 }));
      } else {
        setAnnee(prev => ({ ...prev, compare: prev.cours || 1 }));
      }
      setSemaine(prev => ({ ...prev, compare: (prev.cours || 1) - 1 || 52 }));
    }
  };

  const handleChangePeriode = ({ type, periode, ordre }) => {
    // console.log('Changement de periode', type, periode, ordre);
    if (periode === 'SEMAINE') {
      setSemaine(prev => {
        return {
          ...prev,
          [type]: ordre,
        };
      });
      setMois(prev => ({
        ...prev,
        compare:
          moment()
            .year(annee.compare || 1)
            .week(ordre)
            .hour(0)
            .month() + 1,
      }));
    }
    if (periode === 'MOIS') {
      setMois(prev => {
        return {
          ...prev,
          [type]: ordre,
        };
      });
    }
    if (periode === 'ANNEE') {
      setAnnee(prev => {
        return {
          ...prev,
          [type]: ordre,
        };
      });
    }
  };

  const handleUserAutoCompleteChante = (inputValue: any) => {
    setSelectedUser(inputValue);
    setUserParticipant(inputValue.id !== 'all' ? inputValue : undefined);
  };

  const autoCompleteProps: UserAutocompleteProps = {
    required: false,
    selected: selectedUser,
    onAutocompleteChange: handleUserAutoCompleteChante,
  };

  const SelectUserComponent = (
    <Box className={classes.selectUserBox}>
      <UserInput
        openModal={openEquipe}
        setOpenModal={setOpenEquipe}
        selected={[]}
        setSelected={() => { }}
        mode="autocomplete"
        autoCompleteProps={autoCompleteProps}
      />
    </Box>
  );
  return (
    <RapportPage
      titre={titre}
      rapport={rapport}
      selectedPeriode={selectedPeriode}
      SelectUserComponent={SelectUserComponent}
      periodes={periodes}
      annee={annee}
      mois={mois}
      items={items}
      semaine={semaine}
      onGoBack={handleGoBack}
      onClickPeriode={handleClickPeriode}
      onChangePeriode={handleChangePeriode}
      selectedItem={selectedItem}
      onChangeItem={periode => setSelectedItem(periode)}
    />
  );
};

export default withRouter(Rapport);
