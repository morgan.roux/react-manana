import React, { FC } from 'react';
import { createStyles, Theme, withStyles, WithStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { PROJECT_project } from '../../../../../../graphql/Project/types/PROJECT';
import useStyles from './styles';
import CustomSelect from '../../../../../Common/CustomSelect';
import {} from './../../../../../../types/graphql-global-types';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import { ME_me } from '../../../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../../../services/LocalStorage';
import { SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT } from '../../../../../../Constant/roles';

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
      display: 'flex',
      alignItems: 'flex-start',
      justifyContent: 'space-between',
      '& .MuiIconButton-root': {
        padding: 0,
        marginLeft: theme.spacing(2),
      },
    },
    closeButton: {
      color: theme.palette.grey[500],
    },
    titleDialog: {
      fontSize: '1.125rem',
      fontWeight: 'bold',
      fontFamily: 'Montserrat',
    },
  });

export enum TypeProject {
  PERSONNEL = 'PERSONNEL',
  PHARMACIE = 'PHARMACIE',
  GROUPEMENT = 'GROUPEMENT',
}

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography={true} className={classes.root} {...other}>
      <Typography className={classes.titleDialog}>{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

interface AlertDialogProjectComponentProps {
  modalTitle?: string;
  labelAgree?: string;
  labelDesagree?: string;
  loading?: boolean;
  onClose: () => void;
  setProject: React.Dispatch<React.SetStateAction<PROJECT_project>>;
  project: PROJECT_project;
  onClickDialog: Function;
  handleChangeProjectType: (event: any) => void;
}

const AlertDialogProjectComponent: FC<AlertDialogProjectComponentProps & RouteComponentProps> = ({
  loading,
  onClose,
  setProject,
  project,
  onClickDialog,
  // handleChangeProjectType,
  modalTitle,
  labelAgree,
  labelDesagree,
}) => {
  const classes = useStyles({});
  const currentUser: ME_me = getUser();

  const isAdmin =
    currentUser &&
    currentUser.role &&
    (currentUser.role.code === SUPER_ADMINISTRATEUR ||
      currentUser.role.code === ADMINISTRATEUR_GROUPEMENT);

  let projectTypeList = [
    { code: TypeProject.PERSONNEL, libelle: 'Personnel' },
    { code: TypeProject.PHARMACIE, libelle: 'Pharmacie' },
  ];

  if (isAdmin) {
    projectTypeList = projectTypeList.concat([
      { code: TypeProject.GROUPEMENT, libelle: 'Groupement' },
    ]);
  }

  const handleChange = (event: any) => {
    const { name, value } = event.target;
    setProject({ ...project, [name]: value });
  };

  return (
    <Dialog
      onClose={onClose}
      aria-labelledby="customized-dialog-title"
      className={classes.contentDialog}
      maxWidth="xs"
      open={true}
    >
      <DialogTitle id="customized-dialog-title" onClose={onClose}>
        {modalTitle}
      </DialogTitle>
      <DialogContent>
        <CustomFormTextField
          label="Nom"
          name="name"
          value={project._name}
          onChange={handleChange}
          required={true}
        />
        {!project.projetParent && (
          <CustomSelect
            label="Type"
            list={projectTypeList}
            listId="code"
            index="libelle"
            name="typeProject"
            value={project.typeProject}
            onChange={handleChange}
            required={true}
          />
        )}
      </DialogContent>
      <DialogActions>
        <Button
          onClick={onClose}
          className={classes.btnDesagree}
          disabled={loading}
          color="primary"
        >
          {labelDesagree}
        </Button>
        <Button
          disabled={loading}
          onClick={() => onClickDialog()}
          color="primary"
          className={classes.btnAgree}
          autoFocus={true}
        >
          {labelAgree}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default withRouter(withMobileDialog()(AlertDialogProjectComponent));
