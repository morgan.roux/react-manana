import useStyles from './styles';

export interface CustomTableData {
  [key: string]: any;
}

export interface CustomTableToolbarProps {
  numSelected: number;
}

export interface CustomTableHeadCell {
  disablePadding: boolean;
  id: keyof CustomTableData;
  label: string;
  numeric: boolean;
}

export type Order = 'asc' | 'desc';

export interface CustomTableHeadProps {
  classes: ReturnType<typeof useStyles>;
  numSelected: number;
  onRequestSort: (event: React.MouseEvent<unknown>, property: keyof CustomTableData) => void;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => void;
  order: Order;
  orderBy: string;
  rowCount: number;
  headCells: CustomTableHeadCell[];
  isSelectable: boolean;
}

export interface CustomTableProps {
  headCells: CustomTableHeadCell[];
  rows: CustomTableData[];
  isSelectable: boolean;
  showToolbar: boolean;
}
