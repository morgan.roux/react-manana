import React, { FC } from 'react';
import useStyles from './styles';
import CustomTable from './CustomTable';
import { CustomTableHeadCell, CustomTableData } from './CustomTable/interface';
import { Typography } from '@material-ui/core';

interface FinalUserTableProps {
  data: any[];
}

const FinalUserTable: FC<FinalUserTableProps> = ({ data }) => {
  const classes = useStyles({});

  const createCustomTableData = (
    id: string,
    photo: string,
    name: string,
    origine: string,
    service: string,
    cip: string,
    adresse: string,
    cp: string,
    ville: string,
  ): CustomTableData => {
    return {
      id,
      photo,
      name,
      origine,
      service,
      cip,
      adresse,
      cp,
      ville,
    };
  };

  const rows: any[] = [];
  if (data && data.length > 0) {
    data.map(value => {
      if (value) {
        const photo =
          (value.userPhoto && value.userPhoto.fichier && value.userPhoto.fichier.publicUrl) || '-';
        const userName = value.userName;
        const personnelName =
          value.userPersonnel && (value.userPersonnel.nom || value.userPersonnel.prenom)
            ? `${value.userPersonnel.prenom} ${value.userPersonnel.nom}`
            : null;
        const pPersonnelName =
          value.userPpersonnel && (value.userPpersonnel.nom || value.userPpersonnel.prenom)
            ? `${value.userPpersonnel.prenom} ${value.userPpersonnel.nom}`
            : null;
        const laboratoireName = value.userLaboratoire && value.userLaboratoire.nomLabo;
        const tutilaireName =
          value.userTitulaire &&
          value.userTitulaire.titulaire &&
          (value.userTitulaire.titulaire.nom || value.userTitulaire.titulaire.prenom)
            ? `${value.userTitulaire.titulaire.prenom} ${value.userTitulaire.titulaire.nom}`
            : null;
        const partenaireName = value.userPartenaire && value.userPartenaire.nom;
        const name =
          personnelName ||
          pPersonnelName ||
          laboratoireName ||
          tutilaireName ||
          partenaireName ||
          userName ||
          '-';
        const personnelOrigine = value.userPersonnel ? 'Groupement' : null;
        const pPersonnelOrigine = value.userPpersonnel ? 'Pharmacie' : null;
        const laboratoireOrigine = value.userLaboratoire ? 'Laboratoire' : null;
        const tutilaireOrigine = value.userTitulaire ? 'Titulaire' : null;
        const partenaireOrigine = value.userPartenaire ? 'Partenaire' : null;
        const origine =
          personnelOrigine ||
          pPersonnelOrigine ||
          laboratoireOrigine ||
          tutilaireOrigine ||
          partenaireOrigine ||
          '-';
        const service =
          (value.userPersonnel && value.userPersonnel.service && value.userPersonnel.service.nom) ||
          '-';
        const cip =
          (value.userPpersonnel &&
            value.userPpersonnel.pharmacie &&
            value.userPpersonnel.pharmacie.cip) ||
          '-';
        const personnelAdresse =
          value.userPersonnel && (value.userPersonnel.adresse1 || value.userPersonnel.adresse2);
        const tutilaireAdresse =
          value.userTitulaire &&
          value.userTitulaire.titulaire &&
          (value.userTitulaire.titulaire.adresse1 || value.userTitulaire.titulaire.adresse2);
        const partenaireAdresse =
          value.userPartenaire && (value.userPartenaire.adresse1 || value.userPartenaire.adresse2);
        const adresse = personnelAdresse || tutilaireAdresse || partenaireAdresse || '-';
        const personnelCP = value.userPersonnel && value.userPersonnel.cp;
        const tutilaireCP =
          value.userTitulaire && value.userTitulaire.titulaire && value.userTitulaire.titulaire.cp;
        const partenaireCP = value.userPartenaire && value.userPartenaire.cp;
        const cp = personnelCP || tutilaireCP || partenaireCP || '-';

        const personnelVille = value.userPersonnel && value.userPersonnel.ville;
        const tutilaireVille =
          value.userTitulaire &&
          value.userTitulaire.titulaire &&
          value.userTitulaire.titulaire.ville;
        const partenaireVille = value.userPartenaire && value.userPartenaire.ville;
        const ville = personnelVille || tutilaireVille || partenaireVille || '-';

        const row = createCustomTableData(
          value.id,
          photo,
          name,
          origine,
          service,
          cip,
          adresse,
          cp,
          ville,
        );

        rows.push(row);
      }
    });
  }

  const headCells: CustomTableHeadCell[] = [
    { id: 'photo', numeric: false, disablePadding: false, label: 'PHOTO' },
    { id: 'name', numeric: false, disablePadding: false, label: 'PRENOM & NOM' },
    { id: 'origine', numeric: false, disablePadding: false, label: 'ORIGINE' },
    { id: 'service', numeric: false, disablePadding: false, label: 'SERVICE' },
    { id: 'cip', numeric: false, disablePadding: false, label: 'CIP' },
    { id: 'adresse', numeric: false, disablePadding: false, label: 'ADRESSE' },
    { id: 'cp', numeric: false, disablePadding: false, label: 'CODE POSTAL' },
    { id: 'ville', numeric: false, disablePadding: false, label: 'VILLE' },
  ];

  return (
    <div className={classes.root}>
      <CustomTable headCells={headCells} rows={rows} isSelectable={false} showToolbar={false} />
      <Typography>Total : {rows.length} </Typography>
    </div>
  );
};

export default FinalUserTable;
