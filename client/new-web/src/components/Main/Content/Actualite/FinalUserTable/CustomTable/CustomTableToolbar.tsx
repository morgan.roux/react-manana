import React, { FC } from 'react';
import classnames from 'classnames';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import { Toolbar, Typography, Tooltip, IconButton } from '@material-ui/core';
import { useToolbarStyles } from './styles';
import { CustomTableToolbarProps } from './interface';

const CustomTableToolbar: FC<CustomTableToolbarProps> = ({ numSelected }) => {
  const classes = useToolbarStyles();

  return (
    <Toolbar
      className={classnames(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Typography className={classes.title} color="inherit" variant="subtitle1">
          {numSelected} sélectionné(s)
        </Typography>
      ) : (
        <Typography className={classes.title} variant="h6" id="tableTitle">
          Résultats
        </Typography>
      )}
      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton aria-label="delete">
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : (
        <Tooltip title="Filter list">
          <IconButton aria-label="filter list">
            <FilterListIcon />
          </IconButton>
        </Tooltip>
      )}
    </Toolbar>
  );
};

export default CustomTableToolbar;
