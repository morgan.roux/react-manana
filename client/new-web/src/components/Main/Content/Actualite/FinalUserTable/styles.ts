import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: '24px 0px',
      '& > div': {
        marginTop: 0,
      },
    },
  }),
);

export default useStyles;
