import React, { FC } from 'react';
import useStyles from '../styles';
import {
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  List,
} from '@material-ui/core';
import ArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import { v4 as uuidv4 } from 'uuid';
import { LeftSidebarProps } from '../../Interface';

const SubLeftSidebar: FC<LeftSidebarProps> = ({
  clickedCible,
  setClickedCible,
  // showSubSidebar,
  setShowSubSidebar,
  selectedCollabo,
  selectedLabo,
  selectedPharma,
  selectedPresident,
  selectedPartenaire,
}) => {
  const classes = useStyles({});

  const labelId = `checkbox-list-label-${clickedCible && clickedCible.code}`;

  // Get checkeds number on groupement
  const nbCheckedsOnGroupement = selectedCollabo.length;
  const nbCheckedsOnPharmacie = selectedPharma.length;
  const nbCheckedsOnPresident = selectedPresident.length;
  const nbCheckedsOnLaboratoire = selectedLabo.length;
  const nbCheckedsOnPartenaire = selectedPartenaire.length;

  const handleClickArrowLeft = () => {
    setShowSubSidebar(false);
    setClickedCible(null);
  };

  return (
    <List className={classes.root}>
      <ListItem key={uuidv4()} role={undefined} dense={true} button={true}>
        <ListItemText id={labelId} primary={clickedCible && clickedCible.name} />
        <ListItemText
          className={classes.number}
          primary={
            clickedCible && clickedCible.code === 'COLLGRP' && nbCheckedsOnGroupement
              ? `${nbCheckedsOnGroupement}/${clickedCible.total}`
              : clickedCible && clickedCible.code === 'PHARMA' && nbCheckedsOnPharmacie
              ? `${nbCheckedsOnPharmacie}/${clickedCible.total}`
              : clickedCible && clickedCible.code === 'PDR' && nbCheckedsOnPresident
              ? `${nbCheckedsOnPresident}/${clickedCible.total}`
              : clickedCible && clickedCible.code === 'LABO' && nbCheckedsOnLaboratoire
              ? `${nbCheckedsOnLaboratoire}/${clickedCible.total}`
              : clickedCible && clickedCible.code === 'PARTE' && nbCheckedsOnPartenaire
              ? `${nbCheckedsOnPartenaire}/${clickedCible.total}`
              : clickedCible && clickedCible.total
          }
        />
        <ListItemSecondaryAction onClick={handleClickArrowLeft}>
          <IconButton edge="end" aria-label="actualites" size="medium">
            <ArrowLeft />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
    </List>
  );
};

export default SubLeftSidebar;
