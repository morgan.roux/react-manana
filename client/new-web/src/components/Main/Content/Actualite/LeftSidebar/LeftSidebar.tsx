import React, { useState, useEffect, FC } from 'react';
import useStyles from './styles';
import {
  Typography,
  List,
  ListItem,
  ListItemIcon,
  Checkbox,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
} from '@material-ui/core';
import ArrowRight from '@material-ui/icons/KeyboardArrowRight';
import SubLeftSidebar from './SubLeftSidebar/SubLeftSidebar';
import { useApolloClient, useQuery, useLazyQuery } from '@apollo/client';
import { GET_COUNT_ACTUALITES_CIBLES } from '../../../../../graphql/Actualite';
import {
  COUNT_ACTUALITES_CIBLES,
  COUNT_ACTUALITES_CIBLES_countActualitesCibles,
} from '../../../../../graphql/Actualite/types/COUNT_ACTUALITES_CIBLES';
import {
  groupementRoles,
  pharmacieRoles,
  PRESIDENT_REGION,
  PARTENAIRE_LABORATOIRE,
  PARTENAIRE_SERVICE,
  pharmacieAllRoles,
} from '../../../../../Constant/roles';
import Backdrop from '../../../../Common/Backdrop';
import { GET_MINIMAL_SEARCH } from '../../../../../graphql/search/query';
import {
  MINIMAL_SEARCH,
  MINIMAL_SEARCHVariables,
} from '../../../../../graphql/search/types/MINIMAL_SEARCH';
import { getGroupement } from '../../../../../services/LocalStorage';
import { LeftSidebarProps } from '../Interface';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { TypePresidentCible } from '../../../../../types/graphql-global-types';
import { head, map } from 'lodash';

const LeftSidebar: FC<LeftSidebarProps> = ({
  clickedCible,
  setClickedCible,
  showSubSidebar,
  setShowSubSidebar,
  selectedCollabo,
  setSelectedCollabo,
  selectedPharma,
  setSelectedPharma,
  selectedPharmaRoles,
  setSelectedPharmaRoles,
  selectedPresident,
  setSelectedPresident,
  presidentType,
  setPresidentType,
  selectedLabo,
  setSelectedLabo,
  selectedPartenaire,
  setSelectedPartenaire,
  selectedRoles,
  selectedParent,
  setSelectedParent,
  setSelectedRoles,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const groupement = getGroupement();

  const getCountActualitesCibles = useQuery<COUNT_ACTUALITES_CIBLES>(GET_COUNT_ACTUALITES_CIBLES, {
    fetchPolicy: 'cache-and-network',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  const cibleGroupes =
    getCountActualitesCibles &&
    getCountActualitesCibles.data &&
    getCountActualitesCibles.data.countActualitesCibles;

  // Get checkeds number on groupement
  const nbCheckedsOnGroupement = selectedCollabo.length;
  const nbCheckedsOnPharmacie = selectedPharma.length;
  const nbCheckedsOnPresident = selectedPresident.length;
  const nbCheckedsOnLaboratoire = selectedLabo.length;
  const nbCheckedsOnPartenaire = selectedPartenaire.length;

  /**
   * Search Query
   */
  const [searchQuery, searchQueryResult] = useLazyQuery<MINIMAL_SEARCH, MINIMAL_SEARCHVariables>(
    GET_MINIMAL_SEARCH,
    {
      onError: errors => {
        errors.graphQLErrors.map(err => {
          const snackBarData: SnackVariableInterface = {
            type: 'ERROR',
            message: err.message,
            isOpen: true,
          };
          displaySnackBar(client, snackBarData);
        });
      },
    },
  );

  const [cibleCode, setCibleCode] = useState<string | undefined>(undefined);

  /**
   * Update selected
   */
  const updateSelected = (data: any[], dataType?: string) => {
    switch (cibleCode) {
      case 'COLLGRP':
        if (dataType === 'service') {
          setSelectedCollabo(data);
        }
        break;
      case 'PHARMA':
        if (dataType === 'pharmacie') setSelectedPharma(data);
        setSelectedPharmaRoles(pharmacieAllRoles);
        break;
      case 'PDR':
        if (dataType === 'titulaire') setSelectedPresident(data);
        if (!presidentType) setPresidentType(TypePresidentCible.PRESIDENT);
        break;
      case 'LABO':
        if (dataType === 'laboratoire') setSelectedLabo(data);
        break;
      case 'PARTE':
        if (dataType === 'partenaire') setSelectedPartenaire(data);
        break;
      default:
        break;
    }
  };

  useEffect(() => {
    const data =
      searchQueryResult.data && searchQueryResult.data.search && searchQueryResult.data.search.data;

    if (cibleCode !== undefined) {
      /**
       * Si checkeds contient cibleCode, mettre à jour les données sélectionnées (avec data)
       * Si non met un tableau vide
       */
      if (selectedParent.length > 0 && selectedParent.includes(cibleCode)) {
        if (data && data.length > 0 && !searchQueryResult.loading) {
          const dataType = head(map(data, 'type'));
          updateSelected(data, dataType);
        }
      } else {
        switch (cibleCode) {
          case 'COLLGRP':
            setSelectedCollabo([]);
            break;
          case 'PHARMA':
            setSelectedPharma([]);
            setSelectedPharmaRoles([]);
            break;
          case 'PDR':
            setSelectedPresident([]);
            setPresidentType(null);
            break;
          case 'LABO':
            setSelectedLabo([]);
            break;
          case 'PARTE':
            setSelectedPartenaire([]);
            break;
          default:
            break;
        }
      }
    }
  }, [searchQueryResult, selectedParent, cibleCode]);

  /**
   * Pour la selection cible (parent)
   * Mettre state les roles selon les cibles cliquées
   * A voir la selection cible pour les enfants (ex: sous collaborateurs groupement)
   */
  useEffect(() => {
    let newSelectedRoles: string[] = [];
    if (selectedParent.length > 0) {
      /**
       * Si checkeds includes collaborateurs groupement (colabolateur groupment est coché)
       * On met les rôles correspondant dans selectedRoles
       * Et si on le decoche, on enlève ces rôles
       * Idem pour les autres (pharmacie, partenaire , etc.)
       */

      const _updateSelectedRoles = (checkedCode: string, checkedRoles: string[]) => {
        return selectedParent.includes(checkedCode)
          ? newSelectedRoles.concat(checkedRoles)
          : newSelectedRoles.filter((item: string) => !checkedRoles.includes(item));
      };

      // Pour collaborateurs groupement
      newSelectedRoles = _updateSelectedRoles('COLLGRP', groupementRoles);
      // Pour pharmacie
      newSelectedRoles = _updateSelectedRoles('PHARMA', pharmacieRoles);
      // Pour President de region
      newSelectedRoles = _updateSelectedRoles('PDR', [PRESIDENT_REGION]);
      // Pour laboratoire (partenaire)
      newSelectedRoles = _updateSelectedRoles('LABO', [PARTENAIRE_LABORATOIRE]);
      // Pour partenaire (servie)
      newSelectedRoles = _updateSelectedRoles('PARTE', [PARTENAIRE_SERVICE]);
    }

    setSelectedRoles(newSelectedRoles);
  }, [selectedParent]);

  const _getCibleFromCibleGroupes = (codeCible: string) => {
    return cibleGroupes && cibleGroupes.find(cible => cible && cible.code === codeCible);
  };

  const handleCheckedCible = (cibleCode: string) => () => {
    setCibleCode(cibleCode);
      // TODO: Migration
    //(client as any).writeData({ data: { clickedParentItem: true } });
    const currentIndex = selectedParent.indexOf(cibleCode);
    const newSelectedParent = [...selectedParent];

    if (currentIndex === -1) {
      newSelectedParent.push(cibleCode);

      /**
       * Lancé la requete search pour avoir tous les items (pharmacies, services, etc...)
       */
      switch (cibleCode) {
        case 'COLLGRP':
          const service = _getCibleFromCibleGroupes(cibleCode);
          if (service) {
            searchQuery({
              variables: {
                type: ['service'],
                take: service.total,
                idGroupement: groupement && groupement.id ? groupement.id : '',
              },
            });
          }
          break;
        case 'PHARMA':
          const pharma = _getCibleFromCibleGroupes(cibleCode);
          if (pharma) {
            searchQuery({
              variables: {
                type: ['pharmacie'],
                take: pharma.total,
                query: {
                  query: {
                    bool: {
                      must: [
                        {
                          term: { idGroupement: groupement && groupement.id ? groupement.id : '' },
                        },
                      ],
                    },
                  },
                },
              },
            });
          }
          break;
        case 'PDR':
          const pdr = _getCibleFromCibleGroupes(cibleCode);
          if (pdr) {
            searchQuery({
              variables: {
                type: ['titulaire'],
                query: {
                  query: {
                    bool: {
                      must: [
                        { term: { estPresidentRegion: true } },
                        { term: { idGroupement: groupement.id } },
                      ],
                    },
                  },
                },
                take: pdr.total,
              },
            });
          }
          break;
        case 'LABO':
          const labo = _getCibleFromCibleGroupes(cibleCode);
          if (labo) {
            searchQuery({
              variables: { type: ['laboratoire'], take: labo.total },
            });
          }
          break;
        case 'PARTE':
          const parte = _getCibleFromCibleGroupes(cibleCode);
          if (parte) {
            searchQuery({
              variables: {
                type: ['partenaire'],
                take: parte.total,
                query: { query: { bool: { must: [{ term: { idGroupement: groupement.id } }] } } },
              },
            });
          }
          break;
        default:
          break;
      }
    } else {
      newSelectedParent.splice(currentIndex, 1);
    }

    setSelectedParent(newSelectedParent);
  };

  const handleClickedCible = (cible: COUNT_ACTUALITES_CIBLES_countActualitesCibles) => () => {
    setShowSubSidebar(true);
    setClickedCible(cible);
  };

  if (getCountActualitesCibles.loading && !getCountActualitesCibles.data) {
    return <Backdrop />;
  }

  return (
    <div className={classes.root}>
      {searchQueryResult.loading && !searchQueryResult.data && <Backdrop />}
      <div className={classes.cibleContainer}>
        <div className={classes.headerContainer}>
          <Typography variant="h6">SELECTION DE LA CIBLE</Typography>
          <Typography variant="caption">(incluant les nouveaux utilisateurs)</Typography>
        </div>
        {!showSubSidebar && (
          <List className={classes.root}>
            {cibleGroupes &&
              cibleGroupes.map(cible => {
                if (cible) {
                  const labelId = `checkbox-list-label-${cible.name}`;
                  const isChecked = selectedParent.indexOf(cible.code) !== -1;
                  return (
                    <ListItem
                      key={cible.code}
                      role={undefined}
                      dense={true}
                      button={true}
                      onClick={handleCheckedCible(cible.code)}
                    >
                      <ListItemIcon>
                        <Checkbox
                          edge="start"
                          checked={isChecked}
                          tabIndex={-1}
                          disableRipple={true}
                          inputProps={{ 'aria-labelledby': labelId }}
                        />
                      </ListItemIcon>
                      <ListItemText id={labelId} primary={cible.name} />
                      <ListItemText
                        className={classes.number}
                        primary={
                          cible.code === 'COLLGRP' && nbCheckedsOnGroupement
                            ? `${nbCheckedsOnGroupement}/${cible.total}`
                            : cible.code === 'PHARMA' && nbCheckedsOnPharmacie
                              ? `${nbCheckedsOnPharmacie}/${cible.total}`
                              : cible.code === 'PDR' && nbCheckedsOnPresident
                                ? `${nbCheckedsOnPresident}/${cible.total}`
                                : cible.code === 'LABO' && nbCheckedsOnLaboratoire
                                  ? `${nbCheckedsOnLaboratoire}/${cible.total}`
                                  : cible.code === 'PARTE' && nbCheckedsOnPartenaire
                                    ? `${nbCheckedsOnPartenaire}/${cible.total}`
                                    : cible.total
                        }
                      />
                      <ListItemSecondaryAction onClick={handleClickedCible(cible)}>
                        <IconButton edge="end" aria-label="actualites" size="medium">
                          <ArrowRight />
                        </IconButton>
                      </ListItemSecondaryAction>
                    </ListItem>
                  );
                }
              })}
          </List>
        )}
        {showSubSidebar && clickedCible && (
          <SubLeftSidebar
            clickedCible={clickedCible}
            setClickedCible={setClickedCible}
            showSubSidebar={showSubSidebar}
            setShowSubSidebar={setShowSubSidebar}
            selectedCollabo={selectedCollabo}
            setSelectedCollabo={setSelectedCollabo}
            selectedPharma={selectedPharma}
            setSelectedPharma={setSelectedPharma}
            selectedPharmaRoles={selectedPharmaRoles}
            setSelectedPharmaRoles={setSelectedPharmaRoles}
            selectedPresident={selectedPresident}
            setSelectedPresident={setSelectedPresident}
            presidentType={presidentType}
            setPresidentType={setPresidentType}
            selectedLabo={selectedLabo}
            setSelectedLabo={setSelectedLabo}
            selectedPartenaire={selectedPartenaire}
            setSelectedPartenaire={setSelectedPartenaire}
            selectedRoles={selectedRoles}
            setSelectedRoles={setSelectedRoles}
            selectedParent={selectedParent}
            setSelectedParent={setSelectedParent}
          />
        )}
      </div>
    </div>
  );
};

export default LeftSidebar;
