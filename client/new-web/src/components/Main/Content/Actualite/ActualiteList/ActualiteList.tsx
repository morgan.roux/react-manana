import React, { FC } from 'react';
import { List, Box } from '@material-ui/core';
import useStyles from '../styles';
import ActualiteListItem from './ActualiteListItem';
import { ACTUALITE_actualite } from '../../../../../graphql/Actualite/types/ACTUALITE';
import { LoaderSmall } from '../../../../Dashboard/Content/Loader';
interface ActualiteListProps {
  actualites: any;
  currentActualite: ACTUALITE_actualite | null;
  setCurrentActualite: (actualite: ACTUALITE_actualite | null) => void;
  listResult: any;
}

const ActualiteList: FC<ActualiteListProps> = ({
  actualites,
  currentActualite,
  setCurrentActualite,
  listResult,
}) => {
  const classes = useStyles({});

  return (
    <List component="nav" aria-labelledby="nested-list-subheader" className={classes.root}>
      {listResult && listResult.loading && (
        <Box display="flex" padding="20px 0px">
          <LoaderSmall />
        </Box>
      )}
      {actualites &&
        actualites.map((actualite: ACTUALITE_actualite, index: number) => (
          <ActualiteListItem
            listResult={listResult}
            key={`${actualite.id}_${index}`}
            actualite={actualite}
            active={currentActualite && currentActualite.id === actualite.id ? true : false}
            setCurrentActualite={setCurrentActualite}
          />
        ))}
      {listResult && listResult.loading && (
        <Box display="flex" padding="20px 0px">
          <LoaderSmall />
        </Box>
      )}
    </List>
  );
};

export default ActualiteList;
