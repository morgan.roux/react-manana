import React, { FC } from 'react';
import useStyles from './styles';
import moment from 'moment';
import { Box, Typography } from '@material-ui/core';

interface ActualiteInfoProps {
  actu: any;
}

const PRIORITES = { 1: 'Normal', 2: 'Important', 3: 'Bloquant' };

const ActualiteInfo: FC<ActualiteInfoProps> = ({ actu }) => {
  const classes = useStyles({});

  return (
    <Box
      className={classes.actuInfoRoot}
      display="flex"
      alignItems="center"
      justifyContent="center"
      flexDirection="column"
    >
      <Typography className={classes.type}>{actu && actu.item && actu.item.name}</Typography>
      <Typography className={classes.libelle}>{actu && actu.libelle}</Typography>
      <Box width="100%" display="flex" justifyContent="space-between">
        <Box display="flex" alignItems="baseline" className={classes.labelName}>
          du
          <Typography>{actu && actu.dateDebut && moment(actu.dateDebut).format('L')} </Typography>
          au <Typography> {actu && actu.dateFin && moment(actu.dateFin).format('L')} </Typography>
        </Box>
        <Box display="flex" alignItems="baseline" className={classes.labelName}>
          Créée le :
          <Typography>{actu && actu.dateCreation && moment(actu.dateFin).format('L')}</Typography>
        </Box>
        <Box display="flex" alignItems="baseline" className={classes.labelName}>
          Modifiée le :
          <Typography>
            {actu && actu.dateModification && moment(actu.dateFin).format('L')}
          </Typography>
        </Box>
        <Box display="flex" alignItems="baseline" className={classes.labelName}>
          Type :<Typography>{(actu && actu.item && actu.item.name) || '-'}</Typography>
        </Box>
        <Box display="flex" alignItems="baseline" className={classes.labelName}>
          Labo :
          <Typography>{(actu && actu.laboratoire && actu.laboratoire.nomLabo) || '-'}</Typography>
        </Box>
        <Box display="flex" alignItems="baseline" className={classes.labelName}>
          Priorité :
          <Typography>
            {(actu && actu.niveauPriorite && PRIORITES[actu.niveauPriorite]) || 'Normal'}
          </Typography>
        </Box>
      </Box>
      <Box width="100%" display="flex" justifyContent="space-around ">
        <Box display="flex" alignItems="baseline" className={classes.labelName}>
          Origine :<Typography>{(actu && actu.origine && actu.origine.libelle) || '-'}</Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default ActualiteInfo;
