import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    generateActionContainer: {
      marginTop: 16,
      width: '100%',
      '& > fieldset': {
        border: '1px solid rgba(0, 0, 0, 0.23)',
        borderRadius: 5,
      },
      '& .MuiCheckbox-root, & .MuiCheckbox-colorSecondary.Mui-checked': {
        color: theme.palette.primary.main,
        padding: '0px 9px',
      },
      '& .MuiInputBase-root.Mui-disabled': {
        color: '#ccc',
        '&:hover fieldset': {
          border: '1px solid rgba(0, 0, 0, 0.38)',
          // borderColor: 'rgba(0, 0, 0, 0.38)',
        },
        '& .Mui-focused fieldset': {
          border: '1px solid rgba(0, 0, 0, 0.38)',
          // borderColor: 'rgba(0, 0, 0, 0.38)',
        },
      },
    },
    generateActionDisabled: {
      '& .MuiFormControl-root': {
        '& label, &:hover label': {
          color: 'rgba(0, 0, 0, 0.38)',
        },
        '&.Mui-focused label': {
          color: 'rgba(0, 0, 0, 0.38)',
        },
        '& .MuiOutlinedInput-root.Mui-disabled .MuiOutlinedInput-notchedOutline': {
          borderColor: '#ccc',
        },
      },
    },
    fieldsetLegend: {
      marginLeft: 5,
    },
    formControlLabel: {
      marginRight: 0,
    },
    actionFormContainer: {
      padding: '30px 20px',
      '& > div:nth-child(1)': {
        marginBottom: 15,
      },
    },
    customReactQuillDisable: {
      '&:hover': {
        borderColor: 'rgba(0, 0, 0, 0.23) !important',
      },
    },
    customReactQuill: {
      outline: 'none',
      marginBottom: 18,
      border: '1px solid rgba(0, 0, 0, 0.23)',
      borderRadius: 4,
      '& .ql-toolbar.ql-snow': {
        border: '0px',
        borderBottom: '1px solid rgba(0, 0, 0, 0.23)',
      },
      '& .ql-container': {
        minHeight: 120,
        border: '0px',
      },
      '&:hover': {
        borderColor: theme.palette.primary.main,
      },
      '&:focus': {
        outline: `auto ${theme.palette.primary.main}`,
      },
    },
  }),
);

export default useStyles;
