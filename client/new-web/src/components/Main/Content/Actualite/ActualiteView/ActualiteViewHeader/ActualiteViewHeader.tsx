import React, { FC, MouseEvent, useEffect } from 'react';
import useStyles from '../styles';
import { Typography, Box, Divider } from '@material-ui/core';
import classnames from 'classnames';
import EventIcon from '@material-ui/icons/Event';
import { ActualiteViewProps } from '../ActualiteView';
import moment from 'moment';
import { ME_me } from '../../../../../../graphql/Authentication/types/ME';
import { getUser } from '../../../../../../services/LocalStorage';
import { useMutation, useApolloClient } from '@apollo/client';
import {
  MARK_ACTUALITE_NOT_SEEN,
  MARK_ACTUALITE_NOT_SEENVariables,
} from '../../../../../../graphql/Actualite/types/MARK_ACTUALITE_NOT_SEEN';
import {
  DO_MARK_ACTUALITE_NOT_SEEN,
  DO_MARK_ACTUALITE_SEEN,
} from '../../../../../../graphql/Actualite';
import {
  MARK_ACTUALITE_SEEN,
  MARK_ACTUALITE_SEENVariables,
} from '../../../../../../graphql/Actualite/types/MARK_ACTUALITE_SEEN';
import Backdrop from '../../../../../Common/Backdrop';
import {
  SEEN_BY_PHARMACIE,
  SEEN_BY_PHARMACIEVariables,
} from '../../../../../../graphql/Actualite/types/SEEN_BY_PHARMACIE';
import { DO_SEEN_BY_PHARMACIE } from '../../../../../../graphql/Actualite/mutation';
import CustomButton from '../../../../../Common/CustomButton';
import { SEARCH as SEARCH_QUERY } from '../../../../../../graphql/search/query';
import { SEARCH, SEARCHVariables } from '../../../../../../graphql/search/types/SEARCH';
import SnackVariableInterface from '../../../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import { withRouter, RouteComponentProps } from 'react-router-dom';

const ActualiteViewHeader: FC<ActualiteViewProps & RouteComponentProps> = ({
  actualite,
  listResult,
  setCurrentActualite,
  history,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();

  const currentUser: ME_me = getUser();
  const idPharmacieUser =
    (currentUser &&
      currentUser.userPpersonnel &&
      currentUser.userPpersonnel.pharmacie &&
      currentUser.userPpersonnel.pharmacie.id) ||
    (currentUser &&
      currentUser.userTitulaire &&
      currentUser.userTitulaire.titulaire &&
      currentUser.userTitulaire.titulaire.pharmacieUser &&
      currentUser.userTitulaire.titulaire.pharmacieUser.id);
  const nbPharmacieSeen: number = actualite.nbSeenByPharmacie || 0;

  const markActualiteSeen = (event: MouseEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    if (actualite.niveauPriorite === 3) {
      doMarkActualiteSeen({
        variables: { id: actualite.id, userId: currentUser && currentUser.id },
      });
      // Seen by pharmacie
      if (currentUser && currentUser.role && idPharmacieUser) {
        doSeenByPharmacie({
          variables: {
            id: actualite.id,
            idPharmacieUser,
            userId: currentUser && currentUser.id,
          },
        });
      }
    }
  };

  const markActualiteNotSeen = (event: MouseEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    if (actualite.seen === true) {
      doMarkActualiteNotSeen({
        variables: {
          id: actualite.id,
          idPharmacieUser: idPharmacieUser ? idPharmacieUser : null,
          userId: currentUser && currentUser.id,
        },
      });
    }
  };

  const [doMarkActualiteNotSeen, doMarkActualiteNotSeenResult] = useMutation<
    MARK_ACTUALITE_NOT_SEEN,
    MARK_ACTUALITE_NOT_SEENVariables
  >(DO_MARK_ACTUALITE_NOT_SEEN, {
    update: (cache, { data }) => {
      if (data && data.markAsNotSeenActualite) {
        const req = cache.readQuery<SEARCH, SEARCHVariables>({
          query: SEARCH_QUERY,
          variables: listResult && listResult.variables,
        });
        if (req && req.search && req.search.data) {
          cache.writeQuery({
            query: SEARCH_QUERY,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.map((item: any) => {
                    if (
                      item &&
                      data.markAsNotSeenActualite &&
                      item.id === data.markAsNotSeenActualite.id
                    ) {
                      item.seen = data.markAsNotSeenActualite.seen;
                      return item;
                    }
                  }),
                },
              },
            },
            variables: listResult && listResult.variables,
          });
        }
      }
    },
    onCompleted: data => {
      if (data && data.markAsNotSeenActualite) {
        if (setCurrentActualite) {
          setCurrentActualite(null);
          window.history.pushState(null, '', '/#/actualites');
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  const [doMarkActualiteSeen, doMarkActualiteSeenResult] = useMutation<
    MARK_ACTUALITE_SEEN,
    MARK_ACTUALITE_SEENVariables
  >(DO_MARK_ACTUALITE_SEEN, {
    update: (cache, { data }) => {
      if (data && data.markAsSeenActualite) {
        const req = cache.readQuery<SEARCH, SEARCHVariables>({
          query: SEARCH_QUERY,
          variables: listResult && listResult.variables,
        });
        if (req && req.search && req.search.data) {
          cache.writeQuery({
            query: SEARCH_QUERY,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.map((item: any) => {
                    if (
                      item &&
                      data.markAsSeenActualite &&
                      item.id === data.markAsSeenActualite.id
                    ) {
                      item.seen = data.markAsSeenActualite.seen;
                      return item;
                    }
                  }),
                },
              },
            },
            variables: listResult && listResult.variables,
          });
        }
      }
    },
    // onCompleted: ({ markAsSeenActualite }) => {
    //   if (markAsSeenActualite) {
    //     if (setCurrentActualite) {
    //       setCurrentActualite(markAsSeenActualite);
    //     }
    //     // Refetch search query
    //     if (listResult && listResult.refetch) {
    //       setTimeout(() => {
    //         listResult.refetch();
    //       }, 500);
    //     }
    //   }
    // },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  const [doSeenByPharmacie, doSeenByPharmacieResult] = useMutation<
    SEEN_BY_PHARMACIE,
    SEEN_BY_PHARMACIEVariables
  >(DO_SEEN_BY_PHARMACIE, {
    update: (cache, { data }) => {
      if (data && data.actualiteSeenByPharmacie) {
        const req = cache.readQuery<SEARCH, SEARCHVariables>({
          query: SEARCH_QUERY,
          variables: listResult && listResult.variables,
        });
        if (req && req.search && req.search.data) {
          cache.writeQuery({
            query: SEARCH_QUERY,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.map((item: any) => {
                    if (
                      item &&
                      data.actualiteSeenByPharmacie &&
                      item.id === data.actualiteSeenByPharmacie.id
                    ) {
                      item.nbSeenByPharmacie = data.actualiteSeenByPharmacie.nbSeenByPharmacie;
                      return item;
                    }
                  }),
                },
              },
            },
            variables: listResult && listResult.variables,
          });
        }
      }
    },
    // onCompleted: ({ actualiteSeenByPharmacie }) => {
    //   if (actualiteSeenByPharmacie) {
    //     if (setCurrentActualite) {
    //       setCurrentActualite(actualiteSeenByPharmacie);
    //     }
    //     if (listResult && listResult.refetch) {
    //       setTimeout(() => {
    //         listResult.refetch();
    //       }, 500);
    //     }
    //   }
    // },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  /**
   * Update current Actu for seen
   */
  useEffect(() => {
    const actuList =
      listResult && listResult.data && listResult.data.search && listResult.data.search.data;
    if (actuList && actuList.length > 0) {
      const actu = actuList.find((item: any) => item && item.id === actualite.id);
      if (actu && actu.seen) {
        if (setCurrentActualite) setCurrentActualite(actu);
      }
    }
  }, [listResult]);

  /**
   * Go to operation
   */
  const goToOperation = () => {
    if (actualite.operation && actualite.operation.id) {
      history.push(`/operations-commerciales/${actualite.operation.id}/presentation`);
    }
  };

  return (
    <>
      {(doMarkActualiteSeenResult.loading ||
        doMarkActualiteNotSeenResult.loading ||
        doSeenByPharmacieResult.loading) && <Backdrop />}
      <Box className={classes.headerResume}>
        <Typography className={classnames(classes.MontserratBold, classes.titleHeader)}>
          {actualite.libelle}
        </Typography>

        <Box display="flex" alignItems="center">
          <Box display="flex" alignItems="center">
            <Typography className={classes.paraphe}>
              <EventIcon />
              Période d’activation de l’actualité : du
            </Typography>
            <Typography className={classnames(classes.paraphe, classes.colorPink)}>
              {actualite.dateDebut && new Date(actualite.dateDebut).toLocaleDateString()}
            </Typography>
            <Typography className={classes.paraphe}>au</Typography>
            <Typography className={classnames(classes.paraphe, classes.colorPink)}>
              {actualite.dateFin && new Date(actualite.dateFin).toLocaleDateString()}
            </Typography>
          </Box>

          <Divider orientation="vertical" />

          <Box display="flex" alignItems="center">
            <Typography className={classes.paraphe}>Origine :</Typography>
            <Typography className={classnames(classes.paraphe, classes.colorPink)}>
              {actualite.origine && actualite.origine.libelle}
            </Typography>
          </Box>

          {actualite.niveauPriorite && (
            <>
              <Divider orientation="vertical" />

              <Box display="flex" alignItems="center">
                <Typography className={classes.paraphe}>Priorité :</Typography>
                <Typography className={classnames(classes.paraphe, classes.colorPink)}>
                  {actualite.niveauPriorite === 1
                    ? 'Normal'
                    : actualite.niveauPriorite === 2
                      ? 'Important'
                      : actualite.niveauPriorite === 3
                        ? 'Bloquant'
                        : ''}
                </Typography>
              </Box>
            </>
          )}
        </Box>

        <Box display="flex" flexDirection="row" alignItems="center" marginTop="1.5em">
          <Box display="flex" alignItems="center">
            <Typography className={classes.paraphe}>Créé le :</Typography>
            <Typography className={classnames(classes.paraphe, classes.colorPink)}>
              {moment(actualite.dateCreation).format('L')}
            </Typography>
          </Box>

          {actualite.dateCreation !== actualite.dateModification && (
            <>
              <Divider orientation="vertical" />
              <Box display="flex" alignItems="center">
                <Typography className={classes.paraphe}>Modifié le :</Typography>
                <Typography className={classnames(classes.paraphe, classes.colorPink)}>
                  {moment(actualite.dateModification).format('L')}
                </Typography>
              </Box>
            </>
          )}

          {actualite.seen === false && actualite.niveauPriorite === 3 && (
            <Box display="flex" alignItems="center">
              <CustomButton
                color="secondary"
                className={classes.btnMarkNoSeen}
                onClick={markActualiteSeen}
              >
                Marquer comme Lue
              </CustomButton>
            </Box>
          )}
          {actualite.seen && (
            <Box display="flex" alignItems="center">
              <CustomButton
                color="secondary"
                className={classes.btnMarkNoSeen}
                onClick={markActualiteNotSeen}
              >
                Marquer comme Non Lue
              </CustomButton>
            </Box>
          )}
          {actualite.operation && actualite.operation.id && (
            <Box display="flex" alignItems="center">
              <CustomButton
                color="secondary"
                className={classes.btnMarkNoSeen}
                onClick={goToOperation}
              >
                Voir l'opération commerciale
              </CustomButton>
            </Box>
          )}
        </Box>

        <Box display="flex" flexDirection="row" alignItems="center" marginTop="1.5em">
          <Box display="flex" flexDirection="row" alignItems="center">
            <Box display="flex" flexDirection="column" alignItems="start" paddingRight="4em">
              <Typography className={classes.paraphe}>
                {currentUser && (currentUser.userPpersonnel || currentUser.userTitulaire)
                  ? actualite && actualite.seen === true
                    ? `Consultée par la Pharmacie`
                    : `Non encore consultée`
                  : `Consultée par ${nbPharmacieSeen <= 1
                    ? `${nbPharmacieSeen} Pharmacie`
                    : `${nbPharmacieSeen} Pharmacies`
                  }`}
              </Typography>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default withRouter(ActualiteViewHeader);
