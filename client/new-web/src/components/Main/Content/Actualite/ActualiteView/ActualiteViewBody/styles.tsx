import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: '24px 44px',
      width: '100%',
      marginTop: 190,
    },
    userActionContainer: {
      marginTop: 50,
    },
    hr: {
      border: '1px solid #E5E5E5',
      opacity: 1,
      width: 'auto',
    },
  }),
);

export default useStyles;
