import React, { FC } from 'react';
import ActualiteViewHeader from './ActualiteViewHeader';
import ActualiteViewBody from './ActualiteViewBody';
import { ACTUALITE_actualite } from '../../../../../graphql/Actualite/types/ACTUALITE';
import useStyles from './styles';
import { Box } from '@material-ui/core';

export interface ActualiteViewProps {
  actualite: ACTUALITE_actualite;
  listResult?: any;
  setCurrentActualite?: (actualite: ACTUALITE_actualite | null) => void;
}

const ActualiteView: FC<ActualiteViewProps> = ({ actualite, listResult, setCurrentActualite }) => {
  const classes = useStyles({});

  return (
    <Box className={classes.root}>
      <ActualiteViewHeader
        actualite={actualite}
        listResult={listResult}
        setCurrentActualite={setCurrentActualite}
      />
      <ActualiteViewBody actualite={actualite} setCurrentActualite={setCurrentActualite} />
    </Box>
  );
};

export default ActualiteView;
