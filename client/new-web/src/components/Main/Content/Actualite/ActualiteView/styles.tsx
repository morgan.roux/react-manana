import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      fontFamily: 'Montserrat',
      backgroundColor: theme.palette.background.paper,
      '& .MuiPaper-root': {
        boxShadow: 'inherit!important',
      },
    },
    /* FONT STYLE */
    MontserratRegular: {
      fontFamily: 'Montserrat',
      fontWeight: 400,
    },

    MontserratBold: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
    },
    RobotoRegular: {
      fontFamily: 'Roboto',
      fontWeight: 'normal',
    },
    headerResume: {
      backgroundColor: '#002F58',
      padding: '24px 44px',
      position: 'fixed',
      width: '100%',
      zIndex: 20,
      '& hr': {
        margin: theme.spacing(0, 1),
        height: 18,
        backgroundColor: '#B1B1B1',
      },
    },
    titleHeader: {
      fontSize: '1.25rem',
      color: '#ffffff',
      marginBottom: 10,
    },
    paraphe: {
      fontSize: '0.75rem',
      color: '#B1B1B1',
      display: 'flex',
      alignContent: 'center',
      marginRight: 4,
      '& svg': {
        width: '0.75em',
        height: '0.75em',
        marginRight: 6,
      },
    },
    colorPink: {
      color: '#F11957',
    },
    fixedAppBarTab: {
      '& .MuiPaper-elevation4': {
        position: 'fixed',
      },
      '& div[role="tabpanel"]': {
        paddingTop: 210,
      },
    },
    contentTab: {
      backgroundColor: '#002F58',
      padding: '0 0px 0 46px',
      marginTop: 180,
      '& .MuiTabs-fixed': {
        backgroundColor: '#002F58',
      },
      '& .MuiButtonBase-root': {
        flexGrow: 'inherit',
        flexBasis: 'inherit',
        minWidth: 220,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        minHeight: 48,
      },
      '& .MuiTab-textColorInherit': {
        opacity: 1,
      },
      '& .MuiTab-wrapper': {
        display: 'flex',
        flexDirection: 'row',
        color: '#ffffff',
        fontSize: '0.75rem',
        textTransform: 'initial',
        '& .MuiSvgIcon-root': {
          marginRight: 8,
        },
      },
      '& .Mui-selected': {
        background: '#ffffff',
        '& .MuiTab-wrapper': {
          color: '#002444',
          fontSize: '0.75rem',
          textTransform: 'initial',
        },
      },
      '& .MuiTabs-indicator': {
        display: 'none',
      },
    },
    contenuTab: {},
    seeContentPdf: {
      height: 'calc(100vh - 351px)',
    },
    btnMarkNoSeen: {
      color: '#ffffff',
      // textTransform: 'none',
      marginLeft: 20,
    },
  }),
);

export default useStyles;
