import { useApolloClient, useMutation, useQuery } from '@apollo/client';
import React, { FC, useEffect } from 'react';
import { GET_COMMENTS } from '../../../../../../graphql/Comment';
import { COMMENTS, COMMENTSVariables } from '../../../../../../graphql/Comment/types/COMMENTS';
import { DO_CREATE_GET_PRESIGNED_URL } from '../../../../../../graphql/S3';
import {
  CREATE_GET_PRESIGNED_URL,
  CREATE_GET_PRESIGNED_URLVariables,
} from '../../../../../../graphql/S3/types/CREATE_GET_PRESIGNED_URL';
// import { USER_SMYLEYS, USER_SMYLEYSVariables } from '../../../../../../graphql/Smyley/types/USER_SMYLEYS';
import { GET_SMYLEYS } from '../../../../../../graphql/Smyley';
import { SMYLEYS, SMYLEYSVariables } from '../../../../../../graphql/Smyley/types/SMYLEYS';
import SnackVariableInterface from '../../../../../../Interface/SnackVariableInterface';
import { getGroupement } from '../../../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../../../utils/snackBarUtils';
import { Comment } from '../../../../../Comment';
import CommentList from '../../../../../Comment/CommentList';
import Backdrop from '../../../../../Common/Backdrop';
// import AppPresentation from './AppPresentation';
import UserAction from '../../../../../Common/UserAction';
import { ActualiteViewProps } from '../ActualiteView';
import AttachedFiles from './AttachedFiles';
import usesStyles from './styles';

const ActualiteViewBody: FC<ActualiteViewProps> = ({ actualite, setCurrentActualite }) => {
  const classes = usesStyles();
  const client = useApolloClient();
  const codeItem = 'ACTUALITE';

  const getComments = useQuery<COMMENTS, COMMENTSVariables>(GET_COMMENTS, {
    variables: {
      codeItem,
      idItemAssocie: actualite.id,
      take: 10,
      skip: 0,
    },
    fetchPolicy: 'cache-and-network',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  const groupement = getGroupement();
  // Get smyleys by groupement
  const getSmyleys = useQuery<SMYLEYS, SMYLEYSVariables>(GET_SMYLEYS, {
    variables: {
      idGroupement: groupement && groupement.id,
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  const [doCreateGetPresignedUrls, doCreateGetPresignedUrlsResult] = useMutation<
    CREATE_GET_PRESIGNED_URL,
    CREATE_GET_PRESIGNED_URLVariables
  >(DO_CREATE_GET_PRESIGNED_URL, {
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  const smyleys = getSmyleys && getSmyleys.data && getSmyleys.data.smyleys;
  const presignedUrls =
    doCreateGetPresignedUrlsResult &&
    doCreateGetPresignedUrlsResult.data &&
    doCreateGetPresignedUrlsResult.data.createGetPresignedUrls;

  useEffect(() => {
    const filePaths: string[] = [];
    if (smyleys) {
      smyleys.map(smyley => {
        if (smyley) filePaths.push(smyley.photo);
      });
      if (filePaths.length > 0) {
        // create presigned
        doCreateGetPresignedUrls({ variables: { filePaths } });
      }
    }
  }, [getSmyleys.data]);

  const nbComment =
    (getComments &&
      getComments.data &&
      getComments.data.comments &&
      getComments.data.comments.total) ||
    0;

  const nbSmyley = (actualite && actualite.userSmyleys && actualite.userSmyleys.total) || 0;

  const fetchMoreComments = () => {
    if (getComments.data && getComments.data.comments && getComments.data.comments.data) {
      const { fetchMore } = getComments;
      fetchMore({
        variables: {
          codeItem,
          idItemAssocie: actualite.id,
          skip: getComments.data.comments.data.length,
          take: 10,
        },
        updateQuery: (prev, { fetchMoreResult }) => {
          if (!fetchMoreResult) return prev;
          return {
            ...prev,
            data: [
              ...((fetchMoreResult && fetchMoreResult.comments && fetchMoreResult.comments.data) ||
                []),
              ...((prev && prev.comments && prev.comments.data) || []),
            ],
          };
        },
      });
    }
  };

  return (
    <div className={classes.root}>
      {getComments.loading && !getComments.data && <Backdrop />}
      <p>{actualite.description}</p>
      {/* <AppPresentation actualite={actualite} /> */}
      <AttachedFiles actualite={actualite} setCurrentActualite={setCurrentActualite} />
      <div className={classes.userActionContainer}>
        <hr className={classes.hr} />
        <UserAction
          codeItem={codeItem}
          idSource={actualite.id}
          nbComment={nbComment}
          nbSmyley={nbSmyley}
          userSmyleys={(actualite && actualite.userSmyleys && actualite.userSmyleys) || undefined}
          presignedUrls={presignedUrls}
          smyleys={smyleys}
          actualite={actualite}
        />
        <hr className={classes.hr} />
        {getComments.data && getComments.data.comments && (
          <CommentList
            key={`comment_list_${actualite.id}`}
            comments={getComments.data.comments}
            loading={getComments.loading && !getComments.data}
            fetchMoreComments={fetchMoreComments}
          />
        )}
        <Comment codeItem={codeItem} idSource={actualite.id} />
      </div>
    </div>
  );
};

export default ActualiteViewBody;
