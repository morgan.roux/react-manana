import { useState, ChangeEvent } from 'react';
import { ActualiteInterface, LaboratoireInterface, ProjectInterface } from '../Interface';

const initialLaboratoire: LaboratoireInterface = {
  id: '',
  nom: '',
  __typename: 'Laboratoire',
};

const initialProject: ProjectInterface = {
  id: '',
  name: '',
  __typename: 'Project',
};

const useActualiteForm = (defaultState?: ActualiteInterface) => {
  const initialState: ActualiteInterface = defaultState || {
    libelle: '',
    laboratoire: initialLaboratoire,
    idLaboratoire: null,
    description: '',
    dateDebut: null,
    dateFin: null,
    codeOrigine: '',
    id: undefined,
    actionAuto: undefined,
    niveauPriorite: undefined,
    globalite: undefined,
    idGroupement: undefined,
    project: initialProject,
    idProject: null,
    actionPriorite: null,
    actionDescription: null,
    actionDueDate: null,
    fichierPresentations: undefined,
    fichierCible: undefined,
    idTache: null,
    idFonction: null,
    idUrgence: '',
    idImportance: '',
    tache: null,
    selectedCollegues: [],
    isCheckedTeam: false,
  };

  const [values, setValues] = useState<ActualiteInterface>(initialState);

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      setValues(prevState => ({ ...prevState, [name]: value }));
    }
  };

  const handleChangeActionDescription = (value: string) => {
    setValues(prevState => ({ ...prevState, actionDescription: value }));
  };

  const handleChangeActionDueDate = date => {
    if (date) setValues(prevState => ({ ...prevState, actionDueDate: date }));
  };

  const handleChangeLaboratoireAutocomplete = (inputValue: any) => {
    setValues(prevState => ({
      ...prevState,
      laboratoire: inputValue,
      idLaboratoire: inputValue.id,
    }));
  };

  const handleChangeProjectAutocomplete = (value: any) => {
    setValues(prevState => ({ ...prevState, project: value, idProject: value.id }));
  };

  return {
    handleChange,
    handleChangeLaboratoireAutocomplete,
    handleChangeProjectAutocomplete,
    handleChangeActionDescription,
    handleChangeActionDueDate,
    values,
    setValues,
  };
};

export default useActualiteForm;
