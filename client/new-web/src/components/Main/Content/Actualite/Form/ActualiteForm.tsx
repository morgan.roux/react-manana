import { useApolloClient, useLazyQuery, useQuery } from '@apollo/client';
import { Box, Checkbox, FormControlLabel } from '@material-ui/core';
import classnames from 'classnames';
import { debounce, uniqBy } from 'lodash';
import moment, { Moment } from 'moment';
import React, { ChangeEvent, FC, useEffect, useRef, useState } from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import {
  GET_ACTUALITE,
  GET_ACTUALITE_ORIGINES_BY_USER_ROLE,
  GET_COUNT_ACTUALITES_CIBLES,
} from '../../../../../graphql/Actualite';
import {
  ACTUALITE,
  ACTUALITEVariables,
  ACTUALITE_actualite,
} from '../../../../../graphql/Actualite/types/ACTUALITE';
import {
  ACTUALITE_ORIGINES_BY_USER_ROLE,
  ACTUALITE_ORIGINES_BY_USER_ROLEVariables,
} from '../../../../../graphql/Actualite/types/ACTUALITE_ORIGINES_BY_USER_ROLE';
import {
  COUNT_ACTUALITES_CIBLES,
  COUNT_ACTUALITES_CIBLES_countActualitesCibles,
} from '../../../../../graphql/Actualite/types/COUNT_ACTUALITES_CIBLES';
import { ME_me } from '../../../../../graphql/Authentication/types/ME';
import { DO_SEARCH_PROJECT } from '../../../../../graphql/Project';
import {
  SEARCH_PROJECT,
  SEARCH_PROJECTVariables,
} from '../../../../../graphql/Project/types/SEARCH_PROJECT';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { getGroupement, getUser } from '../../../../../services/LocalStorage';
import { FichierInput } from '../../../../../types/graphql-global-types';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import Backdrop from '../../../../Common/Backdrop';
import CommonFieldsForm from '../../../../Common/CommonFieldsForm';
import CustomAutocomplete from '../../../../Common/CustomAutocomplete';
import { CustomDatePicker } from '../../../../Common/CustomDateTimePicker';
import CustomSelect from '../../../../Common/CustomSelect';
import CustomTextarea from '../../../../Common/CustomTextarea';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import { DateRangePicker } from '../../../../Common/DateRangePicker';
import Dropzone from '../../../../Common/Dropzone';
import PartenaireInput from '../../../../Common/PartenaireInput';
import { TypeProject } from '../../Todo/TodoCreateProject/AlertDialogProjectComponent/AlertDialogProjectComponent';
import {
  ActualiteFormProps,
  ActualiteInterface,
  LaboratoireInterface,
  SelectedInterface,
} from '../Interface';
import useStyles from './styles';

const ActualiteForm: FC<ActualiteFormProps & SelectedInterface & RouteComponentProps> = ({
  selectedFiles,
  setSelectedFiles,
  location: { pathname },
  match: { params },
  history,
  // actuRadioCible,
  setActuRadioCible,
  values,
  setValues,
  generateAction,
  setGenerateAction,
  handleChange,
  handleChangeLaboratoireAutocomplete,
  handleChangeActionDescription,
  handleChangeActionDueDate,
  handleChangeProjectAutocomplete,
  // selectedCollabo,
  setSelectedCollabo,
  // selectedPharma,
  setSelectedPharma,
  // selectedPharmaRoles,
  setSelectedPharmaRoles,
  // selectedPresident,
  setSelectedPresident,
  // presidentType,
  setPresidentType,
  // selectedLabo,
  setSelectedLabo,
  // selectedPartenaire,
  setSelectedPartenaire,
  // selectedRoles,
  // selectedParent,
  setSelectedParent,
  // showSelectableCible,
  setShowSelectableCible,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const currentUser: ME_me = getUser();
  const groupement = getGroupement();
  const currentUserRole = currentUser && currentUser.role && currentUser.role.code;

  const { id } = params as any;

  const {
    libelle,
    laboratoire,
    description,
    dateDebut,
    dateFin,
    codeOrigine,
    project,
    actionPriorite,
    actionDescription,
    actionDueDate,
    niveauPriorite,
  } = values;

  const isOnCreate: boolean = pathname === '/create/actualite';
  const isOnEdit: boolean = pathname.includes('/edit/actualite/');

  const handleChangeCheckbox = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target) {
      setGenerateAction(e.target.checked);
    }
  };

  // Get Actu origins list
  const getActuOrigins = useQuery<
    ACTUALITE_ORIGINES_BY_USER_ROLE,
    ACTUALITE_ORIGINES_BY_USER_ROLEVariables
  >(GET_ACTUALITE_ORIGINES_BY_USER_ROLE, {
    variables: { codeRole: currentUserRole || '' },
    fetchPolicy: 'cache-first',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  const getDates = (startDate: Moment | null, endDate: Moment | null) => {
    setValues((prevState: ActualiteInterface) => ({
      ...prevState,
      dateDebut: startDate,
      dateFin: endDate,
    }));
  };

  // Get actu for edit
  const [actu, setActu] = useState<ACTUALITE_actualite | null>(null);
  const [getActualite, getActualiteResult] = useLazyQuery<ACTUALITE, ACTUALITEVariables>(
    GET_ACTUALITE,
    {
      fetchPolicy: 'cache-first',
      onError: errors => {
        errors.graphQLErrors.map(err => {
          const snackBarData: SnackVariableInterface = {
            type: 'ERROR',
            message: err.message,
            isOpen: true,
          };
          displaySnackBar(client, snackBarData);
        });
      },
    },
  );

  if (getActualiteResult.data && getActualiteResult.data.actualite && actu === null) {
    setActu(getActualiteResult.data.actualite);
  }

  const [
    cibleGroupes,
    setCibleGroupes,
  ] = useState<Array<COUNT_ACTUALITES_CIBLES_countActualitesCibles | null> | null>(null);
  const [getCountActualitesCibles, getCountActualitesCiblesResult] = useLazyQuery<
    COUNT_ACTUALITES_CIBLES
  >(GET_COUNT_ACTUALITES_CIBLES, {
    fetchPolicy: 'cache-first',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });
  if (
    getCountActualitesCiblesResult &&
    getCountActualitesCiblesResult.data &&
    getCountActualitesCiblesResult.data.countActualitesCibles &&
    cibleGroupes === null
  ) {
    setCibleGroupes(getCountActualitesCiblesResult.data.countActualitesCibles);
  }

  // Get Actu if on edit
  useEffect(() => {
    if (isOnEdit && id) {
      getCountActualitesCibles();
      getActualite({ variables: { id } });
    }
  }, [isOnEdit, id]);

  /**
   * Ecouter getActualiteResult and update values
   */
  useEffect(() => {
    if (isOnEdit && actu) {
      const newFichierPresentation: FichierInput[] = [];
      const fakeFiles: File[] = [];
      if (actu && actu.fichierPresentations) {
        actu.fichierPresentations.map(item => {
          if (item && item.chemin && item.type && item.nomOriginal) {
            const file: FichierInput = {
              chemin: item.chemin,
              type: item.type as any,
              nomOriginal: item.nomOriginal,
            };
            const fakeFile: File = { name: item.nomOriginal } as any;

            if (!newFichierPresentation.map(elem => elem.chemin).includes(item.chemin)) {
              newFichierPresentation.push(file);
            }
            if (!fakeFiles.map(elem => elem.name).includes(item.nomOriginal)) {
              fakeFiles.push(fakeFile);
            }
          }
        });
      }

      setSelectedFiles(fakeFiles);

      let newFichierCible: FichierInput | undefined;
      if (actu && actu.actualiteCible && actu.actualiteCible.fichierCible) {
        const file = actu.actualiteCible.fichierCible;
        if (file.chemin && file.type && file.nomOriginal) {
          newFichierCible = {
            chemin: file.chemin,
            type: file.type as any,
            nomOriginal: file.nomOriginal,
          };
        }
      }

      setValues((prevState: ActualiteInterface) => {
        return {
          ...prevState,
          id: actu.id,
          codeOrigine: (actu.origine && actu.origine.code) || '',
          description: actu.description || '',
          libelle: actu.libelle || '',
          laboratoire: {
            id: (actu.laboratoire && actu.laboratoire.id) || '',
            nom: (actu.laboratoire && actu.laboratoire.nomLabo) || '',
            __typename: 'Laboratoire',
          },
          idLaboratoire: (actu.laboratoire && actu.laboratoire.id) || '',
          dateDebut: moment(actu.dateDebut),
          dateFin: moment(actu.dateFin),
          actionAuto: actu.actionAuto || undefined,
          niveauPriorite: actu.niveauPriorite as any,
          globalite: (actu.actualiteCible && actu.actualiteCible.globalite) || undefined,
          idGroupement: actu.idGroupement || undefined,
          fichierPresentations: newFichierPresentation,
          fichierCible: newFichierCible,
          idProject: actu.action && actu.action.project && actu.action.project.id,
          actionDescription: actu.action && actu.action.description,
          actionDueDate: actu.action && actu.action.dateDebut,
          idUrgence: actu.urgence?.id || '',
          idImportance: actu.importance?.id || '',
          idFonction: actu.idFonction,
          idTache: actu.idTache,
          project:
            actu.action && actu.action.project
              ? {
                __typename: 'Project',
                id: actu.action.project.id,
                name: actu.action.project.name || '',
              }
              : undefined,
        };
      });

      // Set actuRadioCible
      if (actu.actualiteCible && actu.actualiteCible.globalite) {
        setActuRadioCible('globalite-user');
      } else if (actu.actualiteCible && actu.actualiteCible.fichierCible) {
        setActuRadioCible('fichier-cible');
      } else {
        setActuRadioCible('selection-cible');
        // Set selecteds cibles
        setSelectedCollabo((actu.servicesCible && uniqBy(actu.servicesCible, 'id')) || []);
        setSelectedPharma((actu.pharmaciesCible && uniqBy(actu.pharmaciesCible, 'id')) || []);
        setSelectedPharmaRoles(
          (actu.pharmaciesRolesCible && actu.pharmaciesRolesCible.map(item => item && item.code)) ||
          [],
        );
        setSelectedPresident((actu.presidentsCibles && uniqBy(actu.presidentsCibles, 'id')) || []);
        setPresidentType(actu.typePresidentCible);
        setSelectedLabo((actu.laboratoiresCible && uniqBy(actu.laboratoiresCible, 'id')) || []);
        setSelectedPartenaire((actu.partenairesCible && uniqBy(actu.partenairesCible, 'id')) || []);

        // Update checked parent cible on left sidebas
        const cibles = [
          { code: 'COLLGRP', name: 'servicesCible' },
          { code: 'PHARMA', name: 'pharmaciesCible' },
          { code: 'PDR', name: 'presidentsCibles' },
          { code: 'LABO', name: 'laboratoiresCible' },
          { code: 'PARTE', name: 'partenairesCible' },
        ];

        const newCheckedsParent: string[] = [];
        if (cibleGroupes && cibleGroupes.length > 0) {
          const newActu: any = actu;
          cibles.map(item => {
            const cibleGroupeItem = cibleGroupes.find(
              elem =>
                elem &&
                item &&
                elem.code === item.code &&
                newActu[item.name] &&
                uniqBy(newActu[item.name], 'id').length === elem.total,
            );
            if (cibleGroupeItem && !newCheckedsParent.includes(cibleGroupeItem.code)) {
              newCheckedsParent.push(cibleGroupeItem.code);
            }
          });
        }

        setSelectedParent(newCheckedsParent);
      }
    }
  }, [actu, isOnEdit, cibleGroupes]);

  /**
   * Mettre à jour les variables à envoyer pour les fichiers de presentation
   * si on effacer les fichiers attachés par defaut lors d'une modification
   */
  useEffect(() => {
    if (isOnEdit) {
      const newFichierPresentation: FichierInput[] = [];
      if (selectedFiles && actu && actu.fichierPresentations) {
        selectedFiles.map(item => {
          const idemFile =
            actu.fichierPresentations &&
            actu.fichierPresentations.find(elem => elem && item && elem.nomOriginal === item.name);
          if (
            idemFile &&
            item &&
            item.name === idemFile.nomOriginal &&
            idemFile.chemin &&
            idemFile.type
          ) {
            const file: FichierInput = {
              chemin: idemFile.chemin,
              type: idemFile.type as any,
              nomOriginal: idemFile.nomOriginal,
            };
            if (!newFichierPresentation.map(elem => elem.chemin).includes(idemFile.chemin)) {
              newFichierPresentation.push(file);
            }
          }
        });
      }
      setValues(prevState => ({
        ...prevState,
        fichierPresentations: newFichierPresentation,
      }));
    }
  }, [selectedFiles, isOnEdit, actu]);

  // WRITE FORM DATA TO FORM
  useEffect(() => {
    if (values && values.project && values.project.name) {
      setQueryTextProject(values.project.name);
    }
  }, [values]);

  const [queryTextProject, setQueryTextProject] = useState<string>('');
  const projectSortBy = [{ name: { order: 'asc' } }];
  const projectFilterBy = [
    { term: { typeProject: TypeProject.GROUPEMENT } },
    { term: { 'groupement.id': groupement && groupement.id } },
    { term: { isRemoved: false } },
    { term: { archived: false } },
  ];
  const projectType = ['project'];
  // Get projects list
  const [searchProjects, searchProjectsResult] = useLazyQuery<
    SEARCH_PROJECT,
    SEARCH_PROJECTVariables
  >(DO_SEARCH_PROJECT, {
    fetchPolicy: 'cache-and-network',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  const debouncedSearch = useRef(
    debounce((query: string, dataType: string) => {
      if (query.length > 0) {
        switch (dataType) {
          case 'project':
            searchProjects({
              variables: {
                type: projectType,
                query,
                sortBy: projectSortBy,
                filterBy: projectFilterBy,
              },
            });
            break;
          default:
            break;
        }
      }
    }, 1000),
  );

  // Debounced Search Project
  useEffect(() => {
    debouncedSearch.current(queryTextProject, 'project');
  }, [queryTextProject]);

  // HIDE SELECTABLE CIBLE
  useEffect(() => {
    setShowSelectableCible(false);
    searchProjects({
      variables: {
        type: projectType,
        take: 10,
        query: queryTextProject,
        sortBy: projectSortBy,
        filterBy: projectFilterBy,
      },
    });
  }, []);

  // const disabledGroupe: boolean = values && values.codeOrigine === 'SERVICE' ? true : false;

  const listProjectPriority = [
    { id: 1, value: 'P1' },
    { id: 2, value: 'P2' },
    { id: 3, value: 'P3' },
    { id: 4, value: 'P4' },
  ];

  const [disabledLabo, setDisabledLabo] = useState<boolean>(false);
  const isRequiredLabo: boolean = values.codeOrigine === 'LABO' ? true : false;

  const prioriteList = [
    { id: 1, libelle: 'Normal' },
    { id: 2, libelle: 'Important' },
    { id: 3, libelle: 'Bloquant' },
  ];

  // DisabledOrigine
  const [disabledOrigine, setDisabledOrigine] = useState<boolean>(false);

  const origines =
    getActuOrigins &&
    getActuOrigins.data &&
    getActuOrigins.data.actualiteOriginesByRole &&
    getActuOrigins.data.actualiteOriginesByRole;

  // Set origine
  useEffect(() => {
    if (origines && origines.length === 1) {
      const origine = origines.find((item, index) => index === 0);
      const origineCode: string = (origine && origine.code) || '';
      if (origineCode !== '' && codeOrigine !== origineCode) {
        setValues(prevState => ({ ...prevState, codeOrigine: origineCode }));
        setDisabledOrigine(true);
      }
    }
  }, [getActuOrigins]);

  // Set labo if it's from labo partenaire
  useEffect(() => {
    if (history.location.state) {
      const { laboratoire } = history.location.state as any;
      if (laboratoire) {
        const labo: LaboratoireInterface = {
          id: laboratoire.id,
          nom: laboratoire.nom,
          __typename: 'Laboratoire',
        };
        const idLabo: string = laboratoire.id;
        setValues(prevState => ({ ...prevState, laboratoire: labo, idLaboratoire: idLabo }));
        setDisabledLabo(true);
      }
    }
  }, [history]);

  const disabledActionInputs = (): boolean => {
    return generateAction ? false : true;
  };

  const disabledActionProject = (): boolean => {
    if (isOnEdit && actu && actu.action && actu.action.project) {
      return true;
    }
    return false;
  };

  useEffect(() => {
    if (isOnCreate && values.idProject === null) {
      setGenerateAction(false);
    }

    if (isOnEdit && actu && actu.action && actu.action.project) {
      setGenerateAction(true);
    }
  }, [actu, isOnEdit, isOnCreate, values]);

  const isOutsideRange = () => false;

  return (
    <>
      {(getActuOrigins.loading || (isOnEdit && (actu === null || cibleGroupes === null))) && (
        <Backdrop />
      )}
      <div className={classes.form}>
        <CustomFormTextField
          label="Libellé"
          name="libelle"
          value={libelle}
          onChange={handleChange}
          required={true}
        />
        <Box width="100%" marginBottom="16px">
          <CommonFieldsForm
            style={{ padding: 0 }}
            selectedUsers={values.selectedCollegues}
            urgence={values.idUrgence}
            urgenceProps={{
              useCode: false,
            }}
            selectUsersFieldLabel="Collègue(s) concerné(s)"
            hideUserInput={true}
            usersModalProps={{
              withNotAssigned: false,
              title: 'Collègue(s) concerné(s)',
              searchPlaceholder: 'Rechercher...',
              assignTeamText: "Toute l'équipe",
              withAssignTeam: true,
              singleSelect: false,
              isCheckedTeam: values.isCheckedTeam,
              setIsCheckedTeam: value =>
                handleChange({ target: { name: 'isCheckedTeam', value } } as ChangeEvent<any>),
            }}
            projet={values.tache?.id || values.idTache || values.idFonction}
            importance={values.idImportance}
            onChangeUsersSelection={users =>
              handleChange({ target: { name: 'selectedCollegues', value: users } } as ChangeEvent<
                any
              >)
            }
            onChangeUrgence={urgence =>
              handleChange({ target: { name: 'idUrgence', value: urgence?.id } } as ChangeEvent<
                any
              >)
            }
            onChangeImportance={importance =>
              handleChange({
                target: { name: 'idImportance', value: importance?.id },
              } as ChangeEvent<any>)
            }
            onChangeProjet={projet => {
              handleChange({ target: { name: 'idTache', value: projet?.idTache } } as ChangeEvent<
                any
              >);
              handleChange({
                target: { name: 'idFonction', value: projet?.idFonction },
              } as ChangeEvent<any>);
              handleChange({ target: { name: 'tache', value: projet } } as ChangeEvent<any>);
            }}
          />
        </Box>
        <CustomTextarea
          label="Description détaillée"
          name="description"
          value={description}
          onChangeTextarea={handleChange}
          rows={2}
          required={true}
        />
        <CustomSelect
          label="Niveau de priorité"
          list={prioriteList}
          listId="id"
          index="libelle"
          name="niveauPriorite"
          value={niveauPriorite || ''}
          onChange={handleChange}
          required={false}
        />
        <PartenaireInput
          key="laboratoire"
          index="laboratoire"
          label="Laboratoire"
          value={laboratoire}
          onChange={laboratoire => handleChangeLaboratoireAutocomplete(laboratoire)}
        />
        <DateRangePicker
          startDate={dateDebut}
          startDateId="startDateUniqueId"
          endDate={dateFin}
          endDateId="endDateUniqueId"
          getDates={getDates}
          label="Période d’activation"
          required={true}
          variant="outlined"
          isOutsideRange={isOutsideRange}
        />
        <CustomSelect
          label="Origine"
          list={
            getActuOrigins && getActuOrigins.data && getActuOrigins.data.actualiteOriginesByRole
          }
          listId="code"
          index="libelle"
          name="codeOrigine"
          value={codeOrigine}
          onChange={handleChange}
          required={true}
          disabled={disabledOrigine}
        />
        <Dropzone
          contentText="Glissez et déposez le document de présentation au format PDF (Obligatoire)"
          selectedFiles={selectedFiles}
          setSelectedFiles={setSelectedFiles}
          multiple={true}
          acceptFiles="application/pdf"
        />

        {/* Generate action form */}
        <div
          className={
            generateAction
              ? classes.generateActionContainer
              : classnames(classes.generateActionContainer, classes.generateActionDisabled)
          }
        >
          <fieldset>
            <legend className={classes.fieldsetLegend}>
              <FormControlLabel
                className={classes.formControlLabel}
                control={
                  <Checkbox
                    checked={generateAction}
                    value={generateAction}
                    onChange={handleChangeCheckbox}
                    name="generateAction"
                  />
                }
                label="Générer une action dans la TODO liste de la pharmacie"
              />
            </legend>
            <div className={classes.actionFormContainer}>
              <CustomAutocomplete
                id="idProject"
                options={searchProjectsResult?.data?.search?.data || []}
                optionLabelKey="name"
                value={project}
                onAutocompleteChange={handleChangeProjectAutocomplete}
                label="Projet"
                required={!disabledActionInputs()}
                disabled={disabledActionInputs() || disabledActionProject()}
              />
              <ReactQuill
                className={
                  disabledActionInputs()
                    ? classnames(classes.customReactQuillDisable, classes.customReactQuill)
                    : classes.customReactQuill
                }
                theme="snow"
                value={actionDescription || ''}
                onChange={handleChangeActionDescription}
                readOnly={disabledActionInputs()}
              />
              <CustomSelect
                label="Priorité"
                list={listProjectPriority}
                listId="id"
                index="value"
                name="actionPriorite"
                value={actionPriorite}
                onChange={handleChange}
                required={!disabledActionInputs()}
                disabled={disabledActionInputs()}
              />
              <CustomDatePicker
                label="Date limite de la tâche"
                onChange={handleChangeActionDueDate}
                name="actionDueDate"
                value={actionDueDate}
                disabled={disabledActionInputs()}
                required={!disabledActionInputs()}
                disablePast={true}
              />
            </div>
          </fieldset>
        </div>
      </div>
    </>
  );
};

export default withRouter(ActualiteForm);
