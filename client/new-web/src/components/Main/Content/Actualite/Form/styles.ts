import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    form: {
      width: '100%',
      maxWidth: 720,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      margin: '0 auto',
      '& > div': {
        marginBottom: 20,
      },
      '& > div:nth-last-child(2)': {
        marginBottom: 0,
        '& > div, & > aside': {
          marginBottom: 0,
        },
      },
    },
    customReactQuill: {
      outline: 'none',
      marginBottom: 18,
      border: '1px solid rgba(0, 0, 0, 0.23)',
      borderRadius: 4,
      '& .ql-toolbar.ql-snow': {
        border: '0px',
        borderBottom: '1px solid rgba(0, 0, 0, 0.23)',
      },
      '& .ql-container': {
        minHeight: 120,
        border: '0px',
      },
      '&:hover': {
        borderColor: theme.palette.primary.main,
      },
      '&:focus': {
        outline: `auto ${theme.palette.primary.main}`,
      },
    },
    customReactQuillDisable: {
      '&:hover': {
        borderColor: 'rgba(0, 0, 0, 0.23) !important',
      },
    },
    generateActionContainer: {
      width: '100%',
      '& > fieldset': {
        border: '1px solid rgba(0, 0, 0, 0.23)',
        borderRadius: 5,
      },
      '& .MuiCheckbox-root, & .MuiCheckbox-colorSecondary.Mui-checked': {
        color: theme.palette.primary.main,
        padding: '0px 9px',
      },
      '& .MuiInputBase-root.Mui-disabled': {
        color: '#ccc',
        '&:hover fieldset': {
          border: '1px solid rgba(0, 0, 0, 0.38)',
          // borderColor: 'rgba(0, 0, 0, 0.38)',
        },
        '& .Mui-focused fieldset': {
          border: '1px solid rgba(0, 0, 0, 0.38)',
          // borderColor: 'rgba(0, 0, 0, 0.38)',
        },
      },
    },
    fieldsetLegend: {
      marginLeft: 5,
    },
    formControlLabel: {
      marginRight: 0,
    },
    actionFormContainer: {
      padding: '30px 20px',
      '& > div:nth-child(1)': {
        marginBottom: 15,
      },
    },
    generateActionDisabled: {
      '& .MuiFormControl-root': {
        '& label, &:hover label': {
          color: 'rgba(0, 0, 0, 0.38)',
        },
        '&.Mui-focused label': {
          color: 'rgba(0, 0, 0, 0.38)',
        },
        '& .MuiOutlinedInput-root.Mui-disabled .MuiOutlinedInput-notchedOutline': {
          borderColor: '#ccc',
        },
      },
    },
    contentDatePicker: {
      '& .MuiGrid-root': {
        position: 'relative',
      },
      '& .MuiInputLabel-formControl': {
        position: 'initial',
        transform: 'translate(0, 14px) scale(1)',
      },
      '& .MuiTextField-root': {
        width: '100%',
      },
      '& .MuiIconButton-root': {
        padding: 4,
        marginBottom: 10,
      },
      '& .MuiFormLabel-root': {
        color: '#B1B1B1',
      },

      '& .MuiFormControl-root': {
        padding: '7px 14px !important',
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.23)',
        marginTop: 0,
      },
      '& .MuiInput-underline:before': {
        display: 'none',
      },
      '& .MuiInputLabel-shrink': {
        transform: 'translate(0, -13.5px) scale(0.75)',
        background: '#ffffff',
        width: 'fit-content',
        padding: '0px 6px',
      },
      '& .MuiInputBase-input': {
        padding: '0 0 10px',
      },
    },
    inputText: {
      width: '100%',
      marginBottom: 12,
      '& .MuiOutlinedInput-root': {
        '&.Mui-focused': {
          '& .MuiOutlinedInput-notchedOutline': {
            // borderColor: '#ccc',
          },
        },
      },
      '& .MuiSelect-selectMenu': {
        fontSize: '0.875rem',
      },
      '& .MuiOutlinedInput-input': { fontSize: '0.875rem' },
      '& .MuiFormLabel-root': {
        color: '#878787',
        fontSize: '0.875rem',
      },
    },
    dateSelectNone: {
      position: 'absolute',
      width: '95%',
      height: '100%',
      left: 0,
      zIndex: 2,
    },
  }),
);

export default useStyles;
