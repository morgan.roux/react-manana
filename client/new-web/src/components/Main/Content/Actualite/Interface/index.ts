import { Moment } from 'moment';
import { TypePresidentCible, FichierInput } from '../../../../../types/graphql-global-types';
import { COUNT_ACTUALITES_CIBLES_countActualitesCibles } from '../../../../../graphql/Actualite/types/COUNT_ACTUALITES_CIBLES';
import { ActuRadioCible } from '../Create/CreateActualite';
import { Dispatch, SetStateAction, ChangeEvent } from 'react';

export interface ActualiteInterface {
  id?: string;
  codeOrigine: string;
  description: string;
  libelle: string;
  laboratoire?: LaboratoireInterface;
  idLaboratoire: string | null;
  project?: ProjectInterface;
  idProject: string | null;
  actionPriorite: number | null;
  actionDescription: string | null;
  actionDueDate: string | null;
  dateDebut: Moment | null;
  dateFin: Moment | null;
  actionAuto?: boolean;
  niveauPriorite?: number;
  globalite?: boolean;
  idGroupement?: string;
  fichierPresentations?: FichierInput[];
  fichierCible?: FichierInput;
  idTache: string | null;
  idFonction: string | null;
  idUrgence: string;
  idImportance: string;
  isCheckedTeam: boolean;
  selectedCollegues: any;
  tache: any;
}

export interface LaboratoireInterface {
  id: string;
  nom: string;
  __typename: string;
}

export interface ProjectInterface {
  id: string;
  name: string;
  __typename: string;
}

export interface ActualiteFormProps {
  loading: boolean;
  selectedFiles: File[];
  setSelectedFiles: Dispatch<SetStateAction<File[]>>;
  actuRadioCible: ActuRadioCible;
  setActuRadioCible: Dispatch<SetStateAction<ActuRadioCible>>;
  values: ActualiteInterface;
  setValues: Dispatch<SetStateAction<ActualiteInterface>>;
  generateAction: boolean;
  setGenerateAction: Dispatch<SetStateAction<boolean>>;
  showSelectableCible: boolean;
  setShowSelectableCible: Dispatch<SetStateAction<boolean>>;
  handleChange: (e: ChangeEvent<any>) => void;
  handleChangeLaboratoireAutocomplete: (inputValue: any) => void;
  handleChangeProjectAutocomplete: (inputValue: any) => void;
  handleChangeActionDescription: (value: string) => void;
  handleChangeActionDueDate: (date: any) => void;
}

export interface CibleInterface {
  clickedCible: COUNT_ACTUALITES_CIBLES_countActualitesCibles;
}

export interface LeftSidebarProps extends SelectedInterface {
  clickedCible: COUNT_ACTUALITES_CIBLES_countActualitesCibles | null;
  setClickedCible: Dispatch<SetStateAction<COUNT_ACTUALITES_CIBLES_countActualitesCibles | null>>;
  showSubSidebar: boolean;
  setShowSubSidebar: Dispatch<SetStateAction<boolean>>;
}

export interface CibleTableProps {
  cible: COUNT_ACTUALITES_CIBLES_countActualitesCibles;
  listResult?: any;
}

export interface SelectedInterface {
  selectedCollabo: any[];
  setSelectedCollabo: Dispatch<SetStateAction<any[]>>;
  selectedPharma: any[];
  setSelectedPharma: Dispatch<SetStateAction<any[]>>;
  selectedPharmaRoles: any[];
  setSelectedPharmaRoles: Dispatch<SetStateAction<any[]>>;
  selectedPresident: any[];
  setSelectedPresident: Dispatch<SetStateAction<any[]>>;
  presidentType: TypePresidentCible | null;
  setPresidentType: Dispatch<SetStateAction<TypePresidentCible | null>>;
  selectedLabo: any[];
  setSelectedLabo: Dispatch<SetStateAction<any[]>>;
  selectedPartenaire: any[];
  setSelectedPartenaire: Dispatch<SetStateAction<any[]>>;
  selectedRoles: any[];
  setSelectedRoles: Dispatch<SetStateAction<any[]>>;
  selectedParent: any[];
  setSelectedParent: Dispatch<SetStateAction<any[]>>;
}
