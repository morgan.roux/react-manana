import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    cibleTableContainer: {
      width: 'auto !important',
      margin: '0px 20px !important',
      '& > div': {
        width: '100% !important',
        margin: '20px 0px !important',
      },
    },
  }),
);

export default useStyles;
