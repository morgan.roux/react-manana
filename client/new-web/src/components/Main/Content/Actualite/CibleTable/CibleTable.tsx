import { useApolloClient, useQuery } from '@apollo/client';
import { Box, Checkbox, FormControlLabel, FormGroup, Radio } from '@material-ui/core';
import { isEqual, uniq } from 'lodash';
import React, { ChangeEvent, Dispatch, FC, SetStateAction, useEffect, useState } from 'react';
import { GET_SEARCH_CUSTOM_CONTENT_LABORATOIRE_MINIM } from '../../../../../graphql/Laboratoire/query';
import { GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE } from '../../../../../graphql/Partenaire/query';
import { GET_SEARCH_CUSTOM_CONTENT_PHARMACIE } from '../../../../../graphql/Pharmacie/query';
import { DO_SEARCH_ROLES } from '../../../../../graphql/Role';
import {
  SEARCH_ROLES,
  SEARCH_ROLESVariables,
} from '../../../../../graphql/Role/types/SEARCH_ROLES';
import { GET_SEARCH_CUSTOM_CONTENT_SERVICE } from '../../../../../graphql/Service';
import { GET_SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE } from '../../../../../graphql/Titulaire/query';
import { getGroupement } from '../../../../../services/LocalStorage';
import { TypePresidentCible } from '../../../../../types/graphql-global-types';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import Backdrop from '../../../../Common/Backdrop';
import CustomContent from '../../../../Common/newCustomContent';
import WithSearch from '../../../../Common/newWithSearch/withSearch';
import { CibleTableProps, SelectedInterface } from '../Interface';
import useStyles from './styles';

const CibleTable: FC<CibleTableProps & SelectedInterface> = ({
  cible,
  selectedCollabo,
  setSelectedCollabo,
  selectedPharma,
  setSelectedPharma,
  selectedPharmaRoles,
  setSelectedPharmaRoles,
  selectedPresident,
  setSelectedPresident,
  presidentType,
  setPresidentType,
  selectedLabo,
  setSelectedLabo,
  selectedPartenaire,
  setSelectedPartenaire,
  // selectedRoles,
  selectedParent,
  setSelectedParent,
  // listResult,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const groupement = getGroupement();

  const [roles, setRoles] = useState<any[]>([]);

  const cibleCode = (cible && cible.code) || '';
  const cibleTotal = (cible && cible.total) || 0;

  /**
   * Get pharmacie roles
   */
  const { data: dataRoles, loading: loadingRoles } = useQuery<SEARCH_ROLES, SEARCH_ROLESVariables>(
    DO_SEARCH_ROLES,
    {
      variables: {
        type: ['role'],
        filterBy: [{ terms: { typeRole: ['PHARMACIE', 'TITULAIRE', 'STATUT'] } }],
        sortBy: [{ nom: { order: 'asc' } }],
      },
    },
  );

  /**
   * Set roles
   */
  useEffect(() => {
    if (
      dataRoles &&
      dataRoles.search &&
      dataRoles.search.data &&
      dataRoles.search.data.length > 0
    ) {
      setRoles(dataRoles.search.data);
    }
  }, [dataRoles]);

  let searchType = '';
  let columns: any[] = [];
  let searchQuery: any = '';

  /**
   * Switch cibleCode pour mettre à jour searchType et columns
   */
  switch (cibleCode) {
    case 'COLLGRP':
      searchType = 'service';
      searchQuery = GET_SEARCH_CUSTOM_CONTENT_SERVICE;
      columns = columns.concat([
        { numeric: false, disablePadding: false, name: 'nom', label: 'NOM' },
        {
          numeric: false,
          disablePadding: false,
          name: 'countUsers',
          label: "NOMBRE D'UTILISATEUR",
          renderer: (value: any) => {
            return (value && value.countUsers) || '0';
          },
        },
      ]);
      break;
    case 'PHARMA':
      searchType = 'pharmacie';
      searchQuery = GET_SEARCH_CUSTOM_CONTENT_PHARMACIE;
      columns = columns.concat([
        { numeric: false, disablePadding: false, name: 'nom', label: 'NOM' },
        {
          numeric: false,
          disablePadding: false,
          name: '',
          label: 'TITULAIRE',
          renderer: (value: any) => {
            return (
              value.titulaires &&
              value.titulaires
                .map((titulaire: any) => {
                  return (titulaire && `${titulaire.prenom} ${titulaire.nom}`) || '-';
                })
                .join(', ')
            );
          },
        },
        { numeric: false, disablePadding: false, name: 'cip', label: 'CIP' },
        {
          numeric: false,
          disablePadding: false,
          name: 'sortie',
          label: 'SORTIE',
        },
        {
          numeric: false,
          disablePadding: false,
          name: 'tele1',
          label: 'TELEPHONE',
        },
        {
          numeric: false,
          disablePadding: false,
          name: 'adresse1',
          label: 'ADRESSE1',
        },
        {
          numeric: false,
          disablePadding: false,
          name: 'cp',
          label: 'CODE POSTAL',
        },
        {
          numeric: false,
          disablePadding: false,
          name: 'ville',
          label: 'VILLE',
        },
        {
          numeric: false,
          disablePadding: false,
          name: '',
          label: 'DEPARTEMENT',
          renderer: (value: any) => {
            return (value && value.departement && value.departement.nom) || '-';
          },
        },
      ]);
      break;
    case 'PDR':
      searchType = 'titulaire';
      searchQuery = GET_SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE;
      columns = columns.concat([
        { numeric: false, disablePadding: false, name: 'prenom', label: 'PRENOM' },
        { numeric: false, disablePadding: false, name: 'nom', label: 'NOM' },
      ]);
      break;
    case 'LABO':
      searchType = 'laboratoire';
      searchQuery = GET_SEARCH_CUSTOM_CONTENT_LABORATOIRE_MINIM;
      columns = columns.concat([
        { numeric: false, disablePadding: false, name: 'nomLabo', label: 'NOM' },
        {
          numeric: false,
          disablePadding: false,
          name: 'sortie',
          label: 'Sortie',
          renderer: (value: any) => {
            return value && value.sortie === 1 ? 'Oui' : 'Non';
          },
        },
      ]);
      break;
    case 'PARTE':
      searchType = 'partenaire';
      searchQuery = GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE;
      columns = columns.concat([
        { numeric: false, disablePadding: false, name: 'nom', label: 'NOM' },
      ]);
      break;
    default:
      break;
  }

  const handleCheckPharmaRoles = (role: string) => () => {
    let newCheckedsPharmaRoles = [...selectedPharmaRoles];
    if (selectedPharmaRoles.map((item: any) => item).includes(role)) {
      newCheckedsPharmaRoles = selectedPharmaRoles.filter((item: any) => item !== role);
    } else {
      newCheckedsPharmaRoles = newCheckedsPharmaRoles.concat(role);
    }

    setSelectedPharmaRoles(newCheckedsPharmaRoles);
  };

  const rolesInPharmaIsChecked = (role: string): boolean => {
    return selectedPharmaRoles &&
      selectedPharmaRoles.find((item: any) => item === role) !== undefined
      ? true
      : false;
  };

  const handleChangeRadioOnPresident = (e: ChangeEvent<HTMLInputElement>) => {
    setPresidentType(e.target.value as TypePresidentCible);
  };

  const RolesCheckbox = () => {
    return (
      <Box width="100%" padding="25px 0px">
        <FormGroup row={true}>
          {cibleCode === 'PHARMA' && (
            <>
              {roles &&
                roles.length > 0 &&
                roles.map((role: any, index: number) => {
                  return (
                    <FormControlLabel
                      key={`roles-checkbox-${index}`}
                      control={
                        <Checkbox
                          checked={rolesInPharmaIsChecked(role.code)}
                          onChange={handleCheckPharmaRoles(role.code)}
                          value={role.code}
                        />
                      }
                      label={role.nom ? role.nom : ''}
                    />
                  );
                })}
            </>
          )}
          {cibleCode === 'PDR' && (
            <>
              <FormControlLabel
                control={
                  <Radio
                    onChange={handleChangeRadioOnPresident}
                    checked={presidentType === TypePresidentCible.PRESIDENT}
                    value={TypePresidentCible.PRESIDENT}
                    name={TypePresidentCible.PRESIDENT}
                  />
                }
                label="Président de Région"
              />
              <FormControlLabel
                control={
                  <Radio
                    onChange={handleChangeRadioOnPresident}
                    checked={presidentType === TypePresidentCible.PHARMACIE}
                    value={TypePresidentCible.PHARMACIE}
                    name={TypePresidentCible.PHARMACIE}
                  />
                }
                label="Les pharmacies adossés au président de région"
              />
            </>
          )}
        </FormGroup>
      </Box>
    );
  };

  /**
   * Afficher un message si on coche des pharmacies mais on ne coche pas des rôles
   */
  useEffect(() => {
    if (
      searchType === 'pharmacie' &&
      ((selectedPharma.length > 0 && selectedPharmaRoles.length === 0) ||
        (selectedPharma.length === 0 && selectedPharmaRoles.length > 0))
    ) {
      displaySnackBar(client, {
        type: 'WARNING',
        message: 'Vous devez cocher au moins un rôle et une pharmacie',
        isOpen: true,
      });
    }
  }, [selectedPharma, selectedPharmaRoles]);

  const getFilterBy = (type: string): any[] => {
    return type === 'titulaire'
      ? [{ term: { estPresidentRegion: true } }, { term: { idGroupement: groupement.id } }]
      : type === 'pharmacie' || type === 'partenaire'
        ? [{ term: { idGroupement: groupement.id } }]
        : [];
  };

  interface ISelected {
    selected: any[];
    setSelected: Dispatch<SetStateAction<any[]>>;
  }

  const getSelected = (): ISelected => {
    switch (cibleCode) {
      case 'COLLGRP':
        return { selected: selectedCollabo, setSelected: setSelectedCollabo };
      case 'PHARMA':
        return { selected: selectedPharma, setSelected: setSelectedPharma };
      case 'PDR':
        return { selected: selectedPresident, setSelected: setSelectedPresident };
      case 'LABO':
        return { selected: selectedLabo, setSelected: setSelectedLabo };
      case 'PARTE':
        return { selected: selectedPartenaire, setSelected: setSelectedPartenaire };
      default:
        return { selected: selectedCollabo, setSelected: setSelectedCollabo };
    }
  };

  /**
   * Update checked on parent
   */
  useEffect(() => {
    let newSelectedParents: any[] = [];
    if (getSelected().selected.length === cibleTotal) {
      newSelectedParents = uniq([...selectedParent, cibleCode]);
      if (!isEqual(newSelectedParents, selectedParent)) {
        setSelectedParent(newSelectedParents);
      }
    } else if (getSelected().selected.includes(cibleCode)) {
      newSelectedParents = uniq(selectedParent.filter(i => i !== cibleCode));
      if (!isEqual(newSelectedParents, selectedParent)) {
        setSelectedParent(newSelectedParents);
      }
    } else {
      if (!isEqual(newSelectedParents, selectedParent)) {
        setSelectedParent(newSelectedParents);
      }
    }
  }, [getSelected().selected, cibleTotal]);

  const childrenProps: any = {
    isSelectable: true,
    paginationCentered: true,
    columns,
    selected: getSelected().selected,
    setSelected: getSelected().setSelected,
  };

  return (
    <Box className={classes.cibleTableContainer}>
      {loadingRoles && <Backdrop />}
      {(cibleCode === 'PHARMA' || cibleCode === 'PDR') && <RolesCheckbox />}
      <WithSearch
        type={searchType}
        props={childrenProps}
        WrappedComponent={CustomContent}
        searchQuery={searchQuery}
        optionalMust={getFilterBy(searchType)}
      />
    </Box>
  );
};

export default CibleTable;
