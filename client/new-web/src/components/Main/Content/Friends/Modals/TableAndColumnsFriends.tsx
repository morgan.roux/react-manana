import { useApolloClient, useLazyQuery } from '@apollo/client';
import { Box } from '@material-ui/core';
import { debounce } from 'lodash';
import React, { useEffect, useRef, useState } from 'react';
import { RouteComponentProps } from 'react-router';
import { DO_SEARCH_GROUPE_AMIS } from '../../../../../graphql/GroupeAmis/query';
import {
  SEARCH_GROUPE_AMIS,
  SEARCH_GROUPE_AMISVariables,
} from '../../../../../graphql/GroupeAmis/types/SEARCH_GROUPE_AMIS';
import { DO_SEARCH_ALL_PHARMACIES } from '../../../../../graphql/Pharmacie';
import {
  SEARCH_ALL_PHARMACIES,
  SEARCH_ALL_PHARMACIESVariables,
} from '../../../../../graphql/Pharmacie/types/SEARCH_ALL_PHARMACIES';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { getGroupement } from '../../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { SearchInput } from '../../../../Dashboard/Content/SearchInput';
import { columns, getColumnsPharmacie } from './ColumsFriends';
import EnhancedTableFriends from './TableFriends';

interface FriendsProp {
  selectionvalue: any;
  query?: any;
  setUserIdsSelected?: (value: any) => void;
  userIdsSelected?: string[];
  setUsersSelected?: (value: any) => void;
  usersSelected?: any[];
}

export interface SkipAndTake {
  skip: number;
  take: number;
}

export interface SortBy {
  column: string;
  order: 'asc' | 'desc' | undefined;
}

const TableAndColumns: React.FC<FriendsProp> = ({
  query,
  setUserIdsSelected,
  userIdsSelected,
  setUsersSelected,
  usersSelected,
  selectionvalue,
}) => {
  const [debouncedText, setDebouncedText] = useState<string>('');
  const [textSearch, setTextSearch] = useState<string>('');
  const [skipAndTake, setSkipAndTake] = useState<SkipAndTake>({
    skip: 0,
    take: 10,
  });

  const getsidGroupement = getGroupement();
  const [sortBy, setSortBy] = useState<SortBy>({
    column: 'nom',
    order: 'asc',
  });

  const onChangeSearchText = (value: string) => {
    setDebouncedText(value);
  };

  const handleClick = (rowId: string, row: any) => (event: React.MouseEvent<unknown>) => {
    event.preventDefault();
    event.stopPropagation();

    if (setUserIdsSelected) {
      setUserIdsSelected(prev =>
        prev.includes(rowId) ? [...prev.filter(value => value !== rowId)] : [...prev, rowId],
      );
    }

    if (setUsersSelected) {
      setUsersSelected(prev =>
        prev.filter(user => user && row && user.id === row.id).length > 0
          ? [...prev.filter(value => value && row && value.id !== row.id)]
          : [...prev, row],
      );
    }
  };

  const mustdefault = [{ term: { idGroupement: getsidGroupement.id } }, { term: { sortie: 0 } }];
  const mustdefaultFriends = [{ term: { isRemoved: 'false' } }];

  const debouncedSearchText = useRef(
    debounce((text: string) => {
      setTextSearch(text);
      searchPharmacie({
        variables: {
          ...query,
          query: {
            query: {
              bool: {
                must: [
                  ...mustdefault,
                  {
                    query_string: {
                      query: text.startsWith('*') || text.endsWith('*') ? text : `*${text}*`,
                      analyze_wildcard: true,
                    },
                  },
                ],
              },
            },
          },
          type: ['pharmacie'],
          skip: skipAndTake.skip,
          take: skipAndTake.take,
        },
      });
      searchGroupeAmis({
        variables: {
          query: {
            query: {
              bool: {
                must: [
                  ...mustdefaultFriends,
                  {
                    query_string: {
                      query: text.startsWith('*') || text.endsWith('*') ? text : `*${text}*`,
                      analyze_wildcard: true,
                    },
                  },
                ],
              },
            },
          },
          type: ['groupeamis'],
          skip: skipAndTake.skip,
          take: skipAndTake.take,
        },
      });
    }, 1500),
  );

  useEffect(() => {
    setDebouncedText(textSearch ? textSearch : '');
    if (textSearch === '') {
      searchPharmacie({
        variables: {
          query: {
            query: {
              bool: {
                must: mustdefault,
              },
            },
            sort: sortBy ? [{ [sortBy.column]: { order: sortBy.order } }] : [],
          },
          type: ['pharmacie'],
          skip: skipAndTake.skip,
          take: skipAndTake.take,
        },
      });
    }
  }, [textSearch]);

  useEffect(() => {
    debouncedSearchText.current(debouncedText);
  }, [debouncedText]);

  const updateSkipAndTake = (skip: number, take: number) => {
    setSkipAndTake({
      skip,
      take,
    });
  };

  const [searchGroupeAmis, results] = useLazyQuery<SEARCH_GROUPE_AMIS, SEARCH_GROUPE_AMISVariables>(
    DO_SEARCH_GROUPE_AMIS,
    {
      fetchPolicy: 'cache-and-network',
    },
  );

  const [searchPharmacie, resultPharmacie] = useLazyQuery<
    SEARCH_ALL_PHARMACIES,
    SEARCH_ALL_PHARMACIESVariables
  >(DO_SEARCH_ALL_PHARMACIES, {
    fetchPolicy: 'cache-and-network',
    variables: {
      query: {
        query: {
          bool: {
            must: mustdefault,
          },
        },
        sort: sortBy ? [{ [sortBy.column]: { order: sortBy.order } }] : [],
      },
      type: ['pharmacie'],
      skip: skipAndTake.skip,
      take: skipAndTake.take,
    },
  });

  useEffect(() => {
    searchGroupeAmis({
      variables: {
        query: {
          query: { bool: { must: mustdefaultFriends } },
          sort: sortBy ? [{ [sortBy.column]: { order: sortBy.order } }] : [],
        },
        type: ['groupeamis'],
        skip: skipAndTake.skip,
        take: skipAndTake.take,
      },
    });

    searchPharmacie({
      variables: {
        query: {
          query: {
            bool: {
              must: mustdefault,
            },
          },
          sort: sortBy ? [{ [sortBy.column]: { order: sortBy.order } }] : [],
        },
        type: ['pharmacie'],
        skip: skipAndTake.skip,
        take: skipAndTake.take,
      },
    });
  }, [selectionvalue, query, skipAndTake, sortBy]);

  return (
    <>
      <Box>
        <SearchInput
          value={debouncedText}
          searchInFullWidth={'xl'}
          noPrefixIcon={true}
          onChange={onChangeSearchText}
          placeholder={'Entrer vos critères de recherche'}
          outlined={true}
        />
      </Box>
      <EnhancedTableFriends
        columns={selectionvalue === 'Pharmacie' ? getColumnsPharmacie : columns}
        handleClick={handleClick}
        updateSkipAndTake={updateSkipAndTake}
        data={
          selectionvalue === 'Pharmacie'
            ? resultPharmacie &&
              resultPharmacie.data &&
              resultPharmacie.data.search &&
              resultPharmacie.data.search.data
              ? resultPharmacie.data.search.data
              : []
            : results && results.data && results.data.search && results.data.search.data
              ? results.data.search.data
              : []
        }
        total={
          selectionvalue === 'Pharmacie'
            ? resultPharmacie &&
              resultPharmacie.data &&
              resultPharmacie.data.search &&
              resultPharmacie.data.search.total
              ? resultPharmacie.data.search.total
              : 0
            : results && results.data && results.data.search && results.data.search.total
              ? results.data.search.total
              : 0
        }
        userIdsSelected={userIdsSelected}
        skipAndTake={skipAndTake}
        sortBy={sortBy}
        setSortBy={setSortBy}
      />
    </>
  );
};
export default TableAndColumns;
