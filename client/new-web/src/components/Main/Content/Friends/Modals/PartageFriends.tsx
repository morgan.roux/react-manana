import { Box, Button, Checkbox, FormControlLabel } from '@material-ui/core';
import moment from 'moment';
import React, { FC, useContext, useEffect, useState } from 'react';
import { useStyles } from './Style';
import { CustomModal } from '../../../../Common/CustomModal';
import { Column } from '../../../../Dashboard/Content/Interface';
import TablepartageGauche from './ColumsFriends';
import TableAndColumns from './TableAndColumnsFriends';
import { differenceBy } from 'lodash';
import { useApolloClient, useLazyQuery, useMutation } from '@apollo/client';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { DO_CREATE_UPDATE_PARTAGE } from '../../../../../graphql/Partage/mutation';
import {
  CREATE_UPDATE_PARTAGE,
  CREATE_UPDATE_PARTAGEVariables,
} from '../../../../../graphql/Partage/types/CREATE_UPDATE_PARTAGE';
import { getUser } from '../../../../../services/LocalStorage';
import { PartageType } from '../../../../../types/graphql-global-types';
import Backdrop from '../../../../Common/Backdrop';

interface propsFriend {
  openModal: boolean;
  persIds?: string[];
  setpersIds?: (value: any) => void;
  handleClose: (value: boolean) => void;
  idActualite: string;
  codeItem: string;
  actualite?: any;
  refetch?: any;
}
const Partage: FC<propsFriend> = ({
  codeItem,
  openModal,
  persIds,
  setpersIds,
  idActualite,
  handleClose,
  actualite,
  refetch,
}) => {
  const [value, setValue] = React.useState('GroupesAmis');
  const classes = useStyles({});
  const client = useApolloClient();
  const userId = getUser();
  const [userIdsSelected, setUserIdsSelected] = useState<any[]>(
    persIds && persIds.length ? persIds : [],
  );
  const [checkedrec, setCheckedrec] = React.useState<boolean>(false);

  const handleChangerec = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCheckedrec(!checkedrec);
  };
  const [clausemodal, setclausemodal] = useState<boolean>(false);
  const result = persIds && differenceBy(userIdsSelected, persIds);

  useEffect(() => {
    setUserIdsSelected(['']);
    setUserIdsSelected(persIds || []);
  }, [persIds]);
  console.log('userIdsSelected', userIdsSelected, 'codeItem', codeItem);

  const [createAndUpdatePartage, { loading: loadingPartage }] = useMutation<
    CREATE_UPDATE_PARTAGE,
    CREATE_UPDATE_PARTAGEVariables
  >(DO_CREATE_UPDATE_PARTAGE, {
    onCompleted: data => {
      refetch();
      if (data && data.createUpdatePartage) {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Partage réussi',
        });
      }
      setclausemodal(false);
      setCheckedrec(false);
      setUserIdsSelected(['']);
    },
    onError: error => {
      console.log('error :>> ', error);
      displaySnackBar(client, { isOpen: true, type: 'ERROR', message: error.message });
    },
  });

  const handleConfirm = () => {
    if (value === 'GroupesAmis') {
      createAndUpdatePartage({
        variables: {
          input: {
            codeItem: codeItem,
            idItemAssocie: idActualite,
            idUserPartageant: userId.id,
            type: checkedrec ? PartageType.RECOMMANDATION : PartageType.PARTAGE,
            dateHeurePartage: moment().toDate(),
            idsGroupesAmis: userIdsSelected,
          },
        },
      });
    } else {
      createAndUpdatePartage({
        variables: {
          input: {
            codeItem: codeItem,
            idItemAssocie: idActualite,
            idUserPartageant: userId.id,
            type: checkedrec ? PartageType.RECOMMANDATION : PartageType.PARTAGE,
            dateHeurePartage: moment().toDate(),
            idsPharmacies: userIdsSelected,
          },
        },
      });
    }
  };

  useEffect(() => {
    setclausemodal(openModal);
  }, [openModal]);

  useEffect(() => {
    setUserIdsSelected([]);
  }, [value]);

  return (
    <>
      {loadingPartage && <Backdrop />}

      <CustomModal
        open={clausemodal}
        setOpen={handleClose}
        title={'Partage'}
        withBtnsActions={false}
        closeIcon={true}
        headerWithBgColor={true}
        maxWidth="lg"
        fullWidth={false}
        centerBtns={true}
      >
        <Box display="flex" flexDirection="column">
          <Box className={classes.espace}>
            <Box className={classes.largeur}>
              <TablepartageGauche value={value} setValue={setValue} />
            </Box>
            <Box className={classes.mainTableUser}>
              <TableAndColumns
                selectionvalue={value}
                userIdsSelected={userIdsSelected}
                setUserIdsSelected={setUserIdsSelected}
              />
            </Box>
          </Box>
          <Box display="flex" justifyContent="center" className={classes.actionButtonContent}>
            <FormControlLabel
              control={
                <Checkbox
                  checked={checkedrec}
                  onChange={handleChangerec}
                  name="recommandation"
                  color="primary"
                />
              }
              label="Recommandation"
            />
            <Button
              variant="contained"
              color="secondary"
              onClick={handleConfirm}
              disabled={userIdsSelected.length === 0 ? true : false}
            >
              Partager
            </Button>
          </Box>
        </Box>
      </CustomModal>
    </>
  );
};

export default Partage;
