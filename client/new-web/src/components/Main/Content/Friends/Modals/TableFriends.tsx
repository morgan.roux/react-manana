import React, { FC, useState, MouseEvent } from 'react';
import {
  TableContainer,
  Table,
  TableBody,
  TableCell,
  Checkbox,
  TableRow,
  TablePagination,
} from '@material-ui/core';
import { Column } from '../../../../Dashboard/Content/Interface';
import TableHead from '../../../../User/UserSelect/Users/Table/TableHead';
import { SkipAndTake, SortBy } from '../../../../User/UserSelect/Users/Users';
import { useStyles } from './Style';
import PaginationActions from '../../../../User/UserSelect/Users/Table/PaginationActions/TablePaginationActions';

interface EnhancedTableProps {
  columns: Column[] | undefined;
  data: any[];
  userIdsSelected?: string[];
  total: number;
  skipAndTake: SkipAndTake;
  sortBy: SortBy;
  setSortBy: (value: any) => void;
  handleClick: (rowId: string, row: any) => (event: MouseEvent<unknown>) => void;
  updateSkipAndTake: (skip: number, take: number) => void;
}

const EnhancedTableFriends: FC<EnhancedTableProps> = ({
  columns,
  data,
  userIdsSelected,
  total,
  skipAndTake,
  sortBy,
  setSortBy,
  handleClick,
  updateSkipAndTake,
}) => {
  const classes = useStyles({});

  return (
    <div className={classes.root}>
      <TableContainer className={classes.tableWrapper}>
        <Table
          className={classes.table}
          aria-labelledby="tableTitle"
          size={'medium'}
          stickyHeader={true}
        >
          <TableHead columns={columns} sortBy={sortBy} setSortBy={setSortBy} />
          <TableBody>
            {data && data.length
              ? data.map((row: any, indexInArray: any) => {
                  if (row) {
                    const index = row.id || indexInArray;
                    const labelId = `enhanced-table-checkbox-${index}`;

                    return (
                      <TableRow hover={true} role="checkbox" tabIndex={-1} key={`row-${index}`}>
                        <TableCell padding="checkbox" key={indexInArray}>
                          <Checkbox
                            onClick={handleClick(row.id, row)}
                            checked={userIdsSelected && userIdsSelected.includes(row.id)}
                            inputProps={{ 'aria-labelledby': labelId }}
                          />
                        </TableCell>
                        {columns
                          ? columns.map((column, columnIndex) => {
                              return (
                                <TableCell
                                  key={`row-${index}-${columnIndex}`}
                                  className={classes.tableCellPadding}
                                  align={column.centered ? 'center' : 'left'}
                                >
                                  {column.renderer
                                    ? column.renderer(row)
                                    : row[column.name]
                                    ? row[column.name]
                                    : '-'}
                                </TableCell>
                              );
                            })
                          : null}
                      </TableRow>
                    );
                  }
                })
              : ''}
          </TableBody>
        </Table>
        <div></div>
      </TableContainer>
      {data && data.length > 0 && (
        <TablePagination
          className={classes.tablePagination}
          labelDisplayedRows={({ from, to, count }) =>
            `${from}-${to === -1 ? count : to} sur ${count !== -1 ? count : to}`
          }
          labelRowsPerPage="Nombre par page"
          component="div"
          count={total}
          rowsPerPage={skipAndTake && skipAndTake.take}
          page={skipAndTake && skipAndTake.skip / skipAndTake.take}
          backIconButtonProps={{
            'aria-label': 'Page Précédentedbsfbdsbdbsb',
          }}
          nextIconButtonProps={{
            'aria-label': 'Page suivante',
          }}
          onChangePage={(_, newPage) => {
            const currentPage = newPage <= 0 ? 0 : newPage;
            console.log('currentPage', currentPage);
            return updateSkipAndTake(currentPage * skipAndTake.take, skipAndTake.take);
          }}
          onChangeRowsPerPage={e =>
            updateSkipAndTake(skipAndTake.skip, parseInt(e.target.value, 10))
          }
          ActionsComponent={PaginationActions}
        />
      )}
    </div>
  );
};

export default EnhancedTableFriends;
