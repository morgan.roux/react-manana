import { useLazyQuery, useQuery } from '@apollo/client';
import { Link } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import React, { Fragment } from 'react';
import { useEffect } from 'react';
import { FC } from 'react';
import {
  DO_SEARCH_ALL_PHARMACIES,
  GET_PHARMACIES,
  GET_SEARCH_CUSTOM_CONTENT_PHARMACIE,
} from '../../../../../graphql/Pharmacie/query';
import { PHARMACIES } from '../../../../../graphql/Pharmacie/types/PHARMACIES';
import {
  SEARCH_ALL_PHARMACIES,
  SEARCH_ALL_PHARMACIESVariables,
} from '../../../../../graphql/Pharmacie/types/SEARCH_ALL_PHARMACIES';
import {
  SEARCH_CUSTOM_CONTENT_PHARMACIE,
  SEARCH_CUSTOM_CONTENT_PHARMACIEVariables,
} from '../../../../../graphql/Pharmacie/types/SEARCH_CUSTOM_CONTENT_PHARMACIE';
import { CustomModal } from '../../../../Common/CustomModal';
import CustomContent from '../../../../Common/newCustomContent';
import SearchInput from '../../../../Common/newCustomContent/SearchInput';
import WithSearch from '../../../../Common/newWithSearch/withSearch';
import { Column } from '../../../../Dashboard/Content/Interface';

interface Modalprops {
  openModal: boolean;
  handleClose: () => void;
}

const InviteFriend: FC<Modalprops> = ({ openModal, handleClose }) => {
  const theme = useTheme();

  const columns: Column[] = [
    {
      name: 'cip',
      label: 'CIP',
      renderer: (value: any) => {
        return value.cip ? value.cip : '-';
      },
    },
    {
      name: 'departement.nom',
      label: 'Platforme',
      renderer: (value: any) => {
        return value.departement && value.departement.nom ? value.departement.nom : '-';
      },
    },
    {
      name: 'contact',
      label: 'Contrat',
      renderer: (value: any) => {
        return value.contact ? value.contact.mailProf : '-';
      },
    },
    {
      name: 'nom',
      label: 'Nom Pharmacie',
      renderer: (value: any) => {
        console.log('nom', value.nom);

        return value.nom ? value.nom : '-';
      },
    },
    {
      name: 'titulaire.fullName',
      label: 'Titulaire(s)',
      renderer: (value: any) => {
        const handleClick = (titulaireId: string) => () => {
          const titulaires: any[] = value.titulaires || [];
          return titulaires.map((t: any, index: number) => {
            const isMultiple: boolean = titulaires.length > 1 && index < titulaires.length - 1;
            if (t) {
              return (
                <Fragment key={`titulaire_link_${index}`}>
                  <Link
                    color="secondary"
                    component="button"
                    variant="body2"
                    onClick={handleClick(t.id)}
                    style={{ color: theme.palette.secondary.main, textAlign: 'left' }}
                  >
                    {t && `${t.fullName}`}
                    {isMultiple && (
                      <>
                        ,<span style={{ visibility: 'hidden' }}>&nbsp;</span>
                      </>
                    )}
                  </Link>
                </Fragment>
              );
            } else {
              return '-';
            }
          });
        };
      },
    },
    {
      name: 'adresse1',
      label: 'Adresse',
      renderer: (value: any) => {
        return value.adresse1 ? value.adresse1 : '-';
      },
    },
    {
      name: 'ville',
      label: 'Ville',
      renderer: (value: any) => {
        return value.ville ? value.ville : '-';
      },
    },
  ];
  //   const [resultVariable, resultdata] = useLazyQuery<
  //     SEARCH_ALL_PHARMACIES,
  //     SEARCH_ALL_PHARMACIESVariables
  //   >(DO_SEARCH_ALL_PHARMACIES, {
  //     variables: {
  //       take: 14,
  //       type: ['pharmacie'],
  //       skip: 0,
  //       query: { query: { bool: { must: [{ term: { sortie: 0 } }] } } },
  //     },
  //   });

  //   useEffect(() => {
  //     if (openModal) {
  //       resultVariable({
  //         variables: {
  //           take: 14,
  //           type: ['pharmacie'],
  //           skip: 0,

  //         },
  //       });
  //     }
  //   }, [openModal]);

  //const handleClose = () => {};

  const childrenProps: any = {
    columns: columns,
    isSelectable: false,
  };

  return (
    <CustomModal
      open={openModal}
      setOpen={handleClose}
      title={'Inviter'}
      withBtnsActions={false}
      closeIcon={true}
      headerWithBgColor={true}
      maxWidth="lg"
      fullWidth={false}
      //   className={classes.usersModalRoot}
      centerBtns={true}
    //onClickConfirm={handleConfirm}
    //actionButton="Partager"
    >
      <SearchInput searchPlaceholder="Rechercher une pharmacie" />
      {/* <CustomContent
          //selected={selected}
          //setSelected={setSelected}
          // checkedItemsQuery={{
          //   name: 'checkedsPharmacie',
          //   type: 'pharmacie',
          //   query: GET_CHECKEDS_PHARMACIE,
          // }}
          listResult={listResult}
          columns={columns}
          isSelectable={true}
        /> */}
      <WithSearch
        WrappedComponent={CustomContent}
        type="pharmacie"
        searchQuery={DO_SEARCH_ALL_PHARMACIES}
        props={childrenProps}
        optionalMust={[]}
      />
    </CustomModal>
  );
};

export default InviteFriend;
