import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import { Link } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import React, { Fragment, useContext, useState } from 'react';
import { useEffect } from 'react';
import { FC } from 'react';
import { useHistory, useParams } from 'react-router';
import {
  DO_CREATE_INVITE_FRIENDS,
  DO_INVITE_PHARMACIE,
} from '../../../../../graphql/GroupAmisDetail/mutations';
import {
  CREATE_INVITE_PHARMACIE,
  CREATE_INVITE_PHARMACIEVariables,
} from '../../../../../graphql/GroupAmisDetail/types/CREATE_INVITE_PHARMACIE';
import { GET_SEARCH_CUSTOM_CONTENT_PHARMACIE } from '../../../../../graphql/Pharmacie/query';
import { getGroupement, getPharmacie } from '../../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import Backdrop from '../../../../Common/Backdrop';
import { CustomModal } from '../../../../Common/CustomModal';
import { groupeClientSortParam } from '../../../../Common/newWithSearch/SortParams';
import WithSearch from '../../../../Common/newWithSearch/withSearch';
import ListFriends from './ListFriends';
import { useFriends } from './useFriends';

interface Modalprops {
  openModal: boolean;
  handleClose: () => void;
  listResult: any;
}

const InviteFriend: FC<Modalprops> = ({ openModal, handleClose, listResult }) => {
  const theme = useTheme();
  const { idgroup }: any = useParams();
  const getsidGroupement = getGroupement();
  const getsidpharmacie = getPharmacie();
  const history = useHistory();

  const lien = history.location.pathname.includes('/membre') ? true : false;
  let idPharmcie = [];
  if (lien) {
    idPharmcie = listResult?.data?.search?.data?.map(select => select?.pharmacie?.id) ?? [];
  }

  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const { columns, deleteRow, onClickConfirmDelete, selected, setSelected } = useFriends(
    setOpenDeleteDialog,
  );
  useEffect(() => {
    setSelected([]);
  }, [openModal]);
  const client = useApolloClient();

  // hooks

  const [createInvitationPharmacies, { loading: invitationLoading }] = useMutation<
    CREATE_INVITE_PHARMACIE,
    CREATE_INVITE_PHARMACIEVariables
  >(DO_INVITE_PHARMACIE, {
    onCompleted: data => {
      if (data && data.invitePharmacies) {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: ' Invitation envoyée avec succès',
        });
      }
      handleClose();
      if (listResult && listResult.refetch) {
        listResult.refetch();
      }
    },
  });

  const idSelected = selected && selected.map(select => select.id);
  const handleConfirme = () => {
    createInvitationPharmacies({
      variables: {
        id: idgroup,
        idsPharmacies: idSelected,
      },
    });
  };
  return (
    <CustomModal
      open={openModal}
      setOpen={handleClose}
      title={'Inviter des amis'}
      withBtnsActions={true}
      closeIcon={true}
      headerWithBgColor={true}
      maxWidth="lg"
      withCancelButton={false}
      disabledButton={idSelected.length > 0 ? false : true}
      fullWidth={false}
      actionButtonTitle={'Inviter'}
      centerBtns={true}
      onClickConfirm={handleConfirme}
    >
      {invitationLoading && <Backdrop />}
      <WithSearch
        WrappedComponent={ListFriends}
        type="pharmacie"
        searchQuery={GET_SEARCH_CUSTOM_CONTENT_PHARMACIE}
        optionalMust={[{ term: { idGroupement: getsidGroupement.id } }, { term: { sortie: 0 } }]}
        optionalMustNot={[{ terms: { _id: idPharmcie } }]}
        take={5}
        skip={0}
        props={{ columns, setSelected, selected }}
      />
    </CustomModal>
  );
};

export default InviteFriend;
