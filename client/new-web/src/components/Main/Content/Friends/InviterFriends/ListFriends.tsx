import { Button } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import React, { Fragment, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import CustomContent from '../../../../Common/newCustomContent/CustomContent';
import SearchInput from '../../../../Common/newCustomContent/SearchInput';
import { useFriends } from './useFriends';

interface IdeeBonnePratiqueProps {
  listResult: any;
  setIdSelected: any;
  test: string;
  setSelected: () => void;
  columns: any;
  selected: any;
}

const ListFriends: React.FC<IdeeBonnePratiqueProps> = ({
  selected,
  setSelected,
  listResult,
  columns,
  setIdSelected,
  test,
}) => {
  const friendList = (
    <CustomContent
      selected={selected}
      setSelected={setSelected}
      {...{ listResult, columns }}
      isSelectable={true}
      showResetFilters={false}
    />
  );

  return (
    <div>
      <div>
        <SearchInput searchPlaceholder=" Rechercher des amis" />
      </div>
      {friendList}
    </div>
  );
};

export default ListFriends;
