import React, { FC, useState, useEffect, useContext } from 'react';
import Box from '@material-ui/core/Box';
import useStyles from './styles';
import Chip from '@material-ui/core/Chip';
import TagFacesIcon from '@material-ui/icons/TagFaces';

interface LaboratoireInterface {
  id: number;
  nom: string;
  nbArticle: number;
  __typename: string;
}
interface ChipData {
  data: LaboratoireInterface[];
  deleteChips: (item: LaboratoireInterface) => void;
}

const SearchChips: FC<ChipData> = ({ data, deleteChips }) => {
  const classes = useStyles({});
  const handleDelete = (chipToDelete: LaboratoireInterface) => () => {
    deleteChips(chipToDelete);
  };

  return (
    <Box className={classes.root}>
      {data.map(item => {
        let icon;

        return (
          <Chip
            key={item.__typename + item.id}
            icon={icon}
            label={item.nom}
            onDelete={handleDelete(item)}
            className={classes.chip}
          />
        );
      })}
    </Box>
  );
};

export default SearchChips;
