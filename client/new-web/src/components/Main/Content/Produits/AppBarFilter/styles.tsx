import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      '& *': {
        letterSpacing: 0,
      },
    },
    appbar: {
      color: theme.palette.primary.main,
      background: theme.palette.common.white,
    },
    menuButton: {},
    activeAffichage: {
      color: theme.palette.secondary.main,
    },
    contentSearch: {
      flexGrow: 1,
      display: 'flex',
      justifyContent: 'start',
      alignItems: 'center',
    },
    title: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: '1rem',
    },
    affichage: {
      fontFamily: 'Roboto',
      fontSize: '0.75rem',
      marginRight: '0.25rem',
    },
    btnPasserCmd: {
      fontWeight: 'bold',
      fontSize: 14,
      fontFamily: 'Roboto',
    },
    toolbar: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    deleteButton: {
      background: '#E0E0E0',
      color: theme.palette.common.black,
      marginRight: theme.spacing(3),
    },
    viewButton: {
      marginRight: theme.spacing(3),
    },
  }),
);

export default useStyles;
