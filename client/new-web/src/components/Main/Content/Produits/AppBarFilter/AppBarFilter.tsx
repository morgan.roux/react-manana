import React, { FC, useState, useEffect, useContext } from 'react';
import Box from '@material-ui/core/Box';
import useStyles from './styles';
import classNames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import ViewListIcon from '@material-ui/icons/ViewList';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import ButtonGoToCommand from '../../OperationCommerciale/ButtonGoToCommand';
import CustomButton from '../../../../Common/CustomButton';
import {
  ViewModule,
  ViewList,
  Delete,
  Add,
  ArrowBack,
  Replay,
  FilterList,
  Filter1,
} from '@material-ui/icons';
import { ConfirmDeleteDialog } from '../../../../Common/ConfirmDialog';
import { useMutation, useApolloClient, useQuery } from '@apollo/client';
import {
  softDeleteCanalArticles,
  softDeleteCanalArticlesVariables,
} from '../../../../../graphql/ProduitCanal/types/softDeleteCanalArticles';
import { Filter } from '@app/ui-kit/components/pages/LaboratoirePage/common/filter';
import { DELETE_CANAL_ARTICLES } from '../../../../../graphql/ProduitCanal/mutation';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { GET_CHECKEDS_PRODUIT } from '../../../../../graphql/ProduitCanal/local';
import { GET_SEARCH_GESTION_PRODUIT_CANAL } from '../../../../../graphql/ProduitCanal/query';
import _ from 'lodash';
import { getUser } from '../../../../../services/LocalStorage';
import { AppAuthorization } from '../../../../../services/authorization';
import { CustomFullScreenModal } from '../../../../Common/CustomModal';
import CreationProduit from '../../../../Dashboard/GestionProduits/CreationProduit';
import SearchContent from '../../../../Common/Filter/SearchContent';
import ProduitFilter from './ProduitFilter';
import FilterAlt from '../../../../../assets/icons/todo/filter_alt.svg';
import { canals, tris } from './ProduitFilter/ProduitFilter';
import { ContentContext, ContentStateInterface } from '../../../../../AppContext';

interface ChipsInterface {
  id: number;
  nom: string;
  nbArticle: number;
  __typename: string;
}

interface AppBarFilterProps {
  changeView: () => void;
  view: string;
  match: {
    params: {
      type: string | undefined;
      id: string | undefined;
      view: string;
      idOperation: string | undefined;
    };
  };
  listResult?: any;
  history: {
    location: { state: any };
  };
}

const AppBarFilter: FC<AppBarFilterProps & RouteComponentProps> = ({
  changeView,
  view,
  location,
  match,
  history: {
    location: { state },
    push,
  },
  listResult,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const idOperation = match && match.params && match.params.idOperation;
  const isInLaboratoireProduits = location.pathname.includes('/laboratoires');
  const isInGestionProduits = location.pathname.includes('/gestion-produits');
  const isInOperationCommande = window.location.hash.includes('#/operation-produits');
  const [openConfirmDialog, setOpenConfirmDialog] = useState(false);
  const [openCreateProduit, setOpenCreateProduit] = useState(false);
  const [sort, setSort] = useState<'asc' | 'desc'>('asc');
  const [isAfterCreateUpdate, setIsAfterCreateUpdate] = useState(false);

  const {
    content: { refetch },
  } = useContext<ContentStateInterface>(ContentContext);

  const [filters, setFilters] = useState({
    canal: [canals[2]],
    reactions: [],
    remises: [],
    tri: [tris[1]],
  });

  const user = getUser();
  const auth = new AppAuthorization(user);

  // change view mode : card or list
  const handleChangeView = () => {
    changeView();
  };

  useEffect(() => {
    if (isAfterCreateUpdate) {
      listResult.refetch();
      setIsAfterCreateUpdate(false);
    }
  }, [isAfterCreateUpdate, openCreateProduit]);

  const checkedsQueryResult = useQuery(GET_CHECKEDS_PRODUIT);
  const checkedItems =
    (checkedsQueryResult && checkedsQueryResult.data && checkedsQueryResult.data.checkedsProduit) ||
    [];

  const goToCreateProduit = () => {
    setOpenCreateProduit(true);
    /* history.push('/gestion-produits/create'); */
  };
  const openCreateModal = (isOpen: boolean) => () => {
    setOpenCreateProduit(isOpen);
  };

  const [doDeleteArticles, doDeleteArticlesResult] = useMutation<
    softDeleteCanalArticles,
    softDeleteCanalArticlesVariables
  >(DELETE_CANAL_ARTICLES, {
    update: (cache, { data }) => {
      if (data && data.softDeleteCanalArticles) {
        if (listResult && listResult.variables) {
          const query = cache.readQuery<any, any>({
            query: GET_SEARCH_GESTION_PRODUIT_CANAL,
            variables: listResult.variables,
          });
          if (query && query.search && query.search.data) {
            cache.writeQuery({
              query: GET_SEARCH_GESTION_PRODUIT_CANAL,
              data: {
                search: {
                  ...query.search,
                  ...{
                    data: _.differenceWith(query.search.data, checkedItems, _.isEqual),
                  },
                },
              },
              variables: listResult.variables,
            });
          }
        }
        // TODO: Migration
       /* (client as any).writeData({
          data: {
            checkedsProduit: null,
          },
        });*/
      }
    },
    onCompleted: data => {
      const snackBarData: SnackVariableInterface = {
        type: 'SUCCESS',
        message: 'Article supprimé avec succès',
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
      setOpenConfirmDialog(false);
    },
  });

  const handleOpenConfirmDialog = () => {
    setOpenConfirmDialog(true);
  };

  const handleDelete = () => {
    doDeleteArticles({
      variables: { ids: checkedItems && checkedItems.map(item => item.id) },
    });
  };

  const clickBack = () => {
    if (state.idLaboratoire)
      push(`/laboratoires/${state.idLaboratoire}/${'partenaire'}`, {
        goBack: true,
      });
  };

  const handleFilterChange = (data: any) => {
    // TODO: Migration
    /*(client as any).writeData({
      data: {
        sort: {
          sortItem: {
            label: 'tri',
            name: filters.tri[0].name,
            direction: sort,
            active: true,
            __typename: 'sortItem',
          },
          __typename: 'sort',
        },
        filters: {
          idCheckeds: [],
          idLaboratoires: [],
          idFamilles: [],
          idGammesCommercials: [],
          idCommandeCanal: filters.canal[0].code,
          idCommandeCanals: [],
          idCommandeTypes: [],
          idActualiteOrigine: null,
          idOcCommande: null,
          idSeen: null,
          idTodoType: null,
          sortie: null,
          laboType: null,
          reaction: filters.reactions.length > 0 ? filters.reactions.map((el: any) => el.name) : [],
          idRemise: filters.remises.length > 0 ? filters.remises.map((el: any) => el.name) : [],
          __typename: 'filters',
        },
      },
    });*/
  };

  const handleSortChange = () => {
    setSort(prevState => (prevState === 'asc' ? 'desc' : 'asc'));
    handleFilterChange(null);
  };

  return (
    <Box className={classes.root}>
      <AppBar position="static" className={classes.appbar}>
        <Toolbar className={classes.toolbar}>
          {state && state.idLaboratoire && (
            <ArrowBack style={{ cursor: 'pointer' }} onClick={clickBack} />
          )}
          <Typography className={classes.title}>
            {isInGestionProduits || idOperation
              ? 'Liste des produits'
              : 'Liste de tous les catalagues produits'}
          </Typography>
          {listResult?.data?.search?.data && (isInGestionProduits || idOperation) ? (
            <>
              <Box>
                <CustomButton
                  color={view === 'card' ? 'primary' : 'inherit'}
                  variant={view === 'card' ? 'contained' : 'outlined'}
                  onClick={handleChangeView}
                  startIcon={<ViewModule />}
                  className={classes.viewButton}
                >
                  Vue colonne
                </CustomButton>
                <CustomButton
                  color={view === 'list' ? 'primary' : 'inherit'}
                  variant={view === 'list' ? 'contained' : 'outlined'}
                  onClick={handleChangeView}
                  startIcon={<ViewList />}
                >
                  Vue tableau
                </CustomButton>
              </Box>
              <Box>
                {checkedItems && checkedItems.length > 0 && (
                  <CustomButton
                    className={classes.deleteButton}
                    startIcon={<Delete />}
                    onClick={handleOpenConfirmDialog}
                    disabled={!auth.isAuthorizedToDeleteProduct()}
                  >
                    Supprimer la selection
                  </CustomButton>
                )}
                {isInGestionProduits && (
                  <CustomButton
                    color="secondary"
                    startIcon={<Add />}
                    onClick={goToCreateProduit}
                    disabled={!auth.isAuthorizedToAddProduct()}
                  >
                    Ajouter un produit
                  </CustomButton>
                )}
                {isInOperationCommande && <ButtonGoToCommand />}

                <ConfirmDeleteDialog
                  title={`Suppression de la sélection (${(checkedItems && checkedItems.length) ||
                    0})`}
                  content="Êtes-vous sûr de vouloir supprimer ces produits"
                  open={openConfirmDialog}
                  setOpen={setOpenConfirmDialog}
                  onClickConfirm={handleDelete}
                  isLoading={doDeleteArticlesResult.loading}
                />
              </Box>
            </>
          ) : listResult?.data?.search?.data ? (
            <Box display="flex">
              {!isInLaboratoireProduits && (
                <Box display="flex" alignItems="center">
                  {idOperation && <ButtonGoToCommand />}
                  <Typography className={classes.affichage}>Type d'affichage :</Typography>
                </Box>
              )}

              <Box marginLeft="10px" alignItems="center" display="flex">
                <IconButton
                  color={view === 'card' ? 'primary' : 'default'}
                  edge="start"
                  className={classNames(classes.menuButton)}
                  onClick={handleChangeView}
                >
                  <ViewModuleIcon />
                </IconButton>
                <IconButton
                  color={view === 'list' ? 'primary' : 'default'}
                  edge="start"
                  className={classNames(classes.menuButton)}
                  onClick={handleChangeView}
                >
                  <ViewListIcon />
                </IconButton>
                {isInLaboratoireProduits && (
                  <>
                    <IconButton onClick={handleSortChange}>
                      {sort === 'asc' ? (
                        <FilterList />
                      ) : (
                        <FilterList style={{ transform: 'rotate(180deg)' }} />
                      )}
                    </IconButton>
                    <Box margin="0px 16px">
                      <Filter
                        filters={[]}
                        children={<ProduitFilter setFilters={setFilters} filters={filters} />}
                        onChange={handleFilterChange}
                        filterIcon={<img src={FilterAlt} />}
                      />
                    </Box>

                    <IconButton onClick={() => refetch()} disabled={!refetch}>
                      <Replay />
                    </IconButton>
                    <SearchContent placeholder="Rechercher un produit" />
                  </>
                )}
              </Box>
            </Box>
          ) : (
            ''
          )}
        </Toolbar>
      </AppBar>
      {openCreateProduit && (
        <CustomFullScreenModal
          open={openCreateProduit}
          setOpen={setOpenCreateProduit}
          title={`Produit`}
          withBtnsActions={false}
        >
          <CreationProduit
            openCloseModal={setOpenCreateProduit}
            setIsAfterCreateUpdate={setIsAfterCreateUpdate}
          />
        </CustomFullScreenModal>
      )}
    </Box>
  );
};

export default withRouter(AppBarFilter);
