import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    MontserratBold: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
    },
    underlined: {
      textDecoration: 'underline',
    },

    pink: {
      color: theme.palette.secondary.main,
    },
    MontserratRegular: {
      fontFamily: 'Montserrat',
      fontWeight: 400,
    },
    secondary: {
      color: theme.palette.secondary.main,
    },
  }),
);
export default useStyles;
