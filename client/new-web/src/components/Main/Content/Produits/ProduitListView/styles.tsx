import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      overflowX: 'auto',
      width: 'calc(100vw - 276px)',
      overflow: 'hidden',
    },
    ocProduit: {
      height: 'calc(100vh - 435px)',
      overflowX: 'auto',
      overflow: 'hidden',
      width: 'calc(100vw - 276px)',
    },
    catalogueProduits: {
      height: 'calc(100vh - 257px)',
      width: 'calc(100vw - 276px)',
      overflowX: 'auto',
      overflow: 'hidden',
    },
    pink: {
      // color: theme.palette.pink.main,
    },
    green: {
      // color: theme.palette.green.main,
    },
    secondary: {
      color: theme.palette.secondary.main,
    },
    panierListeButtonLeft: {
      background: '#EFEFEF',
      textTransform: 'none',
      fontFamily: 'Montserrat',
      fontSize: '0.75rem',
      marginRight: theme.spacing(1),
      minWidth: 116,
    },
    rowMargin: {
      borderSpacing: '16px',
      paddingTop: '16px',
    },
  }),
);

export default useStyles;
