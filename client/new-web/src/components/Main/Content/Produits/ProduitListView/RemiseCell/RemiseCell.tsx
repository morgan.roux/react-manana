import React, { FC, useEffect, useState } from 'react';
import useStyles from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Panier } from '../../../../../../graphql/Panier/types/Panier';
import { Panier_PanierLignes_panierLignes } from '../../../../../../graphql/Panier/types/Panier_PanierLignes';
import {
  ProduitCanal,
  ProduitCanal_remises,
} from '../../../../../../graphql/ProduitCanal/types/ProduitCanal';
import classnames from 'classnames';
import RemisePalier from '../../../Panier/RemisePalier/RemisePalier';
import { useQuery } from '@apollo/client';
import ICurrentPharmacieInterface from '../../../../../../Interface/CurrentPharmacieInterface';
import { GET_CURRENT_PHARMACIE } from '../../../../../../graphql/Pharmacie/local';
import { myPaniers, myPaniersVariables } from '../../../../../../graphql/Panier/types/myPaniers';
import { GET_MY_PANIERS } from '../../../../../../graphql/Panier/query';
import { GET_OPERATION_PANIER } from '../../../../../../graphql/OperationCommerciale/local';
import { GET_LOCAL_PANIER } from '../../../../../../graphql/Panier/local';
import { PharmacieMinimInfo } from '../../../../../../graphql/Pharmacie/types/PharmacieMinimInfo';
interface RemiseCellProps {
  currentCanalArticle: ProduitCanal | null;
  remiseOnly?: boolean;
  type?: string;
  useLocalPanier?: boolean;
  match: {
    params: {
      idOperation: string | undefined;
    };
  };
  isView?: boolean;
}
const RemiseCell: FC<RemiseCellProps & RouteComponentProps<any, any, any>> = ({
  currentCanalArticle,
  useLocalPanier,
  remiseOnly,
  type,
  match,
  isView,
}) => {
  const classes = useStyles({});
  const [currentRemise, setCurrentRemise] = useState<string>('');
  const [isRemiseOpen, setIsRemiseOpen] = useState<boolean>(false);
  const [prixNet, setPrixNet] = useState<number>(0);
  const [totalNet, setTotalNet] = useState<number>(0);
  const [uG, setUG] = useState<number>(0);
  const idOperation = match && match.params && match.params.idOperation;
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const localPanier = useQuery(GET_LOCAL_PANIER, { skip: !useLocalPanier });
  const myPanierResult = useQuery<myPaniers, myPaniersVariables>(GET_MY_PANIERS, {
    variables: {
      idPharmacie:
        (myPharmacie &&
          myPharmacie.data &&
          myPharmacie.data.pharmacie &&
          myPharmacie.data.pharmacie.id) ||
        '',
      take: 10,
      skip: 0,
    },
    //skip: listResult && listResult.data && !listResult.data.search,
  });

  // currentPharmacie
  const currentPharmacie: PharmacieMinimInfo | null =
    (myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie) || null;

  const operationPanierResult = useQuery(GET_OPERATION_PANIER);
  const operationPanier =
    operationPanierResult &&
    operationPanierResult.data &&
    operationPanierResult.data.operationPanier;

  // currentPanier
  const currentPanier: Panier | null = useLocalPanier
    ? localPanier && localPanier.data && localPanier.data.localPanier
    : ({
      panierLignes:
        (myPanierResult &&
          myPanierResult.data &&
          myPanierResult.data.myPaniers &&
          myPanierResult.data.myPaniers
            .map(panier => panier && panier.panierLignes)
            .reduce((total, value) => {
              return total && total.concat(value);
            }, [])) ||
        [],
    } as Panier) || null;

  const getPanierLigneItem = () => {
    const panier = idOperation ? operationPanier : currentPanier;
    if (currentCanalArticle && panier && panier.panierLignes) {
      const currentLigne = panier.panierLignes.find(
        (ligne: Panier_PanierLignes_panierLignes | null) =>
          ligne && ligne.produitCanal && ligne.produitCanal.id === currentCanalArticle.id,
      );
      return currentLigne ? currentLigne : null;
    } else return null;
  };

  const checkRemise = (quantite: number) => {
    const panier = idOperation ? operationPanier : currentPanier;
    const remises =
      currentCanalArticle && currentCanalArticle.remises ? currentCanalArticle.remises : [];
    const prixPhv =
      currentCanalArticle && currentCanalArticle.prixPhv ? currentCanalArticle.prixPhv : 0;
    let pourcentage = 0;
    let qteMin = 0;
    let qteRemisePanachee = 0;
    if (currentCanalArticle && currentCanalArticle.articleSamePanachees) {
      currentCanalArticle.articleSamePanachees.map(article => {
        if (panier && panier.panierLignes) {
          panier.panierLignes.map(ligne => {
            if (
              article &&
              ligne &&
              ligne.quantite &&
              ligne.produitCanal &&
              ligne.produitCanal.id &&
              ligne.produitCanal.id === article.id
            ) {
              qteRemisePanachee += ligne.quantite;
            }
          });
        }
      });
    }
    if (qteRemisePanachee === 0) {
      qteRemisePanachee = quantite;
    }
    const minimalRemise = remises && remises[0];
    remises.map((remise: ProduitCanal_remises | null) => {
      const pourcentageRemise = remise && remise.pourcentageRemise ? remise.pourcentageRemise : 0;
      const quantiteMin = remise && remise.quantiteMin ? remise.quantiteMin : 0;
      const uG = remise && remise.nombreUg ? remise.nombreUg : 0;
      if (
        remise &&
        remise.quantiteMin &&
        remise.quantiteMax &&
        quantite > 0 &&
        qteRemisePanachee >= remise.quantiteMin &&
        qteRemisePanachee <= remise.quantiteMax
      ) {
        pourcentage = pourcentageRemise;
        qteMin = quantiteMin;
        setUG(uG);
      }
    });
    const prixNet = parseFloat((prixPhv - (prixPhv * pourcentage) / 100).toFixed(2));
    const totalNet = parseFloat((prixNet * quantite).toFixed(2));
    setPrixNet(prixNet);
    setTotalNet(totalNet);
    const result =
      pourcentage > 0
        ? `${pourcentage}%`
        : minimalRemise
          ? `${minimalRemise && minimalRemise.pourcentageRemise}% à partir de ${minimalRemise &&
          minimalRemise.quantiteMin} achetés`
          : '0%';
    return setCurrentRemise(result);
  };

  useEffect(() => {
    const stv = currentCanalArticle && currentCanalArticle.stv ? currentCanalArticle.stv : 0;
    const ligne = getPanierLigneItem();
    if (ligne && ligne.quantite) {
      checkRemise(ligne.quantite);
    } else {
      checkRemise(0);
    }
  }, [currentPanier]);

  const handleRemiseButton = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    e.stopPropagation();
    setIsRemiseOpen(true);
  };

  const handleCloseModal = () => {
    if (isRemiseOpen) setIsRemiseOpen(false);
  };

  if (type && type === 'totalNet') {
    return <div className={classes.secondary}>{`${totalNet}€`}</div>;
  }

  if (type && type === 'prixNet') {
    return <>{`${prixNet}€`}</>;
  }

  if (type && type === 'uG') {
    return <>{`${uG}`}</>;
  }

  if (type && type === 'remise') {
    return (
      <div>
        <div
          className={classnames(classes.MontserratBold, classes.secondary, classes.underlined)}
          onClick={handleRemiseButton}
        >
          {currentRemise}
        </div>
        {isRemiseOpen && (
          <RemisePalier
            handleCloseModal={handleCloseModal}
            article={currentCanalArticle}
            isView={isView}
          />
        )}
      </div>
    );
  }

  return (
    <>
      <th
        className={classnames(classes.MontserratBold, classes.pink, classes.underlined)}
        onClick={handleRemiseButton}
      >
        {currentRemise}
      </th>
      {!remiseOnly && (
        <>
          {' '}
          <th className={classnames(classes.MontserratRegular)}>{`${prixNet}€`}</th>
          <th className={classnames(classes.MontserratBold, classes.pink)}>{`${totalNet}€`}</th>
          <th className={classnames(classes.MontserratRegular)}>{uG}</th>
        </>
      )}

      {isRemiseOpen && (
        <RemisePalier
          handleCloseModal={handleCloseModal}
          article={currentCanalArticle}
          isView={isView}
        />
      )}
    </>
  );
};
export default withRouter(RemiseCell);
