import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    headCell: {
      color: '#878787',
      fontWeight: 400,
      fontFamily: 'Roboto',
      fontSize: '14px',
    },
  }),
);

export default useStyles;
