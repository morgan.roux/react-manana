import React, { FC } from 'react';
import classnames from 'classnames';
import useStyles from './styles';
import { Select, MenuItem, FormControl, Box, IconButton } from '@material-ui/core';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import LastPageIcon from '@material-ui/icons/LastPage';
interface PaginationProps {
  take: number;
  skip: number;
  onSkipChanged: (value: number) => void;
  onTakeChanged: (value: number) => void;
  total: number;
}
const Pagination: FC<PaginationProps> = ({ take, skip, onTakeChanged, onSkipChanged, total }) => {
  const classes = useStyles({});
  const onTakeChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    onTakeChanged(event.target.value as number);
  };

  const onFirst = () => {
    if (skip !== 0) onSkipChanged(0);
  };

  const onPrevious = () => {
    if (skip >= take) {
      skip = skip - take;
      onSkipChanged(skip);
    }
  };

  const onNext = () => {
    skip = skip + take;
    onSkipChanged(skip);
  };

  const onLast = () => {
    if (total >= skip) {
      skip = total - take;
      onSkipChanged(skip);
    }
  };

  const disablePrevBtn = (): boolean => skip === 0;
  const disableNextBtn = (): boolean => skip + take >= total;

  const ROWS_OPTIONS = [12, 24, 36, 48, 96, 240, 480, 960].filter(count => count <= total);

  return (
    <Box className={classnames(classes.flexRow, classes.alignCenter)} height="40px">
      <Box
        display="flex"
        alignItems="center"
        justifyContent="center"
        className={classnames(classes.RobotoRegular, classes.douze)}
        marginRight="16px"
      >
        Nombre par page:
        <FormControl classes={{ root: classes.formControl }}>
          <Select
            value={take}
            disableUnderline={true}
            onChange={onTakeChange}
            className={classes.selectInput}
          >
            {ROWS_OPTIONS.map(option => (
              <MenuItem value={option}>{option}</MenuItem>
            ))}
          </Select>
        </FormControl>
        {skip + 1}-{Math.min(skip + take, total)} sur {total}
      </Box>
      <IconButton size="medium" onClick={onFirst} disabled={disablePrevBtn()}>
        <FirstPageIcon />
      </IconButton>
      <IconButton size="medium" onClick={onPrevious} component="span" disabled={disablePrevBtn()}>
        <ChevronLeftIcon />
      </IconButton>
      <IconButton size="medium" onClick={onNext} component="span" disabled={disableNextBtn()}>
        <ChevronRightIcon />
      </IconButton>
      <IconButton size="medium" onClick={onLast} component="span" disabled={disableNextBtn()}>
        <LastPageIcon />
      </IconButton>
    </Box>
  );
};

export default Pagination;
