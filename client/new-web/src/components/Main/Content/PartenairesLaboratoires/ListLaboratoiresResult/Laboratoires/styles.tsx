import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    item: {
      cursor: 'pointer',
      position: 'relative',
      padding: '21px 16px!important',
    },
    active: {
      background:
        'linear-gradient(239deg, rgb(227, 71, 65) 0%, rgb(227, 65, 104) 100%) transparent',
      color: '#fff!important',
      cursor: 'default',
      '& .MuiTypography-root': {
        color: '#ffffff',
      },
    },
    listTitle: {
      fontSize: '1rem',
      fontWeight: 500,
    },
    colorGrey: {
      color: '#878787',
      marginRight: 8,
    },
    subListTitle: {
      fontSize: '0.75rem',
      fontWeight: 500,
    },
    imageContainer: {
      width: 69,
      height: 69,
      overflow: 'hidden',
    },
    image: {
      width: '100%',
      height: '100%',
      objectFit: 'cover',
      borderRadius: 10,
    },
    switch: {
      position: 'absolute',
      bottom: '10px',
      right: '10px',
    },
  }),
);

export default useStyles;
