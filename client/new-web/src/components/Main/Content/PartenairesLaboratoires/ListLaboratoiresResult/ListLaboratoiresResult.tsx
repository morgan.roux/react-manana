import { Box, Grid } from '@material-ui/core';
import React, { FC, useCallback } from 'react';
import { SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire } from '../../../../../graphql/Laboratoire/types/SEARCH_LABORATOIRE_PARTENAIRE';
import Laboratoire from './Laboratoires/Laboratoire';
import useStyles from './styles';
import { BottomScrollListener } from 'react-bottom-scroll-listener';
import CaptureIne from '../../../../../assets/img/image_default22.png';

interface ListLaboratoiresResultProps {
  updateView: (labo: any) => void;
  tableau: any[];
  currentLabo?: any;
  onScroll: any;
}

const ListLaboratoiresResult: FC<ListLaboratoiresResultProps> = ({
  updateView,
  tableau,
  currentLabo,
  onScroll,
}) => {
  const classes = useStyles({});

  const menuClick = (labo: any) => {
    updateView(labo);
  };

  const handleContainerOnBottom = () => {
    onScroll();
  };

  return (
    <BottomScrollListener onBottom={handleContainerOnBottom}>
      {scrollRef => (
        <Grid item={true} xs={3} id="listLaboGrid" className={classes.root} ref={scrollRef as any}>
          {tableau.map((labo: any) => (
            <Laboratoire
              laboratoire={labo}
              key={JSON.stringify(labo)}
              active={currentLabo ? currentLabo.id === labo.id : false}
              onClick={menuClick}
            />
          ))}
          {!tableau.length && (
            <Box className={classes.none}>
              <Box>
                <Box>
                  <img className={classes.imgDefault} src={CaptureIne} />
                </Box>
              </Box>
            </Box>
          )}
        </Grid>
      )}
    </BottomScrollListener>
  );
};

export default ListLaboratoiresResult;
