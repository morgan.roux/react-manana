import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    cont: {
      flexGrow: 1,
      padding: '40px 20px',
    },
    descBox: {
      padding: '16px',
    },
    card: {
      maxWidth: 250,
      minWidth: 250,
      padding: 0,
    },
    ocTitle: {
      background: '#e34067',
      color: 'white',
      textAlign: 'center',
      padding: '9px',
    },
    imgDefault: {
      maxWidth: '400px',
      width: '100%',
    },
    ocCardContent: {
      padding: 0,
    },
    title: {
      fontSize: '1.375rem',
      color: '#4D4D4D',
      fontWeight: 'bold',
      fontFamily: 'Montserrat',
      margin: '24px 0 10px',
    },
    subTitle: {
      fontSize: '0.875rem',
      margin: '10px 0 24px',
      color: '#4D4D4D',
    },
    dateContainer: {
      color: '#888',
      position: 'relative',
      height: '30px',
      '& span': {
        position: 'absolute',
        right: '10px',
      },
    },
  }),
);

export default useStyles;
