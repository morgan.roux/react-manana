import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      background: 'red',
      fontFamily: 'Montserrat',
    },
    toGrey: {
      color: '#B1B1B1',
      fontWeight: 'bold',
      marginRight: '6px',
    },
    toWhite: {
      color: '#ffffff',
      fontWeight: 'bold',
      marginRight: '6px',
    },
    contentAvatar: {
      overflow: 'hidden',
      width: '108px',
      height: '108px',
    },
    span: {
      marginRight: '16px',
      color: '#B1B1B1',
      fontSize: '0.75rem',
      display: 'flex',
      alignItems: 'center',
      fontFamily: 'Montserrat',
    },
    links: {
      marginTop: '15px',
    },
    title: {
      margin: '0',
      fontWeight: 'bold',
      color: 'white',
      marginBottom: '5px',
      fontSize: '1.25rem',
      fontFamily: 'Montserrat',
    },
    bouton: {
      color: 'white',
      fontWeight: 'bold',
      marginRight: '10px',
      borderColor: 'white',
      fontFamily: 'Montserrat',
    },
    avatar: {
      borderRadius: 10,
      objectFit: 'cover',
      width: '100%',
      height: '100%',
      cursor: 'pointer',
    },
    dropzone: {
      width: 'auto',
      padding: '24px',
      border: '1px dashed #585858',
      borderRadius: 5,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      marginBottom: 24,
    },
  }),
);

export default useStyles;
