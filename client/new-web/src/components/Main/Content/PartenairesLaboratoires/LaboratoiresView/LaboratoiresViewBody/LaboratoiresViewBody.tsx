import React, { FC, useState } from 'react';
import usesStyles from './styles';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import LaboratoireActualites from './LaboratoireActualites/LaboratoireActualites';
import { LABORATOIRE_laboratoire } from '../../../../../../graphql/Laboratoire/types/LABORATOIRE';
import LaboratoireCatalogues from './LaboratoireCatalogues';
import LoaboratoireOperations from './LaboratoireOperations';
import DialogActualite from './DialogActualite';
import CaptureIne from '../../../../../../assets/img/image_default22.png';
import AddIcon from '@material-ui/icons/Add';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire } from '../../../../../../graphql/Laboratoire/types/SEARCH_LABORATOIRE_PARTENAIRE';
import Partenariat from './Partenariat';
import Contact from './Contact';
import Ressource from './Ressource';
import Reclamation from './Reclamation';
import Comments from './Comment';

interface ActualiteInterface {
  titreActualite: string;
  dateActualite: string;
  imageActualite: string;
}

interface LaboratoiresViewsBodyProps {
  currentLabo: SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire | undefined;
  currentView: string;
  refetch?: () => void;
}

const LaboratoiresViewBody: FC<LaboratoiresViewsBodyProps & RouteComponentProps> = ({
  currentLabo,
  currentView,
  history,
  refetch,
}) => {
  const [dialogOpen, setDialogOpen] = useState(false);
  const [currentActu, setCurrentActu] = useState(null);
  const [openFormPartenaire, setOpenFormPartenaire] = useState<boolean>(false);
  const [openFormContact, setOpenFormContact] = useState<boolean>(false);
  const [openDetailContact, setOpenDetailContact] = useState<boolean>(false);

  const creationActualite = () => {
    history.push({
      pathname: '/create/actualite',
      state: { idLaboratoire: currentLabo && currentLabo.id },
    });
  };

  const handleSelectActualite = (actu: any) => {
    setCurrentActu(actu);
    setDialogOpen(true);
  };

  const handleCloseViewActualite = () => {
    setDialogOpen(false);
  };

  const classes = usesStyles();

  return (
    <Box className={classes.rootContent}>
      {currentView === 'actualites' && (
        <Box className={classes.root}>
          <Box>
            {/* S'il n'y a pas d'actualité pour le labo actuel */}
            {currentLabo && currentLabo.actualites && currentLabo.actualites.length === 0 && (
              <Box>
                <img className={classes.imgDefault} src={CaptureIne} />
                <Box>
                  <Typography className={classes.title}>Aucun élément</Typography>
                  <Typography className={classes.subTitle}>
                    Cette section ne contient pas encore d'élément. <br /> Navré pour ce
                    désagrement.
                  </Typography>
                </Box>
              </Box>
            )}
            <Box>
              <Button className={classes.pink} onClick={creationActualite}>
                <AddIcon className={classes.icon} />
                Créer une actualité
              </Button>
            </Box>
          </Box>
        </Box>
      )}
      <Box height="100%" width="100%">
        {currentView === 'partenaire' && (
          <Partenariat
            laboratoire={currentLabo && currentLabo.id ? currentLabo : null}
            open={openFormPartenaire}
            setOpen={setOpenFormPartenaire}
            refetchLabo={refetch}
          />
        )}
        {currentView === 'contacts' && (
          <Contact
            laboratoire={currentLabo && currentLabo.id ? currentLabo : null}
            open={openFormContact}
            setOpen={setOpenFormContact}
            openDetail={openDetailContact}
            setOpenDetail={setOpenDetailContact}
          />
        )}
        {currentView === 'ressources' && (
          <Ressource idLaboratoire={currentLabo && currentLabo.id ? currentLabo.id : null} />
        )}
        {currentView === 'comments' && (
          <Comments idLaboratoire={currentLabo && currentLabo.id ? currentLabo.id : null} />
        )}
        {currentView === 'reclamation' && (
          <Reclamation idLaboratoire={currentLabo && currentLabo.id ? currentLabo.id : null} />
        )}
        {currentView === 'actualites' && (
          <LaboratoireActualites currentLabo={currentLabo} handleSelect={handleSelectActualite} />
        )}
        {currentView === 'catalogues' && <LaboratoireCatalogues currentLabo={currentLabo} />}
        {currentView === 'operations' && <LoaboratoireOperations currentLabo={currentLabo} />}
      </Box>
      <DialogActualite
        actualite={currentActu}
        open={dialogOpen}
        handleClose={handleCloseViewActualite}
      ></DialogActualite>
    </Box>
  );
};

export default withRouter(LaboratoiresViewBody);
