import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },

    contentSectionNote: {
      display: 'flex',
      justifyContent: 'space-between',
      width: '100%',
      marginBottom: 25,
      '@media (max-width:900px)': {
        flexWrap: 'wrap',
      },
    },
    globalTabsContainer: {
      marginTop: 15,
      padding: 6,
      display: 'flex',
      justifyContent: 'center',

      '& .MuiTab-wrapper': {
        fontFamily: 'Montserrat',
        fontWeight: 600,
        fontSize: 14,
        textTransform: 'none',
      },
      '& .MuiTabs-indicator': {
        height: 3,
      },
      '& .MuiPaper-root': {
        boxShadow: '0px 4px 4px 0px rgba(0,0,0,0.25) !important',
      },
    },

    ficheContactContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      marginTop: 15,
      maxWidth: 1920,
      margin: '0 auto',
      '& > div': {
        fontFamily: 'Montserrat',
        fontWeight: 600,
        fontSize: 24,
        alignSelf: 'flex-start',
        '@media (max-width:1024px)': {
          fontSize: 20,
        },
      },
    },

    title: {
      letterSpacing: 0,
      opacity: 1,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 35,
      '@media (max-width:1024px)': {
        fontSize: 28,
      },
    },
    subTitles: {
      fontFamily: 'Montserrat',
      fontWeight: 600,
      fontSize: 24,
      marginTop: 20,
      '@media (max-width:1024px)': {
        fontSize: 20,
      },
    },
    titleName: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 24,
      '@media (max-width:1024px)': {
        fontSize: 20,
      },
    },
    titleSub: {
      fontSize: '1rem',
      color: '#9E9E9E',
    },
    contentLabel: {
      display: 'flex',
      alignItem: 'center',
      marginTop: 12,
    },
    labelTitle: {
      fontFamily: 'Montserrat',
      color: '#9E9E9E',
      fontWeight: 500,
      fontSize: '1rem',
      marginRight: 16,
    },
    labelResult: {
      fontFamily: 'Montserrat',
      color: '#1D1D1D',
      fontWeight: 500,
      fontSize: '1rem',
    },
    subtitle: {
      letterSpacing: 0,
      opacity: 1,
      fontFamily: 'Montserrat',
      fontSize: '1rem',
      fontWeight: 'bold',
      wordBreak: 'break-word',
    },
    sections: {
      display: 'flex',
      flexDirection: 'column',
    },

    sectionImage: {
      display: 'flex',
      '& .MuiAvatar-root': {
        maxWidth: 99,
        maxHeight: 99,
        width: 99,
        height: 99,
        marginRight: 19,
        '& img': {
          width: '100%',
          height: '100%',
          objectFit: 'contain',
        },
      },
    },
    contentSection: {
      width: '100%',
    },
    nameContent: {
      borderRadius: 12,
      padding: '25px',
      border: '1px solid #DCDCDC',
      boxShadow: 'none!important',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
      minHeight: 154,
      width: '100%',

      '& > div:nth-child(2)': {
        alignSelf: 'flex-end',
      },
    },
    sectionImg: {
      width: '46%',
      '& > div': {
        borderRadius: 12,
        padding: '8px 20px',
        border: '1px solid #DCDCDC',
        boxShadow: 'none!important',
        minHeight: 156,
        fontSize: '1rem',
        fontWeight: 500,
      },
      '@media (max-width:900px)': {
        width: '98%',
      },
    },
    contentContainer: {
      maxWidth: 1568,
      margin: '25px auto',
      '& .MuiTab-wrapper': {
        fontFamily: 'Montserrat',
        fontWeight: 600,
        fontSize: 14,
        textTransform: 'none',
      },
      '& .MuiTabs-indicator': {
        height: 3,
      },
      '& .MuiPaper-root': {
        // boxShadow: '0px 4px 4px 0px rgba(0,0,0,0.25) !important',
      },
    },
    inputTitle: {
      fontWeight: 'bold',
      fontSize: 18,
      color: theme.palette.secondary.main,
      marginBottom: 5,
    },
  }),
);

export default useStyles;
