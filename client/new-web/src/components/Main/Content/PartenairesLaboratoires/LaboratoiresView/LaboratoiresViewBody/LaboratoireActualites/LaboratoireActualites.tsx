import { useApolloClient, useQuery } from '@apollo/client';
import { CardActionArea, Divider, Typography } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import moment from 'moment';
import React, { FC } from 'react';
import image from '../../../../../../../assets/img/PDF_file_icon.svg';
import { DO_SEARCH_ACTUALITES } from '../../../../../../../graphql/Actualite';
import {
  SEARCH_ACTUALITES,
  SEARCH_ACTUALITESVariables,
} from '../../../../../../../graphql/Actualite/types/SEARCH_ACTUALITES';
import { SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire } from '../../../../../../../graphql/Laboratoire/types/SEARCH_LABORATOIRE_PARTENAIRE';
import { getGroupement } from '../../../../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../../../../utils/snackBarUtils';
import Backdrop from '../../../../../../Common/Backdrop';
import useStyles from './styles';

interface LaboratoireActualitesProps {
  currentLabo: SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire | undefined;
  handleSelect: Function;
}

const LaboratoireActualites: FC<LaboratoireActualitesProps> = ({ currentLabo, handleSelect }) => {
  const classes = useStyles({});
  const imgExt = ['jpg', 'jpeg', 'png', 'gif', 'svg'];

  const client = useApolloClient();

  const grp = getGroupement();
  const idGroupement = (grp && grp.id) || '';

  const { data, loading } = useQuery<SEARCH_ACTUALITES, SEARCH_ACTUALITESVariables>(
    DO_SEARCH_ACTUALITES,
    {
      fetchPolicy: 'cache-and-network',
      variables: {
        type: ['actualite'],
        filterBy: [
          { term: { idGroupement } },
          { term: { isRemoved: false } },
          { term: { 'laboratoire.id': (currentLabo && currentLabo.id) || '' } },
        ],
      },
      onError: error => {
        console.log('error :>> ', error);
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );

  return (
    <Grid container={true} className={classes.cont}>
      {loading && <Backdrop />}
      <Grid item={true} xs={12}>
        <Grid container={true} spacing={2}>
          {data &&
            data.search &&
            data.search.data &&
            data.search.data.map((actualite: any, index) => {
              if (actualite) {
                const file =
                  actualite.fichierPresentations &&
                  actualite.fichierPresentations.find(item => item && item.publicUrl);
                return (
                  <Grid item={true} key={`actualite_card_${index}`}>
                    <Card className={classes.card}>
                      <CardActionArea onClick={() => handleSelect(actualite)}>
                        <CardMedia
                          component="img"
                          alt=""
                          height="160"
                          className={classes.imageContainer}
                          image={
                            file &&
                              file.publicUrl &&
                              imgExt.includes(
                                file.publicUrl.split('.')[file.publicUrl.split('.').length - 1],
                              )
                              ? file.publicUrl
                              : image
                          }
                          title="Ouvrir actualité"
                        />
                        <Divider variant="middle" />
                        <CardContent>
                          <Typography gutterBottom={true} variant="h5" component="h4">
                            {actualite
                              ? actualite.libelle && actualite.libelle.length > 19
                                ? `${actualite.libelle.substr(0, 16)}...`
                                : actualite.libelle
                              : '-'}
                          </Typography>
                        </CardContent>
                      </CardActionArea>
                      <CardActions className={classes.dateContainer}>
                        <span>{actualite ? moment(actualite.dateCreation).format('LL') : ''}</span>
                      </CardActions>
                    </Card>
                  </Grid>
                );
              }
            })}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default LaboratoireActualites;
