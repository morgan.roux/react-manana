import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    dialog: {
      width: '90vw',
      margin: 'auto',
      position: 'relative',
    },
    cont: {
      width: '80vw',
      minHeight: '70vh',
      '& > div': {
        marginTop: 0,
        padding: 0,
      },
    },
    title: {
      backgroundColor: '#000',
      color: '#fff',
    },
    close: {
      position: 'absolute',
      top: '15px',
      right: '10px',
      color: '#fff',
      fontSize: '2em',
      cursor: 'pointer',
    },
    fileContainer: {
      padding: '10px',
      paddingLeft: '5%',
      justifyContent: 'flex-start',
    },
    file: {
      padding: '5px',
      backgroundColor: '#5d5d5d',
      color: '#fff',
      cursor: 'pointer',
      '& svg': {
        marginLeft: '5px',
        marginBottom: '-8px',
      },
    },
    footerTitle: {
      marginBottom: 0,
      paddingLeft: '5%',
    },
    iframe: {
      width: '100%',
      height: '70vh',
    },
    back: {
      position: 'absolute',
      bottom: '80px',
      right: '25px',
      fontSize: '2em',
      cursor: 'pointer',
    },
  }),
);

export default useStyles;
