import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import {
  AppBar,
  Box,
  CssBaseline,
  Hidden,
  Paper,
  Tab,
  Tabs,
  TextField,
  Typography,
} from '@material-ui/core';
import { InsertComment, LibraryBooks, ShowChart } from '@material-ui/icons';
import React, { ChangeEvent, createRef, FC, Fragment, useEffect, useRef, useState } from 'react';
import ReactQuill from 'react-quill';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import useStyles from './styles';
import moment from 'moment';
import { displaySnackBar } from '../../../../../../../../utils/snackBarUtils';
import {
  CREATE_UPDATE_LABORATOIRE_PARTENAIRE,
  CREATE_UPDATE_LABORATOIRE_PARTENAIREVariables,
} from '../../../../../../../../graphql/LaboratoirePartenaire/types/CREATE_UPDATE_LABORATOIRE_PARTENAIRE';
import { DO_CREATE_UPDATE_LABORATOIRE_PARTENAIRE } from '../../../../../../../../graphql/LaboratoirePartenaire';
import {
  StatutPartenaire,
  TypePartenaire,
} from '../../../../../../../../types/graphql-global-types';
import { CustomModal } from '../../../../../../../Common/CustomModal';
import { isMobile } from '../../../../../../../../utils/Helpers';
import Backdrop from '../../../../../../../Common/Backdrop';
import CustomSelect from '../../../../../../../Common/CustomSelect';
import { CustomDatePicker } from '../../../../../../../Common/CustomDateTimePicker';
import { CustomFormTextField } from '../../../../../../../Common/CustomTextField';

interface PartenariatlProps {
  laboratoire: any | null;
  open: boolean;
  setOpen: (value: boolean) => void;
  refetchLabo?: any;
}

interface LaboratoirePartenaireForm {
  id: string;
  idLaboratoire: string;
  debutPartenaire: any;
  finPartenaire: any;
  typePartenaire: any;
  statutPartenaire: any;
}

const DefinePartenariat: FC<PartenariatlProps & RouteComponentProps> = props => {
  const classes = useStyles({});
  const client = useApolloClient();

  // data to fetch from back
  const { laboratoire, open, setOpen, refetchLabo } = props;

  const initialState: LaboratoirePartenaireForm = {
    id: '',
    idLaboratoire: (laboratoire && laboratoire.id) || '',
    debutPartenaire:
      (laboratoire &&
        laboratoire.laboratoirePartenaire &&
        laboratoire.laboratoirePartenaire.debutPartenaire) ||
      null,
    finPartenaire:
      (laboratoire &&
        laboratoire.laboratoirePartenaire &&
        laboratoire.laboratoirePartenaire.finPartenaire) ||
      null,
    typePartenaire:
      (laboratoire &&
        laboratoire.laboratoirePartenaire &&
        laboratoire.laboratoirePartenaire.typePartenaire) ||
      '',
    statutPartenaire: StatutPartenaire.ACTIVER,
  };
  const [values, setValues] = useState<LaboratoirePartenaireForm>(initialState);

  const typePartenaires = [
    { value: TypePartenaire.NEGOCIATION_EN_COURS, label: 'Négociation en cours' },
    { value: TypePartenaire.NON_PARTENAIRE, label: 'Non partenaire' },
    { value: TypePartenaire.NOUVEAU_REFERENCEMENT, label: 'Nouveau Référencement' },
    { value: TypePartenaire.PARTENARIAT_ACTIF, label: 'Partenariat actif' },
  ];

  console.log('------values------', values);

  const FORM_INPUTS = [
    {
      title: 'Informations',
      inputs: [
        {
          label: 'Date début de partenariat',
          type: 'date',
          name: 'debutPartenaire',
          value: values.debutPartenaire,
          inputType: undefined,
          required: true,
        },
        {
          label: 'Date fin de partenariat',
          type: 'date',
          name: 'finPartenaire',
          value: values.finPartenaire,
          inputType: undefined,
          required: true,
        },
        {
          label: 'Type',
          placeholder: 'Type de partenariat',
          type: 'select',
          listId: 'value',
          index: 'label',
          name: 'typePartenaire',
          value: values.typePartenaire,
          inputType: undefined,
          selectOptions: typePartenaires,
        },
      ],
    },
  ];

  React.useEffect(() => {
    setValues(initialState);
  }, [open, laboratoire]);

  React.useEffect(() => {
    if (!open) setValues(initialState);
  }, [open]);

  const handleChangeInput = (e: ChangeEvent<any>) => {
    const { value, name } = e.target;
    setValues(prevState => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleChangeDate = (name: string) => (date: any) => {
    setValues(prevState => ({ ...prevState, [name]: date }));
  };

  const [createUpdateLaboratoirePartenaire, { loading: ceateLoading }] = useMutation<
    CREATE_UPDATE_LABORATOIRE_PARTENAIRE,
    CREATE_UPDATE_LABORATOIRE_PARTENAIREVariables
  >(DO_CREATE_UPDATE_LABORATOIRE_PARTENAIRE, {
    onCompleted: data => {
      if (data && data.createUpdateLaboratoirePartenaire) {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: values.id
            ? 'Modification effectuée avec succès'
            : 'Création effectuée avec succès',
        });
        setValues(initialState);
        setOpen(false);
        if (refetchLabo) refetchLabo();
      }
    },
    onError: error => {
      console.log('error :>> ', error);
      displaySnackBar(client, {
        isOpen: true,
        type: 'ERROR',
        message: error.message,
      });
    },
  });

  const onClickConfirm = () => {
    createUpdateLaboratoirePartenaire({
      variables: {
        input: {
          ...values,
          debutPartenaire: moment(values.debutPartenaire) as any,
          finPartenaire: moment(values.finPartenaire) as any,
        },
      },
    });
  };

  return (
    <CustomModal
      onClick={event => event.stopPropagation()}
      open={open}
      setOpen={setOpen}
      title={'Definir partenariat'}
      withBtnsActions={true}
      withCancelButton={isMobile() ? false : true}
      closeIcon={true}
      headerWithBgColor={true}
      fullWidth={true}
      actionButton={'Enregistrer'}
      onClickConfirm={onClickConfirm}
    >
      {ceateLoading && <Backdrop />}
      <div className={classes.pharmacieFormRoot}>
        <div className={classes.formContainer}>
          {FORM_INPUTS.map((item: any, index: number) => {
            return (
              <Fragment key={`pharmacie_form_item_${index}`}>
                <Typography className={classes.inputTitle}>{item.title}</Typography>
                <div className={classes.inputsContainer}>
                  {item.inputs.map((input: any, inputIndex: number) => {
                    return (
                      <div className={classes.formRow} key={`pharmacie_form_input_${inputIndex}`}>
                        <Typography className={classes.inputLabel}>
                          {input.label} {input.required && <span>*</span>}
                        </Typography>
                        {input.type === 'select' ? (
                          <CustomSelect
                            label=""
                            list={input.selectOptions || []}
                            listId={input.listId || 'id'}
                            index={input.index || 'value'}
                            name={input.name}
                            value={input.value}
                            onChange={handleChangeInput}
                            shrink={false}
                            placeholder={input.placeholder}
                            withPlaceholder={true}
                          />
                        ) : input.type === 'date' ? (
                          <CustomDatePicker
                            placeholder={input.placeholder}
                            onChange={handleChangeDate(input.name)}
                            name={input.name}
                            value={input.value}
                            disabled={
                              input.name === 'finPartenaire' && !values.debutPartenaire
                                ? true
                                : false
                            }
                            minDate={
                              input.name === 'finPartenaire' ? values.debutPartenaire : '1999-1-1'
                            }
                            disablePast={false}
                          />
                        ) : (
                          <CustomFormTextField
                            name={input.name}
                            value={input.value}
                            onChange={handleChangeInput}
                            placeholder={input.placeholder}
                            type={input.inputType}
                          />
                        )}
                      </div>
                    );
                  })}
                </div>
              </Fragment>
            );
          })}
        </div>
      </div>
    </CustomModal>
  );
};

export default withRouter(DefinePartenariat);
