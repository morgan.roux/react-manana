import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    cont: {
      flexGrow: 1,
      padding: '40px 20px',
    },
    card: {
      maxWidth: 250,
    },
    imageContainer: {
      marginTop: '10px',
      marginBottom: '10px',
    },
    dateContainer: {
      color: '#888',
      position: 'relative',
      height: '30px',
      '& span': {
        position: 'absolute',
        right: '10px',
      },
    },
  }),
);

export default useStyles;
