import React, { FC } from 'react';
import { List, Link } from '@material-ui/core';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import CommentItem from '../../../../../../../Comment/CommentItem';
import Backdrop from '../../../../../../../Common/Backdrop';

interface CommentListProps {
  comments: any[];
  total: number;
  fetchMoreComments?: () => void;
  loading?: boolean;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    linkAndLoadingContainer: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    link: {
      color: '#000',
    },
    loader: {
      textAlign: 'right',
    },
  }),
);

const CommentList: FC<CommentListProps> = ({ comments, total, fetchMoreComments, loading }) => {
  const classes = useStyles({});

  return (
    <>
      {loading && <Backdrop />}
      <div className={classes.linkAndLoadingContainer}>
        {fetchMoreComments && comments && comments.length < total && (
          <Link component="button" className={classes.link} onClick={fetchMoreComments}>
            Afficher plus de commentaires
          </Link>
        )}
      </div>
      <List>
        {comments &&
          comments.map((comment: any) => <CommentItem key={comment.id} comment={comment} />)}
      </List>
    </>
  );
};

export default CommentList;
