import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    rootContent: {
      background: 'rgb(255, 255, 255)',
      height: 'calc(100vh - 283px)',
      overflow: 'auto',
    },
    root: {
      textAlign: 'center',
      // height: 'calc(100vh - 312px)',
      height: 'auto',
      paddingTop: 30,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      ' & > * ': {
        margin: theme.spacing(1),
      },
    },
    cont: {
      flexGrow: 1,
    },
    paper: {
      height: 140,
      width: 100,
    },
    imgDefault: {
      maxWidth: '400px',
      width: '100%',
    },
    title: {
      fontSize: '1.375rem',
      color: '#4D4D4D',
      fontWeight: 'bold',
      fontFamily: 'Montserrat',
      margin: '24px 0 10px',
    },
    subTitle: {
      fontSize: '0.875rem',
      margin: '10px 0 24px',
      color: '#4D4D4D',
    },
    pink: {
      background:
        'transparent linear-gradient(231deg, #E34741 0%, #E34168 100%) 0% 0% no-repeat padding-box',
      textTransform: 'uppercase',
      fontFamily: 'Roboto',
      fontSize: '14px',
      fontWeight: 'bold',
      paddingLeft: '16px',
      paddingRight: '16px',
      boxShadow: '0px 2px 2px #00000040',
      borderRadius: '3px',
      color: '#FFFFFF',
    },
    icon: {
      marginRight: '8px',
    },
  }),
);

export default useStyles;
