import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      background: 'rgb(255, 255, 255)',
    },
    none: {
      textAlign: 'center',
      height: 'auto',
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      ' & > * ': {
        margin: theme.spacing(1),
      },
    },
    imgDefault: {
      maxWidth: '400px',
      width: '100%',
    },
    title: {
      fontSize: '1.375rem',
      color: '#4D4D4D',
      fontWeight: 'bold',
      fontFamily: 'Montserrat',
      margin: '24px 0 10px',
    },
    subTitle: {
      fontSize: '0.875rem',
      margin: '10px 0 24px',
      color: '#4D4D4D',
    },
  }),
);

export default useStyles;
