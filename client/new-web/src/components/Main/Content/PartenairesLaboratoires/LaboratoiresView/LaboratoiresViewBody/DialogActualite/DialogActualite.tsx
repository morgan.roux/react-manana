import React, { FC } from 'react';
import useStyles from './styles';
import { Dialog, DialogTitle, DialogContent } from '@material-ui/core';
import Close from '@material-ui/icons/Close';
import ActualiteViewBody from '../../../../Actualite/ActualiteView/ActualiteViewBody';

interface DialogActualiteProps {
  actualite: any;
  open: boolean;
  handleClose: any;
}

const DialogActualite: FC<DialogActualiteProps> = ({ actualite, open, handleClose }) => {
  const classes = useStyles({});

  return (
    <Dialog open={open} onClose={handleClose} maxWidth="xl" className={classes.dialog}>
      <DialogTitle id="simple-dialog-title" className={classes.title}>
        {actualite && actualite.libelle}
      </DialogTitle>
      <Close className={classes.close} onClick={handleClose} />
      <DialogContent className={classes.cont}>
        <ActualiteViewBody actualite={actualite} />
      </DialogContent>
    </Dialog>
  );
};

export default DialogActualite;
