import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      background: 'white',
    },

    allContainer: {
      padding: '12px',
      borderTop: '1px solid #00386A',
      background: 'white',
    },

    linkContainer: {
      padding: '60 px',
      margin: '0 5px',
    },
    linkElement: {
      marginRight: '25px',
      fontSize: '0.75rem',
      fontFamily: 'Montserrat',
      fontWeight: 500,
    },

    active: {
      color: '#e34646',
      fontWeight: 'bold',
    },
  }),
);

export default useStyles;
