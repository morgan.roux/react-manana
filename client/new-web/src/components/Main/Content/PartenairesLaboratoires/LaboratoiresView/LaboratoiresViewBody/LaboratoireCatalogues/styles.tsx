import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    cont: {
      flexGrow: 1,
      height: '100%',
      width: '100%',
    },
    card: {
      maxWidth: 250,
    },
    panierListe: {
      flex: 1,
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      maxHeight: '72vh',
      overflowY: 'auto',
    },
    imgDefault: {
      maxWidth: '400px',
      width: '100%',
    },
    title: {
      fontSize: '1.375rem',
      color: '#4D4D4D',
      fontWeight: 'bold',
      fontFamily: 'Montserrat',
      margin: '24px 0 10px',
    },
    subTitle: {
      fontSize: '0.875rem',
      margin: '10px 0 24px',
      color: '#4D4D4D',
    },
  }),
);

export default useStyles;
