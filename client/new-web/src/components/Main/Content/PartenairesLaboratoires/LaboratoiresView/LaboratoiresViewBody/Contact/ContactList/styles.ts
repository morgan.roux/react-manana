import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    searchInputBox: {
      padding: '20px 20px 15px 20px',
      display: 'flex',
      justifyContent: 'flex-end',
    },
    btnAdd: {
      marginLeft: 8,
    },
    dptIconBackground: {
      color: 'rgba(0, 0, 0, 0.54)',
      marginLeft: 6,
      marginTop: 6,
    },
  }),
);

export default useStyles;
