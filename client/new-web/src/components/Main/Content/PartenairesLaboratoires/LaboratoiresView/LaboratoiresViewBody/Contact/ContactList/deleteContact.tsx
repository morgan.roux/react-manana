import { useApolloClient, useMutation, useQuery } from '@apollo/client';
import { differenceBy } from 'lodash';
import { useContext } from 'react';
import { ContentContext, ContentStateInterface } from '../../../../../../../../AppContext';
import { DO_DELETE_LABO_REPRESENTANT } from '../../../../../../../../graphql/LaboratoireRepresentant';
import {
  DELETE_LABO_REPRESENTANT,
  DELETE_LABO_REPRESENTANTVariables,
} from '../../../../../../../../graphql/LaboratoireRepresentant/types/DELETE_LABO_REPRESENTANT';
import { GET_CHECKED_PARTENAIRE_REPRESENTANT } from '../../../../../../../../graphql/PartenaireRepresentant/local';
import { displaySnackBar } from '../../../../../../../../utils/snackBarUtils';

const useCheckedPartenaireRepresentant = () => {
  const checkedsPartenaireRepresentant = useQuery(GET_CHECKED_PARTENAIRE_REPRESENTANT);
  return (
    checkedsPartenaireRepresentant &&
    checkedsPartenaireRepresentant.data &&
    checkedsPartenaireRepresentant.data.checkedsPartenaireRepresentant
  );
};

export const useDeletePartenaireRepresentant = (setSelected: any) => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const checkeds = useCheckedPartenaireRepresentant();

  const client = useApolloClient();

  const [deleteRepresentant] = useMutation<
    DELETE_LABO_REPRESENTANT,
    DELETE_LABO_REPRESENTANTVariables
  >(DO_DELETE_LABO_REPRESENTANT, {
    update: (cache, { data }) => {
      if (data && data.softDeleteLaboratoireRepresentants) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables: variables,
          });
          if (req && req.search && req.search.data) {
            const source = req.search.data;
            const result = data.softDeleteLaboratoireRepresentants;
            const dif = differenceBy(source, result, 'id');
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: dif,
                  },
                },
              },
              variables: variables,
            });
            setSelected([]);
          }
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
    onCompleted: data => {
      if (data && data.softDeleteLaboratoireRepresentants) {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'Contact supprimé(s) avec succès',
          isOpen: true,
        });
      }
    },
  });

  return deleteRepresentant;
};
