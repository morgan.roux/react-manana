import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      background: 'red'
    },
    toWhite: {
      color: 'white',
      fontWeight: 'bold',
      marginRight: '5'
    }
  }),
);

export default useStyles;
