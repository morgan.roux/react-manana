import { useApolloClient, useQuery } from '@apollo/client';
import { Box } from '@material-ui/core';
import { Delete, Edit, Visibility } from '@material-ui/icons';
import React, { FC, Fragment, useContext, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { ContentContext, ContentStateInterface } from '../../../../../../../../AppContext';
import { DO_SEARCH_LABO_REPRESENTANT } from '../../../../../../../../graphql/LaboratoireRepresentant';
import {
  SEARCH_LABO_REPRESENTANT,
  SEARCH_LABO_REPRESENTANTVariables,
} from '../../../../../../../../graphql/LaboratoireRepresentant/types/SEARCH_LABO_REPRESENTANT';
import { displaySnackBar } from '../../../../../../../../utils/snackBarUtils';
import { ConfirmDeleteDialog } from '../../../../../../../Common/ConfirmDialog';
import CustomAvatar from '../../../../../../../Common/CustomAvatar';
import CustomButton from '../../../../../../../Common/CustomButton';
import CustomContent from '../../../../../../../Common/newCustomContent';
import SearchInput from '../../../../../../../Common/newCustomContent/SearchInput';
import WithSearch from '../../../../../../../Common/newWithSearch/withSearch';
import { Column } from '../../../../../../../Dashboard/Content/Interface';
import TableActionColumn, {
  TableActionColumnMenu,
} from '../../../../../../../Dashboard/TableActionColumn';
import CustomTable, {
  CustomTableColumn,
} from '../../../../../Pilotage/Dashboard/Table/CustomTable';
import Form from '../Form';
import { useDeletePartenaireRepresentant } from './deleteContact';
import useStyles from './styles';

interface ContactListProps {
  laboratoire: any;
  contact: any;
  open: boolean;
  setContact: (value: any) => void;
  setOpen: (value: boolean) => void;
  setOpenDetail: (value: boolean) => void;
}

const ContactList: FC<ContactListProps & RouteComponentProps> = ({
  location: { pathname },
  history: { push },
  laboratoire,
  contact,
  open,
  setOpen,
  setContact,
  setOpenDetail,
}) => {
  const classes = useStyles({});

  const [openAffectationDialog, setOpenAffectationDialog] = useState<boolean>(false);

  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [representantToAffect, setRepresentantToAffect] = useState<any>({});
  const [deleteRow, setDeleteRow] = useState<any>(null);
  const [selected, setSelected] = useState<any[]>([]);
  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(5);
  const [take, setTake] = useState<number>(5);
  const [skip, setSkip] = useState<number>(0);
  const [representants, setRepresentants] = useState<any[]>([]);

  const deleteRepresentant = useDeletePartenaireRepresentant(setSelected);

  const { data, fetchMore } = useQuery<SEARCH_LABO_REPRESENTANT, SEARCH_LABO_REPRESENTANTVariables>(
    DO_SEARCH_LABO_REPRESENTANT,
    {
      fetchPolicy: 'cache-and-network',
      variables: {
        type: ['laboratoirerepresentant'],
        filterBy: [
          { term: { isRemoved: false } },
          { term: { 'laboratoire.id': (laboratoire && laboratoire.id) || '' } },
        ],
        take,
        skip,
      },
      onError: error => {
        console.log('error :>> ', error);
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );

  React.useEffect(() => {
    if (data && data.search && data.search.data) {
      setRepresentants((data && data.search && data.search.data) || []);
    }
  }, [data]);

  const fetchMoreData = (toTake: number = 0) => () => {
    fetchMore({
      variables: {
        type: ['laboratoirerepresentant'],
        filterBy: [
          { term: { isRemoved: false } },
          { term: { 'laboratoire.id': (laboratoire && laboratoire.id) || '' } },
        ],
        take: take + toTake,
        skip,
      },
      updateQuery: (prev: any, { fetchMoreResult }: any) => {
        setTake(take + toTake);
        setRepresentants(
          (fetchMoreResult && fetchMoreResult.search && fetchMoreResult.search.data) || [],
        );
        return fetchMoreResult as any;
      },
    });
  };

  const onClickDelete = (_event: any, row: any) => {
    setOpenDeleteDialog(true);
    setDeleteRow(row);
  };

  const onClickEdit = (_event: any, row: any) => {
    setOpen(true);
    setContact(row);
  };

  const onClickDetail = (_event: any, row: any) => {
    setOpenDetail(true);
    setContact(row);
  };

  const onClickConfirmDelete = () => {
    if (deleteRow) {
      const { id } = deleteRow;
      deleteRepresentant({
        variables: { ids: [id] },
      });
      setDeleteRow(null);
    } else if (selected && selected.length > 0) {
      deleteRepresentant({
        variables: { ids: selected.map(item => item.id) },
      });
    }
    fetchMoreData(0)();
    setOpenDeleteDialog(false);
  };

  const client = useApolloClient();

  const menuItems: TableActionColumnMenu[] = [
    {
      label: 'Modifier',
      icon: <Edit />,
      onClick: onClickEdit,
      disabled: false,
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: onClickDelete,
      disabled: false,
    },
    {
      label: 'Voir détails',
      icon: <Visibility />,
      onClick: onClickDetail,
      disabled: false,
    },
  ];

  const columns: CustomTableColumn[] = [
    {
      label: 'Photo',
      key: '',
      sortable: false,
      disablePadding: false,
      numeric: false,
      renderer: (value: any) => {
        return (
          <CustomAvatar
            name={value && value.fullName}
            url={value && value.photo && value.photo.publicUrl}
          />
        );
      },
    },
    /* { label: 'Fonction', key: 'fonction', disablePadding: false, numeric: false }, */
    { label: 'Nom', key: 'nom', disablePadding: false, numeric: false },
    { label: 'Prénom', key: 'prenom', disablePadding: false, numeric: false },
    {
      label: 'Téléphone',
      key: 'contact.telProf',
      disablePadding: false,
      numeric: false,
      renderer: (value: any) => {
        return (value.contact && value.contact.telProf) || '-';
      },
    },
    {
      label: 'Mail',
      key: 'contact.mailProf',
      disablePadding: false,
      numeric: false,
      renderer: (value: any) => {
        return (value.contact && value.contact.mailProf) || '-';
      },
    },
    /* {
      label: 'Adresse',
      key: 'contact.adresse1',
      disablePadding: false,
      numeric: false,
      renderer: (value: any) => {
        return (value.contact && value.contact.adresse1) || '-';
      },
    }, */
    {
      key: '',
      disablePadding: false,
      numeric: false,
      label: '',
      sortable: false,
      renderer: (value: any) => {
        return (
          <Fragment>
            <TableActionColumn
              row={value}
              baseUrl=""
              showResendEmail={false}
              customMenuItems={menuItems}
            />
          </Fragment>
        );
      },
    },
  ];

  const DeleteDialogContent = () => {
    if (deleteRow) {
      const name = `${deleteRow.prenom || ''} ${deleteRow.nom || ''}`;
      return (
        <span>
          Êtes-vous sur de vouloir supprimer
          <span style={{ fontWeight: 'bold' }}> {name} </span>?
        </span>
      );
    }
    return <span>Êtes-vous sur de vouloir supprimer ces contacts ?</span>;
  };

  return (
    <Box width="100%">
      <Box className={classes.searchInputBox}>
        {/*  <SearchInput searchPlaceholder="Rechercher un contact..." /> */}
        <Box>
          {selected && selected.length > 0 && (
            <CustomButton onClick={() => setOpenDeleteDialog(true)}>
              SUPPRIMER LA SELECTION
            </CustomButton>
          )}
          <CustomButton color="secondary" className={classes.btnAdd} onClick={() => setOpen(true)}>
            Ajout de contact
          </CustomButton>
        </Box>
      </Box>
      <div style={{ padding: 20 }}>
        <CustomTable
          columns={columns}
          showToolbar={false}
          selectable={false}
          data={representants}
          rowCount={representants.length}
          total={representants.length}
          page={page}
          rowsPerPage={rowsPerPage}
          take={take}
          skip={skip}
          setTake={setTake}
          setSkip={setSkip}
          setPage={setPage}
          setRowsPerPage={setRowsPerPage}
        />
      </div>
      <Form
        open={open}
        setOpen={setOpen}
        idLaboratoire={laboratoire && laboratoire.id}
        representant={contact}
        setRepresentant={setContact}
        fetchMoreData={fetchMoreData(0)}
      />
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
    </Box>
  );
};
export default withRouter(ContactList);
