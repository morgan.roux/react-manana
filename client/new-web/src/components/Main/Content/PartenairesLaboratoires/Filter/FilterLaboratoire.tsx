import {
  Box,
  InputAdornment,
  OutlinedInput,
  Typography,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import React, { Fragment, useState } from 'react';
import CustomFilterContent from '../../../../Common/Filter/CustomFilterContent/CustomFilterContent';
import useStyles from './styles';

interface FilterLaboratoireProps {
  handleSearchTxt: (arg0: any) => void;
  setTypesActive?: (value: any) => void;
  typesActive?: number[];
  searchTxt: string;
}

const FilterLaboratoire: React.FC<FilterLaboratoireProps> = ({
  handleSearchTxt,
  setTypesActive,
  typesActive,
  searchTxt,
}) => {
  const classes = useStyles({});
  const initialTypes = [
    {
      nom: 'Partenaire',
      checked: typesActive && typesActive.includes(0) ? true : false,
      value: 0,
    },
    {
      nom: 'Autre',
      checked: typesActive && typesActive.includes(1) ? true : false,
      value: 1,
    },
  ];

  const [types, setTypes] = useState(initialTypes);
  const [text, setTxt] = useState(searchTxt ? searchTxt : '');

  const handleSearch = e => {
    const value = e.target.value;
    setTxt(value);
  };

  const handleType = values => {
    setTypes(values);
    if (values && values.length) {
      if (setTypesActive)
        setTypesActive(values.filter(value => value.checked).map(active => active.value));
    }
  };

  return (
    <Fragment>
      <Box padding="0px 10px 25px">
        <Box padding="8px 0 12px" display="flex" justifyContent="space-between" alignItems="center">
          <Box display="flex" alignItems="center">
            <Typography className={classes.toolbarTitle}>Laboratoires</Typography>
          </Box>
        </Box>
        <Box padding="0px 10px 25px">
          <OutlinedInput
            placeholder={'Rechercher laboratoire...'}
            endAdornment={
              <InputAdornment position="end">
                <SearchIcon />
              </InputAdornment>
            }
            value={text}
            onChange={handleSearch}
            onKeyUp={() => handleSearchTxt(text)}
            labelWidth={0}
          />
        </Box>
        <CustomFilterContent title="Type" datas={types} updateDatas={handleType} />
      </Box>
    </Fragment>
  );
};

export default FilterLaboratoire;
