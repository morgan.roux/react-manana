import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import { LaboratoirePage } from '@app/ui-kit';
import { LaboratoireListProps } from '@app/ui-kit/components/pages/LaboratoirePage/useSideNavList';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import FilterAlt from '../../../../assets/icons/todo/filter_alt.svg';
import { SEARCH_PARAMETRE_LABORATOIRES } from '../../../../federation/search/query';
import {
  SEARCH_PARAMETRE_LABORATOIRES as SEARCH_PARAMETRE_LABORATOIRES_TYPE,
  SEARCH_PARAMETRE_LABORATOIRESVariables,
} from '../../../../federation/search/types/SEARCH_PARAMETRE_LABORATOIRES';
import { SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search_data_ProduitCanal } from '../../../../graphql/ProduitCanal/types/SEARCH_PLAN_MARKETING_PRODUIT_CANALS';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../graphql/S3/mutation';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { getPharmacie } from '../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop';
import NoItemContentImage from '../../../Common/NoItemContentImage';
import { FEDERATION_CLIENT } from '../../../Dashboard/DemarcheQualite/apolloClientFederation';
import noImageSrc from './../../../../assets/img/pas_image.png';
import { GET_LABORATOIRE } from './../../../../federation/partenaire-service/laboratoire/query';
import {
  GET_LABORATOIRE as GET_LABORATOIRE_TYPE,
  GET_LABORATOIREVariables,
} from './../../../../federation/partenaire-service/laboratoire/types/GET_LABORATOIRE';
import {
  GET_LIST_PARTENARIAT_STATUTS,
  GET_LIST_PARTENARIAT_TYPES,
} from './../../../../federation/partenaire-service/partenariat/query';
import {
  GET_LIST_PARTENARIAT_STATUTS as GET_LIST_PARTENARIAT_STATUTS_TYPES,
  GET_LIST_PARTENARIAT_STATUTSVariables,
} from './../../../../federation/partenaire-service/partenariat/types/GET_LIST_PARTENARIAT_STATUTS';
import {
  GET_LIST_PARTENARIAT_TYPES as GET_LIST_PARTENARIAT_TYPES_TYPES,
  GET_LIST_PARTENARIAT_TYPESVariables,
} from './../../../../federation/partenaire-service/partenariat/types/GET_LIST_PARTENARIAT_TYPES';
import LaboratoireFilter from './utils/LaboratoireFilter';
import { SelectAvatar } from './utils/SelectAvatar';
import { useAbout } from './utils/useAbout';
import { useActualite } from './utils/useActualite';
import { useAnalyse } from './utils/useAnalyse';
import { useCatalogue } from './utils/useCatalogue';
import { useCompteRenduClient } from './utils/useCompteRenduClient';
import { useConditionCommerciale } from './utils/useConditionCommerciale';
import { useContact } from './utils/useContact';
import { useHistoriqueCommande } from './utils/useHistoriqueCommande';
import { useHistoriqueFacture } from './utils/useHistoriqueFacture';
import { usePilotage } from './utils/usePilotage';
import { usePlanMarketing } from './utils/usePlanMarketing';
import { usePlanningPlanMarketing } from './utils/usePlanningPlanMarketing';
import { useRemuneration } from './utils/useRemuneration';
import { useSuiviOperationnel } from './utils/useSuiviOperationnel';
const PartenaireLaboratoire: FC<RouteComponentProps<any, any, any>> = ({
  match: { params },
  history: {
    location: { state },
    push,
  },
}) => {
  const pharmacie = getPharmacie();

  const client = useApolloClient();
  const { idLabo, tab, view } = params;

  const [goBack, setGoBack] = useState<boolean>(state && state.goBack ? true : false);

  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(10);
  const [searchTxt, setSearchTxt] = useState<string>('');
  const [searchFilter, setSearchFilter] = useState<any>();
  const [searchDatePartenariatFilter, setSearchDatePartenariatFilter] = useState<any>();
  const [currentView, setCurrentView] = useState<'list' | 'card'>(view);
  const [photo, setPhoto] = useState<any>(null);
  const [openFilterLaboratoire, setOpenFilterLaboratoire] = useState<boolean>(false);

  const must: any[] = [];
  if (searchTxt) {
    must.push({
      query_string: {
        query: `*${searchTxt.replaceAll(' ', '\\ ')}*`,
        analyze_wildcard: true,
        fields: [
          'nom',
          'laboratoireSuite.email',
          'laboratoireSuite.adresse',
          'laboratoireSuite.codePostal',
          'laboratoireSuite.ville',
        ],
      },
    });
  }

  if (searchFilter) {
    const selectedPartenaireFilter: string[] = searchFilter
      .filter(({ keyword }) => keyword === 'partenaire')
      .map(({ code }) => code);
    const selectedIdTypesFilter: string[] = searchFilter
      ?.filter(({ keyword }) => keyword === 'idType')
      .map(({ id }) => id);
    const selectedIdStatusFilter: string[] = searchFilter
      ?.filter(({ keyword }) => keyword === 'idStatut')
      .map(({ id }) => id);

    if (selectedIdTypesFilter.length > 0) {
      must.push({
        terms: {
          couplePharmaciePartenariatTypes: selectedIdTypesFilter.map(
            idType => `${pharmacie.id}-${idType}`,
          ),
        },
      });
    }

    if (selectedIdStatusFilter.length > 0) {
      must.push({
        terms: {
          couplePharmaciePartenariatStatuts: selectedIdStatusFilter.map(
            idStatus => `${pharmacie.id}-${idStatus}`,
          ),
        },
      });
    }
  }

  if (searchDatePartenariatFilter) {
    const dateDebutFilter = searchDatePartenariatFilter.dateDebut || '';
    const dateFinFilter = searchDatePartenariatFilter.dateFin || '';
  }

  const variables = {
    index: ['laboratoires'],
    query: {
      query: {
        bool: {
          must,
        },
      },
      sort: [{ updatedAt: { order: 'desc' } }],
    },
    take,
    skip,
  };

  // FICHIERS
  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
  });

  const [searchLaboratoires, { data: listResult, loading, error, fetchMore }] = useLazyQuery<
    SEARCH_PARAMETRE_LABORATOIRES_TYPE,
    SEARCH_PARAMETRE_LABORATOIRESVariables
  >(SEARCH_PARAMETRE_LABORATOIRES, {
    fetchPolicy: 'cache-and-network',
    variables,
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [loadLaboratoire, loadingLaboratoire] = useLazyQuery<
    GET_LABORATOIRE_TYPE,
    GET_LABORATOIREVariables
  >(GET_LABORATOIRE, {
    client: FEDERATION_CLIENT,
  });

  const [loadTypePartenariat, loadingTypePartenariat] = useLazyQuery<
    GET_LIST_PARTENARIAT_TYPES_TYPES,
    GET_LIST_PARTENARIAT_TYPESVariables
  >(GET_LIST_PARTENARIAT_TYPES, {
    fetchPolicy: 'cache-and-network',
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [loadStatutPartenariat, loadingStatutPartenariat] = useLazyQuery<
    GET_LIST_PARTENARIAT_STATUTS_TYPES,
    GET_LIST_PARTENARIAT_STATUTSVariables
  >(GET_LIST_PARTENARIAT_STATUTS, {
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
    client: FEDERATION_CLIENT,
  });

  const handleFetchMore = (count: number) => {
    if (listResult?.search?.data?.length) {
      fetchMore({
        variables: { ...variables, skip: listResult.search.data.length },

        updateQuery: (prev, { fetchMoreResult }) => {
          if (
            prev &&
            prev.search &&
            prev.search.data &&
            fetchMoreResult &&
            fetchMoreResult.search &&
            fetchMoreResult.search.data
          ) {
            const { data: currentData } = prev.search;
            return {
              ...prev,
              search: {
                ...prev.search,
                data: [...currentData, ...fetchMoreResult.search.data],
                total: fetchMoreResult.search.total,
              },
            };
          }

          return prev;
        },
      });
    }
  };

  const handleSearch = (change: any) => {
    const { skip, take, searchText, filter } = change;
    setSkip(skip);
    setTake(take);
    setSearchTxt(searchText);
    setSearchFilter(filter);
  };

  const handleRequestGoBack = () => {
    if (idLabo) {
      push('/laboratoires');
    } else {
      push('/');
    }
  };

  const handleLaboratoireFilter = (statut: any, type: any, date: any) => {
    if (!statut) {
      statut = [{ keyword: 'idStatut', id: '' }];
    }
    if (!type) {
      type = [{ keyword: 'idType', id: '' }];
    }
    const filter = statut.concat(type);
    setSearchFilter(filter);
    setSearchDatePartenariatFilter(date);
  };

  /**
   *
   * Setter les url
   */

  const selectAvatar = <SelectAvatar photo={photo} setPhoto={setPhoto} />;
  const laboratoireSuivi = useSuiviOperationnel(idLabo, push);
  const { onRequestPlanSearch, laboratoirePlan } = usePlanMarketing(idLabo, push);
  const { onRequestContactSearch, contactProps, setPhotoContact } = useContact(
    idLabo,
    selectAvatar,
    push,
  );

  useEffect(() => {
    setPhotoContact(photo);
  }, [photo]);

  const handleSideNavListClick = (item: string) => {
    if (item !== 'selected' && item !== 'pilotages') {
      if (item === 'catalogue') {
        push(`/laboratoires/${idLabo}/${item}/card`);
      } else {
        push(`/laboratoires/${idLabo}/${item}`);
      }
    } else if (item === 'pilotages') {
      push('/laboratoires/list/pilotages');
    } /*else if (item === 'selected') {
      push(`/laboratoires/${idLabo}/about`);
    }*/
  };

  const handleClickLaboActive = (id: string) => {
    if (!id) {
      // Return to list
      window.history.pushState(null, 'relation labo', `/#/laboratoires/`);
    } else {
      push(`/laboratoires/${id}/selected`);
    }
  };

  const handleCloseFilterLaboratoire = (bool: boolean) => {
    setOpenFilterLaboratoire(bool);
  };

  const { aboutPageProps } = useAbout({
    params: params,
    loading: loadingLaboratoire.loading,
    error: loadingLaboratoire.error,
    laboratoire: loadingLaboratoire.data?.laboratoire as any,
    refetch: loadingLaboratoire.refetch,
  });

  const [actualiteMainContent] = useActualite(idLabo);

  const { analyseProps } = useAnalyse(idLabo);

  const [produitMainContent] = useCatalogue(idLabo, currentView);

  const remunerationProps = useRemuneration({
    tab,
    laboratoire: loadingLaboratoire.data?.laboratoire as any,
    params,
    push,
  });

  ////////////////////CONDITION////////////////////////
  const [
    onRequestSave,
    onRequestSearchCondition,
    onRequestDelete,
    handleChangeStatus,
    handleFetchMoreConditions,
    { type, status, canal },
    totalRow,
    loadingConditions,
    setSaved,
    saving,
    saved,
    handleCancelCondition,
    handleClotureCondition,
    commentaireComponnent,
    onRequestDetail,
  ] = useConditionCommerciale(idLabo);

  //////////////////////////////////////////

  const compteRenduComponentProps = useCompteRenduClient({ params, push });

  const filterByPharmacieInfos = (data: any) => {
    return data.map(laboratoire => {
      const partenaireTypes = laboratoire.pharmacieInfos?.partenariats[pharmacie.id];
      const rdvs = laboratoire.pharmacieInfos?.rdvs[pharmacie.id];
      const partenaireType = partenaireTypes && partenaireTypes.length && partenaireTypes[0];
      const rdv = rdvs && rdvs.length && rdvs[0];
      return { ...laboratoire, partenaireType, rdv };
    });
  };

  const laboratoireListProps: LaboratoireListProps = {
    laboratoires: {
      data: filterByPharmacieInfos(listResult?.search?.data || []),
      error: error as any,
      loading,
      rowsTotal: listResult?.search?.total || 0,
    },
    typesPartenariats: (aboutPageProps.typesPartenariats || []) as any,
    statutsPartenariats: (aboutPageProps.statutsPartenariats || []) as any,
    onRequestSearch: handleSearch,
  };

  console.log('+++++++++++++++++++++++++++++++++++++++++++++++++ : ', laboratoireListProps);

  useEffect(() => {
    searchLaboratoires({
      variables,
    });
  }, [skip, take, searchTxt, searchFilter]);

  useEffect(() => {
    if (!params.tab || params.tab === 'list' || params.tab === 'selected') {
      loadTypePartenariat();
    } else if (params.tab === 'plan') {
      onRequestPlanSearch();
    } else if (params.tab === 'contact') {
      onRequestContactSearch();
    } else if (params.tab === 'factures') {
      getOrigines();
    }
  }, [params.tab]);

  useEffect(() => {
    const idLabo = params.idLabo;
    if (idLabo) {
      loadLaboratoire({
        variables: {
          id: idLabo,
          idPharmacie: getPharmacie().id,
        },
      });
    }
  }, [params.idLabo]);

  const {
    commandes: {
      errorHistoriqueCommande,
      loadingHistoriqueCommande,
      dataHistoriqueCommande,
      onRequestSearch,
      rowsTotal,
      totaux,
    },
    commandes,
    ligneCommande: { loadingLignesCommandes, onRequestLignesCommandes },
    loading: {
      loadingTotalRowsForDocument,
      loadingCommandesForDocument,
      loadingTotauxCommandesForDocument,
    },
    onRequestSaveCommande,
    onRequestDeleteCommande,
    savedCommande,
    setSavedCommande,
    savingCommande,
    onRequestChangeStatut,
    commandeStatuts,
    onRequestGoBack,
  } = useHistoriqueCommande(idLabo, tab, push);

  const [
    handleRequestPlanning,
    planningPlanMarketingsData,
    planningPlanMarketingsLoading,
    handleRequestProduits,
    produitData,
    onRequestChangeStatutAction,
  ] = usePlanningPlanMarketing(idLabo && idLabo !== 'list' ? idLabo : undefined);

  const currentLaboPlanningProps = {
    onRequestPlanning: handleRequestPlanning,
    data: planningPlanMarketingsData,
    loading: planningPlanMarketingsLoading,
    handleRequestProduits,
    produitData,
    onRequestChangeStatutAction,
  };

  const pilotageProps = usePilotage({
    tab,
    idLabo,
    currentLaboPlanningProps,
    push,
  });

  const [
    searchDocuments,
    gettingDocuments,
    handleConsult,
    getOrigines,
    originesLoading,
    handleJoindreCommande,
    handleConsultGed,
    isAuthorizedToAddDocument,
    EditComponent,
    handleEditFacture,
    handleDeleteFacture,
    totauxFacture,
    factureStatuts,
    savingAssociation,
    savedAssociation,
    setSavedAssociation,
    handleGoBackHistoriqueFacture,
    detailComponent,
  ] = useHistoriqueFacture({
    idCurrentLabo: idLabo,
    push,
    currentLabo: loadingLaboratoire.data?.laboratoire,
    loadingTotalRowsForDocument,
    loadingCommandesForDocument,
    loadingTotauxCommandesForDocument,
  });
  const historiqueFactureProps = {
    loading: gettingDocuments.loading,
    error: gettingDocuments.error as any,
    data: gettingDocuments.data?.gedDocuments.nodes as any,
    rowsTotal: gettingDocuments.data?.gedDocuments.nodes.length || 0,
    onRequestSearch: searchDocuments,
    onRequestConsultFacture: handleConsult,
    onJoindreCommande: handleJoindreCommande,
    onConsultGed: handleConsultGed,
    isAuthorizedToAddDocument,
    EditComponent,
    onRequestEdit: handleEditFacture,
    onRequestDelete: handleDeleteFacture,
    totaux: totauxFacture,
    factureStatuts,
    saving: savingAssociation,
    saved: savedAssociation,
    setSaved: setSavedAssociation,
    onRequestGoBack: handleGoBackHistoriqueFacture,
    detailComponent
  };

  if (originesLoading) {
    return <Backdrop />;
  }

  if (originesLoading) {
    return <Backdrop />;
  }

  return (
    <LaboratoirePage
      historiqueCommandePageProps={{
        onRequestGoBack,
        onRequestLignesCommande: onRequestLignesCommandes,
        rowsTotal,
        totaux,
        lignesCommandeChoosen: (
          loadingLignesCommandes.data?.getCommandeLignesByIdCommande || []
        ).map(line => ({
          id: line.id,
          produit: line.produit as any,
          quantite: line.quantiteLivree || 0,
          remise: line.remiseLigne || ('' as any),
          total: line.prixTotalHT || 0,
        })),
        loading: loadingHistoriqueCommande,
        error: errorHistoriqueCommande,
        onRequestSearch,
        commandes: dataHistoriqueCommande,
        onRequestSave: onRequestSaveCommande,
        onRequestDelete: onRequestDeleteCommande,
        saving: savingCommande,
        saved: savedCommande,
        setSaved: setSavedCommande,
        onRequestChangeStatut,
        commandeStatuts,
      }}
      openFilterLaboratoire={openFilterLaboratoire}
      onOpenFilterLaboratoireMobile={setOpenFilterLaboratoire}
      filterIcon={<img src={FilterAlt} />}
      laboratoire={{
        loading: loadingLaboratoire.loading,
        error: loadingLaboratoire.error,
        data: loadingLaboratoire.data?.laboratoire as any,
      }}
      activeItem={params.tab || 'list'}
      laboratoireListProps={laboratoireListProps}
      laboratoireFilter={
        <LaboratoireFilter
          filterTypePartenariat={loadingTypePartenariat.data?.pRTPartenariatTypes.nodes as any}
          filterStatutPartenariat={
            loadingStatutPartenariat.data?.pRTPartenariatStatuts.nodes as any
          }
          onRequestFilter={handleLaboratoireFilter}
          onCloseFilter={handleCloseFilterLaboratoire}
        />
      }
      onRequestGoBack={handleRequestGoBack}
      onSideNavListClick={handleSideNavListClick}
      onClickLaboActive={handleClickLaboActive}
      analyseProps={analyseProps}
      historiqueFactureProps={historiqueFactureProps}
      cataloguePageProps={{ produitMainContent }}
      actualitePageProps={{ actualiteMainContent }}
      laboratoirePlan={laboratoirePlan}
      laboratoireSuivi={laboratoireSuivi as any}
      aboutPageProps={aboutPageProps as any}
      contactProps={contactProps}
      pilotageProps={pilotageProps as any}
      RemunerationPageProps={remunerationProps as any}
      conditionPageProps={
        {
          setSaved: () => setSaved(false),
          saved,
          saving,
          laboratoire: {
            loading: loadingLaboratoire.loading,
            error: loadingLaboratoire.error,
            data: loadingLaboratoire.data?.laboratoire as any,
          },
          onRequestFetchMore: handleFetchMoreConditions,
          onRequestChangeStatus: handleChangeStatus,
          onRequestDelete: onRequestDelete,
          onRequestSave: onRequestSave,
          onRequestSearch: onRequestSearchCondition,
          typesConditionCommande: type || [],
          listStatusConditionCommande: status || [],
          canalCommandes: canal || [],
          total: totalRow,
          loading: loadingConditions.loading || false,
          error: loadingConditions.error,
          data: (loadingConditions.data?.pRTConditionCommerciales.nodes || []).map(item => ({
            id: item.id,
            canal: item.canal,
            type: item.type,
            status: item.statut,
            libelle: item.titre,
            fichiers: item.fichiers || null,
            details: item.description || '',
            periode: {
              debut: item.dateDebut,
              fin: item.dateFin,
            },
          })) as any,
          onCancel: handleCancelCondition,
          onCloturer: handleClotureCondition,
          commentaireComponnent,
          onRequestDetail,
        } as any
      }
      compteRenduProps={compteRenduComponentProps}
      noImageSrc={noImageSrc}
      fetchMore={handleFetchMore}
      imageInfoComponent={
        <NoItemContentImage
          title="Aperçu des laboratoires"
          subtitle="Veuillez cliquez un laboratoire pour voir des informations"
        />
      }
      planningPageProps={{
        data: planningPlanMarketingsData,
        loading: planningPlanMarketingsLoading,
        onRequestPlanning: handleRequestPlanning,
        onRequestChangeStatutAction: onRequestChangeStatutAction,
        onRequestProduits: handleRequestProduits,
        produits: {
          loading: produitData.loading,
          error: produitData.error,
          data: (produitData.data?.search?.data || []).map(produit => ({
            code: produit.produit?.produitCode?.code || '',
            famille: produit.produit?.famille?.libelleFamille || '',
            nom: produit.produit?.libelle || '',
          })),
        },
      }}
    />
  );
};

export default withRouter(PartenaireLaboratoire);
