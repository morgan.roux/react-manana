import { useApolloClient, useLazyQuery, useMutation } from '@apollo/react-hooks';
import moment from 'moment';
import { useEffect, useState } from 'react';
import {
  CREATE_PRT_REMUNERATION,
  CREATE_PRT_REMUNERATION_REGLEMENT,
  DELETE_PRT_REMUNERATION,
  DELETE_PRT_REMUNERATION_REGLEMENT,
  UPDATE_PRT_REMUNERATION,
  UPDATE_PRT_REMUNERATION_REGLEMENT,
} from '../../../../../federation/partenaire-service/remuneration/mutation';
import {
  GET_PRESTATION_TYPE,
  GET_PRT_REGLEMENT_MODE,
  GET_PRT_REMUNERATIONS,
  GET_PRT_REMUNERATIONS_REGLEMENTS,
  GET_REMUNERATION_ANNEE,
  GET_REMUNERATION_PREVUE_EXIST,
} from '../../../../../federation/partenaire-service/remuneration/query';
import {
  CREATE_PRT_REMUNERATIONVariables,
  CREATE_PRT_REMUNERATION as CREATE_PRT_REMUNERATION_TYPE,
} from '../../../../../federation/partenaire-service/remuneration/types/CREATE_PRT_REMUNERATION';
import {
  CREATE_PRT_REMUNERATION_REGLEMENTVariables,
  CREATE_PRT_REMUNERATION_REGLEMENT as CREATE_PRT_REMUNERATION_REGLEMENT_TYPE,
} from '../../../../../federation/partenaire-service/remuneration/types/CREATE_PRT_REMUNERATION_REGLEMENT';
import {
  DELETE_PRT_REMUNERATIONVariables,
  DELETE_PRT_REMUNERATION as DELETE_PRT_REMUNERATION_TYPE,
} from '../../../../../federation/partenaire-service/remuneration/types/DELETE_PRT_REMUNERATION';
import {
  DELETE_PRT_REMUNERATION_REGLEMENT as DELETE_PRT_REMUNERATION_REGLEMENT_TYPE,
  DELETE_PRT_REMUNERATION_REGLEMENTVariables,
} from '../../../../../federation/partenaire-service/remuneration/types/DELETE_PRT_REMUNERATION_REGLEMENT';
import {
  GET_PRESTATION_TYPEVariables,
  GET_PRESTATION_TYPE as GET_PRESTATION_TYPE_TYPE,
} from '../../../../../federation/partenaire-service/remuneration/types/GET_PRESTATION_TYPE';
import {
  GET_PRT_REGLEMENT_MODE as GET_PRT_REGLEMENT_MODE_TYPE,
  GET_PRT_REGLEMENT_MODEVariables,
} from '../../../../../federation/partenaire-service/remuneration/types/GET_PRT_REGLEMENT_MODE';
import {
  GET_PRT_REMUNEARTIONS as GET_PRT_REMUNERATIONS_TYPE,
  GET_PRT_REMUNEARTIONSVariables,
} from '../../../../../federation/partenaire-service/remuneration/types/GET_PRT_REMUNEARTIONS';
import {
  GET_PRT_REMUNERATIONS_REGLEMENTVariables,
  GET_PRT_REMUNERATIONS_REGLEMENT as GET_PRT_REMUNERATIONS_REGLEMENT_TYPE,
} from '../../../../../federation/partenaire-service/remuneration/types/GET_PRT_REMUNERATIONS_REGLEMENT';
import {
  UPDATE_PRT_REMUNERATIONVariables,
  UPDATE_PRT_REMUNERATION as UPDATE_PRT_REMUNERATION_TYPE,
} from '../../../../../federation/partenaire-service/remuneration/types/UPDATE_PRT_REMUNERATION';
import {
  UPDATE_PRT_REMUNERATION_REGLEMENTVariables,
  UPDATE_PRT_REMUNERATION_REGLEMENT as UPDATE_PRT_REMUNERATION_REGLEMENT_TYPE,
} from '../../../../../federation/partenaire-service/remuneration/types/UPDATE_PRT_REMUNERATION_REGLEMENT';
import { getPharmacie } from '../../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { formatFilename } from '../../../../Common/Dropzone/Dropzone';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import { uploadToS3 } from '../../../../../services/S3';
import { PRTRemunerationReglementFilter } from '../../../../../types/federation-global-types';
import {
  GET_REMUNERATION_ANNEEVariables,
  GET_REMUNERATION_ANNEE as GET_REMUNERATION_ANNEE_TYPE,
} from '../../../../../federation/partenaire-service/remuneration/types/GET_REMUNERATION_ANNEE';
import {
  GET_REMUNERATION_PREVUE_EXIST as GET_REMUNERATION_PREVUE_EXIST_TYPE,
  GET_REMUNERATION_PREVUE_EXISTVariables,
} from '../../../../../federation/partenaire-service/remuneration/types/GET_REMUNERATION_PREVUE_EXIST';
import { endTime, startTime } from '../../DemarcheQualite/util';

export const useRemuneration = ({ tab, laboratoire, params, push }) => {
  const [dateFilter, setDateFilter] = useState<Date>(new Date());
  const [searchText, setSearchText] = useState<string>('');
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(0);
  const client = useApolloClient();
  const pharmacie = getPharmacie();

  const [savingRemuneration, setSavingRemuneration] = useState<boolean>(false);
  const [savedRemuneration, setSavedRemuneration] = useState<boolean>(false);
  const [remunerationToEdit, setRemunerationToEdit] = useState<any>(undefined);
  const [valeurRemunerationPrevuToSave, setValeurRemunerationPrevuToSave] = useState<any>(
    undefined,
  );
  const [sortTable, setSortTable] = useState<any>({
    column: null,
    direction: 'DESC',
  });

  const showError = (
    message: string = 'Des erreurs se sont survenues pendant le chargement des fichiers',
  ): void => {
    displaySnackBar(client, {
      type: 'ERROR',
      message: message,
      isOpen: true,
    });
  };

  //'#F46036', '#FBB104', '#FFE43A', '#C2C63F', '#00A745', '#008000'
  const backgroundRemuneration = pourcentage => {
    const tier = 100 / 6;
    const pourcentageTier = pourcentage;
    if (pourcentageTier >= 0 && pourcentage < tier) {
      return '#F46036';
    }
    if (pourcentageTier >= tier && pourcentage < tier * 2) {
      return '#FBB104';
    }
    if (pourcentageTier >= tier * 2 && pourcentage < tier * 3) {
      return '#FFE43A';
    }
    if (pourcentageTier >= tier * 3 && pourcentage < tier * 4) {
      return '#C2C63F';
    }
    if (pourcentageTier >= tier * 4 && pourcentage < tier * 5) {
      return '#00A745';
    }
    if (pourcentageTier >= tier * 5) {
      return '#008000';
    }
  };

  const variablesFilterFinal: PRTRemunerationReglementFilter[] = [
    {
      partenaireType: {
        eq: 'LABORATOIRE',
      },
    },
    {
      idPartenaireTypeAssocie: {
        eq: params.idLabo,
      },
    },
    {
      idPharmacie: {
        eq: pharmacie.id,
      },
    },
    {
      dateReglement: {
        between: {
          lower: `${dateFilter.getFullYear()}-01-01`,
          upper: `${dateFilter.getFullYear()}-12-31`,
        },
      },
    },
  ];

  const [loadReglementMode, loadingReglementMode] = useLazyQuery<
    GET_PRT_REGLEMENT_MODE_TYPE,
    GET_PRT_REGLEMENT_MODEVariables
  >(GET_PRT_REGLEMENT_MODE, { client: FEDERATION_CLIENT });

  const [loadPrestationType, loadingPrestationType] = useLazyQuery<
    GET_PRESTATION_TYPE_TYPE,
    GET_PRESTATION_TYPEVariables
  >(GET_PRESTATION_TYPE, { client: FEDERATION_CLIENT });

  const [loadRemuneration, loadingRemuneration] = useLazyQuery<
    GET_PRT_REMUNERATIONS_TYPE,
    GET_PRT_REMUNEARTIONSVariables
  >(GET_PRT_REMUNERATIONS, { client: FEDERATION_CLIENT });

  const [loadRemunerationPrevueExist, loadingRemunerationPrevueExist] = useLazyQuery<
    GET_REMUNERATION_PREVUE_EXIST_TYPE,
    GET_REMUNERATION_PREVUE_EXISTVariables
  >(GET_REMUNERATION_PREVUE_EXIST, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
  });

  const [loadRemunerationReglement, loadingRemunerationReglement] = useLazyQuery<
    GET_PRT_REMUNERATIONS_REGLEMENT_TYPE,
    GET_PRT_REMUNERATIONS_REGLEMENTVariables
  >(GET_PRT_REMUNERATIONS_REGLEMENTS, {
    client: FEDERATION_CLIENT,
  });

  const [createRemuneration, creatingRemuneration] = useMutation<
    CREATE_PRT_REMUNERATION_TYPE,
    CREATE_PRT_REMUNERATIONVariables
  >(CREATE_PRT_REMUNERATION, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La rémunération a été créee avec success',
        type: 'SUCCESS',
        isOpen: true,
      });
      setSavingRemuneration(false);
      setSavedRemuneration(true);
      loadingRemunerationAnnee.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs sont survenues pendant la création de la rémunération',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [updateRemuneration, updatingRemuneration] = useMutation<
    UPDATE_PRT_REMUNERATION_TYPE,
    UPDATE_PRT_REMUNERATIONVariables
  >(UPDATE_PRT_REMUNERATION, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La rémunération a été modifiéé avec success',
        type: 'SUCCESS',
        isOpen: true,
      });
      setSavingRemuneration(false);
      setSavedRemuneration(true);
      loadingRemunerationAnnee.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs sont survenues pendant la modification de la rémunération',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [createRemunerationReglement, creatingRemunerationReglement] = useMutation<
    CREATE_PRT_REMUNERATION_REGLEMENT_TYPE,
    CREATE_PRT_REMUNERATION_REGLEMENTVariables
  >(CREATE_PRT_REMUNERATION_REGLEMENT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La rémunération a été créee avec success',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingRemunerationReglement.refetch();
      loadingRemunerationAnnee.refetch();
      setSavingRemuneration(false);
      setSavedRemuneration(true);
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs sont survenues pendant la création de la rémunération',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  // FICHIERS
  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
  });

  const [updateRemunerationReglement, updatingRemunerationReglement] = useMutation<
    UPDATE_PRT_REMUNERATION_REGLEMENT_TYPE,
    UPDATE_PRT_REMUNERATION_REGLEMENTVariables
  >(UPDATE_PRT_REMUNERATION_REGLEMENT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La rémunération a été modifiéé avec success',
        type: 'SUCCESS',
        isOpen: true,
      });
      setSavingRemuneration(false);
      setSavedRemuneration(true);
      loadingRemunerationAnnee.refetch();
      loadingRemunerationReglement.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs sont survenues pendant la modification de la rémunération',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [deleteRemunerationReglement, deletingRemunerationReglement] = useMutation<
    DELETE_PRT_REMUNERATION_REGLEMENT_TYPE,
    DELETE_PRT_REMUNERATION_REGLEMENTVariables
  >(DELETE_PRT_REMUNERATION_REGLEMENT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La rémunération a été créee avec success',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingRemunerationReglement.refetch();
      loadingRemunerationAnnee.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs sont survenues pendant la création de la rémunération',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [loadRemunerationAnnee, loadingRemunerationAnnee] = useLazyQuery<
    GET_REMUNERATION_ANNEE_TYPE,
    GET_REMUNERATION_ANNEEVariables
  >(GET_REMUNERATION_ANNEE, { client: FEDERATION_CLIENT, fetchPolicy: 'cache-and-network' });

  useEffect(() => {
    if (tab === 'remuneration') {
      const sortingTableFilter = sortTable.column
        ? [
            {
              field: sortTable.column,
              direction: sortTable.direction,
            },
          ]
        : [];

      loadRemunerationReglement({
        variables: {
          filter: {
            and: variablesFilterFinal,
          },
          sorting: sortingTableFilter,
        },
      });
      loadRemunerationAnnee({
        variables: {
          input: {
            idPartenaireTypeAssocie: params.idLabo,
            partenaireType: 'LABORATOIRE',
            date: dateFilter,
          },
        },
      });
    }
  }, [tab, dateFilter, skip, take, searchText]);

  useEffect(() => {
    if (tab === 'remuneration') {
      loadReglementMode();
      loadPrestationType();
    }
  }, [tab]);

  useEffect(() => {
    if (valeurRemunerationPrevuToSave) {
      //  `${dateFilter.getFullYear()}-${valeurRemunerationPrevuToSave.month + 1}-01`

      const dateDebut = moment
        .utc()
        .set('year', dateFilter.getFullYear())
        .set('month', valeurRemunerationPrevuToSave.month - 1)
        .set('date', 1);

      const dateFin = dateDebut.clone().endOf('month');

      const dateDebutAsIsoString = startTime(dateDebut);
      const dateFinAsIsoString = endTime(dateFin);

      const idPrestationType = loadingPrestationType.data?.prestationTypes.nodes.find(
        prestation => prestation.code === valeurRemunerationPrevuToSave.type,
      )?.id;
      loadRemuneration({
        variables: {
          filter: {
            idPartenaireTypeAssocie: {
              eq: laboratoire.id,
            },
            idPharmacie: {
              eq: pharmacie.id,
            },
            partenaireType: {
              eq: 'LABORATOIRE',
            },
            dateEcheance: {
              between: {
                lower: dateDebutAsIsoString,
                upper: dateFinAsIsoString,
              },
            },
            idPrestationType: {
              eq: idPrestationType,
            },
          },
        },
      });
      createRemuneration({
        variables: {
          input: {
            dateEcheance: dateDebutAsIsoString,
            montantPrevu: parseFloat(valeurRemunerationPrevuToSave.montant),
            montantRealise: 0,
            idPrestationType: idPrestationType || '',
            partenaireType: 'LABORATOIRE',
            idPartenaireTypeAssocie: laboratoire?.id,
          },
        },
      });
    }

    // setValeurRemunerationPrevuToSave(undefined);
  }, [valeurRemunerationPrevuToSave, dateFilter]);

  const saveRemunerationFinal = (data: any, fichiers: any) => {
    if (data.mode === 'creation') {
      if (!data.id) {
        createRemuneration({
          variables: {
            input: {
              dateEcheance: data.dateEcheance,
              montantPrevu: data.estimation,
              montantRealise: data.remunerationFinal.montant,
              idPartenaireTypeAssocie: laboratoire.id,
              partenaireType: 'LABORATOIRE',
              idPrestationType: data.remunerationFinal.type.id,
            } as any,
          },
        }).then(createdRemuneration => {
          createRemunerationReglement({
            variables: {
              input: {
                dateReglement: data.remunerationFinal.date,
                idModeReglement: 'ckgqhc2w5345k0785wuedjyxm',
                idPrestationType: data.remunerationFinal.type.id,
                montant: data.remunerationFinal.montant,
                description: data.remunerationFinal.description,
                idRemuneration: createdRemuneration?.data?.createOnePRTRemuneration?.id,
                idPartenaireTypeAssocie: laboratoire.id,
                fichiers: fichiers,
                partenaireType: 'LABORATOIRE',
              },
            },
          });
        });
        return;
      }
      if (data.id) {
        updateRemuneration({
          variables: {
            id: data.id,
            input: {
              dateEcheance: data.dateEcheance,
              montantPrevu: data.estimation,
              montantRealise: data.remunerationFinal.montant,
              idPartenaireTypeAssocie: laboratoire.id,
              partenaireType: 'LABORATOIRE',
              idPrestationType: data.prestationType.id,
            } as any,
          },
        });
        createRemunerationReglement({
          variables: {
            input: {
              dateReglement: data.remunerationFinal.date,
              idModeReglement: 'ckgqhc2w5345k0785wuedjyxm',
              idPrestationType: data.remunerationFinal.type.id,
              montant: data.remunerationFinal.montant,
              description: data.remunerationFinal.description,
              idRemuneration: data.id,
              idPartenaireTypeAssocie: laboratoire.id,
              fichiers: fichiers,
              partenaireType: 'LABORATOIRE',
            },
          },
        });
        return;
      }
    }

    if (data.mode === 'modification') {
      updateRemunerationReglement({
        variables: {
          id: data.remunerationFinal.id,
          input: {
            dateReglement: data.remunerationFinal.date,
            idModeReglement: data.remunerationFinal.modePaiement?.id,
            idPrestationType: data.remunerationFinal.type.id,
            montant: data.remunerationFinal.montant,
            description: data.remunerationFinal.description,
            idRemuneration: data.id,
            idPartenaireTypeAssocie: laboratoire.id,
            fichiers: fichiers,
            partenaireType: 'LABORATOIRE',
          },
        },
      });
    }
  };

  const handleRequestSave = (data: any) => {
    setSavingRemuneration(true);
    setSavedRemuneration(false);
    if (data?.remunerationFinal.fichiers?.length > 0) {
      const newFiles = data?.remunerationFinal.fichiers.filter(
        file => file && file instanceof File,
      );
      const oldFiles = data?.remunerationFinal.fichiers
        .filter(file => file && !(file instanceof File))
        .map(file => {
          if (file) {
            return {
              chemin: file.chemin,
              nomOriginal: file.nomOriginal,
              type: file.type,
              publicUrl: file.publicUrl,
            };
          }
        });
      if (newFiles.length > 0) {
        const filePaths = newFiles.map(file => formatFilename(file, ``));
        doCreatePutPresignedUrl({
          variables: {
            filePaths,
          },
        }).then(async response => {
          if (response.data?.createPutPresignedUrls) {
            try {
              const fichiers = await Promise.all(
                response.data.createPutPresignedUrls.map((item, index) => {
                  if (item?.presignedUrl) {
                    return data?.remunerationFinal.fichiers
                      ? uploadToS3(
                          data?.remunerationFinal.fichiers[index],
                          item?.presignedUrl,
                        ).then(uploadResult => {
                          if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                            return {
                              chemin: item.filePath,
                              nomOriginal: data.fichiers ? data.fichiers[index].name : '',
                              type: data.fichiers ? data.fichiers[index].type : '',
                            };
                          }
                        })
                      : null;
                  }
                }),
              );
              saveRemunerationFinal(data, [...fichiers, ...oldFiles]);
            } catch (error) {
              showError();
            }
          } else {
            showError();
          }
        });
      } else {
        if (oldFiles.lenght > 0) {
          saveRemunerationFinal(data, oldFiles);
        } else {
          saveRemunerationFinal(data, undefined);
        }
      }
    } else {
      saveRemunerationFinal(data, undefined);
    }
  };

  const handleRequestDeleteRemunerationReglement = (data: any) => {
    deleteRemunerationReglement({
      variables: {
        id: data.id,
      },
    });
  };

  const handleRequestEdit = (data: any) => {
    setRemunerationToEdit(data);
  };

  const handleRequestSearch = ({ filter, searchText, skip, take, sortTable }) => {
    setDateFilter(moment(filter[0].date).toDate());
    setSkip(skip);
    setTake(take);
    setSearchText(searchText);
    setSortTable(sortTable);
  };

  const handleRequestSavingInputRemuneration = (value: string, params: any) => {
    setValeurRemunerationPrevuToSave({
      montant: value,
      month: params.month,
      type: params.column,
    });
  };

  const handleRequestFilterRemuneration = (filter: any) => {
    const date = filter.date;
    const type = filter.type;
    if (laboratoire && type.id) {
      loadRemunerationPrevueExist({
        variables: {
          input: {
            dateEcheance: `${date.getFullYear()}-${moment(date).month() + 1}-01`,
            idPartenaireTypeAssocie: laboratoire.id,
            partenaireType: 'LABORATOIRE',
            idPrestationType: type?.id,
          },
        },
      });
    }
  };

  const handleGoBack = () => {
    push('/laboratoires');
  };

  const dataLoadingRemunerationAnnee = loadingRemunerationAnnee.data?.remunerationAnnee;

  const dataRemunerationAnnee = [
    {
      laboratoire: dataLoadingRemunerationAnnee?.nomPartenaire || '',
      composition: dataLoadingRemunerationAnnee?.composition || '',
      total: {
        prevue: {
          value: dataLoadingRemunerationAnnee?.totalPrevues || 0,
        },
        reel: {
          value: dataLoadingRemunerationAnnee?.totalRealises || 0,
          percentage: dataLoadingRemunerationAnnee?.pourcentageRealises || 0,
          background: dataLoadingRemunerationAnnee?.totalPrevues
            ? backgroundRemuneration(dataLoadingRemunerationAnnee?.pourcentageRealises || 0)
            : '#FFF',
        },
      },
      totalVerse: {
        prevue: {
          value: '0.00',
        },
        reel: {
          value: '0.00',
        },
      },
      months: (dataLoadingRemunerationAnnee?.dataAnnee || []).map(element => {
        const coop = (element.data.types || []).find(type => type.prestation.code === 'COOP');
        const bri = (element.data.types || []).find(type => type.prestation.code === 'BRI');

        return {
          month: element.indexMois,
          prevue: {
            coop: {
              value: coop?.totalMontantPrevuPrestation || '',
              background: '',
              tooltip: '',
            },
            bri: {
              value: bri?.totalMontantPrevuPrestation || '',
              background: '',
              tooltip: '',
            },
            total: {
              value: element.data.totalMontantPrevues || '',
            },
          },
          reel: {
            coop: {
              value: coop?.totalMontantReglementPrestation || '',
              percentage: coop?.pourcentagePrestation || '',
              background: coop?.totalMontantPrevuPrestation
                ? backgroundRemuneration(coop?.pourcentagePrestation || 0)
                : '#FFF',
            },
            bri: {
              value: bri?.totalMontantReglementPrestation || '',
              percentage: bri?.pourcentagePrestation || '',
              background: bri?.totalMontantPrevuPrestation
                ? backgroundRemuneration(bri?.pourcentagePrestation || 0)
                : '#FFF',
            },
            total: {
              value: element.data.totalMontantReglements || '',
              percentage: element.data.pourcentageReelReglement || '',
              background: element.data.totalMontantReglements
                ? backgroundRemuneration(element.data.pourcentageReelReglement || 0)
                : '#FFF',
            },
          },
        };
      }),
    },
  ];

  const dataRemuneration = loadingRemunerationReglement.data?.pRTRemunerationReglements.nodes.map(
    element => {
      return {
        ...element,
        dateReglement: moment(element.dateReglement),
        remuneration: {
          ...element.remuneration,
          dateEcheance: moment(element.remuneration.dateEcheance),
        },
      };
    },
  );

  const remunerationPrevueExist = loadingRemunerationPrevueExist?.data?.remunerationPrevueExist;

  const remunerationPageProps = {
    listPrestationType: {
      data: loadingPrestationType.data?.prestationTypes.nodes,
      error: loadingPrestationType.error,
      loading: loadingPrestationType.loading,
    },
    listTypeRemuneration: loadingPrestationType.data?.prestationTypes.nodes,
    listModePaiement: loadingReglementMode.data?.pRTReglementModes.nodes,
    listReglementMode: {
      data: loadingReglementMode.data?.pRTReglementModes.nodes,
      error: loadingReglementMode.error,
      loading: loadingReglementMode.loading,
    },
    remunerations: {
      data: dataRemuneration as any,
      error: loadingRemunerationReglement.error as any,
      loading: loadingRemunerationReglement.loading,
    },
    onRequestSaving: handleRequestSave,
    onRequestDelete: handleRequestDeleteRemunerationReglement,
    onRequestSearch: handleRequestSearch,
    onRequestEdit: handleRequestEdit,
    onRequestFilterRemuneration: handleRequestFilterRemuneration,
    remunerationPrevue: remunerationPrevueExist,
    remunerationToEdit: { ...remunerationToEdit, file: remunerationToEdit?.fichiers },
    saving: savingRemuneration,
    saved: savedRemuneration,
    setSaved: setSavedRemuneration,
    laboratoire,
    collone: loadingPrestationType.data?.prestationTypes.nodes,
    savingInputRemuneration: {
      loading: valeurRemunerationPrevuToSave ? creatingRemuneration.loading : false,
      onRequestSaveInput: handleRequestSavingInputRemuneration,
    },
    remunerationsTables: {
      data: dataRemunerationAnnee,
      error: loadingRemunerationAnnee?.error as any,
      loading: loadingRemunerationAnnee?.loading,
    },
    onRequestGoBack: handleGoBack,
  };

  return remunerationPageProps;
};
