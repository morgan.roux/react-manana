import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import moment from 'moment';
import { QueryResult } from 'react-apollo';
import { CHANGE_STATUS_ACTION } from '../../../../../federation/basis/todo/action/mutation';
import {
  ACTIONS,
  ACTIONSVariables,
} from '../../../../../federation/basis/todo/action/types/ACTIONS';
import {
  CHANGE_STATUS_ACTION as CHANGE_STATUS_ACTION_TYPE,
  CHANGE_STATUS_ACTIONVariables,
} from '../../../../../federation/basis/todo/action/types/CHANGE_STATUS_ACTION';
import { GET_PLANNING_MARKETINGS } from '../../../../../federation/partenaire-service/PRTPlanningMarketing/query';
import { GET_SEARCH_PLAN_MARKETING_PRODUIT_CANALS } from '../../../../../graphql/ProduitCanal/query';
import { SEARCH_PLAN_MARKETING_PRODUIT_CANALSVariables } from '../../../../../graphql/ProduitCanal/types/SEARCH_PLAN_MARKETING_PRODUIT_CANALS';
import { getPharmacie } from '../../../../../services/LocalStorage';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { useEffect } from 'react';

export const usePlanningPlanMarketing = (
  idPartenaireTypeAssocie?: string,
): [
  (searchText: string, date: any, limit?: number, offset?: number) => void,
  any,
  boolean,
  (idPlanMarketing: string, produits: string[]) => void,
  QueryResult<any, SEARCH_PLAN_MARKETING_PRODUIT_CANALSVariables>,
  (id: string, statut: 'DONE' | 'ACTIVE') => void,
] => {
  const pharmacie = getPharmacie();

  const [loadPlanningPlanMarketings, loadingPlanningPlanMarketings] = useLazyQuery<any>(
    GET_PLANNING_MARKETINGS,
    {
      fetchPolicy: 'cache-and-network',
      client: FEDERATION_CLIENT,
    },
  );

  const [changeStatutAction, changingStaturAction] = useMutation<
    CHANGE_STATUS_ACTION_TYPE,
    CHANGE_STATUS_ACTIONVariables
  >(CHANGE_STATUS_ACTION, { client: FEDERATION_CLIENT });

  const [loadProduits, loadingProduits] = useLazyQuery<
    any,
    SEARCH_PLAN_MARKETING_PRODUIT_CANALSVariables
  >(GET_SEARCH_PLAN_MARKETING_PRODUIT_CANALS);

  const pRTPlanningMarketings = loadingPlanningPlanMarketings.data?.pRTPlanningMarketing || [];

  const data = pRTPlanningMarketings.map(el => ({
    prestataire: el.partenaireTypeAssocie?.nom,
    planMarketings: (el.planMarketings || []).map(el => {
      console.log(el);
      const end = moment(el.dateFin).week();
      const start = moment(el.dateDebut).week();
      const week =
        end === start ? [start] : Array.from({ length: end - start }, (_x, i) => start + i);
      return { week, titre: el.titre, color: el.type?.couleur, planMarketing: el };
    }),
  }));

  const handleRequestPlanning = (
    searchText: string,
    date: any,
    limit?: number,
    offset?: number,
  ) => {
    const input = {
      annee: moment(date).year(),
      partenaireType: 'LABORATOIRE',
      idPharmacie: pharmacie?.id,
      searchText,
      idPartenaireTypeAssocie,
      offset,
      limit,
    };
    loadPlanningPlanMarketings({
      variables: {
        input,
      },
    });
  };

  const handleRequestSearchProduits = (ids: string[]) => {
    let must: any = [
      {
        term: {
          'commandeCanal.code': 'PFL',
        },
      },
      {
        term: {
          'produit.produitTechReg.laboExploitant.id': idPartenaireTypeAssocie,
        },
      },
    ];

    if (ids && ids.length) {
      must.push({
        terms: {
          _id: ids,
        },
      });
    }

    const variables: SEARCH_PLAN_MARKETING_PRODUIT_CANALSVariables = {
      type: ['produitcanal'],
      query: {
        query: {
          bool: {
            must,
          },
        },
      },
    };

    if (ids.length) {
      loadProduits({ variables });
    }
  };

  const handleOnRequestProduits = (idPlanMarketing: string, produits: string[]) => {
    handleRequestSearchProduits(produits);
  };

  const handleOnRequestChangeStatutAction = (id: string, status: 'ACTIVE' | 'DONE') => {
    changeStatutAction({
      variables: {
        id,
        status,
      },
    });
  };

  return [
    handleRequestPlanning,
    data,
    loadingPlanningPlanMarketings.loading,
    handleOnRequestProduits,
    loadingProduits,
    handleOnRequestChangeStatutAction,
  ];
};
