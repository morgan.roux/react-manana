import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { useCallback, useEffect, useState } from 'react';
import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import React from 'react';
import {
  GET_LIST_SUIVI_OPERATIONNEL_TYPES as GET_LIST_SUIVI_OPERATIONNEL_TYPES_TYPE,
  GET_LIST_SUIVI_OPERATIONNEL_TYPESVariables,
} from '../../../../../federation/partenaire-service/suivi-operationnel/types/GET_LIST_SUIVI_OPERATIONNEL_TYPES';

import {
  GET_LIST_SUIVI_OPERATIONNEL_STATUT as GET_LIST_SUIVI_OPERATIONNEL_STATUT_TYPE,
  GET_LIST_SUIVI_OPERATIONNEL_STATUTVariables,
} from '../../../../../federation/partenaire-service/suivi-operationnel/types/GET_LIST_SUIVI_OPERATIONNEL_STATUT';

import {
  GET_LIST_SUIVI_OPERATIONNEL_TYPES,
  GET_LIST_SUIVI_OPERATIONNEL_STATUT,
  GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU,
  GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_TOTAL_ROWS,
} from '../../../../../federation/partenaire-service/suivi-operationnel/query';
import CommentList from '../../../../Comment/CommentList';
import { Comment } from '../../../../Comment';
import {
  GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_TOTAL_ROWS as GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_TOTAL_ROWS_TYPE,
  GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_TOTAL_ROWSVariables,
} from '../../../../../federation/partenaire-service/suivi-operationnel/types/GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_TOTAL_ROWS';

import {
  GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU as GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_TYPE,
  GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDUVariables,
} from '../../../../../federation/partenaire-service/suivi-operationnel/types/GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU';

import {
  GET_SUIVI_OPERATIONNELS as GET_SUIVI_OPERATIONNELS_TYPE,
  GET_SUIVI_OPERATIONNELSVariables,
} from '../../../../../federation/partenaire-service/suivi-operationnel/types/GET_SUIVI_OPERATIONNELS';
import { GET_SUIVI_OPERATIONNELS } from '../../../../../federation/partenaire-service/suivi-operationnel/query';

import {
  CREATE_SUIVI_OPERATIONNEL as CREATE_SUIVI_OPERATIONNEL_TYPE,
  CREATE_SUIVI_OPERATIONNELVariables,
} from '../../../../../federation/partenaire-service/suivi-operationnel/types/CREATE_SUIVI_OPERATIONNEL';
import {
  UPDATE_SUIVI_OPERATIONNEL as UPDATE_SUIVI_OPERATIONNEL_TYPE,
  UPDATE_SUIVI_OPERATIONNELVariables,
} from '../../../../../federation/partenaire-service/suivi-operationnel/types/UPDATE_SUIVI_OPERATIONNEL';
import {
  DELETE_ONE_SUIVI_OPERATIONNEL as DELETE_ONE_SUIVI_OPERATIONNEL_TYPE,
  DELETE_ONE_SUIVI_OPERATIONNELVariables,
} from '../../../../../federation/partenaire-service/suivi-operationnel/types/DELETE_ONE_SUIVI_OPERATIONNEL';
import {
  CHANGE_SUIVI_OPERTIONNEL_STATUT as CHANGE_SUIVI_OPERTIONNEL_STATUT_TYPE,
  CHANGE_SUIVI_OPERTIONNEL_STATUTVariables,
} from '../../../../../federation/partenaire-service/suivi-operationnel/types/CHANGE_SUIVI_OPERTIONNEL_STATUT';
import {
  CREATE_SUIVI_OPERATIONNEL,
  UPDATE_SUIVI_OPERATIONNEL,
  DELETE_ONE_SUIVI_OPERATIONNEL,
  CHANGE_SUIVI_OPERTIONNEL_STATUT,
} from '../../../../../federation/partenaire-service/suivi-operationnel/mutation';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';

import { DO_CREATE_GET_PRESIGNED_URL, DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import { uploadToS3 } from '../../../../../services/S3';
import { formatFilename } from '../../../../Common/Dropzone/Dropzone';
import { PRTSuiviOperationnelFilter } from '../../../../../types/federation-global-types';
import { getGroupement, getPharmacie } from '../../../../../services/LocalStorage';
import {
  FilterAjoutCompteRendu,
  SuiviProps,
} from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/Suivi/interface';
import UserInput from '../../../../Common/UserInput';
import ImportanceInput from '../../../../Common/ImportanceInput';
import {
  ExternalFormDataSuiviOperationnel,
  SuiviFormRequestSavingProps,
} from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/Suivi/SuiviForm/SuiviFormProps';
import CommonFieldsForm from '../../../../Common/CommonFieldsForm';
import UserAction from '../../../../Common/UserAction';
import { content } from 'html2canvas/dist/types/css/property-descriptors/content';
import { COMMENTS, COMMENTSVariables } from '../../../../../graphql/Comment/types/COMMENTS';
import { GET_COMMENTS } from '../../../../../graphql/Comment';
import { ErrorPage, Loader } from '@app/ui-kit';
import {
  USER_SMYLEYS,
  USER_SMYLEYSVariables,
} from '../../../../../graphql/Smyley/types/USER_SMYLEYS';
import { GET_SMYLEYS, GET_USER_SMYLEYS } from '../../../../../graphql/Smyley';
import { SMYLEYS, SMYLEYSVariables } from '../../../../../graphql/Smyley/types/SMYLEYS';
import {
  CREATE_GET_PRESIGNED_URL,
  CREATE_GET_PRESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_GET_PRESIGNED_URL';
import useStyles from '../styles';

interface Suivi {
  error?: Error;
  data?: any[];
  loading?: boolean;
}

export const useSuiviOperationnel = (idCurrentLabo: string, push: any) => {
  const pharmacie = getPharmacie();
  const client = useApolloClient();
  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const [suivi, setSuivi] = useState<Suivi | undefined>();
  const [compteRendu, setCompteRendu] = useState<boolean>(false);
  const [totalRow, setTotalRow] = useState<number>(0);
  const [externalFormData, setExternalFormData] = useState<
    ExternalFormDataSuiviOperationnel | undefined
  >();
  const [openModalResponsable, setOpenModalResponsable] = useState<boolean>(false);
  const [responsables, setResponsables] = useState<any[]>([]);
  const [suiviOperationnelChoosen, setSuiviOperationnelChoosen] = useState<any>();
  const classes = useStyles();

  // QUERY LISTE DES TYPES
  const suiviOperationnelTypes = useQuery<
    GET_LIST_SUIVI_OPERATIONNEL_TYPES_TYPE,
    GET_LIST_SUIVI_OPERATIONNEL_TYPESVariables
  >(GET_LIST_SUIVI_OPERATIONNEL_TYPES, { client: FEDERATION_CLIENT });

  // QUERY LISTE DES STATUTS
  const suiviOperationnelStatuts = useQuery<
    GET_LIST_SUIVI_OPERATIONNEL_STATUT_TYPE,
    GET_LIST_SUIVI_OPERATIONNEL_STATUTVariables
  >(GET_LIST_SUIVI_OPERATIONNEL_STATUT, { client: FEDERATION_CLIENT });

  // FICHIERS
  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
  });

  // QUERY SUIVI
  const [loadSuiviOperationnel, loadingSuiviOperationnel] = useLazyQuery<
    GET_SUIVI_OPERATIONNELS_TYPE,
    GET_SUIVI_OPERATIONNELSVariables
  >(GET_SUIVI_OPERATIONNELS, {
    client: FEDERATION_CLIENT,
  });

  /**create */
  const [createSuiviOperationnel, creatingSuiviOperationnel] = useMutation<
    CREATE_SUIVI_OPERATIONNEL_TYPE,
    CREATE_SUIVI_OPERATIONNELVariables
  >(CREATE_SUIVI_OPERATIONNEL, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: ' Le Suivi Operationnel a été crée avec succés !',
        type: 'SUCCESS',
        isOpen: true,
      });

      loadingSuiviOperationnel.refetch();

      setSaving(false);
      setSaved(true);
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Erreurs lors de la création du Suivi Operationnel ',
        type: 'ERROR',
        isOpen: true,
      });

      setSaving(false);
    },
    client: FEDERATION_CLIENT,
  });

  /**update Suivi Operationnel */
  const [updateSuiviOperationnel, updatingSuiviOperationnel] = useMutation<
    UPDATE_SUIVI_OPERATIONNEL_TYPE,
    UPDATE_SUIVI_OPERATIONNELVariables
  >(UPDATE_SUIVI_OPERATIONNEL, {
    onCompleted: data => {
      setSuiviOperationnelChoosen(data.updateOnePRTSuiviOperationnel);
      displaySnackBar(client, {
        message: 'Le Suivi Operationnel a été modifié avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });

      setSaving(false);
      setSaved(true);
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la modification du Suivi Operationnel',
        type: 'ERROR',
        isOpen: true,
      });

      setSaving(false);
    },
    client: FEDERATION_CLIENT,
  });

  //Delete Suivi Operationnel
  const [deleteSuiviOperationnel, deletingSuiviOperationnel] = useMutation<
    DELETE_ONE_SUIVI_OPERATIONNEL_TYPE,
    DELETE_ONE_SUIVI_OPERATIONNELVariables
  >(DELETE_ONE_SUIVI_OPERATIONNEL, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le Suivi Operationnel a été supprimé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingSuiviOperationnel.refetch();
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression du Suivi Operationnel',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  //Change Suivi Operationnel Statut
  const [changeSuiviStatut, changingSuiviStatut] = useMutation<
    CHANGE_SUIVI_OPERTIONNEL_STATUT_TYPE,
    CHANGE_SUIVI_OPERTIONNEL_STATUTVariables
  >(CHANGE_SUIVI_OPERTIONNEL_STATUT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le statut a été modifié avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingSuiviOperationnel.refetch();
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant le changement du statut',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const saveSuiviOperationnel = (suiviOperationnel: SuiviFormRequestSavingProps, fichiers: any) => {
    if (suiviOperationnel.id) {
      const toSave = {
        montant: parseFloat(suiviOperationnel.montant as any),
        idImportance: suiviOperationnel.idImportance,
        idTache: suiviOperationnel.idTache,
        idFonction: suiviOperationnel.idFontion,
        idContacts: suiviOperationnel.idContacts,
        titre: suiviOperationnel.titre,
        description: suiviOperationnel.description,
        dateHeure: suiviOperationnel.dateSuivi,
        partenaireType: 'LABORATOIRE',
        idTypeAssocie: idCurrentLabo,
        idType: suiviOperationnel.idTypeSuivi,
        fichiers,
        idParticipants: suiviOperationnel.idParticipants,
      };
      updateSuiviOperationnel({
        variables: {
          id: suiviOperationnel.id,
          input: (suiviOperationnel as any).idStatut
            ? { ...toSave, idStatut: (suiviOperationnel as any).idStatut }
            : toSave,
        },
      });
    } else {
      createSuiviOperationnel({
        variables: {
          input: {
            montant: parseFloat(suiviOperationnel.montant as any),
            idImportance: suiviOperationnel.idImportance,
            idTache: suiviOperationnel.idTache,
            idFonction: suiviOperationnel.idFontion,
            idContacts: suiviOperationnel.idContacts,
            titre: suiviOperationnel.titre,
            description: suiviOperationnel.description,
            dateHeure: suiviOperationnel.dateSuivi,
            partenaireType: 'LABORATOIRE',
            idTypeAssocie: idCurrentLabo,
            idType: suiviOperationnel.idTypeSuivi,
            fichiers,
            idParticipants: suiviOperationnel.idParticipants,
          },
        },
      });
    }
  };

  // Save suivi Props
  const handleSuiviSave = (suiviOperationnel: SuiviFormRequestSavingProps) => {
    setSaving(true);
    setSaved(false);
    if (suiviOperationnel.selectedFiles && suiviOperationnel.selectedFiles.length > 0) {
      const newFiles = suiviOperationnel.selectedFiles.filter(file => file && file instanceof File);
      const oldFiles = suiviOperationnel.selectedFiles
        .filter(file => file && !(file instanceof File))
        .map(file => {
          if (file) {
            return {
              chemin: file.chemin,
              nomOriginal: file.nomOriginal,
              type: file.type,
            };
          }
        });

      if (newFiles?.length > 0) {
        const filePaths = newFiles.map(file => formatFilename(file, ``));
        doCreatePutPresignedUrl({
          variables: {
            filePaths,
          },
        }).then(async response => {
          if (response.data?.createPutPresignedUrls) {
            try {
              const fichiers = await Promise.all(
                response.data.createPutPresignedUrls.map((item, index) => {
                  if (item?.presignedUrl) {
                    return suiviOperationnel.selectedFiles
                      ? uploadToS3(suiviOperationnel.selectedFiles[index], item?.presignedUrl).then(
                          uploadResult => {
                            if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                              return {
                                chemin: item.filePath,
                                nomOriginal: suiviOperationnel.selectedFiles
                                  ? suiviOperationnel.selectedFiles[index].name
                                  : '',
                                type: suiviOperationnel.selectedFiles
                                  ? suiviOperationnel.selectedFiles[index].type
                                  : '',
                              };
                            }
                          },
                        )
                      : null;
                  }
                }),
              );
              saveSuiviOperationnel(suiviOperationnel, [...fichiers, ...oldFiles]);
            } catch (error) {}
          } else {
          }
        });
      } else {
        if (oldFiles?.length > 0) {
          saveSuiviOperationnel(suiviOperationnel, oldFiles);
        } else {
          saveSuiviOperationnel(suiviOperationnel, undefined);
        }
      }
    } else {
      saveSuiviOperationnel(suiviOperationnel, undefined);
    }
  };

  const handleFetchMoreSuivis = () => {
    const fetchMore = loadingSuiviOperationnel.fetchMore;
    const variables = loadingSuiviOperationnel.variables;
    const paging = variables.paging;
    if (loadingSuiviOperationnel.data?.pRTSuiviOperationnels.nodes) {
      fetchMore({
        variables: {
          ...variables,
          paging: {
            ...paging,
            offset: loadingSuiviOperationnel.data?.pRTSuiviOperationnels.nodes,
          },
        },

        updateQuery: (prev: any, { fetchMoreResult }) => {
          if (prev?.pRTSuiviOperationnels && fetchMoreResult?.pRTSuiviOperationnels.nodes) {
            return {
              ...prev,
              pRTSuiviOperationnels: {
                ...prev.pRTSuiviOperationnels,
                nodes: [
                  ...prev.pRTSuiviOperationnels.nodes,
                  ...fetchMoreResult.pRTSuiviOperationnels.nodes,
                ],
              },
            };
          }

          return prev;
        },
      });
    }
  };

  // Delete
  const handleSuiviDelete = (suiviOperationnel: any) => {
    deleteSuiviOperationnel({
      variables: {
        input: { id: suiviOperationnel.id },
      },
    });
  };

  const [loadTotalForCompteRendu, loadingTotalForCompteRendu] = useLazyQuery<
    GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_TOTAL_ROWS_TYPE,
    GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_TOTAL_ROWSVariables
  >(GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_TOTAL_ROWS, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  const [loadSuiviFromCompteRendu, loadingSuivFromCompteRendu] = useLazyQuery<
    GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_TYPE,
    GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDUVariables
  >(GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
  });

  useEffect(() => {
    if (compteRendu) {
      setSuivi({
        data: loadingSuivFromCompteRendu.data?.getSuiviOperationnelForCompteRendu || [],
        loading: loadingSuivFromCompteRendu.loading,
        error: loadingSuivFromCompteRendu.error,
      });
      setTotalRow(
        loadingTotalForCompteRendu.data?.getSuiviOperationnelForCompteRenduRowsTotal || 0,
      );
    } else {
      setSuivi({
        data: loadingSuiviOperationnel.data?.pRTSuiviOperationnels.nodes || [],
        loading: loadingSuiviOperationnel.loading,
        error: loadingSuiviOperationnel.error,
      });
    }
  }, [loadingSuiviOperationnel, loadingSuivFromCompteRendu, loadingTotalForCompteRendu]);

  const handleSuiviSearch = ({
    skip,
    take,
    searchText,
    filters,
    filtreCompteRendu,
    sortTable,
  }: any) => {
    if (idCurrentLabo) {
      if (filtreCompteRendu && filtreCompteRendu.active === true) {
        setCompteRendu(true);
        loadSuiviFromCompteRendu({
          variables: {
            idTypeAssocie: idCurrentLabo,
            idCompteRendu: filtreCompteRendu.idCompteRendu,
            searchText,
            skip,
            take,
          },
        });
        loadTotalForCompteRendu({
          variables: {
            idTypeAssocie: idCurrentLabo,
            idCompteRendu: filtreCompteRendu.idCompteRendu,
            searchText,
          },
        });
      } else {
        setCompteRendu(false);
        const keywords = ['idType', 'idStatut'];
        const _filters = keywords
          .map(keyword => {
            const tempObject = { or: [] };
            for (let filter of filters || []) {
              if (filter.keyword === keyword) {
                tempObject.or.push({
                  [keyword]: {
                    eq: filter.id,
                  },
                } as never);
              }
            }
            return tempObject.or.length > 0 ? tempObject : undefined;
          })
          .filter(e => e);

        const filterAnd: PRTSuiviOperationnelFilter[] = [
          {
            partenaireType: {
              eq: 'LABORATOIRE',
            },
          },
          {
            idTypeAssocie: {
              eq: idCurrentLabo,
            },
          },
          {
            idPharmacie: {
              eq: pharmacie.id,
            },
          },
          {
            or: [
              {
                titre: {
                  iLike: `%${searchText}%`,
                },
              },
              {
                description: {
                  iLike: `%${searchText}%`,
                },
              },
            ],
          },
        ];

        const sortTableFilter = sortTable.column
          ? [
              {
                field: sortTable.column,
                direction: sortTable.direction,
              },
            ]
          : [];

        if (_filters.length > 0) {
          _filters.map(filter => {
            filterAnd.push(filter as any);
          });
        }

        loadSuiviOperationnel({
          variables: {
            filter: {
              and: filterAnd,
            },
            paging: {
              offset: skip,
              limit: take,
            },
            sorting: sortTableFilter,
          },
        });
      }
    }
  };

  // Changement de Statut
  const handleSuiviChangeStatut = (id: string, idStatut: string) => {
    changeSuiviStatut({
      variables: {
        id,
        idStatut,
      },
    });
  };

  const listType = {
    error: suiviOperationnelTypes.error as any,
    loading: suiviOperationnelTypes.loading,
    data: suiviOperationnelTypes.data?.pRTSuiviOperationnelTypes.nodes || [],
  };

  const listStatutSuivi = suiviOperationnelStatuts.data?.pRTSuiviOperationnelStatuts.nodes || [];

  const handleParticipant = (participants: any[], _id?: string) => {
    if (participants.length > 0) {
      setResponsables([]);
    }
    setExternalFormData(prev => ({ ...prev, selectedParticipant: participants }));
  };

  const handleChangeImportance = (importance: any) => {
    // if (importance) {
    //   setResponsables([]);
    // }
    setExternalFormData(prev => ({ ...prev, selectedImportance: importance }));
  };

  const handleChangeProject = (project: any) => {
    // if (project) {
    //   setResponsables([]);
    // }
    setExternalFormData(prev => ({ ...prev, selectedTache: project }));
  };

  const handleChangePojectWhenEdit = (idTache: string, idFonction: string) => {
    // if (idTache || idFonction) {
    //   setResponsables([]);
    // }
    setExternalFormData(prev => ({ ...prev, selectedTache: { idTache, idFonction } }));
  };

  const handleGoBack = () => {
    push('/laboratoires');
  };

  // /////////////////// REACTION AND COMMENTS ///////////////////////////////////////////

  const [doCreateGetPresignedUrls, doCreateGetPresignedUrlsResult] = useMutation<
    CREATE_GET_PRESIGNED_URL,
    CREATE_GET_PRESIGNED_URLVariables
  >(DO_CREATE_GET_PRESIGNED_URL);

  const getSmyleys = useQuery<SMYLEYS, SMYLEYSVariables>(GET_SMYLEYS, {
    variables: {
      idGroupement: getGroupement().id,
    },

    onCompleted: smyleysResult => {
      if (smyleysResult.smyleys) {
        const filePaths: string[] = smyleysResult.smyleys
          .filter(item => !!item?.photo)
          .map((item: any) => item.photo);

        if (filePaths.length > 0) {
          doCreateGetPresignedUrls({ variables: { filePaths } });
        }
      }
    },

    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        });
      });
    },
  });

  const [loadUserSmyleys, loadingUserSmyles] = useLazyQuery<USER_SMYLEYS, USER_SMYLEYSVariables>(
    GET_USER_SMYLEYS,
  );

  const [loadCommentaires, loadingCommentaire] = useLazyQuery<COMMENTS, COMMENTSVariables>(
    GET_COMMENTS,
  );

  const handleRefecthComments = useCallback(() => {
    loadingCommentaire.refetch();
  }, [loadingCommentaire]);

  const handleFecthMoreComments = () => {};

  const handleRefecthSmyles = () => () => {
    loadingUserSmyles.refetch();
  };

  useEffect(() => {
    if (suiviOperationnelChoosen) {
      loadCommentaires({
        variables: { codeItem: 'SUIVI_OPERATIONNEL', idItemAssocie: suiviOperationnelChoosen.id },
      });
      loadUserSmyleys({
        variables: {
          codeItem: 'SUIVI_OPERATIONNEL',
          idSource: suiviOperationnelChoosen.id,
        },
      });
    }
  }, [suiviOperationnelChoosen]);

  // ///////////////////////////////////////////////////////////////////////////////////////

  const laboratoireSuivi = {
    onRequestGoBack: handleGoBack,
    suivi,
    onRequest: {
      save: handleSuiviSave,
      delete: handleSuiviDelete,
      next: handleSuiviSearch,
      changeStatut: handleSuiviChangeStatut,
      fetchMore: handleFetchMoreSuivis,
    },
    listType,
    listStatutSuivi,
    saving,
    saved,
    allSelectedFromcommonFieldsComponent: {
      selectedParticipant: (externalFormData?.selectedParticipant || []).map(item => item.id),
      selectedTache: externalFormData?.selectedTache?.idTache,
      selectedFonction: externalFormData?.selectedTache?.idFonction,
      selectedImportance: externalFormData?.selectedImportance?.id,
    },
    commonFieldsComponent: (
      <CommonFieldsForm
        selectedUsers={externalFormData?.selectedParticipant}
        urgence={null}
        urgenceProps={{
          useCode: false,
        }}
        noMarginTop
        usersModalProps={{
          withNotAssigned: false,
          searchPlaceholder: 'Rechercher...',
          withAssignTeam: false,
          singleSelect: false,
        }}
        projet={
          externalFormData?.selectedTache?.idFonction || externalFormData?.selectedTache?.idTache
        }
        importance={externalFormData?.selectedImportance}
        onChangeUsersSelection={handleParticipant}
        onChangeProjet={handleChangeProject}
        onChangeImportance={handleChangeImportance}
        radioImportance={false}
        labelImportance="Importance"
      />
    ),
    setSelectParticipants: handleParticipant,
    setSaved: () => setSaved(false),
    setSelectImportanceToClient: handleChangeImportance,
    setTacheAndFonctionToClient: handleChangePojectWhenEdit,
    responsableComponnent: (
      <UserInput
        className={classes.userInput}
        openModal={openModalResponsable}
        idPartenaireTypeAssocie={idCurrentLabo}
        withAssignTeam={false}
        withNotAssigned={false}
        partenaireType="LABORATOIRE"
        category="CONTACT"
        setOpenModal={setOpenModalResponsable}
        selected={responsables}
        setSelected={value => {
          setResponsables(value);
          setExternalFormData({ ...externalFormData, selectedParticipant: undefined });
        }}
        label="Responsables"
        title="Responsables"
      />
    ),
    responsableSelected: responsables,
    setResponsableSelected: responsables => {
      if (responsables.length > 0) {
        setExternalFormData({ ...externalFormData, selectedParticipant: undefined });
      }
      setResponsables(responsables);
    },
    setSuiviOperationnelChoosen: (suivi: any) => {
      setSuiviOperationnelChoosen(suivi);
    },
    suiviOperationnelSaved: suiviOperationnelChoosen,
    commentaireComponnent: suiviOperationnelChoosen ? (
      <>
        <UserAction
          refetchSmyleys={handleRefecthSmyles}
          codeItem="SUIVI_OPERATIONNEL"
          idSource={suiviOperationnelChoosen.id}
          nbComment={suiviOperationnelChoosen.nombreCommentaires}
          nbSmyley={suiviOperationnelChoosen.nombreReaction}
          userSmyleys={loadingUserSmyles.data?.userSmyleys as any}
          presignedUrls={
            doCreateGetPresignedUrlsResult &&
            doCreateGetPresignedUrlsResult.data &&
            doCreateGetPresignedUrlsResult.data.createGetPresignedUrls
          }
          smyleys={getSmyleys && getSmyleys.data && getSmyleys.data.smyleys}
          hidePartage
          hideRecommandation
        />
        {loadingCommentaire.loading ? (
          <Loader />
        ) : loadingCommentaire.error ? (
          <ErrorPage />
        ) : (
          <CommentList
            key={`comment_list`}
            comments={loadingCommentaire.data?.comments || []}
            fetchMoreComments={handleFecthMoreComments}
          />
        )}
        <Comment
          refetch={handleRefecthComments}
          codeItem="SUIVI_OPERATIONNEL"
          idSource={suiviOperationnelChoosen?.id}
        />
      </>
    ) : (
      undefined
    ),
  };

  return laboratoireSuivi;
};
