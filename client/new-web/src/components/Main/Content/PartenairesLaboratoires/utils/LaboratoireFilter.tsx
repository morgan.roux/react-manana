import React, { FC, FormEvent, useState } from 'react';
import {
  FormGroup,
  FormControlLabel,
  FormLabel,
  Checkbox,
  Box,
  Typography,
} from '@material-ui/core';
import CustomDatePicker from './../../../../Common/CustomDateTimePicker/CustomDatePicker';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { CustomButton } from '@app/ui-kit';

interface LaboratoireFilterProps {
  filterTypePartenariat: any[];
  filterStatutPartenariat: any[];
  onRequestFilter: (statut: any,type: any, date: any) => void;
  onCloseFilter: (open: boolean) => void;
}

const dateFilterValue = {
  dateDebut: null,
  dateFin: null,
};

const LaboratoireFilter: FC<LaboratoireFilterProps> = ({
  filterTypePartenariat,
  filterStatutPartenariat,
  onRequestFilter,
  onCloseFilter,
}) => {
  const [statutChecked, setStatutChecked] = useState<any[]>([]);
  const [typeChecked, setTypeChecked] = useState<any[]>([]);
  const [dateFilter, setDateFilter] = useState<any>(dateFilterValue);

  const handleChangeDateDebut = (date: MaterialUiPickersDate) => {
    setDateFilter({ ...dateFilter, dateDebut: date });
  };
  const handleChangeDateFin = (date: MaterialUiPickersDate) => {
    setDateFilter({ ...dateFilter, dateFin: date });
  };

  const handleChangeType = (event: React.ChangeEvent<HTMLInputElement>) => {
      if(!event.target.checked) {
        const type = typeChecked.reduce((p,c) => (c.id !== event.target.name && p.push(c),p),[]);
        setTypeChecked(type);
      } else {
        const type = typeChecked ? [...typeChecked, {keyword: 'idType', id: event.target.name}] : [];
        setTypeChecked(type);
      }
  };

  const handleChangeStatut = (event: React.ChangeEvent<HTMLInputElement>) => {
      if(!event.target.checked) {
        const statut = statutChecked.reduce((p,c) => (c.id !== event.target.name && p.push(c),p),[]);
        setStatutChecked(statut);
      } else {
        const statut = statutChecked ? [...statutChecked, {keyword: 'idStatut', id: event.target.name}] : [];
        setStatutChecked(statut);
      }
  };

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    onRequestFilter(statutChecked, typeChecked, dateFilter);
    onCloseFilter(false);
  };

  return (
    <form onSubmit={handleSubmit}>
      <Typography style={{ fontWeight: 'bold' }}>Type partenariat</Typography>
      <FormGroup>
        {filterTypePartenariat &&
          filterTypePartenariat.map(({ id, libelle }) => (
            <FormControlLabel
              key={id}
              control={
                <Checkbox
                  checked={typeChecked && typeChecked[id]}
                  onChange={handleChangeType}
                  name={id}
                />
              }
              label={libelle}
            />
          ))}
      </FormGroup>
      <Typography style={{ fontWeight: 'bold' }}>Statut partenariat</Typography>
      <FormGroup>
        {filterStatutPartenariat &&
          filterStatutPartenariat.map(({ id, libelle }) => (
            <FormControlLabel
              key={id}
              control={
                <Checkbox
                  checked={statutChecked && statutChecked[id]}
                  onChange={handleChangeStatut}
                  name={id}
                />
              }
              label={libelle}
            />
          ))}
      </FormGroup>
      {/*
      <Box py={1}>
        <Typography style={{ fontWeight: 'bold' }}>Date du rendez-vous:</Typography>
        <Box py={1}>
          <CustomDatePicker
            style={{ width: '100%' }}
            value={dateFilter.dateDebut}
            onChange={handleChangeDateDebut}
            name="dateDebut"
            label="Date de début"
          />
        </Box>
        <Box py={1}>
          <CustomDatePicker
            style={{ width: '100%' }}
            value={dateFilter.dateFin}
            onChange={handleChangeDateFin}
            name="dateFin"
            label="Date de fin"
          />
        </Box>
      </Box>
      */}
      <Box textAlign="right">
        <CustomButton type="submit" color="secondary">
          Filtrer
        </CustomButton>
      </Box>
    </form>
  );
};

export default LaboratoireFilter;
