import { useLazyQuery } from '@apollo/react-hooks';
import { Box, Typography } from '@material-ui/core';
import moment from 'moment';
import React, { useEffect } from 'react';
import {
  GET_REMUNERATION_SUIVI_OPERATIONNEL,
  GET_REMUNERATION_SUIVI_OPERATIONNELLES,
} from '../../../../../../federation/partenaire-service/pilotage/query';
import {
  GET_REMUNERATION_SUIVI_OPERATIONNELVariables,
  GET_REMUNERATION_SUIVI_OPERATIONNEL as GET_REMUNERATION_SUIVI_OPERATIONNEL_TYPE,
} from '../../../../../../federation/partenaire-service/pilotage/types/GET_REMUNERATION_SUIVI_OPERATIONNEL';
import {
  GET_REMUNERATION_SUIVI_OPERATIONNELLESVariables,
  GET_REMUNERATION_SUIVI_OPERATIONNELLES as GET_REMUNERATION_SUIVI_OPERATIONNELLES_TYPES,
} from '../../../../../../federation/partenaire-service/pilotage/types/GET_REMUNERATION_SUIVI_OPERATIONNELLES';
import { FEDERATION_CLIENT } from '../../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { useStyles } from './styles';

export const useSuiviRemuneration = ({
  tab,
  idLabo,
  viewPilotage,
  takeSuiviRemuneration,
  skipSuiviRemuneration,
  filterOrderTable,
}) => {
  const classes = useStyles();
  const options = ({ title, percent, level, totalPaye, totalPrevu, type }) => {
    return {
      title,
      options: {
        percent,
        level,
        colors: ['#F46036', '#FBB104', '#FFE43A', '#C2C63F', '#00A745', '#008000'],
      },
      descriptionComponent: (
        <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
          <Box>
            <Typography className={classes.montant}>{totalPaye} €</Typography>
          </Box>
          <Box display="flex" flexDirection="column" py={2}>
            <Box display="flex" flexDirection="row">
              <Typography className={classes.libelle}>{type} prévu: &nbsp;</Typography>
              <Typography className={classes.montant}>{totalPrevu || 0} €</Typography>
            </Box>
            <Box display="flex" flexDirection="row">
              <Typography className={classes.libelle}>{type} payé: &nbsp;</Typography>
              <Typography className={classes.montant}>{totalPaye} €</Typography>
            </Box>
          </Box>
        </Box>
      ),
    };
  };

  const [
    loadRemunerationSuiviOperationnelle,
    loadingRemunerationSuiviOperationnelle,
  ] = useLazyQuery<
    GET_REMUNERATION_SUIVI_OPERATIONNEL_TYPE,
    GET_REMUNERATION_SUIVI_OPERATIONNELVariables
  >(GET_REMUNERATION_SUIVI_OPERATIONNEL, { client: FEDERATION_CLIENT });

  const [
    loadRemunerationSuiviOperationnelleTable,
    loadingRemunerationSuiviOperationnelleTable,
  ] = useLazyQuery<
    GET_REMUNERATION_SUIVI_OPERATIONNEL_TYPE,
    GET_REMUNERATION_SUIVI_OPERATIONNELVariables
  >(GET_REMUNERATION_SUIVI_OPERATIONNEL, { client: FEDERATION_CLIENT });

  const [
    loadRemunerationSuiviOperationnelleTables,
    loadingRemunerationSuiviOperationnelleTables,
  ] = useLazyQuery<
    GET_REMUNERATION_SUIVI_OPERATIONNELLES_TYPES,
    GET_REMUNERATION_SUIVI_OPERATIONNELLESVariables
  >(GET_REMUNERATION_SUIVI_OPERATIONNELLES, { client: FEDERATION_CLIENT });

  const dataRemunerationSuiviOperationnel =
    loadingRemunerationSuiviOperationnelle.data?.remunerationSuiviOperationnel;

  const dataRemunerationSuiviOperationnelTable =
    loadingRemunerationSuiviOperationnelleTable.data?.remunerationSuiviOperationnel;

  const suiviRemunerationOperationnelTable = [
    {
      nomPartenaire: dataRemunerationSuiviOperationnelTable?.types[0].nomPartenaire || '',
      BRITotal: {
        prevu:
          (dataRemunerationSuiviOperationnelTable?.types || []).find(
            element => element.prestation.code === 'BRI',
          )?.totalMontantPrevuPrestation || 0,
        realise:
          (dataRemunerationSuiviOperationnelTable?.types || []).find(
            element => element.prestation.code === 'BRI',
          )?.totalMontantReglementPrestation || 0,
        pourcentage:
          (dataRemunerationSuiviOperationnelTable?.types || []).find(
            element => element.prestation.code === 'BRI',
          )?.pourcentagePrestation || 0,
      },
      CoopTotal: {
        prevu:
          (dataRemunerationSuiviOperationnelTable?.types || []).find(
            element => element.prestation.code === 'COOP',
          )?.totalMontantPrevuPrestation || 0,
        realise:
          (dataRemunerationSuiviOperationnelTable?.types || []).find(
            element => element.prestation.code === 'COOP',
          )?.totalMontantReglementPrestation || 0,
        pourcentage:
          (dataRemunerationSuiviOperationnelTable?.types || []).find(
            element => element.prestation.code === 'COOP',
          )?.pourcentagePrestation || 0,
      },
      totalPrevu: dataRemunerationSuiviOperationnelTable?.totalMontantPrevues || 0,
      totalRealise: dataRemunerationSuiviOperationnelTable?.totalMontantReglements || 0,
      pourcentageTotal: dataRemunerationSuiviOperationnelTable?.pourcentageReelReglement,
    },
  ];

  const suiviRemunerationOperationnelTables = loadingRemunerationSuiviOperationnelleTables.data?.remunerationsSuiviOperationnelles.map(
    suiviRemuneration => {
      return {
        nomPartenaire: suiviRemuneration.types[0].nomPartenaire,
        BRITotal: {
          prevu:
            (suiviRemuneration?.types || []).find(element => element.prestation.code === 'BRI')
              ?.totalMontantPrevuPrestation || 0,
          realise:
            (suiviRemuneration?.types || []).find(element => element.prestation.code === 'BRI')
              ?.totalMontantReglementPrestation || 0,
          pourcentage:
            (suiviRemuneration?.types || []).find(element => element.prestation.code === 'BRI')
              ?.pourcentagePrestation || 0,
        },
        CoopTotal: {
          prevu:
            (suiviRemuneration?.types || []).find(element => element.prestation.code === 'COOP')
              ?.totalMontantPrevuPrestation || 0,
          realise:
            (suiviRemuneration?.types || []).find(element => element.prestation.code === 'COOP')
              ?.totalMontantReglementPrestation || 0,
          pourcentage:
            (suiviRemuneration?.types || []).find(element => element.prestation.code === 'COOP')
              ?.pourcentagePrestation || 0,
        },
        totalPrevu: suiviRemuneration?.totalMontantPrevues || 0,
        totalRealise: suiviRemuneration?.totalMontantReglements || 0,
        pourcentageTotal: suiviRemuneration?.pourcentageReelReglement,
      };
    },
  );

  useEffect(() => {
    if (tab === 'pilotage' || tab === 'pilotages') {
      loadRemunerationSuiviOperationnelle({
        variables: {
          input: {
            idPartenaireTypeAssocie: tab === 'pilotage' ? idLabo : tab === 'pilotages' ? '' : '',
            partenaireType: 'LABORATOIRE',
            dateDebut: new Date(),
          },
        },
      });
      if (viewPilotage === 'SUIVI_REMUNERATIONS' && tab === 'pilotage') {
        loadRemunerationSuiviOperationnelleTable({
          variables: {
            input: {
              idPartenaireTypeAssocie: tab === 'pilotage' ? idLabo : tab === 'pilotages' ? '' : '',
              partenaireType: 'LABORATOIRE',
              dateDebut: `01-11-${new Date().getFullYear()}`,
              dateFin: new Date(),
            },
          },
        });
      }
      if (viewPilotage === 'SUIVI_REMUNERATIONS' && tab === 'pilotages') {
        loadRemunerationSuiviOperationnelleTables({
          variables: {
            input: {
              partenaireType: 'LABORATOIRE',
              dateDebut: `01-11-${new Date().getFullYear()}`,
              dateFin: moment(new Date()).endOf('month'),
              skip: skipSuiviRemuneration,
              take: takeSuiviRemuneration,
            },
          },
        });
      }
    }
  }, [tab, viewPilotage, skipSuiviRemuneration, takeSuiviRemuneration]);

  const listDataSuiviRemunerations = (dataRemunerationSuiviOperationnel?.types || []).map(
    element => {
      const percent =
        (element.pourcentagePrestation || 0 / 100) / 100 > 100
          ? 100
          : (element.pourcentagePrestation || 0 / 100) / 100;
      return options({
        title: element.prestation.libelle,
        percent,
        level: 6,
        totalPaye: element.totalMontantReglementPrestation,
        totalPrevu: element.totalMontantPrevuPrestation,
        type: element.prestation.libelle,
      });
    },
  );

  const suiviRemunerationWithTotal = {
    nomPartenaire: 'Total',
    BRITotal: {
      prevu:
        parseFloat(
          (suiviRemunerationOperationnelTables || [])
            .map(element => {
              return element.BRITotal.prevu;
            })
            .reduce((accumulator, current) => accumulator + current, 0)
            .toFixed(2),
        ) || (0 as any),
      realise:
        parseFloat(
          (suiviRemunerationOperationnelTables || [])
            .map(element => {
              return element.BRITotal.realise;
            })
            .reduce((accumulator, current) => accumulator + current, 0)
            .toFixed(2),
        ) || (0 as any),
      pourcentage:
        parseFloat(
          (suiviRemunerationOperationnelTables || [])
            .map(element => {
              return element.BRITotal.pourcentage;
            })
            .reduce((accumulator, current) => accumulator + current, 0)
            .toFixed(2),
        ) || (0 as any),
    },
    CoopTotal: {
      prevu:
        parseFloat(
          (suiviRemunerationOperationnelTables || [])
            .map(element => {
              return element.CoopTotal.prevu;
            })
            .reduce((accumulator, current) => accumulator + current, 0)
            .toFixed(2),
        ) || 0,
      realise:
        parseFloat(
          (suiviRemunerationOperationnelTables || [])
            .map(element => {
              return element.CoopTotal.realise;
            })
            .reduce((accumulator, current) => accumulator + current, 0)
            .toFixed(2),
        ) || 0,
      pourcentage:
        parseFloat(
          (suiviRemunerationOperationnelTables || [])
            .map(element => {
              return element.CoopTotal.pourcentage;
            })
            .reduce((accumulator, current) => accumulator + current, 0)
            .toFixed(2),
        ) || 0,
    },
    totalPrevu:
      parseFloat(
        (suiviRemunerationOperationnelTables || [])
          .map(element => {
            return element.totalPrevu;
          })
          .reduce((accumulator, current) => accumulator + current, 0)
          .toFixed(2),
      ) || 0,
    totalRealise:
      parseFloat(
        (suiviRemunerationOperationnelTables || [])
          .map(element => {
            return element.totalRealise;
          })
          .reduce((accumulator, current) => accumulator + current, 0)
          .toFixed(2),
      ) || 0,
    pourcentageTotal:
      parseFloat(
        (suiviRemunerationOperationnelTables || [])
          .map(element => {
            return element.pourcentageTotal;
          })
          .reduce((accumulator, current) => accumulator + current, 0)
          .toFixed(2),
      ) || 0,
  };

  const suiviRemunerationProps = {
    title: 'Suivi des rémunérations',
    total: options({
      title: 'Total',
      percent:
        (dataRemunerationSuiviOperationnel?.pourcentageReelReglement || 0 / 100) / 100 > 100
          ? 100
          : (dataRemunerationSuiviOperationnel?.pourcentageReelReglement || 0 / 100) / 100,
      level: 6,
      totalPaye: dataRemunerationSuiviOperationnel?.totalMontantReglements,
      totalPrevu: dataRemunerationSuiviOperationnel?.totalMontantPrevues,
      type: 'Total',
    }),
    data: listDataSuiviRemunerations,
    suiviRemunerationEntreDate:
      tab === 'pilotage'
        ? {
            data: suiviRemunerationOperationnelTable,
            loading: loadingRemunerationSuiviOperationnelleTable.loading,
            error: loadingRemunerationSuiviOperationnelleTable.error,
            rowsTotal: suiviRemunerationOperationnelTable.length,
          }
        : tab === 'pilotages'
        ? {
            data: suiviRemunerationOperationnelTables?.concat([suiviRemunerationWithTotal]),
            loading: loadingRemunerationSuiviOperationnelleTables.loading,
            error: loadingRemunerationSuiviOperationnelleTables.error,
            rowsTotal: suiviRemunerationOperationnelTables?.length,
          }
        : {
            data: suiviRemunerationOperationnelTables?.concat([suiviRemunerationWithTotal]),
            loading: loadingRemunerationSuiviOperationnelleTables.loading,
            error: loadingRemunerationSuiviOperationnelleTables.error,
            rowsTotal: suiviRemunerationOperationnelTables?.length,
          },
  };

  return { suiviRemunerationProps, loadingRemunerationSuiviOperationnelle };
};
