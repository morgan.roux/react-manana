import React, { ReactNode, useState } from 'react';

import { QueryLazyOptions, useApolloClient, useLazyQuery, useMutation } from '@apollo/react-hooks';
import {
  DocumentFacture,
  FactureStatut,
  TotauxFacture,
} from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/HistoriqueFacture/HistoriqueFacture';
import { useEffect } from 'react';
import { QueryResult } from 'react-apollo';
import { ORIGINES } from '../../../../../federation/basis/origine/query';
import {
  ORIGINES as ORIGINES_TYPE,
  ORIGINESVariables,
} from '../../../../../federation/basis/origine/types/ORIGINES';
import { GET_DOCUMENTS, GET_TOTAUX_FACTURES } from '../../../../../federation/ged/document/query';
import {
  GET_DOCUMENTS as GET_DOCUMENTS_TYPE,
  GET_DOCUMENTSVariables,
} from '../../../../../federation/ged/document/types/GET_DOCUMENTS';
import { UPDATE_JOINDRE_COMMANDE_FACTURE } from '../../../../../federation/partenaire-service/historique-commande/mutation';
import {
  COMMANDES_FOR_DOCUMENT,
  COMMANDES_FOR_DOCUMENTVariables,
} from '../../../../../federation/partenaire-service/historique-commande/types/COMMANDES_FOR_DOCUMENT';
import {
  COMMANDE_FOR_DOCUMENT_TOTAL_ROWS,
  COMMANDE_FOR_DOCUMENT_TOTAL_ROWSVariables,
} from '../../../../../federation/partenaire-service/historique-commande/types/COMMANDE_FOR_DOCUMENT_TOTAL_ROWS';
import {
  JOINDRE_COMMANDE_FACTURE,
  JOINDRE_COMMANDE_FACTUREVariables,
} from '../../../../../federation/partenaire-service/historique-commande/types/JOINDRE_COMMANDE_FACTURE';
import { AppAuthorization } from '../../../../../services/authorization';
import { getPharmacie, getUser } from '../../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import CreateDocument from '../../EspaceDocumentaire/Content/CreateDocument';
import { formatFilename } from '../../../../Common/Dropzone/Dropzone';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import { uploadToS3 } from '../../../../../services/S3';
import {
  CREATE_DOCUMENT_CATEGORIE,
  DELETE_DOCUMENT_CATEGORIE,
  UPDATE_DOCUMENT_CATEGORIE,
} from '../../../../../federation/ged/document/mutation';
import {
  CREATE_DOCUMENT_CATEGORIE as CREATE_DOCUMENT_CATEGORIE_TYPE,
  CREATE_DOCUMENT_CATEGORIEVariables,
} from '../../../../../federation/ged/document/types/CREATE_DOCUMENT_CATEGORIE';
import {
  UPDATE_DOCUMENT_CATEGORIE as UPDATE_DOCUMENT_CATEGORIE_TYPE,
  UPDATE_DOCUMENT_CATEGORIEVariables,
} from '../../../../../federation/ged/document/types/UPDATE_DOCUMENT_CATEGORIE';
import {
  DELETE_DOCUMENT_CATEGORIE as DELETE_DOCUMENT_CATEGORIE_TYPE,
  DELETE_DOCUMENT_CATEGORIEVariables,
} from '../../../../../federation/ged/document/types/DELETE_DOCUMENT_CATEGORIE';
import {
  TOTAUX_FACTURES,
  TOTAUX_FACTURESVariables,
} from '../../../../../federation/ged/document/types/TOTAUX_FACTURES';
import {
  TOTAUX_COMMANDES_FOR_DOCUMENT,
  TOTAUX_COMMANDES_FOR_DOCUMENTVariables,
} from '../../../../../federation/partenaire-service/historique-commande/types/TOTAUX_COMMANDES_FOR_DOCUMENT';
import { CustomModal } from '@app/ui-kit';
import { isMobile } from '../../../../../utils/Helpers';
import { DetailFacture } from './DetailFacture';

const factureStatuts: FactureStatut[] = [
  {
    id: 'APPROUVE',
    code: 'APPROUVE',
    libelle: 'Validée',
  },
  {
    id: 'REFUSE',
    code: 'REFUSE',
    libelle: 'Refusée',
  },
];

interface UseHistoriqueProps {
  idCurrentLabo?: string;
  push?: (url: string) => void;
  currentLabo?: any;
  loadingTotalRowsForDocument?: QueryResult<
    COMMANDE_FOR_DOCUMENT_TOTAL_ROWS,
    COMMANDE_FOR_DOCUMENT_TOTAL_ROWSVariables
  >;
  loadingCommandesForDocument?: QueryResult<
    COMMANDES_FOR_DOCUMENT,
    COMMANDES_FOR_DOCUMENTVariables
  >;
  loadingTotauxCommandesForDocument?: QueryResult<
    TOTAUX_COMMANDES_FOR_DOCUMENT,
    TOTAUX_COMMANDES_FOR_DOCUMENTVariables
  >;
  isSelectable?: boolean;
  origineAssocie?: any;
}

export const useHistoriqueFacture = ({
  idCurrentLabo,
  push,
  currentLabo,
  loadingCommandesForDocument,
  loadingTotalRowsForDocument,
  loadingTotauxCommandesForDocument,
  isSelectable,
  origineAssocie,
}: UseHistoriqueProps): [
  (change: any) => void,
  QueryResult<GET_DOCUMENTS_TYPE, GET_DOCUMENTSVariables>,
  (facture: DocumentFacture) => void,
  (options?: QueryLazyOptions<ORIGINESVariables> | undefined) => void,
  boolean,
  (ids: string[], idDocument?: string) => void,
  () => void,
  boolean,
  ReactNode,
  (factureToEdit?: DocumentFacture) => void,
  (factureToDelete: DocumentFacture) => void,
  TotauxFacture,
  FactureStatut[],
  boolean,
  boolean,
  (newSaved: boolean) => void,
  () => void,
  ReactNode,
] => {
  const pharmacie = getPharmacie();
  const client = useApolloClient();

  const user = getUser();
  const auth = new AppAuthorization(user);

  const [openReplaceDocument, setOpenReplaceDocument] = useState<boolean>(false);
  const [document, setDocument] = useState<DocumentFacture>();
  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const [savingAssociation, setSavingAssociation] = useState<boolean>(false);
  const [savedAssociation, setSavedAssociation] = useState<boolean>(false);
  // const [openConsultDialog, setOpenConsultDialog] = useState<boolean>(false);

  useEffect(() => {
    if (saved) {
      setOpenReplaceDocument(false);
      setDocument(undefined);
      setSaved(false);
    }
  }, [saved]);

  /** */
  const [getOrigines, gettingOrigines] = useLazyQuery<ORIGINES_TYPE, ORIGINESVariables>(ORIGINES, {
    client: FEDERATION_CLIENT,
  });

  const [getDocuments, gettingDocuments] = useLazyQuery<GET_DOCUMENTS_TYPE, GET_DOCUMENTSVariables>(
    GET_DOCUMENTS,
    {
      fetchPolicy: 'cache-and-network',
      client: FEDERATION_CLIENT,
    },
  );

  const [joindreCommande] = useMutation<
    JOINDRE_COMMANDE_FACTURE,
    JOINDRE_COMMANDE_FACTUREVariables
  >(UPDATE_JOINDRE_COMMANDE_FACTURE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: "L'association aux commandes a été effectuée avec succés !",
        type: 'SUCCESS',
        isOpen: true,
      });

      gettingDocuments.refetch();
      loadingCommandesForDocument?.refetch();
      loadingTotalRowsForDocument?.refetch();
      loadingTotauxCommandesForDocument?.refetch();
      setSavingAssociation(false);
      setSavedAssociation(true);
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Erreurs lors de l'association des commandes à la facture",
        type: 'ERROR',
        isOpen: true,
      });

      setSavingAssociation(false);
    },
    client: FEDERATION_CLIENT,
  });

  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
  });

  const [addDocumentCategorie] = useMutation<
    CREATE_DOCUMENT_CATEGORIE_TYPE,
    CREATE_DOCUMENT_CATEGORIEVariables
  >(CREATE_DOCUMENT_CATEGORIE, {
    onCompleted: data => {
      setSaving(false);
      setSaved(true);

      gettingDocuments.refetch();
      displaySnackBar(client, {
        message: 'La Facture a été ajoutée avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs se sont survenues pendant l'ajout de la facture!",
        type: 'ERROR',
        isOpen: true,
      });

      setSaving(false);
    },
    client: FEDERATION_CLIENT,
  });

  const showError = () => {
    displaySnackBar(client, {
      message: `Une erreur est survenue pendant le chargement du document`,
      type: 'ERROR',
      isOpen: true,
    });
  };

  const [updateDocumentCategorie] = useMutation<
    UPDATE_DOCUMENT_CATEGORIE_TYPE,
    UPDATE_DOCUMENT_CATEGORIEVariables
  >(UPDATE_DOCUMENT_CATEGORIE, {
    onCompleted: () => {
      setSaving(false);
      setSaved(true);

      gettingDocuments.refetch();
      displaySnackBar(client, {
        message: 'La Facture a été modifiée avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la modification de la Facture!',
        type: 'ERROR',
        isOpen: true,
      });
      setSaving(false);
    },
    client: FEDERATION_CLIENT,
  });

  const [deleteDocument] = useMutation<
    DELETE_DOCUMENT_CATEGORIE_TYPE,
    DELETE_DOCUMENT_CATEGORIEVariables
  >(DELETE_DOCUMENT_CATEGORIE, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Cette facture a été suprimée avec succès.',
        type: 'SUCCESS',
        isOpen: true,
      });
      gettingDocuments.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la supression de la facture.',
        type: 'ERROR',
        isOpen: true,
      });
    },
  });

  const [loadTotauxFacture, loadingTotauxFacture] = useLazyQuery<
    TOTAUX_FACTURES,
    TOTAUX_FACTURESVariables
  >(GET_TOTAUX_FACTURES, {
    client: FEDERATION_CLIENT,
  });

  const upsertDocument = ({
    id,
    idSousCategorie,
    description,
    nomenclature,
    numeroVersion,
    idUserVerificateur,
    idUserRedacteur,
    dateHeureParution,
    dateHeureDebutValidite,
    dateHeureFinValidite,
    motCle1,
    motCle2,
    motCle3,
    fichier,
    idOrigine,
    idOrigineAssocie,
    dateFacture,
    hT,
    tva,
    ttc,
    type,

    numeroFacture,
    factureDateReglement,
    idReglementMode,
    numeroCommande,
    isGenererCommande,
    avoirType,
    avoirCorrespondants,
  }): void => {
    if (!id) {
      // setMode('creation');
      addDocumentCategorie({
        variables: {
          input: {
            idSousCategorie: idSousCategorie,
            description: description,
            nomenclature: nomenclature,
            numeroVersion: numeroVersion,
            dateHeureParution: new Date(dateHeureParution),
            dateHeureDebutValidite: new Date(dateHeureDebutValidite),
            dateHeureFinValidite: new Date(dateHeureFinValidite),
            idUserVerificateur: idUserVerificateur,
            idUserRedacteur: idUserRedacteur,
            motCle1: motCle1,
            motCle2: motCle2,
            motCle3: motCle3,
            fichier: fichier,
            idOrigine: idOrigine,
            idOrigineAssocie: idOrigineAssocie,
            factureDate: dateFacture,
            factureTotalHt: hT,
            factureTva: tva,
            factureTotalTtc: ttc,
            type: type,
            statut: 'APPROUVE',

            numeroFacture,
            factureDateReglement,
            idReglementMode,
            numeroCommande,
            isGenererCommande,
            avoirType,
            avoirCorrespondants,
          },
        },
      }).then(() => {
        gettingDocuments.refetch();
      });
    } else {
      // setMode('remplacement');
      updateDocumentCategorie({
        variables: {
          input: {
            description: description,
            nomenclature: nomenclature,
            numeroVersion: numeroVersion,
            dateHeureParution: dateHeureParution,
            dateHeureDebutValidite: dateHeureDebutValidite,
            dateHeureFinValidite: dateHeureFinValidite,
            idSousCategorie: idSousCategorie,
            idUserVerificateur: idUserVerificateur,
            idUserRedacteur: idUserRedacteur,
            motCle1: motCle1,
            motCle2: motCle2,
            motCle3: motCle3,
            fichier: fichier,
            idOrigine: idOrigine,
            idOrigineAssocie: idOrigineAssocie,
            factureDate: dateFacture,
            factureTotalHt: hT,
            factureTva: tva,
            factureTotalTtc: ttc,
            type: type,
            statut: 'APPROUVE',

            numeroFacture,
            factureDateReglement,
            idReglementMode,
            numeroCommande,
            isGenererCommande,
            avoirType,
            avoirCorrespondants,
          },
          id: id,
        },
      }).then(() => {
        gettingDocuments.refetch();
      });
    }
  };

  const handleConsult = (facture: DocumentFacture) => {
    setDocument(facture);
    // setOpenConsultDialog(true);
  };

  const handleConsultGed = () => {
    push && push(`/espace-documentaire`);
  };

  const handleJoindreCommande = (ids: string[], idDocument?: string) => {
    setSavedAssociation(false);
    setSavingAssociation(true);
    if (idDocument) {
      joindreCommande({
        variables: {
          input: {
            idDocument,
            idCommandes: ids,
          },
        },
      });
    }
  };

  const handleEditFacture = (factureToEdit?: DocumentFacture) => {
    setOpenReplaceDocument(true);
    setDocument(factureToEdit);
  };

  const handleReplaceDocument = (doc: any): void => {
    setSaving(true);
    setSaved(false);
    if (doc.launchUpload) {
      const filePaths = [doc.selectedFile].map(fichier => formatFilename(fichier, ``));
      doCreatePutPresignedUrl({
        variables: {
          filePaths,
        },
      })
        .then(async response => {
          if (response.data?.createPutPresignedUrls) {
            try {
              const presignedUrl = response.data.createPutPresignedUrls[0];
              let fichier: any = undefined;
              if (presignedUrl) {
                try {
                  const uploadResult = await uploadToS3(
                    doc.selectedFile,
                    presignedUrl.presignedUrl,
                  );
                  if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                    fichier = {
                      chemin: presignedUrl.filePath,
                      nomOriginal: doc.selectedFile.name,
                      type: doc.selectedFile.type,
                    };
                  }
                } catch (error) {
                  showError();
                  return;
                }
              }
              upsertDocument({
                id: doc.id,
                idSousCategorie: doc.idSousCategorie,
                description: doc.description,
                nomenclature: doc.nomenclature,
                numeroVersion: doc.numeroVersion,
                idUserVerificateur: doc.idUserVerificateur,
                idUserRedacteur: doc.idUserRedacteur,
                dateHeureParution: doc.dateHeureParution,
                dateHeureDebutValidite: doc.dateHeureDebutValidite,
                dateHeureFinValidite: doc.dateHeureFinValidite,
                motCle1: doc.motCle1,
                motCle2: doc.motCle2,
                motCle3: doc.motCle3,
                fichier: fichier,
                idOrigine: doc?.origine?.id || doc?.origine,
                idOrigineAssocie: doc.correspondant.id,
                dateFacture: doc.dateFacture,
                hT: doc.hT ? parseFloat(doc.hT) : undefined,
                tva: doc.tva ? parseFloat(doc.tva) : undefined,
                ttc: doc.ttc ? parseFloat(doc.ttc) : undefined,
                type: doc.type,

                numeroFacture: doc?.numeroFacture,
                factureDateReglement: doc?.factureDateReglement,
                idReglementMode: doc?.idReglementMode,
                numeroCommande: doc?.numeroCommande,
                isGenererCommande: doc?.isGenererCommande,
                avoirType: doc?.avoirType,
                avoirCorrespondants: doc?.avoirCorrespondants,
              });
              return;
            } catch (error) {
              showError();
              setSaving(false);
            }
          }
        })
        .catch(() => showError());
    } else {
      const { nomOriginal, chemin, type } = doc.previousFichier || {};

      upsertDocument({
        id: doc.id,
        idSousCategorie: doc.idSousCategorie,
        description: doc.description,
        nomenclature: doc.nomenclature,
        numeroVersion: doc.numeroVersion,
        idUserVerificateur: doc.idUserVerificateur,
        idUserRedacteur: doc.idUserRedacteur,
        dateHeureParution: doc.dateHeureParution,
        dateHeureDebutValidite: doc.dateHeureDebutValidite,
        dateHeureFinValidite: doc.dateHeureFinValidite,
        motCle1: doc.motCle1,
        motCle2: doc.motCle2,
        motCle3: doc.motCle3,
        fichier: doc.previousFichier ? { nomOriginal, chemin, type } : undefined,
        idOrigine: doc.origine?.id,
        idOrigineAssocie: doc.correspondant?.id,
        dateFacture: 'FACTURE' === doc.type ? doc.dateFacture : undefined,
        hT: 'FACTURE' === doc.type && doc.hT ? parseFloat(doc.hT) : undefined,
        tva: 'FACTURE' === doc.type && doc.tva ? parseFloat(doc.tva) : undefined,
        ttc: 'FACTURE' === doc.type && doc.ttc ? parseFloat(doc.ttc) : undefined,
        type: doc.type,

        numeroFacture: doc?.numeroFacture,
        factureDateReglement: doc?.factureDateReglement,
        idReglementMode: doc?.idReglementMode,
        numeroCommande: doc?.numeroCommande,
        isGenererCommande: doc?.isGenererCommande,
        avoirType: doc?.avoirType,
        avoirCorrespondants: doc?.avoirCorrespondants,
      });
    }
  };

  const handleDeleteFacture = (factureToDelete: DocumentFacture) => {
    if (factureToDelete?.id) {
      deleteDocument({
        variables: {
          input: {
            id: factureToDelete.id,
          },
        },
      });
    }
  };

  const handleGoBackHistoriqueFacture = () => {
    push && push('/laboratoires');
  };

  useEffect(() => {
    getOrigines();
  }, []);

  const origine = origineAssocie?.id
    ? origineAssocie
    : (gettingOrigines.data?.origines.nodes || []).find(origine => origine.code === 'LABORATOIRE');
  const searchDocuments = (change: any) => {
    const filterAnd = [
      {
        idOrigine: {
          eq: origine?.id,
        },
      },
      {
        idOrigineAssocie: {
          eq: origineAssocie?.id || idCurrentLabo,
        },
      },
      {
        idPharmacie: {
          eq: pharmacie.id,
        },
      },
      {
        type: {
          eq: 'FACTURE',
        },
      },
    ];

    if (change.searchText) {
      filterAnd.push({
        numeroFacture: {
          iLike: `%${change.searchText}%`,
        },
      } as any);
    }

    if (isSelectable) {
      filterAnd.push({
        factureTotalHt: {
          gte: 0,
        },
      } as any);
      filterAnd.push({
        factureTotalHt: {
          gte: 0,
        },
      } as any);
    }

    if (origine?.id) {
      const sortTableFilter = change.sortTable.column
        ? [
            {
              field: change.sortTable.column,
              direction: change.sortTable.direction,
            },
          ]
        : [];
      getDocuments({
        variables: {
          filter: {
            and: filterAnd,
          },
          paging: {
            offset: change.skip,
            limit: change.take,
          },
          sorting: sortTableFilter,
        },
      });
      loadTotauxFacture({
        variables: {
          idPharmacie: pharmacie.id,
          idOrigine: origine.id,
          idOrigineAssocie: origineAssocie?.id || idCurrentLabo,
          noAvoir: isSelectable || false,
          searchText: change.searchText ? `%${change.searchText}%` : undefined,
        },
      });
    }
  };

  // const ConsultModal = (
  //   <CustomModal
  //     open={openConsultDialog}
  //     setOpen={setOpenConsultDialog}
  //     title="Facture"
  //     closeIcon
  //     withBtnsActions={false}
  //     headerWithBgColor
  //     disableBackdropClick={true}
  //     fullWidth
  //     maxWidth="md"
  //     fullScreen={!!isMobile()}
  //   >
  //     <iframe
  //       id={document?.id}
  //       style={{
  //         height: 600,
  //         border: 'none',
  //       }}
  //       src={`https://docs.google.com/viewerng/viewer?url=${document?.fichier?.publicUrl}&embedded=true`}
  //       frameBorder="0"
  //       width="100%"
  //     />
  //   </CustomModal>
  // );

  const detailComponent = (
    <DetailFacture
      saving={saving}
      saved={saved}
      setSaved={setSaved}
      activeDocument={document}
      onRequestReplace={handleReplaceDocument}
      onRequestDelete={handleDeleteFacture}
    />
  );

  const EditModal = ( openReplaceDocument ? 
    (<CreateDocument
      open={openReplaceDocument}
      setOpen={setOpenReplaceDocument}
      title={document ? 'Modifier facture' : 'Nouvelle facture'}
      saving={saving}
      activeSousCategorie={document?.sousCategorie}
      document={document}
      mode={document ? 'remplacement' : 'creation'}
      onRequestCreateDocument={handleReplaceDocument}
      defaultType="FACTURE"
      idDefaultOrigine={origine?.id}
      defaultCorrespondant={currentLabo}
    />) : undefined
  );

  return [
    searchDocuments,
    gettingDocuments,
    handleConsult,
    getOrigines,
    gettingOrigines.loading,
    handleJoindreCommande,
    handleConsultGed,
    auth.isAuthorizedToAddGedDocument(),
    EditModal,
    handleEditFacture,
    handleDeleteFacture,
    {
      totalHT: loadingTotauxFacture?.data?.getTotauxFactures?.totalHT || 0,
      totalTVA: loadingTotauxFacture?.data?.getTotauxFactures?.totalTVA || 0,
      totalTTC: loadingTotauxFacture?.data?.getTotauxFactures?.totalTTC || 0,
    },
    factureStatuts,
    savingAssociation,
    savedAssociation,
    setSavedAssociation,
    handleGoBackHistoriqueFacture,
    detailComponent,
  ];
};
