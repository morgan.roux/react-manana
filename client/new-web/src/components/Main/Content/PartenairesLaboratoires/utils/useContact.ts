import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { useState, useEffect } from 'react';
import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';

import {
  GET_PRT_CONTACT as GET_PRT_CONTACT_TYPE,
  GET_PRT_CONTACTVariables,
} from '../../../../../federation/partenaire-service/contact/types/GET_PRT_CONTACT';
import {
  UPDATE_PRT_CONTACT as UPDATE_PRT_CONTACT_TYPE,
  UPDATE_PRT_CONTACTVariables,
} from '../../../../../federation/partenaire-service/contact/types/UPDATE_PRT_CONTACT';
import {
  DELETE_PRT_CONTACT as DELETE_PRT_CONTACT_TYPE,
  DELETE_PRT_CONTACTVariables,
} from '../../../../../federation/partenaire-service/contact/types/DELETE_PRT_CONTACT';
import {
  CREATE_PRT_CONTACT as CREATE_PRT_CONTACT_TYPE,
  CREATE_PRT_CONTACTVariables,
} from '../../../../../federation/partenaire-service/contact/types/CREATE_PRT_CONTACT';

import { GET_PRT_CONTACT } from '../../../../../federation/partenaire-service/contact/query';

import {
  CREATE_PRT_CONTACT,
  UPDATE_PRT_CONTACT,
  DELETE_PRT_CONTACT,
} from '../../../../../federation/partenaire-service/contact/mutation';

import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import { uploadToS3 } from '../../../../../services/S3';
import { formatFilename } from '../../../../Common/Dropzone/Dropzone';
import { PRTContactFilter } from '../../../../../types/federation-global-types';
import { getGroupement, getPharmacie } from '../../../../../services/LocalStorage';
import { FichierInput, TypeFichier } from '../../../../../types/graphql-global-types';

const CIVILITE_LIST = [
  {
    id: 'M.',
    code: 'MONSIEUR',
    libelle: 'Monsieur',
  },
  {
    id: 'Mme',
    code: 'Madame',
    libelle: 'Madame',
  },
  {
    id: 'Mlle',
    code: 'MADEMOISELLE',
    libelle: 'Mademoiselle',
  },
];

export const useContact = (idCurrentLabo: string, AvatarComponent: any, push: any) => {
  const groupement = getGroupement();
  const pharmacie = getPharmacie();

  const client = useApolloClient();
  const [skipContact, setSkipContact] = useState(0);
  const [takeContact, setTakeContact] = useState(10);
  const [searchTextContact, setSearchTextContact] = useState('');
  const [photoContact, setPhotoContact] = useState<any>(null);
  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const [sortTable, setSortTable] = useState<any>({
    column: null,
    direction: 'DESC',
  });

  useEffect(() => {
    if (idCurrentLabo) {
      onRequestContactSearch();
    }
  }, [skipContact, takeContact, searchTextContact, sortTable]);

  // QUERY
  const [loadContact, loadingContact] = useLazyQuery<
    GET_PRT_CONTACT_TYPE,
    GET_PRT_CONTACTVariables
  >(GET_PRT_CONTACT, {
    client: FEDERATION_CLIENT,
  });

  /**create */
  const [createContact, creatingContact] = useMutation<
    CREATE_PRT_CONTACT_TYPE,
    CREATE_PRT_CONTACTVariables
  >(CREATE_PRT_CONTACT, {
    onCompleted: () => {
      setPhotoContact(null);
      displaySnackBar(client, {
        message: ' Le contact a été crée avec succés !',
        type: 'SUCCESS',
        isOpen: true,
      });

      loadingContact.refetch();
      setSaving(false);
      setSaved(true);
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Erreurs lors de la création du Contact.',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  /**update plan Marketing */
  const [updateContact, updatingContact] = useMutation<
    UPDATE_PRT_CONTACT_TYPE,
    UPDATE_PRT_CONTACTVariables
  >(UPDATE_PRT_CONTACT, {
    onCompleted: () => {
      setPhotoContact(null);
      displaySnackBar(client, {
        message: 'Le Contact a été modifié avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });

      loadingContact.refetch();
      setSaving(false);
      setSaved(true);
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la modification du contact de laboratoire',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  /**delete Plan Marketing */
  const [deleteContact, deletingContact] = useMutation<
    DELETE_PRT_CONTACT_TYPE,
    DELETE_PRT_CONTACTVariables
  >(DELETE_PRT_CONTACT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le Contact a été supprimé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingContact.refetch();
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression du contact',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  // FICHIERS
  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
  });

  //   civilite: contact.civilite,
  //               nom: contact.nom,
  //               prenom: contact.prenom,
  //               fonction: contact.fonction,
  //               idPartenaireTypeAssocie: contact.idPartenaireTypeAssocie,
  //               partenaireType: contact.partenaireType,
  //               photo: Photo,
  //               contact: contact.contact

  const onRequestContactSearch = () => {
    const keywords = ['idType', 'idStatut'];
    if (idCurrentLabo) {
      const filterAnd: PRTContactFilter[] = [
        {
          partenaireType: {
            eq: 'LABORATOIRE',
          },
        },
        {
          idPartenaireTypeAssocie: {
            eq: idCurrentLabo,
          },
        },
        {
          idPharmacie: {
            eq: pharmacie.id,
          },
        },
        {
          or: [
            {
              nom: {
                iLike: `%${searchTextContact}%`,
              },
            },
            {
              prenom: {
                iLike: `%${searchTextContact}%`,
              },
            },
            {
              fonction: {
                iLike: `%${searchTextContact}%`,
              },
            },
          ],
        },
      ];

      const sortTableFilter = sortTable.column
        ? [
            {
              field: sortTable.column,
              direction: sortTable.direction,
            },
          ]
        : [];

      loadContact({
        variables: {
          filter: {
            and: filterAnd,
          },
          paging: {
            offset: skipContact,
            limit: takeContact,
          },
          sorting: sortTableFilter,
        },
      });
    }
  };

  const saveContact = (contactInput: any, userPhoto: any) => {
    console.log('detection', userPhoto);
    const contact = contactInput.contact;
    setSaving(true);
    setSaved(false);
    if (contactInput.id) {
      updateContact({
        variables: {
          id: contactInput.id,
          input: {
            civilite: contactInput.civilite,
            fonction: contactInput.fonction,
            nom: contactInput.nom,
            prenom: contactInput.prenom,
            contact: {
              adresse1: contact.adresse1,
              mailPerso: contact.adresseEmail,
              mailProf: contact.adresseEmailProf,
              telProf: contact.bureauTelephone,
              cp: contact.cp,
              telPerso: contact.domicileTelephone,
              telMobPerso: contact.mobileTelephone,
              urlFacebookPerso: contact.facebook,
              urlLinkedInPerso: contact.linkedIn,
              urlMessenger: contact.messenger,
              sitePerso: contact.pageWeb,
              ville: contact.ville,
              whatsappMobPerso: contact.whatsApp,
              urlYoutube: contact.youTube,
            },
            photo: userPhoto ? userPhoto[0] : undefined,
            idPartenaireTypeAssocie: idCurrentLabo,
            partenaireType: 'LABORATOIRE',
          },
        },
      });
    } else {
      createContact({
        variables: {
          input: {
            civilite: contactInput.civilite,
            fonction: contactInput.fonction,
            nom: contactInput.nom,
            prenom: contactInput.prenom,
            contact: {
              adresse1: contact.adresse1,
              mailPerso: contact.adresseEmail,
              mailProf: contact.adresseEmailProf,
              telProf: contact.bureauTelephone,
              cp: contact.cp,
              telPerso: contact.domicileTelephone,
              telMobPerso: contact.mobileTelephone,
              urlFacebookPerso: contact.facebook,
              urlLinkedInPerso: contact.linkedIn,
              urlMessenger: contact.messenger,
              sitePerso: contact.pageWeb,
              ville: contact.ville,
              whatsappMobPerso: contact.whatsApp,
              urlYoutube: contact.youTube,
            },
            photo: userPhoto ? userPhoto[0] : undefined,
            idPartenaireTypeAssocie: idCurrentLabo,
            partenaireType: 'LABORATOIRE',
          },
        },
      });
    }
  };

  const handleSaveContact = (contactInput: any) => {
    console.log('Contact to save', contactInput);

    if (photoContact && photoContact.size) {
      const inputFiles = [photoContact];
      // Presigned file
      const filePathsTab: string[] = [];
      filePathsTab.push(
        `users/photos/${groupement && groupement.id}/${formatFilename(photoContact)}`,
      );
      doCreatePutPresignedUrl({ variables: { filePaths: filePathsTab } }).then(async response => {
        if (response.data?.createPutPresignedUrls) {
          try {
            const fichiers = await Promise.all(
              response.data.createPutPresignedUrls.map((item, index) => {
                if (item?.presignedUrl) {
                  return uploadToS3(inputFiles[index], item?.presignedUrl).then(uploadResult => {
                    if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                      return {
                        chemin: item.filePath,
                        nomOriginal: inputFiles[index].name,
                        type: TypeFichier.PHOTO,
                      };
                    }
                  });
                }
              }),
            );
            saveContact(contactInput, fichiers);
          } catch (error) {}
        } else {
        }
      });
    } else {
      let userPhoto: FichierInput | null = null;
      if (photoContact && photoContact.id) {
        userPhoto = {
          chemin: photoContact.chemin,
          nomOriginal: photoContact.nomOriginal,
          type: TypeFichier.AVATAR,
        };
        saveContact(contactInput, [userPhoto]);
      } else {
        saveContact(contactInput, null);
      }
    }
  };

  // Delete
  const handleDeleteContact = (contact: any) => {
    console.log('contact to delete', contact);
    deleteContact({
      variables: { id: contact.id },
    });
  };

  // Recherche
  const handleContactSearch = ({ skip, take, searchText, sortTable }) => {
    setSkipContact(skip);
    setTakeContact(take);
    setSearchTextContact(searchText);
    setSortTable(sortTable);
  };

  const handleGoBack = () => {
    if (idCurrentLabo) {
      push('/laboratoires');
    } else {
      push('/');
    }
  };

  const contactProps = {
    loading: loadingContact.loading,
    error: loadingContact.error as any,
    rowsTotal: 2,
    onRequestSave: handleSaveContact,
    onRequestDelete: handleDeleteContact,
    saving,
    saved,
    setSaved: (newSaved: boolean) => setSaved(newSaved),
    contacts: {
      error: loadingContact.error as any,
      loading: loadingContact.loading,
      data: loadingContact?.data?.pRTContacts.nodes,
    },
    AvatarComponent,
    civilites: CIVILITE_LIST,
    onRequestSearch: handleContactSearch,
    onRequestGoBack: handleGoBack,
  };

  return { onRequestContactSearch, contactProps, photoContact, setPhotoContact };
};
