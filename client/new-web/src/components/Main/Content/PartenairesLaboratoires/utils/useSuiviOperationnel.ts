import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import React, { useEffect, useState } from 'react';
import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';

import {
  GET_LIST_SUIVI_OPERATIONNEL_TYPES as GET_LIST_SUIVI_OPERATIONNEL_TYPES_TYPE,
  GET_LIST_SUIVI_OPERATIONNEL_TYPESVariables,
} from '../../../../../federation/partenaire-service/suivi-operationnel/types/GET_LIST_SUIVI_OPERATIONNEL_TYPES';

import {
  GET_LIST_SUIVI_OPERATIONNEL_STATUT as GET_LIST_SUIVI_OPERATIONNEL_STATUT_TYPE,
  GET_LIST_SUIVI_OPERATIONNEL_STATUTVariables,
} from '../../../../../federation/partenaire-service/suivi-operationnel/types/GET_LIST_SUIVI_OPERATIONNEL_STATUT';

import {
  GET_LIST_SUIVI_OPERATIONNEL_TYPES,
  GET_LIST_SUIVI_OPERATIONNEL_STATUT,
} from '../../../../../federation/partenaire-service/suivi-operationnel/query';

import {
  GET_SUIVI_OPERATIONNELS as GET_SUIVI_OPERATIONNELS_TYPE,
  GET_SUIVI_OPERATIONNELSVariables,
} from '../../../../../federation/partenaire-service/suivi-operationnel/types/GET_SUIVI_OPERATIONNELS';
import { GET_SUIVI_OPERATIONNELS } from '../../../../../federation/partenaire-service/suivi-operationnel/query';

import {
  CREATE_SUIVI_OPERATIONNEL as CREATE_SUIVI_OPERATIONNEL_TYPE,
  CREATE_SUIVI_OPERATIONNELVariables,
} from '../../../../../federation/partenaire-service/suivi-operationnel/types/CREATE_SUIVI_OPERATIONNEL';
import {
  UPDATE_SUIVI_OPERATIONNEL as UPDATE_SUIVI_OPERATIONNEL_TYPE,
  UPDATE_SUIVI_OPERATIONNELVariables,
} from '../../../../../federation/partenaire-service/suivi-operationnel/types/UPDATE_SUIVI_OPERATIONNEL';
import {
  DELETE_ONE_SUIVI_OPERATIONNEL as DELETE_ONE_SUIVI_OPERATIONNEL_TYPE,
  DELETE_ONE_SUIVI_OPERATIONNELVariables,
} from '../../../../../federation/partenaire-service/suivi-operationnel/types/DELETE_ONE_SUIVI_OPERATIONNEL';
import {
  CHANGE_SUIVI_OPERTIONNEL_STATUT as CHANGE_SUIVI_OPERTIONNEL_STATUT_TYPE,
  CHANGE_SUIVI_OPERTIONNEL_STATUTVariables,
} from '../../../../../federation/partenaire-service/suivi-operationnel/types/CHANGE_SUIVI_OPERTIONNEL_STATUT';
import {
  CREATE_SUIVI_OPERATIONNEL,
  UPDATE_SUIVI_OPERATIONNEL,
  DELETE_ONE_SUIVI_OPERATIONNEL,
  CHANGE_SUIVI_OPERTIONNEL_STATUT,
} from '../../../../../federation/partenaire-service/suivi-operationnel/mutation';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';

import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import { uploadToS3 } from '../../../../../services/S3';
import { formatFilename } from '../../../../Common/Dropzone/Dropzone';
import { PRTSuiviOperationnelFilter } from '../../../../../types/federation-global-types';
import { getPharmacie } from '../../../../../services/LocalStorage';

export const useSuiviOperationnel = (idCurrentLabo: string) => {
  const pharmacie = getPharmacie();

  const client = useApolloClient();
  const [skipSuivi, setSkipSuivi] = React.useState<any>(10);
  const [takeSuivi, setTakeSuivi] = React.useState<any>(10);
  const [searchTextSuivi, setSearchTextSuivi] = React.useState<any>('');
  const [filterSuivi, setFilterSuivi] = React.useState<any[]>();
  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);

  // QUERY LISTE DES TYPES
  const suiviOperationnelTypes = useQuery<
    GET_LIST_SUIVI_OPERATIONNEL_TYPES_TYPE,
    GET_LIST_SUIVI_OPERATIONNEL_TYPESVariables
  >(GET_LIST_SUIVI_OPERATIONNEL_TYPES, { client: FEDERATION_CLIENT });

  // QUERY LISTE DES STATUTS
  const suiviOperationnelStatuts = useQuery<
    GET_LIST_SUIVI_OPERATIONNEL_STATUT_TYPE,
    GET_LIST_SUIVI_OPERATIONNEL_STATUTVariables
  >(GET_LIST_SUIVI_OPERATIONNEL_STATUT, { client: FEDERATION_CLIENT });

  // FICHIERS
  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
  });

  // QUERY SUIVI
  const [loadSuiviOperationnel, loadingSuiviOperationnel] = useLazyQuery<
    GET_SUIVI_OPERATIONNELS_TYPE,
    GET_SUIVI_OPERATIONNELSVariables
  >(GET_SUIVI_OPERATIONNELS, {
    client: FEDERATION_CLIENT,
  });

  /**create */
  const [createSuiviOperationnel, creatingSuiviOperationnel] = useMutation<
    CREATE_SUIVI_OPERATIONNEL_TYPE,
    CREATE_SUIVI_OPERATIONNELVariables
  >(CREATE_SUIVI_OPERATIONNEL, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: ' Le Suivi Operationnel a été crée avec succés !',
        type: 'SUCCESS',
        isOpen: true,
      });

      loadingSuiviOperationnel.refetch();

      setSaving(false);
      setSaved(true);
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Erreurs lors de la création du Suivi Operationnel ',
        type: 'ERROR',
        isOpen: true,
      });

      setSaving(false);
    },
    client: FEDERATION_CLIENT,
  });

  /**update Suivi Operationnel */
  const [updateSuiviOperationnel, updatingSuiviOperationnel] = useMutation<
    UPDATE_SUIVI_OPERATIONNEL_TYPE,
    UPDATE_SUIVI_OPERATIONNELVariables
  >(UPDATE_SUIVI_OPERATIONNEL, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le Suivi Operationnel a été modifié avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });

      setSaving(false);
      setSaved(true);
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la modification du Suivi Operationnel',
        type: 'ERROR',
        isOpen: true,
      });

      setSaving(false);
    },
    client: FEDERATION_CLIENT,
  });

  //Delete Suivi Operationnel
  const [deleteSuiviOperationnel, deletingSuiviOperationnel] = useMutation<
    DELETE_ONE_SUIVI_OPERATIONNEL_TYPE,
    DELETE_ONE_SUIVI_OPERATIONNELVariables
  >(DELETE_ONE_SUIVI_OPERATIONNEL, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le Suivi Operationnel a été supprimé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingSuiviOperationnel.refetch();
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression du Suivi Operationnel',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  //Change Suivi Operationnel Statut
  const [changeSuiviStatut, changingSuiviStatut] = useMutation<
    CHANGE_SUIVI_OPERTIONNEL_STATUT_TYPE,
    CHANGE_SUIVI_OPERTIONNEL_STATUTVariables
  >(CHANGE_SUIVI_OPERTIONNEL_STATUT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le statut a été modifié avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingSuiviOperationnel.refetch();
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant le changement du statut',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const saveSuiviOperationnel = (suiviOperationnel: any, fichiers: any) => {
    if (suiviOperationnel.id) {
      updateSuiviOperationnel({
        variables: {
          id: suiviOperationnel.id,
          input: {
            titre: suiviOperationnel.titre,
            description: suiviOperationnel.description,
            dateHeure: suiviOperationnel.dateSuivi,
            partenaireType: 'LABORATOIRE',
            idTypeAssocie: suiviOperationnel.idPartenaireTypeAssocie,
            idType: suiviOperationnel.idTypeSuivi,
            idStatut: suiviOperationnel.idStatut,
            fichiers,
          },
        },
      });
    } else {
      createSuiviOperationnel({
        variables: {
          input: {
            titre: suiviOperationnel.titre,
            description: suiviOperationnel.description,
            dateHeure: suiviOperationnel.dateSuivi,
            partenaireType: 'LABORATOIRE',
            idTypeAssocie: idCurrentLabo,
            idType: suiviOperationnel.idTypeSuivi,
            idStatut: '',
            fichiers,
          },
        },
      });
    }
  };

  // Save suivi Props
  const handleSuiviSave = (suiviOperationnel: any) => {
    console.log('suiviSave:', suiviOperationnel);

    setSaving(true);
    setSaved(false);

    if (suiviOperationnel.selectedFiles && suiviOperationnel.selectedFiles.length > 0) {
      const newFiles = suiviOperationnel.selectedFiles.filter(file => file && file instanceof File);
      const oldFiles = suiviOperationnel.selectedFiles
        .filter(file => file && !(file instanceof File))
        .map(file => {
          if (file) {
            return {
              chemin: file.chemin,
              nomOriginal: file.nomOriginal,
              type: file.type,
            };
          }
        });
      // console.log('Suivi fichier newFiles', newFiles, 'oldFiles', oldFiles );

      if (newFiles?.length > 0) {
        const filePaths = newFiles.map(file => formatFilename(file, ``));
        doCreatePutPresignedUrl({
          variables: {
            filePaths,
          },
        }).then(async response => {
          if (response.data?.createPutPresignedUrls) {
            try {
              const fichiers = await Promise.all(
                response.data.createPutPresignedUrls.map((item, index) => {
                  if (item?.presignedUrl) {
                    return suiviOperationnel.selectedFiles
                      ? uploadToS3(suiviOperationnel.selectedFiles[index], item?.presignedUrl).then(
                        uploadResult => {
                          if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                            return {
                              chemin: item.filePath,
                              nomOriginal: suiviOperationnel.selectedFiles
                                ? suiviOperationnel.selectedFiles[index].name
                                : '',
                              type: suiviOperationnel.selectedFiles
                                ? suiviOperationnel.selectedFiles[index].type
                                : '',
                            };
                          }
                        },
                      )
                      : null;
                  }
                }),
              );
              saveSuiviOperationnel(suiviOperationnel, [...fichiers, ...oldFiles]);
            } catch (error) { }
          } else {
          }
        });
      } else {
        if (oldFiles?.length > 0) {
          saveSuiviOperationnel(suiviOperationnel, oldFiles);
        } else {
          saveSuiviOperationnel(suiviOperationnel, undefined);
        }
      }
    } else {
      saveSuiviOperationnel(suiviOperationnel, undefined);
    }
  };

  const handleFetchMoreSuivis = () => {
    const fetchMore = loadingSuiviOperationnel.fetchMore;
    const variables = loadingSuiviOperationnel.variables;
    const paging = variables.paging;
    if (loadingSuiviOperationnel.data?.pRTSuiviOperationnels) {
      fetchMore({
        variables: {
          ...variables,
          paging: {
            ...paging,
            offset: loadingSuiviOperationnel.data?.pRTSuiviOperationnels,
          },
        },

        updateQuery: (prev: any, { fetchMoreResult }) => {
          if (prev?.pRTSuiviOperationnels && fetchMoreResult?.pRTSuiviOperationnels) {
            return {
              ...prev,
              pRTSuiviOperationnels: [
                ...prev.pRTSuiviOperationnels,
                ...fetchMoreResult.pRTSuiviOperationnels,
              ],
            };
          }

          return prev;
        },
      });
    }
  };

  // Suivi to edit
  // const handleSuiviEdit = (suiviOperationnel: any) => {
  //   setSuiviToEdit(suiviOperationnel);
  // };

  // Delete
  const handleSuiviDelete = (suiviOperationnel: any) => {
    deleteSuiviOperationnel({
      variables: {
        input: { id: suiviOperationnel.id },
      },
    });
  };

  // Recherche
  const handleSuiviSearch = (skip: number, take: number, searchText: string, filter: any[]) => {
    setSkipSuivi(skip);
    setTakeSuivi(take);
    setSearchTextSuivi(searchText);
    setFilterSuivi(filter);
  };

  // Changement de Statut
  const handleSuiviChangeStatut = (id: string, idStatut: string) => {
    changeSuiviStatut({
      variables: {
        id,
        idStatut,
      },
    });
  };

  const onRequestSuiviSearch = () => {
    const keywords = ['idType', 'idStatut'];
    if (idCurrentLabo) {
      const _filters = keywords
        .map(keyword => {
          const tempObject = { or: [] };
          for (let filter of filterSuivi || []) {
            if (filter.keyword === keyword) {
              tempObject.or.push({
                [keyword]: {
                  eq: filter.id,
                },
              } as never);
            }
          }
          return tempObject.or.length > 0 ? tempObject : undefined;
        })
        .filter(e => e);

      const filterAnd: PRTSuiviOperationnelFilter[] = [
        {
          partenaireType: {
            eq: 'LABORATOIRE',
          },
        },
        {
          idTypeAssocie: {
            eq: idCurrentLabo,
          },
        },
        {
          idPharmacie: {
            eq: pharmacie.id,
          },
        },
        {
          or: [
            {
              titre: {
                iLike: `%${searchTextSuivi}%`,
              },
            },
            {
              description: {
                iLike: `%${searchTextSuivi}%`,
              },
            },
          ],
        },
      ];

      if (_filters.length > 0) {
        _filters.map(filter => {
          filterAnd.push(filter as any);
        });
      }

      console.log('filterAnd');

      loadSuiviOperationnel({
        variables: {
          filter: {
            and: filterAnd,
          },
          paging: {
            offset: skipSuivi,
            limit: takeSuivi,
          },
        },
      });
    }
  };

  // console.log('data Suivi', loadingSuiviOperationnel.data?.pRTSuiviOperationnels);

  useEffect(() => {
    if (idCurrentLabo) {
      onRequestSuiviSearch();
    }
  }, [skipSuivi, takeSuivi, searchTextSuivi, filterSuivi]);

  //Liste des Types de Suivi
  const listType = {
    error: suiviOperationnelTypes.error as any,
    loading: suiviOperationnelTypes.loading,
    data: suiviOperationnelTypes.data?.pRTSuiviOperationnelTypes || [],
  };

  // Liste des statuts
  const listStatutSuivi = suiviOperationnelStatuts.data?.pRTSuiviOperationnelStatuts || [];

  const laboratoireSuivi = {
    suivi: {
      error: loadingSuiviOperationnel.error as any,
      loading: loadingSuiviOperationnel.loading,
      data: loadingSuiviOperationnel.data?.pRTSuiviOperationnels || [],
    },
    onRequest: {
      save: handleSuiviSave,
      delete: handleSuiviDelete,
      next: handleSuiviSearch,
      changeStatut: handleSuiviChangeStatut,
      fetchMore: handleFetchMoreSuivis,
    },
    listType,
    listStatutSuivi,
    saving,
    saved,
    setSaved: (newValue: boolean) => setSaved(false),
  };

  return { onRequestSuiviSearch, laboratoireSuivi };
};
