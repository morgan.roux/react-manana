import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import React, { useEffect, useState } from 'react';
import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { GET_RENDEZ_VOUS_SUJETS_VISITES } from '../../../../../federation/partenaire-service/rendez-vous/rendez-vous-sujet-visite/query';
import {
  GET_RENDEZ_VOUS_SUJETS_VISITES as GET_RENDEZ_VOUS_SUJETS_VISITES_TYPE,
  GET_RENDEZ_VOUS_SUJETS_VISITESVariables,
} from '../../../../../federation/partenaire-service/rendez-vous/rendez-vous-sujet-visite/types/GET_RENDEZ_VOUS_SUJETS_VISITES';
import {
  GET_LIST_PARTENARIAT_TYPES as GET_LIST_PARTENARIAT_TYPES_TYPE,
  GET_LIST_PARTENARIAT_TYPESVariables,
} from '../../../../../federation/partenaire-service/partenariat/types/GET_LIST_PARTENARIAT_TYPES';
import {
  GET_LIST_PARTENARIAT_STATUTS as GET_LIST_PARTENARIAT_STATUTS_TYPE,
  GET_LIST_PARTENARIAT_STATUTSVariables,
} from '../../../../../federation/partenaire-service/partenariat/types/GET_LIST_PARTENARIAT_STATUTS';
import {
  GET_LIST_PARTENARIAT_STATUTS,
  GET_LIST_PARTENARIAT_TYPES,
} from '../../../../../federation/partenaire-service/partenariat/query';
import {
  CREATE_PARTENARIAT as CREATE_PARTENARIAT_TYPE,
  CREATE_PARTENARIATVariables,
} from '../../../../../federation/partenaire-service/partenariat/types/CREATE_PARTENARIAT';
import {
  UPDATE_PARTENARIAT as UPDATE_PARTENARIAT_TYPE,
  UPDATE_PARTENARIATVariables,
} from '../../../../../federation/partenaire-service/partenariat/types/UPDATE_PARTENARIAT';
import {
  CREATE_PARTENARIAT,
  UPDATE_PARTENARIAT,
} from '../../../../../federation/partenaire-service/partenariat/mutation';
import { CREATE_RENDEZ_VOUS } from './../../../../../federation/partenaire-service/rendez-vous/mutation';
import {
  CREATE_RENDEZ_VOUS as CREATE_RENDEZ_VOUS_TYPE,
  CREATE_RENDEZ_VOUSVariables,
} from './../../../../../federation/partenaire-service/rendez-vous/types/CREATE_RENDEZ_VOUS';
import {
  GET_RENDEZ_VOUS as GET_RENDEZ_VOUS_TYPE,
  GET_RENDEZ_VOUSVariables,
} from './../../../../../federation/partenaire-service/rendez-vous/types/GET_RENDEZ_VOUS';
import {
  DELETE_RENDEZ_VOUS as DELETE_RENDEZ_VOUS_TYPE,
  DELETE_RENDEZ_VOUSVariables,
} from './../../../../../federation/partenaire-service/rendez-vous/types/DELETE_RENDEZ_VOUS';
import { DELETE_RENDEZ_VOUS } from './../../../../../federation/partenaire-service/rendez-vous/mutation';
import {
  UPDATE_RENDEZ_VOUS as UPDATE_RENDEZ_VOUS_TYPE,
  UPDATE_RENDEZ_VOUSVariables,
} from './../../../../../federation/partenaire-service/rendez-vous/types/UPDATE_RENDEZ_VOUS';
import { UPDATE_RENDEZ_VOUS } from './../../../../../federation/partenaire-service/rendez-vous/mutation';
import { GET_RENDEZ_VOUS } from './../../../../../federation/partenaire-service/rendez-vous/query';
import { Laboratoire } from '@app/ui-kit';
import UserInput from '../../../../Common/UserInput';
import { getPharmacie } from '../../../../../services/LocalStorage';
import { AboutPageProps } from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/About/useAbout';
import { RendezVousFormOutput } from '@app/ui-kit/components/pages/LaboratoirePage/SideNavList/About/RendezVousForm/RendezVousForm';

export const useAbout = ({
  params,
  laboratoire,
  loading,
  error,
  refetch,
}: {
  params?: any;
  laboratoire?: Laboratoire;
  loading?: boolean;
  error?: Error;
  refetch: () => void;
}) => {
  const client = useApolloClient();
  const pharmacie = getPharmacie();
  const [openModalCollaborateur, setOpenModalCollaborateur] = useState<boolean>(false);
  const [openModalInvites, setOpenModalInvites] = useState<boolean>(false);

  const [savingPartenariat, setSavingPartenariat] = useState<boolean>(false);
  const [savedPartenariat, setSavedPartenariat] = useState<boolean>(false);
  const [savingRdv, setSavingRdv] = useState<boolean>(false);
  const [savedRdv, setSavedRdv] = useState<boolean>(false);
  const [typeUpdateRdv, setTypeUpdateRdv] = useState<string>('update');

  const [invites, setInvites] = useState<any>([]);
  const [userRdvParticipants, setUserRdvParticipants] = useState<any>([]);

  const [skipRdv, setSkipRdv] = useState<number>(0);
  const [takeRdv, setTakeRdv] = useState<number>(2);

  useEffect(() => {
    loadPartenariatStatuts();
    loadPartenariatTypes();
    searchRdvPlanifie();
    searchRdvRealise();
  }, []);

  const loadSubject = useQuery<
    GET_RENDEZ_VOUS_SUJETS_VISITES_TYPE,
    GET_RENDEZ_VOUS_SUJETS_VISITESVariables
  >(GET_RENDEZ_VOUS_SUJETS_VISITES, {
    client: FEDERATION_CLIENT,
  });

  useEffect(() => {
    searchRdvPlanifie();
    searchRdvRealise();
  }, [skipRdv, takeRdv, params.idLabo]);

  const [loadPlanifiesRdv, loadingPlanifiesRdv] = useLazyQuery<
    GET_RENDEZ_VOUS_TYPE,
    GET_RENDEZ_VOUSVariables
  >(GET_RENDEZ_VOUS, { client: FEDERATION_CLIENT });

  const [loadRealisesRdv, loadingRealisesRdv] = useLazyQuery<
    GET_RENDEZ_VOUS_TYPE,
    GET_RENDEZ_VOUSVariables
  >(GET_RENDEZ_VOUS, { client: FEDERATION_CLIENT });

  const [loadPartenariatStatuts, loadingPartenariatStatuts] = useLazyQuery<
    GET_LIST_PARTENARIAT_STATUTS_TYPE,
    GET_LIST_PARTENARIAT_STATUTSVariables
  >(GET_LIST_PARTENARIAT_STATUTS, { client: FEDERATION_CLIENT });

  const [loadPartenariatTypes, loadingPartenariatTypes] = useLazyQuery<
    GET_LIST_PARTENARIAT_TYPES_TYPE,
    GET_LIST_PARTENARIAT_TYPESVariables
  >(GET_LIST_PARTENARIAT_TYPES, { client: FEDERATION_CLIENT });

  const [createRdv, creatingRdv] = useMutation<
    CREATE_RENDEZ_VOUS_TYPE,
    CREATE_RENDEZ_VOUSVariables
  >(CREATE_RENDEZ_VOUS, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Rendez-vous planifié.',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingPlanifiesRdv.refetch();
      setSavingRdv(false);
      setSavedRdv(true);
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la planification du rendez-vous',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [deleteRdv, deletingRdv] = useMutation<
    DELETE_RENDEZ_VOUS_TYPE,
    DELETE_RENDEZ_VOUSVariables
  >(DELETE_RENDEZ_VOUS, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le rendez-vous a été supprimé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingPlanifiesRdv.refetch();
      loadingRealisesRdv.refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression du rendez-vous',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [updateRdv, updatingRdv] = useMutation<
    UPDATE_RENDEZ_VOUS_TYPE,
    UPDATE_RENDEZ_VOUSVariables
  >(UPDATE_RENDEZ_VOUS, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le rendez-vous a été modifié avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingPlanifiesRdv.refetch();
      loadingRealisesRdv.refetch();
      if (typeUpdateRdv !== 'cancel') {
        setSavingRdv(false);
        setSavedRdv(true);
      }
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la modification du rendez-vous',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [updatePartenariat, updatingPartenariat] = useMutation<
    UPDATE_PARTENARIAT_TYPE,
    UPDATE_PARTENARIATVariables
  >(UPDATE_PARTENARIAT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le partenariat a été modifié avec succès!',
        type: 'SUCCESS',
        isOpen: true,
      });

      setSavingPartenariat(false);
      setSavedPartenariat(true);
      refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs se sont survenues pendant l'enregistrement de partenariat",
        type: 'ERROR',
        isOpen: true,
      });
      setSavingPartenariat(false);
    },
    client: FEDERATION_CLIENT,
  });

  const [createPartenariat, creatingPartenariat] = useMutation<
    CREATE_PARTENARIAT_TYPE,
    CREATE_PARTENARIATVariables
  >(CREATE_PARTENARIAT, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le partenariat a été modifié avec succès!',
        type: 'SUCCESS',
        isOpen: true,
      });

      setSavingPartenariat(false);
      setSavedPartenariat(true);

      refetch();
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs se sont survenues pendant l'enregistrement de partenariat",
        type: 'ERROR',
        isOpen: true,
      });
      setSavingPartenariat(false);
    },
    client: FEDERATION_CLIENT,
  });

  const handleRequestSaveRdv = (data: RendezVousFormOutput) => {
    setSavingRdv(true);
    setSavedRdv(false);
    if (laboratoire?.id) {
      if (!data.id) {
        createRdv({
          variables: {
            input: {
              ...data,
              partenaireType: 'LABORATOIRE',
              idPartenaireTypeAssocie: laboratoire.id,
            },
          },
        }).then(() => {
          setUserRdvParticipants([]);
          setInvites([]);
        });
      } else {
        setTypeUpdateRdv('update');
        const id = data.id;
        delete data['id'];
        updateRdv({
          variables: {
            id,
            input: {
              ...data,
              partenaireType: 'LABORATOIRE',
              idPartenaireTypeAssocie: laboratoire.id,
            },
          },
        }).then(() => {
          setUserRdvParticipants([]);
          setInvites([]);
        });
      }
    }
  };

  const handleRequestParticipantInviteEdit = (participants, invites) => {
    setUserRdvParticipants(participants);
    setInvites(invites);
  };

  const handleRequestDeleteRdv = (data: any) => {
    if (laboratoire?.id) {
      deleteRdv({
        variables: {
          id: data.id,
        },
      });
    }
  };

  const handleRequestCancelRdv = (data: any) => {
    if (laboratoire?.id) {
      setTypeUpdateRdv('cancel');
      updateRdv({
        variables: {
          id: data.id,
          input: {
            statut: 'ANNULE',
            partenaireType: 'LABORATOIRE',
            idPartenaireTypeAssocie: laboratoire.id,
            idInvites: data.invites.map(({ id }) => id),
            idUserParticipants: data.participants.map(({ id }) => id),
            idSubject: data.idSubject,
            dateRendezVous: data.dateRendezVous,
            heureDebut: data.heureDebut,
            heureFin: data.heureFin,
          },
        },
      });
    }
  };

  const handleRequestSavePartenariat = (data: any) => {
    if (laboratoire?.id) {
      setSavingPartenariat(true);
      setSavedPartenariat(false);

      if (data.id) {
        updatePartenariat({
          variables: {
            id: data.id,
            update: {
              idPartenaireTypeAssocie: laboratoire.id,
              partenaireType: 'LABORATOIRE',
              ...data,
              dateFin: data.dateFin ? data.dateFin : undefined,
              id: undefined,
              idLabo: undefined,
            },
          },
        });
      } else {
        createPartenariat({
          variables: {
            input: {
              idPartenaireTypeAssocie: laboratoire.id,
              partenaireType: 'LABORATOIRE',
              ...data,
              dateFin: data.dateFin ? data.dateFin : undefined,
              idLabo: undefined,
            },
          },
        });
      }
    }
  };

  const handleRdvSearch = ({ skip, take }) => {
    setSkipRdv(skip);
    setTakeRdv(take);
  };

  const searchRdvPlanifie = () => {
    loadPlanifiesRdv({
      variables: {
        filter: {
          and: [
            {
              idPharmacie: {
                eq: pharmacie.id,
              },
            },
            {
              idPartenaireTypeAssocie: {
                eq: params.idLabo,
              },
            },
            {
              statut: {
                in: ['PLANIFIE', 'REPORTE'],
              },
            },
          ],
        },
        paging: {
          offset: skipRdv,
          limit: takeRdv,
        },
      },
    });
  };

  const searchRdvRealise = () => {
    loadRealisesRdv({
      variables: {
        filter: {
          and: [
            {
              idPharmacie: {
                eq: pharmacie.id,
              },
            },
            {
              idPartenaireTypeAssocie: {
                eq: params.idLabo,
              },
            },
            {
              statut: {
                in: ['REALISE', 'ANNULE'],
              },
            },
          ],
        },
        paging: {
          offset: skipRdv,
          limit: takeRdv,
        },
      },
    });
  };

  const aboutPageProps: AboutPageProps = {
    laboratoire,
    loading: loading || loadingPartenariatStatuts.loading || loadingPartenariatTypes.loading,
    error: error || loadingPartenariatStatuts.error || loadingPartenariatTypes.error,
    typesPartenariats: loadingPartenariatTypes.data?.pRTPartenariatTypes.nodes,
    statutsPartenariats: loadingPartenariatStatuts.data?.pRTPartenariatStatuts.nodes,
    selectedInvites: invites,
    selectedParticipants: userRdvParticipants,
    onRequestParticipantInviteEdit: handleRequestParticipantInviteEdit,
    inviteComponent: (
      <UserInput
        openModal={openModalInvites}
        idPartenaireTypeAssocie={laboratoire?.id}
        withAssignTeam={false}
        withNotAssigned={false}
        partenaireType="LABORATOIRE"
        category="CONTACT"
        setOpenModal={setOpenModalInvites}
        selected={invites}
        setSelected={setInvites}
        label="Représentant labo"
        title="Invités"
      />
    ),
    collaborateurComponent: (
      <UserInput
        openModal={openModalCollaborateur}
        withNotAssigned={false}
        setOpenModal={setOpenModalCollaborateur}
        selected={userRdvParticipants}
        setSelected={setUserRdvParticipants}
        label="Collaborateurs"
      />
    ),
    onRequestSavePartenariat: {
      save: handleRequestSavePartenariat,
      saving: savingPartenariat,
      saved: savedPartenariat,
      setSaved: (newSaved: boolean) => setSavedPartenariat(false),
    },
    onRequestSaveRendezVous: {
      save: handleRequestSaveRdv,
      saving: savingRdv,
      saved: savedRdv,
      setSaved: (newSaved: boolean) => setSavedRdv(false),
    },

    onRequestDeleteRendezVous: {
      delete: handleRequestDeleteRdv,
    },

    onRequestCancelRendezVous: {
      cancel: handleRequestCancelRdv,
    },
    /*
    onRequestReportRendezVous: {
      report: handleRequestReportRdv,
    },
    */
    onRequestSearchRendezVous: handleRdvSearch as any,
    rendezVousPlanifies: {
      loading: loadingPlanifiesRdv.loading,
      error: loadingPlanifiesRdv.error,
      data: loadingPlanifiesRdv.data?.pRTRendezVous.nodes || [],
      rowsTotal: (loadingPlanifiesRdv.data?.pRTRendezVous.nodes || []).length, // TODO : use aggregate
    },
    rendezVousRealises: {
      loading: loadingRealisesRdv.loading,
      error: loadingRealisesRdv.error,
      data: loadingRealisesRdv.data?.pRTRendezVous.nodes || [],
      rowsTotal: (loadingRealisesRdv.data?.pRTRendezVous.nodes || []).length, // TODO : use aggregate
    },
    subjects: loadSubject.data?.pRTRendezVousSujetVisites.nodes || [],
  };

  return { aboutPageProps };
};
