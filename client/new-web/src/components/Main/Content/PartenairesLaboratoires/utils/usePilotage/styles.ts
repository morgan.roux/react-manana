import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    libelle: {
        color: 'rgba(0,0,0,0.6)',
    },
    montant: {
        fontWeight: 'bold',
    },
  }),
);
