import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { useEffect, useState } from 'react';
import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';

import {
  GET_PLAN_MARKETINGS as GET_PLAN_MARKETINGS_TYPE,
  GET_PLAN_MARKETINGSVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/GET_PLAN_MARKETINGS';
import {
  GET_LIST_PLAN_MARKETING_TYPES as GET_LIST_PLAN_MARKETING_TYPES_TYPE,
  GET_LIST_PLAN_MARKETING_TYPESVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/GET_LIST_PLAN_MARKETING_TYPES';
import {
  GET_LIST_PLAN_MARKETING_STATUT as GET_LIST_PLAN_MARKETING_STATUT_TYPE,
  GET_LIST_PLAN_MARKETING_STATUTVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/GET_LIST_PLAN_MARKETING_STATUT';
import {
  CHANGE_PLAN_MARKETING_STATUS as CHANGE_PLAN_MARKETING_STATUS_TYPE,
  CHANGE_PLAN_MARKETING_STATUSVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/CHANGE_PLAN_MARKETING_STATUS';
import {
  UPDATE_PLAN_MARKETING as UPDATE_PLAN_MARKETING_TYPE,
  UPDATE_PLAN_MARKETINGVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/UPDATE_PLAN_MARKETING';
import {
  DELETE_ONE_PLAN_MARKETING as DELETE_ONE_PLAN_MARKETING_TYPE,
  DELETE_ONE_PLAN_MARKETINGVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/DELETE_ONE_PLAN_MARKETING';
import {
  CREATE_PLAN_MARKETING as CREATE_PLAN_MARKETING_TYPE,
  CREATE_PLAN_MARKETINGVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/CREATE_PLAN_MARKETING';

import {
  GET_LIST_PLAN_MARKETING_TYPES,
  GET_PLAN_MARKETINGS,
  GET_LIST_PLAN_MARKETING_STATUT,
  GET_LIST_MISE_AVANT,
} from '../../../../../federation/partenaire-service/planMarketing/query';

import {
  DELETE_ONE_PLAN_MARKETING,
  CHANGE_PLAN_MARKETING_STATUS,
  CREATE_PLAN_MARKETING,
  UPDATE_PLAN_MARKETING,
} from '../../../../../federation/partenaire-service/planMarketing/mutation';

import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import { uploadToS3 } from '../../../../../services/S3';
import { formatFilename } from '../../../../Common/Dropzone/Dropzone';
import { PRTPlanMarketingFilter } from '../../../../../types/federation-global-types';
import { getPharmacie } from '../../../../../services/LocalStorage';
import {
  LIST_MISE_AVANT,
  LIST_MISE_AVANTVariables,
} from '../../../../../federation/partenaire-service/planMarketing/types/LIST_MISE_AVANT';
import { GET_SEARCH_PLAN_MARKETING_PRODUIT_CANALS } from '../../../../../graphql/ProduitCanal/query';
import {SEARCH_PLAN_MARKETING_PRODUIT_CANALS as SEARCH_PLAN_MARKETING_PRODUIT_CANALS_TYPE, SEARCH_PLAN_MARKETING_PRODUIT_CANALSVariables } from '../../../../../graphql/ProduitCanal/types/SEARCH_PLAN_MARKETING_PRODUIT_CANALS';
import { TableChange } from '@app/ui-kit/components/atoms';

export const usePlanMarketing = (idCurrentLabo: string, push: any) => {
  const pharmacie = getPharmacie();

  const client = useApolloClient();
  const [skipPlan, setSkipPlan] = useState(0);
  const [takePlan, setTakePlan] = useState(10);
  const [searchTextPlan, setSearchTextPlan] = useState('');
  const [filterPlan, setFilterPlan] = useState<any[]>();
  const [planToEdit, setPlanToEdit] = useState<any>(undefined);
  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const [sortTable, setSortTable] = useState<any>({
    column: null,
    direction: 'DESC',
  });

  // console.log('data Plan', loadingPlanMarketing.data?.pRTPlanMarketings);

  useEffect(() => {
    if (idCurrentLabo) {
      onRequestPlanSearch();
    }
  }, [skipPlan, takePlan, searchTextPlan, filterPlan, sortTable]);

  // Query PlanMarketing
  const planMarketingTypes = useQuery<
    GET_LIST_PLAN_MARKETING_TYPES_TYPE,
    GET_LIST_PLAN_MARKETING_TYPESVariables
  >(GET_LIST_PLAN_MARKETING_TYPES, { client: FEDERATION_CLIENT });

  const loadMiseAvant = useQuery<LIST_MISE_AVANT, LIST_MISE_AVANTVariables>(GET_LIST_MISE_AVANT, {
    client: FEDERATION_CLIENT,
  });

  // QUERY LISTE DES STATUTS
  const planMarketingStatut = useQuery<
    GET_LIST_PLAN_MARKETING_STATUT_TYPE,
    GET_LIST_PLAN_MARKETING_STATUTVariables
  >(GET_LIST_PLAN_MARKETING_STATUT, { client: FEDERATION_CLIENT });

  // FICHIERS
  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
  });

  // QUERY
  const [loadPlanMarketing, loadingPlanMarketing] = useLazyQuery<
    GET_PLAN_MARKETINGS_TYPE,
    GET_PLAN_MARKETINGSVariables
  >(GET_PLAN_MARKETINGS, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
  });

  const [loadProduits, loadingProduits] = useLazyQuery<
  SEARCH_PLAN_MARKETING_PRODUIT_CANALS_TYPE,
    SEARCH_PLAN_MARKETING_PRODUIT_CANALSVariables
  >(GET_SEARCH_PLAN_MARKETING_PRODUIT_CANALS);

  const [loadPlanMarketingProduits, loadingPlanMarketingProduits] = useLazyQuery<
  SEARCH_PLAN_MARKETING_PRODUIT_CANALS_TYPE,
    SEARCH_PLAN_MARKETING_PRODUIT_CANALSVariables
  >(GET_SEARCH_PLAN_MARKETING_PRODUIT_CANALS); 

  /**create */
  const [createPlanMarketing, creatingPlanMarketing] = useMutation<
    CREATE_PLAN_MARKETING_TYPE,
    CREATE_PLAN_MARKETINGVariables
  >(CREATE_PLAN_MARKETING, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: ' Le plan Marketing a été crée avec succés !',
        type: 'SUCCESS',
        isOpen: true,
      });

      loadingPlanMarketing.refetch();
      setSaving(false);
      setSaved(true);
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Erreurs lors de la création du plan Marketing ',
        type: 'ERROR',
        isOpen: true,
      });

      setSaving(false);
    },
    client: FEDERATION_CLIENT,
  });

  /**update plan Marketing */
  const [updatePlanMarketing, updatingPlanMarketing] = useMutation<
    UPDATE_PLAN_MARKETING_TYPE,
    UPDATE_PLAN_MARKETINGVariables
  >(UPDATE_PLAN_MARKETING, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le plan Marketing a été modifié avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });

      setSaving(false);
      setSaved(true);
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la modification du plan Marketing',
        type: 'ERROR',
        isOpen: true,
      });

      setSaving(false);
    },
    client: FEDERATION_CLIENT,
  });

  /**delete Plan Marketing */
  const [deletePlanMarketing, deletingPlanMarketing] = useMutation<
    DELETE_ONE_PLAN_MARKETING_TYPE,
    DELETE_ONE_PLAN_MARKETINGVariables
  >(DELETE_ONE_PLAN_MARKETING, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le plan Marketing a été supprimé avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingPlanMarketing.refetch();
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la suppression du plan Marketing',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  //Change Statut
  const [changePlanStatut, changingPlanStatut] = useMutation<
    CHANGE_PLAN_MARKETING_STATUS_TYPE,
    CHANGE_PLAN_MARKETING_STATUSVariables
  >(CHANGE_PLAN_MARKETING_STATUS, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'Le statut a été modifié avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });
      loadingPlanMarketing.refetch();
    },

    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant le changement du statut',
        type: 'ERROR',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const handleFetchMorePlans = () => {
    const fetchMore = loadingPlanMarketing.fetchMore;
    const variables = loadingPlanMarketing.variables;
    const paging = variables.paging;
    if (loadingPlanMarketing.data?.pRTPlanMarketings.nodes) {
      fetchMore({
        variables: {
          ...variables,
          paging: {
            ...paging,
            offset: loadingPlanMarketing.data?.pRTPlanMarketings.nodes,
          },
        },

        updateQuery: (prev: any, { fetchMoreResult }) => {
          if (prev?.pRTPlanMarketings && fetchMoreResult?.pRTPlanMarketings.nodes) {
            return {
              ...prev,
              pRTPlanMarketings: {
                ...prev.pRTPlanMarketings,
                nodes: [
                  ...prev.pRTPlanMarketings.nodes,
                  ...fetchMoreResult.pRTPlanMarketings.nodes,
                ],
              },
            };
          }

          return prev;
        },
      });
    }
  };

  const handleRequestSearchProduits = (change?: TableChange | null, ids?: string[]) => {
    let must: any = [
      {
        term: {
          'commandeCanal.code': 'PFL',
        },
      },
      {
        term: {
          'produit.produitTechReg.laboExploitant.id': idCurrentLabo,
        },
      },
    ];

    if (ids && ids.length) {
      must.push({
        terms: {
          _id: ids,
        },
      });
    }

    if (change) {
      const searchText = change.searchText.replace(/\s+/g, '\\ ').replace('/', '\\/');
      must.push({
        query_string: {
          query:
            searchText.startsWith('*') || searchText.endsWith('*') ? searchText : `*${searchText}*`,
          fields: [],
          analyze_wildcard: true,
        },
      });
    }

    const variables: SEARCH_PLAN_MARKETING_PRODUIT_CANALSVariables = {
      type: ['produitCanal'],
      query: {
        query: {
          bool: {
            must,
          },
        },
      },
      take: change?.take,
      skip: change?.skip,
    };

    if (ids && ids.length) {
      loadPlanMarketingProduits({ variables });
    }
    if (change) {
      loadProduits({ variables });
    }
  };

  const saveplanMarketing = (planMarketing: any, fichiers: any) => {
    if (planMarketing.id) {
      updatePlanMarketing({
        variables: {
          id: planMarketing.id,
          input: {
            titre: planMarketing.titre,
            description: planMarketing.description,
            dateDebut: planMarketing.dateDebut,
            dateFin: planMarketing.dateFin,
            partenaireType: 'LABORATOIRE',
            idPartenaireTypeAssocie: idCurrentLabo,
            idType: planMarketing.idTypePlan,
            idStatut: '',
            fichiers,
            idMiseAvants: planMarketing?.idMiseAvants || null,
            idProduits: planMarketing?.idProduits || null,
          } as any,
        },
      });
    } else {
      createPlanMarketing({
        variables: {
          input: {
            titre: planMarketing.titre,
            description: planMarketing.description,
            dateDebut: planMarketing.dateDebut,
            dateFin: planMarketing.dateFin,
            partenaireType: 'LABORATOIRE',
            idPartenaireTypeAssocie: idCurrentLabo,
            idType: planMarketing.idTypePlan,
            fichiers,
            idStatut: '',
            idMiseAvants: planMarketing?.idMiseAvants,
            idProduits: planMarketing?.idProduits || null,
          } as any,
        },
      });
    }
  };


  // Plan marketing Props
  const handlePlanSave = (planMarketing: any) => {
    setSaving(false);
    setSaved(true);

    if (planMarketing?.selectedFiles?.length > 0) {
      console.log('detection');
      const newFiles = planMarketing.selectedFiles.filter(file => file && file instanceof File);
      const oldFiles = planMarketing.selectedFiles
        .filter(file => file && !(file instanceof File))
        .map(file => {
          if (file) {
            return {
              chemin: file.chemin,
              nomOriginal: file.nomOriginal,
              type: file.type,
            };
          }
        });
      console.log('Plan fichier newFiles', newFiles, 'oldFiles', oldFiles);

      if (newFiles?.length > 0) {
        const filePaths = newFiles.map(file => formatFilename(file, ``));
        doCreatePutPresignedUrl({
          variables: {
            filePaths,
          },
        }).then(async response => {
          if (response.data?.createPutPresignedUrls) {
            try {
              const fichiers = await Promise.all(
                response.data.createPutPresignedUrls.map((item, index) => {
                  if (item?.presignedUrl) {
                    return planMarketing.selectedFiles
                      ? uploadToS3(planMarketing.selectedFiles[index], item?.presignedUrl).then(
                        uploadResult => {
                          if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                            return {
                              chemin: item.filePath,
                              nomOriginal: planMarketing.selectedFiles
                                ? planMarketing.selectedFiles[index].name
                                : '',
                              type: planMarketing.selectedFiles
                                ? planMarketing.selectedFiles[index].type
                                : '',
                            };
                          }
                        },
                      )
                      : null;
                  }
                }),
              );
              saveplanMarketing(planMarketing, [...fichiers, ...oldFiles]);
            } catch (error) { }
          } else {
          }
        });
      } else {
        if (oldFiles?.length > 0) {
          saveplanMarketing(planMarketing, oldFiles);
        } else {
          saveplanMarketing(planMarketing, undefined);
        }
      }
    } else {
      saveplanMarketing(planMarketing, undefined);
    }
  };

  // Plan to edit
  const handlePlanEdit = (planMarketing: any) => {
    setPlanToEdit(planMarketing);
  };

  // Delete
  const handlePlanDelete = (planMarketing: any) => {
    deletePlanMarketing({
      variables: {
        input: { id: planMarketing.id },
      },
    });
  };

  // Recherche
  const handlePlanSearch = ({ skip, take, searchText, filter, sortTable }: any) => {
    setSkipPlan(skip);
    setTakePlan(take);
    setSearchTextPlan(searchText);
    setFilterPlan(filter);
    setSortTable(sortTable);
  };

  // Changement de Statut
  const handlePlanChangeStatut = (id: string, idStatut: string) => {
    changePlanStatut({
      variables: {
        id,
        idStatut,
      },
    });
  };

  const handleGoBack = () => {
    push('/laboratoires');
  };

  // Plan Search
  const onRequestPlanSearch = () => {
    const keywords = ['idType', 'idStatut'];
    if (idCurrentLabo) {
      const _filters = keywords
        .map(keyword => {
          const tempObject = { or: [] };
          for (let filter of filterPlan || []) {
            if (filter.keyword === keyword) {
              tempObject.or.push({
                [keyword]: {
                  eq: filter.id,
                },
              } as never);
            }
          }
          return tempObject.or.length > 0 ? tempObject : undefined;
        })
        .filter(e => e);

      const filterAnd: PRTPlanMarketingFilter[] = [
        {
          partenaireType: {
            eq: 'LABORATOIRE',
          },
        },
        {
          idPartenaireTypeAssocie: {
            eq: idCurrentLabo,
          },
        },
        {
          idPharmacie: {
            eq: pharmacie.id,
          },
        },
        {
          or: [
            {
              titre: {
                iLike: `%${searchTextPlan}%`,
              },
            },
            {
              description: {
                iLike: `%${searchTextPlan}%`,
              },
            },
          ],
        },
      ];

      if (_filters.length > 0) {
        _filters.map(filter => {
          filterAnd.push(filter as any);
        });
      }

      const sortTableFilter = sortTable.column
        ? [
            {
              field: sortTable.column === 'periode' ? 'dateDebut' : sortTable.column,
              direction: sortTable.direction,
            },
          ]
        : [];

      loadPlanMarketing({
        variables: {
          filter: {
            and: filterAnd,
          },
          paging: {
            offset: skipPlan,
            limit: takePlan,
          },
          sorting: sortTableFilter,
        },
      });
    }
  };

  //Liste des Types
  const miseAvants = {
    error: loadMiseAvant.error as any,
    loading: loadMiseAvant.loading,
    data: loadMiseAvant.data?.pRTMiseAvants.nodes || ([] as any),
  };

  const listProduits: any = [
    {
      id: '1',
      produit: {
        id: '1',
        produitCode: {
          id: '1',
          code: '123456789',
        },
        libelle: 'algamaris 12° 76g ',
        famille: {
          id: '1',
          libelleFamille: 'Solaire',
        },
        produitPhoto: {
          id: '1',
          fichier: {
            id: '1',
            publicUrl: 'https://fakeimg.pl/250x100/',
          },
        },
      },
    },
    {
      id: '2',
      produit: {
        id: '1',
        produitCode: {
          id: '1',
          code: '987654321',
        },
        libelle: 'xyr - viy 12° 76g ',
        famille: {
          id: '1',
          libelleFamille: 'Covid 19',
        },
        produitPhoto: {
          id: '1',
          fichier: {
            id: '1',
            publicUrl: 'https://fakeimg.pl/100x100/',
          },
        },
      },
    },
  ];

  const produits = {
    error: loadingProduits.error,
    loading: loadingProduits.loading,
    data: loadingProduits.data?.search?.data,
    rowsTotal: loadingProduits.data?.search?.total || 0,
    onRequestSearch: handleRequestSearchProduits,
    planMarketingProduits: loadingPlanMarketingProduits.data?.search?.data || [],
  };

  console.log('PRODUIT', loadingProduits.data)

  const listTypePlan = {
    error: planMarketingTypes.error as any,
    loading: planMarketingTypes.loading,
    data: planMarketingTypes.data?.pRTPlanMarketingTypes.nodes || [],
  };

  // Liste des statuts
  const listStatutPlan = planMarketingStatut.data?.pRTPlanMarketingStatuts.nodes || [];

  const laboratoirePlan = {
    plan: {
      error: loadingPlanMarketing.error as any,
      loading: loadingPlanMarketing.loading,
      data: loadingPlanMarketing.data?.pRTPlanMarketings.nodes || [],
    },
    onRequest: {
      save: handlePlanSave,
      edit: handlePlanEdit,
      delete: handlePlanDelete,
      next: handlePlanSearch,
      changeStatut: handlePlanChangeStatut,
      fetchMore: handleFetchMorePlans,
    },
    listType: listTypePlan,
    listStatutPlan,
    planToEdit: planToEdit,
    saving: saving,
    saved: saved,
    setSaved: setSaved,
    miseAvants,
    produits,
    onRequestGoBack: handleGoBack,
  };

  return { onRequestPlanSearch, laboratoirePlan };
};
