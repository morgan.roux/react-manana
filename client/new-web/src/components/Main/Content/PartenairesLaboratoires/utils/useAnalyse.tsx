import { FEDERATION_CLIENT } from '../../../../Dashboard/DemarcheQualite/apolloClientFederation';
import { useEffect, useState } from 'react';
import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/client';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import {
    CREATE_ANALYSE,
    DELETE_ONE_ANALYSE,
    UPDATE_ANALYSE,
} from '../../../../../federation/partenaire-service/analyse/mutation';
import { GET_LIST_ANALYSE_TYPES } from '../../../../../federation/partenaire-service/analyse/query';
import {
    CREATE_ANALYSE as CREATE_ANALYSE_TYPE,
    CREATE_ANALYSEVariables,
} from '../../../../../federation/partenaire-service/analyse/types/CREATE_ANALYSE';
import {
    DELETE_ONE_ANALYSE as DELETE_ONE_ANALYSE_TYPE,
    DELETE_ONE_ANALYSEVariables,
} from '../../../../../federation/partenaire-service/analyse/types/DELETE_ONE_ANALYSE';
import {
    GET_ANALYSES,
    GET_ANALYSE_AGGREGATES,
} from './../../../../../federation/partenaire-service/analyse/query';
import {
    GET_ANALYSES as GET_ANALYSES_TYPE,
    GET_ANALYSESVariables,
} from './../../../../../federation/partenaire-service/analyse/types/GET_ANALYSES';
import {
    GET_ANALYSE_AGGREGATES as GET_ANALYSES_AGGREGATES_TYPES,
    GET_ANALYSE_AGGREGATESVariables,
} from './../../../../../federation/partenaire-service/analyse/types/GET_ANALYSE_AGGREGATES';
import {
    GET_LIST_ANALYSE_TYPES as GET_LIST_ANALYSE_TYPES_TYPE,
    GET_LIST_ANALYSE_TYPESVariables,
} from './../../../../../federation/partenaire-service/analyse/types/GET_LIST_ANALYSE_TYPES';
import {
    UPDATE_ANALYSE as UPDATE_ANALYSE_TYPE,
    UPDATE_ANALYSEVariables,
} from './../../../../../federation/partenaire-service/analyse/types/UPDATE_ANALYSE';

import {
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import { uploadToS3 } from '../../../../../services/S3';
import { formatFilename } from '../../../../Common/Dropzone/Dropzone';
import { PRTAnalyseFilter } from '../../../../../types/federation-global-types';
import { getPharmacie } from '../../../../../services/LocalStorage';

export const useAnalyse = (idCurrentLabo: string) => {
    const pharmacie = getPharmacie()

    const client = useApolloClient();
    const [analyseToEdit, setAnalyseToEdit] = useState<any>(undefined);
    const [saving, setSaving] = useState<boolean>(false)
    const [saved, setSaved] = useState<boolean>(false)


    const showError = (
        message: string = "Des erreurs se sont survenues pendant le chargement des fichiers",
    ): void => {
        displaySnackBar(client, {
            type: 'ERROR',
            message: message,
            isOpen: true,
        });
    };



    // FICHIERS
    const [doCreatePutPresignedUrl] = useMutation<
        CREATE_PUT_PESIGNED_URL,
        CREATE_PUT_PESIGNED_URLVariables
    >(DO_CREATE_PUT_PESIGNED_URL, {
        client: FEDERATION_CLIENT,
    });



    /**query analyse */
    const analyseTypes = useQuery<GET_LIST_ANALYSE_TYPES_TYPE, GET_LIST_ANALYSE_TYPESVariables>(
        GET_LIST_ANALYSE_TYPES,
        { client: FEDERATION_CLIENT },
    );

    const [loadAnalyses, loadingAnalyses] = useLazyQuery<GET_ANALYSES_TYPE, GET_ANALYSESVariables>(GET_ANALYSES, {
        variables: {
            filter: {
                idPartenaireTypeAssocie: {
                    eq: idCurrentLabo,
                },
            },
        },
        client: FEDERATION_CLIENT,
    });
    const countAnalyse = useQuery<GET_ANALYSES_AGGREGATES_TYPES, GET_ANALYSE_AGGREGATESVariables>(
        GET_ANALYSE_AGGREGATES,
        {
            client: FEDERATION_CLIENT,
        },
    );



    /*MUTATION ANALYSE*/
    /**create */
    const [createAnalyse, creatingAnalyse] = useMutation<
        CREATE_ANALYSE_TYPE,
        CREATE_ANALYSEVariables
    >(CREATE_ANALYSE, {
        onCompleted: () => {
            displaySnackBar(client, {
                message: ' succès !',
                type: 'SUCCESS',
                isOpen: true,
            });

            loadingAnalyses.refetch();
            setSaving(false)
            setSaved(true)
        },
        onError: () => {
            displaySnackBar(client, {
                message: 'Des erreurs se sont survenues pendant la création de l\'analyse',
                type: 'ERROR',
                isOpen: true,
            });
        },
        client: FEDERATION_CLIENT,
    });

    /**update analyse */
    const [updateAnalyse, updatingAnalyse] = useMutation<
        UPDATE_ANALYSE_TYPE,
        UPDATE_ANALYSEVariables
    >(UPDATE_ANALYSE, {
        onCompleted: () => {
            displaySnackBar(client, {
                message: "L'Analyse a été modifié avec succès !",
                type: 'SUCCESS',
                isOpen: true,
            });
            loadingAnalyses.refetch();
            setSaving(false)
            setSaved(true)
        },

        onError: () => {
            displaySnackBar(client, {
                message: "Des erreurs se sont survenues pendant la modification de l'analyse",
                type: 'ERROR',
                isOpen: true,
            });
        },
        client: FEDERATION_CLIENT,
    });

    /**delete analyse */
    const [deleteAnalyse, deletingAnalyse] = useMutation<
        DELETE_ONE_ANALYSE_TYPE,
        DELETE_ONE_ANALYSEVariables
    >(DELETE_ONE_ANALYSE, {
        onCompleted: () => {
            displaySnackBar(client, {
                message: "L'analyse a été supprimé avec succès !",
                type: 'SUCCESS',
                isOpen: true,
            });
            loadingAnalyses.refetch();
        },
        onError: () => {
            displaySnackBar(client, {
                message: "Des erreurs se sont survenues pendant la suppression de l'analyse",
                type: 'ERROR',
                isOpen: true,
            });
        },
        client: FEDERATION_CLIENT,
    });




    const saveAnalyse = (analyse: any, fichiers: any) => {
        if (analyse.id) {
            //console.log('analyseToSave:', analyseToSave);
            updateAnalyse({
                variables: {
                    id: analyse.id,
                    input: {
                        titre: analyse.titre,
                        dateDebut: analyse.dateDebut,
                        dateFin: analyse.dateFin,
                        dateChargement: analyse.dateChargement,
                        fichiers: fichiers,
                        partenaireType: 'LABORATOIRE',
                        idPartenaireTypeAssocie: idCurrentLabo,
                        idType: analyse.idTypeAnalyse.id,
                    } as any,
                },
            });
        } else {
            createAnalyse({
                variables: {
                    input: {
                        titre: analyse.titre,
                        dateDebut: analyse.dateDebut,
                        dateFin: analyse.dateFin,
                        dateChargement: analyse.dateChargement,
                        fichiers: fichiers,
                        partenaireType: 'LABORATOIRE',
                        idPartenaireTypeAssocie: idCurrentLabo,
                        idType: analyse.idTypeAnalyse.id,
                    } as any,
                },
            });
        }
    }


    // Plan marketing Props
    const handleSave = (analyse: any) => {
        console.log('planSave:', analyse);

        setSaving(true)
        setSaved(false)

        if (analyse?.selectedFiles?.length > 0) {
            console.log('detection');
            const newFiles = analyse.selectedFiles.filter(file => file && file instanceof File);
            const oldFiles = analyse.selectedFiles
                .filter(file => file && !(file instanceof File))
                .map(file => {
                    if (file) {
                        return {
                            chemin: file.chemin,
                            nomOriginal: file.nomOriginal,
                            type: file.type,
                        };
                    }
                });
            console.log('Plan fichier newFiles', newFiles, 'oldFiles', oldFiles);

            if (newFiles?.length > 0) {
                const filePaths = newFiles.map(file => formatFilename(file, ``));
                doCreatePutPresignedUrl({
                    variables: {
                        filePaths,
                    },
                }).then(async response => {
                    if (response.data?.createPutPresignedUrls) {
                        try {
                            const fichiers = await Promise.all(
                                response.data.createPutPresignedUrls.map((item, index) => {
                                    if (item?.presignedUrl) {
                                        return analyse.selectedFiles
                                            ? uploadToS3(analyse.selectedFiles[index], item?.presignedUrl).then(
                                                uploadResult => {
                                                    if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                                                        return {
                                                            chemin: item.filePath,
                                                            nomOriginal: analyse.selectedFiles
                                                                ? analyse.selectedFiles[index].name
                                                                : '',
                                                            type: analyse.selectedFiles
                                                                ? analyse.selectedFiles[index].type
                                                                : '',
                                                        };
                                                    }
                                                },
                                            )
                                            : null;
                                    }
                                }),
                            );
                            saveAnalyse(analyse, [...fichiers, ...oldFiles]);
                        } catch (error) {
                            showError()
                        }
                    } else {
                        showError()
                    }
                });
            } else {
                if (oldFiles?.length > 0) {
                    saveAnalyse(analyse, oldFiles);
                } else {
                    saveAnalyse(analyse, undefined);
                }
            }
        } else {
            saveAnalyse(analyse, undefined);
        }
    };


    const handleRequestEdit = (analyse: any) => {
        setAnalyseToEdit(analyse);
    };

    const handleRequestDelete = (analyse: any) => {
        deleteAnalyse({
            variables: {
                input: { id: analyse.id },
            },
        });
    };





    // ANalyse Search
    const handleRequestSearch = ({ skip, take, searchText, filter }: any) => {

        const keywords = ['idType'];
        if (idCurrentLabo) {
            const _filters = keywords
                .map(keyword => {
                    const tempObject = { or: [] };
                    for (let currentfilter of (filter || [])) {
                        if (currentfilter.keyword === keyword) {
                            tempObject.or.push({
                                [keyword]: {
                                    eq: currentfilter.id,
                                },
                            } as never);
                        }
                    }
                    return tempObject.or.length > 0 ? tempObject : undefined;
                })
                .filter(e => e);

            const filterAnd: PRTAnalyseFilter[] = [
                {
                    partenaireType: {
                        eq: 'LABORATOIRE',
                    },
                },
                {
                    idPartenaireTypeAssocie: {
                        eq: idCurrentLabo,
                    },
                },
                {
                    idPharmacie: {
                        eq: pharmacie.id
                    }
                },
                {
                    or: [
                        {
                            titre: {
                                iLike: `%${searchText}%`,
                            },
                        }
                    ],
                },
            ];

            if (_filters.length > 0) {
                _filters.map(filter => {
                    filterAnd.push(filter as any);
                });
            }

            loadAnalyses({
                variables: {
                    filter: {
                        and: filterAnd
                    },
                    paging: {
                        offset: skip,
                        limit: take,
                    },
                },
            });
        }
    };

    /** AnalyseProps */
    const analyseProps = {
        loading: loadingAnalyses.loading || analyseTypes.loading,
        error: loadingAnalyses.error || analyseTypes.error,
        analyseToEdit: analyseToEdit,
        analyseTypes: analyseTypes.data?.pRTAnalyseTypes.nodes,
        rowsTotal: countAnalyse.data?.pRTAnalyseAggregate.count?.id as any,
        data: loadingAnalyses.data?.pRTAnalyses.nodes,
        saving,
        saved,
        setSaved: (newSaved: boolean) => setSaved(!newSaved),
        onRequestEdit: handleRequestEdit,
        onRequestSave: handleSave,
        onRequestDelete: handleRequestDelete,
        onRequestSearch: handleRequestSearch,
    };

    return { handleRequestSearch, analyseProps };
};
