import { useQuery } from '@apollo/client';
import React, { FC } from 'react';
import loadable from '@loadable/component'
import { Route, RouteComponentProps, Switch, withRouter } from 'react-router-dom';
import { ADMINISTRATEUR_GROUPEMENT, SUPER_ADMINISTRATEUR } from '../../Constant/roles';
import {
  CAHIER_LIAISON_URL,
  INTELLIGENCE_COLLECTIVE_URL,
  MESSAGE_DELETED_URL,
  MESSAGE_FORWARD_URL,
  MESSAGE_NEW_URL,
  MESSAGE_REPLY_ALL_URL,
  MESSAGE_REPLY_URL,
  MESSAGE_SENDED_URL,
  MESSAGE_URL,
  PARTAGE_IDEE_BONNE_PRATIQUE_URL,
  PARTENAIRE_SERVICE_URL,
  SHAREINFO_DETAILS_URL,
  SHAREINFO_FRIEND_GROUP_URL,
  SHAREINFO_ITEMS_CHOICE_URL,
  SHAREINFO_LABO_PARTENAIRE_URL,
  SHAREINFO_PARTENAIRE_SERVICE_URL,
} from '../../Constant/url';
import { GET_CURRENT_PHARMACIE } from '../../graphql/Pharmacie/local';
import ICurrentPharmacieInterface from '../../Interface/CurrentPharmacieInterface';
import { actualiteFilter } from '../../services/actualiteFilter';
import { AppAuthorization } from '../../services/authorization';
import { getGroupement } from '../../services/LocalStorage';
import { projectFilter } from '../../services/projectFilter';
import {
  CatalogueLaboratoireProduit,
  CatalogueOperationProduit,
  CatalogueProduit,
  GestionProduit,
} from '../Common/newWithSearch/ComponentInitializer';
import withSearch from '../Common/withSearch';
import withUser from '../Common/withUser';
import Loadman from '../Loading/Loadman';
import SmallLoading from '../Loading/SmallLoading';
import { ProtectedRoute } from '../Router';
import { MainContextProvider } from './mainContext';
import { Backdrop } from '@app/ui-kit';

const CreateComponent = loadable(()=>import('../Common/CreateComponent'), {
  fallback: <Backdrop/>
})

const SuiviApples = loadable(()=>import('../Dashboard/AideEtSupport/SuiviAppels/SuiviAppels'), {
  fallback: <Backdrop/>
})
const RgpdPoliConfidentialite = loadable(()=>import('../Dashboard/Rgpd/RgpdPoliConfidentialite/RgpdPoliConfidentialite'), {
  fallback: <Backdrop/>
})
const MarcheList = loadable(()=>import('../Dashboard/Marche/MarcheList'), {
  fallback: <Backdrop/>
})
const MarcheForm = loadable(()=>import('../Dashboard/Marche/MarcheForm'), {
  fallback: <Backdrop/>
})
const CreationProduit = loadable(()=>import('../Dashboard/GestionProduits/CreationProduit'), {
  fallback: <Backdrop/>
})
const EditComponent = loadable(()=>import('../Common/EditComponent'), {
  fallback: <Backdrop/>
})

const CommandeDetail = loadable(()=>import('./Content/Commandes/CommandeDetail/CommandeDetail'), {
  fallback: <Backdrop/>
})
const CommandeoralePage = loadable(()=>import('./Content/Commande-orale/CommandeOrale'), {
  fallback: <Backdrop/>
})
const CreateActualite = loadable(()=>import('./Content/Actualite/Create'), {
  fallback: <Backdrop/>
})
const Actualite = loadable(()=>import('./Content/Actualite/Actualite'), {
  fallback: <Backdrop/>
})
const Unauthorized = loadable(()=>import('../Unauthorized'), {
  fallback: <Backdrop/>
})
const NotificationPage = loadable(()=>import('../Notification'), {
  fallback: <Backdrop/>
})

const DemarcheQualite = loadable(()=>import('./Content/DemarcheQualite/DemarcheQualite'), {
  fallback: <Backdrop/>
})
const ChecklistEvaluation = loadable(()=>import('./Content/DemarcheQualite/ChecklilstEvaluation/ChecklistEvaluation'), {
  fallback: <Backdrop/>
})
const AutoEvaluation = loadable(()=>import('./Content/DemarcheQualite/AutoEvaluation'), {
  fallback: <Backdrop/>
})
const DetailsActionOperationnelle = loadable(()=>import('./Content/DemarcheQualite/ActionOperationnelle/DetailsActionOperationnelle/DetailsActionOperationnelle'), {
  fallback: <Backdrop/>
})
const ActionOperationnelle = loadable(()=>import('./Content/DemarcheQualite/ActionOperationnelle/ActionOperationnelle'), {
  fallback: <Backdrop/>
})
const Commandes = loadable(()=>import('./Content/Commandes/Commandes'), {
  fallback: <Backdrop/>
})


const FicheIncident = loadable(()=>import('./Content/DemarcheQualite/FicheIncident/FicheIncident'), {
  fallback: <Backdrop/>
})
const FicheCompteRenduForm = loadable(()=>import('./Content/DemarcheQualite/FicheCompteRendu/FicheCompteRenduForm/FicheCompteRenduForm'), {
  fallback: <Backdrop/>
})
const FicheCompteRendu = loadable(()=>import('./Content/DemarcheQualite/FicheCompteRendu/FicheCompteRendu'), {
  fallback: <Backdrop/>
})
const DetailsFicheCompteRendu = loadable(()=>import('./Content/DemarcheQualite/FicheCompteRendu/DetailsCompteRendu/DetailsCompteRendu'), {
  fallback: <Backdrop/>
})
const FicheAmelioration = loadable(()=>import('./Content/DemarcheQualite/FicheAmelioration/FicheAmelioration'), {
  fallback: <Backdrop/>
})
const DetailsFicheAmelioration = loadable(()=>import('./Content/DemarcheQualite/FicheAmelioration/DetailsFicheAmelioration/DetailsFicheAmelioration'), {
  fallback: <Backdrop/>
})

const Tools = loadable(()=>import('./Content/DemarcheQualite/Tools/Tools'), {
  fallback: <Backdrop/>
})
const ReferentielQualite = loadable(()=>import('./Content/DemarcheQualite/ReferentielQualite'), {
  fallback: <Backdrop/>
})
const DetailsOutils = loadable(()=>import('./Content/DemarcheQualite/Outils/DetailsOutils/DetailsOutils'), {
  fallback: <Backdrop/>
})
const Outils = loadable(()=>import('./Content/DemarcheQualite/Outils/Outils'), {
  fallback: <Backdrop/>
})
const FicheIncidentForm = loadable(()=>import('./Content/DemarcheQualite/FicheIncident/FicheIncidentForm/FicheIncidentForm'), {
  fallback: <Backdrop/>
})
const DetailsIncident = loadable(()=>import('./Content/DemarcheQualite/FicheIncident/DetailsIncident/DetailsIncident'), {
  fallback: <Backdrop/>
})

const ListesGroupeAmis = loadable(()=>import('./Content/InteligenceCollective/ListesGroupeAmis'), {
  fallback: <Backdrop/>
})
const CahierLiaison = loadable(()=>import('./Content/InteligenceCollective/CahierLiaison'), {
  fallback: <Backdrop/>
})
const IntelligenceCollective = loadable(()=>import('./Content/InteligenceCollective'), {
  fallback: <Backdrop/>
})
const Homepage = loadable(()=>import('./Content/Homepage'), {
  fallback: <Backdrop/>
})
const Formation = loadable(()=>import('./Content/Formation'), {
  fallback: <Backdrop/>
})
const GestionEspaceDocumentaire = loadable(()=>import('./Content/EspaceDocumentaire/GestionEspaceDocumentaire'), {
  fallback: <Backdrop/>
})

const PartageInformations = loadable(()=>import('./Content/InteligenceCollective/PartageInformations'), {
  fallback: <Backdrop/>
})
const PartageInfoDetails = loadable(()=>import('./Content/InteligenceCollective/PartageInfoDetails/PartageInfoDetails'), {
  fallback: <Backdrop/>
})

const StepperIdea = loadable(()=>import('./Content/InteligenceCollective/PartageIdeeBonnePratique/FormAjout/StepperIdea'), {
  fallback: <Backdrop/>
})
const PartageIdeeBonnePratique = loadable(()=>import('./Content/InteligenceCollective/PartageIdeeBonnePratique'), {
  fallback: <Backdrop/>
})
const ListesPartenaireService = loadable(()=>import('./Content/InteligenceCollective/ListesPartenaireService'), {
  fallback: <Backdrop/>
})
const ListesLaboPartenaire = loadable(()=>import('./Content/InteligenceCollective/ListesLaboPartenaire/ListesLaboPartenaire'), {
  fallback: <Backdrop/>
})

const Panier = loadable(()=>import('./Content/Panier'), {
  fallback: <Backdrop/>
})
const AutrePanier = loadable(()=>import('./Content/Panier/AutrePanier/AutrePanier'), {
  fallback: <Backdrop/>
})
const Recapitulatif = loadable(()=>import('./Content/Panier/Recapitulatif/Recapitulatif'), {
  fallback: <Backdrop/>
})
const OperationCommerciale = loadable(()=>import('./Content/OperationCommerciale'), {
  fallback: <Backdrop/>
})
const Newsletters = loadable(()=>import( './Content/Newsletters'), {
  fallback: <Backdrop/>
})
const MesTraitementAutomatiques = loadable(()=>import('./Content/MesTraitementAutomatiques'), {
  fallback: <Backdrop/>
})
const Messagerie = loadable(()=>import('./Content/Messagerie/Messagerie'), {
  fallback: <Backdrop/>
})

const Rapport = loadable(()=>import('./Content/Rapport'), {
  fallback: <Backdrop/>
})
const Dashboard = loadable(()=>import('./Content/Dashboard/Dashboard'), {
  fallback: <Backdrop/>
})
const ThemePage = loadable(()=>import('./Content/Theme/ThemePage'), {
  fallback: <Backdrop/>
})
const ProduitDetails = loadable(()=>import('./Content/Produits/ProduitDetails/ProduitDetails'), {
  fallback: <Backdrop/>
})
const PilotageDashboard = loadable(()=>import('./Content/Pilotage/Dashboard'), {
  fallback: <Backdrop/>
})
const Cible = loadable(()=>import('./Content/Pilotage/Cible/Cible'), {
  fallback: <Backdrop/>
})

const Pilotage = loadable(()=>import('./Content/Pilotage'), {
  fallback: <Backdrop/>
})
const PartenairesServices = loadable(()=>import('./Content/PartenairesServices'), {
  fallback: <Backdrop/>
})
const PartenairesLaboratoires = loadable(()=>import('./Content/PartenairesLaboratoires'), {
  fallback: <Backdrop/>
})


const ReleveTemperature = loadable(()=>import('./Content/ReleveTemperatures/ReleveTemperature'), {
  fallback: <Backdrop/>
})
const TodoNew = loadable(()=>import('./Content/TodoNew'), {
  fallback: <Backdrop/>
})
interface MainRouterProps {
  user: any;
}

const MainRouter: FC<MainRouterProps & RouteComponentProps> = ({ user }) => {
  const groupement = getGroupement();
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const LazyProduit = Loadman(import('./Content/Produits'), SmallLoading);

  const auth = new AppAuthorization(user);

  const isAdmin =
    user &&
    user.role &&
    (user.role.code === SUPER_ADMINISTRATEUR || user.role.code === ADMINISTRATEUR_GROUPEMENT);

  const operationOptionalSearchMust = isAdmin
    ? {
      must: [
        { term: { idGroupement: groupement && groupement.id } },
        { term: { isRemoved: false } },
      ],
    }
    : {
      must: [
        { term: { idGroupement: groupement && groupement.id } },
        { term: { isRemoved: false } },
      ],
      should: [
        { term: { 'operationPharmacie.globalite': true } },
        {
          bool: {
            must: [
              {
                terms: {
                  'pharmacieCible.id': [
                    myPharmacie &&
                    myPharmacie.data &&
                    myPharmacie.data.pharmacie &&
                    myPharmacie.data.pharmacie.id,
                  ],
                },
              },
              { term: { 'operationPharmacie.globalite': false } },
            ],
          },
        },
      ],
      minimum_should_match: 1,
    };

  const projectOptionalSearchMust = projectFilter(user, groupement && groupement.id);
  const actualiteOptionalSearchMust = actualiteFilter(user, groupement && groupement.id);
  const produitOptionalSearchMust = [{ term: { isRemoved: false } }];
  const ideeBonnePratiqueOptionalSearchMust = [];

  return (
    <MainContextProvider>
      <Switch>
        <Route path="/" exact={true} component={Homepage} {...user} />
        <Route
          path="/rgpd-politique-de-confident/:tab?"
          exact={true}
          component={RgpdPoliConfidentialite}
          {...user}
        />
        <Route path="/unauthorized" exact={true} component={Unauthorized} {...user} />
        <ProtectedRoute
          path="/theme"
          exact={true}
          component={ThemePage}
          {...user}
          isAuthorized={auth.isAuthorizedToChangeTheme()}
        />
        <Route
          path={['/releve-des-temperatures']}
          exact={true}
          component={ReleveTemperature}
          {...user}
          isAuthorized={auth.isAuthorizedToViewReleveTemperature()}
        />

        <ProtectedRoute
          path={['/marches']}
          exact={true}
          component={MarcheList}
          {...user}
          isAuthorized={auth.isAuthorizedToViewMarcheList()}
        />
        <ProtectedRoute
          path={['/promotions']}
          exact={true}
          component={MarcheList}
          {...user}
          isAuthorized={auth.isAuthorizedToViewPromoList()}
        />
        {/* Create marché */}
        <ProtectedRoute
          path={[
            '/marche/create/base-infos',
            '/marche/create/articles',
            '/marche/create/laboratoires',
            '/marche/create/groups-client',
          ]}
          exact={true}
          component={MarcheForm}
          {...user}
          isAuthorized={auth.isAuthorizedToAddMarche()}
        />
        {/* Edit marché */}
        <ProtectedRoute
          path={[
            '/marche/edit/base-infos/:id',
            '/marche/edit/articles/:id',
            '/marche/edit/laboratoires/:id',
            '/marche/edit/groups-client/:id',
          ]}
          exact={true}
          component={MarcheForm}
          {...user}
          isAuthorized={auth.isAuthorizedToEditMarche()}
        />
        {/* Create Promo */}
        <ProtectedRoute
          path={[
            '/promotion/create/base-infos',
            '/promotion/create/articles',
            '/promotion/create/laboratoires',
            '/promotion/create/groups-client',
          ]}
          exact={true}
          component={MarcheForm}
          {...user}
          isAuthorized={auth.isAuthorizedToAddPromo()}
        />
        {/* Edit Promo */}
        <ProtectedRoute
          path={[
            '/promotion/edit/base-infos/:id',
            '/promotion/edit/articles/:id',
            '/promotion/edit/laboratoires/:id',
            '/promotion/edit/groups-client/:id',
          ]}
          exact={true}
          component={MarcheForm}
          {...user}
          isAuthorized={auth.isAuthorizedToEditPromo()}
        />
        {/* Pilotage */}
        <ProtectedRoute
          path="/pilotage"
          exact={true}
          component={Pilotage}
          {...user}
          isAuthorized={
            auth.isAuthorizedToViewPilotageBusiness() || auth.isAuthorizedToViewPilotageFeedback()
          }
        />
        {/* Pilotage business */}
        <ProtectedRoute
          path={['/pilotage/dashboard/business/global']}
          exact={true}
          component={PilotageDashboard}
          {...user}
          isAuthorized={auth.isAuthorizedToViewPilotageBusiness()}
        />
        {/* Pilotage business view by pharmacie */}
        <ProtectedRoute
          path={['/pilotage/dashboard/business/pharmacie']}
          exact={true}
          component={PilotageDashboard}
          {...user}
          isAuthorized={
            auth.isAuthorizedToViewPilotageBusiness() &&
            auth.isAuthorizedToViewPilotageByPharmacie()
          }
        />
        {/* Pilotage business view by operation */}
        <ProtectedRoute
          path={['/pilotage/dashboard/business/operation']}
          exact={true}
          component={PilotageDashboard}
          {...user}
          isAuthorized={
            auth.isAuthorizedToViewPilotageBusiness() &&
            auth.isAuthorizedToViewPilotageByOperation()
          }
        />
        {/* Pilotage feedback */}
        <ProtectedRoute
          path={['/pilotage/dashboard/feedback/global', '/pilotage/dashboard/feedback/item']}
          exact={true}
          component={PilotageDashboard}
          {...user}
          isAuthorized={auth.isAuthorizedToViewPilotageFeedback()}
        />
        {/* Pilotage feedback view by operation */}
        <ProtectedRoute
          path={['/pilotage/dashboard/feedback/pharmacie']}
          exact={true}
          component={PilotageDashboard}
          {...user}
          isAuthorized={
            auth.isAuthorizedToViewPilotageFeedback() &&
            auth.isAuthorizedToViewPilotageByOperation()
          }
        />
        {/* Pilotage publicite*/}
        <ProtectedRoute
          path={['/pilotage/dashboard/publicite']}
          exact={true}
          component={PilotageDashboard}
          {...user}
          isAuthorized={
            auth.isAuthorizedToViewPilotageFeedback() &&
            auth.isAuthorizedToViewPilotageByOperation()
          }
        />
        {/* Catalogue produits */}
        <ProtectedRoute
          path="/catalogue-produits/:view/:id"
          exact={true}
          component={ProduitDetails}
          {...user}
          isAuthorized={auth.isAuthorizedToViewDetailsProductFromCatalogProduct()}
        />
        <ProtectedRoute
          path="/catalogue-produits/:view"
          exact={true}
          component={CatalogueProduit}
          {...user}
          isAuthorized={auth.isAuthorizedToViewListCatalogProduct()}
        />
        <Route
          path={[
            '/operation-produits/:view/:idOperation',
            '/operation-produits/:view/:idOperation/:idRemise',
          ]}
          exact={true}
          component={CatalogueOperationProduit}
          {...user}
        />
        <Route
          path={['/laboratoire-produits/:view/:idLaboratoire']}
          exact={true}
          component={CatalogueLaboratoireProduit}
          {...user}
        />
        {/* Actualites */}
        <Route
          path={['/actualites']}
          exact={true}
          component={withSearch(
            Actualite,
            'actualite',
            null,
            actualiteOptionalSearchMust,
            undefined,
            {
              exclude: [
                'item',
                'laboratoire',
                'servicesCible',
                'pharmaciesCible',
                'pharmaciesRolesCible',
                'presidentsCibles',
                'partenairesCible',
                'laboratoiresCible',
              ],
            },
          )}
          {...user}
        />
        <ProtectedRoute
          path={['/actualite/:id']}
          exact={true}
          component={withSearch(
            Actualite,
            'actualite',
            null,
            actualiteOptionalSearchMust,
            undefined,
            {
              exclude: [
                'item',
                'laboratoire',
                'servicesCible',
                'pharmaciesCible',
                'pharmaciesRolesCible',
                'presidentsCibles',
                'partenairesCible',
                'laboratoiresCible',
              ],
            },
          )}
          {...user}
          isAuthorized={auth.isAuthorizedToViewActu()}
        />
        <ProtectedRoute
          path="/create/actualite"
          exact={true}
          component={withSearch(CreateActualite, 'actualite')}
          isAuthorized={auth.isAuthorizedToCreateActu()}
          {...user}
        />
        <ProtectedRoute
          path="/edit/actualite/:id"
          exact={true}
          component={withSearch(CreateActualite, 'actualite')}
          isAuthorized={auth.isAuthorizedToEditActu()}
          {...user}
        />
        {/* Operation commerciale */}
        <Route
          path={'/operations-commerciales'}
          exact={true}
          component={withSearch(
            OperationCommerciale,
            'operation',
            null,
            operationOptionalSearchMust,
            undefined,
            { exclude: ['actualite'] },
          )}
          {...user}
        />

        <Route
          path={[PARTAGE_IDEE_BONNE_PRATIQUE_URL, `${PARTAGE_IDEE_BONNE_PRATIQUE_URL}/:id`]}
          exact={true}
          component={withSearch(
            PartageIdeeBonnePratique,
            'ideeoubonnepratique',
            null,
            ideeBonnePratiqueOptionalSearchMust,
          )}
          {...user}
        />

        <ProtectedRoute
          path={'/operations-commerciales/:id?/:view?'}
          exact={true}
          component={withSearch(
            OperationCommerciale,
            'operation',
            null,
            operationOptionalSearchMust,
            undefined,
            { exclude: ['actualite'] },
          )}
          {...user}
          isAuthorized={auth.isAuthorizedToViewOC()}
        />
        {/* Add product */}
        <ProtectedRoute
          path={['/gestion-produits/create']}
          exact={true}
          component={CreationProduit}
          {...user}
          isAuthorized={auth.isAuthorizedToAddProduct()}
        />
        {/* Edit product */}
        <ProtectedRoute
          path={['/gestion-produits/create/:id']}
          exact={true}
          component={CreationProduit}
          {...user}
          isAuthorized={auth.isAuthorizedToEditProduct()}
        />
        <Route path={'/gestion-produits/:view'} exact={true} component={GestionProduit} {...user} />
        <Route path="/panier" exact={true} component={Panier} {...user} />
        <Route path="/autrepanier" exact={true} component={AutrePanier} {...user} />
        <Route path="/notifications" exact={true} component={NotificationPage} {...user} />
        <Route
          path={[
            MESSAGE_URL,
            MESSAGE_NEW_URL,
            MESSAGE_SENDED_URL,
            MESSAGE_DELETED_URL,
            `${MESSAGE_URL}/:messageId`,
            `${MESSAGE_SENDED_URL}/:messageId`,
            `${MESSAGE_DELETED_URL}/:messageId`,
            `${MESSAGE_REPLY_URL}/:source/:messageId`,
            `${MESSAGE_REPLY_ALL_URL}/:source/:messageId`,
            `${MESSAGE_FORWARD_URL}/:source/:messageId`,
          ]}
          exact={true}
          component={Messagerie}
          {...user}
        />
        <ProtectedRoute
          path="/recapitulatif/:type"
          exact={true}
          component={Recapitulatif}
          {...user}
          isAuthorized={auth.isAuthorizedToValidateCommande()}
        />
        <Route
          path="/laboratoires/:idLabo?/:tab?/:view?"
          exact={true}
          component={PartenairesLaboratoires}
          {...user}
        />
        <Route path="/dashboard" exact={true} component={Dashboard} {...user} />
        <Route
          path="/rapport/:pilotageType?"
          exact={true}
          component={Rapport}
          {...user}
        />
        <ProtectedRoute
          path="/edit/:type/:id/:filter?"
          exact={true}
          component={EditComponent}
          {...user}
          isAuthorized={auth.isAuthorizedToCreateOC}
        />
        <ProtectedRoute
          path="/create/:type/:filter?"
          exact={true}
          component={CreateComponent}
          {...user}
          isAuthorized={auth.isAuthorizedToEditOC}
        />
        <Route
          path="/prestataires-services"
          exact={true}
          component={withUser(PartenairesServices)}
          {...user}
        />
        <ProtectedRoute
          path="/suivi-commandes"
          exact={true}
          component={withUser(Commandes)}
          {...user}
          isAuthorized={auth.isAuthorizedToViewListCommande()}
        />
        <ProtectedRoute
          path="/suivi-commandes/:id"
          exact={true}
          component={withUser(CommandeDetail)}
          {...user}
          isAuthorized={auth.isAuthorizedToViewDetailsCommande()}
        />
        <Route
          path={`${PARTENAIRE_SERVICE_URL}`}
          exact={true}
          component={PartenairesServices}
          {...user}
        />
        <Route path="/formations" exact={true} component={Formation} {...user} />
        <Route path="/newsletters" exact={true} component={Newsletters} {...user} />
        <Route path="/demarche-qualite" exact={true} component={DemarcheQualite} {...user} />
        <Route
          path="/demarche-qualite/auto-evaluation"
          exact={true}
          component={AutoEvaluation}
          {...user}
        />
        <Route path="/demarche-qualite/outils" exact={true} component={Outils} {...user} />
        <Route
          path="/demarche-qualite/referentiel-qualite"
          exact={true}
          component={ReferentielQualite}
          {...user}
        />
        <Route
          path="/demarche-qualite/fiche-amelioration"
          exact={true}
          component={FicheAmelioration}
          {...user}
        />

        <Route
          path="/demarche-qualite/fiche-amelioration/:id/:idIncident"
          exact={true}
          component={FicheIncidentForm}
          {...user}
        />
        <Route
          path="/demarche-qualite/fiche-amelioration/:id"
          exact={true}
          component={FicheIncidentForm}
          {...user}
        />
        <Route
          path="/demarche-qualite/fiche-amelioration-detail/:id"
          exact={true}
          component={DetailsFicheAmelioration}
          {...user}
        />
        <Route
          path="/demarche-qualite/fiche-incident"
          exact={true}
          component={FicheIncident}
          {...user}
        />
        <Route
          path="/demarche-qualite/fiche-incident/:id"
          exact={true}
          component={FicheIncidentForm}
          {...user}
        />
        <Route
          path="/demarche-qualite/fiche-incident-detail/:id"
          exact={true}
          component={DetailsIncident}
          {...user}
        />

        <Route
          path="/demarche-qualite/compte-rendu"
          exact={true}
          component={FicheCompteRendu}
          {...user}
        />
        <Route
          path="/demarche-qualite/compte-rendu/:id"
          exact={true}
          component={FicheCompteRenduForm}
          {...user}
        />
        <Route
          path="/demarche-qualite/compte-rendu/details/:id"
          component={DetailsFicheCompteRendu}
          {...user}
        />

        <Route
          path="/demarche-qualite/compte-rendu"
          exact={true}
          component={FicheCompteRendu}
          {...user}
        />
        <Route
          path="/demarche-qualite/compte-rendu/:id"
          exact={true}
          component={FicheCompteRenduForm}
          {...user}
        />
        <Route
          path="/demarche-qualite/compte-rendu/details/:id"
          component={DetailsFicheCompteRendu}
          {...user}
        />
        <Route
          path="/demarche-qualite/outils/checklist"
          exact={true}
          component={ChecklistEvaluation}
          {...user}
        />
        <Route
          path="/demarche-qualite/outils/checklist/:id"
          exact={true}
          component={ChecklistEvaluation}
          {...user}
        />
        <Route path="/demarche-qualite" exact={true} component={DemarcheQualite} {...user} />
        <Route path="/demarche-qualite/outils" exact={true} component={Outils} {...user} />
        {/*<Route
        path="/demarche-qualite/outils/:outilType"
        exact={true}
        component={DetailsOutils}
        {...user}
      />*/}

        <Route
          path="/demarche-qualite/outils/:typologie"
          exact={true}
          component={Tools}
          {...user}
        />

        <Route
          path="/demarche-qualite/outils/:typologie/:id"
          exact={true}
          component={Tools}
          {...user}
        />

        {/*<Route
        path="/demarche-qualite/outils/:outilType/:idOutil"
        exact={true}
        component={DetailsOutils}
        {...user}
      />*/}
        <Route path="/demarche-qualite" exact={true} component={DemarcheQualite} {...user} />
        <Route path="/demarche-qualite/outils" exact={true} component={Outils} {...user} />
        <Route
          exact={true}
          path={`${SHAREINFO_FRIEND_GROUP_URL}/:itemId?`}
          component={ListesGroupeAmis}
        />

        <Route
          path="/demarche-qualite/action-operationnelle"
          exact={true}
          component={ActionOperationnelle}
          {...user}
        />

        <Route
          path="/demarche-qualite/action-operationnelle/:id"
          exact={true}
          component={FicheIncidentForm}
          {...user}
        />

        <Route
          path="/demarche-qualite/action-operationnelle-detail/:id"
          exact={true}
          component={DetailsActionOperationnelle}
          {...user}
        />

        <Route
          exact={true}
          path={`${SHAREINFO_LABO_PARTENAIRE_URL}/:itemId?`}
          component={ListesLaboPartenaire}
        />
        <Route
          exact={true}
          path={`${SHAREINFO_PARTENAIRE_SERVICE_URL}/:itemId?`}
          component={ListesPartenaireService}
        />
        <Route exact={true} path={SHAREINFO_ITEMS_CHOICE_URL} component={PartageInformations} />

        <Route
          path={`${SHAREINFO_DETAILS_URL}/:item?/:tab?/:itemId?`}
          exact={true}
          component={PartageInfoDetails}
        />

        {/* <Route path={`/rgpd-politique-de-confident/:tab`} exact={true} component={RgpdPoliConfidentialite} /> */}

        <Route
          path="/demarche-qualite/outils/:outilType"
          exact={true}
          component={DetailsOutils}
          {...user}
        />

        <Route
          path={`${INTELLIGENCE_COLLECTIVE_URL}/:type?`}
          exact={true}
          component={IntelligenceCollective}
          {...user}
        />
        <Route
          path={`${PARTAGE_IDEE_BONNE_PRATIQUE_URL}/create`}
          exact={true}
          component={StepperIdea}
          {...user}
        />

        <Route
          path={`${PARTAGE_IDEE_BONNE_PRATIQUE_URL}/edit/:ideeId`}
          exact={true}
          component={StepperIdea}
          {...user}
        />

        <Route
          path={['/todo/:filter?/:filterId?/:task?/:taskId?']}
          exact={true}
          component={TodoNew}
          {...user}
        />
        <ProtectedRoute
          path="/pilotage/business/:item?"
          exact={true}
          component={Cible}
          {...user}
          isAuthorized={auth.isAuthorizedToViewPilotageBusiness()}
        />
        <ProtectedRoute
          path="/pilotage/feedback/:item?"
          exact={true}
          component={Cible}
          {...user}
          isAuthorized={auth.isAuthorizedToViewPilotageFeedback()}
        />
        {/* Create Liaison information */}
        <Route
          path={[
            `${CAHIER_LIAISON_URL}`,
            `${CAHIER_LIAISON_URL}/create`,
            `${CAHIER_LIAISON_URL}/edit/:idliaison`,
            `${CAHIER_LIAISON_URL}/recue/:idliaison?/:from?`,
            `${CAHIER_LIAISON_URL}/emise/:idliaison?/:from?`,
          ]}
          exact={true}
          component={CahierLiaison}
          {...user}
        />
        {/*
      <Route
        path={`${CAHIER_LIAISON_URL}/viewdetails/:idliaison/:from?`}
        exact={true}
        component={DetailsInfo}
        {...user}
      />
      */}
        <Route
          path="/espace-documentaire/:idDocument?"
          exact={true}
          component={GestionEspaceDocumentaire}
          {...user}
        />

        <Route
          path="/outils-digitaux/traitements-automatiques"
          exact={true}
          component={MesTraitementAutomatiques}
          {...user}
        />

        <Route
          path="/outils-digitaux/commandes-orales"
          exact
          component={CommandeoralePage}
          {...user}
        />

        <Route path="/outils-digitaux/suivi-appels" exact component={SuiviApples} {...user} />

        <Route
          path="/releve-des-temperatures"
          exact={true}
          component={ReleveTemperature}
          {...user}
        />
      </Switch>
    </MainContextProvider>
  );
};

export default withRouter(MainRouter);
