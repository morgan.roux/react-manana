/*global chrome, browser*/
import React, { MouseEvent, useEffect } from 'react';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import useStyles from './styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import IconButton from '@material-ui/core/IconButton';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Draggable from 'react-draggable';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import WebAssetIcon from '@material-ui/icons/WebAsset';
import { APP_SSO_URL } from '../../config';
import { Link } from '@material-ui/core';
import DraggableDialog from './ExtensionDialog';
import { detect } from 'detect-browser';
import { getGroupement, getAccessToken, getUser, getPharmacie } from '../../services/LocalStorage';
import { useLazyQuery, useApolloClient, useQuery } from '@apollo/client';
import { CHROME_EXTENSION_ID } from '../../config';
import CircularProgress from '@material-ui/core/CircularProgress';
// import {browser} from 'firefox-webext-browser';

import {
  APPLICATIONSROLE,
  APPLICATIONSROLEVariables,
} from '../../graphql/Application/types/APPLICATIONSROLE';

import { GET_APPLICATIONS_ROLE } from '../../graphql/Application/query';

import {
  HELLOID_APPLICATION,
  HELLOID_APPLICATIONVariables,
} from '../../graphql/HelloIdApplication/types/HELLOID_APPLICATION';

import { GET_HELLOID_APPLICATIONS } from '../../graphql/HelloIdApplication/query';
import { useValueParameterAsBoolean } from '../../utils/getValueParameter';
import SnackVariableInterface from '../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../utils/snackBarUtils';
import { AWS_HOST } from '../../utils/s3urls';
import ICurrentPharmacieInterface from '../../Interface/CurrentPharmacieInterface';
import { GET_CURRENT_PHARMACIE } from '../../graphql/Pharmacie/local';

// import { browser } from "webextension-polyfill-ts";

const browserInfo = detect();

interface DraggableData {
  node: HTMLElement;
  // lastX + deltaX === x
  x: number;
  y: number;
  deltaX: number;
  deltaY: number;
  lastX: number;
  lastY: number;
}

export interface ResponseMessage {
  type: string;
  message: string;
}

export default function RecipeReviewCard() {
  const classes = useStyles({});
  const [expanded, setExpanded] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [hrefLink, sethrefLink] = React.useState<string | null>(null);
  const client = useApolloClient();
  const helloidParam = useValueParameterAsBoolean('0046');
  // const activeDirectoryParam = useValueParameterAsBoolean('0049');
  // const ftpParam = useValueParameterAsBoolean('0050');
  const applicationsParam = useValueParameterAsBoolean('0051');

  const groupement = getGroupement();
  const accessToken = getAccessToken();
  const user = getUser();

  const { data: myPharmacie } = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

  const { data: applicationsRole, loading: applicationsRoleLoading } = useQuery<
    APPLICATIONSROLE,
    APPLICATIONSROLEVariables
  >(GET_APPLICATIONS_ROLE, {
    // fetchPolicy: 'cache-and-network',
    variables: {
      idgroupement: (groupement && groupement.id) || '',
      coderole: (user && user.role.code) || '',
    },
    onError: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur lors de l'affichage des autres applications, veuillez recharger la page!!`,
        isOpen: true,
      };
      applicationsParam && displaySnackBar(client, snackBarData);
    },
  });

  // useEffect(() => {
  //   applicationsParam && getApplicationsRole({
  //     variables: {
  //       idgroupement : (groupement && groupement.id) || '',
  //       coderole : (user && user.role.code) || ''
  //     }
  //   });
  // }, [applicationsParam, applicationsRole])

  // useEffect(() => {
  //   console.log("test");
  // }, [pharmacie])

  const { data: helloidApplications, loading: helloidApplicationsLoading } = useQuery<
    HELLOID_APPLICATION,
    HELLOID_APPLICATIONVariables
  >(GET_HELLOID_APPLICATIONS, {
    // fetchPolicy: 'cache-and-network',
    variables: {
      iduser: (user && user.id) || '',
      idGroupement: (groupement && groupement.id) || '',
    },
    onError: error => {
      console.log('error ', error);
      /* const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: error.message,
        isOpen: true,
      };
      helloidParam && displaySnackBar(client, snackBarData);
      */
    },
  });

  // useEffect(() => {
  //   console.log("effects");
  //   helloidParam && getHelloIdApplications({
  //     variables: {
  //       iduser : (user && user.id) || '',
  //       idGroupement: (groupement && groupement.id) || '',
  //     }
  //   });
  // }, [helloidParam, helloidApplications])

  const applicationsRoledata = applicationsRole && applicationsRole.applicationsRole;
  const helloidApplicationsData = helloidApplications && helloidApplications.userApplications;
  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  const handleDrag = (e: Event, data: DraggableData) => {
    if (data.x >= 20) {
      console.log('stop');
    }
  };

  const checkExtension = (extensionid: string): Promise<boolean> => {
    return new Promise(resolve => {
      chrome.runtime.sendMessage(extensionid, 'CHECK_EXTENSION', function (
        response: ResponseMessage,
      ) {
        if (response) {
          console.log('response ', response);
          if (response.type === 'success') {
            console.log(response.message);
            return resolve(true);
          } else {
            console.log(response.message);
            return resolve(false);
          }
        } else {
          console.log('no response found from extension!!!');
          return resolve(false);
        }
      });
    });
  };

  const checkFirefoxExtension = (): Promise<boolean> => {
    return new Promise(resolve => {
      if (document.body.classList.contains('plugin-Digital4win.com')) {
        console.log('plugin installed');
        return resolve(true);
      } else {
        return resolve(false);
      }
    });
  };

  // const checkFirefoxExtension2  = (extensionid : string) : Promise<boolean> => {
  //   return new Promise((resolve, reject) =>{
  //     browser.runtime.sendMessage(extensionid, {checking : "CHECK_EXTENSION"}).then(
  //       function(response : ResponseMessage) {
  //         if (response) {
  //           console.log("response ", response);
  //           if (response.message === 'success'){
  //             console.log(response.message);
  //             return resolve(true);
  //           }
  //           else{
  //             console.log(response.message);
  //             return resolve(false);
  //           }
  //         }
  //         else{
  //           console.log("no response found from extension!!!");
  //           return resolve(false);
  //         }
  //       });
  //   })
  // };

  const handleCheckExtension = async (href: string | null, e: MouseEvent) => {
    e.persist();
    sethrefLink(href);
    e.preventDefault();
    let check: boolean = false;
    switch (browserInfo && browserInfo.name) {
      case 'chrome':
        console.log('check chrome extension');
        console.log('CHROME_EXTENSION_ID ', CHROME_EXTENSION_ID);
        const chromeid = CHROME_EXTENSION_ID || '';
        check = await checkExtension(chromeid);
        break;
      case 'firefox':
      case 'edge':
        console.log('check firefox extension');
        check = await checkFirefoxExtension();
        break;
      // case 'edge':
      //   console.log("check edge extension");
      //   break;
      default:
        console.log('browser not supported');
        check = false;
        break;
    }
    if (check) {
      console.log('go to apps');
      if (href) window.open(href, '_blank');
    } else {
      console.log('show popup to add extension');
      setOpen(true);
    }
  };

  // console.log("applicationsRole ", applicationsRole);

  return (
    <div>
      <DraggableDialog open={open} setOpen={setOpen} hrefLink={hrefLink} />
      <Draggable
        axis="x"
        handle="#draggable-content"
        onDrag={(e: any, data: any) => {
          return handleDrag(e, data);
        }}
      >
        <Card className={classes.card}>
          <CardHeader
            id="draggable-content"
            title={<div className={classes.title}>Mes applications</div>}
            className={classes.header}
            action={
              <CardActions disableSpacing={true}>
                <IconButton
                  className={clsx(classes.expand, {
                    [classes.expandOpen]: expanded,
                  })}
                  onClick={handleExpandClick}
                  aria-expanded={expanded}
                  size="small"
                >
                  <ExpandMoreIcon className={classes.expandIcon} />
                </IconButton>
              </CardActions>
            }
          />
          <Collapse in={expanded} timeout="auto">
            <CardContent className={classes.body}>
              <Typography variant="body2" color="textSecondary" component="div">
                <List className={classes.root}>
                  {applicationsRoleLoading && applicationsParam && (
                    <CircularProgress color="secondary" className={classes.loader} />
                  )}
                  {!applicationsRoleLoading &&
                    applicationsParam &&
                    applicationsRoledata &&
                    applicationsRoledata.map(application => {
                      if (application && application.applications) {
                        return (
                          <ListItem key={`list_${application.applications.id}`}>
                            {(application.applications.icon &&
                              application.applications.icon.chemin && (
                                <Avatar
                                  src={`${AWS_HOST}/${application.applications.icon.chemin}`}
                                />
                              )) || (
                                <Avatar>
                                  <WebAssetIcon />
                                </Avatar>
                              )}
                            <Link
                              target="_blank"
                              href={`${APP_SSO_URL}/go-to/${(groupement && groupement.id) ||
                                ''}/${(myPharmacie &&
                                  myPharmacie.pharmacie &&
                                  myPharmacie.pharmacie.cip) ||
                                ''}/${application.applications.id}/${accessToken}`}
                            >
                              <ListItemText
                                className={classes.appname}
                                primary={application.applications.nom}
                              />
                            </Link>
                          </ListItem>
                        );
                      }
                      return '...loading';
                    })}
                  {helloidParam && helloidApplicationsLoading && (
                    <CircularProgress color="secondary" className={classes.loader} />
                  )}
                  {!helloidApplicationsLoading &&
                    helloidParam &&
                    helloidApplicationsData &&
                    helloidApplicationsData.map(application => {
                      if (application && application.helloIdurl) {
                        return (
                          <ListItem key={`list_${application.applicationGUID}`}>
                            {(application.iconlink && <Avatar src={application.iconlink} />) || (
                              <WebAssetIcon />
                            )}
                            <Link
                              onClick={handleCheckExtension.bind(null, application.helloIdurl)}
                              target="_blank"
                              href={application.helloIdurl}
                            >
                              <ListItemText
                                className={classes.appname}
                                primary={application.name}
                              />
                            </Link>
                          </ListItem>
                        );
                      }
                      return '...loading';
                    })}
                </List>
              </Typography>
            </CardContent>
          </Collapse>
        </Card>
      </Draggable>
    </div>
  );
}
