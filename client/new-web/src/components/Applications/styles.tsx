import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      width: 350,
      position: 'absolute',
      bottom: theme.spacing(0),
      right: theme.spacing(3),
      // height : 350,
      zIndex: theme.zIndex.drawer + 1,
      padding: 0,
    },
    title: {
      fontSize: 15,
      color: '#fff',
      fontWeight: 'bold',
    },
    header: {
      backgroundColor: theme.palette.primary.light,
      color: '#fff',
      height: 50,
      cursor: 'move',
    },
    expandIcon: {
      color: '#fff',
    },
    body: {
      padding: 0,
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
      padding: 0,
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    root: {
      width: '100%',
      maxHeight: 300,
      overflow: 'auto',
      maxWidth: 360,
      backgroundColor: theme.palette.background.paper,
      textAlign: 'center',
    },
    appname: {
      color: '#000',
      marginLeft: 5,
    },
    avatarImage: {
      objectFit: 'cover',
      width: '100%',
    },
    loader: {
      margin: 'auto',
    },
  }),
);

export default useStyles;
