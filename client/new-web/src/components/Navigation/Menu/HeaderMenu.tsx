import { useQuery } from '@apollo/client';
import { Box, ClickAwayListener, Hidden, Paper, Popper, PopperProps } from '@material-ui/core';
import Badge from '@material-ui/core/Badge';
import IconButton from '@material-ui/core/IconButton';
import {
  Apps,
  Assignment,
  Dashboard,
  Email,
  Notifications,
  ShoppingCart,
  Equalizer
} from '@material-ui/icons';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { INTELLIGENCE_COLLECTIVE_URL, MESSAGE_URL } from '../../../Constant/url';
import { GET_ME } from '../../../graphql/Authentication/query';
import { ME, ME_me } from '../../../graphql/Authentication/types/ME';
import { GET_NOTIFICATIONS } from '../../../graphql/Notification/query';
import {
  GET_DELETED_NOTIFICATIONS_SUBSCRIPTION,
  GET_NOTIFICATIONS_SUBSCRIPTION,
  GET_UPDATED_NOTIFICATIONS_SUBSCRIPTION,
} from '../../../graphql/Notification/subscription';
import {
  NOTIFICATIONS,
  NOTIFICATIONSVariables,
} from '../../../graphql/Notification/types/NOTIFICATIONS';
import { GET_MY_PANIERS } from '../../../graphql/Panier/query';
import { myPaniers, myPaniersVariables } from '../../../graphql/Panier/types/myPaniers';
import { GET_CURRENT_PHARMACIE } from '../../../graphql/Pharmacie/local';
import { PharmacieMinimInfo } from '../../../graphql/Pharmacie/types/PharmacieMinimInfo';
import { SEARCH } from '../../../graphql/search/query';
import { SEARCH as SEARCH_Interface, SEARCHVariables } from '../../../graphql/search/types/SEARCH';
import ICurrentPharmacieInterface from '../../../Interface/CurrentPharmacieInterface';
import { getGroupement, getUser } from '../../../services/LocalStorage';
import { projectFilter } from '../../../services/projectFilter';
import { NotificationMenu } from '../../Notification';
import { useStyles } from './styles';
import {
  useValueParameterAsBoolean,
  useValueParameterByView,
} from '../../../utils/getValueParameter';
import { isMobile } from '../../../utils/Helpers';

export interface HeaderMenuProps {
  nbMessage: number;
}

const HeaderMenu: FC<HeaderMenuProps & RouteComponentProps> = ({
  history,
  location: { pathname },
  nbMessage,
}) => {
  const [open, setOpen] = React.useState(false);
  const [mobile, setMobile] = React.useState<boolean>(window.innerWidth < 1366);
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);
  const [openMenuApp, setOpenMenuApp] = React.useState<boolean>(false);
  const enableTodo = useValueParameterByView('0207', '0702', isMobile());
  const enableMessagerie = useValueParameterByView('0317', '0720', isMobile());
  const enableCataloguesProduits = useValueParameterByView('0303', '0706', isMobile());
  const enableDashboard = useValueParameterByView('0210', '0745', isMobile());

  const displayRapportGroupement = useValueParameterAsBoolean('0379');
  const displayRapportPharmacie = useValueParameterAsBoolean('0780');


  const [showDashboardMenu, setShowDashboadMenu] = useState<boolean>(false);
  const [showRapportMenu, setShowRapportMenu] = useState<boolean>(false);


  React.useEffect(() => {
    if (/*pathname !== '/dashboard' && pathname !== '/' &&*/ enableDashboard) {
      setShowDashboadMenu(true);
    } else {
      setShowDashboadMenu(false);
    }
  }, [pathname, enableDashboard]);


  React.useEffect(() => {
    if (/*pathname !== '/rapport' && pathname !== '/' &&*/ displayRapportPharmacie && displayRapportGroupement) {
      setShowRapportMenu(true);
    } else {
      setShowRapportMenu(false);
    }
  }, [pathname, displayRapportGroupement, displayRapportPharmacie]);


  // Get currentPharmacie
  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const currentPharmacie: PharmacieMinimInfo | null =
    (myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie) || null;

  // Get groupement
  const groupement = getGroupement();

  // Get currentUser
  const currentUser: ME_me = getUser();

  const handleToggle = (e: React.MouseEvent) => {
    if (window.innerWidth > 600) {
      setAnchorEl(e.currentTarget as HTMLElement);
      setOpen(prev => !prev);
    } else {
      history.push('/notifications');
    }
  };

  const handleDashbordClick = React.useCallback(() => {
    history.push('/dashboard');
  }, [history]);

  const handleRapportClick = React.useCallback(() => {
    const pilotageType = pathname.startsWith('/todo') ? 'TODO' : 'DEMARCHE_QUALITE'

    history.push(`/rapport/${pilotageType}`, { from: pathname });
  }, [history, pathname]);

  const handleClose = (event: React.MouseEvent<EventTarget>) => {
    if (anchorEl && anchorEl.contains(event.target as HTMLElement)) {
      return;
    }
    setOpen(false);
  };

  const handleGoToPanier = () => {
    history.push('/panier');
  };

  const handleGoToMessagerie = () => {
    history.push(MESSAGE_URL);
  };

  const handleGoToIntelligenceCollective = () => {
    // handleMobileMenuClose();
    history.push(INTELLIGENCE_COLLECTIVE_URL);
  };

  const { loading, data, fetchMore, subscribeToMore } = useQuery<
    NOTIFICATIONS,
    NOTIFICATIONSVariables
  >(GET_NOTIFICATIONS, {
    fetchPolicy: 'cache-and-network',
    variables: {
      first: 10,
      skip: 0,
    },
  });

  const notSeenNotification =
    data && data.notifications && data.notifications.notSeen > 0 ? data.notifications.notSeen : 0;

  const fetchMoreNotifications = () => {
    fetchMore({
      variables: {
        first: 10,
        skip:
          data && data.notifications && data.notifications.data
            ? data.notifications.data.length
            : 0,
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev;
        return {
          notifications: {
            __typename: 'NotificationResult',
            data:
              prev &&
                prev.notifications &&
                prev.notifications.data &&
                fetchMoreResult.notifications &&
                fetchMoreResult.notifications.data
                ? [...prev.notifications.data, ...fetchMoreResult.notifications.data]
                : [],
            total: data && data.notifications ? data.notifications.total : 0,
            notSeen: data && data.notifications ? data.notifications.notSeen : 0,
          },
        };
      },
    });
  };

  const subscribeToMoreNotification = () => {
    /* listener for notification add */
    subscribeToMore({
      document: GET_NOTIFICATIONS_SUBSCRIPTION,
      onError: error => {
        console.log('erroraa ', error);
      },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData) return prev;

        const newData = subscriptionData && (subscriptionData.data as any);

        if (newData && newData.notificationAdded) {
          console.log('addedData', JSON.stringify(newData.notificationAdded));
        }

        return {
          notifications: {
            __typename: 'NotificationResult',
            data:
              prev &&
                prev.notifications &&
                prev.notifications.data &&
                newData &&
                newData.notificationAdded
                ? [newData.notificationAdded, ...prev.notifications.data]
                : prev && prev.notifications && prev.notifications.data
                  ? [...prev.notifications.data]
                  : [],
            total:
              (prev && prev.notifications && prev.notifications.total
                ? prev.notifications.total
                : 0) + 1,
            notSeen:
              (prev && prev.notifications && prev.notifications.total
                ? prev.notifications.notSeen
                : 0) + 1,
          } as any,
        };
      },
    });

    /* listener for notification updated */
    subscribeToMore({
      document: GET_UPDATED_NOTIFICATIONS_SUBSCRIPTION,
      onError: error => {
        console.log('erroraa ', error);
      },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData) return prev;
        const updatedData = subscriptionData && (subscriptionData.data as any);
        if (updatedData && updatedData.notificationUpdated) {
          console.log('updatedData', JSON.stringify(updatedData.notificationUpdated));
        }

        const total =
          prev && prev.notifications && prev.notifications.total ? prev.notifications.total : 0;
        const notSeen =
          prev && prev.notifications && prev.notifications.notSeen ? prev.notifications.notSeen : 0;
        console.log('update total : ', total, 'notSeen : ', notSeen);

        return {
          notifications: {
            __typename: 'NotificationResult',
            data:
              prev &&
                prev.notifications &&
                prev.notifications.data &&
                prev.notifications.data.length &&
                updatedData &&
                updatedData.notificationUpdated &&
                updatedData.notificationUpdated.id
                ? [
                  updatedData.notificationUpdated,
                  ...prev.notifications.data.filter(
                    notif => notif && notif.id && notif.id !== updatedData.notificationUpdated.id,
                  ),
                ]
                : prev &&
                  prev.notifications &&
                  prev.notifications.data &&
                  prev.notifications.data.length
                  ? [...prev.notifications.data]
                  : [],
            total:
              updatedData && updatedData.notificationUpdated && updatedData.notificationUpdated.seen
                ? total - 1
                : updatedData &&
                  updatedData.notificationUpdated &&
                  !updatedData.notificationUpdated.seen
                  ? total + 1
                  : total,
            notSeen:
              updatedData && updatedData.notificationUpdated && updatedData.notificationUpdated.seen
                ? notSeen - 1
                : updatedData &&
                  updatedData.notificationUpdated &&
                  !updatedData.notificationUpdated.seen
                  ? notSeen + 1
                  : notSeen,
          } as any,
        };
      },
    });

    /* listener for notification deleted */
    subscribeToMore({
      document: GET_DELETED_NOTIFICATIONS_SUBSCRIPTION,
      onError: error => {
        console.log('erroraa ', error);
      },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData) return prev;

        console.clear();
        const deletedData = subscriptionData && (subscriptionData.data as any);
        if (deletedData && deletedData.notificationDeleted) {
          console.log('deletedData', JSON.stringify(deletedData.notificationDeleted));
        }

        const total =
          prev && prev.notifications && prev.notifications.total ? prev.notifications.total : 0;
        const notSeen =
          prev && prev.notifications && prev.notifications.notSeen ? prev.notifications.notSeen : 0;
        console.log(' delete total : ', total, 'notSeen : ', notSeen);

        return {
          notifications: {
            __typename: 'NotificationResult',
            data:
              prev &&
                prev.notifications &&
                prev.notifications.data &&
                prev.notifications.data.length &&
                deletedData &&
                deletedData.notificationDeleted &&
                deletedData.notificationDeleted.id
                ? [
                  ...prev.notifications.data.filter(
                    notif => notif && notif.id && notif.id !== deletedData.notificationDeleted.id,
                  ),
                ]
                : prev &&
                  prev.notifications &&
                  prev.notifications.data &&
                  prev.notifications.data.length
                  ? [...prev.notifications.data]
                  : [],
            total:
              deletedData &&
                deletedData.notificationDeleted &&
                !deletedData.notificationDeleted.seen
                ? total - 1
                : total,
            notSeen:
              deletedData &&
                deletedData.notificationDeleted &&
                !deletedData.notificationDeleted.seen
                ? notSeen - 1
                : notSeen,
          } as any,
        };
      },
    });
  };

  const listResult = useQuery<SEARCH_Interface, SEARCHVariables>(SEARCH, {
    variables: {
      type: ['project'],
      query: { query: { bool: projectFilter(currentUser, groupement && groupement.id) } },
    },
    skip: !currentPharmacie || (currentPharmacie && !currentPharmacie.id) ? true : false,
  });

  // get task now
  const listProjects =
    (listResult && listResult.data && listResult.data.search && listResult.data.search.data) || [];

  const actionsList: any[] = [];

  if (listProjects) {
    listProjects.map(
      (project: any) =>
        project &&
        project.projectActions &&
        project.projectActions.map((action: any) => actionsList.push(action)),
    );
  }

  let actionsFilterToday: any | null = [];

  if (actionsList && actionsList.length) {
    actionsFilterToday = actionsList.filter(
      (action: any) =>
        !action.isRemoved &&
        action.status === 'ACTIVE' &&
        ((action.userCreation &&
          action.userCreation.id &&
          currentUser &&
          action.userCreation.id === currentUser.id) ||
          (action.assignedUsers &&
            currentUser &&
            action.assignedUsers.map(i => i.id).includes(currentUser.id))) &&
        moment(action.dateDebut).format('DD MMMM YYYY') ===
        moment(Date.now()).format('DD MMMM YYYY'),
    );
  }

  const onClickTaskToday = () => {
    history.push(`/todo/aujourdhui`);
  };

  useEffect(() => {
    subscribeToMoreNotification();
  }, []);

  // myPanier initialisation
  const myPanierResult = useQuery<myPaniers, myPaniersVariables>(GET_MY_PANIERS, {
    variables: { idPharmacie: (currentPharmacie && currentPharmacie.id) || '', take: 10, skip: 0 },
    skip: !currentPharmacie || (currentPharmacie && !currentPharmacie.id) ? true : false,
    fetchPolicy: 'cache-and-network',
  });

  const panierLength =
    myPanierResult &&
    myPanierResult.data &&
    myPanierResult.data.myPaniers &&
    myPanierResult.data.myPaniers
      .map(panier => (panier && panier.panierLignes) || [])
      .reduce((total, value) => {
        return (total && value && total.concat(value)) || [];
      }, []).length;

  React.useEffect(() => {
    window.addEventListener('resize', () => {
      if (window.innerWidth < 1366) setMobile(true);
      else setMobile(false);

      setOpen(false);
      setOpenMenuApp(false);
    });
  },[]);

  const handleShowApps = (e: React.MouseEvent) => {
    setAnchorEl(e.currentTarget as HTMLElement);
    setOpenMenuApp(prev => !prev);
  };

  const me = useQuery<ME>(GET_ME);
  const userTheme: any = me && me.data && me.data.me && me.data.me.theme;
  const classes = useStyles({
    userTheme,
  });

  const clickAway = () => {
    setOpenMenuApp(false);
    setOpen(false);
    setAnchorEl(null);
  };

  useEffect(() => {
    console.log(open);
  }, [open]);

  return (
    <ClickAwayListener onClickAway={clickAway}>
      <div style={{display:'flex'}}>

        {!isMobile() && (
          <>
            {showDashboardMenu && (
              <IconButton onClick={handleDashbordClick}>
                <Dashboard htmlColor="#ffffff" />
              </IconButton>
            )}
            {showRapportMenu && (
              <IconButton onClick={handleRapportClick}>
                <Equalizer htmlColor="#ffffff" />
              </IconButton>
            )}

            {enableCataloguesProduits && (
              <IconButton className={classes.iconBtn} onClick={handleGoToPanier}>
                <Badge badgeContent={panierLength} color="error">
                  <ShoppingCart />
                </Badge>
              </IconButton>
            )}
            {enableTodo && (
              <IconButton className={classes.iconBtn} onClick={onClickTaskToday}>
                <Badge badgeContent={actionsFilterToday.length} color="error">
                  <Assignment />
                </Badge>
              </IconButton>
            )}
            {enableMessagerie && (
              <IconButton
                aria-controls={open ? 'menu-list-grow' : undefined}
                aria-haspopup="true"
                onClick={handleGoToMessagerie}
                aria-label={`show ${nbMessage} new messages`}
                className={classes.iconBtn}
              >
                <Badge badgeContent={nbMessage} color="error">
                  <Email />
                </Badge>
              </IconButton>
            )}
          </>
        )}

        <IconButton
          onClick={handleToggle}
          aria-label={`show ${notSeenNotification} new notifications`}
        >
          <Badge badgeContent={notSeenNotification} color="error">
            <Notifications htmlColor="#FFF" />
          </Badge>
        </IconButton>
        <NotificationMenu
          popperProps={{
            open,
            anchorEl,
            children: null,
          }}
          handleClose={handleClose}
          notifications={data && data.notifications ? data.notifications : null}
          loading={loading}
          fetchMoreNotifications={fetchMoreNotifications}
        />
      </div>
    </ClickAwayListener>
  );
};

export default withRouter(HeaderMenu);
