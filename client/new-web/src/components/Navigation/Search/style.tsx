import { createStyles, fade, Theme, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    search: {
      position: 'relative',
      flex: 1,
      display: 'flex',
      marginRight: 16,
      width: '100%',
      backgroundColor: theme.palette.common.white,
      borderRadius: 6,
      visibility: 'hidden'
    },
    dialog: {
      alignSelf: 'flex-start',
      position: 'relative',
      marginTop: '8%',
    },
  }),
);
