import { createStyles, fade, Theme, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        contentList: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
        },
        contentLeftList: {
            display: 'flex',
            alignItems: 'center',
            '& .MuiTypography-root': {
                fontSize: '0.75rem',
                fontFamily: 'Montserrat',
                fontWeight: '600',
                lineHeight: '18px',
                maxWidth: 294,
            },
        },
        contentImgSearch: {
            '& img': {
                width: 80,
            },
            marginRight: '1rem',
        },
        textContentRight: {
            fontSize: '0.75rem',
            fontFamily: 'Montserrat',
            fontWeight: 400,
            color: '#B1B1B1',
            textAlign: 'right',
            lineHeight: '18px',
            maxWidth: 354,
        }
    }),
);
