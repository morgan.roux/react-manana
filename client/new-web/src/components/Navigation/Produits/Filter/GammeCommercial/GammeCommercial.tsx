import React, { useState, useEffect } from 'react';
import { ContentBox } from '../ContentBox';
import { useQuery, useApolloClient } from '@apollo/client';
import { GET_GAMME_COMMERCIALS } from '../../../../../graphql/CanalGamme/query';
import { GAMME_COMMERCIALS } from '../../../../../graphql/CanalGamme/types/GAMME_COMMERCIALS';
import { ContainerItem, FilterItem } from '../Item';
import { DATA, FILTER_PARAMETER } from '../../../../../Interface/Filter';
import { GET_PARAMETER_LOCAL } from '../../../../../localStates/filter';

const GammeCommercial = (props: any) => {
  const [checkeds, setCheckeds] = useState<number[]>([]);
  const [gammes, setGammes] = useState<DATA[]>([]);
  const client = useApolloClient();
  const results = useQuery<GAMME_COMMERCIALS>(GET_GAMME_COMMERCIALS);
  const filter = useQuery<FILTER_PARAMETER>(GET_PARAMETER_LOCAL);

  useEffect(() => {
    if (filter.data && filter.data.parameter && filter.data.parameter) {
      const { idGammeCommercials } = filter.data.parameter;
      setCheckeds(idGammeCommercials);
    }
  }, [filter]);

  useEffect(() => {
    if (results && results.data && results.data.canalGammes) {
      setGammes(
        results.data.canalGammes
          .filter(gammeCommercial => gammeCommercial && gammeCommercial.countCanalArticles !== 0)
          .map((gammeCommercial: any) => {
            let newSousGamme: any = [];
            if (gammeCommercial.sousGammeCommercial.length) {
              newSousGamme = gammeCommercial.sousGammeCommercial
                .filter((sous: any) => sous.countCanalArticles !== 0)
                .map((sousGame: any) => {
                  return {
                    id: parseInt(sousGame.id, 10),
                    nom: sousGame.libelle,
                    nbArticle: parseInt(sousGame.countCanalArticles),
                    __typename: 'sous_gamme',
                  };
                });
            }
            return {
              id: parseInt(gammeCommercial.id, 10),
              nom: gammeCommercial.libelle,
              nbArticle: parseInt(gammeCommercial.countCanalArticles),
              sousDatas: newSousGamme,
              __typename: 'gamme',
            };
          }),
      );
    }
  }, [results]);

  const onChange = (value: number) => {
    if (value === 0) {
      if (checkeds.length && checkeds.indexOf(value) === 0) {
        setCheckeds([]);
        return [];
      }
      const val = [0, ...gammes.map(gamme => gamme.id)];
      setCheckeds(val);
      return val;
    }

    const currentIndex = checkeds.indexOf(value);
    const newChecked = [...checkeds];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    // TODO: Migration
    /*(client as any).writeData({
      data: {
        parameter: {
          idGammeCommercials: newChecked,
          gammes,
          idLaboratoires:
            filter && filter.data && filter.data.parameter && filter.data.parameter.idLaboratoires
              ? filter.data.parameter.idLaboratoires
              : [],
          idFamilles:
            filter && filter.data && filter.data.parameter && filter.data.parameter.idFamilles
              ? filter.data.parameter.idFamilles
              : [],
          laboratoires:
            filter && filter.data && filter.data.parameter && filter.data.parameter.laboratoires
              ? filter.data.parameter.laboratoires
              : [],
          familles:
            filter && filter.data && filter.data.parameter && filter.data.parameter.familles
              ? filter.data.parameter.familles
              : [],
          searchText:
            filter && filter.data && filter.data.parameter && filter.data.parameter.searchText
              ? filter.data.parameter.searchText
              : '',
          __typename: 'local',
        },
      },
    });*/
    setCheckeds(newChecked);
    return newChecked;
  };

  const fetchMore = () => { };

  return (
    <ContentBox name="Gamme" fetchMore={fetchMore}>
      <ContainerItem open={true}>
        {gammes &&
          gammes
            .sort((g1, g2) => g2.nbArticle - g1.nbArticle)
            .map(gamme => (
              <FilterItem data={gamme} key={gamme.id} values={checkeds} onGetValue={onChange} />
            ))}
      </ContainerItem>
    </ContentBox>
  );
};

export default GammeCommercial;
