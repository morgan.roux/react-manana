import React, { useState, useEffect } from 'react';
import { ContentBox } from '../ContentBox';
import { useQuery, useApolloClient } from '@apollo/client';
import { SEARCH } from '../../../../../graphql/search/query';
import {
  SEARCH as SEARCH_Interface,
  SEARCHVariables,
} from '../../../../../graphql/search/types/SEARCH';
import { ContainerItem, FilterItem } from '../Item';
import { DATA, FILTER_PARAMETER } from '../../../../../Interface/Filter';
import { GET_PARAMETER_LOCAL } from '../../../../../localStates/filter';

const Laboratoire = (props: any) => {
  const [checkeds, setCheckeds] = useState<number[]>([]);
  const [laboratoires, setLaboratoires] = useState<DATA[]>([]);
  const client = useApolloClient();
  const [skip, setSkip] = useState<number>(3);

  const results = useQuery<SEARCH_Interface, SEARCHVariables>(SEARCH, {
    variables: {
      type: ['laboratoire'],
      skip: 0,
      take: 10000,
      query: {
        /* query: {
          bool: { must: { range: { countCanalArticles: { gte: 1 } } } },
        }, */
      },
      sortBy: [{ countCanalArticles: { order: 'desc' } }],
      idPharmacie: '',
    },
    fetchPolicy: 'network-only',
  });

  const filter = useQuery<FILTER_PARAMETER>(GET_PARAMETER_LOCAL);

  useEffect(() => {
    if (filter.data && filter.data.parameter && filter.data.parameter) {
      const { idLaboratoires } = filter.data.parameter;
      setCheckeds(idLaboratoires);
    }
  }, [filter]);

  useEffect(() => {
    if (results && results.data && results.data.search && results.data.search.data) {
      const query = results.data.search.data.slice(0, skip);
      setLaboratoires(
        query.map((labo: any) => {
          return {
            id: parseInt(labo.id, 10),
            nom: labo.nom,
            nbArticle: parseInt(labo.countCanalArticles),
            __typename: 'laboratoire',
          };
        }),
      );
    }
  }, [results]);

  const onChange = (value: number) => {
    if (value === 0) {
      if (checkeds.length && checkeds.indexOf(value) === 0) {
        setCheckeds([]);
        return [];
      }
      const val = [0, ...laboratoires.map(labo => labo.id)];
      setCheckeds(val);
      return val;
    }

    const currentIndex = checkeds.indexOf(value);
    const newChecked = [...checkeds];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    // TODO: Migration
    /*(client as any).writeData({
      data: {
        parameter: {
          idLaboratoires: newChecked,
          laboratoires,
          idGammeCommercials:
            filter &&
              filter.data &&
              filter.data.parameter &&
              filter.data.parameter.idGammeCommercials
              ? filter.data.parameter.idGammeCommercials
              : [],
          idFamilles:
            filter && filter.data && filter.data.parameter && filter.data.parameter.idFamilles
              ? filter.data.parameter.idFamilles
              : [],
          gammes:
            filter && filter.data && filter.data.parameter && filter.data.parameter.gammes
              ? filter.data.parameter.gammes
              : [],
          familles:
            filter && filter.data && filter.data.parameter && filter.data.parameter.familles
              ? filter.data.parameter.familles
              : [],
          searchText:
            filter && filter.data && filter.data.parameter && filter.data.parameter.searchText
              ? filter.data.parameter.searchText
              : '',
          __typename: 'local',
        },
      },
    });*/
    setCheckeds(newChecked);
    return newChecked;
  };

  /* const total =
    laboratoires && laboratoires.length
      ? laboratoires.map(laboratoire => laboratoire.nbArticle).reduce((sum, qte) => sum + qte)
      : 0; */

  const fetchMore = () => {
    if (results && results.data && results.data.search && results.data.search.data) {
      const nextSkip = skip + 3;
      setSkip(nextSkip);
      const query = results.data.search.data.slice(0, nextSkip);
      setLaboratoires(
        query.map((labo: any) => {
          return {
            id: parseInt(labo.id, 10),
            nom: labo.nom,
            nbArticle: parseInt(labo.countCanalArticles),
            __typename: 'laboratoire',
          };
        }),
      );
    }
  };
  return (
    <ContentBox name="Laboratoire" fetchMore={fetchMore}>
      <ContainerItem open={true}>
        {laboratoires &&
          laboratoires.map(laboratoire => (
            <FilterItem
              data={laboratoire}
              key={laboratoire.id}
              values={checkeds}
              onGetValue={onChange}
            />
          ))}
      </ContainerItem>
    </ContentBox>
  );
};

export default Laboratoire;
