import { createStyles, fade, Theme, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: '4px 30px',
    },
    search: {
      display: 'flex',
      position: 'relative',
      border: '1px solid #1D1D1D',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: theme.palette.common.white,
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.97),
      },
      marginTop: 8,
      marginLeft: 0,
      marginRight: 16,
      width: '100%',
    },
    searchIcon: {
      width: 36,
      height: '100%',
      position: 'absolute',
      right: '0',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputRoot: {
      color: 'inherit',
      width: '100%',
    },
    inputInput: {
      padding: '15px 36px 15px 8px',
      width: '100%',
      fontSize: '0.75rem!important',
      '&::placeholder': {
        fontSize: '0.75rem!important',
      },
    },
    contentListProduitItems: {
      borderTop: '1px solid #E3E3E3',
      padding: '20px 0',
    },
    name: {
      fontSize: '0.875rem',
    },
    listContentNameCheckBox: {
      padding: 0,
      overflow: 'hidden',

      '& .MuiListItem-button': {
        paddingLeft: 0,
        paddingTop: 0,
        paddingBottom: 0,
      },
    },
    listContentNameCheckBoxActive: {
      padding: 0,
      height: 'auto',
      overflow: 'auto',

      '& .MuiListItem-button': {
        paddingLeft: 0,
        paddingTop: 0,
        paddingBottom: 0,
      },
    },
    nbrProduits: {
      background: '#F3F3F3',
      fontSize: '0.75rem',
      padding: '2px 8px',
    },
    checkBoxLeftName: {
      '& .MuiCheckbox-root': {
        padding: 0,
        color: '#000000',
        marginRight: 8,
      },
      '&$checked': {
        color: '#000000',
      },
    },
    btnVoir: {
      background: '#004354',
      color: '#ffffff',
      fontSize: '0.75rem',
      textTransform: 'initial',
      marginTop: '1rem',
      padding: '4px 12px',
      '&:hover': {
        background: '#004354',
        opacity: 0.95,
      },
    },
  }),
);
