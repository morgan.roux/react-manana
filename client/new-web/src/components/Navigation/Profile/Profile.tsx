import React, { FC } from 'react';
import { getUser } from '../../../services/LocalStorage';
import useStyles from './styles';
import {
  Menu,
  MenuItem,
  Button,
  ListItemIcon,
  Divider,
  IconButton,
  Box,
  Hidden,
} from '@material-ui/core';
import { RouteComponentProps, withRouter } from 'react-router-dom';
// import {
//   ADMINISTRATEUR_GROUPEMENT,
//   PRESIDENT_REGION,
//   TITULAIRE_PHARMACIE,
// } from '../../../Constant/roles';
import classnames from 'classnames';
import UserInfo from './UserInfo';

import SettingsIcon from '../../../assets/icons/menu/parametres.svg';
import ThemeIcon from '../../../assets/icons/menu/theme.svg';
import SettingsTeamIcon from '../../../assets/icons/menu/parametres_equipe.svg';
import PilotageIcon from '../../../assets/icons/menu/pilotage_activite.svg';
import GestionProdIcon from '../../../assets/icons/menu/gestion_produit.svg';
import GestionMarcheIcon from '../../../assets/icons/menu/gestion_marche.svg';
import GestionPromoIcon from '../../../assets/icons/menu/gestion_promotion.svg';
import PricePolicyIcon from '../../../assets/icons/menu/politique_prix.svg';
import SupportIcon from '../../../assets/icons/menu/support.svg';
import NewsIcon from '../../../assets/icons/menu/quoi_de_neuf.svg';
import LogoutIcon from '../../../assets/icons/menu/deconnexion.svg';
import { AppAuthorization } from '../../../services/authorization';
import { Close, Menu as MenuIcon /*, ChatBubble, GroupWork*/ } from '@material-ui/icons';
import { INTELLIGENCE_COLLECTIVE_URL, MESSAGE_URL } from '../../../Constant/url';
import Pharmacie from '../Pharmacie';
import { isMobile } from '../../../utils/Helpers';
//import PsychologyIcon from '../../Main/Content/InteligenceCollective/Common/Icon/psychology';

interface ProfileInterface {
  inDrawer?: boolean;
  onDrawerClose?: () => void;
}

const Profile: FC<ProfileInterface & RouteComponentProps> = ({
  inDrawer,
  history,
  onDrawerClose,
}) => {
  const classes = useStyles({});
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const isMenuOpen = Boolean(anchorEl);
  const menuId = 'primary-search-account-menu';
  const user = getUser();
  // const userRoleCode = user && user.role && user.role.code;
  const auth = new AppAuthorization(user);

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleGoToSettings = () => {
    handleMenuClose();
    const to = 'user-settings';
    /* if (userRoleCode) {
      switch (userRoleCode) {
        case ADMINISTRATEUR_GROUPEMENT:
          to = `${PERSONNEL_GROUPEMENT_URL}`;
          break;
        case PRESIDENT_REGION:
        case TITULAIRE_PHARMACIE:
          to = `${PERSONNEL_PHARMACIE_URL}`;
          break;
      }
    } */
    history.push(`/db/${to}`);
  };

  const handleGoToTheme = () => {
    handleMenuClose();
    history.push('/db/theme');
  };

  const handleLogout = () => {
    localStorage.clear();
    Promise.resolve(history.push('')).then(_ => window.location.reload());
  };

  const handleGoToSupport = () => {
    history.push('/db/aide-support');
    handleMenuClose();
  };

  const handleGoToHelp = () => {
    handleMenuClose();
  };

  const handleGoToGestionProduit = () => {
    handleMenuClose();
    history.push('/gestion-produits/card');
  };

  const handleGoToPilotage = () => {
    handleMenuClose();
    history.push('/pilotage');
  };

  const handleGoToProductManagement = () => {
    handleMenuClose();
  };

  const handleGoToTeamSettings = () => {
    handleMenuClose();
  };

  const handleGoToUserAccount = () => {
    handleMenuClose();
    history.push('/db/user-settings');
  };

  const handleGoToMarche = () => {
    handleMenuClose();
    history.push('/marches');
  };

  const handleGoToPromo = () => {
    handleMenuClose();
    history.push('/promotions');
  };

  const handleGoToPricePolicy = () => {
    handleMenuClose();
  };

  const handleGoToMessagerie = () => {
    handleMenuClose();
    history.push(MESSAGE_URL);
  };

  const handleGoToIntelligenceCollective = () => {
    handleMenuClose();
    history.push(INTELLIGENCE_COLLECTIVE_URL);
  };

  const profileDropDownMenuPartOne = [
    {
      label: 'Paramètres',
      icon: <img src={SettingsIcon} />,
      onClick: handleGoToSettings,
      disabled: false,
    },
    {
      label: 'Thème',
      icon: <img src={ThemeIcon} />,
      onClick: handleGoToTheme,
      disabled: !auth.isAuthorizedToChangeTheme(),
    },
    {
      label: "Paramètres de l'équipe",
      icon: <img src={SettingsTeamIcon} />,
      onClick: handleGoToTeamSettings,
      disabled: true,
    },
    {
      label: "Pilotage de l'activité",
      icon: <img src={PilotageIcon} />,
      onClick: handleGoToPilotage,
      disabled:
        !auth.isAuthorizedToViewPilotageBusiness() && !auth.isAuthorizedToViewPilotageFeedback(),
    },
    {
      label: 'Gestion produits',
      icon: <img src={GestionProdIcon} />,
      onClick: handleGoToGestionProduit,
      disabled: false,
    },
    {
      label: 'Gestion de marché',
      icon: <img src={GestionMarcheIcon} />,
      onClick: handleGoToMarche,
      disabled: !auth.isAuthorizedToViewMarcheList(),
    },
    {
      label: 'Gestion des promotions',
      icon: <img src={GestionPromoIcon} />,
      onClick: handleGoToPromo,
      disabled: !auth.isAuthorizedToViewPromoList(),
    },
    {
      label: 'Politique des prix',
      icon: <img src={PricePolicyIcon} />,
      onClick: handleGoToPricePolicy,
      disabled: true,
    },
    /*{
      label: 'Messagerie',
      icon: <ChatBubble />,
      onClick: handleGoToMessagerie,
      disabled: false,
    },
    {
      label: 'Intelligence collective',
      icon: <PsychologyIcon />,
      onClick: handleGoToIntelligenceCollective,
      disabled: false,
    },*/
  ];

  const profileDropDownMenuPartTwo = [
    {
      label: 'Support',
      icon: <img src={SupportIcon} />,
      onClick: handleGoToSupport,
      disabled: false,
    },
    {
      label: 'Quoi de neuf ?',
      icon: <img src={NewsIcon} />,
      onClick: handleGoToHelp,
      disabled: true,
    },
    {
      label: 'Déconnexion',
      icon: <img src={LogoutIcon} />,
      onClick: handleLogout,
      disabled: false,
      badgeContent: 0,
    },
  ];

  const [mobile, setMobile] = React.useState<boolean>(window.innerWidth < 960);
  const [xmobile, setXMobile] = React.useState<boolean>(window.innerWidth < 768);

  React.useEffect(() => {
    window.addEventListener('resize', () => {
      if (window.innerWidth < 1280) setMobile(true);
      else setMobile(false);

      if (window.innerWidth < 1050) setXMobile(true);
      else setXMobile(false);
    });
  });

  const handleDrawerClose = () => {
    if (onDrawerClose) {
      onDrawerClose();
    }
  };

  if (inDrawer) {
    return (
      <Box>
        <Box display="flex" justifyContent="flex-end">
          <IconButton onClick={handleDrawerClose}>
            <Close />
          </IconButton>
        </Box>
        <Box>
          <UserInfo arrowDropDown={false} withUserName={true} aria-controls={menuId} />
        </Box>
        <Box>
          {profileDropDownMenuPartOne.map((item, index) => (
            <MenuItem
              key={`profile_menu_drop_down_part_one_${index}`}
              onClick={item.onClick}
              disabled={item.disabled}
            >
              <ListItemIcon style={{ minWidth: 35 }}>{item.icon}</ListItemIcon>
              {item.label}
            </MenuItem>
          ))}
        </Box>

        <Box>
          {profileDropDownMenuPartTwo.map((item, index) => (
            <MenuItem
              key={`profile_menu_drop_down_part_two_${index}`}
              onClick={item.onClick}
              disabled={item.disabled}
            >
              <ListItemIcon style={{ minWidth: 35 }}>{item.icon}</ListItemIcon>
              {item.label}
            </MenuItem>
          ))}
        </Box>
      </Box>
    );
  }

  return (
    <>
      {mobile ? (
        <>
          {!xmobile && (
            <Box>
              <Pharmacie />
            </Box>
          )}

          <IconButton onClick={handleProfileMenuOpen} className={classes.menuIconButton}>
            <MenuIcon htmlColor="#FFF" />
          </IconButton>
        </>
      ) : (
        <>
          <Pharmacie />
          <UserInfo onClick={handleProfileMenuOpen} arrowDropDown={true} aria-controls={menuId} />
        </>
      )}
      <Menu
        anchorEl={anchorEl}
        id={menuId}
        keepMounted={true}
        open={isMenuOpen}
        onClose={handleMenuClose}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        className={classes.menu}
      >
        {mobile && (
          <>
            {xmobile && (
              <Box>
                <Pharmacie />
              </Box>
            )}
            <UserInfo fullWidth={true} />
            <Divider variant="fullWidth" className={classes.divider} />
          </>
        )}
        {profileDropDownMenuPartOne.map((item, index) => (
          <MenuItem
            key={`profile_menu_drop_down_part_one_${index}`}
            onClick={item.onClick}
            disabled={item.disabled}
          >
            <ListItemIcon style={{ minWidth: 35 }}>{item.icon}</ListItemIcon>
            {item.label}
          </MenuItem>
        ))}
        <Divider variant="fullWidth" className={classes.divider} />
        {profileDropDownMenuPartTwo.map((item, index) => (
          <MenuItem
            key={`profile_menu_drop_down_part_two_${index}`}
            onClick={item.onClick}
            disabled={item.disabled}
          >
            <ListItemIcon style={{ minWidth: 35 }}>{item.icon}</ListItemIcon>
            {item.label}
          </MenuItem>
        ))}
      </Menu>
    </>
  );
};

export default withRouter(Profile);
