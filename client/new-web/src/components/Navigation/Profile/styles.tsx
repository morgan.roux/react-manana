import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    iconBtn: {
      color: theme.palette.common.white,
      opacity: 1,
      maxHeight: 50,
    },
    avatar: {
      width: 50,
      height: 50,
      marginRight: 5,
      borderRadius: 50,
    },
    username: {
      fontSize: 16,
    },
    role: {
      fontSize: 12,
    },
    menu: {
      minWidth: 250,
      '& *': {
        color: theme.palette.common.black,
      },
      '& .MuiMenuItem-root': {
        padding: 15,
      },
    },
    divider: {
      margin: '15px auto',
    },
    menuIconButton: {
      [theme.breakpoints.down('md')]: {
        display: 'none',
      },
    },
    mobileInfoContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'start',
    },
    mobileRole: {
      fontFamily: 'Roboto',
      fontSize: 12,
    },
    mobileUsername: {
      fontFamily: 'Roboto',
      fontSize: 16,
      fontWeight: 600,
    },
  }),
);

export default useStyles;
