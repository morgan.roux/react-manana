import React, { FC } from 'react';
import useStyles from './styles';
import { AWS_HOST } from '../../../utils/s3urls';
import { Avatar, Box, Button, ButtonProps, Typography, Hidden } from '@material-ui/core';
import { stringToAvatar } from '../../../utils/Helpers';
import { AccountCircle, ArrowDropDown } from '@material-ui/icons';
import { getUser } from '../../../services/LocalStorage';
import { upperFirst } from 'lodash';

interface UserInfoProps {
  arrowDropDown?: boolean;
  withUserName?: boolean;
}

const UserInfo: FC<UserInfoProps & ButtonProps> = ({ arrowDropDown, withUserName, ...props }) => {
  const classes = useStyles({});
  const user = getUser();
  const userName = user && user.userName;
  const role = user?.role?.nom;
  const userPhoto =
    user && user.userPhoto && user.userPhoto.fichier && user.userPhoto.fichier.chemin;
  const userRoleName = upperFirst(user && user.role && user.role.nom.toLowerCase());

  return (
    <Button {...props} color="inherit" style={{ textTransform: 'initial' }}>
      {userPhoto ? (
        <img src={`${AWS_HOST}/${userPhoto}`} alt="avatar" className={classes.avatar} />
      ) : userName ? (
        <Avatar className={classes.avatar}>{stringToAvatar(userName)}</Avatar>
      ) : (
        <AccountCircle />
      )}
      <Hidden mdDown={true}>
        <Box mr={2}>
          {userName && <Typography align="center">{userName}</Typography>}
          {userRoleName && (
            <Typography className={classes.role} align="center">
              {userRoleName}
            </Typography>
          )}
        </Box>
      </Hidden>
      {withUserName && (
        <Box className={classes.mobileInfoContainer}>
          <Typography className={classes.mobileUsername} align="center">{userName}</Typography>
          <Typography className={classes.mobileRole} align="center">{role}</Typography>
        </Box>
      )}
      {arrowDropDown && <ArrowDropDown />}
    </Button>
  );
};

export default UserInfo;
