import React, { FC } from 'react';
import {
  NOTIFICATIONS,
  NOTIFICATIONSVariables,
  NOTIFICATIONS_notifications_data,
} from '../../../graphql/Notification/types/NOTIFICATIONS';
import Box from '@material-ui/core/Box';
import classNames from 'classnames';
import { useStyles } from '../styles';
import Typography from '@material-ui/core/Typography';
import { Avatar, ListItem as MaterialUIListItem } from '@material-ui/core';
// import IconButton from '@material-ui/core/IconButton';
// import ImgAvatarExemple from '../../../assets/img/img_avatar.png';
// import DeleteIcon from '@material-ui/icons/Delete';
// import ChatBubbleIcon from '@material-ui/icons/ChatBubble';
import { useMutation } from '@apollo/client';
import { GET_NOTIFICATIONS } from '../../../graphql/Notification/query';
import {
  MARK_NOTIFICATION_AS_SEEN,
  MARK_NOTIFICATION_AS_SEENVariables,
} from '../../../graphql/Notification/types/MARK_NOTIFICATION_AS_SEEN';
import { DO_MARK_NOTIFICATION_AS_SEEN } from '../../../graphql/Notification/mutation';
import moment from 'moment';
import 'moment/locale/fr';
import { nl2br } from '../../../services/Html';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { getUser } from '../../../services/LocalStorage';
import { ME_me } from '../../../graphql/Authentication/types/ME';
import { stringToAvatar } from '../../../utils/Helpers';
moment.locale('fr');

interface ListItemProps {
  notification: NOTIFICATIONS_notifications_data;
}

const ListItem: FC<ListItemProps & RouteComponentProps> = ({ notification, history }) => {
  const classes = useStyles({});
  const user: ME_me = getUser();
  const userName: string | null = user && user.userName;
  const userImage: string | null =
    user && user.userPhoto && user.userPhoto.fichier && user.userPhoto.fichier.publicUrl;

  let notificationMsg: string | null = null;

  if (notification.targetName) {
    switch (notification.type) {
      case 'PATENAIRE_ADDED':
        notificationMsg = `Un nouveau partenaire <b>${notification.targetName}</b> vient d'être ajoutée`;
        break;
      case 'ACTUALITE_ADDED':
        notificationMsg = `Une nouvelle actualité <b>${notification.targetName}</b> vient d'être ajoutée`;
        break;
      case 'TODO_ASSIGN':
        notificationMsg = `On vient de vous assigner une tâche dans le projet </b>${notification.targetName}</b>`;
        break;
      default:
        break;
    }
  }

  const [markNotificationAsSeen] = useMutation<
    MARK_NOTIFICATION_AS_SEEN,
    MARK_NOTIFICATION_AS_SEENVariables
  >(DO_MARK_NOTIFICATION_AS_SEEN, {
    update: (cache, { data }) => {
      if (data && data.markNotificationAsSeen) {
        const query = cache.readQuery<NOTIFICATIONS, NOTIFICATIONSVariables>({
          query: GET_NOTIFICATIONS,
          variables: { first: 10, skip: 0 },
        });
        if (query && query.notifications && query.notifications.data) {
          cache.writeQuery<NOTIFICATIONS, NOTIFICATIONSVariables>({
            query: GET_NOTIFICATIONS,
            data: {
              notifications: {
                __typename: 'NotificationResult',
                total: query.notifications.total,
                notSeen: query.notifications.notSeen - 1,
                data: query.notifications.data,
              },
            },
            variables: { first: 10, skip: 0 },
          });
        }
      }
    },
  });

  const clickNotification = () => {
    if (notification.seen === false && notification.type !== 'ACTUALITE_ADDED') {
      markNotificationAsSeen({ variables: { id: notification.id, seen: true } });
    }

    // Go to actualite

    if (notification.type === 'ACTUALITE_ADDED' && notification.targetId) {
      history.push(`/actualite/${notification.targetId}`);
    }
    // GO to Todo
    if (notification.type === 'TODO_ASSIGN' && notification.targetId) {
      history.push(`/project/${notification.targetId}/tasks`);
    }
  };

  return (
    <MaterialUIListItem
      button={true}
      className={classNames(
        classes.listItemNotificationFullPage,
        notification.seen === false ? classes.notSeen : '',
      )}
      onClick={clickNotification}
    >
      <Box
        justifyContent="space-betwwen"
        className={classNames(classes.row, classes.contentNotificationToogleFullPage)}
      >
        <Box
          className={classNames(classes.row, classes.contentNotificationToogleFullPage)}
          justifyContent="start"
        >
          <Box position="relative">
            {userImage && (
              <Avatar alt="Remy Sharp" src={userImage} className={classes.smallAvatar} />
            )}
            {!userImage && (
              <Avatar className={classes.smallAvatar}>{stringToAvatar(userName || '')}</Avatar>
            )}
            {/* <Box position="absolute" className={classes.iconAvatarFullPage}>
              <ChatBubbleIcon
                className={classNames(classes.iconColorChat, classes.iconAvatarSmall)}
              />
            </Box> */}
          </Box>

          <Box justifyContent="space-between" className={classes.col}>
            <Box
              className={classNames(classes.row, classes.textLimitFullPage)}
              flexWrap="wrap"
              alignItems="center"
              justifyContent="start"
            >
              {/* TODO */}
              <Typography className={classes.nameNotification}>
                {notification.from && notification.from ? notification.from.userName : ''}
              </Typography>
              <Typography
                className={classes.pNotification}
                dangerouslySetInnerHTML={
                  { __html: nl2br(notificationMsg || notification.message || '') } as any
                }
              />
            </Box>
            <Box>
              <Typography className={classes.momentNotification}>
                {moment(notification.dateCreation).fromNow()} &bull;{' '}
                {notification.seen ? 'Lu' : 'Non lu'}
              </Typography>
            </Box>
          </Box>
        </Box>

        {/* <IconButton
          className={classNames(classes.iconActionNotif)}
          aria-label="Open drawer"
          edge="start"
        >
          <DeleteIcon />
        </IconButton> */}
      </Box>
    </MaterialUIListItem>
  );
};

export default withRouter(ListItem);
