import React, { FC } from 'react';
import { Popper, Grow, Paper, Box, MenuList, Link, PopperProps } from '@material-ui/core';
import NotificationItem from './NotificationItem';
import { useStyles } from '../styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { LoaderSmall } from '../../Dashboard/Content/Loader';
import { NOTIFICATIONS_notifications } from '../../../graphql/Notification/types/NOTIFICATIONS';
import { Waypoint } from 'react-waypoint';

interface NotificationMenuProps {
  handleClose: (event: React.MouseEvent<EventTarget>) => void;
  notifications: NOTIFICATIONS_notifications | null | undefined;
  loading: boolean;
  fetchMoreNotifications: () => void;
}

const NotificationMenu: FC<{ popperProps: PopperProps } & NotificationMenuProps &
  RouteComponentProps<any, any, any>> = ({
  handleClose,
  history,
  notifications,
  loading,
  fetchMoreNotifications,
  popperProps,
}) => {
  const { anchorEl } = popperProps;
  const position = anchorEl ? (anchorEl as HTMLElement).getBoundingClientRect() : null;
  const classes = useStyles({ position });

  const gotToNotificationPage = () => {
    history.push('/notifications');
  };

  const renderNotificationItem = () => {
    return (
      handleClose &&
      notifications &&
      notifications.data &&
      notifications.data
        .filter(notificationItem => notificationItem && notificationItem.seen === false)
        .map(
          (notification, index) =>
            notification && (
              <>
                <NotificationItem
                  handleClose={handleClose}
                  notification={notification}
                  key={`notif_${index}`}
                />
                {notifications &&
                  notifications.data &&
                  index === notifications.data.length - 1 &&
                  notifications.data.length !== notifications.total && (
                    <Waypoint onEnter={fetchMoreNotifications} />
                  )}
              </>
            ),
        )
    );
  };

  return (
    <Popper {...popperProps} style={{ zIndex: 10000 }}>
      <Paper className={classes.paper}>
        <MenuList className={classes.contentListMenu} id="menu-list-grow">
          {renderNotificationItem()}
        </MenuList>

        {loading && !notifications && (
          <Box textAlign="center" padding="12px 24px">
            <LoaderSmall />
          </Box>
        )}

        <Box className={classes.voirTout} textAlign="center" padding="12px 24px">
          {notifications && notifications.data && notifications.data.length > 0 ? (
            <Link onClick={gotToNotificationPage} className={classes.linkNotificationFullPAge}>
              Voir toutes les notifications
            </Link>
          ) : (
            <div>Aucune notification</div>
          )}
        </Box>
      </Paper>
    </Popper>
  );
};

export default withRouter(NotificationMenu);
