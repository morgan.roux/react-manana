/**
 * Actualite
 */
export const ACTU_VIEW_CODE = '01_01';
export const ACTU_CREATE_CODE = '01_02';
export const ACTU_EDIT_CODE = '01_03';
export const ACTU_DELETE_CODE = '01_04';
export const ACTU_MARK_SEEN_AND_NOT_SEEN_CODE = '01_05';
export const ACTU_LIKE_CODE = '01_06';
export const ACTU_COMMENT_CODE = '01_07';

/**
 * Operation commerciale
 */
export const OC_VIEW_CODE = '02_01';
export const OC_CREATE_CODE = '02_02';
export const OC_EDIT_CODE = '02_03';
export const OC_DELETE_CODE = '02_04';
export const OC_MARK_SEEN_AND_NOT_SEEN_CODE = '02_05';
export const OC_LIKE_CODE = '02_06';
export const OC_COMMENT_CODE = '02_07';
export const OC_VIEW_BUSINESS_CODE = '02_08';
export const OC_VIEW_FEEDBACK_CODE = '02_09';

/**
 * Catalogue des produits
 */
export const CAT_VIEW_LIST_CODE = '03_01';
export const CAT_VIEW_DETAILS_CODE = '03_02';
export const CAT_ADD_TO_CART_CODE = '03_03';
export const CAT_LIKE_ART_CODE = '03_04';

/**
 * Commande
 */
export const CMD_VIEW_LIST_CODE = '04_01';
export const CMD_VIEW_DETAILS_CODE = '04_02';
export const CMD_VALIDATE_CODE = '04_03';

/**
 * Cart (Panier)
 */
export const CART_REMOVE_PRODUCT_CODE = '05_01';
export const CART_EMPTY_CODE = '05_02';

/**
 * Titulaire
 */
export const TIT_VIEW_LIST_CODE = '06_01';
export const TIT_INIT_PWD_CODE = '06_02';
export const TIT_ACTIVATE_DEACTIVATE_USER_CODE = '06_03';
export const TIT_ADD_USER_CODE = '06_04';
export const TIT_EDIT_USER_CODE = '06_05';
export const TIT_VIEW_DETAILS_CODE = '06_06';
export const TIT_VIEW_HISTORY_CODE = '06_07';

/**
 * Président des régions
 */
export const PDR_VIEW_LIST_CODE = '08_01';
export const PDR_INIT_PWD_CODE = '08_02';
export const PDR_ACTIVATE_DEACTIVATE_USER_CODE = '08_03';
export const PDR_ADD_USER_CODE = '08_04';
export const PDR_EDIT_USER_CODE = '08_05';
export const PDR_VIEW_DETAILS_CODE = '08_06';
export const PDR_VIEW_HISTORY_CODE = '08_07';

/**
 * Labo partenaire
 */
export const LABO_VIEW_LIST_CODE = '09_01';
export const LABO_INIT_PWD_CODE = '09_02';
export const LABO_ACTIVATE_DEACTIVATE_USER_CODE = '09_03';
export const LABO_ADD_USER_CODE = '09_04';
export const LABO_EDIT_USER_CODE = '09_05';
export const LABO_VIEW_DETAILS_CODE = '09_06';
export const LABO_VIEW_HISTORY_CODE = '09_07';

/**
 * Personnel pharmacie
 */
export const PPERSO_VIEW_LIST_CODE = '10_01';
export const PPERSO_INIT_PWD_CODE = '10_02';
export const PPERSO_ACTIVATE_DEACTIVATE_USER_CODE = '10_03';
export const PPERSO_ADD_USER_CODE = '10_04';
export const PPERSO_EDIT_USER_CODE = '10_05';
export const PPERSO_VIEW_DETAILS_CODE = '10_06';
export const PPERSO_VIEW_HISTORY_CODE = '10_07';

/**
 * Pharmacie
 */
export const PHARMA_VIEW_LIST_CODE = '11_01';
export const PHARMA_EDIT_CODE = '11_02';

/**
 * Partenaire de service
 */
export const SERV_VIEW_LIST_CODE = '12_01';
export const SERV_INIT_PWD_CODE = '12_02';
export const SERV_ACTIVATE_DEACTIVATE_USER_CODE = '12_03';
export const SERV_ADD_USER_CODE = '12_04';
export const SERV_EDIT_USER_CODE = '12_05';
export const SERV_VIEW_DETAILS_CODE = '12_06';
export const SERV_VIEW_HISTORY_CODE = '12_07';

/**
 * Groupement (Option groupement)
 */
export const GPR_VIEW_OPT_CODE = '13_01';
export const GPR_EDIT_OPT_CODE = '13_02';

/**
 * Publicité
 */
export const PUB_VIEW_LIST_CODE = '14_01';
export const PUB_ADD_CODE = '14_02';
export const PUB_EDIT_CODE = '14_03';
export const PUB_DELETE_CODE = '14_04';

/**
 * Marché
 */
export const MARCHE_VIEW_LIST_CODE = '15_01';
export const MARCHE_ADD_CODE = '15_02';
export const MARCHE_EDIT_CODE = '15_03';
export const MARCHE_DELETE_CODE = '15_04';

/**
 * Article (Produit)
 */
export const PRODUCT_ADD_CODE = '16_01';
export const PRODUCT_EDIT_CODE = '16_02';
export const PRODUCT_DELETE_CODE = '16_03';
export const PRODUCT_VIEW_SALE_HISTORY_CODE = '16_04';

/**
 * Promotion
 */
export const PROMO_VIEW_LIST_CODE = '17_01';
export const PROMO_ADD_CODE = '17_02';
export const PROMO_EDIT_CODE = '17_03';
export const PROMO_DELETE_CODE = '17_04';

/**
 * Pilotage
 */
export const PILOTAGE_BUSINESS_CODE = '18_01';
export const PILOTAGE_FEEDBACK_CODE = '18_02';
export const PILOTAGE_CHANGE_BY_OP_CODE = '18_03';
export const PILOTAGE_CHANGE_BY_PHARMA_CODE = '18_04';

/**
 * Theme
 */
export const THEME_CHANGE_CODE = '19_01';

/**
 * Indexing
 */
export const INDEXING_CODE = '20_01';

/**
 * Releve temperature
 */

 /**
  * Ged
  */
 export const GED_ADD_DOCUMENT = '22_01';
 export const GED_VALIDATE_DOCUMENT = '22_02';

export const RELEVE_TEMPERATURE_VIEW_CODE = '21_01';
