export const PUB_BASE_URL = '/db/espace-publicitaire';

export const PUB_EMPLACEMENT_LIST = [
  { id: 1, value: 'Emplacement 1' },
  { id: 2, value: 'Emplacement 2' },
  { id: 3, value: 'Emplacement 3' },
  { id: 4, value: 'Emplacement 4' },
  { id: 5, value: 'Emplacement 5' },
  { id: 6, value: 'Emplacement 6' },
  { id: 7, value: 'Emplacement 7' },
  { id: 8, value: 'Emplacement 8' },
  { id: 9, value: 'Emplacement 9' },
  { id: 10, value: 'Emplacement 10' },
  { id: 11, value: 'Emplacement 11' },
  { id: 12, value: 'Emplacement 12' },
];

export const PUB_DELAIS_LIST = [
  { id: 3, value: '3' },
  { id: 5, value: '5' },
  { id: 10, value: '10' },
  { id: 20, value: '20' },
  { id: 30, value: '30' },
  { id: 40, value: '40' },
  { id: 50, value: '50' },
  { id: 60, value: '60' },
];
