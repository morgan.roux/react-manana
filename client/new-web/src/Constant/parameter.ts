import { availableThemes } from '../Theme';

export const CODE_LIGNE_TABLEAU = '0048';
export const CODE_FRANCO_PORT = '0024';
export const CODE_THEME = '0054';
export const CODE_NOTIF_TAGS = '0026';
export const CODE_NOTIF_TIMERS = '0027';
export const CODE_PUB_DELAIS = '0104';

export const NOTIF_TAGS = [
  { value: 'all', libelle: 'Tout' },
  { value: 'messageOnly', libelle: 'Messages seulement' },
];

export const NOTIF_TIMERS = [
  { value: '2', libelle: 'Après 2 minutes' },
  { value: '5', libelle: 'Après 5 minutes' },
  { value: '10', libelle: 'Après 10 minutes' },
];

const { angel, caramel, leaf, magic, onyx, raspberry, sky } = availableThemes as any;

export const THEMES_LIST = [
  {
    value: 'onyx',
    libelle: 'Onyx (Par défaut)',
    color: {
      primary: onyx.palette && onyx.palette.primary && onyx.palette.primary.main,
      secondary: onyx.palette && onyx.palette.secondary && onyx.palette.secondary.main,
    },
  },
  {
    value: 'raspberry',
    libelle: 'Raspberry',
    color: {
      primary: raspberry.palette && raspberry.palette.primary && raspberry.palette.primary.main,
      secondary:
        raspberry.palette && raspberry.palette.secondary && raspberry.palette.secondary.main,
    },
  },
  {
    value: 'caramel',
    libelle: 'Caramel',
    color: {
      primary: caramel.palette && caramel.palette.primary && caramel.palette.primary.main,
      secondary: caramel.palette && caramel.palette.secondary && caramel.palette.secondary.main,
    },
  },
  {
    value: 'sky',
    libelle: 'Sky',
    color: {
      primary: sky.palette && sky.palette.primary && sky.palette.primary.main,
      secondary: sky.palette && sky.palette.secondary && sky.palette.secondary.main,
    },
  },
  {
    value: 'leaf',
    libelle: 'Leaf',
    color: {
      primary: leaf.palette && leaf.palette.primary && leaf.palette.primary.main,
      secondary: leaf.palette && leaf.palette.secondary && leaf.palette.secondary.main,
    },
  },
  {
    value: 'magic',
    libelle: 'Magic',
    color: {
      primary: magic.palette && magic.palette.primary && magic.palette.primary.main,
      secondary: magic.palette && magic.palette.secondary && magic.palette.secondary.main,
    },
  },
  {
    value: 'angel',
    libelle: 'Angel',
    color: {
      primary: angel.palette && angel.palette.primary && angel.palette.primary.main,
      secondary: angel.palette && angel.palette.secondary && angel.palette.secondary.main,
    },
  },
];
