export const nl2br = (str: string | null): string | null => {
  return str ? str.replace(new RegExp('\n', 'g'), '<br/>') : str;
};
