import { getUser } from './LocalStorage';

export const permitRoles = (roles: string[]) => {
  const user = getUser();
  if (user && user.role) {
    if (roles.includes(user.role.code)) return true;
  }
  return false;
};
