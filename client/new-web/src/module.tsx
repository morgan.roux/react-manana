import React from 'react';
// @ts-ignore
import loadable from '@loadable/component';
import SmallLoading from './components/Loading/SmallLoading';
import ActualiteIcon from './assets/img/service_illustration/actualites.png'

const  Actualite = loadable(()=> import('./components/adapter/Actualite'), {
  fallback: <SmallLoading/>,

} )  



const webModule: any = {
  features: [
    {
      id: 'FEATURE_ACTUALITE',
      location: 'ACCUEIL',
      name: 'Actualités',
      to: '/actualites',
      icon: ActualiteIcon,
      activationParameters: {
        groupement: '0207',
        pharmacie: '0702',
      },
      preferredOrder: 10,
      attachTo: 'MAIN',
    },
  ],

  routes: [
    {
      path: '/actualites',
      component: Actualite,
      exact: true,
      attachTo: 'MAIN',
    },
  ],
};

export default webModule;
