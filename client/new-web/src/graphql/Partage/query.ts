import gql from 'graphql-tag';

export const GET_SEARCH_CUSTUM_PARTAGE = gql`
  query SEARCH_CUSTOM_CUSTUM_PARTAGE($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Partage {
          id
          dateCreation
          typePartage
        }
      }
    }
  }
`;
