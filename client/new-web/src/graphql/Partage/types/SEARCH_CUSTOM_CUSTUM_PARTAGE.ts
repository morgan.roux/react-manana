/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PartageType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CUSTUM_PARTAGE
// ====================================================

export interface SEARCH_CUSTOM_CUSTUM_PARTAGE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_CUSTOM_CUSTUM_PARTAGE_search_data_Partage {
  __typename: "Partage";
  id: string;
  dateCreation: any;
  typePartage: PartageType;
}

export type SEARCH_CUSTOM_CUSTUM_PARTAGE_search_data = SEARCH_CUSTOM_CUSTUM_PARTAGE_search_data_Action | SEARCH_CUSTOM_CUSTUM_PARTAGE_search_data_Partage;

export interface SEARCH_CUSTOM_CUSTUM_PARTAGE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CUSTUM_PARTAGE_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CUSTUM_PARTAGE {
  search: SEARCH_CUSTOM_CUSTUM_PARTAGE_search | null;
}

export interface SEARCH_CUSTOM_CUSTUM_PARTAGEVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
