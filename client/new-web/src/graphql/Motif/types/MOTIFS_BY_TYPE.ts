/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeMotif } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: MOTIFS_BY_TYPE
// ====================================================

export interface MOTIFS_BY_TYPE_motifs {
  __typename: "Motif";
  id: string;
  libelle: string | null;
}

export interface MOTIFS_BY_TYPE {
  motifs: (MOTIFS_BY_TYPE_motifs | null)[] | null;
}

export interface MOTIFS_BY_TYPEVariables {
  type?: TypeMotif | null;
}
