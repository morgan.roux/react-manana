import gql from 'graphql-tag';

export const GET_MOTIFS_BY_TYPE = gql`
  query MOTIFS_BY_TYPE($type: TypeMotif) {
    motifs(type: $type) {
      id
      libelle
    }
  }
`;
