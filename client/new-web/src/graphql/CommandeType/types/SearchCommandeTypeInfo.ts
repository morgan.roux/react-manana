/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SearchCommandeTypeInfo
// ====================================================

export interface SearchCommandeTypeInfo {
  __typename: "CommandeType";
  type: string;
  id: string;
  libelle: string | null;
  code: string | null;
  countOperationsCommercials: number | null;
}
