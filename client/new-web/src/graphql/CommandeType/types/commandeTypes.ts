/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: commandeTypes
// ====================================================

export interface commandeTypes_commandeTypes {
  __typename: "CommandeType";
  id: string;
  libelle: string | null;
  code: string | null;
  countOperationsCommercials: number | null;
}

export interface commandeTypes {
  commandeTypes: (commandeTypes_commandeTypes | null)[] | null;
}
