import gql from 'graphql-tag';

export const SEARCH_COMMANDE_TYPE_INFO = gql`
  fragment SearchCommandeTypeInfo on CommandeType {
    type
    id
    libelle
    code
    countOperationsCommercials
  }
`;
