/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_EXTERNAL_MAPPING
// ====================================================

export interface DELETE_EXTERNAL_MAPPING_deleteExternalMapping_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface DELETE_EXTERNAL_MAPPING_deleteExternalMapping_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface DELETE_EXTERNAL_MAPPING_deleteExternalMapping_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: DELETE_EXTERNAL_MAPPING_deleteExternalMapping_user_userPhoto_fichier | null;
}

export interface DELETE_EXTERNAL_MAPPING_deleteExternalMapping_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface DELETE_EXTERNAL_MAPPING_deleteExternalMapping_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: DELETE_EXTERNAL_MAPPING_deleteExternalMapping_user_role | null;
  userPhoto: DELETE_EXTERNAL_MAPPING_deleteExternalMapping_user_userPhoto | null;
  pharmacie: DELETE_EXTERNAL_MAPPING_deleteExternalMapping_user_pharmacie | null;
}

export interface DELETE_EXTERNAL_MAPPING_deleteExternalMapping {
  __typename: "ExternalMapping";
  id: string;
  idExternaluser: string | null;
  idClient: string | null;
  password: string | null;
  user: DELETE_EXTERNAL_MAPPING_deleteExternalMapping_user | null;
}

export interface DELETE_EXTERNAL_MAPPING {
  deleteExternalMapping: DELETE_EXTERNAL_MAPPING_deleteExternalMapping | null;
}

export interface DELETE_EXTERNAL_MAPPINGVariables {
  id: string;
}
