import gql from 'graphql-tag';
import { EXTERNAL_MAPPING_INFO } from './fragment';

export const DO_CREATE_EXTERNAL_MAPPING = gql`
  mutation CREATE_EXTERNAL_MAPPING(
    $extenalMappingInput: ExternalMappingInput
    ) {
    createExternalMapping(
        extenalMappingInput: $extenalMappingInput
        ) {
          ...ExternalMappingInfo
    }
  }
  ${EXTERNAL_MAPPING_INFO}
`;

export const DO_UPDATE_EXTERNAL_MAPPING = gql`
  mutation UPDATE_EXTERNAL_MAPPING(
    $id: ID!,
    $extenalMappingInput: ExternalMappingInput
    ) {
    updateExternalMapping(
        id: $id,
        extenalMappingInput: $extenalMappingInput,
        ) {
          ...ExternalMappingInfo
    }
  }
  ${EXTERNAL_MAPPING_INFO}
`;

export const DO_DELETE_EXTERNAL_MAPPING = gql`
  mutation DELETE_EXTERNAL_MAPPING(
    $id: ID!
    ) {
    deleteExternalMapping(
        id: $id
        ) {
          ...ExternalMappingInfo
    }
  }
  ${EXTERNAL_MAPPING_INFO}
`;