import gql from 'graphql-tag';

export const ACTE_INFO = gql`
  fragment ActeInfo on Acte {
    type
    id
    codeActe
    libelleActe
    codeMaj
    dateCreation
    dateModification
  }
`;
