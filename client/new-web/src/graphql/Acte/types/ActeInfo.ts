/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ActeInfo
// ====================================================

export interface ActeInfo {
  __typename: "Acte";
  type: string;
  id: string;
  codeActe: number | null;
  libelleActe: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}
