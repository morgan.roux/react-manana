/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ACTES
// ====================================================

export interface ACTES_actes {
  __typename: "Acte";
  id: string;
  codeActe: number | null;
  libelleActe: string | null;
}

export interface ACTES {
  actes: (ACTES_actes | null)[] | null;
}
