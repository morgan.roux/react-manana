/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TrActeInfo
// ====================================================

export interface TrActeInfo {
  __typename: "TrActe";
  type: string;
  id: string;
  codeActe: number | null;
  libelle: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}
