import gql from 'graphql-tag';
import { TODO_SECTION_INFO_FRAGMENT } from './fragment';

export const DO_CREATE_UPDATE_TODO_SECTION = gql`
  mutation CREATE_UPDATE_TODO_SECTION($input: TodoSectionInput!) {
    createUpdateTodoSection(input: $input) {
      ...TodoSectionInfo
    }
  }
  ${TODO_SECTION_INFO_FRAGMENT}
`;

export const DO_DELETE_TODO_SECTION = gql`
  mutation DELETE_TODO_SECTION($ids: [ID!]!) {
    softDeleteTodoSections(ids: $ids) {
      ...TodoSectionInfo
    }
  }
  ${TODO_SECTION_INFO_FRAGMENT}
`;
