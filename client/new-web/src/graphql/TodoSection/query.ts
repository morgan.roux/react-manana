import gql from 'graphql-tag';
import { TODO_SECTION_INFO_FRAGMENT } from './fragment';
export const GET_SEARCH_TODO_SECTION = gql`
  query SEARCH_TODO_SECTION(
    $type: [String]
    $query: JSON
    $filterBy: JSON
    $sortBy: JSON
    $skip: Int
    $take: Int
  ) {
    search(
      type: $type
      query: $query
      filterBy: $filterBy
      sortBy: $sortBy
      skip: $skip
      take: $take
    ) {
      total
      data {
        ... on TodoSection {
          ...TodoSectionInfo
        }
      }
    }
  }
  ${TODO_SECTION_INFO_FRAGMENT}
`;
