import gql from 'graphql-tag';

const DO_ADD_TASK = gql`
  mutation ADD_TASK_TO_PROJECT($input: ActionInput!) {
    createUpdateAction(input: $input) {
      id
      description
      isInInbox
      isInInboxTeam
      dateDebut
      dateFin
      project {
        id
      }
    }
  }
`;

export { DO_ADD_TASK };
