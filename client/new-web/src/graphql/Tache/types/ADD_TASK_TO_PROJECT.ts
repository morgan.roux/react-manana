/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ActionInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: ADD_TASK_TO_PROJECT
// ====================================================

export interface ADD_TASK_TO_PROJECT_createUpdateAction_project {
  __typename: "Project";
  id: string;
}

export interface ADD_TASK_TO_PROJECT_createUpdateAction {
  __typename: "Action";
  id: string;
  description: string;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  dateDebut: any | null;
  dateFin: any | null;
  project: ADD_TASK_TO_PROJECT_createUpdateAction_project | null;
}

export interface ADD_TASK_TO_PROJECT {
  createUpdateAction: ADD_TASK_TO_PROJECT_createUpdateAction;
}

export interface ADD_TASK_TO_PROJECTVariables {
  input: ActionInput;
}
