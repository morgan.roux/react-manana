import gql from 'graphql-tag';
import { PRESTATAIRE_PHARMACIE_INFO_FRAGMENT } from '../PrestatairePharmacie/fragment';

export const AUTOMATE_INFO_FRAGMENT = gql`
  fragment AutomateInfo on Automate {
    id
    modeleAutomate
    dateCommercialisation
    prestataire {
      ...PrestatairePharmacieInfo
    }
  }
  ${PRESTATAIRE_PHARMACIE_INFO_FRAGMENT}
`;
