import gql from 'graphql-tag';

export const GET_AUTOMATES_WITH_MINIM_INFO = gql`
  query AUTOMATES_WITH_MINIM_INFO {
    automates {
      id
      modeleAutomate
    }
  }
`;
