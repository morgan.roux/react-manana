/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: MessagerieHistoInfo
// ====================================================

export interface MessagerieHistoInfo_userRecepteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MessagerieHistoInfo_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MessagerieHistoInfo {
  __typename: "MessagerieHisto";
  id: string;
  dateHeureLecture: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  userRecepteur: MessagerieHistoInfo_userRecepteur | null;
  userCreation: MessagerieHistoInfo_userCreation | null;
}
