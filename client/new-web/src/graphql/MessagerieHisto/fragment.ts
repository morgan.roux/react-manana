import gql from 'graphql-tag';

export const MESSAGERIE_HISTO_INFO_FRAGMENT = gql`
  fragment MessagerieHistoInfo on MessagerieHisto {
    id
    dateHeureLecture
    dateCreation
    dateModification
    userRecepteur {
      id
      email
      login
      userName
    }
    userCreation {
      id
      email
      login
      userName
    }
  }
`;
