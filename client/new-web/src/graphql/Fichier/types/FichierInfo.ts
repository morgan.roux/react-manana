/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: FichierInfo
// ====================================================

export interface FichierInfo {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}
