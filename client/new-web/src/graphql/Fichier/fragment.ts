import gql from 'graphql-tag';

export const FICHIER_FRAGMENT = gql`
  fragment FichierInfo on Fichier {
    id
    chemin
    nomOriginal
    type
    urlPresigned
    publicUrl
    dateCreation
    dateModification
  }
`;
