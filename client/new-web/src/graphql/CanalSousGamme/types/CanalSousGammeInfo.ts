/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: CanalSousGammeInfo
// ====================================================

export interface CanalSousGammeInfo_gammeCommercial {
  __typename: "CanalGamme";
  id: string;
  codeGamme: number | null;
  libelle: string | null;
  estDefaut: number | null;
}

export interface CanalSousGammeInfo {
  __typename: "CanalSousGamme";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
  estDefaut: number | null;
  gammeCommercial: CanalSousGammeInfo_gammeCommercial | null;
  countCanalArticles: number | null;
}
