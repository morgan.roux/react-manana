import gql from 'graphql-tag';
import { USER_SMYLEY_INFO_FRAGMENT } from './fragment';

export const DO_CREATE_USER_SMYLEY = gql`
  mutation CREATE_USER_SMYLEY($codeItem: String!, $idSource: String!, $idSmyley: String!) {
    createUserSmyley(codeItem: $codeItem, idSource: $idSource, idSmyley: $idSmyley) {
      ...UserSmyleyInfo
    }
  }
  ${USER_SMYLEY_INFO_FRAGMENT}
`;
