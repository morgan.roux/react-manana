/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: UserSmyleyInfo
// ====================================================

export interface UserSmyleyInfo_item {
  __typename: "Item";
  id: string;
}

export interface UserSmyleyInfo_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface UserSmyleyInfo_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface UserSmyleyInfo_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: UserSmyleyInfo_user_userPhoto_fichier | null;
}

export interface UserSmyleyInfo_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface UserSmyleyInfo_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: UserSmyleyInfo_user_role | null;
  userPhoto: UserSmyleyInfo_user_userPhoto | null;
  pharmacie: UserSmyleyInfo_user_pharmacie | null;
}

export interface UserSmyleyInfo_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface UserSmyleyInfo_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: UserSmyleyInfo_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface UserSmyleyInfo_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: UserSmyleyInfo_smyley_groupement_defaultPharmacie_departement | null;
}

export interface UserSmyleyInfo_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface UserSmyleyInfo_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: UserSmyleyInfo_smyley_groupement_groupementLogo_fichier | null;
}

export interface UserSmyleyInfo_smyley_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: UserSmyleyInfo_smyley_groupement_defaultPharmacie | null;
  groupementLogo: UserSmyleyInfo_smyley_groupement_groupementLogo | null;
}

export interface UserSmyleyInfo_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: UserSmyleyInfo_smyley_groupement | null;
}

export interface UserSmyleyInfo {
  __typename: "UserSmyley";
  id: string;
  item: UserSmyleyInfo_item | null;
  idSource: string | null;
  user: UserSmyleyInfo_user | null;
  smyley: UserSmyleyInfo_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}
