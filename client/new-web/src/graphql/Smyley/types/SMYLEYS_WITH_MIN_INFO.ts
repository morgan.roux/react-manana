/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SMYLEYS_WITH_MIN_INFO
// ====================================================

export interface SMYLEYS_WITH_MIN_INFO_smyleys {
  __typename: "Smyley";
  id: string;
  nom: string;
  photo: string;
}

export interface SMYLEYS_WITH_MIN_INFO {
  smyleys: (SMYLEYS_WITH_MIN_INFO_smyleys | null)[] | null;
}

export interface SMYLEYS_WITH_MIN_INFOVariables {
  idGroupement: string;
}
