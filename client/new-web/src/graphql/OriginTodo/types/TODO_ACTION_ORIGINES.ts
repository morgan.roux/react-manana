/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: TODO_ACTION_ORIGINES
// ====================================================

export interface TODO_ACTION_ORIGINES_todoActionOrigines {
  __typename: "TodoActionOrigine";
  libelle: string;
  id: string;
  code: string;
}

export interface TODO_ACTION_ORIGINES {
  todoActionOrigines: TODO_ACTION_ORIGINES_todoActionOrigines[];
}

export interface TODO_ACTION_ORIGINESVariables {
  isRemoved?: boolean | null;
}
