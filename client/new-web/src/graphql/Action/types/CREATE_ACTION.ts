/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ActionInput, ActionStatus, TypeProject } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_ACTION
// ====================================================

export interface CREATE_ACTION_createUpdateAction_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface CREATE_ACTION_createUpdateAction_origine {
  __typename: "TodoActionOrigine";
  id: string;
  code: string;
  libelle: string;
}

export interface CREATE_ACTION_createUpdateAction_section {
  __typename: "TodoSection";
  id: string;
  ordre: number | null;
  libelle: string;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
}

export interface CREATE_ACTION_createUpdateAction_actionParent {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  nbComment: number;
}

export interface CREATE_ACTION_createUpdateAction_subActions_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface CREATE_ACTION_createUpdateAction_subActions {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: CREATE_ACTION_createUpdateAction_subActions_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbComment: number;
}

export interface CREATE_ACTION_createUpdateAction_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
}

export interface CREATE_ACTION_createUpdateAction_project {
  __typename: "Project";
  id: string;
  name: string | null;
  typeProject: TypeProject | null;
}

export interface CREATE_ACTION_createUpdateAction_userCreation_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_ACTION_createUpdateAction_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  role: CREATE_ACTION_createUpdateAction_userCreation_role | null;
}

export interface CREATE_ACTION_createUpdateAction_assignedUsers_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CREATE_ACTION_createUpdateAction_assignedUsers_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CREATE_ACTION_createUpdateAction_assignedUsers_userPhoto_fichier | null;
}

export interface CREATE_ACTION_createUpdateAction_assignedUsers {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  userPhoto: CREATE_ACTION_createUpdateAction_assignedUsers_userPhoto | null;
}

export interface CREATE_ACTION_createUpdateAction_etiquettes_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface CREATE_ACTION_createUpdateAction_etiquettes {
  __typename: "TodoEtiquette";
  id: string;
  ordre: number | null;
  nom: string;
  isRemoved: boolean | null;
  couleur: CREATE_ACTION_createUpdateAction_etiquettes_couleur | null;
}

export interface CREATE_ACTION_createUpdateAction {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  _priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: CREATE_ACTION_createUpdateAction_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  idItemAssocie: string | null;
  nbComment: number;
  origine: CREATE_ACTION_createUpdateAction_origine | null;
  section: CREATE_ACTION_createUpdateAction_section | null;
  actionParent: CREATE_ACTION_createUpdateAction_actionParent | null;
  subActions: CREATE_ACTION_createUpdateAction_subActions[] | null;
  item: CREATE_ACTION_createUpdateAction_item | null;
  project: CREATE_ACTION_createUpdateAction_project | null;
  userCreation: CREATE_ACTION_createUpdateAction_userCreation | null;
  assignedUsers: CREATE_ACTION_createUpdateAction_assignedUsers[] | null;
  etiquettes: CREATE_ACTION_createUpdateAction_etiquettes[] | null;
}

export interface CREATE_ACTION {
  createUpdateAction: CREATE_ACTION_createUpdateAction;
}

export interface CREATE_ACTIONVariables {
  input: ActionInput;
}
