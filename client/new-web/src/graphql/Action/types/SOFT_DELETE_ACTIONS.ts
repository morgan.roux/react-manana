/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ActionStatus, TypeProject } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: SOFT_DELETE_ACTIONS
// ====================================================

export interface SOFT_DELETE_ACTIONS_softDeleteActions_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface SOFT_DELETE_ACTIONS_softDeleteActions_origine {
  __typename: "TodoActionOrigine";
  id: string;
  code: string;
  libelle: string;
}

export interface SOFT_DELETE_ACTIONS_softDeleteActions_section {
  __typename: "TodoSection";
  id: string;
  ordre: number | null;
  libelle: string;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
}

export interface SOFT_DELETE_ACTIONS_softDeleteActions_actionParent {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  nbComment: number;
}

export interface SOFT_DELETE_ACTIONS_softDeleteActions_subActions_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface SOFT_DELETE_ACTIONS_softDeleteActions_subActions {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: SOFT_DELETE_ACTIONS_softDeleteActions_subActions_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbComment: number;
}

export interface SOFT_DELETE_ACTIONS_softDeleteActions_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
}

export interface SOFT_DELETE_ACTIONS_softDeleteActions_project {
  __typename: "Project";
  id: string;
  name: string | null;
  typeProject: TypeProject | null;
}

export interface SOFT_DELETE_ACTIONS_softDeleteActions_userCreation_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SOFT_DELETE_ACTIONS_softDeleteActions_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  role: SOFT_DELETE_ACTIONS_softDeleteActions_userCreation_role | null;
}

export interface SOFT_DELETE_ACTIONS_softDeleteActions_assignedUsers_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SOFT_DELETE_ACTIONS_softDeleteActions_assignedUsers_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SOFT_DELETE_ACTIONS_softDeleteActions_assignedUsers_userPhoto_fichier | null;
}

export interface SOFT_DELETE_ACTIONS_softDeleteActions_assignedUsers {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  userPhoto: SOFT_DELETE_ACTIONS_softDeleteActions_assignedUsers_userPhoto | null;
}

export interface SOFT_DELETE_ACTIONS_softDeleteActions_etiquettes_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface SOFT_DELETE_ACTIONS_softDeleteActions_etiquettes {
  __typename: "TodoEtiquette";
  id: string;
  ordre: number | null;
  nom: string;
  isRemoved: boolean | null;
  couleur: SOFT_DELETE_ACTIONS_softDeleteActions_etiquettes_couleur | null;
}

export interface SOFT_DELETE_ACTIONS_softDeleteActions {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  _priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: SOFT_DELETE_ACTIONS_softDeleteActions_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  idItemAssocie: string | null;
  nbComment: number;
  origine: SOFT_DELETE_ACTIONS_softDeleteActions_origine | null;
  section: SOFT_DELETE_ACTIONS_softDeleteActions_section | null;
  actionParent: SOFT_DELETE_ACTIONS_softDeleteActions_actionParent | null;
  subActions: SOFT_DELETE_ACTIONS_softDeleteActions_subActions[] | null;
  item: SOFT_DELETE_ACTIONS_softDeleteActions_item | null;
  project: SOFT_DELETE_ACTIONS_softDeleteActions_project | null;
  userCreation: SOFT_DELETE_ACTIONS_softDeleteActions_userCreation | null;
  assignedUsers: SOFT_DELETE_ACTIONS_softDeleteActions_assignedUsers[] | null;
  etiquettes: SOFT_DELETE_ACTIONS_softDeleteActions_etiquettes[] | null;
}

export interface SOFT_DELETE_ACTIONS {
  softDeleteActions: SOFT_DELETE_ACTIONS_softDeleteActions[];
}

export interface SOFT_DELETE_ACTIONSVariables {
  ids: string[];
}
