/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { AssignOrUnAssignActionInput, ActionStatus, TypeProject } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: ASSIGN_ACTION_TO_USER
// ====================================================

export interface ASSIGN_ACTION_TO_USER_assignActionToUser_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface ASSIGN_ACTION_TO_USER_assignActionToUser_origine {
  __typename: "TodoActionOrigine";
  id: string;
  code: string;
  libelle: string;
}

export interface ASSIGN_ACTION_TO_USER_assignActionToUser_section {
  __typename: "TodoSection";
  id: string;
  ordre: number | null;
  libelle: string;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
}

export interface ASSIGN_ACTION_TO_USER_assignActionToUser_actionParent {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  nbComment: number;
}

export interface ASSIGN_ACTION_TO_USER_assignActionToUser_subActions_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface ASSIGN_ACTION_TO_USER_assignActionToUser_subActions {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: ASSIGN_ACTION_TO_USER_assignActionToUser_subActions_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbComment: number;
}

export interface ASSIGN_ACTION_TO_USER_assignActionToUser_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
}

export interface ASSIGN_ACTION_TO_USER_assignActionToUser_project {
  __typename: "Project";
  id: string;
  name: string | null;
  typeProject: TypeProject | null;
}

export interface ASSIGN_ACTION_TO_USER_assignActionToUser_userCreation_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface ASSIGN_ACTION_TO_USER_assignActionToUser_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  role: ASSIGN_ACTION_TO_USER_assignActionToUser_userCreation_role | null;
}

export interface ASSIGN_ACTION_TO_USER_assignActionToUser_assignedUsers_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface ASSIGN_ACTION_TO_USER_assignActionToUser_assignedUsers_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: ASSIGN_ACTION_TO_USER_assignActionToUser_assignedUsers_userPhoto_fichier | null;
}

export interface ASSIGN_ACTION_TO_USER_assignActionToUser_assignedUsers {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  userPhoto: ASSIGN_ACTION_TO_USER_assignActionToUser_assignedUsers_userPhoto | null;
}

export interface ASSIGN_ACTION_TO_USER_assignActionToUser_etiquettes_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface ASSIGN_ACTION_TO_USER_assignActionToUser_etiquettes {
  __typename: "TodoEtiquette";
  id: string;
  ordre: number | null;
  nom: string;
  isRemoved: boolean | null;
  couleur: ASSIGN_ACTION_TO_USER_assignActionToUser_etiquettes_couleur | null;
}

export interface ASSIGN_ACTION_TO_USER_assignActionToUser {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  _priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: ASSIGN_ACTION_TO_USER_assignActionToUser_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  idItemAssocie: string | null;
  nbComment: number;
  origine: ASSIGN_ACTION_TO_USER_assignActionToUser_origine | null;
  section: ASSIGN_ACTION_TO_USER_assignActionToUser_section | null;
  actionParent: ASSIGN_ACTION_TO_USER_assignActionToUser_actionParent | null;
  subActions: ASSIGN_ACTION_TO_USER_assignActionToUser_subActions[] | null;
  item: ASSIGN_ACTION_TO_USER_assignActionToUser_item | null;
  project: ASSIGN_ACTION_TO_USER_assignActionToUser_project | null;
  userCreation: ASSIGN_ACTION_TO_USER_assignActionToUser_userCreation | null;
  assignedUsers: ASSIGN_ACTION_TO_USER_assignActionToUser_assignedUsers[] | null;
  etiquettes: ASSIGN_ACTION_TO_USER_assignActionToUser_etiquettes[] | null;
}

export interface ASSIGN_ACTION_TO_USER {
  assignActionToUser: ASSIGN_ACTION_TO_USER_assignActionToUser;
}

export interface ASSIGN_ACTION_TO_USERVariables {
  input: AssignOrUnAssignActionInput;
}
