import gql from 'graphql-tag';
import { NOTIFICATION_INFO_FRAGMENT } from './fragment';

export const GET_NOTIFICATIONS_SUBSCRIPTION = gql`
  subscription NOTIFICATIONS_SUBSCRIPTION {
    notificationAdded {
      ...NotificationInfo
    }
  }
  ${NOTIFICATION_INFO_FRAGMENT}
`;

export const GET_UPDATED_NOTIFICATIONS_SUBSCRIPTION = gql`
  subscription NOTIFICATIONS_UPDATED_SUBSCRIPTION {
    notificationUpdated {
      ...NotificationInfo
    }
  }
  ${NOTIFICATION_INFO_FRAGMENT}
`;

export const GET_DELETED_NOTIFICATIONS_SUBSCRIPTION = gql`
  subscription NOTIFICATIONS_DELETED_SUBSCRIPTION {
    notificationDeleted {
      ...NotificationInfo
    }
  }
  ${NOTIFICATION_INFO_FRAGMENT}
`;
