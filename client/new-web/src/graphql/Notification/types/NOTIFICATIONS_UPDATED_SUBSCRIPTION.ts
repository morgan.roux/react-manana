/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL subscription operation: NOTIFICATIONS_UPDATED_SUBSCRIPTION
// ====================================================

export interface NOTIFICATIONS_UPDATED_SUBSCRIPTION_notificationUpdated_from {
  __typename: "User";
  id: string;
  userName: string | null;
  type: string;
}

export interface NOTIFICATIONS_UPDATED_SUBSCRIPTION_notificationUpdated_to {
  __typename: "User";
  id: string;
  userName: string | null;
  type: string;
}

export interface NOTIFICATIONS_UPDATED_SUBSCRIPTION_notificationUpdated {
  __typename: "Notification";
  id: string;
  type: string | null;
  targetId: string | null;
  targetName: string | null;
  message: string | null;
  seen: boolean | null;
  typeActualite: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  from: NOTIFICATIONS_UPDATED_SUBSCRIPTION_notificationUpdated_from | null;
  to: NOTIFICATIONS_UPDATED_SUBSCRIPTION_notificationUpdated_to | null;
}

export interface NOTIFICATIONS_UPDATED_SUBSCRIPTION {
  notificationUpdated: NOTIFICATIONS_UPDATED_SUBSCRIPTION_notificationUpdated | null;
}
