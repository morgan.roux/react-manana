/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL subscription operation: NOTIFICATIONS_DELETED_SUBSCRIPTION
// ====================================================

export interface NOTIFICATIONS_DELETED_SUBSCRIPTION_notificationDeleted_from {
  __typename: "User";
  id: string;
  userName: string | null;
  type: string;
}

export interface NOTIFICATIONS_DELETED_SUBSCRIPTION_notificationDeleted_to {
  __typename: "User";
  id: string;
  userName: string | null;
  type: string;
}

export interface NOTIFICATIONS_DELETED_SUBSCRIPTION_notificationDeleted {
  __typename: "Notification";
  id: string;
  type: string | null;
  targetId: string | null;
  targetName: string | null;
  message: string | null;
  seen: boolean | null;
  typeActualite: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  from: NOTIFICATIONS_DELETED_SUBSCRIPTION_notificationDeleted_from | null;
  to: NOTIFICATIONS_DELETED_SUBSCRIPTION_notificationDeleted_to | null;
}

export interface NOTIFICATIONS_DELETED_SUBSCRIPTION {
  notificationDeleted: NOTIFICATIONS_DELETED_SUBSCRIPTION_notificationDeleted | null;
}
