import gql from 'graphql-tag';
import { NOTIFICATION_INFO_FRAGMENT } from './fragment';

export const GET_NOTIFICATIONS = gql`
  query NOTIFICATIONS(
    $first: Int
    $skip: Int
    $orderBy: NotificationOrderByInput
    $dateFilterStart: String
    $dateFilterEnd: String
  ) {
    notifications(
      first: $first
      skip: $skip
      orderBy: $orderBy
      dateFilterStart: $dateFilterStart
      dateFilterEnd: $dateFilterEnd
    ) {
      total
      notSeen
      data {
        ...NotificationInfo
      }
    }
  }
  ${NOTIFICATION_INFO_FRAGMENT}
`;
