/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TitulaireAffectationInfo
// ====================================================

export interface TitulaireAffectationInfo_titulaireFonction {
  __typename: "TitulaireFonction";
  id: string;
  nom: string;
  code: string;
}

export interface TitulaireAffectationInfo_departement {
  __typename: "Departement";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface TitulaireAffectationInfo_titulaire {
  __typename: "Titulaire";
  id: string;
  fullName: string | null;
}

export interface TitulaireAffectationInfo_titulaireDemandeAffectation_titulaireRemplacent {
  __typename: "Titulaire";
  id: string;
  fullName: string | null;
}

export interface TitulaireAffectationInfo_titulaireDemandeAffectation {
  __typename: "TitulaireDemandeAffectation";
  id: string;
  titulaireRemplacent: TitulaireAffectationInfo_titulaireDemandeAffectation_titulaireRemplacent | null;
}

export interface TitulaireAffectationInfo {
  __typename: "TitulaireAffectation";
  id: string;
  titulaireFonction: TitulaireAffectationInfo_titulaireFonction | null;
  departement: TitulaireAffectationInfo_departement | null;
  titulaire: TitulaireAffectationInfo_titulaire | null;
  titulaireDemandeAffectation: TitulaireAffectationInfo_titulaireDemandeAffectation | null;
  dateDebut: any | null;
  dateFin: any | null;
}
