import { FICHIER_FRAGMENT } from '../Fichier/fragment';
import gql from 'graphql-tag';

export const GET_SOUS_ITEMS = gql`
  query SOUS_ITEMS($codeItemItem: String) {
    sousItems(codeItemItem: $codeItemItem) {
      id
      code
      name
      codeItem
      fichier {
        ...FichierInfo
      }
      parent {
        code
        name
        codeItem
        fichier {
          ...FichierInfo
        }
        dateCreation
        dateModification
      }
      dateCreation
      dateModification
    }
  }
  ${FICHIER_FRAGMENT}
`;

export const GET_ALL_ITEMS = gql`
  query ALL_ITEMS {
    items {
      id
      code
      codeItem
      name
    }
  }
`;
