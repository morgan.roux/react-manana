import gql from 'graphql-tag';
export const GET_CODEITEM_PREFIX = gql`
  {
    codeItemPrefix @client
  }
`;

export const GET_CHECKEDS_ITEM = gql`
  {
    checkedsItem @client {
      id
    }
  }
`;
