/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SOUS_ITEMS
// ====================================================

export interface SOUS_ITEMS_sousItems_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SOUS_ITEMS_sousItems_parent_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SOUS_ITEMS_sousItems_parent {
  __typename: "Item";
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: SOUS_ITEMS_sousItems_parent_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SOUS_ITEMS_sousItems {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: SOUS_ITEMS_sousItems_fichier | null;
  parent: SOUS_ITEMS_sousItems_parent | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SOUS_ITEMS {
  sousItems: (SOUS_ITEMS_sousItems | null)[] | null;
}

export interface SOUS_ITEMSVariables {
  codeItemItem?: string | null;
}
