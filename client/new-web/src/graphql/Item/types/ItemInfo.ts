/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ItemInfo
// ====================================================

export interface ItemInfo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ItemInfo_parent_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ItemInfo_parent {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: ItemInfo_parent_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ItemInfo {
  __typename: "Item";
  type: string;
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: ItemInfo_fichier | null;
  parent: ItemInfo_parent | null;
  dateCreation: any | null;
  dateModification: any | null;
}
