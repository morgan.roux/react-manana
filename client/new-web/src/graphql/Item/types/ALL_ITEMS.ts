/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ALL_ITEMS
// ====================================================

export interface ALL_ITEMS_items {
  __typename: "Item";
  id: string;
  code: string | null;
  codeItem: string | null;
  name: string | null;
}

export interface ALL_ITEMS {
  items: (ALL_ITEMS_items | null)[] | null;
}
