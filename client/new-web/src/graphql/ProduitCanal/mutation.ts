import gql from 'graphql-tag';
export const DELETE_CANAL_ARTICLE = gql`
  mutation softDeleteCanalArticle($id: ID!) {
    softDeleteCanalArticle(id: $id) {
      id
    }
  }
`;

export const DELETE_CANAL_ARTICLES = gql`
  mutation softDeleteCanalArticles($ids: [ID!]!) {
    softDeleteCanalArticles(ids: $ids) {
      id
    }
  }
`;
