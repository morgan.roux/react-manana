/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data_ProduitCanal_produit_famille {
  __typename: "Famille";
  id: string;
  codeFamille: string | null;
  libelleFamille: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data_ProduitCanal_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data_ProduitCanal_produit_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data_ProduitCanal_produit_produitTechReg_tva {
  __typename: "TVA";
  id: string;
  codeTva: number | null;
  tauxTva: number | null;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data_ProduitCanal_produit_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  laboExploitant: SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data_ProduitCanal_produit_produitTechReg_laboExploitant | null;
  tva: SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data_ProduitCanal_produit_produitTechReg_tva | null;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data_ProduitCanal_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  famille: SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data_ProduitCanal_produit_famille | null;
  produitCode: SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data_ProduitCanal_produit_produitCode | null;
  produitTechReg: SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data_ProduitCanal_produit_produitTechReg | null;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data_ProduitCanal_sousGammeCommercial {
  __typename: "CanalSousGamme";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data_ProduitCanal {
  __typename: "ProduitCanal";
  id: string;
  produit: SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data_ProduitCanal_produit | null;
  sousGammeCommercial: SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data_ProduitCanal_sousGammeCommercial | null;
}

export type SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data = SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data_Action | SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data_ProduitCanal;

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE {
  search: SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHEVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
