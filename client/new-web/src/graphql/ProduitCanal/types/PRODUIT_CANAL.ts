/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: PRODUIT_CANAL
// ====================================================

export interface PRODUIT_CANAL_produitCanal_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface PRODUIT_CANAL_produitCanal_inOtherPanier {
  __typename: "PanierLigne";
  id: string;
}

export interface PRODUIT_CANAL_produitCanal_articleSamePanachees {
  __typename: "ProduitCanal";
  id: string;
}

export interface PRODUIT_CANAL_produitCanal_operations {
  __typename: "Operation";
  id: string;
}

export interface PRODUIT_CANAL_produitCanal_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface PRODUIT_CANAL_produitCanal_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PRODUIT_CANAL_produitCanal_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface PRODUIT_CANAL_produitCanal_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: PRODUIT_CANAL_produitCanal_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface PRODUIT_CANAL_produitCanal_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface PRODUIT_CANAL_produitCanal_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: PRODUIT_CANAL_produitCanal_userSmyleys_data_user_role | null;
  userPhoto: PRODUIT_CANAL_produitCanal_userSmyleys_data_user_userPhoto | null;
  pharmacie: PRODUIT_CANAL_produitCanal_userSmyleys_data_user_pharmacie | null;
}

export interface PRODUIT_CANAL_produitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface PRODUIT_CANAL_produitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: PRODUIT_CANAL_produitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface PRODUIT_CANAL_produitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: PRODUIT_CANAL_produitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface PRODUIT_CANAL_produitCanal_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface PRODUIT_CANAL_produitCanal_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: PRODUIT_CANAL_produitCanal_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface PRODUIT_CANAL_produitCanal_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: PRODUIT_CANAL_produitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: PRODUIT_CANAL_produitCanal_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface PRODUIT_CANAL_produitCanal_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: PRODUIT_CANAL_produitCanal_userSmyleys_data_smyley_groupement | null;
}

export interface PRODUIT_CANAL_produitCanal_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: PRODUIT_CANAL_produitCanal_userSmyleys_data_item | null;
  idSource: string | null;
  user: PRODUIT_CANAL_produitCanal_userSmyleys_data_user | null;
  smyley: PRODUIT_CANAL_produitCanal_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PRODUIT_CANAL_produitCanal_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (PRODUIT_CANAL_produitCanal_userSmyleys_data | null)[] | null;
}

export interface PRODUIT_CANAL_produitCanal_comments_data_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface PRODUIT_CANAL_produitCanal_comments_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PRODUIT_CANAL_produitCanal_comments_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface PRODUIT_CANAL_produitCanal_comments_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: PRODUIT_CANAL_produitCanal_comments_data_user_userPhoto_fichier | null;
}

export interface PRODUIT_CANAL_produitCanal_comments_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface PRODUIT_CANAL_produitCanal_comments_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: PRODUIT_CANAL_produitCanal_comments_data_user_role | null;
  userPhoto: PRODUIT_CANAL_produitCanal_comments_data_user_userPhoto | null;
  pharmacie: PRODUIT_CANAL_produitCanal_comments_data_user_pharmacie | null;
}

export interface PRODUIT_CANAL_produitCanal_comments_data {
  __typename: "Comment";
  id: string;
  content: string;
  idItemAssocie: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichiers: PRODUIT_CANAL_produitCanal_comments_data_fichiers[] | null;
  user: PRODUIT_CANAL_produitCanal_comments_data_user;
}

export interface PRODUIT_CANAL_produitCanal_comments {
  __typename: "CommentResult";
  total: number;
  data: PRODUIT_CANAL_produitCanal_comments_data[];
}

export interface PRODUIT_CANAL_produitCanal_commandeCanal {
  __typename: "CommandeCanal";
  type: string;
  id: string;
  libelle: string | null;
  code: string | null;
  countOperationsCommercials: number | null;
  countCanalArticles: number | null;
}

export interface PRODUIT_CANAL_produitCanal_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PRODUIT_CANAL_produitCanal_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: PRODUIT_CANAL_produitCanal_produit_produitPhoto_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PRODUIT_CANAL_produitCanal_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface PRODUIT_CANAL_produitCanal_produit_famille {
  __typename: "Famille";
  id: string;
  codeFamille: string | null;
  libelleFamille: string | null;
}

export interface PRODUIT_CANAL_produitCanal_produit_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface PRODUIT_CANAL_produitCanal_produit_produitTechReg_acte {
  __typename: "Acte";
  id: string;
  codeActe: number | null;
  libelleActe: string | null;
}

export interface PRODUIT_CANAL_produitCanal_produit_produitTechReg_liste {
  __typename: "Liste";
  id: string;
  codeListe: number | null;
  libelle: string | null;
}

export interface PRODUIT_CANAL_produitCanal_produit_produitTechReg_libelleStockage {
  __typename: "LibelleDivers";
  id: string;
  codeInfo: number | null;
  code: number | null;
}

export interface PRODUIT_CANAL_produitCanal_produit_produitTechReg_tauxss {
  __typename: "TauxSS";
  id: string;
  codeTaux: number | null;
  tauxSS: number | null;
}

export interface PRODUIT_CANAL_produitCanal_produit_produitTechReg_tva {
  __typename: "TVA";
  id: string;
  codeTva: number | null;
  tauxTva: number | null;
}

export interface PRODUIT_CANAL_produitCanal_produit_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  isTipsLpp: number | null;
  isHomeo: number | null;
  laboExploitant: PRODUIT_CANAL_produitCanal_produit_produitTechReg_laboExploitant | null;
  acte: PRODUIT_CANAL_produitCanal_produit_produitTechReg_acte | null;
  liste: PRODUIT_CANAL_produitCanal_produit_produitTechReg_liste | null;
  libelleStockage: PRODUIT_CANAL_produitCanal_produit_produitTechReg_libelleStockage | null;
  tauxss: PRODUIT_CANAL_produitCanal_produit_produitTechReg_tauxss | null;
  tva: PRODUIT_CANAL_produitCanal_produit_produitTechReg_tva | null;
}

export interface PRODUIT_CANAL_produitCanal_produit {
  __typename: "Produit";
  id: string;
  produitPhoto: PRODUIT_CANAL_produitCanal_produit_produitPhoto | null;
  libelle: string | null;
  libelle2: string | null;
  supprimer: any | null;
  surveillanceRenforcee: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  produitCode: PRODUIT_CANAL_produitCanal_produit_produitCode | null;
  famille: PRODUIT_CANAL_produitCanal_produit_famille | null;
  produitTechReg: PRODUIT_CANAL_produitCanal_produit_produitTechReg | null;
}

export interface PRODUIT_CANAL_produitCanal {
  __typename: "ProduitCanal";
  type: string;
  id: string;
  qteStock: number | null;
  qteMin: number | null;
  stv: number | null;
  unitePetitCond: number | null;
  uniteSupCond: number | null;
  prixFab: number | null;
  prixPhv: number | null;
  prixTtc: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  dateRetrait: any | null;
  isActive: boolean | null;
  remises: (PRODUIT_CANAL_produitCanal_remises | null)[] | null;
  qteTotalRemisePanachees: number | null;
  inMyPanier: boolean | null;
  inOtherPanier: PRODUIT_CANAL_produitCanal_inOtherPanier | null;
  articleSamePanachees: (PRODUIT_CANAL_produitCanal_articleSamePanachees | null)[] | null;
  operations: (PRODUIT_CANAL_produitCanal_operations | null)[] | null;
  userSmyleys: PRODUIT_CANAL_produitCanal_userSmyleys | null;
  comments: PRODUIT_CANAL_produitCanal_comments | null;
  commandeCanal: PRODUIT_CANAL_produitCanal_commandeCanal | null;
  produit: PRODUIT_CANAL_produitCanal_produit | null;
}

export interface PRODUIT_CANAL {
  produitCanal: PRODUIT_CANAL_produitCanal | null;
}

export interface PRODUIT_CANALVariables {
  id: string;
  idPharmacie?: string | null;
  skip?: number | null;
  take?: number | null;
  idRemiseOperation?: string | null;
}
