/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: ProduitCanal
// ====================================================

export interface ProduitCanal_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface ProduitCanal_produit_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface ProduitCanal_produit_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  laboExploitant: ProduitCanal_produit_produitTechReg_laboExploitant | null;
}

export interface ProduitCanal_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  libelle2: string | null;
  produitCode: ProduitCanal_produit_produitCode | null;
  produitTechReg: ProduitCanal_produit_produitTechReg | null;
}

export interface ProduitCanal_operations {
  __typename: "Operation";
  id: string;
}

export interface ProduitCanal_inOtherPanier {
  __typename: "PanierLigne";
  id: string;
}

export interface ProduitCanal_articleSamePanachees {
  __typename: "ProduitCanal";
  id: string;
}

export interface ProduitCanal_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface ProduitCanal_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface ProduitCanal_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface ProduitCanal_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: ProduitCanal_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface ProduitCanal_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface ProduitCanal_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: ProduitCanal_userSmyleys_data_user_role | null;
  userPhoto: ProduitCanal_userSmyleys_data_user_userPhoto | null;
  pharmacie: ProduitCanal_userSmyleys_data_user_pharmacie | null;
}

export interface ProduitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface ProduitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: ProduitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface ProduitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: ProduitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface ProduitCanal_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface ProduitCanal_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: ProduitCanal_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface ProduitCanal_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: ProduitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: ProduitCanal_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface ProduitCanal_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: ProduitCanal_userSmyleys_data_smyley_groupement | null;
}

export interface ProduitCanal_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: ProduitCanal_userSmyleys_data_item | null;
  idSource: string | null;
  user: ProduitCanal_userSmyleys_data_user | null;
  smyley: ProduitCanal_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ProduitCanal_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (ProduitCanal_userSmyleys_data | null)[] | null;
}

export interface ProduitCanal_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface ProduitCanal_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface ProduitCanal_comments_data_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface ProduitCanal_comments_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface ProduitCanal_comments_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface ProduitCanal_comments_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: ProduitCanal_comments_data_user_userPhoto_fichier | null;
}

export interface ProduitCanal_comments_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface ProduitCanal_comments_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: ProduitCanal_comments_data_user_role | null;
  userPhoto: ProduitCanal_comments_data_user_userPhoto | null;
  pharmacie: ProduitCanal_comments_data_user_pharmacie | null;
}

export interface ProduitCanal_comments_data {
  __typename: "Comment";
  id: string;
  content: string;
  idItemAssocie: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichiers: ProduitCanal_comments_data_fichiers[] | null;
  user: ProduitCanal_comments_data_user;
}

export interface ProduitCanal_comments {
  __typename: "CommentResult";
  total: number;
  data: ProduitCanal_comments_data[];
}

export interface ProduitCanal {
  __typename: "ProduitCanal";
  type: string;
  id: string;
  qteStock: number | null;
  stv: number | null;
  prixPhv: number | null;
  dateCreation: any | null;
  isActive: boolean | null;
  produit: ProduitCanal_produit | null;
  operations: (ProduitCanal_operations | null)[] | null;
  qteTotalRemisePanachees: number | null;
  inMyPanier: boolean | null;
  inOtherPanier: ProduitCanal_inOtherPanier | null;
  articleSamePanachees: (ProduitCanal_articleSamePanachees | null)[] | null;
  userSmyleys: ProduitCanal_userSmyleys | null;
  commandeCanal: ProduitCanal_commandeCanal | null;
  remises: (ProduitCanal_remises | null)[] | null;
  comments: ProduitCanal_comments | null;
}
