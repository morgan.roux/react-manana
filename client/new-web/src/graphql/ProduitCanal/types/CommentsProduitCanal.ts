/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: CommentsProduitCanal
// ====================================================

export interface CommentsProduitCanal_comments_data_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface CommentsProduitCanal_comments_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CommentsProduitCanal_comments_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CommentsProduitCanal_comments_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CommentsProduitCanal_comments_data_user_userPhoto_fichier | null;
}

export interface CommentsProduitCanal_comments_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CommentsProduitCanal_comments_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CommentsProduitCanal_comments_data_user_role | null;
  userPhoto: CommentsProduitCanal_comments_data_user_userPhoto | null;
  pharmacie: CommentsProduitCanal_comments_data_user_pharmacie | null;
}

export interface CommentsProduitCanal_comments_data {
  __typename: "Comment";
  id: string;
  content: string;
  idItemAssocie: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichiers: CommentsProduitCanal_comments_data_fichiers[] | null;
  user: CommentsProduitCanal_comments_data_user;
}

export interface CommentsProduitCanal_comments {
  __typename: "CommentResult";
  total: number;
  data: CommentsProduitCanal_comments_data[];
}

export interface CommentsProduitCanal {
  __typename: "ProduitCanal";
  comments: CommentsProduitCanal_comments | null;
}
