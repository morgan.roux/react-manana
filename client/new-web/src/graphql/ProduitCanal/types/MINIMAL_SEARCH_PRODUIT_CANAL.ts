/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MINIMAL_SEARCH_PRODUIT_CANAL
// ====================================================

export interface MINIMAL_SEARCH_PRODUIT_CANAL_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface MINIMAL_SEARCH_PRODUIT_CANAL_search_data_ProduitCanal {
  __typename: "ProduitCanal";
  id: string;
}

export type MINIMAL_SEARCH_PRODUIT_CANAL_search_data = MINIMAL_SEARCH_PRODUIT_CANAL_search_data_Action | MINIMAL_SEARCH_PRODUIT_CANAL_search_data_ProduitCanal;

export interface MINIMAL_SEARCH_PRODUIT_CANAL_search {
  __typename: "SearchResult";
  total: number;
  data: (MINIMAL_SEARCH_PRODUIT_CANAL_search_data | null)[] | null;
}

export interface MINIMAL_SEARCH_PRODUIT_CANAL {
  search: MINIMAL_SEARCH_PRODUIT_CANAL_search | null;
}

export interface MINIMAL_SEARCH_PRODUIT_CANALVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
