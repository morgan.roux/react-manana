/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TodoEtiquetteInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_TODO_ETIQUETTE
// ====================================================

export interface CREATE_UPDATE_TODO_ETIQUETTE_createUpdateTodoEtiquette_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
}

export interface CREATE_UPDATE_TODO_ETIQUETTE_createUpdateTodoEtiquette_participants {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface CREATE_UPDATE_TODO_ETIQUETTE_createUpdateTodoEtiquette {
  __typename: "TodoEtiquette";
  id: string;
  _name: string;
  ordre: number | null;
  isInFavoris: boolean | null;
  isRemoved: boolean | null;
  isShared: boolean;
  nbAction: number;
  activeActions: boolean | null;
  couleur: CREATE_UPDATE_TODO_ETIQUETTE_createUpdateTodoEtiquette_couleur | null;
  participants: CREATE_UPDATE_TODO_ETIQUETTE_createUpdateTodoEtiquette_participants[] | null;
}

export interface CREATE_UPDATE_TODO_ETIQUETTE {
  createUpdateTodoEtiquette: CREATE_UPDATE_TODO_ETIQUETTE_createUpdateTodoEtiquette;
}

export interface CREATE_UPDATE_TODO_ETIQUETTEVariables {
  input: TodoEtiquetteInput;
  actionStatus?: string | null;
}
