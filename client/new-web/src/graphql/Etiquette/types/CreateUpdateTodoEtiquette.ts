/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TodoEtiquetteInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CreateUpdateTodoEtiquette
// ====================================================

export interface CreateUpdateTodoEtiquette_createUpdateTodoEtiquette_couleur {
  __typename: "Couleur";
  libelle: string;
}

export interface CreateUpdateTodoEtiquette_createUpdateTodoEtiquette {
  __typename: "TodoEtiquette";
  nom: string;
  couleur: CreateUpdateTodoEtiquette_createUpdateTodoEtiquette_couleur | null;
}

export interface CreateUpdateTodoEtiquette {
  createUpdateTodoEtiquette: CreateUpdateTodoEtiquette_createUpdateTodoEtiquette;
}

export interface CreateUpdateTodoEtiquetteVariables {
  input: TodoEtiquetteInput;
}
