/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ETIQUETTES_BY_USER_WITH_MIN_INFO
// ====================================================

export interface ETIQUETTES_BY_USER_WITH_MIN_INFO_todoEtiquettesByUser_couleur {
  __typename: "Couleur";
  code: string;
  id: string;
}

export interface ETIQUETTES_BY_USER_WITH_MIN_INFO_todoEtiquettesByUser {
  __typename: "TodoEtiquette";
  id: string;
  nom: string;
  couleur: ETIQUETTES_BY_USER_WITH_MIN_INFO_todoEtiquettesByUser_couleur | null;
}

export interface ETIQUETTES_BY_USER_WITH_MIN_INFO {
  todoEtiquettesByUser: ETIQUETTES_BY_USER_WITH_MIN_INFO_todoEtiquettesByUser[];
}

export interface ETIQUETTES_BY_USER_WITH_MIN_INFOVariables {
  idUser: string;
  isRemoved?: boolean | null;
}
