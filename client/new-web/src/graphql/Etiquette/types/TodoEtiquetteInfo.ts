/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TodoEtiquetteInfo
// ====================================================

export interface TodoEtiquetteInfo_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
}

export interface TodoEtiquetteInfo_participants {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface TodoEtiquetteInfo {
  __typename: "TodoEtiquette";
  id: string;
  _name: string;
  ordre: number | null;
  isInFavoris: boolean | null;
  isRemoved: boolean | null;
  isShared: boolean;
  nbAction: number;
  activeActions: boolean | null;
  couleur: TodoEtiquetteInfo_couleur | null;
  participants: TodoEtiquetteInfo_participants[] | null;
}
