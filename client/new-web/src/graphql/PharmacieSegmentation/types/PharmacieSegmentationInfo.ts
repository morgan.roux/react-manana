/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PharmacieSegmentationInfo
// ====================================================

export interface PharmacieSegmentationInfo_typologie {
  __typename: "Typologie";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface PharmacieSegmentationInfo_trancheCA {
  __typename: "TrancheCA";
  id: string;
  libelle: string | null;
  valMax: number | null;
  valMin: number | null;
}

export interface PharmacieSegmentationInfo_qualite {
  __typename: "Qualite";
  id: string;
  libelle: string | null;
}

export interface PharmacieSegmentationInfo_contrat {
  __typename: "Contrat";
  id: string;
  nom: string | null;
  numeroVersion: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieSegmentationInfo {
  __typename: "PharmacieSegmentation";
  id: string;
  dateSignature: any | null;
  typologie: PharmacieSegmentationInfo_typologie | null;
  trancheCA: PharmacieSegmentationInfo_trancheCA | null;
  qualite: PharmacieSegmentationInfo_qualite | null;
  contrat: PharmacieSegmentationInfo_contrat | null;
}
