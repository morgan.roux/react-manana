/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Sexe } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: AvatarInfo
// ====================================================

export interface AvatarInfo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface AvatarInfo_userCreation_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface AvatarInfo_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
  role: AvatarInfo_userCreation_role | null;
}

export interface AvatarInfo {
  __typename: "Avatar";
  id: string;
  description: string | null;
  codeSexe: Sexe | null;
  dateCreation: any | null;
  dateModification: any | null;
  fichier: AvatarInfo_fichier | null;
  userCreation: AvatarInfo_userCreation | null;
}
