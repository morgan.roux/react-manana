/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Sexe } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_AVATARS
// ====================================================

export interface DELETE_AVATARS_deleteAvatars_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface DELETE_AVATARS_deleteAvatars_userCreation_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface DELETE_AVATARS_deleteAvatars_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
  role: DELETE_AVATARS_deleteAvatars_userCreation_role | null;
}

export interface DELETE_AVATARS_deleteAvatars {
  __typename: "Avatar";
  id: string;
  description: string | null;
  codeSexe: Sexe | null;
  dateCreation: any | null;
  dateModification: any | null;
  fichier: DELETE_AVATARS_deleteAvatars_fichier | null;
  userCreation: DELETE_AVATARS_deleteAvatars_userCreation | null;
}

export interface DELETE_AVATARS {
  deleteAvatars: (DELETE_AVATARS_deleteAvatars | null)[] | null;
}

export interface DELETE_AVATARSVariables {
  ids: string[];
}
