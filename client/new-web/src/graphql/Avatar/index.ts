import { DO_CREATE_UPDATE_AVATAR, DO_DELETE_AVATARS } from './mutation';
import { GET_AVATAR } from './query';

export { DO_CREATE_UPDATE_AVATAR, DO_DELETE_AVATARS, GET_AVATAR };
