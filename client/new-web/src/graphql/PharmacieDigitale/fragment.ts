import gql from 'graphql-tag';
import { SERVICE_PHARMACIE_INFO_FRAGMENT } from '../ServicePharmacie/fragment';
import { PRESTATAIRE_PHARMACIE_INFO_FRAGMENT } from '../PrestatairePharmacie/fragment';

export const PHARMACIE_DIGITALE_INFO_FRAGMENT = gql`
  fragment PharmacieDigitaleInfo on PharmacieDigitale {
    id
    flagService
    dateInstallation
    url
    servicePharmacie {
      ...ServicePharmacieInfo
    }
    pharmaciePestataire {
      ...PrestatairePharmacieInfo
    }
  }
  ${SERVICE_PHARMACIE_INFO_FRAGMENT}
  ${PRESTATAIRE_PHARMACIE_INFO_FRAGMENT}
`;
