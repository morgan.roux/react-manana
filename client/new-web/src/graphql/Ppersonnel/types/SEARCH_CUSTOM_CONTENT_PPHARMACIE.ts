/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_PPHARMACIE
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data_Ppersonnel_pharmacie_titulaires_users {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data_Ppersonnel_pharmacie_titulaires {
  __typename: "Titulaire";
  id: string;
  fullName: string | null;
  users: (SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data_Ppersonnel_pharmacie_titulaires_users | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data_Ppersonnel_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  titulaires: (SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data_Ppersonnel_pharmacie_titulaires | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data_Ppersonnel_user_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data_Ppersonnel_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data_Ppersonnel_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data_Ppersonnel_user_userPhoto_fichier | null;
}

export interface SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data_Ppersonnel_user {
  __typename: "User";
  id: string;
  anneeNaissance: number | null;
  status: UserStatus | null;
  email: string | null;
  login: string | null;
  lastLoginDate: any | null;
  role: SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data_Ppersonnel_user_role | null;
  userPhoto: SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data_Ppersonnel_user_userPhoto | null;
}

export interface SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data_Ppersonnel {
  __typename: "Ppersonnel";
  id: string;
  civilite: string | null;
  fullName: string | null;
  sortie: number | null;
  pharmacie: SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data_Ppersonnel_pharmacie | null;
  estAmbassadrice: boolean | null;
  user: SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data_Ppersonnel_user | null;
}

export type SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data = SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data_Action | SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data_Ppersonnel;

export interface SEARCH_CUSTOM_CONTENT_PPHARMACIE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_PPHARMACIE_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_PPHARMACIE {
  search: SEARCH_CUSTOM_CONTENT_PPHARMACIE_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_PPHARMACIEVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
