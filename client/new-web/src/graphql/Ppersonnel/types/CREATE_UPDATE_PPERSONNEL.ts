/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PpersonnelInput, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_PPERSONNEL
// ====================================================

export interface CREATE_UPDATE_PPERSONNEL_createUpdatePpersonnel_pharmacie_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
}

export interface CREATE_UPDATE_PPERSONNEL_createUpdatePpersonnel_pharmacie {
  __typename: "Pharmacie";
  id: string;
  cip: string | null;
  nom: string | null;
  titulaires: (CREATE_UPDATE_PPERSONNEL_createUpdatePpersonnel_pharmacie_titulaires | null)[] | null;
}

export interface CREATE_UPDATE_PPERSONNEL_createUpdatePpersonnel_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
}

export interface CREATE_UPDATE_PPERSONNEL_createUpdatePpersonnel_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_UPDATE_PPERSONNEL_createUpdatePpersonnel {
  __typename: "Ppersonnel";
  type: string;
  id: string;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  estAmbassadrice: boolean | null;
  commentaire: string | null;
  sortie: number | null;
  dateSortie: any | null;
  idGroupement: string | null;
  idPharmacie: string | null;
  pharmacie: CREATE_UPDATE_PPERSONNEL_createUpdatePpersonnel_pharmacie | null;
  user: CREATE_UPDATE_PPERSONNEL_createUpdatePpersonnel_user | null;
  role: CREATE_UPDATE_PPERSONNEL_createUpdatePpersonnel_role | null;
}

export interface CREATE_UPDATE_PPERSONNEL {
  createUpdatePpersonnel: CREATE_UPDATE_PPERSONNEL_createUpdatePpersonnel;
}

export interface CREATE_UPDATE_PPERSONNELVariables {
  input: PpersonnelInput;
}
