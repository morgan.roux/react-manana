/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: PERSONNEL_PHARMACY
// ====================================================

export interface PERSONNEL_PHARMACY_ppersonnel_pharmacie_titulaires_users {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
}

export interface PERSONNEL_PHARMACY_ppersonnel_pharmacie_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  users: (PERSONNEL_PHARMACY_ppersonnel_pharmacie_titulaires_users | null)[] | null;
}

export interface PERSONNEL_PHARMACY_ppersonnel_pharmacie {
  __typename: "Pharmacie";
  id: string;
  cip: string | null;
  nom: string | null;
  titulaires: (PERSONNEL_PHARMACY_ppersonnel_pharmacie_titulaires | null)[] | null;
}

export interface PERSONNEL_PHARMACY_ppersonnel_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface PERSONNEL_PHARMACY_ppersonnel_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: PERSONNEL_PHARMACY_ppersonnel_user_userPhoto_fichier | null;
}

export interface PERSONNEL_PHARMACY_ppersonnel_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  jourNaissance: number | null;
  moisNaissance: number | null;
  anneeNaissance: number | null;
  codeTraitements: (string | null)[] | null;
  userPhoto: PERSONNEL_PHARMACY_ppersonnel_user_userPhoto | null;
}

export interface PERSONNEL_PHARMACY_ppersonnel_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PERSONNEL_PHARMACY_ppersonnel_contact {
  __typename: "Contact";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
}

export interface PERSONNEL_PHARMACY_ppersonnel {
  __typename: "Ppersonnel";
  type: string;
  id: string;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  estAmbassadrice: boolean | null;
  commentaire: string | null;
  sortie: number | null;
  dateSortie: any | null;
  idGroupement: string | null;
  idPharmacie: string | null;
  pharmacie: PERSONNEL_PHARMACY_ppersonnel_pharmacie | null;
  user: PERSONNEL_PHARMACY_ppersonnel_user | null;
  role: PERSONNEL_PHARMACY_ppersonnel_role | null;
  contact: PERSONNEL_PHARMACY_ppersonnel_contact | null;
}

export interface PERSONNEL_PHARMACY {
  ppersonnel: PERSONNEL_PHARMACY_ppersonnel | null;
}

export interface PERSONNEL_PHARMACYVariables {
  id: string;
}
