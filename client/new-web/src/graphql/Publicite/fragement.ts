import gql from 'graphql-tag';

export const PUBLICITE_INFO_FRAGMENT = gql`
  fragment PubliciteInfo on Publicite {
    type
    id
    libelle
    description
    typeEspace
    origineType: origine
    idItemSource
    url
    ordre
    dateDebut
    dateFin
    isRemoved
    dateCreation
    dateModification
    item {
      id
      codeItem
      name
    }
    image {
      id
      nomOriginal
      publicUrl
      chemin
    }
  }
`;
