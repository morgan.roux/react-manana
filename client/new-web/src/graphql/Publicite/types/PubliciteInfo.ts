/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeEspace, OriginePublicite } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: PubliciteInfo
// ====================================================

export interface PubliciteInfo_item {
  __typename: "Item";
  id: string;
  codeItem: string | null;
  name: string | null;
}

export interface PubliciteInfo_image {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
  chemin: string;
}

export interface PubliciteInfo {
  __typename: "Publicite";
  type: string;
  id: string;
  libelle: string | null;
  description: string | null;
  typeEspace: TypeEspace | null;
  origineType: OriginePublicite | null;
  idItemSource: string | null;
  url: string | null;
  ordre: number | null;
  dateDebut: any | null;
  dateFin: any | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
  item: PubliciteInfo_item | null;
  image: PubliciteInfo_image | null;
}
