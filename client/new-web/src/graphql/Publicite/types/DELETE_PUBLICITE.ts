/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeEspace, OriginePublicite } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_PUBLICITE
// ====================================================

export interface DELETE_PUBLICITE_deletePublicite_item {
  __typename: "Item";
  id: string;
  codeItem: string | null;
  name: string | null;
}

export interface DELETE_PUBLICITE_deletePublicite_image {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
  chemin: string;
}

export interface DELETE_PUBLICITE_deletePublicite {
  __typename: "Publicite";
  type: string;
  id: string;
  libelle: string | null;
  description: string | null;
  typeEspace: TypeEspace | null;
  origineType: OriginePublicite | null;
  idItemSource: string | null;
  url: string | null;
  ordre: number | null;
  dateDebut: any | null;
  dateFin: any | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
  item: DELETE_PUBLICITE_deletePublicite_item | null;
  image: DELETE_PUBLICITE_deletePublicite_image | null;
}

export interface DELETE_PUBLICITE {
  deletePublicite: DELETE_PUBLICITE_deletePublicite | null;
}

export interface DELETE_PUBLICITEVariables {
  id: string;
}
