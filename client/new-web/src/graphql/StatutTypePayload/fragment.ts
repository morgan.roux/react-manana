import gql from 'graphql-tag';

export const STATUT_TYPE_PAYLOAD_FRAGMENT = gql`
  fragment StatutTypePayloadInfo on StatutTypePayload {
    id
    code
    libelle
    dateCreation
    dateModification
  }
`;
