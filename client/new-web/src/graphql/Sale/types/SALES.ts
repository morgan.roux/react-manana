/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SALES
// ====================================================

export interface SALES_sales {
  __typename: "Sale";
  date: any | null;
  value: number | null;
}

export interface SALES {
  sales: (SALES_sales | null)[] | null;
}

export interface SALESVariables {
  idPharmacie: string;
}
