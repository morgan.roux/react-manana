/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SaleInfo
// ====================================================

export interface SaleInfo {
  __typename: "Sale";
  date: any | null;
  value: number | null;
}
