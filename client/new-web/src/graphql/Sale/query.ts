import gql from 'graphql-tag';
import { SALE_INFO_FRAGEMENT } from './fragment';

export const GET_SALES = gql`
  query SALES($idPharmacie: ID!) {
    sales(idPharmacie: $idPharmacie) {
      ...SaleInfo
    }
  }
  ${SALE_INFO_FRAGEMENT}
`;
