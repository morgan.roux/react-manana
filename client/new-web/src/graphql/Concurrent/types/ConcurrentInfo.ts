/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ConcurrentInfo
// ====================================================

export interface ConcurrentInfo {
  __typename: "Concurrent";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  telephone1: string | null;
  telephone2: string | null;
  fax: string | null;
  commentaire: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}
