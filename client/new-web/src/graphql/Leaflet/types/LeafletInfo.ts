/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: LeafletInfo
// ====================================================

export interface LeafletInfo {
  __typename: "Leaflet";
  id: string;
  modele: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}
