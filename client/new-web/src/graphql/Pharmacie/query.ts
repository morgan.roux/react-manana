import gql from 'graphql-tag';

import {
  PHARMACIE_MINIM_INFO_FRAGMENT,
  USER_FRAGMENT,
  PHARMACIE_INFO_FRAGMENT,
  PHARMACIE_TABLE_INFO_FRAGMENT,
} from './fragment';

export const GET_MY_PHARMACIE = gql`
  query MY_PHARMACIE {
    me {
      ...User
    }
  }
  ${USER_FRAGMENT}
`;

export const GET_PHARMACIES = gql`
  query PHARMACIES {
    pharmacies {
      ...PharmacieMinimInfo
    }
  }
  ${PHARMACIE_MINIM_INFO_FRAGMENT}
`;

export const GET_PHARMACIE = gql`
  query PHARMACIE($id: ID!) {
    pharmacie(id: $id) {
      ...PharmacieInfo
    }
  }
  ${PHARMACIE_INFO_FRAGMENT}
`;

export const DO_SEARCH_ALL_PHARMACIES = gql`
  query SEARCH_ALL_PHARMACIES(
    $type: [String]
    $query: JSON
    $filterBy: JSON
    $sortBy: JSON
    $skip: Int
    $take: Int
  ) {
    search(
      type: $type
      query: $query
      filterBy: $filterBy
      sortBy: $sortBy
      skip: $skip
      take: $take
    ) {
      total
      data {
        ... on Pharmacie {
          id
          nom
          dateCreation
          ville
          cp
        }
      }
    }
  }
`;

export const GET_SEARCH_CUSTOM_CONTENT_PHARMACIE = gql`
  query SEARCH_CUSTOM_CONTENT_PHARMACIE($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Pharmacie {
          ...PharmacieTableInfo
        }
      }
    }
  }
  ${PHARMACIE_TABLE_INFO_FRAGMENT}
`;

export const GET_PHARMACIE_WITH_MIN_INFO = gql`
  query PHARMACIE_WITH_MIN_INFO($id: ID!) {
    pharmacie(id: $id) {
      id
      nom
    }
  }
`;
