import gql from 'graphql-tag';

export const GET_CURRENT_PHARMACIE = gql`
  {
    pharmacie @client {
      id
      cip
      nom
      ville
      departement {
        id
        nom
        region {
          id
          nom
        }
      }
    }
  }
`;

export const GET_PHARMACIES = gql`
  {
    pharmacies @client {
      id
      cip
      nom
      ville
      departement {
        id
        nom
        region {
          id
          nom
        }
      }
    }
  }
`;

export const GET_CHECKEDS_PHARMACIE = gql`
  {
    checkedsPharmacie @client {
      id
    }
  }
`;
