/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: Titulaire
// ====================================================

export interface Titulaire_pharmacieUser_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface Titulaire_pharmacieUser_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: Titulaire_pharmacieUser_departement_region | null;
}

export interface Titulaire_pharmacieUser {
  __typename: "Pharmacie";
  type: string;
  id: string;
  cip: string | null;
  nom: string | null;
  adresse1: string | null;
  ville: string | null;
  departement: Titulaire_pharmacieUser_departement | null;
}

export interface Titulaire {
  __typename: "Titulaire";
  pharmacieUser: Titulaire_pharmacieUser | null;
}
