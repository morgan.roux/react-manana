/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: Pharmacie_User
// ====================================================

export interface Pharmacie_User_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface Pharmacie_User_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: Pharmacie_User_departement_region | null;
}

export interface Pharmacie_User {
  __typename: "Pharmacie";
  type: string;
  id: string;
  cip: string | null;
  nom: string | null;
  adresse1: string | null;
  ville: string | null;
  departement: Pharmacie_User_departement | null;
}
