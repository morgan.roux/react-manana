/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PharmacieMinimInfo
// ====================================================

export interface PharmacieMinimInfo_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface PharmacieMinimInfo_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: PharmacieMinimInfo_departement_region | null;
}

export interface PharmacieMinimInfo {
  __typename: "Pharmacie";
  type: string;
  id: string;
  cip: string | null;
  nom: string | null;
  adresse1: string | null;
  ville: string | null;
  departement: PharmacieMinimInfo_departement | null;
}
