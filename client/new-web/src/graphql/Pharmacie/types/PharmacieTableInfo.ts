/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: PharmacieTableInfo
// ====================================================

export interface PharmacieTableInfo_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface PharmacieTableInfo_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  sortie: number | null;
}

export interface PharmacieTableInfo_users {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
}

export interface PharmacieTableInfo_segmentation_trancheCA {
  __typename: "TrancheCA";
  id: string;
  libelle: string | null;
}

export interface PharmacieTableInfo_segmentation_contrat {
  __typename: "Contrat";
  id: string;
  nom: string | null;
}

export interface PharmacieTableInfo_segmentation {
  __typename: "PharmacieSegmentation";
  id: string;
  trancheCA: PharmacieTableInfo_segmentation_trancheCA | null;
  contrat: PharmacieTableInfo_segmentation_contrat | null;
}

export interface PharmacieTableInfo_achat_canal {
  __typename: "CommandeCanal";
  id: string;
  libelle: string | null;
}

export interface PharmacieTableInfo_achat {
  __typename: "PharmacieAchat";
  id: string;
  canal: PharmacieTableInfo_achat_canal | null;
}

export interface PharmacieTableInfo {
  __typename: "Pharmacie";
  id: string;
  sortie: number | null;
  cip: string | null;
  numFiness: string | null;
  nom: string | null;
  departement: PharmacieTableInfo_departement | null;
  titulaires: (PharmacieTableInfo_titulaires | null)[] | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  idGroupement: string | null;
  actived: boolean | null;
  users: (PharmacieTableInfo_users | null)[] | null;
  segmentation: PharmacieTableInfo_segmentation | null;
  achat: PharmacieTableInfo_achat | null;
  nbReclamation: number | null;
  nbAppel: number | null;
}
