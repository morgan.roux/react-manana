/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PHARMACIES
// ====================================================

export interface PHARMACIES_pharmacies_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface PHARMACIES_pharmacies_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: PHARMACIES_pharmacies_departement_region | null;
}

export interface PHARMACIES_pharmacies {
  __typename: "Pharmacie";
  type: string;
  id: string;
  cip: string | null;
  nom: string | null;
  adresse1: string | null;
  ville: string | null;
  departement: PHARMACIES_pharmacies_departement | null;
}

export interface PHARMACIES {
  pharmacies: (PHARMACIES_pharmacies | null)[] | null;
}
