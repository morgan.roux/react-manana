import gql from 'graphql-tag';
import { QUALITE_INFO_FRAGMENT } from './fragment';

export const GET_QUALITES = gql`
  query QUALITES {
    qualites {
      ...QualiteInfo
    }
  }
  ${QUALITE_INFO_FRAGMENT}
`;
