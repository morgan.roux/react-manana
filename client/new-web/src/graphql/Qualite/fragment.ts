import gql from 'graphql-tag';

export const QUALITE_INFO_FRAGMENT = gql`
  fragment QualiteInfo on Qualite {
    id
    libelle
  }
`;
