/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: QUALITES
// ====================================================

export interface QUALITES_qualites {
  __typename: "Qualite";
  id: string;
  libelle: string | null;
}

export interface QUALITES {
  qualites: (QUALITES_qualites | null)[] | null;
}
