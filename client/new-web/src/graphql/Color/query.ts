import gql from 'graphql-tag';
import { COLOR_INFO } from './fragment';

export const DO_SEARCH_COLOR = gql`
  query SEARCH_COLOR(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
  ) {
    search(
      type: $type
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on Couleur {
          ...ColorInfo
        }
      }
    }
  }
  ${COLOR_INFO}
`;

export const GET_COLOR = gql`
  query COLOR($id: ID!) {
    couleur(id: $id) {
      ...ColorInfo
    }
  }
  ${COLOR_INFO}
`;
