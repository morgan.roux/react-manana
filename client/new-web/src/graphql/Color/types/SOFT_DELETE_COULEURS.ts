/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: SOFT_DELETE_COULEURS
// ====================================================

export interface SOFT_DELETE_COULEURS_softDeleteCouleurs {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
  dateCreation: any;
  dateModification: any;
}

export interface SOFT_DELETE_COULEURS {
  softDeleteCouleurs: SOFT_DELETE_COULEURS_softDeleteCouleurs[];
}

export interface SOFT_DELETE_COULEURSVariables {
  ids: string[];
}
