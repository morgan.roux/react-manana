/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { LdapType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_ACTIVE_DIRECTORY
// ====================================================

export interface UPDATE_ACTIVE_DIRECTORY_updateActiveDirectoryCredential_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface UPDATE_ACTIVE_DIRECTORY_updateActiveDirectoryCredential_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: UPDATE_ACTIVE_DIRECTORY_updateActiveDirectoryCredential_groupement_defaultPharmacie_departement_region | null;
}

export interface UPDATE_ACTIVE_DIRECTORY_updateActiveDirectoryCredential_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: UPDATE_ACTIVE_DIRECTORY_updateActiveDirectoryCredential_groupement_defaultPharmacie_departement | null;
}

export interface UPDATE_ACTIVE_DIRECTORY_updateActiveDirectoryCredential_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface UPDATE_ACTIVE_DIRECTORY_updateActiveDirectoryCredential_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: UPDATE_ACTIVE_DIRECTORY_updateActiveDirectoryCredential_groupement_groupementLogo_fichier | null;
}

export interface UPDATE_ACTIVE_DIRECTORY_updateActiveDirectoryCredential_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: UPDATE_ACTIVE_DIRECTORY_updateActiveDirectoryCredential_groupement_defaultPharmacie | null;
  groupementLogo: UPDATE_ACTIVE_DIRECTORY_updateActiveDirectoryCredential_groupement_groupementLogo | null;
}

export interface UPDATE_ACTIVE_DIRECTORY_updateActiveDirectoryCredential {
  __typename: "ActiveDirectoryCredential";
  id: string;
  url: string;
  user: string | null;
  password: string | null;
  base: string | null;
  ldapType: LdapType | null;
  groupement: UPDATE_ACTIVE_DIRECTORY_updateActiveDirectoryCredential_groupement | null;
}

export interface UPDATE_ACTIVE_DIRECTORY {
  updateActiveDirectoryCredential: UPDATE_ACTIVE_DIRECTORY_updateActiveDirectoryCredential | null;
}

export interface UPDATE_ACTIVE_DIRECTORYVariables {
  id: string;
  url?: string | null;
  user?: string | null;
  password?: string | null;
  base?: string | null;
  ldapType?: LdapType | null;
}
