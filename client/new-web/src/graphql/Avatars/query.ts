import gql from 'graphql-tag';

export const GET_GROUPEMENT_AVATARS = gql`
  query GROUPEMENT_AVATARS {
    groupementAvatars {
      id
      idGroupement
      codeSexe
      description
      fichier {
        id
        chemin
        nomOriginal
      }
    }
  }
`;
