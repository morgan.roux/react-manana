/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MINIM_PRESTATAIRE_PHARMACIES
// ====================================================

export interface MINIM_PRESTATAIRE_PHARMACIES_prestatairePharmacies {
  __typename: "PrestatairePharmacie";
  id: string;
  nom: string | null;
}

export interface MINIM_PRESTATAIRE_PHARMACIES {
  prestatairePharmacies: (MINIM_PRESTATAIRE_PHARMACIES_prestatairePharmacies | null)[] | null;
}
