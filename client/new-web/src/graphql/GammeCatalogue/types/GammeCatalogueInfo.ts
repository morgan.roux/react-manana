/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: GammeCatalogueInfo
// ====================================================

export interface GammeCatalogueInfo {
  __typename: "GammeCatalogue";
  id: string;
  codeGamme: number | null;
  libelle: string | null;
  numOrdre: number | null;
  estDefaut: number | null;
}
