/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: TVAS
// ====================================================

export interface TVAS_tvas {
  __typename: "TVA";
  id: string;
  codeTva: number | null;
  tauxTva: number | null;
  designation: string | null;
}

export interface TVAS {
  tvas: (TVAS_tvas | null)[] | null;
}
