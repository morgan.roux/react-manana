/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TrTvaInfo
// ====================================================

export interface TrTvaInfo {
  __typename: "TrTVA";
  type: string;
  id: string;
  codeTva: string | null;
  valTva: number | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}
