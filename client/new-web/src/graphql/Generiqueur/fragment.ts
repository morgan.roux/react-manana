import gql from 'graphql-tag';

export const GENERIQUEUR_INFO_FRAGMENT = gql`
  fragment GeneriqueurInfo on Generiqueur {
    id
    nom
  }
`;
