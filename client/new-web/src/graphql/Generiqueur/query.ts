import gql from 'graphql-tag';
import { GENERIQUEUR_INFO_FRAGMENT } from './fragment';

export const GET_GENERIQUEURS = gql`
  query GENERIQUEURS {
    generiqueurs {
      ...GeneriqueurInfo
    }
  }
  ${GENERIQUEUR_INFO_FRAGMENT}
`;
