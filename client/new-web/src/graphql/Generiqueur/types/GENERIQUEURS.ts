/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GENERIQUEURS
// ====================================================

export interface GENERIQUEURS_generiqueurs {
  __typename: "Generiqueur";
  id: string;
  nom: string | null;
}

export interface GENERIQUEURS {
  generiqueurs: (GENERIQUEURS_generiqueurs | null)[] | null;
}
