/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { SsoType, Sendingtype } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_SSO_APPLICATION
// ====================================================

export interface DELETE_SSO_APPLICATION_deleteSsoApplication_icon {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface DELETE_SSO_APPLICATION_deleteSsoApplication_mappings {
  __typename: "MappingVariable";
  id: string;
  nom: string | null;
  value: string | null;
}

export interface DELETE_SSO_APPLICATION_deleteSsoApplication_samlLoginDetail {
  __typename: "SamlLogin";
  id: string;
  idpX509Certificate: string | null;
  spX509Certificate: string | null;
  consumerUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface DELETE_SSO_APPLICATION_deleteSsoApplication_cryptoAesDetail {
  __typename: "CryptoAes256";
  id: string;
  key: string | null;
  requestUrl: string | null;
  hexadecimal: boolean | null;
  Sendingtype: Sendingtype | null;
}

export interface DELETE_SSO_APPLICATION_deleteSsoApplication_cryptoMd5Detail {
  __typename: "CryptoMd5";
  id: string;
  beginGet: number | null;
  endGet: number | null;
  hexadecimal: boolean | null;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
  haveExternalUserMapping: boolean | null;
}

export interface DELETE_SSO_APPLICATION_deleteSsoApplication_cryptoOtherDetail {
  __typename: "CryptoOther";
  id: string;
  function: string | null;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface DELETE_SSO_APPLICATION_deleteSsoApplication_tokenAuthentificationDetail {
  __typename: "TokenAuthentification";
  id: string;
  reqTokenUrl: string | null;
  reqUserUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface DELETE_SSO_APPLICATION_deleteSsoApplication_tokenFtpAuthentificationDetail_ftpid {
  __typename: "FtpConnect";
  id: string;
  host: string;
  port: number | null;
  user: string | null;
  password: string | null;
}

export interface DELETE_SSO_APPLICATION_deleteSsoApplication_tokenFtpAuthentificationDetail {
  __typename: "TokenFtpAuthentification";
  id: string;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
  ftpFilePath: string | null;
  ftpFileName: string | null;
  ftpid: DELETE_SSO_APPLICATION_deleteSsoApplication_tokenFtpAuthentificationDetail_ftpid | null;
}

export interface DELETE_SSO_APPLICATION_deleteSsoApplication {
  __typename: "SsoApplication";
  id: string;
  nom: string;
  url: string;
  icon: DELETE_SSO_APPLICATION_deleteSsoApplication_icon | null;
  ssoType: SsoType;
  ssoTypeid: string | null;
  mappings: (DELETE_SSO_APPLICATION_deleteSsoApplication_mappings | null)[] | null;
  samlLoginDetail: DELETE_SSO_APPLICATION_deleteSsoApplication_samlLoginDetail | null;
  cryptoAesDetail: DELETE_SSO_APPLICATION_deleteSsoApplication_cryptoAesDetail | null;
  cryptoMd5Detail: DELETE_SSO_APPLICATION_deleteSsoApplication_cryptoMd5Detail | null;
  cryptoOtherDetail: DELETE_SSO_APPLICATION_deleteSsoApplication_cryptoOtherDetail | null;
  tokenAuthentificationDetail: DELETE_SSO_APPLICATION_deleteSsoApplication_tokenAuthentificationDetail | null;
  tokenFtpAuthentificationDetail: DELETE_SSO_APPLICATION_deleteSsoApplication_tokenFtpAuthentificationDetail | null;
}

export interface DELETE_SSO_APPLICATION {
  deleteSsoApplication: DELETE_SSO_APPLICATION_deleteSsoApplication | null;
}

export interface DELETE_SSO_APPLICATIONVariables {
  id: string;
}
