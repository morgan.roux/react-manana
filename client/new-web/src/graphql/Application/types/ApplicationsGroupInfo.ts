/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { SsoType, Sendingtype } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: ApplicationsGroupInfo
// ====================================================

export interface ApplicationsGroupInfo_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
}

export interface ApplicationsGroupInfo_applications_icon {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ApplicationsGroupInfo_applications_mappings {
  __typename: "MappingVariable";
  id: string;
  nom: string | null;
  value: string | null;
}

export interface ApplicationsGroupInfo_applications_samlLoginDetail {
  __typename: "SamlLogin";
  id: string;
  idpX509Certificate: string | null;
  spX509Certificate: string | null;
  consumerUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface ApplicationsGroupInfo_applications_cryptoAesDetail {
  __typename: "CryptoAes256";
  id: string;
  key: string | null;
  requestUrl: string | null;
  hexadecimal: boolean | null;
  Sendingtype: Sendingtype | null;
}

export interface ApplicationsGroupInfo_applications_cryptoMd5Detail {
  __typename: "CryptoMd5";
  id: string;
  beginGet: number | null;
  endGet: number | null;
  hexadecimal: boolean | null;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
  haveExternalUserMapping: boolean | null;
}

export interface ApplicationsGroupInfo_applications_cryptoOtherDetail {
  __typename: "CryptoOther";
  id: string;
  function: string | null;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface ApplicationsGroupInfo_applications_tokenAuthentificationDetail {
  __typename: "TokenAuthentification";
  id: string;
  reqTokenUrl: string | null;
  reqUserUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface ApplicationsGroupInfo_applications_tokenFtpAuthentificationDetail_ftpid {
  __typename: "FtpConnect";
  id: string;
  host: string;
  port: number | null;
  user: string | null;
  password: string | null;
}

export interface ApplicationsGroupInfo_applications_tokenFtpAuthentificationDetail {
  __typename: "TokenFtpAuthentification";
  id: string;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
  ftpFilePath: string | null;
  ftpFileName: string | null;
  ftpid: ApplicationsGroupInfo_applications_tokenFtpAuthentificationDetail_ftpid | null;
}

export interface ApplicationsGroupInfo_applications {
  __typename: "SsoApplication";
  id: string;
  nom: string;
  url: string;
  icon: ApplicationsGroupInfo_applications_icon | null;
  ssoType: SsoType;
  ssoTypeid: string | null;
  mappings: (ApplicationsGroupInfo_applications_mappings | null)[] | null;
  samlLoginDetail: ApplicationsGroupInfo_applications_samlLoginDetail | null;
  cryptoAesDetail: ApplicationsGroupInfo_applications_cryptoAesDetail | null;
  cryptoMd5Detail: ApplicationsGroupInfo_applications_cryptoMd5Detail | null;
  cryptoOtherDetail: ApplicationsGroupInfo_applications_cryptoOtherDetail | null;
  tokenAuthentificationDetail: ApplicationsGroupInfo_applications_tokenAuthentificationDetail | null;
  tokenFtpAuthentificationDetail: ApplicationsGroupInfo_applications_tokenFtpAuthentificationDetail | null;
}

export interface ApplicationsGroupInfo {
  __typename: "ApplicationsGroup";
  id: string;
  groupement: ApplicationsGroupInfo_groupement;
  applications: ApplicationsGroupInfo_applications | null;
}
