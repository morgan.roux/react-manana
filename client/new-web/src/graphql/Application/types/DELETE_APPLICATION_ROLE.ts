/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { SsoType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_APPLICATION_ROLE
// ====================================================

export interface DELETE_APPLICATION_ROLE_deleteApplicationRole_role {
  __typename: "Role";
  id: string;
  nom: string | null;
  code: string | null;
}

export interface DELETE_APPLICATION_ROLE_deleteApplicationRole_applications_icon {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface DELETE_APPLICATION_ROLE_deleteApplicationRole_applications {
  __typename: "SsoApplication";
  id: string;
  nom: string;
  url: string;
  icon: DELETE_APPLICATION_ROLE_deleteApplicationRole_applications_icon | null;
  ssoType: SsoType;
  ssoTypeid: string | null;
}

export interface DELETE_APPLICATION_ROLE_deleteApplicationRole {
  __typename: "ApplicationsRole";
  id: string;
  role: DELETE_APPLICATION_ROLE_deleteApplicationRole_role | null;
  applications: DELETE_APPLICATION_ROLE_deleteApplicationRole_applications | null;
}

export interface DELETE_APPLICATION_ROLE {
  deleteApplicationRole: DELETE_APPLICATION_ROLE_deleteApplicationRole | null;
}

export interface DELETE_APPLICATION_ROLEVariables {
  idrole: string;
  idGroupement: string;
  idApplications: string;
}
