/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { SsoType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: ApplicationRoleInfo
// ====================================================

export interface ApplicationRoleInfo_role {
  __typename: "Role";
  id: string;
  nom: string | null;
  code: string | null;
}

export interface ApplicationRoleInfo_applications_icon {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ApplicationRoleInfo_applications {
  __typename: "SsoApplication";
  id: string;
  nom: string;
  url: string;
  icon: ApplicationRoleInfo_applications_icon | null;
  ssoType: SsoType;
  ssoTypeid: string | null;
}

export interface ApplicationRoleInfo {
  __typename: "ApplicationsRole";
  id: string;
  role: ApplicationRoleInfo_role | null;
  applications: ApplicationRoleInfo_applications | null;
}
