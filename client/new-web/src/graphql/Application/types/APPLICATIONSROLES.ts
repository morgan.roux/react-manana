/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { SsoType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: APPLICATIONSROLES
// ====================================================

export interface APPLICATIONSROLES_applicationsRoles_role {
  __typename: "Role";
  id: string;
  nom: string | null;
  code: string | null;
}

export interface APPLICATIONSROLES_applicationsRoles_applications_icon {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface APPLICATIONSROLES_applicationsRoles_applications {
  __typename: "SsoApplication";
  id: string;
  nom: string;
  url: string;
  icon: APPLICATIONSROLES_applicationsRoles_applications_icon | null;
  ssoType: SsoType;
  ssoTypeid: string | null;
}

export interface APPLICATIONSROLES_applicationsRoles {
  __typename: "ApplicationsRole";
  id: string;
  role: APPLICATIONSROLES_applicationsRoles_role | null;
  applications: APPLICATIONSROLES_applicationsRoles_applications | null;
}

export interface APPLICATIONSROLES {
  applicationsRoles: (APPLICATIONSROLES_applicationsRoles | null)[] | null;
}

export interface APPLICATIONSROLESVariables {
  idgroupement: string;
}
