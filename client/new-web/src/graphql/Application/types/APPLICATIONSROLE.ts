/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { SsoType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: APPLICATIONSROLE
// ====================================================

export interface APPLICATIONSROLE_applicationsRole_role {
  __typename: "Role";
  id: string;
  nom: string | null;
  code: string | null;
}

export interface APPLICATIONSROLE_applicationsRole_applications_icon {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface APPLICATIONSROLE_applicationsRole_applications {
  __typename: "SsoApplication";
  id: string;
  nom: string;
  url: string;
  icon: APPLICATIONSROLE_applicationsRole_applications_icon | null;
  ssoType: SsoType;
  ssoTypeid: string | null;
}

export interface APPLICATIONSROLE_applicationsRole {
  __typename: "ApplicationsRole";
  id: string;
  role: APPLICATIONSROLE_applicationsRole_role | null;
  applications: APPLICATIONSROLE_applicationsRole_applications | null;
}

export interface APPLICATIONSROLE {
  applicationsRole: (APPLICATIONSROLE_applicationsRole | null)[] | null;
}

export interface APPLICATIONSROLEVariables {
  idgroupement: string;
  coderole: string;
}
