import gql from 'graphql-tag';
import { APPLICATION_INFO, APPLICATION_ROLE_INFO, SSO_APPLICATION_INFO } from './fragment';

export const GET_APPLICATIONS_GROUP = gql`
  query APPLICATIONSGROUP($idgroupement: ID!) {
    applicationsGroup(idgroupement: $idgroupement) {
      ...ApplicationsGroupInfo
    }
  }
  ${APPLICATION_INFO}
`;

export const GET_SSO_APPLICATION = gql`
  query SSO_APPLICATION($id: ID!) {
    ssoApplication(id: $id) {
      ...SsoApplicationInfo
    }
  }
  ${SSO_APPLICATION_INFO}
`;

export const GET_APPLICATIONS_ROLE = gql`
  query APPLICATIONSROLE($idgroupement: ID!, $coderole: String!) {
    applicationsRole(idgroupement: $idgroupement, coderole: $coderole) {
      ...ApplicationRoleInfo
    }
  }
  ${APPLICATION_ROLE_INFO}
`;

export const GET_APPLICATIONS_ROLES = gql`
  query APPLICATIONSROLES($idgroupement: ID!) {
    applicationsRoles(idgroupement: $idgroupement) {
      ...ApplicationRoleInfo
    }
  }
  ${APPLICATION_ROLE_INFO}
`;