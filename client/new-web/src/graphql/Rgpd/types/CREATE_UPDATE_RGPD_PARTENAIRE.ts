/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RgpdPartenaireInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_RGPD_PARTENAIRE
// ====================================================

export interface CREATE_UPDATE_RGPD_PARTENAIRE_createUpdateRgpdPartenaire {
  __typename: "RgpdPartenaire";
  id: string;
  title: string;
  description: string;
  order: number | null;
}

export interface CREATE_UPDATE_RGPD_PARTENAIRE {
  createUpdateRgpdPartenaire: CREATE_UPDATE_RGPD_PARTENAIRE_createUpdateRgpdPartenaire;
}

export interface CREATE_UPDATE_RGPD_PARTENAIREVariables {
  input: RgpdPartenaireInput;
}
