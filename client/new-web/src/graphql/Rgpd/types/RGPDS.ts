/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: RGPDS
// ====================================================

export interface RGPDS_rgpds {
  __typename: "Rgpd";
  id: string;
  politiqueConfidentialite: string | null;
  conditionUtilisation: string | null;
  informationsCookies: string | null;
}

export interface RGPDS {
  rgpds: RGPDS_rgpds[];
}

export interface RGPDSVariables {
  idGroupement?: string | null;
}
