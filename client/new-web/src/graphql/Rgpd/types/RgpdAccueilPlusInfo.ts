/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: RgpdAccueilPlusInfo
// ====================================================

export interface RgpdAccueilPlusInfo {
  __typename: "RgpdAccueilPlus";
  id: string;
  title: string;
  description: string;
  order: number | null;
}
