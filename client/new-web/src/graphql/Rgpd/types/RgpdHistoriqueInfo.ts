/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: RgpdHistoriqueInfo
// ====================================================

export interface RgpdHistoriqueInfo_user {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface RgpdHistoriqueInfo {
  __typename: "RgpdHistorique";
  id: string;
  accept_all: boolean | null;
  refuse_all: boolean | null;
  user: RgpdHistoriqueInfo_user;
}
