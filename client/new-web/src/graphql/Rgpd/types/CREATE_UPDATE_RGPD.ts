/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RgpdInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_RGPD
// ====================================================

export interface CREATE_UPDATE_RGPD_createUpdateRgpd {
  __typename: "Rgpd";
  id: string;
  politiqueConfidentialite: string | null;
  conditionUtilisation: string | null;
  informationsCookies: string | null;
}

export interface CREATE_UPDATE_RGPD {
  createUpdateRgpd: CREATE_UPDATE_RGPD_createUpdateRgpd;
}

export interface CREATE_UPDATE_RGPDVariables {
  input: RgpdInput;
}
