/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: RGPD_ACCUEILS
// ====================================================

export interface RGPD_ACCUEILS_rgpdAccueils {
  __typename: "RgpdAccueil";
  id: string;
  title: string;
  description: string;
}

export interface RGPD_ACCUEILS {
  rgpdAccueils: RGPD_ACCUEILS_rgpdAccueils[];
}

export interface RGPD_ACCUEILSVariables {
  idGroupement?: string | null;
}
