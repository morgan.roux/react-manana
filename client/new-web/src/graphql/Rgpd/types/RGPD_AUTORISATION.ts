/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: RGPD_AUTORISATION
// ====================================================

export interface RGPD_AUTORISATION_rgpdAutorisation {
  __typename: "RgpdAutorisation";
  id: string;
  title: string;
  description: string;
  order: number | null;
}

export interface RGPD_AUTORISATION {
  rgpdAutorisation: RGPD_AUTORISATION_rgpdAutorisation | null;
}

export interface RGPD_AUTORISATIONVariables {
  id: string;
}
