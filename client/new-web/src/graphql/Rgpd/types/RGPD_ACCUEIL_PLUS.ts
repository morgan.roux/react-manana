/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: RGPD_ACCUEIL_PLUS
// ====================================================

export interface RGPD_ACCUEIL_PLUS_rgpdAccueilPlus {
  __typename: "RgpdAccueilPlus";
  id: string;
  title: string;
  description: string;
  order: number | null;
}

export interface RGPD_ACCUEIL_PLUS {
  rgpdAccueilPlus: RGPD_ACCUEIL_PLUS_rgpdAccueilPlus | null;
}

export interface RGPD_ACCUEIL_PLUSVariables {
  id: string;
}
