/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RgpdHistoriqueInfoPlusType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: RgpdHistoriqueInfoPlusInfo
// ====================================================

export interface RgpdHistoriqueInfoPlusInfo_historique {
  __typename: "RgpdHistorique";
  id: string;
}

export interface RgpdHistoriqueInfoPlusInfo {
  __typename: "RgpdHistoriqueInfoPlus";
  id: string;
  type: RgpdHistoriqueInfoPlusType;
  accepted: boolean;
  idItemAssocie: string;
  historique: RgpdHistoriqueInfoPlusInfo_historique;
}
