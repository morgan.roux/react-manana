/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: RgpdAccueilInfo
// ====================================================

export interface RgpdAccueilInfo {
  __typename: "RgpdAccueil";
  id: string;
  title: string;
  description: string;
}
