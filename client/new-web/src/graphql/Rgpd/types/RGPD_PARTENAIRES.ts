/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: RGPD_PARTENAIRES
// ====================================================

export interface RGPD_PARTENAIRES_rgpdPartenaires {
  __typename: "RgpdPartenaire";
  id: string;
  title: string;
  description: string;
  order: number | null;
}

export interface RGPD_PARTENAIRES {
  rgpdPartenaires: RGPD_PARTENAIRES_rgpdPartenaires[];
}

export interface RGPD_PARTENAIRESVariables {
  idGroupement?: string | null;
}
