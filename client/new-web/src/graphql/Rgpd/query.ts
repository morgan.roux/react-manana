import gql from 'graphql-tag';
import {
  RGPD_ACCUEIL_INFO_FRAGEMENT,
  RGPD_ACCUEIL_PLUS_INFO_FRAGEMENT,
  RGPD_AUTORISATION_INFO_FRAGEMENT,
  RGPD_INFO_FRAGEMENT,
  RGPD_PARTENAIRE_INFO_FRAGEMENT,
} from './fragment';

export const GET_RGPDS = gql`
  query RGPDS($idGroupement: ID) {
    rgpds(idGroupement: $idGroupement) {
      ...RgpdInfo
    }
  }
  ${RGPD_INFO_FRAGEMENT}
`;

export const GET_RGPD = gql`
  query RGPD($id: ID!) {
    rgpd(id: $id) {
      ...RgpdInfo
    }
  }
  ${RGPD_INFO_FRAGEMENT}
`;

export const DO_CHECK_ACCEPT_RGPD = gql`
  query CHECK_ACCEPT_RGPD($idUser: ID!) {
    checkAcceptRgpd(idUser: $idUser)
  }
`;

export const GET_RGPD_ACCUEILS = gql`
  query RGPD_ACCUEILS($idGroupement: ID) {
    rgpdAccueils(idGroupement: $idGroupement) {
      ...RgpdAccueilInfo
    }
  }
  ${RGPD_ACCUEIL_INFO_FRAGEMENT}
`;

export const GET_RGPD_ACCUEIL = gql`
  query RGPD_ACCUEIL($id: ID!) {
    rgpdAccueil(id: $id) {
      ...RgpdAccueilInfo
    }
  }
  ${RGPD_ACCUEIL_INFO_FRAGEMENT}
`;

export const GET_RGPD_ACCUEILS_PLUSES = gql`
  query RGPD_ACCUEILS_PLUSES($idGroupement: ID) {
    rgpdAccueilPluses(idGroupement: $idGroupement) {
      ...RgpdAccueilPlusInfo
    }
  }
  ${RGPD_ACCUEIL_PLUS_INFO_FRAGEMENT}
`;

export const GET_RGPD_ACCUEIL_PLUS = gql`
  query RGPD_ACCUEIL_PLUS($id: ID!) {
    rgpdAccueilPlus(id: $id) {
      ...RgpdAccueilPlusInfo
    }
  }
  ${RGPD_ACCUEIL_PLUS_INFO_FRAGEMENT}
`;

export const GET_RGPD_AUTORISATION = gql`
  query RGPD_AUTORISATION($id: ID!) {
    rgpdAutorisation(id: $id) {
      ...RgpdAutorisationInfo
    }
  }
  ${RGPD_AUTORISATION_INFO_FRAGEMENT}
`;

export const GET_RGPD_AUTORISATIONS = gql`
  query RGPD_AUTORISATIONS($idGroupement: ID) {
    rgpdAutorisations(idGroupement: $idGroupement) {
      ...RgpdAutorisationInfo
    }
  }
  ${RGPD_AUTORISATION_INFO_FRAGEMENT}
`;

export const GET_RGPD_PARTENAIRE = gql`
  query RGPD_PARTENAIRE($id: ID!) {
    rgpdPartenaire(id: $id) {
      ...RgpdPartenaireInfo
    }
  }
  ${RGPD_PARTENAIRE_INFO_FRAGEMENT}
`;

export const GET_RGPD_PARTENAIRES = gql`
  query RGPD_PARTENAIRES($idGroupement: ID) {
    rgpdPartenaires(idGroupement: $idGroupement) {
      ...RgpdPartenaireInfo
    }
  }
  ${RGPD_PARTENAIRE_INFO_FRAGEMENT}
`;
