import gql from 'graphql-tag';
import { IDEE_BONNE_PRATIQUE_FRAGMENT } from './fragment';

export const DO_CREATE_UPDATE_IDEE_BONNE_PRATIQUE = gql`
  mutation CREATE_UPDATE_IDEE_BONNE_PRATIQUE($input: IdeeOuBonnePratiqueInput!) {
    createUpdateIdeeOuBonnePratique(input: $input) {
      ...IdeeOuBonnePratiqueInfo
    }
  }
  ${IDEE_BONNE_PRATIQUE_FRAGMENT}
`;

export const DO_DELETE_IDEE_BONNE_PRATIQUE = gql`
  mutation DELETE_IDEE_BONNE_PRATIQUE($ids: [String!]!) {
    softDeleteIdeeOuBonnePratiques(ids: $ids) {
      ...IdeeOuBonnePratiqueInfo
    }
  }
  ${IDEE_BONNE_PRATIQUE_FRAGMENT}
`;

export const DO_UPDATE_STATUS_IDEE_BONNE_PRATIQUE = gql`
  mutation UPDATE_STATUS_IDEE_BONNE_PRATIQUE(
    $id: ID!
    $status: IdeeOuBonnePratiqueStatus!
    $commentaire: String
  ) {
    updateIdeeOuBonnePratiqueStatus(id: $id, status: $status, commentaire: $commentaire) {
      ...IdeeOuBonnePratiqueInfo
    }
  }
  ${IDEE_BONNE_PRATIQUE_FRAGMENT}
`;

export const DO_CREATE_UPDATE_IDEE_BONNE_PRATIQUE_LU = gql`
  mutation CREATE_UPDATE_IDEE_BONNE_PRATIQUE_LU($input: IdeeOuBonnePratiqueLuInput!) {
    createUpdateIdeeOuBonnePratiqueLu(input: $input) {
      id
      ideeOuBonnePratique {
        id
        title
      }
      user {
        id
        userName
      }
    }
  }
`;
