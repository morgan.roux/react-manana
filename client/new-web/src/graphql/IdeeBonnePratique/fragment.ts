import gql from 'graphql-tag';
import { GROUPEMENT_INFO } from '../Groupement/fragement';
import { USER_INFO_FRAGEMENT } from '../User/fragment';

export const IDEE_BONNE_PRATIQUE_FRAGMENT = gql`
  fragment IdeeOuBonnePratiqueInfo on IdeeOuBonnePratique {
    id
    _origine: origine
    status
    nbLu
    lu
    nbSmyley
    nbComment
    nbPartage
    classification {
      id
      nom
      userCreation {
        ...UserInfo
      }
    }
    title
    auteur {
      ...UserInfo
    }
    concurent
    fournisseur {
      id
      nomLabo
      sortie
      laboSuite {
        id
        nom
      }
    }
    prestataire {
      id
      nom
      commentaire
    }
    groupeAmis {
      id
      nom
    }
    service {
      id
      nom
    }
    beneficiaires_cles
    contexte
    objectifs
    resultats
    facteursClesDeSucces
    contraintes
    groupement {
      ...GroupementInfo
    }
    codeMaj
    isRemoved
    userCreation {
      id
      userName
    }
    dateCreation
    dateModification
    ideeOuBonnePratiqueChangeStatus {
      id
      commentaire
      isRemoved
      status
    }
    ideeOuBonnePratiqueFichierJoints {
      id
      fichier {
        id
        publicUrl
        chemin
        nomOriginal
        type
      }
    }
    ideeOuBonnePratiqueLus {
      id
    }
    ideeOuBonnePratiqueSmyleys {
      id
    }
  }
  ${GROUPEMENT_INFO}
  ${USER_INFO_FRAGEMENT}
`;
