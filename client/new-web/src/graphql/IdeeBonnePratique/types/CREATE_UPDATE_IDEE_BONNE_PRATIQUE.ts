/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { IdeeOuBonnePratiqueInput, IdeeOuBonnePratiqueOrigine, IdeeOuBonnePratiqueStatus, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_IDEE_BONNE_PRATIQUE
// ====================================================

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_classification_userCreation_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_classification_userCreation_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_classification_userCreation_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_classification_userCreation_userPhoto_fichier | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_classification_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_classification_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_classification_userCreation_role | null;
  userPhoto: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_classification_userCreation_userPhoto | null;
  pharmacie: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_classification_userCreation_pharmacie | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_classification {
  __typename: "IdeeOuBonnePratiqueClassification";
  id: string;
  nom: string;
  userCreation: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_classification_userCreation | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_auteur_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_auteur_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_auteur_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_auteur_userPhoto_fichier | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_auteur_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_auteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_auteur_role | null;
  userPhoto: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_auteur_userPhoto | null;
  pharmacie: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_auteur_pharmacie | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_fournisseur_laboSuite {
  __typename: "LaboratoireSuite";
  id: string;
  nom: string | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_fournisseur {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
  laboSuite: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_fournisseur_laboSuite | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_prestataire {
  __typename: "Partenaire";
  id: string;
  nom: string | null;
  commentaire: string | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_groupeAmis {
  __typename: "GroupeAmis";
  id: string;
  nom: string | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_groupement_defaultPharmacie_departement_region | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_groupement_defaultPharmacie_departement | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_groupement_groupementLogo_fichier | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_groupement_defaultPharmacie | null;
  groupementLogo: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_groupement_groupementLogo | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_ideeOuBonnePratiqueChangeStatus {
  __typename: "IdeeOuBonnePratiqueChangeStatus";
  id: string;
  commentaire: string | null;
  isRemoved: boolean | null;
  status: IdeeOuBonnePratiqueStatus;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_ideeOuBonnePratiqueFichierJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_ideeOuBonnePratiqueFichierJoints {
  __typename: "IdeeOuBonnePratiqueFichierJoint";
  id: string;
  fichier: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_ideeOuBonnePratiqueFichierJoints_fichier;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_ideeOuBonnePratiqueLus {
  __typename: "IdeeOuBonnePratiqueLu";
  id: string;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_ideeOuBonnePratiqueSmyleys {
  __typename: "UserSmyley";
  id: string;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique {
  __typename: "IdeeOuBonnePratique";
  id: string;
  _origine: IdeeOuBonnePratiqueOrigine | null;
  status: IdeeOuBonnePratiqueStatus;
  nbLu: number;
  lu: boolean;
  nbSmyley: number;
  nbComment: number;
  nbPartage: number;
  classification: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_classification | null;
  title: string;
  auteur: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_auteur;
  concurent: string | null;
  fournisseur: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_fournisseur | null;
  prestataire: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_prestataire | null;
  groupeAmis: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_groupeAmis | null;
  service: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_service | null;
  beneficiaires_cles: string | null;
  contexte: string | null;
  objectifs: string | null;
  resultats: string | null;
  facteursClesDeSucces: string | null;
  contraintes: string | null;
  groupement: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_groupement | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  userCreation: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_userCreation | null;
  dateCreation: any | null;
  dateModification: any | null;
  ideeOuBonnePratiqueChangeStatus: (CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_ideeOuBonnePratiqueChangeStatus | null)[];
  ideeOuBonnePratiqueFichierJoints: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_ideeOuBonnePratiqueFichierJoints[];
  ideeOuBonnePratiqueLus: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_ideeOuBonnePratiqueLus[];
  ideeOuBonnePratiqueSmyleys: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique_ideeOuBonnePratiqueSmyleys[];
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE {
  createUpdateIdeeOuBonnePratique: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_createUpdateIdeeOuBonnePratique;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUEVariables {
  input: IdeeOuBonnePratiqueInput;
}
