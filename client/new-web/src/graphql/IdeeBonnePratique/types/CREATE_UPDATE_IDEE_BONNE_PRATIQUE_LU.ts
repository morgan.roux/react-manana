/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { IdeeOuBonnePratiqueLuInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_LU
// ====================================================

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_LU_createUpdateIdeeOuBonnePratiqueLu_ideeOuBonnePratique {
  __typename: "IdeeOuBonnePratique";
  id: string;
  title: string;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_LU_createUpdateIdeeOuBonnePratiqueLu_user {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_LU_createUpdateIdeeOuBonnePratiqueLu {
  __typename: "IdeeOuBonnePratiqueLu";
  id: string;
  ideeOuBonnePratique: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_LU_createUpdateIdeeOuBonnePratiqueLu_ideeOuBonnePratique;
  user: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_LU_createUpdateIdeeOuBonnePratiqueLu_user;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_LU {
  createUpdateIdeeOuBonnePratiqueLu: CREATE_UPDATE_IDEE_BONNE_PRATIQUE_LU_createUpdateIdeeOuBonnePratiqueLu;
}

export interface CREATE_UPDATE_IDEE_BONNE_PRATIQUE_LUVariables {
  input: IdeeOuBonnePratiqueLuInput;
}
