/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { IdeeOuBonnePratiqueOrigine, IdeeOuBonnePratiqueStatus, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: IDEE_BONNE_PRATIQUE
// ====================================================

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_classification_userCreation_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_classification_userCreation_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_classification_userCreation_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_classification_userCreation_userPhoto_fichier | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_classification_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_classification_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_classification_userCreation_role | null;
  userPhoto: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_classification_userCreation_userPhoto | null;
  pharmacie: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_classification_userCreation_pharmacie | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_classification {
  __typename: "IdeeOuBonnePratiqueClassification";
  id: string;
  nom: string;
  userCreation: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_classification_userCreation | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_auteur_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_auteur_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_auteur_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_auteur_userPhoto_fichier | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_auteur_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_auteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_auteur_role | null;
  userPhoto: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_auteur_userPhoto | null;
  pharmacie: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_auteur_pharmacie | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_fournisseur_laboSuite {
  __typename: "LaboratoireSuite";
  id: string;
  nom: string | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_fournisseur {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
  laboSuite: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_fournisseur_laboSuite | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_prestataire {
  __typename: "Partenaire";
  id: string;
  nom: string | null;
  commentaire: string | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_groupeAmis {
  __typename: "GroupeAmis";
  id: string;
  nom: string | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_groupement_defaultPharmacie_departement_region | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_groupement_defaultPharmacie_departement | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_groupement_groupementLogo_fichier | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_groupement_defaultPharmacie | null;
  groupementLogo: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_groupement_groupementLogo | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_ideeOuBonnePratiqueChangeStatus {
  __typename: "IdeeOuBonnePratiqueChangeStatus";
  id: string;
  commentaire: string | null;
  isRemoved: boolean | null;
  status: IdeeOuBonnePratiqueStatus;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_ideeOuBonnePratiqueFichierJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_ideeOuBonnePratiqueFichierJoints {
  __typename: "IdeeOuBonnePratiqueFichierJoint";
  id: string;
  fichier: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_ideeOuBonnePratiqueFichierJoints_fichier;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_ideeOuBonnePratiqueLus {
  __typename: "IdeeOuBonnePratiqueLu";
  id: string;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_ideeOuBonnePratiqueSmyleys {
  __typename: "UserSmyley";
  id: string;
}

export interface IDEE_BONNE_PRATIQUE_ideeOuBonnePratique {
  __typename: "IdeeOuBonnePratique";
  id: string;
  _origine: IdeeOuBonnePratiqueOrigine | null;
  status: IdeeOuBonnePratiqueStatus;
  nbLu: number;
  lu: boolean;
  nbSmyley: number;
  nbComment: number;
  nbPartage: number;
  classification: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_classification | null;
  title: string;
  auteur: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_auteur;
  concurent: string | null;
  fournisseur: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_fournisseur | null;
  prestataire: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_prestataire | null;
  groupeAmis: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_groupeAmis | null;
  service: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_service | null;
  beneficiaires_cles: string | null;
  contexte: string | null;
  objectifs: string | null;
  resultats: string | null;
  facteursClesDeSucces: string | null;
  contraintes: string | null;
  groupement: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_groupement | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  userCreation: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_userCreation | null;
  dateCreation: any | null;
  dateModification: any | null;
  ideeOuBonnePratiqueChangeStatus: (IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_ideeOuBonnePratiqueChangeStatus | null)[];
  ideeOuBonnePratiqueFichierJoints: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_ideeOuBonnePratiqueFichierJoints[];
  ideeOuBonnePratiqueLus: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_ideeOuBonnePratiqueLus[];
  ideeOuBonnePratiqueSmyleys: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique_ideeOuBonnePratiqueSmyleys[];
}

export interface IDEE_BONNE_PRATIQUE {
  ideeOuBonnePratique: IDEE_BONNE_PRATIQUE_ideeOuBonnePratique | null;
}

export interface IDEE_BONNE_PRATIQUEVariables {
  id: string;
}
