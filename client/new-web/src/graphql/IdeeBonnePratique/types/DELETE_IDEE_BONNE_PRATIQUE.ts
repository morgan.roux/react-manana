/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { IdeeOuBonnePratiqueOrigine, IdeeOuBonnePratiqueStatus, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_IDEE_BONNE_PRATIQUE
// ====================================================

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_classification_userCreation_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_classification_userCreation_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_classification_userCreation_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_classification_userCreation_userPhoto_fichier | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_classification_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_classification_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_classification_userCreation_role | null;
  userPhoto: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_classification_userCreation_userPhoto | null;
  pharmacie: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_classification_userCreation_pharmacie | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_classification {
  __typename: "IdeeOuBonnePratiqueClassification";
  id: string;
  nom: string;
  userCreation: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_classification_userCreation | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_auteur_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_auteur_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_auteur_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_auteur_userPhoto_fichier | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_auteur_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_auteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_auteur_role | null;
  userPhoto: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_auteur_userPhoto | null;
  pharmacie: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_auteur_pharmacie | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_fournisseur_laboSuite {
  __typename: "LaboratoireSuite";
  id: string;
  nom: string | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_fournisseur {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
  laboSuite: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_fournisseur_laboSuite | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_prestataire {
  __typename: "Partenaire";
  id: string;
  nom: string | null;
  commentaire: string | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_groupeAmis {
  __typename: "GroupeAmis";
  id: string;
  nom: string | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_groupement_defaultPharmacie_departement_region | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_groupement_defaultPharmacie_departement | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_groupement_groupementLogo_fichier | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_groupement_defaultPharmacie | null;
  groupementLogo: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_groupement_groupementLogo | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_ideeOuBonnePratiqueChangeStatus {
  __typename: "IdeeOuBonnePratiqueChangeStatus";
  id: string;
  commentaire: string | null;
  isRemoved: boolean | null;
  status: IdeeOuBonnePratiqueStatus;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_ideeOuBonnePratiqueFichierJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_ideeOuBonnePratiqueFichierJoints {
  __typename: "IdeeOuBonnePratiqueFichierJoint";
  id: string;
  fichier: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_ideeOuBonnePratiqueFichierJoints_fichier;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_ideeOuBonnePratiqueLus {
  __typename: "IdeeOuBonnePratiqueLu";
  id: string;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_ideeOuBonnePratiqueSmyleys {
  __typename: "UserSmyley";
  id: string;
}

export interface DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques {
  __typename: "IdeeOuBonnePratique";
  id: string;
  _origine: IdeeOuBonnePratiqueOrigine | null;
  status: IdeeOuBonnePratiqueStatus;
  nbLu: number;
  lu: boolean;
  nbSmyley: number;
  nbComment: number;
  nbPartage: number;
  classification: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_classification | null;
  title: string;
  auteur: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_auteur;
  concurent: string | null;
  fournisseur: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_fournisseur | null;
  prestataire: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_prestataire | null;
  groupeAmis: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_groupeAmis | null;
  service: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_service | null;
  beneficiaires_cles: string | null;
  contexte: string | null;
  objectifs: string | null;
  resultats: string | null;
  facteursClesDeSucces: string | null;
  contraintes: string | null;
  groupement: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_groupement | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  userCreation: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_userCreation | null;
  dateCreation: any | null;
  dateModification: any | null;
  ideeOuBonnePratiqueChangeStatus: (DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_ideeOuBonnePratiqueChangeStatus | null)[];
  ideeOuBonnePratiqueFichierJoints: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_ideeOuBonnePratiqueFichierJoints[];
  ideeOuBonnePratiqueLus: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_ideeOuBonnePratiqueLus[];
  ideeOuBonnePratiqueSmyleys: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques_ideeOuBonnePratiqueSmyleys[];
}

export interface DELETE_IDEE_BONNE_PRATIQUE {
  softDeleteIdeeOuBonnePratiques: DELETE_IDEE_BONNE_PRATIQUE_softDeleteIdeeOuBonnePratiques[];
}

export interface DELETE_IDEE_BONNE_PRATIQUEVariables {
  ids: string[];
}
