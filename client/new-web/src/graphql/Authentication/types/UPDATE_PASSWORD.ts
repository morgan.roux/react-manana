/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UPDATE_PASSWORD
// ====================================================

export interface UPDATE_PASSWORD_updatePassword {
  __typename: "UpdatePasswordPayload";
  success: boolean | null;
}

export interface UPDATE_PASSWORD {
  updatePassword: UPDATE_PASSWORD_updatePassword | null;
}

export interface UPDATE_PASSWORDVariables {
  lastPassword?: string | null;
  newPassword: string;
}
