/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: RESET_PASSWORD
// ====================================================

export interface RESET_PASSWORD_resetPassword {
  __typename: "LoginPayload";
  accessToken: string | null;
}

export interface RESET_PASSWORD {
  resetPassword: RESET_PASSWORD_resetPassword | null;
}

export interface RESET_PASSWORDVariables {
  password: string;
  token: string;
}
