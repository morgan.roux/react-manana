import gql from 'graphql-tag';

export const USER_SIGNIN = gql`
  mutation SIGNIN($login: String!, $password: String!, $ip: String) {
    login(login: $login, password: $password, ip: $ip) {
      accessToken
    }
  }
`;

export const USER_RESET_PASSWORD = gql`
  mutation RESET_PASSWORD($password: String!, $token: String!) {
    resetPassword(password: $password, token: $token) {
      accessToken
    }
  }
`;

export const USER_FORGOT_PASSWORD = gql`
  mutation FORGOT_PASSWORD($login: String!) {
    forgotPassword(login: $login)
  }
`;

export const USER_UPDATE_PASSWORD = gql`
  mutation UPDATE_PASSWORD($lastPassword: String, $newPassword: String!) {
    updatePassword(lastPassword: $lastPassword, newPassword: $newPassword) {
      success
    }
  }
`;
