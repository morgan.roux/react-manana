/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ActionStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_SHARED_TODO
// ====================================================

export interface SEARCH_SHARED_TODO_action_origine {
  __typename: "TodoActionOrigine";
  id: string;
  type: string;
}

export interface SEARCH_SHARED_TODO_action_project {
  __typename: "Project";
  id: string;
}

export interface SEARCH_SHARED_TODO_action {
  __typename: "Action";
  id: string;
  type: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  isInInbox: boolean | null;
  origine: SEARCH_SHARED_TODO_action_origine | null;
  status: ActionStatus | null;
  nbComment: number;
  project: SEARCH_SHARED_TODO_action_project | null;
}

export interface SEARCH_SHARED_TODO {
  action: SEARCH_SHARED_TODO_action | null;
}

export interface SEARCH_SHARED_TODOVariables {
  id: string;
}
