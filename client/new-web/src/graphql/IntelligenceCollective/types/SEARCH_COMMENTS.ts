/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_COMMENTS
// ====================================================

export interface SEARCH_COMMENTS_comments_data_userCreation_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface SEARCH_COMMENTS_comments_data_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
  role: SEARCH_COMMENTS_comments_data_userCreation_role | null;
}

export interface SEARCH_COMMENTS_comments_data {
  __typename: "Comment";
  id: string;
  content: string;
  dateCreation: any | null;
  userCreation: SEARCH_COMMENTS_comments_data_userCreation | null;
}

export interface SEARCH_COMMENTS_comments {
  __typename: "CommentResult";
  data: SEARCH_COMMENTS_comments_data[];
}

export interface SEARCH_COMMENTS {
  comments: SEARCH_COMMENTS_comments | null;
}

export interface SEARCH_COMMENTSVariables {
  codeItem: string;
  idItemAssocie: string;
}
