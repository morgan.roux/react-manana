/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_GROUPES_AMIS
// ====================================================

export interface SEARCH_GROUPES_AMIS_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Groupement" | "Pharmacie" | "Contact" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "IdeeOuBonnePratique" | "InformationLiaison" | "Ticket" | "TicketOrigine" | "TicketMotif" | "TicketStatut" | "TicketChangementCible" | "Partage" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementStatut" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_GROUPES_AMIS_search_data_GroupeAmis {
  __typename: "GroupeAmis";
  id: string;
  nom: string;
  nbPharmacie: number;
  nbMember: number;
  nbPartage: number;
  nbRecommandation: number;
  dateCreation: any;
}

export type SEARCH_GROUPES_AMIS_search_data = SEARCH_GROUPES_AMIS_search_data_Action | SEARCH_GROUPES_AMIS_search_data_GroupeAmis;

export interface SEARCH_GROUPES_AMIS_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_GROUPES_AMIS_search_data | null)[] | null;
}

export interface SEARCH_GROUPES_AMIS {
  search: SEARCH_GROUPES_AMIS_search | null;
}

export interface SEARCH_GROUPES_AMISVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  filterBy?: any | null;
  sortBy?: any | null;
  skip?: number | null;
  take?: number | null;
}
