/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_TSY_FANTATRO
// ====================================================

export interface SEARCH_TSY_FANTATRO_search_data_Partage {
  __typename: "Partage" | "Item" | "Avatar" | "User" | "Groupement" | "Pharmacie" | "Contact" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "TodoActionType" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "PartenaireServicePartenaire" | "Ppersonnel" | "GroupeAmis" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "IdeeOuBonnePratique" | "InformationLiaison" | "Ticket" | "TicketOrigine" | "TicketMotif" | "TicketStatut" | "TicketChangementCible" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementStatut" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_TSY_FANTATRO_search_data_Action {
  __typename: "Action";
  id: string;
}

export interface SEARCH_TSY_FANTATRO_search_data_Laboratoire_comments_data_user_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface SEARCH_TSY_FANTATRO_search_data_Laboratoire_comments_data_user {
  __typename: "User";
  id: string;
  userName: string | null;
  role: SEARCH_TSY_FANTATRO_search_data_Laboratoire_comments_data_user_role | null;
}

export interface SEARCH_TSY_FANTATRO_search_data_Laboratoire_comments_data {
  __typename: "Comment";
  id: string;
  content: string;
  dateCreation: any | null;
  user: SEARCH_TSY_FANTATRO_search_data_Laboratoire_comments_data_user;
}

export interface SEARCH_TSY_FANTATRO_search_data_Laboratoire_comments {
  __typename: "CommentResult";
  data: SEARCH_TSY_FANTATRO_search_data_Laboratoire_comments_data[];
}

export interface SEARCH_TSY_FANTATRO_search_data_Laboratoire_actions_data_origine {
  __typename: "TodoActionOrigine";
  id: string;
  type: string;
}

export interface SEARCH_TSY_FANTATRO_search_data_Laboratoire_actions_data {
  __typename: "Action";
  id: string;
  priority: number | null;
  ordre: number | null;
  description: string;
  dateEcheance: any | null;
  dateCreation: any | null;
  nbComment: number;
  origine: SEARCH_TSY_FANTATRO_search_data_Laboratoire_actions_data_origine | null;
}

export interface SEARCH_TSY_FANTATRO_search_data_Laboratoire_actions {
  __typename: "ActionResult";
  data: SEARCH_TSY_FANTATRO_search_data_Laboratoire_actions_data[];
}

export interface SEARCH_TSY_FANTATRO_search_data_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  comments: SEARCH_TSY_FANTATRO_search_data_Laboratoire_comments | null;
  actions: SEARCH_TSY_FANTATRO_search_data_Laboratoire_actions | null;
}

export interface SEARCH_TSY_FANTATRO_search_data_Partenaire_comments_data_user_role {
  __typename: "Role";
  nom: string | null;
}

export interface SEARCH_TSY_FANTATRO_search_data_Partenaire_comments_data_user {
  __typename: "User";
  userName: string | null;
  role: SEARCH_TSY_FANTATRO_search_data_Partenaire_comments_data_user_role | null;
}

export interface SEARCH_TSY_FANTATRO_search_data_Partenaire_comments_data {
  __typename: "Comment";
  dateCreation: any | null;
  user: SEARCH_TSY_FANTATRO_search_data_Partenaire_comments_data_user;
}

export interface SEARCH_TSY_FANTATRO_search_data_Partenaire_comments {
  __typename: "CommentResult";
  data: SEARCH_TSY_FANTATRO_search_data_Partenaire_comments_data[];
}

export interface SEARCH_TSY_FANTATRO_search_data_Partenaire {
  __typename: "Partenaire";
  id: string;
  comments: SEARCH_TSY_FANTATRO_search_data_Partenaire_comments | null;
}


export type SEARCH_TSY_FANTATRO_search_data = SEARCH_TSY_FANTATRO_search_data_Partage | SEARCH_TSY_FANTATRO_search_data_Action | SEARCH_TSY_FANTATRO_search_data_Laboratoire | SEARCH_TSY_FANTATRO_search_data_Partenaire;

// export type SEARCH_TSY_FANTATRO_search_data = SEARCH_TSY_FANTATRO_search_data_TodoActionType | SEARCH_TSY_FANTATRO_search_data_Action | SEARCH_TSY_FANTATRO_search_data_Laboratoire | SEARCH_TSY_FANTATRO_search_data_Partenaire;

// export type SEARCH_TSY_FANTATRO_search_data = SEARCH_TSY_FANTATRO_search_data_Partage | SEARCH_TSY_FANTATRO_search_data_Action | SEARCH_TSY_FANTATRO_search_data_Laboratoire | SEARCH_TSY_FANTATRO_search_data_Partenaire;


export interface SEARCH_TSY_FANTATRO_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_TSY_FANTATRO_search_data | null)[] | null;
}

export interface SEARCH_TSY_FANTATRO {
  search: SEARCH_TSY_FANTATRO_search | null;
}

export interface SEARCH_TSY_FANTATROVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  filterBy?: any | null;
  sortBy?: any | null;
  skip?: number | null;
  take?: number | null;
}
