import gql from 'graphql-tag';
import { ACTION_INFO_FRAGEMENT } from '../Action/fragment';
import { COMMENT_INFO_FRAGEMENT } from '../Comment/fragment';
import { TICKET_INFO_FRAGMENT } from '../Ticket/fragment';

const DO_SEARCH_PARTENAIRE = gql`
  query SEARCH_PARTENAIRE(
    $type: [String]
    $query: JSON
    $filterBy: JSON
    $sortBy: JSON
    $skip: Int
    $take: Int
  ) {
    search(
      type: $type
      query: $query
      filterBy: $filterBy
      sortBy: $sortBy
      skip: $skip
      take: $take
    ) {
      total
      data {
        ... on Partenaire {
          id
          nom
          partenaireServiceSuite {
            id
            adresse1
            codePostal
            telephone
            ville
            email
            dateCreation
          }
          partenaireServicePartenaire {
            id
            typePartenaire
            statutPartenaire
            dateFinPartenaire
            dateDebutPartenaire
          }
          services {
            id
            type
            nom
          }
        }
      }
    }
  }
`;

const DO_SEARCH_SHARED_TODO = gql`
  query SEARCH_SHARED_TODO($id: ID!) {
    action(id: $id) {
      id
      type
      ordre
      description
      dateDebut
      dateFin
      priority
      isInInbox
      origine {
        id
        type
      }
      status
      nbComment
      project {
        id
      }
    }
  }
`;

const DO_SEARCH_COMMENTS = gql`
  query SEARCH_COMMENTS($codeItem: String!, $idItemAssocie: String!) {
    comments(codeItem: $codeItem, idItemAssocie: $idItemAssocie) {
      data {
        id
        content
        dateCreation
        userCreation {
          id
          userName
          role {
            id
            nom
          }
        }
      }
    }
  }
`;

const GET_SHARED_INFO = gql`
  query SEARCH_SHARED_INFO(
    $type: [String]
    $query: JSON
    $filterBy: JSON
    $sortBy: JSON
    $skip: Int
    $take: Int
  ) {
    search(
      type: $type
      query: $query
      filterBy: $filterBy
      sortBy: $sortBy
      skip: $skip
      take: $take
    ) {
      total
      data {
        ... on Action {
          ...ActionInfo
        }
        ... on Ticket {
          ...TicketInfo
        }
        ... on InformationLiaison {
          id
          idOrigine
          idOrigineAssocie
          origineAssocie{
            ...on Laboratoire{
              id
              type  
              nomLabo
            }
            ...on Service{
              type
              id
              code
              nom
            }
            ...on Partenaire{
              type
              id
              nom
            }

            ...on GroupeAmis{
              type
              id
              nom
            }
            ... on User{
              type
              id
              userName
            }
          }

          _statusInformation: statut
          _priorityInfo: priority
          _descriptionInfo: description
          _nbComment: nbComment
          nbCollegue
          nbLue
          declarant {
            id
            userName
          }
          # idFicheIncident
          # idFicheAmelioration
          ficheReclamation {
            id
            commentaire
          }
          userCreation {
            id
            userName
          }
          _dateCreationInfo: dateCreation
        }
      }
    }
  }
  ${ACTION_INFO_FRAGEMENT}
  ${TICKET_INFO_FRAGMENT}
`;

const GET_SHARED_COMMENTS = gql`
  query SHARED_COMMENTS(
    $type: [String]
    $query: JSON
    $filterBy: JSON
    $sortBy: JSON
    $skip: Int
    $take: Int
  ) {
    search(
      type: $type
      query: $query
      filterBy: $filterBy
      sortBy: $sortBy
      skip: $skip
      take: $take
    ) {
      total
      data {
        ... on Comment {
          ...CommentInfo
        }
      }
    }
  }
  ${COMMENT_INFO_FRAGEMENT}
`;

export {
  DO_SEARCH_PARTENAIRE,
  DO_SEARCH_SHARED_TODO,
  GET_SHARED_INFO,
  DO_SEARCH_COMMENTS,
  GET_SHARED_COMMENTS,
};
