/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: RESSOURCE
// ====================================================

export interface RESSOURCE_ressource_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
}

export interface RESSOURCE_ressource_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface RESSOURCE_ressource {
  __typename: "Ressource";
  id: string;
  typeRessource: string | null;
  item: RESSOURCE_ressource_item | null;
  code: string | null;
  libelle: string | null;
  fichier: RESSOURCE_ressource_fichier | null;
  url: string | null;
}

export interface RESSOURCE {
  ressource: RESSOURCE_ressource | null;
}

export interface RESSOURCEVariables {
  id: string;
}
