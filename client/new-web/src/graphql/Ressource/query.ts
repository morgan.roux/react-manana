import gql from 'graphql-tag';

export const GET_SEARCH_CUSTOM_CONTENT_RESSOURCE = gql`
  query SEARCH_CUSTOM_CONTENT_RESSOURCE($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Ressource {
          id
          typeRessource
          item {
            id
            code
            name
            codeItem
          }
          code
          libelle
          dateChargement
          url
        }
      }
    }
  }
`;

export const GET_RESSOURCE = gql`
  query RESSOURCE($id: ID!) {
    ressource(id: $id) {
      id
      typeRessource
      item {
        id
        code
        name
        codeItem
      }
      code
      libelle
      fichier {
        id
        chemin
        nomOriginal
        urlPresigned
        publicUrl
      }
      url
    }
  }
`;
