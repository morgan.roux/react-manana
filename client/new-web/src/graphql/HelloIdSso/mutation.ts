import gql from 'graphql-tag';
import { HELLOIDSSO_FRAGMENT } from './fragment';

export const DO_CREATE_HELLOID_SSO = gql`
  mutation CREATE_HELLOID_SSO(
    $idGroupement: ID!, 
    $helloIdUrl: String, 
    $helloIDConsumerUrl: String, 
    $x509Certificate: String, 
    $apiKey: String,
    $apiPass: String
    ) {
      createHelloIdSso(
        idGroupement: $idGroupement, 
        helloIdUrl: $helloIdUrl, 
        helloIDConsumerUrl: $helloIDConsumerUrl, 
        x509Certificate: $x509Certificate, 
        apiKey: $apiKey,
        apiPass: $apiPass
        ) {
          ...HelloIdSsoInfo
    }
  }
  ${HELLOIDSSO_FRAGMENT}
`;

export const DO_UPDATE_HELLOID_SSO = gql`
  mutation UPDATE_HELLOID_SSO(
    $id: ID!
    $helloIdUrl: String, 
    $helloIDConsumerUrl: String, 
    $x509Certificate: String, 
    $apiKey: String,
    $apiPass: String
    ) {
      updateHelloIdSso(
        id: $id, 
        helloIdUrl: $helloIdUrl, 
        helloIDConsumerUrl: $helloIDConsumerUrl, 
        x509Certificate: $x509Certificate, 
        apiKey: $apiKey,
        apiPass: $apiPass
        ) {
          ...HelloIdSsoInfo
    }
  }
  ${HELLOIDSSO_FRAGMENT}
`;