/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CREATE_HELLOID_SSO
// ====================================================

export interface CREATE_HELLOID_SSO_createHelloIdSso_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface CREATE_HELLOID_SSO_createHelloIdSso_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: CREATE_HELLOID_SSO_createHelloIdSso_groupement_defaultPharmacie_departement_region | null;
}

export interface CREATE_HELLOID_SSO_createHelloIdSso_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: CREATE_HELLOID_SSO_createHelloIdSso_groupement_defaultPharmacie_departement | null;
}

export interface CREATE_HELLOID_SSO_createHelloIdSso_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface CREATE_HELLOID_SSO_createHelloIdSso_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: CREATE_HELLOID_SSO_createHelloIdSso_groupement_groupementLogo_fichier | null;
}

export interface CREATE_HELLOID_SSO_createHelloIdSso_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: CREATE_HELLOID_SSO_createHelloIdSso_groupement_defaultPharmacie | null;
  groupementLogo: CREATE_HELLOID_SSO_createHelloIdSso_groupement_groupementLogo | null;
}

export interface CREATE_HELLOID_SSO_createHelloIdSso {
  __typename: "HelloIdSso";
  id: string;
  groupement: CREATE_HELLOID_SSO_createHelloIdSso_groupement | null;
  helloIdUrl: string | null;
  helloIDConsumerUrl: string | null;
  x509Certificate: string | null;
  apiKey: string | null;
  apiPass: string | null;
}

export interface CREATE_HELLOID_SSO {
  createHelloIdSso: CREATE_HELLOID_SSO_createHelloIdSso | null;
}

export interface CREATE_HELLOID_SSOVariables {
  idGroupement: string;
  helloIdUrl?: string | null;
  helloIDConsumerUrl?: string | null;
  x509Certificate?: string | null;
  apiKey?: string | null;
  apiPass?: string | null;
}
