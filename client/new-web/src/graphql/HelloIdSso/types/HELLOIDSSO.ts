/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: HELLOIDSSO
// ====================================================

export interface HELLOIDSSO_helloIdSsoByGroupementId_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface HELLOIDSSO_helloIdSsoByGroupementId_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: HELLOIDSSO_helloIdSsoByGroupementId_groupement_defaultPharmacie_departement_region | null;
}

export interface HELLOIDSSO_helloIdSsoByGroupementId_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: HELLOIDSSO_helloIdSsoByGroupementId_groupement_defaultPharmacie_departement | null;
}

export interface HELLOIDSSO_helloIdSsoByGroupementId_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface HELLOIDSSO_helloIdSsoByGroupementId_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: HELLOIDSSO_helloIdSsoByGroupementId_groupement_groupementLogo_fichier | null;
}

export interface HELLOIDSSO_helloIdSsoByGroupementId_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: HELLOIDSSO_helloIdSsoByGroupementId_groupement_defaultPharmacie | null;
  groupementLogo: HELLOIDSSO_helloIdSsoByGroupementId_groupement_groupementLogo | null;
}

export interface HELLOIDSSO_helloIdSsoByGroupementId {
  __typename: "HelloIdSso";
  id: string;
  groupement: HELLOIDSSO_helloIdSsoByGroupementId_groupement | null;
  helloIdUrl: string | null;
  helloIDConsumerUrl: string | null;
  x509Certificate: string | null;
  apiKey: string | null;
  apiPass: string | null;
}

export interface HELLOIDSSO {
  helloIdSsoByGroupementId: HELLOIDSSO_helloIdSsoByGroupementId | null;
}

export interface HELLOIDSSOVariables {
  idgroupement: string;
}
