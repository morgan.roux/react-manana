import gql from 'graphql-tag';
import { HELLOIDSSO_FRAGMENT } from './fragment';

export const GET_HELLOID_GROUPEMENT = gql`
  query HELLOIDSSO($idgroupement: ID!) {
    helloIdSsoByGroupementId(idgroupement: $idgroupement) {
        ...HelloIdSsoInfo
    }
  }
  ${HELLOIDSSO_FRAGMENT}
`;
