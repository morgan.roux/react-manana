/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: HELLOIDGROUPS
// ====================================================

export interface HELLOIDGROUPS_helloidGroups_application {
  __typename: "Application";
  applicationGUID: string;
  name: string | null;
  type: string | null;
  url: string | null;
  icon: string | null;
  needConfiguration: boolean | null;
  options: number | null;
  settingOptions: number | null;
  isNew: boolean | null;
  lastTimeUsed: any | null;
  nrofTimesUsed: number | null;
  helloIdurl: string | null;
  iconlink: string | null;
}

export interface HELLOIDGROUPS_helloidGroups {
  __typename: "HelloidGroup";
  name: string | null;
  groupGuid: string | null;
  managedByUserGuid: string | null;
  immutableId: string | null;
  isEnabled: boolean | null;
  isDefault: boolean | null;
  isQrEnabled: boolean | null;
  isDeleted: boolean | null;
  source: string | null;
  application: (HELLOIDGROUPS_helloidGroups_application | null)[] | null;
}

export interface HELLOIDGROUPS {
  helloidGroups: (HELLOIDGROUPS_helloidGroups | null)[] | null;
}

export interface HELLOIDGROUPSVariables {
  idgroupement: string;
}
