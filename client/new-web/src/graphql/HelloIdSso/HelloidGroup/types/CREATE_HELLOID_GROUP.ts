/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CREATE_HELLOID_GROUP
// ====================================================

export interface CREATE_HELLOID_GROUP_createHelloIdGroup_application {
  __typename: "Application";
  applicationGUID: string;
  name: string | null;
  type: string | null;
  url: string | null;
  icon: string | null;
  needConfiguration: boolean | null;
  options: number | null;
  settingOptions: number | null;
  isNew: boolean | null;
  lastTimeUsed: any | null;
  nrofTimesUsed: number | null;
  helloIdurl: string | null;
  iconlink: string | null;
}

export interface CREATE_HELLOID_GROUP_createHelloIdGroup {
  __typename: "HelloidGroup";
  name: string | null;
  groupGuid: string | null;
  managedByUserGuid: string | null;
  immutableId: string | null;
  isEnabled: boolean | null;
  isDefault: boolean | null;
  isQrEnabled: boolean | null;
  isDeleted: boolean | null;
  source: string | null;
  application: (CREATE_HELLOID_GROUP_createHelloIdGroup_application | null)[] | null;
}

export interface CREATE_HELLOID_GROUP {
  createHelloIdGroup: CREATE_HELLOID_GROUP_createHelloIdGroup | null;
}

export interface CREATE_HELLOID_GROUPVariables {
  idgroupement: string;
  Name?: string | null;
  IsEnabled?: boolean | null;
  IsDefault?: boolean | null;
  IsQrEnabled?: boolean | null;
  ApplicationGUIDs?: (string | null)[] | null;
}
