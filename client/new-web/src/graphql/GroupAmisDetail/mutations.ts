import gql from 'graphql-tag';

export const DO_CREATE_INVITE_FRIENDS = gql`
  mutation CREATE_UPDATE_INVITE_FRIENDS($input: GroupeAmisDetailInput!) {
    createUpdateGroupeAmisDetail(input: $input) {
      id
    }
  }
`;

export const DO_INVITE_PHARMACIE = gql`
  mutation CREATE_INVITE_PHARMACIE($id: ID!, $idsPharmacies: [ID!]!) {
    invitePharmacies(id: $id, idsPharmacies: $idsPharmacies) {
      id
      type
      groupeAmis {
        id
        nom
      }
      pharmacie {
        id
        nom
      }
    }
  }
`;
