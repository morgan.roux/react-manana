/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GroupeAmisDetails
// ====================================================

export interface GroupeAmisDetails_groupeAmisDetails {
  __typename: "GroupeAmisDetail";
  id: string;
}

export interface GroupeAmisDetails {
  groupeAmisDetails: GroupeAmisDetails_groupeAmisDetails[];
}
