/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CREATE_INVITE_PHARMACIE
// ====================================================

export interface CREATE_INVITE_PHARMACIE_invitePharmacies_groupeAmis {
  __typename: "GroupeAmis";
  id: string;
  nom: string | null;
}

export interface CREATE_INVITE_PHARMACIE_invitePharmacies_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CREATE_INVITE_PHARMACIE_invitePharmacies {
  __typename: "GroupeAmisDetail";
  id: string;
  type: string;
  groupeAmis: CREATE_INVITE_PHARMACIE_invitePharmacies_groupeAmis;
  pharmacie: CREATE_INVITE_PHARMACIE_invitePharmacies_pharmacie;
}

export interface CREATE_INVITE_PHARMACIE {
  invitePharmacies: CREATE_INVITE_PHARMACIE_invitePharmacies[] | null;
}

export interface CREATE_INVITE_PHARMACIEVariables {
  id: string;
  idsPharmacies: string[];
}
