/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GroupeAmisDetailInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_INVITE_FRIENDS
// ====================================================

export interface CREATE_UPDATE_INVITE_FRIENDS_createUpdateGroupeAmisDetail {
  __typename: "GroupeAmisDetail";
  id: string;
}

export interface CREATE_UPDATE_INVITE_FRIENDS {
  createUpdateGroupeAmisDetail: CREATE_UPDATE_INVITE_FRIENDS_createUpdateGroupeAmisDetail;
}

export interface CREATE_UPDATE_INVITE_FRIENDSVariables {
  input: GroupeAmisDetailInput;
}
