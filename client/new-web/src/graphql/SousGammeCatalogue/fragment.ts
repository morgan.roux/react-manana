import gql from 'graphql-tag';
import { GAMME_CATALOGUE_INFO } from '../GammeCatalogue/fragment';
export const SOUS_GAMME_CATALOGUE_INFO = gql`
  fragment SousGammeCatalogueInfo on SousGammeCatalogue {
    id
    codeSousGamme
    libelle
    numOrdre
    estDefaut
    gammeCatalogue {
      ...GammeCatalogueInfo
    }
  }
  ${GAMME_CATALOGUE_INFO}
`;
