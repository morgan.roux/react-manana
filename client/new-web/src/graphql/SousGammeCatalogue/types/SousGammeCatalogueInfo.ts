/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SousGammeCatalogueInfo
// ====================================================

export interface SousGammeCatalogueInfo_gammeCatalogue {
  __typename: "GammeCatalogue";
  id: string;
  codeGamme: number | null;
  libelle: string | null;
  numOrdre: number | null;
  estDefaut: number | null;
}

export interface SousGammeCatalogueInfo {
  __typename: "SousGammeCatalogue";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
  numOrdre: number | null;
  estDefaut: number | null;
  gammeCatalogue: SousGammeCatalogueInfo_gammeCatalogue | null;
}
