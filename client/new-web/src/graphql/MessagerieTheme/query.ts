import gql from 'graphql-tag';
import { MESSAGERIE_THEME_INFO_FRAGMENT } from './fragment';

export const GET_MESSAGERIE_THEMES = gql`
  query MESSAGERIE_THEMES {
    messagerieThemes {
      ...MessagerieThemeInfo
    }
  }
  ${MESSAGERIE_THEME_INFO_FRAGMENT}
`;
