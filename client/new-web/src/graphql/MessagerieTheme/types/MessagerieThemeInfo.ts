/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: MessagerieThemeInfo
// ====================================================

export interface MessagerieThemeInfo {
  __typename: "MessagerieTheme";
  id: string;
  nom: string | null;
}
