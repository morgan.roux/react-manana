import gql from 'graphql-tag';

export const PRESIGNED_URL_INFO_FRAGEMENT = gql`
  fragment PresignedUrlInfo on PresignedUrl {
    filePath
    presignedUrl
  }
`;
