/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CREATE_GET_PESIGNED_URL
// ====================================================

export interface CREATE_GET_PESIGNED_URL_createGetPresignedUrls {
  __typename: 'PresignedUrl';
  filePath: string;
  presignedUrl: string;
}

export interface CREATE_GET_PESIGNED_URL {
  createGetPresignedUrls: (CREATE_GET_PESIGNED_URL_createGetPresignedUrls | null)[] | null;
}

export interface CREATE_GET_PESIGNED_URLVariables {
  filePaths?: (string | null)[] | null;
}
