/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PresignedUrlInfo
// ====================================================

export interface PresignedUrlInfo {
  __typename: "PresignedUrl";
  filePath: string;
  presignedUrl: string;
}
