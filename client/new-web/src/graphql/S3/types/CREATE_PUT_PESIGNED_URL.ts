/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CREATE_PUT_PESIGNED_URL
// ====================================================

export interface CREATE_PUT_PESIGNED_URL_createPutPresignedUrls {
  __typename: "PresignedUrl";
  filePath: string;
  presignedUrl: string;
}

export interface CREATE_PUT_PESIGNED_URL {
  createPutPresignedUrls: (CREATE_PUT_PESIGNED_URL_createPutPresignedUrls | null)[] | null;
}

export interface CREATE_PUT_PESIGNED_URLVariables {
  filePaths?: (string | null)[] | null;
}
