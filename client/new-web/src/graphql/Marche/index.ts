import { DO_CREATE_UPDATE_MARCHE, DO_DELETE_MARCHE } from './mutation';
import { GET_MARCHE } from './query';

export { DO_CREATE_UPDATE_MARCHE, DO_DELETE_MARCHE, GET_MARCHE };
