/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: MarcheLaboratoireInfo
// ====================================================

export interface MarcheLaboratoireInfo_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface MarcheLaboratoireInfo {
  __typename: "MarcheLaboratoire";
  id: string;
  numOrdre: number | null;
  obligatoire: boolean | null;
  laboratoire: MarcheLaboratoireInfo_laboratoire | null;
}
