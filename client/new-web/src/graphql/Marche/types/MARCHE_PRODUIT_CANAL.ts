/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MarcheType, MarcheStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: MARCHE_PRODUIT_CANAL
// ====================================================

export interface MARCHE_PRODUIT_CANAL_marche_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface MARCHE_PRODUIT_CANAL_marche_canalArticles_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface MARCHE_PRODUIT_CANAL_marche_canalArticles_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  libelle2: string | null;
  produitCode: MARCHE_PRODUIT_CANAL_marche_canalArticles_produit_produitCode | null;
}

export interface MARCHE_PRODUIT_CANAL_marche_canalArticles {
  __typename: "ProduitCanal";
  id: string;
  produit: MARCHE_PRODUIT_CANAL_marche_canalArticles_produit | null;
}

export interface MARCHE_PRODUIT_CANAL_marche {
  __typename: "Marche";
  id: string;
  nom: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  marcheType: MarcheType | null;
  avecPalier: boolean | null;
  status: MarcheStatus | null;
  nbAdhesion: number | null;
  nbPalier: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  commandeCanal: MARCHE_PRODUIT_CANAL_marche_commandeCanal | null;
  canalArticles: (MARCHE_PRODUIT_CANAL_marche_canalArticles | null)[] | null;
}

export interface MARCHE_PRODUIT_CANAL {
  marche: MARCHE_PRODUIT_CANAL_marche | null;
}

export interface MARCHE_PRODUIT_CANALVariables {
  id: string;
}
