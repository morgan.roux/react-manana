/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MarcheType, MarcheStatus, MarcheLaboratoireInput, GroupeClienRemisetInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_MARCHE
// ====================================================

export interface CREATE_UPDATE_MARCHE_createUpdateMarche_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface CREATE_UPDATE_MARCHE_createUpdateMarche_canalArticles_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface CREATE_UPDATE_MARCHE_createUpdateMarche_canalArticles_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  libelle2: string | null;
  produitCode: CREATE_UPDATE_MARCHE_createUpdateMarche_canalArticles_produit_produitCode | null;
}

export interface CREATE_UPDATE_MARCHE_createUpdateMarche_canalArticles {
  __typename: "ProduitCanal";
  id: string;
  produit: CREATE_UPDATE_MARCHE_createUpdateMarche_canalArticles_produit | null;
}

export interface CREATE_UPDATE_MARCHE_createUpdateMarche_laboratoires_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface CREATE_UPDATE_MARCHE_createUpdateMarche_laboratoires {
  __typename: "MarcheLaboratoire";
  id: string;
  numOrdre: number | null;
  obligatoire: boolean | null;
  laboratoire: CREATE_UPDATE_MARCHE_createUpdateMarche_laboratoires_laboratoire | null;
}

export interface CREATE_UPDATE_MARCHE_createUpdateMarche_groupeClients_groupeClient {
  __typename: "GroupeClient";
  id: string;
  codeGroupe: number | null;
}

export interface CREATE_UPDATE_MARCHE_createUpdateMarche_groupeClients_remise_remiseDetails {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  remiseSupplementaire: number | null;
}

export interface CREATE_UPDATE_MARCHE_createUpdateMarche_groupeClients_remise {
  __typename: "Remise";
  id: string;
  remiseDetails: (CREATE_UPDATE_MARCHE_createUpdateMarche_groupeClients_remise_remiseDetails | null)[] | null;
}

export interface CREATE_UPDATE_MARCHE_createUpdateMarche_groupeClients {
  __typename: "MarcheGroupeClient";
  id: string;
  groupeClient: CREATE_UPDATE_MARCHE_createUpdateMarche_groupeClients_groupeClient | null;
  remise: CREATE_UPDATE_MARCHE_createUpdateMarche_groupeClients_remise | null;
}

export interface CREATE_UPDATE_MARCHE_createUpdateMarche {
  __typename: "Marche";
  type: string;
  id: string;
  nom: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  marcheType: MarcheType | null;
  avecPalier: boolean | null;
  status: MarcheStatus | null;
  nbAdhesion: number | null;
  nbPalier: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  commandeCanal: CREATE_UPDATE_MARCHE_createUpdateMarche_commandeCanal | null;
  canalArticles: (CREATE_UPDATE_MARCHE_createUpdateMarche_canalArticles | null)[] | null;
  laboratoires: (CREATE_UPDATE_MARCHE_createUpdateMarche_laboratoires | null)[] | null;
  groupeClients: (CREATE_UPDATE_MARCHE_createUpdateMarche_groupeClients | null)[] | null;
}

export interface CREATE_UPDATE_MARCHE {
  /**
   * Marché
   */
  createUpdateMarche: CREATE_UPDATE_MARCHE_createUpdateMarche | null;
}

export interface CREATE_UPDATE_MARCHEVariables {
  id?: string | null;
  nom: string;
  dateDebut: string;
  dateFin: string;
  marcheType: MarcheType;
  avecPalier: boolean;
  status: MarcheStatus;
  codeCanal: string;
  laboratoires?: (MarcheLaboratoireInput | null)[] | null;
  idsCanalArticle?: (string | null)[] | null;
  groupeClients: GroupeClienRemisetInput[];
}
