/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PERSONNEL_FONTIONS
// ====================================================

export interface PERSONNEL_FONTIONS_personnelFonctions_data {
  __typename: "PersonnelFonction";
  id: string;
  nom: string;
  code: string;
}

export interface PERSONNEL_FONTIONS_personnelFonctions {
  __typename: "PersonnelFonctionResult";
  total: number;
  data: (PERSONNEL_FONTIONS_personnelFonctions_data | null)[] | null;
}

export interface PERSONNEL_FONTIONS {
  personnelFonctions: PERSONNEL_FONTIONS_personnelFonctions | null;
}

export interface PERSONNEL_FONTIONSVariables {
  take?: number | null;
  skip?: number | null;
}
