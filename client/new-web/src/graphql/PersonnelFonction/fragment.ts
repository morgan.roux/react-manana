import gql from 'graphql-tag';

export const PERSONNEL_FONCTION_INFO_FRAGMENT = gql`
  fragment PersonnelFonctionInfo on PersonnelFonction {
    id
    nom
    code
  }
`;
