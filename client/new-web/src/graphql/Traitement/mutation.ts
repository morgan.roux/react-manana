import gql from 'graphql-tag';
import { ROLE_INFO_FRAGEMENT } from '../Role/fragment';

export const DO_CREATE_UPDATE_ROLE_TRAITEMENT = gql`
  mutation CREATE_UPDATE_ROLE_TRAITEMENT($codeRole: String!, $codeTraitements: [String]) {
    createUpdateRoleTraitement(codeRole: $codeRole, codeTraitements: $codeTraitements) {
      ...RoleInfo
      roleTraitements {
        codeTraitement
      }
    }
  }
  ${ROLE_INFO_FRAGEMENT}
`;
