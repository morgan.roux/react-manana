import gql from 'graphql-tag';
import { TRAITEMENT_FRAGMENT } from './fragment';
import { ROLE_INFO_FRAGEMENT } from '../Role/fragment';

export const GET_TRAITEMENT_SEARCH = gql`
  query TRAITEMENT_SEARCH($type: [String], $skip: Int, $take: Int, $sortBy: JSON) {
    search(type: $type, take: $take, skip: $skip, sortBy: $sortBy) {
      total
      data {
        ... on Traitement {
          ...TraitementInfo
          roles {
            ...RoleInfo
          }
        }
      }
    }
  }
  ${TRAITEMENT_FRAGMENT}
  ${ROLE_INFO_FRAGEMENT}
`;
