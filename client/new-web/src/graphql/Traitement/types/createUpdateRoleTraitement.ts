/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: createUpdateRoleTraitement
// ====================================================

export interface createUpdateRoleTraitement_createUpdateRoleTraitement {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface createUpdateRoleTraitement {
  createUpdateRoleTraitement: createUpdateRoleTraitement_createUpdateRoleTraitement | null;
}

export interface createUpdateRoleTraitementVariables {
  codeRole: string;
  codeTraitements?: (string | null)[] | null;
}
