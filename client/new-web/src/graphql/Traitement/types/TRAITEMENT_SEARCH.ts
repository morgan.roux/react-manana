/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: TRAITEMENT_SEARCH
// ====================================================

export interface TRAITEMENT_SEARCH_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris";
}

export interface TRAITEMENT_SEARCH_search_data_Traitement_roles {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface TRAITEMENT_SEARCH_search_data_Traitement {
  __typename: "Traitement";
  type: string;
  id: string;
  nom: string | null;
  code: string | null;
  roles: (TRAITEMENT_SEARCH_search_data_Traitement_roles | null)[] | null;
}

export type TRAITEMENT_SEARCH_search_data = TRAITEMENT_SEARCH_search_data_Action | TRAITEMENT_SEARCH_search_data_Traitement;

export interface TRAITEMENT_SEARCH_search {
  __typename: "SearchResult";
  total: number;
  data: (TRAITEMENT_SEARCH_search_data | null)[] | null;
}

export interface TRAITEMENT_SEARCH {
  search: TRAITEMENT_SEARCH_search | null;
}

export interface TRAITEMENT_SEARCHVariables {
  type?: (string | null)[] | null;
  skip?: number | null;
  take?: number | null;
  sortBy?: any | null;
}
