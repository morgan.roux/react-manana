/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TraitementInfo
// ====================================================

export interface TraitementInfo {
  __typename: "Traitement";
  type: string;
  id: string;
  nom: string | null;
  code: string | null;
}
