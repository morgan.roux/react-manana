/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GroupeAmisInput, GroupeAmisType, GroupeAmisStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_GROUPE_AMIS
// ====================================================

export interface CREATE_UPDATE_GROUPE_AMIS_createUpdateGroupeAmis {
  __typename: "GroupeAmis";
  id: string;
  nom: string | null;
  description: string | null;
  typeGroupeAmis: GroupeAmisType;
  nbPharmacie: number;
  groupStatus: GroupeAmisStatus;
  nbMember: number;
  nbPartage: number;
  nbRecommandation: number;
  dateCreation: any | null;
}

export interface CREATE_UPDATE_GROUPE_AMIS {
  createUpdateGroupeAmis: CREATE_UPDATE_GROUPE_AMIS_createUpdateGroupeAmis;
}

export interface CREATE_UPDATE_GROUPE_AMISVariables {
  input: GroupeAmisInput;
}
