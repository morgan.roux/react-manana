import gql from 'graphql-tag';

export const TRANCHE_CA_INFO_FRAGMENT = gql`
  fragment TrancheCAInfo on TrancheCA {
    id
    libelle
    valMax
    valMin
  }
`;
