/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: TRANCHECAS
// ====================================================

export interface TRANCHECAS_trancheCAs {
  __typename: "TrancheCA";
  id: string;
  libelle: string | null;
}

export interface TRANCHECAS {
  trancheCAs: (TRANCHECAS_trancheCAs | null)[] | null;
}
