/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: TRANCHE_CA_S
// ====================================================

export interface TRANCHE_CA_S_trancheCAs {
  __typename: "TrancheCA";
  id: string;
  libelle: string | null;
}

export interface TRANCHE_CA_S {
  trancheCAs: (TRANCHE_CA_S_trancheCAs | null)[] | null;
}
