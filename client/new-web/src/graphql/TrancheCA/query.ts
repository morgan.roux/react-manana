import gql from 'graphql-tag';

export const GET_TRANCHE_CA_S = gql`
  query TRANCHE_CA_S {
    trancheCAs {
      id
      libelle
    }
  }
`;
