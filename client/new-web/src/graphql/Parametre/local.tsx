import gql from 'graphql-tag';
export const DO_GET_PARAMETER_LOCAL = gql`
  {
    parameters @client {
      id
      code
      name
      defaultValue
      dateCreation
      dateModification
      category
      options {
        label
        value
      }
      parameterGroupe {
        id
        code
        nom
      }
      parameterType {
        id
        code
        nom
      }
      value {
        id
        value
      }
    }
  }
`;
