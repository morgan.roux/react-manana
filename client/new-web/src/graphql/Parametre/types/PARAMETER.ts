/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ParamCategory } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: PARAMETER
// ====================================================

export interface PARAMETER_parameter_options {
  __typename: "ParameterOption";
  label: string | null;
  value: string | null;
}

export interface PARAMETER_parameter_parameterGroupe {
  __typename: "ParameterGroupe";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PARAMETER_parameter_parameterType {
  __typename: "ParameterType";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PARAMETER_parameter_value {
  __typename: "ParameterValue";
  id: string;
  value: string | null;
}

export interface PARAMETER_parameter {
  __typename: "Parameter";
  id: string;
  code: string | null;
  name: string | null;
  defaultValue: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  category: ParamCategory | null;
  options: (PARAMETER_parameter_options | null)[] | null;
  parameterGroupe: PARAMETER_parameter_parameterGroupe | null;
  parameterType: PARAMETER_parameter_parameterType | null;
  value: PARAMETER_parameter_value | null;
}

export interface PARAMETER {
  parameter: PARAMETER_parameter | null;
}

export interface PARAMETERVariables {
  id: string;
}
