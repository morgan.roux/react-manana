/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ParamCategory } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: ParameterInfo
// ====================================================

export interface ParameterInfo_options {
  __typename: "ParameterOption";
  label: string | null;
  value: string | null;
}

export interface ParameterInfo_parameterGroupe {
  __typename: "ParameterGroupe";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface ParameterInfo_parameterType {
  __typename: "ParameterType";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface ParameterInfo_value {
  __typename: "ParameterValue";
  id: string;
  value: string | null;
}

export interface ParameterInfo {
  __typename: "Parameter";
  id: string;
  code: string | null;
  name: string | null;
  defaultValue: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  category: ParamCategory | null;
  options: (ParameterInfo_options | null)[] | null;
  parameterGroupe: ParameterInfo_parameterGroupe | null;
  parameterType: ParameterInfo_parameterType | null;
  value: ParameterInfo_value | null;
}
