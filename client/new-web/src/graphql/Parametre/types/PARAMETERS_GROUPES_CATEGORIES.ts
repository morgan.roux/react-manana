/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ParamCategory } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: PARAMETERS_GROUPES_CATEGORIES
// ====================================================

export interface PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories_options {
  __typename: "ParameterOption";
  label: string | null;
  value: string | null;
}

export interface PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories_parameterGroupe {
  __typename: "ParameterGroupe";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories_parameterType {
  __typename: "ParameterType";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories_value {
  __typename: "ParameterValue";
  id: string;
  value: string | null;
}

export interface PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories {
  __typename: "Parameter";
  id: string;
  code: string | null;
  name: string | null;
  defaultValue: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  category: ParamCategory | null;
  options: (PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories_options | null)[] | null;
  parameterGroupe: PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories_parameterGroupe | null;
  parameterType: PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories_parameterType | null;
  value: PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories_value | null;
}

export interface PARAMETERS_GROUPES_CATEGORIES {
  parametersGroupesCategories: (PARAMETERS_GROUPES_CATEGORIES_parametersGroupesCategories | null)[] | null;
}

export interface PARAMETERS_GROUPES_CATEGORIESVariables {
  categories: ParamCategory[];
  groupes: string[];
}
