/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ParamCategory } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: PARAMETER_BY_CODE
// ====================================================

export interface PARAMETER_BY_CODE_parameterByCode_options {
  __typename: "ParameterOption";
  label: string | null;
  value: string | null;
}

export interface PARAMETER_BY_CODE_parameterByCode_parameterGroupe {
  __typename: "ParameterGroupe";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PARAMETER_BY_CODE_parameterByCode_parameterType {
  __typename: "ParameterType";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PARAMETER_BY_CODE_parameterByCode_value {
  __typename: "ParameterValue";
  id: string;
  value: string | null;
}

export interface PARAMETER_BY_CODE_parameterByCode {
  __typename: "Parameter";
  id: string;
  code: string | null;
  name: string | null;
  defaultValue: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  category: ParamCategory | null;
  options: (PARAMETER_BY_CODE_parameterByCode_options | null)[] | null;
  parameterGroupe: PARAMETER_BY_CODE_parameterByCode_parameterGroupe | null;
  parameterType: PARAMETER_BY_CODE_parameterByCode_parameterType | null;
  value: PARAMETER_BY_CODE_parameterByCode_value | null;
}

export interface PARAMETER_BY_CODE {
  parameterByCode: PARAMETER_BY_CODE_parameterByCode | null;
}

export interface PARAMETER_BY_CODEVariables {
  code: string;
}
