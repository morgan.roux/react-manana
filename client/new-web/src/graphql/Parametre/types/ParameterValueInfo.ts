/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ParameterValueInfo
// ====================================================

export interface ParameterValueInfo {
  __typename: "ParameterValue";
  id: string;
  value: string | null;
}
