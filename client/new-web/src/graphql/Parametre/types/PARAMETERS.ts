/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ParamCategory } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: PARAMETERS
// ====================================================

export interface PARAMETERS_parameters_options {
  __typename: "ParameterOption";
  label: string | null;
  value: string | null;
}

export interface PARAMETERS_parameters_parameterGroupe {
  __typename: "ParameterGroupe";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PARAMETERS_parameters_parameterType {
  __typename: "ParameterType";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PARAMETERS_parameters_value {
  __typename: "ParameterValue";
  id: string;
  value: string | null;
}

export interface PARAMETERS_parameters {
  __typename: "Parameter";
  id: string;
  code: string | null;
  name: string | null;
  defaultValue: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  category: ParamCategory | null;
  options: (PARAMETERS_parameters_options | null)[] | null;
  parameterGroupe: PARAMETERS_parameters_parameterGroupe | null;
  parameterType: PARAMETERS_parameters_parameterType | null;
  value: PARAMETERS_parameters_value | null;
}

export interface PARAMETERS {
  parameters: (PARAMETERS_parameters | null)[] | null;
}
