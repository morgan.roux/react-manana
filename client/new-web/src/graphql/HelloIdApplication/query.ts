import gql from 'graphql-tag';
import { HELLOID_APPLICATION_INFO } from './fragment';

export const GET_HELLOID_APPLICATIONS = gql`
  query HELLOID_APPLICATION($iduser: ID!, $idGroupement: ID!) {
    userApplications(iduser: $iduser, idGroupement: $idGroupement) {
      ...HelloidApplication
    }
  }
  ${HELLOID_APPLICATION_INFO}
`;

export const GET_ALL_HELLOID_APPLICATIONS = gql`
  query ALL_HELLOID_APPLICATION($idgroupement: ID!) {
    helloidApplications(idgroupement: $idgroupement) {
      ...HelloidApplication
    }
  }
  ${HELLOID_APPLICATION_INFO}
`;