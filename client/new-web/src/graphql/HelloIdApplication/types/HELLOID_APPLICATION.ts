/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: HELLOID_APPLICATION
// ====================================================

export interface HELLOID_APPLICATION_userApplications {
  __typename: "Application";
  applicationGUID: string;
  name: string | null;
  type: string | null;
  url: string | null;
  icon: string | null;
  needConfiguration: boolean | null;
  options: number | null;
  settingOptions: number | null;
  isNew: boolean | null;
  lastTimeUsed: any | null;
  nrofTimesUsed: number | null;
  helloIdurl: string | null;
  iconlink: string | null;
}

export interface HELLOID_APPLICATION {
  userApplications: (HELLOID_APPLICATION_userApplications | null)[] | null;
}

export interface HELLOID_APPLICATIONVariables {
  iduser: string;
  idGroupement: string;
}
