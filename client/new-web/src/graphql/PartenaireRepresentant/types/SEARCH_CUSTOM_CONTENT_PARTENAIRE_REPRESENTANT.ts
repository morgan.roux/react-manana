/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_search_data_PartenaireRepresentant_photo {
  __typename: "Fichier";
  id: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_search_data_PartenaireRepresentant_contact {
  __typename: "Contact";
  id: string;
  adresse1: string | null;
  ville: string | null;
  telMobProf: string | null;
  telProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  adresse2: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_search_data_PartenaireRepresentant {
  __typename: "PartenaireRepresentant";
  id: string;
  photo: SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_search_data_PartenaireRepresentant_photo | null;
  fonction: string | null;
  nom: string;
  prenom: string | null;
  contact: SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_search_data_PartenaireRepresentant_contact | null;
}

export type SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_search_data = SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_search_data_Action | SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_search_data_PartenaireRepresentant;

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT {
  search: SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANTVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
