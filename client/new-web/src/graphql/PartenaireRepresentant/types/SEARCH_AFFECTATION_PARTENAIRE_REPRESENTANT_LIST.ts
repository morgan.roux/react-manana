/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST
// ====================================================

export interface SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST_search_data_PartenaireRepresentant {
  __typename: "PartenaireRepresentant";
  id: string;
  nom: string;
  prenom: string | null;
}

export type SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST_search_data = SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST_search_data_Action | SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST_search_data_PartenaireRepresentant;

export interface SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST_search_data | null)[] | null;
}

export interface SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST {
  search: SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST_search | null;
}

export interface SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LISTVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
}
