/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_PARTENAIRE_REPRESENTANT
// ====================================================

export interface DELETE_PARTENAIRE_REPRESENTANT_softDeletePartenaireRepresentants {
  __typename: "PartenaireRepresentant";
  id: string;
}

export interface DELETE_PARTENAIRE_REPRESENTANT {
  softDeletePartenaireRepresentants: (DELETE_PARTENAIRE_REPRESENTANT_softDeletePartenaireRepresentants | null)[] | null;
}

export interface DELETE_PARTENAIRE_REPRESENTANTVariables {
  ids: string[];
}
