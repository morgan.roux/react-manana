import gql from 'graphql-tag';

export const GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT = gql`
  query SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
  ) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on PartenaireRepresentant {
          id
          photo {
            id
            urlPresigned
            publicUrl
          }
          fonction
          nom
          prenom
          contact {
            id
            adresse1
            ville
            telMobProf
            telProf
            telPerso
            telMobPerso
            mailProf
            mailPerso
            adresse1
            adresse2
          }
        }
      }
    }
  }
`;

export const GET_PARTENAIRE_REPRESENTANT = gql`
  query PARTENAIRE_REPRESENTANT($id: ID!) {
    partenaireRepresentant(id: $id) {
      id
      civilite
      nom
      prenom
      sexe
      fonction
      afficherComme
      contact {
        id
        adresse1
        ville
        mailProf
        siteProf
        mailPerso
        telProf
        telPerso
        cp
        urlLinkedinProf
        urlFacebookProf
        whatsAppMobProf
        urlMessenger
        urlYoutube
      }
      partenaireType {
        id
        libelle
        codeMaj
        isRemoved
      }
      partenaire {
        id
        partenaireServiceSuite {
          id
          groupement {
            id
            nom
          }
        }
      }
      photo {
        id
        avatar {
          id
          description
          codeSexe
          fichier {
            id
            chemin
            nomOriginal
            type
            urlPresigned
            publicUrl
          }
        }
        chemin
        nomOriginal
        type
        urlPresigned
        publicUrl
      }
      codeMaj
      isRemoved
    }
  }
`;

export const GET_SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST = gql`
  query SEARCH_AFFECTATION_PARTENAIRE_REPRESENTANT_LIST(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
  ) {
    search(
      type: $type
      query: $query
      skip: $skip
      take: $take
      sortBy: $sortBy
      filterBy: $filterBy
    ) {
      total
      data {
        ... on PartenaireRepresentant {
          id
          nom
          prenom
        }
      }
    }
  }
`;
