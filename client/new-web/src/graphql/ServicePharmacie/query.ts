import gql from 'graphql-tag';
import { SERVICE_PHARMACIE_INFO_FRAGMENT } from './fragment';

export const GET_SERVICE_PHARMACIES = gql`
  query SERVICE_PHARMACIES {
    servicePharmacies {
      ...ServicePharmacieInfo
    }
  }
  ${SERVICE_PHARMACIE_INFO_FRAGMENT}
`;
