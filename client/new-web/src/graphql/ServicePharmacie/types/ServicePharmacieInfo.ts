/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeService } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: ServicePharmacieInfo
// ====================================================

export interface ServicePharmacieInfo {
  __typename: "ServicePharmacie";
  id: string | null;
  nom: string | null;
  typeService: TypeService | null;
}
