/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeService } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SERVICE_PHARMACIES
// ====================================================

export interface SERVICE_PHARMACIES_servicePharmacies {
  __typename: "ServicePharmacie";
  id: string | null;
  nom: string | null;
  typeService: TypeService | null;
}

export interface SERVICE_PHARMACIES {
  servicePharmacies: (SERVICE_PHARMACIES_servicePharmacies | null)[] | null;
}
