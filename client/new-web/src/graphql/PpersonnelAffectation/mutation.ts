import gql from 'graphql-tag';
import { PRESDIENT_AFFECTATION_INFO_FRAGMENT } from './fragment';

export const DO_AFFECT_PRESIDENT = gql`
  mutation AFFECT_PRESIDENT($inputs: AffectationInput) {
    createUpdateTitulaireAffectation(inputs: $inputs) {
      ...PresidentAffectationInfo
    }
  }
  ${PRESDIENT_AFFECTATION_INFO_FRAGMENT}
`;
