import gql from 'graphql-tag';
import { PRESIDENT_FONCTION_INFO_FRAGMENT } from '../PpersonnelFonction/fragment';
import { DEPARTEMENT_INFO_FRAGMENT } from '../Departement/fragment';

export const PRESDIENT_AFFECTATION_INFO_FRAGMENT = gql`
  fragment PresidentAffectationInfo on TitulaireAffectation {
    id
    titulaireFonction {
      ...PresidentFonctionInfo
    }
    departement {
      ...DepartementInfo
    }
    titulaire {
      id
      fullName
    }
    titulaireDemandeAffectation {
      id
      titulaireRemplacent {
        id
        fullName
      }
    }
    dateDebut
    dateFin
  }
  ${PRESIDENT_FONCTION_INFO_FRAGMENT}
  ${DEPARTEMENT_INFO_FRAGMENT}
`;
