import gql from 'graphql-tag';

export const PHARMACIE_STAT_CA_INFO_FRAGMENT = gql`
  fragment PharmacieStatCAInfo on PharmacieStatCA {
    id
    exercice
    dateDebut
    dateFin
    caTTC
    caHt
    caTVA1
    tauxTVA1
    caTVA2
    tauxTVA2
    caTVA3
    tauxTVA3
    caTVA4
    tauxTVA4
    caTVA5
    tauxTVA5
    codeMaj
    dateCreation
    dateModification
  }
`;
