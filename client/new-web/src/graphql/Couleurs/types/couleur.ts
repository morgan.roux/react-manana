/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: couleur
// ====================================================

export interface couleur_couleur {
  __typename: "Couleur";
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface couleur {
  couleur: couleur_couleur | null;
}

export interface couleurVariables {
  id: string;
}
