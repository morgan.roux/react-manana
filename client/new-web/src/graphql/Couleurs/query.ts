import gql from 'graphql-tag';

export const GET_COULEURS = gql`
  query couleurs {
    couleurs {
      id
      code
      libelle
      isRemoved
    }
  }
`;
export const GET_COULEUR = gql`
  query couleur($id: ID!) {
    couleur(id: $id) {
      code
      libelle
      isRemoved
    }
  }
`;

export const DO_SEARCH_COULEURS = gql`
  query SEARCH_COULEURS($query: JSON, $skip: Int, $take: Int, $filterBy: JSON, $sortBy: JSON) {
    search(
      type: "couleur"
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on Couleur {
          id
          code
          libelle
        }
      }
    }
  }
`;
