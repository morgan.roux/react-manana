/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeAffectation } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION_search_data_PartenaireRepresentantAffectation_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  code: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION_search_data_PartenaireRepresentantAffectation_partenaireRepresentant {
  __typename: "PartenaireRepresentant";
  id: string;
  nom: string;
  prenom: string | null;
  fonction: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION_search_data_PartenaireRepresentantAffectation_partenaireRepresentantDemandeAffectation_remplacent {
  __typename: "PartenaireRepresentant";
  id: string;
  nom: string;
  prenom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION_search_data_PartenaireRepresentantAffectation_partenaireRepresentantDemandeAffectation {
  __typename: "PartenaireRepresentantDemandeAffectation";
  id: string;
  remplacent: SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION_search_data_PartenaireRepresentantAffectation_partenaireRepresentantDemandeAffectation_remplacent | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION_search_data_PartenaireRepresentantAffectation {
  __typename: "PartenaireRepresentantAffectation";
  id: string;
  codeDepartement: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  typeAffectation: TypeAffectation;
  departement: SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION_search_data_PartenaireRepresentantAffectation_departement | null;
  partenaireRepresentant: SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION_search_data_PartenaireRepresentantAffectation_partenaireRepresentant | null;
  partenaireRepresentantDemandeAffectation: SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION_search_data_PartenaireRepresentantAffectation_partenaireRepresentantDemandeAffectation | null;
}

export type SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION_search_data = SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION_search_data_Action | SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION_search_data_PartenaireRepresentantAffectation;

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION {
  search: SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATIONVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
