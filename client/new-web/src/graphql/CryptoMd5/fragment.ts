import gql from 'graphql-tag';
import { EXTERNAL_MAPPING_INFO } from '../ExternalMapping/fragment';


export const CRYPTO_MD5_INFO = gql`
  fragment CryptoMd5Info on CryptoMd5 {
    id
    beginGet
    endGet
    hexadecimal
    requestUrl
    Sendingtype
    haveExternalUserMapping
    externalUserMappings {
        ...ExternalMappingInfo
    }
    SsoApplication {
        id,
        nom,
        url,
    }
  }
  ${EXTERNAL_MAPPING_INFO}
`;