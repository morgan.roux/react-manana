import gql from 'graphql-tag';

export const GET_TICKET_MOTIFS = gql`
  query TICKET_MOTIFS {
    ticketMotifs {
      id
      nom
    }
  }
`;
