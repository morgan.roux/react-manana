/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: TICKET_MOTIFS
// ====================================================

export interface TICKET_MOTIFS_ticketMotifs {
  __typename: "TicketMotif";
  id: string;
  nom: string;
}

export interface TICKET_MOTIFS {
  ticketMotifs: (TICKET_MOTIFS_ticketMotifs | null)[] | null;
}
