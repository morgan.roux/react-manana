/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_SOFT_PARTENAIRE_SERVICE
// ====================================================

export interface DELETE_SOFT_PARTENAIRE_SERVICE_softDeletePartenaireServices {
  __typename: "Partenaire";
  id: string;
}

export interface DELETE_SOFT_PARTENAIRE_SERVICE {
  softDeletePartenaireServices: (DELETE_SOFT_PARTENAIRE_SERVICE_softDeletePartenaireServices | null)[] | null;
}

export interface DELETE_SOFT_PARTENAIRE_SERVICEVariables {
  ids: string[];
}
