import gql from 'graphql-tag';
import { MESSAGERIE_INFO_FRAGMENT } from './fragment';

export const DO_CREATE_UPDATE_MESSAGERIE = gql`
  mutation CREATE_UPDATE_MESSAGERIE($inputs: MessagerieInput) {
    createUpdateMessagerie(inputs: $inputs) {
      ...MessagerieInfo
    }
  }
  ${MESSAGERIE_INFO_FRAGMENT}
`;

export const DO_DELETE_MESSAGES = gql`
  mutation DELETE_MESSAGES($ids: [ID!]!, $typeMessagerie: MessagerieType, $permanent: Boolean) {
    deleteMessages(ids: $ids, typeMessagerie: $typeMessagerie, permanent: $permanent) {
      ...MessagerieInfo
    }
  }
  ${MESSAGERIE_INFO_FRAGMENT}
`;

export const DO_MARK_MESSAGE_AS_SEEN = gql`
  mutation MARK_MESSAGE_AS_SEEN($idMessageHisto: ID!) {
    markMessageAsSeen(idMessageHisto: $idMessageHisto) {
      ...MessagerieInfo
    }
  }
  ${MESSAGERIE_INFO_FRAGMENT}
`;

export const DO_DELETE_MESSAGES_DB = gql`
  mutation DELETE_MESSAGES_DB($ids: [ID!]!) {
    deleteMessagesInDB(ids: $ids) {
      ...MessagerieInfo
    }
  }
  ${MESSAGERIE_INFO_FRAGMENT}
`;

export const DO_RESTORE_MESSAGES = gql`
  mutation RESTORE_MESSAGES($ids: [ID!]!) {
    restoreMessages(ids: $ids) {
      ...MessagerieInfo
    }
  }
  ${MESSAGERIE_INFO_FRAGMENT}
`;

export const DO_MARK_MESSAGE_AS_UNSEEN = gql`
  mutation MARK_MESSAGE_AS_UNSEEN($idMessageHisto: ID!) {
    markMessageAsUnseen(idMessageHisto: $idMessageHisto) {
      ...MessagerieInfo
    }
  }
  ${MESSAGERIE_INFO_FRAGMENT}
`;
