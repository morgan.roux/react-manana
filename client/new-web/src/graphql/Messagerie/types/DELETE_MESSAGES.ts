/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MessagerieType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_MESSAGES
// ====================================================

export interface DELETE_MESSAGES_deleteMessages_messagerieTheme {
  __typename: "MessagerieTheme";
  id: string;
  nom: string | null;
}

export interface DELETE_MESSAGES_deleteMessages_messagerieSource {
  __typename: "MessagerieSource";
  id: string;
  nom: string | null;
}

export interface DELETE_MESSAGES_deleteMessages_userEmetteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface DELETE_MESSAGES_deleteMessages_recepteurs_userRecepteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface DELETE_MESSAGES_deleteMessages_recepteurs_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface DELETE_MESSAGES_deleteMessages_recepteurs {
  __typename: "MessagerieHisto";
  id: string;
  dateHeureLecture: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  userRecepteur: DELETE_MESSAGES_deleteMessages_recepteurs_userRecepteur | null;
  userCreation: DELETE_MESSAGES_deleteMessages_recepteurs_userCreation | null;
}

export interface DELETE_MESSAGES_deleteMessages_attachments_fichier {
  __typename: "Fichier";
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface DELETE_MESSAGES_deleteMessages_attachments {
  __typename: "MessagerieFichierJoint";
  id: string;
  fichier: DELETE_MESSAGES_deleteMessages_attachments_fichier | null;
}

export interface DELETE_MESSAGES_deleteMessages {
  __typename: "Messagerie";
  id: string;
  typeFilter: string | null;
  typeMessagerie: MessagerieType | null;
  objet: string | null;
  message: string | null;
  lu: boolean | null;
  isRemoved: boolean | null;
  dateHeureMessagerie: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  messagerieTheme: DELETE_MESSAGES_deleteMessages_messagerieTheme | null;
  messagerieSource: DELETE_MESSAGES_deleteMessages_messagerieSource | null;
  userEmetteur: DELETE_MESSAGES_deleteMessages_userEmetteur | null;
  recepteurs: (DELETE_MESSAGES_deleteMessages_recepteurs | null)[] | null;
  attachments: (DELETE_MESSAGES_deleteMessages_attachments | null)[] | null;
}

export interface DELETE_MESSAGES {
  deleteMessages: (DELETE_MESSAGES_deleteMessages | null)[] | null;
}

export interface DELETE_MESSAGESVariables {
  ids: string[];
  typeMessagerie?: MessagerieType | null;
  permanent?: boolean | null;
}
