/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MessagerieType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: FILTER_COUNT_MESSAGERIES
// ====================================================

export interface FILTER_COUNT_MESSAGERIES_filterMessageriesCount {
  __typename: "FilterMessageriesCount";
  libelle: string | null;
  code: string | null;
  count: number | null;
}

export interface FILTER_COUNT_MESSAGERIES {
  filterMessageriesCount: (FILTER_COUNT_MESSAGERIES_filterMessageriesCount | null)[] | null;
}

export interface FILTER_COUNT_MESSAGERIESVariables {
  typeMessagerie?: MessagerieType | null;
}
