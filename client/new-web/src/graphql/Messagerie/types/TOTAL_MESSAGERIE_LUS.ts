/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: TOTAL_MESSAGERIE_LUS
// ====================================================

export interface TOTAL_MESSAGERIE_LUS_messagerieNonLus {
  __typename: "MessagerieResult";
  total: number;
}

export interface TOTAL_MESSAGERIE_LUS {
  messagerieNonLus: TOTAL_MESSAGERIE_LUS_messagerieNonLus | null;
}
