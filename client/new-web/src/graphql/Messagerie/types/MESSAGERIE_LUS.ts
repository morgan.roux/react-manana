/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MESSAGERIE_LUS
// ====================================================

export interface MESSAGERIE_LUS_messagerieLus {
  __typename: "MessagerieResult";
  total: number;
}

export interface MESSAGERIE_LUS {
  messagerieLus: MESSAGERIE_LUS_messagerieLus | null;
}
