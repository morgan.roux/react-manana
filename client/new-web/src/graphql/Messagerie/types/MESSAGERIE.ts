/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MessagerieType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: MESSAGERIE
// ====================================================

export interface MESSAGERIE_messagerie_messagerieTheme {
  __typename: "MessagerieTheme";
  id: string;
  nom: string | null;
}

export interface MESSAGERIE_messagerie_messagerieSource {
  __typename: "MessagerieSource";
  id: string;
  nom: string | null;
}

export interface MESSAGERIE_messagerie_userEmetteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MESSAGERIE_messagerie_recepteurs_userRecepteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MESSAGERIE_messagerie_recepteurs_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MESSAGERIE_messagerie_recepteurs {
  __typename: "MessagerieHisto";
  id: string;
  dateHeureLecture: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  userRecepteur: MESSAGERIE_messagerie_recepteurs_userRecepteur | null;
  userCreation: MESSAGERIE_messagerie_recepteurs_userCreation | null;
}

export interface MESSAGERIE_messagerie_attachments_fichier {
  __typename: "Fichier";
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface MESSAGERIE_messagerie_attachments {
  __typename: "MessagerieFichierJoint";
  id: string;
  fichier: MESSAGERIE_messagerie_attachments_fichier | null;
}

export interface MESSAGERIE_messagerie {
  __typename: "Messagerie";
  id: string;
  typeFilter: string | null;
  typeMessagerie: MessagerieType | null;
  objet: string | null;
  message: string | null;
  lu: boolean | null;
  isRemoved: boolean | null;
  dateHeureMessagerie: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  messagerieTheme: MESSAGERIE_messagerie_messagerieTheme | null;
  messagerieSource: MESSAGERIE_messagerie_messagerieSource | null;
  userEmetteur: MESSAGERIE_messagerie_userEmetteur | null;
  recepteurs: (MESSAGERIE_messagerie_recepteurs | null)[] | null;
  attachments: (MESSAGERIE_messagerie_attachments | null)[] | null;
}

export interface MESSAGERIE {
  /**
   * Messagerie
   */
  messagerie: MESSAGERIE_messagerie | null;
}

export interface MESSAGERIEVariables {
  id: string;
}
