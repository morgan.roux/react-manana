/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MessagerieType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: TOTAL_MESSAGERIES
// ====================================================

export interface TOTAL_MESSAGERIES_messageries {
  __typename: "MessagerieResult";
  total: number;
}

export interface TOTAL_MESSAGERIES {
  messageries: TOTAL_MESSAGERIES_messageries | null;
}

export interface TOTAL_MESSAGERIESVariables {
  typeMessagerie?: MessagerieType | null;
  isRemoved?: boolean | null;
  lu?: boolean | null;
}
