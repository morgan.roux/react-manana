/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MessagerieType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL subscription operation: MESSAGE_SUPPRIMEE_SUBSCRIPTION
// ====================================================

export interface MESSAGE_SUPPRIMEE_SUBSCRIPTION_messagerieSupprimee_messagerieTheme {
  __typename: "MessagerieTheme";
  id: string;
  nom: string | null;
}

export interface MESSAGE_SUPPRIMEE_SUBSCRIPTION_messagerieSupprimee_messagerieSource {
  __typename: "MessagerieSource";
  id: string;
  nom: string | null;
}

export interface MESSAGE_SUPPRIMEE_SUBSCRIPTION_messagerieSupprimee_userEmetteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MESSAGE_SUPPRIMEE_SUBSCRIPTION_messagerieSupprimee_recepteurs_userRecepteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MESSAGE_SUPPRIMEE_SUBSCRIPTION_messagerieSupprimee_recepteurs_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MESSAGE_SUPPRIMEE_SUBSCRIPTION_messagerieSupprimee_recepteurs {
  __typename: "MessagerieHisto";
  id: string;
  dateHeureLecture: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  userRecepteur: MESSAGE_SUPPRIMEE_SUBSCRIPTION_messagerieSupprimee_recepteurs_userRecepteur | null;
  userCreation: MESSAGE_SUPPRIMEE_SUBSCRIPTION_messagerieSupprimee_recepteurs_userCreation | null;
}

export interface MESSAGE_SUPPRIMEE_SUBSCRIPTION_messagerieSupprimee_attachments_fichier {
  __typename: "Fichier";
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface MESSAGE_SUPPRIMEE_SUBSCRIPTION_messagerieSupprimee_attachments {
  __typename: "MessagerieFichierJoint";
  id: string;
  fichier: MESSAGE_SUPPRIMEE_SUBSCRIPTION_messagerieSupprimee_attachments_fichier | null;
}

export interface MESSAGE_SUPPRIMEE_SUBSCRIPTION_messagerieSupprimee {
  __typename: "Messagerie";
  id: string;
  typeFilter: string | null;
  typeMessagerie: MessagerieType | null;
  objet: string | null;
  message: string | null;
  lu: boolean | null;
  isRemoved: boolean | null;
  dateHeureMessagerie: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  messagerieTheme: MESSAGE_SUPPRIMEE_SUBSCRIPTION_messagerieSupprimee_messagerieTheme | null;
  messagerieSource: MESSAGE_SUPPRIMEE_SUBSCRIPTION_messagerieSupprimee_messagerieSource | null;
  userEmetteur: MESSAGE_SUPPRIMEE_SUBSCRIPTION_messagerieSupprimee_userEmetteur | null;
  recepteurs: (MESSAGE_SUPPRIMEE_SUBSCRIPTION_messagerieSupprimee_recepteurs | null)[] | null;
  attachments: (MESSAGE_SUPPRIMEE_SUBSCRIPTION_messagerieSupprimee_attachments | null)[] | null;
}

export interface MESSAGE_SUPPRIMEE_SUBSCRIPTION {
  messagerieSupprimee: MESSAGE_SUPPRIMEE_SUBSCRIPTION_messagerieSupprimee | null;
}
