/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: MessagerieFichierJointInfo
// ====================================================

export interface MessagerieFichierJointInfo_fichier {
  __typename: "Fichier";
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface MessagerieFichierJointInfo {
  __typename: "MessagerieFichierJoint";
  id: string;
  fichier: MessagerieFichierJointInfo_fichier | null;
}
