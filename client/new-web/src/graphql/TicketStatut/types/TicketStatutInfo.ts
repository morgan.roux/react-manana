/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TicketStatutInfo
// ====================================================

export interface TicketStatutInfo {
  __typename: "TicketStatut";
  id: string;
  code: string;
  nom: string;
}
