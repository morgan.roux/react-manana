/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GroupeAmisType, GroupeAmisStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: GroupeAmis
// ====================================================

export interface GroupeAmis_groupeAmis_pharmacieMembres_users {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface GroupeAmis_groupeAmis_pharmacieMembres {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  users: (GroupeAmis_groupeAmis_pharmacieMembres_users | null)[] | null;
}

export interface GroupeAmis_groupeAmis {
  __typename: "GroupeAmis";
  id: string;
  nom: string | null;
  description: string | null;
  typeGroupeAmis: GroupeAmisType;
  nbPharmacie: number;
  groupStatus: GroupeAmisStatus;
  nbMember: number;
  nbPartage: number;
  nbRecommandation: number;
  dateCreation: any | null;
  pharmacieMembres: GroupeAmis_groupeAmis_pharmacieMembres[] | null;
}

export interface GroupeAmis {
  groupeAmis: GroupeAmis_groupeAmis | null;
}

export interface GroupeAmisVariables {
  id: string;
}
