/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GroupeAmisType, GroupeAmisStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: GroupAmisInfo
// ====================================================

export interface GroupAmisInfo {
  __typename: "GroupeAmis";
  id: string;
  nom: string | null;
  description: string | null;
  typeGroupeAmis: GroupeAmisType;
  nbPharmacie: number;
  groupStatus: GroupeAmisStatus;
  nbMember: number;
  nbPartage: number;
  nbRecommandation: number;
  dateCreation: any | null;
}
