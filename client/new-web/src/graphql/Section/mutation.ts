import gql from 'graphql-tag';

export const DO_CREATE_UPDATE_SECTION = gql`
  mutation CREATE_UPDATE_SECTION($input: TodoSectionInput!) {
    createUpdateTodoSection(input: $input) {
      libelle
      ordre
    }
  }
`;
