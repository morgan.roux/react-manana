/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TodoSectionInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_SECTION
// ====================================================

export interface CREATE_UPDATE_SECTION_createUpdateTodoSection {
  __typename: "TodoSection";
  libelle: string;
  ordre: number | null;
}

export interface CREATE_UPDATE_SECTION {
  createUpdateTodoSection: CREATE_UPDATE_SECTION_createUpdateTodoSection;
}

export interface CREATE_UPDATE_SECTIONVariables {
  input: TodoSectionInput;
}
