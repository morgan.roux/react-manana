/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TodoSectionInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CreateUpdateTodoSection
// ====================================================

export interface CreateUpdateTodoSection_createUpdateTodoSection {
  __typename: "TodoSection";
  libelle: string;
  ordre: number | null;
}

export interface CreateUpdateTodoSection {
  createUpdateTodoSection: CreateUpdateTodoSection_createUpdateTodoSection;
}

export interface CreateUpdateTodoSectionVariables {
  input: TodoSectionInput;
}
