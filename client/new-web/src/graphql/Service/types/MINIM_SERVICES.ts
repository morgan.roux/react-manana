/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MINIM_SERVICES
// ====================================================

export interface MINIM_SERVICES_services {
  __typename: "Service";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface MINIM_SERVICES {
  services: (MINIM_SERVICES_services | null)[] | null;
}
