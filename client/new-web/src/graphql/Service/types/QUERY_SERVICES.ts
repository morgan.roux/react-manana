/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: QUERY_SERVICES
// ====================================================

export interface QUERY_SERVICES_services {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  countUsers: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface QUERY_SERVICES {
  services: (QUERY_SERVICES_services | null)[] | null;
}

export interface QUERY_SERVICESVariables {
  idGroupement?: string | null;
}
