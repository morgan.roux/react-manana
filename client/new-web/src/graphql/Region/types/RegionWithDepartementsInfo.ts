/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: RegionWithDepartementsInfo
// ====================================================

export interface RegionWithDepartementsInfo_departements {
  __typename: "Departement";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface RegionWithDepartementsInfo {
  __typename: "Region";
  id: string;
  nom: string | null;
  departements: (RegionWithDepartementsInfo_departements | null)[] | null;
}
