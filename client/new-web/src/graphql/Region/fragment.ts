import gql from 'graphql-tag';

export const REGION_INFO_FRAGMENT = gql`
  fragment RegionInfo on Region {
    id
    nom
  }
`;

export const REGION_WITH_DEPARTEMENTS_INFO_FRAGMENT = gql`
  fragment RegionWithDepartementsInfo on Region {
    id
    nom
    departements {
      id
      code
      nom
    }
  }
`;
