/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_TOTAL
// ====================================================

export interface SEARCH_TOTAL_search {
  __typename: "SearchResult";
  total: number;
}

export interface SEARCH_TOTAL {
  search: SEARCH_TOTAL_search | null;
}

export interface SEARCH_TOTALVariables {
  type?: (string | null)[] | null;
  query?: any | null;
}
