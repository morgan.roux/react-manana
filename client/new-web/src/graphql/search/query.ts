import gql from 'graphql-tag';
import { ACTE_INFO } from '../Acte/fragment';
import { ACTIVITY_INFO_FRAGEMENT } from '../Activite/fragment';
import { ACTUALITE_INFO_FRAGEMENT, SEARCH_ACTUALITE_ORIGINE_INFO } from '../Actualite/fragment';
import { AVATAR_INFO_FRAGEMENT } from '../Avatar/fragement';
import { SEARCH_CANAL_GAMME_INFO } from '../CanalGamme/fragment';
import { COMMANDE_FRAGMENT } from '../Commande/fragment';
import { SEARCH_COMMANDE_CANAL_INFO } from '../CommandeCanal/fragment';
import { SEARCH_COMMANDE_TYPE_INFO } from '../CommandeType/fragment';
import { FAMILLE_INFO } from '../Famille/fragment';
import { SEARCH_GAMME_CATALOGUE_INFO } from '../GammeCatalogue/fragment';
import { GROUP_AMIS_FRAGMENT } from '../GroupAmis/fragment';
import { GROUPE_CLIENT_INFO_FRAGMENT } from '../GroupeClient/fragment';
import { SEARCH_GROUPEMENT_INFO } from '../Groupement/fragement';
import { IDEE_BONNE_PRATIQUE_FRAGMENT } from '../IdeeBonnePratique/fragment';
import { ITEM_INFO_FRAGEMENT } from '../Item/fragment';
import { LABORATOIRE_FILTER_FRAGMENT, SEARCH_LABORATOIRE_FRAGMENT } from '../Laboratoire/fragment';
import { MARCHE_INFO_FRAGMENT } from '../Marche/fragment';
import { SEARCH_OPERATION_INFO } from '../OperationCommerciale/fragment';
import { SEARCH_PARTENAIRE_INFO } from '../Partenaire/fragment';
import { SEARCH_PERSONNEL_FRAGMENT } from '../Personnel/fragment';
import { SEARCH_PHARMACIE_INFO } from '../Pharmacie/fragment';
import { SEARCH_PERSONNEL_PHARMACIE_FRAGMENT } from '../Ppersonnel/fragment';
import { SEARCH_PRODUIT_CANAL_INFO } from '../ProduitCanal/fragment';
import { PROJECT_INFO_FRAGMENT } from '../Project/fragement';
import { PROMOTION_INFO_FRAGMENT } from '../Promotion/fragment';
import { PUBLICITE_INFO_FRAGMENT } from '../Publicite/fragement';
import { ROLE_INFO_FRAGEMENT } from '../Role/fragment';
import { SERVICE_INFO_FRAGEMENT } from '../Service/fragment';
import { SEARCH_TITULAIRE_FRAGMENT } from '../Titulaire/fragment';
import { SEARCH_TVA_INFO, TVA_INFO } from '../TVA/fragment';
import { SEARCH_USER_INFO } from '../User/fragment';

export const SEARCH = gql`
  query SEARCH(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
    $idPharmacie: ID
    $searchActualiteParams: String
    $userId: String
    $idPharmacieUser: ID
    $idGroupement: ID
    $actionStatus: String
    $idRemiseOperation: ID
  ) {
    search(
      type: $type
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on GammeCatalogue {
          ...SearchGammeCatalogueInfo
        }
        ... on Acte {
          ...ActeInfo
        }
        ... on TVA {
          ...TvaInfo
        }
        ... on Personnel {
          ...SearchPersonnelInfo
        }
        ... on Ppersonnel {
          ...SearchPersonnelPharmacieInfo
        }
        ... on Partenaire {
          ...SearchPartenaireInfo
        }
        ... on Titulaire {
          ...SearchTitulaireInfo
        }
        ... on Groupement {
          ...SearchGroupementInfo
        }
        ... on Laboratoire {
          ...LaboratoireFilterInfo
        }
        ... on Pharmacie {
          ...SearchPharmacieInfo
        }
        ... on ProduitCanal {
          ...SearchProduitCanal
        }
        ... on Famille {
          ...FamilleInfo
        }
        ... on Commande {
          ...Commande
        }
        ... on Operation {
          ...SearchOperationInfo
        }
        ... on CanalGamme {
          ...SearchCanalGammeInfo
        }
        ... on Actualite {
          ...ActualiteInfo
        }
        ... on Project {
          ...ProjectInfo
        }
        ... on CommandeType {
          ...SearchCommandeTypeInfo
        }
        ... on CommandeCanal {
          ...SearchCommandeCanalInfo
        }
        ... on ActualiteOrigine {
          ...SearchActualiteOrigineInfo
        }
        ... on User {
          ...SearchUserInfo
        }
        ... on Service {
          ...ServiceInfo
        }
        ... on ActiviteUser {
          ...ActivityInfo
        }
        ... on Publicite {
          ...PubliciteInfo
        }
        ... on Item {
          ...ItemInfo
        }
        ... on IdeeOuBonnePratique {
          ...IdeeOuBonnePratiqueInfo
        }
        ... on Role {
          ...RoleInfo
        }
      }
    }
  }
  ${IDEE_BONNE_PRATIQUE_FRAGMENT}
  ${ITEM_INFO_FRAGEMENT}
  ${ACTUALITE_INFO_FRAGEMENT}
  ${SERVICE_INFO_FRAGEMENT}
  ${PROJECT_INFO_FRAGMENT}
  ${ACTIVITY_INFO_FRAGEMENT}
  ${PUBLICITE_INFO_FRAGMENT}
  ${ACTE_INFO}
  ${TVA_INFO}
  ${SEARCH_CANAL_GAMME_INFO}
  ${SEARCH_PERSONNEL_FRAGMENT}
  ${SEARCH_PERSONNEL_PHARMACIE_FRAGMENT}
  ${SEARCH_PARTENAIRE_INFO}
  ${SEARCH_TITULAIRE_FRAGMENT}
  ${SEARCH_GROUPEMENT_INFO}
  ${LABORATOIRE_FILTER_FRAGMENT}
  ${SEARCH_PRODUIT_CANAL_INFO}
  ${SEARCH_OPERATION_INFO}
  ${SEARCH_GAMME_CATALOGUE_INFO}
  ${SEARCH_COMMANDE_TYPE_INFO}
  ${SEARCH_ACTUALITE_ORIGINE_INFO}
  ${SEARCH_USER_INFO}
  ${SEARCH_COMMANDE_CANAL_INFO}
  ${FAMILLE_INFO}
  ${SEARCH_PHARMACIE_INFO}
  ${COMMANDE_FRAGMENT}
  ${ROLE_INFO_FRAGEMENT}
`;

export const GET_SEARCH_TOTAL = gql`
  query SEARCH_TOTAL($type: [String], $query: JSON) {
    search(type: $type, query: $query) {
      total
    }
  }
`;

export const GET_CUSTOM_CONTENT_SEARCH = gql`
  query CUSTOM_CONTENT_SEARCH(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
    $idPharmacie: ID
    # $idOperation: ID
    $searchActualiteParams: String
    $userId: String
    $idPharmacieUser: ID
    $idGroupement: ID
    $actionStatus: String
    $idRemiseOperation: ID
  ) {
    search(
      type: $type
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on Personnel {
          ...SearchPersonnelInfo
        }
        ... on Ppersonnel {
          ...SearchPersonnelPharmacieInfo
        }
        ... on Partenaire {
          ...SearchPartenaireInfo
        }
        ... on Titulaire {
          ...SearchTitulaireInfo
        }
        ... on Groupement {
          ...SearchGroupementInfo
        }
        ... on Laboratoire {
          ...SearchLaboratoireInfo
        }
        ... on Pharmacie {
          ...SearchPharmacieInfo
        }
        ... on Famille {
          ...FamilleInfo
        }
        ... on Commande {
          ...Commande
        }
        ... on CanalGamme {
          ...SearchCanalGammeInfo
        }
        ... on Actualite {
          ...ActualiteInfo
        }
        ... on Project {
          ...ProjectInfo
        }
        ... on CommandeType {
          ...SearchCommandeTypeInfo
        }
        ... on CommandeCanal {
          ...SearchCommandeCanalInfo
        }
        ... on ActualiteOrigine {
          ...SearchActualiteOrigineInfo
        }
        ... on User {
          ...SearchUserInfo
        }
        ... on Service {
          ...ServiceInfo
        }
        ... on ActiviteUser {
          ...ActivityInfo
        }
        ... on TVA {
          ...SearchTvaInfo
        }
        ... on GroupeClient {
          ...GroupeClientInfo
        }
        ... on Marche {
          ...MarcheInfo
        }
        ... on Promotion {
          ...PromotionInfo
        }
        ... on Avatar {
          ...AvatarInfo
        }
        ... on GroupeAmis {
          ...GroupAmisInfo
        }
        ... on Operation {
          id
          accordCommercial
          commandePassee
          description
          dateDebut
          dateFin
          libelle
          laboratoire {
            id
            nomLabo
          }
          item {
            id
            code
            name
            codeItem
          }
        }
        ... on ProduitCanal {
          type
          id
          qteStock
          stv
          prixPhv
          produit {
            id
            libelle
            produitPhoto {
              id
              fichier {
                id
                publicUrl
                nomOriginal
              }
            }
            produitCode {
              id
              code
              typeCode
              referent
            }
            famille {
              id
              codeFamille
              libelleFamille
            }
            produitTechReg {
              id
              laboExploitant {
                id
                nomLabo
                sortie
              }
              tva {
                id
                codeTva
                tauxTva
              }
            }
          }
          remises(idPharmacie: $idPharmacie, idRemiseOperation: $idRemiseOperation) {
            id
            quantiteMin
            quantiteMax
            pourcentageRemise
            nombreUg
          }
          sousGammeCommercial {
            id
            libelle
          }
        }
      }
    }
  }
  ${ACTUALITE_INFO_FRAGEMENT}
  ${SERVICE_INFO_FRAGEMENT}
  ${PROJECT_INFO_FRAGMENT}
  ${ACTIVITY_INFO_FRAGEMENT}
  ${GROUPE_CLIENT_INFO_FRAGMENT}
  ${MARCHE_INFO_FRAGMENT}
  ${PROMOTION_INFO_FRAGMENT}
  ${AVATAR_INFO_FRAGEMENT}
  ${SEARCH_COMMANDE_TYPE_INFO}
  ${SEARCH_COMMANDE_CANAL_INFO}
  ${SEARCH_USER_INFO}
  ${SEARCH_PARTENAIRE_INFO}
  ${SEARCH_TVA_INFO}
  ${SEARCH_ACTUALITE_ORIGINE_INFO}
  ${SEARCH_CANAL_GAMME_INFO}
  ${SEARCH_PHARMACIE_INFO}
  ${SEARCH_LABORATOIRE_FRAGMENT}
  ${SEARCH_GROUPEMENT_INFO}
  ${SEARCH_TITULAIRE_FRAGMENT}
  ${SEARCH_PERSONNEL_FRAGMENT}
  ${SEARCH_PERSONNEL_PHARMACIE_FRAGMENT}
  ${FAMILLE_INFO}
  ${COMMANDE_FRAGMENT}
  ${GROUP_AMIS_FRAGMENT}
`;

export const GET_MINIMAL_SEARCH = gql`
  query MINIMAL_SEARCH(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
    $idGroupement: ID
  ) {
    search(
      type: $type
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on ProduitCanal {
          type
          id
          produit {
            id
            libelle
            libelle2
          }
        }
        ... on Service {
          type
          id
          code
          nom
          countUsers(idGroupement: $idGroupement)
        }
        ... on Pharmacie {
          type
          id
          sortie
          cip
          numFiness
          nom
          departement {
            id
            nom
          }
          titulaires {
            id
            nom
            prenom
            fullName
          }
          adresse1
          adresse2
          cp
          ville
          idGroupement
          actived
        }
        ... on Titulaire {
          type
          id
          nom
          prenom
          fullName
          estPresidentRegion
          idGroupement
          pharmacies {
            id
          }
        }
        ... on Laboratoire {
          type
          id
          nomLabo
          sortie
        }
        ... on Partenaire {
          type
          id
          nom
          idGroupement
        }
      }
    }
  }
`;
