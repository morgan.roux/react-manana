/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GROUPE_CLIENT
// ====================================================

export interface GROUPE_CLIENT_groupeClient_pharmacies {
  __typename: "Pharmacie";
  id: string;
}

export interface GROUPE_CLIENT_groupeClient {
  __typename: "GroupeClient";
  id: string;
  codeGroupe: number | null;
  nomGroupe: string | null;
  nomCommercial: string | null;
  dateValiditeDebut: any | null;
  dateValiditeFin: any | null;
  pharmacies: (GROUPE_CLIENT_groupeClient_pharmacies | null)[] | null;
}

export interface GROUPE_CLIENT {
  groupeClient: GROUPE_CLIENT_groupeClient | null;
}

export interface GROUPE_CLIENTVariables {
  id: string;
}
