/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_GROUPE_CLIENT
// ====================================================

export interface CREATE_UPDATE_GROUPE_CLIENT_createUpdateGroupeClient_pharmacies {
  __typename: "Pharmacie";
  id: string;
}

export interface CREATE_UPDATE_GROUPE_CLIENT_createUpdateGroupeClient {
  __typename: "GroupeClient";
  id: string;
  codeGroupe: number | null;
  nomGroupe: string | null;
  nomCommercial: string | null;
  dateValiditeDebut: any | null;
  dateValiditeFin: any | null;
  pharmacies: (CREATE_UPDATE_GROUPE_CLIENT_createUpdateGroupeClient_pharmacies | null)[] | null;
}

export interface CREATE_UPDATE_GROUPE_CLIENT {
  /**
   * Groupe client
   */
  createUpdateGroupeClient: CREATE_UPDATE_GROUPE_CLIENT_createUpdateGroupeClient | null;
}

export interface CREATE_UPDATE_GROUPE_CLIENTVariables {
  id?: string | null;
  nom: string;
  dateValiditeDebut: string;
  dateValiditeFin: string;
  idPharmacies: string[];
}
