import gql from 'graphql-tag';

export const GROUPE_CLIENT_INFO_FRAGMENT = gql`
  fragment GroupeClientInfo on GroupeClient {
    id
    codeGroupe
    nomGroupe
    nomCommercial
    dateValiditeDebut
    dateValiditeFin
  }
`;

export const GROUPE_CLIENT_INFO_FRAGMENT_WITH_PHARMA = gql`
  fragment GroupeClientInfoWithPharma on GroupeClient {
    id
    codeGroupe
    nomGroupe
    nomCommercial
    dateValiditeDebut
    dateValiditeFin
    pharmacies {
      id
    }
  }
`;
