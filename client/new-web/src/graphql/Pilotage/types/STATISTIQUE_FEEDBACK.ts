/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PilotageFeedback, FeedbackIndicateur } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: STATISTIQUE_FEEDBACK
// ====================================================

export interface STATISTIQUE_FEEDBACK_statistiqueFeedback {
  __typename: "FeedbakPayload";
  objectif: number | null;
  value: number | null;
  nbPharmacieSmeyley: number | null;
}

export interface STATISTIQUE_FEEDBACK {
  statistiqueFeedback: STATISTIQUE_FEEDBACK_statistiqueFeedback | null;
}

export interface STATISTIQUE_FEEDBACKVariables {
  codeItem: string;
  pilotage: PilotageFeedback;
  indicateur: FeedbackIndicateur;
  idItemAssocies?: (string | null)[] | null;
  idPharmacies?: (string | null)[] | null;
}
