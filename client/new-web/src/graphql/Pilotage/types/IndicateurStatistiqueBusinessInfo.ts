/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: IndicateurStatistiqueBusinessInfo
// ====================================================

export interface IndicateurStatistiqueBusinessInfo_operations_commandeCanal {
  __typename: "StatutTypePayload";
  id: string;
  libelle: string | null;
}

export interface IndicateurStatistiqueBusinessInfo_operations {
  __typename: "Operation";
  id: string;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  commandeCanal: IndicateurStatistiqueBusinessInfo_operations_commandeCanal | null;
}

export interface IndicateurStatistiqueBusinessInfo_pharmacies {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  cp: string | null;
  cip: string | null;
  ville: string | null;
}

export interface IndicateurStatistiqueBusinessInfo {
  __typename: "IndicateurStatistiqueBusiness";
  caTotal: number | null;
  nbCommandes: number | null;
  nbPharmacies: number | null;
  nbProduits: number | null;
  nbLignes: number | null;
  caMoyenParPharmacie: number | null;
  totalRemise: number | null;
  globaliteRemise: number | null;
  operations: (IndicateurStatistiqueBusinessInfo_operations | null)[] | null;
  pharmacies: (IndicateurStatistiqueBusinessInfo_pharmacies | null)[] | null;
}
