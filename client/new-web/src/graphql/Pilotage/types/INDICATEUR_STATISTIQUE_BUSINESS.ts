/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PilotageBusiness } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: INDICATEUR_STATISTIQUE_BUSINESS
// ====================================================

export interface INDICATEUR_STATISTIQUE_BUSINESS_indicateurStatistiqueBusiness_operations_commandeCanal {
  __typename: "StatutTypePayload";
  id: string;
  libelle: string | null;
}

export interface INDICATEUR_STATISTIQUE_BUSINESS_indicateurStatistiqueBusiness_operations {
  __typename: "Operation";
  id: string;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  commandeCanal: INDICATEUR_STATISTIQUE_BUSINESS_indicateurStatistiqueBusiness_operations_commandeCanal | null;
}

export interface INDICATEUR_STATISTIQUE_BUSINESS_indicateurStatistiqueBusiness_pharmacies {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  cp: string | null;
  cip: string | null;
  ville: string | null;
}

export interface INDICATEUR_STATISTIQUE_BUSINESS_indicateurStatistiqueBusiness {
  __typename: "IndicateurStatistiqueBusiness";
  caTotal: number | null;
  nbCommandes: number | null;
  nbPharmacies: number | null;
  nbProduits: number | null;
  nbLignes: number | null;
  caMoyenParPharmacie: number | null;
  totalRemise: number | null;
  globaliteRemise: number | null;
  operations: (INDICATEUR_STATISTIQUE_BUSINESS_indicateurStatistiqueBusiness_operations | null)[] | null;
  pharmacies: (INDICATEUR_STATISTIQUE_BUSINESS_indicateurStatistiqueBusiness_pharmacies | null)[] | null;
}

export interface INDICATEUR_STATISTIQUE_BUSINESS {
  /**
   * Pharmacie Statistique Business
   */
  indicateurStatistiqueBusiness: INDICATEUR_STATISTIQUE_BUSINESS_indicateurStatistiqueBusiness | null;
}

export interface INDICATEUR_STATISTIQUE_BUSINESSVariables {
  pilotage: PilotageBusiness;
  idPharmacies?: (string | null)[] | null;
  idOperations?: (string | null)[] | null;
  idPharmaciesSelected?: (string | null)[] | null;
}
