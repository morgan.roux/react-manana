import gql from 'graphql-tag';
import { LOGICIEL_INFO_FRAGMENT } from '../Logiciel/fragment';
import { AUTOMATE_INFO_FRAGMENT } from '../Automate/fragment';

export const PHARMACIE_INFORMATIQUE_INFO_FRAGMENT = gql`
  fragment PharmacieInformatiqueInfo on PharmacieInformatique {
    id
    numVersion
    dateLogiciel
    nbrePoste
    nbreComptoir
    nbreBackOffice
    nbreBureau
    commentaire
    logiciel {
      ...LogicielInfo
    }
    automate1 {
      ...AutomateInfo
    }
    dateInstallation1
    automate2 {
      ...AutomateInfo
    }
    dateInstallation2
    automate3 {
      ...AutomateInfo
    }
    dateInstallation3
  }
  ${LOGICIEL_INFO_FRAGMENT}
  ${AUTOMATE_INFO_FRAGMENT}
`;
