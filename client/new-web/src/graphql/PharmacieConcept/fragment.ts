import gql from 'graphql-tag';
import { PRESTATAIRE_PHARMACIE_INFO_FRAGMENT } from '../PrestatairePharmacie/fragment';
import { FACADE_INFO_FRAGMENT } from '../Facade/fragment';
import { LEAFLET_INFO_FRAGMENT } from '../Leaflet/fragment';

export const PHARMACIE_CONCEPT_INFO_FRAGMENT = gql`
  fragment PharmacieConceptInfo on PharmacieConcept {
    id
    dateInstallConcept
    avecConcept
    avecFacade
    dateInstallFacade
    conformiteFacade
    surfaceTotale
    surfaceVente
    SurfaceVitrine
    volumeLeaflet
    nbreLineaire
    nbreVitrine
    otcLibAcces
    commentaire
    concept {
      id
      typeConcept
      modele
    }
    fournisseurMobilier {
      ...PrestatairePharmacieInfo
    }
    enseigniste {
      ...PrestatairePharmacieInfo
    }
    facade {
      ...FacadeInfo
    }
    leaflet {
      ...LeafletInfo
    }
  }
  ${PRESTATAIRE_PHARMACIE_INFO_FRAGMENT}
  ${FACADE_INFO_FRAGMENT}
  ${LEAFLET_INFO_FRAGMENT}
`;
