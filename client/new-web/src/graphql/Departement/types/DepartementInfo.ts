/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: DepartementInfo
// ====================================================

export interface DepartementInfo {
  __typename: "Departement";
  id: string;
  code: string | null;
  nom: string | null;
}
