/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: DepartementWithRegionInfo
// ====================================================

export interface DepartementWithRegionInfo_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface DepartementWithRegionInfo {
  __typename: "Departement";
  id: string;
  code: string | null;
  nom: string | null;
  region: DepartementWithRegionInfo_region | null;
}
