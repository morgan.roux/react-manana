/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_ACTION_TYPES
// ====================================================

export interface GET_ACTION_TYPES_todoActionTypes {
  __typename: "TodoActionType";
  libelle: string;
  id: string;
}

export interface GET_ACTION_TYPES {
  todoActionTypes: GET_ACTION_TYPES_todoActionTypes[];
}
