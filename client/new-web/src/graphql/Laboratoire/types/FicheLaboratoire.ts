/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: FicheLaboratoire
// ====================================================

export interface FicheLaboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}
