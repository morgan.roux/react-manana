/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePartenaire, StatutPartenaire } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: LaboratoireFilterInfo
// ====================================================

export interface LaboratoireFilterInfo_laboratoirePartenaire {
  __typename: "LaboratoirePartenaire";
  id: string;
  debutPartenaire: any | null;
  finPartenaire: any | null;
  typePartenaire: TypePartenaire | null;
  statutPartenaire: StatutPartenaire | null;
}

export interface LaboratoireFilterInfo_photo {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface LaboratoireFilterInfo {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  countCanalArticles: number | null;
  sortie: number | null;
  laboratoirePartenaire: LaboratoireFilterInfo_laboratoirePartenaire | null;
  photo: LaboratoireFilterInfo_photo | null;
}
