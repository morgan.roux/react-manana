/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_LABORATOIRE
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_LABORATOIRE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_CUSTOM_CONTENT_LABORATOIRE_search_data_Laboratoire_user {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_CUSTOM_CONTENT_LABORATOIRE_search_data_Laboratoire_laboSuite {
  __typename: "LaboratoireSuite";
  id: string;
  adresse: string | null;
  webSiteUrl: string | null;
  telephone: string | null;
  telecopie: string | null;
  ville: string | null;
  codePostal: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_LABORATOIRE_search_data_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  user: SEARCH_CUSTOM_CONTENT_LABORATOIRE_search_data_Laboratoire_user | null;
  laboSuite: SEARCH_CUSTOM_CONTENT_LABORATOIRE_search_data_Laboratoire_laboSuite | null;
}

export type SEARCH_CUSTOM_CONTENT_LABORATOIRE_search_data = SEARCH_CUSTOM_CONTENT_LABORATOIRE_search_data_Action | SEARCH_CUSTOM_CONTENT_LABORATOIRE_search_data_Laboratoire;

export interface SEARCH_CUSTOM_CONTENT_LABORATOIRE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_LABORATOIRE_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_LABORATOIRE {
  search: SEARCH_CUSTOM_CONTENT_LABORATOIRE_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_LABORATOIREVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
