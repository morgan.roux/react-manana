/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ActualiteLaboratoireInfo
// ====================================================

export interface ActualiteLaboratoireInfo {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}
