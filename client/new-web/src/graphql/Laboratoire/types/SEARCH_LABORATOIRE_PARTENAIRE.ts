/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePartenaire, StatutPartenaire } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_LABORATOIRE_PARTENAIRE
// ====================================================

export interface SEARCH_LABORATOIRE_PARTENAIRE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_laboSuite {
  __typename: "LaboratoireSuite";
  id: string;
  adresse: string | null;
  webSiteUrl: string | null;
  telephone: string | null;
  telecopie: string | null;
  ville: string | null;
  codePostal: string | null;
}

export interface SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_actualites_fichierPresentations {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_actualites {
  __typename: "Actualite";
  id: string;
  libelle: string | null;
  dateCreation: any | null;
  fichierPresentations: (SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_actualites_fichierPresentations | null)[] | null;
}

export interface SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_photo {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_laboratoirePartenaire {
  __typename: "LaboratoirePartenaire";
  id: string;
  debutPartenaire: any | null;
  finPartenaire: any | null;
  typePartenaire: TypePartenaire | null;
  statutPartenaire: StatutPartenaire | null;
}

export interface SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  sortie: number | null;
  laboSuite: SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_laboSuite | null;
  actualites: (SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_actualites | null)[] | null;
  photo: SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_photo | null;
  laboratoirePartenaire: SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_laboratoirePartenaire | null;
}

export type SEARCH_LABORATOIRE_PARTENAIRE_search_data = SEARCH_LABORATOIRE_PARTENAIRE_search_data_Action | SEARCH_LABORATOIRE_PARTENAIRE_search_data_Laboratoire;

export interface SEARCH_LABORATOIRE_PARTENAIRE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_LABORATOIRE_PARTENAIRE_search_data | null)[] | null;
}

export interface SEARCH_LABORATOIRE_PARTENAIRE {
  search: SEARCH_LABORATOIRE_PARTENAIRE_search | null;
}

export interface SEARCH_LABORATOIRE_PARTENAIREVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
