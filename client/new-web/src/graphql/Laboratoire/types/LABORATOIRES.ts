/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePartenaire, StatutPartenaire, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: LABORATOIRES
// ====================================================

export interface LABORATOIRES_laboratoires_laboSuite {
  __typename: "LaboratoireSuite";
  id: string;
  ville: string | null;
  adresse: string | null;
  codePostal: string | null;
  telephone: string | null;
  webSiteUrl: string | null;
  telecopie: string | null;
}

export interface LABORATOIRES_laboratoires_actualites_fichierPresentations {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface LABORATOIRES_laboratoires_actualites {
  __typename: "Actualite";
  id: string;
  libelle: string | null;
  dateCreation: any | null;
  description: string | null;
  fichierPresentations: (LABORATOIRES_laboratoires_actualites_fichierPresentations | null)[] | null;
}

export interface LABORATOIRES_laboratoires_laboratoirePartenaire {
  __typename: "LaboratoirePartenaire";
  id: string;
  debutPartenaire: any | null;
  finPartenaire: any | null;
  typePartenaire: TypePartenaire | null;
  statutPartenaire: StatutPartenaire | null;
}

export interface LABORATOIRES_laboratoires_photo {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface LABORATOIRES_laboratoires_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
}

export interface LABORATOIRES_laboratoires {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  countCanalArticles: number | null;
  sortie: number | null;
  laboSuite: LABORATOIRES_laboratoires_laboSuite | null;
  actualites: (LABORATOIRES_laboratoires_actualites | null)[] | null;
  laboratoirePartenaire: LABORATOIRES_laboratoires_laboratoirePartenaire | null;
  photo: LABORATOIRES_laboratoires_photo | null;
  user: LABORATOIRES_laboratoires_user | null;
}

export interface LABORATOIRES {
  laboratoires: (LABORATOIRES_laboratoires | null)[] | null;
}
