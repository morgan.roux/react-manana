/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { LaboratoireSuiteInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_LABORATOIRE
// ====================================================

export interface CREATE_UPDATE_LABORATOIRE_createUpdateLaboratoire_laboSuite {
  __typename: "LaboratoireSuite";
  id: string;
  adresse: string | null;
  telephone: string | null;
  ville: string | null;
  codePostal: string | null;
  webSiteUrl: string | null;
}

export interface CREATE_UPDATE_LABORATOIRE_createUpdateLaboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
  laboSuite: CREATE_UPDATE_LABORATOIRE_createUpdateLaboratoire_laboSuite | null;
}

export interface CREATE_UPDATE_LABORATOIRE {
  createUpdateLaboratoire: CREATE_UPDATE_LABORATOIRE_createUpdateLaboratoire | null;
}

export interface CREATE_UPDATE_LABORATOIREVariables {
  id: string;
  nomLabo: string;
  suite?: LaboratoireSuiteInput | null;
}
