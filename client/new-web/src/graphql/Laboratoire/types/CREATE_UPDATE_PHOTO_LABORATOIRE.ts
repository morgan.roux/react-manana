/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FichierInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_PHOTO_LABORATOIRE
// ====================================================

export interface CREATE_UPDATE_PHOTO_LABORATOIRE_updatePhotoLaboratoire_laboSuite {
  __typename: "LaboratoireSuite";
  id: string;
  adresse: string | null;
  telephone: string | null;
  ville: string | null;
  codePostal: string | null;
  webSiteUrl: string | null;
}

export interface CREATE_UPDATE_PHOTO_LABORATOIRE_updatePhotoLaboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
  laboSuite: CREATE_UPDATE_PHOTO_LABORATOIRE_updatePhotoLaboratoire_laboSuite | null;
}

export interface CREATE_UPDATE_PHOTO_LABORATOIRE {
  updatePhotoLaboratoire: CREATE_UPDATE_PHOTO_LABORATOIRE_updatePhotoLaboratoire | null;
}

export interface CREATE_UPDATE_PHOTO_LABORATOIREVariables {
  id: string;
  photo?: FichierInput | null;
}
