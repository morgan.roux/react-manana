/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus, TypePartenaire, StatutPartenaire } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: SearchLaboratoireInfo
// ====================================================

export interface SearchLaboratoireInfo_actualites_fichierPresentations {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface SearchLaboratoireInfo_actualites {
  __typename: "Actualite";
  id: string;
  libelle: string | null;
  description: string | null;
  fichierPresentations: (SearchLaboratoireInfo_actualites_fichierPresentations | null)[] | null;
  dateCreation: any | null;
}

export interface SearchLaboratoireInfo__user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
}

export interface SearchLaboratoireInfo_laboratoirePartenaire {
  __typename: "LaboratoirePartenaire";
  id: string;
  debutPartenaire: any | null;
  finPartenaire: any | null;
  typePartenaire: TypePartenaire | null;
  statutPartenaire: StatutPartenaire | null;
}

export interface SearchLaboratoireInfo_photo {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface SearchLaboratoireInfo {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  sortie: number | null;
  countCanalArticles: number | null;
  actualites: (SearchLaboratoireInfo_actualites | null)[] | null;
  _user: SearchLaboratoireInfo__user | null;
  laboratoirePartenaire: SearchLaboratoireInfo_laboratoirePartenaire | null;
  photo: SearchLaboratoireInfo_photo | null;
}
