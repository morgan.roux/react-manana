/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: setLaboratoireSortie
// ====================================================

export interface setLaboratoireSortie_updateLaboratoireSortie_actualites_fichierPresentations {
  __typename: "Fichier";
  publicUrl: string | null;
}

export interface setLaboratoireSortie_updateLaboratoireSortie_actualites {
  __typename: "Actualite";
  libelle: string | null;
  dateCreation: any | null;
  fichierPresentations: (setLaboratoireSortie_updateLaboratoireSortie_actualites_fichierPresentations | null)[] | null;
  description: string | null;
}

export interface setLaboratoireSortie_updateLaboratoireSortie {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
  actualites: (setLaboratoireSortie_updateLaboratoireSortie_actualites | null)[] | null;
}

export interface setLaboratoireSortie {
  updateLaboratoireSortie: setLaboratoireSortie_updateLaboratoireSortie | null;
}

export interface setLaboratoireSortieVariables {
  id: string;
  sortie?: number | null;
}
