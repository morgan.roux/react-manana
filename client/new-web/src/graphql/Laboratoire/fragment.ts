import gql from 'graphql-tag';

export const LABORATOIRE_FILTER_FRAGMENT = gql`
  fragment LaboratoireFilterInfo on Laboratoire {
    type
    id
    nomLabo
    countCanalArticles
    sortie
    laboratoirePartenaire {
      id
      debutPartenaire
      finPartenaire
      typePartenaire
      statutPartenaire
    }
    photo {
      id
      chemin
      nomOriginal
      publicUrl
    }
  }
`;

export const LABORATOIRE_INFO_FRAGMENT = gql`
  fragment LaboratoireInfo on Laboratoire {
    type
    id
    nomLabo
    countCanalArticles
    sortie
    laboSuite {
      id
      ville
      adresse
      codePostal
      telephone
    }
    actualites {
      id
      libelle
      dateCreation
      description
      fichierPresentations {
        id
        nomOriginal
        publicUrl
      }
    }
    laboratoirePartenaire {
      id
      debutPartenaire
      finPartenaire
      typePartenaire
      statutPartenaire
    }
    photo {
      id
      chemin
      nomOriginal
      publicUrl
    }
  }
`;

export const FICHE_LABORATOIRE_FRAGMENT = gql`
  fragment FicheLaboratoire on Laboratoire {
    id
    nomLabo
    sortie
  }
`;

export const ACTUALITE_LABORATOIRE_INFO_FRAGMENT = gql`
  fragment ActualiteLaboratoireInfo on Laboratoire {
    id
    nomLabo
  }
`;

export const SEARCH_LABORATOIRE_FRAGMENT = gql`
  fragment SearchLaboratoireInfo on Laboratoire {
    type
    id
    nomLabo
    sortie
    countCanalArticles
    actualites {
      id
      libelle
      description
      fichierPresentations {
        id
        nomOriginal
        publicUrl
      }
      dateCreation
    }
    _user: user {
      id
      email
      login
      status
      userName
    }
    laboratoirePartenaire {
      id
      debutPartenaire
      finPartenaire
      typePartenaire
      statutPartenaire
    }
    photo {
      id
      chemin
      nomOriginal
      publicUrl
    }
  }
`;
