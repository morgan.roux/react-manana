/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { CommentInput, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_COMMENT
// ====================================================

export interface CREATE_COMMENT_createComment_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface CREATE_COMMENT_createComment_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_COMMENT_createComment_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CREATE_COMMENT_createComment_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CREATE_COMMENT_createComment_user_userPhoto_fichier | null;
}

export interface CREATE_COMMENT_createComment_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CREATE_COMMENT_createComment_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CREATE_COMMENT_createComment_user_role | null;
  userPhoto: CREATE_COMMENT_createComment_user_userPhoto | null;
  pharmacie: CREATE_COMMENT_createComment_user_pharmacie | null;
}

export interface CREATE_COMMENT_createComment {
  __typename: "Comment";
  id: string;
  content: string;
  idItemAssocie: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichiers: CREATE_COMMENT_createComment_fichiers[] | null;
  user: CREATE_COMMENT_createComment_user;
}

export interface CREATE_COMMENT {
  createComment: CREATE_COMMENT_createComment | null;
}

export interface CREATE_COMMENTVariables {
  input: CommentInput;
}
