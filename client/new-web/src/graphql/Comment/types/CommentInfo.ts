/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: CommentInfo
// ====================================================

export interface CommentInfo_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface CommentInfo_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CommentInfo_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CommentInfo_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CommentInfo_user_userPhoto_fichier | null;
}

export interface CommentInfo_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CommentInfo_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CommentInfo_user_role | null;
  userPhoto: CommentInfo_user_userPhoto | null;
  pharmacie: CommentInfo_user_pharmacie | null;
}

export interface CommentInfo {
  __typename: "Comment";
  id: string;
  content: string;
  idItemAssocie: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichiers: CommentInfo_fichiers[] | null;
  user: CommentInfo_user;
}
