import gql from 'graphql-tag';
import { USER_INFO_FRAGEMENT } from '../User/fragment';

export const COMMENT_INFO_FRAGEMENT = gql`
  fragment CommentInfo on Comment {
    id
    content
    idItemAssocie
    dateCreation
    dateModification
    fichiers {
      id
      chemin
      nomOriginal
      type
    }
    user {
      ...UserInfo
    }
  }
  ${USER_INFO_FRAGEMENT}
`;

export const COMMENT_RESULT_INFO_FRAGEMENT = gql`
  fragment CommentResultInfo on CommentResult {
    total
    data {
      ...CommentInfo
    }
  }
  ${COMMENT_INFO_FRAGEMENT}
`;
