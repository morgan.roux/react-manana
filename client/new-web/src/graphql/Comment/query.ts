import { COMMENT_INFO_FRAGEMENT, COMMENT_RESULT_INFO_FRAGEMENT } from './fragment';
import gql from 'graphql-tag';

export const GET_COMMENTS = gql`
  query COMMENTS($codeItem: String!, $idItemAssocie: String!, $take: Int, $skip: Int) {
    comments(codeItem: $codeItem, idItemAssocie: $idItemAssocie, take: $take, skip: $skip) {
      ...CommentResultInfo
    }
  }
  ${COMMENT_RESULT_INFO_FRAGEMENT}
`;

export const GET_SEARCH_COMMENT = gql`
  query SEARCH_COMMENT($query: JSON, $skip: Int, $take: Int, $filterBy: JSON, $sortBy: JSON) {
    search(
      type: "comment"
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on Comment {
          ...CommentInfo
        }
      }
    }
  }

  ${COMMENT_INFO_FRAGEMENT}
`;
