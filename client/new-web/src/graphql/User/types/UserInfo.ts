/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: UserInfo
// ====================================================

export interface UserInfo_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface UserInfo_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface UserInfo_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: UserInfo_userPhoto_fichier | null;
}

export interface UserInfo_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface UserInfo {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: UserInfo_role | null;
  userPhoto: UserInfo_userPhoto | null;
  pharmacie: UserInfo_pharmacie | null;
}
