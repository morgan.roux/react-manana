/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FichierInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_USER_PHOTO
// ====================================================

export interface UPDATE_USER_PHOTO_updateUserPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface UPDATE_USER_PHOTO_updateUserPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: UPDATE_USER_PHOTO_updateUserPhoto_fichier | null;
}

export interface UPDATE_USER_PHOTO {
  updateUserPhoto: UPDATE_USER_PHOTO_updateUserPhoto | null;
}

export interface UPDATE_USER_PHOTOVariables {
  userPhoto?: FichierInput | null;
}
