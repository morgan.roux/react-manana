/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: USER_WITH_MIN_INFO
// ====================================================

export interface USER_WITH_MIN_INFO_user {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface USER_WITH_MIN_INFO {
  user: USER_WITH_MIN_INFO_user | null;
}

export interface USER_WITH_MIN_INFOVariables {
  id: string;
}
