/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: GET_SEARCH_USER_IDS
// ====================================================

export interface GET_SEARCH_USER_IDS_search_data_Action {
  __typename: "Action" | "TodoActionType" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface GET_SEARCH_USER_IDS_search_data_User_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface GET_SEARCH_USER_IDS_search_data_User_contact {
  __typename: "Contact";
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
}

export interface GET_SEARCH_USER_IDS_search_data_User {
  __typename: "User";
  id: string;
  userName: string | null;
  login: string | null;
  email: string | null;
  role: GET_SEARCH_USER_IDS_search_data_User_role | null;
  status: UserStatus | null;
  contact: GET_SEARCH_USER_IDS_search_data_User_contact | null;
}

export type GET_SEARCH_USER_IDS_search_data = GET_SEARCH_USER_IDS_search_data_Action | GET_SEARCH_USER_IDS_search_data_User;

export interface GET_SEARCH_USER_IDS_search {
  __typename: "SearchResult";
  total: number;
  data: (GET_SEARCH_USER_IDS_search_data | null)[] | null;
}

export interface GET_SEARCH_USER_IDS {
  search: GET_SEARCH_USER_IDS_search | null;
}

export interface GET_SEARCH_USER_IDSVariables {
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
}
