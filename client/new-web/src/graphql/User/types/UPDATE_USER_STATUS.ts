/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_USER_STATUS
// ====================================================

export interface UPDATE_USER_STATUS_updateUserStatus {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
}

export interface UPDATE_USER_STATUS {
  updateUserStatus: UPDATE_USER_STATUS_updateUserStatus | null;
}

export interface UPDATE_USER_STATUSVariables {
  id: string;
  status: string;
}
