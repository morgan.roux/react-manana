/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: RESEND_USER_EMAIL
// ====================================================

export interface RESEND_USER_EMAIL {
  resendUserEmail: boolean | null;
}

export interface RESEND_USER_EMAILVariables {
  email: string;
  login: string;
}
