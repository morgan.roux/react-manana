/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SearchUserInfo
// ====================================================

export interface SearchUserInfo_contact {
  __typename: "Contact";
  telPerso: string | null;
}

export interface SearchUserInfo {
  __typename: "User";
  type: string;
  id: string;
  userName: string | null;
  contact: SearchUserInfo_contact | null;
  phoneNumber: string | null;
}
