/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: RESET_USER_PASSWORD
// ====================================================

export interface RESET_USER_PASSWORD_resetUserPassword {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
}

export interface RESET_USER_PASSWORD {
  resetUserPassword: RESET_USER_PASSWORD_resetUserPassword | null;
}

export interface RESET_USER_PASSWORDVariables {
  id: string;
  password: string;
}
