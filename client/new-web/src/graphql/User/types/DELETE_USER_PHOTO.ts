/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_USER_PHOTO
// ====================================================

export interface DELETE_USER_PHOTO_deleteUserPhoto {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface DELETE_USER_PHOTO {
  deleteUserPhoto: DELETE_USER_PHOTO_deleteUserPhoto | null;
}

export interface DELETE_USER_PHOTOVariables {
  chemin: string;
}
