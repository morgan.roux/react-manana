/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: USERS_GROUPEMENT
// ====================================================

export interface USERS_GROUPEMENT_usersGroupement_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface USERS_GROUPEMENT_usersGroupement_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface USERS_GROUPEMENT_usersGroupement_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: USERS_GROUPEMENT_usersGroupement_userPhoto_fichier | null;
}

export interface USERS_GROUPEMENT_usersGroupement_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface USERS_GROUPEMENT_usersGroupement {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: USERS_GROUPEMENT_usersGroupement_role | null;
  userPhoto: USERS_GROUPEMENT_usersGroupement_userPhoto | null;
  pharmacie: USERS_GROUPEMENT_usersGroupement_pharmacie | null;
}

export interface USERS_GROUPEMENT {
  usersGroupement: (USERS_GROUPEMENT_usersGroupement | null)[] | null;
}

export interface USERS_GROUPEMENTVariables {
  idGroupement: string;
}
