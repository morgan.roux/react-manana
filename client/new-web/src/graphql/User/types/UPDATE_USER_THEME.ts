/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UPDATE_USER_THEME
// ====================================================

export interface UPDATE_USER_THEME_updateUserTheme {
  __typename: "User";
  id: string;
  theme: string | null;
}

export interface UPDATE_USER_THEME {
  updateUserTheme: UPDATE_USER_THEME_updateUserTheme | null;
}

export interface UPDATE_USER_THEMEVariables {
  theme: string;
}
