/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FichierInput, UserStatus } from "./../../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_USER_COLLABORATEUR
// ====================================================

export interface CREATE_USER_COLLABORATEUR_createUserCollaborateurOfficine_pharmacie {
  __typename: "Pharmacie";
  id: string;
  cip: string | null;
}

export interface CREATE_USER_COLLABORATEUR_createUserCollaborateurOfficine_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
}

export interface CREATE_USER_COLLABORATEUR_createUserCollaborateurOfficine_role {
  __typename: "Role";
  code: string | null;
  nom: string | null;
}

export interface CREATE_USER_COLLABORATEUR_createUserCollaborateurOfficine {
  __typename: "Ppersonnel";
  type: string;
  id: string;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  estAmbassadrice: boolean | null;
  commentaire: string | null;
  sortie: number | null;
  dateSortie: any | null;
  idGroupement: string | null;
  idPharmacie: string | null;
  pharmacie: CREATE_USER_COLLABORATEUR_createUserCollaborateurOfficine_pharmacie | null;
  user: CREATE_USER_COLLABORATEUR_createUserCollaborateurOfficine_user | null;
  role: CREATE_USER_COLLABORATEUR_createUserCollaborateurOfficine_role | null;
}

export interface CREATE_USER_COLLABORATEUR {
  createUserCollaborateurOfficine: CREATE_USER_COLLABORATEUR_createUserCollaborateurOfficine | null;
}

export interface CREATE_USER_COLLABORATEURVariables {
  idGroupement: string;
  id: string;
  idPharmacie: string;
  email: string;
  login: string;
  role: string;
  userId?: string | null;
  day?: number | null;
  month?: number | null;
  year?: number | null;
  userPhoto?: FichierInput | null;
  codeTraitements?: (string | null)[] | null;
}
