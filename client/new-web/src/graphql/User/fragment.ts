import gql from 'graphql-tag';
import { ROLE_INFO_FRAGEMENT } from '../Role/fragment';

export const USER_INFO_FRAGEMENT = gql`
  fragment UserInfo on User {
    id
    email
    login
    status
    userName
    type
    theme
    nbReclamation
    nbAppel
    role {
      ...RoleInfo
    }
    userPhoto {
      id
      fichier {
        id
        publicUrl
      }
    }
    pharmacie {
      id
      nom
    }
  }
  ${ROLE_INFO_FRAGEMENT}
`;

export const SEARCH_USER_INFO = gql`
  fragment SearchUserInfo on User {
    type
    id
    userName
    contact {
      telPerso
    }
    phoneNumber
  }
`;
