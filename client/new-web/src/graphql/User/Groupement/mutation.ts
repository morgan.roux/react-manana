import gql from 'graphql-tag';

export const DO_CREATE_USER_GROUPEMENT = gql`
  mutation CREATE_USER_GROUPEMENT(
    $idGroupement: ID!
    $id: ID!
    $email: String
    $login: String!
    $role: String!
    $userId: ID
    $day: Int
    $month: Int
    $year: Int
    $userPhoto: FichierInput
    $codeTraitements: [String]
  ) {
    createUserGroupement(
      idGroupement: $idGroupement
      id: $id
      email: $email
      login: $login
      role: $role
      userId: $userId
      day: $day
      month: $month
      year: $year
      userPhoto: $userPhoto
      codeTraitements: $codeTraitements
    ) {
      type
      id
      sortie
      civilite
      nom
      prenom
      commentaire
      dateSortie
      idGroupement
      user {
        id
        email
        login
        userPhoto {
          id
          fichier {
            id
            chemin
            nomOriginal
            publicUrl
          }
        }
      }
      role {
        code
        nom
      }
    }
  }
`;
