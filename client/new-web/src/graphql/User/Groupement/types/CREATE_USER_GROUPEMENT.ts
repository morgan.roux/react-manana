/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FichierInput } from "./../../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_USER_GROUPEMENT
// ====================================================

export interface CREATE_USER_GROUPEMENT_createUserGroupement_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface CREATE_USER_GROUPEMENT_createUserGroupement_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CREATE_USER_GROUPEMENT_createUserGroupement_user_userPhoto_fichier | null;
}

export interface CREATE_USER_GROUPEMENT_createUserGroupement_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userPhoto: CREATE_USER_GROUPEMENT_createUserGroupement_user_userPhoto | null;
}

export interface CREATE_USER_GROUPEMENT_createUserGroupement_role {
  __typename: "Role";
  code: string | null;
  nom: string | null;
}

export interface CREATE_USER_GROUPEMENT_createUserGroupement {
  __typename: "Personnel";
  type: string;
  id: string;
  sortie: number | null;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  commentaire: string | null;
  dateSortie: any | null;
  idGroupement: string | null;
  user: CREATE_USER_GROUPEMENT_createUserGroupement_user | null;
  role: CREATE_USER_GROUPEMENT_createUserGroupement_role | null;
}

export interface CREATE_USER_GROUPEMENT {
  createUserGroupement: CREATE_USER_GROUPEMENT_createUserGroupement | null;
}

export interface CREATE_USER_GROUPEMENTVariables {
  idGroupement: string;
  id: string;
  email?: string | null;
  login: string;
  role: string;
  userId?: string | null;
  day?: number | null;
  month?: number | null;
  year?: number | null;
  userPhoto?: FichierInput | null;
  codeTraitements?: (string | null)[] | null;
}
