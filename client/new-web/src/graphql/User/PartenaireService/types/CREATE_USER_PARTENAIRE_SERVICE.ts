/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FichierInput, UserStatus } from "./../../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_USER_PARTENAIRE_SERVICE
// ====================================================

export interface CREATE_USER_PARTENAIRE_SERVICE_createUserPartenaireService_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
}

export interface CREATE_USER_PARTENAIRE_SERVICE_createUserPartenaireService_role {
  __typename: "Role";
  code: string | null;
  nom: string | null;
}

export interface CREATE_USER_PARTENAIRE_SERVICE_createUserPartenaireService {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
  commentaire: string | null;
  idGroupement: string | null;
  user: CREATE_USER_PARTENAIRE_SERVICE_createUserPartenaireService_user | null;
  role: CREATE_USER_PARTENAIRE_SERVICE_createUserPartenaireService_role | null;
}

export interface CREATE_USER_PARTENAIRE_SERVICE {
  createUserPartenaireService: CREATE_USER_PARTENAIRE_SERVICE_createUserPartenaireService | null;
}

export interface CREATE_USER_PARTENAIRE_SERVICEVariables {
  idGroupement: string;
  id: string;
  email?: string | null;
  login: string;
  userPhoto?: FichierInput | null;
  codeTraitements?: (string | null)[] | null;
  day?: number | null;
  month?: number | null;
  year?: number | null;
  idPharmacie: string;
}
