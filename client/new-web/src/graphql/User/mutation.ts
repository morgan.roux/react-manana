import gql from 'graphql-tag';
import { FICHIER_FRAGMENT } from '../Fichier/fragment';

export const DO_UPDATE_USER_STATUS = gql`
  mutation UPDATE_USER_STATUS($id: ID!, $status: String!) {
    updateUserStatus(id: $id, status: $status) {
      id
      email
      login
      status
    }
  }
`;

export const DO_RESEND_USER_EMAIL = gql`
  mutation RESEND_USER_EMAIL($email: String!, $login: String!) {
    resendUserEmail(email: $email, login: $login)
  }
`;

export const DO_UPDATE_USER_PHOTO = gql`
  mutation UPDATE_USER_PHOTO($userPhoto: FichierInput) {
    updateUserPhoto(userPhoto: $userPhoto) {
      id
      fichier {
        id
        chemin
        nomOriginal
        type
      }
    }
  }
`;

export const DO_RESET_USER_PASSWORD = gql`
  mutation RESET_USER_PASSWORD($id: ID!, $password: String!) {
    resetUserPassword(id: $id, password: $password) {
      id
      email
      login
      status
    }
  }
`;

export const DO_DELETE_USER_PHOTO = gql`
  mutation DELETE_USER_PHOTO($chemin: String!) {
    deleteUserPhoto(chemin: $chemin) {
      ...FichierInfo
    }
  }
  ${FICHIER_FRAGMENT}
`;

export const DO_UPDATE_USER_THEME = gql`
  mutation UPDATE_USER_THEME($theme: String!) {
    updateUserTheme(theme: $theme) {
      id
      theme
    }
  }
`;
