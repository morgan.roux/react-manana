import gql from 'graphql-tag';

export const DO_CREATE_USER_PARTENAIRE_LABORATOIRE = gql`
  mutation CREATE_USER_PARTENAIRE_LABORATOIRE(
    $idGroupement: ID!
    $id: ID!
    $email: String
    $login: String!
    $userId: ID
    $userPhoto: FichierInput
    $codeTraitements: [String]
  ) {
    createUserPartenaireLaboratoire(
      idGroupement: $idGroupement
      id: $id
      email: $email
      login: $login
      userId: $userId
      userPhoto: $userPhoto
      codeTraitements: $codeTraitements
    ) {
      type
      id
      nomLabo
      user {
        id
        email
        login
      }
    }
  }
`;
