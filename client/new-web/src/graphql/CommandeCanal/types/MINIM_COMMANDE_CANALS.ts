/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MINIM_COMMANDE_CANALS
// ====================================================

export interface MINIM_COMMANDE_CANALS_commandeCanals {
  __typename: "CommandeCanal";
  id: string;
  libelle: string | null;
  code: string | null;
}

export interface MINIM_COMMANDE_CANALS {
  commandeCanals: (MINIM_COMMANDE_CANALS_commandeCanals | null)[] | null;
}
