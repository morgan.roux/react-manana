/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: commandeCanals
// ====================================================

export interface commandeCanals_commandeCanals {
  __typename: "CommandeCanal";
  type: string;
  id: string;
  libelle: string | null;
  code: string | null;
  countOperationsCommercials: number | null;
  countCanalArticles: number | null;
}

export interface commandeCanals {
  commandeCanals: (commandeCanals_commandeCanals | null)[] | null;
}
