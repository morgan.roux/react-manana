import gql from 'graphql-tag';

export const GET_CODE_CANAL = gql`
  {
    CurrentCanalCode @client {
      id
      code
    }
  }
`;
