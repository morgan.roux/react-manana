/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: URGENCES
// ====================================================

export interface URGENCES_urgences {
  __typename: "Urgence";
  id: string;
  code: string | null;
  couleur: string | null;
  libelle: string | null;
}

export interface URGENCES {
  urgences: (URGENCES_urgences | null)[] | null;
}
