import gql from 'graphql-tag';

export const URGENCE_FRAGMENT = gql`
  fragment UrgenceInfo on Urgence {
    id
    code
    couleur
    libelle
  }
`;
