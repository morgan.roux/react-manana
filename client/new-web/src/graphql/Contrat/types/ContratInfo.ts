/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ContratInfo
// ====================================================

export interface ContratInfo {
  __typename: "Contrat";
  id: string;
  nom: string | null;
  numeroVersion: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}
