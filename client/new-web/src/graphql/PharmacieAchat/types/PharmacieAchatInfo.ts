/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PharmacieAchatInfo
// ====================================================

export interface PharmacieAchatInfo_canal {
  __typename: "CommandeCanal";
  type: string;
  id: string;
  libelle: string | null;
  code: string | null;
  countOperationsCommercials: number | null;
  countCanalArticles: number | null;
}

export interface PharmacieAchatInfo {
  __typename: "PharmacieAchat";
  id: string;
  dateDebut: any | null;
  dateFin: any | null;
  identifiantAchatCanal: string | null;
  commentaire: string | null;
  canal: PharmacieAchatInfo_canal | null;
}
