import gql from 'graphql-tag';
import { GROUP_AMIS_DETAIL_FRAGMENT } from './fragment';

export const DO_GROUP_AMIS_DETAIL = gql`
  query GROUP_AMIS_DETAIL($type: [String], $query: JSON, $skip: Int, $take: Int, $filterBy: JSON) {
    search(type: $type, query: $query, skip: $skip, take: $take, filterBy: $filterBy) {
      total
      data {
        ... on GroupeAmisDetail {
          ...GroupAmisDetailInfo
        }
      }
    }
  }
  ${GROUP_AMIS_DETAIL_FRAGMENT}
`;

export const DO_GROUP_AMIS = gql`
  query GROUP_AMIS($id: ID!) {
    groupeAmis(id: $id) {
      nom
      id
    }
  }
`;

export const DO_PRODUIT_CANAL_PARTAGE = gql`
  query PRODUIT_CANAL_PARTAGE(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
  ) {
    search(type: $type, query: $query, skip: $skip, take: $take, filterBy: $filterBy) {
      total
      data {
        ... on ProduitCanal {
          isShared
          isActive
          isRemoved
          id
          type
          promotions {
            userCreated {
              userName
            }
          }

          userSmyleys {
            data {
              user {
                userName
              }
            }
          }
          produit {
            libelle
            service {
              nom
            }
          }
          pharmacieRemisePaliers {
            nom
          }
          pharmacieRemisePanachees {
            nom
            titulaires {
              fullName
            }
            users {
              role {
                nom
              }
            }
          }
          partage {
            typePartage
            groupeAmisCibles {
              groupeAmis {
                id
                nom
              }
            }
          }
        }
      }
    }
  }
`;
