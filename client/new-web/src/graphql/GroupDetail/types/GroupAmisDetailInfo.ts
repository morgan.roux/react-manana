/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: GroupAmisDetailInfo
// ====================================================

export interface GroupAmisDetailInfo_pharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface GroupAmisDetailInfo_pharmacie_titulaires {
  __typename: "Titulaire";
  id: string;
  fullName: string | null;
}

export interface GroupAmisDetailInfo_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  cip: string | null;
  cp: string | null;
  ville: string | null;
  departement: GroupAmisDetailInfo_pharmacie_departement | null;
  titulaires: (GroupAmisDetailInfo_pharmacie_titulaires | null)[] | null;
}

export interface GroupAmisDetailInfo_groupeAmis {
  __typename: "GroupeAmis";
  id: string;
  nbMember: number;
  nom: string | null;
}

export interface GroupAmisDetailInfo {
  __typename: "GroupeAmisDetail";
  id: string;
  dateCreation: any;
  pharmacie: GroupAmisDetailInfo_pharmacie;
  groupeAmis: GroupAmisDetailInfo_groupeAmis;
}
