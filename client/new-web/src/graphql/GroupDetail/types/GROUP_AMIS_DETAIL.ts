/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GROUP_AMIS_DETAIL
// ====================================================

export interface GROUP_AMIS_DETAIL_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface GROUP_AMIS_DETAIL_search_data_GroupeAmisDetail_pharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface GROUP_AMIS_DETAIL_search_data_GroupeAmisDetail_pharmacie_titulaires {
  __typename: "Titulaire";
  id: string;
  fullName: string | null;
}

export interface GROUP_AMIS_DETAIL_search_data_GroupeAmisDetail_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  cip: string | null;
  cp: string | null;
  ville: string | null;
  departement: GROUP_AMIS_DETAIL_search_data_GroupeAmisDetail_pharmacie_departement | null;
  titulaires: (GROUP_AMIS_DETAIL_search_data_GroupeAmisDetail_pharmacie_titulaires | null)[] | null;
}

export interface GROUP_AMIS_DETAIL_search_data_GroupeAmisDetail_groupeAmis {
  __typename: "GroupeAmis";
  id: string;
  nbMember: number;
  nom: string | null;
}

export interface GROUP_AMIS_DETAIL_search_data_GroupeAmisDetail {
  __typename: "GroupeAmisDetail";
  id: string;
  dateCreation: any;
  pharmacie: GROUP_AMIS_DETAIL_search_data_GroupeAmisDetail_pharmacie;
  groupeAmis: GROUP_AMIS_DETAIL_search_data_GroupeAmisDetail_groupeAmis;
}

export type GROUP_AMIS_DETAIL_search_data = GROUP_AMIS_DETAIL_search_data_Action | GROUP_AMIS_DETAIL_search_data_GroupeAmisDetail;

export interface GROUP_AMIS_DETAIL_search {
  __typename: "SearchResult";
  total: number;
  data: (GROUP_AMIS_DETAIL_search_data | null)[] | null;
}

export interface GROUP_AMIS_DETAIL {
  search: GROUP_AMIS_DETAIL_search | null;
}

export interface GROUP_AMIS_DETAILVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
}
