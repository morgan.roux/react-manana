/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: SearchTitulaireInfo
// ====================================================

export interface SearchTitulaireInfo_pharmacies_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface SearchTitulaireInfo_pharmacies {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  cip: string | null;
  departement: SearchTitulaireInfo_pharmacies_departement | null;
}

export interface SearchTitulaireInfo_users_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SearchTitulaireInfo_users_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
  urlPresigned: string | null;
}

export interface SearchTitulaireInfo_users_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SearchTitulaireInfo_users_userPhoto_fichier | null;
}

export interface SearchTitulaireInfo_users {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  jourNaissance: number | null;
  moisNaissance: number | null;
  anneeNaissance: number | null;
  pharmacie: SearchTitulaireInfo_users_pharmacie | null;
  userPhoto: SearchTitulaireInfo_users_userPhoto | null;
}

export interface SearchTitulaireInfo_pharmacieUser {
  __typename: "Pharmacie";
  id: string;
  cip: string | null;
  nom: string | null;
}

export interface SearchTitulaireInfo {
  __typename: "Titulaire";
  type: string;
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  estPresidentRegion: boolean | null;
  idGroupement: string | null;
  pharmacies: (SearchTitulaireInfo_pharmacies | null)[] | null;
  users: (SearchTitulaireInfo_users | null)[] | null;
  pharmacieUser: SearchTitulaireInfo_pharmacieUser | null;
}
