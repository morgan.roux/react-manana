/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ContactInput, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: createUpdateTitulaire
// ====================================================

export interface createUpdateTitulaire_createUpdateTitulaire_users_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface createUpdateTitulaire_createUpdateTitulaire_users_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: createUpdateTitulaire_createUpdateTitulaire_users_userPhoto_fichier | null;
}

export interface createUpdateTitulaire_createUpdateTitulaire_users {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  jourNaissance: number | null;
  moisNaissance: number | null;
  anneeNaissance: number | null;
  codeTraitements: (string | null)[] | null;
  userPhoto: createUpdateTitulaire_createUpdateTitulaire_users_userPhoto | null;
}

export interface createUpdateTitulaire_createUpdateTitulaire_pharmacieUser {
  __typename: "Pharmacie";
  id: string;
  cip: string | null;
}

export interface createUpdateTitulaire_createUpdateTitulaire_pharmacies {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface createUpdateTitulaire_createUpdateTitulaire_contact {
  __typename: "Contact";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
}

export interface createUpdateTitulaire_createUpdateTitulaire {
  __typename: "Titulaire";
  type: string;
  id: string;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  estPresidentRegion: boolean | null;
  idGroupement: string | null;
  users: (createUpdateTitulaire_createUpdateTitulaire_users | null)[] | null;
  pharmacieUser: createUpdateTitulaire_createUpdateTitulaire_pharmacieUser | null;
  pharmacies: (createUpdateTitulaire_createUpdateTitulaire_pharmacies | null)[] | null;
  contact: createUpdateTitulaire_createUpdateTitulaire_contact | null;
}

export interface createUpdateTitulaire {
  createUpdateTitulaire: createUpdateTitulaire_createUpdateTitulaire | null;
}

export interface createUpdateTitulaireVariables {
  id?: string | null;
  civilite?: string | null;
  nom: string;
  prenom?: string | null;
  contact?: ContactInput | null;
  idPharmacies: string[];
}
