import gql from 'graphql-tag';

export const GET_CHECKEDS_TITULAIRE_PHARMACIE = gql`
  {
    checkedsTitulairePharmacie @client {
      id
    }
  }
`;
