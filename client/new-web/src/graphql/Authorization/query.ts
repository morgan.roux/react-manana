import gql from 'graphql-tag';
import { GROUPEMENT_INFO } from '../Groupement/fragement';
import { USER_INFO_FRAGEMENT } from '../User/fragment';


const GET_AUTHORIZATION_LIST = gql`
  query AUTHORIZATION_LIST {
    rgpdAutorisations {
      id
      title
      description
      order
      dateCreation
      dateModification
      userCreation {
        ...UserInfo
      }
      groupement {
        ...GroupementInfo
      }
    }
    } 
      ${USER_INFO_FRAGEMENT}
      ${GROUPEMENT_INFO}
`;
    
export { GET_AUTHORIZATION_LIST};
