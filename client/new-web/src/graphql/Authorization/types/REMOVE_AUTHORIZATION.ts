/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: REMOVE_AUTHORIZATION
// ====================================================

export interface REMOVE_AUTHORIZATION_deleteRgpdAutorisation_userCreation_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface REMOVE_AUTHORIZATION_deleteRgpdAutorisation_userCreation_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface REMOVE_AUTHORIZATION_deleteRgpdAutorisation_userCreation_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: REMOVE_AUTHORIZATION_deleteRgpdAutorisation_userCreation_userPhoto_fichier | null;
}

export interface REMOVE_AUTHORIZATION_deleteRgpdAutorisation_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface REMOVE_AUTHORIZATION_deleteRgpdAutorisation_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: REMOVE_AUTHORIZATION_deleteRgpdAutorisation_userCreation_role | null;
  userPhoto: REMOVE_AUTHORIZATION_deleteRgpdAutorisation_userCreation_userPhoto | null;
  pharmacie: REMOVE_AUTHORIZATION_deleteRgpdAutorisation_userCreation_pharmacie | null;
}

export interface REMOVE_AUTHORIZATION_deleteRgpdAutorisation_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface REMOVE_AUTHORIZATION_deleteRgpdAutorisation_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: REMOVE_AUTHORIZATION_deleteRgpdAutorisation_groupement_defaultPharmacie_departement_region | null;
}

export interface REMOVE_AUTHORIZATION_deleteRgpdAutorisation_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: REMOVE_AUTHORIZATION_deleteRgpdAutorisation_groupement_defaultPharmacie_departement | null;
}

export interface REMOVE_AUTHORIZATION_deleteRgpdAutorisation_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface REMOVE_AUTHORIZATION_deleteRgpdAutorisation_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: REMOVE_AUTHORIZATION_deleteRgpdAutorisation_groupement_groupementLogo_fichier | null;
}

export interface REMOVE_AUTHORIZATION_deleteRgpdAutorisation_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: REMOVE_AUTHORIZATION_deleteRgpdAutorisation_groupement_defaultPharmacie | null;
  groupementLogo: REMOVE_AUTHORIZATION_deleteRgpdAutorisation_groupement_groupementLogo | null;
}

export interface REMOVE_AUTHORIZATION_deleteRgpdAutorisation {
  __typename: "RgpdAutorisation";
  id: string;
  title: string;
  description: string;
  order: number | null;
  dateCreation: any;
  dateModification: any;
  userCreation: REMOVE_AUTHORIZATION_deleteRgpdAutorisation_userCreation | null;
  groupement: REMOVE_AUTHORIZATION_deleteRgpdAutorisation_groupement | null;
}

export interface REMOVE_AUTHORIZATION {
  deleteRgpdAutorisation: REMOVE_AUTHORIZATION_deleteRgpdAutorisation | null;
}

export interface REMOVE_AUTHORIZATIONVariables {
  id: string;
}
