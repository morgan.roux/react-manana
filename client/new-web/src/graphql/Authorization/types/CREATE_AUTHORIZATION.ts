/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RgpdAutorisationInput, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_AUTHORIZATION
// ====================================================

export interface CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_userCreation_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_userCreation_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_userCreation_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_userCreation_userPhoto_fichier | null;
}

export interface CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_userCreation_role | null;
  userPhoto: CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_userCreation_userPhoto | null;
  pharmacie: CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_userCreation_pharmacie | null;
}

export interface CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_groupement_defaultPharmacie_departement_region | null;
}

export interface CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_groupement_defaultPharmacie_departement | null;
}

export interface CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_groupement_groupementLogo_fichier | null;
}

export interface CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_groupement_defaultPharmacie | null;
  groupementLogo: CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_groupement_groupementLogo | null;
}

export interface CREATE_AUTHORIZATION_createUpdateRgpdAutorisation {
  __typename: "RgpdAutorisation";
  id: string;
  title: string;
  description: string;
  order: number | null;
  dateCreation: any;
  dateModification: any;
  userCreation: CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_userCreation | null;
  groupement: CREATE_AUTHORIZATION_createUpdateRgpdAutorisation_groupement | null;
}

export interface CREATE_AUTHORIZATION {
  createUpdateRgpdAutorisation: CREATE_AUTHORIZATION_createUpdateRgpdAutorisation;
}

export interface CREATE_AUTHORIZATIONVariables {
  input: RgpdAutorisationInput;
}
