/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PharmacieComptaInfo
// ====================================================

export interface PharmacieComptaInfo {
  __typename: "PharmacieCompta";
  id: string;
  siret: string | null;
  codeERP: string | null;
  ape: string | null;
  tvaIntra: string | null;
  rcs: string | null;
  contactCompta: string | null;
  chequeGrp: boolean | null;
  valeurChequeGrp: number | null;
  commentaireChequeGrp: string | null;
  droitAccesGrp: boolean | null;
  valeurChequeAccesGrp: number | null;
  commentaireChequeAccesGrp: string | null;
  structureJuridique: string | null;
  denominationSociale: string | null;
  telCompta: string | null;
  modifStatut: boolean | null;
  raisonModif: string | null;
  dateModifStatut: any | null;
  nomBanque: string | null;
  banqueGuichet: string | null;
  banqueCompte: string | null;
  banqueRib: string | null;
  banqueCle: string | null;
  dateMajRib: any | null;
  iban: string | null;
  swift: string | null;
  dateMajIban: any | null;
}
