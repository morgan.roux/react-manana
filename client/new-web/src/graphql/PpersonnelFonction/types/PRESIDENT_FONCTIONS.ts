/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PRESIDENT_FONCTIONS
// ====================================================

export interface PRESIDENT_FONCTIONS_titulaireFonctions_data {
  __typename: "TitulaireFonction";
  id: string;
  nom: string;
  code: string;
}

export interface PRESIDENT_FONCTIONS_titulaireFonctions {
  __typename: "TitulaireFonctionResult";
  total: number;
  data: (PRESIDENT_FONCTIONS_titulaireFonctions_data | null)[] | null;
}

export interface PRESIDENT_FONCTIONS {
  titulaireFonctions: PRESIDENT_FONCTIONS_titulaireFonctions | null;
}

export interface PRESIDENT_FONCTIONSVariables {
  take?: number | null;
  skip?: number | null;
}
