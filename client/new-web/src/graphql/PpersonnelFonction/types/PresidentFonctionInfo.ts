/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PresidentFonctionInfo
// ====================================================

export interface PresidentFonctionInfo {
  __typename: "TitulaireFonction";
  id: string;
  nom: string;
  code: string;
}
