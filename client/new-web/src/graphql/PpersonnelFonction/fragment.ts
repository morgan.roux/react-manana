import gql from 'graphql-tag';

export const PRESIDENT_FONCTION_INFO_FRAGMENT = gql`
  fragment PresidentFonctionInfo on TitulaireFonction {
    id
    nom
    code
  }
`;
