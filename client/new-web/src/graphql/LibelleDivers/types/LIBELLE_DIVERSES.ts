/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: LIBELLE_DIVERSES
// ====================================================

export interface LIBELLE_DIVERSES_libelleDiverses {
  __typename: "LibelleDivers";
  id: string;
  codeInfo: number | null;
  code: number | null;
  libelle: string | null;
}

export interface LIBELLE_DIVERSES {
  libelleDiverses: (LIBELLE_DIVERSES_libelleDiverses | null)[] | null;
}

export interface LIBELLE_DIVERSESVariables {
  code: number;
}
