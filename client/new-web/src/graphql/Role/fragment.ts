import gql from 'graphql-tag';

export const ROLE_INFO_FRAGEMENT = gql`
  fragment RoleInfo on Role {
    type
    id
    code
    nom
  }
`;
