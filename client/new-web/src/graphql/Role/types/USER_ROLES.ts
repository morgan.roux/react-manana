/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: USER_ROLES
// ====================================================

export interface USER_ROLES_roles {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface USER_ROLES {
  roles: (USER_ROLES_roles | null)[] | null;
}
