/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ROLES_SEARCH
// ====================================================

export interface ROLES_SEARCH_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface ROLES_SEARCH_search_data_Role_roleTraitements {
  __typename: "RoleTraitement";
  codeTraitement: string | null;
}

export interface ROLES_SEARCH_search_data_Role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  roleTraitements: (ROLES_SEARCH_search_data_Role_roleTraitements | null)[] | null;
}

export type ROLES_SEARCH_search_data = ROLES_SEARCH_search_data_Action | ROLES_SEARCH_search_data_Role;

export interface ROLES_SEARCH_search {
  __typename: "SearchResult";
  total: number;
  data: (ROLES_SEARCH_search_data | null)[] | null;
}

export interface ROLES_SEARCH {
  search: ROLES_SEARCH_search | null;
}

export interface ROLES_SEARCHVariables {
  type?: (string | null)[] | null;
  sortBy?: any | null;
  filterBy?: any | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
