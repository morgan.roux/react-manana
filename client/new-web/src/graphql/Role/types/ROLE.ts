/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ROLE
// ====================================================

export interface ROLE_role_roleTraitements {
  __typename: "RoleTraitement";
  codeTraitement: string | null;
}

export interface ROLE_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  roleTraitements: (ROLE_role_roleTraitements | null)[] | null;
}

export interface ROLE {
  role: ROLE_role | null;
}

export interface ROLEVariables {
  id: string;
}
