import gql from 'graphql-tag';
import { ROLE_INFO_FRAGEMENT } from './fragment';

export const GET_USER_ROLES = gql`
  query USER_ROLES {
    roles {
      ...RoleInfo
    }
  }
  ${ROLE_INFO_FRAGEMENT}
`;

export const DO_SEARCH_ROLES = gql`
  query SEARCH_ROLES(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
  ) {
    search(
      type: $type
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on Role {
          id
          nom
          code
          # typeRole
          # groupeRole
        }
      }
    }
  }
`;

export const GET_ROLE = gql`
  query ROLE($id: ID!) {
    role(id: $id) {
      ...RoleInfo
      roleTraitements {
        codeTraitement
      }
    }
  }
  ${ROLE_INFO_FRAGEMENT}
`;

export const GET_ROLES_SEARCH = gql`
  query ROLES_SEARCH(
    $type: [String]
    $sortBy: JSON
    $filterBy: JSON
    $query: JSON
    $skip: Int
    $take: Int
  ) {
    search(
      type: $type
      sortBy: $sortBy
      filterBy: $filterBy
      query: $query
      take: $take
      skip: $skip
    ) {
      total
      data {
        ... on Role {
          ...RoleInfo
          roleTraitements {
            codeTraitement
          }
        }
      }
    }
  }
  ${ROLE_INFO_FRAGEMENT}
`;
