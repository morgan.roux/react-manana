import gql from 'graphql-tag';
import { FTP_INFO } from './fragment';

export const DO_CREATE_FTP = gql`
  mutation CREATE_FTP(
    $idGroupement: ID!, 
    $host: String, 
    $port: Int, 
    $user: String, 
    $password: String,
    ) {
      createFtpConnect(
        idGroupement: $idGroupement, 
        host: $host, 
        port: $port, 
        user: $user, 
        password: $password,
        ) {
          ...FtpInfo
    }
  }
  ${FTP_INFO}
`;

export const DO_UPDATE_FTP = gql`
  mutation UPDATE_FTP(
    $id: ID!
    $host: String, 
    $port: Int, 
    $user: String, 
    $password: String,
    ) {
      updateFtpConnect(
        id: $id, 
        host: $host, 
        port: $port, 
        user: $user, 
        password: $password,
        ) {
          ...FtpInfo
    }
  }
  ${FTP_INFO}
`;