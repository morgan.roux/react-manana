/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: FTP_GROUPEMENT
// ====================================================

export interface FTP_GROUPEMENT_ftpGroupement_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface FTP_GROUPEMENT_ftpGroupement_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: FTP_GROUPEMENT_ftpGroupement_groupement_defaultPharmacie_departement_region | null;
}

export interface FTP_GROUPEMENT_ftpGroupement_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: FTP_GROUPEMENT_ftpGroupement_groupement_defaultPharmacie_departement | null;
}

export interface FTP_GROUPEMENT_ftpGroupement_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface FTP_GROUPEMENT_ftpGroupement_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: FTP_GROUPEMENT_ftpGroupement_groupement_groupementLogo_fichier | null;
}

export interface FTP_GROUPEMENT_ftpGroupement_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: FTP_GROUPEMENT_ftpGroupement_groupement_defaultPharmacie | null;
  groupementLogo: FTP_GROUPEMENT_ftpGroupement_groupement_groupementLogo | null;
}

export interface FTP_GROUPEMENT_ftpGroupement {
  __typename: "FtpConnect";
  id: string;
  host: string;
  port: number | null;
  user: string | null;
  password: string | null;
  groupement: FTP_GROUPEMENT_ftpGroupement_groupement | null;
}

export interface FTP_GROUPEMENT {
  ftpGroupement: FTP_GROUPEMENT_ftpGroupement | null;
}

export interface FTP_GROUPEMENTVariables {
  idgroupement: string;
}
