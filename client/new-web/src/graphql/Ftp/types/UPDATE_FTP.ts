/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UPDATE_FTP
// ====================================================

export interface UPDATE_FTP_updateFtpConnect_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface UPDATE_FTP_updateFtpConnect_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: UPDATE_FTP_updateFtpConnect_groupement_defaultPharmacie_departement_region | null;
}

export interface UPDATE_FTP_updateFtpConnect_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: UPDATE_FTP_updateFtpConnect_groupement_defaultPharmacie_departement | null;
}

export interface UPDATE_FTP_updateFtpConnect_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface UPDATE_FTP_updateFtpConnect_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: UPDATE_FTP_updateFtpConnect_groupement_groupementLogo_fichier | null;
}

export interface UPDATE_FTP_updateFtpConnect_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: UPDATE_FTP_updateFtpConnect_groupement_defaultPharmacie | null;
  groupementLogo: UPDATE_FTP_updateFtpConnect_groupement_groupementLogo | null;
}

export interface UPDATE_FTP_updateFtpConnect {
  __typename: "FtpConnect";
  id: string;
  host: string;
  port: number | null;
  user: string | null;
  password: string | null;
  groupement: UPDATE_FTP_updateFtpConnect_groupement | null;
}

export interface UPDATE_FTP {
  updateFtpConnect: UPDATE_FTP_updateFtpConnect | null;
}

export interface UPDATE_FTPVariables {
  id: string;
  host?: string | null;
  port?: number | null;
  user?: string | null;
  password?: string | null;
}
