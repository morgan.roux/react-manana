import gql from 'graphql-tag';
import { FTP_INFO } from './fragment';

export const GET_FTP_GROUPEMENT = gql`
  query FTP_GROUPEMENT($idgroupement: ID!) {
    ftpGroupement(idgroupement: $idgroupement) {
        ...FtpInfo
    }
  }
  ${FTP_INFO}
`;
