/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { InformationLiaisonInput, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_INFORMATION_LIAISON
// ====================================================

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_item {
  __typename: "Item";
  id: string;
  name: string | null;
  code: string | null;
  codeItem: string | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_origineAssocie_Action {
  __typename: "Action" | "TodoActionType" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "LaboratoireRessource" | "LaboratoireRepresentant" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  type: string;
  nomLabo: string | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_origineAssocie_Service {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_origineAssocie_Partenaire {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_origineAssocie_GroupeAmis {
  __typename: "GroupeAmis";
  type: string;
  id: string;
  nom: string | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_origineAssocie_User {
  __typename: "User";
  type: string;
  id: string;
  userName: string | null;
}

export type CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_origineAssocie = CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_origineAssocie_Action | CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_origineAssocie_Laboratoire | CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_origineAssocie_Service | CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_origineAssocie_Partenaire | CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_origineAssocie_GroupeAmis | CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_origineAssocie_User;

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_declarant_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_declarant_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_declarant_userPhoto_fichier | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_declarant {
  __typename: "User";
  id: string;
  userName: string | null;
  userPhoto: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_declarant_userPhoto | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_importance {
  __typename: "Importance";
  id: string;
  ordre: number | null;
  libelle: string | null;
  couleur: string | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_urgence {
  __typename: "Urgence";
  id: string;
  code: string | null;
  libelle: string | null;
  couleur: string | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees_informationLiaison {
  __typename: "InformationLiaison";
  id: string;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees_userConcernee_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees_userConcernee_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees_userConcernee_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees_userConcernee_userPhoto_fichier | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees_userConcernee_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees_userConcernee {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees_userConcernee_role | null;
  userPhoto: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees_userConcernee_userPhoto | null;
  pharmacie: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees_userConcernee_pharmacie | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees_prisEnCharge_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  publicUrl: string | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees_prisEnCharge_fichiersJoints {
  __typename: "InformationLiaisonPrisChargeFichierJoint";
  id: string;
  fichier: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees_prisEnCharge_fichiersJoints_fichier;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees_prisEnCharge {
  __typename: "InformationLiaisonPrisCharge";
  id: string;
  date: any;
  description: string;
  fichiersJoints: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees_prisEnCharge_fichiersJoints[] | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees {
  __typename: "InformationLiaisonUserConcernee";
  id: string;
  statut: string;
  dateCreation: any;
  dateStatutModification: any | null;
  dateModification: any;
  informationLiaison: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees_informationLiaison;
  userConcernee: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees_userConcernee;
  prisEnCharge: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees_prisEnCharge | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_ficheReclamation {
  __typename: "Ticket";
  id: string;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_todoAction {
  __typename: "Action";
  id: string;
  description: string;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_groupement {
  __typename: "Groupement";
  id: string;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_userCreation {
  __typename: "User";
  id: string;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_userModification {
  __typename: "User";
  id: string;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_prisEnCharge_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_prisEnCharge_fichiersJoints {
  __typename: "InformationLiaisonPrisChargeFichierJoint";
  id: string;
  fichier: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_prisEnCharge_fichiersJoints_fichier;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_prisEnCharge {
  __typename: "InformationLiaisonPrisCharge";
  id: string;
  date: any;
  description: string;
  fichiersJoints: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_prisEnCharge_fichiersJoints[] | null;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_fichiersJoints {
  __typename: "InformationLiaisonFichierJoint";
  id: string;
  fichier: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_fichiersJoints_fichier;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison {
  __typename: "InformationLiaison";
  id: string;
  type: string;
  idItem: string | null;
  item: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_item | null;
  idItemAssocie: string | null;
  idType: string | null;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  origineAssocie: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_origineAssocie | null;
  statut: string;
  priority: string | null;
  bloquant: boolean | null;
  titre: string;
  description: string;
  dateCreation: any;
  dateModification: any;
  nbCollegue: number | null;
  nbComment: number | null;
  nbLue: number | null;
  declarant: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_declarant | null;
  idFonction: string | null;
  idTache: string | null;
  importance: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_importance | null;
  urgence: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_urgence | null;
  colleguesConcernees: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_colleguesConcernees[] | null;
  idFicheIncident: string | null;
  idFicheAmelioration: string | null;
  ficheReclamation: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_ficheReclamation | null;
  todoAction: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_todoAction | null;
  groupement: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_groupement | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  userCreation: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_userCreation | null;
  userModification: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_userModification | null;
  prisEnCharge: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_prisEnCharge | null;
  fichiersJoints: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_fichiersJoints[] | null;
  userSmyleys: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison_userSmyleys | null;
  nbPartage: number;
  nbRecommandation: number;
  isShared: boolean;
}

export interface CREATE_UPDATE_INFORMATION_LIAISON {
  createUpdateInformationLiaison: CREATE_UPDATE_INFORMATION_LIAISON_createUpdateInformationLiaison;
}

export interface CREATE_UPDATE_INFORMATION_LIAISONVariables {
  input: InformationLiaisonInput;
}
