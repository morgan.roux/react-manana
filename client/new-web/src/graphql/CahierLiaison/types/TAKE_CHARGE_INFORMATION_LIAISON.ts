/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TakeChargeInformationLiaisonInput, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: TAKE_CHARGE_INFORMATION_LIAISON
// ====================================================

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_item {
  __typename: "Item";
  id: string;
  name: string | null;
  code: string | null;
  codeItem: string | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_origineAssocie_Action {
  __typename: "Action" | "TodoActionType" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "LaboratoireRessource" | "LaboratoireRepresentant" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  type: string;
  nomLabo: string | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_origineAssocie_Service {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_origineAssocie_Partenaire {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_origineAssocie_GroupeAmis {
  __typename: "GroupeAmis";
  type: string;
  id: string;
  nom: string | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_origineAssocie_User {
  __typename: "User";
  type: string;
  id: string;
  userName: string | null;
}

export type TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_origineAssocie = TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_origineAssocie_Action | TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_origineAssocie_Laboratoire | TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_origineAssocie_Service | TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_origineAssocie_Partenaire | TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_origineAssocie_GroupeAmis | TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_origineAssocie_User;

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_declarant_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_declarant_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_declarant_userPhoto_fichier | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_declarant {
  __typename: "User";
  id: string;
  userName: string | null;
  userPhoto: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_declarant_userPhoto | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_importance {
  __typename: "Importance";
  id: string;
  ordre: number | null;
  libelle: string | null;
  couleur: string | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_urgence {
  __typename: "Urgence";
  id: string;
  code: string | null;
  libelle: string | null;
  couleur: string | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees_informationLiaison {
  __typename: "InformationLiaison";
  id: string;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees_userConcernee_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees_userConcernee_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees_userConcernee_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees_userConcernee_userPhoto_fichier | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees_userConcernee_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees_userConcernee {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees_userConcernee_role | null;
  userPhoto: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees_userConcernee_userPhoto | null;
  pharmacie: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees_userConcernee_pharmacie | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees_prisEnCharge_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  publicUrl: string | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees_prisEnCharge_fichiersJoints {
  __typename: "InformationLiaisonPrisChargeFichierJoint";
  id: string;
  fichier: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees_prisEnCharge_fichiersJoints_fichier;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees_prisEnCharge {
  __typename: "InformationLiaisonPrisCharge";
  id: string;
  date: any;
  description: string;
  fichiersJoints: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees_prisEnCharge_fichiersJoints[] | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees {
  __typename: "InformationLiaisonUserConcernee";
  id: string;
  statut: string;
  dateCreation: any;
  dateStatutModification: any | null;
  dateModification: any;
  informationLiaison: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees_informationLiaison;
  userConcernee: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees_userConcernee;
  prisEnCharge: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees_prisEnCharge | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_ficheReclamation {
  __typename: "Ticket";
  id: string;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_todoAction {
  __typename: "Action";
  id: string;
  description: string;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_groupement {
  __typename: "Groupement";
  id: string;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_userCreation {
  __typename: "User";
  id: string;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_userModification {
  __typename: "User";
  id: string;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_prisEnCharge_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_prisEnCharge_fichiersJoints {
  __typename: "InformationLiaisonPrisChargeFichierJoint";
  id: string;
  fichier: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_prisEnCharge_fichiersJoints_fichier;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_prisEnCharge {
  __typename: "InformationLiaisonPrisCharge";
  id: string;
  date: any;
  description: string;
  fichiersJoints: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_prisEnCharge_fichiersJoints[] | null;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_fichiersJoints {
  __typename: "InformationLiaisonFichierJoint";
  id: string;
  fichier: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_fichiersJoints_fichier;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison {
  __typename: "InformationLiaison";
  id: string;
  type: string;
  idItem: string | null;
  item: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_item | null;
  idItemAssocie: string | null;
  idType: string | null;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  origineAssocie: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_origineAssocie | null;
  statut: string;
  priority: string | null;
  bloquant: boolean | null;
  titre: string;
  description: string;
  dateCreation: any;
  dateModification: any;
  nbCollegue: number | null;
  nbComment: number | null;
  nbLue: number | null;
  declarant: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_declarant | null;
  idFonction: string | null;
  idTache: string | null;
  importance: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_importance | null;
  urgence: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_urgence | null;
  colleguesConcernees: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_colleguesConcernees[] | null;
  idFicheIncident: string | null;
  idFicheAmelioration: string | null;
  ficheReclamation: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_ficheReclamation | null;
  todoAction: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_todoAction | null;
  groupement: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_groupement | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  userCreation: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_userCreation | null;
  userModification: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_userModification | null;
  prisEnCharge: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_prisEnCharge | null;
  fichiersJoints: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_fichiersJoints[] | null;
  userSmyleys: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison_userSmyleys | null;
  nbPartage: number;
  nbRecommandation: number;
  isShared: boolean;
}

export interface TAKE_CHARGE_INFORMATION_LIAISON {
  takeChargeInformationLiaison: TAKE_CHARGE_INFORMATION_LIAISON_takeChargeInformationLiaison;
}

export interface TAKE_CHARGE_INFORMATION_LIAISONVariables {
  input: TakeChargeInformationLiaisonInput;
}
