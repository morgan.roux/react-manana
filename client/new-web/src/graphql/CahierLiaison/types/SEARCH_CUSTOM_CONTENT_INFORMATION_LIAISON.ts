/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_item {
  __typename: "Item";
  id: string;
  name: string | null;
  code: string | null;
  codeItem: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_Action {
  __typename: "Action" | "TodoActionType" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "LaboratoireRessource" | "LaboratoireRepresentant" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  type: string;
  nomLabo: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_Service {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_Partenaire {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_GroupeAmis {
  __typename: "GroupeAmis";
  type: string;
  id: string;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_User {
  __typename: "User";
  type: string;
  id: string;
  userName: string | null;
}

export type SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie = SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_Action | SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_Laboratoire | SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_Service | SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_Partenaire | SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_GroupeAmis | SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_User;

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_declarant_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_declarant_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_declarant_userPhoto_fichier | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_declarant {
  __typename: "User";
  id: string;
  userName: string | null;
  userPhoto: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_declarant_userPhoto | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_importance {
  __typename: "Importance";
  id: string;
  ordre: number | null;
  libelle: string | null;
  couleur: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_urgence {
  __typename: "Urgence";
  id: string;
  code: string | null;
  libelle: string | null;
  couleur: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_informationLiaison {
  __typename: "InformationLiaison";
  id: string;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_userConcernee_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_userConcernee_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_userConcernee_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_userConcernee_userPhoto_fichier | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_userConcernee_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_userConcernee {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_userConcernee_role | null;
  userPhoto: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_userConcernee_userPhoto | null;
  pharmacie: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_userConcernee_pharmacie | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_prisEnCharge_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  publicUrl: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_prisEnCharge_fichiersJoints {
  __typename: "InformationLiaisonPrisChargeFichierJoint";
  id: string;
  fichier: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_prisEnCharge_fichiersJoints_fichier;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_prisEnCharge {
  __typename: "InformationLiaisonPrisCharge";
  id: string;
  date: any;
  description: string;
  fichiersJoints: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_prisEnCharge_fichiersJoints[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees {
  __typename: "InformationLiaisonUserConcernee";
  id: string;
  statut: string;
  dateCreation: any;
  dateStatutModification: any | null;
  dateModification: any;
  informationLiaison: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_informationLiaison;
  userConcernee: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_userConcernee;
  prisEnCharge: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_prisEnCharge | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_ficheReclamation {
  __typename: "Ticket";
  id: string;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_todoAction {
  __typename: "Action";
  id: string;
  description: string;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_groupement {
  __typename: "Groupement";
  id: string;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_userCreation {
  __typename: "User";
  id: string;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_userModification {
  __typename: "User";
  id: string;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_prisEnCharge_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_prisEnCharge_fichiersJoints {
  __typename: "InformationLiaisonPrisChargeFichierJoint";
  id: string;
  fichier: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_prisEnCharge_fichiersJoints_fichier;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_prisEnCharge {
  __typename: "InformationLiaisonPrisCharge";
  id: string;
  date: any;
  description: string;
  fichiersJoints: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_prisEnCharge_fichiersJoints[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_fichiersJoints {
  __typename: "InformationLiaisonFichierJoint";
  id: string;
  fichier: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_fichiersJoints_fichier;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison {
  __typename: "InformationLiaison";
  id: string;
  type: string;
  idItem: string | null;
  item: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_item | null;
  idItemAssocie: string | null;
  idType: string | null;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  origineAssocie: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie | null;
  statut: string;
  priority: string | null;
  bloquant: boolean | null;
  titre: string;
  description: string;
  dateCreation: any;
  dateModification: any;
  nbCollegue: number | null;
  nbComment: number | null;
  nbLue: number | null;
  declarant: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_declarant | null;
  idFonction: string | null;
  idTache: string | null;
  importance: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_importance | null;
  urgence: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_urgence | null;
  colleguesConcernees: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees[] | null;
  idFicheIncident: string | null;
  idFicheAmelioration: string | null;
  ficheReclamation: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_ficheReclamation | null;
  todoAction: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_todoAction | null;
  groupement: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_groupement | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  userCreation: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_userCreation | null;
  userModification: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_userModification | null;
  prisEnCharge: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_prisEnCharge | null;
  fichiersJoints: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_fichiersJoints[] | null;
  userSmyleys: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison_userSmyleys | null;
  nbPartage: number;
  nbRecommandation: number;
  isShared: boolean;
}

export type SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data = SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_Action | SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data_InformationLiaison;

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON {
  search: SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISONVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
}
