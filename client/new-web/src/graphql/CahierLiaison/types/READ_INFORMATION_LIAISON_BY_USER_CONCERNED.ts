/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: READ_INFORMATION_LIAISON_BY_USER_CONCERNED
// ====================================================

export interface READ_INFORMATION_LIAISON_BY_USER_CONCERNED_readInformationLiaisonByUserConcerned {
  __typename: "InformationLiaisonUserConcernee";
  id: string;
  statut: string;
}

export interface READ_INFORMATION_LIAISON_BY_USER_CONCERNED {
  readInformationLiaisonByUserConcerned: READ_INFORMATION_LIAISON_BY_USER_CONCERNED_readInformationLiaisonByUserConcerned;
}

export interface READ_INFORMATION_LIAISON_BY_USER_CONCERNEDVariables {
  id: string;
  lu: boolean;
}
