import gql from 'graphql-tag';
import { USER_INFO_FRAGEMENT } from '../User/fragment';

export const INFORMATION_LIAISON_INFO_FRAGEMENT = gql`
  fragment InformationLiaisonInfo on InformationLiaison {
    id
    type
    idItem
    item{
      id
      name
      code
      codeItem
    }
    idItemAssocie
    idType
    idOrigine
    idOrigineAssocie
    origineAssocie{
            ...on Laboratoire{
              id
              type  
              nomLabo
            }
            ...on Service{
              type
              id
              code
              nom
            }
            ...on Partenaire{
              type
              id
              nom
            }

            ...on GroupeAmis{
              type
              id
              nom
            }
            ... on User{
              type
              id
              userName
            }
          }
    statut
    priority
    bloquant
    titre
    description
    dateCreation
    dateModification
    nbCollegue
    nbComment
    nbLue
    declarant {
      id
      userName
      userPhoto {
        id
        fichier {
          id
          urlPresigned
          publicUrl
        }
      }
    }
    idFonction
    idTache
    importance {
      id
      ordre
      libelle
      couleur
    }
    urgence {
      id
      code
      libelle
      couleur
    }
    colleguesConcernees {
      id
      statut
      dateCreation
      dateStatutModification
      dateModification
      informationLiaison {
        id
      }
      userConcernee {
        ...UserInfo
      }
      prisEnCharge {
        id
        date
        description
        fichiersJoints {
          id
          fichier {
            id
            chemin
            nomOriginal
            type
            publicUrl
          }
        }
      }
    }
    idFicheIncident
    idFicheAmelioration
    ficheReclamation {
      id
    }
    todoAction {
      id
      description
    }
    groupement {
      id
    }
    codeMaj
    isRemoved
    userCreation {
      id
    }
    userModification {
      id
    }
    dateCreation
    dateModification
    prisEnCharge {
      id
      date
      description
      fichiersJoints {
        id
        fichier {
          id
          publicUrl
          chemin
          nomOriginal
          type
        }
      }
    }
    fichiersJoints {
      id
      fichier {
        id
        publicUrl
        chemin
        nomOriginal
        type
      }
    }
    userSmyleys {
      total
    }
    nbPartage
    nbRecommandation
    isShared
  }
  ${USER_INFO_FRAGEMENT}
`;
