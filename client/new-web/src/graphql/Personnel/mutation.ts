import gql from 'graphql-tag';
import { PERSONNEL_INFO_FRAGMENT } from './fragment';

export const DO_CREATE_PERSONNEL = gql`
  mutation CREATE_PERSONNEL(
    $id: ID
    $codeService: String!
    $respHierarch: ID
    $sortie: Int
    $dateSortie: DateTime
    $civilite: String
    $nom: String
    $prenom: String
    $sexe: Sexe
    $contact: ContactInput
    $commentaire: String
    $idGroupement: ID
  ) {
    createUpdatePersonnel(
      id: $id
      codeService: $codeService
      respHierarch: $respHierarch
      sortie: $sortie
      dateSortie: $dateSortie
      civilite: $civilite
      nom: $nom
      prenom: $prenom
      sexe: $sexe
      contact: $contact
      commentaire: $commentaire
      idGroupement: $idGroupement
    ) {
      ...PersonnelInfo
    }
  }
  ${PERSONNEL_INFO_FRAGMENT}
`;

export const DO_DELETE_PERSONNELS = gql`
  mutation DELETE_PERSONNELS($ids: [ID!]!) {
    deletePersonnels(ids: $ids) {
      ...PersonnelInfo
    }
  }
  ${PERSONNEL_INFO_FRAGMENT}
`;
