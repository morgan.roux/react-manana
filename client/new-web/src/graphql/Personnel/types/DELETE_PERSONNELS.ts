/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Sexe, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_PERSONNELS
// ====================================================

export interface DELETE_PERSONNELS_deletePersonnels_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface DELETE_PERSONNELS_deletePersonnels_respHierarch {
  __typename: "Personnel";
  id: string;
}

export interface DELETE_PERSONNELS_deletePersonnels_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface DELETE_PERSONNELS_deletePersonnels__user {
  __typename: "User";
  id: string;
  status: UserStatus | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface DELETE_PERSONNELS_deletePersonnels_contact {
  __typename: "Contact";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
}

export interface DELETE_PERSONNELS_deletePersonnels {
  __typename: "Personnel";
  type: string;
  id: string;
  service: DELETE_PERSONNELS_deletePersonnels_service | null;
  respHierarch: DELETE_PERSONNELS_deletePersonnels_respHierarch | null;
  sortie: number | null;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  sexe: Sexe | null;
  fullName: string | null;
  commentaire: string | null;
  dateSortie: any | null;
  idGroupement: string | null;
  role: DELETE_PERSONNELS_deletePersonnels_role | null;
  _user: DELETE_PERSONNELS_deletePersonnels__user | null;
  contact: DELETE_PERSONNELS_deletePersonnels_contact | null;
}

export interface DELETE_PERSONNELS {
  deletePersonnels: (DELETE_PERSONNELS_deletePersonnels | null)[] | null;
}

export interface DELETE_PERSONNELSVariables {
  ids: string[];
}
