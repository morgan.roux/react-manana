/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Sexe, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: PersonnelInfo
// ====================================================

export interface PersonnelInfo_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface PersonnelInfo_respHierarch {
  __typename: "Personnel";
  id: string;
}

export interface PersonnelInfo_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PersonnelInfo__user {
  __typename: "User";
  id: string;
  status: UserStatus | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PersonnelInfo_contact {
  __typename: "Contact";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
}

export interface PersonnelInfo {
  __typename: "Personnel";
  type: string;
  id: string;
  service: PersonnelInfo_service | null;
  respHierarch: PersonnelInfo_respHierarch | null;
  sortie: number | null;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  sexe: Sexe | null;
  fullName: string | null;
  commentaire: string | null;
  dateSortie: any | null;
  idGroupement: string | null;
  role: PersonnelInfo_role | null;
  _user: PersonnelInfo__user | null;
  contact: PersonnelInfo_contact | null;
}
