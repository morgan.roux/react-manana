/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: SearchPersonnelInfo
// ====================================================

export interface SearchPersonnelInfo__user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
  urlPresigned: string | null;
}

export interface SearchPersonnelInfo__user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SearchPersonnelInfo__user_userPhoto_fichier | null;
}

export interface SearchPersonnelInfo__user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userPhoto: SearchPersonnelInfo__user_userPhoto | null;
}

export interface SearchPersonnelInfo_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SearchPersonnelInfo_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface SearchPersonnelInfo {
  __typename: "Personnel";
  type: string;
  id: string;
  sortie: number | null;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  commentaire: string | null;
  dateSortie: any | null;
  idGroupement: string | null;
  _user: SearchPersonnelInfo__user | null;
  role: SearchPersonnelInfo_role | null;
  service: SearchPersonnelInfo_service | null;
}
