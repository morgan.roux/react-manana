/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search_data_Personnel_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
  urlPresigned: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search_data_Personnel_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search_data_Personnel_user_userPhoto_fichier | null;
}

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search_data_Personnel_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userPhoto: SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search_data_Personnel_user_userPhoto | null;
}

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search_data_Personnel_contact {
  __typename: "Contact";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search_data_Personnel_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search_data_Personnel_service {
  __typename: "Service";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search_data_Personnel {
  __typename: "Personnel";
  id: string;
  sortie: number | null;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  commentaire: string | null;
  dateSortie: any | null;
  idGroupement: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  user: SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search_data_Personnel_user | null;
  contact: SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search_data_Personnel_contact | null;
  role: SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search_data_Personnel_role | null;
  service: SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search_data_Personnel_service | null;
}

export type SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search_data = SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search_data_Action | SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search_data_Personnel;

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT {
  search: SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENTVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
