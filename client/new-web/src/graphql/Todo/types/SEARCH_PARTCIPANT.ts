/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_PARTCIPANT
// ====================================================

export interface SEARCH_PARTCIPANT_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_PARTCIPANT_search_data_Project_participants_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface SEARCH_PARTCIPANT_search_data_Project_participants_pharmacie {
  __typename: "Pharmacie";
  id: string;
}

export interface SEARCH_PARTCIPANT_search_data_Project_participants {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  role: SEARCH_PARTCIPANT_search_data_Project_participants_role | null;
  pharmacie: SEARCH_PARTCIPANT_search_data_Project_participants_pharmacie | null;
}

export interface SEARCH_PARTCIPANT_search_data_Project_userCreation_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface SEARCH_PARTCIPANT_search_data_Project_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
  role: SEARCH_PARTCIPANT_search_data_Project_userCreation_role | null;
}

export interface SEARCH_PARTCIPANT_search_data_Project {
  __typename: "Project";
  id: string;
  participants: SEARCH_PARTCIPANT_search_data_Project_participants[] | null;
  userCreation: SEARCH_PARTCIPANT_search_data_Project_userCreation | null;
}

export type SEARCH_PARTCIPANT_search_data = SEARCH_PARTCIPANT_search_data_Action | SEARCH_PARTCIPANT_search_data_Project;

export interface SEARCH_PARTCIPANT_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_PARTCIPANT_search_data | null)[] | null;
}

export interface SEARCH_PARTCIPANT {
  search: SEARCH_PARTCIPANT_search | null;
}

export interface SEARCH_PARTCIPANTVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  filterBy?: any | null;
  sortBy?: any | null;
  skip?: number | null;
  take?: number | null;
}
