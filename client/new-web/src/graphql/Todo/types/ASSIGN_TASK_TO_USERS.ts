/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ActionStatus, TypeProject } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: ASSIGN_TASK_TO_USERS
// ====================================================

export interface ASSIGN_TASK_TO_USERS_assignActionUsers_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface ASSIGN_TASK_TO_USERS_assignActionUsers_origine {
  __typename: "TodoActionOrigine";
  id: string;
  code: string;
  libelle: string;
}

export interface ASSIGN_TASK_TO_USERS_assignActionUsers_section {
  __typename: "TodoSection";
  id: string;
  ordre: number | null;
  libelle: string;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
}

export interface ASSIGN_TASK_TO_USERS_assignActionUsers_actionParent {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  nbComment: number;
}

export interface ASSIGN_TASK_TO_USERS_assignActionUsers_subActions_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface ASSIGN_TASK_TO_USERS_assignActionUsers_subActions {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: ASSIGN_TASK_TO_USERS_assignActionUsers_subActions_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbComment: number;
}

export interface ASSIGN_TASK_TO_USERS_assignActionUsers_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
}

export interface ASSIGN_TASK_TO_USERS_assignActionUsers_project {
  __typename: "Project";
  id: string;
  name: string | null;
  typeProject: TypeProject | null;
}

export interface ASSIGN_TASK_TO_USERS_assignActionUsers_userCreation_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface ASSIGN_TASK_TO_USERS_assignActionUsers_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  role: ASSIGN_TASK_TO_USERS_assignActionUsers_userCreation_role | null;
}

export interface ASSIGN_TASK_TO_USERS_assignActionUsers_assignedUsers_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface ASSIGN_TASK_TO_USERS_assignActionUsers_assignedUsers_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: ASSIGN_TASK_TO_USERS_assignActionUsers_assignedUsers_userPhoto_fichier | null;
}

export interface ASSIGN_TASK_TO_USERS_assignActionUsers_assignedUsers {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  userPhoto: ASSIGN_TASK_TO_USERS_assignActionUsers_assignedUsers_userPhoto | null;
}

export interface ASSIGN_TASK_TO_USERS_assignActionUsers_etiquettes_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface ASSIGN_TASK_TO_USERS_assignActionUsers_etiquettes {
  __typename: "TodoEtiquette";
  id: string;
  ordre: number | null;
  nom: string;
  isRemoved: boolean | null;
  couleur: ASSIGN_TASK_TO_USERS_assignActionUsers_etiquettes_couleur | null;
}

export interface ASSIGN_TASK_TO_USERS_assignActionUsers {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  _priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: ASSIGN_TASK_TO_USERS_assignActionUsers_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  idItemAssocie: string | null;
  nbComment: number;
  origine: ASSIGN_TASK_TO_USERS_assignActionUsers_origine | null;
  section: ASSIGN_TASK_TO_USERS_assignActionUsers_section | null;
  actionParent: ASSIGN_TASK_TO_USERS_assignActionUsers_actionParent | null;
  subActions: ASSIGN_TASK_TO_USERS_assignActionUsers_subActions[] | null;
  item: ASSIGN_TASK_TO_USERS_assignActionUsers_item | null;
  project: ASSIGN_TASK_TO_USERS_assignActionUsers_project | null;
  userCreation: ASSIGN_TASK_TO_USERS_assignActionUsers_userCreation | null;
  assignedUsers: ASSIGN_TASK_TO_USERS_assignActionUsers_assignedUsers[] | null;
  etiquettes: ASSIGN_TASK_TO_USERS_assignActionUsers_etiquettes[] | null;
}

export interface ASSIGN_TASK_TO_USERS {
  assignActionUsers: ASSIGN_TASK_TO_USERS_assignActionUsers;
}

export interface ASSIGN_TASK_TO_USERSVariables {
  idAction: string;
  idUserDestinations: string[];
}
