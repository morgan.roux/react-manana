/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PARTICIPANT_LIST
// ====================================================

export interface PARTICIPANT_LIST_project_participants_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface PARTICIPANT_LIST_project_participants {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  role: PARTICIPANT_LIST_project_participants_role | null;
}

export interface PARTICIPANT_LIST_project_userCreation_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface PARTICIPANT_LIST_project_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
  role: PARTICIPANT_LIST_project_userCreation_role | null;
}

export interface PARTICIPANT_LIST_project {
  __typename: "Project";
  participants: PARTICIPANT_LIST_project_participants[] | null;
  userCreation: PARTICIPANT_LIST_project_userCreation | null;
}

export interface PARTICIPANT_LIST {
  project: PARTICIPANT_LIST_project | null;
}

export interface PARTICIPANT_LISTVariables {
  id: string;
}
