/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RemoveParticipantInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: deleteParticipant
// ====================================================

export interface deleteParticipant_removeParticipantFromProject {
  __typename: "Project";
  id: string;
}

export interface deleteParticipant {
  removeParticipantFromProject: deleteParticipant_removeParticipantFromProject;
}

export interface deleteParticipantVariables {
  input: RemoveParticipantInput;
}
