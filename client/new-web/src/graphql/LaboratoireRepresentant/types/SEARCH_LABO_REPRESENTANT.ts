/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePartenaire, StatutPartenaire, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_LABO_REPRESENTANT
// ====================================================

export interface SEARCH_LABO_REPRESENTANT_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_contact {
  __typename: "Contact";
  id: string;
  adresse1: string | null;
  ville: string | null;
  mailProf: string | null;
  siteProf: string | null;
  mailPerso: string | null;
  telProf: string | null;
  telPerso: string | null;
  cp: string | null;
  urlLinkedinProf: string | null;
  urlFacebookProf: string | null;
  whatsAppMobProf: string | null;
  urlMessenger: string | null;
  urlYoutube: string | null;
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_laboratoire_laboSuite {
  __typename: "LaboratoireSuite";
  id: string;
  ville: string | null;
  adresse: string | null;
  codePostal: string | null;
  telephone: string | null;
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_laboratoire_actualites_fichierPresentations {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_laboratoire_actualites {
  __typename: "Actualite";
  id: string;
  libelle: string | null;
  dateCreation: any | null;
  description: string | null;
  fichierPresentations: (SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_laboratoire_actualites_fichierPresentations | null)[] | null;
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_laboratoire_laboratoirePartenaire {
  __typename: "LaboratoirePartenaire";
  id: string;
  debutPartenaire: any | null;
  finPartenaire: any | null;
  typePartenaire: TypePartenaire | null;
  statutPartenaire: StatutPartenaire | null;
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_laboratoire_photo {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_laboratoire {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  countCanalArticles: number | null;
  sortie: number | null;
  laboSuite: SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_laboratoire_laboSuite | null;
  actualites: (SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_laboratoire_actualites | null)[] | null;
  laboratoirePartenaire: SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_laboratoire_laboratoirePartenaire | null;
  photo: SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_laboratoire_photo | null;
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userCreation_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userCreation_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userCreation_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userCreation_userPhoto_fichier | null;
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userCreation_role | null;
  userPhoto: SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userCreation_userPhoto | null;
  pharmacie: SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userCreation_pharmacie | null;
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userModification_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userModification_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userModification_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userModification_userPhoto_fichier | null;
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userModification_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userModification {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userModification_role | null;
  userPhoto: SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userModification_userPhoto | null;
  pharmacie: SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userModification_pharmacie | null;
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_photo {
  __typename: "Fichier";
  id: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  nomOriginal: string;
  chemin: string;
}

export interface SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant {
  __typename: "LaboratoireRepresentant";
  type: string;
  id: string;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  sexe: string | null;
  fonction: string | null;
  commentaire: string | null;
  afficherComme: string | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  contact: SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_contact | null;
  laboratoire: SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_laboratoire | null;
  userCreation: SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userCreation | null;
  userModification: SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_userModification | null;
  photo: SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant_photo | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export type SEARCH_LABO_REPRESENTANT_search_data = SEARCH_LABO_REPRESENTANT_search_data_Action | SEARCH_LABO_REPRESENTANT_search_data_LaboratoireRepresentant;

export interface SEARCH_LABO_REPRESENTANT_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_LABO_REPRESENTANT_search_data | null)[] | null;
}

export interface SEARCH_LABO_REPRESENTANT {
  search: SEARCH_LABO_REPRESENTANT_search | null;
}

export interface SEARCH_LABO_REPRESENTANTVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
}
