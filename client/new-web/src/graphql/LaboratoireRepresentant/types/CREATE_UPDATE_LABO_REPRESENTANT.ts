/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { LaboratoireRepresentantInput, TypePartenaire, StatutPartenaire, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_LABO_REPRESENTANT
// ====================================================

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_contact {
  __typename: "Contact";
  id: string;
  adresse1: string | null;
  ville: string | null;
  mailProf: string | null;
  siteProf: string | null;
  mailPerso: string | null;
  telProf: string | null;
  telPerso: string | null;
  cp: string | null;
  urlLinkedinProf: string | null;
  urlFacebookProf: string | null;
  whatsAppMobProf: string | null;
  urlMessenger: string | null;
  urlYoutube: string | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_laboratoire_laboSuite {
  __typename: "LaboratoireSuite";
  id: string;
  ville: string | null;
  adresse: string | null;
  codePostal: string | null;
  telephone: string | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_laboratoire_actualites_fichierPresentations {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_laboratoire_actualites {
  __typename: "Actualite";
  id: string;
  libelle: string | null;
  dateCreation: any | null;
  description: string | null;
  fichierPresentations: (CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_laboratoire_actualites_fichierPresentations | null)[] | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_laboratoire_laboratoirePartenaire {
  __typename: "LaboratoirePartenaire";
  id: string;
  debutPartenaire: any | null;
  finPartenaire: any | null;
  typePartenaire: TypePartenaire | null;
  statutPartenaire: StatutPartenaire | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_laboratoire_photo {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_laboratoire {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  countCanalArticles: number | null;
  sortie: number | null;
  laboSuite: CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_laboratoire_laboSuite | null;
  actualites: (CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_laboratoire_actualites | null)[] | null;
  laboratoirePartenaire: CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_laboratoire_laboratoirePartenaire | null;
  photo: CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_laboratoire_photo | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userCreation_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userCreation_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userCreation_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userCreation_userPhoto_fichier | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userCreation_role | null;
  userPhoto: CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userCreation_userPhoto | null;
  pharmacie: CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userCreation_pharmacie | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userModification_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userModification_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userModification_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userModification_userPhoto_fichier | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userModification_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userModification {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userModification_role | null;
  userPhoto: CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userModification_userPhoto | null;
  pharmacie: CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userModification_pharmacie | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_photo {
  __typename: "Fichier";
  id: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  nomOriginal: string;
  chemin: string;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant {
  __typename: "LaboratoireRepresentant";
  type: string;
  id: string;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  sexe: string | null;
  fonction: string | null;
  commentaire: string | null;
  afficherComme: string | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  contact: CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_contact | null;
  laboratoire: CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_laboratoire | null;
  userCreation: CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userCreation | null;
  userModification: CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_userModification | null;
  photo: CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant_photo | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANT {
  createUpdateLaboratoireRepresentant: CREATE_UPDATE_LABO_REPRESENTANT_createUpdateLaboratoireRepresentant | null;
}

export interface CREATE_UPDATE_LABO_REPRESENTANTVariables {
  input: LaboratoireRepresentantInput;
}
