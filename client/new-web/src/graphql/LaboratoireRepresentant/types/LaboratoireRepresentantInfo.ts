/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePartenaire, StatutPartenaire, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: LaboratoireRepresentantInfo
// ====================================================

export interface LaboratoireRepresentantInfo_contact {
  __typename: "Contact";
  id: string;
  adresse1: string | null;
  ville: string | null;
  mailProf: string | null;
  siteProf: string | null;
  mailPerso: string | null;
  telProf: string | null;
  telPerso: string | null;
  cp: string | null;
  urlLinkedinProf: string | null;
  urlFacebookProf: string | null;
  whatsAppMobProf: string | null;
  urlMessenger: string | null;
  urlYoutube: string | null;
}

export interface LaboratoireRepresentantInfo_laboratoire_laboSuite {
  __typename: "LaboratoireSuite";
  id: string;
  ville: string | null;
  adresse: string | null;
  codePostal: string | null;
  telephone: string | null;
}

export interface LaboratoireRepresentantInfo_laboratoire_actualites_fichierPresentations {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface LaboratoireRepresentantInfo_laboratoire_actualites {
  __typename: "Actualite";
  id: string;
  libelle: string | null;
  dateCreation: any | null;
  description: string | null;
  fichierPresentations: (LaboratoireRepresentantInfo_laboratoire_actualites_fichierPresentations | null)[] | null;
}

export interface LaboratoireRepresentantInfo_laboratoire_laboratoirePartenaire {
  __typename: "LaboratoirePartenaire";
  id: string;
  debutPartenaire: any | null;
  finPartenaire: any | null;
  typePartenaire: TypePartenaire | null;
  statutPartenaire: StatutPartenaire | null;
}

export interface LaboratoireRepresentantInfo_laboratoire_photo {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface LaboratoireRepresentantInfo_laboratoire {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  countCanalArticles: number | null;
  sortie: number | null;
  laboSuite: LaboratoireRepresentantInfo_laboratoire_laboSuite | null;
  actualites: (LaboratoireRepresentantInfo_laboratoire_actualites | null)[] | null;
  laboratoirePartenaire: LaboratoireRepresentantInfo_laboratoire_laboratoirePartenaire | null;
  photo: LaboratoireRepresentantInfo_laboratoire_photo | null;
}

export interface LaboratoireRepresentantInfo_userCreation_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface LaboratoireRepresentantInfo_userCreation_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface LaboratoireRepresentantInfo_userCreation_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: LaboratoireRepresentantInfo_userCreation_userPhoto_fichier | null;
}

export interface LaboratoireRepresentantInfo_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface LaboratoireRepresentantInfo_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: LaboratoireRepresentantInfo_userCreation_role | null;
  userPhoto: LaboratoireRepresentantInfo_userCreation_userPhoto | null;
  pharmacie: LaboratoireRepresentantInfo_userCreation_pharmacie | null;
}

export interface LaboratoireRepresentantInfo_userModification_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface LaboratoireRepresentantInfo_userModification_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface LaboratoireRepresentantInfo_userModification_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: LaboratoireRepresentantInfo_userModification_userPhoto_fichier | null;
}

export interface LaboratoireRepresentantInfo_userModification_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface LaboratoireRepresentantInfo_userModification {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: LaboratoireRepresentantInfo_userModification_role | null;
  userPhoto: LaboratoireRepresentantInfo_userModification_userPhoto | null;
  pharmacie: LaboratoireRepresentantInfo_userModification_pharmacie | null;
}

export interface LaboratoireRepresentantInfo_photo {
  __typename: "Fichier";
  id: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  nomOriginal: string;
  chemin: string;
}

export interface LaboratoireRepresentantInfo {
  __typename: "LaboratoireRepresentant";
  type: string;
  id: string;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  sexe: string | null;
  fonction: string | null;
  commentaire: string | null;
  afficherComme: string | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  contact: LaboratoireRepresentantInfo_contact | null;
  laboratoire: LaboratoireRepresentantInfo_laboratoire | null;
  userCreation: LaboratoireRepresentantInfo_userCreation | null;
  userModification: LaboratoireRepresentantInfo_userModification | null;
  photo: LaboratoireRepresentantInfo_photo | null;
  dateCreation: any | null;
  dateModification: any | null;
}
