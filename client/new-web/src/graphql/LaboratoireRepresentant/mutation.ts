import gql from 'graphql-tag';
import { LABORATOIRE_REPRESENTANT_INFO } from './fragments';

export const DO_CREATE_UPDATE_LABO_REPRESENTANT = gql`
  mutation CREATE_UPDATE_LABO_REPRESENTANT($input: LaboratoireRepresentantInput!) {
    createUpdateLaboratoireRepresentant(input: $input) {
      ...LaboratoireRepresentantInfo
    }
  }
  ${LABORATOIRE_REPRESENTANT_INFO}
`;

export const DO_DELETE_LABO_REPRESENTANT = gql`
  mutation DELETE_LABO_REPRESENTANT($ids: [ID!]!) {
    softDeleteLaboratoireRepresentants(ids: $ids) {
      ...LaboratoireRepresentantInfo
    }
  }
  ${LABORATOIRE_REPRESENTANT_INFO}
`;
