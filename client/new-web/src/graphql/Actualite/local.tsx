import gql from 'graphql-tag';

export const GET_ACTU_CIBLE = gql`
  {
    actuCible @client
  }
`;

export const GET_SELECTED_ROLES = gql`
  {
    selectedRoles @client
  }
`;

export const GET_SHOW_SELECTABLE_CIBLE = gql`
  {
    showSelectableCible @client
  }
`;

export const GET_SHOW_SUB_SIDEBAR = gql`
  {
    showSubSidebar @client
  }
`;

export const GET_CHECKEDS_CIBLE_PARENT = gql`
  {
    checkedsCibleParent @client
  }
`;

export const GET_CLICKED_NEXT_ON_STEPPER = gql`
  {
    clickedNextOnStepper @client
  }
`;

export const GET_CLICKED_CIBLE = gql`
  {
    clickedCible @client {
      code
      name
      total
      __typename
    }
  }
`;

export const GET_CHECKEDS_ON_CIBLE = gql`
  {
    checkedsOnCible @client {
      checkedsOnGroupement {
        type
        id
        code
        nom
        countUsers
      }
      checkedsOnPharmacie {
        pharmacies {
          type
          id
          sortie
          cip
          numFiness
          nom
          departement {
            id
            nom
          }
          titulaires {
            id
            nom
            prenom
            fullName
          }
          adresse1
          adresse2
          cp
          ville
          dateSortie
          idGroupement
          actived
        }
        roles
      }
      checkedsOnPresident {
        presidents {
          type
          id
          nom
          prenom
          fullName
          estPresidentRegion
          idGroupement
          pharmacies {
            id
          }
        }
        type
      }
      checkedsOnLaboratoire {
        type
        id
        nomLabo
        sortie
      }
      checkedsOnPartenaire {
        type
        id
        nom
        idGroupement
      }
    }
  }
`;

export const GET_GENERATE_ACTION = gql`
  {
    generateAction @client
  }
`;

export const GET_CHECKEDS_ACTUALITE = gql`
  {
    checkedsActualite @client {
      id
      libelle
      description
    }
  }
`;
