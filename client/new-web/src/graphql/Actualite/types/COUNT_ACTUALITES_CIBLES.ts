/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: COUNT_ACTUALITES_CIBLES
// ====================================================

export interface COUNT_ACTUALITES_CIBLES_countActualitesCibles {
  __typename: "ActualitesCiblesCountResult";
  code: string;
  name: string;
  total: number;
}

export interface COUNT_ACTUALITES_CIBLES {
  countActualitesCibles: (COUNT_ACTUALITES_CIBLES_countActualitesCibles | null)[] | null;
}
