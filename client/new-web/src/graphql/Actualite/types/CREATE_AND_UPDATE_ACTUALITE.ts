/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FichierInput, PharmacieRolesInput, PresidentRegionsInput, TypePresidentCible, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_AND_UPDATE_ACTUALITE
// ====================================================

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_item_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_item_parent_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_item_parent {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_item_parent_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_item {
  __typename: "Item";
  type: string;
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_item_fichier | null;
  parent: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_item_parent | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_origine {
  __typename: "ActualiteOrigine";
  id: string;
  code: string | null;
  libelle: string | null;
  ordre: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_fichierPresentations {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_actualiteCible_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  type: string;
  nomOriginal: string;
  publicUrl: string | null;
  urlPresigned: string | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_actualiteCible {
  __typename: "ActualiteCible";
  id: string | null;
  globalite: boolean | null;
  fichierCible: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_actualiteCible_fichierCible | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_servicesCible {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  countUsers: number | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_comments {
  __typename: "CommentResult";
  total: number;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_user_role | null;
  userPhoto: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_user_userPhoto | null;
  pharmacie: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_user_pharmacie | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_smyley_groupement | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_item | null;
  idSource: string | null;
  user: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_user | null;
  smyley: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys_data | null)[] | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_pharmaciesCible_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_pharmaciesCible_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_pharmaciesCible {
  __typename: "Pharmacie";
  type: string;
  id: string;
  sortie: number | null;
  cip: string | null;
  numFiness: string | null;
  nom: string | null;
  departement: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_pharmaciesCible_departement | null;
  titulaires: (CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_pharmaciesCible_titulaires | null)[] | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  idGroupement: string | null;
  actived: boolean | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_pharmaciesRolesCible {
  __typename: "Role";
  id: string;
  code: string | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_presidentsCibles_pharmacies {
  __typename: "Pharmacie";
  id: string;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_presidentsCibles {
  __typename: "Titulaire";
  type: string;
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  estPresidentRegion: boolean | null;
  idGroupement: string | null;
  pharmacies: (CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_presidentsCibles_pharmacies | null)[] | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_partenairesCible {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
  idGroupement: string | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_laboratoiresCible {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_operation {
  __typename: "Operation";
  id: string;
  commandePassee: boolean | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_action_project {
  __typename: "Project";
  id: string;
  name: string | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_action {
  __typename: "Action";
  id: string;
  description: string;
  priority: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  project: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_action_project | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_importance {
  __typename: "Importance";
  id: string;
  ordre: number | null;
  libelle: string | null;
  couleur: string | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_urgence {
  __typename: "Urgence";
  id: string;
  code: string | null;
  libelle: string | null;
  couleur: string | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite {
  __typename: "Actualite";
  type: string;
  id: string;
  idGroupement: string | null;
  idTache: string | null;
  idFonction: string | null;
  dateEcheance: any | null;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  isRemoved: boolean | null;
  description: string | null;
  actionAuto: boolean | null;
  niveauPriorite: number | null;
  typePresidentCible: TypePresidentCible | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbSeenByPharmacie: number | null;
  nbPartage: number;
  seen: boolean | null;
  item: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_item | null;
  origine: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_origine | null;
  laboratoire: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_laboratoire | null;
  fichierPresentations: (CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_fichierPresentations | null)[] | null;
  actualiteCible: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_actualiteCible | null;
  servicesCible: (CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_servicesCible | null)[] | null;
  comments: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_comments | null;
  userSmyleys: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_userSmyleys | null;
  pharmaciesCible: (CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_pharmaciesCible | null)[] | null;
  pharmaciesRolesCible: (CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_pharmaciesRolesCible | null)[] | null;
  presidentsCibles: (CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_presidentsCibles | null)[] | null;
  partenairesCible: (CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_partenairesCible | null)[] | null;
  laboratoiresCible: (CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_laboratoiresCible | null)[] | null;
  operation: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_operation | null;
  action: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_action | null;
  importance: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_importance | null;
  urgence: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite_urgence | null;
}

export interface CREATE_AND_UPDATE_ACTUALITE {
  createUpdateActualite: CREATE_AND_UPDATE_ACTUALITE_createUpdateActualite | null;
}

export interface CREATE_AND_UPDATE_ACTUALITEVariables {
  id?: string | null;
  codeItem: string;
  codeOrigine: string;
  description?: string | null;
  actionAuto?: boolean | null;
  niveauPriorite?: number | null;
  idLaboratoire?: string | null;
  libelle?: string | null;
  dateDebut: string;
  dateFin: string;
  fichierPresentations: FichierInput[];
  globalite?: boolean | null;
  fichierCible?: FichierInput | null;
  pharmacieRoles?: PharmacieRolesInput | null;
  presidentRegions?: PresidentRegionsInput | null;
  services?: string[] | null;
  laboratoires?: string[] | null;
  partenaires?: string[] | null;
  idProject?: string | null;
  actionPriorite?: number | null;
  actionDescription?: string | null;
  actionDueDate?: string | null;
  userId?: string | null;
  idPharmacieUser?: string | null;
  idImportance: string;
  idUrgence: string;
  idTache?: string | null;
  idFonction?: string | null;
}
