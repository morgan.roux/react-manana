/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ActualiteCibleInfo
// ====================================================

export interface ActualiteCibleInfo_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  type: string;
  nomOriginal: string;
  publicUrl: string | null;
  urlPresigned: string | null;
}

export interface ActualiteCibleInfo {
  __typename: "ActualiteCible";
  id: string | null;
  globalite: boolean | null;
  fichierCible: ActualiteCibleInfo_fichierCible | null;
  dateCreation: any | null;
  dateModification: any | null;
}
