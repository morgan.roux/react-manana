/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SearchActualiteOrigineInfo
// ====================================================

export interface SearchActualiteOrigineInfo {
  __typename: "ActualiteOrigine";
  type: string;
  id: string;
  code: string | null;
  libelle: string | null;
  ordre: number | null;
  countActualites: number | null;
}
