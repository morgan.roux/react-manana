/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ACTUALITE_ORIGINES_BY_USER_ROLE
// ====================================================

export interface ACTUALITE_ORIGINES_BY_USER_ROLE_actualiteOriginesByRole {
  __typename: "ActualiteOrigine";
  id: string;
  code: string | null;
  libelle: string | null;
  ordre: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ACTUALITE_ORIGINES_BY_USER_ROLE {
  actualiteOriginesByRole: (ACTUALITE_ORIGINES_BY_USER_ROLE_actualiteOriginesByRole | null)[] | null;
}

export interface ACTUALITE_ORIGINES_BY_USER_ROLEVariables {
  codeRole: string;
}
