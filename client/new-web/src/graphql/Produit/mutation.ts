import gql from 'graphql-tag';

export const CREATE_UPDATE_ARTICLE = gql`
  mutation createUpdateArticle(
    $idProduit: ID
    $libelle: String
    $dateSuppression: String
    $isGenerique: Int
    $fichierImage: FichierInput
    $typeCodeReferent: Int
    $codeReference: String
    $codeFamille: String
    $idCategorie: Int
    $codeTva: Int
    $codeActe: Int
    $pxAchat: Float
    $pxVente: Float
    $codeListe: Int
    $codeTauxss: Int
    $codeLibelleStockage: Int
    $codeLibelleStatut: Int
    $codeLibelleCasher: Int
    $codeLibelleConditionnement: Int
    $idLaboTitulaire: String
    $idLaboExploitant: String
    $idLaboDistributaire: String
    $produitCanaux: [ProduitCanalInput]
  ) {
    createUpdateProduit(
      idProduit: $idProduit
      libelle: $libelle
      dateSuppression: $dateSuppression
      isGenerique: $isGenerique
      fichierImage: $fichierImage
      typeCodeReferent: $typeCodeReferent
      codeReference: $codeReference
      codeFamille: $codeFamille
      idCategorie: $idCategorie
      codeTva: $codeTva
      codeActe: $codeActe
      pxAchat: $pxAchat
      pxVente: $pxVente
      codeListe: $codeListe
      codeTauxss: $codeTauxss
      codeLibelleStockage: $codeLibelleStockage
      codeLibelleStatut: $codeLibelleStatut
      codeLibelleCasher: $codeLibelleCasher
      codeLibelleConditionnement: $codeLibelleConditionnement
      idLaboTitulaire: $idLaboTitulaire
      idLaboExploitant: $idLaboExploitant
      idLaboDistributaire: $idLaboDistributaire
      produitCanaux: $produitCanaux
    ) {
      id
      canauxArticle {
        id
      }
    }
  }
`;

export const UPDATE_PRODUIT_DATE_SUPPRIMER = gql`
  mutation updateProduitSupprimer($id: ID!, $date: String!) {
    updateProduitSupprimer(id: $id, date: $date) {
      id
      supprimer
    }
  }
`;
