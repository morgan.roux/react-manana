/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PRODUIT_TYPE_CODES
// ====================================================

export interface PRODUIT_TYPE_CODES_produitTypeCodes {
  __typename: "ProduitTypeCode";
  id: string;
  resipIdTypeCode: number | null;
  libelle: string | null;
}

export interface PRODUIT_TYPE_CODES {
  produitTypeCodes: (PRODUIT_TYPE_CODES_produitTypeCodes | null)[] | null;
}
