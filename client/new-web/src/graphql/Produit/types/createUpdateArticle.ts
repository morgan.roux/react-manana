/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FichierInput, ProduitCanalInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: createUpdateArticle
// ====================================================

export interface createUpdateArticle_createUpdateProduit_canauxArticle {
  __typename: "ProduitCanal";
  id: string;
}

export interface createUpdateArticle_createUpdateProduit {
  __typename: "Produit";
  id: string;
  canauxArticle: (createUpdateArticle_createUpdateProduit_canauxArticle | null)[] | null;
}

export interface createUpdateArticle {
  createUpdateProduit: createUpdateArticle_createUpdateProduit | null;
}

export interface createUpdateArticleVariables {
  idProduit?: string | null;
  libelle?: string | null;
  dateSuppression?: string | null;
  isGenerique?: number | null;
  fichierImage?: FichierInput | null;
  typeCodeReferent?: number | null;
  codeReference?: string | null;
  codeFamille?: string | null;
  idCategorie?: number | null;
  codeTva?: number | null;
  codeActe?: number | null;
  pxAchat?: number | null;
  pxVente?: number | null;
  codeListe?: number | null;
  codeTauxss?: number | null;
  codeLibelleStockage?: number | null;
  codeLibelleStatut?: number | null;
  codeLibelleCasher?: number | null;
  codeLibelleConditionnement?: number | null;
  idLaboTitulaire?: string | null;
  idLaboExploitant?: string | null;
  idLaboDistributaire?: string | null;
  produitCanaux?: (ProduitCanalInput | null)[] | null;
}
