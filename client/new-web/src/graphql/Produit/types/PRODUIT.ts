/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PRODUIT
// ====================================================

export interface PRODUIT_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PRODUIT_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: PRODUIT_produit_produitPhoto_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PRODUIT_produit_categorie {
  __typename: "ProduitCategorie";
  id: string;
  resipIdCategorie: number | null;
  libelle: string | null;
}

export interface PRODUIT_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface PRODUIT_produit_famille {
  __typename: "Famille";
  id: string;
  codeFamille: string | null;
  libelleFamille: string | null;
}

export interface PRODUIT_produit_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface PRODUIT_produit_produitTechReg_laboDistirbuteur {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface PRODUIT_produit_produitTechReg_laboTitulaire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface PRODUIT_produit_produitTechReg_acte {
  __typename: "Acte";
  id: string;
  codeActe: number | null;
  libelleActe: string | null;
}

export interface PRODUIT_produit_produitTechReg_liste {
  __typename: "Liste";
  id: string;
  codeListe: number | null;
  libelle: string | null;
}

export interface PRODUIT_produit_produitTechReg_libelleStockage {
  __typename: "LibelleDivers";
  id: string;
  codeInfo: number | null;
  code: number | null;
}

export interface PRODUIT_produit_produitTechReg_tauxss {
  __typename: "TauxSS";
  id: string;
  codeTaux: number | null;
  tauxSS: number | null;
}

export interface PRODUIT_produit_produitTechReg_tva {
  __typename: "TVA";
  id: string;
  codeTva: number | null;
  tauxTva: number | null;
}

export interface PRODUIT_produit_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  isTipsLpp: number | null;
  isHomeo: number | null;
  prixAchat: number | null;
  prixAchatHT: number | null;
  prixVenteTTC: number | null;
  laboExploitant: PRODUIT_produit_produitTechReg_laboExploitant | null;
  laboDistirbuteur: PRODUIT_produit_produitTechReg_laboDistirbuteur | null;
  laboTitulaire: PRODUIT_produit_produitTechReg_laboTitulaire | null;
  acte: PRODUIT_produit_produitTechReg_acte | null;
  liste: PRODUIT_produit_produitTechReg_liste | null;
  libelleStockage: PRODUIT_produit_produitTechReg_libelleStockage | null;
  tauxss: PRODUIT_produit_produitTechReg_tauxss | null;
  tva: PRODUIT_produit_produitTechReg_tva | null;
}

export interface PRODUIT_produit_canauxArticle_sousGammeCommercial_gammeCommercial {
  __typename: "CanalGamme";
  id: string;
  codeGamme: number | null;
  libelle: string | null;
  estDefaut: number | null;
}

export interface PRODUIT_produit_canauxArticle_sousGammeCommercial {
  __typename: "CanalSousGamme";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
  estDefaut: number | null;
  gammeCommercial: PRODUIT_produit_canauxArticle_sousGammeCommercial_gammeCommercial | null;
  countCanalArticles: number | null;
}

export interface PRODUIT_produit_canauxArticle_sousGammeCatalogue_gammeCatalogue {
  __typename: "GammeCatalogue";
  id: string;
  codeGamme: number | null;
  libelle: string | null;
  numOrdre: number | null;
  estDefaut: number | null;
}

export interface PRODUIT_produit_canauxArticle_sousGammeCatalogue {
  __typename: "SousGammeCatalogue";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
  numOrdre: number | null;
  estDefaut: number | null;
  gammeCatalogue: PRODUIT_produit_canauxArticle_sousGammeCatalogue_gammeCatalogue | null;
}

export interface PRODUIT_produit_canauxArticle_commandeCanal {
  __typename: "CommandeCanal";
  type: string;
  id: string;
  libelle: string | null;
  code: string | null;
  countOperationsCommercials: number | null;
  countCanalArticles: number | null;
}

export interface PRODUIT_produit_canauxArticle {
  __typename: "ProduitCanal";
  id: string;
  qteStock: number | null;
  stv: number | null;
  unitePetitCond: number | null;
  prixFab: number | null;
  prixPhv: number | null;
  datePeremption: any | null;
  dateRetrait: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  sousGammeCommercial: PRODUIT_produit_canauxArticle_sousGammeCommercial | null;
  sousGammeCatalogue: PRODUIT_produit_canauxArticle_sousGammeCatalogue | null;
  commandeCanal: PRODUIT_produit_canauxArticle_commandeCanal | null;
  isRemoved: boolean | null;
}

export interface PRODUIT_produit {
  __typename: "Produit";
  id: string;
  produitPhoto: PRODUIT_produit_produitPhoto | null;
  libelle: string | null;
  libelle2: string | null;
  isReferentGenerique: number | null;
  supprimer: any | null;
  surveillanceRenforcee: number | null;
  categorie: PRODUIT_produit_categorie | null;
  produitCode: PRODUIT_produit_produitCode | null;
  famille: PRODUIT_produit_famille | null;
  produitTechReg: PRODUIT_produit_produitTechReg | null;
  canauxArticle: (PRODUIT_produit_canauxArticle | null)[] | null;
}

export interface PRODUIT {
  produit: PRODUIT_produit | null;
}

export interface PRODUITVariables {
  id: string;
}
