/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeFichier } from '../../../types/graphql-global-types';

// ====================================================
// GraphQL query operation: ARTICLE
// ====================================================

export interface ARTICLE_article_articlePhoto_fichier {
  __typename: 'Fichier';
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: TypeFichier | null;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ARTICLE_article_articlePhoto {
  __typename: 'ArticlePhoto';
  id: string;
  fichier: ARTICLE_article_articlePhoto_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ARTICLE_article_tva {
  __typename: 'TrTVA';
  type: string;
  id: string;
  codeTva: string | null;
  valTva: number | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ARTICLE_article_tableau {
  __typename: 'TrTableau';
  type: string;
  id: string;
  codeTableau: string | null;
  libelle: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ARTICLE_article_marge {
  __typename: 'TrMarge';
  type: string;
  id: string;
  codeMarge: string | null;
  libelle: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ARTICLE_article_forme {
  __typename: 'TrForme';
  type: string;
  id: string;
  codeForme: string | null;
  libelle: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ARTICLE_article_stockage {
  __typename: 'TrStockage';
  type: string;
  id: string;
  codeStockage: string | null;
  libelle: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ARTICLE_article_rbtArticle {
  __typename: 'RbtArticle';
  type: string;
  id: string;
  codeRbt: string | null;
  libelle: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ARTICLE_article_acte {
  __typename: 'TrActe';
  type: string;
  id: string;
  codeActe: number | null;
  libelle: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ARTICLE_article_laboratoire_actualites_fichierPresentations {
  __typename: 'Fichier';
  id: string;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface ARTICLE_article_laboratoire_actualites {
  __typename: 'Actualite';
  id: string;
  libelle: string | null;
  dateCreation: any | null;
  description: string | null;
  fichierPresentations:
    | (ARTICLE_article_laboratoire_actualites_fichierPresentations | null)[]
    | null;
}

export interface ARTICLE_article_laboratoire {
  __typename: 'Laboratoire';
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  codeGroupLabo: string | null;
  countCanalArticles: number | null;
  sortie: number | null;
  actualites: (ARTICLE_article_laboratoire_actualites | null)[] | null;
}

export interface ARTICLE_article_famille {
  __typename: 'TrFamille';
  id: string;
  idFamille: number;
  codeFamille: string;
  nomFamille: string | null;
  majFamille: string | null;
}

export interface ARTICLE_article_canauxArticle_laboratoire_actualites_fichierPresentations {
  __typename: 'Fichier';
  id: string;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface ARTICLE_article_canauxArticle_laboratoire_actualites {
  __typename: 'Actualite';
  id: string;
  libelle: string | null;
  dateCreation: any | null;
  description: string | null;
  fichierPresentations:
    | (ARTICLE_article_canauxArticle_laboratoire_actualites_fichierPresentations | null)[]
    | null;
}

export interface ARTICLE_article_canauxArticle_laboratoire {
  __typename: 'Laboratoire';
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  codeGroupLabo: string | null;
  countCanalArticles: number | null;
  sortie: number | null;
  actualites: (ARTICLE_article_canauxArticle_laboratoire_actualites | null)[] | null;
}

export interface ARTICLE_article_canauxArticle_sousGammeCommercial_gammeCommercial {
  __typename: 'CanalGamme';
  id: string;
  idGamme: number | null;
  codeGamme: number | null;
  libelle: string | null;
  estDefaut: number | null;
}

export interface ARTICLE_article_canauxArticle_sousGammeCommercial {
  __typename: 'CanalSousGamme';
  id: string;
  idSousGamme: number | null;
  codeSousGamme: number | null;
  libelle: string | null;
  estDefaut: number | null;
  gammeCommercial: ARTICLE_article_canauxArticle_sousGammeCommercial_gammeCommercial | null;
  countCanalArticles: number | null;
}

export interface ARTICLE_article_canauxArticle_sousGammeCatalogue_gammeCatalogue {
  __typename: 'GammeCatalogue';
  id: string;
  codeGamme: number | null;
  libelle: string | null;
  numOrdre: number | null;
  estDefaut: number | null;
}

export interface ARTICLE_article_canauxArticle_sousGammeCatalogue {
  __typename: 'SousGammeCatalogue';
  id: string;
  idSousGamme: number | null;
  codeSousGamme: number | null;
  libelle: string | null;
  numOrdre: number | null;
  estDefaut: number | null;
  gammeCatalogue: ARTICLE_article_canauxArticle_sousGammeCatalogue_gammeCatalogue | null;
}

export interface ARTICLE_article_canauxArticle_commandeCanal {
  __typename: 'CommandeCanal';
  type: string;
  id: string;
  libelle: string | null;
  code: string | null;
  countOperationsCommercials: number | null;
  countCanalArticles: number | null;
}

export interface ARTICLE_article_canauxArticle {
  __typename: 'CanalArticle';
  id: string;
  qteStock: number | null;
  stv: number | null;
  unitePetitCond: number | null;
  prixFab: number | null;
  prixPhv: number | null;
  laboratoire: ARTICLE_article_canauxArticle_laboratoire | null;
  datePeremption: any | null;
  dateRetrait: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  sousGammeCommercial: ARTICLE_article_canauxArticle_sousGammeCommercial | null;
  sousGammeCatalogue: ARTICLE_article_canauxArticle_sousGammeCatalogue | null;
  commandeCanal: ARTICLE_article_canauxArticle_commandeCanal | null;
  isRemoved: boolean | null;
}

export interface ARTICLE_article {
  __typename: 'Article';
  id: string;
  idArticle: number | null;
  articlePhoto: ARTICLE_article_articlePhoto | null;
  nomArticle: string | null;
  typeCodeReferent: string | null;
  codeReference: string | null;
  pxAchatArticle: number | null;
  pxVenteArticle: number | null;
  tva: ARTICLE_article_tva | null;
  tableau: ARTICLE_article_tableau | null;
  marge: ARTICLE_article_marge | null;
  forme: ARTICLE_article_forme | null;
  stockage: ARTICLE_article_stockage | null;
  rbtArticle: ARTICLE_article_rbtArticle | null;
  acte: ARTICLE_article_acte | null;
  laboratoire: ARTICLE_article_laboratoire | null;
  famille: ARTICLE_article_famille | null;
  dateSuppression: any | null;
  hospitalArticle: number | null;
  generiqArticle: number | null;
  carteFidelite: number | null;
  canauxArticle: (ARTICLE_article_canauxArticle | null)[] | null;
}

export interface ARTICLE {
  article: ARTICLE_article | null;
}

export interface ARTICLEVariables {
  idArticle: string;
}
