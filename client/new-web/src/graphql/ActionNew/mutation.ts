import gql from 'graphql-tag';

export const DO_CREATE_UPDATE_PROJECT = gql`
  mutation CREATE_UPDATE_PROJECT($input: ProjectInput!) {
    createUpdateProject(input: $input) {
      id
      name
    }
  }
`;

export const DO_DELETE_PROJECT = gql`
  mutation DELETE_PROJECT($ids: [ID!]!) {
    softDeleteProjects(ids: $ids) {
      id
      type
    }
  }
`;

export const DO_CREATE_UPDATE_ACTION = gql`
  mutation CREATE_UPDATE_ACTION($input: ActionInput!) {
    createUpdateAction(input: $input) {
      id
      type
    }
  }
`;

export const DO_DELETE_ACTIONS = gql`
  mutation DELETE_ACTIONS($ids: [ID!]!) {
    softDeleteActions(ids: $ids) {
      id
      type
      isRemoved
    }
  }
`;

// export const DO_ASSIGN_ACTION_TO_USER = gql`
//   mutation ASSIGN_ACTION_TO_USER($input: AssignOrUnAssignActionInput!) {
//     assignActionToUser(input: $input) {
//       id
//       type
//     }
//   }
// `;

export const DO_UNASSING_USER_FROM_ACTION = gql`
  mutation UNASSING_USER_FROM_ACTION($input: AssignOrUnAssignActionInput!) {
    unAssignUserInAction(input: $input) {
      id
      type
    }
  }
`;
