/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ActionStatus, TypeProject, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: TODO_ACTION_ATTRIBUTIONS
// ====================================================

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_origine {
  __typename: "TodoActionOrigine";
  id: string;
  code: string;
  libelle: string;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_section {
  __typename: "TodoSection";
  id: string;
  ordre: number | null;
  libelle: string;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_actionParent {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  nbComment: number;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_subActions_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_subActions {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_subActions_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbComment: number;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_project {
  __typename: "Project";
  id: string;
  name: string | null;
  typeProject: TypeProject | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_userCreation_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  role: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_userCreation_role | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_assignedUsers_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_assignedUsers_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_assignedUsers_userPhoto_fichier | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_assignedUsers {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  userPhoto: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_assignedUsers_userPhoto | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_etiquettes_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_etiquettes {
  __typename: "TodoEtiquette";
  id: string;
  ordre: number | null;
  nom: string;
  isRemoved: boolean | null;
  couleur: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_etiquettes_couleur | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  _priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  idItemAssocie: string | null;
  nbComment: number;
  origine: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_origine | null;
  section: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_section | null;
  actionParent: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_actionParent | null;
  subActions: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_subActions[] | null;
  item: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_item | null;
  project: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_project | null;
  userCreation: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_userCreation | null;
  assignedUsers: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_assignedUsers[] | null;
  etiquettes: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action_etiquettes[] | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userSource_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userSource_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userSource_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userSource_userPhoto_fichier | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userSource_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userSource {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userSource_role | null;
  userPhoto: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userSource_userPhoto | null;
  pharmacie: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userSource_pharmacie | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userDestination_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userDestination_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userDestination_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userDestination_userPhoto_fichier | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userDestination_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userDestination {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userDestination_role | null;
  userPhoto: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userDestination_userPhoto | null;
  pharmacie: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userDestination_pharmacie | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userCreation_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userCreation_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userCreation_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userCreation_userPhoto_fichier | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userCreation_role | null;
  userPhoto: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userCreation_userPhoto | null;
  pharmacie: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userCreation_pharmacie | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userModification_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userModification_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userModification_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userModification_userPhoto_fichier | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userModification_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userModification {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userModification_role | null;
  userPhoto: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userModification_userPhoto | null;
  pharmacie: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userModification_pharmacie | null;
}

export interface TODO_ACTION_ATTRIBUTIONS_todoActionAttributions {
  __typename: "TodoActionAttribution";
  id: string;
  action: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_action;
  userSource: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userSource;
  userDestination: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userDestination;
  isRemoved: boolean | null;
  userCreation: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userCreation | null;
  userModification: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions_userModification | null;
  dateCreation: any;
  dateModification: any;
}

export interface TODO_ACTION_ATTRIBUTIONS {
  todoActionAttributions: TODO_ACTION_ATTRIBUTIONS_todoActionAttributions[];
}

export interface TODO_ACTION_ATTRIBUTIONSVariables {
  isRemoved?: boolean | null;
}
