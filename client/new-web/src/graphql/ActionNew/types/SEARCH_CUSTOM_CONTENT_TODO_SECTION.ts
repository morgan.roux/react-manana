/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeProject, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_TODO_SECTION
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_projetParent {
  __typename: "Project";
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_user_userPhoto_fichier | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_user_role | null;
  userPhoto: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_user_userPhoto | null;
  pharmacie: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_user_pharmacie | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_subProjects_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_subProjects_projetParent {
  __typename: "Project";
  id: string;
  _name: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_subProjects_participants {
  __typename: "User";
  id: string;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_subProjects_user {
  __typename: "User";
  id: string;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_subProjects {
  __typename: "Project";
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  isRemoved: boolean | null;
  isInFavoris: boolean | null;
  ordre: number | null;
  nbAction: number;
  couleur: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_subProjects_couleur | null;
  projetParent: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_subProjects_projetParent | null;
  participants: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_subProjects_participants[] | null;
  user: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_subProjects_user;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_participants {
  __typename: "User";
  id: string;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project {
  __typename: "Project";
  type: string;
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  activeActions: boolean | null;
  isRemoved: boolean | null;
  isInFavoris: boolean | null;
  idFonction: string | null;
  idTache: string | null;
  ordre: number | null;
  nbAction: number;
  projetParent: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_projetParent | null;
  couleur: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_couleur | null;
  groupement: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_groupement | null;
  user: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_user;
  subProjects: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_subProjects[] | null;
  dateCreation: any | null;
  dateModification: any | null;
  participants: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project_participants[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_user_userPhoto_fichier | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_user_role | null;
  userPhoto: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_user_userPhoto | null;
  pharmacie: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_user_pharmacie | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userCreation_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userCreation_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userCreation_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userCreation_userPhoto_fichier | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userCreation_role | null;
  userPhoto: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userCreation_userPhoto | null;
  pharmacie: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userCreation_pharmacie | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userModification_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userModification_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userModification_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userModification_userPhoto_fichier | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userModification_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userModification {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userModification_role | null;
  userPhoto: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userModification_userPhoto | null;
  pharmacie: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userModification_pharmacie | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection {
  __typename: "TodoSection";
  id: string;
  type: string;
  ordre: number | null;
  libelle: string;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  project: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_project | null;
  user: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_user;
  isRemoved: boolean | null;
  isArchived: boolean | null;
  codeMaj: string | null;
  userCreation: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userCreation | null;
  userModification: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection_userModification | null;
  dateCreation: any;
  dateModification: any;
}

export type SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data = SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_Action | SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data_TodoSection;

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_TODO_SECTION_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTION {
  search: SEARCH_CUSTOM_CONTENT_TODO_SECTION_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_TODO_SECTIONVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  actionStatus?: string | null;
}
