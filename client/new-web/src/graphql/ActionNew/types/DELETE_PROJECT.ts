/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_PROJECT
// ====================================================

export interface DELETE_PROJECT_softDeleteProjects {
  __typename: "Project";
  id: string;
  type: string;
}

export interface DELETE_PROJECT {
  softDeleteProjects: DELETE_PROJECT_softDeleteProjects[];
}

export interface DELETE_PROJECTVariables {
  ids: string[];
}
