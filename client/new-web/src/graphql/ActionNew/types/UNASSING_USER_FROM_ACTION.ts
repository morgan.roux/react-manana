/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { AssignOrUnAssignActionInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: UNASSING_USER_FROM_ACTION
// ====================================================

export interface UNASSING_USER_FROM_ACTION_unAssignUserInAction {
  __typename: "Action";
  id: string;
  type: string;
}

export interface UNASSING_USER_FROM_ACTION {
  unAssignUserInAction: UNASSING_USER_FROM_ACTION_unAssignUserInAction;
}

export interface UNASSING_USER_FROM_ACTIONVariables {
  input: AssignOrUnAssignActionInput;
}
