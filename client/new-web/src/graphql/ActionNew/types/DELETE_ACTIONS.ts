/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_ACTIONS
// ====================================================

export interface DELETE_ACTIONS_softDeleteActions {
  __typename: "Action";
  id: string;
  type: string;
  isRemoved: boolean | null;
}

export interface DELETE_ACTIONS {
  softDeleteActions: DELETE_ACTIONS_softDeleteActions[];
}

export interface DELETE_ACTIONSVariables {
  ids: string[];
}
