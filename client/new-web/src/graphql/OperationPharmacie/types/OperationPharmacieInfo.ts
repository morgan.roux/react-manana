/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: OperationPharmacieInfo
// ====================================================

export interface OperationPharmacieInfo_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OperationPharmacieInfo_pharmaciedetails_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface OperationPharmacieInfo_pharmaciedetails {
  __typename: "OperationPharmacieDetail";
  id: string;
  pharmacie: OperationPharmacieInfo_pharmaciedetails_pharmacie | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OperationPharmacieInfo {
  __typename: "OperationPharmacie";
  id: string;
  globalite: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
  fichierCible: OperationPharmacieInfo_fichierCible | null;
  pharmaciedetails: (OperationPharmacieInfo_pharmaciedetails | null)[] | null;
}
