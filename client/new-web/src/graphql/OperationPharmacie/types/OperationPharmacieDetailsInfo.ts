/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: OperationPharmacieDetailsInfo
// ====================================================

export interface OperationPharmacieDetailsInfo_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface OperationPharmacieDetailsInfo {
  __typename: "OperationPharmacieDetail";
  id: string;
  pharmacie: OperationPharmacieDetailsInfo_pharmacie | null;
  dateCreation: any | null;
  dateModification: any | null;
}
