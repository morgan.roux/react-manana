/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { AffectationInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: AFFECT_PERSONNEL
// ====================================================

export interface AFFECT_PERSONNEL_createUpdatePersonnelAffectation_personnelFonction {
  __typename: "PersonnelFonction";
  id: string;
  nom: string;
  code: string;
}

export interface AFFECT_PERSONNEL_createUpdatePersonnelAffectation_departement {
  __typename: "Departement";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface AFFECT_PERSONNEL_createUpdatePersonnelAffectation_personnel {
  __typename: "Personnel";
  id: string;
  fullName: string | null;
}

export interface AFFECT_PERSONNEL_createUpdatePersonnelAffectation_personnelDemandeAffectation_personnelRemplacent {
  __typename: "Personnel";
  id: string;
  fullName: string | null;
}

export interface AFFECT_PERSONNEL_createUpdatePersonnelAffectation_personnelDemandeAffectation {
  __typename: "PersonnelDemandeAffectation";
  id: string;
  personnelRemplacent: AFFECT_PERSONNEL_createUpdatePersonnelAffectation_personnelDemandeAffectation_personnelRemplacent | null;
}

export interface AFFECT_PERSONNEL_createUpdatePersonnelAffectation {
  __typename: "PersonnelAffectation";
  id: string;
  personnelFonction: AFFECT_PERSONNEL_createUpdatePersonnelAffectation_personnelFonction | null;
  departement: AFFECT_PERSONNEL_createUpdatePersonnelAffectation_departement | null;
  personnel: AFFECT_PERSONNEL_createUpdatePersonnelAffectation_personnel | null;
  personnelDemandeAffectation: AFFECT_PERSONNEL_createUpdatePersonnelAffectation_personnelDemandeAffectation | null;
  dateDebut: any | null;
  dateFin: any | null;
}

export interface AFFECT_PERSONNEL {
  /**
   * Affectation
   */
  createUpdatePersonnelAffectation: (AFFECT_PERSONNEL_createUpdatePersonnelAffectation | null)[] | null;
}

export interface AFFECT_PERSONNELVariables {
  inputs?: AffectationInput | null;
}
