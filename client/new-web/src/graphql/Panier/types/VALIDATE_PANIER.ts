/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: VALIDATE_PANIER
// ====================================================

export interface VALIDATE_PANIER_validatePanierAndCreateCommande {
  __typename: "Commande";
  id: string;
}

export interface VALIDATE_PANIER {
  validatePanierAndCreateCommande: VALIDATE_PANIER_validatePanierAndCreateCommande | null;
}

export interface VALIDATE_PANIERVariables {
  idPanier: string;
  idPharmacie: string;
  commentaireInterne?: string | null;
  commentaireExterne?: string | null;
  codeCanalCommande: string;
}
