/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: articleSamePanachees
// ====================================================

export interface articleSamePanachees_produitCanal_articleSamePanachees_operations {
  __typename: "Operation";
  id: string;
}

export interface articleSamePanachees_produitCanal_articleSamePanachees_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface articleSamePanachees_produitCanal_articleSamePanachees_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface articleSamePanachees_produitCanal_articleSamePanachees_produit_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface articleSamePanachees_produitCanal_articleSamePanachees_produit_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  laboExploitant: articleSamePanachees_produitCanal_articleSamePanachees_produit_produitTechReg_laboExploitant | null;
}

export interface articleSamePanachees_produitCanal_articleSamePanachees_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  produitCode: articleSamePanachees_produitCanal_articleSamePanachees_produit_produitCode | null;
  produitTechReg: articleSamePanachees_produitCanal_articleSamePanachees_produit_produitTechReg | null;
}

export interface articleSamePanachees_produitCanal_articleSamePanachees {
  __typename: "ProduitCanal";
  id: string;
  prixPhv: number | null;
  qteStock: number | null;
  stv: number | null;
  operations: (articleSamePanachees_produitCanal_articleSamePanachees_operations | null)[] | null;
  remises: (articleSamePanachees_produitCanal_articleSamePanachees_remises | null)[] | null;
  produit: articleSamePanachees_produitCanal_articleSamePanachees_produit | null;
}

export interface articleSamePanachees_produitCanal {
  __typename: "ProduitCanal";
  id: string;
  articleSamePanachees: (articleSamePanachees_produitCanal_articleSamePanachees | null)[] | null;
}

export interface articleSamePanachees {
  produitCanal: articleSamePanachees_produitCanal | null;
}

export interface articleSamePanacheesVariables {
  id: string;
  idPharmacie?: string | null;
  idRemiseOperation?: string | null;
}
