/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PANIER_PRICE_BY_CANAL
// ====================================================

export interface PANIER_PRICE_BY_CANAL_panierPriceByCanal {
  __typename: "PanierPriceByCanal";
  nbrRef: number | null;
  prixBaseTotalHT: number | null;
  valeurRemiseTotal: number | null;
  prixNetTotalHT: number | null;
  remiseGlobale: number | null;
  quantite: number | null;
  uniteGratuite: number | null;
}

export interface PANIER_PRICE_BY_CANAL {
  panierPriceByCanal: PANIER_PRICE_BY_CANAL_panierPriceByCanal | null;
}

export interface PANIER_PRICE_BY_CANALVariables {
  idPanier: string;
  codeCanalCommande: string;
}
