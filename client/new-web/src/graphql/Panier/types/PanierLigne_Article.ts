/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: PanierLigne_Article
// ====================================================

export interface PanierLigne_Article_produitCanal_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PanierLigne_Article_produitCanal_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: PanierLigne_Article_produitCanal_produit_produitPhoto_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PanierLigne_Article_produitCanal_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface PanierLigne_Article_produitCanal_produit_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface PanierLigne_Article_produitCanal_produit_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  laboExploitant: PanierLigne_Article_produitCanal_produit_produitTechReg_laboExploitant | null;
}

export interface PanierLigne_Article_produitCanal_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  libelle2: string | null;
  produitPhoto: PanierLigne_Article_produitCanal_produit_produitPhoto | null;
  produitCode: PanierLigne_Article_produitCanal_produit_produitCode | null;
  produitTechReg: PanierLigne_Article_produitCanal_produit_produitTechReg | null;
}

export interface PanierLigne_Article_produitCanal_inOtherPanier {
  __typename: "PanierLigne";
  id: string;
}

export interface PanierLigne_Article_produitCanal_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface PanierLigne_Article_produitCanal_comments_data_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface PanierLigne_Article_produitCanal_comments_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PanierLigne_Article_produitCanal_comments_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface PanierLigne_Article_produitCanal_comments_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: PanierLigne_Article_produitCanal_comments_data_user_userPhoto_fichier | null;
}

export interface PanierLigne_Article_produitCanal_comments_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface PanierLigne_Article_produitCanal_comments_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: PanierLigne_Article_produitCanal_comments_data_user_role | null;
  userPhoto: PanierLigne_Article_produitCanal_comments_data_user_userPhoto | null;
  pharmacie: PanierLigne_Article_produitCanal_comments_data_user_pharmacie | null;
}

export interface PanierLigne_Article_produitCanal_comments_data {
  __typename: "Comment";
  id: string;
  content: string;
  idItemAssocie: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichiers: PanierLigne_Article_produitCanal_comments_data_fichiers[] | null;
  user: PanierLigne_Article_produitCanal_comments_data_user;
}

export interface PanierLigne_Article_produitCanal_comments {
  __typename: "CommentResult";
  total: number;
  data: PanierLigne_Article_produitCanal_comments_data[];
}

export interface PanierLigne_Article_produitCanal_sousGammeCommercial {
  __typename: "CanalSousGamme";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
  estDefaut: number | null;
  countCanalArticles: number | null;
}

export interface PanierLigne_Article_produitCanal_articleSamePanachees_operations {
  __typename: "Operation";
  id: string;
}

export interface PanierLigne_Article_produitCanal_articleSamePanachees_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface PanierLigne_Article_produitCanal_articleSamePanachees_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface PanierLigne_Article_produitCanal_articleSamePanachees_produit_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface PanierLigne_Article_produitCanal_articleSamePanachees_produit_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  laboExploitant: PanierLigne_Article_produitCanal_articleSamePanachees_produit_produitTechReg_laboExploitant | null;
}

export interface PanierLigne_Article_produitCanal_articleSamePanachees_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  produitCode: PanierLigne_Article_produitCanal_articleSamePanachees_produit_produitCode | null;
  produitTechReg: PanierLigne_Article_produitCanal_articleSamePanachees_produit_produitTechReg | null;
}

export interface PanierLigne_Article_produitCanal_articleSamePanachees {
  __typename: "ProduitCanal";
  id: string;
  prixPhv: number | null;
  qteStock: number | null;
  stv: number | null;
  operations: (PanierLigne_Article_produitCanal_articleSamePanachees_operations | null)[] | null;
  remises: (PanierLigne_Article_produitCanal_articleSamePanachees_remises | null)[] | null;
  produit: PanierLigne_Article_produitCanal_articleSamePanachees_produit | null;
}

export interface PanierLigne_Article_produitCanal_commandeCanal {
  __typename: "CommandeCanal";
  type: string;
  id: string;
  libelle: string | null;
  code: string | null;
  countOperationsCommercials: number | null;
  countCanalArticles: number | null;
}

export interface PanierLigne_Article_produitCanal {
  __typename: "ProduitCanal";
  type: string;
  id: string;
  produit: PanierLigne_Article_produitCanal_produit | null;
  qteStock: number | null;
  qteMin: number | null;
  stv: number | null;
  unitePetitCond: number | null;
  uniteSupCond: number | null;
  prixFab: number | null;
  prixPhv: number | null;
  prixTtc: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  dateRetrait: any | null;
  isActive: boolean | null;
  qteTotalRemisePanachees: number | null;
  inMyPanier: boolean | null;
  inOtherPanier: PanierLigne_Article_produitCanal_inOtherPanier | null;
  remises: (PanierLigne_Article_produitCanal_remises | null)[] | null;
  comments: PanierLigne_Article_produitCanal_comments | null;
  sousGammeCommercial: PanierLigne_Article_produitCanal_sousGammeCommercial | null;
  articleSamePanachees: (PanierLigne_Article_produitCanal_articleSamePanachees | null)[] | null;
  commandeCanal: PanierLigne_Article_produitCanal_commandeCanal | null;
}

export interface PanierLigne_Article {
  __typename: "PanierLigne";
  produitCanal: PanierLigne_Article_produitCanal | null;
  validate: boolean | null;
}
