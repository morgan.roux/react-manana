import gql from 'graphql-tag';

export const DO_CREATE_PARTENAIRE = gql`
  mutation CREATE_PARTENAIRE(
    $idGroupement: ID!
    $nom: String!
    $adresse1: String
    $adresse2: String
    $cp: String
    $ville: String
    $pays: String
    $telBureau: String
    $telMobile: String
    $mail: String
    $site: String
    $commentaire: String
  ) {
    createPartenaire(
      idGroupement: $idGroupement
      nom: $nom
      adresse1: $adresse1
      adresse2: $adresse2
      cp: $cp
      ville: $ville
      pays: $pays
      telBureau: $telBureau
      telMobile: $telMobile
      mail: $mail
      site: $site
      commentaire: $commentaire
    ) {
      type
      id
      nom
      commentaire
      idGroupement
      user {
        id
        email
        login
        status
      }
      role {
        code
        nom
      }
    }
  }
`;
