/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_PARTENAIRE
// ====================================================

export interface CREATE_PARTENAIRE_createPartenaire_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
}

export interface CREATE_PARTENAIRE_createPartenaire_role {
  __typename: "Role";
  code: string | null;
  nom: string | null;
}

export interface CREATE_PARTENAIRE_createPartenaire {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
  commentaire: string | null;
  idGroupement: string | null;
  user: CREATE_PARTENAIRE_createPartenaire_user | null;
  role: CREATE_PARTENAIRE_createPartenaire_role | null;
}

export interface CREATE_PARTENAIRE {
  createPartenaire: CREATE_PARTENAIRE_createPartenaire | null;
}

export interface CREATE_PARTENAIREVariables {
  idGroupement: string;
  nom: string;
  adresse1?: string | null;
  adresse2?: string | null;
  cp?: string | null;
  ville?: string | null;
  pays?: string | null;
  telBureau?: string | null;
  telMobile?: string | null;
  mail?: string | null;
  site?: string | null;
  commentaire?: string | null;
}
