import gql from 'graphql-tag';
import { FICHIER_FRAGMENT } from '../Fichier/fragment';
import { ITEM_INFO_FRAGEMENT } from '../Item/fragment';
import { OPERATION_ARTICLE_CIBLE_INFO_FRAGEMENT } from '../OperationArticleCible/fragment';
import { OPERATION_PHARMACIE_INFO_FRAGEMENT } from '../OperationPharmacie/fragment';
import { SMYLEY_INFO_FRAGEMENT, USER_SMYLEY_INFO_FRAGMENT } from '../Smyley/fragment';
import { STATUT_TYPE_PAYLOAD_FRAGMENT } from '../StatutTypePayload/fragment';
import { USER_INFO_FRAGEMENT } from '../User/fragment';
import { OPERATION_ARTICLE_INFO_FRAGEMENT } from './../OperationArticle/fragment';

export const OPERATION_COMMERCIALE_PRESENTATION_FRAGEMENT = gql`
  fragment OperationCommercialePresentation on Operation {
    id
    accordCommercial
    commandePassee
    isRemoved
    seen(idPharmacie: $idPharmacieUser, userId: $userId)
    pharmacieVisitors {
      id
    }
    laboratoire {
      id
      nomLabo
    }
    idGroupement
    pharmacieCible {
      id
    }
    fichierPresentations {
      ...FichierInfo
    }
    libelle
    niveauPriorite
    description
    dateDebut
    dateModification
    dateFin
    dateCreation
    dateModification
    nombrePharmaciesConsultes
    nombrePharmaciesCommandees
    nombreProduitsCommandes
    nombreTotalTypeProduitsCommandes
    CAMoyenParPharmacie
    CATotal
    nombreProduitsMoyenParPharmacie
    qteProduitsPharmacieCommandePassee
    qteTotalProduitsCommandePassee
    nombreCommandePassee
    totalCommandePassee
    actions {
      total
    }
    operationArticles {
      total
    }
    operationPharmacie {
      ...OperationPharmacieInfo
    }
    commandeType {
      ...StatutTypePayloadInfo
    }
    commandeCanal {
      ...StatutTypePayloadInfo
    }
    userSmyleys {
      total
      data {
        ...UserSmyleyInfo
      }
    }
    comments {
      total
    }
  }
  ${USER_SMYLEY_INFO_FRAGMENT}
  ${OPERATION_PHARMACIE_INFO_FRAGEMENT}
  ${STATUT_TYPE_PAYLOAD_FRAGMENT}
  ${FICHIER_FRAGMENT}
`;

export const OPERATION_COMMERCIALE_INFO_FRAGEMENT = gql`
  fragment OperationCommercialeInfo on Operation {
    id
    accordCommercial
    commandePassee
    isRemoved
    seen(idPharmacie: $idPharmacieUser, userId: $userId)
    pharmacieVisitors {
      id
    }
    laboratoire {
      id
      nomLabo
    }
    idGroupement
    pharmacieCible {
      id
    }
    fichierPresentations {
      ...FichierInfo
    }
    libelle
    niveauPriorite
    description
    dateDebut
    dateModification
    dateFin
    dateCreation
    dateModification
    nombrePharmaciesConsultes
    nombrePharmaciesCommandees
    nombreProduitsCommandes
    nombreTotalTypeProduitsCommandes
    CAMoyenParPharmacie
    CATotal
    nombreProduitsMoyenParPharmacie
    qteProduitsPharmacieCommandePassee
    qteTotalProduitsCommandePassee
    nombreCommandePassee
    totalCommandePassee
    caMoyenPharmacie
    nbPharmacieCommande
    nbMoyenLigne
    item {
      ...ItemInfo
    }
    actions {
      total
      data {
        id
        description
        dateDebut
        dateFin
        priority
        project {
          id
          name
        }
      }
    }
    operationPharmacie {
      ...OperationPharmacieInfo
    }
    operationArticles {
      ...OperationArticleInfo
    }
    operationArticleCible {
      ...OperationArticleCibleInfo
    }
    commandeType {
      ...StatutTypePayloadInfo
    }
    commandeCanal {
      ...StatutTypePayloadInfo
    }
    userSmyleys {
      total
      data {
        ...UserSmyleyInfo
      }
    }
    comments {
      total
    }
    marche {
      id
      nom
      remise(idPharmacie: $idPharmacie) {
        id
      }
    }
    promotion {
      id
      nom
      remise(idPharmacie: $idPharmacie) {
        id
      }
    }
  }
  ${ITEM_INFO_FRAGEMENT}
  ${USER_SMYLEY_INFO_FRAGMENT}
  ${OPERATION_ARTICLE_INFO_FRAGEMENT}
  ${OPERATION_ARTICLE_CIBLE_INFO_FRAGEMENT}
  ${OPERATION_PHARMACIE_INFO_FRAGEMENT}
  ${STATUT_TYPE_PAYLOAD_FRAGMENT}
  ${FICHIER_FRAGMENT}
`;

export const OPERATION_COMMERCIALE_RESULT_INFO_FRAGEMENT = gql`
  fragment OperationCommercialeResultInfo on OperationResult {
    total
    data {
      ...OperationCommercialeInfo
    }
  }
  ${OPERATION_COMMERCIALE_INFO_FRAGEMENT}
`;

export const SEARCH_OPERATION_INFO = gql`
  fragment SearchOperationInfo on Operation {
    type
    id
    commandePassee
    isRemoved
    seen(idPharmacie: $idPharmacieUser, userId: $userId)
    caMoyenPharmacie
    nbPharmacieCommande
    nbMoyenLigne
    commandeType {
      id
      code
    }
    fichierPresentations {
      id
      chemin
      nomOriginal
      publicUrl
    }
    commandeCanal {
      id
      libelle
    }
    laboratoire {
      id
      nomLabo
    }
    pharmacieVisitors {
      id
    }
    idGroupement
    pharmacieCible {
      id
    }
    operationPharmacie {
      id
      globalite
      pharmaciedetails {
        id
        pharmacie {
          id
        }
      }
    }
    operationArticles {
      total
    }
    libelle
    niveauPriorite
    description
    dateDebut
    dateModification
    dateFin
    dateCreation
    dateModification
    nombrePharmaciesConsultes
    nombrePharmaciesCommandees
    nombreProduitsCommandes
    nombreTotalTypeProduitsCommandes
    nombreProduitsMoyenParPharmacie
    qteProduitsPharmacieCommandePassee
    qteTotalProduitsCommandePassee
    nombreCommandePassee
    totalCommandePassee
    CATotal
    CAMoyenParPharmacie
    userSmyleys {
      total
      data {
        id
        item {
          id
        }
        idSource
        user {
          ...UserInfo
        }
        smyley {
          ...SmyleyInfo
        }
        isRemoved
        dateCreation
        dateModification
      }
    }
    comments {
      total
    }
    item {
      id
      code
      codeItem
      name
    }
    marche {
      id
      nom
      remise(idPharmacie: $idPharmacie) {
        id
      }
    }
    promotion {
      id
      nom
      remise(idPharmacie: $idPharmacie) {
        id
      }
    }
  }
  ${USER_INFO_FRAGEMENT}
  ${SMYLEY_INFO_FRAGEMENT}
`;
