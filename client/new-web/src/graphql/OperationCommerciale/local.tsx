import gql from 'graphql-tag';
export const GET_OPERATION_PANIER = gql`
  {
    operationPanier @client {
      panierLignes {
        id
        quantite
        produitCanal {
          id
          prixPhv
          remises {
            id
            nombreUg
            pourcentageRemise
            quantiteMax
            quantiteMin
          }
        }
      }
    }
  }
`;

export const GET_CHECKEDS_OPERATION = gql`
  {
    checkedsOperation @client {
      id
      description
      dateDebut
      dateFin
      libelle
      commandeType {
        id
        libelle
      }
      commandeCanal {
        id
        libelle
      }
      laboratoire {
        id
        nomLabo
      }
      CAMoyenParPharmacie
      CATotal
    }
  }
`;
