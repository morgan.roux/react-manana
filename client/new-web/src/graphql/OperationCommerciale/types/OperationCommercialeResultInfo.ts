/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: OperationCommercialeResultInfo
// ====================================================

export interface OperationCommercialeResultInfo_data_pharmacieVisitors {
  __typename: "Pharmacie";
  id: string;
}

export interface OperationCommercialeResultInfo_data_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface OperationCommercialeResultInfo_data_pharmacieCible {
  __typename: "Pharmacie";
  id: string;
}

export interface OperationCommercialeResultInfo_data_fichierPresentations {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OperationCommercialeResultInfo_data_item_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OperationCommercialeResultInfo_data_item_parent_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OperationCommercialeResultInfo_data_item_parent {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: OperationCommercialeResultInfo_data_item_parent_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OperationCommercialeResultInfo_data_item {
  __typename: "Item";
  type: string;
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: OperationCommercialeResultInfo_data_item_fichier | null;
  parent: OperationCommercialeResultInfo_data_item_parent | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OperationCommercialeResultInfo_data_actions_data_project {
  __typename: "Project";
  id: string;
  name: string | null;
}

export interface OperationCommercialeResultInfo_data_actions_data {
  __typename: "Action";
  id: string;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  project: OperationCommercialeResultInfo_data_actions_data_project | null;
}

export interface OperationCommercialeResultInfo_data_actions {
  __typename: "ActionResult";
  total: number;
  data: OperationCommercialeResultInfo_data_actions_data[];
}

export interface OperationCommercialeResultInfo_data_operationPharmacie_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OperationCommercialeResultInfo_data_operationPharmacie_pharmaciedetails_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface OperationCommercialeResultInfo_data_operationPharmacie_pharmaciedetails {
  __typename: "OperationPharmacieDetail";
  id: string;
  pharmacie: OperationCommercialeResultInfo_data_operationPharmacie_pharmaciedetails_pharmacie | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OperationCommercialeResultInfo_data_operationPharmacie {
  __typename: "OperationPharmacie";
  id: string;
  globalite: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
  fichierCible: OperationCommercialeResultInfo_data_operationPharmacie_fichierCible | null;
  pharmaciedetails: (OperationCommercialeResultInfo_data_operationPharmacie_pharmaciedetails | null)[] | null;
}

export interface OperationCommercialeResultInfo_data_operationArticles_data_produitCanal_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
}

export interface OperationCommercialeResultInfo_data_operationArticles_data_produitCanal_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  nomOriginal: string;
}

export interface OperationCommercialeResultInfo_data_operationArticles_data_produitCanal_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: OperationCommercialeResultInfo_data_operationArticles_data_produitCanal_produit_produitPhoto_fichier | null;
}

export interface OperationCommercialeResultInfo_data_operationArticles_data_produitCanal_produit {
  __typename: "Produit";
  id: string;
  produitPhoto: OperationCommercialeResultInfo_data_operationArticles_data_produitCanal_produit_produitPhoto | null;
}

export interface OperationCommercialeResultInfo_data_operationArticles_data_produitCanal_articleSamePanachees {
  __typename: "ProduitCanal";
  id: string;
}

export interface OperationCommercialeResultInfo_data_operationArticles_data_produitCanal_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface OperationCommercialeResultInfo_data_operationArticles_data_produitCanal {
  __typename: "ProduitCanal";
  id: string;
  qteStock: number | null;
  stv: number | null;
  prixPhv: number | null;
  commandeCanal: OperationCommercialeResultInfo_data_operationArticles_data_produitCanal_commandeCanal | null;
  produit: OperationCommercialeResultInfo_data_operationArticles_data_produitCanal_produit | null;
  articleSamePanachees: (OperationCommercialeResultInfo_data_operationArticles_data_produitCanal_articleSamePanachees | null)[] | null;
  remises: (OperationCommercialeResultInfo_data_operationArticles_data_produitCanal_remises | null)[] | null;
}

export interface OperationCommercialeResultInfo_data_operationArticles_data {
  __typename: "OperationArticle";
  id: string;
  produitCanal: OperationCommercialeResultInfo_data_operationArticles_data_produitCanal | null;
  quantite: number | null;
}

export interface OperationCommercialeResultInfo_data_operationArticles {
  __typename: "OperationArticleResult";
  total: number;
  data: (OperationCommercialeResultInfo_data_operationArticles_data | null)[] | null;
}

export interface OperationCommercialeResultInfo_data_operationArticleCible_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OperationCommercialeResultInfo_data_operationArticleCible {
  __typename: "OperationArticleCible";
  id: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichierCible: OperationCommercialeResultInfo_data_operationArticleCible_fichierCible | null;
}

export interface OperationCommercialeResultInfo_data_commandeType {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OperationCommercialeResultInfo_data_commandeCanal {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OperationCommercialeResultInfo_data_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface OperationCommercialeResultInfo_data_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface OperationCommercialeResultInfo_data_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface OperationCommercialeResultInfo_data_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: OperationCommercialeResultInfo_data_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface OperationCommercialeResultInfo_data_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface OperationCommercialeResultInfo_data_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: OperationCommercialeResultInfo_data_userSmyleys_data_user_role | null;
  userPhoto: OperationCommercialeResultInfo_data_userSmyleys_data_user_userPhoto | null;
  pharmacie: OperationCommercialeResultInfo_data_userSmyleys_data_user_pharmacie | null;
}

export interface OperationCommercialeResultInfo_data_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface OperationCommercialeResultInfo_data_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: OperationCommercialeResultInfo_data_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface OperationCommercialeResultInfo_data_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: OperationCommercialeResultInfo_data_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface OperationCommercialeResultInfo_data_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface OperationCommercialeResultInfo_data_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: OperationCommercialeResultInfo_data_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface OperationCommercialeResultInfo_data_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: OperationCommercialeResultInfo_data_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: OperationCommercialeResultInfo_data_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface OperationCommercialeResultInfo_data_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: OperationCommercialeResultInfo_data_userSmyleys_data_smyley_groupement | null;
}

export interface OperationCommercialeResultInfo_data_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: OperationCommercialeResultInfo_data_userSmyleys_data_item | null;
  idSource: string | null;
  user: OperationCommercialeResultInfo_data_userSmyleys_data_user | null;
  smyley: OperationCommercialeResultInfo_data_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OperationCommercialeResultInfo_data_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (OperationCommercialeResultInfo_data_userSmyleys_data | null)[] | null;
}

export interface OperationCommercialeResultInfo_data_comments {
  __typename: "CommentResult";
  total: number;
}

export interface OperationCommercialeResultInfo_data_marche_remise {
  __typename: "Remise";
  id: string;
}

export interface OperationCommercialeResultInfo_data_marche {
  __typename: "Marche";
  id: string;
  nom: string | null;
  remise: OperationCommercialeResultInfo_data_marche_remise | null;
}

export interface OperationCommercialeResultInfo_data_promotion_remise {
  __typename: "Remise";
  id: string;
}

export interface OperationCommercialeResultInfo_data_promotion {
  __typename: "Promotion";
  id: string;
  nom: string | null;
  remise: OperationCommercialeResultInfo_data_promotion_remise | null;
}

export interface OperationCommercialeResultInfo_data {
  __typename: "Operation";
  id: string;
  accordCommercial: boolean | null;
  commandePassee: boolean | null;
  isRemoved: boolean | null;
  seen: boolean | null;
  pharmacieVisitors: (OperationCommercialeResultInfo_data_pharmacieVisitors | null)[] | null;
  laboratoire: OperationCommercialeResultInfo_data_laboratoire | null;
  idGroupement: string | null;
  pharmacieCible: (OperationCommercialeResultInfo_data_pharmacieCible | null)[] | null;
  fichierPresentations: (OperationCommercialeResultInfo_data_fichierPresentations | null)[] | null;
  libelle: string | null;
  niveauPriorite: number | null;
  description: string | null;
  dateDebut: any | null;
  dateModification: any | null;
  dateFin: any | null;
  dateCreation: any | null;
  nombrePharmaciesConsultes: number | null;
  nombrePharmaciesCommandees: number | null;
  nombreProduitsCommandes: number | null;
  nombreTotalTypeProduitsCommandes: number | null;
  CAMoyenParPharmacie: number | null;
  CATotal: number | null;
  nombreProduitsMoyenParPharmacie: number | null;
  qteProduitsPharmacieCommandePassee: number | null;
  qteTotalProduitsCommandePassee: number | null;
  nombreCommandePassee: number | null;
  totalCommandePassee: number | null;
  caMoyenPharmacie: number | null;
  nbPharmacieCommande: number | null;
  nbMoyenLigne: number | null;
  item: OperationCommercialeResultInfo_data_item | null;
  actions: OperationCommercialeResultInfo_data_actions | null;
  operationPharmacie: OperationCommercialeResultInfo_data_operationPharmacie | null;
  operationArticles: OperationCommercialeResultInfo_data_operationArticles | null;
  operationArticleCible: OperationCommercialeResultInfo_data_operationArticleCible | null;
  commandeType: OperationCommercialeResultInfo_data_commandeType | null;
  commandeCanal: OperationCommercialeResultInfo_data_commandeCanal | null;
  userSmyleys: OperationCommercialeResultInfo_data_userSmyleys | null;
  comments: OperationCommercialeResultInfo_data_comments | null;
  marche: OperationCommercialeResultInfo_data_marche | null;
  promotion: OperationCommercialeResultInfo_data_promotion | null;
}

export interface OperationCommercialeResultInfo {
  __typename: "OperationResult";
  total: number;
  data: (OperationCommercialeResultInfo_data | null)[] | null;
}
