/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: deleteOperation
// ====================================================

export interface deleteOperation_deleteOperation {
  __typename: "Operation";
  id: string;
}

export interface deleteOperation {
  deleteOperation: deleteOperation_deleteOperation | null;
}

export interface deleteOperationVariables {
  id: string;
}
