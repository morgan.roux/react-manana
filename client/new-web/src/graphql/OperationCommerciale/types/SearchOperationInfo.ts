/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: SearchOperationInfo
// ====================================================

export interface SearchOperationInfo_commandeType {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
}

export interface SearchOperationInfo_fichierPresentations {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface SearchOperationInfo_commandeCanal {
  __typename: "StatutTypePayload";
  id: string;
  libelle: string | null;
}

export interface SearchOperationInfo_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface SearchOperationInfo_pharmacieVisitors {
  __typename: "Pharmacie";
  id: string;
}

export interface SearchOperationInfo_pharmacieCible {
  __typename: "Pharmacie";
  id: string;
}

export interface SearchOperationInfo_operationPharmacie_pharmaciedetails_pharmacie {
  __typename: "Pharmacie";
  id: string;
}

export interface SearchOperationInfo_operationPharmacie_pharmaciedetails {
  __typename: "OperationPharmacieDetail";
  id: string;
  pharmacie: SearchOperationInfo_operationPharmacie_pharmaciedetails_pharmacie | null;
}

export interface SearchOperationInfo_operationPharmacie {
  __typename: "OperationPharmacie";
  id: string;
  globalite: boolean | null;
  pharmaciedetails: (SearchOperationInfo_operationPharmacie_pharmaciedetails | null)[] | null;
}

export interface SearchOperationInfo_operationArticles {
  __typename: "OperationArticleResult";
  total: number;
}

export interface SearchOperationInfo_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface SearchOperationInfo_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SearchOperationInfo_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SearchOperationInfo_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SearchOperationInfo_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface SearchOperationInfo_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SearchOperationInfo_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SearchOperationInfo_userSmyleys_data_user_role | null;
  userPhoto: SearchOperationInfo_userSmyleys_data_user_userPhoto | null;
  pharmacie: SearchOperationInfo_userSmyleys_data_user_pharmacie | null;
}

export interface SearchOperationInfo_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface SearchOperationInfo_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: SearchOperationInfo_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface SearchOperationInfo_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: SearchOperationInfo_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface SearchOperationInfo_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface SearchOperationInfo_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: SearchOperationInfo_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface SearchOperationInfo_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: SearchOperationInfo_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: SearchOperationInfo_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface SearchOperationInfo_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: SearchOperationInfo_userSmyleys_data_smyley_groupement | null;
}

export interface SearchOperationInfo_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: SearchOperationInfo_userSmyleys_data_item | null;
  idSource: string | null;
  user: SearchOperationInfo_userSmyleys_data_user | null;
  smyley: SearchOperationInfo_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SearchOperationInfo_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (SearchOperationInfo_userSmyleys_data | null)[] | null;
}

export interface SearchOperationInfo_comments {
  __typename: "CommentResult";
  total: number;
}

export interface SearchOperationInfo_item {
  __typename: "Item";
  id: string;
  code: string | null;
  codeItem: string | null;
  name: string | null;
}

export interface SearchOperationInfo_marche_remise {
  __typename: "Remise";
  id: string;
}

export interface SearchOperationInfo_marche {
  __typename: "Marche";
  id: string;
  nom: string | null;
  remise: SearchOperationInfo_marche_remise | null;
}

export interface SearchOperationInfo_promotion_remise {
  __typename: "Remise";
  id: string;
}

export interface SearchOperationInfo_promotion {
  __typename: "Promotion";
  id: string;
  nom: string | null;
  remise: SearchOperationInfo_promotion_remise | null;
}

export interface SearchOperationInfo {
  __typename: "Operation";
  type: string;
  id: string;
  commandePassee: boolean | null;
  isRemoved: boolean | null;
  seen: boolean | null;
  caMoyenPharmacie: number | null;
  nbPharmacieCommande: number | null;
  nbMoyenLigne: number | null;
  commandeType: SearchOperationInfo_commandeType | null;
  fichierPresentations: (SearchOperationInfo_fichierPresentations | null)[] | null;
  commandeCanal: SearchOperationInfo_commandeCanal | null;
  laboratoire: SearchOperationInfo_laboratoire | null;
  pharmacieVisitors: (SearchOperationInfo_pharmacieVisitors | null)[] | null;
  idGroupement: string | null;
  pharmacieCible: (SearchOperationInfo_pharmacieCible | null)[] | null;
  operationPharmacie: SearchOperationInfo_operationPharmacie | null;
  operationArticles: SearchOperationInfo_operationArticles | null;
  libelle: string | null;
  niveauPriorite: number | null;
  description: string | null;
  dateDebut: any | null;
  dateModification: any | null;
  dateFin: any | null;
  dateCreation: any | null;
  nombrePharmaciesConsultes: number | null;
  nombrePharmaciesCommandees: number | null;
  nombreProduitsCommandes: number | null;
  nombreTotalTypeProduitsCommandes: number | null;
  nombreProduitsMoyenParPharmacie: number | null;
  qteProduitsPharmacieCommandePassee: number | null;
  qteTotalProduitsCommandePassee: number | null;
  nombreCommandePassee: number | null;
  totalCommandePassee: number | null;
  CATotal: number | null;
  CAMoyenParPharmacie: number | null;
  userSmyleys: SearchOperationInfo_userSmyleys | null;
  comments: SearchOperationInfo_comments | null;
  item: SearchOperationInfo_item | null;
  marche: SearchOperationInfo_marche | null;
  promotion: SearchOperationInfo_promotion | null;
}
