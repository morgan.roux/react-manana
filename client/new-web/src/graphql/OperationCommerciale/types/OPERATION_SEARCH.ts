/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: OPERATION_SEARCH
// ====================================================

export interface OPERATION_SEARCH_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface OPERATION_SEARCH_search_data_Operation_pharmacieVisitors {
  __typename: "Pharmacie";
  id: string;
}

export interface OPERATION_SEARCH_search_data_Operation_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface OPERATION_SEARCH_search_data_Operation_pharmacieCible {
  __typename: "Pharmacie";
  id: string;
}

export interface OPERATION_SEARCH_search_data_Operation_fichierPresentations {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OPERATION_SEARCH_search_data_Operation_item_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OPERATION_SEARCH_search_data_Operation_item_parent_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OPERATION_SEARCH_search_data_Operation_item_parent {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: OPERATION_SEARCH_search_data_Operation_item_parent_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OPERATION_SEARCH_search_data_Operation_item {
  __typename: "Item";
  type: string;
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: OPERATION_SEARCH_search_data_Operation_item_fichier | null;
  parent: OPERATION_SEARCH_search_data_Operation_item_parent | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OPERATION_SEARCH_search_data_Operation_actions_data_project {
  __typename: "Project";
  id: string;
  name: string | null;
}

export interface OPERATION_SEARCH_search_data_Operation_actions_data {
  __typename: "Action";
  id: string;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  project: OPERATION_SEARCH_search_data_Operation_actions_data_project | null;
}

export interface OPERATION_SEARCH_search_data_Operation_actions {
  __typename: "ActionResult";
  total: number;
  data: OPERATION_SEARCH_search_data_Operation_actions_data[];
}

export interface OPERATION_SEARCH_search_data_Operation_operationPharmacie_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OPERATION_SEARCH_search_data_Operation_operationPharmacie_pharmaciedetails_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface OPERATION_SEARCH_search_data_Operation_operationPharmacie_pharmaciedetails {
  __typename: "OperationPharmacieDetail";
  id: string;
  pharmacie: OPERATION_SEARCH_search_data_Operation_operationPharmacie_pharmaciedetails_pharmacie | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OPERATION_SEARCH_search_data_Operation_operationPharmacie {
  __typename: "OperationPharmacie";
  id: string;
  globalite: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
  fichierCible: OPERATION_SEARCH_search_data_Operation_operationPharmacie_fichierCible | null;
  pharmaciedetails: (OPERATION_SEARCH_search_data_Operation_operationPharmacie_pharmaciedetails | null)[] | null;
}

export interface OPERATION_SEARCH_search_data_Operation_operationArticles_data_produitCanal_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
}

export interface OPERATION_SEARCH_search_data_Operation_operationArticles_data_produitCanal_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  nomOriginal: string;
}

export interface OPERATION_SEARCH_search_data_Operation_operationArticles_data_produitCanal_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: OPERATION_SEARCH_search_data_Operation_operationArticles_data_produitCanal_produit_produitPhoto_fichier | null;
}

export interface OPERATION_SEARCH_search_data_Operation_operationArticles_data_produitCanal_produit {
  __typename: "Produit";
  id: string;
  produitPhoto: OPERATION_SEARCH_search_data_Operation_operationArticles_data_produitCanal_produit_produitPhoto | null;
}

export interface OPERATION_SEARCH_search_data_Operation_operationArticles_data_produitCanal_articleSamePanachees {
  __typename: "ProduitCanal";
  id: string;
}

export interface OPERATION_SEARCH_search_data_Operation_operationArticles_data_produitCanal_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface OPERATION_SEARCH_search_data_Operation_operationArticles_data_produitCanal {
  __typename: "ProduitCanal";
  id: string;
  qteStock: number | null;
  stv: number | null;
  prixPhv: number | null;
  commandeCanal: OPERATION_SEARCH_search_data_Operation_operationArticles_data_produitCanal_commandeCanal | null;
  produit: OPERATION_SEARCH_search_data_Operation_operationArticles_data_produitCanal_produit | null;
  articleSamePanachees: (OPERATION_SEARCH_search_data_Operation_operationArticles_data_produitCanal_articleSamePanachees | null)[] | null;
  remises: (OPERATION_SEARCH_search_data_Operation_operationArticles_data_produitCanal_remises | null)[] | null;
}

export interface OPERATION_SEARCH_search_data_Operation_operationArticles_data {
  __typename: "OperationArticle";
  id: string;
  produitCanal: OPERATION_SEARCH_search_data_Operation_operationArticles_data_produitCanal | null;
  quantite: number | null;
}

export interface OPERATION_SEARCH_search_data_Operation_operationArticles {
  __typename: "OperationArticleResult";
  total: number;
  data: (OPERATION_SEARCH_search_data_Operation_operationArticles_data | null)[] | null;
}

export interface OPERATION_SEARCH_search_data_Operation_operationArticleCible_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OPERATION_SEARCH_search_data_Operation_operationArticleCible {
  __typename: "OperationArticleCible";
  id: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichierCible: OPERATION_SEARCH_search_data_Operation_operationArticleCible_fichierCible | null;
}

export interface OPERATION_SEARCH_search_data_Operation_commandeType {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OPERATION_SEARCH_search_data_Operation_commandeCanal {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OPERATION_SEARCH_search_data_Operation_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface OPERATION_SEARCH_search_data_Operation_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface OPERATION_SEARCH_search_data_Operation_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface OPERATION_SEARCH_search_data_Operation_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: OPERATION_SEARCH_search_data_Operation_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface OPERATION_SEARCH_search_data_Operation_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface OPERATION_SEARCH_search_data_Operation_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: OPERATION_SEARCH_search_data_Operation_userSmyleys_data_user_role | null;
  userPhoto: OPERATION_SEARCH_search_data_Operation_userSmyleys_data_user_userPhoto | null;
  pharmacie: OPERATION_SEARCH_search_data_Operation_userSmyleys_data_user_pharmacie | null;
}

export interface OPERATION_SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface OPERATION_SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: OPERATION_SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface OPERATION_SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: OPERATION_SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface OPERATION_SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface OPERATION_SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: OPERATION_SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface OPERATION_SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: OPERATION_SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: OPERATION_SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface OPERATION_SEARCH_search_data_Operation_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: OPERATION_SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement | null;
}

export interface OPERATION_SEARCH_search_data_Operation_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: OPERATION_SEARCH_search_data_Operation_userSmyleys_data_item | null;
  idSource: string | null;
  user: OPERATION_SEARCH_search_data_Operation_userSmyleys_data_user | null;
  smyley: OPERATION_SEARCH_search_data_Operation_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OPERATION_SEARCH_search_data_Operation_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (OPERATION_SEARCH_search_data_Operation_userSmyleys_data | null)[] | null;
}

export interface OPERATION_SEARCH_search_data_Operation_comments {
  __typename: "CommentResult";
  total: number;
}

export interface OPERATION_SEARCH_search_data_Operation_marche_remise {
  __typename: "Remise";
  id: string;
}

export interface OPERATION_SEARCH_search_data_Operation_marche {
  __typename: "Marche";
  id: string;
  nom: string | null;
  remise: OPERATION_SEARCH_search_data_Operation_marche_remise | null;
}

export interface OPERATION_SEARCH_search_data_Operation_promotion_remise {
  __typename: "Remise";
  id: string;
}

export interface OPERATION_SEARCH_search_data_Operation_promotion {
  __typename: "Promotion";
  id: string;
  nom: string | null;
  remise: OPERATION_SEARCH_search_data_Operation_promotion_remise | null;
}

export interface OPERATION_SEARCH_search_data_Operation {
  __typename: "Operation";
  id: string;
  accordCommercial: boolean | null;
  commandePassee: boolean | null;
  isRemoved: boolean | null;
  seen: boolean | null;
  pharmacieVisitors: (OPERATION_SEARCH_search_data_Operation_pharmacieVisitors | null)[] | null;
  laboratoire: OPERATION_SEARCH_search_data_Operation_laboratoire | null;
  idGroupement: string | null;
  pharmacieCible: (OPERATION_SEARCH_search_data_Operation_pharmacieCible | null)[] | null;
  fichierPresentations: (OPERATION_SEARCH_search_data_Operation_fichierPresentations | null)[] | null;
  libelle: string | null;
  niveauPriorite: number | null;
  description: string | null;
  dateDebut: any | null;
  dateModification: any | null;
  dateFin: any | null;
  dateCreation: any | null;
  nombrePharmaciesConsultes: number | null;
  nombrePharmaciesCommandees: number | null;
  nombreProduitsCommandes: number | null;
  nombreTotalTypeProduitsCommandes: number | null;
  CAMoyenParPharmacie: number | null;
  CATotal: number | null;
  nombreProduitsMoyenParPharmacie: number | null;
  qteProduitsPharmacieCommandePassee: number | null;
  qteTotalProduitsCommandePassee: number | null;
  nombreCommandePassee: number | null;
  totalCommandePassee: number | null;
  caMoyenPharmacie: number | null;
  nbPharmacieCommande: number | null;
  nbMoyenLigne: number | null;
  item: OPERATION_SEARCH_search_data_Operation_item | null;
  actions: OPERATION_SEARCH_search_data_Operation_actions | null;
  operationPharmacie: OPERATION_SEARCH_search_data_Operation_operationPharmacie | null;
  operationArticles: OPERATION_SEARCH_search_data_Operation_operationArticles | null;
  operationArticleCible: OPERATION_SEARCH_search_data_Operation_operationArticleCible | null;
  commandeType: OPERATION_SEARCH_search_data_Operation_commandeType | null;
  commandeCanal: OPERATION_SEARCH_search_data_Operation_commandeCanal | null;
  userSmyleys: OPERATION_SEARCH_search_data_Operation_userSmyleys | null;
  comments: OPERATION_SEARCH_search_data_Operation_comments | null;
  marche: OPERATION_SEARCH_search_data_Operation_marche | null;
  promotion: OPERATION_SEARCH_search_data_Operation_promotion | null;
}

export type OPERATION_SEARCH_search_data = OPERATION_SEARCH_search_data_Action | OPERATION_SEARCH_search_data_Operation;

export interface OPERATION_SEARCH_search {
  __typename: "SearchResult";
  total: number;
  data: (OPERATION_SEARCH_search_data | null)[] | null;
}

export interface OPERATION_SEARCH {
  search: OPERATION_SEARCH_search | null;
}

export interface OPERATION_SEARCHVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
  idPharmacie?: string | null;
  userId?: string | null;
  idPharmacieUser?: string | null;
  idRemiseOperation?: string | null;
}
