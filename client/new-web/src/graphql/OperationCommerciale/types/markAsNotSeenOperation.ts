/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: markAsNotSeenOperation
// ====================================================

export interface markAsNotSeenOperation_markAsNotSeenOperation {
  __typename: "Operation";
  id: string;
  seen: boolean | null;
  nombrePharmaciesConsultes: number | null;
}

export interface markAsNotSeenOperation {
  markAsNotSeenOperation: markAsNotSeenOperation_markAsNotSeenOperation | null;
}

export interface markAsNotSeenOperationVariables {
  id: string;
  idPharmacieUser?: string | null;
  userId?: string | null;
}
