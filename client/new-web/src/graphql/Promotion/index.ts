import { DO_CREATE_UPDATE_PROMOTION, DO_DELETE_PROMOTION } from './mutation';
import { GET_PROMOTION } from './query';

export { DO_CREATE_UPDATE_PROMOTION, DO_DELETE_PROMOTION, GET_PROMOTION };
