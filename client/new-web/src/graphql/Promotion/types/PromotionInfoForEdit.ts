/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PromotionType, PromotioStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: PromotionInfoForEdit
// ====================================================

export interface PromotionInfoForEdit_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface PromotionInfoForEdit_userCreated {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface PromotionInfoForEdit_canalArticles_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface PromotionInfoForEdit_canalArticles_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  libelle2: string | null;
  produitCode: PromotionInfoForEdit_canalArticles_produit_produitCode | null;
}

export interface PromotionInfoForEdit_canalArticles {
  __typename: "ProduitCanal";
  id: string;
  produit: PromotionInfoForEdit_canalArticles_produit | null;
}

export interface PromotionInfoForEdit_groupeClients_groupeClient {
  __typename: "GroupeClient";
  id: string;
  codeGroupe: number | null;
}

export interface PromotionInfoForEdit_groupeClients_remise_remiseDetails {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  remiseSupplementaire: number | null;
}

export interface PromotionInfoForEdit_groupeClients_remise {
  __typename: "Remise";
  id: string;
  remiseDetails: (PromotionInfoForEdit_groupeClients_remise_remiseDetails | null)[] | null;
}

export interface PromotionInfoForEdit_groupeClients {
  __typename: "PromotionGroupeClient";
  id: string;
  groupeClient: PromotionInfoForEdit_groupeClients_groupeClient | null;
  remise: PromotionInfoForEdit_groupeClients_remise | null;
}

export interface PromotionInfoForEdit {
  __typename: "Promotion";
  type: string;
  id: string;
  nom: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  promotionType: PromotionType | null;
  status: PromotioStatus | null;
  dateCreation: any | null;
  dateModification: any | null;
  commandeCanal: PromotionInfoForEdit_commandeCanal | null;
  userCreated: PromotionInfoForEdit_userCreated | null;
  canalArticles: (PromotionInfoForEdit_canalArticles | null)[] | null;
  groupeClients: (PromotionInfoForEdit_groupeClients | null)[] | null;
}
