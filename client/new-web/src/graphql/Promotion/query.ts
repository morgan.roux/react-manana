import gql from 'graphql-tag';
import { PROMOTION_INFO_FRAGMENT_FOR_EDIT, PROMOTION_INFO_FRAGMENT } from './fragment';

export const GET_PROMOTION = gql`
  query PROMOTION($id: ID!) {
    promotion(id: $id) {
      ...PromotionInfoForEdit
    }
  }
  ${PROMOTION_INFO_FRAGMENT_FOR_EDIT}
`;

export const GET_SEARCH_CUSTOM_CONTENT_PROMOTION = gql`
  query SEARCH_CUSTOM_CONTENT_PROMOTION($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Promotion {
          ...PromotionInfo
        }
      }
    }
  }
  ${PROMOTION_INFO_FRAGMENT}
`;

export const GET_PROMOTION_PRODUIT_CANAL = gql`
  query PROMOTION_PRODUIT_CANAL($id: ID!) {
    promotion(id: $id) {
      type
      id
      nom
      dateDebut
      dateFin
      promotionType
      status
      dateCreation
      dateModification
      commandeCanal {
        id
        code
        libelle
      }
      userCreated {
        id
        userName
      }
      canalArticles {
        id
        produit {
          id
          libelle
          libelle2
          produitCode {
            id
            code
            typeCode
            referent
          }
        }
      }
    }
  }
`;
