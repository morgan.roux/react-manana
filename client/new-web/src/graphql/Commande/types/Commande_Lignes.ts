/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: Commande_Lignes
// ====================================================

export interface Commande_Lignes_commandeLignes_produitCanal_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface Commande_Lignes_commandeLignes_produitCanal_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: Commande_Lignes_commandeLignes_produitCanal_produit_produitPhoto_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface Commande_Lignes_commandeLignes_produitCanal_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface Commande_Lignes_commandeLignes_produitCanal_produit_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface Commande_Lignes_commandeLignes_produitCanal_produit_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  laboExploitant: Commande_Lignes_commandeLignes_produitCanal_produit_produitTechReg_laboExploitant | null;
}

export interface Commande_Lignes_commandeLignes_produitCanal_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  libelle2: string | null;
  produitPhoto: Commande_Lignes_commandeLignes_produitCanal_produit_produitPhoto | null;
  produitCode: Commande_Lignes_commandeLignes_produitCanal_produit_produitCode | null;
  produitTechReg: Commande_Lignes_commandeLignes_produitCanal_produit_produitTechReg | null;
}

export interface Commande_Lignes_commandeLignes_produitCanal_inOtherPanier {
  __typename: "PanierLigne";
  id: string;
}

export interface Commande_Lignes_commandeLignes_produitCanal_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface Commande_Lignes_commandeLignes_produitCanal_comments_data_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface Commande_Lignes_commandeLignes_produitCanal_comments_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface Commande_Lignes_commandeLignes_produitCanal_comments_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface Commande_Lignes_commandeLignes_produitCanal_comments_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: Commande_Lignes_commandeLignes_produitCanal_comments_data_user_userPhoto_fichier | null;
}

export interface Commande_Lignes_commandeLignes_produitCanal_comments_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface Commande_Lignes_commandeLignes_produitCanal_comments_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: Commande_Lignes_commandeLignes_produitCanal_comments_data_user_role | null;
  userPhoto: Commande_Lignes_commandeLignes_produitCanal_comments_data_user_userPhoto | null;
  pharmacie: Commande_Lignes_commandeLignes_produitCanal_comments_data_user_pharmacie | null;
}

export interface Commande_Lignes_commandeLignes_produitCanal_comments_data {
  __typename: "Comment";
  id: string;
  content: string;
  idItemAssocie: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichiers: Commande_Lignes_commandeLignes_produitCanal_comments_data_fichiers[] | null;
  user: Commande_Lignes_commandeLignes_produitCanal_comments_data_user;
}

export interface Commande_Lignes_commandeLignes_produitCanal_comments {
  __typename: "CommentResult";
  total: number;
  data: Commande_Lignes_commandeLignes_produitCanal_comments_data[];
}

export interface Commande_Lignes_commandeLignes_produitCanal {
  __typename: "ProduitCanal";
  type: string;
  id: string;
  produit: Commande_Lignes_commandeLignes_produitCanal_produit | null;
  qteStock: number | null;
  qteMin: number | null;
  stv: number | null;
  unitePetitCond: number | null;
  uniteSupCond: number | null;
  prixFab: number | null;
  prixPhv: number | null;
  prixTtc: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  dateRetrait: any | null;
  isActive: boolean | null;
  qteTotalRemisePanachees: number | null;
  inMyPanier: boolean | null;
  inOtherPanier: Commande_Lignes_commandeLignes_produitCanal_inOtherPanier | null;
  remises: (Commande_Lignes_commandeLignes_produitCanal_remises | null)[] | null;
  comments: Commande_Lignes_commandeLignes_produitCanal_comments | null;
}

export interface Commande_Lignes_commandeLignes_status {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface Commande_Lignes_commandeLignes {
  __typename: "CommandeLigne";
  type: string;
  id: string;
  produitCanal: Commande_Lignes_commandeLignes_produitCanal | null;
  status: Commande_Lignes_commandeLignes_status | null;
  optimiser: number | null;
  quantiteCdee: number | null;
  quantiteLivree: number | null;
  uniteGratuite: number | null;
  prixBaseHT: number | null;
  remiseLigne: number | null;
  remiseGamme: number | null;
  prixNetUnitaireHT: number | null;
  prixTotalHT: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface Commande_Lignes {
  __typename: "Commande";
  id: string;
  commandeLignes: (Commande_Lignes_commandeLignes | null)[] | null;
}
