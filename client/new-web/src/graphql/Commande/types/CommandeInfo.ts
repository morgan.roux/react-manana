/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: CommandeInfo
// ====================================================

export interface CommandeInfo_pharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface CommandeInfo_pharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: CommandeInfo_pharmacie_departement_region | null;
}

export interface CommandeInfo_pharmacie {
  __typename: "Pharmacie";
  type: string;
  id: string;
  cip: string | null;
  nom: string | null;
  adresse1: string | null;
  ville: string | null;
  departement: CommandeInfo_pharmacie_departement | null;
}

export interface CommandeInfo_owner_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CommandeInfo_owner_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CommandeInfo_owner_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CommandeInfo_owner_userPhoto_fichier | null;
}

export interface CommandeInfo_owner_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CommandeInfo_owner {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CommandeInfo_owner_role | null;
  userPhoto: CommandeInfo_owner_userPhoto | null;
  pharmacie: CommandeInfo_owner_pharmacie | null;
}

export interface CommandeInfo_operation_pharmacieVisitors {
  __typename: "Pharmacie";
  id: string;
}

export interface CommandeInfo_operation_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface CommandeInfo_operation_pharmacieCible {
  __typename: "Pharmacie";
  id: string;
}

export interface CommandeInfo_operation_fichierPresentations {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeInfo_operation_item_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeInfo_operation_item_parent_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeInfo_operation_item_parent {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: CommandeInfo_operation_item_parent_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeInfo_operation_item {
  __typename: "Item";
  type: string;
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: CommandeInfo_operation_item_fichier | null;
  parent: CommandeInfo_operation_item_parent | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeInfo_operation_actions_data_project {
  __typename: "Project";
  id: string;
  name: string | null;
}

export interface CommandeInfo_operation_actions_data {
  __typename: "Action";
  id: string;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  project: CommandeInfo_operation_actions_data_project | null;
}

export interface CommandeInfo_operation_actions {
  __typename: "ActionResult";
  total: number;
  data: CommandeInfo_operation_actions_data[];
}

export interface CommandeInfo_operation_operationPharmacie_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeInfo_operation_operationPharmacie_pharmaciedetails_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CommandeInfo_operation_operationPharmacie_pharmaciedetails {
  __typename: "OperationPharmacieDetail";
  id: string;
  pharmacie: CommandeInfo_operation_operationPharmacie_pharmaciedetails_pharmacie | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeInfo_operation_operationPharmacie {
  __typename: "OperationPharmacie";
  id: string;
  globalite: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
  fichierCible: CommandeInfo_operation_operationPharmacie_fichierCible | null;
  pharmaciedetails: (CommandeInfo_operation_operationPharmacie_pharmaciedetails | null)[] | null;
}

export interface CommandeInfo_operation_operationArticles_data_produitCanal_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
}

export interface CommandeInfo_operation_operationArticles_data_produitCanal_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  nomOriginal: string;
}

export interface CommandeInfo_operation_operationArticles_data_produitCanal_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: CommandeInfo_operation_operationArticles_data_produitCanal_produit_produitPhoto_fichier | null;
}

export interface CommandeInfo_operation_operationArticles_data_produitCanal_produit {
  __typename: "Produit";
  id: string;
  produitPhoto: CommandeInfo_operation_operationArticles_data_produitCanal_produit_produitPhoto | null;
}

export interface CommandeInfo_operation_operationArticles_data_produitCanal_articleSamePanachees {
  __typename: "ProduitCanal";
  id: string;
}

export interface CommandeInfo_operation_operationArticles_data_produitCanal_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface CommandeInfo_operation_operationArticles_data_produitCanal {
  __typename: "ProduitCanal";
  id: string;
  qteStock: number | null;
  stv: number | null;
  prixPhv: number | null;
  commandeCanal: CommandeInfo_operation_operationArticles_data_produitCanal_commandeCanal | null;
  produit: CommandeInfo_operation_operationArticles_data_produitCanal_produit | null;
  articleSamePanachees: (CommandeInfo_operation_operationArticles_data_produitCanal_articleSamePanachees | null)[] | null;
  remises: (CommandeInfo_operation_operationArticles_data_produitCanal_remises | null)[] | null;
}

export interface CommandeInfo_operation_operationArticles_data {
  __typename: "OperationArticle";
  id: string;
  produitCanal: CommandeInfo_operation_operationArticles_data_produitCanal | null;
  quantite: number | null;
}

export interface CommandeInfo_operation_operationArticles {
  __typename: "OperationArticleResult";
  total: number;
  data: (CommandeInfo_operation_operationArticles_data | null)[] | null;
}

export interface CommandeInfo_operation_operationArticleCible_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeInfo_operation_operationArticleCible {
  __typename: "OperationArticleCible";
  id: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichierCible: CommandeInfo_operation_operationArticleCible_fichierCible | null;
}

export interface CommandeInfo_operation_commandeType {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeInfo_operation_commandeCanal {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeInfo_operation_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface CommandeInfo_operation_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CommandeInfo_operation_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CommandeInfo_operation_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CommandeInfo_operation_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface CommandeInfo_operation_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CommandeInfo_operation_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CommandeInfo_operation_userSmyleys_data_user_role | null;
  userPhoto: CommandeInfo_operation_userSmyleys_data_user_userPhoto | null;
  pharmacie: CommandeInfo_operation_userSmyleys_data_user_pharmacie | null;
}

export interface CommandeInfo_operation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface CommandeInfo_operation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: CommandeInfo_operation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface CommandeInfo_operation_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: CommandeInfo_operation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface CommandeInfo_operation_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface CommandeInfo_operation_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: CommandeInfo_operation_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface CommandeInfo_operation_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: CommandeInfo_operation_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: CommandeInfo_operation_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface CommandeInfo_operation_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: CommandeInfo_operation_userSmyleys_data_smyley_groupement | null;
}

export interface CommandeInfo_operation_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: CommandeInfo_operation_userSmyleys_data_item | null;
  idSource: string | null;
  user: CommandeInfo_operation_userSmyleys_data_user | null;
  smyley: CommandeInfo_operation_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeInfo_operation_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (CommandeInfo_operation_userSmyleys_data | null)[] | null;
}

export interface CommandeInfo_operation_comments {
  __typename: "CommentResult";
  total: number;
}

export interface CommandeInfo_operation_marche_remise {
  __typename: "Remise";
  id: string;
}

export interface CommandeInfo_operation_marche {
  __typename: "Marche";
  id: string;
  nom: string | null;
  remise: CommandeInfo_operation_marche_remise | null;
}

export interface CommandeInfo_operation_promotion_remise {
  __typename: "Remise";
  id: string;
}

export interface CommandeInfo_operation_promotion {
  __typename: "Promotion";
  id: string;
  nom: string | null;
  remise: CommandeInfo_operation_promotion_remise | null;
}

export interface CommandeInfo_operation {
  __typename: "Operation";
  id: string;
  accordCommercial: boolean | null;
  commandePassee: boolean | null;
  isRemoved: boolean | null;
  seen: boolean | null;
  pharmacieVisitors: (CommandeInfo_operation_pharmacieVisitors | null)[] | null;
  laboratoire: CommandeInfo_operation_laboratoire | null;
  idGroupement: string | null;
  pharmacieCible: (CommandeInfo_operation_pharmacieCible | null)[] | null;
  fichierPresentations: (CommandeInfo_operation_fichierPresentations | null)[] | null;
  libelle: string | null;
  niveauPriorite: number | null;
  description: string | null;
  dateDebut: any | null;
  dateModification: any | null;
  dateFin: any | null;
  dateCreation: any | null;
  nombrePharmaciesConsultes: number | null;
  nombrePharmaciesCommandees: number | null;
  nombreProduitsCommandes: number | null;
  nombreTotalTypeProduitsCommandes: number | null;
  CAMoyenParPharmacie: number | null;
  CATotal: number | null;
  nombreProduitsMoyenParPharmacie: number | null;
  qteProduitsPharmacieCommandePassee: number | null;
  qteTotalProduitsCommandePassee: number | null;
  nombreCommandePassee: number | null;
  totalCommandePassee: number | null;
  caMoyenPharmacie: number | null;
  nbPharmacieCommande: number | null;
  nbMoyenLigne: number | null;
  item: CommandeInfo_operation_item | null;
  actions: CommandeInfo_operation_actions | null;
  operationPharmacie: CommandeInfo_operation_operationPharmacie | null;
  operationArticles: CommandeInfo_operation_operationArticles | null;
  operationArticleCible: CommandeInfo_operation_operationArticleCible | null;
  commandeType: CommandeInfo_operation_commandeType | null;
  commandeCanal: CommandeInfo_operation_commandeCanal | null;
  userSmyleys: CommandeInfo_operation_userSmyleys | null;
  comments: CommandeInfo_operation_comments | null;
  marche: CommandeInfo_operation_marche | null;
  promotion: CommandeInfo_operation_promotion | null;
}

export interface CommandeInfo_commandeType {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeInfo_commandeStatut {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeInfo_commandeLignes_pharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface CommandeInfo_commandeLignes_pharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: CommandeInfo_commandeLignes_pharmacie_departement_region | null;
}

export interface CommandeInfo_commandeLignes_pharmacie {
  __typename: "Pharmacie";
  type: string;
  id: string;
  cip: string | null;
  nom: string | null;
  adresse1: string | null;
  ville: string | null;
  departement: CommandeInfo_commandeLignes_pharmacie_departement | null;
}

export interface CommandeInfo_commandeLignes_produitCanal_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeInfo_commandeLignes_produitCanal_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: CommandeInfo_commandeLignes_produitCanal_produit_produitPhoto_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeInfo_commandeLignes_produitCanal_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface CommandeInfo_commandeLignes_produitCanal_produit_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface CommandeInfo_commandeLignes_produitCanal_produit_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  laboExploitant: CommandeInfo_commandeLignes_produitCanal_produit_produitTechReg_laboExploitant | null;
}

export interface CommandeInfo_commandeLignes_produitCanal_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  libelle2: string | null;
  produitPhoto: CommandeInfo_commandeLignes_produitCanal_produit_produitPhoto | null;
  produitCode: CommandeInfo_commandeLignes_produitCanal_produit_produitCode | null;
  produitTechReg: CommandeInfo_commandeLignes_produitCanal_produit_produitTechReg | null;
}

export interface CommandeInfo_commandeLignes_produitCanal_inOtherPanier {
  __typename: "PanierLigne";
  id: string;
}

export interface CommandeInfo_commandeLignes_produitCanal_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface CommandeInfo_commandeLignes_produitCanal_comments_data_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface CommandeInfo_commandeLignes_produitCanal_comments_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CommandeInfo_commandeLignes_produitCanal_comments_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CommandeInfo_commandeLignes_produitCanal_comments_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CommandeInfo_commandeLignes_produitCanal_comments_data_user_userPhoto_fichier | null;
}

export interface CommandeInfo_commandeLignes_produitCanal_comments_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CommandeInfo_commandeLignes_produitCanal_comments_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CommandeInfo_commandeLignes_produitCanal_comments_data_user_role | null;
  userPhoto: CommandeInfo_commandeLignes_produitCanal_comments_data_user_userPhoto | null;
  pharmacie: CommandeInfo_commandeLignes_produitCanal_comments_data_user_pharmacie | null;
}

export interface CommandeInfo_commandeLignes_produitCanal_comments_data {
  __typename: "Comment";
  id: string;
  content: string;
  idItemAssocie: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichiers: CommandeInfo_commandeLignes_produitCanal_comments_data_fichiers[] | null;
  user: CommandeInfo_commandeLignes_produitCanal_comments_data_user;
}

export interface CommandeInfo_commandeLignes_produitCanal_comments {
  __typename: "CommentResult";
  total: number;
  data: CommandeInfo_commandeLignes_produitCanal_comments_data[];
}

export interface CommandeInfo_commandeLignes_produitCanal {
  __typename: "ProduitCanal";
  type: string;
  id: string;
  produit: CommandeInfo_commandeLignes_produitCanal_produit | null;
  qteStock: number | null;
  qteMin: number | null;
  stv: number | null;
  unitePetitCond: number | null;
  uniteSupCond: number | null;
  prixFab: number | null;
  prixPhv: number | null;
  prixTtc: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  dateRetrait: any | null;
  isActive: boolean | null;
  qteTotalRemisePanachees: number | null;
  inMyPanier: boolean | null;
  inOtherPanier: CommandeInfo_commandeLignes_produitCanal_inOtherPanier | null;
  remises: (CommandeInfo_commandeLignes_produitCanal_remises | null)[] | null;
  comments: CommandeInfo_commandeLignes_produitCanal_comments | null;
}

export interface CommandeInfo_commandeLignes_status {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeInfo_commandeLignes {
  __typename: "CommandeLigne";
  type: string;
  id: string;
  pharmacie: CommandeInfo_commandeLignes_pharmacie | null;
  produitCanal: CommandeInfo_commandeLignes_produitCanal | null;
  status: CommandeInfo_commandeLignes_status | null;
  optimiser: number | null;
  quantiteCdee: number | null;
  quantiteLivree: number | null;
  uniteGratuite: number | null;
  prixBaseHT: number | null;
  remiseLigne: number | null;
  remiseGamme: number | null;
  prixNetUnitaireHT: number | null;
  prixTotalHT: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeInfo {
  __typename: "Commande";
  type: string;
  id: string;
  codeReference: string | null;
  pharmacie: CommandeInfo_pharmacie | null;
  owner: CommandeInfo_owner | null;
  operation: CommandeInfo_operation | null;
  commandeType: CommandeInfo_commandeType | null;
  commandeStatut: CommandeInfo_commandeStatut | null;
  dateCommande: any | null;
  nbrRef: number | null;
  quantite: number | null;
  uniteGratuite: number | null;
  prixBaseTotalHT: number | null;
  valeurRemiseTotal: number | null;
  prixNetTotalHT: number | null;
  fraisPort: number | null;
  valeurFrancoPort: number | null;
  remiseGlobale: number | null;
  commentaireInterne: string | null;
  commentaireExterne: string | null;
  pathFile: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  commandeLignes: (CommandeInfo_commandeLignes | null)[] | null;
}
