/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ArticleCommande } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_OPERATION_COMMANDE
// ====================================================

export interface CREATE_OPERATION_COMMANDE_createOperationCommande {
  __typename: "Commande";
  id: string;
}

export interface CREATE_OPERATION_COMMANDE {
  createOperationCommande: CREATE_OPERATION_COMMANDE_createOperationCommande | null;
}

export interface CREATE_OPERATION_COMMANDEVariables {
  idPharmacie: string;
  idOperation: string;
  idPublicite?: string | null;
  articlesCommande?: (ArticleCommande | null)[] | null;
  commentaireInterne?: string | null;
  commentaireExterne?: string | null;
}
