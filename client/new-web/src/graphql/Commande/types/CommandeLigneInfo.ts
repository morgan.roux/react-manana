/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: CommandeLigneInfo
// ====================================================

export interface CommandeLigneInfo_pharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface CommandeLigneInfo_pharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: CommandeLigneInfo_pharmacie_departement_region | null;
}

export interface CommandeLigneInfo_pharmacie {
  __typename: "Pharmacie";
  type: string;
  id: string;
  cip: string | null;
  nom: string | null;
  adresse1: string | null;
  ville: string | null;
  departement: CommandeLigneInfo_pharmacie_departement | null;
}

export interface CommandeLigneInfo_produitCanal_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeLigneInfo_produitCanal_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: CommandeLigneInfo_produitCanal_produit_produitPhoto_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeLigneInfo_produitCanal_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface CommandeLigneInfo_produitCanal_produit_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface CommandeLigneInfo_produitCanal_produit_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  laboExploitant: CommandeLigneInfo_produitCanal_produit_produitTechReg_laboExploitant | null;
}

export interface CommandeLigneInfo_produitCanal_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  libelle2: string | null;
  produitPhoto: CommandeLigneInfo_produitCanal_produit_produitPhoto | null;
  produitCode: CommandeLigneInfo_produitCanal_produit_produitCode | null;
  produitTechReg: CommandeLigneInfo_produitCanal_produit_produitTechReg | null;
}

export interface CommandeLigneInfo_produitCanal_inOtherPanier {
  __typename: "PanierLigne";
  id: string;
}

export interface CommandeLigneInfo_produitCanal_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface CommandeLigneInfo_produitCanal_comments_data_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface CommandeLigneInfo_produitCanal_comments_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CommandeLigneInfo_produitCanal_comments_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CommandeLigneInfo_produitCanal_comments_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CommandeLigneInfo_produitCanal_comments_data_user_userPhoto_fichier | null;
}

export interface CommandeLigneInfo_produitCanal_comments_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CommandeLigneInfo_produitCanal_comments_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CommandeLigneInfo_produitCanal_comments_data_user_role | null;
  userPhoto: CommandeLigneInfo_produitCanal_comments_data_user_userPhoto | null;
  pharmacie: CommandeLigneInfo_produitCanal_comments_data_user_pharmacie | null;
}

export interface CommandeLigneInfo_produitCanal_comments_data {
  __typename: "Comment";
  id: string;
  content: string;
  idItemAssocie: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichiers: CommandeLigneInfo_produitCanal_comments_data_fichiers[] | null;
  user: CommandeLigneInfo_produitCanal_comments_data_user;
}

export interface CommandeLigneInfo_produitCanal_comments {
  __typename: "CommentResult";
  total: number;
  data: CommandeLigneInfo_produitCanal_comments_data[];
}

export interface CommandeLigneInfo_produitCanal {
  __typename: "ProduitCanal";
  type: string;
  id: string;
  produit: CommandeLigneInfo_produitCanal_produit | null;
  qteStock: number | null;
  qteMin: number | null;
  stv: number | null;
  unitePetitCond: number | null;
  uniteSupCond: number | null;
  prixFab: number | null;
  prixPhv: number | null;
  prixTtc: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  dateRetrait: any | null;
  isActive: boolean | null;
  qteTotalRemisePanachees: number | null;
  inMyPanier: boolean | null;
  inOtherPanier: CommandeLigneInfo_produitCanal_inOtherPanier | null;
  remises: (CommandeLigneInfo_produitCanal_remises | null)[] | null;
  comments: CommandeLigneInfo_produitCanal_comments | null;
}

export interface CommandeLigneInfo_status {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CommandeLigneInfo {
  __typename: "CommandeLigne";
  type: string;
  id: string;
  pharmacie: CommandeLigneInfo_pharmacie | null;
  produitCanal: CommandeLigneInfo_produitCanal | null;
  status: CommandeLigneInfo_status | null;
  optimiser: number | null;
  quantiteCdee: number | null;
  quantiteLivree: number | null;
  uniteGratuite: number | null;
  prixBaseHT: number | null;
  remiseLigne: number | null;
  remiseGamme: number | null;
  prixNetUnitaireHT: number | null;
  prixTotalHT: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}
