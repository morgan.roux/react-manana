/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UPDATE_STATUT_COMMANDE
// ====================================================

export interface UPDATE_STATUT_COMMANDE_updateStatutCommande_commandeType {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface UPDATE_STATUT_COMMANDE_updateStatutCommande_commandeStatut {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface UPDATE_STATUT_COMMANDE_updateStatutCommande {
  __typename: "Commande";
  type: string;
  id: string;
  commandeType: UPDATE_STATUT_COMMANDE_updateStatutCommande_commandeType | null;
  commandeStatut: UPDATE_STATUT_COMMANDE_updateStatutCommande_commandeStatut | null;
  dateCommande: any | null;
  nbrRef: number | null;
  quantite: number | null;
  uniteGratuite: number | null;
  prixBaseTotalHT: number | null;
  valeurRemiseTotal: number | null;
  prixNetTotalHT: number | null;
  fraisPort: number | null;
  valeurFrancoPort: number | null;
  remiseGlobale: number | null;
  commentaireInterne: string | null;
  commentaireExterne: string | null;
  pathFile: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface UPDATE_STATUT_COMMANDE {
  updateStatutCommande: UPDATE_STATUT_COMMANDE_updateStatutCommande | null;
}

export interface UPDATE_STATUT_COMMANDEVariables {
  id: string;
  statut?: string | null;
}
