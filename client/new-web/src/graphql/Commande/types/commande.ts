/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: commande
// ====================================================

export interface commande_commande_commandeStatut {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface commande_commande_operation {
  __typename: "Operation";
  id: string;
  libelle: string | null;
}

export interface commande_commande_commandeLignes_produitCanal_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface commande_commande_commandeLignes_produitCanal_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: commande_commande_commandeLignes_produitCanal_produit_produitPhoto_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface commande_commande_commandeLignes_produitCanal_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface commande_commande_commandeLignes_produitCanal_produit_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface commande_commande_commandeLignes_produitCanal_produit_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  laboExploitant: commande_commande_commandeLignes_produitCanal_produit_produitTechReg_laboExploitant | null;
}

export interface commande_commande_commandeLignes_produitCanal_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  libelle2: string | null;
  produitPhoto: commande_commande_commandeLignes_produitCanal_produit_produitPhoto | null;
  produitCode: commande_commande_commandeLignes_produitCanal_produit_produitCode | null;
  produitTechReg: commande_commande_commandeLignes_produitCanal_produit_produitTechReg | null;
}

export interface commande_commande_commandeLignes_produitCanal_inOtherPanier {
  __typename: "PanierLigne";
  id: string;
}

export interface commande_commande_commandeLignes_produitCanal_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface commande_commande_commandeLignes_produitCanal_comments_data_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface commande_commande_commandeLignes_produitCanal_comments_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface commande_commande_commandeLignes_produitCanal_comments_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface commande_commande_commandeLignes_produitCanal_comments_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: commande_commande_commandeLignes_produitCanal_comments_data_user_userPhoto_fichier | null;
}

export interface commande_commande_commandeLignes_produitCanal_comments_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface commande_commande_commandeLignes_produitCanal_comments_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: commande_commande_commandeLignes_produitCanal_comments_data_user_role | null;
  userPhoto: commande_commande_commandeLignes_produitCanal_comments_data_user_userPhoto | null;
  pharmacie: commande_commande_commandeLignes_produitCanal_comments_data_user_pharmacie | null;
}

export interface commande_commande_commandeLignes_produitCanal_comments_data {
  __typename: "Comment";
  id: string;
  content: string;
  idItemAssocie: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichiers: commande_commande_commandeLignes_produitCanal_comments_data_fichiers[] | null;
  user: commande_commande_commandeLignes_produitCanal_comments_data_user;
}

export interface commande_commande_commandeLignes_produitCanal_comments {
  __typename: "CommentResult";
  total: number;
  data: commande_commande_commandeLignes_produitCanal_comments_data[];
}

export interface commande_commande_commandeLignes_produitCanal {
  __typename: "ProduitCanal";
  type: string;
  id: string;
  produit: commande_commande_commandeLignes_produitCanal_produit | null;
  qteStock: number | null;
  qteMin: number | null;
  stv: number | null;
  unitePetitCond: number | null;
  uniteSupCond: number | null;
  prixFab: number | null;
  prixPhv: number | null;
  prixTtc: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  dateRetrait: any | null;
  isActive: boolean | null;
  qteTotalRemisePanachees: number | null;
  inMyPanier: boolean | null;
  inOtherPanier: commande_commande_commandeLignes_produitCanal_inOtherPanier | null;
  remises: (commande_commande_commandeLignes_produitCanal_remises | null)[] | null;
  comments: commande_commande_commandeLignes_produitCanal_comments | null;
}

export interface commande_commande_commandeLignes_status {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface commande_commande_commandeLignes {
  __typename: "CommandeLigne";
  type: string;
  id: string;
  produitCanal: commande_commande_commandeLignes_produitCanal | null;
  status: commande_commande_commandeLignes_status | null;
  optimiser: number | null;
  quantiteCdee: number | null;
  quantiteLivree: number | null;
  uniteGratuite: number | null;
  prixBaseHT: number | null;
  remiseLigne: number | null;
  remiseGamme: number | null;
  prixNetUnitaireHT: number | null;
  prixTotalHT: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface commande_commande {
  __typename: "Commande";
  type: string;
  id: string;
  codeReference: string | null;
  dateCommande: any | null;
  nbrRef: number | null;
  quantite: number | null;
  commandeStatut: commande_commande_commandeStatut | null;
  operation: commande_commande_operation | null;
  uniteGratuite: number | null;
  prixBaseTotalHT: number | null;
  valeurRemiseTotal: number | null;
  prixNetTotalHT: number | null;
  fraisPort: number | null;
  valeurFrancoPort: number | null;
  remiseGlobale: number | null;
  commentaireInterne: string | null;
  commentaireExterne: string | null;
  pathFile: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  commandeLignes: (commande_commande_commandeLignes | null)[] | null;
}

export interface commande {
  commande: commande_commande | null;
}

export interface commandeVariables {
  id: string;
  idPharmacie: string;
  skip?: number | null;
  take?: number | null;
  idRemiseOperation?: string | null;
}
