import gql from 'graphql-tag';
import { COMMANDE_LIGNES_FRAGMENT, STATUS_TYPE_PAYLOAD_INFO_FRAGMENT } from './fragment';
import { PRODUIT_CANAL_INFO_FRAGMENT } from '../ProduitCanal/fragment';
import { CANAL_SOUS_GAMME_INFO } from '../CanalSousGamme/fragment';
import { SOUS_GAMME_CATALOGUE_INFO } from '../SousGammeCatalogue/fragment';
import { COMMANDE_CANAL_INFO_FRAGMENT } from '../CommandeCanal/fragment';

export const GET_COMMANDE_LIGNES = gql`
  query commande($id: ID!, $idPharmacie: ID!, $skip: Int, $take: Int, $idRemiseOperation: ID) {
    commande(id: $id) {
      type
      id
      codeReference
      dateCommande
      nbrRef
      quantite
      commandeStatut {
        ...StatutTypePayloadInfo
      }
      operation {
        id
        libelle
      }
      uniteGratuite
      prixBaseTotalHT
      valeurRemiseTotal
      prixNetTotalHT
      fraisPort
      valeurFrancoPort
      remiseGlobale
      commentaireInterne
      commentaireExterne
      pathFile
      dateCreation
      dateModification
      commandeLignes {
        type
        id
        produitCanal {
          ...ProduitCanalInfo
        }
        status {
          id
          code
          libelle
          dateCreation
          dateModification
        }
        optimiser
        quantiteCdee
        quantiteLivree
        uniteGratuite
        prixBaseHT
        remiseLigne
        remiseGamme
        prixNetUnitaireHT
        prixTotalHT
        dateCreation
        dateModification
      }
    }
  }
  ${STATUS_TYPE_PAYLOAD_INFO_FRAGMENT}
  ${PRODUIT_CANAL_INFO_FRAGMENT}
`;

export const GET_SEARCH_CUSTOM_CONTENT_COMMANDE = gql`
  query SEARCH_CUSTOM_CONTENT_COMMANDE($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Commande {
          id
          codeReference
          operation {
            id
            libelle
          }
          commandeType {
            id
            libelle
          }
          commandeStatut {
            id
            libelle
          }
          owner {
            id
            userName
          }
          prixNetTotalHT
          dateCreation
        }
      }
    }
  }
`;
