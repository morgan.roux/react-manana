import gql from 'graphql-tag';
import { USER_INFO_FRAGEMENT } from '../User/fragment';
import { OPERATION_COMMERCIALE_INFO_FRAGEMENT } from '../OperationCommerciale/fragment';
import { PHARMACIE_MINIM_INFO_FRAGMENT } from '../Pharmacie/fragment';
import { PRODUIT_CANAL_INFO_FRAGMENT } from '../ProduitCanal/fragment';
import { CANAL_SOUS_GAMME_INFO } from '../CanalSousGamme/fragment';
import { SOUS_GAMME_CATALOGUE_INFO } from '../SousGammeCatalogue/fragment';
import { COMMANDE_CANAL_INFO_FRAGMENT } from '../CommandeCanal/fragment';

export const STATUS_TYPE_PAYLOAD_INFO_FRAGMENT = gql`
  fragment StatutTypePayloadInfo on StatutTypePayload {
    id
    code
    libelle
    dateCreation
    dateModification
  }
`;

export const COMMANDE_LIGNE_INFO_FRAGMENT = gql`
  fragment CommandeLigneInfo on CommandeLigne {
    type
    id
    pharmacie {
      ...PharmacieMinimInfo
    }
    produitCanal {
      ...ProduitCanalInfo
    }
    status {
      ...StatutTypePayloadInfo
    }
    optimiser
    quantiteCdee
    quantiteLivree
    uniteGratuite
    prixBaseHT
    remiseLigne
    remiseGamme
    prixNetUnitaireHT
    prixTotalHT
    dateCreation
    dateModification
  }
  ${PHARMACIE_MINIM_INFO_FRAGMENT}
  ${PRODUIT_CANAL_INFO_FRAGMENT}
  ${STATUS_TYPE_PAYLOAD_INFO_FRAGMENT}
`;

export const COMMANDE_INFO_FRAGEMENT = gql`
  fragment CommandeInfo on Commande {
    type
    id
    codeReference
    pharmacie {
      ...PharmacieMinimInfo
    }
    owner {
      ...UserInfo
    }
    operation {
      ...OperationCommercialeInfo
    }
    commandeType {
      ...StatutTypePayloadInfo
    }
    commandeStatut {
      ...StatutTypePayloadInfo
    }
    dateCommande
    nbrRef
    quantite
    uniteGratuite
    prixBaseTotalHT
    valeurRemiseTotal
    prixNetTotalHT
    fraisPort
    valeurFrancoPort
    remiseGlobale
    commentaireInterne
    commentaireExterne
    pathFile
    dateCreation
    dateModification
    commandeLignes {
      ...CommandeLigneInfo
    }
  }
  ${PHARMACIE_MINIM_INFO_FRAGMENT}
  ${USER_INFO_FRAGEMENT}
  ${OPERATION_COMMERCIALE_INFO_FRAGEMENT}
  ${STATUS_TYPE_PAYLOAD_INFO_FRAGMENT}
  ${COMMANDE_LIGNE_INFO_FRAGMENT}
`;

export const COMMANDE_LIGNES_FRAGMENT = gql`
  fragment Commande_Lignes on Commande {
    id
    commandeLignes {
      type
      id
      produitCanal {
        ...ProduitCanalInfo
      }
      status {
        id
        code
        libelle
        dateCreation
        dateModification
      }
      optimiser
      quantiteCdee
      quantiteLivree
      uniteGratuite
      prixBaseHT
      remiseLigne
      remiseGamme
      prixNetUnitaireHT
      prixTotalHT
      dateCreation
      dateModification
    }
  }
  ${PRODUIT_CANAL_INFO_FRAGMENT}
`;

export const COMMANDE_FRAGMENT = gql`
  fragment Commande on Commande {
    id
    operation {
      id
      libelle
    }
    ...Commande_Lignes
  }
  ${COMMANDE_LIGNES_FRAGMENT}
`;
