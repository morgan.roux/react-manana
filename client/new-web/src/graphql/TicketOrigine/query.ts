import gql from 'graphql-tag';

export const GET_TICKET_ORIGINES = gql`
  query TICKET_ORIGINES {
    ticketOrigines {
      id
      nom
    }
  }
`;
