/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TicketMotifInfo
// ====================================================

export interface TicketMotifInfo {
  __typename: "TicketMotif";
  id: string;
  nom: string;
}
