/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TicketVisibilite, TicketAppel, OrigineType, StatusTicket } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: TicketInfo
// ====================================================

export interface TicketInfo_declarant_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface TicketInfo_declarant_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface TicketInfo_declarant {
  __typename: "User";
  id: string;
  userName: string | null;
  role: TicketInfo_declarant_role | null;
  pharmacie: TicketInfo_declarant_pharmacie | null;
}

export interface TicketInfo_usersConcernees_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface TicketInfo_usersConcernees_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface TicketInfo_usersConcernees {
  __typename: "User";
  id: string;
  userName: string | null;
  role: TicketInfo_usersConcernees_role | null;
  pharmacie: TicketInfo_usersConcernees_pharmacie | null;
}

export interface TicketInfo_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  photo: string;
}

export interface TicketInfo_motif {
  __typename: "TicketMotif";
  id: string;
  nom: string;
}

export interface TicketInfo_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface TicketInfo_userCreation_userPersonnel_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface TicketInfo_userCreation_userPersonnel {
  __typename: "Personnel";
  id: string;
  service: TicketInfo_userCreation_userPersonnel_service | null;
}

export interface TicketInfo_userCreation_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface TicketInfo_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
  pharmacie: TicketInfo_userCreation_pharmacie | null;
  userPersonnel: TicketInfo_userCreation_userPersonnel | null;
  role: TicketInfo_userCreation_role | null;
}

export interface TicketInfo_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface TicketInfo {
  __typename: "Ticket";
  type: string;
  id: string;
  idOrganisation: string | null;
  nomOrganisation: string | null;
  dateHeureSaisie: any | null;
  commentaire: string | null;
  visibilite: TicketVisibilite | null;
  priority: number | null;
  typeAppel: TicketAppel | null;
  declarant: TicketInfo_declarant | null;
  usersConcernees: (TicketInfo_usersConcernees | null)[] | null;
  smyley: TicketInfo_smyley | null;
  motif: TicketInfo_motif | null;
  userCreation: TicketInfo_userCreation | null;
  fichiers: (TicketInfo_fichiers | null)[] | null;
  origineType: OrigineType | null;
  statusTicket: StatusTicket | null;
}
