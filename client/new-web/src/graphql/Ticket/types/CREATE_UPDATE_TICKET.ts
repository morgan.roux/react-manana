/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TicketInput, TicketVisibilite, TicketType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_TICKET
// ====================================================

export interface CREATE_UPDATE_TICKET_createUpdateTicket {
  __typename: "Ticket";
  id: string;
  commentaire: string | null;
  visibilite: TicketVisibilite | null;
  typeTicket: TicketType | null;
}

export interface CREATE_UPDATE_TICKET {
  createUpdateTicket: CREATE_UPDATE_TICKET_createUpdateTicket | null;
}

export interface CREATE_UPDATE_TICKETVariables {
  input: TicketInput;
}
