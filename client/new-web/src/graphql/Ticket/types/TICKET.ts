/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TicketVisibilite, TicketAppel, OrigineType, StatusTicket } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: TICKET
// ====================================================

export interface TICKET_ticket_declarant_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface TICKET_ticket_declarant_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface TICKET_ticket_declarant {
  __typename: "User";
  id: string;
  userName: string | null;
  role: TICKET_ticket_declarant_role | null;
  pharmacie: TICKET_ticket_declarant_pharmacie | null;
}

export interface TICKET_ticket_usersConcernees_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface TICKET_ticket_usersConcernees_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface TICKET_ticket_usersConcernees {
  __typename: "User";
  id: string;
  userName: string | null;
  role: TICKET_ticket_usersConcernees_role | null;
  pharmacie: TICKET_ticket_usersConcernees_pharmacie | null;
}

export interface TICKET_ticket_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  photo: string;
}

export interface TICKET_ticket_motif {
  __typename: "TicketMotif";
  id: string;
  nom: string;
}

export interface TICKET_ticket_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface TICKET_ticket_userCreation_userPersonnel_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface TICKET_ticket_userCreation_userPersonnel {
  __typename: "Personnel";
  id: string;
  service: TICKET_ticket_userCreation_userPersonnel_service | null;
}

export interface TICKET_ticket_userCreation_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface TICKET_ticket_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
  pharmacie: TICKET_ticket_userCreation_pharmacie | null;
  userPersonnel: TICKET_ticket_userCreation_userPersonnel | null;
  role: TICKET_ticket_userCreation_role | null;
}

export interface TICKET_ticket_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface TICKET_ticket {
  __typename: "Ticket";
  type: string;
  id: string;
  idOrganisation: string | null;
  nomOrganisation: string | null;
  dateHeureSaisie: any | null;
  commentaire: string | null;
  visibilite: TicketVisibilite | null;
  priority: number | null;
  typeAppel: TicketAppel | null;
  declarant: TICKET_ticket_declarant | null;
  usersConcernees: (TICKET_ticket_usersConcernees | null)[] | null;
  smyley: TICKET_ticket_smyley | null;
  motif: TICKET_ticket_motif | null;
  userCreation: TICKET_ticket_userCreation | null;
  fichiers: (TICKET_ticket_fichiers | null)[] | null;
  origineType: OrigineType | null;
  statusTicket: StatusTicket | null;
}

export interface TICKET {
  ticket: TICKET_ticket | null;
}

export interface TICKETVariables {
  id: string;
}
