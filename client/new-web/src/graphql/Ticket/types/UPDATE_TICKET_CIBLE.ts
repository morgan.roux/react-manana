/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TicketCibleInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_TICKET_CIBLE
// ====================================================

export interface UPDATE_TICKET_CIBLE_updateTicketCible {
  __typename: "TicketChangementCible";
  id: string;
  dateHeureAffectation: any;
  commentaire: string | null;
}

export interface UPDATE_TICKET_CIBLE {
  updateTicketCible: UPDATE_TICKET_CIBLE_updateTicketCible | null;
}

export interface UPDATE_TICKET_CIBLEVariables {
  input: TicketCibleInput;
}
