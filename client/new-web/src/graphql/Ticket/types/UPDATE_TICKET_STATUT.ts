/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { StatusTicket } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_TICKET_STATUT
// ====================================================

export interface UPDATE_TICKET_STATUT_updateTicketStatut {
  __typename: "Ticket";
  id: string;
  statusTicket: StatusTicket | null;
}

export interface UPDATE_TICKET_STATUT {
  updateTicketStatut: UPDATE_TICKET_STATUT_updateTicketStatut | null;
}

export interface UPDATE_TICKET_STATUTVariables {
  id: string;
  status: StatusTicket;
}
