import { DO_CREATE_UPDATE_TICKET } from './mutation';
import { GET_SEARCH_CUSTOM_CONTENT_TICKET } from './query';

export { DO_CREATE_UPDATE_TICKET, GET_SEARCH_CUSTOM_CONTENT_TICKET };
