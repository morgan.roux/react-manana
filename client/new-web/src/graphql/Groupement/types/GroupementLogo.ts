/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: GroupementLogo
// ====================================================

export interface GroupementLogo_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface GroupementLogo_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: GroupementLogo_groupementLogo_fichier | null;
}

export interface GroupementLogo {
  __typename: "Groupement";
  groupementLogo: GroupementLogo_groupementLogo | null;
}
