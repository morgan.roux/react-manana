import gql from 'graphql-tag';

export const DO_CREATE_GROUPEMENT = gql`
  mutation CREATE_GROUPEMENT(
    $nom: String!
    $adresse1: String!
    $adresse2: String
    $cp: String!
    $ville: String!
    $pays: String
    $telBureau: String!
    $telMobile: String
    $mail: String!
    $site: String
    $commentaire: String
    $nomResponsable: String!
    $prenomResponsable: String!
    $dateSortie: DateTime
    $sortie: Int
    $avatar: FichierInput
  ) {
    createGroupement(
      nom: $nom
      adresse1: $adresse1
      adresse2: $adresse2
      cp: $cp
      ville: $ville
      pays: $pays
      telBureau: $telBureau
      telMobile: $telMobile
      mail: $mail
      site: $site
      commentaire: $commentaire
      nomResponsable: $nomResponsable
      prenomResponsable: $prenomResponsable
      dateSortie: $dateSortie
      sortie: $sortie
      avatar: $avatar
    ) {
      id
      nom
      adresse1
      adresse2
      cp
      ville
      pays
      telBureau
      telMobile
      mail
      site
      commentaire
      nomResponsable
      prenomResponsable
      dateSortie
      sortie
      groupementLogo {
        id
        fichier {
          id
          chemin
        }
      }
    }
  }
`;

export const DO_UPDATE_GROUPEMENT = gql`
  mutation UPDATE_GROUPEMENT(
    $id: ID!
    $nom: String!
    $adresse1: String!
    $adresse2: String
    $cp: String!
    $ville: String!
    $pays: String
    $telBureau: String!
    $telMobile: String
    $mail: String!
    $site: String
    $commentaire: String
    $nomResponsable: String!
    $prenomResponsable: String!
    $dateSortie: DateTime
    $sortie: Int
    $avatar: FichierInput
  ) {
    updateGroupement(
      id: $id
      nom: $nom
      adresse1: $adresse1
      adresse2: $adresse2
      cp: $cp
      ville: $ville
      pays: $pays
      telBureau: $telBureau
      telMobile: $telMobile
      mail: $mail
      site: $site
      commentaire: $commentaire
      nomResponsable: $nomResponsable
      prenomResponsable: $prenomResponsable
      dateSortie: $dateSortie
      sortie: $sortie
      avatar: $avatar
    ) {
      id
      nom
      adresse1
      adresse2
      cp
      ville
      pays
      telBureau
      telMobile
      mail
      site
      commentaire
      nomResponsable
      prenomResponsable
      dateSortie
      sortie
      groupementLogo {
        id
        fichier {
          id
          chemin
        }
      }
    }
  }
`;

export const DO_DELETE_GROUPEMENT = gql`
  mutation DELETE_GROUPEMENT($id: ID!) {
    deleteGroupement(id: $id) {
      id
    }
  }
`;

export const DO_CREATE_GROUPEMENT_LOGO = gql`
  mutation CREATE_GROUPEMENT_LOGO($id: ID!, $chemin: String!, $nomOriginal: String!) {
    createGroupementLogo(id: $id, chemin: $chemin, nomOriginal: $nomOriginal) {
      id
      chemin
      nomOriginal
    }
  }
`;

export const DO_DELETE_GROUPEMENT_LOGO = gql`
  mutation DELETE_GROUPEMENT_LOGO($chemin: String!) {
    deleteGroupementLogo(chemin: $chemin) {
      id
    }
  }
`;
