import gql from 'graphql-tag';
import { GROUPEMENT_INFO } from './fragement';

export const GET_GROUPEMENTS = gql`
  query GROUPEMENTS {
    groupements {
      id
      nom
      groupementLogo {
        id
        fichier {
          id
          chemin
        }
      }
    }
  }
`;
export const GET_GROUPEMENT = gql`
  query GROUPEMENT($id: ID!) {
    groupement(id: $id) {
      id
      ...GroupementInfo
    }
  }
  ${GROUPEMENT_INFO}
`;


export const GET_GROUPEMENT_WITHOUT_PHARMACIES = gql`
  query GROUPEMENT_WITHOUT_PHARMACIES($id: ID!) {
    groupement(id: $id) {
      id
      pharmacies {
        id
        nom
        ville
        cip
        adresse1
        type
        departement {
          id
          nom
          region {
            id
            nom
          }
        }
      }
      ...GroupementInfo
    }
  }
  ${GROUPEMENT_INFO}
`;
