import gql from 'graphql-tag';
import { SUIVI_PUBLICITAIRE_FRAGMENT } from './fragment';

export const GET_SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIRE = gql`
  query SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIRE(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
  ) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on SuiviPublicitaire {
          ...SuiviPublicitaireInfo
        }
      }
    }
  }
  ${SUIVI_PUBLICITAIRE_FRAGMENT}
`;
