/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIRE
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIRE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIRE_search_data_SuiviPublicitaire_publicite {
  __typename: "Publicite";
  id: string;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
}

export interface SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIRE_search_data_SuiviPublicitaire_commande {
  __typename: "Commande";
  id: string;
}

export interface SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIRE_search_data_SuiviPublicitaire {
  __typename: "SuiviPublicitaire";
  id: string;
  publicite: SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIRE_search_data_SuiviPublicitaire_publicite | null;
  commande: SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIRE_search_data_SuiviPublicitaire_commande | null;
  nbView: number | null;
  nbClick: number | null;
  nbAccesOp: number | null;
  nbCommande: number | null;
}

export type SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIRE_search_data = SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIRE_search_data_Action | SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIRE_search_data_SuiviPublicitaire;

export interface SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIRE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIRE_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIRE {
  search: SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIRE_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_SUIVI_PUBLICITAIREVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
