/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: incrementPubliciteViewCount
// ====================================================

export interface incrementPubliciteViewCount_incrementPubliciteViewCount_publicite {
  __typename: "Publicite";
  id: string;
}

export interface incrementPubliciteViewCount_incrementPubliciteViewCount {
  __typename: "SuiviPublicitaire";
  id: string;
  publicite: incrementPubliciteViewCount_incrementPubliciteViewCount_publicite | null;
  nbView: number | null;
  nbClick: number | null;
  nbAccesOp: number | null;
  nbCommande: number | null;
}

export interface incrementPubliciteViewCount {
  incrementPubliciteViewCount: incrementPubliciteViewCount_incrementPubliciteViewCount | null;
}

export interface incrementPubliciteViewCountVariables {
  idPublicite: string;
}
