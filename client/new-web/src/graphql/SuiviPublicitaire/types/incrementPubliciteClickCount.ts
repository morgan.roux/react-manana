/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: incrementPubliciteClickCount
// ====================================================

export interface incrementPubliciteClickCount_incrementPubliciteClickCount_publicite {
  __typename: "Publicite";
  id: string;
}

export interface incrementPubliciteClickCount_incrementPubliciteClickCount {
  __typename: "SuiviPublicitaire";
  id: string;
  publicite: incrementPubliciteClickCount_incrementPubliciteClickCount_publicite | null;
  nbView: number | null;
  nbClick: number | null;
  nbAccesOp: number | null;
  nbCommande: number | null;
}

export interface incrementPubliciteClickCount {
  incrementPubliciteClickCount: incrementPubliciteClickCount_incrementPubliciteClickCount | null;
}

export interface incrementPubliciteClickCountVariables {
  idPublicite: string;
}
