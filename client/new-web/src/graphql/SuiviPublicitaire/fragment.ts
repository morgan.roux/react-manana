import gql from 'graphql-tag';

export const SUIVI_PUBLICITAIRE_FRAGMENT = gql`
  fragment SuiviPublicitaireInfo on SuiviPublicitaire {
    id
    publicite {
      id
      libelle
      dateDebut
      dateFin
    }
    commande {
      id
    }
    nbView
    nbClick
    nbAccesOp
    nbCommande
  }
`;
