import gql from 'graphql-tag';
import { GROSSISTE_INFO_FRAGMENT } from './fragment';

export const GET_GROSSISTES = gql`
  query GROSSISTES {
    grossistes {
      ...GrossisteInfo
    }
  }
  ${GROSSISTE_INFO_FRAGMENT}
`;
