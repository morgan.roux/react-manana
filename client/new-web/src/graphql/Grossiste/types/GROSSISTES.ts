/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GROSSISTES
// ====================================================

export interface GROSSISTES_grossistes {
  __typename: "Grossiste";
  id: string;
  nom: string | null;
}

export interface GROSSISTES {
  grossistes: (GROSSISTES_grossistes | null)[] | null;
}
