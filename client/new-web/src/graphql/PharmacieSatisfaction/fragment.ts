import gql from 'graphql-tag';

export const PHARMACIE_SATISFACTION_INFO_FRAGMENT = gql`
  fragment PharmacieSatisfactionInfo on PharmacieSatisfaction {
    id
    dateSaisie
    commentaire
    smyley {
      id
      photo
      note
      nom
    }
    user {
      id
      userName
    }
  }
`;
