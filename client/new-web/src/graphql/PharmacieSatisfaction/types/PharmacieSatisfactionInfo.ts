/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PharmacieSatisfactionInfo
// ====================================================

export interface PharmacieSatisfactionInfo_smyley {
  __typename: "Smyley";
  id: string;
  photo: string;
  note: number | null;
  nom: string;
}

export interface PharmacieSatisfactionInfo_user {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface PharmacieSatisfactionInfo {
  __typename: "PharmacieSatisfaction";
  id: string;
  dateSaisie: any | null;
  commentaire: string | null;
  smyley: PharmacieSatisfactionInfo_smyley | null;
  user: PharmacieSatisfactionInfo_user | null;
}
