/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeProject, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_PROJECTS
// ====================================================

export interface DELETE_PROJECTS_softDeleteProjects_projetParent {
  __typename: "Project";
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface DELETE_PROJECTS_softDeleteProjects_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
}

export interface DELETE_PROJECTS_softDeleteProjects_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
}

export interface DELETE_PROJECTS_softDeleteProjects_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface DELETE_PROJECTS_softDeleteProjects_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface DELETE_PROJECTS_softDeleteProjects_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: DELETE_PROJECTS_softDeleteProjects_user_userPhoto_fichier | null;
}

export interface DELETE_PROJECTS_softDeleteProjects_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface DELETE_PROJECTS_softDeleteProjects_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: DELETE_PROJECTS_softDeleteProjects_user_role | null;
  userPhoto: DELETE_PROJECTS_softDeleteProjects_user_userPhoto | null;
  pharmacie: DELETE_PROJECTS_softDeleteProjects_user_pharmacie | null;
}

export interface DELETE_PROJECTS_softDeleteProjects_subProjects_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
}

export interface DELETE_PROJECTS_softDeleteProjects_subProjects_projetParent {
  __typename: "Project";
  id: string;
  _name: string | null;
}

export interface DELETE_PROJECTS_softDeleteProjects_subProjects_participants {
  __typename: "User";
  id: string;
}

export interface DELETE_PROJECTS_softDeleteProjects_subProjects_user {
  __typename: "User";
  id: string;
}

export interface DELETE_PROJECTS_softDeleteProjects_subProjects {
  __typename: "Project";
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  isRemoved: boolean | null;
  isInFavoris: boolean | null;
  ordre: number | null;
  nbAction: number;
  couleur: DELETE_PROJECTS_softDeleteProjects_subProjects_couleur | null;
  projetParent: DELETE_PROJECTS_softDeleteProjects_subProjects_projetParent | null;
  participants: DELETE_PROJECTS_softDeleteProjects_subProjects_participants[] | null;
  user: DELETE_PROJECTS_softDeleteProjects_subProjects_user;
}

export interface DELETE_PROJECTS_softDeleteProjects_participants {
  __typename: "User";
  id: string;
}

export interface DELETE_PROJECTS_softDeleteProjects {
  __typename: "Project";
  type: string;
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  activeActions: boolean | null;
  isRemoved: boolean | null;
  isInFavoris: boolean | null;
  idFonction: string | null;
  idTache: string | null;
  ordre: number | null;
  nbAction: number;
  projetParent: DELETE_PROJECTS_softDeleteProjects_projetParent | null;
  couleur: DELETE_PROJECTS_softDeleteProjects_couleur | null;
  groupement: DELETE_PROJECTS_softDeleteProjects_groupement | null;
  user: DELETE_PROJECTS_softDeleteProjects_user;
  subProjects: DELETE_PROJECTS_softDeleteProjects_subProjects[] | null;
  dateCreation: any | null;
  dateModification: any | null;
  participants: DELETE_PROJECTS_softDeleteProjects_participants[] | null;
}

export interface DELETE_PROJECTS {
  softDeleteProjects: DELETE_PROJECTS_softDeleteProjects[];
}

export interface DELETE_PROJECTSVariables {
  ids: string[];
  actionStatus?: string | null;
}
