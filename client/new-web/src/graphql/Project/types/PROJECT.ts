/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeProject, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: PROJECT
// ====================================================

export interface PROJECT_project_projetParent {
  __typename: "Project";
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PROJECT_project_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
}

export interface PROJECT_project_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
}

export interface PROJECT_project_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PROJECT_project_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface PROJECT_project_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: PROJECT_project_user_userPhoto_fichier | null;
}

export interface PROJECT_project_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface PROJECT_project_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: PROJECT_project_user_role | null;
  userPhoto: PROJECT_project_user_userPhoto | null;
  pharmacie: PROJECT_project_user_pharmacie | null;
}

export interface PROJECT_project_subProjects_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
}

export interface PROJECT_project_subProjects_projetParent {
  __typename: "Project";
  id: string;
  _name: string | null;
}

export interface PROJECT_project_subProjects_participants {
  __typename: "User";
  id: string;
}

export interface PROJECT_project_subProjects_user {
  __typename: "User";
  id: string;
}

export interface PROJECT_project_subProjects {
  __typename: "Project";
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  isRemoved: boolean | null;
  isInFavoris: boolean | null;
  ordre: number | null;
  nbAction: number;
  couleur: PROJECT_project_subProjects_couleur | null;
  projetParent: PROJECT_project_subProjects_projetParent | null;
  participants: PROJECT_project_subProjects_participants[] | null;
  user: PROJECT_project_subProjects_user;
}

export interface PROJECT_project_participants {
  __typename: "User";
  id: string;
}

export interface PROJECT_project {
  __typename: "Project";
  type: string;
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  activeActions: boolean | null;
  isRemoved: boolean | null;
  isInFavoris: boolean | null;
  idFonction: string | null;
  idTache: string | null;
  ordre: number | null;
  nbAction: number;
  projetParent: PROJECT_project_projetParent | null;
  couleur: PROJECT_project_couleur | null;
  groupement: PROJECT_project_groupement | null;
  user: PROJECT_project_user;
  subProjects: PROJECT_project_subProjects[] | null;
  dateCreation: any | null;
  dateModification: any | null;
  participants: PROJECT_project_participants[] | null;
}

export interface PROJECT {
  project: PROJECT_project | null;
}

export interface PROJECTVariables {
  id: string;
  actionStatus?: string | null;
}
