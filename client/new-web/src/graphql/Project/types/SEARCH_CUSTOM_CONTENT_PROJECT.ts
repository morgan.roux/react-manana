/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeProject, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_PROJECT
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_projetParent {
  __typename: "Project";
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
}

export interface SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_user_userPhoto_fichier | null;
}

export interface SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_user_role | null;
  userPhoto: SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_user_userPhoto | null;
  pharmacie: SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_user_pharmacie | null;
}

export interface SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_subProjects_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
}

export interface SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_subProjects_projetParent {
  __typename: "Project";
  id: string;
  _name: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_subProjects_participants {
  __typename: "User";
  id: string;
}

export interface SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_subProjects_user {
  __typename: "User";
  id: string;
}

export interface SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_subProjects {
  __typename: "Project";
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  isRemoved: boolean | null;
  isInFavoris: boolean | null;
  ordre: number | null;
  nbAction: number;
  couleur: SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_subProjects_couleur | null;
  projetParent: SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_subProjects_projetParent | null;
  participants: SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_subProjects_participants[] | null;
  user: SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_subProjects_user;
}

export interface SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_participants {
  __typename: "User";
  id: string;
}

export interface SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project {
  __typename: "Project";
  type: string;
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  activeActions: boolean | null;
  isRemoved: boolean | null;
  isInFavoris: boolean | null;
  idFonction: string | null;
  idTache: string | null;
  ordre: number | null;
  nbAction: number;
  projetParent: SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_projetParent | null;
  couleur: SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_couleur | null;
  groupement: SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_groupement | null;
  user: SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_user;
  subProjects: SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_subProjects[] | null;
  dateCreation: any | null;
  dateModification: any | null;
  participants: SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project_participants[] | null;
}

export type SEARCH_CUSTOM_CONTENT_PROJECT_search_data = SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Action | SEARCH_CUSTOM_CONTENT_PROJECT_search_data_Project;

export interface SEARCH_CUSTOM_CONTENT_PROJECT_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_PROJECT_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_PROJECT {
  search: SEARCH_CUSTOM_CONTENT_PROJECT_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_PROJECTVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
  actionStatus?: string | null;
}
