/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeProject, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: ProjectInfo
// ====================================================

export interface ProjectInfo_projetParent {
  __typename: "Project";
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ProjectInfo_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
}

export interface ProjectInfo_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
}

export interface ProjectInfo_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface ProjectInfo_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface ProjectInfo_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: ProjectInfo_user_userPhoto_fichier | null;
}

export interface ProjectInfo_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface ProjectInfo_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: ProjectInfo_user_role | null;
  userPhoto: ProjectInfo_user_userPhoto | null;
  pharmacie: ProjectInfo_user_pharmacie | null;
}

export interface ProjectInfo_subProjects_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
}

export interface ProjectInfo_subProjects_projetParent {
  __typename: "Project";
  id: string;
  _name: string | null;
}

export interface ProjectInfo_subProjects_participants {
  __typename: "User";
  id: string;
}

export interface ProjectInfo_subProjects_user {
  __typename: "User";
  id: string;
}

export interface ProjectInfo_subProjects {
  __typename: "Project";
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  isRemoved: boolean | null;
  isInFavoris: boolean | null;
  ordre: number | null;
  nbAction: number;
  couleur: ProjectInfo_subProjects_couleur | null;
  projetParent: ProjectInfo_subProjects_projetParent | null;
  participants: ProjectInfo_subProjects_participants[] | null;
  user: ProjectInfo_subProjects_user;
}

export interface ProjectInfo_participants {
  __typename: "User";
  id: string;
}

export interface ProjectInfo {
  __typename: "Project";
  type: string;
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  activeActions: boolean | null;
  isRemoved: boolean | null;
  isInFavoris: boolean | null;
  idFonction: string | null;
  idTache: string | null;
  ordre: number | null;
  nbAction: number;
  projetParent: ProjectInfo_projetParent | null;
  couleur: ProjectInfo_couleur | null;
  groupement: ProjectInfo_groupement | null;
  user: ProjectInfo_user;
  subProjects: ProjectInfo_subProjects[] | null;
  dateCreation: any | null;
  dateModification: any | null;
  participants: ProjectInfo_participants[] | null;
}
