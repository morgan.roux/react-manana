/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PROJECTS_BY_USER_WITH_MIN_INFO
// ====================================================

export interface PROJECTS_BY_USER_WITH_MIN_INFO_projectsByUser_projetParent {
  __typename: "Project";
  id: string;
  name: string | null;
}

export interface PROJECTS_BY_USER_WITH_MIN_INFO_projectsByUser {
  __typename: "Project";
  id: string;
  name: string | null;
  projetParent: PROJECTS_BY_USER_WITH_MIN_INFO_projectsByUser_projetParent | null;
}

export interface PROJECTS_BY_USER_WITH_MIN_INFO {
  projectsByUser: PROJECTS_BY_USER_WITH_MIN_INFO_projectsByUser[];
}

export interface PROJECTS_BY_USER_WITH_MIN_INFOVariables {
  idUser: string;
  isRemoved?: boolean | null;
}
