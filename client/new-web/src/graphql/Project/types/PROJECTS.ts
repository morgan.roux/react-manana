/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeProject, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: PROJECTS
// ====================================================

export interface PROJECTS_projects_projetParent {
  __typename: "Project";
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PROJECTS_projects_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
}

export interface PROJECTS_projects_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
}

export interface PROJECTS_projects_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PROJECTS_projects_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface PROJECTS_projects_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: PROJECTS_projects_user_userPhoto_fichier | null;
}

export interface PROJECTS_projects_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface PROJECTS_projects_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: PROJECTS_projects_user_role | null;
  userPhoto: PROJECTS_projects_user_userPhoto | null;
  pharmacie: PROJECTS_projects_user_pharmacie | null;
}

export interface PROJECTS_projects_subProjects_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
}

export interface PROJECTS_projects_subProjects_projetParent {
  __typename: "Project";
  id: string;
  _name: string | null;
}

export interface PROJECTS_projects_subProjects_participants {
  __typename: "User";
  id: string;
}

export interface PROJECTS_projects_subProjects_user {
  __typename: "User";
  id: string;
}

export interface PROJECTS_projects_subProjects {
  __typename: "Project";
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  isRemoved: boolean | null;
  isInFavoris: boolean | null;
  ordre: number | null;
  nbAction: number;
  couleur: PROJECTS_projects_subProjects_couleur | null;
  projetParent: PROJECTS_projects_subProjects_projetParent | null;
  participants: PROJECTS_projects_subProjects_participants[] | null;
  user: PROJECTS_projects_subProjects_user;
}

export interface PROJECTS_projects_participants {
  __typename: "User";
  id: string;
}

export interface PROJECTS_projects {
  __typename: "Project";
  type: string;
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  activeActions: boolean | null;
  isRemoved: boolean | null;
  isInFavoris: boolean | null;
  idFonction: string | null;
  idTache: string | null;
  ordre: number | null;
  nbAction: number;
  projetParent: PROJECTS_projects_projetParent | null;
  couleur: PROJECTS_projects_couleur | null;
  groupement: PROJECTS_projects_groupement | null;
  user: PROJECTS_projects_user;
  subProjects: PROJECTS_projects_subProjects[] | null;
  dateCreation: any | null;
  dateModification: any | null;
  participants: PROJECTS_projects_participants[] | null;
}

export interface PROJECTS {
  projects: PROJECTS_projects[];
}

export interface PROJECTSVariables {
  isRemoved?: boolean | null;
  actionStatus?: string | null;
}
