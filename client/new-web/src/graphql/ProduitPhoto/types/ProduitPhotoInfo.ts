/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ProduitPhotoInfo
// ====================================================

export interface ProduitPhotoInfo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ProduitPhotoInfo {
  __typename: "ProduitPhoto";
  id: string;
  fichier: ProduitPhotoInfo_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}
