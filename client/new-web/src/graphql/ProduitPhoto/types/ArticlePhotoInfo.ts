/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeFichier } from '../../../types/graphql-global-types';

// ====================================================
// GraphQL fragment: ArticlePhotoInfo
// ====================================================

export interface ArticlePhotoInfo_fichier {
  __typename: 'Fichier';
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: TypeFichier | null;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ArticlePhotoInfo {
  __typename: 'ArticlePhoto';
  id: string;
  fichier: ArticlePhotoInfo_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}
