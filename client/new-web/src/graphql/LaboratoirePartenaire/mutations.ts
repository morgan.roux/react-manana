import gql from 'graphql-tag';
import { LABORATOIRE_PARTENAIRE_INFO_FRAGMENT } from './fragments';

export const DO_CREATE_UPDATE_LABORATOIRE_PARTENAIRE = gql`
  mutation CREATE_UPDATE_LABORATOIRE_PARTENAIRE($input: LaboratoirePartenaireInput!) {
    createUpdateLaboratoirePartenaire(input: $input) {
      ...LaboratoirePartenaireInfo
    }
  }
  ${LABORATOIRE_PARTENAIRE_INFO_FRAGMENT}
`;

export const DO_DELETE_LABORATOIRE_PARTENAIRE = gql`
  mutation DELETE_LABORATOIRE_PARTENAIRE($ids: [ID!]!) {
    softDeleteLaboratoirePartenaire(ids: $ids) {
      ...LaboratoirePartenaireInfo
    }
  }
  ${LABORATOIRE_PARTENAIRE_INFO_FRAGMENT}
`;
