import gql from 'graphql-tag';

export const GET_SEARCH_CUSTOM_CONTENT_LABORATOIRE_RESSOURCE = gql`
  query SEARCH_CUSTOM_CONTENT_LABORATOIRE_RESSOURCE(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
  ) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on LaboratoireRessource {
          id
          typeRessource
          item {
            id
            code
            name
            codeItem
          }
          code
          libelle
          dateChargement
          url
          fichier {
            id
            chemin
            nomOriginal
            urlPresigned
            publicUrl
          }
        }
      }
    }
  }
`;

export const GET_LABORATOIRE_RESSOURCE = gql`
  query LABORATOIRE_RESSOURCE($id: ID!) {
    laboratoireRessource(id: $id) {
      id
      typeRessource
      item {
        id
        code
        name
        codeItem
      }
      code
      libelle
      fichier {
        id
        chemin
        nomOriginal
        urlPresigned
        publicUrl
      }
      url
    }
  }
`;

export const DO_SEARCH_LABORATOIRE_RESSOURCE = gql`
  query SEARCH_LABORATOIRE_RESSOURCE(
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
  ) {
    search(
      type: "laboratoireressource"
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on LaboratoireRessource {
          id
          typeRessource
          item {
            id
            code
            name
            codeItem
          }
          code
          libelle
          dateChargement
          url
          fichier {
            id
            chemin
            nomOriginal
            urlPresigned
            publicUrl
          }
        }
      }
    }
  }
`;
