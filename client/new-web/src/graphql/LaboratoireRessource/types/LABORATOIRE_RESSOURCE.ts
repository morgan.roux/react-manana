/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: LABORATOIRE_RESSOURCE
// ====================================================

export interface LABORATOIRE_RESSOURCE_laboratoireRessource_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
}

export interface LABORATOIRE_RESSOURCE_laboratoireRessource_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface LABORATOIRE_RESSOURCE_laboratoireRessource {
  __typename: "LaboratoireRessource";
  id: string;
  typeRessource: string | null;
  item: LABORATOIRE_RESSOURCE_laboratoireRessource_item | null;
  code: string | null;
  libelle: string | null;
  fichier: LABORATOIRE_RESSOURCE_laboratoireRessource_fichier | null;
  url: string | null;
}

export interface LABORATOIRE_RESSOURCE {
  laboratoireRessource: LABORATOIRE_RESSOURCE_laboratoireRessource | null;
}

export interface LABORATOIRE_RESSOURCEVariables {
  id: string;
}
