/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_LABORATOIRE_RESSOURCE
// ====================================================

export interface SEARCH_LABORATOIRE_RESSOURCE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_LABORATOIRE_RESSOURCE_search_data_LaboratoireRessource_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
}

export interface SEARCH_LABORATOIRE_RESSOURCE_search_data_LaboratoireRessource_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface SEARCH_LABORATOIRE_RESSOURCE_search_data_LaboratoireRessource {
  __typename: "LaboratoireRessource";
  id: string;
  typeRessource: string | null;
  item: SEARCH_LABORATOIRE_RESSOURCE_search_data_LaboratoireRessource_item | null;
  code: string | null;
  libelle: string | null;
  dateChargement: any | null;
  url: string | null;
  fichier: SEARCH_LABORATOIRE_RESSOURCE_search_data_LaboratoireRessource_fichier | null;
}

export type SEARCH_LABORATOIRE_RESSOURCE_search_data = SEARCH_LABORATOIRE_RESSOURCE_search_data_Action | SEARCH_LABORATOIRE_RESSOURCE_search_data_LaboratoireRessource;

export interface SEARCH_LABORATOIRE_RESSOURCE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_LABORATOIRE_RESSOURCE_search_data | null)[] | null;
}

export interface SEARCH_LABORATOIRE_RESSOURCE {
  search: SEARCH_LABORATOIRE_RESSOURCE_search | null;
}

export interface SEARCH_LABORATOIRE_RESSOURCEVariables {
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
}
