/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: AideInfo
// ====================================================

export interface AideInfo {
  __typename: "Aide";
  id: string;
  title: string;
  description: string;
}
