import gql from 'graphql-tag';
import { AIDE_INFO } from './fragment';

export const DO_SEARCH_AIDES = gql`
  query SEARCH_AIDES($query: JSON, $skip: Int, $take: Int, $filterBy: JSON, $sortBy: JSON) {
    search(
      type: "aide"
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on Aide {
          ...AideInfo
        }
      }
    }
  }
  ${AIDE_INFO}
`;
