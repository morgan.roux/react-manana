/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GAMME_COMMERCIALS
// ====================================================

export interface GAMME_COMMERCIALS_canalGammes_sousGamme {
  __typename: "CanalSousGamme";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
  estDefaut: number | null;
  countCanalArticles: number | null;
}

export interface GAMME_COMMERCIALS_canalGammes {
  __typename: "CanalGamme";
  id: string;
  codeGamme: number | null;
  libelle: string | null;
  countCanalArticles: number | null;
  sousGamme: (GAMME_COMMERCIALS_canalGammes_sousGamme | null)[] | null;
}

export interface GAMME_COMMERCIALS {
  canalGammes: (GAMME_COMMERCIALS_canalGammes | null)[] | null;
}
