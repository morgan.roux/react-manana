/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: CanalSousGamme
// ====================================================

export interface CanalSousGamme_sousGamme {
  __typename: "CanalSousGamme";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
  estDefaut: number | null;
  countCanalArticles: number | null;
}

export interface CanalSousGamme {
  __typename: "CanalGamme";
  sousGamme: (CanalSousGamme_sousGamme | null)[] | null;
}
