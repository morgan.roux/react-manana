/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SearchCanalGammeInfo
// ====================================================

export interface SearchCanalGammeInfo_sousGamme {
  __typename: "CanalSousGamme";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
  countCanalArticles: number | null;
}

export interface SearchCanalGammeInfo {
  __typename: "CanalGamme";
  type: string;
  id: string;
  codeGamme: number | null;
  libelle: string | null;
  countCanalArticles: number | null;
  sousGamme: (SearchCanalGammeInfo_sousGamme | null)[] | null;
}
