import gql from 'graphql-tag';

export const CANAL_SOUS_GAMME_FRAGMENT = gql`
  fragment CanalSousGamme on CanalGamme {
    sousGamme {
      id
      codeSousGamme
      libelle
      estDefaut
      countCanalArticles
    }
  }
`;

export const GET_GAMME_COMMERCIAL = gql`
  query GAMME_COMMERCIAL($id: ID!) {
    canalGamme(id: $id) {
      id
      codeGamme
      libelle
      countCanalArticles
      ...CanalSousGamme
    }
    ${CANAL_SOUS_GAMME_FRAGMENT}
  }
`;

export const GET_GAMME_COMMERCIALS = gql`
  query GAMME_COMMERCIALS {
    canalGammes {
      id
      codeGamme
      libelle
      countCanalArticles
      sousGamme {
        id
        codeSousGamme
        libelle
        estDefaut
        countCanalArticles
      }
    }
  }
`;
