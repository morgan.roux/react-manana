import gql from 'graphql-tag';

export const TYPOLOGIE_INFO_FRAGMENT = gql`
  fragment TypologieInfo on Typologie {
    id
    code
    libelle
  }
`;
