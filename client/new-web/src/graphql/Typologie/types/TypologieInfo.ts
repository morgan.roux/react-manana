/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TypologieInfo
// ====================================================

export interface TypologieInfo {
  __typename: "Typologie";
  id: string;
  code: string | null;
  libelle: string | null;
}
