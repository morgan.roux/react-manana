/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: OperationArticleInfo
// ====================================================

export interface OperationArticleInfo_data_produitCanal_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
}

export interface OperationArticleInfo_data_produitCanal_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  nomOriginal: string;
}

export interface OperationArticleInfo_data_produitCanal_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: OperationArticleInfo_data_produitCanal_produit_produitPhoto_fichier | null;
}

export interface OperationArticleInfo_data_produitCanal_produit {
  __typename: "Produit";
  id: string;
  produitPhoto: OperationArticleInfo_data_produitCanal_produit_produitPhoto | null;
}

export interface OperationArticleInfo_data_produitCanal_articleSamePanachees {
  __typename: "ProduitCanal";
  id: string;
}

export interface OperationArticleInfo_data_produitCanal_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface OperationArticleInfo_data_produitCanal {
  __typename: "ProduitCanal";
  id: string;
  qteStock: number | null;
  stv: number | null;
  prixPhv: number | null;
  commandeCanal: OperationArticleInfo_data_produitCanal_commandeCanal | null;
  produit: OperationArticleInfo_data_produitCanal_produit | null;
  articleSamePanachees: (OperationArticleInfo_data_produitCanal_articleSamePanachees | null)[] | null;
  remises: (OperationArticleInfo_data_produitCanal_remises | null)[] | null;
}

export interface OperationArticleInfo_data {
  __typename: "OperationArticle";
  id: string;
  produitCanal: OperationArticleInfo_data_produitCanal | null;
  quantite: number | null;
}

export interface OperationArticleInfo {
  __typename: "OperationArticleResult";
  total: number;
  data: (OperationArticleInfo_data | null)[] | null;
}
