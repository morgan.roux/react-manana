/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum COCommandeOraleSortFields {
  cloturee = "cloturee",
  commentaire = "commentaire",
  createdAt = "createdAt",
  dateHeure = "dateHeure",
  designation = "designation",
  forme = "forme",
  id = "id",
  idGroupement = "idGroupement",
  idPassation = "idPassation",
  idPharmacie = "idPharmacie",
  idReception = "idReception",
  idUser = "idUser",
  quantite = "quantite",
  updatedAt = "updatedAt",
}

export enum COGroupeSortFields {
  createdAt = "createdAt",
  id = "id",
  idGroupement = "idGroupement",
  idPharmacie = "idPharmacie",
  nom = "nom",
  ordre = "ordre",
  updatedAt = "updatedAt",
}

export enum CanalSortFields {
  code = "code",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum DQActionOperationnelleSortFields {
  createdAt = "createdAt",
  dateAction = "dateAction",
  dateEcheance = "dateEcheance",
  description = "description",
  id = "id",
  idCause = "idCause",
  idFonction = "idFonction",
  idFonctionAInformer = "idFonctionAInformer",
  idGroupement = "idGroupement",
  idImportance = "idImportance",
  idOrigine = "idOrigine",
  idOrigineAssocie = "idOrigineAssocie",
  idPharmacie = "idPharmacie",
  idStatut = "idStatut",
  idTache = "idTache",
  idTacheAInformer = "idTacheAInformer",
  idType = "idType",
  idUrgence = "idUrgence",
  idUserAuteur = "idUserAuteur",
  idUserSuivi = "idUserSuivi",
  isPrivate = "isPrivate",
  updatedAt = "updatedAt",
}

export enum DQActiviteSortFields {
  createdAt = "createdAt",
  id = "id",
  idGroupement = "idGroupement",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum DQFicheAmeliorationActionSortFields {
  createdAt = "createdAt",
  dateHeureMiseEnplace = "dateHeureMiseEnplace",
  description = "description",
  id = "id",
  idGroupement = "idGroupement",
  idPharmacie = "idPharmacie",
  updatedAt = "updatedAt",
}

export enum DQFicheAmeliorationSortFields {
  createdAt = "createdAt",
  dateAmelioration = "dateAmelioration",
  dateEcheance = "dateEcheance",
  description = "description",
  id = "id",
  idAction = "idAction",
  idCause = "idCause",
  idFonction = "idFonction",
  idFonctionAInformer = "idFonctionAInformer",
  idGroupement = "idGroupement",
  idImportance = "idImportance",
  idOrigine = "idOrigine",
  idOrigineAssocie = "idOrigineAssocie",
  idPharmacie = "idPharmacie",
  idSolution = "idSolution",
  idStatut = "idStatut",
  idTache = "idTache",
  idTacheAInformer = "idTacheAInformer",
  idType = "idType",
  idUrgence = "idUrgence",
  idUserAuteur = "idUserAuteur",
  isPrivate = "isPrivate",
  updatedAt = "updatedAt",
}

export enum DQFicheCauseSortFields {
  code = "code",
  createdAt = "createdAt",
  createdBy = "createdBy",
  id = "id",
  libelle = "libelle",
  updatedAt = "updatedAt",
  updatedBy = "updatedBy",
}

export enum DQFicheIncidentSortFields {
  createdAt = "createdAt",
  dateEcheance = "dateEcheance",
  dateIncident = "dateIncident",
  description = "description",
  id = "id",
  idCause = "idCause",
  idFicheAmelioration = "idFicheAmelioration",
  idFonction = "idFonction",
  idFonctionAInformer = "idFonctionAInformer",
  idGroupement = "idGroupement",
  idImportance = "idImportance",
  idOrigine = "idOrigine",
  idOrigineAssocie = "idOrigineAssocie",
  idPharmacie = "idPharmacie",
  idSolution = "idSolution",
  idStatut = "idStatut",
  idTache = "idTache",
  idTacheAInformer = "idTacheAInformer",
  idTicketReclamation = "idTicketReclamation",
  idType = "idType",
  idUrgence = "idUrgence",
  idUserDeclarant = "idUserDeclarant",
  isPrivate = "isPrivate",
  updatedAt = "updatedAt",
}

export enum DQFicheTypeSortFields {
  code = "code",
  couleur = "couleur",
  createdAt = "createdAt",
  createdBy = "createdBy",
  id = "id",
  libelle = "libelle",
  updatedAt = "updatedAt",
  updatedBy = "updatedBy",
}

export enum DQMTFonctionSortFields {
  createdAt = "createdAt",
  id = "id",
  idGroupement = "idGroupement",
  idPharmacie = "idPharmacie",
  libelle = "libelle",
  ordre = "ordre",
  updatedAt = "updatedAt",
}

export enum DQMTResponsableTypeSortFields {
  code = "code",
  couleur = "couleur",
  createdAt = "createdAt",
  id = "id",
  idGroupement = "idGroupement",
  idPharmacie = "idPharmacie",
  libelle = "libelle",
  max = "max",
  ordre = "ordre",
  updatedAt = "updatedAt",
}

export enum DQMTTacheResponsableSortFields {
  createdAt = "createdAt",
  id = "id",
  idFonction = "idFonction",
  idGroupement = "idGroupement",
  idPharmacie = "idPharmacie",
  idTache = "idTache",
  idType = "idType",
  idUser = "idUser",
  updatedAt = "updatedAt",
}

export enum DQPerceptionSortFields {
  createdAt = "createdAt",
  id = "id",
  idGroupement = "idGroupement",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum DQReunionActionSortFields {
  createdAt = "createdAt",
  id = "id",
  idGroupement = "idGroupement",
  idPharmacie = "idPharmacie",
  idReunion = "idReunion",
  idTodoAction = "idTodoAction",
  idTypeAssocie = "idTypeAssocie",
  type = "type",
  updatedAt = "updatedAt",
}

export enum DQReunionSortFields {
  createdAt = "createdAt",
  description = "description",
  id = "id",
  idPharmacie = "idPharmacie",
  idResponsable = "idResponsable",
  idUserAnimateur = "idUserAnimateur",
  updatedAt = "updatedAt",
}

export enum DQStatutSortFields {
  code = "code",
  createdAt = "createdAt",
  id = "id",
  idGroupement = "idGroupement",
  libelle = "libelle",
  type = "type",
  updatedAt = "updatedAt",
}

export enum GedCategorieSortFields {
  base = "base",
  createdAt = "createdAt",
  id = "id",
  idGroupement = "idGroupement",
  idPartenaireValidateur = "idPartenaireValidateur",
  idPharmacie = "idPharmacie",
  libelle = "libelle",
  type = "type",
  updatedAt = "updatedAt",
  validation = "validation",
  workflowValidation = "workflowValidation",
}

export enum GedDocumentChangementStatutSortFields {
  commentaire = "commentaire",
  createdAt = "createdAt",
  id = "id",
  idDocument = "idDocument",
  idGroupement = "idGroupement",
  status = "status",
  updatedAt = "updatedAt",
}

export enum GedDocumentSortFields {
  createdAt = "createdAt",
  dateHeureDebutValidite = "dateHeureDebutValidite",
  dateHeureFinValidite = "dateHeureFinValidite",
  dateHeureParution = "dateHeureParution",
  description = "description",
  factureDate = "factureDate",
  factureDateReglement = "factureDateReglement",
  factureTotalHt = "factureTotalHt",
  factureTotalTtc = "factureTotalTtc",
  factureTva = "factureTva",
  id = "id",
  idClientRapprochementAttribut = "idClientRapprochementAttribut",
  idCommande = "idCommande",
  idDocumentARemplacer = "idDocumentARemplacer",
  idFichier = "idFichier",
  idFournisseurRapprochementAttribut = "idFournisseurRapprochementAttribut",
  idGroupement = "idGroupement",
  idOrigine = "idOrigine",
  idOrigineAssocie = "idOrigineAssocie",
  idPharmacie = "idPharmacie",
  idReglementMode = "idReglementMode",
  idSousCategorie = "idSousCategorie",
  idUserRedacteur = "idUserRedacteur",
  idUserVerificateur = "idUserVerificateur",
  isGenererCommande = "isGenererCommande",
  isOcr = "isOcr",
  motCle1 = "motCle1",
  motCle2 = "motCle2",
  motCle3 = "motCle3",
  nomenclature = "nomenclature",
  numeroCommande = "numeroCommande",
  numeroFacture = "numeroFacture",
  numeroVersion = "numeroVersion",
  source = "source",
  type = "type",
  updatedAt = "updatedAt",
}

export enum ImportanceSortFields {
  couleur = "couleur",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  ordre = "ordre",
  supprime = "supprime",
  updatedAt = "updatedAt",
}

export enum InformationLiaisonTypeSortFields {
  code = "code",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  supprime = "supprime",
  updatedAt = "updatedAt",
}

export enum ItemScoringSortFields {
  coefficientAppetance = "coefficientAppetance",
  coefficientImportance = "coefficientImportance",
  coefficientUrgence = "coefficientUrgence",
  dateEcheance = "dateEcheance",
  description = "description",
  id = "id",
  idImportance = "idImportance",
  idItem = "idItem",
  idItemAssocie = "idItemAssocie",
  idPharmacie = "idPharmacie",
  idUrgence = "idUrgence",
  idUser = "idUser",
  nomItem = "nomItem",
  nombreJoursRetard = "nombreJoursRetard",
  ordreStatut = "ordreStatut",
  score = "score",
  statut = "statut",
}

export enum ItemSortFields {
  code = "code",
  codeItem = "codeItem",
  createdAt = "createdAt",
  id = "id",
  idParameterGroupement = "idParameterGroupement",
  idParameterPharmacie = "idParameterPharmacie",
  name = "name",
  updatedAt = "updatedAt",
}

export enum LaboratoireSortFields {
  createdAt = "createdAt",
  id = "id",
  idLaboSuite = "idLaboSuite",
  idLaboratoireRattachement = "idLaboratoireRattachement",
  idPhoto = "idPhoto",
  idUser = "idUser",
  nom = "nom",
  sortie = "sortie",
  supprime = "supprime",
  updatedAt = "updatedAt",
}

export enum OptionPartageSortFields {
  codeMaj = "codeMaj",
  createdAt = "createdAt",
  createdBy = "createdBy",
  id = "id",
  idGroupement = "idGroupement",
  idParametre = "idParametre",
  idPharmacie = "idPharmacie",
  idType = "idType",
  updatedAt = "updatedAt",
  updatedBy = "updatedBy",
}

export enum OptionPartageTypeSortFields {
  code = "code",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum OrigineSortFields {
  code = "code",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum PRTAnalyseSortFields {
  createdAt = "createdAt",
  dateChargement = "dateChargement",
  dateDebut = "dateDebut",
  dateFin = "dateFin",
  id = "id",
  idPartenaireTypeAssocie = "idPartenaireTypeAssocie",
  idPharmacie = "idPharmacie",
  idType = "idType",
  partenaireType = "partenaireType",
  titre = "titre",
  updatedAt = "updatedAt",
}

export enum PRTAnalyseTypeSortFields {
  code = "code",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum PRTCommandeSortFields {
  ValeurFrancoPort = "ValeurFrancoPort",
  commentaireExterne = "commentaireExterne",
  commentaireInterne = "commentaireInterne",
  createdAt = "createdAt",
  dateCommande = "dateCommande",
  fraisPort = "fraisPort",
  id = "id",
  idCanal = "idCanal",
  idCommandeType = "idCommandeType",
  idGroupement = "idGroupement",
  idLaboratoire = "idLaboratoire",
  idOperation = "idOperation",
  idPharmacie = "idPharmacie",
  idPublicite = "idPublicite",
  idUser = "idUser",
  nbrRef = "nbrRef",
  pathFile = "pathFile",
  prixBaseTotalHT = "prixBaseTotalHT",
  prixNetTotalHT = "prixNetTotalHT",
  prixNetTotalTTC = "prixNetTotalTTC",
  quantite = "quantite",
  remiseGlobale = "remiseGlobale",
  source = "source",
  statut = "statut",
  totalTVA = "totalTVA",
  uniteGratuite = "uniteGratuite",
  updatedAt = "updatedAt",
  valeurRemiseTotal = "valeurRemiseTotal",
}

export enum PRTCommandeStatutSortFields {
  code = "code",
  createdAt = "createdAt",
  id = "id",
  idGroupement = "idGroupement",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum PRTCompteRenduSortFields {
  codeMaj = "codeMaj",
  conclusion = "conclusion",
  createdAt = "createdAt",
  createdBy = "createdBy",
  gestionPerime = "gestionPerime",
  id = "id",
  idGroupement = "idGroupement",
  idPartenaireTypeAssocie = "idPartenaireTypeAssocie",
  idPharmacie = "idPharmacie",
  partenaireType = "partenaireType",
  rapportVisite = "rapportVisite",
  remiseEchantillon = "remiseEchantillon",
  titre = "titre",
  updatedAt = "updatedAt",
  updatedBy = "updatedBy",
}

export enum PRTConditionCommercialeSortFields {
  createdAt = "createdAt",
  dateDebut = "dateDebut",
  dateFin = "dateFin",
  description = "description",
  id = "id",
  idCanal = "idCanal",
  idGroupement = "idGroupement",
  idPartenaireTypeAssocie = "idPartenaireTypeAssocie",
  idPharmacie = "idPharmacie",
  idStatut = "idStatut",
  idType = "idType",
  partenaireType = "partenaireType",
  titre = "titre",
  updatedAt = "updatedAt",
}

export enum PRTConditionCommercialeStatutSortFields {
  code = "code",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum PRTConditionCommercialeTypeSortFields {
  code = "code",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum PRTContactSortFields {
  civilite = "civilite",
  codeMaj = "codeMaj",
  createdAt = "createdAt",
  createdBy = "createdBy",
  fonction = "fonction",
  id = "id",
  idAvatar = "idAvatar",
  idContact = "idContact",
  idGroupement = "idGroupement",
  idPartenaireTypeAssocie = "idPartenaireTypeAssocie",
  idPharmacie = "idPharmacie",
  idPhoto = "idPhoto",
  nom = "nom",
  partenaireType = "partenaireType",
  prenom = "prenom",
  supprime = "supprime",
  updatedAt = "updatedAt",
  updatedBy = "updatedBy",
}

export enum PRTMiseAvantSortFields {
  code = "code",
  couleur = "couleur",
  createdAt = "createdAt",
  id = "id",
  idPharmacie = "idPharmacie",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum PRTPartenariatStatutSortFields {
  code = "code",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum PRTPartenariatTypeSortFields {
  code = "code",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum PRTPlanMarketingSortFields {
  createdAt = "createdAt",
  dateDebut = "dateDebut",
  dateFin = "dateFin",
  description = "description",
  id = "id",
  idGroupement = "idGroupement",
  idPartenaireTypeAssocie = "idPartenaireTypeAssocie",
  idPharmacie = "idPharmacie",
  idStatut = "idStatut",
  idType = "idType",
  partenaireType = "partenaireType",
  titre = "titre",
  updatedAt = "updatedAt",
}

export enum PRTPlanMarketingStatutSortFields {
  code = "code",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum PRTPlanMarketingTypeActionSortFields {
  createdAt = "createdAt",
  description = "description",
  id = "id",
  idFonction = "idFonction",
  idGroupement = "idGroupement",
  idImportance = "idImportance",
  idPharmacie = "idPharmacie",
  idPlanMarketingType = "idPlanMarketingType",
  idTache = "idTache",
  jourLancement = "jourLancement",
  updatedAt = "updatedAt",
}

export enum PRTPlanMarketingTypeSortFields {
  code = "code",
  couleur = "couleur",
  createdAt = "createdAt",
  id = "id",
  idPharmacie = "idPharmacie",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum PRTReglementModeSortFields {
  code = "code",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum PRTRemunerationReglementSortFields {
  createdAt = "createdAt",
  createdBy = "createdBy",
  dateReglement = "dateReglement",
  description = "description",
  id = "id",
  idGroupement = "idGroupement",
  idModeReglement = "idModeReglement",
  idPartenaireTypeAssocie = "idPartenaireTypeAssocie",
  idPharmacie = "idPharmacie",
  idPrestationType = "idPrestationType",
  idRemuneration = "idRemuneration",
  montant = "montant",
  partenaireType = "partenaireType",
  updatedAt = "updatedAt",
  updatedBy = "updatedBy",
}

export enum PRTRemunerationSortFields {
  createdAt = "createdAt",
  createdBy = "createdBy",
  dateEcheance = "dateEcheance",
  id = "id",
  idGroupement = "idGroupement",
  idPartenaireTypeAssocie = "idPartenaireTypeAssocie",
  idPharmacie = "idPharmacie",
  idPrestationType = "idPrestationType",
  montantPrevu = "montantPrevu",
  montantRealise = "montantRealise",
  partenaireType = "partenaireType",
  updatedAt = "updatedAt",
  updatedBy = "updatedBy",
}

export enum PRTRendezVousSortFields {
  createdAt = "createdAt",
  createdBy = "createdBy",
  dateRendezVous = "dateRendezVous",
  heureDebut = "heureDebut",
  heureFin = "heureFin",
  id = "id",
  idGroupement = "idGroupement",
  idPartenaireTypeAssocie = "idPartenaireTypeAssocie",
  idPharmacie = "idPharmacie",
  idSubject = "idSubject",
  note = "note",
  ordreJour = "ordreJour",
  partenaireType = "partenaireType",
  statut = "statut",
  updatedAt = "updatedAt",
  updatedBy = "updatedBy",
}

export enum PRTRendezVousSujetVisiteSortFields {
  code = "code",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum PRTSuiviOperationnelSortFields {
  createdAt = "createdAt",
  createdBy = "createdBy",
  dateHeure = "dateHeure",
  description = "description",
  id = "id",
  idFonction = "idFonction",
  idGroupement = "idGroupement",
  idImportance = "idImportance",
  idPharmacie = "idPharmacie",
  idStatut = "idStatut",
  idTache = "idTache",
  idType = "idType",
  idTypeAssocie = "idTypeAssocie",
  montant = "montant",
  partenaireType = "partenaireType",
  titre = "titre",
  updatedAt = "updatedAt",
}

export enum PRTSuiviOperationnelStatutSortFields {
  code = "code",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum PRTSuiviOperationnelTypeSortFields {
  code = "code",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum PrestationTypeSortFields {
  code = "code",
  couleur = "couleur",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum RTCapteurSortFields {
  createdAt = "createdAt",
  id = "id",
  idPharmacie = "idPharmacie",
  idPointAcces = "idPointAcces",
  nom = "nom",
  numeroSerie = "numeroSerie",
  updatedAt = "updatedAt",
}

export enum RTFrigoSortFields {
  createdAt = "createdAt",
  id = "id",
  idPharmacie = "idPharmacie",
  nom = "nom",
  temperatureBasse = "temperatureBasse",
  temperatureHaute = "temperatureHaute",
  temporisation = "temporisation",
  typeReleve = "typeReleve",
  updatedAt = "updatedAt",
}

export enum RTPointAccesSortFields {
  createdAt = "createdAt",
  id = "id",
  idPharmacie = "idPharmacie",
  nom = "nom",
  numeroSerie = "numeroSerie",
  updatedAt = "updatedAt",
}

export enum RTReleveSortFields {
  createdAt = "createdAt",
  id = "id",
  idCapteur = "idCapteur",
  idFrigo = "idFrigo",
  idPointAcces = "idPointAcces",
  idStatus = "idStatus",
  idUser = "idUser",
  temperature = "temperature",
  type = "type",
  updatedAt = "updatedAt",
}

export enum RTStatusSortFields {
  code = "code",
  couleur = "couleur",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

/**
 * Sort Directions
 */
export enum SortDirection {
  ASC = "ASC",
  DESC = "DESC",
}

/**
 * Sort Nulls Options
 */
export enum SortNulls {
  NULLS_FIRST = "NULLS_FIRST",
  NULLS_LAST = "NULLS_LAST",
}

export enum SuiviAppelTypeSortFields {
  commentaire = "commentaire",
  createdAt = "createdAt",
  id = "id",
  idFonction = "idFonction",
  idGroupement = "idGroupement",
  idImportance = "idImportance",
  idPharmacie = "idPharmacie",
  idTache = "idTache",
  idUrgence = "idUrgence",
  idUserDeclarant = "idUserDeclarant",
  idUserInterlocuteur = "idUserInterlocuteur",
  status = "status",
  updatedAt = "updatedAt",
}

export enum TATraitementExecutionSortFields {
  createdAt = "createdAt",
  dateEcheance = "dateEcheance",
  id = "id",
  idPharmacie = "idPharmacie",
  idTraitement = "idTraitement",
  updatedAt = "updatedAt",
}

export enum TATraitementSortFields {
  createdAt = "createdAt",
  dateDebut = "dateDebut",
  dateHeureFin = "dateHeureFin",
  description = "description",
  heure = "heure",
  id = "id",
  idFonction = "idFonction",
  idImportance = "idImportance",
  idPharmacie = "idPharmacie",
  idTache = "idTache",
  idType = "idType",
  idUrgence = "idUrgence",
  isPrivate = "isPrivate",
  jourMois = "jourMois",
  moisAnnee = "moisAnnee",
  nombreOccurencesFin = "nombreOccurencesFin",
  repetition = "repetition",
  repetitionUnite = "repetitionUnite",
  updatedAt = "updatedAt",
}

export enum TATraitementTypeSortFields {
  code = "code",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  updatedAt = "updatedAt",
}

export enum TodoActionSortFields {
  createdAt = "createdAt",
  dateDebut = "dateDebut",
  dateFin = "dateFin",
  description = "description",
  id = "id",
  idActionParent = "idActionParent",
  idImportance = "idImportance",
  idItemAssocie = "idItemAssocie",
  ordre = "ordre",
  status = "status",
  supprime = "supprime",
  updatedAt = "updatedAt",
}

export enum UrgenceSortFields {
  code = "code",
  couleur = "couleur",
  createdAt = "createdAt",
  id = "id",
  libelle = "libelle",
  supprime = "supprime",
  updatedAt = "updatedAt",
}

export interface ActiviteInput {
  activiteTypeCode: string;
  itemCode: string;
  idItemAssocie: string;
  log?: string | null;
}

export interface AppetenceInput {
  idItem: string;
  ordre: number;
}

export interface AttachmentInput {
  content: string;
  filename: string;
  type: string;
}

export interface BooleanFieldComparison {
  is?: boolean | null;
  isNot?: boolean | null;
}

export interface COCommandeOraleAggregateFilter {
  and?: COCommandeOraleAggregateFilter[] | null;
  or?: COCommandeOraleAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  idUser?: StringFieldComparison | null;
  designation?: StringFieldComparison | null;
  quantite?: IntFieldComparison | null;
  forme?: StringFieldComparison | null;
  dateHeure?: DateFieldComparison | null;
  cloturee?: BooleanFieldComparison | null;
  commentaire?: StringFieldComparison | null;
  idPassation?: StringFieldComparison | null;
  idReception?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface COCommandeOraleFilter {
  and?: COCommandeOraleFilter[] | null;
  or?: COCommandeOraleFilter[] | null;
  id?: IDFilterComparison | null;
  idUser?: StringFieldComparison | null;
  designation?: StringFieldComparison | null;
  quantite?: IntFieldComparison | null;
  forme?: StringFieldComparison | null;
  dateHeure?: DateFieldComparison | null;
  cloturee?: BooleanFieldComparison | null;
  commentaire?: StringFieldComparison | null;
  idPassation?: StringFieldComparison | null;
  idReception?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface COCommandeOraleInput {
  designation: string;
  idUser: string;
  quantite: number;
  dateHeure: any;
  forme: string;
  commentaire?: string | null;
  cloturee: boolean;
}

export interface COCommandeOraleSort {
  field: COCommandeOraleSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface COGroupeFilter {
  and?: COGroupeFilter[] | null;
  or?: COGroupeFilter[] | null;
  id?: IDFilterComparison | null;
  nom?: StringFieldComparison | null;
  ordre?: NumberFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface COGroupeInput {
  nom: string;
  ordre: number;
}

export interface COGroupeSort {
  field: COGroupeSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface COPassationInput {
  idUser: string;
  idCommandeOrale: string;
  idSourceAppro: string;
  dateHeure: any;
  commentaire?: string | null;
}

export interface COReceptionInput {
  idUser: string;
  idCommandeOrale: string;
  dateHeure: any;
  commentaire?: string | null;
}

export interface COSourceApproAttributInput {
  identifiant: string;
  valeur: string;
}

export interface COSourceApproInput {
  nom: string;
  ordre: number;
  tel: string;
  attributs: COSourceApproAttributInput[];
  commentaire?: string | null;
  idGroupeSourceAppro: string;
}

export interface CanalFilter {
  and?: CanalFilter[] | null;
  or?: CanalFilter[] | null;
  id?: IDFilterComparison | null;
  libelle?: StringFieldComparison | null;
  code?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface CanalSort {
  field: CanalSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface ClientInput {
  nom: string;
  prenom: string;
  pays: string;
  adresse: string;
  tel: string;
  ville: string;
  email: string;
  cp: string;
}

export interface ContactInputs {
  adresse1?: string | null;
  adresse2?: string | null;
  cp?: string | null;
  ville?: string | null;
  pays?: string | null;
  telProf?: string | null;
  telPerso?: string | null;
  telMobProf?: string | null;
  telMobPerso?: string | null;
  faxProf?: string | null;
  faxPerso?: string | null;
  mailProf?: string | null;
  mailPerso?: string | null;
  sitePerso?: string | null;
  siteProf?: string | null;
  compteSkypeProf?: string | null;
  compteSkypePerso?: string | null;
  urlLinkedInProf?: string | null;
  urlLinkedInPerso?: string | null;
  urlTwitterPerso?: string | null;
  urlTwitterProf?: string | null;
  urlFacebookProf?: string | null;
  urlFacebookPerso?: string | null;
  codeMaj?: string | null;
  urlMessenger?: string | null;
  urlYoutube?: string | null;
  whatsappMobProf?: string | null;
  whatsappMobPerso?: string | null;
}

export interface CreateManyItemScoringsInput {
  itemScorings: ItemScoringInput[];
}

export interface CreateOneDQMTFonctionInput {
  dQMTFonction: DQMTFonctionInput;
}

export interface CreateOneDQMTTacheInput {
  dQMTTache: DQMTTacheInput;
}

export interface CreateOneItemScoringInput {
  itemScoring: ItemScoringInput;
}

export interface CreateOneOrigineInput {
  origine: OrigineInput;
}

export interface CreateOnePRTCommandeInput {
  pRTCommande: PRTCommandeInput;
}

export interface CreateOnePRTMiseAvantInput {
  pRTMiseAvant: PRTMiseAvantInput;
}

export interface CreateOnePRTPlanMarketingTypeInput {
  pRTPlanMarketingType: PRTPlanMarketingTypeInput;
}

export interface DQActionOperationnelleFilter {
  and?: DQActionOperationnelleFilter[] | null;
  or?: DQActionOperationnelleFilter[] | null;
  id?: IDFilterComparison | null;
  description?: StringFieldComparison | null;
  idStatut?: StringFieldComparison | null;
  idUserAuteur?: StringFieldComparison | null;
  idUserSuivi?: StringFieldComparison | null;
  idUrgence?: StringFieldComparison | null;
  idImportance?: StringFieldComparison | null;
  idFonction?: StringFieldComparison | null;
  idTache?: StringFieldComparison | null;
  idFonctionAInformer?: StringFieldComparison | null;
  idTacheAInformer?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idType?: StringFieldComparison | null;
  idCause?: StringFieldComparison | null;
  dateAction?: DateFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idOrigine?: StringFieldComparison | null;
  idOrigineAssocie?: StringFieldComparison | null;
  isPrivate?: BooleanFieldComparison | null;
  dateEcheance?: DateFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface DQActionOperationnelleInput {
  idStatut?: string | null;
  idUserAuteur: string;
  idUserSuivi: string;
  description: string;
  idType: string;
  idCause: string;
  dateAction: any;
  dateEcheance: any;
  idImportance: string;
  idFonction?: string | null;
  idTache?: string | null;
  idFonctionAInformer?: string | null;
  idTacheAInformer?: string | null;
  idUserParticipants?: DQUserParticipantInput[] | null;
  fichiers?: FichierInput[] | null;
  idOrigine: string;
  idOrigineAssocie?: string | null;
  isPrivate: boolean;
}

export interface DQActionOperationnelleSort {
  field: DQActionOperationnelleSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface DQActiviteFilter {
  and?: DQActiviteFilter[] | null;
  or?: DQActiviteFilter[] | null;
  id?: IDFilterComparison | null;
  libelle?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface DQActiviteSort {
  field: DQActiviteSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface DQChecklistEvaluationClotureInput {
  commentaire?: string | null;
  fichiers?: (FichierInput | null)[] | null;
}

export interface DQChecklistEvaluationInput {
  ordre: number;
  libelle: string;
}

export interface DQChecklistInput {
  ordre: number;
  libelle: string;
  idPharmacie?: string | null;
  sections?: (DQChecklistSectionInput | null)[] | null;
}

export interface DQChecklistSectionInput {
  id?: string | null;
  ordre: number;
  libelle: string;
  items?: (DQChecklistSectionItemInput | null)[] | null;
}

export interface DQChecklistSectionItemInput {
  id?: string | null;
  ordre: number;
  libelle: string;
}

export interface DQExigenceInput {
  ordre?: number | null;
  titre?: string | null;
  finalite?: string | null;
  exemples?: (DQSousExempleInput | null)[] | null;
  questions?: (DQSousQuestionInput | null)[] | null;
  outils?: (DQSousThemeOutilInput | null)[] | null;
}

export interface DQFicheAmeliorationActionFilter {
  and?: DQFicheAmeliorationActionFilter[] | null;
  or?: DQFicheAmeliorationActionFilter[] | null;
  id?: IDFilterComparison | null;
  description?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  dateHeureMiseEnplace?: DateFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface DQFicheAmeliorationActionInput {
  description: string;
  dateHeureMiseEnplace: any;
  fichiers: FichierInput[];
}

export interface DQFicheAmeliorationActionSort {
  field: DQFicheAmeliorationActionSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface DQFicheAmeliorationAggregateFilter {
  and?: DQFicheAmeliorationAggregateFilter[] | null;
  or?: DQFicheAmeliorationAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  description?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idType?: StringFieldComparison | null;
  idCause?: StringFieldComparison | null;
  dateAmelioration?: DateFieldComparison | null;
  idUrgence?: StringFieldComparison | null;
  idImportance?: StringFieldComparison | null;
  idFonction?: StringFieldComparison | null;
  idTache?: StringFieldComparison | null;
  idFonctionAInformer?: StringFieldComparison | null;
  idTacheAInformer?: StringFieldComparison | null;
  idAction?: StringFieldComparison | null;
  dateEcheance?: DateFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
  idOrigine?: StringFieldComparison | null;
  idOrigineAssocie?: StringFieldComparison | null;
  idSolution?: StringFieldComparison | null;
  idUserAuteur?: StringFieldComparison | null;
  idStatut?: StringFieldComparison | null;
  isPrivate?: BooleanFieldComparison | null;
}

export interface DQFicheAmeliorationDeleteFilter {
  and?: DQFicheAmeliorationDeleteFilter[] | null;
  or?: DQFicheAmeliorationDeleteFilter[] | null;
  id?: IDFilterComparison | null;
  description?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idType?: StringFieldComparison | null;
  idCause?: StringFieldComparison | null;
  dateAmelioration?: DateFieldComparison | null;
  idUrgence?: StringFieldComparison | null;
  idImportance?: StringFieldComparison | null;
  idFonction?: StringFieldComparison | null;
  idTache?: StringFieldComparison | null;
  idFonctionAInformer?: StringFieldComparison | null;
  idTacheAInformer?: StringFieldComparison | null;
  idAction?: StringFieldComparison | null;
  dateEcheance?: DateFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
  idOrigine?: StringFieldComparison | null;
  idOrigineAssocie?: StringFieldComparison | null;
  idSolution?: StringFieldComparison | null;
  idUserAuteur?: StringFieldComparison | null;
  idStatut?: StringFieldComparison | null;
  isPrivate?: BooleanFieldComparison | null;
}

export interface DQFicheAmeliorationFilter {
  and?: DQFicheAmeliorationFilter[] | null;
  or?: DQFicheAmeliorationFilter[] | null;
  id?: IDFilterComparison | null;
  description?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idType?: StringFieldComparison | null;
  idCause?: StringFieldComparison | null;
  dateAmelioration?: DateFieldComparison | null;
  idUrgence?: StringFieldComparison | null;
  idImportance?: StringFieldComparison | null;
  idFonction?: StringFieldComparison | null;
  idTache?: StringFieldComparison | null;
  idFonctionAInformer?: StringFieldComparison | null;
  idTacheAInformer?: StringFieldComparison | null;
  idAction?: StringFieldComparison | null;
  dateEcheance?: DateFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
  idOrigine?: StringFieldComparison | null;
  idOrigineAssocie?: StringFieldComparison | null;
  idSolution?: StringFieldComparison | null;
  idUserAuteur?: StringFieldComparison | null;
  idStatut?: StringFieldComparison | null;
  isPrivate?: BooleanFieldComparison | null;
}

export interface DQFicheAmeliorationInput {
  description: string;
  idStatut?: string | null;
  idOrigine?: string | null;
  idOrigineAssocie?: string | null;
  idType: string;
  idCause: string;
  dateAmelioration: any;
  dateEcheance: any;
  idUserAuteur: string;
  fichiers?: FichierInput[] | null;
  idUserParticipants?: DQUserParticipantInput[] | null;
  idImportance: string;
  idFonction?: string | null;
  idTache?: string | null;
  idFonctionAInformer?: string | null;
  idTacheAInformer?: string | null;
  isPrivate: boolean;
}

export interface DQFicheAmeliorationSort {
  field: DQFicheAmeliorationSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface DQFicheCauseFilter {
  and?: DQFicheCauseFilter[] | null;
  or?: DQFicheCauseFilter[] | null;
  id?: IDFilterComparison | null;
  code?: StringFieldComparison | null;
  libelle?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
  updatedBy?: StringFieldComparison | null;
  createdBy?: StringFieldComparison | null;
}

export interface DQFicheCauseSort {
  field: DQFicheCauseSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface DQFicheIncidentAggregateFilter {
  and?: DQFicheIncidentAggregateFilter[] | null;
  or?: DQFicheIncidentAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  description?: StringFieldComparison | null;
  idFicheAmelioration?: StringFieldComparison | null;
  idTicketReclamation?: StringFieldComparison | null;
  idType?: StringFieldComparison | null;
  idCause?: StringFieldComparison | null;
  dateIncident?: DateFieldComparison | null;
  idUserDeclarant?: StringFieldComparison | null;
  idOrigine?: StringFieldComparison | null;
  idOrigineAssocie?: StringFieldComparison | null;
  idStatut?: StringFieldComparison | null;
  idSolution?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idUrgence?: StringFieldComparison | null;
  idImportance?: StringFieldComparison | null;
  idFonction?: StringFieldComparison | null;
  idTache?: StringFieldComparison | null;
  idFonctionAInformer?: StringFieldComparison | null;
  idTacheAInformer?: StringFieldComparison | null;
  dateEcheance?: DateFieldComparison | null;
  isPrivate?: BooleanFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface DQFicheIncidentDeleteFilter {
  and?: DQFicheIncidentDeleteFilter[] | null;
  or?: DQFicheIncidentDeleteFilter[] | null;
  id?: IDFilterComparison | null;
  description?: StringFieldComparison | null;
  idFicheAmelioration?: StringFieldComparison | null;
  idTicketReclamation?: StringFieldComparison | null;
  idType?: StringFieldComparison | null;
  idCause?: StringFieldComparison | null;
  dateIncident?: DateFieldComparison | null;
  idUserDeclarant?: StringFieldComparison | null;
  idOrigine?: StringFieldComparison | null;
  idOrigineAssocie?: StringFieldComparison | null;
  idStatut?: StringFieldComparison | null;
  idSolution?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idUrgence?: StringFieldComparison | null;
  idImportance?: StringFieldComparison | null;
  idFonction?: StringFieldComparison | null;
  idTache?: StringFieldComparison | null;
  idFonctionAInformer?: StringFieldComparison | null;
  idTacheAInformer?: StringFieldComparison | null;
  dateEcheance?: DateFieldComparison | null;
  isPrivate?: BooleanFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface DQFicheIncidentFilter {
  and?: DQFicheIncidentFilter[] | null;
  or?: DQFicheIncidentFilter[] | null;
  id?: IDFilterComparison | null;
  description?: StringFieldComparison | null;
  idFicheAmelioration?: StringFieldComparison | null;
  idTicketReclamation?: StringFieldComparison | null;
  idType?: StringFieldComparison | null;
  idCause?: StringFieldComparison | null;
  dateIncident?: DateFieldComparison | null;
  idUserDeclarant?: StringFieldComparison | null;
  idOrigine?: StringFieldComparison | null;
  idOrigineAssocie?: StringFieldComparison | null;
  idStatut?: StringFieldComparison | null;
  idSolution?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idUrgence?: StringFieldComparison | null;
  idImportance?: StringFieldComparison | null;
  idFonction?: StringFieldComparison | null;
  idTache?: StringFieldComparison | null;
  idFonctionAInformer?: StringFieldComparison | null;
  idTacheAInformer?: StringFieldComparison | null;
  dateEcheance?: DateFieldComparison | null;
  isPrivate?: BooleanFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface DQFicheIncidentInput {
  description: string;
  idOrigine: string;
  idOrigineAssocie?: string | null;
  idStatut?: string | null;
  idUserDeclarant: string;
  idType: string;
  idCause: string;
  dateIncident: any;
  dateEcheance: any;
  fichiers?: FichierInput[] | null;
  idImportance: string;
  idFonction?: string | null;
  idTache?: string | null;
  idFonctionAInformer?: string | null;
  idTacheAInformer?: string | null;
  idUserParticipants?: DQUserParticipantInput[] | null;
  isPrivate: boolean;
}

export interface DQFicheIncidentSort {
  field: DQFicheIncidentSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface DQFicheTypeFilter {
  and?: DQFicheTypeFilter[] | null;
  or?: DQFicheTypeFilter[] | null;
  id?: IDFilterComparison | null;
  code?: StringFieldComparison | null;
  libelle?: StringFieldComparison | null;
  couleur?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
  updatedBy?: StringFieldComparison | null;
  createdBy?: StringFieldComparison | null;
}

export interface DQFicheTypeSort {
  field: DQFicheTypeSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface DQMTFonctionFilter {
  and?: DQMTFonctionFilter[] | null;
  or?: DQMTFonctionFilter[] | null;
  id?: IDFilterComparison | null;
  ordre?: IntFieldComparison | null;
  libelle?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface DQMTFonctionInput {
  ordre: number;
  libelle: string;
}

export interface DQMTFonctionSort {
  field: DQMTFonctionSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface DQMTResponsableTypeFilter {
  and?: DQMTResponsableTypeFilter[] | null;
  or?: DQMTResponsableTypeFilter[] | null;
  id?: IDFilterComparison | null;
  ordre?: IntFieldComparison | null;
  max?: IntFieldComparison | null;
  code?: StringFieldComparison | null;
  couleur?: StringFieldComparison | null;
  libelle?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface DQMTResponsableTypeSort {
  field: DQMTResponsableTypeSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface DQMTTacheInput {
  idFonction: string;
  ordre: number;
  libelle: string;
}

export interface DQMTTacheResponsableFilter {
  and?: DQMTTacheResponsableFilter[] | null;
  or?: DQMTTacheResponsableFilter[] | null;
  id?: IDFilterComparison | null;
  idType?: StringFieldComparison | null;
  idUser?: StringFieldComparison | null;
  idFonction?: StringFieldComparison | null;
  idTache?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface DQMTTacheResponsableInput {
  idFonction: string;
  idTache?: string | null;
  idType: string;
}

export interface DQMTTacheResponsableSort {
  field: DQMTTacheResponsableSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface DQOutilInput {
  ordre: number;
  libelle: string;
  fichier?: FichierInput | null;
}

export interface DQPerceptionFilter {
  and?: DQPerceptionFilter[] | null;
  or?: DQPerceptionFilter[] | null;
  id?: IDFilterComparison | null;
  libelle?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface DQPerceptionSort {
  field: DQPerceptionSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface DQReunionActionAggregateFilter {
  and?: DQReunionActionAggregateFilter[] | null;
  or?: DQReunionActionAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  type?: StringFieldComparison | null;
  idTypeAssocie?: StringFieldComparison | null;
  idTodoAction?: StringFieldComparison | null;
  idReunion?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface DQReunionActionFilter {
  and?: DQReunionActionFilter[] | null;
  or?: DQReunionActionFilter[] | null;
  id?: IDFilterComparison | null;
  type?: StringFieldComparison | null;
  idTypeAssocie?: StringFieldComparison | null;
  idTodoAction?: StringFieldComparison | null;
  idReunion?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface DQReunionActionInput {
  type: string;
  idTypeAssocie: string;
  idTodoAction?: string | null;
}

export interface DQReunionActionSort {
  field: DQReunionActionSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface DQReunionAggregateFilter {
  and?: DQReunionAggregateFilter[] | null;
  or?: DQReunionAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  description?: StringFieldComparison | null;
  idUserAnimateur?: StringFieldComparison | null;
  idResponsable?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface DQReunionDeleteFilter {
  and?: DQReunionDeleteFilter[] | null;
  or?: DQReunionDeleteFilter[] | null;
  id?: IDFilterComparison | null;
  description?: StringFieldComparison | null;
  idUserAnimateur?: StringFieldComparison | null;
  idResponsable?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface DQReunionFilter {
  and?: DQReunionFilter[] | null;
  or?: DQReunionFilter[] | null;
  id?: IDFilterComparison | null;
  description?: StringFieldComparison | null;
  idUserAnimateur?: StringFieldComparison | null;
  idResponsable?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface DQReunionInput {
  description: string;
  idUserAnimateur: string;
  idResponsable: string;
  actions?: DQReunionActionInput[] | null;
  idUserParticipants?: string[] | null;
}

export interface DQReunionSort {
  field: DQReunionSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface DQSolutionInput {
  description: string;
  idUser: string;
}

export interface DQSousExempleInput {
  ordre?: number | null;
  exemple: string;
}

export interface DQSousQuestionInput {
  ordre?: number | null;
  question: string;
}

export interface DQSousThemeInput {
  ordre?: number | null;
  nom?: string | null;
}

export interface DQSousThemeOutilInput {
  idOutilAssocie: string;
  typologie: string;
}

export interface DQStatutFilter {
  and?: DQStatutFilter[] | null;
  or?: DQStatutFilter[] | null;
  id?: IDFilterComparison | null;
  code?: StringFieldComparison | null;
  type?: StringFieldComparison | null;
  libelle?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface DQStatutSort {
  field: DQStatutSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface DQThemeInput {
  ordre?: number | null;
  nom?: string | null;
}

export interface DQUserParticipantInput {
  idUser: string;
  type: string;
}

export interface DateFieldComparison {
  is?: boolean | null;
  isNot?: boolean | null;
  eq?: any | null;
  neq?: any | null;
  gt?: any | null;
  gte?: any | null;
  lt?: any | null;
  lte?: any | null;
  in?: any[] | null;
  notIn?: any[] | null;
  between?: DateFieldComparisonBetween | null;
  notBetween?: DateFieldComparisonBetween | null;
}

export interface DateFieldComparisonBetween {
  lower: any;
  upper: any;
}

export interface DeleteManyDQFicheAmeliorationsInput {
  filter: DQFicheAmeliorationDeleteFilter;
}

export interface DeleteManyDQFicheIncidentsInput {
  filter: DQFicheIncidentDeleteFilter;
}

export interface DeleteManyDQReunionsInput {
  filter: DQReunionDeleteFilter;
}

export interface DeleteOneInput {
  id: string;
}

export interface EmailInput {
  subject?: string | null;
  to: (string | null)[];
  from?: string | null;
  cc?: string | null;
  html?: string | null;
  templateId?: string | null;
  templateData?: any | null;
  attachment?: AttachmentInput | null;
}

export interface FichierInput {
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface FloatFieldComparison {
  is?: boolean | null;
  isNot?: boolean | null;
  eq?: number | null;
  neq?: number | null;
  gt?: number | null;
  gte?: number | null;
  lt?: number | null;
  lte?: number | null;
  in?: number[] | null;
  notIn?: number[] | null;
  between?: FloatFieldComparisonBetween | null;
  notBetween?: FloatFieldComparisonBetween | null;
}

export interface FloatFieldComparisonBetween {
  lower: number;
  upper: number;
}

export interface GedCategorieAnneeMoisInput {
  idPartenaireValidateur?: string | null;
  statut: string;
  validation?: string | null;
  type: string;
}

export interface GedCategorieFilter {
  and?: GedCategorieFilter[] | null;
  or?: GedCategorieFilter[] | null;
  id?: IDFilterComparison | null;
  libelle?: StringFieldComparison | null;
  base?: BooleanFieldComparison | null;
  type?: StringFieldComparison | null;
  workflowValidation?: StringFieldComparison | null;
  idPartenaireValidateur?: StringFieldComparison | null;
  validation?: BooleanFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface GedCategorieInput {
  validation?: boolean | null;
  workflowValidation?: string | null;
  idPartenaireValidateur?: string | null;
  idUserParticipants?: string[] | null;
  type?: string | null;
  libelle: string;
}

export interface GedCategorieSort {
  field: GedCategorieSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface GedDocumentChangementStatutFilter {
  and?: GedDocumentChangementStatutFilter[] | null;
  or?: GedDocumentChangementStatutFilter[] | null;
  id?: IDFilterComparison | null;
  idDocument?: StringFieldComparison | null;
  status?: StringFieldComparison | null;
  commentaire?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface GedDocumentChangementStatutInput {
  commentaire: string;
  status: string;
  idDocument: string;
}

export interface GedDocumentChangementStatutSort {
  field: GedDocumentChangementStatutSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface GedDocumentEmailInput {
  attachment?: AttachmentInput | null;
  to: (string | null)[];
  html?: string | null;
}

export interface GedDocumentFilter {
  and?: GedDocumentFilter[] | null;
  or?: GedDocumentFilter[] | null;
  id?: IDFilterComparison | null;
  description?: StringFieldComparison | null;
  nomenclature?: StringFieldComparison | null;
  numeroVersion?: StringFieldComparison | null;
  motCle1?: StringFieldComparison | null;
  motCle2?: StringFieldComparison | null;
  motCle3?: StringFieldComparison | null;
  dateHeureParution?: DateFieldComparison | null;
  dateHeureDebutValidite?: DateFieldComparison | null;
  dateHeureFinValidite?: DateFieldComparison | null;
  idUserRedacteur?: StringFieldComparison | null;
  idUserVerificateur?: StringFieldComparison | null;
  idFichier?: StringFieldComparison | null;
  idDocumentARemplacer?: StringFieldComparison | null;
  idSousCategorie?: StringFieldComparison | null;
  idOrigine?: StringFieldComparison | null;
  idOrigineAssocie?: StringFieldComparison | null;
  factureTotalHt?: FloatFieldComparison | null;
  factureTva?: FloatFieldComparison | null;
  factureTotalTtc?: FloatFieldComparison | null;
  type?: StringFieldComparison | null;
  factureDate?: DateFieldComparison | null;
  factureDateReglement?: DateFieldComparison | null;
  idReglementMode?: StringFieldComparison | null;
  numeroFacture?: StringFieldComparison | null;
  idCommande?: StringFieldComparison | null;
  isGenererCommande?: BooleanFieldComparison | null;
  numeroCommande?: IntFieldComparison | null;
  idFournisseurRapprochementAttribut?: StringFieldComparison | null;
  idClientRapprochementAttribut?: StringFieldComparison | null;
  source?: StringFieldComparison | null;
  isOcr?: BooleanFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
  sousCategorie?: GedDocumentFilterGedSousCategorieFilter | null;
}

export interface GedDocumentFilterGedSousCategorieFilter {
  and?: GedDocumentFilterGedSousCategorieFilter[] | null;
  or?: GedDocumentFilterGedSousCategorieFilter[] | null;
  id?: IDFilterComparison | null;
  libelle?: StringFieldComparison | null;
  base?: BooleanFieldComparison | null;
  workflowValidation?: StringFieldComparison | null;
  idPartenaireValidateur?: StringFieldComparison | null;
  validation?: BooleanFieldComparison | null;
  idCategorie?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface GedDocumentFournisseurInput {
  idPartenaireValidateur?: string | null;
  idPharmacie?: string | null;
  statut: string;
  validation?: string | null;
  type: string;
}

export interface GedDocumentInput {
  description?: string | null;
  nomenclature?: string | null;
  numeroVersion?: string | null;
  motCle1?: string | null;
  motCle2?: string | null;
  motCle3?: string | null;
  dateHeureParution?: any | null;
  dateHeureDebutValidite?: any | null;
  dateHeureFinValidite?: any | null;
  idUserRedacteur?: string | null;
  idUserVerificateur?: string | null;
  idDocumentARemplacer?: string | null;
  idSousCategorie?: string | null;
  type?: string | null;
  idOrigine?: string | null;
  idOrigineAssocie?: string | null;
  factureTotalHt?: number | null;
  factureTva?: number | null;
  factureTotalTtc?: number | null;
  factureDate?: any | null;
  factureDateReglement?: any | null;
  idReglementMode?: string | null;
  numeroFacture?: string | null;
  avoirType?: string | null;
  avoirCorrespondants?: string[] | null;
  isGenererCommande?: boolean | null;
  numeroCommande?: number | null;
  fichier: FichierInput;
  statut?: string | null;
}

export interface GedDocumentSort {
  field: GedDocumentSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface GedSearchDocumentInput {
  proprietaire: string;
  idSousCategorie?: string | null;
  idPartenaireValidateur?: string | null;
  accesRapide?: string | null;
  validation?: string | null;
  searchText?: string | null;
  sortBy?: string | null;
  sortDirection?: string | null;
  filterBy?: string | null;
  offset?: number | null;
  limit?: number | null;
  type?: string | null;
  idOrigine?: string | null;
  idOrigineAssocie?: string | null;
  dateDebut?: any | null;
  dateFin?: any | null;
}

export interface GedSousCategorieInput {
  workflowValidation?: string | null;
  idPartenaireValidateur?: string | null;
  idCategorie: string;
  libelle: string;
  validation?: boolean | null;
  idUserParticipants?: string[] | null;
}

export interface IDFilterComparison {
  is?: boolean | null;
  isNot?: boolean | null;
  eq?: string | null;
  neq?: string | null;
  gt?: string | null;
  gte?: string | null;
  lt?: string | null;
  lte?: string | null;
  like?: string | null;
  notLike?: string | null;
  iLike?: string | null;
  notILike?: string | null;
  in?: string[] | null;
  notIn?: string[] | null;
}

export interface ImportanceFilter {
  and?: ImportanceFilter[] | null;
  or?: ImportanceFilter[] | null;
  id?: IDFilterComparison | null;
  ordre?: IntFieldComparison | null;
  libelle?: StringFieldComparison | null;
  couleur?: StringFieldComparison | null;
  supprime?: BooleanFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface ImportanceSort {
  field: ImportanceSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface InformationLiaisonTypeFilter {
  and?: InformationLiaisonTypeFilter[] | null;
  or?: InformationLiaisonTypeFilter[] | null;
  id?: IDFilterComparison | null;
  code?: StringFieldComparison | null;
  libelle?: StringFieldComparison | null;
  supprime?: BooleanFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface InformationLiaisonTypeSort {
  field: InformationLiaisonTypeSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface IntFieldComparison {
  is?: boolean | null;
  isNot?: boolean | null;
  eq?: number | null;
  neq?: number | null;
  gt?: number | null;
  gte?: number | null;
  lt?: number | null;
  lte?: number | null;
  in?: number[] | null;
  notIn?: number[] | null;
  between?: IntFieldComparisonBetween | null;
  notBetween?: IntFieldComparisonBetween | null;
}

export interface IntFieldComparisonBetween {
  lower: number;
  upper: number;
}

export interface ItemAggregateFilter {
  and?: ItemAggregateFilter[] | null;
  or?: ItemAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  code?: StringFieldComparison | null;
  codeItem?: StringFieldComparison | null;
  idParameterGroupement?: StringFieldComparison | null;
  idParameterPharmacie?: StringFieldComparison | null;
  name?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface ItemFilter {
  and?: ItemFilter[] | null;
  or?: ItemFilter[] | null;
  id?: IDFilterComparison | null;
  code?: StringFieldComparison | null;
  codeItem?: StringFieldComparison | null;
  idParameterGroupement?: StringFieldComparison | null;
  idParameterPharmacie?: StringFieldComparison | null;
  name?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface ItemScoringAggregateFilter {
  and?: ItemScoringAggregateFilter[] | null;
  or?: ItemScoringAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  idItem?: StringFieldComparison | null;
  statut?: StringFieldComparison | null;
  idUrgence?: StringFieldComparison | null;
  idImportance?: StringFieldComparison | null;
  nomItem?: StringFieldComparison | null;
  ordreStatut?: IntFieldComparison | null;
  description?: StringFieldComparison | null;
  idItemAssocie?: StringFieldComparison | null;
  idUser?: StringFieldComparison | null;
  dateEcheance?: DateFieldComparison | null;
  coefficientUrgence?: IntFieldComparison | null;
  coefficientImportance?: IntFieldComparison | null;
  coefficientAppetance?: IntFieldComparison | null;
  nombreJoursRetard?: IntFieldComparison | null;
  score?: IntFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
}

export interface ItemScoringFilter {
  and?: ItemScoringFilter[] | null;
  or?: ItemScoringFilter[] | null;
  id?: IDFilterComparison | null;
  idItem?: StringFieldComparison | null;
  statut?: StringFieldComparison | null;
  idUrgence?: StringFieldComparison | null;
  idImportance?: StringFieldComparison | null;
  nomItem?: StringFieldComparison | null;
  ordreStatut?: IntFieldComparison | null;
  description?: StringFieldComparison | null;
  idItemAssocie?: StringFieldComparison | null;
  idUser?: StringFieldComparison | null;
  dateEcheance?: DateFieldComparison | null;
  coefficientUrgence?: IntFieldComparison | null;
  coefficientImportance?: IntFieldComparison | null;
  coefficientAppetance?: IntFieldComparison | null;
  nombreJoursRetard?: IntFieldComparison | null;
  score?: IntFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
}

export interface ItemScoringInput {
  idItem: string;
  idItemAssocie: string;
  idUser: string;
  dateEcheance?: any | null;
  coefficientUrgence: number;
  coefficientImportance: number;
  coefficientAppetance: number;
  nombreJoursRetard: number;
  score: number;
}

export interface ItemScoringPersonnalisationInput {
  type: string;
  idTypeAssocie: string;
  ordre: number;
}

export interface ItemScoringSort {
  field: ItemScoringSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface ItemScoringUpdateFilter {
  and?: ItemScoringUpdateFilter[] | null;
  or?: ItemScoringUpdateFilter[] | null;
  id?: IDFilterComparison | null;
  idItem?: StringFieldComparison | null;
  statut?: StringFieldComparison | null;
  idUrgence?: StringFieldComparison | null;
  idImportance?: StringFieldComparison | null;
  nomItem?: StringFieldComparison | null;
  ordreStatut?: IntFieldComparison | null;
  description?: StringFieldComparison | null;
  idItemAssocie?: StringFieldComparison | null;
  idUser?: StringFieldComparison | null;
  dateEcheance?: DateFieldComparison | null;
  coefficientUrgence?: IntFieldComparison | null;
  coefficientImportance?: IntFieldComparison | null;
  coefficientAppetance?: IntFieldComparison | null;
  nombreJoursRetard?: IntFieldComparison | null;
  score?: IntFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
}

export interface ItemSort {
  field: ItemSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface LaboratoireFilter {
  and?: LaboratoireFilter[] | null;
  or?: LaboratoireFilter[] | null;
  id?: IDFilterComparison | null;
  nom?: StringFieldComparison | null;
  idLaboSuite?: StringFieldComparison | null;
  idUser?: StringFieldComparison | null;
  supprime?: BooleanFieldComparison | null;
  sortie?: NumberFieldComparison | null;
  idLaboratoireRattachement?: StringFieldComparison | null;
  idPhoto?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface LaboratoireSort {
  field: LaboratoireSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface NumberFieldComparison {
  is?: boolean | null;
  isNot?: boolean | null;
  eq?: number | null;
  neq?: number | null;
  gt?: number | null;
  gte?: number | null;
  lt?: number | null;
  lte?: number | null;
  in?: number[] | null;
  notIn?: number[] | null;
  between?: NumberFieldComparisonBetween | null;
  notBetween?: NumberFieldComparisonBetween | null;
}

export interface NumberFieldComparisonBetween {
  lower: number;
  upper: number;
}

export interface OffsetPaging {
  limit?: number | null;
  offset?: number | null;
}

export interface OptionPartageFilter {
  and?: OptionPartageFilter[] | null;
  or?: OptionPartageFilter[] | null;
  id?: IDFilterComparison | null;
  idParametre?: StringFieldComparison | null;
  idType?: StringFieldComparison | null;
  codeMaj?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
  updatedBy?: StringFieldComparison | null;
  createdBy?: StringFieldComparison | null;
}

export interface OptionPartageInput {
  idParametre: string;
  idType: string;
  idPharmacie: string;
}

export interface OptionPartageSort {
  field: OptionPartageSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface OptionPartageTypeFilter {
  and?: OptionPartageTypeFilter[] | null;
  or?: OptionPartageTypeFilter[] | null;
  id?: IDFilterComparison | null;
  code?: StringFieldComparison | null;
  libelle?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface OptionPartageTypeSort {
  field: OptionPartageTypeSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface OrigineFilter {
  and?: OrigineFilter[] | null;
  or?: OrigineFilter[] | null;
  id?: IDFilterComparison | null;
  code?: StringFieldComparison | null;
  libelle?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface OrigineInput {
  code: string;
  libelle: string;
}

export interface OrigineSort {
  field: OrigineSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTAnalyseAggregateFilter {
  and?: PRTAnalyseAggregateFilter[] | null;
  or?: PRTAnalyseAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  titre?: StringFieldComparison | null;
  dateDebut?: DateFieldComparison | null;
  dateFin?: DateFieldComparison | null;
  dateChargement?: DateFieldComparison | null;
  idType?: StringFieldComparison | null;
  partenaireType?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idPartenaireTypeAssocie?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTAnalyseFilter {
  and?: PRTAnalyseFilter[] | null;
  or?: PRTAnalyseFilter[] | null;
  id?: IDFilterComparison | null;
  titre?: StringFieldComparison | null;
  dateDebut?: DateFieldComparison | null;
  dateFin?: DateFieldComparison | null;
  dateChargement?: DateFieldComparison | null;
  idType?: StringFieldComparison | null;
  partenaireType?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idPartenaireTypeAssocie?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTAnalyseInput {
  titre: string;
  dateDebut: any;
  dateFin: any;
  dateChargement: any;
  idType: string;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  fichiers?: FichierInput[] | null;
}

export interface PRTAnalyseSort {
  field: PRTAnalyseSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTAnalyseTypeFilter {
  and?: PRTAnalyseTypeFilter[] | null;
  or?: PRTAnalyseTypeFilter[] | null;
  id?: IDFilterComparison | null;
  libelle?: StringFieldComparison | null;
  code?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTAnalyseTypeSort {
  field: PRTAnalyseTypeSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTCommandeAggregateFilter {
  and?: PRTCommandeAggregateFilter[] | null;
  or?: PRTCommandeAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  idLaboratoire?: StringFieldComparison | null;
  dateCommande?: DateFieldComparison | null;
  nbrRef?: NumberFieldComparison | null;
  quantite?: IntFieldComparison | null;
  uniteGratuite?: NumberFieldComparison | null;
  prixBaseTotalHT?: NumberFieldComparison | null;
  valeurRemiseTotal?: NumberFieldComparison | null;
  prixNetTotalHT?: NumberFieldComparison | null;
  fraisPort?: NumberFieldComparison | null;
  ValeurFrancoPort?: NumberFieldComparison | null;
  remiseGlobale?: NumberFieldComparison | null;
  commentaireInterne?: StringFieldComparison | null;
  commentaireExterne?: StringFieldComparison | null;
  pathFile?: StringFieldComparison | null;
  idOperation?: StringFieldComparison | null;
  idUser?: StringFieldComparison | null;
  idCommandeType?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idPublicite?: StringFieldComparison | null;
  statut?: StringFieldComparison | null;
  idCanal?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  prixNetTotalTTC?: NumberFieldComparison | null;
  totalTVA?: NumberFieldComparison | null;
  source?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTCommandeDocumentInput {
  idDocument: string;
  idCommandes?: string[] | null;
}

export interface PRTCommandeFilter {
  and?: PRTCommandeFilter[] | null;
  or?: PRTCommandeFilter[] | null;
  id?: IDFilterComparison | null;
  idLaboratoire?: StringFieldComparison | null;
  dateCommande?: DateFieldComparison | null;
  nbrRef?: NumberFieldComparison | null;
  quantite?: IntFieldComparison | null;
  uniteGratuite?: NumberFieldComparison | null;
  prixBaseTotalHT?: NumberFieldComparison | null;
  valeurRemiseTotal?: NumberFieldComparison | null;
  prixNetTotalHT?: NumberFieldComparison | null;
  fraisPort?: NumberFieldComparison | null;
  ValeurFrancoPort?: NumberFieldComparison | null;
  remiseGlobale?: NumberFieldComparison | null;
  commentaireInterne?: StringFieldComparison | null;
  commentaireExterne?: StringFieldComparison | null;
  pathFile?: StringFieldComparison | null;
  idOperation?: StringFieldComparison | null;
  idUser?: StringFieldComparison | null;
  idCommandeType?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idPublicite?: StringFieldComparison | null;
  statut?: StringFieldComparison | null;
  idCanal?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  prixNetTotalTTC?: NumberFieldComparison | null;
  totalTVA?: NumberFieldComparison | null;
  source?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTCommandeInput {
  idLaboratoire: string;
  nbrRef: number;
  dateCommande: any;
  prixNetTotalHT?: number | null;
  totalTVA?: number | null;
  prixNetTotalTTC?: number | null;
  source?: string | null;
  statut?: string | null;
}

export interface PRTCommandeSort {
  field: PRTCommandeSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTCommandeStatutFilter {
  and?: PRTCommandeStatutFilter[] | null;
  or?: PRTCommandeStatutFilter[] | null;
  id?: IDFilterComparison | null;
  libelle?: StringFieldComparison | null;
  code?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTCommandeStatutSort {
  field: PRTCommandeStatutSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTCompteRenduAggregateFilter {
  and?: PRTCompteRenduAggregateFilter[] | null;
  or?: PRTCompteRenduAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  titre?: StringFieldComparison | null;
  remiseEchantillon?: BooleanFieldComparison | null;
  gestionPerime?: StringFieldComparison | null;
  rapportVisite?: StringFieldComparison | null;
  conclusion?: StringFieldComparison | null;
  idPartenaireTypeAssocie?: StringFieldComparison | null;
  partenaireType?: StringFieldComparison | null;
  codeMaj?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  updatedBy?: StringFieldComparison | null;
  createdBy?: StringFieldComparison | null;
}

export interface PRTCompteRenduConditionCommercialeAggregateFilter {
  and?: PRTCompteRenduConditionCommercialeAggregateFilter[] | null;
  or?: PRTCompteRenduConditionCommercialeAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  idConditionCommerciale?: StringFieldComparison | null;
  idCompteRendu?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTCompteRenduConditionCommercialeInput {
  idPartenaireTypeAssocie: string;
  partenaireType: string;
}

export interface PRTCompteRenduFilter {
  and?: PRTCompteRenduFilter[] | null;
  or?: PRTCompteRenduFilter[] | null;
  id?: IDFilterComparison | null;
  titre?: StringFieldComparison | null;
  remiseEchantillon?: BooleanFieldComparison | null;
  gestionPerime?: StringFieldComparison | null;
  rapportVisite?: StringFieldComparison | null;
  conclusion?: StringFieldComparison | null;
  idPartenaireTypeAssocie?: StringFieldComparison | null;
  partenaireType?: StringFieldComparison | null;
  codeMaj?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  updatedBy?: StringFieldComparison | null;
  createdBy?: StringFieldComparison | null;
}

export interface PRTCompteRenduInput {
  idUserParticipants?: string[] | null;
  titre: string;
  remiseEchantillon: boolean;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  idResponsables?: string[] | null;
  idSuiviOperationnels?: string[] | null;
  conditionCommerciales?: PRTConditionCommercialeInput[] | null;
  planMarketings?: PRTPlanMarketingInput[] | null;
  conclusion?: string | null;
  rapportVisite?: string | null;
  gestionPerime?: string | null;
}

export interface PRTCompteRenduPlanMarketingAggregateFilter {
  and?: PRTCompteRenduPlanMarketingAggregateFilter[] | null;
  or?: PRTCompteRenduPlanMarketingAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  idPlanMarketing?: StringFieldComparison | null;
  idCompteRendu?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTCompteRenduPlanMarketingInput {
  idPartenaireTypeAssocie: string;
  partenaireType: string;
}

export interface PRTCompteRenduSort {
  field: PRTCompteRenduSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTConditionCommercialeAggregateFilter {
  and?: PRTConditionCommercialeAggregateFilter[] | null;
  or?: PRTConditionCommercialeAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  idPartenaireTypeAssocie?: StringFieldComparison | null;
  partenaireType?: StringFieldComparison | null;
  idCanal?: StringFieldComparison | null;
  idType?: StringFieldComparison | null;
  idStatut?: StringFieldComparison | null;
  titre?: StringFieldComparison | null;
  dateDebut?: DateFieldComparison | null;
  dateFin?: DateFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  description?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTConditionCommercialeFilter {
  and?: PRTConditionCommercialeFilter[] | null;
  or?: PRTConditionCommercialeFilter[] | null;
  id?: IDFilterComparison | null;
  idPartenaireTypeAssocie?: StringFieldComparison | null;
  partenaireType?: StringFieldComparison | null;
  idCanal?: StringFieldComparison | null;
  idType?: StringFieldComparison | null;
  idStatut?: StringFieldComparison | null;
  titre?: StringFieldComparison | null;
  dateDebut?: DateFieldComparison | null;
  dateFin?: DateFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  description?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTConditionCommercialeInput {
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  idCanal?: string | null;
  idType: string;
  idStatut?: string | null;
  titre: string;
  dateDebut: any;
  dateFin: any;
  description?: string | null;
  fichiers?: FichierInput[] | null;
}

export interface PRTConditionCommercialeSort {
  field: PRTConditionCommercialeSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTConditionCommercialeStatutFilter {
  and?: PRTConditionCommercialeStatutFilter[] | null;
  or?: PRTConditionCommercialeStatutFilter[] | null;
  id?: IDFilterComparison | null;
  libelle?: StringFieldComparison | null;
  code?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTConditionCommercialeStatutSort {
  field: PRTConditionCommercialeStatutSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTConditionCommercialeTypeFilter {
  and?: PRTConditionCommercialeTypeFilter[] | null;
  or?: PRTConditionCommercialeTypeFilter[] | null;
  id?: IDFilterComparison | null;
  libelle?: StringFieldComparison | null;
  code?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTConditionCommercialeTypeSort {
  field: PRTConditionCommercialeTypeSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTContactFilter {
  and?: PRTContactFilter[] | null;
  or?: PRTContactFilter[] | null;
  id?: IDFilterComparison | null;
  civilite?: StringFieldComparison | null;
  nom?: StringFieldComparison | null;
  prenom?: StringFieldComparison | null;
  fonction?: StringFieldComparison | null;
  codeMaj?: StringFieldComparison | null;
  supprime?: BooleanFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
  idPartenaireTypeAssocie?: StringFieldComparison | null;
  partenaireType?: StringFieldComparison | null;
  updatedBy?: StringFieldComparison | null;
  createdBy?: StringFieldComparison | null;
  idPhoto?: StringFieldComparison | null;
  idAvatar?: StringFieldComparison | null;
  idContact?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
}

export interface PRTContactInput {
  civilite: string;
  nom: string;
  prenom: string;
  fonction: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  contact: ContactInputs;
  photo?: FichierInput | null;
}

export interface PRTContactSort {
  field: PRTContactSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTMiseAvantAggregateFilter {
  and?: PRTMiseAvantAggregateFilter[] | null;
  or?: PRTMiseAvantAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  libelle?: StringFieldComparison | null;
  code?: StringFieldComparison | null;
  couleur?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTMiseAvantFilter {
  and?: PRTMiseAvantFilter[] | null;
  or?: PRTMiseAvantFilter[] | null;
  id?: IDFilterComparison | null;
  libelle?: StringFieldComparison | null;
  code?: StringFieldComparison | null;
  couleur?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTMiseAvantInput {
  libelle: string;
  code: string;
  couleur?: string | null;
}

export interface PRTMiseAvantSort {
  field: PRTMiseAvantSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTPartenariatInput {
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  dateDebut: any;
  dateFin?: any | null;
  idStatut: string;
  idType: string;
}

export interface PRTPartenariatStatutFilter {
  and?: PRTPartenariatStatutFilter[] | null;
  or?: PRTPartenariatStatutFilter[] | null;
  id?: IDFilterComparison | null;
  libelle?: StringFieldComparison | null;
  code?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTPartenariatStatutSort {
  field: PRTPartenariatStatutSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTPartenariatTypeFilter {
  and?: PRTPartenariatTypeFilter[] | null;
  or?: PRTPartenariatTypeFilter[] | null;
  id?: IDFilterComparison | null;
  libelle?: StringFieldComparison | null;
  code?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTPartenariatTypeSort {
  field: PRTPartenariatTypeSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTPlanMarketingAggregateFilter {
  and?: PRTPlanMarketingAggregateFilter[] | null;
  or?: PRTPlanMarketingAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  idType?: StringFieldComparison | null;
  titre?: StringFieldComparison | null;
  description?: StringFieldComparison | null;
  dateDebut?: DateFieldComparison | null;
  dateFin?: DateFieldComparison | null;
  idStatut?: StringFieldComparison | null;
  partenaireType?: StringFieldComparison | null;
  idPartenaireTypeAssocie?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTPlanMarketingDeleteOneInput {
  id: string;
}

export interface PRTPlanMarketingFilter {
  and?: PRTPlanMarketingFilter[] | null;
  or?: PRTPlanMarketingFilter[] | null;
  id?: IDFilterComparison | null;
  idType?: StringFieldComparison | null;
  titre?: StringFieldComparison | null;
  description?: StringFieldComparison | null;
  dateDebut?: DateFieldComparison | null;
  dateFin?: DateFieldComparison | null;
  idStatut?: StringFieldComparison | null;
  partenaireType?: StringFieldComparison | null;
  idPartenaireTypeAssocie?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTPlanMarketingInput {
  idType: string;
  titre: string;
  description?: string | null;
  dateDebut: any;
  dateFin: any;
  idStatut?: string | null;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  fichiers?: FichierInput[] | null;
  idMiseAvants?: string[] | null;
  idProduits?: string[] | null;
}

export interface PRTPlanMarketingSort {
  field: PRTPlanMarketingSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTPlanMarketingStatutFilter {
  and?: PRTPlanMarketingStatutFilter[] | null;
  or?: PRTPlanMarketingStatutFilter[] | null;
  id?: IDFilterComparison | null;
  libelle?: StringFieldComparison | null;
  code?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTPlanMarketingStatutSort {
  field: PRTPlanMarketingStatutSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTPlanMarketingTypeActionFilter {
  and?: PRTPlanMarketingTypeActionFilter[] | null;
  or?: PRTPlanMarketingTypeActionFilter[] | null;
  id?: IDFilterComparison | null;
  jourLancement?: IntFieldComparison | null;
  description?: StringFieldComparison | null;
  idPlanMarketingType?: StringFieldComparison | null;
  idFonction?: StringFieldComparison | null;
  idTache?: StringFieldComparison | null;
  idImportance?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTPlanMarketingTypeActionInput {
  jourLancement: number;
  idParticipants?: string[] | null;
  description: string;
  idPlanMarketingType: string;
  idFonction?: string | null;
  idTache?: string | null;
  idImportance: string;
}

export interface PRTPlanMarketingTypeActionSort {
  field: PRTPlanMarketingTypeActionSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTPlanMarketingTypeFilter {
  and?: PRTPlanMarketingTypeFilter[] | null;
  or?: PRTPlanMarketingTypeFilter[] | null;
  id?: IDFilterComparison | null;
  libelle?: StringFieldComparison | null;
  code?: StringFieldComparison | null;
  couleur?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
}

export interface PRTPlanMarketingTypeInput {
  libelle: string;
  code: string;
  couleur?: string | null;
}

export interface PRTPlanMarketingTypeSort {
  field: PRTPlanMarketingTypeSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTPlanningMarketingInput {
  annee?: number | null;
  partenaireType: string;
  idPharmacie: string;
  searchText?: string | null;
  idPartenaireTypeAssocie?: string | null;
  offset?: number | null;
  limit?: number | null;
  dateDebut?: any | null;
  dateFin?: any | null;
}

export interface PRTReglementModeFilter {
  and?: PRTReglementModeFilter[] | null;
  or?: PRTReglementModeFilter[] | null;
  id?: IDFilterComparison | null;
  code?: StringFieldComparison | null;
  libelle?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTReglementModeSort {
  field: PRTReglementModeSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTRemunerationAggregateFilter {
  and?: PRTRemunerationAggregateFilter[] | null;
  or?: PRTRemunerationAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  dateEcheance?: DateFieldComparison | null;
  montantPrevu?: NumberFieldComparison | null;
  montantRealise?: NumberFieldComparison | null;
  idPartenaireTypeAssocie?: StringFieldComparison | null;
  partenaireType?: StringFieldComparison | null;
  idPrestationType?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  updatedBy?: StringFieldComparison | null;
  createdBy?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTRemunerationAnneeInput {
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  date: any;
}

export interface PRTRemunerationEvolutionInput {
  dateDebut: any;
  dateFin?: any | null;
  idPartenaireTypeAssocie?: string | null;
  partenaireType: string;
  indexMonth?: number | null;
}

export interface PRTRemunerationFilter {
  and?: PRTRemunerationFilter[] | null;
  or?: PRTRemunerationFilter[] | null;
  id?: IDFilterComparison | null;
  dateEcheance?: DateFieldComparison | null;
  montantPrevu?: NumberFieldComparison | null;
  montantRealise?: NumberFieldComparison | null;
  idPartenaireTypeAssocie?: StringFieldComparison | null;
  partenaireType?: StringFieldComparison | null;
  idPrestationType?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  updatedBy?: StringFieldComparison | null;
  createdBy?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTRemunerationInput {
  dateEcheance: any;
  montantPrevu?: number | null;
  montantRealise: number;
  idPrestationType: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
}

export interface PRTRemunerationMensuelleInput {
  dateDebut: any;
  dateFin?: any | null;
  idPartenaireTypeAssocie?: string | null;
  partenaireType: string;
}

export interface PRTRemunerationPrevueExistInput {
  dateEcheance: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idPrestationType: string;
}

export interface PRTRemunerationReglementAggregateFilter {
  and?: PRTRemunerationReglementAggregateFilter[] | null;
  or?: PRTRemunerationReglementAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  idPrestationType?: StringFieldComparison | null;
  idModeReglement?: StringFieldComparison | null;
  idRemuneration?: StringFieldComparison | null;
  montant?: NumberFieldComparison | null;
  description?: StringFieldComparison | null;
  dateReglement?: DateFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
  updatedBy?: StringFieldComparison | null;
  createdBy?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  partenaireType?: StringFieldComparison | null;
  idPartenaireTypeAssocie?: StringFieldComparison | null;
}

export interface PRTRemunerationReglementFilter {
  and?: PRTRemunerationReglementFilter[] | null;
  or?: PRTRemunerationReglementFilter[] | null;
  id?: IDFilterComparison | null;
  idPrestationType?: StringFieldComparison | null;
  idModeReglement?: StringFieldComparison | null;
  idRemuneration?: StringFieldComparison | null;
  montant?: NumberFieldComparison | null;
  description?: StringFieldComparison | null;
  dateReglement?: DateFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
  updatedBy?: StringFieldComparison | null;
  createdBy?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  partenaireType?: StringFieldComparison | null;
  idPartenaireTypeAssocie?: StringFieldComparison | null;
}

export interface PRTRemunerationReglementInput {
  idPrestationType: string;
  idModeReglement: string;
  idRemuneration?: string | null;
  montant: number;
  description?: string | null;
  idPartenaireTypeAssocie: string;
  dateReglement: any;
  partenaireType: string;
  fichiers?: FichierInput[] | null;
}

export interface PRTRemunerationReglementSort {
  field: PRTRemunerationReglementSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTRemunerationSort {
  field: PRTRemunerationSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTRemunerationsEvolutionsInput {
  partenaireType: string;
  dateDebut: any;
  dateFin?: any | null;
  skip: number;
  take: number;
}

export interface PRTRendezVousAggregateFilter {
  and?: PRTRendezVousAggregateFilter[] | null;
  or?: PRTRendezVousAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  ordreJour?: StringFieldComparison | null;
  idSubject?: StringFieldComparison | null;
  heureDebut?: StringFieldComparison | null;
  heureFin?: StringFieldComparison | null;
  statut?: StringFieldComparison | null;
  dateRendezVous?: DateFieldComparison | null;
  note?: StringFieldComparison | null;
  idPartenaireTypeAssocie?: StringFieldComparison | null;
  partenaireType?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  createdBy?: StringFieldComparison | null;
  updatedBy?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTRendezVousFilter {
  and?: PRTRendezVousFilter[] | null;
  or?: PRTRendezVousFilter[] | null;
  id?: IDFilterComparison | null;
  ordreJour?: StringFieldComparison | null;
  idSubject?: StringFieldComparison | null;
  heureDebut?: StringFieldComparison | null;
  heureFin?: StringFieldComparison | null;
  statut?: StringFieldComparison | null;
  dateRendezVous?: DateFieldComparison | null;
  note?: StringFieldComparison | null;
  idPartenaireTypeAssocie?: StringFieldComparison | null;
  partenaireType?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  createdBy?: StringFieldComparison | null;
  updatedBy?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTRendezVousInput {
  idSubject?: string | null;
  heureDebut: string;
  heureFin?: string | null;
  statut?: string | null;
  dateRendezVous: any;
  idUserParticipants?: string[] | null;
  idInvites?: string[] | null;
  note?: string | null;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
}

export interface PRTRendezVousSort {
  field: PRTRendezVousSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTRendezVousSujetVisiteFilter {
  and?: PRTRendezVousSujetVisiteFilter[] | null;
  or?: PRTRendezVousSujetVisiteFilter[] | null;
  id?: IDFilterComparison | null;
  libelle?: StringFieldComparison | null;
  code?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTRendezVousSujetVisiteSort {
  field: PRTRendezVousSujetVisiteSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTSuiviOperationnelAggregateFilter {
  and?: PRTSuiviOperationnelAggregateFilter[] | null;
  or?: PRTSuiviOperationnelAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  idTypeAssocie?: StringFieldComparison | null;
  partenaireType?: StringFieldComparison | null;
  montant?: NumberFieldComparison | null;
  idImportance?: StringFieldComparison | null;
  idTache?: StringFieldComparison | null;
  idFonction?: StringFieldComparison | null;
  idType?: StringFieldComparison | null;
  idStatut?: StringFieldComparison | null;
  titre?: StringFieldComparison | null;
  dateHeure?: DateFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  createdBy?: StringFieldComparison | null;
  description?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTSuiviOperationnelFilter {
  and?: PRTSuiviOperationnelFilter[] | null;
  or?: PRTSuiviOperationnelFilter[] | null;
  id?: IDFilterComparison | null;
  idTypeAssocie?: StringFieldComparison | null;
  partenaireType?: StringFieldComparison | null;
  montant?: NumberFieldComparison | null;
  idImportance?: StringFieldComparison | null;
  idTache?: StringFieldComparison | null;
  idFonction?: StringFieldComparison | null;
  idType?: StringFieldComparison | null;
  idStatut?: StringFieldComparison | null;
  titre?: StringFieldComparison | null;
  dateHeure?: DateFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  createdBy?: StringFieldComparison | null;
  description?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTSuiviOperationnelInput {
  idParticipants?: string[] | null;
  idContacts?: string[] | null;
  idTypeAssocie: string;
  montant: number;
  idImportance?: string | null;
  idTache?: string | null;
  idFonction?: string | null;
  partenaireType: string;
  idType: string;
  idStatut?: string | null;
  titre: string;
  dateHeure: any;
  description?: string | null;
  fichiers?: FichierInput[] | null;
}

export interface PRTSuiviOperationnelSort {
  field: PRTSuiviOperationnelSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTSuiviOperationnelStatutFilter {
  and?: PRTSuiviOperationnelStatutFilter[] | null;
  or?: PRTSuiviOperationnelStatutFilter[] | null;
  id?: IDFilterComparison | null;
  libelle?: StringFieldComparison | null;
  code?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTSuiviOperationnelStatutSort {
  field: PRTSuiviOperationnelStatutSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PRTSuiviOperationnelTypeFilter {
  and?: PRTSuiviOperationnelTypeFilter[] | null;
  or?: PRTSuiviOperationnelTypeFilter[] | null;
  id?: IDFilterComparison | null;
  libelle?: StringFieldComparison | null;
  code?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PRTSuiviOperationnelTypeSort {
  field: PRTSuiviOperationnelTypeSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface PilotageStatistiqueInput {
  codeItem: string;
  uniteTemps?: string | null;
  idUserParticipants?: string[] | null;
  dateDebut?: any | null;
  dateFin?: any | null;
}

export interface PrestationTypeFilter {
  and?: PrestationTypeFilter[] | null;
  or?: PrestationTypeFilter[] | null;
  id?: IDFilterComparison | null;
  code?: StringFieldComparison | null;
  libelle?: StringFieldComparison | null;
  couleur?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface PrestationTypeSort {
  field: PrestationTypeSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface RTCapteurFilter {
  and?: RTCapteurFilter[] | null;
  or?: RTCapteurFilter[] | null;
  id?: IDFilterComparison | null;
  nom?: StringFieldComparison | null;
  numeroSerie?: StringFieldComparison | null;
  idPointAcces?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface RTCapteurSort {
  field: RTCapteurSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface RTFrigoAggregateFilter {
  and?: RTFrigoAggregateFilter[] | null;
  or?: RTFrigoAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  nom?: StringFieldComparison | null;
  temperatureBasse?: NumberFieldComparison | null;
  temperatureHaute?: NumberFieldComparison | null;
  temporisation?: NumberFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  typeReleve?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface RTFrigoCapteurInput {
  nom: string;
  numeroSerie: string;
  pointAcces: RTFrigoPointAccesInput;
}

export interface RTFrigoFilter {
  and?: RTFrigoFilter[] | null;
  or?: RTFrigoFilter[] | null;
  id?: IDFilterComparison | null;
  nom?: StringFieldComparison | null;
  temperatureBasse?: NumberFieldComparison | null;
  temperatureHaute?: NumberFieldComparison | null;
  temporisation?: NumberFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  typeReleve?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface RTFrigoInput {
  nom: string;
  temperatureBasse: number;
  temperatureHaute: number;
  temporisation: number;
  typeReleve: string;
  idTraitements?: string[] | null;
  capteurs?: RTFrigoCapteurInput[] | null;
}

export interface RTFrigoPointAccesInput {
  nom: string;
  numeroSerie: string;
}

export interface RTFrigoSort {
  field: RTFrigoSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface RTPointAccesFilter {
  and?: RTPointAccesFilter[] | null;
  or?: RTPointAccesFilter[] | null;
  id?: IDFilterComparison | null;
  nom?: StringFieldComparison | null;
  numeroSerie?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface RTPointAccesSort {
  field: RTPointAccesSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface RTReleveAggregateFilter {
  and?: RTReleveAggregateFilter[] | null;
  or?: RTReleveAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  temperature?: NumberFieldComparison | null;
  idUser?: StringFieldComparison | null;
  idStatus?: StringFieldComparison | null;
  idFrigo?: StringFieldComparison | null;
  type?: StringFieldComparison | null;
  idCapteur?: StringFieldComparison | null;
  idPointAcces?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface RTReleveFilter {
  and?: RTReleveFilter[] | null;
  or?: RTReleveFilter[] | null;
  id?: IDFilterComparison | null;
  temperature?: NumberFieldComparison | null;
  idUser?: StringFieldComparison | null;
  idStatus?: StringFieldComparison | null;
  idFrigo?: StringFieldComparison | null;
  type?: StringFieldComparison | null;
  idCapteur?: StringFieldComparison | null;
  idPointAcces?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface RTReleveInput {
  temperature?: number | null;
  type: string;
  idStatus?: string | null;
  idFrigo: string;
  idCapteur?: string | null;
  idPointAcces?: string | null;
}

export interface RTReleveSort {
  field: RTReleveSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface RTStatusFilter {
  and?: RTStatusFilter[] | null;
  or?: RTStatusFilter[] | null;
  id?: IDFilterComparison | null;
  code?: StringFieldComparison | null;
  couleur?: StringFieldComparison | null;
  libelle?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface RTStatusSort {
  field: RTStatusSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface StringFieldComparison {
  is?: boolean | null;
  isNot?: boolean | null;
  eq?: string | null;
  neq?: string | null;
  gt?: string | null;
  gte?: string | null;
  lt?: string | null;
  lte?: string | null;
  like?: string | null;
  notLike?: string | null;
  iLike?: string | null;
  notILike?: string | null;
  in?: string[] | null;
  notIn?: string[] | null;
}

export interface SuiviAppelInput {
  idUserDeclarant: string;
  idUserInterlocuteur: string;
  idFonction?: string | null;
  idTache?: string | null;
  idImportance: string;
  idUrgence: string;
  idUserParticipants?: string[] | null;
  fichiers?: FichierInput[] | null;
  commentaire?: string | null;
}

export interface SuiviAppelTypeAggregateFilter {
  and?: SuiviAppelTypeAggregateFilter[] | null;
  or?: SuiviAppelTypeAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  idUserDeclarant?: StringFieldComparison | null;
  idUserInterlocuteur?: StringFieldComparison | null;
  idFonction?: StringFieldComparison | null;
  idTache?: StringFieldComparison | null;
  idImportance?: StringFieldComparison | null;
  idUrgence?: StringFieldComparison | null;
  commentaire?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  status?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface SuiviAppelTypeFilter {
  and?: SuiviAppelTypeFilter[] | null;
  or?: SuiviAppelTypeFilter[] | null;
  id?: IDFilterComparison | null;
  idUserDeclarant?: StringFieldComparison | null;
  idUserInterlocuteur?: StringFieldComparison | null;
  idFonction?: StringFieldComparison | null;
  idTache?: StringFieldComparison | null;
  idImportance?: StringFieldComparison | null;
  idUrgence?: StringFieldComparison | null;
  commentaire?: StringFieldComparison | null;
  idGroupement?: StringFieldComparison | null;
  status?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface SuiviAppelTypeSort {
  field: SuiviAppelTypeSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface TATraitementAggregateFilter {
  and?: TATraitementAggregateFilter[] | null;
  or?: TATraitementAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  description?: StringFieldComparison | null;
  idType?: StringFieldComparison | null;
  idFonction?: StringFieldComparison | null;
  idTache?: StringFieldComparison | null;
  idImportance?: StringFieldComparison | null;
  idUrgence?: StringFieldComparison | null;
  repetition?: IntFieldComparison | null;
  repetitionUnite?: StringFieldComparison | null;
  heure?: StringFieldComparison | null;
  jourMois?: IntFieldComparison | null;
  moisAnnee?: IntFieldComparison | null;
  dateHeureFin?: DateFieldComparison | null;
  nombreOccurencesFin?: IntFieldComparison | null;
  dateDebut?: DateFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  isPrivate?: BooleanFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface TATraitementExecutionAggregateFilter {
  and?: TATraitementExecutionAggregateFilter[] | null;
  or?: TATraitementExecutionAggregateFilter[] | null;
  id?: IDFilterComparison | null;
  idTraitement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  dateEcheance?: DateFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface TATraitementExecutionChangementStatutInput {
  idTraitementExecution: string;
  status: string;
  commentaire: string;
}

export interface TATraitementExecutionFilter {
  and?: TATraitementExecutionFilter[] | null;
  or?: TATraitementExecutionFilter[] | null;
  id?: IDFilterComparison | null;
  idTraitement?: StringFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  dateEcheance?: DateFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface TATraitementExecutionSort {
  field: TATraitementExecutionSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface TATraitementFilter {
  and?: TATraitementFilter[] | null;
  or?: TATraitementFilter[] | null;
  id?: IDFilterComparison | null;
  description?: StringFieldComparison | null;
  idType?: StringFieldComparison | null;
  idFonction?: StringFieldComparison | null;
  idTache?: StringFieldComparison | null;
  idImportance?: StringFieldComparison | null;
  idUrgence?: StringFieldComparison | null;
  repetition?: IntFieldComparison | null;
  repetitionUnite?: StringFieldComparison | null;
  heure?: StringFieldComparison | null;
  jourMois?: IntFieldComparison | null;
  moisAnnee?: IntFieldComparison | null;
  dateHeureFin?: DateFieldComparison | null;
  nombreOccurencesFin?: IntFieldComparison | null;
  dateDebut?: DateFieldComparison | null;
  idPharmacie?: StringFieldComparison | null;
  isPrivate?: BooleanFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface TATraitementInput {
  description: string;
  idType: string;
  idFonction?: string | null;
  idTache?: string | null;
  idImportance: string;
  idUrgence: string;
  repetition: number;
  repetitionUnite: string;
  heure: string;
  joursSemaine?: number[] | null;
  jourMois?: number | null;
  moisAnnee?: number | null;
  dateHeureFin?: any | null;
  dateDebut: any;
  nombreOccurencesFin?: number | null;
  idUserParticipants?: string[] | null;
  isPrivate: boolean;
}

export interface TATraitementSort {
  field: TATraitementSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface TATraitementTypeFilter {
  and?: TATraitementTypeFilter[] | null;
  or?: TATraitementTypeFilter[] | null;
  id?: IDFilterComparison | null;
  code?: StringFieldComparison | null;
  libelle?: StringFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface TATraitementTypeSort {
  field: TATraitementTypeSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface TodoActionFilter {
  and?: TodoActionFilter[] | null;
  or?: TodoActionFilter[] | null;
  id?: IDFilterComparison | null;
  ordre?: NumberFieldComparison | null;
  description?: StringFieldComparison | null;
  dateDebut?: DateFieldComparison | null;
  dateFin?: DateFieldComparison | null;
  idActionParent?: StringFieldComparison | null;
  idItemAssocie?: StringFieldComparison | null;
  idImportance?: StringFieldComparison | null;
  status?: StringFieldComparison | null;
  supprime?: BooleanFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface TodoActionSort {
  field: TodoActionSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

export interface UpdateManyItemScoringsInput {
  filter: ItemScoringUpdateFilter;
  update: ItemScoringInput;
}

export interface UpdateOneDQMTFonctionInput {
  id: string;
  update: DQMTFonctionInput;
}

export interface UpdateOneDQMTTacheInput {
  id: string;
  update: DQMTTacheInput;
}

export interface UpdateOneItemScoringInput {
  id: string;
  update: ItemScoringInput;
}

export interface UpdateOnePRTCommandeInput {
  id: string;
  update: PRTCommandeInput;
}

export interface UpdateOnePRTMiseAvantInput {
  id: string;
  update: PRTMiseAvantInput;
}

export interface UpdateOnePRTPlanMarketingTypeInput {
  id: string;
  update: PRTPlanMarketingTypeInput;
}

export interface UrgenceFilter {
  and?: UrgenceFilter[] | null;
  or?: UrgenceFilter[] | null;
  id?: IDFilterComparison | null;
  code?: StringFieldComparison | null;
  libelle?: StringFieldComparison | null;
  couleur?: StringFieldComparison | null;
  supprime?: BooleanFieldComparison | null;
  createdAt?: DateFieldComparison | null;
  updatedAt?: DateFieldComparison | null;
}

export interface UrgenceSort {
  field: UrgenceSortFields;
  direction: SortDirection;
  nulls?: SortNulls | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
