import gql from 'graphql-tag';

export const GET_PARAMETER_LOCAL = gql`
  {
    parameter @client {
      idFamilles
      idLaboratoires
    }
  }
`;
