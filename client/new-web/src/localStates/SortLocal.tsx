import gql from 'graphql-tag';

const GET_LOCAL_SORT = gql`
  {
    sort @client {
      sortItem {
        label
        name
        direction
        active
      }
    }
  }
`;

export default GET_LOCAL_SORT;
