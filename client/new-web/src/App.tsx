import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import ThemeProvider from './Theme/ThemeProvider';
import { GlobalSnackBar } from './components/Common/GlobalSnackBar';
import ForgotPassword from './components/Authentication/Password/ForgotPassword';
import ResetPassword from './components/Authentication/Password/ResetPassword';
import Loadman from './components/Loading/Loadman';
import withUser from './components/Common/withUser';
import CreateGroupement from './components/Groupement/Create';
import SelectGroupement from './components/Groupement';
import Navigation from './components/Navigation';
import UpdatePassword from './components/Authentication/Password/UpdatePassword';
import { SetPassword } from './components/Authentication/Password/SetPassword';
import UpdateGroupement from './components/Groupement/Update';
import './App.css';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import IpValidation from './components/Authentication/SignIn/IpValidation/IpValidation';
import { SUPER_ADMINISTRATEUR } from './Constant/roles';
import { setIp } from './services/LocalStorage';
import reduxStore from './redux/store';
import { Provider as ReduxProvider } from 'react-redux';

const SignIn = Loadman(import('./components/Authentication/SignIn'));
const SsoSignIn = Loadman(import('./components/Authentication/Sso-signin'));

const App = () => {
  React.useEffect(() => {
    fetch('https://api.ipify.org/?format=json')
      .then(results => results.json())
      .then(data => {
        setIp(data.ip);
      });
  }, []);

  return (
    <ReduxProvider store={reduxStore}>
      <ThemeProvider>
        <GlobalSnackBar />
        <HashRouter>
          <Switch>
            <Route exact={true} path="/signin" component={SignIn} />
            <Route exact={true} path="/sso-signin" component={SsoSignIn} />
            <Route exact={true} path="/password" component={ForgotPassword} />
            <Route exact={true} path="/password/reset/:token" component={ResetPassword} />
            <Route exact={true} path="/password/set/:token" component={SetPassword} />
            <Route exact={true} path="/password/update" component={withUser(UpdatePassword)} />
            <Route path="/ip/:token/:status" exact={true} component={IpValidation} />
            <Route
              exact={true}
              path="/groupement/choice"
              component={withUser(SelectGroupement, { permitRoles: [SUPER_ADMINISTRATEUR] })}
            />
            <Route
              exact={true}
              path="/groupement/new"
              component={withUser(CreateGroupement, { permitRoles: [SUPER_ADMINISTRATEUR] })}
            />
            <Route
              exact={true}
              path="/groupement/edit"
              component={withUser(UpdateGroupement, { permitRoles: [SUPER_ADMINISTRATEUR] })}
            />
            <Route
              exact={true}
              strict={true}
              path="/*"
              component={withUser(Navigation, {
                withGroupement: true,
                withLastPasswordChanged: true,
              })}
            />
          </Switch>
        </HashRouter>
      </ThemeProvider>
    </ReduxProvider>
  );
};

export default App;
