const HtmlWebpackPlugin = require('html-webpack-plugin');
//const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const Dotenv = require('dotenv-webpack');
const ModuleFederationPlugin = require('webpack').container.ModuleFederationPlugin;
const { HotModuleReplacementPlugin } = require("webpack");
const path = require('path');
const deps = require('./package.json').dependencies;

const singletonDeps = Object.keys(deps).reduce((singletonResult, name) => {
  return {
    ...singletonResult,
    [name]: {
      singleton: true,
    },
  };
}, {});

module.exports = {
  entry: './src/index',
  mode: 'development',
 // target: 'node',
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    hot: true,
    historyApiFallback: true,
    port: 3020,
  },
  watchOptions: {
    ignored: /node_modules/,
  },
  output: {
    publicPath: 'auto',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    fallback: {
      "path": require.resolve("path-browserify")
    }
  },
  module: {
    rules: [
      {
        test: /bootstrap\.tsx$/,
        loader: 'bundle-loader',
        options: {
          lazy: true,
        },
      },
      {
        test: /\.tsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          presets: ['@babel/preset-react', '@babel/preset-typescript', ["@babel/preset-env", { useBuiltIns: 'usage', "corejs": 3, "targets": "> 0.25%, not dead","modules": false }]],
          plugins: ["@babel/proposal-class-properties", "@babel/transform-runtime"/*, {
            "regenerator": true,
            "corejs": 3
          }]*/]
        },
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/i,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      },
      {
        test: /\.css$/i,
        use: [/*{
          loader: MiniCssExtractPlugin.loader,
          options: {
            esModule: false,
          },
        },*/ 'style-loader', 'css-loader'],
      },
    ],
  },
  plugins: [
    new Dotenv(),
    new ModuleFederationPlugin({
      name: 'web',
      filename: 'remoteEntry.js',
      library: { type: 'var', name: 'web' },
      exposes: {
        './module': './src/module',
      },
      shared: {
        ...deps,
        react: { singleton: true, eager: true, requiredVersion: deps.react },
        "react-dom": {
          singleton: true, eager: true, requiredVersion: deps["react-dom"],
        },
        '@app/ui-kit': {
          singleton: true,
        },
        '@app/types': {
          singleton: true,
        },
      },

    }),
    new CleanWebpackPlugin(),
    new HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      favicon: './public/gcr_pharma.png',
      template: './public/index.html',
    })/*,
    new MiniCssExtractPlugin({
      filename: "styles.[contentHash].css",
      ignoreOrder: false, 
  }),*/
  ],
};
