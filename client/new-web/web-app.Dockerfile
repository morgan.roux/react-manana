# Use Node 12
FROM node:12-alpine

WORKDIR /app

COPY . /app/

RUN yarn install 

RUN yarn build

EXPOSE 3020

CMD ["yarn", "start"]
